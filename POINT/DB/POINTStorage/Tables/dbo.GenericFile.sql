CREATE TABLE [dbo].[GenericFile]
(
[GenericFileID] [uniqueidentifier] NOT NULL,
[FileName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FileSize] [int] NOT NULL,
[FileData] [image] NOT NULL,
[ContentType] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TimeStamp] [datetime] NOT NULL,
[Deleted] [bit] NOT NULL CONSTRAINT [DF_GenericFile_Deleted] DEFAULT ((0))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[GenericFile] ADD CONSTRAINT [PK_GenericFile] PRIMARY KEY CLUSTERED  ([GenericFileID]) ON [PRIMARY]
GO
