IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'point')
CREATE LOGIN [point] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [point] FOR LOGIN [point]
GO
