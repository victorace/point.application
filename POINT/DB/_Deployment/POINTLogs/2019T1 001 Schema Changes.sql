/*
Run this script on:

        pointtest.verzorgdeoverdracht.nl.TXX-DB-POINTLOGS-R    -  This database will be modified

to synchronize it with:

        ..PointLogs

You are recommended to back up your database before running this script

Script created by SQL Compare version 12.3.3.4490 from Red Gate Software Ltd at 17/12/2018 12:22:21

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[LogRead]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
IF COL_LENGTH(N'[dbo].[LogRead]', N'RoleName') IS NULL
ALTER TABLE [dbo].[LogRead] ADD[RoleName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL
IF COL_LENGTH(N'[dbo].[LogRead]', N'RoleName') IS NULL
ALTER TABLE [dbo].[LogRead] ADD
[RoleName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
