
UPDATE dbo.MutClient SET CompositionHousekeeping = '0' WHERE CompositionHousekeeping IN ('Alleenstaand')
GO
UPDATE dbo.MutClient SET CompositionHousekeeping = '1' WHERE CompositionHousekeeping IN ('Uitsluitend met partner')
GO
UPDATE dbo.MutClient SET CompositionHousekeeping = '2' WHERE CompositionHousekeeping IN ('Met partner en kinderen', 'Zonder partner met kinderen', 'Heeft huishouden van volwassenen met 1 of meer kinderen')
GO
UPDATE dbo.MutClient SET CompositionHousekeeping = '4' WHERE CompositionHousekeeping IN ('Woont als kind samen met ouders in ouderlijk huis')
GO
UPDATE dbo.MutClient SET CompositionHousekeeping = '7' WHERE CompositionHousekeeping IN ('Heeft een ander meerpersoonshuishouden', 'Woont in zorginstelling met verblijf', 'Anders')
GO
UPDATE dbo.MutClient SET CompositionHousekeeping = '7' where CompositionHousekeeping not in ('0', '1', '2', '4', '7', '') and CompositionHousekeeping is not null
GO
UPDATE dbo.MutClient SET CompositionHousekeeping = NULL where CompositionHousekeeping = ''
GO


UPDATE dbo.MutClient SET Gender = '1' WHERE Gender IN ('OM','m')
GO
UPDATE dbo.MutClient SET Gender = '2' WHERE Gender IN ('OF','v')
GO