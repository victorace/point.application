

/*

[dbo].[AfterCareType]

*/


UPDATE [dbo].[AfterCareType] SET [Description] = 'Somatiek' WHERE AfterCareTypeID = 1
UPDATE [dbo].[AfterCareType] SET [Description] = 'Psychische Gezondheidszorg' WHERE AfterCareTypeID = 2
UPDATE [dbo].[AfterCareType] SET [Description] = 'Psychische Gezondheidszorg met BOPZ verklaring' WHERE AfterCareTypeID = 3
UPDATE [dbo].[AfterCareType] SET [Description] = 'Eerste Lijns Verblijf - Laag-complex' WHERE AfterCareTypeID = 4
UPDATE [dbo].[AfterCareType] SET [Description] = 'Eerste Lijns Verblijf - Hoog-complex' WHERE AfterCareTypeID = 5
UPDATE [dbo].[AfterCareType] SET [Description] = 'Eerste Lijns Verblijf - Palliatief' WHERE AfterCareTypeID = 6
UPDATE [dbo].[AfterCareType] SET [Description] = 'Geriatrische Revalidatie Zorg - CVA' WHERE AfterCareTypeID = 7
UPDATE [dbo].[AfterCareType] SET [Description] = 'Geriatrische Revalidatie Zorg - Orthopedie' WHERE AfterCareTypeID = 8
UPDATE [dbo].[AfterCareType] SET [Description] = 'Geriatrische Revalidatie Zorg - Amputatie' WHERE AfterCareTypeID = 10
UPDATE [dbo].[AfterCareType] SET [Description] = 'Geriatrische Revalidatie Zorg - Overig (niet gespecificeerd)' WHERE AfterCareTypeID = 11
UPDATE [dbo].[AfterCareType] SET [Description] = 'Ziekenhuis verplaatste Zorg' WHERE AfterCareTypeID = 12
UPDATE [dbo].[AfterCareType] SET [Description] = 'Medisch Specialistische Revalidatie' WHERE AfterCareTypeID = 13
UPDATE [dbo].[AfterCareType] SET [Description] = 'Kindzorg intramuraal' WHERE AfterCareTypeID = 14
UPDATE [dbo].[AfterCareType] SET [Description] = 'Overig intramuraal' WHERE AfterCareTypeID = 15
UPDATE [dbo].[AfterCareType] SET [Description] = 'Huishoudelijke Verzorging' WHERE AfterCareTypeID = 18
UPDATE [dbo].[AfterCareType] SET [Description] = 'Persoonlijke Verzorging/Verpleging' WHERE AfterCareTypeID = 19
UPDATE [dbo].[AfterCareType] SET [Description] = 'Medisch Specialistische Verpleging Thuis' WHERE AfterCareTypeID = 20
UPDATE [dbo].[AfterCareType] SET [Description] =  'Thuiszorg' WHERE AfterCareTypeID = 24
UPDATE [dbo].[AfterCareType] SET [Description] = 'Geriatrische Revalidatie Zorg' WHERE AfterCareTypeID = 36
UPDATE [dbo].[AfterCareType] SET [Description] = 'Geriatrische Revalidatie Zorg - Trauma' WHERE AfterCareTypeID = 37
UPDATE [dbo].[AfterCareType] SET [Description] = 'Crisisbed, noodbed' WHERE AfterCareTypeID = 38
UPDATE [dbo].[AfterCareType] SET [Description] = 'Gespecialiseerde Verpleging' WHERE AfterCareTypeID = 39


/*

[dbo].[Region]

*/

UPDATE [dbo].[Region] SET [CapacityBedsPublic] = 1 WHERE [RegionID] = 1		-- Den Haag
UPDATE [dbo].[Region] SET [CapacityBedsPublic] = 1 WHERE [RegionID] = 2		-- Delft
UPDATE [dbo].[Region] SET [CapacityBedsPublic] = 1 WHERE [RegionID] = 16	-- Apeldoorn-Zutphen
UPDATE [dbo].[Region] SET [CapacityBedsPublic] = 1 WHERE [RegionID] = 18	-- ZZ_Demo
UPDATE [dbo].[Region] SET [CapacityBedsPublic] = 1 WHERE [RegionID] = 20	-- Leiden e.o.
UPDATE [dbo].[Region] SET [CapacityBedsPublic] = 1 WHERE [RegionID] = 21	-- Haarlem-Hoofddorp
UPDATE [dbo].[Region] SET [CapacityBedsPublic] = 1 WHERE [RegionID] = 25	-- Zwolle-Meppel
UPDATE [dbo].[Region] SET [CapacityBedsPublic] = 1 WHERE [RegionID] = 28	-- Deventer
UPDATE [dbo].[Region] SET [CapacityBedsPublic] = 1 WHERE [RegionID] = 31	-- Assen
UPDATE [dbo].[Region] SET [CapacityBedsPublic] = 1 WHERE [RegionID] = 32	-- Hoogeveen
UPDATE [dbo].[Region] SET [CapacityBedsPublic] = 1 WHERE [RegionID] = 33	-- Emmen
UPDATE [dbo].[Region] SET [CapacityBedsPublic] = 1 WHERE [RegionID] = 34	-- Zaandam
UPDATE [dbo].[Region] SET [CapacityBedsPublic] = 1 WHERE [RegionID] = 35	-- Zeeland
