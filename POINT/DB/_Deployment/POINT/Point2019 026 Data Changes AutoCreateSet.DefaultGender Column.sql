UPDATE dbo.AutoCreateSet SET DefaultGender = '1' WHERE DefaultGender = 'm'
GO
UPDATE dbo.AutoCreateSet SET DefaultGender = '2' WHERE DefaultGender = 'v'
GO
UPDATE dbo.AutoCreateSet SET DefaultGender = '0' WHERE DefaultGender = 'o'
GO
UPDATE dbo.AutoCreateSet SET DefaultGender = '0' WHERE DefaultGender IS NULL OR DefaultGender NOT IN ('0','1','2')
GO
