DISABLE TRIGGER dbo.Client_InsertUpdate ON dbo.Client
GO

DECLARE @NewLan TABLE
(
  OldId int, 
  [NewID] INT,
  Name VARCHAR(100)
)

INSERT INTO @NewLan (OldId, NewID) VALUES  
(1,1),
(2,2),
(5,3),
(6,4),
(7,5),
(8,6),
(9,7),
(10,8),
(11,9),
(12,10),
(13,11),
(14,12),
(15,13),
(16,14),
(17,15),
(18,16),
(19,17),
(20,18),
(21,19),
(22,20),
(23,21),
(24,22),
(25,23),
(26,24),
(27,25),
(28,26),
(29,27),
(30,28)

UPDATE client SET LanguageID = 15 WHERE LanguageID = 3
UPDATE client SET LanguageID = 30 WHERE LanguageID = 4
UPDATE client SET LanguageID = 12 WHERE LanguageID = 31
UPDATE client SET LanguageID = 12 WHERE LanguageID = 32


update cl SET cl.LanguageID = l.[NewID]
FROM dbo.Client cl
INNER JOIN @NewLan l ON l.OldId = cl.LanguageID
GO

ENABLE TRIGGER dbo.Client_InsertUpdate ON dbo.Client
GO