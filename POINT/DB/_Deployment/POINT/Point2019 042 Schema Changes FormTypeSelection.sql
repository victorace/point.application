/*
Run this script on:

        pointtest.verzorgdeoverdracht.nl.TXX-DB-POINT-R    -  This database will be modified

to synchronize it with:

        ..PointDev

You are recommended to back up your database before running this script

Script created by SQL Compare version 12.3.3.4490 from Red Gate Software Ltd at 21/05/2019 12:44:28

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[FormTypeSelectionOrganization]'
GO
IF OBJECT_ID(N'[dbo].[FormTypeSelectionOrganization]', 'U') IS NULL
CREATE TABLE [dbo].[FormTypeSelectionOrganization]
(
[FormTypeSelectionID] [int] NOT NULL,
[OrganizationID] [int] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_FormTypeSelectionOrganization] on [dbo].[FormTypeSelectionOrganization]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE name = N'PK_FormTypeSelectionOrganization' AND object_id = OBJECT_ID(N'[dbo].[FormTypeSelectionOrganization]'))
ALTER TABLE [dbo].[FormTypeSelectionOrganization] ADD CONSTRAINT [PK_FormTypeSelectionOrganization] PRIMARY KEY CLUSTERED  ([FormTypeSelectionID], [OrganizationID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[FormTypeSelection]'
GO
IF OBJECT_ID(N'[dbo].[FormTypeSelection]', 'U') IS NULL
CREATE TABLE [dbo].[FormTypeSelection]
(
[FormTypeSelectionID] [int] NOT NULL IDENTITY(1, 1),
[FlowDefinitionID] [int] NOT NULL,
[TypeID] [int] NOT NULL,
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Value] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_FormTypeSelection] on [dbo].[FormTypeSelection]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE name = N'PK_FormTypeSelection' AND object_id = OBJECT_ID(N'[dbo].[FormTypeSelection]'))
ALTER TABLE [dbo].[FormTypeSelection] ADD CONSTRAINT [PK_FormTypeSelection] PRIMARY KEY CLUSTERED  ([FormTypeSelectionID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
