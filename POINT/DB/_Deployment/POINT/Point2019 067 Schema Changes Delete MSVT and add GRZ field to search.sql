/*
Run this script on:

        pointtest.verzorgdeoverdracht.nl.TXX-DB-POINT-R    -  This database will be modified

to synchronize it with:

        ..PointDev

You are recommended to back up your database before running this script

Script created by SQL Compare version 12.3.3.4490 from Red Gate Software Ltd at 21/06/2019 12:41:57

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_FlowInstanceSearchValues_AcceptedByID_Includes] from [dbo].[FlowInstanceSearchValues]'
GO
IF EXISTS (SELECT 1 FROM sys.indexes WHERE name = N'IX_FlowInstanceSearchValues_AcceptedByID_Includes' AND object_id = OBJECT_ID(N'[dbo].[FlowInstanceSearchValues]'))
DROP INDEX [IX_FlowInstanceSearchValues_AcceptedByID_Includes] ON [dbo].[FlowInstanceSearchValues]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_FlowInstanceSearchValues_ClientFullNameScopeType_All] from [dbo].[FlowInstanceSearchValues]'
GO
IF EXISTS (SELECT 1 FROM sys.indexes WHERE name = N'IX_FlowInstanceSearchValues_ClientFullNameScopeType_All' AND object_id = OBJECT_ID(N'[dbo].[FlowInstanceSearchValues]'))
DROP INDEX [IX_FlowInstanceSearchValues_ClientFullNameScopeType_All] ON [dbo].[FlowInstanceSearchValues]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_FlowInstanceSearchValues_PatientNumberScopeType_All] from [dbo].[FlowInstanceSearchValues]'
GO
IF EXISTS (SELECT 1 FROM sys.indexes WHERE name = N'IX_FlowInstanceSearchValues_PatientNumberScopeType_All' AND object_id = OBJECT_ID(N'[dbo].[FlowInstanceSearchValues]'))
DROP INDEX [IX_FlowInstanceSearchValues_PatientNumberScopeType_All] ON [dbo].[FlowInstanceSearchValues]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[FlowInstanceSearchValues]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
IF COL_LENGTH(N'[dbo].[FlowInstanceSearchValues]', N'LastMsvtFormContent') IS NOT NULL
ALTER TABLE [dbo].[FlowInstanceSearchValues] DROP COLUMN [LastMsvtFormContent]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
IF (COL_LENGTH(N'[dbo].[FlowInstanceSearchValues]', N'LastMsvtFormDateTime') IS NOT NULL) AND (COL_LENGTH(N'[dbo].[FlowInstanceSearchValues]', N'LastGrzFormDateTime') IS NULL)
EXEC sp_rename N'[dbo].[FlowInstanceSearchValues].[LastMsvtFormDateTime]', N'LastGrzFormDateTime', N'COLUMN'
GO
UPDATE [dbo].[FlowInstanceSearchValues] SET LastGrzFormDateTime = NULL;
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_FlowInstanceSearchValues_AcceptedByID_Includes] on [dbo].[FlowInstanceSearchValues]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE name = N'IX_FlowInstanceSearchValues_AcceptedByID_Includes' AND object_id = OBJECT_ID(N'[dbo].[FlowInstanceSearchValues]'))
CREATE NONCLUSTERED INDEX [IX_FlowInstanceSearchValues_AcceptedByID_Includes] ON [dbo].[FlowInstanceSearchValues] ([AcceptedByID]) INCLUDE ([AcceptedBy], [AcceptedByII], [AcceptedByIIID], [AcceptedByTelephoneNumber], [AcceptedByTelephoneVVT], [AcceptedByVVT], [AcceptedByVVTID], [AcceptedDateTP], [ActivePhaseText], [BehandelaarNaam], [BehandelaarSpecialisme], [CareBeginDate], [ClientBirthDate], [ClientCivilServiceNumber], [ClientFullname], [ClientGender], [CopyOfTransferID], [DatumEindeBehandelingMedischSpecialist], [DepartmentName], [DesiredHealthCareProvider], [DestinationHospital], [DestinationHospitalDepartment], [DestinationHospitalLocation], [DischargePatientAcceptedYNByVVT], [DischargeProposedStartDate], [FlowDefinitionName], [FlowInstanceID], [FullDepartmentName], [GewensteIngangsdatum], [HealthInsuranceCompany], [HealthInsuranceCompanyID], [IsInterrupted], [LastAdditionalTPMemoDateTime], [LastGrzFormDateTime], [LastTransferAttachmentUploadDate], [LastTransferMemoDateTime], [LocationName], [MedischeSituatieDatumOpname], [OrganizationName], [PatientNumber], [ReadModelTimeStamp], [RequestFormZHVVTType], [RequestTransferPointIntakeDate], [ScopeType], [TransferCreatedBy], [TransferCreatedByID], [TransferCreatedDate], [TransferDate], [TransferDateVVT], [TransferID], [TransferTaskDueDate], [TyperingNazorgCombined], [ZorgInZorginstellingVoorkeurPatient1])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_FlowInstanceSearchValues_ClientFullNameScopeType_All] on [dbo].[FlowInstanceSearchValues]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE name = N'IX_FlowInstanceSearchValues_ClientFullNameScopeType_All' AND object_id = OBJECT_ID(N'[dbo].[FlowInstanceSearchValues]'))
CREATE NONCLUSTERED INDEX [IX_FlowInstanceSearchValues_ClientFullNameScopeType_All] ON [dbo].[FlowInstanceSearchValues] ([ClientFullname], [ScopeType]) INCLUDE ([AcceptedBy], [AcceptedByID], [AcceptedByII], [AcceptedByIIID], [AcceptedByTelephoneNumber], [AcceptedByTelephoneVVT], [AcceptedByVVT], [AcceptedByVVTID], [AcceptedDateTP], [ActivePhaseText], [BehandelaarNaam], [BehandelaarSpecialisme], [CareBeginDate], [ClientBirthDate], [ClientCivilServiceNumber], [ClientGender], [CopyOfTransferID], [DatumEindeBehandelingMedischSpecialist], [DepartmentName], [DesiredHealthCareProvider], [DestinationHospital], [DestinationHospitalDepartment], [DestinationHospitalLocation], [DischargePatientAcceptedYNByVVT], [DischargeProposedStartDate], [FlowDefinitionName], [FlowInstanceID], [FullDepartmentName], [GewensteIngangsdatum], [HealthInsuranceCompany], [HealthInsuranceCompanyID], [IsInterrupted], [LastAdditionalTPMemoDateTime], [LastGrzFormDateTime], [LastTransferAttachmentUploadDate], [LastTransferMemoDateTime], [LocationName], [MedischeSituatieDatumOpname], [OrganizationName], [PatientNumber], [ReadModelTimeStamp], [RequestFormZHVVTType], [RequestTransferPointIntakeDate], [TransferCreatedBy], [TransferCreatedByID], [TransferCreatedDate], [TransferDate], [TransferDateVVT], [TransferID], [TransferTaskDueDate], [TyperingNazorgCombined], [ZorgInZorginstellingVoorkeurPatient1])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_FlowInstanceSearchValues_PatientNumberScopeType_All] on [dbo].[FlowInstanceSearchValues]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE name = N'IX_FlowInstanceSearchValues_PatientNumberScopeType_All' AND object_id = OBJECT_ID(N'[dbo].[FlowInstanceSearchValues]'))
CREATE NONCLUSTERED INDEX [IX_FlowInstanceSearchValues_PatientNumberScopeType_All] ON [dbo].[FlowInstanceSearchValues] ([PatientNumber], [ScopeType]) INCLUDE ([AcceptedBy], [AcceptedByID], [AcceptedByII], [AcceptedByIIID], [AcceptedByTelephoneNumber], [AcceptedByTelephoneVVT], [AcceptedByVVT], [AcceptedByVVTID], [AcceptedDateTP], [ActivePhaseText], [BehandelaarNaam], [BehandelaarSpecialisme], [CareBeginDate], [ClientBirthDate], [ClientCivilServiceNumber], [ClientFullname], [ClientGender], [CopyOfTransferID], [DatumEindeBehandelingMedischSpecialist], [DepartmentName], [DesiredHealthCareProvider], [DestinationHospital], [DestinationHospitalDepartment], [DestinationHospitalLocation], [DischargePatientAcceptedYNByVVT], [DischargeProposedStartDate], [FlowDefinitionName], [FlowInstanceID], [FullDepartmentName], [GewensteIngangsdatum], [HealthInsuranceCompany], [HealthInsuranceCompanyID], [IsInterrupted], [LastAdditionalTPMemoDateTime], [LastGrzFormDateTime], [LastTransferAttachmentUploadDate], [LastTransferMemoDateTime], [LocationName], [MedischeSituatieDatumOpname], [OrganizationName], [ReadModelTimeStamp], [RequestFormZHVVTType], [RequestTransferPointIntakeDate], [TransferCreatedBy], [TransferCreatedByID], [TransferCreatedDate], [TransferDate], [TransferDateVVT], [TransferID], [TransferTaskDueDate], [TyperingNazorgCombined], [ZorgInZorginstellingVoorkeurPatient1])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
