/*
Run this script on:

pointreview.verzorgdeoverdracht.nl.TXX-DB-POINT-R    -  This database will be modified

to synchronize it with:

..PointDev

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 12.3.3.4490 from Red Gate Software Ltd at 13/06/2019 10:26:42

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION

PRINT(N'Update rows in [dbo].[ReportDefinition]')
UPDATE [dbo].[ReportDefinition] SET [Category]=N'Ontvangen' WHERE [ReportDefinitionID] = 29
UPDATE [dbo].[ReportDefinition] SET [Category]=N'Ontvangen' WHERE [ReportDefinitionID] = 30
UPDATE [dbo].[ReportDefinition] SET [Category]=N'Ontvangen', [Description]=N'In dit rapport worden de dossiers getoond welke wel naar uw organisatie(s) zijn gestuurd, maar door een andere zorgaanbieder in behandeling zijn genomen. 
<b>Oorzaken:</b>
 - De verzendende partij kan zelf voor een andere zorgaanbieder gekozen hebben
 - Een van uw medewerkers heeft aangegeven de patiënt niet aan te kunnen nemen waarna het dossier bij een ander terecht is gekomen. 

Dit overzicht toont een platte lijst van alle betrokken patienten
 
<i>Wanneer 1 dossier meerdere malen geweigerd is wordt deze 1 keer getoond en geteld.</i>' WHERE [ReportDefinitionID] = 31
PRINT(N'Operation applied to 3 rows out of 3')
COMMIT TRANSACTION
GO
