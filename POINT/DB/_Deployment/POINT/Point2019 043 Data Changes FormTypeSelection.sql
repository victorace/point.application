/*
Run this script on:

pointreview.verzorgdeoverdracht.nl.TXX-DB-POINT-R    -  This database will be modified

to synchronize it with:

..PointDev

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 12.3.3.4490 from Red Gate Software Ltd at 21/05/2019 12:49:28

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION

PRINT(N'Drop constraints from [dbo].[FlowField]')
ALTER TABLE [dbo].[FlowField] NOCHECK CONSTRAINT [FK_FlowField_FlowFieldDataSource]

PRINT(N'Drop constraint FK_dbo.FlowFieldAttribute_dbo.FlowField_FlowFieldID from [dbo].[FlowFieldAttribute]')
ALTER TABLE [dbo].[FlowFieldAttribute] NOCHECK CONSTRAINT [FK_dbo.FlowFieldAttribute_dbo.FlowField_FlowFieldID]

PRINT(N'Drop constraint FK_FlowFieldAttribute_FlowField from [dbo].[FlowFieldAttribute]')
ALTER TABLE [dbo].[FlowFieldAttribute] NOCHECK CONSTRAINT [FK_FlowFieldAttribute_FlowField]

PRINT(N'Update row in [dbo].[FlowField]')
UPDATE [dbo].[FlowField] SET [FlowFieldDataSourceID]=177 WHERE [FlowFieldID] = 642

PRINT(N'Update row in [dbo].[FlowFieldDataSource]')
UPDATE [dbo].[FlowFieldDataSource] SET [JsonData]=N'{"Options": [
{"Text": "Geen","Value": "addressRemark_none"},
{"Text": "Appartement met lift", "Value": "addressRemark_apartment_with_elevator"},
{"Text": "Bel klant voor aankomst", "Value": "addressRemark_phoneClientBeforeArrival"},
{"Text": "Bezorging bij de achteringang", "Value": "addressRemark_deliveryAtRearEntrance"},
{"Text": "Geduld aan de deur", "Value": "addressRemark_patienceAtDoor"},
{"Text": "Grind pad", "Value": "addressRemark_gravel_path"},
{"Text": "Herhaaldelijk aan de deur bellen", "Value": "addressRemark_ringRepeatedly"},
{"Text": "Hotel campingpark", "Value": "addressRemark_hotel_camping_park"},
{"Text": "Inchecken bij de receptie", "Value": "addressRemark_checkInAtReceptionCare"},
{"Text": "Klant kan niet zelf openen", "Value": "addressRemark_client_cannot_open"},
{"Text": "Parkeer probleem", "Value": "addressRemark_parking_problem"},
{"Text": "Risico van infectie", "Value": "addressRemark_risk_of_infection"},
{"Text": "Trap", "Value": "addressRemark_stairway"},
{"Text": "Verzorgingstehuis", "Value": "addressRemark_nursing_home"}
]}' WHERE [FlowFieldDataSourceID] = 176

PRINT(N'Add row to [dbo].[FlowFieldDataSource]')
SET IDENTITY_INSERT [dbo].[FlowFieldDataSource] ON
INSERT INTO [dbo].[FlowFieldDataSource] ([FlowFieldDataSourceID], [JsonData]) VALUES (177, N'{"Type": "Entity","MethodName": "LookUp/GetVOFormTypeSelection?TransferID={0}"}')
SET IDENTITY_INSERT [dbo].[FlowFieldDataSource] OFF

PRINT(N'Add rows to [dbo].[FormTypeSelection]')
SET IDENTITY_INSERT [dbo].[FormTypeSelection] ON
INSERT INTO [dbo].[FormTypeSelection] ([FormTypeSelectionID], [FlowDefinitionID], [TypeID], [Name], [Value]) VALUES (34, 1, 202, N'VO formulier in POINT', N'Standaard')
INSERT INTO [dbo].[FormTypeSelection] ([FormTypeSelectionID], [FlowDefinitionID], [TypeID], [Name], [Value]) VALUES (35, 1, 202, N'VO als bijlage', N'Pdf')
INSERT INTO [dbo].[FormTypeSelection] ([FormTypeSelectionID], [FlowDefinitionID], [TypeID], [Name], [Value]) VALUES (36, 1, 202, N'VO meegegeven', N'Aan patient')
INSERT INTO [dbo].[FormTypeSelection] ([FormTypeSelectionID], [FlowDefinitionID], [TypeID], [Name], [Value]) VALUES (37, 2, 202, N'VO formulier in POINT', N'Standaard')
INSERT INTO [dbo].[FormTypeSelection] ([FormTypeSelectionID], [FlowDefinitionID], [TypeID], [Name], [Value]) VALUES (38, 2, 202, N'VO als bijlage', N'Pdf')
INSERT INTO [dbo].[FormTypeSelection] ([FormTypeSelectionID], [FlowDefinitionID], [TypeID], [Name], [Value]) VALUES (39, 2, 202, N'VO meegegeven', N'Aan patient')
INSERT INTO [dbo].[FormTypeSelection] ([FormTypeSelectionID], [FlowDefinitionID], [TypeID], [Name], [Value]) VALUES (40, 3, 202, N'VO formulier in POINT', N'Standaard')
INSERT INTO [dbo].[FormTypeSelection] ([FormTypeSelectionID], [FlowDefinitionID], [TypeID], [Name], [Value]) VALUES (41, 3, 202, N'VO als bijlage', N'Pdf')
INSERT INTO [dbo].[FormTypeSelection] ([FormTypeSelectionID], [FlowDefinitionID], [TypeID], [Name], [Value]) VALUES (42, 3, 202, N'VO meegegeven', N'Aan patient')
INSERT INTO [dbo].[FormTypeSelection] ([FormTypeSelectionID], [FlowDefinitionID], [TypeID], [Name], [Value]) VALUES (43, 4, 202, N'VO formulier in POINT', N'Standaard')
INSERT INTO [dbo].[FormTypeSelection] ([FormTypeSelectionID], [FlowDefinitionID], [TypeID], [Name], [Value]) VALUES (44, 4, 202, N'VO als bijlage', N'Pdf')
INSERT INTO [dbo].[FormTypeSelection] ([FormTypeSelectionID], [FlowDefinitionID], [TypeID], [Name], [Value]) VALUES (45, 4, 202, N'VO meegegeven', N'Aan patient')
INSERT INTO [dbo].[FormTypeSelection] ([FormTypeSelectionID], [FlowDefinitionID], [TypeID], [Name], [Value]) VALUES (46, 5, 202, N'VO formulier in POINT', N'Standaard')
INSERT INTO [dbo].[FormTypeSelection] ([FormTypeSelectionID], [FlowDefinitionID], [TypeID], [Name], [Value]) VALUES (47, 5, 202, N'VO als bijlage', N'Pdf')
INSERT INTO [dbo].[FormTypeSelection] ([FormTypeSelectionID], [FlowDefinitionID], [TypeID], [Name], [Value]) VALUES (48, 5, 202, N'VO meegegeven', N'Aan patient')
INSERT INTO [dbo].[FormTypeSelection] ([FormTypeSelectionID], [FlowDefinitionID], [TypeID], [Name], [Value]) VALUES (49, 7, 202, N'VO formulier in POINT', N'Standaard')
INSERT INTO [dbo].[FormTypeSelection] ([FormTypeSelectionID], [FlowDefinitionID], [TypeID], [Name], [Value]) VALUES (50, 7, 202, N'VO als bijlage', N'Pdf')
INSERT INTO [dbo].[FormTypeSelection] ([FormTypeSelectionID], [FlowDefinitionID], [TypeID], [Name], [Value]) VALUES (51, 7, 202, N'VO meegegeven', N'Aan patient')
SET IDENTITY_INSERT [dbo].[FormTypeSelection] OFF
PRINT(N'Operation applied to 18 rows out of 18')

PRINT(N'Add constraints to [dbo].[FlowField]')
ALTER TABLE [dbo].[FlowField] WITH CHECK CHECK CONSTRAINT [FK_FlowField_FlowFieldDataSource]
ALTER TABLE [dbo].[FlowFieldAttribute] WITH CHECK CHECK CONSTRAINT [FK_dbo.FlowFieldAttribute_dbo.FlowField_FlowFieldID]
ALTER TABLE [dbo].[FlowFieldAttribute] WITH CHECK CHECK CONSTRAINT [FK_FlowFieldAttribute_FlowField]
COMMIT TRANSACTION
GO
