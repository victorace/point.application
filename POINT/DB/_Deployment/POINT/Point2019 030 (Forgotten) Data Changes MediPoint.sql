/*
Run this script on:

pointreview.verzorgdeoverdracht.nl.TXX-DB-POINT-R    -  This database will be modified

to synchronize it with:

..Point

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 12.3.3.4490 from Red Gate Software Ltd at 01/05/2019 16:06:29

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION

PRINT(N'Drop constraint FK_FlowField_FlowFieldDataSource from [dbo].[FlowField]')
ALTER TABLE [dbo].[FlowField] NOCHECK CONSTRAINT [FK_FlowField_FlowFieldDataSource]

PRINT(N'Drop constraints from [dbo].[AidProduct]')
ALTER TABLE [dbo].[AidProduct] NOCHECK CONSTRAINT [FK_AidProduct_AidProductGroup]
ALTER TABLE [dbo].[AidProduct] NOCHECK CONSTRAINT [FK_AidProduct_Supplier]

PRINT(N'Update rows in [dbo].[FlowFieldDataSource]')
UPDATE [dbo].[FlowFieldDataSource] SET [JsonData]=N'{"Options": [
{"Text": "Geen","Value": "addressRemark_none"},
{"Text": "Appartement met lift", "Value": "addressRemark_apartment_with_elevator"},
{"Text": "Bel klant voor aankomst", "Value": "addressRemark_phoneClientBeforeArrival"},
{"Text": "Bezorging bij de achteringang", "Value": "addressRemark_deliveryAtRearEntrance"},
{"Text": "Geduld aan de deur", "Value": "addressRemark_patienceAtDoor"},
{"Text": "Grind pad", "Value": "addressRemark_gravel_path"},
{"Text": "Herhaaldelijk aan de deur bellen", "Value": "addressRemark_ringRepeatedly"},
{"Text": "Hotel campingpark", "Value": "addressRemark_hotel_camping_park"},
{"Text": "Inchecken bij de receptie", "Value": "addressRemark_checkInAtReceptionCare"},
{"Text": "Klant kan niet zelf openen", "Value": "addressRemark_client_cannot_open"},
{"Text": "Parkeer probleem", "Value": "addressRemark_parking_problem"},
{"Text": "Risico van infectie", "Value": "addressRemark_risk_of_infection"},
{"Text": "Trap", "Value": "addressRemark_stairway"},
{"Text": "Verzorgingstehuis", "Value": "addressRemark_nursing_home"}
]}' WHERE [FlowFieldDataSourceID] = 176
PRINT(N'Operation applied to 1 rows out of 2')

PRINT(N'Update rows in [dbo].[AidProduct]')
UPDATE [dbo].[AidProduct] SET [Code]='10000' WHERE [AidProductID] = 10000
UPDATE [dbo].[AidProduct] SET [Code]='10010' WHERE [AidProductID] = 10010
UPDATE [dbo].[AidProduct] SET [Code]='10020' WHERE [AidProductID] = 10020
UPDATE [dbo].[AidProduct] SET [Code]='10030' WHERE [AidProductID] = 10030
UPDATE [dbo].[AidProduct] SET [Code]='10040' WHERE [AidProductID] = 10040
UPDATE [dbo].[AidProduct] SET [Code]='10050' WHERE [AidProductID] = 10050
UPDATE [dbo].[AidProduct] SET [Code]='10500' WHERE [AidProductID] = 10500
UPDATE [dbo].[AidProduct] SET [Code]='10510' WHERE [AidProductID] = 10510
UPDATE [dbo].[AidProduct] SET [Code]='10520' WHERE [AidProductID] = 10520
UPDATE [dbo].[AidProduct] SET [Code]='10530' WHERE [AidProductID] = 10530
UPDATE [dbo].[AidProduct] SET [Code]='10540' WHERE [AidProductID] = 10540
UPDATE [dbo].[AidProduct] SET [Code]='20000' WHERE [AidProductID] = 20000
UPDATE [dbo].[AidProduct] SET [Code]='20010' WHERE [AidProductID] = 20010
UPDATE [dbo].[AidProduct] SET [Code]='20020' WHERE [AidProductID] = 20020
UPDATE [dbo].[AidProduct] SET [Code]='20030' WHERE [AidProductID] = 20030
UPDATE [dbo].[AidProduct] SET [Code]='20040' WHERE [AidProductID] = 20040
UPDATE [dbo].[AidProduct] SET [Code]='20050' WHERE [AidProductID] = 20050
UPDATE [dbo].[AidProduct] SET [Code]='20060' WHERE [AidProductID] = 20060
UPDATE [dbo].[AidProduct] SET [Code]='20110' WHERE [AidProductID] = 20110
UPDATE [dbo].[AidProduct] SET [Code]='20120' WHERE [AidProductID] = 20120
UPDATE [dbo].[AidProduct] SET [Code]='20300' WHERE [AidProductID] = 20300
UPDATE [dbo].[AidProduct] SET [Code]='20310' WHERE [AidProductID] = 20310
UPDATE [dbo].[AidProduct] SET [Code]='20320' WHERE [AidProductID] = 20320
UPDATE [dbo].[AidProduct] SET [Code]='20600' WHERE [AidProductID] = 20600
UPDATE [dbo].[AidProduct] SET [Code]='30000' WHERE [AidProductID] = 30000
UPDATE [dbo].[AidProduct] SET [Code]='30010' WHERE [AidProductID] = 30010
UPDATE [dbo].[AidProduct] SET [Code]='30020' WHERE [AidProductID] = 30020
UPDATE [dbo].[AidProduct] SET [Code]='30030' WHERE [AidProductID] = 30030
UPDATE [dbo].[AidProduct] SET [Code]='30040' WHERE [AidProductID] = 30040
UPDATE [dbo].[AidProduct] SET [Code]='30050' WHERE [AidProductID] = 30050
UPDATE [dbo].[AidProduct] SET [Code]='30060' WHERE [AidProductID] = 30060
UPDATE [dbo].[AidProduct] SET [Code]='30200' WHERE [AidProductID] = 30200
UPDATE [dbo].[AidProduct] SET [Code]='30210' WHERE [AidProductID] = 30210
UPDATE [dbo].[AidProduct] SET [Code]='30300' WHERE [AidProductID] = 30300
UPDATE [dbo].[AidProduct] SET [Code]='30310' WHERE [AidProductID] = 30310
UPDATE [dbo].[AidProduct] SET [Code]='30320' WHERE [AidProductID] = 30320
UPDATE [dbo].[AidProduct] SET [Code]='30400' WHERE [AidProductID] = 30400
UPDATE [dbo].[AidProduct] SET [Code]='30410' WHERE [AidProductID] = 30410
UPDATE [dbo].[AidProduct] SET [Code]='30420' WHERE [AidProductID] = 30420
UPDATE [dbo].[AidProduct] SET [Code]='40000' WHERE [AidProductID] = 40000
UPDATE [dbo].[AidProduct] SET [Code]='40010' WHERE [AidProductID] = 40010
UPDATE [dbo].[AidProduct] SET [Code]='40020' WHERE [AidProductID] = 40020
UPDATE [dbo].[AidProduct] SET [Code]='40050' WHERE [AidProductID] = 40050
UPDATE [dbo].[AidProduct] SET [Code]='40100' WHERE [AidProductID] = 40100
UPDATE [dbo].[AidProduct] SET [Code]='40110' WHERE [AidProductID] = 40110
UPDATE [dbo].[AidProduct] SET [Code]='40120' WHERE [AidProductID] = 40120
UPDATE [dbo].[AidProduct] SET [Code]='40130' WHERE [AidProductID] = 40130
UPDATE [dbo].[AidProduct] SET [Code]='50000' WHERE [AidProductID] = 50000
UPDATE [dbo].[AidProduct] SET [Code]='50005' WHERE [AidProductID] = 50005
UPDATE [dbo].[AidProduct] SET [Code]='50007' WHERE [AidProductID] = 50007
UPDATE [dbo].[AidProduct] SET [Code]='50010' WHERE [AidProductID] = 50010
UPDATE [dbo].[AidProduct] SET [Code]='50200' WHERE [AidProductID] = 50200
UPDATE [dbo].[AidProduct] SET [Code]='50300' WHERE [AidProductID] = 50300
UPDATE [dbo].[AidProduct] SET [Code]='60000' WHERE [AidProductID] = 60000
UPDATE [dbo].[AidProduct] SET [Code]='60010' WHERE [AidProductID] = 60010
UPDATE [dbo].[AidProduct] SET [Code]='60020' WHERE [AidProductID] = 60020
UPDATE [dbo].[AidProduct] SET [Code]='60030' WHERE [AidProductID] = 60030
UPDATE [dbo].[AidProduct] SET [Code]='60040' WHERE [AidProductID] = 60040
UPDATE [dbo].[AidProduct] SET [Code]='60100' WHERE [AidProductID] = 60100
UPDATE [dbo].[AidProduct] SET [Code]='60110' WHERE [AidProductID] = 60110
UPDATE [dbo].[AidProduct] SET [Code]='60120' WHERE [AidProductID] = 60120
UPDATE [dbo].[AidProduct] SET [Code]='60130' WHERE [AidProductID] = 60130
UPDATE [dbo].[AidProduct] SET [Code]='60200' WHERE [AidProductID] = 60200
UPDATE [dbo].[AidProduct] SET [Code]='60210' WHERE [AidProductID] = 60210
UPDATE [dbo].[AidProduct] SET [Code]='60220' WHERE [AidProductID] = 60220
UPDATE [dbo].[AidProduct] SET [Code]='60300' WHERE [AidProductID] = 60300
UPDATE [dbo].[AidProduct] SET [Code]='80000' WHERE [AidProductID] = 80000
UPDATE [dbo].[AidProduct] SET [Code]='80010' WHERE [AidProductID] = 80010
UPDATE [dbo].[AidProduct] SET [Code]='80020' WHERE [AidProductID] = 80020
UPDATE [dbo].[AidProduct] SET [Code]='80030' WHERE [AidProductID] = 80030
UPDATE [dbo].[AidProduct] SET [Code]='80031' WHERE [AidProductID] = 80031
UPDATE [dbo].[AidProduct] SET [Code]='80040' WHERE [AidProductID] = 80040
UPDATE [dbo].[AidProduct] SET [Code]='80041' WHERE [AidProductID] = 80041
UPDATE [dbo].[AidProduct] SET [Code]='80070' WHERE [AidProductID] = 80070
UPDATE [dbo].[AidProduct] SET [Code]='80080' WHERE [AidProductID] = 80080
UPDATE [dbo].[AidProduct] SET [Code]='90000' WHERE [AidProductID] = 90000
UPDATE [dbo].[AidProduct] SET [Code]='90010' WHERE [AidProductID] = 90010
UPDATE [dbo].[AidProduct] SET [Code]='90020' WHERE [AidProductID] = 90020
UPDATE [dbo].[AidProduct] SET [Code]='90030' WHERE [AidProductID] = 90030
UPDATE [dbo].[AidProduct] SET [Code]='90040' WHERE [AidProductID] = 90040
UPDATE [dbo].[AidProduct] SET [Code]='90050' WHERE [AidProductID] = 90050
UPDATE [dbo].[AidProduct] SET [Code]='90060' WHERE [AidProductID] = 90060
UPDATE [dbo].[AidProduct] SET [Code]='90070' WHERE [AidProductID] = 90070
UPDATE [dbo].[AidProduct] SET [Code]='90080' WHERE [AidProductID] = 90080
UPDATE [dbo].[AidProduct] SET [Code]='90090' WHERE [AidProductID] = 90090
UPDATE [dbo].[AidProduct] SET [Code]='90100' WHERE [AidProductID] = 90100
UPDATE [dbo].[AidProduct] SET [Code]='90110' WHERE [AidProductID] = 90110
UPDATE [dbo].[AidProduct] SET [Code]='90120' WHERE [AidProductID] = 90120
UPDATE [dbo].[AidProduct] SET [Code]='90130' WHERE [AidProductID] = 90130
UPDATE [dbo].[AidProduct] SET [Code]='90140' WHERE [AidProductID] = 90140
UPDATE [dbo].[AidProduct] SET [Code]='90150' WHERE [AidProductID] = 90150
UPDATE [dbo].[AidProduct] SET [Code]='90300' WHERE [AidProductID] = 90300
UPDATE [dbo].[AidProduct] SET [Code]='90310' WHERE [AidProductID] = 90310
UPDATE [dbo].[AidProduct] SET [Code]='90330' WHERE [AidProductID] = 90330
UPDATE [dbo].[AidProduct] SET [Code]='90340' WHERE [AidProductID] = 90340
UPDATE [dbo].[AidProduct] SET [Code]='90350' WHERE [AidProductID] = 90350
UPDATE [dbo].[AidProduct] SET [Code]='200000' WHERE [AidProductID] = 200000
UPDATE [dbo].[AidProduct] SET [Code]='200001' WHERE [AidProductID] = 200001
UPDATE [dbo].[AidProduct] SET [Code]='200002' WHERE [AidProductID] = 200002
UPDATE [dbo].[AidProduct] SET [Code]='200003' WHERE [AidProductID] = 200003
UPDATE [dbo].[AidProduct] SET [Code]='200004' WHERE [AidProductID] = 200004
UPDATE [dbo].[AidProduct] SET [Code]='200005' WHERE [AidProductID] = 200005
UPDATE [dbo].[AidProduct] SET [Code]='200006' WHERE [AidProductID] = 200006
UPDATE [dbo].[AidProduct] SET [Code]='200007' WHERE [AidProductID] = 200007
UPDATE [dbo].[AidProduct] SET [Code]='200008' WHERE [AidProductID] = 200008
UPDATE [dbo].[AidProduct] SET [Code]='200009' WHERE [AidProductID] = 200009
UPDATE [dbo].[AidProduct] SET [Code]='200010' WHERE [AidProductID] = 200010
UPDATE [dbo].[AidProduct] SET [Code]='2305001', [InsurersUZOVIList]='[3313-132],[3311-108],[3311-109],[3360],[3358],[3351],[3355],[3353],[3336],[9018],[3328],[3339],[3330],[0212],[8995],[7095],[9015],[8965],[0101],[3334],[8956],[0736],[8972],[3344],[3343],[7084],[8960],[8958],[7032],[3333],[9664],[7037],[8971],[3314],[3329],[3332],[7085],[3311],[3313],[7029]' WHERE [AidProductID] = 200011
UPDATE [dbo].[AidProduct] SET [Code]='2305002', [InsurersUZOVIList]='[3313-132],[3311-108],[3311-109],[3360],[3358],[3351],[3355],[3353],[3336],[9018],[3328],[3339],[3330],[0212],[8995],[7095],[9015],[8965],[0101],[3334],[8956],[0736],[8972],[3344],[3343],[7084],[8960],[8958],[7032],[3333],[9664],[7037],[8971],[3314],[3329],[3332],[7085],[3311],[3313],[7029]' WHERE [AidProductID] = 200012
UPDATE [dbo].[AidProduct] SET [Code]='1805002', [InsurersUZOVIList]='[3313-132],[3311-108],[3311-109],[3360],[3358],[3351],[3355],[3353],[3336],[9018],[3328],[3339],[3330],[0212],[8995],[7095],[9015],[8965],[0101],[3334],[8956],[0736],[8972],[3344],[3343],[7084],[8960],[8958],[7032],[3333],[9664],[7037],[8971],[3314],[3329],[3332],[7085],[3311],[3313],[7029]' WHERE [AidProductID] = 200013
UPDATE [dbo].[AidProduct] SET [Code]='1805004', [InsurersUZOVIList]='[3313-132],[3311-108],[3311-109],[3360],[3358],[3351],[3355],[3353],[3336],[9018],[3328],[3339],[3330],[0212],[8995],[7095],[9015],[8965],[0101],[3334],[8956],[0736],[8972],[3344],[3343],[7084],[8960],[8958],[7032],[3333],[9664],[7037],[8971],[3314],[3329],[3332],[7085],[3311],[3313],[7029]' WHERE [AidProductID] = 200014
UPDATE [dbo].[AidProduct] SET [Code]='1805005', [InsurersUZOVIList]='[3313-132],[3311-108],[3311-109],[3360],[3358],[3351],[3355],[3353],[3336],[9018],[3328],[3339],[3330],[0212],[8995],[7095],[9015],[8965],[0101],[3334],[8956],[0736],[8972],[3344],[3343],[7084],[8960],[8958],[7032],[3333],[9664],[7037],[8971],[3314],[3329],[3332],[7085],[3311],[3313],[7029]' WHERE [AidProductID] = 200015
UPDATE [dbo].[AidProduct] SET [Code]='1802001', [InsurersUZOVIList]='[3313-132],[3311-108],[3311-109],[3360],[3358],[3351],[3355],[3353],[3336],[9018],[3328],[3339],[3330],[0212],[8995],[7095],[9015],[8965],[0101],[3334],[8956],[0736],[8972],[3344],[3343],[7084],[8960],[8958],[7032],[3333],[9664],[7037],[8971],[3314],[3329],[3332],[7085],[3311],[3313],[7029]' WHERE [AidProductID] = 200016
UPDATE [dbo].[AidProduct] SET [Code]='1806001', [InsurersUZOVIList]='[3313-132],[3311-108],[3311-109],[3360],[3358],[3351],[3355],[3353],[3336],[9018],[3328],[3339],[3330],[0212],[8995],[7095],[9015],[8965],[0101],[3334],[8956],[0736],[8972],[3344],[3343],[7084],[8960],[8958],[7032],[3333],[9664],[7037],[8971],[3314],[3329],[3332],[7085],[3311],[3313],[7029]' WHERE [AidProductID] = 200017
UPDATE [dbo].[AidProduct] SET [Code]='1803001', [InsurersUZOVIList]='[3313-132],[3311-108],[3311-109],[3360],[3358],[3351],[3355],[3353],[3336],[9018],[3328],[3339],[3330],[0212],[8995],[7095],[9015],[8965],[0101],[3334],[8956],[0736],[8972],[3344],[3343],[7084],[8960],[8958],[7032],[3333],[9664],[7037],[8971],[3314],[3329],[3332],[7085],[3311],[3313],[7029]' WHERE [AidProductID] = 200018
UPDATE [dbo].[AidProduct] SET [Code]='1801001', [InsurersUZOVIList]='[3313-132],[3311-108],[3311-109],[3360],[3358],[3351],[3355],[3353],[3336],[9018],[3328],[3339],[3330],[0212],[8995],[7095],[9015],[8965],[0101],[3334],[8956],[0736],[8972],[3344],[3343],[7084],[8960],[8958],[7032],[3333],[9664],[7037],[8971],[3314],[3329],[3332],[7085],[3311],[3313],[7029]' WHERE [AidProductID] = 200019
UPDATE [dbo].[AidProduct] SET [Code]='1804001', [InsurersUZOVIList]='[3313-132],[3311-108],[3311-109],[3360],[3358],[3351],[3355],[3353],[3336],[9018],[3328],[3339],[3330],[0212],[8995],[7095],[9015],[8965],[0101],[3334],[8956],[0736],[8972],[3344],[3343],[7084],[8960],[8958],[7032],[3333],[9664],[7037],[8971],[3314],[3329],[3332],[7085],[3311],[3313],[7029]' WHERE [AidProductID] = 200020
UPDATE [dbo].[AidProduct] SET [Code]='1804002', [InsurersUZOVIList]='[3313-132],[3311-108],[3311-109],[3360],[3358],[3351],[3355],[3353],[3336],[9018],[3328],[3339],[3330],[0212],[8995],[7095],[9015],[8965],[0101],[3334],[8956],[0736],[8972],[3344],[3343],[7084],[8960],[8958],[7032],[3333],[9664],[7037],[8971],[3314],[3329],[3332],[7085],[3311],[3313],[7029]' WHERE [AidProductID] = 200021
UPDATE [dbo].[AidProduct] SET [Code]='1804003', [InsurersUZOVIList]='[3313-132],[3311-108],[3311-109],[3360],[3358],[3351],[3355],[3353],[3336],[9018],[3328],[3339],[3330],[0212],[8995],[7095],[9015],[8965],[0101],[3334],[8956],[0736],[8972],[3344],[3343],[7084],[8960],[8958],[7032],[3333],[9664],[7037],[8971],[3314],[3329],[3332],[7085],[3311],[3313],[7029]' WHERE [AidProductID] = 200022
UPDATE [dbo].[AidProduct] SET [Code]='1102007', [InsurersUZOVIList]='[3313-132],[3311-108],[3311-109],[3360],[3358],[3351],[3355],[3353],[3336],[9018],[3328],[3339],[3330],[0212],[8995],[7095],[9015],[8965],[0101],[3334],[8956],[0736],[8972],[3344],[3343],[7084],[8960],[8958],[7032],[3333],[9664],[7037],[8971],[3314],[3329],[3332],[7085],[3311],[3313],[7029]' WHERE [AidProductID] = 200023
UPDATE [dbo].[AidProduct] SET [Code]='1101001', [InsurersUZOVIList]='[3313-132],[3311-108],[3311-109],[3360],[3358],[3351],[3355],[3353],[3336],[9018],[3328],[3339],[3330],[0212],[8995],[7095],[9015],[8965],[0101],[3334],[8956],[0736],[8972],[3344],[3343],[7084],[8960],[8958],[7032],[3333],[9664],[7037],[8971],[3314],[3329],[3332],[7085],[3311],[3313],[7029]' WHERE [AidProductID] = 200024
UPDATE [dbo].[AidProduct] SET [Code]='1102001', [InsurersUZOVIList]='[3313-132],[3311-108],[3311-109],[3360],[3358],[3351],[3355],[3353],[3336],[9018],[3328],[3339],[3330],[0212],[8995],[7095],[9015],[8965],[0101],[3334],[8956],[0736],[8972],[3344],[3343],[7084],[8960],[8958],[7032],[3333],[9664],[7037],[8971],[3314],[3329],[3332],[7085],[3311],[3313],[7029]' WHERE [AidProductID] = 200025
UPDATE [dbo].[AidProduct] SET [Code]='414001', [InsurersUZOVIList]='[3313-132],[3311-108],[3311-109],[3360],[3358],[3351],[3355],[3353],[3336],[9018],[3328],[3339],[3330],[0212],[8995],[7095],[9015],[8965],[0101],[3334],[8956],[0736],[8972],[3344],[3343],[7084],[8960],[8958],[7032],[3333],[9664],[7037],[8971],[3314],[3329],[3332],[7085],[3311],[3313],[7029]' WHERE [AidProductID] = 200026
UPDATE [dbo].[AidProduct] SET [Code]='415001', [InsurersUZOVIList]='[3313-132],[3311-108],[3311-109],[3360],[3358],[3351],[3355],[3353],[3336],[9018],[3328],[3339],[3330],[0212],[8995],[7095],[9015],[8965],[0101],[3334],[8956],[0736],[8972],[3344],[3343],[7084],[8960],[8958],[7032],[3333],[9664],[7037],[8971],[3314],[3329],[3332],[7085],[3311],[3313],[7029]' WHERE [AidProductID] = 200027
UPDATE [dbo].[AidProduct] SET [Code]='415004', [InsurersUZOVIList]='[3313-132],[3311-108],[3311-109],[3360],[3358],[3351],[3355],[3353],[3336],[9018],[3328],[3339],[3330],[0212],[8995],[7095],[9015],[8965],[0101],[3334],[8956],[0736],[8972],[3344],[3343],[7084],[8960],[8958],[7032],[3333],[9664],[7037],[8971],[3314],[3329],[3332],[7085],[3311],[3313],[7029]' WHERE [AidProductID] = 200028
UPDATE [dbo].[AidProduct] SET [Code]='418001', [InsurersUZOVIList]='[3313-132],[3311-108],[3311-109],[3360],[3358],[3351],[3355],[3353],[3336],[9018],[3328],[3339],[3330],[0212],[8995],[7095],[9015],[8965],[0101],[3334],[8956],[0736],[8972],[3344],[3343],[7084],[8960],[8958],[7032],[3333],[9664],[7037],[8971],[3314],[3329],[3332],[7085],[3311],[3313],[7029]' WHERE [AidProductID] = 200029
UPDATE [dbo].[AidProduct] SET [Code]='418002', [InsurersUZOVIList]='[3313-132],[3311-108],[3311-109],[3360],[3358],[3351],[3355],[3353],[3336],[9018],[3328],[3339],[3330],[0212],[8995],[7095],[9015],[8965],[0101],[3334],[8956],[0736],[8972],[3344],[3343],[7084],[8960],[8958],[7032],[3333],[9664],[7037],[8971],[3314],[3329],[3332],[7085],[3311],[3313],[7029]' WHERE [AidProductID] = 200030
UPDATE [dbo].[AidProduct] SET [Code]='418004', [InsurersUZOVIList]='[3313-132],[3311-108],[3311-109],[3360],[3358],[3351],[3355],[3353],[3336],[9018],[3328],[3339],[3330],[0212],[8995],[7095],[9015],[8965],[0101],[3334],[8956],[0736],[8972],[3344],[3343],[7084],[8960],[8958],[7032],[3333],[9664],[7037],[8971],[3314],[3329],[3332],[7085],[3311],[3313],[7029]' WHERE [AidProductID] = 200031
UPDATE [dbo].[AidProduct] SET [Code]='402001', [InsurersUZOVIList]='[3313-132],[3311-108],[3311-109],[3360],[3358],[3351],[3355],[3353],[3336],[9018],[3328],[3339],[3330],[0212],[8995],[7095],[9015],[8965],[0101],[3334],[8956],[0736],[8972],[3344],[3343],[7084],[8960],[8958],[7032],[3333],[9664],[7037],[8971],[3314],[3329],[3332],[7085],[3311],[3313],[7029]' WHERE [AidProductID] = 200032
UPDATE [dbo].[AidProduct] SET [Code]='402003', [InsurersUZOVIList]='[3313-132],[3311-108],[3311-109],[3360],[3358],[3351],[3355],[3353],[3336],[9018],[3328],[3339],[3330],[0212],[8995],[7095],[9015],[8965],[0101],[3334],[8956],[0736],[8972],[3344],[3343],[7084],[8960],[8958],[7032],[3333],[9664],[7037],[8971],[3314],[3329],[3332],[7085],[3311],[3313],[7029]' WHERE [AidProductID] = 200033
UPDATE [dbo].[AidProduct] SET [Code]='431001', [InsurersUZOVIList]='[3313-132],[3311-108],[3311-109],[3360],[3358],[3351],[3355],[3353],[3336],[9018],[3328],[3339],[3330],[0212],[8995],[7095],[9015],[8965],[0101],[3334],[8956],[0736],[8972],[3344],[3343],[7084],[8960],[8958],[7032],[3333],[9664],[7037],[8971],[3314],[3329],[3332],[7085],[3311],[3313],[7029]' WHERE [AidProductID] = 200034
UPDATE [dbo].[AidProduct] SET [Code]='419002', [InsurersUZOVIList]='[3313-132],[3311-108],[3311-109],[3360],[3358],[3351],[3355],[3353],[3336],[9018],[3328],[3339],[3330],[0212],[8995],[7095],[9015],[8965],[0101],[3334],[8956],[0736],[8972],[3344],[3343],[7084],[8960],[8958],[7032],[3333],[9664],[7037],[8971],[3314],[3329],[3332],[7085],[3311],[3313],[7029]' WHERE [AidProductID] = 200035
UPDATE [dbo].[AidProduct] SET [Code]='416001', [InsurersUZOVIList]='[3313-132],[3311-108],[3311-109],[3360],[3358],[3351],[3355],[3353],[3336],[9018],[3328],[3339],[3330],[0212],[8995],[7095],[9015],[8965],[0101],[3334],[8956],[0736],[8972],[3344],[3343],[7084],[8960],[8958],[7032],[3333],[9664],[7037],[8971],[3314],[3329],[3332],[7085],[3311],[3313],[7029]' WHERE [AidProductID] = 200036
UPDATE [dbo].[AidProduct] SET [Code]='401001', [InsurersUZOVIList]='[3313-132],[3311-108],[3311-109],[3360],[3358],[3351],[3355],[3353],[3336],[9018],[3328],[3339],[3330],[0212],[8995],[7095],[9015],[8965],[0101],[3334],[8956],[0736],[8972],[3344],[3343],[7084],[8960],[8958],[7032],[3333],[9664],[7037],[8971],[3314],[3329],[3332],[7085],[3311],[3313],[7029]' WHERE [AidProductID] = 200037
UPDATE [dbo].[AidProduct] SET [Code]='417001', [InsurersUZOVIList]='[3313-132],[3311-108],[3311-109],[3360],[3358],[3351],[3355],[3353],[3336],[9018],[3328],[3339],[3330],[0212],[8995],[7095],[9015],[8965],[0101],[3334],[8956],[0736],[8972],[3344],[3343],[7084],[8960],[8958],[7032],[3333],[9664],[7037],[8971],[3314],[3329],[3332],[7085],[3311],[3313],[7029]' WHERE [AidProductID] = 200038
UPDATE [dbo].[AidProduct] SET [Code]='401006', [InsurersUZOVIList]='[3313-132],[3311-108],[3311-109],[3360],[3358],[3351],[3355],[3353],[3336],[9018],[3328],[3339],[3330],[0212],[8995],[7095],[9015],[8965],[0101],[3334],[8956],[0736],[8972],[3344],[3343],[7084],[8960],[8958],[7032],[3333],[9664],[7037],[8971],[3314],[3329],[3332],[7085],[3311],[3313],[7029]' WHERE [AidProductID] = 200039
UPDATE [dbo].[AidProduct] SET [Code]='417005', [InsurersUZOVIList]='[3313-132],[3311-108],[3311-109],[3360],[3358],[3351],[3355],[3353],[3336],[9018],[3328],[3339],[3330],[0212],[8995],[7095],[9015],[8965],[0101],[3334],[8956],[0736],[8972],[3344],[3343],[7084],[8960],[8958],[7032],[3333],[9664],[7037],[8971],[3314],[3329],[3332],[7085],[3311],[3313],[7029]' WHERE [AidProductID] = 200040
UPDATE [dbo].[AidProduct] SET [Code]='412001', [InsurersUZOVIList]='[3313-132],[3311-108],[3311-109],[3360],[3358],[3351],[3355],[3353],[3336],[9018],[3328],[3339],[3330],[0212],[8995],[7095],[9015],[8965],[0101],[3334],[8956],[0736],[8972],[3344],[3343],[7084],[8960],[8958],[7032],[3333],[9664],[7037],[8971],[3314],[3329],[3332],[7085],[3311],[3313],[7029]' WHERE [AidProductID] = 200041
UPDATE [dbo].[AidProduct] SET [Code]='412002', [InsurersUZOVIList]='[3313-132],[3311-108],[3311-109],[3360],[3358],[3351],[3355],[3353],[3336],[9018],[3328],[3339],[3330],[0212],[8995],[7095],[9015],[8965],[0101],[3334],[8956],[0736],[8972],[3344],[3343],[7084],[8960],[8958],[7032],[3333],[9664],[7037],[8971],[3314],[3329],[3332],[7085],[3311],[3313],[7029]' WHERE [AidProductID] = 200042
UPDATE [dbo].[AidProduct] SET [Code]='311007', [InsurersUZOVIList]='[3313-132],[3311-108],[3311-109],[3360],[3358],[3351],[3355],[3353],[3336],[9018],[3328],[3339],[3330],[0212],[8995],[7095],[9015],[8965],[0101],[3334],[8956],[0736],[8972],[3344],[3343],[7084],[8960],[8958],[7032],[3333],[9664],[7037],[8971],[3314],[3329],[3332],[7085],[3311],[3313],[7029]' WHERE [AidProductID] = 200043
UPDATE [dbo].[AidProduct] SET [Code]='311001', [InsurersUZOVIList]='[3313-132],[3311-108],[3311-109],[3360],[3358],[3351],[3355],[3353],[3336],[9018],[3328],[3339],[3330],[0212],[8995],[7095],[9015],[8965],[0101],[3334],[8956],[0736],[8972],[3344],[3343],[7084],[8960],[8958],[7032],[3333],[9664],[7037],[8971],[3314],[3329],[3332],[7085],[3311],[3313],[7029]' WHERE [AidProductID] = 200044
UPDATE [dbo].[AidProduct] SET [Code]='2002001', [InsurersUZOVIList]='[3313-132],[3311-108],[3311-109],[3360],[3358],[3351],[3355],[3353],[3336],[9018],[3328],[3339],[3330],[0212],[8995],[7095],[9015],[8965],[0101],[3334],[8956],[0736],[8972],[3344],[3343],[7084],[8960],[8958],[7095],[7032],[0736],[3333],[3334],[8956],[9664],[7037],[8971],[3314],[3329],[3332],[7085],[9015],[3311],[3313],[7029]' WHERE [AidProductID] = 200045
UPDATE [dbo].[AidProduct] SET [Code]='2001001', [InsurersUZOVIList]='[3313-132],[3311-108],[3311-109],[3360],[3358],[3351],[3355],[3353],[3336],[9018],[3328],[3339],[3330],[0212],[8995],[7095],[9015],[8965],[0101],[3334],[8956],[0736],[8972],[3344],[3343],[7084],[8960],[8958],[7032],[3333],[9664],[7037],[8971],[3314],[3329],[3332],[7085],[3311],[3313],[7029]' WHERE [AidProductID] = 200046
UPDATE [dbo].[AidProduct] SET [Code]='2014004', [InsurersUZOVIList]='[3313-132],[3311-108],[3311-109],[3360],[3358],[3351],[3355],[3353],[3336],[9018],[3328],[3339],[3330],[0212],[8995],[7095],[9015],[8965],[0101],[3334],[8956],[0736],[8972],[3344],[3343],[7084],[8960],[8958],[7032],[3333],[9664],[7037],[8971],[3314],[3329],[3332],[7085],[3311],[3313],[7029]' WHERE [AidProductID] = 200047
UPDATE [dbo].[AidProduct] SET [Code]='2014005', [InsurersUZOVIList]='[3313-132],[3311-108],[3311-109],[3360],[3358],[3351],[3355],[3353],[3336],[9018],[3328],[3339],[3330],[0212],[8995],[7095],[9015],[8965],[0101],[3334],[8956],[0736],[8972],[3344],[3343],[7084],[8960],[8958],[7032],[3333],[9664],[7037],[8971],[3314],[3329],[3332],[7085],[3311],[3313],[7029]' WHERE [AidProductID] = 200048
UPDATE [dbo].[AidProduct] SET [Code]='2015001', [InsurersUZOVIList]='[3313-132],[3311-108],[3311-109],[3360],[3358],[3351],[3355],[3353],[3336],[9018],[3328],[3339],[3330],[0212],[8995],[7095],[9015],[8965],[0101],[3334],[8956],[0736],[8972],[3344],[3343],[7084],[8960],[8958],[7032],[3333],[9664],[7037],[8971],[3314],[3329],[3332],[7085],[3311],[3313],[7029]' WHERE [AidProductID] = 200049
UPDATE [dbo].[AidProduct] SET [Code]='2015002', [InsurersUZOVIList]='[3313-132],[3311-108],[3311-109],[3360],[3358],[3351],[3355],[3353],[3336],[9018],[3328],[3339],[3330],[0212],[8995],[7095],[9015],[8965],[0101],[3334],[8956],[0736],[8972],[3344],[3343],[7084],[8960],[8958],[7032],[3333],[9664],[7037],[8971],[3314],[3329],[3332],[7085],[3311],[3313],[7029]' WHERE [AidProductID] = 200050
UPDATE [dbo].[AidProduct] SET [Code]='2005001', [InsurersUZOVIList]='[3313-132],[3311-108],[3311-109],[3360],[3358],[3351],[3355],[3353],[3336],[9018],[3328],[3339],[3330],[0212],[8995],[7095],[9015],[8965],[0101],[3334],[8956],[0736],[8972],[3344],[3343],[7084],[8960],[8958],[7032],[3333],[9664],[7037],[8971],[3314],[3329],[3332],[7085],[3311],[3313],[7029]' WHERE [AidProductID] = 200051
UPDATE [dbo].[AidProduct] SET [Code]='2006001', [InsurersUZOVIList]='[3313-132],[3311-108],[3311-109],[3360],[3358],[3351],[3355],[3353],[3336],[9018],[3328],[3339],[3330],[0212],[8995],[7095],[9015],[8965],[0101],[3334],[8956],[0736],[8972],[3344],[3343],[7084],[8960],[8958],[7032],[3333],[9664],[7037],[8971],[3314],[3329],[3332],[7085],[3311],[3313],[7029]' WHERE [AidProductID] = 200052
UPDATE [dbo].[AidProduct] SET [Code]='2016001', [InsurersUZOVIList]='[3313-132],[3311-108],[3311-109],[3360],[3358],[3351],[3355],[3353],[3336],[9018],[3328],[3339],[3330],[0212],[8995],[7095],[9015],[8965],[0101],[3334],[8956],[0736],[8972],[3344],[3343],[7084],[8960],[8958],[7032],[3333],[9664],[7037],[8971],[3314],[3329],[3332],[7085],[3311],[3313],[7029]' WHERE [AidProductID] = 200053
UPDATE [dbo].[AidProduct] SET [Code]='1906001', [InsurersUZOVIList]='[3313-132],[3311-108],[3311-109],[3360],[3358],[3351],[3355],[3353],[3336],[9018],[3328],[3339],[3330],[0212],[8995],[7095],[9015],[8965],[0101],[3334],[8956],[0736],[8972],[3344],[3343],[7084],[8960],[8958],[7095],[7032],[0736],[3333],[3334],[8956],[9664],[7037],[8971],[3314],[3329],[3332],[7085],[9015],[3311],[3313],[7029]' WHERE [AidProductID] = 200054
UPDATE [dbo].[AidProduct] SET [Code]='1003001', [InsurersUZOVIList]='[3313-132],[3311-108],[3311-109],[3360],[3358],[3351],[3355],[3353],[3336],[9018],[3328],[3339],[3330],[0212],[8995],[7095],[9015],[8965],[0101],[3334],[8956],[0736],[8972],[3344],[3343],[7084],[8960],[8958],[7032],[3333],[9664],[7037],[8971],[3314],[3329],[3332],[7085],[3311],[3313],[7029]' WHERE [AidProductID] = 200055
UPDATE [dbo].[AidProduct] SET [Code]='1004001', [InsurersUZOVIList]='[3313-132],[3311-108],[3311-109],[3360],[3358],[3351],[3355],[3353],[3336],[9018],[3328],[3339],[3330],[0212],[8995],[7095],[9015],[8965],[0101],[3334],[8956],[0736],[8972],[3344],[3343],[7084],[8960],[8958],[7032],[3333],[9664],[7037],[8971],[3314],[3329],[3332],[7085],[3311],[3313],[7029]' WHERE [AidProductID] = 200056
UPDATE [dbo].[AidProduct] SET [Code]='208002', [InsurersUZOVIList]='[3313-132],[3311-108],[3311-109],[3360],[3358],[3351],[3355],[3353],[3336],[9018],[3328],[3339],[3330],[0212],[8995],[7095],[9015],[8965],[0101],[3334],[8956],[0736],[8972],[3344],[3343],[7084],[8960],[8958],[7032],[3333],[9664],[7037],[8971],[3314],[3329],[3332],[7085],[3311],[3313],[7029]' WHERE [AidProductID] = 200057
UPDATE [dbo].[AidProduct] SET [Code]='203006', [InsurersUZOVIList]='[3313-132],[3313-132],[3311-108],[3311-108],[3311-109],[3311-109],[3360],[3358],[3351],[3355],[3353],[3336],[9018],[3328],[3339],[3330],[0212],[8995],[7095],[9015],[8965],[0101],[3334],[8956],[0736],[8972],[3344],[3343],[7084],[8960],[8958],[7032],[3333],[9664],[7037],[8971],[3314],[3329],[3332],[7085],[3311],[3311],[3313],[3313],[7029]' WHERE [AidProductID] = 200058
UPDATE [dbo].[AidProduct] SET [Code]='204004', [InsurersUZOVIList]='[3313-132],[3313-132],[3311-108],[3311-108],[3311-109],[3311-109],[3360],[3358],[3351],[3355],[3353],[3336],[9018],[3328],[3339],[3330],[0212],[8995],[7095],[9015],[8965],[0101],[3334],[8956],[0736],[8972],[3344],[3343],[7084],[8960],[8958],[7032],[3333],[9664],[7037],[8971],[3314],[3329],[3332],[7085],[3311],[3311],[3313],[3313],[7029]' WHERE [AidProductID] = 200059
UPDATE [dbo].[AidProduct] SET [Code]='205005', [InsurersUZOVIList]='[3313-132],[3313-132],[3311-108],[3311-108],[3311-109],[3311-109],[3360],[3358],[3351],[3355],[3353],[3336],[9018],[3328],[3339],[3330],[0212],[8995],[7095],[9015],[8965],[0101],[3334],[8956],[0736],[8972],[3344],[3343],[7084],[8960],[8958],[7032],[3333],[9664],[7037],[8971],[3314],[3329],[3332],[7085],[3311],[3311],[3313],[3313],[7029]' WHERE [AidProductID] = 200060
UPDATE [dbo].[AidProduct] SET [Code]='1202001', [InsurersUZOVIList]='[3313-132],[3311-108],[3311-109],[3360],[3358],[3351],[3355],[3353],[3336],[9018],[3328],[3339],[3330],[0212],[8995],[7095],[9015],[8965],[0101],[3334],[8956],[0736],[8972],[3344],[3343],[7084],[8960],[8958],[7032],[3333],[9664],[7037],[8971],[3314],[3329],[3332],[7085],[3311],[3313],[7029]' WHERE [AidProductID] = 200061
UPDATE [dbo].[AidProduct] SET [Code]='1203001', [InsurersUZOVIList]='[3313-132],[3311-108],[3311-109],[3360],[3358],[3351],[3355],[3353],[3336],[9018],[3328],[3339],[3330],[0212],[8995],[7095],[9015],[8965],[0101],[3334],[8956],[0736],[8972],[3344],[3343],[7084],[8960],[8958],[7032],[3333],[9664],[7037],[8971],[3314],[3329],[3332],[7085],[3311],[3313],[7029]' WHERE [AidProductID] = 200062
UPDATE [dbo].[AidProduct] SET [Code]='1203002', [InsurersUZOVIList]='[3313-132],[3311-108],[3311-109],[3360],[3358],[3351],[3355],[3353],[3336],[9018],[3328],[3339],[3330],[0212],[8995],[7095],[9015],[8965],[0101],[3334],[8956],[0736],[8972],[3344],[3343],[7084],[8960],[8958],[7032],[3333],[9664],[7037],[8971],[3314],[3329],[3332],[7085],[3311],[3313],[7029]' WHERE [AidProductID] = 200063
UPDATE [dbo].[AidProduct] SET [Code]='1203003', [InsurersUZOVIList]='[3313-132],[3311-108],[3311-109],[3360],[3358],[3351],[3355],[3353],[3336],[9018],[3328],[3339],[3330],[0212],[8995],[7095],[9015],[8965],[0101],[3334],[8956],[0736],[8972],[3344],[3343],[7084],[8960],[8958],[7032],[3333],[9664],[7037],[8971],[3314],[3329],[3332],[7085],[3311],[3313],[7029]' WHERE [AidProductID] = 200064
UPDATE [dbo].[AidProduct] SET [Code]='1203004', [InsurersUZOVIList]='[3313-132],[3311-108],[3311-109],[3360],[3358],[3351],[3355],[3353],[3336],[9018],[3328],[3339],[3330],[0212],[8995],[7095],[9015],[8965],[0101],[3334],[8956],[0736],[8972],[3344],[3343],[7084],[8960],[8958],[7032],[3333],[9664],[7037],[8971],[3314],[3329],[3332],[7085],[3311],[3313],[7029]' WHERE [AidProductID] = 200065
UPDATE [dbo].[AidProduct] SET [Code]='1201001', [InsurersUZOVIList]='[3313-132],[3311-108],[3311-109],[3360],[3358],[3351],[3355],[3353],[3336],[9018],[3328],[3339],[3330],[0212],[8995],[7095],[9015],[8965],[0101],[3334],[8956],[0736],[8972],[3344],[3343],[7084],[8960],[8958],[7032],[3333],[9664],[7037],[8971],[3314],[3329],[3332],[7085],[3311],[3313],[7029]' WHERE [AidProductID] = 200066
UPDATE [dbo].[AidProduct] SET [Code]='1201002', [InsurersUZOVIList]='[3313-132],[3311-108],[3311-109],[3360],[3358],[3351],[3355],[3353],[3336],[9018],[3328],[3339],[3330],[0212],[8995],[7095],[9015],[8965],[0101],[3334],[8956],[0736],[8972],[3344],[3343],[7084],[8960],[8958],[7032],[3333],[9664],[7037],[8971],[3314],[3329],[3332],[7085],[3311],[3313],[7029]' WHERE [AidProductID] = 200067
UPDATE [dbo].[AidProduct] SET [Code]='1201003', [InsurersUZOVIList]='[3313-132],[3311-108],[3311-109],[3360],[3358],[3351],[3355],[3353],[3336],[9018],[3328],[3339],[3330],[0212],[8995],[7095],[9015],[8965],[0101],[3334],[8956],[0736],[8972],[3344],[3343],[7084],[8960],[8958],[7032],[3333],[9664],[7037],[8971],[3314],[3329],[3332],[7085],[3311],[3313],[7029]' WHERE [AidProductID] = 200068
UPDATE [dbo].[AidProduct] SET [Code]='1201004', [InsurersUZOVIList]='[3313-132],[3311-108],[3311-109],[3360],[3358],[3351],[3355],[3353],[3336],[9018],[3328],[3339],[3330],[0212],[8995],[7095],[9015],[8965],[0101],[3334],[8956],[0736],[8972],[3344],[3343],[7084],[8960],[8958],[7032],[3333],[9664],[7037],[8971],[3314],[3329],[3332],[7085],[3311],[3313],[7029]' WHERE [AidProductID] = 200069
UPDATE [dbo].[AidProduct] SET [Code]='1901006', [InsurersUZOVIList]='[3313-132],[3311-108],[3311-109],[3360],[3358],[3351],[3355],[3353],[3336],[9018],[3328],[3339],[3330],[0212],[8995],[7095],[9015],[8965],[0101],[3334],[8956],[0736],[8972],[3344],[3343],[7084],[8960],[8958],[7032],[3333],[9664],[7037],[8971],[3314],[3329],[3332],[7085],[3311],[3313],[7029]' WHERE [AidProductID] = 200070
UPDATE [dbo].[AidProduct] SET [Code]='1901008', [InsurersUZOVIList]='[3313-132],[3311-108],[3311-109],[3360],[3358],[3351],[3355],[3353],[3336],[9018],[3328],[3339],[3330],[0212],[8995],[7095],[9015],[8965],[0101],[3334],[8956],[0736],[8972],[3344],[3343],[7084],[8960],[8958],[7032],[3333],[9664],[7037],[8971],[3314],[3329],[3332],[7085],[3311],[3313],[7029]' WHERE [AidProductID] = 200071
UPDATE [dbo].[AidProduct] SET [Code]='1901010', [InsurersUZOVIList]='[3313-132],[3311-108],[3311-109],[3360],[3358],[3351],[3355],[3353],[3336],[9018],[3328],[3339],[3330],[0212],[8995],[7095],[9015],[8965],[0101],[3334],[8956],[0736],[8972],[3344],[3343],[7084],[8960],[8958],[7032],[3333],[9664],[7037],[8971],[3314],[3329],[3332],[7085],[3311],[3313],[7029]' WHERE [AidProductID] = 200072
UPDATE [dbo].[AidProduct] SET [Code]='1901011', [InsurersUZOVIList]='[3313-132],[3311-108],[3311-109],[3360],[3358],[3351],[3355],[3353],[3336],[9018],[3328],[3339],[3330],[0212],[8995],[7095],[9015],[8965],[0101],[3334],[8956],[0736],[8972],[3344],[3343],[7084],[8960],[8958],[7032],[3333],[9664],[7037],[8971],[3314],[3329],[3332],[7085],[3311],[3313],[7029]' WHERE [AidProductID] = 200073
UPDATE [dbo].[AidProduct] SET [Code]='1901012', [InsurersUZOVIList]='[3313-132],[3311-108],[3311-109],[3360],[3358],[3351],[3355],[3353],[3336],[9018],[3328],[3339],[3330],[0212],[8995],[7095],[9015],[8965],[0101],[3334],[8956],[0736],[8972],[3344],[3343],[7084],[8960],[8958],[7032],[3333],[9664],[7037],[8971],[3314],[3329],[3332],[7085],[3311],[3313],[7029]' WHERE [AidProductID] = 200074
UPDATE [dbo].[AidProduct] SET [Code]='2002003', [InsurersUZOVIList]='[3313-132],[3311-108],[3311-109],[3360],[3358],[3351],[3355],[3353],[3336],[9018],[3328],[3339],[3330],[0212],[8995],[7095],[9015],[8965],[0101],[3334],[8956],[0736],[8972],[3344],[3343],[7084],[8960],[8958],[7095],[7032],[0736],[3333],[3334],[8956],[9664],[7037],[8971],[3314],[3329],[3332],[7085],[9015],[3311],[3313],[7029]' WHERE [AidProductID] = 200075
PRINT(N'Operation applied to 172 rows out of 172')
ALTER TABLE [dbo].[FlowField] WITH CHECK CHECK CONSTRAINT [FK_FlowField_FlowFieldDataSource]

PRINT(N'Add constraints to [dbo].[AidProduct]')
ALTER TABLE [dbo].[AidProduct] WITH CHECK CHECK CONSTRAINT [FK_AidProduct_AidProductGroup]
ALTER TABLE [dbo].[AidProduct] WITH CHECK CHECK CONSTRAINT [FK_AidProduct_Supplier]
COMMIT TRANSACTION
GO
