/*
Run this script on:

pointreview.verzorgdeoverdracht.nl.TXX-DB-POINT-R    -  This database will be modified

to synchronize it with:

..PointDev

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 12.3.3.4490 from Red Gate Software Ltd at 13/06/2019 09:07:42

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION

PRINT(N'Drop constraints from [dbo].[ReportDefinitionFlowDefinition]')
ALTER TABLE [dbo].[ReportDefinitionFlowDefinition] NOCHECK CONSTRAINT [FK_ReportDefinitionFlowDefinition_FlowDefinition]
ALTER TABLE [dbo].[ReportDefinitionFlowDefinition] NOCHECK CONSTRAINT [FK_ReportDefinitionFlowDefinition_FlowDefinition1]

PRINT(N'Delete rows from [dbo].[ReportDefinitionFlowDefinition]')
DELETE FROM [dbo].[ReportDefinitionFlowDefinition] WHERE [ReportDefinitionID] = 19 AND [FlowDefinitionID] = 2
DELETE FROM [dbo].[ReportDefinitionFlowDefinition] WHERE [ReportDefinitionID] = 19 AND [FlowDefinitionID] = 3
DELETE FROM [dbo].[ReportDefinitionFlowDefinition] WHERE [ReportDefinitionID] = 19 AND [FlowDefinitionID] = 4
PRINT(N'Operation applied to 3 rows out of 3')

PRINT(N'Delete row from [dbo].[ReportDefinition]')
DELETE FROM [dbo].[ReportDefinition] WHERE [ReportDefinitionID] = 19

PRINT(N'Add constraints to [dbo].[ReportDefinitionFlowDefinition]')
ALTER TABLE [dbo].[ReportDefinitionFlowDefinition] WITH CHECK CHECK CONSTRAINT [FK_ReportDefinitionFlowDefinition_FlowDefinition]
ALTER TABLE [dbo].[ReportDefinitionFlowDefinition] WITH CHECK CHECK CONSTRAINT [FK_ReportDefinitionFlowDefinition_FlowDefinition1]
COMMIT TRANSACTION
GO
