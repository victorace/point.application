/*
Run this script on:

pointreview.verzorgdeoverdracht.nl.TXX-DB-POINT-R    -  This database will be modified

to synchronize it with:

..PointDB

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 12.3.3.4490 from Red Gate Software Ltd at 5/8/2019 2:31:51 PM

*/
		
SET XACT_ABORT ON
GO
SET ARITHABORT ON
GO
BEGIN TRANSACTION

PRINT(N'Add 1 row to [dbo].[AfterCareTileGroup]')
SET IDENTITY_INSERT [dbo].[AfterCareTileGroup] ON
INSERT INTO [dbo].[AfterCareTileGroup] ([AfterCareTileGroupID], [Name], [Sortorder], [ColorCode]) VALUES (5, N'Ziekenhuis', 5, N'#0066cc')
SET IDENTITY_INSERT [dbo].[AfterCareTileGroup] OFF
COMMIT TRANSACTION
GO
