/*
Run this script on:

pointreview.verzorgdeoverdracht.nl.TXX-DB-POINT-R    -  This database will be modified

to synchronize it with:

LINKASSIST-NB18.2019T2

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 12.4.12.5042 from Red Gate Software Ltd at 5/10/2019 10:01:38 AM

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION

PRINT(N'Update rows in [dbo].[FlowDefinitionFormTypeFlowField]')
UPDATE [dbo].[FlowDefinitionFormTypeFlowField] SET [TakeOver]=1 WHERE [FlowDefinitionFormTypeFlowFieldID] = 577 AND [FlowDefinitionID] = 7 AND [FormTypeID] = 243 AND [FlowFieldID] = 209
UPDATE [dbo].[FlowDefinitionFormTypeFlowField] SET [TakeOver]=1 WHERE [FlowDefinitionFormTypeFlowFieldID] = 578 AND [FlowDefinitionID] = 7 AND [FormTypeID] = 243 AND [FlowFieldID] = 205
UPDATE [dbo].[FlowDefinitionFormTypeFlowField] SET [TakeOver]=1 WHERE [FlowDefinitionFormTypeFlowFieldID] = 579 AND [FlowDefinitionID] = 7 AND [FormTypeID] = 243 AND [FlowFieldID] = 198
UPDATE [dbo].[FlowDefinitionFormTypeFlowField] SET [TakeOver]=1 WHERE [FlowDefinitionFormTypeFlowFieldID] = 580 AND [FlowDefinitionID] = 7 AND [FormTypeID] = 243 AND [FlowFieldID] = 195
UPDATE [dbo].[FlowDefinitionFormTypeFlowField] SET [TakeOver]=1 WHERE [FlowDefinitionFormTypeFlowFieldID] = 581 AND [FlowDefinitionID] = 7 AND [FormTypeID] = 243 AND [FlowFieldID] = 192
UPDATE [dbo].[FlowDefinitionFormTypeFlowField] SET [TakeOver]=1 WHERE [FlowDefinitionFormTypeFlowFieldID] = 582 AND [FlowDefinitionID] = 7 AND [FormTypeID] = 243 AND [FlowFieldID] = 201
UPDATE [dbo].[FlowDefinitionFormTypeFlowField] SET [TakeOver]=1 WHERE [FlowDefinitionFormTypeFlowFieldID] = 583 AND [FlowDefinitionID] = 7 AND [FormTypeID] = 243 AND [FlowFieldID] = 206
PRINT(N'Operation applied to 7 rows out of 7')

PRINT(N'Add rows to [dbo].[FlowDefinitionFormTypeFlowField]')
SET IDENTITY_INSERT [dbo].[FlowDefinitionFormTypeFlowField] ON
INSERT INTO [dbo].[FlowDefinitionFormTypeFlowField] ([FlowDefinitionFormTypeFlowFieldID], [FlowDefinitionID], [FormTypeID], [FlowFieldID], [TakeOver]) VALUES (1002, 7, 243, 117, 1)
INSERT INTO [dbo].[FlowDefinitionFormTypeFlowField] ([FlowDefinitionFormTypeFlowFieldID], [FlowDefinitionID], [FormTypeID], [FlowFieldID], [TakeOver]) VALUES (1003, 7, 243, 183, 1)
INSERT INTO [dbo].[FlowDefinitionFormTypeFlowField] ([FlowDefinitionFormTypeFlowFieldID], [FlowDefinitionID], [FormTypeID], [FlowFieldID], [TakeOver]) VALUES (1004, 7, 243, 184, 1)
INSERT INTO [dbo].[FlowDefinitionFormTypeFlowField] ([FlowDefinitionFormTypeFlowFieldID], [FlowDefinitionID], [FormTypeID], [FlowFieldID], [TakeOver]) VALUES (1005, 7, 243, 185, 1)
INSERT INTO [dbo].[FlowDefinitionFormTypeFlowField] ([FlowDefinitionFormTypeFlowFieldID], [FlowDefinitionID], [FormTypeID], [FlowFieldID], [TakeOver]) VALUES (1006, 7, 243, 204, 1)
INSERT INTO [dbo].[FlowDefinitionFormTypeFlowField] ([FlowDefinitionFormTypeFlowFieldID], [FlowDefinitionID], [FormTypeID], [FlowFieldID], [TakeOver]) VALUES (1007, 7, 243, 212, 1)
INSERT INTO [dbo].[FlowDefinitionFormTypeFlowField] ([FlowDefinitionFormTypeFlowFieldID], [FlowDefinitionID], [FormTypeID], [FlowFieldID], [TakeOver]) VALUES (1008, 7, 243, 213, 1)
INSERT INTO [dbo].[FlowDefinitionFormTypeFlowField] ([FlowDefinitionFormTypeFlowFieldID], [FlowDefinitionID], [FormTypeID], [FlowFieldID], [TakeOver]) VALUES (1009, 7, 243, 214, 1)
INSERT INTO [dbo].[FlowDefinitionFormTypeFlowField] ([FlowDefinitionFormTypeFlowFieldID], [FlowDefinitionID], [FormTypeID], [FlowFieldID], [TakeOver]) VALUES (1010, 7, 243, 215, 1)
INSERT INTO [dbo].[FlowDefinitionFormTypeFlowField] ([FlowDefinitionFormTypeFlowFieldID], [FlowDefinitionID], [FormTypeID], [FlowFieldID], [TakeOver]) VALUES (1011, 7, 243, 217, 1)
INSERT INTO [dbo].[FlowDefinitionFormTypeFlowField] ([FlowDefinitionFormTypeFlowFieldID], [FlowDefinitionID], [FormTypeID], [FlowFieldID], [TakeOver]) VALUES (1012, 7, 243, 218, 1)
INSERT INTO [dbo].[FlowDefinitionFormTypeFlowField] ([FlowDefinitionFormTypeFlowFieldID], [FlowDefinitionID], [FormTypeID], [FlowFieldID], [TakeOver]) VALUES (1013, 7, 243, 219, 1)
INSERT INTO [dbo].[FlowDefinitionFormTypeFlowField] ([FlowDefinitionFormTypeFlowFieldID], [FlowDefinitionID], [FormTypeID], [FlowFieldID], [TakeOver]) VALUES (1014, 7, 243, 220, 1)
INSERT INTO [dbo].[FlowDefinitionFormTypeFlowField] ([FlowDefinitionFormTypeFlowFieldID], [FlowDefinitionID], [FormTypeID], [FlowFieldID], [TakeOver]) VALUES (1015, 7, 243, 285, 1)
INSERT INTO [dbo].[FlowDefinitionFormTypeFlowField] ([FlowDefinitionFormTypeFlowFieldID], [FlowDefinitionID], [FormTypeID], [FlowFieldID], [TakeOver]) VALUES (1016, 7, 243, 286, 1)
SET IDENTITY_INSERT [dbo].[FlowDefinitionFormTypeFlowField] OFF
PRINT(N'Operation applied to 15 rows out of 15')
COMMIT TRANSACTION
GO
