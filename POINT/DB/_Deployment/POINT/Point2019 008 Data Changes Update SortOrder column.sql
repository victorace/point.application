-- Fill SortOrder column. 
BEGIN TRY
	Declare @RegionID int;
    BEGIN TRAN
		DECLARE cur CURSOR FOR SELECT Distinct RegionID From Templet Where Deleted = 0;  
		OPEN cur;  
		FETCH NEXT FROM cur into @RegionID;  
		WHILE @@FETCH_STATUS = 0  
		BEGIN
			PRINT 'Update sort order for region: ' + cast(@regionID as varchar(4));
			Update maintable set maintable.SortOrder = orderidtable.position from Templet maintable
			Inner Join
			(
			  select templetid,
			  regionid,
			  RANK() OVER (ORDER BY uploaddate asc) AS position
					FROM templet where regionid = @RegionID and deleted = 0
			) orderidtable
			On maintable.TempletID = orderidtable.TempletID
			Where maintable.RegionID = @RegionID and deleted = 0
			FETCH NEXT FROM cur into @RegionID;  
		END;  
		CLOSE cur;  
		DEALLOCATE cur;  
    COMMIT TRAN
END TRY
BEGIN CATCH
    PRINT 'In CATCH Block'
    IF(@@TRANCOUNT > 0)
        ROLLBACK TRAN;
END CATCH
GO