/*
Run this script on:

pointtest.verzorgdeoverdracht.nl.TXX-DB-POINT-R    -  This database will be modified

to synchronize it with:

NB1404.Point

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 12.3.3.4490 from Red Gate Software Ltd at 29-4-2019 11:04:56

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION

PRINT(N'Delete rows from [dbo].[FieldReceivedValueMapping]')
DELETE FROM [dbo].[FieldReceivedValueMapping] WHERE [FieldReceivedValueMappingID] = 68
DELETE FROM [dbo].[FieldReceivedValueMapping] WHERE [FieldReceivedValueMappingID] = 69
DELETE FROM [dbo].[FieldReceivedValueMapping] WHERE [FieldReceivedValueMappingID] = 70
DELETE FROM [dbo].[FieldReceivedValueMapping] WHERE [FieldReceivedValueMappingID] = 71
DELETE FROM [dbo].[FieldReceivedValueMapping] WHERE [FieldReceivedValueMappingID] = 72
DELETE FROM [dbo].[FieldReceivedValueMapping] WHERE [FieldReceivedValueMappingID] = 73
DELETE FROM [dbo].[FieldReceivedValueMapping] WHERE [FieldReceivedValueMappingID] = 74
DELETE FROM [dbo].[FieldReceivedValueMapping] WHERE [FieldReceivedValueMappingID] = 75
DELETE FROM [dbo].[FieldReceivedValueMapping] WHERE [FieldReceivedValueMappingID] = 76
DELETE FROM [dbo].[FieldReceivedValueMapping] WHERE [FieldReceivedValueMappingID] = 77
DELETE FROM [dbo].[FieldReceivedValueMapping] WHERE [FieldReceivedValueMappingID] = 78
DELETE FROM [dbo].[FieldReceivedValueMapping] WHERE [FieldReceivedValueMappingID] = 79
DELETE FROM [dbo].[FieldReceivedValueMapping] WHERE [FieldReceivedValueMappingID] = 80
DELETE FROM [dbo].[FieldReceivedValueMapping] WHERE [FieldReceivedValueMappingID] = 81
DELETE FROM [dbo].[FieldReceivedValueMapping] WHERE [FieldReceivedValueMappingID] = 82
DELETE FROM [dbo].[FieldReceivedValueMapping] WHERE [FieldReceivedValueMappingID] = 83
DELETE FROM [dbo].[FieldReceivedValueMapping] WHERE [FieldReceivedValueMappingID] = 84
DELETE FROM [dbo].[FieldReceivedValueMapping] WHERE [FieldReceivedValueMappingID] = 85
DELETE FROM [dbo].[FieldReceivedValueMapping] WHERE [FieldReceivedValueMappingID] = 86
DELETE FROM [dbo].[FieldReceivedValueMapping] WHERE [FieldReceivedValueMappingID] = 87
DELETE FROM [dbo].[FieldReceivedValueMapping] WHERE [FieldReceivedValueMappingID] = 88
DELETE FROM [dbo].[FieldReceivedValueMapping] WHERE [FieldReceivedValueMappingID] = 89
DELETE FROM [dbo].[FieldReceivedValueMapping] WHERE [FieldReceivedValueMappingID] = 90
DELETE FROM [dbo].[FieldReceivedValueMapping] WHERE [FieldReceivedValueMappingID] = 91
DELETE FROM [dbo].[FieldReceivedValueMapping] WHERE [FieldReceivedValueMappingID] = 92
DELETE FROM [dbo].[FieldReceivedValueMapping] WHERE [FieldReceivedValueMappingID] = 93
PRINT(N'Operation applied to 26 rows out of 26')

PRINT(N'Add rows to [dbo].[FieldReceivedValueMapping]')
SET IDENTITY_INSERT [dbo].[FieldReceivedValueMapping] ON
INSERT INTO [dbo].[FieldReceivedValueMapping] ([FieldReceivedValueMappingID], [FieldName], [SourceValue], [TargetValue]) VALUES (217, 'Gender', 'm', '1')
INSERT INTO [dbo].[FieldReceivedValueMapping] ([FieldReceivedValueMappingID], [FieldName], [SourceValue], [TargetValue]) VALUES (218, 'Gender', 'v', '2')
INSERT INTO [dbo].[FieldReceivedValueMapping] ([FieldReceivedValueMappingID], [FieldName], [SourceValue], [TargetValue]) VALUES (219, 'Gender', 'o', '3')
INSERT INTO [dbo].[FieldReceivedValueMapping] ([FieldReceivedValueMappingID], [FieldName], [SourceValue], [TargetValue]) VALUES (220, 'CivilClass', '1', '1')
INSERT INTO [dbo].[FieldReceivedValueMapping] ([FieldReceivedValueMappingID], [FieldName], [SourceValue], [TargetValue]) VALUES (221, 'CivilClass', '2', '2')
INSERT INTO [dbo].[FieldReceivedValueMapping] ([FieldReceivedValueMappingID], [FieldName], [SourceValue], [TargetValue]) VALUES (222, 'CivilClass', '3', '3')
INSERT INTO [dbo].[FieldReceivedValueMapping] ([FieldReceivedValueMappingID], [FieldName], [SourceValue], [TargetValue]) VALUES (223, 'CivilClass', '4', '')
INSERT INTO [dbo].[FieldReceivedValueMapping] ([FieldReceivedValueMappingID], [FieldName], [SourceValue], [TargetValue]) VALUES (224, 'CivilClass', '5', '0')
INSERT INTO [dbo].[FieldReceivedValueMapping] ([FieldReceivedValueMappingID], [FieldName], [SourceValue], [TargetValue]) VALUES (225, 'CivilClass', '6', '4')
INSERT INTO [dbo].[FieldReceivedValueMapping] ([FieldReceivedValueMappingID], [FieldName], [SourceValue], [TargetValue]) VALUES (226, 'CompositionHousekeeping', '1', '0')
INSERT INTO [dbo].[FieldReceivedValueMapping] ([FieldReceivedValueMappingID], [FieldName], [SourceValue], [TargetValue]) VALUES (227, 'CompositionHousekeeping', '2', '1')
INSERT INTO [dbo].[FieldReceivedValueMapping] ([FieldReceivedValueMappingID], [FieldName], [SourceValue], [TargetValue]) VALUES (228, 'CompositionHousekeeping', '3', '2')
INSERT INTO [dbo].[FieldReceivedValueMapping] ([FieldReceivedValueMappingID], [FieldName], [SourceValue], [TargetValue]) VALUES (229, 'CompositionHousekeeping', '4', '2')
INSERT INTO [dbo].[FieldReceivedValueMapping] ([FieldReceivedValueMappingID], [FieldName], [SourceValue], [TargetValue]) VALUES (230, 'CompositionHousekeeping', '5', '4')
INSERT INTO [dbo].[FieldReceivedValueMapping] ([FieldReceivedValueMappingID], [FieldName], [SourceValue], [TargetValue]) VALUES (231, 'CompositionHousekeeping', '6', '2')
INSERT INTO [dbo].[FieldReceivedValueMapping] ([FieldReceivedValueMappingID], [FieldName], [SourceValue], [TargetValue]) VALUES (232, 'CompositionHousekeeping', '7', '7')
INSERT INTO [dbo].[FieldReceivedValueMapping] ([FieldReceivedValueMappingID], [FieldName], [SourceValue], [TargetValue]) VALUES (233, 'CompositionHousekeeping', '8', '7')
INSERT INTO [dbo].[FieldReceivedValueMapping] ([FieldReceivedValueMappingID], [FieldName], [SourceValue], [TargetValue]) VALUES (234, 'CompositionHousekeeping', '9', '7')
INSERT INTO [dbo].[FieldReceivedValueMapping] ([FieldReceivedValueMappingID], [FieldName], [SourceValue], [TargetValue]) VALUES (235, 'HousingType', '1', '0')
INSERT INTO [dbo].[FieldReceivedValueMapping] ([FieldReceivedValueMappingID], [FieldName], [SourceValue], [TargetValue]) VALUES (236, 'HousingType', '2', '1')
INSERT INTO [dbo].[FieldReceivedValueMapping] ([FieldReceivedValueMappingID], [FieldName], [SourceValue], [TargetValue]) VALUES (237, 'HousingType', '3', '2')
INSERT INTO [dbo].[FieldReceivedValueMapping] ([FieldReceivedValueMappingID], [FieldName], [SourceValue], [TargetValue]) VALUES (238, 'HousingType', '4', '2')
INSERT INTO [dbo].[FieldReceivedValueMapping] ([FieldReceivedValueMappingID], [FieldName], [SourceValue], [TargetValue]) VALUES (239, 'HousingType', '5', '4')
INSERT INTO [dbo].[FieldReceivedValueMapping] ([FieldReceivedValueMappingID], [FieldName], [SourceValue], [TargetValue]) VALUES (240, 'HousingType', '6', '2')
INSERT INTO [dbo].[FieldReceivedValueMapping] ([FieldReceivedValueMappingID], [FieldName], [SourceValue], [TargetValue]) VALUES (241, 'HousingType', '7', '2')
INSERT INTO [dbo].[FieldReceivedValueMapping] ([FieldReceivedValueMappingID], [FieldName], [SourceValue], [TargetValue]) VALUES (242, 'HousingType', '8', '4')
INSERT INTO [dbo].[FieldReceivedValueMapping] ([FieldReceivedValueMappingID], [FieldName], [SourceValue], [TargetValue]) VALUES (243, 'HousingType', '9', '2')
INSERT INTO [dbo].[FieldReceivedValueMapping] ([FieldReceivedValueMappingID], [FieldName], [SourceValue], [TargetValue]) VALUES (244, 'HousingType', '10', '2')
SET IDENTITY_INSERT [dbo].[FieldReceivedValueMapping] OFF
PRINT(N'Operation applied to 28 rows out of 28')
COMMIT TRANSACTION
GO
