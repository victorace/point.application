/*
Run this script on:

        pointtest.verzorgdeoverdracht.nl.TXX-DB-POINT-R    -  This database will be modified

to synchronize it with:

        ..Point

You are recommended to back up your database before running this script

Script created by SQL Compare version 12.3.3.4490 from Red Gate Software Ltd at 05/04/2019 14:57:40

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_Client_BirthDate_Includes] from [dbo].[Client]'
GO
IF EXISTS (SELECT 1 FROM sys.indexes WHERE name = N'IX_Client_BirthDate_Includes' AND object_id = OBJECT_ID(N'[dbo].[Client]'))
DROP INDEX [IX_Client_BirthDate_Includes] ON [dbo].[Client]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_Employee_DepartmentIDInactive] from [dbo].[Employee]'
GO
IF EXISTS (SELECT 1 FROM sys.indexes WHERE name = N'IX_Employee_DepartmentIDInactive' AND object_id = OBJECT_ID(N'[dbo].[Employee]'))
DROP INDEX [IX_Employee_DepartmentIDInactive] ON [dbo].[Employee]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_Employee_EmploeeID_NameFields] from [dbo].[Employee]'
GO
IF EXISTS (SELECT 1 FROM sys.indexes WHERE name = N'IX_Employee_EmploeeID_NameFields' AND object_id = OBJECT_ID(N'[dbo].[Employee]'))
DROP INDEX [IX_Employee_EmploeeID_NameFields] ON [dbo].[Employee]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Organization]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
IF COL_LENGTH(N'[dbo].[Organization]', N'OrganizationLogoID') IS NULL
ALTER TABLE [dbo].[Organization] ADD[OrganizationLogoID] [int] NULL
IF COL_LENGTH(N'[dbo].[Organization]', N'OrganizationLogoID') IS NULL
ALTER TABLE [dbo].[Organization] ADD
[OrganizationLogoID] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[FlowInstance]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
IF COL_LENGTH(N'[dbo].[FlowInstance]', N'RelatedTransferID') IS NULL
ALTER TABLE [dbo].[FlowInstance] ADD[RelatedTransferID] [int] NULL
IF COL_LENGTH(N'[dbo].[FlowInstance]', N'RelatedTransferID') IS NULL
ALTER TABLE [dbo].[FlowInstance] ADD
[RelatedTransferID] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Employee]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Employee] ALTER COLUMN [Gender] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_Employee_DepartmentIDInactive] on [dbo].[Employee]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE name = N'IX_Employee_DepartmentIDInactive' AND object_id = OBJECT_ID(N'[dbo].[Employee]'))
CREATE NONCLUSTERED INDEX [IX_Employee_DepartmentIDInactive] ON [dbo].[Employee] ([DepartmentID], [InActive]) INCLUDE ([BIG], [ChangePasswordByEmployee], [CreatedWithAutoCreateSetID], [DigitalSignatureID], [EditAllForms], [EmailAddress], [EmployeeID], [ExternID], [FirstName], [Gender], [IPAddresses], [LastName], [LastRealLoginDate], [LockAction], [MFAConfirmedDate], [MFAKey], [MFANumber], [MFARequired], [MiddleName], [ModifiedTimeStamp], [PhoneNumber], [Position], [ReadOnlyReciever], [ReadOnlySender], [UserId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_Employee_EmploeeID_NameFields] on [dbo].[Employee]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE name = N'IX_Employee_EmploeeID_NameFields' AND object_id = OBJECT_ID(N'[dbo].[Employee]'))
CREATE NONCLUSTERED INDEX [IX_Employee_EmploeeID_NameFields] ON [dbo].[Employee] ([EmployeeID]) INCLUDE ([BIG], [ChangePasswordByEmployee], [CreatedWithAutoCreateSetID], [DepartmentID], [DigitalSignatureID], [EditAllForms], [EmailAddress], [ExternID], [FirstName], [Gender], [InActive], [IPAddresses], [LastName], [LockAction], [MFAConfirmedDate], [MFANumber], [MFARequired], [MiddleName], [ModifiedTimeStamp], [PhoneNumber], [Position], [ReadOnlyReciever], [ReadOnlySender], [UserId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Client]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Client] ALTER COLUMN [Gender] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Client] ALTER COLUMN [CivilClass] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Client] ALTER COLUMN [CompositionHousekeeping] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Client] ALTER COLUMN [HousingType] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_Client_BirthDate_Includes] on [dbo].[Client]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE name = N'IX_Client_BirthDate_Includes' AND object_id = OBJECT_ID(N'[dbo].[Client]'))
CREATE NONCLUSTERED INDEX [IX_Client_BirthDate_Includes] ON [dbo].[Client] ([BirthDate]) INCLUDE ([CivilServiceNumber], [ClientID], [Gender], [HealthInsuranceCompanyID], [InsuranceNumber], [MaidenName], [PatientNumber])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[MutClient]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[MutClient] ALTER COLUMN [HousingType] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[TransferAttachment]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
IF COL_LENGTH(N'[dbo].[TransferAttachment]', N'RelatedAttachmentID') IS NULL
ALTER TABLE [dbo].[TransferAttachment] ADD[RelatedAttachmentID] [int] NULL
IF COL_LENGTH(N'[dbo].[TransferAttachment]', N'RelatedAttachmentID') IS NULL
ALTER TABLE [dbo].[TransferAttachment] ADD
[RelatedAttachmentID] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[MutOrganization]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
IF COL_LENGTH(N'[dbo].[MutOrganization]', N'OrganizationLogoID') IS NULL
ALTER TABLE [dbo].[MutOrganization] ADD[OrganizationLogoID] [int] NULL
IF COL_LENGTH(N'[dbo].[MutOrganization]', N'OrganizationLogoID') IS NULL
ALTER TABLE [dbo].[MutOrganization] ADD
[OrganizationLogoID] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Templet]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
IF COL_LENGTH(N'[dbo].[Templet]', N'SortOrder') IS NULL
ALTER TABLE [dbo].[Templet] ADD[SortOrder] [int] NULL
IF COL_LENGTH(N'[dbo].[Templet]', N'SortOrder') IS NULL
ALTER TABLE [dbo].[Templet] ADD
[SortOrder] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[FlowDefinitionFormTypeFlowField]'
GO
IF OBJECT_ID(N'[dbo].[FlowDefinitionFormTypeFlowField]', 'U') IS NULL
CREATE TABLE [dbo].[FlowDefinitionFormTypeFlowField]
(
[FlowDefinitionFormTypeFlowFieldID] [int] NOT NULL IDENTITY(1, 1),
[FlowDefinitionID] [int] NOT NULL,
[FormTypeID] [int] NOT NULL,
[FlowFieldID] [int] NOT NULL,
[TakeOver] [bit] NOT NULL CONSTRAINT [DF_FlowDefinitionFormTypeFlowField_TakeOver] DEFAULT ((1))
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[TransferMemo]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
IF COL_LENGTH(N'[dbo].[TransferMemo]', N'RelatedMemoID') IS NULL
ALTER TABLE [dbo].[TransferMemo] ADD[RelatedMemoID] [int] NULL
IF COL_LENGTH(N'[dbo].[TransferMemo]', N'RelatedMemoID') IS NULL
ALTER TABLE [dbo].[TransferMemo] ADD
[RelatedMemoID] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[PersonData]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[PersonData] ALTER COLUMN [Gender] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[OrganizationLogo]'
GO
IF OBJECT_ID(N'[dbo].[OrganizationLogo]', 'U') IS NULL
CREATE TABLE [dbo].[OrganizationLogo]
(
[OrganizationLogoID] [int] NOT NULL IDENTITY(1, 1),
[ContentLength] [int] NOT NULL,
[ContentType] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FileName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FileData] [image] NOT NULL,
[InActive] [bit] NOT NULL,
[ModifiedTimeStamp] [datetime] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_OrganizationLogo] on [dbo].[OrganizationLogo]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE name = N'PK_OrganizationLogo' AND object_id = OBJECT_ID(N'[dbo].[OrganizationLogo]'))
ALTER TABLE [dbo].[OrganizationLogo] ADD CONSTRAINT [PK_OrganizationLogo] PRIMARY KEY CLUSTERED  ([OrganizationLogoID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[Organization]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Organization_OrganizationLogo]', 'F') AND parent_object_id = OBJECT_ID(N'[dbo].[Organization]', 'U'))
ALTER TABLE [dbo].[Organization] ADD CONSTRAINT [FK_Organization_OrganizationLogo] FOREIGN KEY ([OrganizationLogoID]) REFERENCES [dbo].[OrganizationLogo] ([OrganizationLogoID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
