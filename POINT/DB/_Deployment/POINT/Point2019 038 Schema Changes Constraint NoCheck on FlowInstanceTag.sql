/*
Run this script on:

        pointtest.verzorgdeoverdracht.nl.TXX-DB-POINT-R    -  This database will be modified

to synchronize it with:

        ..PointDev

You are recommended to back up your database before running this script

Script created by SQL Compare version 12.3.3.4490 from Red Gate Software Ltd at 14/05/2019 12:23:26

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[FlowInstanceTag]'
GO
IF EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_FlowInstanceTag_FlowInstanceReportValues]', 'F') AND parent_object_id = OBJECT_ID(N'[dbo].[FlowInstanceTag]', 'U'))
ALTER TABLE [dbo].[FlowInstanceTag] DROP CONSTRAINT [FK_FlowInstanceTag_FlowInstanceReportValues]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[FlowInstanceTag]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_FlowInstanceTag_FlowInstanceReportValues]', 'F') AND parent_object_id = OBJECT_ID(N'[dbo].[FlowInstanceTag]', 'U'))
ALTER TABLE [dbo].[FlowInstanceTag] WITH NOCHECK  ADD CONSTRAINT [FK_FlowInstanceTag_FlowInstanceReportValues] FOREIGN KEY ([FlowInstanceID]) REFERENCES [dbo].[FlowInstanceReportValues] ([FlowInstanceID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Disabling constraints on [dbo].[FlowInstanceTag]'
GO
IF EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_FlowInstanceTag_FlowInstanceReportValues]', 'F') AND parent_object_id = OBJECT_ID(N'[dbo].[FlowInstanceTag]', 'U'))
ALTER TABLE [dbo].[FlowInstanceTag] NOCHECK CONSTRAINT [FK_FlowInstanceTag_FlowInstanceReportValues]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
