/*
Run this script on:

pointreview.verzorgdeoverdracht.nl.TXX-DB-POINT-R    -  This database will be modified

to synchronize it with:

..Point

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 12.3.3.4490 from Red Gate Software Ltd at 08/05/2019 14:41:21

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION

PRINT(N'Drop constraints from [dbo].[FlowFieldAttribute]')
ALTER TABLE [dbo].[FlowFieldAttribute] NOCHECK CONSTRAINT [FK_dbo.FlowFieldAttribute_dbo.FlowField_FlowFieldID]
ALTER TABLE [dbo].[FlowFieldAttribute] NOCHECK CONSTRAINT [FK_dbo.FlowFieldAttribute_dbo.FormType_FormTypeID]
ALTER TABLE [dbo].[FlowFieldAttribute] NOCHECK CONSTRAINT [FK_FlowFieldAttribute_FlowField]

PRINT(N'Update row in [dbo].[FlowFieldAttribute]')
UPDATE [dbo].[FlowFieldAttribute] SET [Required]=1 WHERE [FlowFieldAttributeID] = 275

PRINT(N'Add constraints to [dbo].[FlowFieldAttribute]')
ALTER TABLE [dbo].[FlowFieldAttribute] WITH CHECK CHECK CONSTRAINT [FK_dbo.FlowFieldAttribute_dbo.FlowField_FlowFieldID]
ALTER TABLE [dbo].[FlowFieldAttribute] WITH CHECK CHECK CONSTRAINT [FK_dbo.FlowFieldAttribute_dbo.FormType_FormTypeID]
ALTER TABLE [dbo].[FlowFieldAttribute] WITH CHECK CHECK CONSTRAINT [FK_FlowFieldAttribute_FlowField]
COMMIT TRANSACTION
GO
