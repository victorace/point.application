/*
Run this script on:

pointreview.verzorgdeoverdracht.nl.TXX-DB-POINT-R    -  This database will be modified

to synchronize it with:

..PointDev

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 12.3.3.4490 from Red Gate Software Ltd at 24/09/2019 11:48:03

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION

PRINT(N'Add row to [dbo].[RazorTemplate]')
SET IDENTITY_INSERT [dbo].[RazorTemplate] ON
INSERT INTO [dbo].[RazorTemplate] ([RazorTemplateID], [TemplateName], [TemplateCode], [Contents], [CommunicationLogType]) VALUES (1072, N'Dossier is verplaatst naar uw afdeling/organisatie', N'DossierIsRelocatedToYou', N'@model Point.Database.Models.ViewModels.EmailRelocateDossierViewModel

Geachte heer/mevrouw,<br />
<br />
Er is zojuist een Transferdossier verplaatst van<br />
@Model.SourceDepartment<br />
naar de onderstaande afdeling van uw organisatie<br />
@Model.DestinationDepartment<br />
<br />
De reden van verplaatsing is: <br />
<pre>@Model.RelocateReason</pre>
<br />
Het gaat om @Model.ClientName<br />
<br />
U kunt geen antwoord terugmailen op dit bericht, maar klik op onderstaande link om het Transferdossier te openen.<br />
<br />
<a href="@Model.DossierUrl">Dossier patient</a><br />
<br />
Met vriendelijke groet,<br />
<br />
@Model.ResponsibleEmployee<br />
@Model.ResponsibleDepartment<br />
@Model.ResponsibleTelephoneNumber', 23)
SET IDENTITY_INSERT [dbo].[RazorTemplate] OFF
COMMIT TRANSACTION
GO
