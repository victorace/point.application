DISABLE TRIGGER dbo.Client_InsertUpdate ON dbo.Client
GO
DISABLE TRIGGER dbo.Employee_Sync_Update ON dbo.Employee
GO

UPDATE dbo.Client SET Gender = '1' WHERE Gender IN ('OM','m')
GO
UPDATE dbo.Client SET Gender = '2' WHERE Gender IN ('OF','v')
GO

UPDATE dbo.Employee SET Gender = '1' WHERE Gender IN ('OM','m')
GO
UPDATE dbo.Employee SET Gender = '2' WHERE Gender IN ('OF','v')
GO
UPDATE dbo.Employee SET Gender = '0' WHERE Gender IN ('OUN','o')
GO
UPDATE dbo.Employee SET Gender = NULL WHERE Gender NOT IN ('0','1','2') AND Gender IS NOT NULL
GO

UPDATE dbo.PersonData SET Gender = '1' WHERE Gender IN ('OM','m')
GO
UPDATE dbo.PersonData SET Gender = '2' WHERE Gender IN ('OF','v')
GO

ENABLE TRIGGER dbo.Client_InsertUpdate ON dbo.Client
GO
ENABLE TRIGGER dbo.Employee_Sync_Update ON dbo.Employee
GO