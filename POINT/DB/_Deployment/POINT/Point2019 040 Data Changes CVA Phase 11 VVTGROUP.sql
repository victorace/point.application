/*
Run this script on:

pointreview.verzorgdeoverdracht.nl.TXX-DB-POINT-R    -  This database will be modified

to synchronize it with:

..PointDev

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 12.3.3.4490 from Red Gate Software Ltd at 14/05/2019 13:01:21

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION

PRINT(N'Drop constraints from [dbo].[PhaseDefinition]')
ALTER TABLE [dbo].[PhaseDefinition] NOCHECK CONSTRAINT [FK_dbo.PhaseDefinition_dbo.FlowDefinition_FlowDefinitionID]
ALTER TABLE [dbo].[PhaseDefinition] NOCHECK CONSTRAINT [FK_dbo.PhaseDefinition_dbo.OrganizationType_OrganizationTypeID]

PRINT(N'Drop constraint FK_dbo.FlowPhaseAttribute_dbo.PhaseDefinition_PhaseDefinitionID from [dbo].[FlowPhaseAttribute]')
ALTER TABLE [dbo].[FlowPhaseAttribute] NOCHECK CONSTRAINT [FK_dbo.FlowPhaseAttribute_dbo.PhaseDefinition_PhaseDefinitionID]

PRINT(N'Drop constraint FK_dbo.GeneralActionPhase_dbo.PhaseDefinition_PhaseDefinitionID from [dbo].[GeneralActionPhase]')
ALTER TABLE [dbo].[GeneralActionPhase] NOCHECK CONSTRAINT [FK_dbo.GeneralActionPhase_dbo.PhaseDefinition_PhaseDefinitionID]

PRINT(N'Drop constraint FK_PhaseDefinitionNavigation_PhaseDefinition from [dbo].[PhaseDefinitionNavigation]')
ALTER TABLE [dbo].[PhaseDefinitionNavigation] NOCHECK CONSTRAINT [FK_PhaseDefinitionNavigation_PhaseDefinition]

PRINT(N'Drop constraint FK_PhaseDefinitionNavigation_TargetPhaseDefinition from [dbo].[PhaseDefinitionNavigation]')
ALTER TABLE [dbo].[PhaseDefinitionNavigation] NOCHECK CONSTRAINT [FK_PhaseDefinitionNavigation_TargetPhaseDefinition]

PRINT(N'Drop constraint FK_PhaseDefinitionRights_PhaseDefinition from [dbo].[PhaseDefinitionRights]')
ALTER TABLE [dbo].[PhaseDefinitionRights] NOCHECK CONSTRAINT [FK_PhaseDefinitionRights_PhaseDefinition]

PRINT(N'Update row in [dbo].[PhaseDefinition]')
UPDATE [dbo].[PhaseDefinition] SET [CodeGroup]=N'VVTGROUP' WHERE [PhaseDefinitionID] = 53

PRINT(N'Add constraints to [dbo].[PhaseDefinition]')
ALTER TABLE [dbo].[PhaseDefinition] WITH CHECK CHECK CONSTRAINT [FK_dbo.PhaseDefinition_dbo.FlowDefinition_FlowDefinitionID]
ALTER TABLE [dbo].[PhaseDefinition] WITH CHECK CHECK CONSTRAINT [FK_dbo.PhaseDefinition_dbo.OrganizationType_OrganizationTypeID]
ALTER TABLE [dbo].[FlowPhaseAttribute] WITH CHECK CHECK CONSTRAINT [FK_dbo.FlowPhaseAttribute_dbo.PhaseDefinition_PhaseDefinitionID]
ALTER TABLE [dbo].[GeneralActionPhase] WITH CHECK CHECK CONSTRAINT [FK_dbo.GeneralActionPhase_dbo.PhaseDefinition_PhaseDefinitionID]
ALTER TABLE [dbo].[PhaseDefinitionNavigation] WITH CHECK CHECK CONSTRAINT [FK_PhaseDefinitionNavigation_PhaseDefinition]
ALTER TABLE [dbo].[PhaseDefinitionNavigation] WITH CHECK CHECK CONSTRAINT [FK_PhaseDefinitionNavigation_TargetPhaseDefinition]
ALTER TABLE [dbo].[PhaseDefinitionRights] WITH CHECK CHECK CONSTRAINT [FK_PhaseDefinitionRights_PhaseDefinition]
COMMIT TRANSACTION
GO
