/*
Run this script on:

        pointtest.verzorgdeoverdracht.nl.TXX-DB-POINT-R    -  This database will be modified

to synchronize it with:

        ..Point

You are recommended to back up your database before running this script

Script created by SQL Compare version 12.3.3.4490 from Red Gate Software Ltd at 24/04/2019 16:36:23

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[FlowInstanceTag]'
GO
IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PK_FlowInstanceTag]', 'PK') AND parent_object_id = OBJECT_ID(N'[dbo].[FlowInstanceTag]', 'U'))
ALTER TABLE [dbo].[FlowInstanceTag] DROP CONSTRAINT [PK_FlowInstanceTag]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[FlowInstanceTag]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[FlowInstanceTag] ALTER COLUMN [Tag] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_FlowInstanceTag] on [dbo].[FlowInstanceTag]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE name = N'PK_FlowInstanceTag' AND object_id = OBJECT_ID(N'[dbo].[FlowInstanceTag]'))
ALTER TABLE [dbo].[FlowInstanceTag] ADD CONSTRAINT [PK_FlowInstanceTag] PRIMARY KEY CLUSTERED  ([FlowInstanceID], [Tag])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[FlowInstanceTag]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_FlowInstanceTag_FlowInstanceReportValues]', 'F') AND parent_object_id = OBJECT_ID(N'[dbo].[FlowInstanceTag]', 'U'))
ALTER TABLE [dbo].[FlowInstanceTag] ADD CONSTRAINT [FK_FlowInstanceTag_FlowInstanceReportValues] FOREIGN KEY ([FlowInstanceID]) REFERENCES [dbo].[FlowInstanceReportValues] ([FlowInstanceID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
