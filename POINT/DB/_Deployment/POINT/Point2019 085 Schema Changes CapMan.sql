/*
Run this script on a database with the schema represented by:

        LINKASSIST-NB20.PointDB    -  This database will be modified. The scripts folder will not be modified.

to synchronize it with:

        LINKASSIST-NB20.PointDB

You are recommended to back up your database before running this script

Script created by SQL Compare Engine version 12.3.3.2177 from Red Gate Software Ltd at 10/1/2019 5:58:55 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [ix_DepartmentCapacityPublic_Includes] on [dbo].[DepartmentCapacityPublic]'
GO
CREATE NONCLUSTERED INDEX [ix_DepartmentCapacityPublic_Includes] ON [dbo].[DepartmentCapacityPublic] ([DepartmentCapacityPublicID]) INCLUDE ([AfterCareGroupID], [AfterCareTypeID], [Capacity1], [Capacity2], [Capacity3], [Capacity4], [DepartmentName], [IsActive], [LocationCity], [LocationName], [LocationPostalCode], [OrganizationName], [RegionID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [ix_DepartmentCapacityPublic_RegionID_AfterCareGroupID_AfterCareTypeID] on [dbo].[DepartmentCapacityPublic]'
GO
CREATE NONCLUSTERED INDEX [ix_DepartmentCapacityPublic_RegionID_AfterCareGroupID_AfterCareTypeID] ON [dbo].[DepartmentCapacityPublic] ([RegionID], [AfterCareGroupID], [AfterCareTypeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO


/*

ADD TABLE [dbo].[RegionAfterCareType]

*/

SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[RegionAfterCareType]'
GO

CREATE TABLE [dbo].[RegionAfterCareType]
(
[RegionAfterCareTypeID] [int] NOT NULL IDENTITY(1, 1),
[RegionID] [int] NOT NULL,
[AfterCareTypeID] [int] NOT NULL
)

IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO



/*
Run this script on a database with the schema represented by:

        LINKASSIST-NB20.PointDB    -  This database will be modified. The scripts folder will not be modified.

to synchronize it with:

        LINKASSIST-NB20.PointDB

You are recommended to back up your database before running this script

Script created by SQL Compare Engine version 12.3.3.2177 from Red Gate Software Ltd at 10/1/2019 6:00:47 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_RegionAfterCareType] on [dbo].[RegionAfterCareType]'
GO
ALTER TABLE [dbo].[RegionAfterCareType] ADD CONSTRAINT [PK_RegionAfterCareType] PRIMARY KEY CLUSTERED  ([RegionAfterCareTypeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [ix_RegionAfterCareType_RegionID_AfterCareTypeID] on [dbo].[RegionAfterCareType]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [ix_RegionAfterCareType_RegionID_AfterCareTypeID] ON [dbo].[RegionAfterCareType] ([RegionID], [AfterCareTypeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO

