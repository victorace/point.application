/*
Run this script on:

        pointreview.verzorgdeoverdracht.nl.TXX-DE-DB-POINT-R    -  This database will be modified

to synchronize it with a database with the schema represented by:

        LINKASSIST-NB18.PointDev

You are recommended to back up your database before running this script

Script created by SQL Compare version 12.4.9.4945 from Red Gate Software Ltd at 8/27/2019 12:26:25 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[OrganizationProject]'
GO
ALTER TABLE [dbo].[OrganizationProject] DROP CONSTRAINT [DF_OrganizationProject_RequiredForReceiver]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[OrganizationProject]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[OrganizationProject] DROP
COLUMN [RequiredForReceiver]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
