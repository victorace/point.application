/*
Run this script on:

        pointtest.verzorgdeoverdracht.nl.TXX-DB-POINT-R    -  This database will be modified

to synchronize it with:

        ..Point

You are recommended to back up your database before running this script

Script created by SQL Compare version 12.3.3.4490 from Red Gate Software Ltd at 01/05/2019 15:42:26

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[AidProduct]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
IF COL_LENGTH(N'[dbo].[AidProduct]', N'Code') IS NULL
ALTER TABLE [dbo].[AidProduct] ADD[Code] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
IF COL_LENGTH(N'[dbo].[AidProduct]', N'InsurersUZOVIList') IS NULL
ALTER TABLE [dbo].[AidProduct] ADD[InsurersUZOVIList] [varchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
IF COL_LENGTH(N'[dbo].[AidProduct]', N'Code') IS NULL AND COL_LENGTH(N'[dbo].[AidProduct]', N'InsurersUZOVIList') IS NULL
ALTER TABLE [dbo].[AidProduct] ADD
[Code] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InsurersUZOVIList] [varchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[HealthInsurer]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
IF COL_LENGTH(N'[dbo].[HealthInsurer]', N'MediPointCode') IS NULL
ALTER TABLE [dbo].[HealthInsurer] ADD[MediPointCode] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
IF COL_LENGTH(N'[dbo].[HealthInsurer]', N'MediPointCode') IS NULL
ALTER TABLE [dbo].[HealthInsurer] ADD
[MediPointCode] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[CommunicationLog]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
IF COL_LENGTH(N'[dbo].[CommunicationLog]', N'ErrorMessage') IS NULL
ALTER TABLE [dbo].[CommunicationLog] ADD[ErrorMessage] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
IF COL_LENGTH(N'[dbo].[CommunicationLog]', N'ErrorMessage') IS NULL
ALTER TABLE [dbo].[CommunicationLog] ADD
[ErrorMessage] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
