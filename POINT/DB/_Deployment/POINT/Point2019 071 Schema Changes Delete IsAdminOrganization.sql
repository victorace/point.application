/*
Run this script on:

        pointtest.verzorgdeoverdracht.nl.TXX-DB-POINT-R    -  This database will be modified

to synchronize it with:

        ..PointDev

You are recommended to back up your database before running this script

Script created by SQL Compare version 12.3.3.4490 from Red Gate Software Ltd at 10/07/2019 14:05:29

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[Organization]'
GO
IF EXISTS (SELECT 1 FROM sys.columns WHERE name = N'IsAdminOrganization' AND object_id = OBJECT_ID(N'[dbo].[Organization]', 'U') AND default_object_id = OBJECT_ID(N'[dbo].[DF_Organization_IsAdminOrganization]', 'D'))
ALTER TABLE [dbo].[Organization] DROP CONSTRAINT [DF_Organization_IsAdminOrganization]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_Organization_OrganizationID_Includes] from [dbo].[Organization]'
GO
IF EXISTS (SELECT 1 FROM sys.indexes WHERE name = N'IX_Organization_OrganizationID_Includes' AND object_id = OBJECT_ID(N'[dbo].[Organization]'))
DROP INDEX [IX_Organization_OrganizationID_Includes] ON [dbo].[Organization]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO

PRINT N'Setting invisible on FlowDefinitionParticipation where IsAdminOrganization = 1'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
UPDATE fp SET fp.Participation = 3 from dbo.FlowDefinitionParticipation fp
INNER JOIN dbo.Location l ON l.LocationID = fp.LocationID
INNER JOIN dbo.Organization o ON o.OrganizationID = l.OrganizationID
WHERE o.IsAdminOrganization = 1 AND fp.Participation > 0
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO

PRINT N'Altering [dbo].[Organization]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
IF COL_LENGTH(N'[dbo].[Organization]', N'IsAdminOrganization') IS NOT NULL
ALTER TABLE [dbo].[Organization] DROP COLUMN [IsAdminOrganization]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_Organization_OrganizationID_Includes] on [dbo].[Organization]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE name = N'IX_Organization_OrganizationID_Includes' AND object_id = OBJECT_ID(N'[dbo].[Organization]'))
CREATE NONCLUSTERED INDEX [IX_Organization_OrganizationID_Includes] ON [dbo].[Organization] ([OrganizationID]) INCLUDE ([AGB], [ChangePasswordPeriod], [CreateUserViaSSO], [DeBlockEmails], [DeBlockEmailsToOrganization], [HashPrefix], [HL7ConnectionCallFromPoint], [HL7ConnectionDifferentMenu], [HL7Interface], [HL7InterfaceExtraFields], [Image], [ImageName], [Inactive], [InterfaceNumber], [IPAddresses], [MessageToGPByClose], [MessageToGPByDefinitive], [MFAAuthType], [MFARequired], [MFATimeout], [ModifiedByEmployeeID], [ModifiedByScreenID], [ModifiedTimeStamp], [Name], [NedapCryptoCertificateID], [NedapMedewerkerNummer], [OrganizationTypeID], [PageLockTimeout], [RegionID], [ShortConnectionID], [ShowUnloadMessage], [SSO], [SSOHashType], [SSOKey], [URL], [UseAnonymousClient], [UseDigitalSignature], [UsesPalliativeCareInVO], [UseTasks], [WSInterface], [WSInterfaceExtraFields], [ZorgmailPassword], [ZorgmailUsername])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
