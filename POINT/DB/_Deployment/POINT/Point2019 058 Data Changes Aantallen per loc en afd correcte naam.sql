/*
Run this script on:

pointreview.verzorgdeoverdracht.nl.TXX-DB-POINT-R    -  This database will be modified

to synchronize it with:

..PointDev

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 12.3.3.4490 from Red Gate Software Ltd at 13/06/2019 08:32:47

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION

PRINT(N'Update rows in [dbo].[ReportDefinition]')
UPDATE [dbo].[ReportDefinition] SET [ReportName]=N'Aantallen per locatie / afdeling' WHERE [ReportDefinitionID] = 1
UPDATE [dbo].[ReportDefinition] SET [ReportName]=N'Aantallen per locatie / afdeling' WHERE [ReportDefinitionID] = 2
PRINT(N'Operation applied to 2 rows out of 2')
COMMIT TRANSACTION
GO
