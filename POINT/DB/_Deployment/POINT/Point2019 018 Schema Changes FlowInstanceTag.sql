/*
Run this script on:

        pointtest.verzorgdeoverdracht.nl.TXX-DB-POINT-R    -  This database will be modified

to synchronize it with:

        ..Point

You are recommended to back up your database before running this script

Script created by SQL Compare version 12.3.3.4490 from Red Gate Software Ltd at 23/04/2019 11:28:14

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[FlowInstanceTag]'
GO
IF OBJECT_ID(N'[dbo].[FlowInstanceTag]', 'U') IS NULL
CREATE TABLE [dbo].[FlowInstanceTag]
(
[FlowInstanceID] [int] NOT NULL,
[Tag] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_FlowInstanceTag] on [dbo].[FlowInstanceTag]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE name = N'PK_FlowInstanceTag' AND object_id = OBJECT_ID(N'[dbo].[FlowInstanceTag]'))
ALTER TABLE [dbo].[FlowInstanceTag] ADD CONSTRAINT [PK_FlowInstanceTag] PRIMARY KEY CLUSTERED  ([FlowInstanceID], [Tag])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
