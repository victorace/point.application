-- eenmalige script:
-- Deze zcript zorgt voor een update op nieuwe kolom "RelatedTransferID"
-- "-1" betekent dat voor deze flowinstance hoef niet meer gezocht worden op related transfers (omdat ze hebben waarschijnlijk al aanvraag formulier, e-overdracht, comm.journaal,...)
-- Als deze belijft op NULL, bij het opennen van dashboard van bestande dossiers, wordt er gecontroleerd voor related transfers.


Update FlowInstance set RelatedTransferID = -1 Where RelatedTransferID is null