/*
Run this script on:

pointreview.verzorgdeoverdracht.nl.TXX-DE-DB-POINT-R    -  This database will be modified

to synchronize it with:

LINKASSIST-NB18.PointDev

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 12.4.12.5042 from Red Gate Software Ltd at 8/19/2019 1:41:24 PM

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION

PRINT(N'Drop constraints from [dbo].[FlowDefinition]')
ALTER TABLE [dbo].[FlowDefinition] NOCHECK CONSTRAINT [FK_dbo.FlowDefinition_dbo.FlowType_FlowTypeID]

PRINT(N'Update rows in [dbo].[FlowDefinition]')
UPDATE [dbo].[FlowDefinition] SET [Description]=N'Transfer van Thuiszorg/Verpleeghuis naar Ziekenhuis', [SortOrder]=3 WHERE [FlowDefinitionID] = 1
UPDATE [dbo].[FlowDefinition] SET [Description]=N'Transfer van Ziekenhuis naar Thuiszorg/Verpleeghuis standaard', [SortOrder]=1 WHERE [FlowDefinitionID] = 2
UPDATE [dbo].[FlowDefinition] SET [Description]=N'Transfer van Ziekenhuis naar Thuiszorg/Verpleeghuis zonder Transferpunt', [SortOrder]=2 WHERE [FlowDefinitionID] = 3
UPDATE [dbo].[FlowDefinition] SET [Description]=N'Transfer van Ziekenhuis naar Thuiszorg/Verpleeghuis met CVA registratie', [SortOrder]=7 WHERE [FlowDefinitionID] = 4
UPDATE [dbo].[FlowDefinition] SET [SortOrder]=6 WHERE [FlowDefinitionID] = 5
UPDATE [dbo].[FlowDefinition] SET [Description]=N'Transfer van HA naar Thuiszorg/Verpleeghuis', [SortOrder]=5 WHERE [FlowDefinitionID] = 6
UPDATE [dbo].[FlowDefinition] SET [Description]=N'Transfer van Thuiszorg/Verpleeghuis naar Thuiszorg/Verpleeghuis', [SortOrder]=4 WHERE [FlowDefinitionID] = 7
PRINT(N'Operation applied to 7 rows out of 7')

PRINT(N'Add constraints to [dbo].[FlowDefinition]')
ALTER TABLE [dbo].[FlowDefinition] WITH CHECK CHECK CONSTRAINT [FK_dbo.FlowDefinition_dbo.FlowType_FlowTypeID]
COMMIT TRANSACTION
GO
