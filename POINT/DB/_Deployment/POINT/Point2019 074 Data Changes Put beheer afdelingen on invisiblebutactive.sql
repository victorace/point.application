PRINT 'Zet alle beheer afdelingen op Invisible but active op bestaaande gekoppelde flows'

INSERT INTO dbo.FlowDefinitionParticipation
	( FlowDefinitionID ,
		LocationID ,
		DepartmentID ,
		Participation ,
		FlowDirection ,
		ModifiedTimeStamp
	)

SELECT fdpl.FlowDefinitionID, l.LocationID, d.DepartmentID, 3, fdpl.FlowDirection, GETDATE()
	FROM dbo.Organization o
		INNER JOIN dbo.Location l ON l.OrganizationID = o.OrganizationID
		INNER JOIN dbo.Department d ON d.LocationID = l.LocationID
		LEFT OUTER JOIN dbo.FlowDefinitionParticipation fdp ON fdp.DepartmentID = d.DepartmentID
		INNER JOIN dbo.FlowDefinitionParticipation fdpl ON fdpl.LocationID = l.LocationID

	WHERE d.Inactive = 0 AND l.Inactive = 0 AND o.Inactive = 0
	AND d.Name LIKE '%beheer%' and fdp.Participation is NULL

	ORDER BY l.OrganizationID, d.DepartmentID, fdpl.FlowDefinitionID

PRINT 'Done'