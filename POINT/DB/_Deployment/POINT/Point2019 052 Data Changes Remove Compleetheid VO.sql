/*
Run this script on:

pointreview.verzorgdeoverdracht.nl.TXX-DB-POINT-R    -  This database will be modified

to synchronize it with:

..PointDev

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 12.3.3.4490 from Red Gate Software Ltd at 31/05/2019 09:05:18

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION

PRINT(N'Drop constraints from [dbo].[ReportDefinitionFlowDefinition]')
ALTER TABLE [dbo].[ReportDefinitionFlowDefinition] NOCHECK CONSTRAINT [FK_ReportDefinitionFlowDefinition_FlowDefinition]
ALTER TABLE [dbo].[ReportDefinitionFlowDefinition] NOCHECK CONSTRAINT [FK_ReportDefinitionFlowDefinition_FlowDefinition1]

PRINT(N'Delete rows from [dbo].[ReportDefinitionFlowDefinition]')
DELETE FROM [dbo].[ReportDefinitionFlowDefinition] WHERE [ReportDefinitionID] = 32 AND [FlowDefinitionID] = 1
DELETE FROM [dbo].[ReportDefinitionFlowDefinition] WHERE [ReportDefinitionID] = 32 AND [FlowDefinitionID] = 2
DELETE FROM [dbo].[ReportDefinitionFlowDefinition] WHERE [ReportDefinitionID] = 32 AND [FlowDefinitionID] = 3
DELETE FROM [dbo].[ReportDefinitionFlowDefinition] WHERE [ReportDefinitionID] = 32 AND [FlowDefinitionID] = 4
DELETE FROM [dbo].[ReportDefinitionFlowDefinition] WHERE [ReportDefinitionID] = 32 AND [FlowDefinitionID] = 5
DELETE FROM [dbo].[ReportDefinitionFlowDefinition] WHERE [ReportDefinitionID] = 32 AND [FlowDefinitionID] = 6
DELETE FROM [dbo].[ReportDefinitionFlowDefinition] WHERE [ReportDefinitionID] = 32 AND [FlowDefinitionID] = 7
PRINT(N'Operation applied to 7 rows out of 7')

PRINT(N'Delete row from [dbo].[ReportDefinition]')
DELETE FROM [dbo].[ReportDefinition] WHERE [ReportDefinitionID] = 32

PRINT(N'Add constraints to [dbo].[ReportDefinitionFlowDefinition]')
ALTER TABLE [dbo].[ReportDefinitionFlowDefinition] WITH CHECK CHECK CONSTRAINT [FK_ReportDefinitionFlowDefinition_FlowDefinition]
ALTER TABLE [dbo].[ReportDefinitionFlowDefinition] WITH CHECK CHECK CONSTRAINT [FK_ReportDefinitionFlowDefinition_FlowDefinition1]
COMMIT TRANSACTION
GO
