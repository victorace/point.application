/*
Run this script on:

pointreview.verzorgdeoverdracht.nl.TXX-DB-POINT-R    -  This database will be modified

to synchronize it with:

LINKASSIST-NB18.2019T2

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 12.4.12.5042 from Red Gate Software Ltd at 5/7/2019 5:07:40 PM

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION

PRINT(N'Delete rows from [dbo].[FlowDefinitionFormTypeFlowField]')
DELETE FROM [dbo].[FlowDefinitionFormTypeFlowField] WHERE [FlowDefinitionFormTypeFlowFieldID] = 577 AND [FlowDefinitionID] = 7 AND [FormTypeID] = 243 AND [FlowFieldID] = 209 AND [TakeOver] = 1
DELETE FROM [dbo].[FlowDefinitionFormTypeFlowField] WHERE [FlowDefinitionFormTypeFlowFieldID] = 578 AND [FlowDefinitionID] = 7 AND [FormTypeID] = 243 AND [FlowFieldID] = 205 AND [TakeOver] = 1
DELETE FROM [dbo].[FlowDefinitionFormTypeFlowField] WHERE [FlowDefinitionFormTypeFlowFieldID] = 579 AND [FlowDefinitionID] = 7 AND [FormTypeID] = 243 AND [FlowFieldID] = 198 AND [TakeOver] = 1
DELETE FROM [dbo].[FlowDefinitionFormTypeFlowField] WHERE [FlowDefinitionFormTypeFlowFieldID] = 580 AND [FlowDefinitionID] = 7 AND [FormTypeID] = 243 AND [FlowFieldID] = 195 AND [TakeOver] = 1
DELETE FROM [dbo].[FlowDefinitionFormTypeFlowField] WHERE [FlowDefinitionFormTypeFlowFieldID] = 581 AND [FlowDefinitionID] = 7 AND [FormTypeID] = 243 AND [FlowFieldID] = 192 AND [TakeOver] = 1
DELETE FROM [dbo].[FlowDefinitionFormTypeFlowField] WHERE [FlowDefinitionFormTypeFlowFieldID] = 582 AND [FlowDefinitionID] = 7 AND [FormTypeID] = 243 AND [FlowFieldID] = 201 AND [TakeOver] = 1
DELETE FROM [dbo].[FlowDefinitionFormTypeFlowField] WHERE [FlowDefinitionFormTypeFlowFieldID] = 583 AND [FlowDefinitionID] = 7 AND [FormTypeID] = 243 AND [FlowFieldID] = 206 AND [TakeOver] = 1
PRINT(N'Operation applied to 7 rows out of 7')

PRINT(N'Add rows to [dbo].[FlowDefinitionFormTypeFlowField]')
SET IDENTITY_INSERT [dbo].[FlowDefinitionFormTypeFlowField] ON
INSERT INTO [dbo].[FlowDefinitionFormTypeFlowField] ([FlowDefinitionFormTypeFlowFieldID], [FlowDefinitionID], [FormTypeID], [FlowFieldID], [TakeOver]) VALUES (577, 7, 243, 209, 0)
INSERT INTO [dbo].[FlowDefinitionFormTypeFlowField] ([FlowDefinitionFormTypeFlowFieldID], [FlowDefinitionID], [FormTypeID], [FlowFieldID], [TakeOver]) VALUES (578, 7, 243, 205, 0)
INSERT INTO [dbo].[FlowDefinitionFormTypeFlowField] ([FlowDefinitionFormTypeFlowFieldID], [FlowDefinitionID], [FormTypeID], [FlowFieldID], [TakeOver]) VALUES (579, 7, 243, 198, 0)
INSERT INTO [dbo].[FlowDefinitionFormTypeFlowField] ([FlowDefinitionFormTypeFlowFieldID], [FlowDefinitionID], [FormTypeID], [FlowFieldID], [TakeOver]) VALUES (580, 7, 243, 195, 0)
INSERT INTO [dbo].[FlowDefinitionFormTypeFlowField] ([FlowDefinitionFormTypeFlowFieldID], [FlowDefinitionID], [FormTypeID], [FlowFieldID], [TakeOver]) VALUES (581, 7, 243, 192, 0)
INSERT INTO [dbo].[FlowDefinitionFormTypeFlowField] ([FlowDefinitionFormTypeFlowFieldID], [FlowDefinitionID], [FormTypeID], [FlowFieldID], [TakeOver]) VALUES (582, 7, 243, 201, 0)
INSERT INTO [dbo].[FlowDefinitionFormTypeFlowField] ([FlowDefinitionFormTypeFlowFieldID], [FlowDefinitionID], [FormTypeID], [FlowFieldID], [TakeOver]) VALUES (583, 7, 243, 206, 0)
SET IDENTITY_INSERT [dbo].[FlowDefinitionFormTypeFlowField] OFF
PRINT(N'Operation applied to 7 rows out of 7')
COMMIT TRANSACTION
GO
