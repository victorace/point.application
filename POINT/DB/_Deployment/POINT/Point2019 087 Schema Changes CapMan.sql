/*
Run this script on a database with the schema represented by:

        LINKASSIST-NB20.PointDB    -  This database will be modified. The scripts folder will not be modified.

to synchronize it with:

        LINKASSIST-NB20.PointDB

You are recommended to back up your database before running this script

Script created by SQL Compare Engine version 12.3.3.2177 from Red Gate Software Ltd at 10/1/2019 5:58:55 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [dbo].[DepartmentCapacityPublic]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[DepartmentCapacityPublic] ADD
[Latitude] [float] NOT NULL CONSTRAINT [DF__Departmen__Latit__3AE33C88] DEFAULT ((0)),
[Longitude] [float] NOT NULL CONSTRAINT [DF__Departmen__Longi__3BD760C1] DEFAULT ((0))


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [ix_DepartmentCapacityPublic_Includes] on [dbo].[DepartmentCapacityPublic]'
GO

IF EXISTS (
	SELECT * FROM sys.indexes i 
	INNER JOIN sys.objects o on i.object_id = o.object_id
	WHERE i.name = 'ix_DepartmentCapacityPublic_Includes' AND o.name = 'DepartmentCapacityPublic'
)
BEGIN
	PRINT 'index bestaat al'
	DROP INDEX DepartmentCapacityPublic.ix_DepartmentCapacityPublic_Includes
END 

CREATE NONCLUSTERED INDEX [ix_DepartmentCapacityPublic_Includes] ON [dbo].[DepartmentCapacityPublic] ([DepartmentCapacityPublicID]) INCLUDE ([RegionID], [AfterCareGroupID], [AfterCareTypeID], [Capacity1], [Capacity2], [Capacity3], [Capacity4], [DepartmentName], [IsActive], [LocationCity], [LocationName], [LocationPostalCode], [Latitude], [Longitude], [OrganizationName])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO

IF EXISTS (
	SELECT * FROM sys.indexes i 
	INNER JOIN sys.objects o on i.object_id = o.object_id
	WHERE i.name = 'ix_DepartmentCapacityPublic_RegionID_AfterCareGroupID_AfterCareTypeID' AND o.name = 'DepartmentCapacityPublic'
)
BEGIN
	PRINT 'index bestaat al'
	DROP INDEX DepartmentCapacityPublic.ix_DepartmentCapacityPublic_RegionID_AfterCareGroupID_AfterCareTypeID
END


PRINT N'Creating index [ix_DepartmentCapacityPublic_RegionID_AfterCareGroupID_AfterCareTypeID] on [dbo].[DepartmentCapacityPublic]'
GO
CREATE NONCLUSTERED INDEX [ix_DepartmentCapacityPublic_RegionID_AfterCareGroupID_AfterCareTypeID] ON [dbo].[DepartmentCapacityPublic] ([RegionID], [AfterCareGroupID], [AfterCareTypeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO



