/*
Run this script on:

        pointtest.verzorgdeoverdracht.nl.TXX-DB-POINT-R    -  This database will be modified

to synchronize it with:

        ..Point

You are recommended to back up your database before running this script

Script created by SQL Compare version 12.3.3.4490 from Red Gate Software Ltd at 23/04/2019 14:30:24

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[OrganizationCSVTemplate]'
GO
IF OBJECT_ID(N'[dbo].[OrganizationCSVTemplate]', 'U') IS NULL
CREATE TABLE [dbo].[OrganizationCSVTemplate]
(
[OrganizationCSVTemplateID] [int] NOT NULL IDENTITY(1, 1),
[OrganizationID] [int] NOT NULL,
[CSVTemplateTypeID] [int] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_OrganizationCSVTemplate_1] on [dbo].[OrganizationCSVTemplate]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE name = N'PK_OrganizationCSVTemplate_1' AND object_id = OBJECT_ID(N'[dbo].[OrganizationCSVTemplate]'))
ALTER TABLE [dbo].[OrganizationCSVTemplate] ADD CONSTRAINT [PK_OrganizationCSVTemplate_1] PRIMARY KEY CLUSTERED  ([OrganizationCSVTemplateID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_OrganizationCSVTemplate_OrganizationIDCSVTemplateTypeID] on [dbo].[OrganizationCSVTemplate]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE name = N'IX_OrganizationCSVTemplate_OrganizationIDCSVTemplateTypeID' AND object_id = OBJECT_ID(N'[dbo].[OrganizationCSVTemplate]'))
CREATE UNIQUE NONCLUSTERED INDEX [IX_OrganizationCSVTemplate_OrganizationIDCSVTemplateTypeID] ON [dbo].[OrganizationCSVTemplate] ([OrganizationID], [CSVTemplateTypeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[OrganizationCSVTemplateColumn]'
GO
IF OBJECT_ID(N'[dbo].[OrganizationCSVTemplateColumn]', 'U') IS NULL
CREATE TABLE [dbo].[OrganizationCSVTemplateColumn]
(
[OrganizationCSVTemplateID] [int] NOT NULL,
[ColumnSource] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Position] [int] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_OrganizationCSVTemplateColumns] on [dbo].[OrganizationCSVTemplateColumn]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE name = N'PK_OrganizationCSVTemplateColumns' AND object_id = OBJECT_ID(N'[dbo].[OrganizationCSVTemplateColumn]'))
ALTER TABLE [dbo].[OrganizationCSVTemplateColumn] ADD CONSTRAINT [PK_OrganizationCSVTemplateColumns] PRIMARY KEY CLUSTERED  ([OrganizationCSVTemplateID], [ColumnSource])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[OrganizationCSVTemplateColumn]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OrganizationCSVTemplateColumn_OrganizationCSVTemplate]', 'F') AND parent_object_id = OBJECT_ID(N'[dbo].[OrganizationCSVTemplateColumn]', 'U'))
ALTER TABLE [dbo].[OrganizationCSVTemplateColumn] ADD CONSTRAINT [FK_OrganizationCSVTemplateColumn_OrganizationCSVTemplate] FOREIGN KEY ([OrganizationCSVTemplateID]) REFERENCES [dbo].[OrganizationCSVTemplate] ([OrganizationCSVTemplateID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[OrganizationCSVTemplate]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OrganizationCSVTemplate_OrganizationCSVTemplate]', 'F') AND parent_object_id = OBJECT_ID(N'[dbo].[OrganizationCSVTemplate]', 'U'))
ALTER TABLE [dbo].[OrganizationCSVTemplate] ADD CONSTRAINT [FK_OrganizationCSVTemplate_OrganizationCSVTemplate] FOREIGN KEY ([OrganizationCSVTemplateID]) REFERENCES [dbo].[OrganizationCSVTemplate] ([OrganizationCSVTemplateID])
GO
IF NOT EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OrganizationCSVTemplate_Organization]', 'F') AND parent_object_id = OBJECT_ID(N'[dbo].[OrganizationCSVTemplate]', 'U'))
ALTER TABLE [dbo].[OrganizationCSVTemplate] ADD CONSTRAINT [FK_OrganizationCSVTemplate_Organization] FOREIGN KEY ([OrganizationID]) REFERENCES [dbo].[Organization] ([OrganizationID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
