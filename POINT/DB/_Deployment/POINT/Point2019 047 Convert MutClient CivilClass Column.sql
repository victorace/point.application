UPDATE dbo.MutClient SET CivilClass = '0' WHERE CivilClass IN ('D')
GO
UPDATE dbo.MutClient SET CivilClass = '1' WHERE CivilClass IN ('M')
GO
UPDATE dbo.MutClient SET CivilClass = '2' WHERE CivilClass IN ('S')
GO
UPDATE dbo.MutClient SET CivilClass = '3' WHERE CivilClass IN ('P')
GO
UPDATE dbo.MutClient SET CivilClass = '4' WHERE CivilClass IN ('W')
GO
UPDATE dbo.MutClient SET CivilClass = NULL where CivilClass not in ('0', '1', '2', '3', '4') and civilclass is not null
GO
