PRINT 'Convert Client.HousingType naar ZIB type'
GO

DISABLE TRIGGER dbo.Client_InsertUpdate ON dbo.Client
GO

UPDATE dbo.Client SET HousingType = '0' WHERE HousingType = 'Eengezinswoning bovenwoning'
GO
UPDATE dbo.Client SET HousingType = '1' WHERE HousingType = 'Benedenwoning'
GO
UPDATE dbo.Client SET HousingType = '2' WHERE HousingType = 'Eengezinswoning'
GO
UPDATE dbo.Client SET HousingType = '3' WHERE HousingType in ('Portiekwoning', 'Woning met trap', 'Woning met lift')
GO
UPDATE dbo.Client SET HousingType = '4' WHERE HousingType = 'Pension/aanleunwoning'
GO
UPDATE dbo.Client SET HousingType = '7' WHERE HousingType IN ('Verzorgingstehuis', 'Verpleeghuis')
GO
UPDATE dbo.Client SET HousingType = '8' WHERE HousingType = 'Zwervend/dakloos'
GO
UPDATE dbo.Client SET HousingType = '9' WHERE HousingType = 'Anders'
GO

UPDATE dbo.Client SET HousingType = NULL WHERE HousingType NOT IN ('0','1','2','3','4','7','8','9') AND HousingType IS NOT NULL
GO

ENABLE TRIGGER dbo.Client_InsertUpdate ON dbo.Client
GO

UPDATE dbo.MutClient SET HousingType = '0' WHERE HousingType = 'Eengezinswoning bovenwoning'
GO
UPDATE dbo.MutClient SET HousingType = '1' WHERE HousingType = 'Benedenwoning'
GO
UPDATE dbo.MutClient SET HousingType = '2' WHERE HousingType = 'Eengezinswoning'
GO
UPDATE dbo.MutClient SET HousingType = '3' WHERE HousingType in ('Portiekwoning', 'Woning met trap', 'Woning met lift')
GO
UPDATE dbo.MutClient SET HousingType = '4' WHERE HousingType = 'Pension/aanleunwoning'
GO
UPDATE dbo.MutClient SET HousingType = '7' WHERE HousingType IN ('Verzorgingstehuis', 'Verpleeghuis')
GO
UPDATE dbo.MutClient SET HousingType = '8' WHERE HousingType = 'Zwervend/dakloos'
GO
UPDATE dbo.MutClient SET HousingType = '9' WHERE HousingType = 'Anders'
GO

UPDATE dbo.MutClient SET HousingType = NULL WHERE HousingType NOT IN ('0','1','2','3','4','7','8','9') AND HousingType IS NOT NULL
GO

UPDATE dbo.FlowFieldValue SET FlowFieldID = 1339 WHERE FlowFieldID = 551
GO
UPDATE dbo.FlowFieldValue SET Value = '0' WHERE FlowFieldID = 1339 AND Value = 'Eengezinswoning bovenwoning'
GO
UPDATE dbo.FlowFieldValue SET Value = '1' WHERE FlowFieldID = 1339 AND Value = 'Benedenwoning'
GO
UPDATE dbo.FlowFieldValue SET Value = '2' WHERE FlowFieldID = 1339 AND Value = 'Eengezinswoning'
GO
UPDATE dbo.FlowFieldValue SET Value = '3' WHERE FlowFieldID = 1339 AND Value in ('Portiekwoning', 'Woning met trap', 'Woning met lift')
GO
UPDATE dbo.FlowFieldValue SET Value = '4' WHERE FlowFieldID = 1339 AND Value = 'Pension/aanleunwoning'
GO
UPDATE dbo.FlowFieldValue SET Value = '7' WHERE FlowFieldID = 1339 AND Value IN ('Verzorgingstehuis', 'Verpleeghuis')
GO
UPDATE dbo.FlowFieldValue SET Value = '8' WHERE FlowFieldID = 1339 AND Value = 'Zwervend/dakloos'
GO
UPDATE dbo.FlowFieldValue SET Value = '9' WHERE FlowFieldID = 1339 AND Value = 'Anders'
GO
UPDATE dbo.FlowFieldValue SET Value = NULL WHERE FlowFieldID = 1339 AND Value NOT IN ('0','1','2','3','4','7','8','9') AND Value IS NOT NULL
GO

PRINT 'Done'