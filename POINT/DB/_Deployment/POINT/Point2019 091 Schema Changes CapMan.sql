/*
Run this script on a database with the schema represented by:

        LINKASSIST-NB20.PointDB    -  This database will be modified. The scripts folder will not be modified.

to synchronize it with:

        LINKASSIST-NB20.PointDB

You are recommended to back up your database before running this script

Script created by SQL Compare Engine version 12.3.3.2177 from Red Gate Software Ltd at 10/1/2019 5:58:55 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [dbo].[DepartmentCapacityPublic]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO

ALTER TABLE [dbo].[DepartmentCapacityPublic]  WITH CHECK ADD  CONSTRAINT [FK_DepartmentCapacityPublic_Location] FOREIGN KEY([LocationID])
REFERENCES [dbo].[Location] ([LocationID])
GO

ALTER TABLE [dbo].[DepartmentCapacityPublic] CHECK CONSTRAINT [FK_DepartmentCapacityPublic_Location]
GO

IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO



