/*

ADD TABLE [dbo].[DepartmentCapacityPublic]

*/

SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DepartmentCapacityPublic]'
GO
CREATE TABLE [dbo].[DepartmentCapacityPublic]
(
[DepartmentCapacityPublicID] [int] NOT NULL IDENTITY(1, 1),
[AfterCareTypeID] [int] NULL,
[AfterCareTypeName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AfterCareGroupID] [int] NULL,
[RegionID] [int] NOT NULL,
[RegionName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OrganizationID] [int] NOT NULL,
[OrganizationName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LocationID] [int] NOT NULL,
[LocationName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LocationStreet] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LocationNumber] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LocationPostalCode] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LocationCity] [varchar] (512) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LocationEmailAddress] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DepartmentID] [int] NOT NULL,
[DepartmentName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DepartmentPhoneNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DepartmentFaxNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DepartmentEmailAddress] [varchar] (1024) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsActive] [bit] NOT NULL CONSTRAINT [DF_DepartmentCapacityPublic_IsActive] DEFAULT ((0)),
[AdjustmentCapacityDate] [datetime] NULL,
[Capacity1] [int] NULL,
[Capacity2] [int] NULL,
[Capacity3] [int] NULL,
[Capacity4] [int] NULL,
[Information] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModifiedBy] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModifiedOn] [datetime] NULL,
[CapacityFunctionality] [bit] NULL,
[OrganizationTypeID] [int] NOT NULL CONSTRAINT [DF_DepartmentCapacityPublic_OrganizationTypeID] DEFAULT ((0))
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_DepartmentCapacityPublic] on [dbo].[DepartmentCapacityPublic]'
GO
ALTER TABLE [dbo].[DepartmentCapacityPublic] ADD CONSTRAINT [PK_DepartmentCapacityPublic] PRIMARY KEY CLUSTERED  ([DepartmentCapacityPublicID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO




/*

ADD COLUMN [AfterCareType].dbo.[Description]

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO

BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[AfterCareType]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO

ALTER TABLE [dbo].[AfterCareType] ADD
[Description] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO

IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO


/*

ADD COLUMN [Region].dbo.[CapacityBedsPublic]

*/

SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Region]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO

ALTER TABLE [dbo].[Region] ADD
[CapacityBedsPublic] [bit] NOT NULL CONSTRAINT [DF__Region__Capacity__76390C80] DEFAULT ((0))
GO

IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO


