/*
Run this script on:

pointreview.verzorgdeoverdracht.nl.TXX-DB-POINT-R    -  This database will be modified

to synchronize it with:

..Point

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 12.3.3.4490 from Red Gate Software Ltd at 29/04/2019 11:11:38

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION

PRINT(N'Update rows in [dbo].[ReportDefinition]')
UPDATE [dbo].[ReportDefinition] SET [ReportName]=N'KPI Snelheid in behandeling nemen' WHERE [ReportDefinitionID] = 27
UPDATE [dbo].[ReportDefinition] SET [ReportName]=N'KPI Snelheid reactie/antwoord' WHERE [ReportDefinitionID] = 28
PRINT(N'Operation applied to 2 rows out of 2')
COMMIT TRANSACTION
GO
