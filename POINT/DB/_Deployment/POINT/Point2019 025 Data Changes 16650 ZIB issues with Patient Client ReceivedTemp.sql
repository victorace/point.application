/*
Run this script on:

pointtest.verzorgdeoverdracht.nl.TXX-DB-POINT-R    -  This database will be modified

to synchronize it with:

NB1404.Point

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 12.3.3.4490 from Red Gate Software Ltd at 29-4-2019 15:43:24

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION

PRINT(N'Delete rows from [dbo].[FieldReceivedValueMapping]')
DELETE FROM [dbo].[FieldReceivedValueMapping] WHERE [FieldReceivedValueMappingID] = 217
DELETE FROM [dbo].[FieldReceivedValueMapping] WHERE [FieldReceivedValueMappingID] = 218
DELETE FROM [dbo].[FieldReceivedValueMapping] WHERE [FieldReceivedValueMappingID] = 219
DELETE FROM [dbo].[FieldReceivedValueMapping] WHERE [FieldReceivedValueMappingID] = 220
DELETE FROM [dbo].[FieldReceivedValueMapping] WHERE [FieldReceivedValueMappingID] = 221
DELETE FROM [dbo].[FieldReceivedValueMapping] WHERE [FieldReceivedValueMappingID] = 222
DELETE FROM [dbo].[FieldReceivedValueMapping] WHERE [FieldReceivedValueMappingID] = 223
DELETE FROM [dbo].[FieldReceivedValueMapping] WHERE [FieldReceivedValueMappingID] = 224
DELETE FROM [dbo].[FieldReceivedValueMapping] WHERE [FieldReceivedValueMappingID] = 225
DELETE FROM [dbo].[FieldReceivedValueMapping] WHERE [FieldReceivedValueMappingID] = 226
DELETE FROM [dbo].[FieldReceivedValueMapping] WHERE [FieldReceivedValueMappingID] = 227
DELETE FROM [dbo].[FieldReceivedValueMapping] WHERE [FieldReceivedValueMappingID] = 228
DELETE FROM [dbo].[FieldReceivedValueMapping] WHERE [FieldReceivedValueMappingID] = 229
DELETE FROM [dbo].[FieldReceivedValueMapping] WHERE [FieldReceivedValueMappingID] = 230
DELETE FROM [dbo].[FieldReceivedValueMapping] WHERE [FieldReceivedValueMappingID] = 231
DELETE FROM [dbo].[FieldReceivedValueMapping] WHERE [FieldReceivedValueMappingID] = 232
DELETE FROM [dbo].[FieldReceivedValueMapping] WHERE [FieldReceivedValueMappingID] = 233
DELETE FROM [dbo].[FieldReceivedValueMapping] WHERE [FieldReceivedValueMappingID] = 234
DELETE FROM [dbo].[FieldReceivedValueMapping] WHERE [FieldReceivedValueMappingID] = 235
DELETE FROM [dbo].[FieldReceivedValueMapping] WHERE [FieldReceivedValueMappingID] = 236
DELETE FROM [dbo].[FieldReceivedValueMapping] WHERE [FieldReceivedValueMappingID] = 237
DELETE FROM [dbo].[FieldReceivedValueMapping] WHERE [FieldReceivedValueMappingID] = 238
DELETE FROM [dbo].[FieldReceivedValueMapping] WHERE [FieldReceivedValueMappingID] = 239
DELETE FROM [dbo].[FieldReceivedValueMapping] WHERE [FieldReceivedValueMappingID] = 240
DELETE FROM [dbo].[FieldReceivedValueMapping] WHERE [FieldReceivedValueMappingID] = 241
DELETE FROM [dbo].[FieldReceivedValueMapping] WHERE [FieldReceivedValueMappingID] = 242
DELETE FROM [dbo].[FieldReceivedValueMapping] WHERE [FieldReceivedValueMappingID] = 243
DELETE FROM [dbo].[FieldReceivedValueMapping] WHERE [FieldReceivedValueMappingID] = 244
PRINT(N'Operation applied to 28 rows out of 28')

PRINT(N'Add rows to [dbo].[FieldReceivedValueMapping]')
SET IDENTITY_INSERT [dbo].[FieldReceivedValueMapping] ON
INSERT INTO [dbo].[FieldReceivedValueMapping] ([FieldReceivedValueMappingID], [FieldName], [SourceValue], [TargetValue]) VALUES (245, 'Gender', NULL, '0')
INSERT INTO [dbo].[FieldReceivedValueMapping] ([FieldReceivedValueMappingID], [FieldName], [SourceValue], [TargetValue]) VALUES (246, 'Gender', 'm', '1')
INSERT INTO [dbo].[FieldReceivedValueMapping] ([FieldReceivedValueMappingID], [FieldName], [SourceValue], [TargetValue]) VALUES (247, 'Gender', 'v', '2')
INSERT INTO [dbo].[FieldReceivedValueMapping] ([FieldReceivedValueMappingID], [FieldName], [SourceValue], [TargetValue]) VALUES (248, 'Gender', 'o', '0')
INSERT INTO [dbo].[FieldReceivedValueMapping] ([FieldReceivedValueMappingID], [FieldName], [SourceValue], [TargetValue]) VALUES (249, 'CivilClass', '1', '1')
INSERT INTO [dbo].[FieldReceivedValueMapping] ([FieldReceivedValueMappingID], [FieldName], [SourceValue], [TargetValue]) VALUES (250, 'CivilClass', '2', '2')
INSERT INTO [dbo].[FieldReceivedValueMapping] ([FieldReceivedValueMappingID], [FieldName], [SourceValue], [TargetValue]) VALUES (251, 'CivilClass', '3', '3')
INSERT INTO [dbo].[FieldReceivedValueMapping] ([FieldReceivedValueMappingID], [FieldName], [SourceValue], [TargetValue]) VALUES (252, 'CivilClass', '4', NULL)
INSERT INTO [dbo].[FieldReceivedValueMapping] ([FieldReceivedValueMappingID], [FieldName], [SourceValue], [TargetValue]) VALUES (253, 'CivilClass', '5', '0')
INSERT INTO [dbo].[FieldReceivedValueMapping] ([FieldReceivedValueMappingID], [FieldName], [SourceValue], [TargetValue]) VALUES (254, 'CivilClass', '6', '4')
INSERT INTO [dbo].[FieldReceivedValueMapping] ([FieldReceivedValueMappingID], [FieldName], [SourceValue], [TargetValue]) VALUES (255, 'CompositionHousekeeping', '1', '0')
INSERT INTO [dbo].[FieldReceivedValueMapping] ([FieldReceivedValueMappingID], [FieldName], [SourceValue], [TargetValue]) VALUES (256, 'CompositionHousekeeping', '2', '1')
INSERT INTO [dbo].[FieldReceivedValueMapping] ([FieldReceivedValueMappingID], [FieldName], [SourceValue], [TargetValue]) VALUES (257, 'CompositionHousekeeping', '3', '2')
INSERT INTO [dbo].[FieldReceivedValueMapping] ([FieldReceivedValueMappingID], [FieldName], [SourceValue], [TargetValue]) VALUES (258, 'CompositionHousekeeping', '4', '2')
INSERT INTO [dbo].[FieldReceivedValueMapping] ([FieldReceivedValueMappingID], [FieldName], [SourceValue], [TargetValue]) VALUES (259, 'CompositionHousekeeping', '5', '4')
INSERT INTO [dbo].[FieldReceivedValueMapping] ([FieldReceivedValueMappingID], [FieldName], [SourceValue], [TargetValue]) VALUES (260, 'CompositionHousekeeping', '6', '2')
INSERT INTO [dbo].[FieldReceivedValueMapping] ([FieldReceivedValueMappingID], [FieldName], [SourceValue], [TargetValue]) VALUES (261, 'CompositionHousekeeping', '7', '7')
INSERT INTO [dbo].[FieldReceivedValueMapping] ([FieldReceivedValueMappingID], [FieldName], [SourceValue], [TargetValue]) VALUES (262, 'CompositionHousekeeping', '8', '7')
INSERT INTO [dbo].[FieldReceivedValueMapping] ([FieldReceivedValueMappingID], [FieldName], [SourceValue], [TargetValue]) VALUES (263, 'CompositionHousekeeping', '9', '7')
INSERT INTO [dbo].[FieldReceivedValueMapping] ([FieldReceivedValueMappingID], [FieldName], [SourceValue], [TargetValue]) VALUES (264, 'HousingType', '1', '0')
INSERT INTO [dbo].[FieldReceivedValueMapping] ([FieldReceivedValueMappingID], [FieldName], [SourceValue], [TargetValue]) VALUES (265, 'HousingType', '2', '1')
INSERT INTO [dbo].[FieldReceivedValueMapping] ([FieldReceivedValueMappingID], [FieldName], [SourceValue], [TargetValue]) VALUES (266, 'HousingType', '3', '2')
INSERT INTO [dbo].[FieldReceivedValueMapping] ([FieldReceivedValueMappingID], [FieldName], [SourceValue], [TargetValue]) VALUES (267, 'HousingType', '4', '2')
INSERT INTO [dbo].[FieldReceivedValueMapping] ([FieldReceivedValueMappingID], [FieldName], [SourceValue], [TargetValue]) VALUES (268, 'HousingType', '5', '4')
INSERT INTO [dbo].[FieldReceivedValueMapping] ([FieldReceivedValueMappingID], [FieldName], [SourceValue], [TargetValue]) VALUES (269, 'HousingType', '6', '2')
INSERT INTO [dbo].[FieldReceivedValueMapping] ([FieldReceivedValueMappingID], [FieldName], [SourceValue], [TargetValue]) VALUES (270, 'HousingType', '7', '2')
INSERT INTO [dbo].[FieldReceivedValueMapping] ([FieldReceivedValueMappingID], [FieldName], [SourceValue], [TargetValue]) VALUES (271, 'HousingType', '8', '4')
INSERT INTO [dbo].[FieldReceivedValueMapping] ([FieldReceivedValueMappingID], [FieldName], [SourceValue], [TargetValue]) VALUES (272, 'HousingType', '9', '2')
INSERT INTO [dbo].[FieldReceivedValueMapping] ([FieldReceivedValueMappingID], [FieldName], [SourceValue], [TargetValue]) VALUES (273, 'HousingType', '10', '2')
SET IDENTITY_INSERT [dbo].[FieldReceivedValueMapping] OFF
PRINT(N'Operation applied to 29 rows out of 29')
COMMIT TRANSACTION
GO
