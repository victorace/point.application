DISABLE TRIGGER dbo.Client_InsertUpdate ON dbo.Client
GO

UPDATE dbo.Client SET CompositionHousekeeping = '0' WHERE CompositionHousekeeping IN ('Alleenstaand')
GO
UPDATE dbo.Client SET CompositionHousekeeping = '1' WHERE CompositionHousekeeping IN ('Uitsluitend met partner')
GO
UPDATE dbo.Client SET CompositionHousekeeping = '2' WHERE CompositionHousekeeping IN ('Met partner en kinderen', 'Zonder partner met kinderen', 'Heeft huishouden van volwassenen met 1 of meer kinderen')
GO
UPDATE dbo.Client SET CompositionHousekeeping = '4' WHERE CompositionHousekeeping IN ('Woont als kind samen met ouders in ouderlijk huis')
GO
UPDATE dbo.Client SET CompositionHousekeeping = '7' WHERE CompositionHousekeeping IN ('Heeft een ander meerpersoonshuishouden', 'Woont in zorginstelling met verblijf', 'Anders')
GO
UPDATE dbo.Client SET CompositionHousekeeping = '7' where CompositionHousekeeping not in ('0', '1', '2', '4', '7', '') and CompositionHousekeeping is not null
GO
UPDATE dbo.Client SET CompositionHousekeeping = NULL where CompositionHousekeeping = ''
GO

ENABLE TRIGGER dbo.Client_InsertUpdate ON dbo.Client
GO

UPDATE dbo.FlowFieldValue SET Value = '0' WHERE FlowFieldID = 1324 and Value = 'Alleenstaand'
GO
UPDATE dbo.FlowFieldValue SET Value = '1' WHERE FlowFieldID = 1324 and Value = 'Uitsluitend met partner'
GO
UPDATE dbo.FlowFieldValue SET Value = '2' WHERE FlowFieldID = 1324 and Value IN ('Met partner en kinderen', 'Zonder partner met kinderen', 'Heeft huishouden van volwassenen met 1 of meer kinderen')
GO
UPDATE dbo.FlowFieldValue SET Value = '4' WHERE FlowFieldID = 1324 and Value = 'Woont als kind samen met ouders in ouderlijk huis'
GO
UPDATE dbo.FlowFieldValue SET Value = '7' WHERE FlowFieldID = 1324 and Value IN ('Heeft een ander meerpersoonshuishouden', 'Woont in zorginstelling met verblijf', 'Anders')
GO
UPDATE dbo.FlowFieldValue SET Value = '7' WHERE FlowFieldID = 1324 and Value not in ('0', '1', '2', '4', '7', '') and Value is not null
GO
UPDATE dbo.FlowFieldValue SET Value = NULL where FlowFieldID = 1324 AND Value = ''
GO