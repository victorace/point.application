/*
Run this script on:

pointreview.verzorgdeoverdracht.nl.TXX-DB-POINT-R    -  This database will be modified

to synchronize it with:

..PointDev

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 12.3.3.4490 from Red Gate Software Ltd at 19/06/2019 14:31:18

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION

PRINT(N'Update row in [dbo].[ReportDefinition]')
UPDATE [dbo].[ReportDefinition] SET [Description]=N'Dit rapport geeft een overzicht van de capaciteit van uw afdeling/locatie

<b>Afdeling overzicht</b>
Toont absolute waardes die ingegeven zijn per locatie en per afdeling/zorgtype.

<b>Capaciteit per week</b>
Toont gemiddelde minimale / maximale  capaciteit van de dagen van de  week.

<b>Capaciteit per categorie / maand</b>
Toont gemiddelde minimale / maximale capaciteit gegroepeerd per zorgcategorie (Intramuraal/Thuiszorg/Hospice).' WHERE [ReportDefinitionID] = 36
COMMIT TRANSACTION
GO
