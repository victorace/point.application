/*
Run this script on a database with the same schema as:

LINKASSIST-NB18.PointDev – the database with this schema will be modified

to synchronize its data with:

LINKASSIST-NB18.PointDev – this database will not be modified

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 12.4.12.5042 from Red Gate Software Ltd at 7/9/2019 9:22:16 AM

*/
			
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION

PRINT(N'Add row to [dbo].[RazorTemplate]')
SET IDENTITY_INSERT [dbo].[RazorTemplate] ON
INSERT INTO [dbo].[RazorTemplate] ([RazorTemplateID], [TemplateName], [TemplateCode], [Contents], [CommunicationLogType]) VALUES (1071, N'POINT signaleringen: Laatste {0},  Open aantal {1}', N'AutoSignaleringEmail', N'@model Point.Database.Models.ViewModels.EmailSignaleringReminder

<style type="text/css">
.headercolor{
	background-color:#00396A;
	color:white;
	font-family:sans-serif;
	font-size:14px;
}
.tablestyle{
	border-spacing : 15px 3px;
}

.rowcolor{
	background-color:#E5E5E5;
	color:black;
	font-family:sans-serif;
	font-size:14px;
}
</style>
Geachte heer/mevrouw,<br />
U heeft één of meer openstaande POINT signaleringen.<br />
Het betreft:<br />
<br />
 <table class="tablestyle">
        <thead class="headercolor" >
            <tr>
                <th>Datum/ Tijd</th>
                <th>Door</th>
                <th>Soort/ Commentaar</th>
            </tr>
        </thead>
        @if (Model.SenderSignalering.Any())
        {
            <tbody>
                @foreach (var item in Model.SenderSignalering)
                {
                    <tr class="rowcolor">
                        <td >@item.SignaleringDateTime</td>
                        <td >@item.OrganizationName</td>
                        <td >@item.SignaleringTypeComment</td>                    
                    </tr>
                }
            </tbody>
        }
    </table>
	<br />
	Klik op onderstaande link om naar POINT te gaan om deze signaleringen te kunnen opvolgen/afhandelen <br />
	<br />
	@Model.Url <br/>

	<br />
	<br />
	Deze email is automatisch verstuurd door POINT ', 22)
SET IDENTITY_INSERT [dbo].[RazorTemplate] OFF
COMMIT TRANSACTION
GO
