/*
Run this script on:

        pointtest.verzorgdeoverdracht.nl.TXX-DB-POINT-R    -  This database will be modified

to synchronize it with:

        ..PointDev

You are recommended to back up your database before running this script

Script created by SQL Compare version 12.3.3.4490 from Red Gate Software Ltd at 25/09/2019 15:15:00

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[AidProductOrderItem]'
GO
IF EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AidProductOrderItem_AidProduct]', 'F') AND parent_object_id = OBJECT_ID(N'[dbo].[AidProductOrderItem]', 'U'))
ALTER TABLE [dbo].[AidProductOrderItem] DROP CONSTRAINT [FK_AidProductOrderItem_AidProduct]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[AidProduct]'
GO
IF EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AidProduct_AidProductGroup]', 'F') AND parent_object_id = OBJECT_ID(N'[dbo].[AidProduct]', 'U'))
ALTER TABLE [dbo].[AidProduct] DROP CONSTRAINT [FK_AidProduct_AidProductGroup]
GO
IF EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AidProduct_Supplier]', 'F') AND parent_object_id = OBJECT_ID(N'[dbo].[AidProduct]', 'U'))
ALTER TABLE [dbo].[AidProduct] DROP CONSTRAINT [FK_AidProduct_Supplier]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[AidProductOrderItem]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AidProductOrderItem_AidProduct]', 'F') AND parent_object_id = OBJECT_ID(N'[dbo].[AidProductOrderItem]', 'U'))
ALTER TABLE [dbo].[AidProductOrderItem] ADD CONSTRAINT [FK_AidProductOrderItem_AidProduct] FOREIGN KEY ([AidProductID]) REFERENCES [dbo].[AidProduct] ([AidProductID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[AidProduct]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AidProduct_AidProductGroup]', 'F') AND parent_object_id = OBJECT_ID(N'[dbo].[AidProduct]', 'U'))
ALTER TABLE [dbo].[AidProduct] ADD CONSTRAINT [FK_AidProduct_AidProductGroup] FOREIGN KEY ([AidProductGroupID]) REFERENCES [dbo].[AidProductGroup] ([AidProductGroupID])
GO
IF NOT EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AidProduct_Supplier]', 'F') AND parent_object_id = OBJECT_ID(N'[dbo].[AidProduct]', 'U'))
ALTER TABLE [dbo].[AidProduct] ADD CONSTRAINT [FK_AidProduct_Supplier] FOREIGN KEY ([SupplierID]) REFERENCES [dbo].[Supplier] ([SupplierID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
