/*
Run this script on:

        pointtest.verzorgdeoverdracht.nl.TXX-DB-POINT-R    -  This database will be modified

to synchronize it with:

        ..PointDev

You are recommended to back up your database before running this script

Script created by SQL Compare version 12.3.3.4490 from Red Gate Software Ltd at 22/05/2019 10:47:26

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Client]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
IF COL_LENGTH(N'[dbo].[Client]', N'HousingTypeComment') IS NULL
ALTER TABLE [dbo].[Client] ADD[HousingTypeComment] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
IF COL_LENGTH(N'[dbo].[Client]', N'HousingTypeComment') IS NULL
ALTER TABLE [dbo].[Client] ADD
[HousingTypeComment] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[MutClient]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
IF COL_LENGTH(N'[dbo].[MutClient]', N'HousingTypeComment') IS NULL
ALTER TABLE [dbo].[MutClient] ADD[HousingTypeComment] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
IF COL_LENGTH(N'[dbo].[MutClient]', N'HousingTypeComment') IS NULL
ALTER TABLE [dbo].[MutClient] ADD
[HousingTypeComment] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering trigger [dbo].[Client_InsertUpdate] on [dbo].[Client]'
GO

ALTER TRIGGER [dbo].[Client_InsertUpdate] 
   ON  [dbo].[Client]  
   AFTER UPDATE, INSERT 
AS  
BEGIN 
 
	SET NOCOUNT ON; 
 
	IF (SELECT COUNT(*) FROM Inserted) = 0 
	BEGIN 
		RETURN 
	END 
 
	IF (SELECT Enabled FROM Logging) = 0 
	BEGIN 
		RETURN 
	END 
 
	-- BIJWERKEN LOGGING 
	DECLARE	@LogClientID	INT, 
	@ClientID				INT, 
	@TimeStamp				DATETIME, 
	@EmployeeID				INT, 
	@Action					VARCHAR(1), 
	@ScreenID				INT, 
	@Version				INT 
 
	SET NOCOUNT ON; 
	 
	-- Check of timestamp wel een update krijgt. Zoniet dan is er sprake 
	-- van een oude SP die de update uitvoerd. 
	IF UPDATE([TimeStamp]) 
	BEGIN 
		SELECT 
			@TimeStamp  = [TimeStamp], 
			@EmployeeID	= [EmployeeID], 
			@ScreenID	= [ScreenID] 
		FROM Inserted 
	END 
 
	SELECT @ClientID = ClientID 
	FROM Inserted 
 
	IF exists (select * from deleted) 
		SELECT @Action = 'U' 
	ELSE 
		SELECT @Action = 'I' 
	 
 
	 
	SELECT @Version = -1 --(SELECT ISNULL(MAX(Version), 0) + 1 FROM LogClient where LogClient.ClientID = @ClientID) 
 
	INSERT INTO LogClient 
			   (ClientID, 
			    [TimeStamp], 
				EmployeeID, 
				[Action], 
				ScreenID, 
				[Version]) 
		 VALUES 
			(@ClientID, 
			 @TimeStamp, 
			 @EmployeeID, 
			 @Action, 
			 @ScreenID, 
			 @Version) 
 
	SET @LogClientID = SCOPE_IDENTITY(); 
 
	DECLARE @MutClientID INT 
	 
	INSERT INTO MutClient( 
		LogClientID,	 
		ClientID, 
		CivilServiceNumber, 
		PatientNumber, 
		Gender, 
		Salutation, 
		Initials, 
		FirstName, 
		MiddleName, 
		LastName, 
		MaidenName, 
		BirthDate, 
		HealthInsuranceCompanyID, 
		InsuranceNumber, 
		StreetName, 
		Number, 
		PostalCode, 
		City, 
		Country, 
		PhoneNumberGeneral, 
		PhoneNumberMobile, 
		PhoneNumberWork, 
		Email, 
		ContactPersonName, 
		ContactPersonRelationType, 
		ContactPersonBirthDate, 
		ContactPersonPhoneNumberGeneral, 
		ContactPersonPhoneNumberMobile, 
		ContactPersonPhoneWork, 
		GeneralPractitionerName, 
		GeneralPractitionerPhoneNumber, 
		PharmacyName, 
		PharmacyPhoneNumber, 
		CivilClass, 
		CompositionHousekeeping, 
		ChildrenInHousekeeping, 
		HousingType, 
		HousingTypeComment, 
		HealthCareProvider, 
		ClientCreatedBy, 
		ClientCreatedDate, 
		ClientModifiedBy, 
		ClientModifiedDate,		 
		NationalityID, 
		LanguageID, 
		TimeStamp, 
		EmployeeID, 
		ScreenID, 
		ContactPersonEmail, 
		ContactPersonName2, 
		ContactPersonRelationType2, 
		ContactPersonBirthDate2, 
		ContactPersonPhoneNumberGeneral2, 
		ContactPersonPhoneNumberMobile2, 
		ContactPersonPhoneNumberWork2, 
		ContactPersonEmail2, 
		ContactPersonName3, 
		ContactPersonRelationType3, 
		ContactPersonBirthDate3, 
		ContactPersonPhoneNumberGeneral3, 
		ContactPersonPhoneNumberMobile3, 
		ContactPersonPhoneNumberWork3, 
		ContactPersonEmail3, 
		UsesPatientPortal, 
		OriginalClientID, 
		AddressGPForZorgmail, 
		TemporaryStreetName, 
		TemporaryNumber, 
		TemporaryPostalCode, 
		TemporaryCity, 
		TemporaryCountry, 
		PartnerName, 
		PartnerMiddleName, 
		HuisnummerToevoeging, 
		Postbus, 
        VisitNumber,
		ClientIsAnonymous,
		HasNoCivilServiceNumber
		) 
	SELECT 
		@LogClientID,
		ClientID, 
		CivilServiceNumber, 
		PatientNumber, 
		Gender, 
		Salutation, 
		Initials, 
		FirstName, 
		MiddleName, 
		LastName, 
		MaidenName, 
		BirthDate, 
		HealthInsuranceCompanyID, 
		InsuranceNumber, 
		StreetName, 
		Number, 
		PostalCode, 
		City, 
		Country, 
		PhoneNumberGeneral, 
		PhoneNumberMobile, 
		PhoneNumberWork, 
		Email, 
		ContactPersonName, 
		ContactPersonRelationType, 
		ContactPersonBirthDate, 
		ContactPersonPhoneNumberGeneral, 
		ContactPersonPhoneNumberMobile, 
		ContactPersonPhoneWork, 
		GeneralPractitionerName, 
		GeneralPractitionerPhoneNumber, 
		PharmacyName, 
		PharmacyPhoneNumber, 
		CivilClass, 
		CompositionHousekeeping, 
		ChildrenInHousekeeping, 
		HousingType, 
		HousingTypeComment, 
		HealthCareProvider, 
		ClientCreatedBy, 
		ClientCreatedDate, 
		ClientModifiedBy, 
		ClientModifiedDate,		 
		NationalityID, 
		LanguageID, 
		TimeStamp, 
		EmployeeID, 
		ScreenID, 
		ContactPersonEmail, 
		ContactPersonName2, 
		ContactPersonRelationType2, 
		ContactPersonBirthDate2, 
		ContactPersonPhoneNumberGeneral2, 
		ContactPersonPhoneNumberMobile2, 
		ContactPersonPhoneNumberWork2, 
		ContactPersonEmail2, 
		ContactPersonName3, 
		ContactPersonRelationType3, 
		ContactPersonBirthDate3, 
		ContactPersonPhoneNumberGeneral3, 
		ContactPersonPhoneNumberMobile3, 
		ContactPersonPhoneNumberWork3, 
		ContactPersonEmail3, 
		UsesPatientPortal, 
		OriginalClientID, 
		AddressGPForZorgmail, 
		TemporaryStreetName, 
		TemporaryNumber, 
		TemporaryPostalCode, 
		TemporaryCity, 
		TemporaryCountry, 
		PartnerName, 
		PartnerMiddleName, 
		HuisnummerToevoeging, 
		Postbus, 
        VisitNumber,
		ClientIsAnonymous,
		HasNoCivilServiceNumber
	FROM Inserted 
 
	SET @MutClientID = SCOPE_IDENTITY(); 
 
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
