PRINT 'Omzetten flowfields CivilClass naar ZIB'

UPDATE dbo.FlowFieldValue SET Value = '0' WHERE FlowFieldID = 415 AND Value = 'D'
GO
UPDATE dbo.FlowFieldValue SET Value = '1' WHERE FlowFieldID = 415 AND Value = 'M'
GO
UPDATE dbo.FlowFieldValue SET Value = '2' WHERE FlowFieldID = 415 AND Value = 'S'
GO
UPDATE dbo.FlowFieldValue SET Value = '3' WHERE FlowFieldID = 415 AND Value = 'P'
GO
UPDATE dbo.FlowFieldValue SET Value = '4' WHERE FlowFieldID = 415 AND Value = 'W'
GO
UPDATE dbo.FlowFieldValue SET Value = NULL WHERE FlowFieldID = 415 AND Value not in ('0', '1', '2', '3', '4') and Value is not null
GO

UPDATE dbo.MutFlowFieldValue SET Value = '0' WHERE FlowFieldID = 415 AND Value = 'D'
GO
UPDATE dbo.MutFlowFieldValue SET Value = '1' WHERE FlowFieldID = 415 AND Value = 'M'
GO
UPDATE dbo.MutFlowFieldValue SET Value = '2' WHERE FlowFieldID = 415 AND Value = 'S'
GO
UPDATE dbo.MutFlowFieldValue SET Value = '3' WHERE FlowFieldID = 415 AND Value = 'P'
GO
UPDATE dbo.MutFlowFieldValue SET Value = '4' WHERE FlowFieldID = 415 AND Value = 'W'
GO
UPDATE dbo.MutFlowFieldValue SET Value = NULL WHERE FlowFieldID = 415 AND Value not in ('0', '1', '2', '3', '4') and Value is not null
GO

PRINT 'Done'