/*
Run this script on:

pointreview.verzorgdeoverdracht.nl.TXX-DB-POINT-R    -  This database will be modified

to synchronize it with:

..Point

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 12.3.3.4490 from Red Gate Software Ltd at 11/04/2019 15:29:59

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION

PRINT(N'Drop constraints from [dbo].[PhaseDefinitionNavigation]')
ALTER TABLE [dbo].[PhaseDefinitionNavigation] NOCHECK CONSTRAINT [FK_PhaseDefinitionNavigation_PhaseDefinition]
ALTER TABLE [dbo].[PhaseDefinitionNavigation] NOCHECK CONSTRAINT [FK_PhaseDefinitionNavigation_TargetPhaseDefinition]

PRINT(N'Drop constraints from [dbo].[GeneralActionPhase]')
ALTER TABLE [dbo].[GeneralActionPhase] NOCHECK CONSTRAINT [FK_dbo.GeneralActionPhase_dbo.GeneralAction_GeneralActionID]
ALTER TABLE [dbo].[GeneralActionPhase] NOCHECK CONSTRAINT [FK_dbo.GeneralActionPhase_dbo.PhaseDefinition_PhaseDefinitionID]
ALTER TABLE [dbo].[GeneralActionPhase] NOCHECK CONSTRAINT [FK_GeneralActionPhase_OrganizationType]

PRINT(N'Drop constraints from [dbo].[PhaseDefinition]')
ALTER TABLE [dbo].[PhaseDefinition] NOCHECK CONSTRAINT [FK_dbo.PhaseDefinition_dbo.FlowDefinition_FlowDefinitionID]
ALTER TABLE [dbo].[PhaseDefinition] NOCHECK CONSTRAINT [FK_dbo.PhaseDefinition_dbo.OrganizationType_OrganizationTypeID]

PRINT(N'Drop constraint FK_dbo.FlowPhaseAttribute_dbo.PhaseDefinition_PhaseDefinitionID from [dbo].[FlowPhaseAttribute]')
ALTER TABLE [dbo].[FlowPhaseAttribute] NOCHECK CONSTRAINT [FK_dbo.FlowPhaseAttribute_dbo.PhaseDefinition_PhaseDefinitionID]

PRINT(N'Drop constraint FK_PhaseDefinitionRights_PhaseDefinition from [dbo].[PhaseDefinitionRights]')
ALTER TABLE [dbo].[PhaseDefinitionRights] NOCHECK CONSTRAINT [FK_PhaseDefinitionRights_PhaseDefinition]

PRINT(N'Drop constraints from [dbo].[GeneralAction]')
ALTER TABLE [dbo].[GeneralAction] NOCHECK CONSTRAINT [FK_dbo.GeneralAction_dbo.GeneralActionType_GeneralActionTypeID]

PRINT(N'Delete rows from [dbo].[PhaseDefinitionNavigation]')
DELETE FROM [dbo].[PhaseDefinitionNavigation] WHERE [PhaseDefinitionID] = 71 AND [NextPhaseDefinitionID] = 71 AND [ActionTypeID] = 3
DELETE FROM [dbo].[PhaseDefinitionNavigation] WHERE [PhaseDefinitionID] = 80 AND [NextPhaseDefinitionID] = 80 AND [ActionTypeID] = 3
DELETE FROM [dbo].[PhaseDefinitionNavigation] WHERE [PhaseDefinitionID] = 83 AND [NextPhaseDefinitionID] = 83 AND [ActionTypeID] = 3
DELETE FROM [dbo].[PhaseDefinitionNavigation] WHERE [PhaseDefinitionID] = 84 AND [NextPhaseDefinitionID] = 84 AND [ActionTypeID] = 3
DELETE FROM [dbo].[PhaseDefinitionNavigation] WHERE [PhaseDefinitionID] = 107 AND [NextPhaseDefinitionID] = 107 AND [ActionTypeID] = 3
PRINT(N'Operation applied to 5 rows out of 5')

PRINT(N'Delete rows from [dbo].[GeneralActionPhase]')
DELETE FROM [dbo].[GeneralActionPhase] WHERE [GeneralActionPhaseID] = 290
DELETE FROM [dbo].[GeneralActionPhase] WHERE [GeneralActionPhaseID] = 291
DELETE FROM [dbo].[GeneralActionPhase] WHERE [GeneralActionPhaseID] = 292
DELETE FROM [dbo].[GeneralActionPhase] WHERE [GeneralActionPhaseID] = 293
DELETE FROM [dbo].[GeneralActionPhase] WHERE [GeneralActionPhaseID] = 294
PRINT(N'Operation applied to 5 rows out of 5')

PRINT(N'Update rows in [dbo].[PhaseDefinition]')
UPDATE [dbo].[PhaseDefinition] SET [PhaseName]=N'Doorlopend dossier', [Subcategory]=NULL WHERE [PhaseDefinitionID] = 71
UPDATE [dbo].[PhaseDefinition] SET [PhaseName]=N'Doorlopend dossier', [Subcategory]=NULL WHERE [PhaseDefinitionID] = 80
UPDATE [dbo].[PhaseDefinition] SET [PhaseName]=N'Doorlopend dossier', [Subcategory]=NULL WHERE [PhaseDefinitionID] = 83
UPDATE [dbo].[PhaseDefinition] SET [PhaseName]=N'Doorlopend dossier', [Subcategory]=NULL WHERE [PhaseDefinitionID] = 84
UPDATE [dbo].[PhaseDefinition] SET [PhaseName]=N'Doorlopend dossier', [Subcategory]=NULL WHERE [PhaseDefinitionID] = 107
PRINT(N'Operation applied to 5 rows out of 5')

PRINT(N'Add rows to [dbo].[GeneralAction]')
SET IDENTITY_INSERT [dbo].[GeneralAction] ON
INSERT INTO [dbo].[GeneralAction] ([GeneralActionID], [GeneralActionTypeID], [MethodName], [Name], [Description], [Rights], [ClassNameMenuItem], [ClassNameGlyphicon], [OrderNumber], [Subcategory], [OnFormSetVersionOnly]) VALUES (37, 3, N'FormType/DeleteDoorlopendDossier', N'Verwijderen', N'Verwijderen van het doorlopend dossier', N'X', NULL, N'glyphicon glyphicon-remove', 12, N'Meer acties', 1)
INSERT INTO [dbo].[GeneralAction] ([GeneralActionID], [GeneralActionTypeID], [MethodName], [Name], [Description], [Rights], [ClassNameMenuItem], [ClassNameGlyphicon], [OrderNumber], [Subcategory], [OnFormSetVersionOnly]) VALUES (38, 3, N'FormType/CloseDoorlopendDossier', N'Afsluiten', N'Doorlopend dossier afsluiten', N'X', NULL, N'glyphicon glyphicon-remove', 13, N'Meer acties', 1)
SET IDENTITY_INSERT [dbo].[GeneralAction] OFF
PRINT(N'Operation applied to 2 rows out of 2')

PRINT(N'Add rows to [dbo].[GeneralActionPhase]')
SET IDENTITY_INSERT [dbo].[GeneralActionPhase] ON
INSERT INTO [dbo].[GeneralActionPhase] ([GeneralActionPhaseID], [PhaseDefinitionID], [GeneralActionID], [PhaseStatusBegin], [PhaseStatusEnd], [OrganizationTypeID]) VALUES (295, 71, 37, 1, NULL, NULL)
INSERT INTO [dbo].[GeneralActionPhase] ([GeneralActionPhaseID], [PhaseDefinitionID], [GeneralActionID], [PhaseStatusBegin], [PhaseStatusEnd], [OrganizationTypeID]) VALUES (296, 80, 37, 1, NULL, NULL)
INSERT INTO [dbo].[GeneralActionPhase] ([GeneralActionPhaseID], [PhaseDefinitionID], [GeneralActionID], [PhaseStatusBegin], [PhaseStatusEnd], [OrganizationTypeID]) VALUES (297, 83, 37, 1, NULL, NULL)
INSERT INTO [dbo].[GeneralActionPhase] ([GeneralActionPhaseID], [PhaseDefinitionID], [GeneralActionID], [PhaseStatusBegin], [PhaseStatusEnd], [OrganizationTypeID]) VALUES (298, 84, 37, 1, NULL, NULL)
INSERT INTO [dbo].[GeneralActionPhase] ([GeneralActionPhaseID], [PhaseDefinitionID], [GeneralActionID], [PhaseStatusBegin], [PhaseStatusEnd], [OrganizationTypeID]) VALUES (299, 107, 37, 1, NULL, NULL)
INSERT INTO [dbo].[GeneralActionPhase] ([GeneralActionPhaseID], [PhaseDefinitionID], [GeneralActionID], [PhaseStatusBegin], [PhaseStatusEnd], [OrganizationTypeID]) VALUES (300, 71, 38, 1, 1, NULL)
INSERT INTO [dbo].[GeneralActionPhase] ([GeneralActionPhaseID], [PhaseDefinitionID], [GeneralActionID], [PhaseStatusBegin], [PhaseStatusEnd], [OrganizationTypeID]) VALUES (301, 80, 38, 1, 1, NULL)
INSERT INTO [dbo].[GeneralActionPhase] ([GeneralActionPhaseID], [PhaseDefinitionID], [GeneralActionID], [PhaseStatusBegin], [PhaseStatusEnd], [OrganizationTypeID]) VALUES (302, 83, 38, 1, 1, NULL)
INSERT INTO [dbo].[GeneralActionPhase] ([GeneralActionPhaseID], [PhaseDefinitionID], [GeneralActionID], [PhaseStatusBegin], [PhaseStatusEnd], [OrganizationTypeID]) VALUES (303, 84, 38, 1, 1, NULL)
INSERT INTO [dbo].[GeneralActionPhase] ([GeneralActionPhaseID], [PhaseDefinitionID], [GeneralActionID], [PhaseStatusBegin], [PhaseStatusEnd], [OrganizationTypeID]) VALUES (304, 107, 38, 1, 1, NULL)
SET IDENTITY_INSERT [dbo].[GeneralActionPhase] OFF
PRINT(N'Operation applied to 10 rows out of 10')

PRINT(N'Add constraints to [dbo].[PhaseDefinitionNavigation]')
ALTER TABLE [dbo].[PhaseDefinitionNavigation] WITH CHECK CHECK CONSTRAINT [FK_PhaseDefinitionNavigation_PhaseDefinition]
ALTER TABLE [dbo].[PhaseDefinitionNavigation] WITH CHECK CHECK CONSTRAINT [FK_PhaseDefinitionNavigation_TargetPhaseDefinition]

PRINT(N'Add constraints to [dbo].[GeneralActionPhase]')
ALTER TABLE [dbo].[GeneralActionPhase] WITH CHECK CHECK CONSTRAINT [FK_dbo.GeneralActionPhase_dbo.GeneralAction_GeneralActionID]
ALTER TABLE [dbo].[GeneralActionPhase] WITH CHECK CHECK CONSTRAINT [FK_dbo.GeneralActionPhase_dbo.PhaseDefinition_PhaseDefinitionID]
ALTER TABLE [dbo].[GeneralActionPhase] WITH CHECK CHECK CONSTRAINT [FK_GeneralActionPhase_OrganizationType]

PRINT(N'Add constraints to [dbo].[PhaseDefinition]')
ALTER TABLE [dbo].[PhaseDefinition] WITH CHECK CHECK CONSTRAINT [FK_dbo.PhaseDefinition_dbo.FlowDefinition_FlowDefinitionID]
ALTER TABLE [dbo].[PhaseDefinition] WITH CHECK CHECK CONSTRAINT [FK_dbo.PhaseDefinition_dbo.OrganizationType_OrganizationTypeID]
ALTER TABLE [dbo].[FlowPhaseAttribute] WITH CHECK CHECK CONSTRAINT [FK_dbo.FlowPhaseAttribute_dbo.PhaseDefinition_PhaseDefinitionID]
ALTER TABLE [dbo].[PhaseDefinitionRights] WITH CHECK CHECK CONSTRAINT [FK_PhaseDefinitionRights_PhaseDefinition]

PRINT(N'Add constraints to [dbo].[GeneralAction]')
ALTER TABLE [dbo].[GeneralAction] WITH CHECK CHECK CONSTRAINT [FK_dbo.GeneralAction_dbo.GeneralActionType_GeneralActionTypeID]
COMMIT TRANSACTION
GO
