/*
Run this script on:

pointreview.verzorgdeoverdracht.nl.TXX-DB-POINT-R    -  This database will be modified

to synchronize it with:

..Point

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 12.3.3.4490 from Red Gate Software Ltd at 01/05/2019 15:51:10

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION

PRINT(N'Drop constraints from [dbo].[FlowFieldAttribute]')
ALTER TABLE [dbo].[FlowFieldAttribute] NOCHECK CONSTRAINT [FK_dbo.FlowFieldAttribute_dbo.FlowField_FlowFieldID]
ALTER TABLE [dbo].[FlowFieldAttribute] NOCHECK CONSTRAINT [FK_dbo.FlowFieldAttribute_dbo.FormType_FormTypeID]
ALTER TABLE [dbo].[FlowFieldAttribute] NOCHECK CONSTRAINT [FK_FlowFieldAttribute_FlowField]

PRINT(N'Drop constraints from [dbo].[FlowField]')
ALTER TABLE [dbo].[FlowField] NOCHECK CONSTRAINT [FK_FlowField_FlowFieldDataSource]

PRINT(N'Drop constraints from [dbo].[AidProduct]')
ALTER TABLE [dbo].[AidProduct] NOCHECK CONSTRAINT [FK_AidProduct_AidProductGroup]
ALTER TABLE [dbo].[AidProduct] NOCHECK CONSTRAINT [FK_AidProduct_Supplier]

PRINT(N'Update row in [dbo].[FlowField]')
UPDATE [dbo].[FlowField] SET [Description]=N'Met de POINT-lijst worden de hulpmiddelen vastgelegd die benodigd zijn, de bestelling vindt separaat plaats.<br/>
Met de MediPoint/Vegro lijst kunnen artikelen in POINT worden ingegeven waarna met de [Bestel] knop de artikelen direct bij MediPoint/Vegro besteld worden.<br/>
<br/>
Daar is dus geen extra handeling meer voor benodigd. <br/>
De bevestiging van de order verschijnt ook terug in POINT. <br/>
<br/>
LET OP: De artikelen die niet in de MediPoint/Vegro lijst kunnen worden geselecteerd, dienen nog op een andere wijze (telefonisch) te worden besteld.<br/>
Deze kunnen in POINT worden vastgelegd door een extra hulpmiddelen formulier aan te maken en de standaard lijst toe te passen.' WHERE [FlowFieldID] = 2506

PRINT(N'Update rows in [dbo].[HealthInsurer]')
UPDATE [dbo].[HealthInsurer] SET [MediPointCode]='8958' WHERE [HealthInsurerID] = 6
UPDATE [dbo].[HealthInsurer] SET [MediPointCode]='8956' WHERE [HealthInsurerID] = 10
UPDATE [dbo].[HealthInsurer] SET [MediPointCode]='9018' WHERE [HealthInsurerID] = 24
UPDATE [dbo].[HealthInsurer] SET [MediPointCode]='7084' WHERE [HealthInsurerID] = 25
UPDATE [dbo].[HealthInsurer] SET [MediPointCode]='7029' WHERE [HealthInsurerID] = 27
UPDATE [dbo].[HealthInsurer] SET [MediPointCode]='8971' WHERE [HealthInsurerID] = 37
UPDATE [dbo].[HealthInsurer] SET [MediPointCode]='8972' WHERE [HealthInsurerID] = 39
UPDATE [dbo].[HealthInsurer] SET [MediPointCode]='3313' WHERE [HealthInsurerID] = 40
UPDATE [dbo].[HealthInsurer] SET [MediPointCode]='9015' WHERE [HealthInsurerID] = 50
UPDATE [dbo].[HealthInsurer] SET [MediPointCode]='8960' WHERE [HealthInsurerID] = 57
UPDATE [dbo].[HealthInsurer] SET [MediPointCode]='8965' WHERE [HealthInsurerID] = 62
UPDATE [dbo].[HealthInsurer] SET [MediPointCode]='0736' WHERE [HealthInsurerID] = 63
UPDATE [dbo].[HealthInsurer] SET [MediPointCode]='3314' WHERE [HealthInsurerID] = 78
UPDATE [dbo].[HealthInsurer] SET [MediPointCode]='7032' WHERE [HealthInsurerID] = 81
UPDATE [dbo].[HealthInsurer] SET [MediPointCode]='0101' WHERE [HealthInsurerID] = 93
UPDATE [dbo].[HealthInsurer] SET [MediPointCode]='7095' WHERE [HealthInsurerID] = 94
UPDATE [dbo].[HealthInsurer] SET [MediPointCode]='7095' WHERE [HealthInsurerID] = 95
UPDATE [dbo].[HealthInsurer] SET [MediPointCode]='0212' WHERE [HealthInsurerID] = 103
UPDATE [dbo].[HealthInsurer] SET [MediPointCode]='3311' WHERE [HealthInsurerID] = 104
UPDATE [dbo].[HealthInsurer] SET [MediPointCode]='7085' WHERE [HealthInsurerID] = 105
UPDATE [dbo].[HealthInsurer] SET [MediPointCode]='7037' WHERE [HealthInsurerID] = 110
UPDATE [dbo].[HealthInsurer] SET [MediPointCode]='3328' WHERE [HealthInsurerID] = 147
UPDATE [dbo].[HealthInsurer] SET [MediPointCode]='9664' WHERE [HealthInsurerID] = 149
UPDATE [dbo].[HealthInsurer] SET [MediPointCode]='3329' WHERE [HealthInsurerID] = 150
UPDATE [dbo].[HealthInsurer] SET [MediPointCode]='3330' WHERE [HealthInsurerID] = 151
UPDATE [dbo].[HealthInsurer] SET [MediPointCode]='3332' WHERE [HealthInsurerID] = 153
UPDATE [dbo].[HealthInsurer] SET [MediPointCode]='3333' WHERE [HealthInsurerID] = 154
UPDATE [dbo].[HealthInsurer] SET [MediPointCode]='3334' WHERE [HealthInsurerID] = 155
UPDATE [dbo].[HealthInsurer] SET [MediPointCode]='3336' WHERE [HealthInsurerID] = 156
UPDATE [dbo].[HealthInsurer] SET [MediPointCode]='3339' WHERE [HealthInsurerID] = 289
UPDATE [dbo].[HealthInsurer] SET [MediPointCode]='3343' WHERE [HealthInsurerID] = 293
UPDATE [dbo].[HealthInsurer] SET [MediPointCode]='3344' WHERE [HealthInsurerID] = 294
UPDATE [dbo].[HealthInsurer] SET [MediPointCode]='8995' WHERE [HealthInsurerID] = 299
UPDATE [dbo].[HealthInsurer] SET [MediPointCode]='3353' WHERE [HealthInsurerID] = 302
UPDATE [dbo].[HealthInsurer] SET [MediPointCode]='3353' WHERE [HealthInsurerID] = 303
UPDATE [dbo].[HealthInsurer] SET [MediPointCode]='3351' WHERE [HealthInsurerID] = 305
UPDATE [dbo].[HealthInsurer] SET [MediPointCode]='3355' WHERE [HealthInsurerID] = 308
UPDATE [dbo].[HealthInsurer] SET [MediPointCode]='3358' WHERE [HealthInsurerID] = 311
UPDATE [dbo].[HealthInsurer] SET [MediPointCode]='3360' WHERE [HealthInsurerID] = 313
PRINT(N'Operation applied to 39 rows out of 39')

PRINT(N'Add rows to [dbo].[AidProductGroup]')
INSERT INTO [dbo].[AidProductGroup] ([AidProductGroupID], [Name], [Code]) VALUES (10, N'Gipssteunen', N'GPSTN')
INSERT INTO [dbo].[AidProductGroup] ([AidProductGroupID], [Name], [Code]) VALUES (11, N'Drempelhulpen', N'DRPLH')
INSERT INTO [dbo].[AidProductGroup] ([AidProductGroupID], [Name], [Code]) VALUES (12, N'Draaischijven', N'DRISC')
INSERT INTO [dbo].[AidProductGroup] ([AidProductGroupID], [Name], [Code]) VALUES (13, N'Sta-/loopbeugels', N'SLBGH')
INSERT INTO [dbo].[AidProductGroup] ([AidProductGroupID], [Name], [Code]) VALUES (14, N'Transferplanken', N'TRSFP')
INSERT INTO [dbo].[AidProductGroup] ([AidProductGroupID], [Name], [Code]) VALUES (15, N'Rol-/glijlakens', N'RGLKN')
INSERT INTO [dbo].[AidProductGroup] ([AidProductGroupID], [Name], [Code]) VALUES (16, N'Tilliften', N'TLLFT')
INSERT INTO [dbo].[AidProductGroup] ([AidProductGroupID], [Name], [Code]) VALUES (17, N'Badplanken', N'BDPLK')
INSERT INTO [dbo].[AidProductGroup] ([AidProductGroupID], [Name], [Code]) VALUES (18, N'Douchekrukken', N'DCHKR')
INSERT INTO [dbo].[AidProductGroup] ([AidProductGroupID], [Name], [Code]) VALUES (19, N'Douchestoelen', N'DCHST')
INSERT INTO [dbo].[AidProductGroup] ([AidProductGroupID], [Name], [Code]) VALUES (20, N'Douche-/toiletstoelen', N'DTSTL')
INSERT INTO [dbo].[AidProductGroup] ([AidProductGroupID], [Name], [Code]) VALUES (21, N'Ondersteken', N'ONSTK')
INSERT INTO [dbo].[AidProductGroup] ([AidProductGroupID], [Name], [Code]) VALUES (22, N'Toiletrekken', N'TOIRK')
INSERT INTO [dbo].[AidProductGroup] ([AidProductGroupID], [Name], [Code]) VALUES (23, N'Toiletstoelen', N'TOIST')
INSERT INTO [dbo].[AidProductGroup] ([AidProductGroupID], [Name], [Code]) VALUES (24, N'Toiletverhogers', N'TOIVH')
INSERT INTO [dbo].[AidProductGroup] ([AidProductGroupID], [Name], [Code]) VALUES (25, N'Bedcarriers', N'BDCAR')
INSERT INTO [dbo].[AidProductGroup] ([AidProductGroupID], [Name], [Code]) VALUES (26, N'Hoog-laag bedden', N'HLBED')
INSERT INTO [dbo].[AidProductGroup] ([AidProductGroupID], [Name], [Code]) VALUES (27, N'Kinderbedden', N'KDBED')
INSERT INTO [dbo].[AidProductGroup] ([AidProductGroupID], [Name], [Code]) VALUES (28, N'Bedbeugels', N'BEDBG')
INSERT INTO [dbo].[AidProductGroup] ([AidProductGroupID], [Name], [Code]) VALUES (29, N'Bedleestafels', N'BEDLT')
INSERT INTO [dbo].[AidProductGroup] ([AidProductGroupID], [Name], [Code]) VALUES (30, N'Bedverhogers', N'BEDVH')
INSERT INTO [dbo].[AidProductGroup] ([AidProductGroupID], [Name], [Code]) VALUES (31, N'Bedverlengers', N'BEDVL')
INSERT INTO [dbo].[AidProductGroup] ([AidProductGroupID], [Name], [Code]) VALUES (32, N'Dekenbogen', N'DEKBG')
INSERT INTO [dbo].[AidProductGroup] ([AidProductGroupID], [Name], [Code]) VALUES (33, N'Papegaaien', N'PAPEG')
INSERT INTO [dbo].[AidProductGroup] ([AidProductGroupID], [Name], [Code]) VALUES (34, N'Rugsteunen', N'RUGST')
INSERT INTO [dbo].[AidProductGroup] ([AidProductGroupID], [Name], [Code]) VALUES (35, N'Windringen', N'WNDRI')
INSERT INTO [dbo].[AidProductGroup] ([AidProductGroupID], [Name], [Code]) VALUES (36, N'Trippelstoelen', N'TRPST')
INSERT INTO [dbo].[AidProductGroup] ([AidProductGroupID], [Name], [Code]) VALUES (37, N'Standaarden', N'STNDR')
INSERT INTO [dbo].[AidProductGroup] ([AidProductGroupID], [Name], [Code]) VALUES (38, N'AD-matrassen', N'ADMAT')
INSERT INTO [dbo].[AidProductGroup] ([AidProductGroupID], [Name], [Code]) VALUES (39, N'Tilbanden', N'TILBD')
INSERT INTO [dbo].[AidProductGroup] ([AidProductGroupID], [Name], [Code]) VALUES (40, N'Toiletbanden', N'TOIBD')
INSERT INTO [dbo].[AidProductGroup] ([AidProductGroupID], [Name], [Code]) VALUES (41, N'AD-zitkussens', N'ADZKS')
PRINT(N'Operation applied to 32 rows out of 32')

PRINT(N'Add rows to [dbo].[FlowFieldDataSource]')
SET IDENTITY_INSERT [dbo].[FlowFieldDataSource] ON
INSERT INTO [dbo].[FlowFieldDataSource] ([FlowFieldDataSourceID], [JsonData]) VALUES (175, N'{"Options": [{"Text": "<Maak een keuze>", "Value": ""}, {"Text": "Tussen 08:00 - 11:00", "Value": "08_to_11_hours"},{"Text": "Tussen 11:00 - 14:00", "Value": "11_to_14_hours"},{"Text": "Tussen 14:00 - 17:00", "Value": "14_to_17_hours"}]}')
INSERT INTO [dbo].[FlowFieldDataSource] ([FlowFieldDataSourceID], [JsonData]) VALUES (176, N'{"Options": [
{"Text": "Geen","Value": "addressRemark_none"},
{"Text": "Appartement met lift", "Value": "addressRemark_apartment_with_elevator"},
{"Text": "Bel klant voor aankomst", "Value": "addressRemark_phoneClientBeforeArrival"},
{"Text": "Bezorging bij de achteringang", "Value": "addressRemark_deliveryAtRearEntrance"},
{"Text": "Geduld aan de deur", "Value": "addressRemark_patienceAtDoor"},
{"Text": "Grind pad", "Value": "addressRemark_gravel_path"},
{"Text": "Herhaaldelijk aan de deur bellen", "Value": "addressRemark_ringRepeatedly"},
{"Text": "Hotel campingpark", "Value": "addressRemark_hotel_camping_park"},
{"Text": "Inchecken bij de receptie", "Value": "addressRemark_checkInAtReceptionCare"},
{"Text": "Klant kan niet zelf openen", "Value": "addressRemark_client_cannot_open"},
{"Text": "Parkeer probleem", "Value": "addressRemark_parking_problem"},
{"Text": "Risico van infectie", "Value": "addressRemark_risk_of_infection"},
{"Text": "Trap", "Value": "addressRemark_stairway"},
{"Text": "Verzorgingstehuis", "Value": "addressRemark_nursing_home"}
]}')
SET IDENTITY_INSERT [dbo].[FlowFieldDataSource] OFF
PRINT(N'Operation applied to 2 rows out of 2')

PRINT(N'Add row to [dbo].[Supplier]')
INSERT INTO [dbo].[Supplier] ([SupplierID], [Name], [DirectOrdering], [PhoneNumber]) VALUES (3, N'MediPoint', 0, NULL)

PRINT(N'Add rows to [dbo].[AidProduct]')
INSERT INTO [dbo].[AidProduct] ([AidProductID], [AidProductGroupID], [SupplierID], [Name], [Description], [ImageData], [Price], [PricePerDay], [TransportCosts], [AuthorizationForm], [Code], [InsurersUZOVIList]) VALUES (200011, 10, 3, N'Gipssteun links', N'Ter ondersteuning van bijvoorbeeld bij een gebroken been. Een gipssteun kan niet zonder een rolstoel besteld worden. Wordt ook wel genoemd: beenlade of gipslade.', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL)
INSERT INTO [dbo].[AidProduct] ([AidProductID], [AidProductGroupID], [SupplierID], [Name], [Description], [ImageData], [Price], [PricePerDay], [TransportCosts], [AuthorizationForm], [Code], [InsurersUZOVIList]) VALUES (200012, 10, 3, N'Gipssteun rechts', N'Ter ondersteuning van bijvoorbeeld bij een gebroken been. Een gipssteun kan niet zonder een rolstoel besteld worden. Wordt ook wel genoemd: beenlade of gipslade.', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL)
INSERT INTO [dbo].[AidProduct] ([AidProductID], [AidProductGroupID], [SupplierID], [Name], [Description], [ImageData], [Price], [PricePerDay], [TransportCosts], [AuthorizationForm], [Code], [InsurersUZOVIList]) VALUES (200013, 11, 3, N'Drempelhulp 2 cm.', N'Ter overbrugging van een hoogteverschil bij bijvoorbeeld een drempel binnenshuis.', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL)
INSERT INTO [dbo].[AidProduct] ([AidProductID], [AidProductGroupID], [SupplierID], [Name], [Description], [ImageData], [Price], [PricePerDay], [TransportCosts], [AuthorizationForm], [Code], [InsurersUZOVIList]) VALUES (200014, 11, 3, N'Drempelhulp 4 cm.', N'Ter overbrugging van een hoogteverschil bij bijvoorbeeld een drempel binnenshuis.', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL)
INSERT INTO [dbo].[AidProduct] ([AidProductID], [AidProductGroupID], [SupplierID], [Name], [Description], [ImageData], [Price], [PricePerDay], [TransportCosts], [AuthorizationForm], [Code], [InsurersUZOVIList]) VALUES (200015, 11, 3, N'Drempelhulp 5 cm.', N'Ter overbrugging van een hoogteverschil bij bijvoorbeeld een drempel binnenshuis.', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL)
INSERT INTO [dbo].[AidProduct] ([AidProductID], [AidProductGroupID], [SupplierID], [Name], [Description], [ImageData], [Price], [PricePerDay], [TransportCosts], [AuthorizationForm], [Code], [InsurersUZOVIList]) VALUES (200016, 12, 3, N'Draaischijf standaard', N'Met hulp een staande transfer maken tussen bijvoorbeeld bed en rolstoel. Het is hierbij van belang dat de gebruiker goed kan staan op minimaal 1 been.', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL)
INSERT INTO [dbo].[AidProduct] ([AidProductID], [AidProductGroupID], [SupplierID], [Name], [Description], [ImageData], [Price], [PricePerDay], [TransportCosts], [AuthorizationForm], [Code], [InsurersUZOVIList]) VALUES (200017, 12, 3, N'Draaischijf Turner', N'Om een transfer te maken middels draaien tussen bijvoorbeeld bed en rolstoel. Het is hierbij van belang dat de gebruiker zich kan vasthouden aan de beugel, rompstabiliteit heeft en kan staan op minimaal 1 been.', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL)
INSERT INTO [dbo].[AidProduct] ([AidProductID], [AidProductGroupID], [SupplierID], [Name], [Description], [ImageData], [Price], [PricePerDay], [TransportCosts], [AuthorizationForm], [Code], [InsurersUZOVIList]) VALUES (200018, 13, 3, N'Sta-/loopbeugel standaard', N'Transferhulpmiddel waarbij een goede rompbalans en stafunctie nodig is, maar de client niet lang kan staan of zich zelfstandig kan verplaatsen.', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL)
INSERT INTO [dbo].[AidProduct] ([AidProductID], [AidProductGroupID], [SupplierID], [Name], [Description], [ImageData], [Price], [PricePerDay], [TransportCosts], [AuthorizationForm], [Code], [InsurersUZOVIList]) VALUES (200019, 14, 3, N'Transferplank standaard', N'Hulpmiddel voor het maken van een transfer van rolstoel naar bed, toilet of auto ter overbrugging van een kleine ruimte.', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL)
INSERT INTO [dbo].[AidProduct] ([AidProductID], [AidProductGroupID], [SupplierID], [Name], [Description], [ImageData], [Price], [PricePerDay], [TransportCosts], [AuthorizationForm], [Code], [InsurersUZOVIList]) VALUES (200020, 15, 3, N'Rol-/glijlaken middel: 122 x 68 cm', N'Voor verplaatsing binnen de grenzen van het bed van de client ter ondersteuning van zorg.', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL)
INSERT INTO [dbo].[AidProduct] ([AidProductID], [AidProductGroupID], [SupplierID], [Name], [Description], [ImageData], [Price], [PricePerDay], [TransportCosts], [AuthorizationForm], [Code], [InsurersUZOVIList]) VALUES (200021, 15, 3, N'Rol-/glijlaken klein: 87 x 68 cm', N'Voor verplaatsing binnen de grenzen van het bed van de client ter ondersteuning van zorg.', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL)
INSERT INTO [dbo].[AidProduct] ([AidProductID], [AidProductGroupID], [SupplierID], [Name], [Description], [ImageData], [Price], [PricePerDay], [TransportCosts], [AuthorizationForm], [Code], [InsurersUZOVIList]) VALUES (200022, 15, 3, N'Rol-/glijlaken groot: 210 x 100 cm', N'Voor verplaatsing binnen de grenzen van het bed van de client ter ondersteuning van zorg.', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL)
INSERT INTO [dbo].[AidProduct] ([AidProductID], [AidProductGroupID], [SupplierID], [Name], [Description], [ImageData], [Price], [PricePerDay], [TransportCosts], [AuthorizationForm], [Code], [InsurersUZOVIList]) VALUES (200023, 16, 3, N'Tillift actief XL', N'Om een transfer te kunnen maken met cliënten met sta-functie. Hierbij dient de cliënt minimaal steun te kunnen nemen op 1 been en te beschikken over een goede rompbalans. De manoeuvreerruimte dient groter dan 2 meter te zijn. Er dient een gladde ondergrond aanwezig te zijn. De bediener van de lift dient een zorgvuldige instructie te hebben gehad. De tilband voor de actieve lift bestel je apart.', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL)
INSERT INTO [dbo].[AidProduct] ([AidProductID], [AidProductGroupID], [SupplierID], [Name], [Description], [ImageData], [Price], [PricePerDay], [TransportCosts], [AuthorizationForm], [Code], [InsurersUZOVIList]) VALUES (200024, 16, 3, N'Tillift passief standaard', N'Voor de transfer van clienten zonder sta-functie van het bed naar toilet of (rol-)stoel. Een tilband (sling) bestel je apart.', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL)
INSERT INTO [dbo].[AidProduct] ([AidProductID], [AidProductGroupID], [SupplierID], [Name], [Description], [ImageData], [Price], [PricePerDay], [TransportCosts], [AuthorizationForm], [Code], [InsurersUZOVIList]) VALUES (200025, 16, 3, N'Tillift actief standaard', N'Om een transfer te kunnen maken met cliënten met sta-functie. Hierbij dient de cliënt minimaal steun te kunnen nemen op 1 been en te beschikken over een goede rompbalans. De manoeuvreerruimte dient groter dan 2 meter te zijn. Er dient een gladde ondergrond aanwezig te zijn. De bediener van de lift dient een zorgvuldige instructie te hebben gehad. De tilband voor de actieve lift bestel je apart.', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL)
INSERT INTO [dbo].[AidProduct] ([AidProductID], [AidProductGroupID], [SupplierID], [Name], [Description], [ImageData], [Price], [PricePerDay], [TransportCosts], [AuthorizationForm], [Code], [InsurersUZOVIList]) VALUES (200026, 17, 3, N'Badplank', N'Om in en uit bad te stappen of zithulp in de badkuip. Hiervoor is rompbalans nodig.', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL)
INSERT INTO [dbo].[AidProduct] ([AidProductID], [AidProductGroupID], [SupplierID], [Name], [Description], [ImageData], [Price], [PricePerDay], [TransportCosts], [AuthorizationForm], [Code], [InsurersUZOVIList]) VALUES (200027, 18, 3, N'Douchekruk standaard', N'Zithulp bij douchen en ondersteuning in verband met (lichte) evenwichtsproblemen en/of vermoeidheidsklachten. De cliënt heeft hiervoor een goede rompbalans en armfunctie nodig.', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL)
INSERT INTO [dbo].[AidProduct] ([AidProductID], [AidProductGroupID], [SupplierID], [Name], [Description], [ImageData], [Price], [PricePerDay], [TransportCosts], [AuthorizationForm], [Code], [InsurersUZOVIList]) VALUES (200028, 18, 3, N'Douchekruk XL', N'Zithulp bij douchen en ondersteuning in verband met (lichte) evenwichtsproblemen en/of vermoeidheidsklachten. De cliënt heeft hiervoor een goede rompbalans en armfunctie nodig.', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL)
INSERT INTO [dbo].[AidProduct] ([AidProductID], [AidProductGroupID], [SupplierID], [Name], [Description], [ImageData], [Price], [PricePerDay], [TransportCosts], [AuthorizationForm], [Code], [InsurersUZOVIList]) VALUES (200029, 19, 3, N'Douchestoel standaard', N'Zithulp bij douchen en ondersteuning in verband met (lichte) evenwichtsproblemen en/of vermoeidheidsklachten.', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL)
INSERT INTO [dbo].[AidProduct] ([AidProductID], [AidProductGroupID], [SupplierID], [Name], [Description], [ImageData], [Price], [PricePerDay], [TransportCosts], [AuthorizationForm], [Code], [InsurersUZOVIList]) VALUES (200030, 19, 3, N'Douchestoel verrijdbaar', N'Een verrijdbare douchestoel voor mensen die niet of beperkt kunnen staan.', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL)
INSERT INTO [dbo].[AidProduct] ([AidProductID], [AidProductGroupID], [SupplierID], [Name], [Description], [ImageData], [Price], [PricePerDay], [TransportCosts], [AuthorizationForm], [Code], [InsurersUZOVIList]) VALUES (200031, 19, 3, N'Douchestoel XL', N'Een verrijdbare douchestoel voor mensen die niet of beperkt kunnen staan.', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL)
INSERT INTO [dbo].[AidProduct] ([AidProductID], [AidProductGroupID], [SupplierID], [Name], [Description], [ImageData], [Price], [PricePerDay], [TransportCosts], [AuthorizationForm], [Code], [InsurersUZOVIList]) VALUES (200032, 20, 3, N'Douche-/toiletstoel standaard', N'Om zowel zelfstandig en op tijd naar het toilet te kunnen gaan en als ondersteuning bij het douchen.', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL)
INSERT INTO [dbo].[AidProduct] ([AidProductID], [AidProductGroupID], [SupplierID], [Name], [Description], [ImageData], [Price], [PricePerDay], [TransportCosts], [AuthorizationForm], [Code], [InsurersUZOVIList]) VALUES (200033, 20, 3, N'Douche-/toiletstoel XL', N'Om zowel zelfstandig en op tijd naar het toilet te kunnen gaan en als ondersteuning bij het douchen.', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL)
INSERT INTO [dbo].[AidProduct] ([AidProductID], [AidProductGroupID], [SupplierID], [Name], [Description], [ImageData], [Price], [PricePerDay], [TransportCosts], [AuthorizationForm], [Code], [InsurersUZOVIList]) VALUES (200034, 20, 3, N'Douche-/toiletstoel verrijdbaar', N'Om zelfstandig naar het toilet te kunnen gaan en/of  staand te kunnen douchen. Wordt geleverd met een emmer of ondersteek.', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL)
INSERT INTO [dbo].[AidProduct] ([AidProductID], [AidProductGroupID], [SupplierID], [Name], [Description], [ImageData], [Price], [PricePerDay], [TransportCosts], [AuthorizationForm], [Code], [InsurersUZOVIList]) VALUES (200035, 21, 3, N'Ondersteek standaard', N'Een ondersteek is bedoeld voor toiletgebruik op bed. Dit artikel wordt vaak gebruikt door bedlegerige patiënten of bij thuisbevallingen.', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL)
INSERT INTO [dbo].[AidProduct] ([AidProductID], [AidProductGroupID], [SupplierID], [Name], [Description], [ImageData], [Price], [PricePerDay], [TransportCosts], [AuthorizationForm], [Code], [InsurersUZOVIList]) VALUES (200036, 22, 3, N'Overzettoiletstoel standaard', N'Een overzettoiletstoel is geschikt voor mensen die moeite hebben om op het toilet te gaan zitten en daarna weer op te staan. Met de handgrepen aan de armleuningen is er extra steunmogelijkheid van de armen.', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL)
INSERT INTO [dbo].[AidProduct] ([AidProductID], [AidProductGroupID], [SupplierID], [Name], [Description], [ImageData], [Price], [PricePerDay], [TransportCosts], [AuthorizationForm], [Code], [InsurersUZOVIList]) VALUES (200037, 23, 3, N'Toiletstoel standaard', N'Om zelfstandig of op tijd naar het toilet te kunnen gaan.', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL)
INSERT INTO [dbo].[AidProduct] ([AidProductID], [AidProductGroupID], [SupplierID], [Name], [Description], [ImageData], [Price], [PricePerDay], [TransportCosts], [AuthorizationForm], [Code], [InsurersUZOVIList]) VALUES (200038, 23, 3, N'Toiletstoel verrijdbaar standaard', N'Om zelfstandig of op tijd naar het toilet te kunnen gaan. De stoel kan gemakkelijk in meerdere ruimtes gereden worden.', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL)
INSERT INTO [dbo].[AidProduct] ([AidProductID], [AidProductGroupID], [SupplierID], [Name], [Description], [ImageData], [Price], [PricePerDay], [TransportCosts], [AuthorizationForm], [Code], [InsurersUZOVIList]) VALUES (200039, 23, 3, N'Toiletstoel XL', N'Om zelfstandig of op tijd naar het toilet te kunnen gaan.', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL)
INSERT INTO [dbo].[AidProduct] ([AidProductID], [AidProductGroupID], [SupplierID], [Name], [Description], [ImageData], [Price], [PricePerDay], [TransportCosts], [AuthorizationForm], [Code], [InsurersUZOVIList]) VALUES (200040, 23, 3, N'Toiletstoel verrijdbaar XL', N'Om zelfstandig of op tijd naar het toilet te kunnen gaan. De stoel kan gemakkelijk in meerdere ruimtes gereden worden.', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL)
INSERT INTO [dbo].[AidProduct] ([AidProductID], [AidProductGroupID], [SupplierID], [Name], [Description], [ImageData], [Price], [PricePerDay], [TransportCosts], [AuthorizationForm], [Code], [InsurersUZOVIList]) VALUES (200041, 24, 3, N'Toiletverhoger 5 cm.', N'Verhoging van het toilet om makkelijk te gaan zitten en op te staan van het toilet.', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL)
INSERT INTO [dbo].[AidProduct] ([AidProductID], [AidProductGroupID], [SupplierID], [Name], [Description], [ImageData], [Price], [PricePerDay], [TransportCosts], [AuthorizationForm], [Code], [InsurersUZOVIList]) VALUES (200042, 24, 3, N'Toiletverhoger 10 cm.', N'Verhoging van het toilet om makkelijk te gaan zitten en op te staan van het toilet.', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL)
INSERT INTO [dbo].[AidProduct] ([AidProductID], [AidProductGroupID], [SupplierID], [Name], [Description], [ImageData], [Price], [PricePerDay], [TransportCosts], [AuthorizationForm], [Code], [InsurersUZOVIList]) VALUES (200043, 26, 3, N'Hoog-laag bed XL (excl. matras)', N'Om verzorging op bed te kunnen realiseren. Door de hoog-laag functie kan de verzorgende veilig en gemakkelijk werken. De cliënt stapt gemakkelijker in en uit bed. Het hoog-laag bed wordt geleverd inclusief bedhekken en papegaai maar zonder matras. Bestel voor mensen langer dan 185 cm een bedverlenger. Voor kindere of een extra laag hoog laag bed kun je het beste medipoint bellen.', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL)
INSERT INTO [dbo].[AidProduct] ([AidProductID], [AidProductGroupID], [SupplierID], [Name], [Description], [ImageData], [Price], [PricePerDay], [TransportCosts], [AuthorizationForm], [Code], [InsurersUZOVIList]) VALUES (200044, 26, 3, N'Hoog-laag bed 90 x 200 cm. standaard (excl. matras)', N'Om verzorging op bed te kunnen realiseren. Door de hoog-laag functie kan de verzorgende veilig en gemakkelijk werken. De cliënt stapt gemakkelijker in en uit bed. Het hoog-laag bed wordt geleverd inclusief bedhekken en papegaai maar zonder matras. Bestel voor mensen langer dan 185 cm een bedverlenger. Voor kindere of een extra laag hoog laag bed kun je het beste medipoint bellen.', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL)
INSERT INTO [dbo].[AidProduct] ([AidProductID], [AidProductGroupID], [SupplierID], [Name], [Description], [ImageData], [Price], [PricePerDay], [TransportCosts], [AuthorizationForm], [Code], [InsurersUZOVIList]) VALUES (200045, 28, 3, N'1. Bedbeugel - enkel', N'De bedbeugel is geschikt voor bedden met een bedombouw of bedhek. Met de bedbeugel kan de client met steun in en uit bed stappen en ondersteund worden bij het omdraaien in bed. De beugel bestaat uit losse onderdelen die door middel van drukknoppen eenvoudig in elkaar geschoven kunnen worden.', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL)
INSERT INTO [dbo].[AidProduct] ([AidProductID], [AidProductGroupID], [SupplierID], [Name], [Description], [ImageData], [Price], [PricePerDay], [TransportCosts], [AuthorizationForm], [Code], [InsurersUZOVIList]) VALUES (200046, 29, 3, N'Bedleestafel (loslevering)', N'Ter ondersteuning van algemene dagelijkse levensverrichtingen (ADL). In hoogte verstelbaar.', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL)
INSERT INTO [dbo].[AidProduct] ([AidProductID], [AidProductGroupID], [SupplierID], [Name], [Description], [ImageData], [Price], [PricePerDay], [TransportCosts], [AuthorizationForm], [Code], [InsurersUZOVIList]) VALUES (200047, 30, 3, N'Bedverhoger 14 cm.', N'Voor (kraam)zorg vanuit bed moet je bed een minimale hoogte hebben van 80 cm. Verhoog je bed eenvoudig zelf met behulp van bedverhogers die in twee maten verkrijgbaar zijn', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL)
INSERT INTO [dbo].[AidProduct] ([AidProductID], [AidProductGroupID], [SupplierID], [Name], [Description], [ImageData], [Price], [PricePerDay], [TransportCosts], [AuthorizationForm], [Code], [InsurersUZOVIList]) VALUES (200048, 30, 3, N'Bedverhoger 28 cm.', N'Voor (kraam)zorg vanuit bed moet je bed een minimale hoogte hebben van 80 cm. Verhoog je bed eenvoudig zelf met behulp van bedverhogers die in twee maten verkrijgbaar zijn', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL)
INSERT INTO [dbo].[AidProduct] ([AidProductID], [AidProductGroupID], [SupplierID], [Name], [Description], [ImageData], [Price], [PricePerDay], [TransportCosts], [AuthorizationForm], [Code], [InsurersUZOVIList]) VALUES (200049, 31, 3, N'Bedverlenger 10 cm incl. matrasdeel', N'Bedverlenger inclusief matrasdeel. Belangrijk: Het extra matrasdeel moet aan het hoofdeinde geplaatst worden, niet aan de verlengde kant.', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL)
INSERT INTO [dbo].[AidProduct] ([AidProductID], [AidProductGroupID], [SupplierID], [Name], [Description], [ImageData], [Price], [PricePerDay], [TransportCosts], [AuthorizationForm], [Code], [InsurersUZOVIList]) VALUES (200050, 31, 3, N'Bedverlenger 20 cm incl. matrasdeel', N'Bedverlenger inclusief matrasdeel. Belangrijk: Het extra matrasdeel moet aan het hoofdeinde geplaatst worden, niet aan de verlengde kant.', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL)
INSERT INTO [dbo].[AidProduct] ([AidProductID], [AidProductGroupID], [SupplierID], [Name], [Description], [ImageData], [Price], [PricePerDay], [TransportCosts], [AuthorizationForm], [Code], [InsurersUZOVIList]) VALUES (200051, 32, 3, N'Dekenboog (loslevering)', N'Met deze dekenboog voorkomt u druk die op de benen onstaat door een dekbed.', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL)
INSERT INTO [dbo].[AidProduct] ([AidProductID], [AidProductGroupID], [SupplierID], [Name], [Description], [ImageData], [Price], [PricePerDay], [TransportCosts], [AuthorizationForm], [Code], [InsurersUZOVIList]) VALUES (200052, 33, 3, N'Papegaai vrijstaand', N'Een papegaai steunt bij het draaien en verplaatsen in bed voor de client.', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL)
INSERT INTO [dbo].[AidProduct] ([AidProductID], [AidProductGroupID], [SupplierID], [Name], [Description], [ImageData], [Price], [PricePerDay], [TransportCosts], [AuthorizationForm], [Code], [InsurersUZOVIList]) VALUES (200053, 34, 3, N'Rugsteun (loslevering)', N'Met behulp van de rugsteun kan de client een zithouding aannemen in bed.', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL)
INSERT INTO [dbo].[AidProduct] ([AidProductID], [AidProductGroupID], [SupplierID], [Name], [Description], [ImageData], [Price], [PricePerDay], [TransportCosts], [AuthorizationForm], [Code], [InsurersUZOVIList]) VALUES (200054, 35, 3, N'Windring', N'De met lucht gevulde rubberen band, is bedoeld om de druk bij de stuit of de anaalstreek te verminderen tijdens het zitten.', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL)
INSERT INTO [dbo].[AidProduct] ([AidProductID], [AidProductGroupID], [SupplierID], [Name], [Description], [ImageData], [Price], [PricePerDay], [TransportCosts], [AuthorizationForm], [Code], [InsurersUZOVIList]) VALUES (200055, 36, 3, N'Trippelstoel, elektrisch verstelbaar standaard', N'Voor het zittend door het huis verplaatsen met behulp van de voeten (''trippelen''). Indien er sprake is van verminderde kracht om op te staan of bij risico op valgevaar is deze stoel elektrisch in hoogte verstelbaar. De stoel is uitgevoerd met zwenkwielen en heeft een rem met handbediening, zodat je cliënt veilig op kan staan. Heeft je cliënt een heupbeperking? Neem dan contact op met de leverancier voor een trippelstoel met arthrodesezitting.', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL)
INSERT INTO [dbo].[AidProduct] ([AidProductID], [AidProductGroupID], [SupplierID], [Name], [Description], [ImageData], [Price], [PricePerDay], [TransportCosts], [AuthorizationForm], [Code], [InsurersUZOVIList]) VALUES (200056, 36, 3, N'Trippelstoel standaard (gasveer verstelling)', N'Voor het zittend door het huis verplaatsen met behulp van de voeten (''trippelen''). Indien er sprake is van risico op valgevaar is deze stoel met een gasveer in hoogte verstelbaar. De stoel is uitgevoerd met zwenkwielen en heeft een rem met handbediening, zodat je cliënt veilig op kan staan. Heeft je cliënt een heupbeperking? Neem dan contact op met de leverancier voor een trippelstoel met arthrodesezitting.', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL)
INSERT INTO [dbo].[AidProduct] ([AidProductID], [AidProductGroupID], [SupplierID], [Name], [Description], [ImageData], [Price], [PricePerDay], [TransportCosts], [AuthorizationForm], [Code], [InsurersUZOVIList]) VALUES (200057, 37, 3, N'Standaard matras', N'Standaard matras met incontinentiehoes.', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL)
INSERT INTO [dbo].[AidProduct] ([AidProductID], [AidProductGroupID], [SupplierID], [Name], [Description], [ImageData], [Price], [PricePerDay], [TransportCosts], [AuthorizationForm], [Code], [InsurersUZOVIList]) VALUES (200058, 38, 3, N'Statisch AD-matras standaard', N'Dit anti-decubitus matras kan preventief maar ook curatief (genezend) worden ingezet voor mensen met een laag risico op decubitus. De afmeting van het matras is 85-90 cm x 200 cm. Het matras is opgebouwd uit een onderlaag van koudschuim en een toplaag van traagschuim.', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL)
INSERT INTO [dbo].[AidProduct] ([AidProductID], [AidProductGroupID], [SupplierID], [Name], [Description], [ImageData], [Price], [PricePerDay], [TransportCosts], [AuthorizationForm], [Code], [InsurersUZOVIList]) VALUES (200059, 38, 3, N'Licht dynamisch AD-matras standaard', N'Dit anti-decubitus matras is geschikt voor mensen met beginnende decubitus of een gemiddeld risico op decubitus. Het matras bestaat uit een ondermatras met daarop een toplaag van luchtcellen. Dit matras biedt optimale drukverdeling. De luchtbanen in het matras wisselen in een cyclus. Het matras kun je ook statisch inzetten voor het maken van transfers en verzorging op bed. Afmetingen: 85-90 x 200 cm.', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL)
INSERT INTO [dbo].[AidProduct] ([AidProductID], [AidProductGroupID], [SupplierID], [Name], [Description], [ImageData], [Price], [PricePerDay], [TransportCosts], [AuthorizationForm], [Code], [InsurersUZOVIList]) VALUES (200060, 38, 3, N'Sterk dynamisch AD-matras standaard', N'Dit anti-decubitus matras is geschikt voor mensen met decubitus of met een hoog risico op decubitus. Bijvoorbeeld voor terminale patiënten. Dit matras biedt optimale drukverdeling. De luchtbanen in het matras wisselen. Het matras kun je ook statisch inzetten voor het maken van transfers en verzorging op bed. Afmetingen: 85-90 x 200 cm Als er sprake is van decubitus wordt geadviseerd om ook een anti-decubitus zitkussen te bestellen.', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL)
INSERT INTO [dbo].[AidProduct] ([AidProductID], [AidProductGroupID], [SupplierID], [Name], [Description], [ImageData], [Price], [PricePerDay], [TransportCosts], [AuthorizationForm], [Code], [InsurersUZOVIList]) VALUES (200061, 39, 3, N'Tilband actief', N'Tilband bij een actieve tillift.', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL)
INSERT INTO [dbo].[AidProduct] ([AidProductID], [AidProductGroupID], [SupplierID], [Name], [Description], [ImageData], [Price], [PricePerDay], [TransportCosts], [AuthorizationForm], [Code], [InsurersUZOVIList]) VALUES (200062, 40, 3, N'Toiletband passief maat S', N'Toiletband te gebruiken als tilband bij een passieve tillift voor transfer van en naar toilet. Een client heeft nog spierspanning in het bovenlijf nodig om gebruik te kunnen maken van deze band.', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL)
INSERT INTO [dbo].[AidProduct] ([AidProductID], [AidProductGroupID], [SupplierID], [Name], [Description], [ImageData], [Price], [PricePerDay], [TransportCosts], [AuthorizationForm], [Code], [InsurersUZOVIList]) VALUES (200063, 40, 3, N'Toiletband passief maat M', N'Toiletband te gebruiken als tilband bij een passieve tillift voor transfer van en naar toilet. Een client heeft nog spierspanning in het bovenlijf nodig om gebruik te kunnen maken van deze band.', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL)
INSERT INTO [dbo].[AidProduct] ([AidProductID], [AidProductGroupID], [SupplierID], [Name], [Description], [ImageData], [Price], [PricePerDay], [TransportCosts], [AuthorizationForm], [Code], [InsurersUZOVIList]) VALUES (200064, 40, 3, N'Toiletband passief maat L', N'Toiletband te gebruiken als tilband bij een passieve tillift voor transfer van en naar toilet. Een client heeft nog spierspanning in het bovenlijf nodig om gebruik te kunnen maken van deze band.', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL)
INSERT INTO [dbo].[AidProduct] ([AidProductID], [AidProductGroupID], [SupplierID], [Name], [Description], [ImageData], [Price], [PricePerDay], [TransportCosts], [AuthorizationForm], [Code], [InsurersUZOVIList]) VALUES (200065, 40, 3, N'Toiletband passief maat XL', N'Toiletband te gebruiken als tilband bij een passieve tillift voor transfer van en naar toilet. Een client heeft nog spierspanning in het bovenlijf nodig om gebruik te kunnen maken van deze band.', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL)
INSERT INTO [dbo].[AidProduct] ([AidProductID], [AidProductGroupID], [SupplierID], [Name], [Description], [ImageData], [Price], [PricePerDay], [TransportCosts], [AuthorizationForm], [Code], [InsurersUZOVIList]) VALUES (200066, 39, 3, N'Tilband passief, maat XL', N'Standaard tilband. Alleen te gebruiken met een passieve tillift.', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL)
INSERT INTO [dbo].[AidProduct] ([AidProductID], [AidProductGroupID], [SupplierID], [Name], [Description], [ImageData], [Price], [PricePerDay], [TransportCosts], [AuthorizationForm], [Code], [InsurersUZOVIList]) VALUES (200067, 39, 3, N'Tilband passief, maat L', N'Standaard tilband. Alleen te gebruiken met een passieve tillift.', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL)
INSERT INTO [dbo].[AidProduct] ([AidProductID], [AidProductGroupID], [SupplierID], [Name], [Description], [ImageData], [Price], [PricePerDay], [TransportCosts], [AuthorizationForm], [Code], [InsurersUZOVIList]) VALUES (200068, 39, 3, N'Tilband passief, maat M', N'Standaard tilband. Alleen te gebruiken met een passieve tillift.', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL)
INSERT INTO [dbo].[AidProduct] ([AidProductID], [AidProductGroupID], [SupplierID], [Name], [Description], [ImageData], [Price], [PricePerDay], [TransportCosts], [AuthorizationForm], [Code], [InsurersUZOVIList]) VALUES (200069, 39, 3, N'Tilband passief, maat S', N'Standaard tilband. Alleen te gebruiken met een passieve tillift.', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL)
INSERT INTO [dbo].[AidProduct] ([AidProductID], [AidProductGroupID], [SupplierID], [Name], [Description], [ImageData], [Price], [PricePerDay], [TransportCosts], [AuthorizationForm], [Code], [InsurersUZOVIList]) VALUES (200070, 41, 3, N'AD-zitkussen Roho, laag profiel', N'Voor clienten welke langere tijd in een stoel zitten en decubitus hebben ontwikkeld of een hoog risico op decubitus hebben. Dit anti-decubitus zitkussen heeft drukverdelende eigenschappen door de luchtcompartimenten. Inclusief pomp en incontinentie hoes.', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL)
INSERT INTO [dbo].[AidProduct] ([AidProductID], [AidProductGroupID], [SupplierID], [Name], [Description], [ImageData], [Price], [PricePerDay], [TransportCosts], [AuthorizationForm], [Code], [InsurersUZOVIList]) VALUES (200071, 41, 3, N'AD-zitkussen GelCell', N'Doordat het kussen is gevuld met gel en foam wordt de lichaamsdruk verdeeld over het hele zitvlak. Ook wordt door de gel en foam de lichaamswarmte beter verdeeld en gelijkmatiger afgevoerd. Dit kussen is uitermate geschikt voor cliënten met een gemiddeld tot zwaar gewicht en beginnend decubitus of een verhoogd risico op decubitus.', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL)
INSERT INTO [dbo].[AidProduct] ([AidProductID], [AidProductGroupID], [SupplierID], [Name], [Description], [ImageData], [Price], [PricePerDay], [TransportCosts], [AuthorizationForm], [Code], [InsurersUZOVIList]) VALUES (200072, 41, 3, N'AD-zitkussen Roho, hoog profiel', N'Voor clienten welke langere tijd in een stoel zitten en decubitus hebben ontwikkeld of een hoog risico op decubitus hebben. Dit anti-decubitus zitkussen heeft drukverdelende eigenschappen door de luchtcompartimenten. Inclusief pomp en incontinentie hoes.', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL)
INSERT INTO [dbo].[AidProduct] ([AidProductID], [AidProductGroupID], [SupplierID], [Name], [Description], [ImageData], [Price], [PricePerDay], [TransportCosts], [AuthorizationForm], [Code], [InsurersUZOVIList]) VALUES (200073, 41, 3, N'AD-zitkussen traagfoam 5 cm.', N'Voor clienten welke langere tijd in een stoel of trippelstoel zitten en beginnende decubitus of een matig risico op decubitus hebben.', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL)
INSERT INTO [dbo].[AidProduct] ([AidProductID], [AidProductGroupID], [SupplierID], [Name], [Description], [ImageData], [Price], [PricePerDay], [TransportCosts], [AuthorizationForm], [Code], [InsurersUZOVIList]) VALUES (200074, 41, 3, N'AD-zitkussen traagfoam 7 cm.', N'Voor clienten welke langere tijd in een stoel of trippelstoel zitten en beginnende decubitus of een matig risico op decubitus hebben.', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL)
INSERT INTO [dbo].[AidProduct] ([AidProductID], [AidProductGroupID], [SupplierID], [Name], [Description], [ImageData], [Price], [PricePerDay], [TransportCosts], [AuthorizationForm], [Code], [InsurersUZOVIList]) VALUES (200075, 28, 3, N'2. Bedbeugel - dubbel', N'De dubbele bedbuegel geeft de client steun aan beide zijden van het bed. Het geeft steun bij het draaien in bed en het veilig in en uit bed stappen. Dit stevige hulpmiddel heeft aan beide zijden een grote handgreep. Eenvoudig in het gebruik. De beugel legt je cliënt onder het matras. De lengte van de beugel kan worden ingesteld.', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL)
PRINT(N'Operation applied to 65 rows out of 65')

PRINT(N'Add rows to [dbo].[FlowField]')
INSERT INTO [dbo].[FlowField] ([FlowFieldID], [Name], [Label], [Description], [Type], [Format], [Regexp], [MaxLength], [UseInReadModel], [FlowFieldDataSourceID]) VALUES (3108, N'MediPointTransportAddressRemark', N'Leveringsinformatie voor transporteur', NULL, 7, NULL, NULL, NULL, 0, 176)
INSERT INTO [dbo].[FlowField] ([FlowFieldID], [Name], [Label], [Description], [Type], [Format], [Regexp], [MaxLength], [UseInReadModel], [FlowFieldDataSourceID]) VALUES (3109, N'AfleverTijdMediPoint', N'Aflevertijd', NULL, 7, NULL, NULL, NULL, 0, 175)
INSERT INTO [dbo].[FlowField] ([FlowFieldID], [Name], [Label], [Description], [Type], [Format], [Regexp], [MaxLength], [UseInReadModel], [FlowFieldDataSourceID]) VALUES (3110, N'OntvangerBevestigingsEmailAdresTransferpunt', N'Bevestiging e-mail van bestelling naar', NULL, 1, NULL, NULL, NULL, 0, NULL)
INSERT INTO [dbo].[FlowField] ([FlowFieldID], [Name], [Label], [Description], [Type], [Format], [Regexp], [MaxLength], [UseInReadModel], [FlowFieldDataSourceID]) VALUES (3111, N'OntvangerBevestigingsEmailAdresPatient', N'Bevestiging e-mail van bestelling naar', NULL, 1, NULL, NULL, NULL, 0, NULL)
PRINT(N'Operation applied to 4 rows out of 4')

PRINT(N'Add rows to [dbo].[FlowFieldAttribute]')
INSERT INTO [dbo].[FlowFieldAttribute] ([FlowFieldAttributeID], [FlowFieldID], [FormTypeID], [IsActive], [OriginFormTypeID], [OriginFlowFieldID], [OriginCopyToAll], [Required], [Visible], [ReadOnly], [SignalOnChange], [RequiredByFlowFieldID], [RequiredByFlowFieldValue]) VALUES (3124, 3109, 242, 1, NULL, NULL, 0, 1, 1, 0, 1, NULL, NULL)
INSERT INTO [dbo].[FlowFieldAttribute] ([FlowFieldAttributeID], [FlowFieldID], [FormTypeID], [IsActive], [OriginFormTypeID], [OriginFlowFieldID], [OriginCopyToAll], [Required], [Visible], [ReadOnly], [SignalOnChange], [RequiredByFlowFieldID], [RequiredByFlowFieldValue]) VALUES (3125, 3110, 242, 1, NULL, NULL, 0, 1, 1, 0, 1, NULL, NULL)
INSERT INTO [dbo].[FlowFieldAttribute] ([FlowFieldAttributeID], [FlowFieldID], [FormTypeID], [IsActive], [OriginFormTypeID], [OriginFlowFieldID], [OriginCopyToAll], [Required], [Visible], [ReadOnly], [SignalOnChange], [RequiredByFlowFieldID], [RequiredByFlowFieldValue]) VALUES (3126, 3111, 242, 1, NULL, NULL, 0, 1, 1, 0, 1, NULL, NULL)
INSERT INTO [dbo].[FlowFieldAttribute] ([FlowFieldAttributeID], [FlowFieldID], [FormTypeID], [IsActive], [OriginFormTypeID], [OriginFlowFieldID], [OriginCopyToAll], [Required], [Visible], [ReadOnly], [SignalOnChange], [RequiredByFlowFieldID], [RequiredByFlowFieldValue]) VALUES (3127, 3108, 242, 1, NULL, NULL, 0, 1, 1, 0, 1, NULL, NULL)
PRINT(N'Operation applied to 4 rows out of 4')

PRINT(N'Add constraints to [dbo].[FlowFieldAttribute]')
ALTER TABLE [dbo].[FlowFieldAttribute] WITH CHECK CHECK CONSTRAINT [FK_dbo.FlowFieldAttribute_dbo.FlowField_FlowFieldID]
ALTER TABLE [dbo].[FlowFieldAttribute] WITH CHECK CHECK CONSTRAINT [FK_dbo.FlowFieldAttribute_dbo.FormType_FormTypeID]
ALTER TABLE [dbo].[FlowFieldAttribute] WITH CHECK CHECK CONSTRAINT [FK_FlowFieldAttribute_FlowField]

PRINT(N'Add constraints to [dbo].[FlowField]')
ALTER TABLE [dbo].[FlowField] WITH CHECK CHECK CONSTRAINT [FK_FlowField_FlowFieldDataSource]

PRINT(N'Add constraints to [dbo].[AidProduct]')
ALTER TABLE [dbo].[AidProduct] WITH CHECK CHECK CONSTRAINT [FK_AidProduct_AidProductGroup]
ALTER TABLE [dbo].[AidProduct] WITH CHECK CHECK CONSTRAINT [FK_AidProduct_Supplier]
COMMIT TRANSACTION
GO
