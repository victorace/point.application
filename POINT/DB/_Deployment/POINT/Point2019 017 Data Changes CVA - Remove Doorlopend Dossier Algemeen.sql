/*
Run this script on:

pointreview.verzorgdeoverdracht.nl.TXX-DB-POINT-R    -  This database will be modified

to synchronize it with:

..Point

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 12.3.3.4490 from Red Gate Software Ltd at 12/04/2019 10:03:46

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION

PRINT 'Remove FormSetVersions and PhaseInstances before removing DD Algemeen from CVA'

	DELETE fsv from dbo.FormSetVersion fsv 
	INNER JOIN dbo.PhaseInstance phi ON phi.PhaseInstanceID = fsv.PhaseInstanceID
	WHERE phi.PhaseDefinitionID = 80
	DELETE phi from dbo.PhaseInstance phi WHERE PhaseDefinitionID = 80

PRINT 'Done'

PRINT(N'Drop constraints from [dbo].[PhaseDefinitionRights]')
ALTER TABLE [dbo].[PhaseDefinitionRights] NOCHECK CONSTRAINT [FK_PhaseDefinitionRights_DepartmentType]
ALTER TABLE [dbo].[PhaseDefinitionRights] NOCHECK CONSTRAINT [FK_PhaseDefinitionRights_OrganizationType]
ALTER TABLE [dbo].[PhaseDefinitionRights] NOCHECK CONSTRAINT [FK_PhaseDefinitionRights_PhaseDefinition]

PRINT(N'Drop constraints from [dbo].[PhaseDefinitionNavigation]')
ALTER TABLE [dbo].[PhaseDefinitionNavigation] NOCHECK CONSTRAINT [FK_PhaseDefinitionNavigation_PhaseDefinition]
ALTER TABLE [dbo].[PhaseDefinitionNavigation] NOCHECK CONSTRAINT [FK_PhaseDefinitionNavigation_TargetPhaseDefinition]

PRINT(N'Drop constraints from [dbo].[GeneralActionPhase]')
ALTER TABLE [dbo].[GeneralActionPhase] NOCHECK CONSTRAINT [FK_dbo.GeneralActionPhase_dbo.GeneralAction_GeneralActionID]
ALTER TABLE [dbo].[GeneralActionPhase] NOCHECK CONSTRAINT [FK_dbo.GeneralActionPhase_dbo.PhaseDefinition_PhaseDefinitionID]
ALTER TABLE [dbo].[GeneralActionPhase] NOCHECK CONSTRAINT [FK_GeneralActionPhase_OrganizationType]

PRINT(N'Drop constraints from [dbo].[PhaseDefinition]')
ALTER TABLE [dbo].[PhaseDefinition] NOCHECK CONSTRAINT [FK_dbo.PhaseDefinition_dbo.FlowDefinition_FlowDefinitionID]
ALTER TABLE [dbo].[PhaseDefinition] NOCHECK CONSTRAINT [FK_dbo.PhaseDefinition_dbo.OrganizationType_OrganizationTypeID]

PRINT(N'Drop constraint FK_dbo.FlowPhaseAttribute_dbo.PhaseDefinition_PhaseDefinitionID from [dbo].[FlowPhaseAttribute]')
ALTER TABLE [dbo].[FlowPhaseAttribute] NOCHECK CONSTRAINT [FK_dbo.FlowPhaseAttribute_dbo.PhaseDefinition_PhaseDefinitionID]

PRINT(N'Drop constraints from [dbo].[GeneralAction]')
ALTER TABLE [dbo].[GeneralAction] NOCHECK CONSTRAINT [FK_dbo.GeneralAction_dbo.GeneralActionType_GeneralActionTypeID]

PRINT(N'Delete rows from [dbo].[PhaseDefinitionRights]')
DELETE FROM [dbo].[PhaseDefinitionRights] WHERE [PhaseDefinitionRightsID] = 1122
DELETE FROM [dbo].[PhaseDefinitionRights] WHERE [PhaseDefinitionRightsID] = 3124
PRINT(N'Operation applied to 2 rows out of 2')

PRINT(N'Delete row from [dbo].[PhaseDefinitionNavigation]')
DELETE FROM [dbo].[PhaseDefinitionNavigation] WHERE [PhaseDefinitionID] = 80 AND [NextPhaseDefinitionID] = 80 AND [ActionTypeID] = 1

PRINT(N'Delete rows from [dbo].[GeneralActionPhase]')
DELETE FROM [dbo].[GeneralActionPhase] WHERE [GeneralActionPhaseID] = 296
DELETE FROM [dbo].[GeneralActionPhase] WHERE [GeneralActionPhaseID] = 301
PRINT(N'Operation applied to 2 rows out of 2')

PRINT(N'Delete row from [dbo].[PhaseDefinition]')
DELETE FROM [dbo].[PhaseDefinition] WHERE [PhaseDefinitionID] = 80

PRINT(N'Update row in [dbo].[PhaseDefinition]')
UPDATE [dbo].[PhaseDefinition] SET [Subcategory]=NULL, [MenuItemType]=2 WHERE [PhaseDefinitionID] = 64

PRINT(N'Update rows in [dbo].[GeneralAction]')
UPDATE [dbo].[GeneralAction] SET [HidePhaseButtons]=1 WHERE [GeneralActionID] = 32
UPDATE [dbo].[GeneralAction] SET [HidePhaseButtons]=1 WHERE [GeneralActionID] = 37
UPDATE [dbo].[GeneralAction] SET [HidePhaseButtons]=1 WHERE [GeneralActionID] = 38
PRINT(N'Operation applied to 3 rows out of 3')

PRINT(N'Add constraints to [dbo].[PhaseDefinitionRights]')
ALTER TABLE [dbo].[PhaseDefinitionRights] WITH CHECK CHECK CONSTRAINT [FK_PhaseDefinitionRights_DepartmentType]
ALTER TABLE [dbo].[PhaseDefinitionRights] WITH CHECK CHECK CONSTRAINT [FK_PhaseDefinitionRights_OrganizationType]
ALTER TABLE [dbo].[PhaseDefinitionRights] WITH CHECK CHECK CONSTRAINT [FK_PhaseDefinitionRights_PhaseDefinition]

PRINT(N'Add constraints to [dbo].[PhaseDefinitionNavigation]')
ALTER TABLE [dbo].[PhaseDefinitionNavigation] WITH CHECK CHECK CONSTRAINT [FK_PhaseDefinitionNavigation_PhaseDefinition]
ALTER TABLE [dbo].[PhaseDefinitionNavigation] WITH CHECK CHECK CONSTRAINT [FK_PhaseDefinitionNavigation_TargetPhaseDefinition]

PRINT(N'Add constraints to [dbo].[GeneralActionPhase]')
ALTER TABLE [dbo].[GeneralActionPhase] WITH CHECK CHECK CONSTRAINT [FK_dbo.GeneralActionPhase_dbo.GeneralAction_GeneralActionID]
ALTER TABLE [dbo].[GeneralActionPhase] WITH CHECK CHECK CONSTRAINT [FK_dbo.GeneralActionPhase_dbo.PhaseDefinition_PhaseDefinitionID]
ALTER TABLE [dbo].[GeneralActionPhase] WITH CHECK CHECK CONSTRAINT [FK_GeneralActionPhase_OrganizationType]

PRINT(N'Add constraints to [dbo].[PhaseDefinition]')
ALTER TABLE [dbo].[PhaseDefinition] WITH CHECK CHECK CONSTRAINT [FK_dbo.PhaseDefinition_dbo.FlowDefinition_FlowDefinitionID]
ALTER TABLE [dbo].[PhaseDefinition] WITH CHECK CHECK CONSTRAINT [FK_dbo.PhaseDefinition_dbo.OrganizationType_OrganizationTypeID]
ALTER TABLE [dbo].[FlowPhaseAttribute] WITH CHECK CHECK CONSTRAINT [FK_dbo.FlowPhaseAttribute_dbo.PhaseDefinition_PhaseDefinitionID]

PRINT(N'Add constraints to [dbo].[GeneralAction]')
ALTER TABLE [dbo].[GeneralAction] WITH CHECK CHECK CONSTRAINT [FK_dbo.GeneralAction_dbo.GeneralActionType_GeneralActionTypeID]
COMMIT TRANSACTION
GO
