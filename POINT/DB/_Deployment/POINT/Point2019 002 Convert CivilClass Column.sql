DISABLE TRIGGER dbo.Client_InsertUpdate ON dbo.Client
GO

UPDATE dbo.Client SET CivilClass = '0' WHERE CivilClass IN ('D')
GO
UPDATE dbo.Client SET CivilClass = '1' WHERE CivilClass IN ('M')
GO
UPDATE dbo.Client SET CivilClass = '2' WHERE CivilClass IN ('S')
GO
UPDATE dbo.Client SET CivilClass = '3' WHERE CivilClass IN ('P')
GO
UPDATE dbo.Client SET CivilClass = '4' WHERE CivilClass IN ('W')
GO
UPDATE dbo.Client SET CivilClass = NULL where CivilClass not in ('0', '1', '2', '3', '4') and civilclass is not null
GO

ENABLE TRIGGER dbo.Client_InsertUpdate ON dbo.Client
GO
