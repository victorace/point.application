/*
Run this script on:

NB1404.Main    -  This database will be modified

to synchronize it with:

NB1404.Dev

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 12.3.3.4490 from Red Gate Software Ltd at 13-6-2019 13:41:49

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION

PRINT(N'Update rows in [dbo].[FieldReceivedValueMapping]')
UPDATE [dbo].[FieldReceivedValueMapping] SET [TargetValue]='1' WHERE [FieldReceivedValueMappingID] = 6001
UPDATE [dbo].[FieldReceivedValueMapping] SET [TargetValue]='3' WHERE [FieldReceivedValueMappingID] = 6002
UPDATE [dbo].[FieldReceivedValueMapping] SET [TargetValue]='3' WHERE [FieldReceivedValueMappingID] = 6003
UPDATE [dbo].[FieldReceivedValueMapping] SET [TargetValue]='3' WHERE [FieldReceivedValueMappingID] = 6004
UPDATE [dbo].[FieldReceivedValueMapping] SET [TargetValue]='7' WHERE [FieldReceivedValueMappingID] = 6006
UPDATE [dbo].[FieldReceivedValueMapping] SET [TargetValue]='7' WHERE [FieldReceivedValueMappingID] = 6007
UPDATE [dbo].[FieldReceivedValueMapping] SET [TargetValue]='8' WHERE [FieldReceivedValueMappingID] = 6008
PRINT(N'Operation applied to 7 rows out of 7')

PRINT(N'Add rows to [dbo].[FieldReceivedValueMapping]')
SET IDENTITY_INSERT [dbo].[FieldReceivedValueMapping] ON
INSERT INTO [dbo].[FieldReceivedValueMapping] ([FieldReceivedValueMappingID], [FieldName], [SourceValue], [TargetValue]) VALUES (6000, 'HousingType', '0', '0')
INSERT INTO [dbo].[FieldReceivedValueMapping] ([FieldReceivedValueMappingID], [FieldName], [SourceValue], [TargetValue]) VALUES (6013, 'HousingType', '11', '9')
SET IDENTITY_INSERT [dbo].[FieldReceivedValueMapping] OFF
PRINT(N'Operation applied to 2 rows out of 2')
COMMIT TRANSACTION
GO
