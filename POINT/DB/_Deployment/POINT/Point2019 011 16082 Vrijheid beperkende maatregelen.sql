/*
Run this script on:

pointtest.verzorgdeoverdracht.nl.TXX-DB-POINT-R    -  This database will be modified

to synchronize it with:

NB1404.Point

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 12.3.3.4490 from Red Gate Software Ltd at 11-4-2019 11:35:50

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION

PRINT(N'Drop constraints from [dbo].[FlowFieldAttribute]')
ALTER TABLE [dbo].[FlowFieldAttribute] NOCHECK CONSTRAINT [FK_dbo.FlowFieldAttribute_dbo.FlowField_FlowFieldID]
ALTER TABLE [dbo].[FlowFieldAttribute] NOCHECK CONSTRAINT [FK_dbo.FlowFieldAttribute_dbo.FormType_FormTypeID]
ALTER TABLE [dbo].[FlowFieldAttribute] NOCHECK CONSTRAINT [FK_FlowFieldAttribute_FlowField]

PRINT(N'Drop constraints from [dbo].[FlowField]')
ALTER TABLE [dbo].[FlowField] NOCHECK CONSTRAINT [FK_FlowField_FlowFieldDataSource]

PRINT(N'Add rows to [dbo].[FlowField]')
INSERT INTO [dbo].[FlowField] ([FlowFieldID], [Name], [Label], [Description], [Type], [Format], [Regexp], [MaxLength], [UseInReadModel], [FlowFieldDataSourceID]) VALUES (2120, N'VrijheidsbeperkendeMaatregelen', N'Vrijheidsbeperkende maatregelen', NULL, 5, NULL, NULL, NULL, 0, 88)
INSERT INTO [dbo].[FlowField] ([FlowFieldID], [Name], [Label], [Description], [Type], [Format], [Regexp], [MaxLength], [UseInReadModel], [FlowFieldDataSourceID]) VALUES (2121, N'VrijheidsbeperkendeMaatregelenSoortTafelbladRolstoel', N'Tafelblad aan de rolstoel', NULL, 6, NULL, NULL, NULL, 0, NULL)
INSERT INTO [dbo].[FlowField] ([FlowFieldID], [Name], [Label], [Description], [Type], [Format], [Regexp], [MaxLength], [UseInReadModel], [FlowFieldDataSourceID]) VALUES (2122, N'VrijheidsbeperkendeMaatregelenSoortBokshandschoenen', N'Bokshandschoenen', NULL, 6, NULL, NULL, NULL, 0, NULL)
INSERT INTO [dbo].[FlowField] ([FlowFieldID], [Name], [Label], [Description], [Type], [Format], [Regexp], [MaxLength], [UseInReadModel], [FlowFieldDataSourceID]) VALUES (2123, N'VrijheidsbeperkendeMaatregelenSoortFixatie', N'Fixatie', NULL, 6, NULL, NULL, NULL, 0, NULL)
INSERT INTO [dbo].[FlowField] ([FlowFieldID], [Name], [Label], [Description], [Type], [Format], [Regexp], [MaxLength], [UseInReadModel], [FlowFieldDataSourceID]) VALUES (2124, N'VrijheidsbeperkendeMaatregelenSoortAnders', N'Anders', NULL, 6, NULL, NULL, NULL, 0, NULL)
INSERT INTO [dbo].[FlowField] ([FlowFieldID], [Name], [Label], [Description], [Type], [Format], [Regexp], [MaxLength], [UseInReadModel], [FlowFieldDataSourceID]) VALUES (2125, N'VrijheidsbeperkendeMaatregelenAnders', NULL, NULL, 1, NULL, NULL, 4000, 0, NULL)
PRINT(N'Operation applied to 6 rows out of 6')

PRINT(N'Add rows to [dbo].[FlowFieldAttribute]')
INSERT INTO [dbo].[FlowFieldAttribute] ([FlowFieldAttributeID], [FlowFieldID], [FormTypeID], [IsActive], [OriginFormTypeID], [OriginFlowFieldID], [OriginCopyToAll], [Required], [Visible], [ReadOnly], [SignalOnChange], [RequiredByFlowFieldID], [RequiredByFlowFieldValue]) VALUES (3118, 2120, 202, 1, NULL, NULL, 0, 0, 1, 0, 0, NULL, NULL)
INSERT INTO [dbo].[FlowFieldAttribute] ([FlowFieldAttributeID], [FlowFieldID], [FormTypeID], [IsActive], [OriginFormTypeID], [OriginFlowFieldID], [OriginCopyToAll], [Required], [Visible], [ReadOnly], [SignalOnChange], [RequiredByFlowFieldID], [RequiredByFlowFieldValue]) VALUES (3119, 2121, 202, 1, NULL, NULL, 0, 0, 1, 0, 0, NULL, NULL)
INSERT INTO [dbo].[FlowFieldAttribute] ([FlowFieldAttributeID], [FlowFieldID], [FormTypeID], [IsActive], [OriginFormTypeID], [OriginFlowFieldID], [OriginCopyToAll], [Required], [Visible], [ReadOnly], [SignalOnChange], [RequiredByFlowFieldID], [RequiredByFlowFieldValue]) VALUES (3120, 2122, 202, 1, NULL, NULL, 0, 0, 1, 0, 0, NULL, NULL)
INSERT INTO [dbo].[FlowFieldAttribute] ([FlowFieldAttributeID], [FlowFieldID], [FormTypeID], [IsActive], [OriginFormTypeID], [OriginFlowFieldID], [OriginCopyToAll], [Required], [Visible], [ReadOnly], [SignalOnChange], [RequiredByFlowFieldID], [RequiredByFlowFieldValue]) VALUES (3121, 2123, 202, 1, NULL, NULL, 0, 0, 1, 0, 0, NULL, NULL)
INSERT INTO [dbo].[FlowFieldAttribute] ([FlowFieldAttributeID], [FlowFieldID], [FormTypeID], [IsActive], [OriginFormTypeID], [OriginFlowFieldID], [OriginCopyToAll], [Required], [Visible], [ReadOnly], [SignalOnChange], [RequiredByFlowFieldID], [RequiredByFlowFieldValue]) VALUES (3122, 2124, 202, 1, NULL, NULL, 0, 0, 1, 0, 0, NULL, NULL)
INSERT INTO [dbo].[FlowFieldAttribute] ([FlowFieldAttributeID], [FlowFieldID], [FormTypeID], [IsActive], [OriginFormTypeID], [OriginFlowFieldID], [OriginCopyToAll], [Required], [Visible], [ReadOnly], [SignalOnChange], [RequiredByFlowFieldID], [RequiredByFlowFieldValue]) VALUES (3123, 2125, 202, 1, NULL, NULL, 0, 0, 1, 0, 0, NULL, NULL)
PRINT(N'Operation applied to 6 rows out of 6')

PRINT(N'Add constraints to [dbo].[FlowFieldAttribute]')
ALTER TABLE [dbo].[FlowFieldAttribute] WITH CHECK CHECK CONSTRAINT [FK_dbo.FlowFieldAttribute_dbo.FlowField_FlowFieldID]
ALTER TABLE [dbo].[FlowFieldAttribute] WITH CHECK CHECK CONSTRAINT [FK_dbo.FlowFieldAttribute_dbo.FormType_FormTypeID]
ALTER TABLE [dbo].[FlowFieldAttribute] WITH CHECK CHECK CONSTRAINT [FK_FlowFieldAttribute_FlowField]

PRINT(N'Add constraints to [dbo].[FlowField]')
ALTER TABLE [dbo].[FlowField] WITH CHECK CHECK CONSTRAINT [FK_FlowField_FlowFieldDataSource]
COMMIT TRANSACTION
GO
