/*
Run this script on:

pointreview.verzorgdeoverdracht.nl.TXX-DB-POINT-R    -  This database will be modified

to synchronize it with:

..PointDev

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 12.3.3.4490 from Red Gate Software Ltd at 14/05/2019 12:54:27

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION

PRINT(N'Drop constraints from [dbo].[PhaseDefinitionRights]')
ALTER TABLE [dbo].[PhaseDefinitionRights] NOCHECK CONSTRAINT [FK_PhaseDefinitionRights_DepartmentType]
ALTER TABLE [dbo].[PhaseDefinitionRights] NOCHECK CONSTRAINT [FK_PhaseDefinitionRights_OrganizationType]
ALTER TABLE [dbo].[PhaseDefinitionRights] NOCHECK CONSTRAINT [FK_PhaseDefinitionRights_PhaseDefinition]

PRINT(N'Drop constraint FK_SignaleringTarget_SignaleringTrigger from [dbo].[SignaleringTarget]')
ALTER TABLE [dbo].[SignaleringTarget] NOCHECK CONSTRAINT [FK_SignaleringTarget_SignaleringTrigger]

PRINT(N'Drop constraints from [dbo].[PhaseDefinition]')
ALTER TABLE [dbo].[PhaseDefinition] NOCHECK CONSTRAINT [FK_dbo.PhaseDefinition_dbo.FlowDefinition_FlowDefinitionID]
ALTER TABLE [dbo].[PhaseDefinition] NOCHECK CONSTRAINT [FK_dbo.PhaseDefinition_dbo.OrganizationType_OrganizationTypeID]

PRINT(N'Drop constraint FK_dbo.FlowPhaseAttribute_dbo.PhaseDefinition_PhaseDefinitionID from [dbo].[FlowPhaseAttribute]')
ALTER TABLE [dbo].[FlowPhaseAttribute] NOCHECK CONSTRAINT [FK_dbo.FlowPhaseAttribute_dbo.PhaseDefinition_PhaseDefinitionID]

PRINT(N'Drop constraint FK_dbo.GeneralActionPhase_dbo.PhaseDefinition_PhaseDefinitionID from [dbo].[GeneralActionPhase]')
ALTER TABLE [dbo].[GeneralActionPhase] NOCHECK CONSTRAINT [FK_dbo.GeneralActionPhase_dbo.PhaseDefinition_PhaseDefinitionID]

PRINT(N'Drop constraint FK_PhaseDefinitionNavigation_PhaseDefinition from [dbo].[PhaseDefinitionNavigation]')
ALTER TABLE [dbo].[PhaseDefinitionNavigation] NOCHECK CONSTRAINT [FK_PhaseDefinitionNavigation_PhaseDefinition]

PRINT(N'Drop constraint FK_PhaseDefinitionNavigation_TargetPhaseDefinition from [dbo].[PhaseDefinitionNavigation]')
ALTER TABLE [dbo].[PhaseDefinitionNavigation] NOCHECK CONSTRAINT [FK_PhaseDefinitionNavigation_TargetPhaseDefinition]

PRINT(N'Add row to [dbo].[PhaseDefinition]')
SET IDENTITY_INSERT [dbo].[PhaseDefinition] ON
INSERT INTO [dbo].[PhaseDefinition] ([PhaseDefinitionID], [FlowDefinitionID], [Phase], [PhaseName], [OrganizationTypeID], [BeginFlow], [EndFlow], [Description], [Executor], [Subcategory], [CodeGroup], [HasPrintButton], [Maincategory], [OrderNumber], [ReadOnlyOnDone], [RedirectToPhaseDefinitionIDOnNext], [AccessGroup], [ShowReadOnlyAndEmpty], [FormTypeID], [CanResend], [DashboardMenuItemType], [MenuItemType], [FlowHandling]) VALUES (131, 5, -1.0, N'Communicatie journaal', 1, 0, 0, N'Raadplegen van en toevoegen aan het communicatiejournaal', N'ZH', NULL, NULL, NULL, N'Proces', 83, NULL, NULL, NULL, 0, 223, 0, 0, 0, NULL)
SET IDENTITY_INSERT [dbo].[PhaseDefinition] OFF

PRINT(N'Add row to [dbo].[SignaleringTrigger]')
SET IDENTITY_INSERT [dbo].[SignaleringTrigger] ON
INSERT INTO [dbo].[SignaleringTrigger] ([SignaleringTriggerID], [FlowDefinitionID], [FormTypeID], [GeneralActionID], [Name], [Description], [ShowPrompt], [MinActivePhase], [MaxActivePhase]) VALUES (54, 5, 223, NULL, N'Communicatiejournaal regel is toegevoegd/aangepast', NULL, 0, 3, 4)
SET IDENTITY_INSERT [dbo].[SignaleringTrigger] OFF

PRINT(N'Add rows to [dbo].[PhaseDefinitionRights]')
SET IDENTITY_INSERT [dbo].[PhaseDefinitionRights] ON
INSERT INTO [dbo].[PhaseDefinitionRights] ([PhaseDefinitionRightsID], [PhaseDefinitionID], [OrganizationTypeID], [DepartmentTypeID], [MaxRights], [FlowDirection]) VALUES (7217, 131, NULL, 1, 'X', 2)
INSERT INTO [dbo].[PhaseDefinitionRights] ([PhaseDefinitionRightsID], [PhaseDefinitionID], [OrganizationTypeID], [DepartmentTypeID], [MaxRights], [FlowDirection]) VALUES (7218, 131, 1, 2, 'X', 2)
SET IDENTITY_INSERT [dbo].[PhaseDefinitionRights] OFF
PRINT(N'Operation applied to 2 rows out of 2')

PRINT(N'Add constraints to [dbo].[PhaseDefinitionRights]')
ALTER TABLE [dbo].[PhaseDefinitionRights] WITH CHECK CHECK CONSTRAINT [FK_PhaseDefinitionRights_DepartmentType]
ALTER TABLE [dbo].[PhaseDefinitionRights] WITH CHECK CHECK CONSTRAINT [FK_PhaseDefinitionRights_OrganizationType]
ALTER TABLE [dbo].[PhaseDefinitionRights] WITH CHECK CHECK CONSTRAINT [FK_PhaseDefinitionRights_PhaseDefinition]
ALTER TABLE [dbo].[SignaleringTarget] WITH CHECK CHECK CONSTRAINT [FK_SignaleringTarget_SignaleringTrigger]

PRINT(N'Add constraints to [dbo].[PhaseDefinition]')
ALTER TABLE [dbo].[PhaseDefinition] WITH CHECK CHECK CONSTRAINT [FK_dbo.PhaseDefinition_dbo.FlowDefinition_FlowDefinitionID]
ALTER TABLE [dbo].[PhaseDefinition] WITH CHECK CHECK CONSTRAINT [FK_dbo.PhaseDefinition_dbo.OrganizationType_OrganizationTypeID]
ALTER TABLE [dbo].[FlowPhaseAttribute] WITH CHECK CHECK CONSTRAINT [FK_dbo.FlowPhaseAttribute_dbo.PhaseDefinition_PhaseDefinitionID]
ALTER TABLE [dbo].[GeneralActionPhase] WITH CHECK CHECK CONSTRAINT [FK_dbo.GeneralActionPhase_dbo.PhaseDefinition_PhaseDefinitionID]
ALTER TABLE [dbo].[PhaseDefinitionNavigation] WITH CHECK CHECK CONSTRAINT [FK_PhaseDefinitionNavigation_PhaseDefinition]
ALTER TABLE [dbo].[PhaseDefinitionNavigation] WITH CHECK CHECK CONSTRAINT [FK_PhaseDefinitionNavigation_TargetPhaseDefinition]
COMMIT TRANSACTION
GO
