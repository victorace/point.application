PRINT 'Migrate Projects to tags'

Delete from FlowInstanceTag

Insert into FlowInstanceTag (FlowInstanceID, Tag)

SELECT fip.FlowInstanceID, p.Name FROM dbo.FlowInstanceReportValuesProjects fip
INNER JOIN dbo.OrganizationProject p ON p.OrganizationProjectID = fip.OrganizationProjectID

Group by fip.FlowInstanceID, p.Name
order by fip.FlowInstanceID, p.Name

PRINT 'Done'