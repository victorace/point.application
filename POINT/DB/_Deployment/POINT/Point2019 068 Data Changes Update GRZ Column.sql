PRINT 'Update ReadModel kolom GRZ'

UPDATE SearchUIFieldConfiguration SET Field_Name = 'LastGrzFormDateTime', Column_FullName = 'GRZ', Column_HeaderName = 'GRZ' WHERE Column_FullName = 'MSVT'

UPDATE dbo.FlowInstanceSearchValues SET LastGrzFormDateTime = NULL where LastGrzFormDateTime IS NOT NULL

UPDATE s SET s.LastGrzFormDateTime = f.UpdateDate
FROM dbo.FlowInstanceSearchValues s
INNER JOIN dbo.FormSetVersion f ON f.TransferID = s.TransferID
WHERE f.Deleted = 0 AND f.FormTypeID = 216 and f.UpdateDate is not NULL

--Some have no updatedate....
UPDATE s SET s.LastGrzFormDateTime = f.CreateDate
FROM dbo.FlowInstanceSearchValues s
INNER JOIN dbo.FormSetVersion f ON f.TransferID = s.TransferID
WHERE f.Deleted = 0 AND f.FormTypeID = 216 and f.UpdateDate is NULL

PRINT 'Done'