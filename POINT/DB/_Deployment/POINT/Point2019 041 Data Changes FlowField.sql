/*
Run this script on:

pointreview.verzorgdeoverdracht.nl.TXX-DB-POINT-R    -  This database will be modified

to synchronize it with:

LINKASSIST-NB20.PointDB

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 12.3.3.4490 from Red Gate Software Ltd at 5/14/2019 1:41:32 PM

*/
		
SET XACT_ABORT ON
GO
SET ARITHABORT ON
GO
BEGIN TRANSACTION

PRINT(N'Update 1 row in [dbo].[FlowField]')
UPDATE [dbo].[FlowField] SET [MaxLength]=12 WHERE [FlowFieldID] = 1665
COMMIT TRANSACTION
GO
