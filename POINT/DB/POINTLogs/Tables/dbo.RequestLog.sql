CREATE TABLE [dbo].[RequestLog]
(
[RequestLogID] [int] NOT NULL IDENTITY(1, 1),
[TransferID] [int] NULL,
[EmployeeID] [int] NULL,
[RoleName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Timestamp] [datetime] NOT NULL,
[RequestActionType] [int] NOT NULL CONSTRAINT [DF_RequestLog_RequestActionType] DEFAULT ((0)),
[FormTypeID] [int] NULL,
[GeneralActionID] [int] NULL,
[Method] [varchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[Url] [varchar] (256) COLLATE Latin1_General_CI_AS NOT NULL,
[Querystring] [varchar] (2048) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RequestLog] ADD CONSTRAINT [PK_RequestLog] PRIMARY KEY CLUSTERED  ([RequestLogID]) ON [PRIMARY]
GO
