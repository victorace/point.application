CREATE TABLE [dbo].[EntityLog]
(
[EntityLogID] [int] NOT NULL IDENTITY(1, 1),
[EntityLogSetID] [uniqueidentifier] NOT NULL,
[ChangeType] [int] NOT NULL CONSTRAINT [DF_EntityLog_ChangeType] DEFAULT ((0)),
[TableName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PrimaryKeys] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FieldName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OldValue] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NewValue] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[EntityLog] ADD CONSTRAINT [pk_entitylog] PRIMARY KEY CLUSTERED  ([EntityLogID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EntityLog] ADD CONSTRAINT [FK_EntityLog_EntityLogSet] FOREIGN KEY ([EntityLogSetID]) REFERENCES [dbo].[EntityLogSet] ([EntityLogSetID]) ON DELETE CASCADE
GO
