CREATE TABLE [dbo].[LogRead]
(
[LogReadID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NULL,
[TransferID] [int] NULL,
[Timestamp] [datetime] NULL,
[EmployeeID] [int] NULL,
[Action] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ScreenID] [int] NULL,
[RoleName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[RoleID] [uniqueidentifier] NULL,
[Url] [varchar] (2048) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Querystring] [varchar] (2048) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[LogRead] ADD CONSTRAINT [PK_LogRead] PRIMARY KEY CLUSTERED  ([LogReadID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_LogRead_TimeStamp_EmployeeID] ON [dbo].[LogRead] ([Timestamp], [EmployeeID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_LogRead_TimeStamp_TransferID] ON [dbo].[LogRead] ([Timestamp], [TransferID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_LogRead_TransferID] ON [dbo].[LogRead] ([TransferID]) ON [PRIMARY]
GO
