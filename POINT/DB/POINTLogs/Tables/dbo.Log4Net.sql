CREATE TABLE [dbo].[Log4Net]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Date] [datetime] NOT NULL,
[Thread] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Level] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Logger] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Message] [varchar] (MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Exception] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Url] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QueryString] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PostData] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Cookies] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UserAgent] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsAjaxRequest] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmployeeID] [int] NULL,
[EmployeeUserName] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MachineName] [varchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WindowsIdentity] [varchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransferID] [int] NULL,
[Stacktrace] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ErrorNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClassName] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MethodName] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LineNumber] [int] NULL,
[ErrorCode] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
