CREATE TABLE [dbo].[LogQuery]
(
[LogQueryID] [int] NOT NULL IDENTITY(1, 1),
[Query] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StartTime] [datetime] NOT NULL,
[EndTime] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[LogQuery] ADD CONSTRAINT [PK_LogQuery] PRIMARY KEY CLUSTERED  ([LogQueryID]) ON [PRIMARY]
GO
