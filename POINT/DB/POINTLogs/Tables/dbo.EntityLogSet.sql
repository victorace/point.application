CREATE TABLE [dbo].[EntityLogSet]
(
[EntityLogSetID] [uniqueidentifier] NOT NULL,
[EmployeeID] [int] NOT NULL CONSTRAINT [DF_EntityLogSet_EmployeeID] DEFAULT ((0)),
[ScreenID] [int] NOT NULL CONSTRAINT [DF_EntityLogSet_ScreenID] DEFAULT ((0)),
[TimeStamp] [datetime] NOT NULL CONSTRAINT [DF_EntityLogSet_TimeStamp] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EntityLogSet] ADD CONSTRAINT [PK_EntityLogSet] PRIMARY KEY CLUSTERED  ([EntityLogSetID]) ON [PRIMARY]
GO
