IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'Point')
CREATE LOGIN [Point] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [Point] FOR LOGIN [Point]
GO
