CREATE TABLE [dbo].[Organization]
(
[OrganizationID] [int] NOT NULL,
[SSO] [bit] NOT NULL CONSTRAINT [DF_OrganizationConfiguration_SSO] DEFAULT ((0)),
[SSOKey] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[SSOHashType] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[CreateUserViaSSO] [bit] NOT NULL CONSTRAINT [DF_OrganizationConfiguration_CreateUserViaSSO] DEFAULT ((0)),
[AGB] [varchar] (20) COLLATE Latin1_General_CI_AS NULL,
[Inactive] [bit] NOT NULL CONSTRAINT [DF_Organization_Inactive] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Organization] ADD CONSTRAINT [PK_Organization] PRIMARY KEY CLUSTERED  ([OrganizationID]) ON [PRIMARY]
GO
