CREATE TABLE [dbo].[Department]
(
[DepartmentID] [int] NOT NULL,
[OrganizationID] [int] NOT NULL,
[LocationID] [int] NOT NULL CONSTRAINT [DF_Department_LocationID] DEFAULT ((-1)),
[AGB] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Inactive] [bit] NOT NULL CONSTRAINT [DF_Department_Inactive] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Department] ADD CONSTRAINT [PK_Department] PRIMARY KEY CLUSTERED  ([DepartmentID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Department] ADD CONSTRAINT [FK_Department_Organization] FOREIGN KEY ([OrganizationID]) REFERENCES [dbo].[Organization] ([OrganizationID])
GO
