SET IDENTITY_INSERT [dbo].[ZorgAdviesItemGroup] ON
INSERT INTO [dbo].[ZorgAdviesItemGroup] ([ZorgAdviesItemGroupID], [OrderNumber], [GroupName], [ParentZorgAdviesItemGroupID]) VALUES (30, 0, N'Afspraken', 2)
SET IDENTITY_INSERT [dbo].[ZorgAdviesItemGroup] OFF
SET IDENTITY_INSERT [dbo].[ZorgAdviesItemGroup] ON
INSERT INTO [dbo].[ZorgAdviesItemGroup] ([ZorgAdviesItemGroupID], [OrderNumber], [GroupName], [ParentZorgAdviesItemGroupID]) VALUES (2, 0, N'Algemeen', NULL)
INSERT INTO [dbo].[ZorgAdviesItemGroup] ([ZorgAdviesItemGroupID], [OrderNumber], [GroupName], [ParentZorgAdviesItemGroupID]) VALUES (3, 1, N'Lichamelijk welbevinden en Gezondheid', NULL)
INSERT INTO [dbo].[ZorgAdviesItemGroup] ([ZorgAdviesItemGroupID], [OrderNumber], [GroupName], [ParentZorgAdviesItemGroupID]) VALUES (7, 0, N'Voeding', 3)
INSERT INTO [dbo].[ZorgAdviesItemGroup] ([ZorgAdviesItemGroupID], [OrderNumber], [GroupName], [ParentZorgAdviesItemGroupID]) VALUES (8, 1, N'Huid', 3)
INSERT INTO [dbo].[ZorgAdviesItemGroup] ([ZorgAdviesItemGroupID], [OrderNumber], [GroupName], [ParentZorgAdviesItemGroupID]) VALUES (9, 2, N'Toediening systemen / Overige Verpleegtechnische handelingen', 3)
INSERT INTO [dbo].[ZorgAdviesItemGroup] ([ZorgAdviesItemGroupID], [OrderNumber], [GroupName], [ParentZorgAdviesItemGroupID]) VALUES (10, 3, N'Uitscheiding', 3)
INSERT INTO [dbo].[ZorgAdviesItemGroup] ([ZorgAdviesItemGroupID], [OrderNumber], [GroupName], [ParentZorgAdviesItemGroupID]) VALUES (11, 4, N'Mobiliteit', 3)
INSERT INTO [dbo].[ZorgAdviesItemGroup] ([ZorgAdviesItemGroupID], [OrderNumber], [GroupName], [ParentZorgAdviesItemGroupID]) VALUES (12, 5, N'Wassen', 3)
INSERT INTO [dbo].[ZorgAdviesItemGroup] ([ZorgAdviesItemGroupID], [OrderNumber], [GroupName], [ParentZorgAdviesItemGroupID]) VALUES (13, 6, N'Mond verzorging', 3)
INSERT INTO [dbo].[ZorgAdviesItemGroup] ([ZorgAdviesItemGroupID], [OrderNumber], [GroupName], [ParentZorgAdviesItemGroupID]) VALUES (14, 7, N'Aan- en uitkleden', 3)
INSERT INTO [dbo].[ZorgAdviesItemGroup] ([ZorgAdviesItemGroupID], [OrderNumber], [GroupName], [ParentZorgAdviesItemGroupID]) VALUES (15, 8, N'Toiletgang', 3)
INSERT INTO [dbo].[ZorgAdviesItemGroup] ([ZorgAdviesItemGroupID], [OrderNumber], [GroupName], [ParentZorgAdviesItemGroupID]) VALUES (16, 9, N'Eten/drinken', 3)
INSERT INTO [dbo].[ZorgAdviesItemGroup] ([ZorgAdviesItemGroupID], [OrderNumber], [GroupName], [ParentZorgAdviesItemGroupID]) VALUES (17, 10, N'Slaap', 3)
INSERT INTO [dbo].[ZorgAdviesItemGroup] ([ZorgAdviesItemGroupID], [OrderNumber], [GroupName], [ParentZorgAdviesItemGroupID]) VALUES (18, 11, N'Zintuigen', 3)
INSERT INTO [dbo].[ZorgAdviesItemGroup] ([ZorgAdviesItemGroupID], [OrderNumber], [GroupName], [ParentZorgAdviesItemGroupID]) VALUES (19, 12, N'Pijn', 3)
INSERT INTO [dbo].[ZorgAdviesItemGroup] ([ZorgAdviesItemGroupID], [OrderNumber], [GroupName], [ParentZorgAdviesItemGroupID]) VALUES (20, 13, N'Seksualiteit en voortplanting', 3)
SET IDENTITY_INSERT [dbo].[ZorgAdviesItemGroup] OFF
SET IDENTITY_INSERT [dbo].[ZorgAdviesItemGroup] ON
INSERT INTO [dbo].[ZorgAdviesItemGroup] ([ZorgAdviesItemGroupID], [OrderNumber], [GroupName], [ParentZorgAdviesItemGroupID]) VALUES (5, 1, N'Medicatie (hulp bij toediening)', 2)
INSERT INTO [dbo].[ZorgAdviesItemGroup] ([ZorgAdviesItemGroupID], [OrderNumber], [GroupName], [ParentZorgAdviesItemGroupID]) VALUES (6, 2, N'Controle Lichaamsfuncties', 2)
SET IDENTITY_INSERT [dbo].[ZorgAdviesItemGroup] OFF
