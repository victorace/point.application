SET IDENTITY_INSERT [dbo].[ReportDefinition] ON
INSERT INTO [dbo].[ReportDefinition] ([ReportDefinitionID], [FileName], [ExcelFriendlyFileName], [ReportName], [ReportActionName], [FlowDirection], [Category], [Description], [FilterPartial], [NeedsOrganizationID], [Visible]) VALUES (3, N'InStroomAantallenIndicerend.rdlc', N'DumpStroomReportData.rdlc', N'Aantallen per indicerende organisatie', N'/Reporting/InstroomReportIndicerend', 1, N'Ontvangen', N'Dit rapport biedt inzicht in de aantallen per locatie, afdeling en specialisme welke de indicerende organisatie heeft ontvangen.', N'_DefaultFilter', 0, 1)
INSERT INTO [dbo].[ReportDefinition] ([ReportDefinitionID], [FileName], [ExcelFriendlyFileName], [ReportName], [ReportActionName], [FlowDirection], [Category], [Description], [FilterPartial], [NeedsOrganizationID], [Visible]) VALUES (6, N'UitstroomAantallenPerMaand.rdlc', N'DumpStroomReportData.rdlc', N'Aantallen per maand', N'/Reporting/UitStroomAantallenPerMaand', 0, N'Verstuurd', N'Dit rapport biedt inzicht in de aantallen per maand welke de organisatie heeft verstuurd.', N'_DefaultFilter', 0, 1)
INSERT INTO [dbo].[ReportDefinition] ([ReportDefinitionID], [FileName], [ExcelFriendlyFileName], [ReportName], [ReportActionName], [FlowDirection], [Category], [Description], [FilterPartial], [NeedsOrganizationID], [Visible]) VALUES (8, N'InStroomAantallenPerMaand.rdlc', N'DumpStroomReportData.rdlc', N'Aantallen per maand', N'/Reporting/InStroomAantallenPerMaand', 1, N'Ontvangen', N'Dit rapport biedt inzicht in de aantallen per maand welke de organisatie heeft ontvangen.', N'_DefaultFilter', 0, 1)
INSERT INTO [dbo].[ReportDefinition] ([ReportDefinitionID], [FileName], [ExcelFriendlyFileName], [ReportName], [ReportActionName], [FlowDirection], [Category], [Description], [FilterPartial], [NeedsOrganizationID], [Visible]) VALUES (9, N'UitStroomOntslagBehaald.rdlc', N'DumpStroomTrendData.rdlc', N'Aantallen ontslagdatum behaald', N'/Reporting/UitStroomOntslagDatumBehaaldReport', 0, N'Verstuurd', N'Dit rapport is gebaseerd op de gewenste ontslagdatum versus de gerealiseerde ontslagdatum.

Indien de gerealiseerde ontslagdatum gelijk (of kleiner) is dan de gewenste ontslagdatum dan telt deze als "behaald".
 
Indien 1 van deze datums niet is opgegeven dan telt deze niet mee.', N'_DefaultFilter', 0, 1)
INSERT INTO [dbo].[ReportDefinition] ([ReportDefinitionID], [FileName], [ExcelFriendlyFileName], [ReportName], [ReportActionName], [FlowDirection], [Category], [Description], [FilterPartial], [NeedsOrganizationID], [Visible]) VALUES (10, N'UitStroomProcestijden.rdlc', N'DumpStroomTrendData.rdlc', N'Procestijden', N'/Reporting/UitStroomProcesTijdenReport', 0, N'Verstuurd', N'Het inzichtelijk maken van de gemiddelde doorlooptijd per fase, per maand naar zorgtype.

<b>Fase uitleg</b>
 - Pre transfer fase: Verschil tussen [Aanvraag formulier datum opname] en [Aanvraagformulier datum verzonden]
 - Transfer fase: Verschil [Aanvraagformulier datum verzonden] en [Datum aanvraag bij de ontvangende organisatie]
 - Ontvang en besluit fase: Verschil [Datum aanvraag bij de ontvangende organisatie] en [Datum besluit van de ontvangende organisatie]
 - Overdracht fase: [Datum besluit van de ontvangende organisatie] en [Gerealiseerde ontslagdatum]
 
<i>Dossiers met fases die meer dan 100 dagen uit elkaar liggen niet meegerekend, dit om een meer realistisch beeld te krijgen van daadwerkelijke doorlooptijden.</i>', N'_DefaultFilter', 0, 1)
INSERT INTO [dbo].[ReportDefinition] ([ReportDefinitionID], [FileName], [ExcelFriendlyFileName], [ReportName], [ReportActionName], [FlowDirection], [Category], [Description], [FilterPartial], [NeedsOrganizationID], [Visible]) VALUES (11, N'UitStroomZorgType.rdlc', N'DumpStroomReportData.rdlc', N'Aantallen per zorgtype', N'/Reporting/UitStroomZorgType', 0, N'Verstuurd', N'Dit rapport biedt inzicht in de aantallen per zorgtype welke de organisatie heeft verstuurd.', N'_DefaultFilter', 0, 1)
INSERT INTO [dbo].[ReportDefinition] ([ReportDefinitionID], [FileName], [ExcelFriendlyFileName], [ReportName], [ReportActionName], [FlowDirection], [Category], [Description], [FilterPartial], [NeedsOrganizationID], [Visible]) VALUES (12, N'UitStroomTop10.rdlc', N'', N'Top 10 ontvangende organisaties', N'/Reporting/UitStroomTop10Report', 0, N'Verstuurd', N'Dit rapport laat de 10 organisaties zien waar de meeste dossier naar zijn verstuurd.', N'_DefaultFilter', 0, 1)
INSERT INTO [dbo].[ReportDefinition] ([ReportDefinitionID], [FileName], [ExcelFriendlyFileName], [ReportName], [ReportActionName], [FlowDirection], [Category], [Description], [FilterPartial], [NeedsOrganizationID], [Visible]) VALUES (13, N'UitStroomVerkeerdeBedDagen.rdlc', N'DumpStroomTrendData.rdlc', N'Som van verkeerde bed dagen', N'/Reporting/UitStroomVKBReport', 0, N'Verstuurd', N'Het inzichtelijk maken van de totaal aantal [Verkeerde Bed Dagen] per afdeling en per maand

<i><b>Verkeerde Bed Dagen</b>
Verschil tussen [Aanvraag formulier gewenste ontslagdatum], [Gerealiseerde ontslagdatum]

Dossiers met fases die meer dan 100 dagen uit elkaar liggen niet meegerekend, dit om een meer realistisch beeld te krijgen van daadwerkelijke doorlooptijden.</i>', N'_DefaultFilter', 0, 1)
INSERT INTO [dbo].[ReportDefinition] ([ReportDefinitionID], [FileName], [ExcelFriendlyFileName], [ReportName], [ReportActionName], [FlowDirection], [Category], [Description], [FilterPartial], [NeedsOrganizationID], [Visible]) VALUES (15, N'InStroomZorgType.rdlc', N'DumpStroomReportData.rdlc', N'Aantallen per zorgtype', N'/Reporting/InStroomZorgType', 1, N'Ontvangen', N'Het inzichtelijk maken van de gemiddelde doorlooptijd per fase, per maand naar zorgtype.

Fase uitleg:
 - Pre transfer fase: Verschil tussen [Aanvraag formulier datum opname] en [Aanvraagformulier datum verzonden]
 - Transfer fase: Verschil [Aanvraagformulier datum verzonden] en [Datum aanvraag bij de ontvangende organisatie]
 - Ontvang en besluit fase: Verschil [Datum aanvraag bij de ontvangende organisatie] en [Datum besluit van de ontvangende organisatie]
 - Overdracht fase: [Datum besluit van de ontvangende organisatie] en [Gerealiseerde ontslagdatum]', N'_DefaultFilter', 0, 1)
INSERT INTO [dbo].[ReportDefinition] ([ReportDefinitionID], [FileName], [ExcelFriendlyFileName], [ReportName], [ReportActionName], [FlowDirection], [Category], [Description], [FilterPartial], [NeedsOrganizationID], [Visible]) VALUES (16, N'InStroomTop10.rdlc', N'', N'Top 10 versturende organisaties', N'/Reporting/InStroomTop10Report', 1, N'Ontvangen', N'Dit rapport laat de 10 organisaties zien waar de meeste dossier van zijn ontvangen.', N'_DefaultFilter', 0, 1)
INSERT INTO [dbo].[ReportDefinition] ([ReportDefinitionID], [FileName], [ExcelFriendlyFileName], [ReportName], [ReportActionName], [FlowDirection], [Category], [Description], [FilterPartial], [NeedsOrganizationID], [Visible]) VALUES (17, N'UitStroomOntslagDatumGepasseerd.rdlc', N'DumpDischargeDatePassed.rdlc', N'Gewenste ontslagdatum gepasseerd', N'/Reporting/UitStroomOntslagDatumGepasseerd', 0, N'Verstuurd', N'Dit rapport geeft een overzicht van dossiers waarvan de gewenste ontslagdatum in het verleden ligt en (nog) geen gerealiseerde ontslagdatum is opgegeven. 

<b>Het overzicht toont:</b>
 - Versturende organisatie/locatie/afdeling
 - Patient
 - Gewenste ontslagdatum
 - Totaal aantal werk- en kalenderdagen verstreken

<i>De gewenste ontslagdatum kan ingevuld worden in het [Aanvraag formulier].</i>', N'_DefaultFilter', 0, 1)
INSERT INTO [dbo].[ReportDefinition] ([ReportDefinitionID], [FileName], [ExcelFriendlyFileName], [ReportName], [ReportActionName], [FlowDirection], [Category], [Description], [FilterPartial], [NeedsOrganizationID], [Visible]) VALUES (21, N'InStroomProcestijden.rdlc', N'DumpStroomTrendData.rdlc', N'Procestijden', N'/Reporting/InStroomProcesTijdenReport', 1, N'Ontvangen', N'Het inzichtelijk maken van de gemiddelde doorlooptijd per fase, per maand naar zorgtype.

<b>Fase uitleg</b>
 - Pre transfer fase: Verschil tussen [Aanvraag formulier datum opname] en [Aanvraagformulier datum verzonden]
 - Transfer fase: Verschil [Aanvraagformulier datum verzonden] en [Datum aanvraag bij de ontvangende organisatie]
 - Ontvang en besluit fase: Verschil [Datum aanvraag bij de ontvangende organisatie] en [Datum besluit van de ontvangende organisatie]
 - Overdracht fase: [Datum besluit van de ontvangende organisatie] en [Gerealiseerde ontslagdatum]
 
<i>Dossiers met fases die meer dan 100 dagen uit elkaar liggen niet meegerekend, dit om een meer realistisch beeld te krijgen van daadwerkelijke doorlooptijden.</i>', N'_DefaultFilter', 0, 1)
INSERT INTO [dbo].[ReportDefinition] ([ReportDefinitionID], [FileName], [ExcelFriendlyFileName], [ReportName], [ReportActionName], [FlowDirection], [Category], [Description], [FilterPartial], [NeedsOrganizationID], [Visible]) VALUES (23, N'UitStroomStuurinfoZH.rdlc', N'DumpStuurinfo.rdlc', N'Stuurinfo ZH', N'/Reporting/UitStroomStuurInfoZH', 0, N'Verstuurd', N'Dit rapport biedt inzicht in de bewerkingsduur van dossiers en kan gebruikt worden om bij te sturen in het proces
 
 <b>Velden:</b>
 - Versturende organisatie, Patient
 - Datum aanmaak, Datum opname
 - Duur onderbroken
 - Gewenste/Gerealiseerde ontslagdatum
 - Optimale opnameduur (gewenste ontslagdatum – datum opname)
 - Aanvraag verstuurd naar TP
 - Beschikbaar gestelde dagen voor ontslag (Aanvraag verstuurd naar TP – Gewenste ontslagdatum)
 - Bewerkingsduur transferpunt (Verstuurd naar ZA – Aanvraag verstuurd naar TP)
 - Verstuurd naar indicerende organisatie, in behandeling genomen, besluit naar TP
 - Bewerkingduur Indicerende (Verstuurd naar indicerende organisatie – Indicerende besluit naar TP)
 - Verstuurd naar ZA, ZA In behandeling genomen, ZA Besluit Ja/Nee
 - ZA Patient wordt in zorg genomen
 - Bewerkingduur ZA In beh. genomen (ZA In behandeling genomen – Verstuurd naar ZA)
 - Bewerkingduur ZA besluit (ZA Besluit Ja/Nee – ZA In behandeling genomen)
 - Bewerkingduur overdracht (Gerealiseerde ontslagdatum – ZA Patient wordt in zorg genomen)
 - Gerealiseerde opnameduur (Gerealiseerde ontslagdatum – Datum opname)
 - Verkeerde bed (week)dagen (Gerealiseerde ontslagdatum – Gewenste ontslagdatum)
 - Nazorg categorie/type
 - Ontvangende/indicerende organisatie
 
 <i>Alle looptijden zijn in dagen vermeld</i>', N'_DefaultFilter', 0, 1)
INSERT INTO [dbo].[ReportDefinition] ([ReportDefinitionID], [FileName], [ExcelFriendlyFileName], [ReportName], [ReportActionName], [FlowDirection], [Category], [Description], [FilterPartial], [NeedsOrganizationID], [Visible]) VALUES (24, N'UitStroomDICA.rdlc', N'UitStroomDICA.rdlc', N'CVA DICA', N'/Reporting/UitStroomDICA', 0, N'Overig', N'Dit genereert een CVA DICA rapport', N'_DefaultFilter', 0, 1)
INSERT INTO [dbo].[ReportDefinition] ([ReportDefinitionID], [FileName], [ExcelFriendlyFileName], [ReportName], [ReportActionName], [FlowDirection], [Category], [Description], [FilterPartial], [NeedsOrganizationID], [Visible]) VALUES (25, N'UitStroomKPIInBehandeling.rdlc', N'', N'KPI Snelheid in behandeling nemen ontvanger', N'/Reporting/UitStroomKPIInBehandelingReport', 0, N'Verstuurd', N'Dit rapport geeft inzicht in de mate waarin de norm voor het “In behandeling nemen” van het dossier is behaald. 
Bij de filtercriteria kan buiten de andere filtercriteria de normtijd (in uren) en norm (in %) worden ingegeven.

<i>Dossiers met fases die meer dan 100 dagen uit elkaar liggen niet meegerekend, dit om een meer realistisch beeld te krijgen van daadwerkelijke doorlooptijden.</i>', N'_KPIFilter', 0, 1)
INSERT INTO [dbo].[ReportDefinition] ([ReportDefinitionID], [FileName], [ExcelFriendlyFileName], [ReportName], [ReportActionName], [FlowDirection], [Category], [Description], [FilterPartial], [NeedsOrganizationID], [Visible]) VALUES (26, N'UitStroomKPITerugmelding.rdlc', N'', N'KPI Snelheid reactie/antwoord ontvanger', N'/Reporting/UitStroomKPITerugmeldingReport', 0, N'Verstuurd', N'Dit rapport geeft inzicht in de mate waarin de norm voor “Terugmelding Ja/Nee” van het dossier is behaald.
Bij de filtercriteria kan buiten de andere filtercriteria de normtijd (in uren) en norm (in %) worden ingegeven.

<i>Dossiers met fases die meer dan 100 dagen uit elkaar liggen niet meegerekend, dit om een meer realistisch beeld te krijgen van daadwerkelijke doorlooptijden.</i>', N'_KPIFilterTerugMelding', 0, 1)
INSERT INTO [dbo].[ReportDefinition] ([ReportDefinitionID], [FileName], [ExcelFriendlyFileName], [ReportName], [ReportActionName], [FlowDirection], [Category], [Description], [FilterPartial], [NeedsOrganizationID], [Visible]) VALUES (27, N'InStroomKPIInBehandeling.rdlc', N'', N'KPI Snelheid in behandeling nemen', N'/Reporting/InStroomKPIInBehandelingReport', 1, N'Ontvangen', N'Dit rapport geeft inzicht in de mate waarin de norm voor het “In behandeling nemen” van het dossier is behaald. 
Bij de filtercriteria kan buiten de andere filtercriteria de normtijd (in uren) en norm (in %) worden ingegeven.

<i>Dossiers met fases die meer dan 100 dagen uit elkaar liggen niet meegerekend, dit om een meer realistisch beeld te krijgen van daadwerkelijke doorlooptijden.</i>', N'_KPIFilter', 0, 1)
INSERT INTO [dbo].[ReportDefinition] ([ReportDefinitionID], [FileName], [ExcelFriendlyFileName], [ReportName], [ReportActionName], [FlowDirection], [Category], [Description], [FilterPartial], [NeedsOrganizationID], [Visible]) VALUES (28, N'InStroomKPITerugmelding.rdlc', N'', N'KPI Snelheid reactie/antwoord', N'/Reporting/InStroomKPITerugmeldingReport', 1, N'Ontvangen', N'Dit rapport geeft inzicht in de mate waarin de norm voor “Terugmelding Ja/Nee” van het dossier is behaald.
Bij de filtercriteria kan buiten de andere filtercriteria de normtijd (in uren) en norm (in %) worden ingegeven.

<i>Dossiers met fases die meer dan 100 dagen uit elkaar liggen niet meegerekend, dit om een meer realistisch beeld te krijgen van daadwerkelijke doorlooptijden.</i>', N'_KPIFilterTerugMelding', 0, 1)
INSERT INTO [dbo].[ReportDefinition] ([ReportDefinitionID], [FileName], [ExcelFriendlyFileName], [ReportName], [ReportActionName], [FlowDirection], [Category], [Description], [FilterPartial], [NeedsOrganizationID], [Visible]) VALUES (29, N'InStroomUitval.rdlc', N'InStroomUitval.rdlc', N'Uitval aantallen/details', N'/Reporting/InStroomUitvalReport', 1, N'Ontvangen', N'In dit rapport worden de dossiers getoond welke wel naar uw organisatie(s) zijn gestuurd, maar door een andere zorgaanbieder in behandeling zijn genomen. 
Alleen dossiers waarbij de actuele zorgaanbieder anders is dan dan de gekozen organisatie(s) worden in dit rapport vermeld.

<b>Oorzaken:</b>
 - De verzendende partij kan zelf voor een andere zorgaanbieder gekozen hebben
 - Een van uw medewerkers heeft aangegeven de patiënt niet aan te kunnen nemen waarna het dossier bij een ander terecht is gekomen. 
 
Dit overzicht toont verder de medewerker en de reden van een geweigerd verzoek tot overname.
 
<i>Wanneer 1 dossier meerdere malen geweigerd is wordt deze 1 keer getoond en geteld.</i>', N'_DefaultFilter', 1, 1)
INSERT INTO [dbo].[ReportDefinition] ([ReportDefinitionID], [FileName], [ExcelFriendlyFileName], [ReportName], [ReportActionName], [FlowDirection], [Category], [Description], [FilterPartial], [NeedsOrganizationID], [Visible]) VALUES (30, N'InStroomTotalen.rdlc', N'InStroomTotalen.rdlc', N'Uitval percentages / totalen', N'/Reporting/InStroomTotalenReport', 1, N'Ontvangen', N'Het inzichtelijk maken van uitgevallen dossiers versus het totaal in behandeling genomen dossiers.
Dit word getoond per versturende organisatie per maand
 
<i>Wanneer 1 dossier meerdere malen geweigerd is wordt deze 1 keer getoond en geteld.</i>', N'_DefaultFilter', 1, 1)
INSERT INTO [dbo].[ReportDefinition] ([ReportDefinitionID], [FileName], [ExcelFriendlyFileName], [ReportName], [ReportActionName], [FlowDirection], [Category], [Description], [FilterPartial], [NeedsOrganizationID], [Visible]) VALUES (31, N'InStroomWaarIsDePatientGebleven.rdlc', N'InStroomWaarIsDePatientGebleven.rdlc', N'Waar is de patient gebleven', N'/Reporting/InStroomWaarIsDePatientGebleven', 1, N'Ontvangen', N'In dit rapport worden de dossiers getoond welke wel naar uw organisatie(s) zijn gestuurd, maar door een andere zorgaanbieder in behandeling zijn genomen. 
<b>Oorzaken:</b>
 - De verzendende partij kan zelf voor een andere zorgaanbieder gekozen hebben
 - Een van uw medewerkers heeft aangegeven de patiënt niet aan te kunnen nemen waarna het dossier bij een ander terecht is gekomen. 

Dit overzicht toont een platte lijst van alle betrokken patienten
 
<i>Wanneer 1 dossier meerdere malen geweigerd is wordt deze 1 keer getoond en geteld.</i>', N'_DefaultFilter', 1, 1)
INSERT INTO [dbo].[ReportDefinition] ([ReportDefinitionID], [FileName], [ExcelFriendlyFileName], [ReportName], [ReportActionName], [FlowDirection], [Category], [Description], [FilterPartial], [NeedsOrganizationID], [Visible]) VALUES (33, N'AfdelingReport.rdlc', N'AfdelingReport.rdlc', N'Afdeling overzicht', N'/Reporting/AfdelingReport', 2, N'Overig', N'Overzicht van alle afdelingen', N'_DepartmentFilter', 0, 0)
INSERT INTO [dbo].[ReportDefinition] ([ReportDefinitionID], [FileName], [ExcelFriendlyFileName], [ReportName], [ReportActionName], [FlowDirection], [Category], [Description], [FilterPartial], [NeedsOrganizationID], [Visible]) VALUES (36, N'AfdelingCapacityReport.rdlc', N'AfdelingCapacityReportExcel.rdlc', N'Capaciteit rapport', N'/Reporting/AfdelingCapacityReport', 1, N'Overig', N'Dit rapport geeft een overzicht van de capaciteit van uw afdeling/locatie

<b>Afdeling overzicht</b>
Toont absolute waardes die ingegeven zijn per locatie en per afdeling/zorgtype.

<b>Capaciteit per week</b>
Toont gemiddelde minimale / maximale  capaciteit van de dagen van de  week.

<b>Capaciteit per categorie / maand</b>
Toont gemiddelde minimale / maximale capaciteit gegroepeerd per zorgcategorie (Intramuraal/Thuiszorg/Hospice).', N'_DepartmentCapacityFilter', 0, 1)
INSERT INTO [dbo].[ReportDefinition] ([ReportDefinitionID], [FileName], [ExcelFriendlyFileName], [ReportName], [ReportActionName], [FlowDirection], [Category], [Description], [FilterPartial], [NeedsOrganizationID], [Visible]) VALUES (37, N'', N'', N'CSV Dump alle dossiers', N'/Reporting/CSVDump', 2, N'Overig', N'Met de dump kan een export worden verkregen van de belangrijkste velden uit POINT, zodat de gebruiker naar eigen inzicht adhoc dwarsdoorsneden kan maken.', N'_DefaultFilter', 0, 1)
INSERT INTO [dbo].[ReportDefinition] ([ReportDefinitionID], [FileName], [ExcelFriendlyFileName], [ReportName], [ReportActionName], [FlowDirection], [Category], [Description], [FilterPartial], [NeedsOrganizationID], [Visible]) VALUES (38, N'TransferMonitorReport.rdlc', N'', N'Transfer Monitor', N'/Reporting/TransferMonitor', 2, N'Overig', N'Dit rapport geeft een overzicht van aantallen per type dossier.<br/>Ook toont dit dossier de doorlooptijden per type dossier.<br/>Hierbij kun je een vergelijking maken met 2 sets van afdelingen over een bepaalde periode.<br/>Type dossier is verder uitgebreid met type "Voorrang".<br/><br/><b>Een dossier is van dit type als:</b><br/>1. Dossier Is aangemaakt door een acute afdeling<br/>2. Als het dossier is aangeduid als Palliatief.', N'_CompareFilter', 0, 1)
INSERT INTO [dbo].[ReportDefinition] ([ReportDefinitionID], [FileName], [ExcelFriendlyFileName], [ReportName], [ReportActionName], [FlowDirection], [Category], [Description], [FilterPartial], [NeedsOrganizationID], [Visible]) VALUES (40, N'InStroomAantallenOverzicht.rdlc', N'DumpStroomReportData.rdlc', N'Overzicht ontvangende organisaties', N'/Reporting/UitstroomReportOverzicht', 0, N'Verstuurd', N'Dit rapport biedt inzicht in de ontvangende locatie, afdeling en specialisme welke naartoe is verstuurd.', N'_DefaultFilter', 0, 1)
INSERT INTO [dbo].[ReportDefinition] ([ReportDefinitionID], [FileName], [ExcelFriendlyFileName], [ReportName], [ReportActionName], [FlowDirection], [Category], [Description], [FilterPartial], [NeedsOrganizationID], [Visible]) VALUES (41, N'UitStroomOverzicht.rdlc', N'DumpStroomReportData.rdlc', N'Overzicht versturende organisaties', N'/Reporting/InstroomReportOverzicht', 1, N'Ontvangen', N'Dit rapport biedt inzicht in de versturende locatie, afdeling en specialisme vanwaar dossiers zijn ontvangen.', N'_DefaultFilter', 0, 1)
SET IDENTITY_INSERT [dbo].[ReportDefinition] OFF
SET IDENTITY_INSERT [dbo].[ReportDefinition] ON
INSERT INTO [dbo].[ReportDefinition] ([ReportDefinitionID], [FileName], [ExcelFriendlyFileName], [ReportName], [ReportActionName], [FlowDirection], [Category], [Description], [FilterPartial], [NeedsOrganizationID], [Visible]) VALUES (1, N'UitStroomAantallenLocatie.rdlc', N'DumpStroomReportData.rdlc', N'Aantallen per locatie / afdeling', N'/Reporting/UitstroomReportLocatie', 0, N'Verstuurd', N'Dit rapport biedt inzicht in de aantallen per locatie, afdeling en specialisme welke de organisatie heeft verstuurd.', N'_DefaultFilter', 0, 1)
INSERT INTO [dbo].[ReportDefinition] ([ReportDefinitionID], [FileName], [ExcelFriendlyFileName], [ReportName], [ReportActionName], [FlowDirection], [Category], [Description], [FilterPartial], [NeedsOrganizationID], [Visible]) VALUES (2, N'InStroomAantallenLocatie.rdlc', N'DumpStroomReportData.rdlc', N'Aantallen per locatie / afdeling', N'/Reporting/InstroomReportLocatie', 1, N'Ontvangen', N'Dit rapport biedt inzicht in de aantallen per locatie, afdeling en specialisme welke de organisatie heeft ontvangen.', N'_DefaultFilter', 0, 1)
SET IDENTITY_INSERT [dbo].[ReportDefinition] OFF
