SET IDENTITY_INSERT [dbo].[AfterCareCategory] ON
INSERT INTO [dbo].[AfterCareCategory] ([AfterCareCategoryID], [Name], [Abbreviation], [Sortorder]) VALUES (1, 'Thuiszorg', 'TZ', 0)
INSERT INTO [dbo].[AfterCareCategory] ([AfterCareCategoryID], [Name], [Abbreviation], [Sortorder]) VALUES (2, 'Zorginstelling', 'IM', 1)
INSERT INTO [dbo].[AfterCareCategory] ([AfterCareCategoryID], [Name], [Abbreviation], [Sortorder]) VALUES (3, 'Hospice', 'HO', 2)
SET IDENTITY_INSERT [dbo].[AfterCareCategory] OFF
