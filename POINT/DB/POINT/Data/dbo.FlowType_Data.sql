SET IDENTITY_INSERT [dbo].[FlowType] ON
INSERT INTO [dbo].[FlowType] ([FlowTypeID], [Name], [Description], [EmployeeID], [Timestamp], [ScreenName]) VALUES (5, N'VVT-VVT', N'VVT naar VVT', 55, '2017-09-22 14:33:00.000', N'AdminScript')
SET IDENTITY_INSERT [dbo].[FlowType] OFF
SET IDENTITY_INSERT [dbo].[FlowType] ON
INSERT INTO [dbo].[FlowType] ([FlowTypeID], [Name], [Description], [EmployeeID], [Timestamp], [ScreenName]) VALUES (1, N'VVT-ZH', N'VVT naar Ziekenhuis', 55, '2015-03-25 20:11:00.523', N'AdminScript')
INSERT INTO [dbo].[FlowType] ([FlowTypeID], [Name], [Description], [EmployeeID], [Timestamp], [ScreenName]) VALUES (2, N'ZH-VVT', N'Ziekenhuis naar VVT', 55, '2015-05-01 11:58:41.680', N'AdminScript')
INSERT INTO [dbo].[FlowType] ([FlowTypeID], [Name], [Description], [EmployeeID], [Timestamp], [ScreenName]) VALUES (3, N'ZH-ZH', N'Ziekenhuis naar Ziekenhuis', 55, '2016-01-19 14:52:22.290', N'AdminScript')
INSERT INTO [dbo].[FlowType] ([FlowTypeID], [Name], [Description], [EmployeeID], [Timestamp], [ScreenName]) VALUES (4, N'HA-VVT', N'Huisarts naar VVT', 55, '2017-08-07 09:46:00.000', N'AdminScript')
SET IDENTITY_INSERT [dbo].[FlowType] OFF
