SET IDENTITY_INSERT [dbo].[HealthInsurerConcern] ON
INSERT INTO [dbo].[HealthInsurerConcern] ([HealthInsurerConcernID], [Name]) VALUES (1, N'ACHMEA')
INSERT INTO [dbo].[HealthInsurerConcern] ([HealthInsurerConcernID], [Name]) VALUES (2, N'ASR ZIEKTEKOSTENVERZEKERINGEN')
INSERT INTO [dbo].[HealthInsurerConcern] ([HealthInsurerConcernID], [Name]) VALUES (3, N'COOPERATIE VGZ')
INSERT INTO [dbo].[HealthInsurerConcern] ([HealthInsurerConcernID], [Name]) VALUES (4, N'CZ')
INSERT INTO [dbo].[HealthInsurerConcern] ([HealthInsurerConcernID], [Name]) VALUES (5, N'DE FRIESLAND')
INSERT INTO [dbo].[HealthInsurerConcern] ([HealthInsurerConcernID], [Name]) VALUES (6, N'DSW-STAD HOLLAND')
INSERT INTO [dbo].[HealthInsurerConcern] ([HealthInsurerConcernID], [Name]) VALUES (7, N'ENO')
INSERT INTO [dbo].[HealthInsurerConcern] ([HealthInsurerConcernID], [Name]) VALUES (8, N'MENZIS')
INSERT INTO [dbo].[HealthInsurerConcern] ([HealthInsurerConcernID], [Name]) VALUES (9, N'ONVZ')
INSERT INTO [dbo].[HealthInsurerConcern] ([HealthInsurerConcernID], [Name]) VALUES (10, N'ZORG EN ZEKERHEID')
INSERT INTO [dbo].[HealthInsurerConcern] ([HealthInsurerConcernID], [Name]) VALUES (11, N'ONBEKEND')
INSERT INTO [dbo].[HealthInsurerConcern] ([HealthInsurerConcernID], [Name]) VALUES (12, N'IPTIQ LIFE S.A.')
SET IDENTITY_INSERT [dbo].[HealthInsurerConcern] OFF
