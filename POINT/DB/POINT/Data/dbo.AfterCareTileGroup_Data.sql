SET IDENTITY_INSERT [dbo].[AfterCareTileGroup] ON
INSERT INTO [dbo].[AfterCareTileGroup] ([AfterCareTileGroupID], [Name], [Sortorder], [ColorCode]) VALUES (5, N'Ziekenhuis', 5, N'#0066cc')
SET IDENTITY_INSERT [dbo].[AfterCareTileGroup] OFF
SET IDENTITY_INSERT [dbo].[AfterCareTileGroup] ON
INSERT INTO [dbo].[AfterCareTileGroup] ([AfterCareTileGroupID], [Name], [Sortorder], [ColorCode]) VALUES (1, N'WLZ', 1, N'#ff9f42')
INSERT INTO [dbo].[AfterCareTileGroup] ([AfterCareTileGroupID], [Name], [Sortorder], [ColorCode]) VALUES (2, N'ZVW', 2, N'#b9dd4c')
INSERT INTO [dbo].[AfterCareTileGroup] ([AfterCareTileGroupID], [Name], [Sortorder], [ColorCode]) VALUES (3, N'Anders', 3, N'#00BDF4')
INSERT INTO [dbo].[AfterCareTileGroup] ([AfterCareTileGroupID], [Name], [Sortorder], [ColorCode]) VALUES (4, N'Overig', 4, N'#d55c50')
SET IDENTITY_INSERT [dbo].[AfterCareTileGroup] OFF
