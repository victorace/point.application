SET IDENTITY_INSERT [dbo].[HealthAidDevice] ON
INSERT INTO [dbo].[HealthAidDevice] ([HealthAidDeviceId], [Name], [Sortorder]) VALUES (1, 'Rollator', 1)
INSERT INTO [dbo].[HealthAidDevice] ([HealthAidDeviceId], [Name], [Sortorder]) VALUES (2, 'Rolstoel', 2)
INSERT INTO [dbo].[HealthAidDevice] ([HealthAidDeviceId], [Name], [Sortorder]) VALUES (3, 'Krukken/stok', 3)
INSERT INTO [dbo].[HealthAidDevice] ([HealthAidDeviceId], [Name], [Sortorder]) VALUES (4, 'Postoel', 4)
INSERT INTO [dbo].[HealthAidDevice] ([HealthAidDeviceId], [Name], [Sortorder]) VALUES (5, 'Tillift', 5)
INSERT INTO [dbo].[HealthAidDevice] ([HealthAidDeviceId], [Name], [Sortorder]) VALUES (6, 'Hoog/laagbed', 6)
INSERT INTO [dbo].[HealthAidDevice] ([HealthAidDeviceId], [Name], [Sortorder]) VALUES (7, 'Zuurstof', 7)
INSERT INTO [dbo].[HealthAidDevice] ([HealthAidDeviceId], [Name], [Sortorder]) VALUES (8, 'Anti decubitus matras', 8)
INSERT INTO [dbo].[HealthAidDevice] ([HealthAidDeviceId], [Name], [Sortorder]) VALUES (9, 'Anders', 9)
SET IDENTITY_INSERT [dbo].[HealthAidDevice] OFF
