SET IDENTITY_INSERT [dbo].[GeneralActionType] ON
INSERT INTO [dbo].[GeneralActionType] ([GeneralActionTypeID], [Name], [Description]) VALUES (4, N'MenuProcess', N'Handmatig uit te voeren acties via de dossier-sectie in menu')
INSERT INTO [dbo].[GeneralActionType] ([GeneralActionTypeID], [Name], [Description]) VALUES (5, N'MenuTransfer', N'Handmatig uit te voeren acties via de aanvullend dossier-sectie in menu')
SET IDENTITY_INSERT [dbo].[GeneralActionType] OFF
SET IDENTITY_INSERT [dbo].[GeneralActionType] ON
INSERT INTO [dbo].[GeneralActionType] ([GeneralActionTypeID], [Name], [Description]) VALUES (1, N'Automatic', N'Automatisch/Code-driven uit te voeren acties')
INSERT INTO [dbo].[GeneralActionType] ([GeneralActionTypeID], [Name], [Description]) VALUES (2, N'Menu', N'Handmatig uit te voeren acties via menu-optie')
INSERT INTO [dbo].[GeneralActionType] ([GeneralActionTypeID], [Name], [Description]) VALUES (3, N'Phase', N'Fase-specifieke actie, handmatig uit te voeren via knop')
SET IDENTITY_INSERT [dbo].[GeneralActionType] OFF
