SET IDENTITY_INSERT [dbo].[TemplateDefault] ON
INSERT INTO [dbo].[TemplateDefault] ([TemplateDefaultID], [TemplateTypeID], [TemplateDefaultText], [Inactive], [OrganizationTypeID]) VALUES (2, 2, 'Geachte heer/mevrouw,
Voor uw POINT account is een aanvraag gedaan voor een nieuw wachtwoord. Indien dit onjuist is, wijzigt u dan niets in POINT en meldt deze incorrecte wachtwoord aanvraag a.u.b. binnen uw eigen organisatie aan de POINT contactpersoon of -beheerder.

Uw nieuwe wachtwoord : [ResetPassword.NewPassword]

Let op!
U moet uw wachtwoord direct veranderen na inloggen.
Uw nieuwe wachtwoord moet voldoen aan de volgende criteria:
- bestaat minimaal uit 8 karakters, 
- bevat minimaal één cijfer, 
- bevat minimaal één letter, 
- bevat minimaal één speciaal leesteken

Met vriendelijke groet,

POINT


P.S. Wist u dat u alle vragen over uw account kunt stellen aan de POINT beheerder van uw eigen organisatie of de POINT Regiobeheerder?', 0, NULL)
INSERT INTO [dbo].[TemplateDefault] ([TemplateDefaultID], [TemplateTypeID], [TemplateDefaultText], [Inactive], [OrganizationTypeID]) VALUES (4, 5, 'Geachte heer/mevrouw {Condition:[Client.GeneralPractitionerName]=!EMPTY}[Client.GeneralPractitionerName]{/Condition},  
 
Dit is een notificatiebericht van POINT. 
Er is een reactie gekomen op de zorgvraag die u voor patient [Client.ClientNameFormatted] heeft uitgezet bij zorginstelling [OtherDetails.PreviousNazorg]. 
Deze zorginstelling heeft aangeven de zorg NIET te kunnen leveren.  
De zorgvraag is niet automatisch doorgestuurd naar een andere zorginstelling. 
 
Dit bericht is automatisch verstuurd door POINT', 0, 12)
INSERT INTO [dbo].[TemplateDefault] ([TemplateDefaultID], [TemplateTypeID], [TemplateDefaultText], [Inactive], [OrganizationTypeID]) VALUES (5, 6, 'Geachte heer/mevrouw {Condition:[Client.GeneralPractitionerName]=!EMPTY}[Client.GeneralPractitionerName]{/Condition}, 

Dit is een notificatiebericht van POINT.
Er is een reactie gekomen op de zorgvraag die u voor patient [Client.ClientNameFormatted] heeft uitgezet bij zorginstelling [OtherDetails.PreviousNazorg].
Deze zorginstelling heeft aangeven de zorg NIET te kunnen leveren. 
De zorgvraag is automatisch doorgestuurd naar [OtherDetails.Nazorg].

<a href="[OtherDetails.DossierUrl]">Dossier</a>

Dit bericht is automatisch verstuurd door POINT', 0, 12)
INSERT INTO [dbo].[TemplateDefault] ([TemplateDefaultID], [TemplateTypeID], [TemplateDefaultText], [Inactive], [OrganizationTypeID]) VALUES (6, 7, 'Beste beheerder van point,

{Condition:[UnblockEmail.AdminUserName] =!EMPTY}
Je beschikt over een POINT beheeraccount waarmee je gebruikers kunt de-blokkeren.
{/Condition}
Een account is vergrendeld en de gebruiker verzoekt jou via deze e-mail dit account te de-blokkeren.
{Condition:[UnblockEmail.OtherAdminEmail]=!EMPTY}
Deze mail is ook gestuurd naar een andere beheerder, namelijk [UnblockEmail.OtherAdminEmail]
{/Condition}

Het gaat om de volgende gebruiker:

Gebruikersnaam: [UnblockEmail.EmployeeUserName]
Medewerkersnaam: [UnblockEmail.EmployeeFullName] 
E-mail adres: [UnblockEmail.EmployeeEmail] 
Telefoonnummer: [UnblockEmail.EmployeePhone] 
Afdeling: [UnblockEmail.EmployeeDepartment] 
Afd. telefoonnummer: [UnblockEmail.EmployeeDepartmentPhone] 

Hoe deze gebruiker te de-blokkeren?
  
1. Ga naar het medewerker beheerscherm via de volgende link: 
[UnblockEmail.EditEmployeePage] 
{Condition:[UnblockEmail.AdminUserName] =!EMPTY}
Log hierbij in met het account: [UnblockEmail.AdminUserName]
{/Condition}
2. Vul in daar de gebruikersnaam in en klik op “Zoeken”.
3. Selecteer de juiste gebruiker door hierop te klikken. 
4. Ga naar het tabblad “Account” en haal bij “Medewerker is geblokkeerd” het vinkje weg. 
5. Druk op de knop “Opslaan en Sluiten”.
De gebruiker kan dan weer inloggen. U kunt dit aan haar/hem terugkoppel via telefoon of e-mail.

Met vriendelijke groet,
POINT Systeem mail', 0, NULL)
INSERT INTO [dbo].[TemplateDefault] ([TemplateDefaultID], [TemplateTypeID], [TemplateDefaultText], [Inactive], [OrganizationTypeID]) VALUES (7, 8, 'Beste gebruiker van Point,

U kunt uw aangevraagde dump rapportage hier downloaden:

[DownloadLink.AsyncReportDownloadLink]
Met vriendelijke groet,
Point.', 0, NULL)
INSERT INTO [dbo].[TemplateDefault] ([TemplateDefaultID], [TemplateTypeID], [TemplateDefaultText], [Inactive], [OrganizationTypeID]) VALUES (9, 9, 'Beste gebruiker van Point,

U kunt uw aangevraagde logactie hier downloaden:

[DownloadLink.AsyncReportDownloadLink]
Met vriendelijke groet,
Point.', 0, NULL)
INSERT INTO [dbo].[TemplateDefault] ([TemplateDefaultID], [TemplateTypeID], [TemplateDefaultText], [Inactive], [OrganizationTypeID]) VALUES (10, 10, 'Beste gebruiker van Point,

Het is niet gelukt om de door u aangevraagde uitgebreide logging-leesactie rapportage te maken. 
Voor meer informatie, neem s.v.p contact op met het POINT team via het doorsturen van deze email 
aan support@linkassist.nl met vermelding van uw POINT gebruikersnaam. 

Met vriendelijke groet,
Point.
', 0, NULL)
SET IDENTITY_INSERT [dbo].[TemplateDefault] OFF
SET IDENTITY_INSERT [dbo].[TemplateDefault] ON
INSERT INTO [dbo].[TemplateDefault] ([TemplateDefaultID], [TemplateTypeID], [TemplateDefaultText], [Inactive], [OrganizationTypeID]) VALUES (1, 1, 'Geachte heer/mevrouw {Condition:[Client.GeneralPractitionerName]=!EMPTY}[Client.GeneralPractitionerName]{/Condition}, 

Hierbij informeren wij u over de nazorg inzet na ontslag uit ons 
ziekenhuis van de onderstaande patient.
Dit bericht staat los van de ontslagbrief.

{Condition:[Client.CivilServiceNumber]=!EMPTY}BSN nummer: [Client.CivilServiceNumber]{/Condition}
{Condition:[Client.ClientNameFormatted]=!EMPTY}Naam patient: [Client.ClientNameFormatted]{/Condition}
{Condition:[Client.BirthDate]=!EMPTY}Geboortedatum: [DateFields.BirthDateValue]{/Condition}
Adres patient: [Client.StreetName], [Client.Number], [Client.PostalCode], [Client.City]
{Condition:[DateFields.MedischeSituatieDatumOpname]=!EMPTY}Datum opname: [DateFields.MedischeSituatieDatumOpname]{/Condition}
{Condition:[DateFields.PatiëntWordtOpgenomenDatum]=!EMPTY}Datum opname: [DateFields.PatiëntWordtOpgenomenDatum]{/Condition}
{Condition:[DateFields.DischargeRealizedDate]=!EMPTY}Datum ontslag uit ziekenhuis: [DateFields.DischargeRealizedDate]{/Condition}
{Condition:[DateFields.DischargeRealizedDate]=EMPTY}
{Condition:[DateFields.CareBeginDate]=!EMPTY}Datum ontslag uit ziekenhuis: [DateFields.CareBeginDate]{/Condition}
{/Condition}
{Condition:[Wlz.InformatieHuisartsenbericht]=!EMPTY}Aanvullende gegevens: [Wlz.InformatieHuisartsenbericht]{/Condition}

Ziekenhuis: [Organization.Name], [Location.Name]
Ziekenhuis afdeling: [Department.Name]
{Condition:[Department.PhoneNumber]=!EMPTY || [Department.EmailAddress]=!EMPTY}Ziekenhuis contact: [Department.PhoneNumber] [Department.EmailAddress]{/Condition}
{Condition:[FieldValue.BehandelaarNaam]=!EMPTY}Behandelend specialist: [FieldValue.BehandelaarNaam]{/Condition}
{Condition:[OtherDetails.Nazorg]=!EMPTY}Nazorg wordt geleverd door: [OtherDetails.Nazorg]{/Condition}
{Condition:[OtherDetails.AfterCareDefinitive]=!EMPTY}Typering nazorg: [OtherDetails.AfterCareDefinitive]{/Condition}
{Condition:[FieldValue.PrognoseVerwachteOntwikkeling]=!EMPTY}Voorlopige prognose: [FieldValue.PrognoseVerwachteOntwikkeling]{/Condition}

Wanneer dit bericht u ten onrechte heeft bereikt,
verzoeken wij u een email te sturen naar {Condition:[Department.EmailAddress]=!EMPTY}[Department.EmailAddress]{/Condition}.

Hoogachtend,

[Organization.Name]', 0, 1)
INSERT INTO [dbo].[TemplateDefault] ([TemplateDefaultID], [TemplateTypeID], [TemplateDefaultText], [Inactive], [OrganizationTypeID]) VALUES (3, 4, 'Geachte heer/mevrouw {Condition:[Client.GeneralPractitionerName]=!EMPTY}[Client.GeneralPractitionerName]{/Condition}, 

Dit is een notificatiebericht van POINT.
Er is een reactie gekomen op de zorgvraag die u voor patient [Client.ClientNameFormatted] heeft uitgezet bij zorginstelling [OtherDetails.Nazorg].
Deze zorginstelling heeft aangeven de zorg WEL te kunnen leveren en wel met ingang van [FieldValue.DischargeProposedStartDate].
{Condition:[FieldValue.DischargeRemarks]=!EMPTY}
De zorginstelling heeft hierbij nog de volgende opmerking gegeven:

[FieldValue.DischargeRemarks]
{/Condition}

Dit bericht is automatisch verstuurd door POINT', 0, 12)
INSERT INTO [dbo].[TemplateDefault] ([TemplateDefaultID], [TemplateTypeID], [TemplateDefaultText], [Inactive], [OrganizationTypeID]) VALUES (8, 3, 'Geachte Heer/mevrouw [Client.MiddleName] [Client.LastName] 
 
U bent voor medische behandeling in ons ziekenhuis geweest op afdeling [Department.Name] [Department.PhoneNumber] 
Na opname/behandeling in het ziekenhuis is er nazorg voor u afgesproken. 
De nazorg zal gegeven worden door [ReceivingDepartment.FullName] 
Tel: [ReceivingDepartment.PhoneNumber] 
De zorg wordt door hen geleverd vanaf [AfterCareCategory.StartDate] 
{Condition:[AfterCareFinancing.Name]=!EMPTY}En wordt bekostigd vanuit [AfterCareFinancing.Name]{/Condition}{Condition:[Nazorg.Agreement]=!EMPTY} 
 
De nazorg die we afgesproken hebben bestaat uit hulp bij of verpleegkundige zorg bij 
[Nazorg.Agreement]{/Condition} 
{Condition:[AidProductOrderItem.Orders]=!EMPTY} 
[AidProductOrderItem.Orders]{/Condition} 
Voor vragen kunt u zich richten tot de organisaties die u zorg en andere service(s) gaan bieden of via onderstaande gegevens. 
 
Met vriendelijke groet, 
 
[SendingEmployee.OrganizationName] 
[SendingEmployee.DepartmentName] 
[SendingEmployee.EmployeeFullName] 
{Condition:[SendingEmployee.DepartmentPhoneNumber]=!EMPTY}Tel. [SendingEmployee.DepartmentPhoneNumber]{/Condition} 
{Condition:[SendingEmployee.DepartmentEmailAddress]=!EMPTY}Email [SendingEmployee.DepartmentEmailAddress]{/Condition} 
 
Deze brief is automatisch gemaakt en daarom niet ondertekend.', 0, 1)
SET IDENTITY_INSERT [dbo].[TemplateDefault] OFF
