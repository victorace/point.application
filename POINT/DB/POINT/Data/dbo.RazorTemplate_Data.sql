SET IDENTITY_INSERT [dbo].[RazorTemplate] ON
INSERT INTO [dbo].[RazorTemplate] ([RazorTemplateID], [TemplateName], [TemplateCode], [Contents], [CommunicationLogType]) VALUES (1072, N'Dossier is verplaatst naar uw afdeling/organisatie', N'DossierIsRelocatedToYou', N'@model Point.Database.Models.ViewModels.EmailRelocateDossierViewModel

Geachte heer/mevrouw,<br />
<br />
Er is zojuist een Transferdossier verplaatst van<br />
@Model.SourceDepartment<br />
naar de onderstaande afdeling van uw organisatie<br />
@Model.DestinationDepartment<br />
<br />
De reden van verplaatsing is: <br />
<pre>@Model.RelocateReason</pre>
<br />
Het gaat om @Model.ClientName<br />
<br />
U kunt geen antwoord terugmailen op dit bericht, maar klik op onderstaande link om het Transferdossier te openen.<br />
<br />
<a href="@Model.DossierUrl">Dossier patient</a><br />
<br />
Met vriendelijke groet,<br />
<br />
@Model.ResponsibleEmployee<br />
@Model.ResponsibleDepartment<br />
@Model.ResponsibleTelephoneNumber', 23)
SET IDENTITY_INSERT [dbo].[RazorTemplate] OFF
SET IDENTITY_INSERT [dbo].[RazorTemplate] ON
INSERT INTO [dbo].[RazorTemplate] ([RazorTemplateID], [TemplateName], [TemplateCode], [Contents], [CommunicationLogType]) VALUES (10, N'Verzoek tot indicatiestelling', N'EmailZHCIZ', N'@model Point.Database.Models.ViewModels.EmailZHCISViewModel
Geachte heer/mevrouw,<br />
<br />
Er is zojuist een Transferdossier naar uw organisatie/locatie, @Model.RecievingOrganization, verstuurd.<br />
Dit betreft de vraag om een indicatie te stellen voor een patient/client die wij momenteel in behandeling/zorg hebben.<br />
Het gaat om @Model.PatientName.<br />
<br />
U kunt geen antwoord terugmailen op dit bericht, maar klik op onderstaande link om het Transferdossier te openen.<br />
<br />
<a href="@Model.DossierUrl">Dossier patient</a><br />
<br />
Middels de knop [In behandeling nemen II] kunt u het Transferdossier in behandeling nemen.<br />
Nadat u het indicatiebesluit heeft ingevoerd kunt u het dossier terugsturen naar het Transferpunt middels de knop [Versturen].<br />
<br />
Met vriendelijke groet,<br />
<br />
@Model.SendingUser<br />
@Model.SendingLocation<br />
@Model.SendingTelephoneNumber', 5)
INSERT INTO [dbo].[RazorTemplate] ([RazorTemplateID], [TemplateName], [TemplateCode], [Contents], [CommunicationLogType]) VALUES (11, N'Verzoek overname zorgverlening', N'EmailZHVVT', N'@model Point.Database.Models.ViewModels.EmailZHVVTViewModel
Geachte heer/mevrouw,<br />
<br />
Er is zojuist een Transferdossier naar @Model.ReceivingDepartmentFullName verstuurd. <br />
Dit betreft de vraag om het overnemen van de zorg van een patient/client die wij momenteel in behandeling/zorg hebben.<br />
Het gaat om @Model.PatientName.<br />
<br />
U kunt geen antwoord terugmailen op dit bericht, maar klik op onderstaande link om het Transferdossier te openen.<br />
<br />
<a href="@Model.DossierUrl">Dossier patient</a><br />
<br />
Via de knop [In behandeling nemen VVT] kunt u het Transferdossier in behandeling nemen.<br />
Nadat u een datum heeft ingevoerd in het veld [Patient kan opgenomen worden met ingang van] kunt u via de knop [Versturen] het dossier naar ons terugsturen.<br />
<br />
Met vriendelijke groet,<br />
<br />
@Model.SendingUser<br />
@Model.SendingLocation<br />
@Model.SendingTelephoneNumber', 4)
INSERT INTO [dbo].[RazorTemplate] ([RazorTemplateID], [TemplateName], [TemplateCode], [Contents], [CommunicationLogType]) VALUES (19, N'Overname zorgverlening/behandeling', N'EmailVVTZH', N'@model Point.Database.Models.ViewModels.EmailVVTZHViewModel
Geachte heer/mevrouw,<br /><br />
Wij hebben een bericht ontvangen dat patient @Model.PatientName. die in zorg was bij zorginstelling @Model.SendingLocation naar ons ziekenhuis komt of hier al aanwezig is.<br /><br />
Het POINT dossier van deze transfer hebben wij doorgestuurd naar uw afdeling. Dit POINT dossier bevat een Verpleegkundige overdracht om een goede zorg overdracht te waarborgen.<br /><br />
Klik op onderstaande link om de verpleegkundige overdracht te raadplegen.<br />
<br />
<a href="@Model.DossierUrl">Dossier patient</a><br />
<br />
U kunt geen antwoord terugmailen op dit bericht.<br /><br />
<br />
Met vriendelijke groet,<br />
<br />
@Model.SendingUser<br />
@Model.SendingLocation<br />
@Model.SendingTelephoneNumber', 6)
INSERT INTO [dbo].[RazorTemplate] ([RazorTemplateID], [TemplateName], [TemplateCode], [Contents], [CommunicationLogType]) VALUES (43, N'Patientoverdracht Verpleeghuis/Thuiszorg naar Afdeling Ziekenhuis', N'NOTIFYZHVO', N'@model Point.Database.Models.ViewModels.EmailVVTZHViewModel
Geachte heer/mevrouw,<br /><br />
Patient @Model.PatientName die bij onze organisatie al in zorg was, komt naar of is aanwezig in uw ziekenhuis. Wij hebben een Verpleegkundige overdracht opgesteld om een goede zorg overdracht te waarborgen.<br />
<br />
@if (@Model.IsAltered)
{
    <text>De verpleegkundige overdracht voor deze patient was reeds verstuurd, maar is daarna door ons gewijzigd.</text><br /><br />
}
Klik op onderstaande link om de verpleegkundige overdracht te raadplegen.<br /><br />
<a href="@Model.DossierUrl">Dossier patient</a><br />
<br />
U kunt geen antwoord terugmailen op dit bericht.<br /><br />
<br />
Met vriendelijke groet,<br />
<br />
@Model.SendingUser<br />
@Model.SendingLocation<br />
@Model.SendingTelephoneNumber', 12)
INSERT INTO [dbo].[RazorTemplate] ([RazorTemplateID], [TemplateName], [TemplateCode], [Contents], [CommunicationLogType]) VALUES (44, N'Verpleegkundige overdracht definitief', N'NOTIFYVVTVO', N'@model Point.Database.Models.ViewModels.EmailZHVVTViewModel
Geachte heer/mevrouw,<br />
<br />
Uw organisatie/locatie, @Model.RecievingOrganization @Model.ReceivingLocation @Model.ReceivingDepartment, heeft aangegeven de zorg over te nemen van patient @Model.PatientName.<br />
<br />
De verpleegkundige overdracht voor deze patient is definitief gemaakt.<br />
<br />
Klik op onderstaande link om de verpleegkundige overdracht te openen.<br />
<a href="@Model.DossierUrl">Dossier patient</a><br />
<br />
U kunt geen antwoord terugmailen op dit bericht.<br />
<br />
Als u via deze link de gegevens niet kunt benaderen, dan<br />
  - Heeft u met uw inlogaccount waarschijnlijk niet de juiste rechten voor de genoemde organisatie / locatie<br />
  - Rechten kunnen worden gewijzigd door de beheerder van uw eigen organisatie<br />
<br />
<br />
Met vriendelijk groeten,<br />
<br />
<br />
@Model.SendingUser<br />
<br />
@Model.SendingLocation<br />
@Model.SendingTelephoneNumber', 12)
INSERT INTO [dbo].[RazorTemplate] ([RazorTemplateID], [TemplateName], [TemplateCode], [Contents], [CommunicationLogType]) VALUES (48, N'POINT dossier (tijdelijk) ingetrokken of naar andere zorgaanbieder gestuurd', N'EmailCancelZHVVT', N'@model Point.Database.Models.ViewModels.EmailZHVVTViewModel
Geachte heer/mevrouw,<br />
<br />
Uw organisatie/locatie/afdeling, @Model.ReceivingDepartmentFullName, heeft eerder van ons een verzoek ontvangen om de zorg over te nemen van een patiënt/cliënt die wij momenteel in behandeling/zorg hebben.<br />
Het betreft patient @Model.PatientName<br />
Dit verzoek is op basis van uw antwoord, of door wijzigingen in de situatie van de patiënt, (tijdelijk) ingetrokken of uitgezet bij een andere VVT.<br />
@Model.CancelReason <br />
Uw organisatie heeft daarom geen toegang meer tot het POINT dossier van deze patiënt/cliënt.<br />
Indien het dossier gewijzigd is en eventueel opnieuw naar uw organisatie gestuurd wordt, ontvangt u daarvan een bericht per email en heeft u weer toegang tot het dossier. U moet het dossier opnieuw in behandeling nemen en beantwoorden via de knop ‘verstuur naar transferpunt’.<br />
<br />
<br />
U kunt geen antwoord terugmailen op dit bericht.<br />
<br />
Met vriendelijk groeten,<br />
<br />
@Model.SendingUser<br />
@Model.SendingLocation<br />
@Model.SendingTelephoneNumber', 13)
INSERT INTO [dbo].[RazorTemplate] ([RazorTemplateID], [TemplateName], [TemplateCode], [Contents], [CommunicationLogType]) VALUES (50, N'Point Error', N'EXCEPTION', N'@model Point.ServiceBus.Models.ExceptionMessage

<h1>@Model.Title (@Model.ErrorNumber)</h1>
<pre>@Model.Message</pre>

<h2>Variabelen</h2>
<pre>TransferID      : @Model.TransferID
EmployeeID      : @Model.EmployeeID
Username        : @Model.EmployeeUserName
UserAgent       : @Model.UserAgent 
MachineName     : @Model.MachineName
WindowsUsername : @Model.WindowsIdentity
Timestamp       : @Model.TimeStamp
HuidigeUrl      : @Model.CurrentUrl
IsAjaxRequest   : @Model.IsAjaxRequest
QueryString     : @Model.QueryString
Cookies         : @Model.Cookies
PostData        : @Model.PostData</pre>

    <h2>Inner Exception</h2>
<pre>@Model.InnerException</pre>

    <h2>StackTrace</h2>
<pre>@Model.StackTrace</pre>', 11)
INSERT INTO [dbo].[RazorTemplate] ([RazorTemplateID], [TemplateName], [TemplateCode], [Contents], [CommunicationLogType]) VALUES (51, N'Verpleegkundige overdracht gewijzigd', N'NOTIFYZHVVTVOCHANGE', N'@model Point.Database.Models.ViewModels.EmailZHVVTViewModel
Geachte heer/mevrouw,<br />
<br />
Uw organisatie/locatie, @Model.RecievingOrganization @Model.ReceivingLocation @Model.ReceivingDepartment, heeft aangegeven de zorg over te nemen van patient @Model.PatientName.<br />
<br />
De verpleegkundige overdracht voor deze patient was reeds definitief gemaakt maar is daarna door ons gewijzigd.<br />
<br />
Klik op onderstaande link om de verpleegkundige overdracht te raadplegen.<br /><br />
<a href="@Model.DossierUrl">Dossier patient</a><br />
<br />
U kunt geen antwoord terugmailen op dit bericht.<br /><br />
<br />
Met vriendelijk groeten,<br />
<br />
<br />
@Model.SendingUser<br />
<br />
@Model.SendingLocation<br />
@Model.SendingTelephoneNumber', 12)
INSERT INTO [dbo].[RazorTemplate] ([RazorTemplateID], [TemplateName], [TemplateCode], [Contents], [CommunicationLogType]) VALUES (55, N'Verzoek overname zorgverlening', N'EmailZHVVT-NOACK', N'@model Point.Database.Models.ViewModels.EmailZHVVTViewModel
Geachte heer/mevrouw,<br />
<br />
Er is zojuist een POINT Transferdossier naar @Model.ReceivingDepartmentFullName, verstuurd. <br />
Dat dossier betreft de overname van zorgverlening aangaande een patient/client die wij momenteel in behandeling/zorg hebben.<br />
Het gaat om @Model.PatientName.<br />
<br />
U kunt geen antwoord terugmailen op dit bericht, maar klik op onderstaande link om het Transferdossier te openen.<br />
<br />
<a href="@Model.DossierUrl">Dossier patient</a><br />
<br />
Met vriendelijke groet,<br />
<br />
@Model.SendingUser<br />
@Model.SendingLocation<br />
@Model.SendingTelephoneNumber', 4)
INSERT INTO [dbo].[RazorTemplate] ([RazorTemplateID], [TemplateName], [TemplateCode], [Contents], [CommunicationLogType]) VALUES (58, N'Patientoverdracht VVT naar Transferpunt Ziekenhuis', N'NOTIFYZHTPVO', N'@model Point.Database.Models.ViewModels.EmailVVTZHViewModel
Geachte heer/mevrouw,<br /><br />
Patient @Model.PatientName die bij onze organisatie al in zorg was, komt naar of is aanwezig in uw ziekenhuis. Wij hebben een Verpleegkundige overdracht opgesteld om een goede zorg overdracht te waarborgen.<br />
<br />
@if (@Model.IsAltered)
{
    <text>De verpleegkundige overdracht voor deze patient was reeds verstuurd, maar is daarna door ons gewijzigd.</text><br /><br />
}
Klik op onderstaande link om de verpleegkundige overdracht te raadplegen.<br /><br />
<a href="@Model.DossierUrl">Dossier patient</a><br />
<br />
U kunt geen antwoord terugmailen op dit bericht.<br /><br />
<br />
Met vriendelijke groet,<br />
<br />
@Model.SendingUser<br />
@Model.SendingLocation<br />
@Model.SendingTelephoneNumber', 12)
INSERT INTO [dbo].[RazorTemplate] ([RazorTemplateID], [TemplateName], [TemplateCode], [Contents], [CommunicationLogType]) VALUES (66, N'POINT Communicatie Journaal memo toegevoegd', N'EmailTransferMemoNotification', N'@model Point.Database.Models.ViewModels.EmailTransferMemoNotificationViewModel
Geachte heer/mevrouw,<br />
<br />
Er is zojuist een memo toegevoegd in het  POINT communicatie journaal van een<br />
@Model.DoorlopendDossierType doorlopend dossier die mogelijk voor u van belang is.<br />
Het gaat om het dossier van patient @Model.PatientName.<br />
<br />
U kunt geen antwoord terugmailen op dit bericht. Klik op onderstaande link om het  communicatie journaal van het doorlopend dossier te openen.<br />
<br />
<a href="@Model.DossierUrl">Doorlopend dossier </a><br />
<br />
<br />
Met vriendelijke groet,<br />
<br />
@Model.SendingUser<br />
<br />
@Model.SendingLocation<br />
@Model.SendingTelephoneNumber', 0)
INSERT INTO [dbo].[RazorTemplate] ([RazorTemplateID], [TemplateName], [TemplateCode], [Contents], [CommunicationLogType]) VALUES (67, N'Verzoek feedback Doorlopend Dossier', N'EmailDoorlopendDossierFeedback', N'@model Point.Database.Models.ViewModels.EmailDossierFeedbackViewModel
@using Point.Infrastructure.Extensions

Geachte heer/mevrouw,<br /><br />
Wij hebben recentelijk een POINT transferdossier opgesteld ten behoeve van u/uw organisatie Afdeling/team.<br /><br />
Om de zorg daarna ook af te kunnen stemmen, is een Doorlopend Dossier (DD) gecreëerd.  Voor dit DD wordt u periodiek per email gevraagd om gedurende de indicatie periode<br />
feedback te geven over de zorg en de situatie van de patient. Het gaat om het volgende dossier:<br /><br />
Patientnaam: @Model.ClientName<br />
Doorlopend dossier: @Model.Type<br />
Naam: @Model.Title<br />
Startdatum periode: @Model.StartDate.ToPointDateDisplay()<br />
Einddatum periode: @Model.EndDate.ToPointDateDisplay()<br />
Gewenste feedback interval: 1 keer per @Model.Quantity dagen<br /><br />
Klik op onderstaande link om het dossier te openen waarin u de feedback over de uitvoering kunt invoeren. Het ziekenhuis kan dit inzien en hierin ook notities plaatsen wanneer de patient mij tussentijds bezoekt. Deze notities zijn voor u inzichtelijk op dezelfde plek.
<br /><br />
<a href="@Model.DossierUrl">Doorlopend dossier </a><br /><br />
Deze e-mail kan als reminder meerdere malen worden verstuurd aangezien er meerdere data zijn waarop feedback wordt gevraagd.<br /><br />
U kunt geen antwoord terugmailen op dit bericht.<br /><br />
Met vriendelijk groeten,<br />
@Model.SendingEmployeeName<br />
@Model.SendingLocationName<br />
@Model.SendingOrganizationName<br />
@Model.SendingEmployeeTelephoneNumber<br />', 10)
INSERT INTO [dbo].[RazorTemplate] ([RazorTemplateID], [TemplateName], [TemplateCode], [Contents], [CommunicationLogType]) VALUES (68, N'Patient overdracht ZH-ZH', N'NOTIFYZHZHVO', N'@model Point.Database.Models.ViewModels.EmailZHZHViewModel
Geachte heer/mevrouw,<br /><br />
Patient @Model.PatientName die bij onze organisatie al in zorg was, komt naar of is aanwezig in uw ziekenhuis. Wij hebben een Verpleegkundige overdracht opgesteld om een goede zorg overdracht te waarborgen.<br />
<br />
Klik op onderstaande link om de verpleegkundige overdracht te raadplegen.<br /><br />
<a href="@Model.DossierUrl">Dossier patient</a><br />
<br />
U kunt geen antwoord terugmailen op dit bericht.<br /><br />
<br />
Met vriendelijke groet,<br />
<br />
@Model.SendingUser<br />
@Model.SendingLocation<br />
@Model.SendingTelephoneNumber', 12)
INSERT INTO [dbo].[RazorTemplate] ([RazorTemplateID], [TemplateName], [TemplateCode], [Contents], [CommunicationLogType]) VALUES (70, N'Verpleegkundige overdracht gewijzigd', N'NOTIFYZHZHVOCHANGE', N'@model Point.Database.Models.ViewModels.EmailZHZHViewModel
Geachte heer/mevrouw,<br />
<br />
Uw organisatie/locatie, @Model.RecievingOrganization @Model.RecievingLocation @Model.RecievingDepartment, heeft aangegeven de zorg over te nemen van patient @Model.PatientName.<br />
<br />
De verpleegkundige overdracht voor deze patient was reeds definitief gemaakt maar is daarna door ons gewijzigd.<br />
<br />
Klik op onderstaande link om de verpleegkundige overdracht te raadplegen.<br /><br />
<a href="@Model.DossierUrl">Dossier patient</a><br />
<br />
U kunt geen antwoord terugmailen op dit bericht.<br /><br />
<br />
Met vriendelijk groeten,<br />
<br />
<br />
@Model.SendingUser<br />
<br />
@Model.SendingLocation<br />
@Model.SendingTelephoneNumber', 12)
INSERT INTO [dbo].[RazorTemplate] ([RazorTemplateID], [TemplateName], [TemplateCode], [Contents], [CommunicationLogType]) VALUES (71, N'Verpleegkundige overdracht gewijzigd', N'NOTIFYVVTVVTVOCHANGE', N'@model Point.Database.Models.ViewModels.EmailVVTVVTViewModel
Geachte heer/mevrouw,<br />
<br />
Uw organisatie/locatie, @Model.RecievingOrganization @Model.ReceivingLocation @Model.ReceivingDepartment, heeft aangegeven de zorg over te nemen van patient @Model.PatientName.<br />
<br />
De verpleegkundige overdracht voor deze patient was reeds definitief gemaakt maar is daarna door ons gewijzigd.<br />
<br />
Klik op onderstaande link om de verpleegkundige overdracht te raadplegen.<br /><br />
<a href="@Model.DossierUrl">Dossier patient</a><br />
<br />
U kunt geen antwoord terugmailen op dit bericht.<br /><br />
<br />
Met vriendelijk groeten,<br />
<br />
<br />
@Model.SendingUser<br />
<br />
@Model.SendingLocation<br />
@Model.SendingTelephoneNumber', 12)
INSERT INTO [dbo].[RazorTemplate] ([RazorTemplateID], [TemplateName], [TemplateCode], [Contents], [CommunicationLogType]) VALUES (72, N'Verpleegkundige overdracht definitief', N'NOTIFYVVTVVTVO', N'@model Point.Database.Models.ViewModels.EmailVVTVVTViewModel
Geachte heer/mevrouw,<br />
<br />
Uw organisatie/locatie, @Model.RecievingOrganization @Model.ReceivingLocation @Model.ReceivingDepartment, heeft aangegeven de zorg over te nemen van patient @Model.PatientName.<br />
<br />
De verpleegkundige overdracht voor deze patient is definitief gemaakt.<br />
<br />
Klik op onderstaande link om de verpleegkundige overdracht te openen.<br />
<a href="@Model.DossierUrl">Dossier patient</a><br />
<br />
U kunt geen antwoord terugmailen op dit bericht.<br />
<br />
Als u via deze link de gegevens niet kunt benaderen, dan<br />
  - Heeft u met uw inlogaccount waarschijnlijk niet de juiste rechten voor de genoemde organisatie / locatie<br />
  - Rechten kunnen worden gewijzigd door de beheerder van uw eigen organisatie<br />
<br />
<br />
Met vriendelijk groeten,<br />
<br />
<br />
@Model.SendingUser<br />
<br />
@Model.SendingLocation<br />
@Model.SendingTelephoneNumber', 12)
INSERT INTO [dbo].[RazorTemplate] ([RazorTemplateID], [TemplateName], [TemplateCode], [Contents], [CommunicationLogType]) VALUES (74, N'Verzoek bemiddeling zorgverlening', N'EmailHATO', N'@model Point.Database.Models.ViewModels.EmailHATOViewModel
Geachte heer/mevrouw,<br />
<br />
Er is zojuist een Transferdossier naar uw organisatie, @Model.ReceivingDepartmentFullName verstuurd. <br />
Dit betreft de vraag om te bemiddelen tussen de Huisarts en een geschikte zorgaanbieder.<br />
Het gaat om @Model.PatientName.<br />
<br />
U kunt geen antwoord terugmailen op dit bericht, maar klik op onderstaande link om het Transferdossier te openen.<br />
<br />
<a href="@Model.DossierUrl">Dossier patient</a><br />
<br />
Via de knop [Verzenden VVT] kunt u het Transferdossier naar een geschikte zorgaanbieder sturen.<br />
<br />
Met vriendelijke groet,<br />
<br />
@Model.SendingUser<br />
@Model.SendingLocation<br />
@Model.SendingTelephoneNumber', 4)
INSERT INTO [dbo].[RazorTemplate] ([RazorTemplateID], [TemplateName], [TemplateCode], [Contents], [CommunicationLogType]) VALUES (75, N'Notificatie zorgverlening', N'EmailZHRealizedVVT', N'@model Point.Database.Models.ViewModels.EmailZHVVTViewModel
Geachte heer/mevrouw,<br />
<br />
Uw organisatie @Model.ReceivingDepartmentFullName heeft zojuist toegang gekregen  tot een POINT Transferdossier.<br />
Het gaat om @Model.PatientName.<br />
<br />
U kunt geen antwoord terugmailen op dit bericht, maar klik op onderstaande link om het Transferdossier te openen.<br />
<br />
<a href="@Model.DossierUrl">Dossier patient</a><br />
<br />
Met vriendelijke groet,<br />
<br />
@Model.SendingUser<br />
@Model.SendingLocation<br />
@Model.SendingTelephoneNumber', 18)
INSERT INTO [dbo].[RazorTemplate] ([RazorTemplateID], [TemplateName], [TemplateCode], [Contents], [CommunicationLogType]) VALUES (76, N'Overname zorgverlening/behandeling', N'EmailZHZH', N'@model Point.Database.Models.ViewModels.EmailZHZHViewModel
Geachte heer/mevrouw,<br /><br />
Wij hebben een bericht ontvangen dat patient @Model.PatientName. die in zorg was bij zorginstelling @Model.SendingLocation naar ons ziekenhuis komt of hier al aanwezig is.<br /><br />
Het POINT dossier van deze transfer hebben wij doorgestuurd naar uw afdeling. Dit POINT dossier bevat een Verpleegkundige overdracht om een goede zorg overdracht te waarborgen.<br /><br />
Klik op onderstaande link om de verpleegkundige overdracht te raadplegen.<br />
<br />
<a href="@Model.DossierUrl">Dossier patient</a><br />
<br />
U kunt geen antwoord terugmailen op dit bericht.<br /><br />
<br />
Met vriendelijke groet,<br />
<br />
@Model.SendingUser<br />
@Model.SendingLocation<br />
@Model.SendingTelephoneNumber', 6)
INSERT INTO [dbo].[RazorTemplate] ([RazorTemplateID], [TemplateName], [TemplateCode], [Contents], [CommunicationLogType]) VALUES (77, N'Verzoek overname zorgverlening', N'EmailHAVVT', N'@model Point.Database.Models.ViewModels.EmailHAVVTViewModel
Geachte heer/mevrouw,<br />
<br />
Er is zojuist een Transferdossier naar @Model.ReceivingDepartmentFullName verstuurd. <br />
Dit betreft de vraag om het overnemen van de zorg van een patient/client die wij momenteel in behandeling/zorg hebben.<br />
Het gaat om @Model.PatientName.<br />
<br />
U kunt geen antwoord terugmailen op dit bericht, maar klik op onderstaande link om het Transferdossier te openen.<br />
<br />
<a href="@Model.DossierUrl">Dossier patient</a><br />
<br />
Via de knop [In behandeling nemen VVT] kunt u het Transferdossier in behandeling nemen.<br />
Nadat u een datum heeft ingevoerd in het veld [Patient kan opgenomen worden met ingang van] kunt u via de knop [Versturen] het dossier naar ons terugsturen.<br />
<br />
Met vriendelijke groet,<br />
<br />
@Model.SendingUser<br />
@Model.SendingLocation<br />
@Model.SendingTelephoneNumber', 4)
INSERT INTO [dbo].[RazorTemplate] ([RazorTemplateID], [TemplateName], [TemplateCode], [Contents], [CommunicationLogType]) VALUES (79, N'Verzoek overname zorgverlening', N'EmailVVTVVT', N'@model Point.Database.Models.ViewModels.EmailVVTVVTViewModel
Geachte heer/mevrouw,<br />
<br />
Er is zojuist een Transferdossier naar @Model.ReceivingDepartmentFullName verstuurd. <br />
Dit betreft de vraag om het overnemen van de zorg van een patient/client die wij momenteel in behandeling/zorg hebben.<br />
Het gaat om @Model.PatientName.<br />
<br />
U kunt geen antwoord terugmailen op dit bericht, maar klik op onderstaande link om het Transferdossier te openen.<br />
<br />
<a href="@Model.DossierUrl">Dossier patient</a><br />
<br />
Via de knop [In behandeling nemen VVT] kunt u het Transferdossier in behandeling nemen.<br />
Nadat u een datum heeft ingevoerd in het veld [Patient kan opgenomen worden met ingang van] kunt u via de knop [Versturen] het dossier naar ons terugsturen.<br />
<br />
Met vriendelijke groet,<br />
<br />
@Model.SendingUser<br />
@Model.SendingLocation<br />
@Model.SendingTelephoneNumber', 4)
INSERT INTO [dbo].[RazorTemplate] ([RazorTemplateID], [TemplateName], [TemplateCode], [Contents], [CommunicationLogType]) VALUES (80, N'Alternatieve datum zorg goedgekeurd', N'EvaluateDecisionYes', N'@model Point.Database.Models.ViewModels.EmailViewModel
Geachte heer/mevrouw,<br />
<br />
U heeft een reactie gegeven op een POINT transferdossier dat naar @Model.ReceivingDepartment is gestuurd.<br />
Hierbij heeft u aangegeven de zorgvraag t.a.v. Patient @Model.PatientName te kunnen leveren echter niet vanaf de gevraagde startdatum.<br />
U heeft een andere startdatum voorgesteld.<br />
De door u voorgestelde datum voor het in zorg nemen is geaccepteerd.<br />
<br />
U kunt geen antwoord terugmailen op dit bericht, maar klik op onderstaande link om het Transferdossier te openen.<br />
<br />
<a href="@Model.DossierUrl">Dossier patient</a><br />
<br />
Met vriendelijke groet,<br />
<br />
@Model.SendingUser<br />
@Model.SendingLocation<br />
@Model.SendingTelephoneNumber', 4)
INSERT INTO [dbo].[RazorTemplate] ([RazorTemplateID], [TemplateName], [TemplateCode], [Contents], [CommunicationLogType]) VALUES (81, N'Verzoek tot GRZ indicatiestelling', N'EmailZHGRZ', N'@model Point.Database.Models.ViewModels.EmailViewModel
Geachte heer/mevrouw,<br />
<br />
Er is zojuist een Transferdossier naar uw organisatie/locatie, @Model.ReceivingOrganization, verstuurd.<br />
Dit betreft de vraag om een GRZ indicatie te stellen voor een patient/client die wij momenteel in behandeling/zorg hebben.<br />
Het gaat om @Model.PatientName.<br />
<br />
U kunt geen antwoord terugmailen op dit bericht, maar klik op onderstaande link om het Transferdossier te openen.<br />
<br />
<a href="@Model.DossierUrl">Dossier patient</a><br />
<br />
Middels de knop [In behandeling nemen] kunt u het Transferdossier in behandeling nemen.<br />
Nadat u het formulier [GRZ Uitslag Triage] heeft ingevoerd kunt u het dossier terugsturen naar het Transferpunt middels de knop [Versturen].<br />
<br />
Met vriendelijke groet,<br />
<br />
@Model.SendingUser<br />
@Model.SendingLocation<br />
@Model.SendingTelephoneNumber', 20)
INSERT INTO [dbo].[RazorTemplate] ([RazorTemplateID], [TemplateName], [TemplateCode], [Contents], [CommunicationLogType]) VALUES (1071, N'POINT signaleringen: Laatste {0},  Open aantal {1}', N'AutoSignaleringEmail', N'@model Point.Database.Models.ViewModels.EmailSignaleringReminder

<style type="text/css">
.headercolor{
	background-color:#00396A;
	color:white;
	font-family:sans-serif;
	font-size:14px;
}
.tablestyle{
	border-spacing : 15px 3px;
}

.rowcolor{
	background-color:#E5E5E5;
	color:black;
	font-family:sans-serif;
	font-size:14px;
}
</style>
Geachte heer/mevrouw,<br />
U heeft één of meer openstaande POINT signaleringen.<br />
Het betreft:<br />
<br />
 <table class="tablestyle">
        <thead class="headercolor" >
            <tr>
                <th>Datum/ Tijd</th>
                <th>Door</th>
                <th>Soort/ Commentaar</th>
            </tr>
        </thead>
        @if (Model.SenderSignalering.Any())
        {
            <tbody>
                @foreach (var item in Model.SenderSignalering)
                {
                    <tr class="rowcolor">
                        <td >@item.SignaleringDateTime</td>
                        <td >@item.OrganizationName</td>
                        <td >@item.SignaleringTypeComment</td>                    
                    </tr>
                }
            </tbody>
        }
    </table>
	<br />
	Klik op onderstaande link om naar POINT te gaan om deze signaleringen te kunnen opvolgen/afhandelen <br />
	<br />
	@Model.Url <br/>

	<br />
	<br />
	Deze email is automatisch verstuurd door POINT ', 22)
SET IDENTITY_INSERT [dbo].[RazorTemplate] OFF
