SET IDENTITY_INSERT [dbo].[DepartmentType] ON
INSERT INTO [dbo].[DepartmentType] ([DepartmentTypeID], [Name]) VALUES (3, 'Centraal meldpunt')
SET IDENTITY_INSERT [dbo].[DepartmentType] OFF
SET IDENTITY_INSERT [dbo].[DepartmentType] ON
INSERT INTO [dbo].[DepartmentType] ([DepartmentTypeID], [Name]) VALUES (1, 'Afdeling')
INSERT INTO [dbo].[DepartmentType] ([DepartmentTypeID], [Name]) VALUES (2, 'Transferpunt')
SET IDENTITY_INSERT [dbo].[DepartmentType] OFF
