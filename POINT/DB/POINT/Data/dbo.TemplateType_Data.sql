SET IDENTITY_INSERT [dbo].[TemplateType] ON
INSERT INTO [dbo].[TemplateType] ([TemplateTypeID], [Name], [Description], [CanBeOrganizationSpecific]) VALUES (10, 'ErrorLogReadMail', 'LogReadMailError', 0)
SET IDENTITY_INSERT [dbo].[TemplateType] OFF
SET IDENTITY_INSERT [dbo].[TemplateType] ON
INSERT INTO [dbo].[TemplateType] ([TemplateTypeID], [Name], [Description], [CanBeOrganizationSpecific]) VALUES (1, 'ZorgMailZHVVTJa', 'ZorgMailZHVVTJa', 1)
INSERT INTO [dbo].[TemplateType] ([TemplateTypeID], [Name], [Description], [CanBeOrganizationSpecific]) VALUES (2, 'PasswordResetMail', 'PasswordResetMail', 0)
INSERT INTO [dbo].[TemplateType] ([TemplateTypeID], [Name], [Description], [CanBeOrganizationSpecific]) VALUES (3, 'PatientenBrief', 'PatientenBrief', 1)
INSERT INTO [dbo].[TemplateType] ([TemplateTypeID], [Name], [Description], [CanBeOrganizationSpecific]) VALUES (4, 'ZorgMailHAVVTJa', 'ZorgMailHAVVTJa', 1)
INSERT INTO [dbo].[TemplateType] ([TemplateTypeID], [Name], [Description], [CanBeOrganizationSpecific]) VALUES (5, 'ZorgMailHAVVTNee', 'ZorgMailHAVVTNee', 1)
INSERT INTO [dbo].[TemplateType] ([TemplateTypeID], [Name], [Description], [CanBeOrganizationSpecific]) VALUES (6, 'ZorgMailHAVVTNeeEnNieuweVVT', 'ZorgMailHAVVTNeeEnNieuweVVT', 1)
INSERT INTO [dbo].[TemplateType] ([TemplateTypeID], [Name], [Description], [CanBeOrganizationSpecific]) VALUES (7, 'UnblockUserMail', 'UnblockUserMail', 0)
INSERT INTO [dbo].[TemplateType] ([TemplateTypeID], [Name], [Description], [CanBeOrganizationSpecific]) VALUES (8, 'DumpReportMail', 'DumpReportMail', 0)
SET IDENTITY_INSERT [dbo].[TemplateType] OFF
SET IDENTITY_INSERT [dbo].[TemplateType] ON
INSERT INTO [dbo].[TemplateType] ([TemplateTypeID], [Name], [Description], [CanBeOrganizationSpecific]) VALUES (9, 'LogReadMail', 'LogReadMail', 0)
SET IDENTITY_INSERT [dbo].[TemplateType] OFF
