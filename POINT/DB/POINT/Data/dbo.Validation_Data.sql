SET IDENTITY_INSERT [dbo].[Validation] ON
INSERT INTO [dbo].[Validation] ([ValidationID], [Method], [FieldAttributes], [WarningTriggers], [ErrorTriggers], [Messages]) VALUES (2, 'DateDiffMax60days', '{ Members : [149, 73]}', '{ Members : ["Load", "Blur"]}', '', '{ Members: ["{0} en {1} liggen ver uit elkaar.", "{1} ligt vóór {0}."]}')
SET IDENTITY_INSERT [dbo].[Validation] OFF
