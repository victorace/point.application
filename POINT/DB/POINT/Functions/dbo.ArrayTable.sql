
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[ArrayTable]
(	@Text		VARCHAR (max),
	@Delimiter	VARCHAR (50)		)  
RETURNS @Table TABLE 
(	Pos	INT IDENTITY(1,1),	
	Value	VARCHAR (255)		)
AS  
BEGIN 

	IF @Text <> '' AND @Text IS NOT NULL
	BEGIN
		WHILE CHARINDEX ( @Delimiter, @Text ) <> 0
		BEGIN
			INSERT INTO @Table (Value)
			SELECT LEFT(@Text, CHARINDEX ( @Delimiter , @Text ) - 1) 

			SET @Text = RIGHT(@Text, LEN(@Text) -  CHARINDEX ( @Delimiter , @Text ))
			--SET @Text = SUBSTRING(@Text, CHARINDEX ( @Delimiter , @Text ) + LEN(@Delimiter) + 1, LEN(@Text))
		END
		INSERT INTO @Table (Value)
		VALUES (@Text)
	END
	RETURN
END

GO
