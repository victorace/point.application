SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[ArrayTableInt]
(
    @Text        VARCHAR (max),
    @Delimiter    CHAR (1)
)  
RETURNS @Table TABLE 
(
    Pos INT IDENTITY(1,1),    
    Value INT
)
AS  

BEGIN 

    IF @Text <> '' AND @Text IS NOT NULL
    BEGIN
        INSERT INTO @Table (Value)
        SELECT Value
          FROM dbo.ArrayTable2(@Text, @Delimiter)
         WHERE ISNUMERIC(Value) = 1
         ORDER BY POS
    END

    RETURN
END
GO
