SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[ArrayTableIntBlanks]
(
    @Text         VARCHAR (max),
    @Delimiter    CHAR (1),
    @Blanks       VARCHAR(50) = NULL,
    @BlanksNumber INT = -999
)  
RETURNS @Table TABLE 
(
    Pos INT IDENTITY(1,1),    
    Value INT
)
AS  

BEGIN 

    IF @Text <> '' AND @Text IS NOT NULL
    BEGIN
        INSERT INTO @Table (Value)
        SELECT REPLACE(Value, @Blanks, @BlanksNumber) 
          FROM dbo.ArrayTable2(@Text, @Delimiter)
         WHERE ISNUMERIC(REPLACE(Value, @Blanks, @BlanksNumber)) = 1
         ORDER BY POS
    END

    RETURN
END

GO
