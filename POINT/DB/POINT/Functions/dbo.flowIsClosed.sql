SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[flowIsClosed] ( @transferID INT )
RETURNS BIT
AS
    BEGIN


        DECLARE @PhaseDefinitionsEnd TABLE
            (
              PhaseDefinitionID INT
            );

        INSERT  INTO @PhaseDefinitionsEnd
                SELECT  PhaseDefinitionID
                FROM    PhaseDefinition WITH ( NOLOCK )
                WHERE   EndFlow = 1;

        DECLARE @status INT = 0;
        SELECT TOP 1
                @status = ISNULL(Status, 0)
        FROM    FlowInstance WITH ( NOLOCK )
                JOIN PhaseInstance WITH ( NOLOCK ) ON PhaseInstance.FlowInstanceID = FlowInstance.FlowInstanceID
                JOIN @PhaseDefinitionsEnd pde ON pde.PhaseDefinitionID = PhaseInstance.PhaseDefinitionID
        WHERE   TransferID = @transferID
        ORDER BY PhaseInstanceID DESC;
       
        RETURN CASE @status WHEN 2 THEN 1 ELSE 0 END;
	
    END;		


GO
