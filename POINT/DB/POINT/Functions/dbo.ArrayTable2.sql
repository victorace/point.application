SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- ===========================================================================
-- Author:		Phil Clymans
-- Create date: 10-11-2010
-- Description:	Old version can truncate input, this version uses VARCHAR(MAX)
-- ===========================================================================

CREATE FUNCTION [dbo].[ArrayTable2]
(	@Text		VARCHAR (MAX),
	@Delimiter	VARCHAR (50)		)  
RETURNS @Table TABLE 
(	Pos	INT IDENTITY(1,1),	
	Value	VARCHAR (MAX)		)
AS  
BEGIN 

	IF @Text <> '' AND @Text IS NOT NULL
	BEGIN
		WHILE CHARINDEX ( @Delimiter, @Text ) <> 0
		BEGIN
			INSERT INTO @Table (Value)
			SELECT LEFT(@Text, CHARINDEX ( @Delimiter , @Text ) - 1) 

			SET @Text = RIGHT(@Text, LEN(@Text) -  CHARINDEX ( @Delimiter , @Text ))
		END
		INSERT INTO @Table (Value)
		VALUES (@Text)
	END
	RETURN
END


GO
