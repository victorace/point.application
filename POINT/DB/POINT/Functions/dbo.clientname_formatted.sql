SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION  [dbo].[clientname_formatted]
(@clientid INT)  
RETURNS VARCHAR (100) AS  

BEGIN 
DECLARE @Return VARCHAR(100)

SELECT @Return =
	CASE LEN(lastname)
	WHEN 0 THEN ''
	ELSE
		CASE ClientIsAnonymous 
		WHEN 1 THEN
			UPPER(SUBSTRING(lastname,1,1)) + FORMAT(BirthDate, 'ddMM')
		ELSE 
			LTRIM(RTRIM(UPPER(SUBSTRING(lastname,1,1)) + SUBSTRING(lastname,2,LEN(lastname)-1) + ISNULL(', ' + initials,'') + ISNULL(' ' + firstname,'') + ISNULL(' ' + middlename,'')))
		END
	END
FROM
	client with(nolock)
WHERE
	clientid = @clientid

RETURN @Return

END
GO
