CREATE TABLE [dbo].[FormTypeOrganization]
(
[FormTypeOrganizationID] [int] NOT NULL IDENTITY(1, 1),
[FormTypeID] [int] NOT NULL,
[OrganizationID] [int] NOT NULL,
[FlowDefinitionID] [int] NOT NULL CONSTRAINT [DF_FormTypeOrganization_FlowDefinitionID] DEFAULT ((2)),
[ModifiedTimeStamp] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FormTypeOrganization] ADD CONSTRAINT [PK_FormTypeOrganization] PRIMARY KEY CLUSTERED  ([FormTypeOrganizationID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FormTypeOrganization] ADD CONSTRAINT [FK_FormTypeOrganization_FormType] FOREIGN KEY ([FormTypeID]) REFERENCES [dbo].[FormType] ([FormTypeID])
GO
ALTER TABLE [dbo].[FormTypeOrganization] ADD CONSTRAINT [FK_FormTypeOrganization_Organization] FOREIGN KEY ([OrganizationID]) REFERENCES [dbo].[Organization] ([OrganizationID])
GO
