CREATE TABLE [dbo].[FieldReceivedTemp]
(
[FieldReceivedTempID] [int] NOT NULL IDENTITY(1, 1),
[MessageReceivedTempID] [int] NULL,
[Timestamp] [datetime] NOT NULL,
[FieldName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FieldValue] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[FieldReceivedTemp] ADD CONSTRAINT [PK_dbo.FieldReceivedTemp] PRIMARY KEY CLUSTERED  ([FieldReceivedTempID]) ON [PRIMARY]
GO
