CREATE TABLE [dbo].[ServiceArea]
(
[ServiceAreaID] [int] NOT NULL IDENTITY(1, 1),
[Description] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Inactive] [bit] NOT NULL CONSTRAINT [DF_ServiceArea_Inactive] DEFAULT ((0)),
[OrganizationID] [int] NULL,
[ModifiedTimeStamp] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ServiceArea] ADD CONSTRAINT [PK_ServiceArea] PRIMARY KEY CLUSTERED  ([ServiceAreaID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ServiceArea] ADD CONSTRAINT [FK_ServiceArea_Organization] FOREIGN KEY ([OrganizationID]) REFERENCES [dbo].[Organization] ([OrganizationID])
GO
