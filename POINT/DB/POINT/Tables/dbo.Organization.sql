CREATE TABLE [dbo].[Organization]
(
[OrganizationID] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OrganizationTypeID] [int] NOT NULL,
[NedapCryptoCertificateID] [int] NULL,
[Inactive] [bit] NOT NULL CONSTRAINT [DF_Organization_Inactive] DEFAULT ((0)),
[HL7Interface] [bit] NOT NULL CONSTRAINT [DF_Organization_HL7Interface] DEFAULT ('FALSE'),
[WSInterface] [bit] NOT NULL CONSTRAINT [DF_Organization_WSInterface] DEFAULT ('FALSE'),
[InterfaceNumber] [int] NULL,
[RegionID] [int] NULL,
[ChangePasswordPeriod] [int] NULL,
[ShortConnectionID] [bit] NOT NULL CONSTRAINT [DF_Organization_ShortConnectionID] DEFAULT ((0)),
[AGB] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[URL] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HL7InterfaceExtraFields] [bit] NOT NULL,
[HL7ConnectionDifferentMenu] [bit] NOT NULL CONSTRAINT [DF_Organization_HL7ConnectionDifferentMenu] DEFAULT ((0)),
[HL7ConnectionCallFromPoint] [bit] NOT NULL CONSTRAINT [DF_Organization_HL7ConnectionCallFromPoint] DEFAULT ((0)),
[ShowUnloadMessage] [bit] NOT NULL CONSTRAINT [DF_Organization_ShowUnloadMessage] DEFAULT ((1)),
[Image] [bit] NOT NULL CONSTRAINT [DF_Organization_Image] DEFAULT ((0)),
[ImageName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SSO] [bit] NOT NULL CONSTRAINT [DF_Organization_SSO] DEFAULT ((0)),
[SSOKey] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModifiedTimeStamp] [datetime] NULL,
[ModifiedByEmployeeID] [int] NULL,
[ModifiedByScreenID] [int] NULL,
[MessageToGPByClose] [bit] NOT NULL CONSTRAINT [DF_Organization_MessageToGPByClose] DEFAULT ((0)),
[MessageToGPByDefinitive] [bit] NOT NULL CONSTRAINT [DF_Organization_MessageToGPByDefinitive] DEFAULT ((0)),
[ZorgmailUsername] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ZorgmailPassword] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WSInterfaceExtraFields] [bit] NOT NULL CONSTRAINT [DF_Organization_WSInterfaceExtraFields] DEFAULT ('FALSE'),
[HashPrefix] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PageLockTimeout] [int] NULL,
[CreateUserViaSSO] [bit] NOT NULL CONSTRAINT [DF_Organization_CreateUserViaSSO] DEFAULT ((0)),
[SSOHashType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UsesPalliativeCareInVO] [bit] NOT NULL CONSTRAINT [DF_Organization_UsesPalliativeCareInVO] DEFAULT ((0)),
[MFARequired] [bit] NOT NULL CONSTRAINT [DF_Organization_UsesTwoFactorAuthentication] DEFAULT ((0)),
[MFAAuthType] [int] NULL,
[MFATimeout] [int] NULL,
[UseAnonymousClient] [bit] NOT NULL CONSTRAINT [DF_Organization_UseAnonymousClient] DEFAULT ((0)),
[UseDigitalSignature] [bit] NOT NULL CONSTRAINT [DF_Organization_HasDigitalSignature] DEFAULT ((0)),
[NedapMedewerkerNummer] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UseTasks] [bit] NOT NULL CONSTRAINT [DF_Organization_UseTasks] DEFAULT ((0)),
[IPAddresses] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DeBlockEmails] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DeBlockEmailsToOrganization] [bit] NOT NULL CONSTRAINT [DF_Organization_DeBlockEmailsToOrganization] DEFAULT ((0)),
[OrganizationLogoID] [int] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create trigger [dbo].[Organization_Auth_Sync_InsertUpdate]  
   on  [dbo].[Organization]  
   after insert, update
as   
begin  
  
	-- SET NOCOUNT ON added to prevent extra result sets from  
	-- interfering with SELECT statements.  
	set nocount on;  

	insert into dbo.AuthOrganization (OrganizationID, SSO, SSOKey, SSOHashType, CreateUserViaSSO, AGB)
	select OrganizationID, isnull(SSO, 0), SSOKey, SSOHashType, isnull(CreateUserViaSSO, 0), AGB from dbo.Organization
	where OrganizationID not in (select dbo.AuthOrganization.OrganizationID from dbo.AuthOrganization);

	update dbo.AuthOrganization 
		set SSO = isnull(Inserted.SSO, 0),
			SSOKey = Inserted.SSOKey,
			SSOHashType = Inserted.SSOHashType,
			CreateUserViaSSO = isnull(Inserted.CreateUserViaSSO, 0),
			AGB = Inserted.AGB
	from dbo.AuthOrganization  
		inner join Inserted on Inserted.OrganizationID = AuthOrganization.OrganizationID;

end;
GO
ALTER TABLE [dbo].[Organization] ADD CONSTRAINT [PK_Organization] PRIMARY KEY CLUSTERED  ([OrganizationID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Organization_Inactive] ON [dbo].[Organization] ([Inactive]) INCLUDE ([OrganizationID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Organization_Inactive_Includes] ON [dbo].[Organization] ([Inactive]) INCLUDE ([OrganizationID], [OrganizationTypeID], [RegionID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Organization_Inactive_RegionID_Includes] ON [dbo].[Organization] ([Inactive], [RegionID]) INCLUDE ([Name], [OrganizationID], [OrganizationTypeID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Organization_OrganizationID_Includes] ON [dbo].[Organization] ([OrganizationID]) INCLUDE ([AGB], [ChangePasswordPeriod], [CreateUserViaSSO], [DeBlockEmails], [DeBlockEmailsToOrganization], [HashPrefix], [HL7ConnectionCallFromPoint], [HL7ConnectionDifferentMenu], [HL7Interface], [HL7InterfaceExtraFields], [Image], [ImageName], [Inactive], [InterfaceNumber], [IPAddresses], [MessageToGPByClose], [MessageToGPByDefinitive], [MFAAuthType], [MFARequired], [MFATimeout], [ModifiedByEmployeeID], [ModifiedByScreenID], [ModifiedTimeStamp], [Name], [NedapCryptoCertificateID], [NedapMedewerkerNummer], [OrganizationTypeID], [PageLockTimeout], [RegionID], [ShortConnectionID], [ShowUnloadMessage], [SSO], [SSOHashType], [SSOKey], [URL], [UseAnonymousClient], [UseDigitalSignature], [UsesPalliativeCareInVO], [UseTasks], [WSInterface], [WSInterfaceExtraFields], [ZorgmailPassword], [ZorgmailUsername]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Organization_OrganizationID_OrganizationTypeID] ON [dbo].[Organization] ([OrganizationID]) INCLUDE ([Name], [OrganizationTypeID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Organization_RegionID] ON [dbo].[Organization] ([RegionID]) INCLUDE ([OrganizationID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Organization] ADD CONSTRAINT [FK_Organization_OrganizationLogo] FOREIGN KEY ([OrganizationLogoID]) REFERENCES [dbo].[OrganizationLogo] ([OrganizationLogoID])
GO
ALTER TABLE [dbo].[Organization] ADD CONSTRAINT [FK_Organization_Region] FOREIGN KEY ([RegionID]) REFERENCES [dbo].[Region] ([RegionID])
GO
