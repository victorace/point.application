CREATE TABLE [dbo].[AfterCareTypeAfterCareFinancing]
(
[AfterCareTypeID] [int] NOT NULL,
[AfterCareFinancingID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AfterCareTypeAfterCareFinancing] ADD CONSTRAINT [PK_AfterCareTypeAfterCareFinancing] PRIMARY KEY CLUSTERED  ([AfterCareTypeID], [AfterCareFinancingID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AfterCareTypeAfterCareFinancing] ADD CONSTRAINT [FK_AfterCareTypeAfterCareFinancing_AfterCareFinancing] FOREIGN KEY ([AfterCareFinancingID]) REFERENCES [dbo].[AfterCareFinancing] ([AfterCareFinancingID])
GO
ALTER TABLE [dbo].[AfterCareTypeAfterCareFinancing] ADD CONSTRAINT [FK_AfterCareTypeAfterCareFinancing_AfterCareType] FOREIGN KEY ([AfterCareTypeID]) REFERENCES [dbo].[AfterCareType] ([AfterCareTypeID])
GO
