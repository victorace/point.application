CREATE TABLE [dbo].[OrganizationType]
(
[OrganizationTypeID] [int] NOT NULL,
[Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CanAddTransferPoint] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[OrganizationType] ADD CONSTRAINT [PK_OrganizationType] PRIMARY KEY CLUSTERED  ([OrganizationTypeID]) ON [PRIMARY]
GO
