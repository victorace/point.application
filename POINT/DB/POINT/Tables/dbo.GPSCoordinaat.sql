CREATE TABLE [dbo].[GPSCoordinaat]
(
[GPSCoordinaatID] [int] NOT NULL,
[Straat] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HuisNrVan] [int] NULL,
[HuisNrTot] [int] NULL,
[Postcode] [nvarchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GPSPlaatsID] [int] NULL,
[GPSGemeenteID] [int] NULL,
[GPSProvincieID] [int] NULL,
[Latitude] [float] NULL,
[Longitude] [float] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[GPSCoordinaat] ADD CONSTRAINT [PK_GPSCoordinaat] PRIMARY KEY CLUSTERED  ([GPSCoordinaatID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_GPSCoordinaat_LatitudeLongitude] ON [dbo].[GPSCoordinaat] ([Latitude], [Longitude]) INCLUDE ([Postcode]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_GPSCoordinaat] ON [dbo].[GPSCoordinaat] ([Postcode]) INCLUDE ([GPSCoordinaatID], [GPSGemeenteID], [GPSPlaatsID], [GPSProvincieID], [HuisNrTot], [HuisNrVan], [Latitude], [Longitude], [Straat]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[GPSCoordinaat] ADD CONSTRAINT [FK_GPSCoordinaat_GPSGemeente] FOREIGN KEY ([GPSGemeenteID]) REFERENCES [dbo].[GPSGemeente] ([GPSGemeenteID])
GO
ALTER TABLE [dbo].[GPSCoordinaat] ADD CONSTRAINT [FK_GPSCoordinaat_GPSPlaats] FOREIGN KEY ([GPSPlaatsID]) REFERENCES [dbo].[GPSPlaats] ([GPSPlaatsID])
GO
ALTER TABLE [dbo].[GPSCoordinaat] ADD CONSTRAINT [FK_GPSCoordinaat_GPSProvincie] FOREIGN KEY ([GPSProvincieID]) REFERENCES [dbo].[GPSProvincie] ([GPSProvincieID])
GO
