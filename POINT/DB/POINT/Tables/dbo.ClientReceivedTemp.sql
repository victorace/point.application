CREATE TABLE [dbo].[ClientReceivedTemp]
(
[TimeStamp] [datetime] NOT NULL CONSTRAINT [DF_ClientReceivedTemp_TimeStamp] DEFAULT (getdate()),
[GUID] [uniqueidentifier] NOT NULL,
[OrganisationID] [int] NOT NULL,
[SenderUserName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SendDate] [datetime] NOT NULL,
[PatientNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Hash] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CivilServiceNumber] [varchar] (9) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Gender] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Salutation] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Initials] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirstName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MiddleName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaidenName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BirthDate] [datetime] NULL,
[StreetName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Number] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PostalCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Country] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PhoneNumberGeneral] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PhoneNumberMobile] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PhoneNumberWork] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContactPersonName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContactPersonRelationType] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContactPersonBirthDate] [datetime] NULL,
[ContactPersonPhoneNumberGeneral] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContactPersonPhoneNumberMobile] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContactPersonPhoneNumberWork] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HealthInsuranceCompanyUZOVICode] [int] NULL,
[InsuranceNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GeneralPractitionerName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GeneralPractitionerPhoneNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CivilClass] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CompositionHousekeeping] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChildrenInHousekeeping] [int] NULL,
[HousingType] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HealthCareProvider] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AddressGPForZorgmail] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[ClientReceivedTemp_ins] ON [dbo].[ClientReceivedTemp]
    FOR INSERT
AS
    BEGIN 

        DECLARE @OrganizationID INT ,
            @PatientNumber VARCHAR(50) ,
            @CivilServiceNumber VARCHAR(9) ,
            @Gender VARCHAR(1) ,
            @Salutation VARCHAR(255) ,
            @Initials VARCHAR(50) ,
            @FirstName VARCHAR(255) ,
            @MiddleName VARCHAR(50) ,
            @LastName VARCHAR(255) ,
            @MaidenName VARCHAR(255) ,
            @BirthDate DATETIME ,
            @StreetName VARCHAR(255) ,
            @Number VARCHAR(255) ,
            @PostalCode VARCHAR(255) ,
            @City VARCHAR(255) ,
            @Country VARCHAR(255) ,
            @PhoneNumberGeneral VARCHAR(255) ,
            @PhoneNumberMobile VARCHAR(255) ,
            @PhoneNumberWork VARCHAR(255);

        SELECT  @OrganizationID = OrganisationID ,
                @PatientNumber = PatientNumber ,
                @CivilServiceNumber = CivilServiceNumber ,
                @Gender = Gender ,
                @Salutation = Salutation ,
                @Initials = Initials ,
                @FirstName = FirstName ,
                @MiddleName = MiddleName ,
                @LastName = LastName ,
                @MaidenName = MaidenName ,
                @BirthDate = BirthDate ,
                @StreetName = StreetName ,
                @Number = Number ,
                @PostalCode = PostalCode ,
                @City = City ,
                @Country = Country ,
                @PhoneNumberGeneral = PhoneNumberGeneral ,
                @PhoneNumberMobile = PhoneNumberMobile ,
                @PhoneNumberWork = PhoneNumberWork
        FROM    INSERTED;

        IF EXISTS ( SELECT  1
                    FROM    Organization WITH ( NOLOCK )
                            INNER JOIN Location WITH ( NOLOCK ) ON Organization.OrganizationID = Location.OrganizationID
                            INNER JOIN Department WITH ( NOLOCK ) ON Location.LocationID = Department.LocationID
                            INNER JOIN FlowInstance WITH ( NOLOCK ) ON FlowInstance.StartedByDepartmentID = Department.DepartmentID
                            INNER JOIN Transfer WITH ( NOLOCK ) ON Transfer.TransferID = FlowInstance.TransferID
                            INNER JOIN Client WITH ( NOLOCK ) ON Client.ClientID = Transfer.ClientID
                    WHERE   Client.PatientNumber = @PatientNumber
                            AND Organization.OrganizationID = @OrganizationID
                            AND Organization.Inactive = 0
                            AND Location.Inactive = 0
                            AND Department.Inactive = 0 )
            BEGIN

                SET NOCOUNT ON;

                DECLARE @flowinstanceclient TABLE
                    (
                      RowID INT IDENTITY(1, 1) ,
                      FlowInstanceID INT ,
                      ClientID INT
                    );
                INSERT  INTO @flowinstanceclient
                        ( FlowInstanceID ,
                          ClientID
						 )
                        SELECT  FlowInstance.FlowInstanceID ,
                                Transfer.ClientID
                        FROM    FlowInstance WITH ( NOLOCK )
                                INNER JOIN Transfer WITH ( NOLOCK ) ON Transfer.TransferID = FlowInstance.TransferID
                                INNER JOIN Client WITH ( NOLOCK ) ON Client.ClientID = Transfer.ClientID
                                INNER JOIN Department WITH ( NOLOCK ) ON Department.DepartmentID = FlowInstance.StartedByDepartmentID
                                INNER JOIN Location WITH ( NOLOCK ) ON Location.LocationID = Department.LocationID
                                INNER JOIN Organization WITH ( NOLOCK ) ON Organization.OrganizationID = Location.OrganizationID
                        WHERE   FlowInstance.StartedByOrganizationID = @OrganizationID
                                AND Client.PatientNumber = @PatientNumber
                                AND Organization.Inactive = 0
                                AND Location.Inactive = 0
                                AND Department.Inactive = 0
                                AND ( dbo.flowIsClosed(Transfer.TransferID) = 0 );

                UPDATE  [Client]
                SET     [CivilServiceNumber] = @CivilServiceNumber ,
                        [PatientNumber] = @PatientNumber ,
                        [Gender] = @Gender ,
                        [Salutation] = @Salutation ,
                        [Initials] = @Initials ,
                        [FirstName] = @FirstName ,
                        [MiddleName] = @MiddleName ,
                        [LastName] = @LastName ,
                        [MaidenName] = @MaidenName ,
                        [BirthDate] = @BirthDate ,
                        [StreetName] = @StreetName ,
                        [Number] = @Number ,
                        [PostalCode] = @PostalCode ,
                        [City] = @City ,
                        [Country] = @Country ,
                        [PhoneNumberGeneral] = @PhoneNumberGeneral ,
                        [PhoneNumberMobile] = @PhoneNumberMobile ,
                        [PhoneNumberWork] = @PhoneNumberWork
                WHERE   Client.ClientID IN ( SELECT ClientID
                                             FROM   @flowinstanceclient );

                DECLARE @maxrowid INT;
                SELECT  @maxrowid = MAX(RowID)
                FROM    @flowinstanceclient;

                DECLARE @rowid INT = 1;
                DECLARE @flowinstanceid INT;
                DECLARE @clientid INT;

                WHILE @rowid <= @maxrowid
                    BEGIN
                        SELECT  @flowinstanceid = FlowInstanceID ,
                                @clientid = ClientID
                        FROM    @flowinstanceclient
                        WHERE   RowID = @rowid;

                        UPDATE  dbo.FlowInstanceSearchValues
                        SET     ClientCivilServiceNumber = @CivilServiceNumber ,
                                ClientBirthDate = @BirthDate ,
                                ClientFullname = dbo.clientname_formatted(@clientid) ,
                                ClientGender = @Gender
                        WHERE   FlowInstanceID = @flowinstanceid;

                        SELECT  @rowid = @rowid + 1;

                    END;
            END;
    END;
GO
ALTER TABLE [dbo].[ClientReceivedTemp] ADD CONSTRAINT [PK_ClientReceivedTemp_1] PRIMARY KEY CLUSTERED  ([TimeStamp], [GUID], [OrganisationID]) ON [PRIMARY]
GO
