CREATE TABLE [dbo].[FlowFieldAttributeException]
(
[FlowFieldAttributeExceptionID] [int] NOT NULL IDENTITY(1, 1),
[FlowFieldAttributeID] [int] NOT NULL,
[RegionID] [int] NULL,
[OrganizationID] [int] NULL,
[ReadOnly] [bit] NOT NULL CONSTRAINT [DF_FlowFieldAttributeException_ReadOnly] DEFAULT ((0)),
[Visible] [bit] NOT NULL CONSTRAINT [DF_FlowFieldAttributeException_Visible] DEFAULT ((1)),
[Required] [bit] NOT NULL CONSTRAINT [DF_FlowFieldAttributeException_Required] DEFAULT ((0)),
[SignalOnChange] [bit] NOT NULL CONSTRAINT [DF_FlowFieldAttributeException_SignalOnChange] DEFAULT ((0)),
[PhaseDefinitionID] [int] NULL,
[RequiredByFlowFieldID] [int] NULL,
[RequiredByFlowFieldValue] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[FlowFieldAttributeException] ADD CONSTRAINT [PK_FlowFieldAttributeException] PRIMARY KEY CLUSTERED  ([FlowFieldAttributeExceptionID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FlowFieldAttributeException] ADD CONSTRAINT [FK_FlowFieldAttributeException_FlowField] FOREIGN KEY ([RequiredByFlowFieldID]) REFERENCES [dbo].[FlowField] ([FlowFieldID])
GO
ALTER TABLE [dbo].[FlowFieldAttributeException] ADD CONSTRAINT [FK_FlowFieldAttributeException_FlowFieldAttribute] FOREIGN KEY ([FlowFieldAttributeID]) REFERENCES [dbo].[FlowFieldAttribute] ([FlowFieldAttributeID])
GO
