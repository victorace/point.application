CREATE TABLE [dbo].[ActionCodeHealthInsurerFormType]
(
[ActionCodeHealthInsurerFormTypeID] [int] NOT NULL IDENTITY(1, 1),
[ActionCodeHealthInsurerID] [int] NOT NULL,
[FormTypeID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ActionCodeHealthInsurerFormType] ADD CONSTRAINT [PK_ActionCodeHealthInsurerFormTypeID] PRIMARY KEY CLUSTERED  ([ActionCodeHealthInsurerFormTypeID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ActionCodeHealthInsurerFormType] ADD CONSTRAINT [FK_ActionCodeHealthInsurerFormType_ActionCodeHealthInsurer] FOREIGN KEY ([ActionCodeHealthInsurerID]) REFERENCES [dbo].[ActionCodeHealthInsurer] ([ActionCodeHealthInsurerID])
GO
ALTER TABLE [dbo].[ActionCodeHealthInsurerFormType] ADD CONSTRAINT [FK_ActionCodeHealthInsurerFormType_FormType] FOREIGN KEY ([FormTypeID]) REFERENCES [dbo].[FormType] ([FormTypeID])
GO
