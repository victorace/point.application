CREATE TABLE [dbo].[AfterCareType]
(
[AfterCareTypeID] [int] NOT NULL,
[Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Abbreviation] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StartDate] [datetime] NOT NULL,
[Enddate] [datetime] NOT NULL,
[ReportCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AfterCareCategoryID] [int] NULL,
[AfterCareGroupID] [int] NULL,
[Sortorder] [int] NOT NULL,
[OrganizationTypeID] [int] NOT NULL CONSTRAINT [DF_AfterCareType_OrganizationTypeID] DEFAULT ((2)),
[Inactive] [bit] NOT NULL CONSTRAINT [DF_AfterCareType_Inactive] DEFAULT ((0)),
[Description] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AfterCareType] ADD CONSTRAINT [PK_AfterCareType] PRIMARY KEY CLUSTERED  ([AfterCareTypeID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AfterCareType] ADD CONSTRAINT [FK_AfterCareType_AfterCareCategory] FOREIGN KEY ([AfterCareCategoryID]) REFERENCES [dbo].[AfterCareCategory] ([AfterCareCategoryID])
GO
ALTER TABLE [dbo].[AfterCareType] ADD CONSTRAINT [FK_AfterCareType_AfterCareGroup] FOREIGN KEY ([AfterCareGroupID]) REFERENCES [dbo].[AfterCareGroup] ([AfterCareGroupID])
GO
