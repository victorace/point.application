CREATE TABLE [dbo].[FlowDefinitionParticipation]
(
[FlowDefinitionParticipationID] [int] NOT NULL IDENTITY(1, 1),
[FlowDefinitionID] [int] NOT NULL,
[LocationID] [int] NOT NULL,
[DepartmentID] [int] NULL,
[Participation] [int] NOT NULL CONSTRAINT [DF_FlowDefinitionParticipation_Participation] DEFAULT ((0)),
[FlowDirection] [int] NOT NULL CONSTRAINT [DF_FlowDefinitionParticipation_FlowDirection] DEFAULT ((2)),
[ModifiedTimeStamp] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FlowDefinitionParticipation] ADD CONSTRAINT [PK_FlowDefinitionParticipation] PRIMARY KEY CLUSTERED  ([FlowDefinitionParticipationID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FlowDefinitionParticipation] ADD CONSTRAINT [FK_FlowDefinitionParticipation_Department] FOREIGN KEY ([DepartmentID]) REFERENCES [dbo].[Department] ([DepartmentID])
GO
ALTER TABLE [dbo].[FlowDefinitionParticipation] ADD CONSTRAINT [FK_FlowDefinitionParticipation_FlowDefinition] FOREIGN KEY ([FlowDefinitionID]) REFERENCES [dbo].[FlowDefinition] ([FlowDefinitionID])
GO
ALTER TABLE [dbo].[FlowDefinitionParticipation] ADD CONSTRAINT [FK_FlowDefinitionParticipation_Location] FOREIGN KEY ([LocationID]) REFERENCES [dbo].[Location] ([LocationID])
GO
