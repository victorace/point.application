CREATE TABLE [dbo].[LogTransferProjects]
(
[LogTransferProjectsID] [int] NOT NULL IDENTITY(1, 1),
[TransferID] [int] NOT NULL,
[ProjectID] [int] NOT NULL,
[TimeStamp] [datetime] NULL,
[EmployeeID] [int] NULL,
[Action] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ScreenID] [int] NULL,
[Version] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[LogTransferProjects] ADD CONSTRAINT [PK_LogTransferProjects] PRIMARY KEY CLUSTERED  ([LogTransferProjectsID]) ON [PRIMARY]
GO
