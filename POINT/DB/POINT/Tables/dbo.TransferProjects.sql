CREATE TABLE [dbo].[TransferProjects]
(
[TransferID] [int] NOT NULL,
[ProjectID] [int] NOT NULL,
[TimeStamp] [datetime] NULL,
[EmployeeID] [int] NULL,
[ScreenID] [int] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- ========================================================================
-- Author:		Phil Clymans
-- Create date: 26-04-2010
-- Description:	Logging
-- ========================================================================
CREATE TRIGGER [dbo].[TransferProjects_InsertUpdate]
   ON  [dbo].[TransferProjects] 
   AFTER UPDATE, INSERT
AS 
BEGIN

	SET NOCOUNT ON;

	IF (SELECT COUNT(*) FROM Inserted) = 0
	BEGIN
		RETURN
	END

	IF (SELECT Enabled FROM Logging) = 0
	BEGIN
		RETURN
	END

	-- BIJWERKEN LOGGING
	DECLARE	@LogTransferProjectsID	INT,
	@TransferID				INT,
	@ProjectID				INT,
	@TimeStamp				DATETIME,
	@EmployeeID				INT,
	@Action					VARCHAR(1),
	@ScreenID				INT,
	@Version				INT

	SET NOCOUNT ON;
	
	-- Check of timestamp wel een update krijgt. Zoniet dan is er sprake
	-- van een oude SP die de update uitvoerd.
	IF UPDATE([TimeStamp])
	BEGIN
		SELECT
			@TimeStamp  = [TimeStamp],
			@EmployeeID	= [EmployeeID],
			@ScreenID	= [ScreenID]
		FROM Inserted
	END

	SELECT @TransferID = TransferID,
		   @ProjectID = ProjectID
	FROM Inserted

	IF exists (select * from deleted)
		SELECT @Action = 'U'
	ELSE
		SELECT @Action = 'I'
	
	SELECT @Version = (SELECT ISNULL(MAX(Version), 0) + 1 FROM LogTransferProjects where LogTransferProjects.TransferID = @TransferID AND LogTransferProjects.ProjectID = @ProjectID)

	INSERT INTO LogTransferProjects
			   (TransferID,
				ProjectID,
			    [TimeStamp],
				EmployeeID,
				[Action],
				ScreenID,
				[Version])
		 VALUES
			(@TransferID,
			 @ProjectID,
			 @TimeStamp,
			 @EmployeeID,
			 @Action,
			 @ScreenID,
			 @Version)

	SET @LogTransferProjectsID = SCOPE_IDENTITY();

	DECLARE @MutTransferProjectsID INT
	
	INSERT INTO MutTransferProjects(
		LogTransferProjectsID,	
		TransferID,
		ProjectID,
		TimeStamp,
		EmployeeID,
		ScreenID)
	SELECT @LogTransferProjectsID, * from Inserted

	SET @MutTransferProjectsID = SCOPE_IDENTITY();

END



GO
ALTER TABLE [dbo].[TransferProjects] ADD CONSTRAINT [PK_TransferProjects] PRIMARY KEY CLUSTERED  ([TransferID], [ProjectID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TransferProjects] ADD CONSTRAINT [FK_TransferProjects_Project] FOREIGN KEY ([ProjectID]) REFERENCES [dbo].[Project] ([ProjectID])
GO
ALTER TABLE [dbo].[TransferProjects] ADD CONSTRAINT [FK_TransferProjects_Transfer] FOREIGN KEY ([TransferID]) REFERENCES [dbo].[Transfer] ([TransferID])
GO
