CREATE TABLE [dbo].[FlowInstanceSearchValues]
(
[FlowInstanceID] [int] NOT NULL,
[TransferID] [int] NOT NULL,
[IsInterrupted] [bit] NOT NULL,
[CopyOfTransferID] [int] NULL,
[ActivePhaseText] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AcceptedBy] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AcceptedByID] [int] NULL,
[AcceptedByII] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AcceptedByIIID] [int] NULL,
[AcceptedByVVT] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AcceptedByVVTID] [int] NULL,
[AcceptedByTelephoneNumber] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AcceptedDateTP] [datetime] NULL,
[BehandelaarSpecialisme] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CareBeginDate] [datetime] NULL,
[ClientBirthDate] [datetime] NULL,
[ClientFullname] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClientGender] [nvarchar] (18) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DatumEindeBehandelingMedischSpecialist] [datetime] NULL,
[FullDepartmentName] [nvarchar] (512) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DepartmentName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DesiredHealthCareProvider] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DestinationHospital] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DestinationHospitalDepartment] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DischargePatientAcceptedYNByVVT] [nvarchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DischargeProposedStartDate] [datetime] NULL,
[LastAdditionalTPMemoDateTime] [datetime] NULL,
[LastTransferAttachmentUploadDate] [datetime] NULL,
[LastTransferMemoDateTime] [datetime] NULL,
[LocationName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OrganizationName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RequestFormZHVVTType] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransferCreatedDate] [datetime] NULL,
[TransferDate] [datetime] NULL,
[TransferDateVVT] [datetime] NULL,
[TyperingNazorgCombined] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ScopeType] [int] NULL,
[FlowDefinitionName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RequestTransferPointIntakeDate] [datetime] NULL,
[ClientCivilServiceNumber] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransferCreatedByID] [int] NULL,
[TransferCreatedBy] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DestinationHospitalLocation] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BehandelaarNaam] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MedischeSituatieDatumOpname] [datetime] NULL,
[AcceptedByTelephoneVVT] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ZorgInZorginstellingVoorkeurPatient1] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HealthInsuranceCompany] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HealthInsuranceCompanyID] [int] NULL,
[PatientNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReadModelTimeStamp] [datetime] NULL,
[GewensteIngangsdatum] [datetime] NULL,
[TransferTaskDueDate] [datetime] NULL,
[Deleted] [bit] NOT NULL CONSTRAINT [DF_FlowInstanceSearchValues_Deleted] DEFAULT ((0)),
[FlowDefinitionID] [int] NOT NULL CONSTRAINT [DF_FlowInstanceSearchValues_FlowDefinitionID] DEFAULT ((0)),
[LastGrzFormDateTime] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[FlowInstanceSearchValues] ADD CONSTRAINT [PK_FlowInstanceSearchValues_1] PRIMARY KEY CLUSTERED  ([FlowInstanceID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FlowInstanceSearchValues_AcceptedByID_Includes] ON [dbo].[FlowInstanceSearchValues] ([AcceptedByID]) INCLUDE ([AcceptedBy], [AcceptedByII], [AcceptedByIIID], [AcceptedByTelephoneNumber], [AcceptedByTelephoneVVT], [AcceptedByVVT], [AcceptedByVVTID], [AcceptedDateTP], [ActivePhaseText], [BehandelaarNaam], [BehandelaarSpecialisme], [CareBeginDate], [ClientBirthDate], [ClientCivilServiceNumber], [ClientFullname], [ClientGender], [CopyOfTransferID], [DatumEindeBehandelingMedischSpecialist], [DepartmentName], [DesiredHealthCareProvider], [DestinationHospital], [DestinationHospitalDepartment], [DestinationHospitalLocation], [DischargePatientAcceptedYNByVVT], [DischargeProposedStartDate], [FlowDefinitionName], [FlowInstanceID], [FullDepartmentName], [GewensteIngangsdatum], [HealthInsuranceCompany], [HealthInsuranceCompanyID], [IsInterrupted], [LastAdditionalTPMemoDateTime], [LastGrzFormDateTime], [LastTransferAttachmentUploadDate], [LastTransferMemoDateTime], [LocationName], [MedischeSituatieDatumOpname], [OrganizationName], [PatientNumber], [ReadModelTimeStamp], [RequestFormZHVVTType], [RequestTransferPointIntakeDate], [ScopeType], [TransferCreatedBy], [TransferCreatedByID], [TransferCreatedDate], [TransferDate], [TransferDateVVT], [TransferID], [TransferTaskDueDate], [TyperingNazorgCombined], [ZorgInZorginstellingVoorkeurPatient1]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FlowInstanceSearchValues_AcceptedByID_Includes_FlowInstanceID_ScopeType] ON [dbo].[FlowInstanceSearchValues] ([AcceptedByID]) INCLUDE ([FlowInstanceID], [ScopeType]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FlowInstanceSearchValues_AcceptedByID_AcceptedByIIID_AcceptedByVVTID] ON [dbo].[FlowInstanceSearchValues] ([AcceptedByID], [AcceptedByIIID], [AcceptedByVVTID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FlowInstanceSearchValues_ClientFullNameScopeType_All] ON [dbo].[FlowInstanceSearchValues] ([ClientFullname], [ScopeType]) INCLUDE ([AcceptedBy], [AcceptedByID], [AcceptedByII], [AcceptedByIIID], [AcceptedByTelephoneNumber], [AcceptedByTelephoneVVT], [AcceptedByVVT], [AcceptedByVVTID], [AcceptedDateTP], [ActivePhaseText], [BehandelaarNaam], [BehandelaarSpecialisme], [CareBeginDate], [ClientBirthDate], [ClientCivilServiceNumber], [ClientGender], [CopyOfTransferID], [DatumEindeBehandelingMedischSpecialist], [DepartmentName], [DesiredHealthCareProvider], [DestinationHospital], [DestinationHospitalDepartment], [DestinationHospitalLocation], [DischargePatientAcceptedYNByVVT], [DischargeProposedStartDate], [FlowDefinitionName], [FlowInstanceID], [FullDepartmentName], [GewensteIngangsdatum], [HealthInsuranceCompany], [HealthInsuranceCompanyID], [IsInterrupted], [LastAdditionalTPMemoDateTime], [LastGrzFormDateTime], [LastTransferAttachmentUploadDate], [LastTransferMemoDateTime], [LocationName], [MedischeSituatieDatumOpname], [OrganizationName], [PatientNumber], [ReadModelTimeStamp], [RequestFormZHVVTType], [RequestTransferPointIntakeDate], [TransferCreatedBy], [TransferCreatedByID], [TransferCreatedDate], [TransferDate], [TransferDateVVT], [TransferID], [TransferTaskDueDate], [TyperingNazorgCombined], [ZorgInZorginstellingVoorkeurPatient1]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FlowInstanceSearchValues_DeletedFlowDefinitionID_ScopeType] ON [dbo].[FlowInstanceSearchValues] ([Deleted], [FlowDefinitionID]) INCLUDE ([ScopeType]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FlowInstanceSearchValues_PatientNumberScopeType_All] ON [dbo].[FlowInstanceSearchValues] ([PatientNumber], [ScopeType]) INCLUDE ([AcceptedBy], [AcceptedByID], [AcceptedByII], [AcceptedByIIID], [AcceptedByTelephoneNumber], [AcceptedByTelephoneVVT], [AcceptedByVVT], [AcceptedByVVTID], [AcceptedDateTP], [ActivePhaseText], [BehandelaarNaam], [BehandelaarSpecialisme], [CareBeginDate], [ClientBirthDate], [ClientCivilServiceNumber], [ClientFullname], [ClientGender], [CopyOfTransferID], [DatumEindeBehandelingMedischSpecialist], [DepartmentName], [DesiredHealthCareProvider], [DestinationHospital], [DestinationHospitalDepartment], [DestinationHospitalLocation], [DischargePatientAcceptedYNByVVT], [DischargeProposedStartDate], [FlowDefinitionName], [FlowInstanceID], [FullDepartmentName], [GewensteIngangsdatum], [HealthInsuranceCompany], [HealthInsuranceCompanyID], [IsInterrupted], [LastAdditionalTPMemoDateTime], [LastGrzFormDateTime], [LastTransferAttachmentUploadDate], [LastTransferMemoDateTime], [LocationName], [MedischeSituatieDatumOpname], [OrganizationName], [ReadModelTimeStamp], [RequestFormZHVVTType], [RequestTransferPointIntakeDate], [TransferCreatedBy], [TransferCreatedByID], [TransferCreatedDate], [TransferDate], [TransferDateVVT], [TransferID], [TransferTaskDueDate], [TyperingNazorgCombined], [ZorgInZorginstellingVoorkeurPatient1]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FlowInstanceSearchValues_ScopeType_ClientFullname_Includes] ON [dbo].[FlowInstanceSearchValues] ([ScopeType], [ClientFullname]) INCLUDE ([FlowInstanceID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FlowInstanceID_ScopeType] ON [dbo].[FlowInstanceSearchValues] ([ScopeType], [Deleted], [FlowDefinitionID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_TransferID] ON [dbo].[FlowInstanceSearchValues] ([TransferID] DESC) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FlowInstanceSearchValues] ADD CONSTRAINT [FK_FlowInstanceSearchValues_FlowInstance] FOREIGN KEY ([FlowInstanceID]) REFERENCES [dbo].[FlowInstance] ([FlowInstanceID])
GO
