CREATE TABLE [dbo].[TransferMemoTemplate]
(
[TransferMemoTemplateID] [int] NOT NULL IDENTITY(1, 1),
[OrganizationID] [int] NOT NULL,
[TransferMemoTypeID] [int] NOT NULL,
[Name] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TemplateText] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[TransferMemoTemplate] ADD CONSTRAINT [PK_TransferMemoTemplate] PRIMARY KEY CLUSTERED  ([TransferMemoTemplateID]) ON [PRIMARY]
GO
