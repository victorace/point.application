CREATE TABLE [dbo].[AfterCareFinancing]
(
[AfterCareFinancingID] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Abbreviation] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ShowInCapacity] [bit] NOT NULL,
[StartDate] [datetime] NOT NULL,
[EndDate] [datetime] NOT NULL,
[Sortorder] [int] NOT NULL,
[ColorCode] [nchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_AfterCareFinancing_ColorCode] DEFAULT (N'#0097cf'),
[Description] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AfterCareFinancing] ADD CONSTRAINT [PK_AfterCareFinancing] PRIMARY KEY CLUSTERED  ([AfterCareFinancingID]) ON [PRIMARY]
GO
