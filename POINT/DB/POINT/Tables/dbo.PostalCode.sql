CREATE TABLE [dbo].[PostalCode]
(
[PostalCodeID] [int] NOT NULL IDENTITY(1, 1),
[StartPostalCode] [char] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EndPostalCode] [char] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModifiedTimeStamp] [datetime] NULL,
[EmployeeID] [int] NULL,
[ScreenID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PostalCode] ADD CONSTRAINT [PK_PostalCode] PRIMARY KEY CLUSTERED  ([PostalCodeID]) ON [PRIMARY]
GO
