CREATE TABLE [dbo].[UsedHashCodes]
(
[HashCode] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EmployeeID] [int] NULL,
[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF_usedhashcodes_createddate] DEFAULT (getdate()),
[UsedHashCodeId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_UsedHashCodes_UsedHashCodeId] DEFAULT (newid()),
[IsAuth] [bit] NOT NULL CONSTRAINT [DF_UsedHashCodes_IsSAML] DEFAULT ((0))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[UsedHashCodes] ADD CONSTRAINT [PK_UsedHashCodes] PRIMARY KEY CLUSTERED  ([UsedHashCodeId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[UsedHashCodes] ADD CONSTRAINT [FK_UsedHashCodes_Employee] FOREIGN KEY ([EmployeeID]) REFERENCES [dbo].[Employee] ([EmployeeID])
GO
