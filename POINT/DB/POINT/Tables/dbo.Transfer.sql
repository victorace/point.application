CREATE TABLE [dbo].[Transfer]
(
[TransferID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[TimeStamp] [datetime] NULL,
[EmployeeID] [int] NULL,
[ScreenID] [int] NULL,
[OriginalTransferID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Transfer] ADD CONSTRAINT [PK_Transfer] PRIMARY KEY CLUSTERED  ([TransferID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_ClientID] ON [dbo].[Transfer] ([ClientID]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Transfer_ClientID] ON [dbo].[Transfer] ([ClientID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Transfer_TransferID_ClientID] ON [dbo].[Transfer] ([TransferID]) INCLUDE ([ClientID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Transfer] ADD CONSTRAINT [FK_dbo.Transfer_dbo.Client_ClientID] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Client] ([ClientID])
GO
