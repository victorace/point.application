CREATE TABLE [dbo].[AttachmentReceivedTemp]
(
[AttachmentReceivedTempID] [int] NOT NULL IDENTITY(1, 1),
[Timestamp] [datetime] NOT NULL,
[GUID] [uniqueidentifier] NOT NULL,
[OrganisationID] [int] NOT NULL,
[SenderUserName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SendDate] [datetime] NOT NULL,
[PatientNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CivilServiceNumber] [varchar] (9) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VisitNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BirthDate] [datetime] NOT NULL,
[Hash] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AttachmentTypeID] [int] NOT NULL,
[Base64EncodedData] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FileName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[AttachmentReceivedTemp] ADD CONSTRAINT [PK_AttachmentReceivedTemp] PRIMARY KEY CLUSTERED  ([AttachmentReceivedTempID]) ON [PRIMARY]
GO
