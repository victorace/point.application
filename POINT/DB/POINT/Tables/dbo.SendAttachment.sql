CREATE TABLE [dbo].[SendAttachment]
(
[SendAttachmentID] [int] NOT NULL IDENTITY(1, 1),
[SendDestinationType] [int] NULL,
[OrganizationID] [int] NULL,
[AttachmentTypeID] [int] NOT NULL,
[Allowed] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SendAttachment] ADD CONSTRAINT [PK_SendAttachment] PRIMARY KEY CLUSTERED  ([SendAttachmentID]) ON [PRIMARY]
GO
