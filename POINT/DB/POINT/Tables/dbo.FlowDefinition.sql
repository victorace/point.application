CREATE TABLE [dbo].[FlowDefinition]
(
[FlowDefinitionID] [int] NOT NULL IDENTITY(1, 1),
[FlowTypeID] [int] NOT NULL,
[Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InActive] [bit] NOT NULL,
[DateBegin] [datetime] NOT NULL,
[DateEnd] [datetime] NOT NULL,
[EmployeeID] [int] NULL,
[Timestamp] [datetime] NULL,
[ScreenName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HasFormTab] [bit] NOT NULL CONSTRAINT [DF_FlowDefinition_HasFormTab] DEFAULT ((0)),
[SortOrder] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FlowDefinition] ADD CONSTRAINT [PK_dbo.FlowDefinition] PRIMARY KEY CLUSTERED  ([FlowDefinitionID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FlowTypeID] ON [dbo].[FlowDefinition] ([FlowTypeID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FlowDefinition] ADD CONSTRAINT [FK_dbo.FlowDefinition_dbo.FlowType_FlowTypeID] FOREIGN KEY ([FlowTypeID]) REFERENCES [dbo].[FlowType] ([FlowTypeID])
GO
