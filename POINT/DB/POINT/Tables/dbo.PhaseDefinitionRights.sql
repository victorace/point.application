CREATE TABLE [dbo].[PhaseDefinitionRights]
(
[PhaseDefinitionRightsID] [int] NOT NULL IDENTITY(1, 1),
[PhaseDefinitionID] [int] NOT NULL,
[OrganizationTypeID] [int] NULL,
[DepartmentTypeID] [int] NOT NULL,
[FlowDirection] [int] NOT NULL CONSTRAINT [DF_PhaseDefinitionRights_FlowDirection] DEFAULT ((2)),
[MaxRights] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PhaseDefinitionRights] ADD CONSTRAINT [PK_PhaseDefinitionRights] PRIMARY KEY CLUSTERED  ([PhaseDefinitionRightsID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_PhaseDefinitionRight_PhaseDefinitionID] ON [dbo].[PhaseDefinitionRights] ([PhaseDefinitionID]) INCLUDE ([DepartmentTypeID], [FlowDirection], [MaxRights], [OrganizationTypeID], [PhaseDefinitionRightsID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PhaseDefinitionRights] ADD CONSTRAINT [FK_PhaseDefinitionRights_DepartmentType] FOREIGN KEY ([DepartmentTypeID]) REFERENCES [dbo].[DepartmentType] ([DepartmentTypeID])
GO
ALTER TABLE [dbo].[PhaseDefinitionRights] ADD CONSTRAINT [FK_PhaseDefinitionRights_OrganizationType] FOREIGN KEY ([OrganizationTypeID]) REFERENCES [dbo].[OrganizationType] ([OrganizationTypeID])
GO
ALTER TABLE [dbo].[PhaseDefinitionRights] ADD CONSTRAINT [FK_PhaseDefinitionRights_PhaseDefinition] FOREIGN KEY ([PhaseDefinitionID]) REFERENCES [dbo].[PhaseDefinition] ([PhaseDefinitionID])
GO
