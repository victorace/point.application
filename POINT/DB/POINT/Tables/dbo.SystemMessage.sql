CREATE TABLE [dbo].[SystemMessage]
(
[SystemMessageID] [int] NOT NULL IDENTITY(1, 1),
[Title] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Message] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StartDate] [datetime] NOT NULL,
[EndDate] [datetime] NOT NULL,
[FunctionCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IsPermanent] [bit] NOT NULL,
[IsActive] [bit] NOT NULL,
[LinkReference] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_SystemMessage_LinkReference] DEFAULT (''),
[LinkText] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_SystemMessage_LinkText] DEFAULT ('')
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[SystemMessage] ADD CONSTRAINT [PK_SystemMessage] PRIMARY KEY CLUSTERED  ([SystemMessageID]) ON [PRIMARY]
GO
