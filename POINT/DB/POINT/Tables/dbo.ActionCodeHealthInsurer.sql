CREATE TABLE [dbo].[ActionCodeHealthInsurer]
(
[ActionCodeHealthInsurerID] [int] NOT NULL IDENTITY(1, 1),
[Complex] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActionGroupHealthInsurerID] [int] NULL,
[ActionCodeName] [varchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TimeLimitMin] [int] NULL,
[TimeLimitMax] [int] NULL,
[DateBegin] [datetime] NULL,
[DateEnd] [datetime] NULL,
[ActionCodeHealthInsurerNumber] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActionCodeHealthInsurerNumberOnScreen] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ActionCodeHealthInsurer] ADD CONSTRAINT [PK_ActionCodeHealthInsurer] PRIMARY KEY CLUSTERED  ([ActionCodeHealthInsurerID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ActionCodeHealthInsurer] ADD CONSTRAINT [FK_ActionCodeHealthInsurer_ActionGroupHealthInsurer] FOREIGN KEY ([ActionGroupHealthInsurerID]) REFERENCES [dbo].[ActionGroupHealthInsurer] ([ActionGroupHealthInsurerID])
GO
