CREATE TABLE [dbo].[Medicine]
(
[MedicineID] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Code] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SortOrder] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Medicine] ADD CONSTRAINT [PK_Medicine] PRIMARY KEY CLUSTERED  ([MedicineID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
