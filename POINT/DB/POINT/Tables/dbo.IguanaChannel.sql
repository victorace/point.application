CREATE TABLE [dbo].[IguanaChannel]
(
[ChannelID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ChannelName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChannelOn] [bit] NOT NULL CONSTRAINT [DF_IguanaChannel_ChannelOn] DEFAULT ((0)),
[Source] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SourceOn] [bit] NOT NULL CONSTRAINT [DF_IguanaChannel_SourceOn] DEFAULT ((0)),
[Destination] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DestinationOn] [bit] NOT NULL CONSTRAINT [DF_IguanaChannel_DestinationOn] DEFAULT ((0)),
[MessagesQueued] [int] NOT NULL CONSTRAINT [DF_IguanaChannel_MessagesQueued] DEFAULT ((0)),
[Monitor] [bit] NOT NULL CONSTRAINT [DF_IguanaChannel_Monitor] DEFAULT ((0)),
[TimeStamp] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[IguanaChannel] ADD CONSTRAINT [PK_IguanaChannel] PRIMARY KEY CLUSTERED  ([ChannelID]) ON [PRIMARY]
GO
