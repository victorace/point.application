CREATE TABLE [dbo].[FunctionRole]
(
[FunctionRoleID] [int] NOT NULL IDENTITY(1, 1),
[EmployeeID] [int] NULL,
[FunctionRoleTypeID] [int] NULL,
[FunctionRoleLevelID] [int] NULL,
[ModifiedTimeStamp] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FunctionRole] ADD CONSTRAINT [PK_FunctionRole] PRIMARY KEY CLUSTERED  ([FunctionRoleID]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_FunctionRole] ON [dbo].[FunctionRole] ([EmployeeID], [FunctionRoleTypeID], [FunctionRoleLevelID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FunctionRole] ADD CONSTRAINT [FK_FunctionRole_Employee] FOREIGN KEY ([EmployeeID]) REFERENCES [dbo].[Employee] ([EmployeeID])
GO
