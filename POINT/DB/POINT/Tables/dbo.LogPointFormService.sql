CREATE TABLE [dbo].[LogPointFormService]
(
[LogPointFormServiceID] [int] NOT NULL IDENTITY(1, 1),
[Timestamp] [datetime] NOT NULL,
[OrganizationID] [int] NULL,
[Hash] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[URL] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Request] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[LogPointFormService] ADD CONSTRAINT [PK_LogPointFormService] PRIMARY KEY CLUSTERED  ([LogPointFormServiceID]) ON [PRIMARY]
GO
