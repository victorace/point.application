CREATE TABLE [dbo].[OrganizationProject]
(
[OrganizationProjectID] [int] NOT NULL IDENTITY(1, 1),
[OrganizationID] [int] NOT NULL,
[Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[InActive] [bit] NOT NULL CONSTRAINT [DF_OrganizationProject_InActive] DEFAULT ((0)),
[ModifiedTimeStamp] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[OrganizationProject] ADD CONSTRAINT [PK_OrganizationProject] PRIMARY KEY CLUSTERED  ([OrganizationProjectID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[OrganizationProject] ADD CONSTRAINT [FK_OrganizationProject_Organization] FOREIGN KEY ([OrganizationID]) REFERENCES [dbo].[Organization] ([OrganizationID])
GO
