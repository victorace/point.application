CREATE TABLE [dbo].[DoctorLocation]
(
[DoctorID] [int] NOT NULL,
[LocationID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DoctorLocation] ADD CONSTRAINT [PK_DoctorLocation] PRIMARY KEY CLUSTERED  ([DoctorID], [LocationID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DoctorLocation] ADD CONSTRAINT [FK_DoctorLocation_Doctor] FOREIGN KEY ([DoctorID]) REFERENCES [dbo].[Doctor] ([DoctorID])
GO
ALTER TABLE [dbo].[DoctorLocation] ADD CONSTRAINT [FK_DoctorLocation_Location] FOREIGN KEY ([LocationID]) REFERENCES [dbo].[Location] ([LocationID])
GO
