CREATE TABLE [dbo].[HealthInsurer]
(
[HealthInsurerID] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SortOrder] [int] NULL,
[UZOVI] [int] NULL,
[InActive] [bit] NOT NULL CONSTRAINT [DF_HealthInsurer_InActive] DEFAULT ((0)),
[HealthInsurerConcernID] [int] NULL,
[VegroCode] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MediPointCode] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[HealthInsurer] ADD CONSTRAINT [PK_HealthInsurer] PRIMARY KEY CLUSTERED  ([HealthInsurerID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_HealthInsurer_HealthInsurerID_Name] ON [dbo].[HealthInsurer] ([HealthInsurerID]) INCLUDE ([Name]) ON [PRIMARY]
GO
