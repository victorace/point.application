CREATE TABLE [dbo].[ServiceBusLog]
(
[ServiceBusLogID] [int] NOT NULL IDENTITY(1, 1),
[ObjectData] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ObjectType] [varchar] (400) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SentDateTime] [datetime] NOT NULL,
[Queue] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Label] [varchar] (400) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Processed] [bit] NOT NULL,
[ProcessedDateTime] [datetime] NULL,
[UniqueID] [bigint] NOT NULL,
[Expired] [bit] NOT NULL CONSTRAINT [DF_ServiceBusLog_Expired] DEFAULT ((0))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[ServiceBusLog] ADD CONSTRAINT [PK_ServiceBusLog] PRIMARY KEY CLUSTERED  ([ServiceBusLogID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_ServiceBusLog] ON [dbo].[ServiceBusLog] ([UniqueID]) ON [PRIMARY]
GO
