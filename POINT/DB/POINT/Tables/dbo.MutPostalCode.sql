CREATE TABLE [dbo].[MutPostalCode]
(
[MutPostalCodeID] [int] NOT NULL IDENTITY(1, 1),
[LogPostalCodeID] [int] NOT NULL,
[PostalCodeID] [int] NOT NULL,
[StartPostalCode] [char] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EndPostalCode] [char] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TimeStamp] [datetime] NULL,
[EmployeeID] [int] NULL,
[ScreenID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MutPostalCode] ADD CONSTRAINT [PK_MutPostalCode] PRIMARY KEY CLUSTERED  ([MutPostalCodeID]) ON [PRIMARY]
GO
