CREATE TABLE [dbo].[LogOrganization]
(
[LogOrganizationID] [int] NOT NULL IDENTITY(1, 1),
[OrganizationID] [int] NULL,
[TimeStamp] [datetime] NULL,
[EmployeeID] [int] NULL,
[Action] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ScreenID] [int] NULL,
[Version] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[LogOrganization] ADD CONSTRAINT [PK_LogOrganization] PRIMARY KEY CLUSTERED  ([LogOrganizationID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_LogOrganization_OrganizationID] ON [dbo].[LogOrganization] ([OrganizationID]) ON [PRIMARY]
GO
