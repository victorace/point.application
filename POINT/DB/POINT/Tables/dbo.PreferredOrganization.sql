CREATE TABLE [dbo].[PreferredOrganization]
(
[OrganizationID] [int] NOT NULL,
[PreferredOrganizationID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PreferredOrganization] ADD CONSTRAINT [PK_PreferredOrganization_1] PRIMARY KEY CLUSTERED  ([OrganizationID], [PreferredOrganizationID]) ON [PRIMARY]
GO
