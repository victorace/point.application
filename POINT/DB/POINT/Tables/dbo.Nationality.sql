CREATE TABLE [dbo].[Nationality]
(
[NationalityID] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InActive] [bit] NOT NULL CONSTRAINT [DF_Nationality_InActive] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Nationality] ADD CONSTRAINT [PK_Nationality] PRIMARY KEY CLUSTERED  ([NationalityID]) ON [PRIMARY]
GO
