CREATE TABLE [dbo].[ClientQuery]
(
[MESSAGE_ID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__ClientQue__MESSA__208DA251] DEFAULT (''),
[STATUS] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__ClientQue__STATU__2181C68A] DEFAULT ('W')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ClientQuery] ADD CONSTRAINT [PK__ClientQu__F610EE440B92856B] PRIMARY KEY CLUSTERED  ([MESSAGE_ID]) ON [PRIMARY]
GO
