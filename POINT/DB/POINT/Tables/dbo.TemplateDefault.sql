CREATE TABLE [dbo].[TemplateDefault]
(
[TemplateDefaultID] [int] NOT NULL IDENTITY(1, 1),
[TemplateTypeID] [int] NULL,
[TemplateDefaultText] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Inactive] [bit] NOT NULL CONSTRAINT [DF_TemplateDefault_Inactive] DEFAULT ((0)),
[OrganizationTypeID] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[TemplateDefault] ADD CONSTRAINT [PK_TempletDefaultHealthCareMail] PRIMARY KEY CLUSTERED  ([TemplateDefaultID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TemplateDefault] ADD CONSTRAINT [FK_TemplateDefault_TemplateType] FOREIGN KEY ([TemplateTypeID]) REFERENCES [dbo].[TemplateType] ([TemplateTypeID])
GO
