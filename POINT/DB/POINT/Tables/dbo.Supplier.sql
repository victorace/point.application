CREATE TABLE [dbo].[Supplier]
(
[SupplierID] [int] NOT NULL,
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DirectOrdering] [bit] NOT NULL CONSTRAINT [DF_Supplier_HasDirectOrdering] DEFAULT ((0)),
[PhoneNumber] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Supplier] ADD CONSTRAINT [PK_Supplier] PRIMARY KEY CLUSTERED  ([SupplierID]) ON [PRIMARY]
GO
