CREATE TABLE [dbo].[SendFormType]
(
[SendFormTypeID] [int] NOT NULL IDENTITY(1, 1),
[SendDestinationType] [int] NULL,
[OrganizationID] [int] NULL,
[FormTypeID] [int] NOT NULL,
[Allowed] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SendFormType] ADD CONSTRAINT [PK_SendFormType] PRIMARY KEY CLUSTERED  ([SendFormTypeID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SendFormType] ADD CONSTRAINT [FK_dbo.SendFormType_dbo.FormType_FormTypeID] FOREIGN KEY ([FormTypeID]) REFERENCES [dbo].[FormType] ([FormTypeID])
GO
