CREATE TABLE [dbo].[FlowPhaseAttribute]
(
[FlowPhaseAttributeID] [int] NOT NULL IDENTITY(1, 1),
[PhaseDefinitionID] [int] NOT NULL,
[RoleID] [uniqueidentifier] NULL,
[OrganizationTypeID] [int] NULL,
[ClassNameMenuItem] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[FlowPhaseAttribute] ADD CONSTRAINT [PK_dbo.FlowPhaseAttribute] PRIMARY KEY CLUSTERED  ([FlowPhaseAttributeID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_OrganizationTypeID] ON [dbo].[FlowPhaseAttribute] ([OrganizationTypeID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_PhaseDefinitionID] ON [dbo].[FlowPhaseAttribute] ([PhaseDefinitionID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_RoleID] ON [dbo].[FlowPhaseAttribute] ([RoleID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FlowPhaseAttribute] ADD CONSTRAINT [FK_dbo.FlowPhaseAttribute_dbo.OrganizationType_OrganizationTypeID] FOREIGN KEY ([OrganizationTypeID]) REFERENCES [dbo].[OrganizationType] ([OrganizationTypeID])
GO
ALTER TABLE [dbo].[FlowPhaseAttribute] ADD CONSTRAINT [FK_dbo.FlowPhaseAttribute_dbo.PhaseDefinition_PhaseDefinitionID] FOREIGN KEY ([PhaseDefinitionID]) REFERENCES [dbo].[PhaseDefinition] ([PhaseDefinitionID])
GO
ALTER TABLE [dbo].[FlowPhaseAttribute] ADD CONSTRAINT [FK_dbo.FlowPhaseAttribute_dbo.Aspnet_Roles_RoleID] FOREIGN KEY ([RoleID]) REFERENCES [dbo].[aspnet_Roles] ([RoleId])
GO
