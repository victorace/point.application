CREATE TABLE [dbo].[LogMedicineCard]
(
[LogMedicineCardID] [int] NOT NULL IDENTITY(1, 1),
[TransferID] [int] NULL,
[MedicineCardID] [int] NULL,
[TimeStamp] [datetime] NULL,
[EmployeeID] [int] NULL,
[Action] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ScreenID] [int] NULL,
[Version] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[LogMedicineCard] ADD CONSTRAINT [PK_LogMedicineCard] PRIMARY KEY CLUSTERED  ([LogMedicineCardID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_LogMedicineCard_MedicineCardID] ON [dbo].[LogMedicineCard] ([MedicineCardID]) ON [PRIMARY]
GO
