CREATE TABLE [dbo].[MutActionHealthInsurer]
(
[MutActionHealthInsurerID] [int] NOT NULL IDENTITY(1, 1),
[LogActionHealthInsurerID] [int] NULL,
[ActionHealthInsurerID] [int] NOT NULL,
[ActionCodeHealthInsurerID] [int] NULL,
[TransferID] [int] NULL,
[FormSetVersionID] [int] NULL,
[Amount] [int] NULL,
[Unit] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExpectedTime] [int] NULL,
[Definition] [varchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TimeStamp] [datetime] NULL,
[EmployeeID] [int] NULL,
[ScreenID] [int] NULL,
[UnitFrequency] [int] NULL,
[CustomFrequency] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MutActionHealthInsurer] ADD CONSTRAINT [PK_MutActionHealthInsurer] PRIMARY KEY CLUSTERED  ([MutActionHealthInsurerID]) ON [PRIMARY]
GO
