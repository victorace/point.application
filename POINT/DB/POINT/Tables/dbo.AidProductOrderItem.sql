CREATE TABLE [dbo].[AidProductOrderItem]
(
[AidProductOrderItemID] [int] NOT NULL IDENTITY(1, 1),
[FormSetVersionID] [int] NOT NULL,
[AidProductID] [int] NOT NULL,
[Quantity] [int] NOT NULL CONSTRAINT [DF_AidProductOrderItem_Quantity] DEFAULT ((1))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AidProductOrderItem] ADD CONSTRAINT [PK_AidProductOrderItem] PRIMARY KEY CLUSTERED  ([AidProductOrderItemID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AidProductOrderItem] ADD CONSTRAINT [FK_AidProductOrderItem_AidProduct] FOREIGN KEY ([AidProductID]) REFERENCES [dbo].[AidProduct] ([AidProductID])
GO
ALTER TABLE [dbo].[AidProductOrderItem] ADD CONSTRAINT [FK_AidProductOrderItem_FormSetVersion] FOREIGN KEY ([FormSetVersionID]) REFERENCES [dbo].[FormSetVersion] ([FormSetVersionID])
GO
