CREATE TABLE [dbo].[ScheduleHistory]
(
[ScheduleHistoryID] [int] NOT NULL IDENTITY(1, 1),
[FrequencyID] [int] NOT NULL,
[Timestamp] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ScheduleHistory] ADD CONSTRAINT [PK_dbo.ScheduleHistory] PRIMARY KEY CLUSTERED  ([ScheduleHistoryID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FrequencyID] ON [dbo].[ScheduleHistory] ([FrequencyID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ScheduleHistory] ADD CONSTRAINT [FK_dbo.ScheduleHistory_dbo.Frequency_FrequencyID] FOREIGN KEY ([FrequencyID]) REFERENCES [dbo].[Frequency] ([FrequencyID])
GO
