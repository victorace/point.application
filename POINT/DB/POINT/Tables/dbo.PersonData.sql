CREATE TABLE [dbo].[PersonData]
(
[PersonDataID] [int] NOT NULL IDENTITY(1, 1),
[Gender] [int] NULL,
[Salutation] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Initials] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirstName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MiddleName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaidenName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PartnerName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PartnerMiddleName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BirthDate] [datetime] NULL,
[PhoneNumberGeneral] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PhoneNumberMobile] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PhoneNumberWork] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PersonData] ADD CONSTRAINT [PK_PersonalName] PRIMARY KEY CLUSTERED  ([PersonDataID]) ON [PRIMARY]
GO
