CREATE TABLE [dbo].[ActionGroupHealthInsurer]
(
[ActionGroupHealthInsurerID] [int] NOT NULL IDENTITY(1, 1),
[ActionGroupName] [varchar] (400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ActionGroupHealthInsurer] ADD CONSTRAINT [PK_ActionGroupHealthInsurer] PRIMARY KEY CLUSTERED  ([ActionGroupHealthInsurerID]) ON [PRIMARY]
GO
