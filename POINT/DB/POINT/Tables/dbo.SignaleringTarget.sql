CREATE TABLE [dbo].[SignaleringTarget]
(
[SignaleringTargetID] [int] NOT NULL IDENTITY(1, 1),
[Action] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SignaleringTriggerID] [int] NOT NULL,
[OrganizationTypeID] [int] NOT NULL,
[RegionID] [int] NULL,
[OrganizationID] [int] NULL,
[MinActivePhase] [int] NOT NULL CONSTRAINT [DF_SignaleringTarget_MinActivePhase] DEFAULT ((0)),
[MaxActivePhase] [int] NOT NULL CONSTRAINT [DF_SignaleringTarget_MaxActivePhase] DEFAULT ((10)),
[TargetDepartment] [bit] NOT NULL CONSTRAINT [DF_SignaleringTarget_TargetDepartment] DEFAULT ((0)),
[DepartmentTypeID] [int] NULL,
[FlowDirection] [int] NOT NULL CONSTRAINT [DF_SignaleringTarget_FlowDirection] DEFAULT ((2))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SignaleringTarget] ADD CONSTRAINT [PK_SignaleringTarget] PRIMARY KEY CLUSTERED  ([SignaleringTargetID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SignaleringTarget] ADD CONSTRAINT [FK_SignaleringTarget_DepartmentType] FOREIGN KEY ([DepartmentTypeID]) REFERENCES [dbo].[DepartmentType] ([DepartmentTypeID])
GO
ALTER TABLE [dbo].[SignaleringTarget] ADD CONSTRAINT [FK_SignaleringTarget_SignaleringTrigger] FOREIGN KEY ([SignaleringTriggerID]) REFERENCES [dbo].[SignaleringTrigger] ([SignaleringTriggerID])
GO
