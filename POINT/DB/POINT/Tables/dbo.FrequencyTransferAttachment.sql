CREATE TABLE [dbo].[FrequencyTransferAttachment]
(
[FrequencyTransferAttachmentID] [int] NOT NULL IDENTITY(1, 1),
[FrequencyID] [int] NOT NULL,
[AttachmentID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FrequencyTransferAttachment] ADD CONSTRAINT [PK_dbo.FrequencyTransferAttachment] PRIMARY KEY CLUSTERED  ([FrequencyTransferAttachmentID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FrequencyTransferAttachment] ADD CONSTRAINT [FK_dbo.FrequencyTransferAttachment_dbo.Frequency_FrequencyID] FOREIGN KEY ([FrequencyID]) REFERENCES [dbo].[Frequency] ([FrequencyID])
GO
ALTER TABLE [dbo].[FrequencyTransferAttachment] ADD CONSTRAINT [FK_dbo.FrequencyTransferAttachment_dbo.TransferAttachment_AttachmentID] FOREIGN KEY ([AttachmentID]) REFERENCES [dbo].[TransferAttachment] ([AttachmentID])
GO
