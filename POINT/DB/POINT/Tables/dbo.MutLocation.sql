CREATE TABLE [dbo].[MutLocation]
(
[MutLocationID] [int] NOT NULL IDENTITY(1, 1),
[LogLocationID] [int] NULL,
[LocationID] [int] NOT NULL,
[OrganizationID] [int] NOT NULL,
[Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StreetName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Number] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Country] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PostalCode] [char] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (512) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransferPointID] [int] NULL,
[Inactive] [bit] NULL,
[PhoneNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FaxNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmailAddress] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CapacityNotAvailbleTill] [datetime] NULL,
[RecieveEmail] [bit] NOT NULL,
[Capacity] [bit] NOT NULL,
[NumberPlaces] [int] NOT NULL,
[CapacityInfo] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CapacityInfoChanged] [datetime] NULL,
[IPCheck] [bit] NULL,
[IPAddress] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Area] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TimeStamp] [datetime] NULL,
[EmployeeID] [int] NULL,
[ScreenID] [int] NULL,
[FlowParticipation] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MutLocation] ADD CONSTRAINT [PK_MutLocation] PRIMARY KEY CLUSTERED  ([MutLocationID]) ON [PRIMARY]
GO
