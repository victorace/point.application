CREATE TABLE [dbo].[AidProductOrderAnswer]
(
[AidProductOrderAnswerID] [int] NOT NULL IDENTITY(1, 1),
[QuestionID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Value] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UniqueFormKey] [int] NOT NULL,
[FormSetVersionID] [int] NULL,
[ProductGroupCode] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExtraItems] [bit] NOT NULL CONSTRAINT [DF_AidProductOrderAnswer_ExtraItems] DEFAULT ((0)),
[ExtraItemsGroupCode] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[AidProductOrderAnswer] ADD CONSTRAINT [PK_AidProductOrderAnswer] PRIMARY KEY CLUSTERED  ([AidProductOrderAnswerID]) ON [PRIMARY]
GO
