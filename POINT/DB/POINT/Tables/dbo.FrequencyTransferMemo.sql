CREATE TABLE [dbo].[FrequencyTransferMemo]
(
[FrequencyTransferMemoID] [int] NOT NULL IDENTITY(1, 1),
[FrequencyID] [int] NOT NULL,
[TransferMemoID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FrequencyTransferMemo] ADD CONSTRAINT [PK_dbo.FrequencyTransferMemo] PRIMARY KEY CLUSTERED  ([FrequencyTransferMemoID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FrequencyID] ON [dbo].[FrequencyTransferMemo] ([FrequencyID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_TransferMemoID] ON [dbo].[FrequencyTransferMemo] ([TransferMemoID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FrequencyTransferMemo] ADD CONSTRAINT [FK_dbo.FrequencyTransferMemo_dbo.Frequency_FrequencyID] FOREIGN KEY ([FrequencyID]) REFERENCES [dbo].[Frequency] ([FrequencyID])
GO
ALTER TABLE [dbo].[FrequencyTransferMemo] ADD CONSTRAINT [FK_dbo.FrequencyTransferMemo_dbo.TransferMemo_TransferMemoID] FOREIGN KEY ([TransferMemoID]) REFERENCES [dbo].[TransferMemo] ([TransferMemoID])
GO
