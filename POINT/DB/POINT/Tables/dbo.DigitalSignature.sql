CREATE TABLE [dbo].[DigitalSignature]
(
[DigitalSignatureID] [int] NOT NULL IDENTITY(1, 1),
[ContentLength] [int] NOT NULL,
[ContentType] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FileName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FileData] [image] NOT NULL,
[InActive] [bit] NOT NULL CONSTRAINT [DF_DigitalSignature_Inactive] DEFAULT ((0)),
[EmployeeID] [int] NOT NULL,
[Timestamp] [datetime] NOT NULL,
[ScreenName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[DigitalSignature] ADD CONSTRAINT [PK_DigitalSignature] PRIMARY KEY CLUSTERED  ([DigitalSignatureID]) ON [PRIMARY]
GO
