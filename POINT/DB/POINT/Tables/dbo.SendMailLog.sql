CREATE TABLE [dbo].[SendMailLog]
(
[SendMailLogID] [int] NOT NULL IDENTITY(1, 1),
[TransferID] [int] NULL,
[FileName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Code] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Timestamp] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SendMailLog] ADD CONSTRAINT [PK_SendMailLog] PRIMARY KEY CLUSTERED  ([SendMailLogID]) ON [PRIMARY]
GO
