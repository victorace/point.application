CREATE TABLE [dbo].[LogCreateUser]
(
[LogCreateUserID] [int] NOT NULL IDENTITY(1, 1),
[OrganizationID] [int] NULL,
[EmployeeID] [int] NULL,
[FirstName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MiddleName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Gender] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Position] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PhoneNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmailAddress] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExternID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UserName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UsernamePrefix] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RoleName] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AllOrganizationDepartmentsLinked] [bit] NOT NULL CONSTRAINT [DF_LogCreateUser_AllOrganizationDepartmentsLinked] DEFAULT ((0)),
[ChangePasswordByEmployee] [bit] NOT NULL CONSTRAINT [DF_LogCreateUser_ChangePasswordByEmployee] DEFAULT ((0)),
[DepartmentID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LocationID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LocationName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DepartmentName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UrlInlog] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExtraParametersUrl] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HashCode] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IP] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Comment] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UserCreated] [bit] NOT NULL CONSTRAINT [DF_LogCreateUser_ClientCreated] DEFAULT ((0)),
[Timestamp] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[LogCreateUser] ADD CONSTRAINT [PK_LogCreateUser] PRIMARY KEY CLUSTERED  ([LogCreateUserID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[LogCreateUser] ADD CONSTRAINT [FK_LogCreateUser_Organization] FOREIGN KEY ([OrganizationID]) REFERENCES [dbo].[Organization] ([OrganizationID])
GO
