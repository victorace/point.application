CREATE TABLE [dbo].[MedicineCard]
(
[MedicineCardID] [int] NOT NULL IDENTITY(1, 1),
[TransferID] [int] NOT NULL,
[MedicineID] [int] NOT NULL,
[NotRegisteredMedicine] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Dosage] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Frequence] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FinishDate] [datetime] NULL,
[Prescription] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TimeStamp] [datetime] NULL,
[EmployeeID] [int] NULL,
[ScreenID] [int] NULL,
[FormSetVersionID] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- ========================================================================
-- Author:		Phil Clymans
-- Create date: 26-04-2010
-- Description:	Logging
-- ========================================================================
CREATE TRIGGER [dbo].[MedicineCard_InsertUpdate]
   ON  [dbo].[MedicineCard] 
   AFTER UPDATE, INSERT
AS 
BEGIN

	SET NOCOUNT ON;

	IF (SELECT COUNT(*) FROM Inserted) = 0
	BEGIN
		RETURN
	END

	IF (SELECT Enabled FROM Logging) = 0
	BEGIN
		RETURN
	END

	DECLARE @ID INT
	SELECT @ID = MedicineCardID FROM inserted

	-- BIJWERKEN LOGGING
	DECLARE	@LogMedicineCardID	INT,
	@TransferID					INT,
	@MedicineCardID				INT,
	@TimeStamp					DATETIME,
	@EmployeeID					INT,
	@Action						VARCHAR(1),
	@ScreenID					INT,
	@Version					INT

	SET NOCOUNT ON;
	
	-- Check of timestamp wel een update krijgt. Zoniet dan is er sprake
	-- van een oude SP die de update uitvoerd.
	IF UPDATE([TimeStamp])
	BEGIN
		SELECT
			@TimeStamp  = [TimeStamp],
			@EmployeeID	= [EmployeeID],
			@ScreenID	= [ScreenID]
		FROM Inserted
	END

	SELECT @MedicineCardID = MedicineCardID, 
		   @TransferID     = TransferID
	FROM Inserted

	IF exists (select * from deleted)
		SELECT @Action = 'U'
	ELSE
		SELECT @Action = 'I'
		
	SELECT @Version = (SELECT ISNULL(MAX(Version), 0) + 1 FROM LogMedicineCard where LogMedicineCard.MedicineCardID = @MedicineCardID)

	INSERT INTO LogMedicineCard
			   (TransferID,
			    MedicineCardID,
			    [TimeStamp],
				EmployeeID,
				[Action],
				ScreenID,
				[Version])
		 VALUES
			(@TransferID,
			 @MedicineCardID,
			 @TimeStamp,
			 @EmployeeID,
			 @Action,
			 @ScreenID,
			 @Version)

	SET @LogMedicineCardID = SCOPE_IDENTITY();

	DECLARE @MutMedicineCardID INT
	
	INSERT INTO MutMedicineCard(
		LogMedicineCardID,
		MedicineCardID,
		TransferID,
		MedicineID,
		NotRegisteredMedicine,
		Dosage,
		Frequence,
		FinishDate,
		Prescription,
		TimeStamp,
		EmployeeID,
		ScreenID,
		FormSetVersionID)
	SELECT @LogMedicineCardID,
			MedicineCardID,
			TransferID,
			MedicineID,
			NotRegisteredMedicine,
			Dosage,
			Frequence,
			FinishDate,
			Prescription,
			TimeStamp,
			EmployeeID,
			ScreenID,
			FormSetVersionID
	FROM Inserted

	SET @MutMedicineCardID = SCOPE_IDENTITY();

END
GO
ALTER TABLE [dbo].[MedicineCard] ADD CONSTRAINT [PK_MedicineCard] PRIMARY KEY CLUSTERED  ([MedicineCardID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MedicineCard] ADD CONSTRAINT [FK_MedicineCard_Medicine] FOREIGN KEY ([MedicineID]) REFERENCES [dbo].[Medicine] ([MedicineID])
GO
ALTER TABLE [dbo].[MedicineCard] ADD CONSTRAINT [FK_MedicineCard_Transfer] FOREIGN KEY ([TransferID]) REFERENCES [dbo].[Transfer] ([TransferID])
GO
