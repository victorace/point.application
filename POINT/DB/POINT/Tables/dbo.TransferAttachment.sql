CREATE TABLE [dbo].[TransferAttachment]
(
[AttachmentID] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UploadDate] [datetime] NOT NULL,
[EmployeeID] [int] NULL,
[TransferID] [int] NULL,
[Deleted] [bit] NOT NULL CONSTRAINT [DF_TransferAttachment_Deleted] DEFAULT ((0)),
[TimeStamp] [datetime] NULL,
[ScreenID] [int] NULL,
[AttachmentTypeID] [int] NOT NULL,
[AttachmentSource] [int] NOT NULL,
[ShowInAttachmentList] [bit] NOT NULL CONSTRAINT [DF_TransferAttachment_ShowInAttachmentList] DEFAULT ((1)),
[FormSetVersionID] [int] NULL,
[GenericFileID] [uniqueidentifier] NOT NULL,
[RelatedAttachmentID] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- ========================================================================
-- Author:		Phil Clymans
-- Create date: 26-04-2010
-- Description:	Logging
-- ========================================================================
CREATE TRIGGER [dbo].[TransferAttachment_InsertUpdate]
   ON  [dbo].[TransferAttachment] 
   AFTER UPDATE, INSERT
AS 
BEGIN

	SET NOCOUNT ON;

	IF (SELECT COUNT(*) FROM Inserted) = 0
	BEGIN
		RETURN
	END
	
	DECLARE @ID INT
			
	IF (SELECT Enabled FROM Logging) = 0
	BEGIN
		RETURN
	END

	SELECT @ID = AttachmentID FROM inserted

	-- BIJWERKEN LOGGING
	DECLARE	@LogTransferAttachmentID	INT,
			@TransferID					INT,
			@AttachmentID				INT,
			@TimeStamp					DATETIME,
			@EmployeeID					INT,
			@Action						VARCHAR(1),
			@ScreenID					INT,
			@Version					INT

	SET NOCOUNT ON;
	
	-- Check of timestamp wel een update krijgt. Zoniet dan is er sprake
	-- van een oude SP die de update uitvoerd.
	IF UPDATE([TimeStamp])
	BEGIN
		SELECT
			@TimeStamp  = [TimeStamp],
			@EmployeeID	= [EmployeeID],
			@ScreenID	= [ScreenID]
		FROM Inserted
	END

	SELECT	@TransferID   = TransferID,
			@AttachmentID = AttachmentID
	FROM Inserted

	IF exists (select * from deleted)
		SELECT @Action = 'U'
	ELSE
		SELECT @Action = 'I'
	
	SELECT @Version = (SELECT ISNULL(MAX(Version), 0) + 1 FROM LogTransferAttachment where LogTransferAttachment.AttachmentID = @AttachmentID)

	INSERT INTO LogTransferAttachment
			   (TransferID,
			    AttachmentID,
			    [TimeStamp],
				EmployeeID,
				[Action],
				ScreenID,
				[Version])
		 VALUES
			(@TransferID,
			 @AttachmentID,
			 @TimeStamp,
			 @EmployeeID,
			 @Action,
			 @ScreenID,
			 @Version)

	SET @LogTransferAttachmentID = SCOPE_IDENTITY();

	DECLARE @MutTransferAttachmentID INT,
	@Name NVARCHAR(50),
	@Deleted BIT,
	@FormSetVersionID INT,
	@GenericFileID uniqueidentifier
	
	SELECT 
		@Name = Name,
		@TransferID = TransferID,
		@Deleted = Deleted,
		@FormSetVersionID = FormSetVersionID,
		@GenericFileID = GenericFileID
		FROM Inserted
	
	INSERT INTO MutTransferAttachment(
		LogTransferAttachmentID,
		AttachmentID,
		Name,
		EmployeeID,
		TransferID,
		Deleted,
		TimeStamp,
		ScreenID,
		FormSetVersionID,
		GenericFileID)
		VALUES (
			@LogTransferAttachmentID, 
			@AttachmentID,
			@Name,
			@EmployeeID,
			@TransferID,
			@Deleted,
			@TimeStamp,
			@ScreenID,
			@FormSetVersionID,
			@GenericFileID)

	SET @MutTransferAttachmentID = SCOPE_IDENTITY();

END
GO
ALTER TABLE [dbo].[TransferAttachment] ADD CONSTRAINT [PK_TransferAttachment] PRIMARY KEY CLUSTERED  ([AttachmentID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [TransferAttachment_Ind] ON [dbo].[TransferAttachment] ([TransferID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_TransferAttachment_TransferID] ON [dbo].[TransferAttachment] ([TransferID]) INCLUDE ([Name], [UploadDate]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TransferAttachment] ADD CONSTRAINT [FK_TransferAttachment_FormSetVersion] FOREIGN KEY ([FormSetVersionID]) REFERENCES [dbo].[FormSetVersion] ([FormSetVersionID])
GO
ALTER TABLE [dbo].[TransferAttachment] ADD CONSTRAINT [FK_TransferAttachment_Transfer] FOREIGN KEY ([TransferID]) REFERENCES [dbo].[Transfer] ([TransferID])
GO
