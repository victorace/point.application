CREATE TABLE [dbo].[Signalering]
(
[SignaleringID] [int] NOT NULL IDENTITY(1, 1),
[FlowInstanceID] [int] NOT NULL,
[Created] [datetime] NOT NULL,
[CreatedByEmployeeID] [int] NOT NULL,
[LastUpdated] [datetime] NULL,
[LastUpdatedByEmployeeID] [int] NULL,
[Completed] [datetime] NULL,
[CompletedByEmployeeID] [int] NULL,
[Status] [int] NOT NULL,
[SignaleringTargetID] [int] NOT NULL,
[Message] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Comment] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedByDepartmentID] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Signalering] ADD CONSTRAINT [PK_Signalering] PRIMARY KEY CLUSTERED  ([SignaleringID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Signalering_CreatedByEmployeeIDSignaleringTargetIDStatus] ON [dbo].[Signalering] ([FlowInstanceID], [CreatedByDepartmentID], [SignaleringTargetID], [Status]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Signalering] ADD CONSTRAINT [FK_Signalering_SignaleringTarget] FOREIGN KEY ([SignaleringTargetID]) REFERENCES [dbo].[SignaleringTarget] ([SignaleringTargetID])
GO
