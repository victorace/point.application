CREATE TABLE [dbo].[TransfersForSearchColumn]
(
[TransfersForSearchColumnID] [int] NOT NULL IDENTITY(1, 1),
[TransfersForSearchCommonID] [int] NOT NULL,
[AccessibleHeaderText] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Width] [int] NOT NULL,
[Visible] [bit] NOT NULL CONSTRAINT [DF_TransfersForSearchColumn_Visible] DEFAULT ((1)),
[SortOrder] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TransfersForSearchColumn] ADD CONSTRAINT [PK_TransfersForSearchColumn] PRIMARY KEY CLUSTERED  ([TransfersForSearchColumnID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_TransfersForSearchColumn_TransfersForSearchCommonID] ON [dbo].[TransfersForSearchColumn] ([TransfersForSearchCommonID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TransfersForSearchColumn] ADD CONSTRAINT [FK_TransfersForSearchColumn_TransfersForSearchCommon] FOREIGN KEY ([TransfersForSearchCommonID]) REFERENCES [dbo].[TransfersForSearchCommon] ([TransfersForSearchCommonID])
GO
