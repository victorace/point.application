CREATE TABLE [dbo].[Templet]
(
[TempletID] [int] NOT NULL IDENTITY(1, 1),
[RegionID] [int] NULL,
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UploadDate] [datetime] NULL,
[FileData] [image] NULL,
[FileSize] [int] NULL,
[FileName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContentType] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmployeeID] [int] NULL,
[Deleted] [bit] NOT NULL CONSTRAINT [DF_Templet_Deleted] DEFAULT ((0)),
[TempletTypeID] [int] NULL,
[SortOrder] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Templet] ADD CONSTRAINT [PK_Templet] PRIMARY KEY CLUSTERED  ([TempletID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Templet] ADD CONSTRAINT [FK_Templet_Employee] FOREIGN KEY ([EmployeeID]) REFERENCES [dbo].[Employee] ([EmployeeID])
GO
ALTER TABLE [dbo].[Templet] ADD CONSTRAINT [FK_Templet_Region] FOREIGN KEY ([RegionID]) REFERENCES [dbo].[Region] ([RegionID])
GO
