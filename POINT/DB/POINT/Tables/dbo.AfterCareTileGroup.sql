CREATE TABLE [dbo].[AfterCareTileGroup]
(
[AfterCareTileGroupID] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Sortorder] [int] NOT NULL,
[ColorCode] [nchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AfterCareTileGroup] ADD CONSTRAINT [PK_AfterCareTileGroup] PRIMARY KEY CLUSTERED  ([AfterCareTileGroupID]) ON [PRIMARY]
GO
