CREATE TABLE [dbo].[FlowFieldDataSource]
(
[FlowFieldDataSourceID] [int] NOT NULL IDENTITY(1, 1),
[JsonData] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FlowFieldDataSource] ADD CONSTRAINT [PK_FlowFieldDataSource] PRIMARY KEY CLUSTERED  ([FlowFieldDataSourceID]) ON [PRIMARY]
GO
