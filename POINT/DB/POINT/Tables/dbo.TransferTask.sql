CREATE TABLE [dbo].[TransferTask]
(
[TransferTaskID] [int] NOT NULL IDENTITY(1, 1),
[TransferID] [int] NOT NULL,
[Description] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Comments] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDate] [datetime] NOT NULL,
[CreatedByEmployeeID] [int] NOT NULL,
[DueDate] [datetime] NOT NULL,
[CompletedDate] [datetime] NULL,
[CompletedByEmployeeID] [int] NULL,
[Status] [int] NULL,
[Inactive] [bit] NOT NULL CONSTRAINT [DF_TransferTask_Inactive] DEFAULT ((0))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[TransferTask] ADD CONSTRAINT [PK_Task] PRIMARY KEY CLUSTERED  ([TransferTaskID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_TransferTask_TransferID] ON [dbo].[TransferTask] ([TransferID]) INCLUDE ([Comments], [CompletedByEmployeeID], [CompletedDate], [CreatedByEmployeeID], [CreatedDate], [Description], [DueDate], [Inactive], [Status], [TransferTaskID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TransferTask] ADD CONSTRAINT [FK_TransferTask_Transfer] FOREIGN KEY ([TransferID]) REFERENCES [dbo].[Transfer] ([TransferID])
GO
