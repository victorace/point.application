CREATE TABLE [dbo].[GeneralActionType]
(
[GeneralActionTypeID] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[GeneralActionType] ADD CONSTRAINT [PK_dbo.GeneralActionType] PRIMARY KEY CLUSTERED  ([GeneralActionTypeID]) ON [PRIMARY]
GO
