CREATE TABLE [dbo].[CommunicationQueue]
(
[CommunicationQueueID] [int] NOT NULL IDENTITY(1, 1),
[TransferID] [int] NULL,
[ClientID] [int] NULL,
[Timestamp] [datetime] NULL,
[EmployeeID] [int] NULL,
[OrganizationID] [int] NULL,
[MailType] [int] NULL,
[ReceivingOrganizationID] [int] NULL,
[ReceivingDepartmentID] [int] NULL,
[ReasonNotSend] [varchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Handled] [bit] NOT NULL,
[FormSetVersionID] [int] NULL,
[TransferAttachmentID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CommunicationQueue] ADD CONSTRAINT [PK_CommunicationQueue] PRIMARY KEY CLUSTERED  ([CommunicationQueueID]) ON [PRIMARY]
GO
