CREATE TABLE [dbo].[SignaleringTrigger]
(
[SignaleringTriggerID] [int] NOT NULL IDENTITY(1, 1),
[FlowDefinitionID] [int] NOT NULL,
[FormTypeID] [int] NULL,
[GeneralActionID] [int] NULL,
[Name] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ShowPrompt] [bit] NOT NULL CONSTRAINT [DF_SignaleringTrigger_ShowPrompt] DEFAULT ((0)),
[MinActivePhase] [int] NOT NULL CONSTRAINT [DF_SignaleringTrigger_MinActivePhase_1] DEFAULT ((0)),
[MaxActivePhase] [int] NOT NULL CONSTRAINT [DF_SignaleringTrigger_MaxActivePhase_1] DEFAULT ((10))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[SignaleringTrigger] ADD CONSTRAINT [PK_SignaleringTrigger] PRIMARY KEY CLUSTERED  ([SignaleringTriggerID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_SignaleringTrigger_All] ON [dbo].[SignaleringTrigger] ([FlowDefinitionID], [FormTypeID], [GeneralActionID], [MinActivePhase], [MaxActivePhase]) INCLUDE ([Description], [Name], [ShowPrompt], [SignaleringTriggerID]) ON [PRIMARY]
GO
