CREATE TABLE [dbo].[OrganizationSetting]
(
[OrganizationSettingID] [int] NOT NULL IDENTITY(1, 1),
[OrganizationID] [int] NOT NULL,
[LocationID] [int] NULL,
[DepartmentID] [int] NULL,
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Value] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModifiedTimeStamp] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[OrganizationSetting] ADD CONSTRAINT [PK_OrganizationSetting] PRIMARY KEY CLUSTERED  ([OrganizationSettingID]) ON [PRIMARY]
GO
