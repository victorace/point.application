CREATE TABLE [dbo].[Specialism]
(
[SpecialismID] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OrganizationTypeID] [int] NULL,
[Inactive] [bit] NOT NULL CONSTRAINT [DF_Specialism_Inactive] DEFAULT ((0)),
[CapacityTypeID] [int] NULL,
[SpecialismType] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AfterCareTypeID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Specialism] ADD CONSTRAINT [PK_Specialism] PRIMARY KEY CLUSTERED  ([SpecialismID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Specialism] ADD CONSTRAINT [FK_Specialism_AfterCareType] FOREIGN KEY ([AfterCareTypeID]) REFERENCES [dbo].[AfterCareType] ([AfterCareTypeID])
GO
