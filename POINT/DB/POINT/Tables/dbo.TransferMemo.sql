CREATE TABLE [dbo].[TransferMemo]
(
[TransferMemoID] [int] NOT NULL IDENTITY(1, 1),
[EmployeeID] [int] NULL,
[EmployeeName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MemoDateTime] [datetime] NULL,
[MemoContent] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Target] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransferID] [int] NULL,
[Deleted] [bit] NOT NULL CONSTRAINT [DF_TransferMemo_Deleted] DEFAULT ((0)),
[TransferMemoTypeID] [int] NULL,
[RelatedMemoID] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[TransferMemo] ADD CONSTRAINT [PK_TransferMemo] PRIMARY KEY CLUSTERED  ([TransferMemoID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_TransferMemo_MemoDateTime_Includes] ON [dbo].[TransferMemo] ([MemoDateTime]) INCLUDE ([TransferID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_TransferID] ON [dbo].[TransferMemo] ([TransferID]) INCLUDE ([Deleted], [EmployeeID], [EmployeeName], [MemoDateTime], [Target], [TransferMemoID], [TransferMemoTypeID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_TransferMemo_TransferID_Target] ON [dbo].[TransferMemo] ([TransferID], [Target]) INCLUDE ([EmployeeName], [MemoDateTime]) ON [PRIMARY]
GO
