CREATE TABLE [dbo].[FlowInstance]
(
[FlowInstanceID] [int] NOT NULL IDENTITY(1, 1),
[FlowDefinitionID] [int] NOT NULL,
[TransferID] [int] NOT NULL,
[CreatedDate] [datetime] NOT NULL,
[ClosedDate] [datetime] NULL,
[EmployeeID] [int] NULL,
[Timestamp] [datetime] NULL,
[ScreenName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OriginalFlowInstanceID] [int] NULL,
[Deleted] [bit] NOT NULL CONSTRAINT [DF_FlowInstance_IsDeleted] DEFAULT ((0)),
[DeletedDate] [datetime] NULL,
[DeletedByEmployeeID] [int] NULL,
[StartedByDepartmentID] [int] NOT NULL,
[StartedByOrganizationID] [int] NOT NULL,
[StartedByEmployeeID] [int] NULL,
[Interrupted] [bit] NOT NULL CONSTRAINT [DF_FlowInstance_Interrupted] DEFAULT ((0)),
[InterruptedDate] [datetime] NULL,
[InterruptedByEmployeeID] [int] NULL,
[IsAcute] [bit] NOT NULL CONSTRAINT [DF_FlowInstance_IsAcute] DEFAULT ((0)),
[RelatedTransferID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FlowInstance] ADD CONSTRAINT [PK_FlowInstance] PRIMARY KEY CLUSTERED  ([FlowInstanceID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Deleted] ON [dbo].[FlowInstance] ([Deleted]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_DeletedFlowDefinitionID_FlowInstanceID] ON [dbo].[FlowInstance] ([Deleted], [FlowDefinitionID]) INCLUDE ([FlowInstanceID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FlowDefinitionID] ON [dbo].[FlowInstance] ([FlowDefinitionID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FlowInstance_FlowDefinitionID_Deleted_Includes] ON [dbo].[FlowInstance] ([FlowDefinitionID], [Deleted]) INCLUDE ([FlowInstanceID], [StartedByDepartmentID], [StartedByOrganizationID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_DeletedStartedByDepartmentID] ON [dbo].[FlowInstance] ([FlowDefinitionID], [Deleted]) INCLUDE ([FlowInstanceID], [StartedByDepartmentID], [TransferID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_DeletedStartedByOrganizationID] ON [dbo].[FlowInstance] ([FlowDefinitionID], [Deleted]) INCLUDE ([FlowInstanceID], [StartedByOrganizationID], [TransferID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [FlowInstance_FlowDefinitionIDDeletedStartedByDepartmentID] ON [dbo].[FlowInstance] ([FlowDefinitionID], [StartedByDepartmentID], [Deleted]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [FlowInstance_FlowDefinitionIDDeletedStartedByEmployeeID] ON [dbo].[FlowInstance] ([FlowDefinitionID], [StartedByEmployeeID], [Deleted]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [FlowInstance_FlowDefinitionIDDeletedStartedByOrganizationID] ON [dbo].[FlowInstance] ([FlowDefinitionID], [StartedByOrganizationID], [Deleted]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FlowInstance_StartedByDepartmentID_Includes] ON [dbo].[FlowInstance] ([StartedByDepartmentID]) INCLUDE ([FlowInstanceID], [TransferID]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_TransferID] ON [dbo].[FlowInstance] ([TransferID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_TransferIDDeleted_FlowInstanceID] ON [dbo].[FlowInstance] ([TransferID], [Deleted]) INCLUDE ([FlowInstanceID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FlowInstance] ADD CONSTRAINT [FK_dbo.FlowInstance_dbo.FlowDefinition_FlowDefinitionID] FOREIGN KEY ([FlowDefinitionID]) REFERENCES [dbo].[FlowDefinition] ([FlowDefinitionID])
GO
ALTER TABLE [dbo].[FlowInstance] ADD CONSTRAINT [FK_dbo.FlowInstance_dbo.Transfer_TransferID] FOREIGN KEY ([TransferID]) REFERENCES [dbo].[Transfer] ([TransferID])
GO
