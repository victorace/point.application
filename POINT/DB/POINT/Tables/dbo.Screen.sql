CREATE TABLE [dbo].[Screen]
(
[ScreenID] [int] NOT NULL IDENTITY(3000, 1),
[FormTypeID] [int] NULL,
[GeneralActionID] [int] NULL,
[URL] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ScreenTypes] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ScreenTitle] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Screen] ADD CONSTRAINT [PK__ScreenSt__0AB60F856A8C75F8] PRIMARY KEY CLUSTERED  ([ScreenID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Screen_FormTypeID] ON [dbo].[Screen] ([FormTypeID]) INCLUDE ([GeneralActionID], [ScreenID], [ScreenTitle], [ScreenTypes], [URL]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Screen_GeneralActionID] ON [dbo].[Screen] ([GeneralActionID]) INCLUDE ([FormTypeID], [ScreenID], [ScreenTitle], [ScreenTypes], [URL]) ON [PRIMARY]
GO
