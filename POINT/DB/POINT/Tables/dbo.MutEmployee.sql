CREATE TABLE [dbo].[MutEmployee]
(
[MutEmployeeID] [int] NOT NULL IDENTITY(1, 1),
[LogEmployeeID] [int] NULL,
[EmployeeID] [int] NOT NULL,
[DepartmentID] [int] NOT NULL,
[FirstName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MiddleName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Gender] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Position] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PhoneNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UserId] [uniqueidentifier] NULL,
[EmailAddress] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InActive] [bit] NULL,
[ExternID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChangePasswordByEmployee] [bit] NULL,
[BIG] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ViewRights] [bit] NULL,
[TimeStamp] [datetime] NULL,
[ModifiedByEmployeeID] [int] NULL,
[ScreenID] [int] NULL,
[MembershipRolesAction] [varchar] (400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UserLogin] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AllOrganizationDepartmentsLinked] [bit] NULL,
[MSVTFunctionality] [bit] NULL CONSTRAINT [DF_MutEmployee_MSVTFunctionality] DEFAULT ((0)),
[MFANumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MFAConfirmedDate] [datetime] NULL,
[DossierLevel] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FunctionRoles] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Departments] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LockAction] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DigitalSignatureID] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[MutEmployee] ADD CONSTRAINT [PK_MutEmployee] PRIMARY KEY CLUSTERED  ([MutEmployeeID]) ON [PRIMARY]
GO
