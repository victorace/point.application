CREATE TABLE [dbo].[CryptoCertificate]
(
[CryptoCertificateID] [int] NOT NULL IDENTITY(1, 1),
[FriendlyName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FileName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PrivateKey] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ExpiryDate] [datetime] NOT NULL,
[CertificateType] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FileData] [image] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[CryptoCertificate] ADD CONSTRAINT [PK_CryptoCertificate] PRIMARY KEY CLUSTERED  ([CryptoCertificateID]) ON [PRIMARY]
GO
