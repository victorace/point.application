CREATE TABLE [dbo].[Location]
(
[LocationID] [int] NOT NULL IDENTITY(1, 1),
[OrganizationID] [int] NOT NULL,
[Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StreetName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Number] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Country] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PostalCode] [char] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (512) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Inactive] [bit] NOT NULL CONSTRAINT [DF_Location_Inactive] DEFAULT ((0)),
[RecieveEmail] [bit] NOT NULL CONSTRAINT [DF_Location_RecieveEmail] DEFAULT ((0)),
[CapacityInfo] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IPCheck] [bit] NOT NULL CONSTRAINT [DF_Location_IPCheck] DEFAULT ((0)),
[IPAddress] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Area] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModifiedTimestamp] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Location] ADD CONSTRAINT [PK_Location] PRIMARY KEY CLUSTERED  ([LocationID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Location_Inactive] ON [dbo].[Location] ([Inactive]) INCLUDE ([LocationID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Location_OrganizationID] ON [dbo].[Location] ([OrganizationID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Location_OrganizationIDInactive] ON [dbo].[Location] ([OrganizationID], [Inactive]) INCLUDE ([Area], [CapacityInfo], [City], [Country], [IPAddress], [IPCheck], [LocationID], [Name], [Number], [PostalCode], [RecieveEmail], [StreetName]) ON [PRIMARY]
GO
