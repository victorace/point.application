CREATE TABLE [dbo].[MutTransferTask]
(
[MutTransferTaskID] [int] NOT NULL IDENTITY(1, 1),
[TransferTaskID] [int] NOT NULL,
[TransferID] [int] NULL,
[Description] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Comments] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDate] [datetime] NULL,
[CreatedByEmployeeID] [int] NULL,
[DueDate] [datetime] NULL,
[CompletedDate] [datetime] NULL,
[CompletedByEmployeeID] [int] NULL,
[Status] [int] NULL,
[Inactive] [bit] NULL,
[Timestamp] [datetime] NULL,
[EmployeeID] [int] NULL,
[ScreenName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[MutTransferTask] ADD CONSTRAINT [PK_MutTransferTask] PRIMARY KEY CLUSTERED  ([MutTransferTaskID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MutTransferTask] ADD CONSTRAINT [FK_MutTransferTask_TransferTask] FOREIGN KEY ([TransferTaskID]) REFERENCES [dbo].[TransferTask] ([TransferTaskID])
GO
