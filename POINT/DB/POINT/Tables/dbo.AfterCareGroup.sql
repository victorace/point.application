CREATE TABLE [dbo].[AfterCareGroup]
(
[AfterCareGroupID] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Sortorder] [int] NOT NULL,
[AfterCareTileGroupID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AfterCareGroup] ADD CONSTRAINT [PK_AfterCareGroup] PRIMARY KEY CLUSTERED  ([AfterCareGroupID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AfterCareGroup] ADD CONSTRAINT [FK_AfterCareGroup_AfterCareTileGroup] FOREIGN KEY ([AfterCareTileGroupID]) REFERENCES [dbo].[AfterCareTileGroup] ([AfterCareTileGroupID])
GO
