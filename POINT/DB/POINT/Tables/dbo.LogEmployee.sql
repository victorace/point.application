CREATE TABLE [dbo].[LogEmployee]
(
[LogEmployeeID] [int] NOT NULL IDENTITY(1, 1),
[EmployeeID] [int] NULL,
[TimeStamp] [datetime] NULL,
[ModifiedByEmployeeID] [int] NULL,
[Action] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ScreenID] [int] NULL,
[Version] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[LogEmployee] ADD CONSTRAINT [PK_LogEmployee] PRIMARY KEY CLUSTERED  ([LogEmployeeID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_LogEmployee_EmployeeID] ON [dbo].[LogEmployee] ([EmployeeID]) ON [PRIMARY]
GO
