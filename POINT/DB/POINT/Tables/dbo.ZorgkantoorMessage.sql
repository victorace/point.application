CREATE TABLE [dbo].[ZorgkantoorMessage]
(
[MessageID] [int] NOT NULL IDENTITY(1, 1),
[TransferID] [int] NULL,
[AGBCode] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AGBCodeZorgKantoor] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Agreement] [bit] NULL,
[SocialSecurityNumber] [varchar] (9) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BirthDate] [datetime] NULL,
[ClientNumber] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IndicationDecisionNumber] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IndicationGivenNumber] [datetime] NULL,
[DateReceived] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ZorgkantoorMessage] ADD CONSTRAINT [PK_ZorgkantoorMessage] PRIMARY KEY CLUSTERED  ([MessageID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
