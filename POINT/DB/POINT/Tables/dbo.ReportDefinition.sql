CREATE TABLE [dbo].[ReportDefinition]
(
[ReportDefinitionID] [int] NOT NULL IDENTITY(1, 1),
[FileName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ExcelFriendlyFileName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReportName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ReportActionName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FlowDirection] [int] NOT NULL CONSTRAINT [DF_ReportDefinition_FlowDirection] DEFAULT ((0)),
[Category] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FilterPartial] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_ReportDefinition_FilterPartial] DEFAULT (''),
[NeedsOrganizationID] [bit] NOT NULL CONSTRAINT [DF_ReportDefinition_NeedsOrganizationID] DEFAULT ((0)),
[Visible] [bit] NOT NULL CONSTRAINT [DF_ReportDefinition_Visible] DEFAULT ((1))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[ReportDefinition] ADD CONSTRAINT [PK_ReportDefinition] PRIMARY KEY CLUSTERED  ([ReportDefinitionID]) ON [PRIMARY]
GO
