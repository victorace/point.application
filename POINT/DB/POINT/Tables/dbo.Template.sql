CREATE TABLE [dbo].[Template]
(
[TemplateID] [int] NOT NULL IDENTITY(1, 1),
[TemplateTypeID] [int] NULL,
[OrganizationID] [int] NULL,
[TemplateText] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Template] ADD CONSTRAINT [PK_TempletHealthCareMail] PRIMARY KEY CLUSTERED  ([TemplateID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Template] ADD CONSTRAINT [FK_Template_Organization] FOREIGN KEY ([OrganizationID]) REFERENCES [dbo].[Organization] ([OrganizationID])
GO
ALTER TABLE [dbo].[Template] ADD CONSTRAINT [FK_Template_TemplateType] FOREIGN KEY ([TemplateTypeID]) REFERENCES [dbo].[TemplateType] ([TemplateTypeID])
GO
