CREATE TABLE [dbo].[MutFlowFieldValue]
(
[MutFlowFieldValueID] [int] NOT NULL IDENTITY(1, 1),
[FlowFieldValueID] [int] NOT NULL,
[FlowFieldID] [int] NOT NULL,
[FormSetVersionID] [int] NULL,
[Value] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmployeeID] [int] NULL,
[Timestamp] [datetime] NULL,
[ScreenName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Source] [int] NOT NULL CONSTRAINT [DF_MutFlowFieldValue_Source] DEFAULT ((0))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[MutFlowFieldValue] ADD CONSTRAINT [PK_dbo.MutFlowFieldValue] PRIMARY KEY CLUSTERED  ([MutFlowFieldValueID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FlowFieldID] ON [dbo].[MutFlowFieldValue] ([FlowFieldID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FlowFieldValueID] ON [dbo].[MutFlowFieldValue] ([FlowFieldValueID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FormSetVersionID] ON [dbo].[MutFlowFieldValue] ([FormSetVersionID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_MutFlowFieldValue_FormSetVersionID_FlowFieldID_Includes] ON [dbo].[MutFlowFieldValue] ([FormSetVersionID], [FlowFieldID]) INCLUDE ([EmployeeID], [FlowFieldValueID], [MutFlowFieldValueID], [ScreenName], [Timestamp], [Value]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MutFlowFieldValue] ADD CONSTRAINT [FK_dbo.MutFlowFieldValue_dbo.FlowField_FlowFieldID] FOREIGN KEY ([FlowFieldID]) REFERENCES [dbo].[FlowField] ([FlowFieldID])
GO
ALTER TABLE [dbo].[MutFlowFieldValue] ADD CONSTRAINT [FK_dbo.MutFlowFieldValue_dbo.FormSetVersion_FormSetVersionID] FOREIGN KEY ([FormSetVersionID]) REFERENCES [dbo].[FormSetVersion] ([FormSetVersionID])
GO
