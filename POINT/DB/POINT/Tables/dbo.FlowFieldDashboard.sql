CREATE TABLE [dbo].[FlowFieldDashboard]
(
[FlowFieldDashboardID] [int] NOT NULL IDENTITY(1, 1),
[FlowFieldID] [int] NOT NULL,
[FlowDefinitionID] [int] NOT NULL,
[FormTypeID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FlowFieldDashboard] ADD CONSTRAINT [PK_FlowFieldDashboard] PRIMARY KEY CLUSTERED  ([FlowFieldDashboardID]) ON [PRIMARY]
GO
