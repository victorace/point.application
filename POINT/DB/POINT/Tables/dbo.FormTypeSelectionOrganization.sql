CREATE TABLE [dbo].[FormTypeSelectionOrganization]
(
[FormTypeSelectionID] [int] NOT NULL,
[OrganizationID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FormTypeSelectionOrganization] ADD CONSTRAINT [PK_FormTypeSelectionOrganization] PRIMARY KEY CLUSTERED  ([FormTypeSelectionID], [OrganizationID]) ON [PRIMARY]
GO
