CREATE TABLE [dbo].[RelationsType]
(
[RelationsTypeID] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RelationsType] ADD CONSTRAINT [PK_RelationsType] PRIMARY KEY CLUSTERED  ([RelationsTypeID]) ON [PRIMARY]
GO
