CREATE TABLE [dbo].[FlowFieldValue]
(
[FlowFieldValueID] [int] NOT NULL IDENTITY(1, 1),
[FlowFieldID] [int] NOT NULL,
[FormSetVersionID] [int] NULL,
[Value] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Timestamp] [datetime] NULL,
[EmployeeID] [int] NULL,
[ScreenName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FlowInstanceID] [int] NOT NULL,
[Source] [int] NOT NULL CONSTRAINT [DF_FlowFieldValue_Source] DEFAULT ((0))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[FlowFieldValue] ADD CONSTRAINT [PK_dbo.FlowFieldValue] PRIMARY KEY CLUSTERED  ([FlowFieldValueID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FlowFieldID] ON [dbo].[FlowFieldValue] ([FlowFieldID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FlowFieldValue_FlowFieldIDFlowInstanceID] ON [dbo].[FlowFieldValue] ([FlowFieldID], [FlowInstanceID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FlowFieldValue_FlowFieldIDFlowInstanceID_Filtered] ON [dbo].[FlowFieldValue] ([FlowFieldID], [FlowInstanceID]) INCLUDE ([EmployeeID], [FlowFieldValueID], [FormSetVersionID], [ScreenName], [Timestamp], [Value]) WHERE ([FlowFieldID] IN ((4), (7), (8), (9), (10), (11), (12), (45), (47), (73), (149), (275), (288), (295), (303), (307), (308), (309), (310), (723), (1052), (1053), (1100), (1156), (1157), (1168), (1520), (1609), (1622), (1600), (1630), (2012), (1607), (1608), (1618), (1619), (1623), (1624), (1621), (1645), (1647), (1660), (1661), (1673), (1668), (1650))) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FlowFieldValue_FlowInstanceID] ON [dbo].[FlowFieldValue] ([FlowInstanceID]) INCLUDE ([FlowFieldID], [FlowFieldValueID], [FormSetVersionID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FormSetVersionID] ON [dbo].[FlowFieldValue] ([FormSetVersionID]) INCLUDE ([FlowInstanceID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FlowFieldValue] ADD CONSTRAINT [FK_dbo.FlowFieldValue_dbo.FlowField_FlowFieldID] FOREIGN KEY ([FlowFieldID]) REFERENCES [dbo].[FlowField] ([FlowFieldID])
GO
ALTER TABLE [dbo].[FlowFieldValue] ADD CONSTRAINT [FK_dbo.FlowFieldValue_dbo.FormSetVersion_FormSetVersionID] FOREIGN KEY ([FormSetVersionID]) REFERENCES [dbo].[FormSetVersion] ([FormSetVersionID])
GO
ALTER TABLE [dbo].[FlowFieldValue] ADD CONSTRAINT [FK_FlowFieldValue_FlowInstance] FOREIGN KEY ([FlowInstanceID]) REFERENCES [dbo].[FlowInstance] ([FlowInstanceID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
