CREATE TABLE [dbo].[ActionCodeHealthInsurerRegio]
(
[ActionCodeHealthInsurerRegionID] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RegionID] [int] NULL,
[DateBegin] [datetime] NULL,
[DateEnd] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ActionCodeHealthInsurerRegio] ADD CONSTRAINT [PK_ActionCodeHealthInsurerID] PRIMARY KEY CLUSTERED  ([ActionCodeHealthInsurerRegionID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ActionCodeHealthInsurerRegio] ADD CONSTRAINT [FK_ActionCodeHealthInsurerRegio_Region] FOREIGN KEY ([RegionID]) REFERENCES [dbo].[Region] ([RegionID])
GO
