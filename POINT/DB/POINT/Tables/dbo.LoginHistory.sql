CREATE TABLE [dbo].[LoginHistory]
(
[LoginID] [int] NOT NULL IDENTITY(1, 1),
[LoginUsername] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LoginOrganizationName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LoginDateTime] [datetime] NULL,
[LoginResolution] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LoginInfo] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LoginIP] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LoginBrowser] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClientIP] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LocalAddress] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LoginFailed] [bit] NOT NULL CONSTRAINT [DF_LoginHistory_LoginFailed] DEFAULT ((0))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[LoginHistory] ADD CONSTRAINT [PK_LoginHistory] PRIMARY KEY CLUSTERED  ([LoginID]) ON [PRIMARY]
GO
