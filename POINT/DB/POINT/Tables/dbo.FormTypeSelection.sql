CREATE TABLE [dbo].[FormTypeSelection]
(
[FormTypeSelectionID] [int] NOT NULL IDENTITY(1, 1),
[FlowDefinitionID] [int] NOT NULL,
[TypeID] [int] NOT NULL,
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Value] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FormTypeSelection] ADD CONSTRAINT [PK_FormTypeSelection] PRIMARY KEY CLUSTERED  ([FormTypeSelectionID]) ON [PRIMARY]
GO
