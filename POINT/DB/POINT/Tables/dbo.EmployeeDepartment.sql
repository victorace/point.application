CREATE TABLE [dbo].[EmployeeDepartment]
(
[EmployeeID] [int] NOT NULL,
[DepartmentID] [int] NOT NULL,
[ModifiedTimeStamp] [datetime] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
 
-- ============================================= 
-- Author:		<Author,,Name> 
-- Create date: <Create Date,,> 
-- Description:	<Description,,> 
-- ============================================= 
CREATE TRIGGER [dbo].[EmployeeDepartment_Sync_Delete] 
   ON  [dbo].[EmployeeDepartment] 
   AFTER DELETE 
AS  
BEGIN 
 
	-- SET NOCOUNT ON added to prevent extra result sets from 
	-- interfering with SELECT statements. 
	SET NOCOUNT ON; 
 
	DELETE olddep 
	FROM Old_EmployeeDepartment olddep 
	INNER JOIN Deleted ON olddep.DepartmentID = Deleted.DepartmentID AND olddep.EmployeeID = Deleted.EmployeeID 
 
END 
 
 
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
  
  
-- =============================================  
-- Author:		<Author,,Name>  
-- Create date: <Create Date,,>  
-- Description:	<Description,,>  
-- =============================================  
CREATE TRIGGER [dbo].[EmployeeDepartment_Sync_Insert]  
   ON  [dbo].[EmployeeDepartment]  
   AFTER INSERT  
AS   
BEGIN  
  
	-- SET NOCOUNT ON added to prevent extra result sets from  
	-- interfering with SELECT statements.  
	SET NOCOUNT ON;  
  
	--For an insert:  
	INSERT INTO Old_EmployeeDepartment  
	        (   
			  EmployeeID ,  
			  DepartmentID  
	        )  
	SELECT   
		Inserted.EmployeeID ,  
		Inserted.DepartmentID  
	FROM INSERTED 
	INNER JOIN Old_Department on Old_Department.DepartmentID = Inserted.DepartmentID 
	INNER JOIN Old_Employee on Old_Employee.EmployeeID = inserted.EmployeeID
END 
  
GO
CREATE NONCLUSTERED INDEX [IX_EmployeeDepartment_DepartmentID] ON [dbo].[EmployeeDepartment] ([DepartmentID]) INCLUDE ([EmployeeID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_EmployeeDepartment_EmployeeID] ON [dbo].[EmployeeDepartment] ([EmployeeID]) INCLUDE ([DepartmentID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EmployeeDepartment] WITH NOCHECK ADD CONSTRAINT [FK_EmployeeDepartment_Department] FOREIGN KEY ([DepartmentID]) REFERENCES [dbo].[Department] ([DepartmentID])
GO
ALTER TABLE [dbo].[EmployeeDepartment] WITH NOCHECK ADD CONSTRAINT [FK_EmployeeDepartment_Employee] FOREIGN KEY ([EmployeeID]) REFERENCES [dbo].[Employee] ([EmployeeID])
GO
