CREATE TABLE [dbo].[MutTransferIndexedFunctions]
(
[MutTransferIndexedFunctionsID] [int] NOT NULL IDENTITY(1, 1),
[LogTransferIndexedFunctionsID] [int] NOT NULL,
[TransferID] [int] NOT NULL,
[IndexedFunctionID] [int] NOT NULL,
[IndexedFunctionClass] [tinyint] NULL,
[FromDate] [datetime] NULL,
[DueDate] [datetime] NULL,
[Remarks] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RemarksPersonal] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NumberZZP] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TimeStamp] [datetime] NULL,
[EmployeeID] [int] NULL,
[ScreenID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MutTransferIndexedFunctions] ADD CONSTRAINT [PK_MutTransferIndexedFunctions] PRIMARY KEY CLUSTERED  ([MutTransferIndexedFunctionsID]) ON [PRIMARY]
GO
