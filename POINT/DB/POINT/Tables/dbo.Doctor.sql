CREATE TABLE [dbo].[Doctor]
(
[DoctorID] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SearchName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SpecialismID] [int] NULL,
[OrganizationID] [int] NULL,
[AGB] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BIG] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PhoneNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Inactive] [bit] NOT NULL CONSTRAINT [DF_Doctor_Inactive] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Doctor] ADD CONSTRAINT [PK_Doctor] PRIMARY KEY CLUSTERED  ([DoctorID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Doctor_OrganizationID_Inactive_Includes] ON [dbo].[Doctor] ([OrganizationID], [Inactive]) INCLUDE ([AGB], [BIG], [DoctorID], [Name], [PhoneNumber], [SearchName], [SpecialismID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Doctor] ADD CONSTRAINT [FK_Doctor_Specialism] FOREIGN KEY ([SpecialismID]) REFERENCES [dbo].[Specialism] ([SpecialismID])
GO
