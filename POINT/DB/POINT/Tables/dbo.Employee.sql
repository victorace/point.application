CREATE TABLE [dbo].[Employee]
(
[EmployeeID] [int] NOT NULL IDENTITY(1, 1),
[DepartmentID] [int] NOT NULL,
[FirstName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MiddleName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Gender] [int] NULL,
[Position] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PhoneNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UserId] [uniqueidentifier] NULL,
[EmailAddress] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InActive] [bit] NOT NULL CONSTRAINT [DF_Employee_InActive] DEFAULT ((0)),
[ExternID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_Employee_ExternID] DEFAULT ('NULL'),
[ChangePasswordByEmployee] [bit] NOT NULL CONSTRAINT [DF_Employee_ChangePasswordByEmployee] DEFAULT ((0)),
[BIG] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModifiedTimeStamp] [datetime] NULL,
[MFANumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MFAKey] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MFAConfirmedDate] [datetime] NULL,
[EditAllForms] [bit] NOT NULL CONSTRAINT [DF_Employee_EditAllForms] DEFAULT ((0)),
[ReadOnlySender] [bit] NOT NULL CONSTRAINT [DF_Employee_ReadOnlySender] DEFAULT ((0)),
[ReadOnlyReciever] [bit] NOT NULL CONSTRAINT [DF_Employee_ReadOnlyReciever] DEFAULT ((0)),
[LockAction] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DigitalSignatureID] [int] NULL,
[CreatedWithAutoCreateSetID] [int] NULL,
[IPAddresses] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MFARequired] [bit] NOT NULL,
[LastRealLoginDate] [datetime] NULL,
[AutoLockAccountDate] [datetime] NULL,
[AutoEmailSignalering] [bit] NOT NULL CONSTRAINT [DF_Employee_AutoEmailSignalering] DEFAULT ((0))
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [dbo].[Employee_Sync_Insert]
   ON  [dbo].[Employee]
   AFTER INSERT
AS 
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--For an insert:
	INSERT INTO Old_Employee
	        ( 
			  EmployeeID ,
			  DepartmentID ,
	          FirstName ,
	          MiddleName ,
	          LastName ,
	          Gender ,
	          Position ,
	          PhoneNumber ,
	          UserId ,
	          EmailAddress ,
	          InActive ,
	          ExternID ,
	          ChangePasswordByEmployee ,
	          BIG ,
	          TimeStamp ,
	          MFANumber ,
	          MFAConfirmedDate
	        )
	SELECT 
		Inserted.EmployeeID ,
		Inserted.DepartmentID ,
	    Inserted.FirstName ,
	    Inserted.MiddleName ,
	    Inserted.LastName ,
	    Inserted.Gender ,
	    Inserted.Position ,
	    Inserted.PhoneNumber ,
	    Inserted.UserId ,
	    Inserted.EmailAddress ,
	    Inserted.InActive ,
	    Inserted.ExternID ,
	    Inserted.ChangePasswordByEmployee ,
	    Inserted.BIG ,
	    Inserted.ModifiedTimeStamp ,
	    Inserted.MFANumber ,
	    Inserted.MFAConfirmedDate
	FROM INSERTED
	INNER JOIN dbo.Old_Department ON dbo.Old_Department.DepartmentID = Inserted.DepartmentID
	WHERE NOT EXISTS (SELECT dbo.Old_Employee.EmployeeID FROM dbo.Old_Employee WHERE dbo.Old_Employee.EmployeeID = Inserted.EmployeeID)
END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- ============================================= 
-- Author:		<Author,,Name> 
-- Create date: <Create Date,,> 
-- Description:	<Description,,> 
-- ============================================= 
CREATE TRIGGER [dbo].[Employee_Sync_Update] 
   ON  [dbo].[Employee] 
   AFTER UPDATE 
AS  
BEGIN 
 
	-- SET NOCOUNT ON added to prevent extra result sets from 
	-- interfering with SELECT statements. 
	SET NOCOUNT ON; 
 
	--For an update 
	UPDATE Old_Employee 
	SET  
	Old_Employee.FirstName = INSERTED.FirstName, 
	Old_Employee.MiddleName = INSERTED.MiddleName, 
	Old_Employee.LastName = INSERTED.LastName, 
	Old_Employee.Gender = INSERTED.Gender, 
	Old_Employee.Position = INSERTED.Position, 
	Old_Employee.UserId = INSERTED.UserId, 
	Old_Employee.EmailAddress = INSERTED.EmailAddress, 
	Old_Employee.InActive = INSERTED.InActive, 
	Old_Employee.ExternID = INSERTED.ExternID, 
	Old_Employee.ChangePasswordByEmployee = INSERTED.ChangePasswordByEmployee, 
	Old_Employee.BIG = INSERTED.BIG, 
	Old_Employee.TimeStamp = INSERTED.ModifiedTimeStamp, 
	Old_Employee.MFANumber = INSERTED.MFANumber, 
	Old_Employee.MFAConfirmedDate = INSERTED.MFAConfirmedDate 
	FROM Old_Employee 
	INNER JOIN INSERTED ON Inserted.EmployeeID = dbo.Old_Employee.EmployeeID 
    -- Insert statements for trigger here 
 
END
GO
ALTER TABLE [dbo].[Employee] ADD CONSTRAINT [PK_Employee] PRIMARY KEY CLUSTERED  ([EmployeeID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Employee_DepartmentIDInactive] ON [dbo].[Employee] ([DepartmentID], [InActive]) INCLUDE ([BIG], [ChangePasswordByEmployee], [CreatedWithAutoCreateSetID], [DigitalSignatureID], [EditAllForms], [EmailAddress], [EmployeeID], [ExternID], [FirstName], [Gender], [IPAddresses], [LastName], [LastRealLoginDate], [LockAction], [MFAConfirmedDate], [MFAKey], [MFANumber], [MFARequired], [MiddleName], [ModifiedTimeStamp], [PhoneNumber], [Position], [ReadOnlyReciever], [ReadOnlySender], [UserId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Employee] ADD CONSTRAINT [IX_Employee] UNIQUE NONCLUSTERED  ([EmployeeID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Employee_EmploeeID_NameFields] ON [dbo].[Employee] ([EmployeeID]) INCLUDE ([BIG], [ChangePasswordByEmployee], [CreatedWithAutoCreateSetID], [DepartmentID], [DigitalSignatureID], [EditAllForms], [EmailAddress], [ExternID], [FirstName], [Gender], [InActive], [IPAddresses], [LastName], [LockAction], [MFAConfirmedDate], [MFANumber], [MFARequired], [MiddleName], [ModifiedTimeStamp], [PhoneNumber], [Position], [ReadOnlyReciever], [ReadOnlySender], [UserId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [ix_Employee_InActive_includes] ON [dbo].[Employee] ([InActive]) INCLUDE ([DepartmentID], [UserId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Employee_ExternID_Inactive] ON [dbo].[Employee] ([InActive], [ExternID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Employee_LastName] ON [dbo].[Employee] ([LastName]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Employee_UserId_InActive] ON [dbo].[Employee] ([UserId], [InActive]) INCLUDE ([DepartmentID], [EmployeeID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Employee] ADD CONSTRAINT [FK_Employee_aspnet_Users] FOREIGN KEY ([UserId]) REFERENCES [dbo].[aspnet_Users] ([UserId])
GO
ALTER TABLE [dbo].[Employee] ADD CONSTRAINT [FK_Employee_Department] FOREIGN KEY ([DepartmentID]) REFERENCES [dbo].[Department] ([DepartmentID])
GO
ALTER TABLE [dbo].[Employee] ADD CONSTRAINT [FK_Employee_DigitalSignature] FOREIGN KEY ([DigitalSignatureID]) REFERENCES [dbo].[DigitalSignature] ([DigitalSignatureID])
GO
