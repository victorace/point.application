CREATE TABLE [dbo].[Department]
(
[DepartmentID] [int] NOT NULL IDENTITY(1, 1),
[LocationID] [int] NOT NULL,
[DepartmentTypeID] [int] NOT NULL,
[Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PhoneNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FaxNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmailAddress] [varchar] (1024) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RecieveEmail] [bit] NOT NULL CONSTRAINT [DF_Department_RecieveEmail] DEFAULT ((0)),
[Inactive] [bit] NOT NULL CONSTRAINT [DF_Department_Inactive] DEFAULT ((0)),
[RehabilitationCenter] [bit] NOT NULL CONSTRAINT [DF_Department_RehabilitationCenter] DEFAULT ((0)),
[IntensiveRehabilitationNursinghome] [bit] NOT NULL CONSTRAINT [DF_Department_IntensiveRehabilitationNursinghome] DEFAULT ((0)),
[EmailAddressTransferSend] [varchar] (1024) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_Department_EmailAddressTransferSend] DEFAULT (NULL),
[RecieveEmailTransferSend] [bit] NOT NULL CONSTRAINT [DF_Department_RecieveEmailTransferSend] DEFAULT ('0'),
[ZorgmailTransferSend] [varchar] (1024) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_Department_ZorgmailTransferSend] DEFAULT (NULL),
[RecieveZorgmailTransferSend] [bit] NOT NULL CONSTRAINT [DF_Department_RecieveZorgmailTransferSend] DEFAULT ('0'),
[ModifiedTimeStamp] [datetime] NULL,
[CapacityDepartment] [int] NULL CONSTRAINT [DF__Departmen__Capac__20C2AC7B] DEFAULT ((-1)),
[CapacityDepartmentNext] [int] NULL CONSTRAINT [DF__Departmen__Capac__21B6D0B4] DEFAULT ((-1)),
[Information] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AdjustmentCapacityDate] [datetime] NULL,
[CapacityDepartmentDate] [datetime] NULL,
[CapacityDepartmentNextDate] [datetime] NULL,
[UserID] [int] NULL,
[CapacityDepartment3] [int] NULL CONSTRAINT [DF__Departmen__Capac__2A4C16B5] DEFAULT ((-1)),
[CapacityDepartment4] [int] NULL CONSTRAINT [DF__Departmen__Capac__2B403AEE] DEFAULT ((-1)),
[CapacityDepartment3Date] [datetime] NULL,
[CapacityDepartment4Date] [datetime] NULL,
[CapacityFunctionality] [bit] NOT NULL CONSTRAINT [DF__Departmen__Capac__2C345F27] DEFAULT ((0)),
[RecieveCDATransferSend] [bit] NOT NULL CONSTRAINT [DF__Departmen__Recie__7ABCFF24] DEFAULT ((0)),
[AfterCareType1ID] [int] NULL,
[AddressGPForZorgmail] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AGB] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SortOrder] [int] NULL,
[ExternID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsAcute] [bit] NOT NULL CONSTRAINT [DF_Department_IsAcute] DEFAULT ((0))
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE trigger [dbo].[Department_Auth_Sync_InsertUpdate]  
   on  [dbo].[Department]  
   after insert, update  
as   
begin  
 
	-- SET NOCOUNT ON added to prevent extra result sets from  
	-- interfering with SELECT statements.  
	set nocount on;  

	insert into dbo.AuthOrganization (OrganizationID, SSO, SSOKey, SSOHashType, CreateUserViaSSO, AGB, Inactive)
	select OrganizationID, isnull(SSO, 0), SSOKey, SSOHashType, isnull(CreateUserViaSSO, 0), AGB, Inactive from dbo.Organization
	where OrganizationID not in (select dbo.AuthOrganization.OrganizationID from dbo.AuthOrganization);

	insert into dbo.AuthDepartment (DepartmentID, OrganizationID, LocationID, AGB, Inactive)
		select DepartmentID, Organization.OrganizationID, Location.LocationID, Department.AGB, Department.Inactive from dbo.Department
		inner join dbo.Location on Department.LocationID = Location.LocationID
		inner join dbo.Organization on Location.OrganizationID = Organization.OrganizationID
	where DepartmentID not in (select dbo.AuthDepartment.DepartmentID from dbo.AuthDepartment);

	update dbo.AuthDepartment 
		set 
			OrganizationID = Location.OrganizationID, 
			LocationID = Location.LocationID,
			AGB = Inserted.AGB,
			Inactive = Inserted.Inactive	
	from dbo.AuthDepartment  
		inner join Inserted on Inserted.DepartmentID = AuthDepartment.DepartmentID
		inner join dbo.Location on Inserted.LocationID = Location.LocationID; 

end;
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[Department_DepartmentCapacity_Insert] ON [dbo].[Department] AFTER INSERT, UPDATE   
AS   
BEGIN   
       
  SET NOCOUNT ON   
   
   --Update of these fields is not logged: Capacity, CapacityNext, Information, AdjustmentCapacityDate   
  IF (UPDATE(CapacityDepartment) OR UPDATE(CapacityDepartmentNext) OR UPDATE(Information) OR UPDATE(AdjustmentCapacityDate)   
    OR UPDATE(CapacityDepartment3) OR UPDATE(CapacityDepartment4))   
    BEGIN   
   
      INSERT INTO DepartmentCapacity([DepartmentID]   
        --,[DepartmentName]   
        ,[CapacityDepartment]    
        ,[CapacityDepartmentDate]   
        ,[CapacityDepartmentNext]   
        ,[CapacityDepartmentNextDate]   
        ,[CapacityDepartment3]    
        ,[CapacityDepartment3Date]   
        ,[CapacityDepartment4]   
        ,[CapacityDepartment4Date]           
        ,[Information]   
        ,[AdjustmentCapacityDate]   
        ,[UserID]   
        )   
      SELECT         
         [DepartmentID]   
        --,[Name]   
        ,[CapacityDepartment]    
        ,[CapacityDepartmentDate]   
        ,[CapacityDepartmentNext]   
        ,[CapacityDepartmentNextDate]   
        ,[CapacityDepartment3]    
        ,[CapacityDepartment3Date]   
        ,[CapacityDepartment4]   
        ,[CapacityDepartment4Date]           
        ,[Information]   
        ,[AdjustmentCapacityDate]   
        ,[UserID]
      FROM Inserted i   
      WHERE NULLIF(AdjustmentCapacityDate, '') IS NOT NULL   
       
    --RETURN   
  END   
 END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[Department_Sync_Insert]  
   ON  [dbo].[Department]  
   AFTER INSERT  
AS   
BEGIN  
  
	-- SET NOCOUNT ON added to prevent extra result sets from  
	-- interfering with SELECT statements.  
	SET NOCOUNT ON;  
  
	--For an insert:  
	INSERT INTO Old_Department  
	        ( DepartmentID ,  
	          LocationID ,  
	          DepartmentTypeID ,  
	          Name ,  
	          PhoneNumber ,  
	          FaxNumber ,  
	          EmailAddress ,  
	          RecieveEmail ,  
	          Inactive ,  
	          RehabilitationCenter ,  
	          IntensiveRehabilitationNursinghome ,  
	          EmailAddressTransferSend ,  
	          RecieveEmailTransferSend ,  
	          ZorgmailTransferSend ,  
	          RecieveZorgmailTransferSend ,  
	          TimeStamp ,  
	          CapacityDepartment ,  
	          CapacityDepartmentNext ,  
	          Information ,  
	          AdjustmentCapacityDate ,  
	          CapacityDepartmentDate ,  
	          CapacityDepartmentNextDate ,  
	          UserID ,  
	         -- UserName ,  
	          CapacityDepartment3 ,  
	          CapacityDepartment4 ,  
	          CapacityDepartment3Date ,  
	          CapacityDepartment4Date ,  
	          CapacityFunctionality ,  
	          RecieveCDATransferSend
	        )  
	SELECT   
		Inserted.DepartmentID ,  
	    Inserted.LocationID ,  
	    Inserted.DepartmentTypeID ,  
	    Inserted.Name ,  
	    Inserted.PhoneNumber ,  
	    Inserted.FaxNumber ,  
	    Inserted.EmailAddress ,  
	    Inserted.RecieveEmail ,  
	    Inserted.Inactive ,  
	    Inserted.RehabilitationCenter ,  
	    Inserted.IntensiveRehabilitationNursinghome ,  
	    Inserted.EmailAddressTransferSend ,  
	    Inserted.RecieveEmailTransferSend ,  
	    Inserted.ZorgmailTransferSend ,  
	    Inserted.RecieveZorgmailTransferSend ,  
	    Inserted.ModifiedTimeStamp ,  
	    Inserted.CapacityDepartment ,  
	    Inserted.CapacityDepartmentNext ,  
	    Inserted.Information ,  
	    Inserted.AdjustmentCapacityDate ,  
	    Inserted.CapacityDepartmentDate ,  
	    Inserted.CapacityDepartmentNextDate ,  
	    Inserted.UserID ,  
	   -- Inserted.UserName ,  
	    Inserted.CapacityDepartment3 ,  
	    Inserted.CapacityDepartment4 ,  
	    Inserted.CapacityDepartment3Date ,  
	    Inserted.CapacityDepartment4Date ,  
	    Inserted.CapacityFunctionality ,  
	    Inserted.RecieveCDATransferSend
	FROM INSERTED  
	INNER JOIN Old_Location ON Old_Location.LocationID = Inserted.LocationID  
END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[Department_Sync_Update]  
   ON  [dbo].[Department]  
   AFTER UPDATE  
AS   
BEGIN  
  
	 
 
	-- SET NOCOUNT ON added to prevent extra result sets from  
	-- interfering with SELECT statements.  
	SET NOCOUNT ON;  
 
	--For an update  
	UPDATE Old_Department  
	SET   
	Old_Department.DepartmentTypeID = INSERTED.DepartmentTypeID,  
	Old_Department.Name = INSERTED.Name,  
	Old_Department.PhoneNumber = INSERTED.PhoneNumber,  
	Old_Department.FaxNumber = INSERTED.FaxNumber,  
	Old_Department.EmailAddress = INSERTED.EmailAddress,  
	Old_Department.RecieveEmail = INSERTED.RecieveEmail,  
	Old_Department.Inactive = INSERTED.Inactive,  
	Old_Department.RehabilitationCenter = INSERTED.RehabilitationCenter,  
	Old_Department.IntensiveRehabilitationNursinghome = INSERTED.IntensiveRehabilitationNursinghome,  
	Old_Department.EmailAddressTransferSend = INSERTED.EmailAddressTransferSend,  
	Old_Department.RecieveEmailTransferSend = INSERTED.RecieveEmailTransferSend,  
	Old_Department.ZorgmailTransferSend = INSERTED.ZorgmailTransferSend,  
	Old_Department.RecieveZorgmailTransferSend = INSERTED.RecieveZorgmailTransferSend,  
	Old_Department.TimeStamp = INSERTED.ModifiedTimeStamp,  
	Old_Department.CapacityDepartment = INSERTED.CapacityDepartment,  
	Old_Department.CapacityDepartmentNext = INSERTED.CapacityDepartmentNext,  
	Old_Department.Information = INSERTED.Information,  
	Old_Department.AdjustmentCapacityDate = INSERTED.AdjustmentCapacityDate,  
	Old_Department.CapacityDepartmentDate = INSERTED.CapacityDepartmentDate,  
	Old_Department.CapacityDepartmentNextDate = INSERTED.CapacityDepartmentNextDate,  
	Old_Department.UserID = INSERTED.UserID,  
--	Old_Department.UserName = INSERTED.UserName,  
	Old_Department.CapacityDepartment3 = INSERTED.CapacityDepartment3,  
	Old_Department.CapacityDepartment4 = INSERTED.CapacityDepartment4,  
	Old_Department.CapacityDepartment3Date = INSERTED.CapacityDepartment3Date,  
	Old_Department.CapacityDepartment4Date = INSERTED.CapacityDepartment4Date,  
	Old_Department.CapacityFunctionality = INSERTED.CapacityFunctionality,  
	Old_Department.RecieveCDATransferSend = INSERTED.RecieveCDATransferSend
	  
	FROM Old_Department  
	INNER JOIN INSERTED ON Inserted.DepartmentID = dbo.Old_Department.DepartmentID
 
END
GO
ALTER TABLE [dbo].[Department] ADD CONSTRAINT [PK_Department] PRIMARY KEY CLUSTERED  ([DepartmentID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Department_DepartmentID_Includes] ON [dbo].[Department] ([DepartmentID]) INCLUDE ([AddressGPForZorgmail], [AdjustmentCapacityDate], [AfterCareType1ID], [AGB], [CapacityDepartment], [CapacityDepartment3], [CapacityDepartment3Date], [CapacityDepartment4], [CapacityDepartment4Date], [CapacityDepartmentDate], [CapacityDepartmentNext], [CapacityDepartmentNextDate], [CapacityFunctionality], [DepartmentTypeID], [EmailAddress], [EmailAddressTransferSend], [ExternID], [FaxNumber], [Inactive], [Information], [IntensiveRehabilitationNursinghome], [IsAcute], [LocationID], [ModifiedTimeStamp], [Name], [PhoneNumber], [RecieveCDATransferSend], [RecieveEmail], [RecieveEmailTransferSend], [RecieveZorgmailTransferSend], [RehabilitationCenter], [SortOrder], [UserID], [ZorgmailTransferSend]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Department_Inactive_Includes] ON [dbo].[Department] ([Inactive]) INCLUDE ([CapacityFunctionality], [DepartmentID], [LocationID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Department_Inactive] ON [dbo].[Department] ([Inactive]) INCLUDE ([DepartmentID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Department_LocationID] ON [dbo].[Department] ([LocationID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Department] ADD CONSTRAINT [FK_Department_AfterCareType] FOREIGN KEY ([AfterCareType1ID]) REFERENCES [dbo].[AfterCareType] ([AfterCareTypeID])
GO
ALTER TABLE [dbo].[Department] ADD CONSTRAINT [FK_Department_DepartmentType] FOREIGN KEY ([DepartmentTypeID]) REFERENCES [dbo].[DepartmentType] ([DepartmentTypeID])
GO
ALTER TABLE [dbo].[Department] ADD CONSTRAINT [FK_Department_Location] FOREIGN KEY ([LocationID]) REFERENCES [dbo].[Location] ([LocationID])
GO
