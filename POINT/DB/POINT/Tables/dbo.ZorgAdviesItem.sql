CREATE TABLE [dbo].[ZorgAdviesItem]
(
[ZorgAdviesItemID] [int] NOT NULL IDENTITY(1, 1),
[ZorgAdviesItemGroupID] [int] NOT NULL,
[SubCategory] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Label] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NeedsFrequencyAndTime] [bit] NOT NULL CONSTRAINT [DF_ZorgPlanItem_NeedsFrequencyAndTime] DEFAULT ((0)),
[OrderNumber] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ZorgAdviesItem] ADD CONSTRAINT [PK_ZorgAdviesItem] PRIMARY KEY CLUSTERED  ([ZorgAdviesItemID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_ZorgAdviesItem] ON [dbo].[ZorgAdviesItem] ([OrderNumber]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ZorgAdviesItem] ADD CONSTRAINT [FK_ZorgPlanItem_ZorgPlanItemGroup] FOREIGN KEY ([ZorgAdviesItemGroupID]) REFERENCES [dbo].[ZorgAdviesItemGroup] ([ZorgAdviesItemGroupID])
GO
