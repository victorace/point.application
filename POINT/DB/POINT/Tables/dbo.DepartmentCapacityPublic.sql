CREATE TABLE [dbo].[DepartmentCapacityPublic]
(
[DepartmentCapacityPublicID] [int] NOT NULL IDENTITY(1, 1),
[AfterCareTypeID] [int] NULL,
[AfterCareTypeName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AfterCareGroupID] [int] NULL,
[RegionID] [int] NOT NULL,
[RegionName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OrganizationID] [int] NOT NULL,
[OrganizationName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LocationID] [int] NOT NULL,
[LocationName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LocationStreet] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LocationNumber] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LocationPostalCode] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LocationCity] [varchar] (512) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LocationEmailAddress] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DepartmentID] [int] NOT NULL,
[DepartmentName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DepartmentPhoneNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DepartmentFaxNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DepartmentEmailAddress] [varchar] (1024) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsActive] [bit] NOT NULL CONSTRAINT [DF_DepartmentCapacityPublic_IsActive] DEFAULT ((0)),
[AdjustmentCapacityDate] [datetime] NULL,
[Capacity1] [int] NULL,
[Capacity2] [int] NULL,
[Capacity3] [int] NULL,
[Capacity4] [int] NULL,
[Information] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModifiedBy] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModifiedOn] [datetime] NULL,
[CapacityFunctionality] [bit] NULL,
[OrganizationTypeID] [int] NOT NULL CONSTRAINT [DF_DepartmentCapacityPublic_OrganizationTypeID] DEFAULT ((0)),
[Latitude] [float] NOT NULL CONSTRAINT [DF__Departmen__Latit__3AE33C88] DEFAULT ((0)),
[Longitude] [float] NOT NULL CONSTRAINT [DF__Departmen__Longi__3BD760C1] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DepartmentCapacityPublic] ADD CONSTRAINT [PK_DepartmentCapacityPublic] PRIMARY KEY CLUSTERED  ([DepartmentCapacityPublicID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [ix_DepartmentCapacityPublic_Includes] ON [dbo].[DepartmentCapacityPublic] ([DepartmentCapacityPublicID]) INCLUDE ([AfterCareGroupID], [AfterCareTypeID], [Capacity1], [Capacity2], [Capacity3], [Capacity4], [DepartmentName], [IsActive], [Latitude], [LocationCity], [LocationName], [LocationPostalCode], [Longitude], [OrganizationName], [RegionID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [ix_DepartmentCapacityPublic_RegionID_AfterCareGroupID_AfterCareTypeID] ON [dbo].[DepartmentCapacityPublic] ([RegionID], [AfterCareGroupID], [AfterCareTypeID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DepartmentCapacityPublic] ADD CONSTRAINT [FK_DepartmentCapacityPublic_Location] FOREIGN KEY ([LocationID]) REFERENCES [dbo].[Location] ([LocationID])
GO
