CREATE TABLE [dbo].[ZorgAdviesItemValue]
(
[ZorgAdviesItemValueID] [int] NOT NULL IDENTITY(1, 1),
[ZorgAdviesItemID] [int] NOT NULL,
[FlowInstanceID] [int] NOT NULL,
[Text] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActionBy] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActionFrequency] [int] NULL,
[ActionTimespan] [int] NULL,
[ActionTime] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ZorgAdviesItemValue] ADD CONSTRAINT [PK_ZorgAdviesItemValue] PRIMARY KEY CLUSTERED  ([ZorgAdviesItemValueID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ZorgAdviesItemValue] ADD CONSTRAINT [FK_ZorgPlanItemValue_ZorgPlanItem] FOREIGN KEY ([ZorgAdviesItemID]) REFERENCES [dbo].[ZorgAdviesItem] ([ZorgAdviesItemID])
GO
