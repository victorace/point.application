CREATE TABLE [dbo].[ActionCodeHealthInsurerList]
(
[ActionCodeHealthInsurerListID] [int] NOT NULL IDENTITY(1, 1),
[ActionCodeHealthInsurerRegionID] [int] NULL,
[ActionCodeHealthInsurerID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ActionCodeHealthInsurerList] ADD CONSTRAINT [PK_ActionCodeHealthInsurerList] PRIMARY KEY CLUSTERED  ([ActionCodeHealthInsurerListID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ActionCodeHealthInsurerList] ADD CONSTRAINT [FK_ActionCodeHealthInsurerList_ActionCodeHealthInsurer] FOREIGN KEY ([ActionCodeHealthInsurerID]) REFERENCES [dbo].[ActionCodeHealthInsurer] ([ActionCodeHealthInsurerID])
GO
ALTER TABLE [dbo].[ActionCodeHealthInsurerList] ADD CONSTRAINT [FK_ActionCodeHealthInsurerList_ActionCodeHealthInsurerRegio] FOREIGN KEY ([ActionCodeHealthInsurerRegionID]) REFERENCES [dbo].[ActionCodeHealthInsurerRegio] ([ActionCodeHealthInsurerRegionID])
GO
