CREATE TABLE [dbo].[FlowInstanceOrganization]
(
[FlowInstanceID] [int] NOT NULL,
[FlowDirection] [int] NOT NULL,
[OrganizationID] [int] NOT NULL,
[LocationID] [int] NOT NULL,
[DepartmentID] [int] NOT NULL,
[InviteType] [int] NOT NULL,
[InviteStatus] [int] NOT NULL,
[InviteSend] [datetime] NULL,
[ClientIsAnonymous] [bit] NOT NULL CONSTRAINT [DF_FlowInstanceOrganization_ClientIsAnonymous] DEFAULT ((0)),
[HandlingBy] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HandlingRemark] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HandlingDateTime] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FlowInstanceOrganization] ADD CONSTRAINT [PK_FlowInstanceOrganization] PRIMARY KEY CLUSTERED  ([FlowInstanceID], [FlowDirection], [DepartmentID], [InviteType], [InviteStatus]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FlowInstanceOrganization_DepartmentID] ON [dbo].[FlowInstanceOrganization] ([DepartmentID], [FlowDirection]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FlowInstanceOrganization_FlowInstanceID] ON [dbo].[FlowInstanceOrganization] ([FlowInstanceID]) INCLUDE ([ClientIsAnonymous], [DepartmentID], [FlowDirection], [HandlingBy], [HandlingDateTime], [HandlingRemark], [InviteSend], [InviteStatus], [InviteType], [LocationID], [OrganizationID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FlowInstanceOrganization_LocationID] ON [dbo].[FlowInstanceOrganization] ([LocationID], [FlowDirection]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FlowInstanceOrganization_OrganizationID] ON [dbo].[FlowInstanceOrganization] ([OrganizationID], [FlowDirection]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FlowInstanceOrganization] ADD CONSTRAINT [FK_FlowInstanceOrganization_Department] FOREIGN KEY ([DepartmentID]) REFERENCES [dbo].[Department] ([DepartmentID])
GO
ALTER TABLE [dbo].[FlowInstanceOrganization] WITH NOCHECK ADD CONSTRAINT [FK_FlowInstanceOrganization_FlowInstanceReportValues] FOREIGN KEY ([FlowInstanceID]) REFERENCES [dbo].[FlowInstanceReportValues] ([FlowInstanceID])
GO
ALTER TABLE [dbo].[FlowInstanceOrganization] WITH NOCHECK ADD CONSTRAINT [FK_FlowInstanceOrganization_FlowInstanceSearchValues] FOREIGN KEY ([FlowInstanceID]) REFERENCES [dbo].[FlowInstanceSearchValues] ([FlowInstanceID])
GO
ALTER TABLE [dbo].[FlowInstanceOrganization] ADD CONSTRAINT [FK_FlowInstanceOrganization_Organization] FOREIGN KEY ([OrganizationID]) REFERENCES [dbo].[Organization] ([OrganizationID])
GO
ALTER TABLE [dbo].[FlowInstanceOrganization] NOCHECK CONSTRAINT [FK_FlowInstanceOrganization_FlowInstanceReportValues]
GO
ALTER TABLE [dbo].[FlowInstanceOrganization] NOCHECK CONSTRAINT [FK_FlowInstanceOrganization_FlowInstanceSearchValues]
GO
