CREATE TABLE [dbo].[ZorgKantoorFunctiesIn]
(
[ZorgKantoorMessageID] [int] NOT NULL IDENTITY(1, 1),
[Code] [int] NULL,
[StartDatum] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EindDatum] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Klasse] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ZZPNummer] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ZZPCodeIndicatie] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DatumToewijzing] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MessageID] [int] NULL,
[StartdateYear] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StartdateMonth] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StartdateDay] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EnddateYear] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EnddateMonth] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EnddateDay] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateGivenYear] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateGivenMonth] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateGivenDay] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StartDatumD] [datetime] NULL,
[EindDatumD] [datetime] NULL,
[DatumToewijzingD] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ZorgKantoorFunctiesIn] ADD CONSTRAINT [PK_ZorgKantoorFunctiesIn] PRIMARY KEY CLUSTERED  ([ZorgKantoorMessageID]) ON [PRIMARY]
GO
