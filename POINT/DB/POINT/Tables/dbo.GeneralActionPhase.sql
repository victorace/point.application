CREATE TABLE [dbo].[GeneralActionPhase]
(
[GeneralActionPhaseID] [int] NOT NULL IDENTITY(1, 1),
[PhaseDefinitionID] [int] NOT NULL,
[GeneralActionID] [int] NOT NULL,
[PhaseStatusBegin] [int] NULL,
[PhaseStatusEnd] [int] NULL,
[OrganizationTypeID] [int] NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[GeneralActionPhase] ADD
CONSTRAINT [FK_GeneralActionPhase_OrganizationType] FOREIGN KEY ([OrganizationTypeID]) REFERENCES [dbo].[OrganizationType] ([OrganizationTypeID])
GO
ALTER TABLE [dbo].[GeneralActionPhase] ADD CONSTRAINT [PK_dbo.GeneralActionPhase] PRIMARY KEY CLUSTERED  ([GeneralActionPhaseID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_GeneralActionID] ON [dbo].[GeneralActionPhase] ([GeneralActionID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_PhaseDefinitionID] ON [dbo].[GeneralActionPhase] ([PhaseDefinitionID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[GeneralActionPhase] ADD CONSTRAINT [FK_dbo.GeneralActionPhase_dbo.GeneralAction_GeneralActionID] FOREIGN KEY ([GeneralActionID]) REFERENCES [dbo].[GeneralAction] ([GeneralActionID])
GO
ALTER TABLE [dbo].[GeneralActionPhase] ADD CONSTRAINT [FK_dbo.GeneralActionPhase_dbo.PhaseDefinition_PhaseDefinitionID] FOREIGN KEY ([PhaseDefinitionID]) REFERENCES [dbo].[PhaseDefinition] ([PhaseDefinitionID])
GO
