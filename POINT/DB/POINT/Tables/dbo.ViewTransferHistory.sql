CREATE TABLE [dbo].[ViewTransferHistory]
(
[ViewTransferHistoryID] [int] NOT NULL IDENTITY(1, 1),
[EmployeeID] [int] NOT NULL,
[TransferID] [int] NOT NULL,
[ViewDateTime] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ViewTransferHistory] ADD CONSTRAINT [PK_ViewTransferHistory] PRIMARY KEY CLUSTERED  ([ViewTransferHistoryID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_ViewTransferHistory_EmployeeID] ON [dbo].[ViewTransferHistory] ([EmployeeID]) INCLUDE ([TransferID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_ViewTransferHistory_EmployeeIDTransferID] ON [dbo].[ViewTransferHistory] ([EmployeeID], [TransferID]) ON [PRIMARY]
GO
