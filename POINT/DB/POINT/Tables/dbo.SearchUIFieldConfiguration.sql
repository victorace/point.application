CREATE TABLE [dbo].[SearchUIFieldConfiguration]
(
[SearchUIFieldConfigurationID] [int] NOT NULL IDENTITY(1, 1),
[SearchUIConfigurationID] [int] NOT NULL,
[Field_IsWellKnown] [bit] NULL,
[Field_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Column_Width] [int] NOT NULL CONSTRAINT [DF_SearchUIFieldConfiguration_Column_Width] DEFAULT ((150)),
[Column_IsVisible] [bit] NOT NULL,
[Column_Index] [int] NOT NULL,
[Icon_ShowAsIcon] [bit] NOT NULL,
[Icon_GlyphName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Icon_GlyphNameFalse] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Icon_TreatEmptyAsNull] [bit] NOT NULL,
[Icon_TreatNullAsFalse] [bit] NOT NULL,
[UseInSearchFilter] [bit] NOT NULL,
[Column_CssClass] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_SearchUIFieldConfiguration_Field_CssClass] DEFAULT (N'search-cell-medium'),
[Column_FullName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_SearchUIFieldConfiguration_Column_FullName] DEFAULT (''),
[Column_HeaderName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_SearchUIFieldConfiguration_Column_HeaderName] DEFAULT (''),
[Column_IsSortable] [bit] NOT NULL CONSTRAINT [DF_SearchUIFieldConfiguration_Column_IsSortable] DEFAULT ((1)),
[SelectedInSearchFilter] [bit] NOT NULL CONSTRAINT [DF_SearchUIFieldConfiguration_SelectedInSearchFilter] DEFAULT ((0)),
[SearchDestination] [int] NOT NULL CONSTRAINT [DF_SearchUIFieldConfiguration_ClickDestination] DEFAULT ((0)),
[SearchPropertyType] [int] NOT NULL CONSTRAINT [DF_SearchUIFieldConfiguration_SearchPropertyType] DEFAULT ((0)),
[SearchFieldOwningType] [int] NOT NULL CONSTRAINT [DF_SearchUIFieldConfiguration_SearchFieldOwningType] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SearchUIFieldConfiguration] ADD CONSTRAINT [PK_dbo.SearchUIFieldConfiguration] PRIMARY KEY CLUSTERED  ([SearchUIFieldConfigurationID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SearchUIFieldConfiguration] ADD CONSTRAINT [FK_dbo.SearchUIFieldConfiguration_dbo.SearchUIConfiguration_SearchUIConfigurationID] FOREIGN KEY ([SearchUIConfigurationID]) REFERENCES [dbo].[SearchUIConfiguration] ([SearchUIConfigurationID]) ON DELETE CASCADE
GO
