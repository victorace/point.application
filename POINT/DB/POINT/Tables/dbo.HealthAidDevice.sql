CREATE TABLE [dbo].[HealthAidDevice]
(
[HealthAidDeviceId] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Sortorder] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[HealthAidDevice] ADD CONSTRAINT [PK_HealthAidDevice] PRIMARY KEY CLUSTERED  ([HealthAidDeviceId]) ON [PRIMARY]
GO
