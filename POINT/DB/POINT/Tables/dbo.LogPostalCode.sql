CREATE TABLE [dbo].[LogPostalCode]
(
[LogPostalCodeID] [int] NOT NULL IDENTITY(1, 1),
[PostalCodeID] [int] NOT NULL,
[TimeStamp] [datetime] NULL,
[EmployeeID] [int] NULL,
[Action] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ScreenID] [int] NULL,
[Version] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[LogPostalCode] ADD CONSTRAINT [PK_LogPostalCode] PRIMARY KEY CLUSTERED  ([LogPostalCodeID]) ON [PRIMARY]
GO
