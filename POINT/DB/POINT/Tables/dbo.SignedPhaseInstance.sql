CREATE TABLE [dbo].[SignedPhaseInstance]
(
[SignedPhaseInstanceID] [int] NOT NULL IDENTITY(1, 1),
[PhaseInstanceID] [int] NOT NULL,
[SignedByID] [int] NOT NULL,
[DigitalSignatureID] [int] NOT NULL,
[EmployeeID] [int] NOT NULL,
[Timestamp] [datetime] NOT NULL,
[ScreenName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SignedPhaseInstance] ADD CONSTRAINT [PK_SignedPhaseInstance] PRIMARY KEY CLUSTERED  ([SignedPhaseInstanceID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SignedPhaseInstance] ADD CONSTRAINT [FK_SignedPhaseInstance_DigitalSignature] FOREIGN KEY ([DigitalSignatureID]) REFERENCES [dbo].[DigitalSignature] ([DigitalSignatureID])
GO
ALTER TABLE [dbo].[SignedPhaseInstance] ADD CONSTRAINT [FK_SignedPhaseInstance_Employee] FOREIGN KEY ([SignedByID]) REFERENCES [dbo].[Employee] ([EmployeeID])
GO
ALTER TABLE [dbo].[SignedPhaseInstance] ADD CONSTRAINT [FK_SignedPhaseInstance_PhaseInstance] FOREIGN KEY ([PhaseInstanceID]) REFERENCES [dbo].[PhaseInstance] ([PhaseInstanceID])
GO
