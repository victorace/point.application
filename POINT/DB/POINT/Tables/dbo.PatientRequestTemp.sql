CREATE TABLE [dbo].[PatientRequestTemp]
(
[PatientRequestTempID] [int] NOT NULL IDENTITY(1, 1),
[Timestamp] [datetime] NOT NULL,
[GUID] [uniqueidentifier] NOT NULL,
[InterfaceCode] [uniqueidentifier] NULL,
[OrganizationID] [int] NULL,
[PatientNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Handled] [bit] NOT NULL CONSTRAINT [DF_PatientRequestTemp_Handled] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PatientRequestTemp] ADD CONSTRAINT [PK_PatientRequestTemp] PRIMARY KEY CLUSTERED  ([PatientRequestTempID]) ON [PRIMARY]
GO
