CREATE TABLE [dbo].[MutDepartment]
(
[MutDepartmentID] [int] NOT NULL IDENTITY(1, 1),
[LogDepartmentID] [int] NULL,
[LocationID] [int] NOT NULL,
[DepartmentID] [int] NOT NULL,
[DepartmentTypeID] [int] NOT NULL,
[SpecialismID] [int] NULL,
[Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PhoneNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FaxNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmailAddress] [varchar] (1024) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RecieveEmail] [bit] NOT NULL,
[MainContactID] [int] NULL,
[Inactive] [bit] NULL,
[SpecialismID2] [int] NULL,
[RehabilitationCenter] [bit] NULL,
[IntensiveRehabilitationNursinghome] [bit] NULL,
[EmailAddressTransferSend] [varchar] (1024) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RecieveEmailTransferSend] [bit] NULL,
[ZorgmailTransferSend] [varchar] (1024) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RecieveZorgmailTransferSend] [bit] NULL,
[TimeStamp] [datetime] NULL,
[EmployeeID] [int] NULL,
[ScreenID] [int] NULL,
[CapacityFunctionality] [bit] NULL CONSTRAINT [DF__MutDepart__Capac__2D288360] DEFAULT ((0)),
[PointParticipatorCareAccess] [bit] NULL CONSTRAINT [DF__MutDepart__Point__6849492E] DEFAULT ((0)),
[RecieveCDATransferSend] [bit] NULL CONSTRAINT [DF__MutDepart__Recie__7BB1235D] DEFAULT ((0)),
[CapacityDepartmentHomeCare] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CapacityDepartmentNextHomeCare] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CapacityDepartment3HomeCare] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CapacityDepartment4HomeCare] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AfterCareType1ID] [int] NULL,
[AfterCareType2ID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MutDepartment] ADD CONSTRAINT [PK_MutDepartment] PRIMARY KEY CLUSTERED  ([MutDepartmentID]) ON [PRIMARY]
GO
