CREATE TABLE [dbo].[GPSPlaats]
(
[GPSPlaatsID] [int] NOT NULL IDENTITY(1, 1),
[Naam] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GPSGemeenteID] [int] NULL,
[GPSProvincieID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[GPSPlaats] ADD CONSTRAINT [PK__Plaatsen__3214EC278A5C4AB9] PRIMARY KEY CLUSTERED  ([GPSPlaatsID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[GPSPlaats] ADD CONSTRAINT [FK_GPSPlaats_GPSGemeente] FOREIGN KEY ([GPSGemeenteID]) REFERENCES [dbo].[GPSGemeente] ([GPSGemeenteID])
GO
ALTER TABLE [dbo].[GPSPlaats] ADD CONSTRAINT [FK_GPSPlaats_GPSProvincie] FOREIGN KEY ([GPSProvincieID]) REFERENCES [dbo].[GPSProvincie] ([GPSProvincieID])
GO
