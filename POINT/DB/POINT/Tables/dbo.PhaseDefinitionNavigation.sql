CREATE TABLE [dbo].[PhaseDefinitionNavigation]
(
[PhaseDefinitionID] [int] NOT NULL,
[NextPhaseDefinitionID] [int] NOT NULL,
[ActionTypeID] [int] NOT NULL,
[ActionLabel] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Validate] [bit] NOT NULL CONSTRAINT [DF_PhaseDefinitionNavigation_Validate] DEFAULT ((0)),
[Visible] [bit] NOT NULL CONSTRAINT [DF_PhaseDefinitionNavigation_Visible] DEFAULT ((1))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PhaseDefinitionNavigation] ADD CONSTRAINT [PK_PhaseDefinitionNavigation_1] PRIMARY KEY CLUSTERED  ([PhaseDefinitionID], [NextPhaseDefinitionID], [ActionTypeID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PhaseDefinitionNavigation] ADD CONSTRAINT [FK_PhaseDefinitionNavigation_PhaseDefinition] FOREIGN KEY ([PhaseDefinitionID]) REFERENCES [dbo].[PhaseDefinition] ([PhaseDefinitionID])
GO
ALTER TABLE [dbo].[PhaseDefinitionNavigation] ADD CONSTRAINT [FK_PhaseDefinitionNavigation_TargetPhaseDefinition] FOREIGN KEY ([NextPhaseDefinitionID]) REFERENCES [dbo].[PhaseDefinition] ([PhaseDefinitionID])
GO
