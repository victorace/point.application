CREATE TABLE [dbo].[UsedMFACode]
(
[UsedMFACodeID] [int] NOT NULL IDENTITY(1, 1),
[MFACodeType] [int] NOT NULL,
[EmployeeID] [int] NOT NULL,
[OrganizationID] [int] NOT NULL,
[MFANumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MFAKey] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SentDateTime] [datetime] NOT NULL,
[MFACode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ClaimDateTime] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[UsedMFACode] ADD CONSTRAINT [PK_UsedMFACode] PRIMARY KEY CLUSTERED  ([UsedMFACodeID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[UsedMFACode] ADD CONSTRAINT [FK_UsedMFACode_EmployeeID] FOREIGN KEY ([EmployeeID]) REFERENCES [dbo].[Employee] ([EmployeeID])
GO
ALTER TABLE [dbo].[UsedMFACode] ADD CONSTRAINT [FK_UsedMFACode_OrganizationID] FOREIGN KEY ([OrganizationID]) REFERENCES [dbo].[Organization] ([OrganizationID])
GO
