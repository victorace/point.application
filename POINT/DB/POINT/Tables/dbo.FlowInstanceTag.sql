CREATE TABLE [dbo].[FlowInstanceTag]
(
[FlowInstanceID] [int] NOT NULL,
[Tag] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FlowInstanceTag] ADD CONSTRAINT [PK_FlowInstanceTag] PRIMARY KEY CLUSTERED  ([FlowInstanceID], [Tag]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FlowInstanceTag] WITH NOCHECK ADD CONSTRAINT [FK_FlowInstanceTag_FlowInstanceReportValues] FOREIGN KEY ([FlowInstanceID]) REFERENCES [dbo].[FlowInstanceReportValues] ([FlowInstanceID])
GO
ALTER TABLE [dbo].[FlowInstanceTag] NOCHECK CONSTRAINT [FK_FlowInstanceTag_FlowInstanceReportValues]
GO
