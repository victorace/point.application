CREATE TABLE [dbo].[FieldsMessageReceivedTemp]
(
[FieldReceivedTempID] [int] NOT NULL IDENTITY(1, 1),
[MessageReceivedTempID] [int] NULL,
[TimeStamp] [datetime] NOT NULL CONSTRAINT [DF_FieldReceivedTemp_TimeStamp] DEFAULT (getdate()),
[FieldName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FieldValue] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Status] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[FieldsMessageReceivedTemp] ADD CONSTRAINT [PK_FieldReceivedTemp] PRIMARY KEY CLUSTERED  ([FieldReceivedTempID]) ON [PRIMARY]
GO
