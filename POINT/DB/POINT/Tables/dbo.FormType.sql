CREATE TABLE [dbo].[FormType]
(
[FormTypeID] [int] NOT NULL,
[Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[URL] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TypeID] [int] NULL,
[Remove] [bit] NOT NULL CONSTRAINT [DF_FormType_Remove] DEFAULT ((0)),
[Order] [int] NULL,
[IsPrintable] [bit] NOT NULL CONSTRAINT [DF_FormType_IsPrintable] DEFAULT ((0)),
[IsVisibleInHistory] [bit] NOT NULL CONSTRAINT [DF_FormType_IsVisibleInHistory] DEFAULT ((0)),
[HasPageLocking] [bit] NOT NULL CONSTRAINT [DF_FormType_HasPageLocking] DEFAULT ((0)),
[PrintName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrintOnNewPage] [bit] NULL,
[HasDigitalSignature] [bit] NOT NULL CONSTRAINT [DF_FormType_IsSignable] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FormType] ADD CONSTRAINT [PK_FormType] PRIMARY KEY CLUSTERED  ([FormTypeID]) ON [PRIMARY]
GO
