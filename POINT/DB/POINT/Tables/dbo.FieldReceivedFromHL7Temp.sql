CREATE TABLE [dbo].[FieldReceivedFromHL7Temp]
(
[FieldReceivedFromHL7TempID] [int] NOT NULL IDENTITY(1, 1),
[TimeStamp] [datetime] NULL,
[MessageID] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GUID] [uniqueidentifier] NULL,
[OrganisationID] [int] NULL,
[SenderUserName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SendDate] [datetime] NULL,
[PatientNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MessageType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MedischeSituatieDatumOpname] [datetime] NULL,
[DatumEindeBehandelingMedischSpecialist] [datetime] NULL,
[Kamernummer] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BehandelaarNaam] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MedischeSituatieRedenOpname] [varchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[FieldReceivedFromHL7Temp_ins]

ON [dbo].[FieldReceivedFromHL7Temp]

FOR INSERT

AS

BEGIN 

      DECLARE 
       @TimeStamp DATETIME
      ,@MessageID VARCHAR(255)
      ,@GUID UNIQUEIDENTIFIER      
      ,@OrganisationID INT
      ,@SenderUserName VARCHAR(255)
      ,@SendDate DATETIME
      ,@PatientNumber VARCHAR(50)
      ,@MessageType VARCHAR(50)
      ,@MedischeSituatieDatumOpname DATETIME
      ,@DatumEindeBehandelingMedischSpecialist DATETIME
      ,@Kamernummer VARCHAR(255)
      ,@BehandelaarNaam VARCHAR(255)
      ,@MedischeSituatieRedenOpname VARCHAR(4000)
      SET NOCOUNT ON;
      SELECT  
       @TimeStamp         = [TimeStamp]
      ,@MessageID         = [MessageID]
      ,@GUID              = [GUID]     
      ,@OrganisationID  = [OrganisationID]
      ,@SenderUserName  = [SenderUserName]
      ,@SendDate        = [SendDate]
      ,@PatientNumber   = [PatientNumber]
      ,@MessageType     = [MessageType]
      ,@MedischeSituatieDatumOpname = [MedischeSituatieDatumOpname]
      ,@DatumEindeBehandelingMedischSpecialist = [DatumEindeBehandelingMedischSpecialist]
      ,@Kamernummer     = [Kamernummer]
      ,@BehandelaarNaam = [BehandelaarNaam]
      ,@MedischeSituatieRedenOpname = [MedischeSituatieRedenOpname]
      FROM INSERTED

      IF @MessageType = 'A19'
      BEGIN
            IF EXISTS (SELECT 1 FROM Organization 
            WHERE
                        Organization.OrganizationID = @OrganisationID AND
                        Organization.Inactive = 0)
            BEGIN
                        --EXECUTE WHEN ORGANIZATION EXISTS
            

                  DECLARE @MessageReceivedTempID AS INT
                  DECLARE @FieldName AS NVARCHAR(100)
                  DECLARE @FieldValue AS NVARCHAR(MAX)
                  
				  --  Point.Models.Enums.Source = 30 (HL7)
				  DECLARE @source AS INT = 30
				  
                  INSERT INTO MessageReceivedTemp
                        ([TimeStamp], OrganisationID, [GUID], SenderUserName, SendDate, PatientNumber, [Type], Source)
                        Values (@TimeStamp, @OrganisationID, @GUID, @SenderUserName, @SendDate, @PatientNumber, @MessageType, @source)
                        
                        SET @MessageReceivedTempID = SCOPE_IDENTITY();

                        --set @MedischeSituatieDatumOpname = CASE
                        --                                              WHEN @MedischeSituatieDatumOpname > '19000101' THEN @MedischeSituatieDatumOpname
                        --                                              ELSE ''
                        --                                              END

                  IF @MedischeSituatieDatumOpname > '19000101'   
                  BEGIN                                                                                                                                         
                        INSERT INTO FieldReceivedTemp
                             (MessageReceivedTempID, [Timestamp], FieldName, FieldValue)
                             Values (@MessageReceivedTempID, @Timestamp, 'MedischeSituatieDatumOpname', 
                                   (CONVERT(VARCHAR(8), @MedischeSituatieDatumOpname, 112) + REPLACE(CONVERT(varchar(8), @MedischeSituatieDatumOpname, 108 ),':','')))
                  END

                        --SET @DatumEindeBehandelingMedischSpecialist = CASE
                        --                                              WHEN @DatumEindeBehandelingMedischSpecialist > '19000101' THEN @DatumEindeBehandelingMedischSpecialist
                        --                                              ELSE ''
                        --                                              END
                             
                  
                  IF @DatumEindeBehandelingMedischSpecialist > '19000101'    
                  BEGIN                   
                        INSERT INTO FieldReceivedTemp
                             (MessageReceivedTempID, [Timestamp], FieldName, FieldValue)
                             Values (@MessageReceivedTempID, @Timestamp, 'DatumEindeBehandelingMedischSpecialist', 
                             (CONVERT(VARCHAR(8), @DatumEindeBehandelingMedischSpecialist, 112) + REPLACE(CONVERT(varchar(8), @DatumEindeBehandelingMedischSpecialist, 108 ),':','')))
                  END
                  
            IF @Kamernummer IS NOT NULL
            BEGIN
                        INSERT INTO FieldReceivedTemp
                               (MessageReceivedTempID, [Timestamp], FieldName, FieldValue)
                                Values (@MessageReceivedTempID, @Timestamp, 'Kamernummer', @Kamernummer)
            END

            IF @BehandelaarNaam IS NOT NULL
            BEGIN
                        INSERT INTO FieldReceivedTemp
                             (MessageReceivedTempID, [Timestamp], FieldName, FieldValue)
                             Values (@MessageReceivedTempID, @Timestamp, 'BehandelaarNaam', @BehandelaarNaam)
            END

            IF @MedischeSituatieRedenOpname IS NOT NULL 
            BEGIN                      
                        INSERT INTO FieldReceivedTemp
                             (MessageReceivedTempID, [Timestamp], FieldName, FieldValue)
                             Values (@MessageReceivedTempID, @Timestamp, 'MedischeSituatieRedenOpname', @MedischeSituatieRedenOpname)                      
                        END
            END
      END

END
GO
ALTER TABLE [dbo].[FieldReceivedFromHL7Temp] ADD CONSTRAINT [PK_FieldReceivedFromHL7TempI] PRIMARY KEY CLUSTERED  ([FieldReceivedFromHL7TempID]) ON [PRIMARY]
GO
