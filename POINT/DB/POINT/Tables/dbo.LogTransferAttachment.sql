CREATE TABLE [dbo].[LogTransferAttachment]
(
[LogTransferAttachmentID] [int] NOT NULL IDENTITY(1, 1),
[TransferID] [int] NULL,
[AttachmentID] [int] NULL,
[TimeStamp] [datetime] NULL,
[EmployeeID] [int] NULL,
[Action] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ScreenID] [int] NULL,
[Version] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[LogTransferAttachment] ADD CONSTRAINT [PK_LogTransferAttachment] PRIMARY KEY CLUSTERED  ([LogTransferAttachmentID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_LogTransferAttachment_AttachmentID] ON [dbo].[LogTransferAttachment] ([AttachmentID]) ON [PRIMARY]
GO
