CREATE TABLE [dbo].[FlowFieldAttribute]
(
[FlowFieldAttributeID] [int] NOT NULL,
[FlowFieldID] [int] NOT NULL,
[FormTypeID] [int] NOT NULL,
[IsActive] [bit] NOT NULL,
[OriginFormTypeID] [int] NULL,
[OriginFlowFieldID] [int] NULL,
[OriginCopyToAll] [bit] NOT NULL CONSTRAINT [DF_FlowFieldAttribute_OriginCopyToAll] DEFAULT ((0)),
[Required] [bit] NOT NULL CONSTRAINT [DF_FlowFieldAttribute_Required] DEFAULT ((0)),
[Visible] [bit] NOT NULL CONSTRAINT [DF_FlowFieldAttribute_Visible] DEFAULT ((1)),
[ReadOnly] [bit] NOT NULL CONSTRAINT [DF_FlowFieldAttribute_ReadOnly] DEFAULT ((0)),
[SignalOnChange] [bit] NOT NULL CONSTRAINT [DF_FlowFieldAttribute_SignalOnChange_1] DEFAULT ((0)),
[RequiredByFlowFieldID] [int] NULL,
[RequiredByFlowFieldValue] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[FlowFieldAttribute] ADD CONSTRAINT [PK_dbo.FlowFieldAttribute] PRIMARY KEY CLUSTERED  ([FlowFieldAttributeID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FlowFieldID] ON [dbo].[FlowFieldAttribute] ([FlowFieldID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FormTypeID] ON [dbo].[FlowFieldAttribute] ([FormTypeID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FlowFieldAttribute] ADD CONSTRAINT [FK_dbo.FlowFieldAttribute_dbo.FlowField_FlowFieldID] FOREIGN KEY ([FlowFieldID]) REFERENCES [dbo].[FlowField] ([FlowFieldID])
GO
ALTER TABLE [dbo].[FlowFieldAttribute] ADD CONSTRAINT [FK_dbo.FlowFieldAttribute_dbo.FormType_FormTypeID] FOREIGN KEY ([FormTypeID]) REFERENCES [dbo].[FormType] ([FormTypeID])
GO
ALTER TABLE [dbo].[FlowFieldAttribute] ADD CONSTRAINT [FK_FlowFieldAttribute_FlowField] FOREIGN KEY ([RequiredByFlowFieldID]) REFERENCES [dbo].[FlowField] ([FlowFieldID])
GO
