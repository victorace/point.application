CREATE TABLE [dbo].[HtmlText]
(
[RegionID] [int] NOT NULL,
[HtmlText] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedByEmployeeID] [int] NULL,
[CreatedDate] [datetime] NULL,
[ModifiedByEmployeeID] [int] NULL,
[ModifiedDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[HtmlText] ADD CONSTRAINT [PK_HtmlText] PRIMARY KEY CLUSTERED  ([RegionID]) ON [PRIMARY]
GO
