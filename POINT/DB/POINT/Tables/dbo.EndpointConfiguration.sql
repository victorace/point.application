CREATE TABLE [dbo].[EndpointConfiguration]
(
[EndpointConfigurationID] [int] NOT NULL IDENTITY(1, 1),
[OrganizationID] [int] NOT NULL,
[FormTypeID] [int] NOT NULL,
[Model] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Method] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Address] [nvarchar] (1024) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Username] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Password] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Label] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Inactive] [bit] NOT NULL CONSTRAINT [DF_EndpointConfiguration_Inactive] DEFAULT ((0)),
[SkipCertificateCheck] [bit] NOT NULL CONSTRAINT [DF_EndpointConfiguration_SkipCertificateCheck] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EndpointConfiguration] ADD CONSTRAINT [PK_EndpointConfiguration] PRIMARY KEY CLUSTERED  ([EndpointConfigurationID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EndpointConfiguration] ADD CONSTRAINT [FK_EndpointConfiguration_FormType] FOREIGN KEY ([FormTypeID]) REFERENCES [dbo].[FormType] ([FormTypeID])
GO
