CREATE TABLE [dbo].[RADIUSServerSetting]
(
[RADIUSServerSettingID] [int] NOT NULL IDENTITY(1, 1),
[OrganizationID] [int] NOT NULL,
[ServerIPAddress1] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FallbackServerIPAddress2] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_RADIUSServerSetting_Primair] DEFAULT ((0)),
[SharedSecret] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Retries] [smallint] NULL,
[TimeOut] [smallint] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RADIUSServerSetting] ADD CONSTRAINT [PK_RADIUSServerSetting] PRIMARY KEY CLUSTERED  ([RADIUSServerSettingID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RADIUSServerSetting] ADD CONSTRAINT [FK_RADIUSServerSetting_Organization] FOREIGN KEY ([OrganizationID]) REFERENCES [dbo].[Organization] ([OrganizationID])
GO
