CREATE TABLE [dbo].[PreferredLocation]
(
[LocationID] [int] NOT NULL,
[PreferredLocationID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PreferredLocation] ADD CONSTRAINT [PK_PreferredLocation_1] PRIMARY KEY CLUSTERED  ([LocationID], [PreferredLocationID]) ON [PRIMARY]
GO
