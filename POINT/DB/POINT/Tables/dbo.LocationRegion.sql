CREATE TABLE [dbo].[LocationRegion]
(
[LocationID] [int] NOT NULL,
[RegionID] [int] NOT NULL,
[ModifiedTimeStamp] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[LocationRegion] ADD CONSTRAINT [PK_LocationRegion] PRIMARY KEY CLUSTERED  ([LocationID], [RegionID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[LocationRegion] ADD CONSTRAINT [FK_LocationRegion_Location] FOREIGN KEY ([LocationID]) REFERENCES [dbo].[Location] ([LocationID])
GO
ALTER TABLE [dbo].[LocationRegion] ADD CONSTRAINT [FK_LocationRegion_Region] FOREIGN KEY ([RegionID]) REFERENCES [dbo].[Region] ([RegionID])
GO
