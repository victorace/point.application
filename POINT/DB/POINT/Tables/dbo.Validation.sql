CREATE TABLE [dbo].[Validation]
(
[ValidationID] [int] NOT NULL IDENTITY(1, 1),
[Method] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FieldAttributes] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WarningTriggers] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ErrorTriggers] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Messages] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Validation] ADD CONSTRAINT [PK_Validation] PRIMARY KEY CLUSTERED  ([ValidationID]) ON [PRIMARY]
GO
