CREATE TABLE [dbo].[GPSGemeente]
(
[GPSGemeenteID] [int] NOT NULL IDENTITY(1, 1),
[Naam] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GPSProvincieID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[GPSGemeente] ADD CONSTRAINT [PK__Gemeente__3214EC277A6E4BC2] PRIMARY KEY CLUSTERED  ([GPSGemeenteID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[GPSGemeente] ADD CONSTRAINT [FK_GPSGemeente_GPSProvincie] FOREIGN KEY ([GPSProvincieID]) REFERENCES [dbo].[GPSProvincie] ([GPSProvincieID])
GO
