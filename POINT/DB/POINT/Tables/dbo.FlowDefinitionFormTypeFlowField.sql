CREATE TABLE [dbo].[FlowDefinitionFormTypeFlowField]
(
[FlowDefinitionFormTypeFlowFieldID] [int] NOT NULL IDENTITY(1, 1),
[FlowDefinitionID] [int] NOT NULL,
[FormTypeID] [int] NOT NULL,
[FlowFieldID] [int] NOT NULL,
[TakeOver] [bit] NOT NULL CONSTRAINT [DF_FlowDefinitionFormTypeFlowField_TakeOver] DEFAULT ((1))
) ON [PRIMARY]
GO
