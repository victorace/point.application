CREATE TABLE [dbo].[WebRequestLog]
(
[WebRequestLogID] [int] NOT NULL IDENTITY(1, 1),
[TimeStamp] [datetime] NOT NULL,
[Sent] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Received] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ExceptionMessage] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[WebRequestLog] ADD CONSTRAINT [PK_WebRequests_1] PRIMARY KEY CLUSTERED  ([WebRequestLogID]) ON [PRIMARY]
GO
