CREATE TABLE [dbo].[DepartmentType]
(
[DepartmentTypeID] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DepartmentType] ADD CONSTRAINT [PK_DepartmentType] PRIMARY KEY CLUSTERED  ([DepartmentTypeID]) ON [PRIMARY]
GO
