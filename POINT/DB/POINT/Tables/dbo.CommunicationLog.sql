CREATE TABLE [dbo].[CommunicationLog]
(
[CommunicationLogID] [int] NOT NULL IDENTITY(1, 1),
[FromAddress] [varchar] (1024) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FromName] [varchar] (1024) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ToAddress] [varchar] (1024) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ToName] [varchar] (1024) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CCAddress] [varchar] (1024) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BCCAddress] [varchar] (1024) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Subject] [varchar] (1024) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Content] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Timestamp] [datetime] NULL,
[Description] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransferID] [int] NULL,
[ClientID] [int] NULL,
[EmployeeID] [int] NULL,
[OrganizationID] [int] NULL,
[MailType] [int] NULL,
[ErrorMessage] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[CommunicationLog] ADD CONSTRAINT [PK_CommunicationLog] PRIMARY KEY CLUSTERED  ([CommunicationLogID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_CommunicationLog_Description] ON [dbo].[CommunicationLog] ([Description]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_CommunicationLog_TransferIDMailType] ON [dbo].[CommunicationLog] ([TransferID], [MailType]) ON [PRIMARY]
GO
