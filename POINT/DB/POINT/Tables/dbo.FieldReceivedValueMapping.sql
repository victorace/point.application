CREATE TABLE [dbo].[FieldReceivedValueMapping]
(
[FieldReceivedValueMappingID] [int] NOT NULL IDENTITY(1, 1),
[FieldName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SourceValue] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TargetValue] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[FieldReceivedValueMapping] ADD CONSTRAINT [PK_FieldReceivedValueMappings] PRIMARY KEY CLUSTERED  ([FieldReceivedValueMappingID]) ON [PRIMARY]
GO
