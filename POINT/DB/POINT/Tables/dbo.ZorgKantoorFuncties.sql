CREATE TABLE [dbo].[ZorgKantoorFuncties]
(
[ZorgKantoorMessageID] [int] NOT NULL IDENTITY(1, 1),
[MessageID] [int] NOT NULL,
[Code] [int] NULL,
[Startdate] [datetime] NULL,
[Enddate] [datetime] NULL,
[Class] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ZZPNumber] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ZZPCodeIndication] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateGiven] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ZorgKantoorFuncties] ADD CONSTRAINT [PK_ZorgKantoorFuncties] PRIMARY KEY CLUSTERED  ([ZorgKantoorMessageID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ZorgKantoorFuncties] ADD CONSTRAINT [FK_ZorgKantoorFuncties_ZorgkantoorMessage] FOREIGN KEY ([MessageID]) REFERENCES [dbo].[ZorgkantoorMessage] ([MessageID])
GO
