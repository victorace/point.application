CREATE TABLE [dbo].[TransferMemoChanges]
(
[TransferMemoChangesID] [int] NOT NULL IDENTITY(1, 1),
[TransferMemoID] [int] NOT NULL,
[OriginalContent] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmployeeID] [int] NULL,
[EmployeeName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateTimeChanged] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[TransferMemoChanges] ADD CONSTRAINT [PK_TransferMemoChanges] PRIMARY KEY CLUSTERED  ([TransferMemoChangesID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_TransferMemoChanges_TransferMemoID] ON [dbo].[TransferMemoChanges] ([TransferMemoID]) ON [PRIMARY]
GO
