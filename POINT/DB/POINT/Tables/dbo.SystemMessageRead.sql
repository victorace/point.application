CREATE TABLE [dbo].[SystemMessageRead]
(
[SystemMessageReadID] [int] NOT NULL IDENTITY(1, 1),
[EmployeeID] [int] NOT NULL,
[SystemMessageID] [int] NOT NULL,
[DateRead] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SystemMessageRead] ADD CONSTRAINT [PK_SystemMessageRead] PRIMARY KEY CLUSTERED  ([SystemMessageReadID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_SystemMessageRead_EmployeeID] ON [dbo].[SystemMessageRead] ([EmployeeID]) ON [PRIMARY]
GO
