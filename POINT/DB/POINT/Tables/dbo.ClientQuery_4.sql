CREATE TABLE [dbo].[ClientQuery_4]
(
[MESSAGE_ID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__ClientQue__MESSA__6CE315C2] DEFAULT (''),
[STATUS] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__ClientQue__STATU__6DD739FB] DEFAULT ('W')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ClientQuery_4] ADD CONSTRAINT [PK__ClientQuery_4__6BEEF189] PRIMARY KEY CLUSTERED  ([MESSAGE_ID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
