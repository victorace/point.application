CREATE TABLE [dbo].[MutOrganization]
(
[MutOrganizationID] [int] NOT NULL IDENTITY(1, 1),
[LogOrganizationID] [int] NULL,
[OrganizationID] [int] NOT NULL,
[Name] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OrganizationTypeID] [int] NOT NULL,
[NedapCryptoCertificateID] [int] NULL,
[Inactive] [bit] NULL,
[HL7Interface] [bit] NULL,
[WSInterface] [bit] NULL,
[InterfaceNumber] [int] NULL,
[RegionID] [int] NULL,
[ChangePasswordPeriod] [int] NULL,
[ShortConnectionID] [bit] NULL,
[AGB] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[URL] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ConnectionZorgkantoor] [bit] NULL,
[HL7InterfaceExtraFields] [bit] NULL,
[OrganizationSubType] [int] NULL,
[HL7ConnectionDifferentMenu] [bit] NULL,
[HL7ConnectionCallFromPoint] [bit] NULL,
[UsesRadius] [bit] NOT NULL,
[UsesPatientPortal] [bit] NULL,
[ShowUnloadMessage] [bit] NULL,
[ButtonRedirectOrganization] [bit] NULL,
[ButtonRedirectOrganizationName] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[URLRedirectOrganization] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Image] [bit] NULL,
[ImageName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SSO] [bit] NULL,
[SSOKey] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TimeStamp] [datetime] NULL,
[EmployeeID] [int] NULL,
[ScreenID] [int] NULL,
[MessageToGPByClose] [bit] NULL,
[MessageToGPByDefinitive] [bit] NULL,
[ZorgmailUsername] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ZorgmailPassword] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WSInterfaceExtraFields] [bit] NULL,
[HashPrefix] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PageLockTimeout] [int] NULL,
[CreateUserViaSSO] [bit] NULL,
[NVZReport] [bit] NULL,
[SSOHashType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UsesPalliativeCareInVO] [bit] NOT NULL CONSTRAINT [DF_MutOrganization_UsesPalliativeCareInVO] DEFAULT ((0)),
[IsAdminOrganization] [bit] NOT NULL CONSTRAINT [DF_MutOrganization_IsAdminOrganization] DEFAULT ((0)),
[HasCVA3MonthRegistration] [bit] NOT NULL CONSTRAINT [DF_MutOrganization_HasCVA3MonthRegistration] DEFAULT ((1)),
[MFARequired] [bit] NOT NULL CONSTRAINT [DF_MutOrganization_TwoFactorAuthentication] DEFAULT ((0)),
[UseAnonymousClient] [bit] NOT NULL CONSTRAINT [DF_MutOrganization_UseAnonymousClient] DEFAULT ((0)),
[UseDigitalSignature] [bit] NOT NULL CONSTRAINT [DF_MutOrganization_HasDigitalSignature] DEFAULT ((0)),
[NedapMedewerkerNummer] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UseTasks] [bit] NOT NULL CONSTRAINT [DF_MutOrganization_UseTasks] DEFAULT ((0)),
[OrganizationLogoID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MutOrganization] ADD CONSTRAINT [PK_MutOrganization] PRIMARY KEY CLUSTERED  ([MutOrganizationID]) ON [PRIMARY]
GO
