CREATE TABLE [dbo].[FlowField]
(
[FlowFieldID] [int] NOT NULL,
[Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Label] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Type] [int] NOT NULL,
[Format] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Regexp] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaxLength] [int] NULL,
[UseInReadModel] [bit] NOT NULL CONSTRAINT [DF_FlowField_UseInReadModel] DEFAULT ((0)),
[FlowFieldDataSourceID] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[FlowField] ADD CONSTRAINT [PK_dbo.FlowField] PRIMARY KEY CLUSTERED  ([FlowFieldID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FlowField] ADD CONSTRAINT [IX_FlowField_Name] UNIQUE NONCLUSTERED  ([Name]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FlowField_UseInReadModel] ON [dbo].[FlowField] ([UseInReadModel]) INCLUDE ([Description], [FlowFieldDataSourceID], [FlowFieldID], [Format], [Label], [MaxLength], [Name], [Regexp], [Type]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FlowField] ADD CONSTRAINT [FK_FlowField_FlowFieldDataSource] FOREIGN KEY ([FlowFieldDataSourceID]) REFERENCES [dbo].[FlowFieldDataSource] ([FlowFieldDataSourceID])
GO
