CREATE TABLE [dbo].[RazorTemplate]
(
[RazorTemplateID] [int] NOT NULL IDENTITY(1, 1),
[TemplateName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TemplateCode] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Contents] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CommunicationLogType] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[RazorTemplate] ADD CONSTRAINT [PK_RazorTemplate] PRIMARY KEY CLUSTERED  ([RazorTemplateID]) ON [PRIMARY]
GO
