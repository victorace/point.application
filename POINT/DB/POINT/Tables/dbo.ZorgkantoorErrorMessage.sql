CREATE TABLE [dbo].[ZorgkantoorErrorMessage]
(
[MessageID] [int] NOT NULL IDENTITY(1, 1),
[TransferID] [int] NULL,
[AGBCode] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AGBCodeZorgKantoor] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Agreement] [bit] NULL,
[SocialSecurityNumber] [varchar] (9) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BirthDate] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClientNumber] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IndicationDecisionNumber] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IndicationGivenNumber] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateIndication] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateReceived] [datetime] NOT NULL,
[message] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[ZorgkantoorErrorMessage] ADD CONSTRAINT [PK_ZorgkantoorErrorMessage] PRIMARY KEY CLUSTERED  ([MessageID]) ON [PRIMARY]
GO
