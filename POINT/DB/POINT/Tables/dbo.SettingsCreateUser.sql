CREATE TABLE [dbo].[SettingsCreateUser]
(
[SettingsCreateUserID] [int] NOT NULL IDENTITY(1, 1),
[OrganizationID] [int] NULL,
[LocationName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DepartmentName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Gender] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PhoneNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Password] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AllOrganizationDepartmentsLinked] [bit] NOT NULL CONSTRAINT [DF_SettingsCreateUser_AllOrganizationDepartmentsLinked] DEFAULT ((0)),
[DefaultLocation] [bit] NOT NULL CONSTRAINT [DF_SettingsCreateUser_DefaultLocation] DEFAULT ((0)),
[RoleName] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UsernamePrefix] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChangePasswordByEmployee] [bit] NOT NULL CONSTRAINT [DF_SettingsCreateUser_ChangePasswordByEmployee] DEFAULT ((0)),
[EmailAddress] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Position] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MSVTEdit] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SettingsCreateUser] ADD CONSTRAINT [PK_CreateUserSettings] PRIMARY KEY CLUSTERED  ([SettingsCreateUserID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SettingsCreateUser] ADD CONSTRAINT [FK_SettingsCreateUser_Organization] FOREIGN KEY ([OrganizationID]) REFERENCES [dbo].[Organization] ([OrganizationID])
GO
