CREATE TABLE [dbo].[AutoCreateSet]
(
[AutoCreateSetID] [int] NOT NULL IDENTITY(1, 1),
[LocationID] [int] NOT NULL,
[AutoCreateCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Name] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IsDefaultForOrganization] [bit] NOT NULL,
[UsernamePrefix] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultGender] [int] NOT NULL,
[DefaultPhoneNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultEmailAddress] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultPassword] [nvarchar] (512) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultDossierLevel] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChangePasswordByEmployee] [bit] NOT NULL CONSTRAINT [DF_AutoCreateSet_ChangePasswordByEmployee] DEFAULT ((0)),
[MSVTEdit] [bit] NOT NULL CONSTRAINT [DF_AutoCreateSet_MSVTEdit] DEFAULT ((0)),
[ModifiedTimeStamp] [datetime] NULL,
[ReadOnlySender] [bit] NOT NULL CONSTRAINT [DF_AutoCreateSet_ReadOnlySender] DEFAULT ((0)),
[ReadOnlyReciever] [bit] NOT NULL CONSTRAINT [DF_AutoCreateSet_ReadOnlyReciever] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AutoCreateSet] ADD CONSTRAINT [PK_AutoCreateSet] PRIMARY KEY CLUSTERED  ([AutoCreateSetID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AutoCreateSet] ADD CONSTRAINT [FK_AutoCreateSet_Location] FOREIGN KEY ([LocationID]) REFERENCES [dbo].[Location] ([LocationID])
GO
