CREATE TABLE [dbo].[ZorgAdviesItemGroup]
(
[ZorgAdviesItemGroupID] [int] NOT NULL IDENTITY(1, 1),
[OrderNumber] [int] NOT NULL,
[GroupName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ParentZorgAdviesItemGroupID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ZorgAdviesItemGroup] ADD CONSTRAINT [PK_ZorgAdviesItemGroup] PRIMARY KEY CLUSTERED  ([ZorgAdviesItemGroupID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_ZorgAdviesItemGroup] ON [dbo].[ZorgAdviesItemGroup] ([OrderNumber]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ZorgAdviesItemGroup] ADD CONSTRAINT [FK_ZorgPlanItemGroup_ZorgPlanItemGroup] FOREIGN KEY ([ParentZorgAdviesItemGroupID]) REFERENCES [dbo].[ZorgAdviesItemGroup] ([ZorgAdviesItemGroupID])
GO
