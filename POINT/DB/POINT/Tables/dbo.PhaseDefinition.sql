CREATE TABLE [dbo].[PhaseDefinition]
(
[PhaseDefinitionID] [int] NOT NULL IDENTITY(1, 1),
[FlowDefinitionID] [int] NOT NULL,
[Phase] [decimal] (4, 1) NOT NULL,
[PhaseName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OrganizationTypeID] [int] NOT NULL,
[FormTypeID] [int] NULL,
[BeginFlow] [bit] NOT NULL,
[EndFlow] [bit] NOT NULL,
[Description] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Executor] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Subcategory] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CodeGroup] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HasPrintButton] [bit] NULL,
[Maincategory] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OrderNumber] [int] NULL,
[ReadOnlyOnDone] [bit] NULL,
[RedirectToPhaseDefinitionIDOnNext] [int] NULL,
[AccessGroup] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ShowReadOnlyAndEmpty] [bit] NOT NULL CONSTRAINT [DF_PhaseDefinition_ShowReadOnlyAndEmpty] DEFAULT ((0)),
[DashboardMenuItemType] [int] NULL CONSTRAINT [DF_PhaseDefinition_SelectableForm] DEFAULT ((0)),
[CanResend] [bit] NOT NULL CONSTRAINT [DF_PhaseDefinition_CanResend] DEFAULT ((0)),
[MenuItemType] [int] NULL CONSTRAINT [DF_PhaseDefinition_MenuFormType] DEFAULT ((0)),
[FlowHandling] [int] NULL CONSTRAINT [DF_PhaseDefinition_FlowHandling] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PhaseDefinition] ADD CONSTRAINT [PK_dbo.PhaseDefinition] PRIMARY KEY CLUSTERED  ([PhaseDefinitionID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_PhaseDefinition_BeginFlow] ON [dbo].[PhaseDefinition] ([BeginFlow]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_PhaseDefinition_EndFlow] ON [dbo].[PhaseDefinition] ([EndFlow]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FlowDefinitionID] ON [dbo].[PhaseDefinition] ([FlowDefinitionID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_PhaseDefinition_FormTypeID] ON [dbo].[PhaseDefinition] ([FormTypeID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_OrganizationTypeID] ON [dbo].[PhaseDefinition] ([OrganizationTypeID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PhaseDefinition] ADD CONSTRAINT [FK_dbo.PhaseDefinition_dbo.FlowDefinition_FlowDefinitionID] FOREIGN KEY ([FlowDefinitionID]) REFERENCES [dbo].[FlowDefinition] ([FlowDefinitionID])
GO
ALTER TABLE [dbo].[PhaseDefinition] ADD CONSTRAINT [FK_dbo.PhaseDefinition_dbo.OrganizationType_OrganizationTypeID] FOREIGN KEY ([OrganizationTypeID]) REFERENCES [dbo].[OrganizationType] ([OrganizationTypeID])
GO
