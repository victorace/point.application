CREATE TABLE [dbo].[OrganizationCSVTemplateColumn]
(
[OrganizationCSVTemplateID] [int] NOT NULL,
[ColumnSource] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Position] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[OrganizationCSVTemplateColumn] ADD CONSTRAINT [PK_OrganizationCSVTemplateColumns] PRIMARY KEY CLUSTERED  ([OrganizationCSVTemplateID], [ColumnSource]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[OrganizationCSVTemplateColumn] ADD CONSTRAINT [FK_OrganizationCSVTemplateColumn_OrganizationCSVTemplate] FOREIGN KEY ([OrganizationCSVTemplateID]) REFERENCES [dbo].[OrganizationCSVTemplate] ([OrganizationCSVTemplateID])
GO
