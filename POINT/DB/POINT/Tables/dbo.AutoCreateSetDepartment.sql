CREATE TABLE [dbo].[AutoCreateSetDepartment]
(
[AutoCreateSetID] [int] NOT NULL,
[DepartmentID] [int] NOT NULL,
[ModifiedTimeStamp] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AutoCreateSetDepartment] ADD CONSTRAINT [PK_AutoCreateSetDepartment] PRIMARY KEY CLUSTERED  ([AutoCreateSetID], [DepartmentID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AutoCreateSetDepartment] ADD CONSTRAINT [FK_AutoCreateSetDepartment_AutoCreateSet] FOREIGN KEY ([AutoCreateSetID]) REFERENCES [dbo].[AutoCreateSet] ([AutoCreateSetID])
GO
ALTER TABLE [dbo].[AutoCreateSetDepartment] ADD CONSTRAINT [FK_AutoCreateSetDepartment_Department] FOREIGN KEY ([DepartmentID]) REFERENCES [dbo].[Department] ([DepartmentID])
GO
