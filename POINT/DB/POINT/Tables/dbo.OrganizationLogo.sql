CREATE TABLE [dbo].[OrganizationLogo]
(
[OrganizationLogoID] [int] NOT NULL IDENTITY(1, 1),
[ContentLength] [int] NOT NULL,
[ContentType] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FileName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FileData] [image] NOT NULL,
[InActive] [bit] NOT NULL,
[ModifiedTimeStamp] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[OrganizationLogo] ADD CONSTRAINT [PK_OrganizationLogo] PRIMARY KEY CLUSTERED  ([OrganizationLogoID]) ON [PRIMARY]
GO
