CREATE TABLE [dbo].[ClientQRY_8]
(
[MESSAGE_ID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__ClientQRY_8__MESSAGE] DEFAULT (''),
[PARENT_ID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__ClientQRY_8__PARENT] DEFAULT (''),
[PatientID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MsgID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MsgID_1] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TimeStamp] [datetime] NULL CONSTRAINT [DF_ClientQRY_8_TimeStamp] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ClientQRY_8] ADD CONSTRAINT [PK__ClientQRY_8] PRIMARY KEY CLUSTERED  ([MESSAGE_ID], [PARENT_ID], [PatientID]) ON [PRIMARY]
GO
