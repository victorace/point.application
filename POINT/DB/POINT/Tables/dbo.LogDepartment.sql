CREATE TABLE [dbo].[LogDepartment]
(
[LogDepartmentID] [int] NOT NULL IDENTITY(1, 1),
[DepartmentID] [int] NULL,
[TimeStamp] [datetime] NULL,
[EmployeeID] [int] NULL,
[Action] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ScreenID] [int] NULL,
[Version] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[LogDepartment] ADD CONSTRAINT [PK_LogDepartment] PRIMARY KEY CLUSTERED  ([LogDepartmentID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_LogDepartment_DepartmentID] ON [dbo].[LogDepartment] ([DepartmentID]) ON [PRIMARY]
GO
