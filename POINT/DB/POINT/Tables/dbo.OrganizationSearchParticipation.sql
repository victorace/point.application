CREATE TABLE [dbo].[OrganizationSearchParticipation]
(
[OrganizationSearchParticipationID] [int] NOT NULL IDENTITY(1, 1),
[OrganizationSearchID] [int] NOT NULL,
[FlowDefinitionID] [int] NOT NULL,
[FlowDirection] [int] NOT NULL,
[Participation] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[OrganizationSearchParticipation] ADD CONSTRAINT [PK_OrganizationSearchParticipation] PRIMARY KEY CLUSTERED  ([OrganizationSearchParticipationID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_OrganizationSearchParticipation_FlowDefinitionID] ON [dbo].[OrganizationSearchParticipation] ([FlowDefinitionID], [FlowDirection]) INCLUDE ([OrganizationSearchID], [OrganizationSearchParticipationID], [Participation]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_OrganizationSearchParticipation_OrganizationSearchID] ON [dbo].[OrganizationSearchParticipation] ([OrganizationSearchID], [FlowDirection]) INCLUDE ([FlowDefinitionID], [OrganizationSearchParticipationID], [Participation]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[OrganizationSearchParticipation] ADD CONSTRAINT [FK_OrganizationSearchParticipation_FlowDefinition] FOREIGN KEY ([FlowDefinitionID]) REFERENCES [dbo].[FlowDefinition] ([FlowDefinitionID])
GO
ALTER TABLE [dbo].[OrganizationSearchParticipation] ADD CONSTRAINT [FK_OrganizationSearchParticipation_OrganizationSearch] FOREIGN KEY ([OrganizationSearchID]) REFERENCES [dbo].[OrganizationSearch] ([OrganizationSearchID])
GO
