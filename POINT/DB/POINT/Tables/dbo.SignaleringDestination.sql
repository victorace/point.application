CREATE TABLE [dbo].[SignaleringDestination]
(
[SignaleringDestinationID] [int] NOT NULL IDENTITY(1, 1),
[SignaleringID] [int] NOT NULL,
[DepartmentID] [int] NULL,
[LocationID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SignaleringDestination] ADD CONSTRAINT [PK_SignaleringDestination] PRIMARY KEY CLUSTERED  ([SignaleringDestinationID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_SignaleringDestination_LocationDepartment] ON [dbo].[SignaleringDestination] ([DepartmentID], [LocationID]) INCLUDE ([SignaleringDestinationID], [SignaleringID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_SignaleringDestination_SignaleringID_Includes] ON [dbo].[SignaleringDestination] ([SignaleringID]) INCLUDE ([DepartmentID], [LocationID], [SignaleringDestinationID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SignaleringDestination] ADD CONSTRAINT [FK_SignaleringDestination_Department] FOREIGN KEY ([DepartmentID]) REFERENCES [dbo].[Department] ([DepartmentID])
GO
ALTER TABLE [dbo].[SignaleringDestination] ADD CONSTRAINT [FK_SignaleringDestination_Location] FOREIGN KEY ([LocationID]) REFERENCES [dbo].[Location] ([LocationID])
GO
ALTER TABLE [dbo].[SignaleringDestination] ADD CONSTRAINT [FK_SignaleringDestination_Signalering] FOREIGN KEY ([SignaleringID]) REFERENCES [dbo].[Signalering] ([SignaleringID])
GO
