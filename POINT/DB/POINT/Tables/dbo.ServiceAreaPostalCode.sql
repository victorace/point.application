CREATE TABLE [dbo].[ServiceAreaPostalCode]
(
[ServiceAreaPostalCodeID] [int] NOT NULL IDENTITY(1, 1),
[DepartmentID] [int] NULL,
[ServiceAreaID] [int] NULL,
[PostalCodeID] [int] NULL,
[ModifiedTimeStamp] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ServiceAreaPostalCode] ADD CONSTRAINT [PK_ServiceAreaPostalCode] PRIMARY KEY CLUSTERED  ([ServiceAreaPostalCodeID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ServiceAreaPostalCode] WITH NOCHECK ADD CONSTRAINT [FK_ServiceAreaPostalCode_Department] FOREIGN KEY ([DepartmentID]) REFERENCES [dbo].[Department] ([DepartmentID])
GO
ALTER TABLE [dbo].[ServiceAreaPostalCode] ADD CONSTRAINT [FK_ServiceAreaPostalCode_PostalCode] FOREIGN KEY ([PostalCodeID]) REFERENCES [dbo].[PostalCode] ([PostalCodeID])
GO
ALTER TABLE [dbo].[ServiceAreaPostalCode] ADD CONSTRAINT [FK_ServiceAreaPostalCode_ServiceArea] FOREIGN KEY ([ServiceAreaID]) REFERENCES [dbo].[ServiceArea] ([ServiceAreaID])
GO
ALTER TABLE [dbo].[ServiceAreaPostalCode] NOCHECK CONSTRAINT [FK_ServiceAreaPostalCode_Department]
GO
