CREATE TABLE [dbo].[PageLock]
(
[PageLockID] [int] NOT NULL IDENTITY(1, 1),
[TransferID] [int] NOT NULL,
[EmployeeID] [int] NOT NULL,
[URL] [varchar] (1024) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SessionID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TimeStamp] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PageLock] ADD CONSTRAINT [PK_PageLock] PRIMARY KEY CLUSTERED  ([PageLockID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_PageLock_SessionID] ON [dbo].[PageLock] ([SessionID]) INCLUDE ([EmployeeID], [PageLockID], [TimeStamp], [TransferID], [URL]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PageLock] ADD CONSTRAINT [FK_PageLock_Employee] FOREIGN KEY ([EmployeeID]) REFERENCES [dbo].[Employee] ([EmployeeID])
GO
