CREATE TABLE [dbo].[OrganizationSearchRegion]
(
[OrganizationSearchRegionID] [int] NOT NULL IDENTITY(1, 1),
[OrganizationSearchID] [int] NOT NULL,
[RegionID] [int] NOT NULL,
[RegionName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[OrganizationSearchRegion] ADD CONSTRAINT [PK_OrganizationSearchRegion] PRIMARY KEY CLUSTERED  ([OrganizationSearchRegionID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_OrganizationSearchRegion_IDRegionIDRegionName] ON [dbo].[OrganizationSearchRegion] ([OrganizationSearchID]) INCLUDE ([OrganizationSearchRegionID], [RegionID], [RegionName]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_OrganizationSearchRegion_RegionID] ON [dbo].[OrganizationSearchRegion] ([RegionID]) INCLUDE ([OrganizationSearchID], [OrganizationSearchRegionID], [RegionName]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[OrganizationSearchRegion] ADD CONSTRAINT [FK_OrganizationSearchRegion_OrganizationSearch] FOREIGN KEY ([OrganizationSearchID]) REFERENCES [dbo].[OrganizationSearch] ([OrganizationSearchID])
GO
