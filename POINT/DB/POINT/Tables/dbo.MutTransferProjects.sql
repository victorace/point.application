CREATE TABLE [dbo].[MutTransferProjects]
(
[MutTransferProjectsID] [int] NOT NULL IDENTITY(1, 1),
[LogTransferProjectsID] [int] NOT NULL,
[TransferID] [int] NOT NULL,
[ProjectID] [int] NOT NULL,
[TimeStamp] [datetime] NULL,
[EmployeeID] [int] NULL,
[ScreenID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MutTransferProjects] ADD CONSTRAINT [PK_MutTransferProjects] PRIMARY KEY CLUSTERED  ([MutTransferProjectsID]) ON [PRIMARY]
GO
