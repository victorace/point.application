CREATE TABLE [dbo].[ZorgKantoorErrorFuncties]
(
[ZorgKantoorErrorMessageID] [int] NOT NULL IDENTITY(1, 1),
[MessageID] [int] NOT NULL,
[Code] [int] NULL,
[Startdate] [char] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Enddate] [char] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Class] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ZZPNumber] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ZZPCodeIndication] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ZorgKantoorErrorFuncties] ADD CONSTRAINT [PK_ZorgKantoorErrorFuncties] PRIMARY KEY CLUSTERED  ([ZorgKantoorErrorMessageID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ZorgKantoorErrorFuncties] ADD CONSTRAINT [FK_ZorgKantoorErrorFuncties_ZorgkantoorErrorMessage] FOREIGN KEY ([MessageID]) REFERENCES [dbo].[ZorgkantoorErrorMessage] ([MessageID])
GO
