CREATE TABLE [dbo].[ClientQuery_3]
(
[MESSAGE_ID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__ClientQue__MESSA__6E0C4425] DEFAULT (''),
[STATUS] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__ClientQue__STATU__6F00685E] DEFAULT ('W')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ClientQuery_3] ADD CONSTRAINT [PK__ClientQuery_3__6D181FEC] PRIMARY KEY CLUSTERED  ([MESSAGE_ID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
