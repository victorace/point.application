CREATE TABLE [dbo].[LogTransferIndexedFunctions]
(
[LogTransferIndexedFunctionsID] [int] NOT NULL IDENTITY(1, 1),
[TransferID] [int] NOT NULL,
[IndexedFunctionID] [int] NOT NULL,
[ClientID] [int] NULL,
[TimeStamp] [datetime] NULL,
[EmployeeID] [int] NULL,
[Action] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ScreenID] [int] NULL,
[Version] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[LogTransferIndexedFunctions] ADD CONSTRAINT [PK_LogTransferIndexedFunctions] PRIMARY KEY CLUSTERED  ([LogTransferIndexedFunctionsID]) ON [PRIMARY]
GO
