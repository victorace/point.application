CREATE TABLE [dbo].[AidProduct]
(
[AidProductID] [int] NOT NULL,
[AidProductGroupID] [int] NOT NULL,
[SupplierID] [int] NOT NULL,
[Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ImageData] [image] NULL,
[Price] [decimal] (4, 2) NOT NULL CONSTRAINT [DF_AidProduct_Price] DEFAULT ((0)),
[PricePerDay] [decimal] (4, 2) NOT NULL CONSTRAINT [DF_AidProduct_PricePerDay] DEFAULT ((0)),
[TransportCosts] [decimal] (4, 2) NOT NULL CONSTRAINT [DF_AidProduct_TransportCosts] DEFAULT ((0)),
[AuthorizationForm] [bit] NOT NULL CONSTRAINT [DF_AidProduct_AuthorizationForm] DEFAULT ((0)),
[Code] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InsurersUZOVIList] [varchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[AidProduct] ADD CONSTRAINT [PK_AidSupplies] PRIMARY KEY CLUSTERED  ([AidProductID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AidProduct] ADD CONSTRAINT [FK_AidProduct_AidProductGroup] FOREIGN KEY ([AidProductGroupID]) REFERENCES [dbo].[AidProductGroup] ([AidProductGroupID])
GO
ALTER TABLE [dbo].[AidProduct] ADD CONSTRAINT [FK_AidProduct_Supplier] FOREIGN KEY ([SupplierID]) REFERENCES [dbo].[Supplier] ([SupplierID])
GO
