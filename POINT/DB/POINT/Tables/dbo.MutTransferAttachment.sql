CREATE TABLE [dbo].[MutTransferAttachment]
(
[MutTransferAttachmentID] [int] NOT NULL IDENTITY(1, 1),
[LogTransferAttachmentID] [int] NOT NULL,
[AttachmentID] [int] NOT NULL,
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FormSetVersionID] [int] NULL,
[EmployeeID] [int] NULL,
[TransferID] [int] NULL,
[Deleted] [bit] NULL,
[TimeStamp] [datetime] NULL,
[ScreenID] [int] NULL,
[GenericFileID] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MutTransferAttachment] ADD CONSTRAINT [PK_MutTransferAttachment] PRIMARY KEY CLUSTERED  ([MutTransferAttachmentID]) ON [PRIMARY]
GO
