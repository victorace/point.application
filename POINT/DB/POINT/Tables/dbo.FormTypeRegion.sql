CREATE TABLE [dbo].[FormTypeRegion]
(
[FormTypeID] [int] NOT NULL,
[RegionID] [int] NOT NULL,
[FlowDefinitionID] [int] NOT NULL CONSTRAINT [DF_FormTypeRegion_FlowDefinitionID] DEFAULT ((2)),
[ModifiedTimeStamp] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FormTypeRegion] ADD CONSTRAINT [PK_FormTypeRegion_1] PRIMARY KEY CLUSTERED  ([FormTypeID], [RegionID], [FlowDefinitionID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FormTypeRegion] ADD CONSTRAINT [FK_FormTypeRegion_FormType] FOREIGN KEY ([FormTypeID]) REFERENCES [dbo].[FormType] ([FormTypeID])
GO
ALTER TABLE [dbo].[FormTypeRegion] ADD CONSTRAINT [FK_FormTypeRegion_Region] FOREIGN KEY ([RegionID]) REFERENCES [dbo].[Region] ([RegionID])
GO
