CREATE TABLE [dbo].[GeneralAction]
(
[GeneralActionID] [int] NOT NULL IDENTITY(1, 1),
[GeneralActionTypeID] [int] NOT NULL,
[MethodName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Rights] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClassNameMenuItem] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClassNameGlyphicon] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OrderNumber] [int] NULL,
[Subcategory] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OnFormSetVersionOnly] [bit] NULL,
[HidePhaseButtons] [bit] NOT NULL CONSTRAINT [DF_GeneralAction_HidePhaseButtons] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[GeneralAction] ADD CONSTRAINT [PK_dbo.GeneralAction] PRIMARY KEY CLUSTERED  ([GeneralActionID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_GeneralActionTypeID] ON [dbo].[GeneralAction] ([GeneralActionTypeID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[GeneralAction] ADD CONSTRAINT [FK_dbo.GeneralAction_dbo.GeneralActionType_GeneralActionTypeID] FOREIGN KEY ([GeneralActionTypeID]) REFERENCES [dbo].[GeneralActionType] ([GeneralActionTypeID])
GO
