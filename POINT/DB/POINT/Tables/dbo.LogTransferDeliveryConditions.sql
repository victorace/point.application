CREATE TABLE [dbo].[LogTransferDeliveryConditions]
(
[LogTransferDeliveryConditionID] [int] NOT NULL IDENTITY(1, 1),
[TransferID] [int] NOT NULL,
[DeliveryConditionID] [int] NOT NULL,
[ClientID] [int] NULL,
[TimeStamp] [datetime] NULL,
[EmployeeID] [int] NULL,
[Action] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ScreenID] [int] NULL,
[Version] [int] NULL,
[TypeID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[LogTransferDeliveryConditions] ADD CONSTRAINT [PK_LogTransferDeliveryConditions] PRIMARY KEY CLUSTERED  ([LogTransferDeliveryConditionID]) ON [PRIMARY]
GO
