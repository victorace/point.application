CREATE TABLE [dbo].[MutMedicineCard]
(
[MutMedicineCardID] [int] NOT NULL IDENTITY(1, 1),
[LogMedicineCardID] [int] NOT NULL,
[MedicineCardID] [int] NOT NULL,
[TransferID] [int] NOT NULL,
[MedicineID] [int] NOT NULL,
[NotRegisteredMedicine] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Dosage] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Frequence] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FinishDate] [datetime] NULL,
[Prescription] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TimeStamp] [datetime] NULL,
[EmployeeID] [int] NULL,
[ScreenID] [int] NULL,
[FormSetVersionID] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[MutMedicineCard] ADD CONSTRAINT [PK_MutMedicineCard] PRIMARY KEY CLUSTERED  ([MutMedicineCardID]) ON [PRIMARY]
GO
