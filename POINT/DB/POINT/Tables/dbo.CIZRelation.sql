CREATE TABLE [dbo].[CIZRelation]
(
[CIZRelationID] [int] NOT NULL IDENTITY(1, 1),
[Description] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EnumerationValue] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CIZRelation] ADD CONSTRAINT [PK_CIZRelation] PRIMARY KEY CLUSTERED  ([CIZRelationID]) ON [PRIMARY]
GO
