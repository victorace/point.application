CREATE TABLE [dbo].[ContactPerson]
(
[ContactPersonID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[PersonDataID] [int] NOT NULL,
[CIZRelationID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ContactPerson] ADD CONSTRAINT [PK_ContactPerson] PRIMARY KEY CLUSTERED  ([ContactPersonID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_ContactPerson_PersonDataID_Includes] ON [dbo].[ContactPerson] ([PersonDataID]) INCLUDE ([ContactPersonID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ContactPerson] ADD CONSTRAINT [FK_ContactPerson_CIZRelation] FOREIGN KEY ([CIZRelationID]) REFERENCES [dbo].[CIZRelation] ([CIZRelationID])
GO
ALTER TABLE [dbo].[ContactPerson] ADD CONSTRAINT [FK_ContactPerson_Client] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Client] ([ClientID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[ContactPerson] ADD CONSTRAINT [FK_ContactPerson_PersonData] FOREIGN KEY ([PersonDataID]) REFERENCES [dbo].[PersonData] ([PersonDataID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
