CREATE TABLE [dbo].[LogLocation]
(
[LogLocationID] [int] NOT NULL IDENTITY(1, 1),
[LocationID] [int] NULL,
[TimeStamp] [datetime] NULL,
[EmployeeID] [int] NULL,
[Action] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ScreenID] [int] NULL,
[Version] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[LogLocation] ADD CONSTRAINT [PK_LogLocation] PRIMARY KEY CLUSTERED  ([LogLocationID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_LogLocation_LocationID] ON [dbo].[LogLocation] ([LocationID]) ON [PRIMARY]
GO
