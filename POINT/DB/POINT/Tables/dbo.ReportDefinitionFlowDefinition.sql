CREATE TABLE [dbo].[ReportDefinitionFlowDefinition]
(
[ReportDefinitionID] [int] NOT NULL,
[FlowDefinitionID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ReportDefinitionFlowDefinition] ADD CONSTRAINT [PK_ReportDefinitionOrganizationType] PRIMARY KEY CLUSTERED  ([ReportDefinitionID], [FlowDefinitionID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ReportDefinitionFlowDefinition] ADD CONSTRAINT [FK_ReportDefinitionFlowDefinition_FlowDefinition] FOREIGN KEY ([FlowDefinitionID]) REFERENCES [dbo].[FlowDefinition] ([FlowDefinitionID])
GO
ALTER TABLE [dbo].[ReportDefinitionFlowDefinition] ADD CONSTRAINT [FK_ReportDefinitionFlowDefinition_FlowDefinition1] FOREIGN KEY ([FlowDefinitionID]) REFERENCES [dbo].[FlowDefinition] ([FlowDefinitionID])
GO
