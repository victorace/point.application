CREATE TABLE [dbo].[Frequency]
(
[FrequencyID] [int] NOT NULL IDENTITY(1, 1),
[TransferID] [int] NOT NULL,
[FormSetVersionID] [int] NULL,
[Quantity] [int] NOT NULL,
[Type] [int] NOT NULL,
[StartDate] [datetime] NOT NULL,
[EndDate] [datetime] NOT NULL,
[Status] [int] NOT NULL,
[EmployeeID] [int] NULL,
[Timestamp] [datetime] NULL,
[ScreenName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Cancelled] [bit] NOT NULL CONSTRAINT [DF_Frequency_Cancelled] DEFAULT ((0)),
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FeedbackType] [int] NOT NULL CONSTRAINT [DF_Frequency_FrequencyFeedbackType] DEFAULT ((1))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Frequency] ADD CONSTRAINT [PK_dbo.Frequency] PRIMARY KEY CLUSTERED  ([FrequencyID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_EmployeeID] ON [dbo].[Frequency] ([EmployeeID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FormSetVersionID] ON [dbo].[Frequency] ([FormSetVersionID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_TransferID] ON [dbo].[Frequency] ([TransferID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Frequency_TransferIDTypeCancelled] ON [dbo].[Frequency] ([TransferID], [Type], [Cancelled]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Frequency] ADD CONSTRAINT [FK_dbo.Frequency_dbo.Employee_EmployeeID] FOREIGN KEY ([EmployeeID]) REFERENCES [dbo].[Employee] ([EmployeeID])
GO
ALTER TABLE [dbo].[Frequency] ADD CONSTRAINT [FK_dbo.Frequency_dbo.Transfer_TransferID] FOREIGN KEY ([TransferID]) REFERENCES [dbo].[Transfer] ([TransferID])
GO
