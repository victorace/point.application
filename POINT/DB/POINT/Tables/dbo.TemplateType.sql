CREATE TABLE [dbo].[TemplateType]
(
[TemplateTypeID] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CanBeOrganizationSpecific] [bit] NOT NULL CONSTRAINT [DF_TemplateType_IsManageable
manageable]]] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TemplateType] ADD CONSTRAINT [PK_TemplateType] PRIMARY KEY CLUSTERED  ([TemplateTypeID]) ON [PRIMARY]
GO
