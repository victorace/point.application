CREATE TABLE [dbo].[OrganizationSearchPostalCode]
(
[OrganizationSearchPostalCodeID] [int] NOT NULL IDENTITY(1, 1),
[OrganizationSearchID] [int] NOT NULL,
[StartPostalCode] [char] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EndPostalCode] [char] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[OrganizationSearchPostalCode] ADD CONSTRAINT [PK_OrganizationSearchPostalCode] PRIMARY KEY CLUSTERED  ([OrganizationSearchPostalCodeID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_OrganizationSearchPostalCode_OrganizationSearchID] ON [dbo].[OrganizationSearchPostalCode] ([OrganizationSearchID]) INCLUDE ([EndPostalCode], [OrganizationSearchPostalCodeID], [StartPostalCode]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_OrganizationSearchPostalCode_Postalcode] ON [dbo].[OrganizationSearchPostalCode] ([StartPostalCode], [EndPostalCode]) INCLUDE ([OrganizationSearchID], [OrganizationSearchPostalCodeID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[OrganizationSearchPostalCode] ADD CONSTRAINT [FK_OrganizationSearchPostalCode_OrganizationSearch] FOREIGN KEY ([OrganizationSearchID]) REFERENCES [dbo].[OrganizationSearch] ([OrganizationSearchID])
GO
