CREATE TABLE [dbo].[MessageReceivedTemp]
(
[MessageReceivedTempID] [int] NOT NULL IDENTITY(1, 1),
[TimeStamp] [datetime] NOT NULL,
[GUID] [uniqueidentifier] NOT NULL,
[OrganisationID] [int] NOT NULL,
[SenderUserName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SendDate] [datetime] NOT NULL,
[PatientNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VisitNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Hash] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Type] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MessageID] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Source] [int] NOT NULL CONSTRAINT [DF_MessageReceivedTemp_Source] DEFAULT ((0))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[MessageReceivedTemp] ADD CONSTRAINT [PK_dbo.MessageReceivedTemp] PRIMARY KEY CLUSTERED  ([MessageReceivedTempID]) ON [PRIMARY]
GO
