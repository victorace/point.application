CREATE TABLE [dbo].[FlowDefinitionFormType]
(
[FlowDefinitionID] [int] NOT NULL,
[FormTypeID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FlowDefinitionFormType] ADD CONSTRAINT [PK_FlowDefinitionFormType] PRIMARY KEY CLUSTERED  ([FlowDefinitionID], [FormTypeID]) ON [PRIMARY]
GO
