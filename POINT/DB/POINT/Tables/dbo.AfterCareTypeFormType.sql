CREATE TABLE [dbo].[AfterCareTypeFormType]
(
[AfterCareTypeFormTypeID] [int] NOT NULL IDENTITY(1, 1),
[AfterCareTypeID] [int] NOT NULL,
[FormTypeID] [int] NOT NULL CONSTRAINT [DF_AfterCareTypeFormType_FormTypeID] DEFAULT ((202))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AfterCareTypeFormType] ADD CONSTRAINT [PK_AfterCareTypeFormType] PRIMARY KEY CLUSTERED  ([AfterCareTypeFormTypeID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AfterCareTypeFormType] ADD CONSTRAINT [FK_AfterCareTypeFormType_AfterCareType] FOREIGN KEY ([AfterCareTypeID]) REFERENCES [dbo].[AfterCareType] ([AfterCareTypeID])
GO
ALTER TABLE [dbo].[AfterCareTypeFormType] ADD CONSTRAINT [FK_AfterCareTypeFormType_FormType] FOREIGN KEY ([FormTypeID]) REFERENCES [dbo].[FormType] ([FormTypeID])
GO
