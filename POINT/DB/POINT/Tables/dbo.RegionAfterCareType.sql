CREATE TABLE [dbo].[RegionAfterCareType]
(
[RegionAfterCareTypeID] [int] NOT NULL IDENTITY(1, 1),
[RegionID] [int] NOT NULL,
[AfterCareTypeID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RegionAfterCareType] ADD CONSTRAINT [PK_RegionAfterCareType] PRIMARY KEY CLUSTERED  ([RegionAfterCareTypeID]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [ix_RegionAfterCareType_RegionID_AfterCareTypeID] ON [dbo].[RegionAfterCareType] ([RegionID], [AfterCareTypeID]) ON [PRIMARY]
GO
