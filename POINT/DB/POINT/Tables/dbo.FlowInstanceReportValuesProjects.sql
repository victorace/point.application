CREATE TABLE [dbo].[FlowInstanceReportValuesProjects]
(
[FlowInstanceID] [int] NOT NULL,
[OrganizationProjectID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FlowInstanceReportValuesProjects] ADD CONSTRAINT [PK_FlowInstanceReportValuesProjects] PRIMARY KEY CLUSTERED  ([FlowInstanceID], [OrganizationProjectID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FlowInstanceReportValuesProjects_FlowInstanceID] ON [dbo].[FlowInstanceReportValuesProjects] ([FlowInstanceID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FlowInstanceReportValuesProjects] ADD CONSTRAINT [FK_FlowInstanceReportValuesProjects_FlowInstanceReportValues] FOREIGN KEY ([FlowInstanceID]) REFERENCES [dbo].[FlowInstanceReportValues] ([FlowInstanceID])
GO
ALTER TABLE [dbo].[FlowInstanceReportValuesProjects] ADD CONSTRAINT [FK_FlowInstanceReportValuesProjects_OrganizationProject] FOREIGN KEY ([OrganizationProjectID]) REFERENCES [dbo].[OrganizationProject] ([OrganizationProjectID])
GO
