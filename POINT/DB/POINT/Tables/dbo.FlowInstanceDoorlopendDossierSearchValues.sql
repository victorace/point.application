CREATE TABLE [dbo].[FlowInstanceDoorlopendDossierSearchValues]
(
[FlowInstanceID] [int] NOT NULL,
[FrequencyType] [int] NOT NULL,
[FrequencyID] [int] NOT NULL,
[FrequencyStartDate] [datetime] NULL,
[FrequencyEndDate] [datetime] NULL,
[FrequencyName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastFrequencyTransferMemoID] [int] NULL,
[LastFrequencyTransferMemoDateTime] [datetime] NULL,
[LastFrequencyTransferMemoEmployeeName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastFrequencyTransferMemoTarget] [int] NULL,
[LastFrequencyAttachmentID] [int] NULL,
[LastFrequencyAttachmentUploadDate] [datetime] NULL,
[LastFrequencyAttachmentSource] [int] NULL,
[FrequencyScopeType] [int] NULL,
[FrequencyStatus] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FlowInstanceDoorlopendDossierSearchValues] ADD CONSTRAINT [PK_FlowInstanceDoorlopendDossierSearchValues] PRIMARY KEY CLUSTERED  ([FlowInstanceID], [FrequencyID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FlowInstanceDoorlopendDossierSearchValues] ADD CONSTRAINT [FK_FlowInstanceDoorlopendDossierSearchValues_FlowInstanceSearchValues] FOREIGN KEY ([FlowInstanceID]) REFERENCES [dbo].[FlowInstanceSearchValues] ([FlowInstanceID]) ON DELETE CASCADE
GO
