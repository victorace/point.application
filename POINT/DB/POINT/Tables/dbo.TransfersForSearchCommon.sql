CREATE TABLE [dbo].[TransfersForSearchCommon]
(
[TransfersForSearchCommonID] [int] NOT NULL IDENTITY(1, 1),
[DepartmentID] [int] NULL,
[OrganizationID] [int] NULL,
[DefaultSortColumn] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DefaultSortOrder] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RefreshTimer] [int] NULL CONSTRAINT [DF_TransfersForSearchCommon_Refresh] DEFAULT ((0)),
[ScopeTypes] [varchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UsePagination] [bit] NOT NULL CONSTRAINT [DF__Transfers__UsePa__60FD2D21] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TransfersForSearchCommon] ADD CONSTRAINT [PK_TransfersForSearch] PRIMARY KEY CLUSTERED  ([TransfersForSearchCommonID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_TransfersForSearchCommon_DepartmentID] ON [dbo].[TransfersForSearchCommon] ([DepartmentID]) INCLUDE ([TransfersForSearchCommonID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_TransfersForSearchCommon_OrganizationID] ON [dbo].[TransfersForSearchCommon] ([OrganizationID]) INCLUDE ([TransfersForSearchCommonID]) ON [PRIMARY]
GO
