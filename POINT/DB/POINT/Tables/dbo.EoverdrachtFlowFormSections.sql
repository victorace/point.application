CREATE TABLE [dbo].[EoverdrachtFlowFormSections]
(
[Section] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Subsection] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[H4] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FormGroup] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FlowFieldID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EoverdrachtFlowFormSections] ADD CONSTRAINT [PK__Eoverdra__629714EBFB749835] PRIMARY KEY CLUSTERED  ([FlowFieldID]) ON [PRIMARY]
GO
