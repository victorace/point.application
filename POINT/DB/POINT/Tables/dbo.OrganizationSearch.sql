CREATE TABLE [dbo].[OrganizationSearch]
(
[OrganizationSearchID] [int] NOT NULL IDENTITY(1, 1),
[OrganizationID] [int] NOT NULL,
[OrganizationName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OrganizationTypeID] [int] NOT NULL,
[LocationID] [int] NOT NULL,
[LocationName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[City] [varchar] (512) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DepartmentID] [int] NOT NULL,
[DepartmentName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IsRehabilitationCenter] [bit] NOT NULL,
[IsIntensiveRehabilitationNursingHome] [bit] NOT NULL,
[Capacity0] [int] NULL,
[Capacity1] [int] NULL,
[Capacity2] [int] NULL,
[Capacity3] [int] NULL,
[OrganizationTypeName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AfterCareType] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DepartmentTypeID] [int] NOT NULL CONSTRAINT [DF_OrganizationSearch_DepartmentTypeID] DEFAULT ((1)),
[LocationPostcode] [varchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AfterCareCategory] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[OrganizationSearch] ADD CONSTRAINT [PK_OrganizationSearch_1] PRIMARY KEY CLUSTERED  ([OrganizationSearchID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_OrganizationSearch_DepartmentID] ON [dbo].[OrganizationSearch] ([DepartmentID]) INCLUDE ([OrganizationSearchID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_OrganizationSearch_LocationID_Includes] ON [dbo].[OrganizationSearch] ([LocationID]) INCLUDE ([AfterCareType], [Capacity0], [Capacity1], [Capacity2], [Capacity3], [City], [DepartmentID], [DepartmentName], [DepartmentTypeID], [IsIntensiveRehabilitationNursingHome], [IsRehabilitationCenter], [LocationName], [LocationPostcode], [OrganizationID], [OrganizationName], [OrganizationSearchID], [OrganizationTypeID], [OrganizationTypeName]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_OrganizationSearch_OrganizationNameCity] ON [dbo].[OrganizationSearch] ([OrganizationTypeID], [OrganizationID], [OrganizationName], [City]) INCLUDE ([AfterCareType], [Capacity0], [Capacity1], [Capacity2], [Capacity3], [DepartmentID], [DepartmentName], [DepartmentTypeID], [IsIntensiveRehabilitationNursingHome], [IsRehabilitationCenter], [LocationID], [LocationName], [LocationPostcode], [OrganizationSearchID], [OrganizationTypeName]) ON [PRIMARY]
GO
