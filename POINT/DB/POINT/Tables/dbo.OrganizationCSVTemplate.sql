CREATE TABLE [dbo].[OrganizationCSVTemplate]
(
[OrganizationCSVTemplateID] [int] NOT NULL IDENTITY(1, 1),
[OrganizationID] [int] NOT NULL,
[CSVTemplateTypeID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[OrganizationCSVTemplate] ADD CONSTRAINT [PK_OrganizationCSVTemplate_1] PRIMARY KEY CLUSTERED  ([OrganizationCSVTemplateID]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_OrganizationCSVTemplate_OrganizationIDCSVTemplateTypeID] ON [dbo].[OrganizationCSVTemplate] ([OrganizationID], [CSVTemplateTypeID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[OrganizationCSVTemplate] ADD CONSTRAINT [FK_OrganizationCSVTemplate_Organization] FOREIGN KEY ([OrganizationID]) REFERENCES [dbo].[Organization] ([OrganizationID])
GO
ALTER TABLE [dbo].[OrganizationCSVTemplate] ADD CONSTRAINT [FK_OrganizationCSVTemplate_OrganizationCSVTemplate] FOREIGN KEY ([OrganizationCSVTemplateID]) REFERENCES [dbo].[OrganizationCSVTemplate] ([OrganizationCSVTemplateID])
GO
