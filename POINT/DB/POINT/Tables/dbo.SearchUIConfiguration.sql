CREATE TABLE [dbo].[SearchUIConfiguration]
(
[SearchUIConfigurationID] [int] NOT NULL IDENTITY(1, 1),
[DepartmentID] [int] NULL,
[OrganizationID] [int] NULL,
[SearchType] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SortOrder_Field_IsWellKnown] [bit] NOT NULL,
[SortOrder_Field_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SortOrder_OrderByDescending] [bit] NOT NULL,
[AutoRefreshSeconds] [int] NOT NULL,
[DossierMenus_OnlyCVA] [bit] NOT NULL,
[DossierMenus_OnlyMSVT] [bit] NOT NULL,
[DossierMenus_AllAcceptCVA] [bit] NOT NULL,
[DossierMenus_AllAcceptMSVT] [bit] NOT NULL,
[DossierMenus_AllAcceptCVAAndMSVT] [bit] NOT NULL,
[PageSize] [int] NOT NULL,
[FlowDefinitionID] [int] NOT NULL CONSTRAINT [DF_SearchUIConfiguration_FlowDefinitionID] DEFAULT ((2))
) ON [PRIMARY]
ALTER TABLE [dbo].[SearchUIConfiguration] ADD
CONSTRAINT [FK_SearchUIConfiguration_FlowDefinition] FOREIGN KEY ([FlowDefinitionID]) REFERENCES [dbo].[FlowDefinition] ([FlowDefinitionID])
ALTER TABLE [dbo].[SearchUIConfiguration] ADD 
CONSTRAINT [PK_dbo.SearchUIConfiguration] PRIMARY KEY CLUSTERED  ([SearchUIConfigurationID]) ON [PRIMARY]
GO

ALTER TABLE [dbo].[SearchUIConfiguration] ADD CONSTRAINT [FK_SearchUIConfiguration_Department] FOREIGN KEY ([DepartmentID]) REFERENCES [dbo].[Department] ([DepartmentID])
GO
ALTER TABLE [dbo].[SearchUIConfiguration] ADD CONSTRAINT [FK_SearchUIConfiguration_Organization] FOREIGN KEY ([OrganizationID]) REFERENCES [dbo].[Organization] ([OrganizationID])
GO
