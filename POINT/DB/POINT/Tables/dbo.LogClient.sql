CREATE TABLE [dbo].[LogClient]
(
[LogClientID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NULL,
[TimeStamp] [datetime] NULL,
[EmployeeID] [int] NULL,
[Action] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ScreenID] [int] NULL,
[Version] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[LogClient] ADD CONSTRAINT [PK_LogClient] PRIMARY KEY CLUSTERED  ([LogClientID]) ON [PRIMARY]
GO
