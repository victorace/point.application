CREATE TABLE [dbo].[PhaseInstance]
(
[PhaseInstanceID] [int] NOT NULL IDENTITY(1, 1),
[PhaseDefinitionID] [int] NOT NULL,
[FlowInstanceID] [int] NOT NULL,
[Status] [int] NOT NULL,
[StartProcessDate] [datetime] NOT NULL,
[StartedByEmployeeID] [int] NOT NULL,
[RequestDate] [datetime] NULL,
[RealizedDate] [datetime] NULL,
[RealizedByEmployeeID] [int] NULL,
[EmployeeID] [int] NULL,
[Timestamp] [datetime] NULL,
[ScreenName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Cancelled] [bit] NOT NULL CONSTRAINT [DF_PhaseInstance_Inactive] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PhaseInstance] ADD CONSTRAINT [PK_dbo.PhaseInstance] PRIMARY KEY CLUSTERED  ([PhaseInstanceID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FlowInstanceID] ON [dbo].[PhaseInstance] ([FlowInstanceID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FlowInstanceIDCancelled_PhaseInstanceID] ON [dbo].[PhaseInstance] ([FlowInstanceID], [Cancelled]) INCLUDE ([PhaseDefinitionID], [PhaseInstanceID], [RealizedDate], [StartProcessDate], [Status]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_PhaseDefinitionID] ON [dbo].[PhaseInstance] ([PhaseDefinitionID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_PhaseInstance_FlowInstanceIDPhaseDefinitionIDCancelled] ON [dbo].[PhaseInstance] ([PhaseDefinitionID], [FlowInstanceID], [Cancelled], [PhaseInstanceID] DESC) INCLUDE ([Status]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_PhaseInstance_PhaseDefinitionID_Status_Cancelled_Includes] ON [dbo].[PhaseInstance] ([PhaseDefinitionID], [Status], [Cancelled]) INCLUDE ([FlowInstanceID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_RealizedByEmployeeID] ON [dbo].[PhaseInstance] ([RealizedByEmployeeID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_StartedByEmployeeID] ON [dbo].[PhaseInstance] ([StartedByEmployeeID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Status_PhaseInstanceID] ON [dbo].[PhaseInstance] ([Status]) INCLUDE ([PhaseInstanceID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_PhaseInstance_Status_PhaseDefinitionID_Cancelled_Includes] ON [dbo].[PhaseInstance] ([Status], [PhaseDefinitionID], [Cancelled]) INCLUDE ([FlowInstanceID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PhaseInstance] ADD CONSTRAINT [FK_dbo.PhaseInstance_dbo.Employee_RealizedByEmployeeID] FOREIGN KEY ([RealizedByEmployeeID]) REFERENCES [dbo].[Employee] ([EmployeeID])
GO
ALTER TABLE [dbo].[PhaseInstance] ADD CONSTRAINT [FK_dbo.PhaseInstance_dbo.Employee_StartedByEmployeeID] FOREIGN KEY ([StartedByEmployeeID]) REFERENCES [dbo].[Employee] ([EmployeeID])
GO
ALTER TABLE [dbo].[PhaseInstance] ADD CONSTRAINT [FK_dbo.PhaseInstance_dbo.FlowInstance_FlowInstanceID] FOREIGN KEY ([FlowInstanceID]) REFERENCES [dbo].[FlowInstance] ([FlowInstanceID])
GO
ALTER TABLE [dbo].[PhaseInstance] ADD CONSTRAINT [FK_dbo.PhaseInstance_dbo.PhaseDefinition_PhaseDefinitionID] FOREIGN KEY ([PhaseDefinitionID]) REFERENCES [dbo].[PhaseDefinition] ([PhaseDefinitionID])
GO
