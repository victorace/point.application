CREATE TABLE [dbo].[LogActionHealthInsurer]
(
[LogActionHealthInsurerID] [int] NOT NULL IDENTITY(1, 1),
[ActionHealthInsurerID] [int] NULL,
[TimeStamp] [datetime] NULL,
[EmployeeID] [int] NULL,
[Action] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ScreenID] [int] NULL,
[Version] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[LogActionHealthInsurer] ADD CONSTRAINT [PK_LogActionHealthInsurer] PRIMARY KEY CLUSTERED  ([LogActionHealthInsurerID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_LogActionHealthInsurer_ActionHealthInsurerID] ON [dbo].[LogActionHealthInsurer] ([ActionHealthInsurerID]) ON [PRIMARY]
GO
