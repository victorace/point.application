CREATE TABLE [dbo].[FlowInstanceStatus]
(
[FlowInstanceStatusID] [int] NOT NULL IDENTITY(1, 1),
[FlowInstanceStatusTypeID] [int] NOT NULL,
[FlowInstanceID] [int] NOT NULL,
[StatusDate] [datetime] NOT NULL,
[Comment] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmployeeID] [int] NULL,
[Timestamp] [datetime] NULL,
[ScreenName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[FlowInstanceStatus] ADD CONSTRAINT [PK_FlowInstanceStatus_1] PRIMARY KEY CLUSTERED  ([FlowInstanceStatusID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FlowInstanceStatus_FlowInstanceID_All] ON [dbo].[FlowInstanceStatus] ([FlowInstanceID]) INCLUDE ([Comment], [EmployeeID], [FlowInstanceStatusID], [FlowInstanceStatusTypeID], [ScreenName], [StatusDate], [Timestamp]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FlowInstanceStatus_FlowInstanceIDTypeID_All] ON [dbo].[FlowInstanceStatus] ([FlowInstanceStatusTypeID], [FlowInstanceID]) INCLUDE ([Comment], [EmployeeID], [FlowInstanceStatusID], [ScreenName], [StatusDate], [Timestamp]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FlowInstanceStatus] ADD CONSTRAINT [FK_FlowInstanceStatus_Employee] FOREIGN KEY ([EmployeeID]) REFERENCES [dbo].[Employee] ([EmployeeID])
GO
