CREATE TABLE [dbo].[HealthInsurerConcern]
(
[HealthInsurerConcernID] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[HealthInsurerConcern] ADD CONSTRAINT [PK_HealthInsurerConcern] PRIMARY KEY CLUSTERED  ([HealthInsurerConcernID]) ON [PRIMARY]
GO
