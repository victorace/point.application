CREATE TABLE [dbo].[ClientReceivedTempHL7_15]
(
[TimeStamp] [datetime] NOT NULL CONSTRAINT [DF_ClientReceivedTempHL7_15_TimeStamp] DEFAULT (getdate()),
[MessageID] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OrganisationID] [int] NOT NULL,
[SenderUserName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SendDate] [datetime] NULL,
[PatientNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PatientNumberMerge] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CivilServiceNumber] [varchar] (9) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Gender] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Salutation] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Initials] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirstName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MiddleName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaidenName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BirthDate] [datetime] NULL,
[StreetName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Number] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PostalCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Country] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PhoneNumberGeneral] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PhoneNumberMobile] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PhoneNumberWork] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContactPersonName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContactPersonRelationType] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContactPersonBirthDate] [datetime] NULL,
[ContactPersonPhoneNumberGeneral] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContactPersonPhoneNumberMobile] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContactPersonPhoneNumberWork] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HealthInsuranceCompanyUZOVICode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InsuranceNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GeneralPractitionerName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GeneralPractitionerPhoneNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HealthCareProvider] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MessageType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AddressGPForZorgmail] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VisitNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[ClientReceivedTempHL7_15_ins] ON [dbo].[ClientReceivedTempHL7_15]
    FOR INSERT
AS
    BEGIN 

        DECLARE @OrganizationID INT ,
            @PatientNumber VARCHAR(50) ,
            @PatientNumberMerge VARCHAR(50) ,
            @CivilServiceNumber VARCHAR(9) ,
            @Gender VARCHAR(1) ,
            @Salutation VARCHAR(255) ,
            @Initials VARCHAR(50) ,
            @FirstName VARCHAR(255) ,
            @MiddleName VARCHAR(50) ,
            @LastName VARCHAR(255) ,
            @MaidenName VARCHAR(255) ,
            @BirthDate DATETIME ,
            @StreetName VARCHAR(255) ,
            @Number VARCHAR(255) ,
            @PostalCode VARCHAR(6) ,
            @City VARCHAR(255) ,
            @Country VARCHAR(255) ,
            @PhoneNumberGeneral VARCHAR(255) ,
            @PhoneNumberMobile VARCHAR(255) ,
            @PhoneNumberWork VARCHAR(255) ,
            @MessageType VARCHAR(50);

        SELECT  @OrganizationID = OrganisationID ,
                @PatientNumber = PatientNumber ,
                @PatientNumberMerge = PatientNumberMerge ,
                @CivilServiceNumber = CivilServiceNumber ,
                @Gender = Gender ,
                @Salutation = Salutation ,
                @Initials = Initials ,
                @FirstName = FirstName ,
                @MiddleName = MiddleName ,
                @LastName = LastName ,
                @MaidenName = MaidenName ,
                @BirthDate = BirthDate ,
                @StreetName = StreetName ,
                @Number = Number ,
                @PostalCode = PostalCode ,
                @City = City ,
                @Country = Country ,
                @PhoneNumberGeneral = PhoneNumberGeneral ,
                @PhoneNumberMobile = PhoneNumberMobile ,
                @PhoneNumberWork = PhoneNumberWork ,
                @MessageType = MessageType
        FROM    INSERTED;

        IF @MessageType = 'A08'
            BEGIN
                IF EXISTS ( SELECT  1
                            FROM    Organization WITH ( NOLOCK )
                                    INNER JOIN Location WITH ( NOLOCK ) ON Organization.OrganizationID = Location.OrganizationID
                                    INNER JOIN Department WITH ( NOLOCK ) ON Location.LocationID = Department.LocationID
                                    INNER JOIN FlowInstance WITH ( NOLOCK ) ON FlowInstance.StartedByDepartmentID = Department.DepartmentID
                                    INNER JOIN Transfer WITH ( NOLOCK ) ON Transfer.TransferID = FlowInstance.TransferID
                                    INNER JOIN Client WITH ( NOLOCK ) ON Client.ClientID = Transfer.ClientID
                            WHERE   Client.PatientNumber = @PatientNumber
                                    AND Organization.OrganizationID = @OrganizationID
                                    AND Organization.Inactive = 0
                                    AND Location.Inactive = 0
                                    AND Department.Inactive = 0 )
                    BEGIN
                        SET NOCOUNT ON;

                        DECLARE @flowinstanceclient TABLE
                            (
                              RowID INT IDENTITY(1, 1) ,
                              FlowInstanceID INT ,
                              ClientID INT
                            );
                        INSERT  INTO @flowinstanceclient
                                ( FlowInstanceID ,
                                  ClientID
						        )
                                SELECT  FlowInstance.FlowInstanceID ,
                                        Transfer.ClientID
                                FROM    FlowInstance WITH ( NOLOCK )
                                        INNER JOIN Transfer WITH ( NOLOCK ) ON Transfer.TransferID = FlowInstance.TransferID
                                        INNER JOIN Client WITH ( NOLOCK ) ON Client.ClientID = Transfer.ClientID
                                        INNER JOIN Department WITH ( NOLOCK ) ON Department.DepartmentID = FlowInstance.StartedByDepartmentID
                                        INNER JOIN Location WITH ( NOLOCK ) ON Location.LocationID = Department.LocationID
                                        INNER JOIN Organization WITH ( NOLOCK ) ON Organization.OrganizationID = Location.OrganizationID
                                WHERE   FlowInstance.StartedByOrganizationID = @OrganizationID
                                        AND Client.PatientNumber = @PatientNumber
                                        AND Organization.Inactive = 0
                                        AND Location.Inactive = 0
                                        AND Department.Inactive = 0
                                        AND ( dbo.flowIsClosed(Transfer.TransferID) = 0 );

                        UPDATE  [Client]
                        SET     [CivilServiceNumber] = CASE WHEN @CivilServiceNumber NOT LIKE '%DontUpd%'
                                                            THEN @CivilServiceNumber
                                                            WHEN @CivilServiceNumber IS NULL
                                                            THEN NULL
                                                       END ,
                                [PatientNumber] = @PatientNumber ,
                                [Gender] = @Gender ,
                                [Salutation] = @Salutation ,
                                [Initials] = CASE WHEN @Initials NOT LIKE '%DontUpd%'
                                                  THEN @Initials
                                                  WHEN @Initials IS NULL
                                                  THEN NULL
                                                  ELSE Initials
                                             END ,
                                [FirstName] = CASE WHEN @FirstName NOT LIKE '%DontUpd%'
                                                   THEN @FirstName
                                                   WHEN @FirstName IS NULL
                                                   THEN NULL
                                                   ELSE FirstName
                                              END ,
                                [MiddleName] = CASE WHEN @MiddleName NOT LIKE '%DontUpd%'
                                                    THEN @MiddleName
                                                    WHEN @MiddleName IS NULL
                                                    THEN NULL
                                                    ELSE MiddleName
                                               END ,
                                [LastName] = CASE WHEN @LastName NOT LIKE '%DontUpd%'
                                                  THEN @LastName
--								When @LastName is null Then null
                                                  ELSE LastName
                                             END ,
                                [MaidenName] = CASE WHEN @MaidenName NOT LIKE '%DontUpd%'
                                                    THEN @MaidenName
                                                    WHEN @MaidenName IS NULL
                                                    THEN NULL
                                                    ELSE MaidenName
                                               END ,
                                [BirthDate] = @BirthDate ,
                                [StreetName] = CASE WHEN @StreetName NOT LIKE '%DontUpd%'
                                                    THEN @StreetName
                                                    WHEN @StreetName IS NULL
                                                    THEN NULL
                                                    ELSE StreetName
                                               END ,
                                [Number] = CASE WHEN @Number NOT LIKE '%DontUpd%'
                                                THEN @Number
                                                WHEN @Number IS NULL THEN NULL
                                                ELSE Number
                                           END ,
                                [PostalCode] = CASE WHEN @PostalCode NOT LIKE '%DontUp%'
                                                    THEN @PostalCode
                                                    WHEN @PostalCode IS NULL
                                                    THEN NULL
                                                    ELSE PostalCode
                                               END ,
                                [City] = CASE WHEN @City NOT LIKE '%DontUpd%'
                                              THEN @City
                                              WHEN @City IS NULL THEN NULL
                                              ELSE City
                                         END ,
                                [Country] = @Country ,
                                [PhoneNumberGeneral] = CASE WHEN @PhoneNumberGeneral NOT LIKE '%DontUpd%'
                                                            THEN @PhoneNumberGeneral
                                                            WHEN @PhoneNumberGeneral IS NULL
                                                            THEN NULL
                                                            ELSE PhoneNumberGeneral
                                                       END ,
                                [PhoneNumberMobile] = CASE WHEN @PhoneNumberMobile NOT LIKE '%DontUpd%'
                                                           THEN @PhoneNumberMobile
                                                           WHEN @PhoneNumberMobile IS NULL
                                                           THEN NULL
                                                           ELSE PhoneNumberMobile
                                                      END ,
                                [PhoneNumberWork] = @PhoneNumberWork ,
                                [ClientModifiedBy] = ( SELECT EmployeeID
                                                       FROM   Employee
                                                       WHERE  LastName = 'HL7Interface'
                                                     ) ,
                                [ClientModifiedDate] = GETDATE()
                        WHERE   Client.ClientID IN (
                                SELECT  ClientID
                                FROM    @flowinstanceclient );

                        DECLARE @maxrowid INT;
                        SELECT  @maxrowid = MAX(RowID)
                        FROM    @flowinstanceclient;

                        DECLARE @rowid INT = 1;
                        DECLARE @flowinstanceid INT;
                        DECLARE @clientid INT;

                        WHILE @rowid <= @maxrowid
                            BEGIN
                                SELECT  @flowinstanceid = FlowInstanceID ,
                                        @clientid = ClientID
                                FROM    @flowinstanceclient
                                WHERE   RowID = @rowid;

                                UPDATE  dbo.FlowInstanceSearchValues
                                SET     ClientCivilServiceNumber = CASE
                                                              WHEN @CivilServiceNumber NOT LIKE '%DontUpd%'
                                                              THEN @CivilServiceNumber
                                                              WHEN @CivilServiceNumber IS NULL
                                                              THEN NULL
                     END ,
                                        ClientBirthDate = @BirthDate ,
                                        ClientFullname = dbo.clientname_formatted(@clientid) ,
                                        ClientGender = @Gender
                                WHERE   FlowInstanceID = @flowinstanceid;

                                SELECT  @rowid = @rowid + 1;

                            END;
                    END;
            END;

    END;
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[ClientReceivedTempHL7_15_upd] ON [dbo].[ClientReceivedTempHL7_15]
    FOR UPDATE
AS
/* Let op! In deze koppeling huisnummer van een patient wordt niet updated bij A08 bericht - wens van de klant */
/* huisnummer van een patient komt in het veld Straat te staan*/
    BEGIN 

        DECLARE @OrganizationID INT ,
            @PatientNumber VARCHAR(50) ,
            @PatientNumberMerge VARCHAR(50) ,
            @CivilServiceNumber VARCHAR(9) ,
            @Gender VARCHAR(1) ,
            @Salutation VARCHAR(255) ,
            @Initials VARCHAR(50) ,
            @FirstName VARCHAR(255) ,
            @MiddleName VARCHAR(50) ,
            @LastName VARCHAR(255) ,
            @MaidenName VARCHAR(255) ,
            @BirthDate DATETIME ,
            @StreetName VARCHAR(255) ,
            @Number VARCHAR(255) ,
            @PostalCode VARCHAR(6) ,
            @City VARCHAR(255) ,
            @Country VARCHAR(255) ,
            @PhoneNumberGeneral VARCHAR(255) ,
            @PhoneNumberMobile VARCHAR(255) ,
            @PhoneNumberWork VARCHAR(255) ,
            @MessageType VARCHAR(50);

        SELECT  @OrganizationID = OrganisationID ,
                @PatientNumber = PatientNumber ,
                @PatientNumberMerge = PatientNumberMerge ,
                @CivilServiceNumber = CivilServiceNumber ,
                @Gender = Gender ,
                @Salutation = Salutation ,
                @Initials = Initials ,
                @FirstName = FirstName ,
                @MiddleName = MiddleName ,
                @LastName = LastName ,
                @MaidenName = MaidenName ,
                @BirthDate = BirthDate ,
                @StreetName = StreetName ,
                @PostalCode = PostalCode ,
                @City = City ,
                @Country = Country ,
                @PhoneNumberGeneral = PhoneNumberGeneral ,
                @PhoneNumberMobile = PhoneNumberMobile ,
                @PhoneNumberWork = PhoneNumberWork ,
                @MessageType = MessageType
        FROM    INSERTED;

        IF @MessageType = 'A08'
            BEGIN
                IF EXISTS ( SELECT  1
                            FROM    Organization WITH ( NOLOCK )
                                    INNER JOIN Location WITH ( NOLOCK ) ON Organization.OrganizationID = Location.OrganizationID
                                    INNER JOIN Department WITH ( NOLOCK ) ON Location.LocationID = Department.LocationID
									INNER JOIN FlowInstance WITH ( NOLOCK ) ON FlowInstance.StartedByDepartmentID = Department.DepartmentID
                                    INNER JOIN Transfer WITH ( NOLOCK ) ON Transfer.TransferID = FlowInstance.TransferID
                                    INNER JOIN Client WITH ( NOLOCK ) ON Client.ClientID = Transfer.ClientID
                            WHERE   Client.PatientNumber = @PatientNumber
                                    AND Organization.OrganizationID = @OrganizationID
                                    AND Organization.Inactive = 0
                                    AND Location.Inactive = 0
                                    AND Department.Inactive = 0 )
                    BEGIN
                        SET NOCOUNT ON;

						DECLARE @flowinstanceclient TABLE
                            (
                              RowID INT IDENTITY(1, 1) ,
                              FlowInstanceID INT ,
                              ClientID INT
                            );
                        INSERT  INTO @flowinstanceclient
                                ( FlowInstanceID ,
                                  ClientID
						        )
                                SELECT  FlowInstance.FlowInstanceID ,
                                        Transfer.ClientID
                                FROM    FlowInstance WITH ( NOLOCK )
                                        INNER JOIN Transfer WITH ( NOLOCK ) ON Transfer.TransferID = FlowInstance.TransferID
                                        INNER JOIN Client WITH ( NOLOCK ) ON Client.ClientID = Transfer.ClientID
                                        INNER JOIN Department WITH ( NOLOCK ) ON Department.DepartmentID = FlowInstance.StartedByDepartmentID
                                        INNER JOIN Location WITH ( NOLOCK ) ON Location.LocationID = Department.LocationID
                                        INNER JOIN Organization WITH ( NOLOCK ) ON Organization.OrganizationID = Location.OrganizationID
                                WHERE   FlowInstance.StartedByOrganizationID = @OrganizationID
                                        AND Client.PatientNumber = @PatientNumber
                                        AND Organization.Inactive = 0
                                        AND Location.Inactive = 0
                                        AND Department.Inactive = 0
                                        AND ( dbo.flowIsClosed(Transfer.TransferID) = 0 );

                        UPDATE  [Client]
                        SET     [CivilServiceNumber] = CASE WHEN @CivilServiceNumber NOT LIKE '%DontUpd%'
                                                            THEN @CivilServiceNumber
                                                            WHEN @LastName IS NULL
                                                            THEN NULL
                                                       END ,
                                [PatientNumber] = @PatientNumber ,
                                [Gender] = @Gender ,
                                [Salutation] = @Salutation ,
                                [Initials] = CASE WHEN @Initials NOT LIKE '%DontUpd%'
                                                  THEN @Initials
                                                  WHEN @Initials IS NULL
                                                  THEN NULL
                                                  ELSE Initials
                                             END ,
                                [FirstName] = CASE WHEN @FirstName NOT LIKE '%DontUpd%'
                                                   THEN @FirstName
                                                   WHEN @FirstName IS NULL
                                                   THEN NULL
                                                   ELSE FirstName
                                              END ,
                                [MiddleName] = CASE WHEN @MiddleName NOT LIKE '%DontUpd%'
                                                    THEN @MiddleName
                                                    WHEN @MiddleName IS NULL
                                                    THEN NULL
                                                    ELSE MiddleName
                                               END ,
                                [LastName] = CASE WHEN @LastName NOT LIKE '%DontUpd%'
                                                  THEN @LastName
--								When @LastName is null Then null
                                                  ELSE LastName
                                             END ,
                                [MaidenName] = CASE WHEN @MaidenName NOT LIKE '%DontUpd%'
                                                    THEN @MaidenName
                                                    WHEN @MaidenName IS NULL
                                                    THEN NULL
                                                    ELSE MaidenName
                                               END ,
                                [BirthDate] = @BirthDate ,
                                [StreetName] = CASE WHEN @StreetName NOT LIKE '%DontUpd%'
                                                    THEN @StreetName
                                                    WHEN @StreetName IS NULL
              THEN NULL
                                                    ELSE StreetName
                                               END ,
                                [Number] = CASE WHEN @Number NOT LIKE '%DontUpd%'
                                                THEN @Number
                                                WHEN @Number IS NULL THEN NULL
                                                ELSE Number
                                           END ,
                                [PostalCode] = CASE WHEN @PostalCode NOT LIKE '%DontUp%'
                                                    THEN @PostalCode
                                                    WHEN @PostalCode IS NULL
                                                    THEN NULL
                                                    ELSE PostalCode
                                               END ,
                                [City] = CASE WHEN @City NOT LIKE '%DontUpd%'
                                              THEN @City
                                              WHEN @City IS NULL THEN NULL
                                              ELSE City
                                         END ,
                                [Country] = @Country ,
                                [PhoneNumberGeneral] = CASE WHEN @PhoneNumberGeneral NOT LIKE '%DontUpd%'
                                                            THEN @PhoneNumberGeneral
                                                            WHEN @PhoneNumberGeneral IS NULL
                                                            THEN NULL
                                                            ELSE PhoneNumberGeneral
                                                       END ,
                                [PhoneNumberMobile] = CASE WHEN @PhoneNumberMobile NOT LIKE '%DontUpd%'
                                                           THEN @PhoneNumberMobile
                                                           WHEN @PhoneNumberMobile IS NULL
                                                           THEN NULL
                                                           ELSE PhoneNumberMobile
                                                      END ,
                                [PhoneNumberWork] = @PhoneNumberWork ,
                                [ClientModifiedBy] = ( SELECT EmployeeID
                                                       FROM   Employee
                                                       WHERE  LastName = 'HL7Interface'
                                                     ) ,
                                [ClientModifiedDate] = GETDATE()
                        WHERE   Client.ClientID IN (
                                SELECT  ClientID
                                FROM    @flowinstanceclient );

                        DECLARE @maxrowid INT;
                        SELECT  @maxrowid = MAX(RowID)
                        FROM    @flowinstanceclient;

                        DECLARE @rowid INT = 1;
                        DECLARE @flowinstanceid INT;
                        DECLARE @clientid INT;

                        WHILE @rowid <= @maxrowid
                            BEGIN
                                SELECT  @flowinstanceid = FlowInstanceID ,
                                        @clientid = ClientID
                                FROM    @flowinstanceclient
                                WHERE   RowID = @rowid;

                                UPDATE  dbo.FlowInstanceSearchValues
                                SET     ClientCivilServiceNumber = CASE
                                                              WHEN @CivilServiceNumber NOT LIKE '%DontUpd%'
                                                              THEN @CivilServiceNumber
                                                              WHEN @CivilServiceNumber IS NULL
                                      THEN NULL
                                                              END ,
                                        ClientBirthDate = @BirthDate ,
                                        ClientFullname = dbo.clientname_formatted(@clientid) ,
                                        ClientGender = @Gender
                                WHERE   FlowInstanceID = @flowinstanceid;

                                SELECT  @rowid = @rowid + 1;

                            END;
                    END;
            END;

    END;
GO
ALTER TABLE [dbo].[ClientReceivedTempHL7_15] ADD CONSTRAINT [PK__ClientReceivedTempHL7_15] PRIMARY KEY CLUSTERED  ([TimeStamp], [MessageID], [OrganisationID]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description_15', N'Old patient number. It has to be replaced with PatientNumber(new number).', 'SCHEMA', N'dbo', 'TABLE', N'ClientReceivedTempHL7_15', 'COLUMN', N'PatientNumberMerge'
GO
