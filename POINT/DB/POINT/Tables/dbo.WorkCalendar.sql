CREATE TABLE [dbo].[WorkCalendar]
(
[dt] [smalldatetime] NOT NULL,
[isWeekDay] AS (CONVERT([bit],case  when datepart(weekday,[dt])=(7) OR datepart(weekday,[dt])=(1) then (0) else (1) end,(0))),
[isWorkDay] [bit] NOT NULL CONSTRAINT [DF_WorkCalendar_isWorkDay] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WorkCalendar] ADD CONSTRAINT [PK_WorkCalendar] PRIMARY KEY CLUSTERED  ([dt]) ON [PRIMARY]
GO
