CREATE TABLE [dbo].[FlowInstanceReportValuesAttachmentDetail]
(
[FlowInstanceID] [int] NOT NULL,
[AttachmentTypeID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FlowInstanceReportValuesAttachmentDetail] ADD CONSTRAINT [PK_FlowInstanceReportValuesAttachmentDetail] PRIMARY KEY CLUSTERED  ([FlowInstanceID], [AttachmentTypeID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FlowInstanceReportValuesAttachmentDetail_FlowInstanceID] ON [dbo].[FlowInstanceReportValuesAttachmentDetail] ([FlowInstanceID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FlowInstanceReportValuesAttachmentDetail] ADD CONSTRAINT [FK_FlowInstanceReportValuesAttachmentDetail_FlowInstanceReportValues] FOREIGN KEY ([FlowInstanceID]) REFERENCES [dbo].[FlowInstanceReportValues] ([FlowInstanceID])
GO
