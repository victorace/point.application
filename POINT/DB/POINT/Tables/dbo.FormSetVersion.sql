CREATE TABLE [dbo].[FormSetVersion]
(
[FormSetVersionID] [int] NOT NULL IDENTITY(1, 1),
[TransferID] [int] NOT NULL,
[FormTypeID] [int] NOT NULL,
[CreateDate] [datetime] NOT NULL,
[CreatedBy] [varchar] (400) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreatedByID] [int] NOT NULL,
[UpdatedBy] [varchar] (400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [varchar] (400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Deleted] [bit] NOT NULL CONSTRAINT [DF_FormSetVersion_Deleted] DEFAULT ((0)),
[UpdateDate] [datetime] NULL,
[UpdatedByID] [int] NULL,
[ScreenID] [int] NULL,
[PhaseInstanceID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FormSetVersion] ADD CONSTRAINT [PK_FormSetVersion] PRIMARY KEY CLUSTERED  ([FormSetVersionID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_PhaseInstanceIDDeleted_FormsetVersionID] ON [dbo].[FormSetVersion] ([Deleted], [PhaseInstanceID]) INCLUDE ([FormSetVersionID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FormSetVersion] ADD CONSTRAINT [IX_FormSetVersion] UNIQUE NONCLUSTERED  ([FormSetVersionID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FormSetVersion_FormSetVersionID_Deleted] ON [dbo].[FormSetVersion] ([FormSetVersionID]) INCLUDE ([Deleted]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FormTypeID] ON [dbo].[FormSetVersion] ([FormTypeID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_PhaseInstanceID] ON [dbo].[FormSetVersion] ([PhaseInstanceID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_TransferID] ON [dbo].[FormSetVersion] ([TransferID]) INCLUDE ([CreateDate], [CreatedBy], [CreatedByID], [Deleted], [Description], [FormSetVersionID], [FormTypeID], [PhaseInstanceID], [ScreenID], [UpdateDate], [UpdatedBy], [UpdatedByID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_FormSetVersion_TransferID_Deleted] ON [dbo].[FormSetVersion] ([TransferID], [Deleted]) INCLUDE ([CreateDate]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FormSetVersion] ADD CONSTRAINT [FK_dbo.FormSetVersion_dbo.FormType_FormTypeID] FOREIGN KEY ([FormTypeID]) REFERENCES [dbo].[FormType] ([FormTypeID])
GO
ALTER TABLE [dbo].[FormSetVersion] ADD CONSTRAINT [FK_FormSetVersion_PhaseInstance] FOREIGN KEY ([PhaseInstanceID]) REFERENCES [dbo].[PhaseInstance] ([PhaseInstanceID])
GO
