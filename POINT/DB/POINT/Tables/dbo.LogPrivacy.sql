CREATE TABLE [dbo].[LogPrivacy]
(
[LogPrivacyID] [int] NOT NULL IDENTITY(1, 1),
[OrganizationID] [int] NOT NULL,
[ClientID] [int] NULL,
[AttachmentID] [int] NULL,
[Action] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Timestamp] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[LogPrivacy] ADD CONSTRAINT [PK_LogPrivacy] PRIMARY KEY CLUSTERED  ([LogPrivacyID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_LogPrivacy_AttachmentID] ON [dbo].[LogPrivacy] ([AttachmentID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_LogPrivacy_ClientID] ON [dbo].[LogPrivacy] ([ClientID]) ON [PRIMARY]
GO
