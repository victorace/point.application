CREATE TABLE [dbo].[ActionHealthInsurer]
(
[ActionHealthInsurerID] [int] NOT NULL IDENTITY(1, 1),
[ActionCodeHealthInsurerID] [int] NULL,
[TransferID] [int] NULL,
[FormSetVersionID] [int] NULL,
[Amount] [int] NULL,
[Unit] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExpectedTime] [int] NULL,
[Definition] [varchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TimeStamp] [datetime] NULL,
[EmployeeID] [int] NULL,
[ScreenID] [int] NULL,
[UnitFrequency] [int] NULL,
[CustomFrequency] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- ========================================================================
-- Author:		Olga Shtareva
-- Create date: 20-01-2012
-- Description:	Logging
-- ========================================================================
CREATE TRIGGER [dbo].[ActionHealthInsurer_InsertUpdate]
   ON  [dbo].[ActionHealthInsurer] 
   AFTER UPDATE, INSERT
AS 
BEGIN

	SET NOCOUNT ON;

	IF (SELECT COUNT(*) FROM Inserted) = 0
	BEGIN
		RETURN
	END

	IF (SELECT Enabled FROM Logging) = 0
	BEGIN
		RETURN
	END

	-- BIJWERKEN LOGGING
	DECLARE	@LogActionHealthInsurerID	INT,
	@ActionHealthInsurerID	INT,
	@TimeStamp				DATETIME,
	@EmployeeID				INT,
	@Action					VARCHAR(1),
	@ScreenID				INT,
	@Version				INT

	SET NOCOUNT ON;
	
	IF UPDATE([TimeStamp])
	BEGIN
		SELECT
			@TimeStamp  = [TimeStamp],
			@EmployeeID	= [EmployeeID],
			@ScreenID	= [ScreenID]
		FROM Inserted
	END

	SELECT @ActionHealthInsurerID = ActionHealthInsurerID
	FROM Inserted

	IF exists (select * from deleted)
		SELECT @Action = 'U'
	ELSE
		SELECT @Action = 'I'
		
	SELECT @Version = (SELECT ISNULL(MAX(Version), 0) + 1 FROM LogActionHealthInsurer where LogActionHealthInsurer.ActionHealthInsurerID = @ActionHealthInsurerID)

	INSERT INTO LogActionHealthInsurer
			   (ActionHealthInsurerID,
			    [TimeStamp],
				EmployeeID,
				[Action],
				ScreenID,
				[Version])
		 VALUES
			(@ActionHealthInsurerID,
			 @TimeStamp,
			 @EmployeeID,
			 @Action,
			 @ScreenID,
			 @Version)

	SET @LogActionHealthInsurerID = SCOPE_IDENTITY();

	DECLARE @MutActionHealthInsurerID INT
	
	INSERT INTO MutActionHealthInsurer(
		LogActionHealthInsurerID,
		ActionHealthInsurerID,
		ActionCodeHealthInsurerID,
		TransferID,
		FormSetVersionID,
		Amount,
		Unit,
		ExpectedTime,
		Definition,
		TimeStamp,
		EmployeeID,
		ScreenID,
	    UnitFrequency,
		CustomFrequency)
		
	SELECT @LogActionHealthInsurerID, * from Inserted

	SET @MutActionHealthInsurerID = SCOPE_IDENTITY();

END
GO
ALTER TABLE [dbo].[ActionHealthInsurer] ADD CONSTRAINT [PK_ActionHealthInsurer] PRIMARY KEY CLUSTERED  ([ActionHealthInsurerID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_ActionHealthInsurer_ActionCodeHealthInsurerID_TransferID_FormSetVersionID_Includes] ON [dbo].[ActionHealthInsurer] ([ActionCodeHealthInsurerID], [TransferID], [FormSetVersionID]) INCLUDE ([ActionHealthInsurerID], [Amount], [Definition], [EmployeeID], [ExpectedTime], [ScreenID], [TimeStamp], [Unit]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_ActionHealthInsurer_FormSetVersionID] ON [dbo].[ActionHealthInsurer] ([FormSetVersionID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_ActionHealthInsurer_TransferID_FormSetVersionID] ON [dbo].[ActionHealthInsurer] ([TransferID], [FormSetVersionID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ActionHealthInsurer] ADD CONSTRAINT [FK_ActionHealthInsurer_FormSetVersion] FOREIGN KEY ([FormSetVersionID]) REFERENCES [dbo].[FormSetVersion] ([FormSetVersionID])
GO
ALTER TABLE [dbo].[ActionHealthInsurer] ADD CONSTRAINT [FK_ActionHealthInsurer_Transfer] FOREIGN KEY ([TransferID]) REFERENCES [dbo].[Transfer] ([TransferID])
GO
