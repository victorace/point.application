CREATE TABLE [dbo].[ZorgkantoorMessageIn]
(
[MessageID] [int] NOT NULL IDENTITY(1, 1),
[PointDossierNummer] [int] NULL,
[AGBCode] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AGBCodeZorgKantoor] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InzageIndicatieBesluit] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BSN] [int] NULL,
[GeboorteDatum] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClientNumber] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IndicatieBesluitNummer] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BesluitAfgiftedatum] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[timeStamp] [datetime] NOT NULL,
[HashCode] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ZorgkantoorMessageIn] ADD CONSTRAINT [PK_ZorgkantoorMessageIn] PRIMARY KEY CLUSTERED  ([MessageID]) ON [PRIMARY]
GO
