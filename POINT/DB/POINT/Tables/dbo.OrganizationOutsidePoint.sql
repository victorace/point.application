CREATE TABLE [dbo].[OrganizationOutsidePoint]
(
[OrganizationOutsidePointID] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OrganizationTypeID] [int] NULL,
[PostalCode] [char] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Code] [int] NULL,
[OrganizationID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[OrganizationOutsidePoint] ADD CONSTRAINT [PK_OrganizationOutsidePoint] PRIMARY KEY CLUSTERED  ([OrganizationOutsidePointID]) ON [PRIMARY]
GO
