CREATE TABLE [dbo].[Client]
(
[ClientID] [int] NOT NULL IDENTITY(1, 1),
[CivilServiceNumber] [varchar] (9) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PatientNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Gender] [int] NULL,
[Salutation] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Initials] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirstName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MiddleName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaidenName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BirthDate] [datetime] NULL,
[HealthInsuranceCompanyID] [int] NULL,
[InsuranceNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StreetName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Number] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PostalCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Country] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PhoneNumberGeneral] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PhoneNumberMobile] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PhoneNumberWork] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContactPersonName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContactPersonRelationType] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContactPersonBirthDate] [datetime] NULL,
[ContactPersonPhoneNumberGeneral] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContactPersonPhoneNumberMobile] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContactPersonPhoneWork] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GeneralPractitionerName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GeneralPractitionerPhoneNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PharmacyName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PharmacyPhoneNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CivilClass] [int] NULL,
[CompositionHousekeeping] [int] NULL,
[ChildrenInHousekeeping] [int] NULL,
[HousingType] [int] NULL,
[HousingTypeComment] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HealthCareProvider] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClientCreatedBy] [int] NULL,
[ClientCreatedDate] [datetime] NULL,
[ClientModifiedBy] [int] NULL,
[ClientModifiedDate] [datetime] NULL,
[NationalityID] [int] NULL,
[LanguageID] [int] NULL,
[TimeStamp] [datetime] NULL,
[EmployeeID] [int] NULL,
[ScreenID] [int] NULL,
[ContactPersonEmail] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContactPersonName2] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContactPersonRelationType2] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContactPersonBirthDate2] [datetime] NULL,
[ContactPersonPhoneNumberGeneral2] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContactPersonPhoneNumberMobile2] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContactPersonPhoneNumberWork2] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContactPersonEmail2] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContactPersonName3] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContactPersonRelationType3] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContactPersonBirthDate3] [datetime] NULL,
[ContactPersonPhoneNumberGeneral3] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContactPersonPhoneNumberMobile3] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContactPersonPhoneNumberWork3] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContactPersonEmail3] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UsesPatientPortal] [bit] NOT NULL CONSTRAINT [DF_Client_UsesPatientPortal] DEFAULT ((0)),
[OriginalClientID] [int] NULL,
[AddressGPForZorgmail] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TemporaryStreetName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TemporaryNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TemporaryPostalCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TemporaryCity] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TemporaryCountry] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PartnerName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PartnerMiddleName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HuisnummerToevoeging] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Postbus] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VisitNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClientIsAnonymous] [bit] NOT NULL CONSTRAINT [DF_Client_IsAnonymous] DEFAULT ((0)),
[HasNoCivilServiceNumber] [bit] NOT NULL CONSTRAINT [DF__Client__HasCivil__2AF6ED82] DEFAULT ((0))
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[Client_InsertUpdate] 
   ON  [dbo].[Client]  
   AFTER UPDATE, INSERT 
AS  
BEGIN 
 
	SET NOCOUNT ON; 
 
	IF (SELECT COUNT(*) FROM Inserted) = 0 
	BEGIN 
		RETURN 
	END 
 
	IF (SELECT Enabled FROM Logging) = 0 
	BEGIN 
		RETURN 
	END 
 
	-- BIJWERKEN LOGGING 
	DECLARE	@LogClientID	INT, 
	@ClientID				INT, 
	@TimeStamp				DATETIME, 
	@EmployeeID				INT, 
	@Action					VARCHAR(1), 
	@ScreenID				INT, 
	@Version				INT 
 
	SET NOCOUNT ON; 
	 
	-- Check of timestamp wel een update krijgt. Zoniet dan is er sprake 
	-- van een oude SP die de update uitvoerd. 
	IF UPDATE([TimeStamp]) 
	BEGIN 
		SELECT 
			@TimeStamp  = [TimeStamp], 
			@EmployeeID	= [EmployeeID], 
			@ScreenID	= [ScreenID] 
		FROM Inserted 
	END 
 
	SELECT @ClientID = ClientID 
	FROM Inserted 
 
	IF exists (select * from deleted) 
		SELECT @Action = 'U' 
	ELSE 
		SELECT @Action = 'I' 
	 
 
	 
	SELECT @Version = -1 --(SELECT ISNULL(MAX(Version), 0) + 1 FROM LogClient where LogClient.ClientID = @ClientID) 
 
	INSERT INTO LogClient 
			   (ClientID, 
			    [TimeStamp], 
				EmployeeID, 
				[Action], 
				ScreenID, 
				[Version]) 
		 VALUES 
			(@ClientID, 
			 @TimeStamp, 
			 @EmployeeID, 
			 @Action, 
			 @ScreenID, 
			 @Version) 
 
	SET @LogClientID = SCOPE_IDENTITY(); 
 
	DECLARE @MutClientID INT 
	 
	INSERT INTO MutClient( 
		LogClientID,	 
		ClientID, 
		CivilServiceNumber, 
		PatientNumber, 
		Gender, 
		Salutation, 
		Initials, 
		FirstName, 
		MiddleName, 
		LastName, 
		MaidenName, 
		BirthDate, 
		HealthInsuranceCompanyID, 
		InsuranceNumber, 
		StreetName, 
		Number, 
		PostalCode, 
		City, 
		Country, 
		PhoneNumberGeneral, 
		PhoneNumberMobile, 
		PhoneNumberWork, 
		Email, 
		ContactPersonName, 
		ContactPersonRelationType, 
		ContactPersonBirthDate, 
		ContactPersonPhoneNumberGeneral, 
		ContactPersonPhoneNumberMobile, 
		ContactPersonPhoneWork, 
		GeneralPractitionerName, 
		GeneralPractitionerPhoneNumber, 
		PharmacyName, 
		PharmacyPhoneNumber, 
		CivilClass, 
		CompositionHousekeeping, 
		ChildrenInHousekeeping, 
		HousingType, 
		HousingTypeComment, 
		HealthCareProvider, 
		ClientCreatedBy, 
		ClientCreatedDate, 
		ClientModifiedBy, 
		ClientModifiedDate,		 
		NationalityID, 
		LanguageID, 
		TimeStamp, 
		EmployeeID, 
		ScreenID, 
		ContactPersonEmail, 
		ContactPersonName2, 
		ContactPersonRelationType2, 
		ContactPersonBirthDate2, 
		ContactPersonPhoneNumberGeneral2, 
		ContactPersonPhoneNumberMobile2, 
		ContactPersonPhoneNumberWork2, 
		ContactPersonEmail2, 
		ContactPersonName3, 
		ContactPersonRelationType3, 
		ContactPersonBirthDate3, 
		ContactPersonPhoneNumberGeneral3, 
		ContactPersonPhoneNumberMobile3, 
		ContactPersonPhoneNumberWork3, 
		ContactPersonEmail3, 
		UsesPatientPortal, 
		OriginalClientID, 
		AddressGPForZorgmail, 
		TemporaryStreetName, 
		TemporaryNumber, 
		TemporaryPostalCode, 
		TemporaryCity, 
		TemporaryCountry, 
		PartnerName, 
		PartnerMiddleName, 
		HuisnummerToevoeging, 
		Postbus, 
        VisitNumber,
		ClientIsAnonymous,
		HasNoCivilServiceNumber
		) 
	SELECT 
		@LogClientID,
		ClientID, 
		CivilServiceNumber, 
		PatientNumber, 
		Gender, 
		Salutation, 
		Initials, 
		FirstName, 
		MiddleName, 
		LastName, 
		MaidenName, 
		BirthDate, 
		HealthInsuranceCompanyID, 
		InsuranceNumber, 
		StreetName, 
		Number, 
		PostalCode, 
		City, 
		Country, 
		PhoneNumberGeneral, 
		PhoneNumberMobile, 
		PhoneNumberWork, 
		Email, 
		ContactPersonName, 
		ContactPersonRelationType, 
		ContactPersonBirthDate, 
		ContactPersonPhoneNumberGeneral, 
		ContactPersonPhoneNumberMobile, 
		ContactPersonPhoneWork, 
		GeneralPractitionerName, 
		GeneralPractitionerPhoneNumber, 
		PharmacyName, 
		PharmacyPhoneNumber, 
		CivilClass, 
		CompositionHousekeeping, 
		ChildrenInHousekeeping, 
		HousingType, 
		HousingTypeComment, 
		HealthCareProvider, 
		ClientCreatedBy, 
		ClientCreatedDate, 
		ClientModifiedBy, 
		ClientModifiedDate,		 
		NationalityID, 
		LanguageID, 
		TimeStamp, 
		EmployeeID, 
		ScreenID, 
		ContactPersonEmail, 
		ContactPersonName2, 
		ContactPersonRelationType2, 
		ContactPersonBirthDate2, 
		ContactPersonPhoneNumberGeneral2, 
		ContactPersonPhoneNumberMobile2, 
		ContactPersonPhoneNumberWork2, 
		ContactPersonEmail2, 
		ContactPersonName3, 
		ContactPersonRelationType3, 
		ContactPersonBirthDate3, 
		ContactPersonPhoneNumberGeneral3, 
		ContactPersonPhoneNumberMobile3, 
		ContactPersonPhoneNumberWork3, 
		ContactPersonEmail3, 
		UsesPatientPortal, 
		OriginalClientID, 
		AddressGPForZorgmail, 
		TemporaryStreetName, 
		TemporaryNumber, 
		TemporaryPostalCode, 
		TemporaryCity, 
		TemporaryCountry, 
		PartnerName, 
		PartnerMiddleName, 
		HuisnummerToevoeging, 
		Postbus, 
        VisitNumber,
		ClientIsAnonymous,
		HasNoCivilServiceNumber
	FROM Inserted 
 
	SET @MutClientID = SCOPE_IDENTITY(); 
 
END

GO
ALTER TABLE [dbo].[Client] ADD CONSTRAINT [PK_Client] PRIMARY KEY CLUSTERED  ([ClientID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Client_BirthDate_Includes] ON [dbo].[Client] ([BirthDate]) INCLUDE ([CivilServiceNumber], [ClientID], [Gender], [HealthInsuranceCompanyID], [InsuranceNumber], [MaidenName], [PatientNumber]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Client_BirthDate_Includes_ClientID] ON [dbo].[Client] ([BirthDate]) INCLUDE ([ClientID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_ClientBSN] ON [dbo].[Client] ([CivilServiceNumber]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Client] ADD CONSTRAINT [IX_Client] UNIQUE NONCLUSTERED  ([ClientID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Client_NameFields] ON [dbo].[Client] ([ClientID]) INCLUDE ([FirstName], [Initials], [LastName], [MiddleName]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Client_PatientNumber] ON [dbo].[Client] ([PatientNumber]) ON [PRIMARY]
GO
