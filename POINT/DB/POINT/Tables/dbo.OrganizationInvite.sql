CREATE TABLE [dbo].[OrganizationInvite]
(
[OrganizationInviteID] [int] NOT NULL IDENTITY(1, 1),
[FlowInstanceID] [int] NOT NULL,
[SendingPhaseDefinitionID] [int] NULL,
[RecievingPhaseDefinitionID] [int] NOT NULL,
[OrganizationID] [int] NOT NULL,
[DepartmentID] [int] NOT NULL,
[InviteStatus] [int] NOT NULL,
[InviteOrder] [int] NOT NULL CONSTRAINT [DF_OrganizationInvite_InviteOrder] DEFAULT ((0)),
[InviteCreated] [datetime] NOT NULL,
[InviteSend] [datetime] NULL,
[NeedsAcknowledgement] [bit] NOT NULL CONSTRAINT [DF_OrganizationInvite_NeedsAcknowledge] DEFAULT ((0)),
[InviteType] [int] NOT NULL CONSTRAINT [DF_OrganizationInvite_InviteType] DEFAULT ((0)),
[HandlingBy] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HandlingRemark] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HandlingDateTime] [datetime] NULL,
[CreatedByID] [int] NOT NULL CONSTRAINT [DF_OrganizationInvite_CreatedByID] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[OrganizationInvite] ADD CONSTRAINT [PK_OrganizationInvite] PRIMARY KEY CLUSTERED  ([OrganizationInviteID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_OrganizationInvite_FlowInstanceID] ON [dbo].[OrganizationInvite] ([FlowInstanceID]) INCLUDE ([InviteStatus]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[OrganizationInvite] ADD CONSTRAINT [FK_OrganizationInvite_Department] FOREIGN KEY ([DepartmentID]) REFERENCES [dbo].[Department] ([DepartmentID])
GO
ALTER TABLE [dbo].[OrganizationInvite] ADD CONSTRAINT [FK_OrganizationInvite_FlowInstance] FOREIGN KEY ([FlowInstanceID]) REFERENCES [dbo].[FlowInstance] ([FlowInstanceID])
GO
ALTER TABLE [dbo].[OrganizationInvite] ADD CONSTRAINT [FK_OrganizationInvite_Organization] FOREIGN KEY ([OrganizationID]) REFERENCES [dbo].[Organization] ([OrganizationID])
GO
ALTER TABLE [dbo].[OrganizationInvite] ADD CONSTRAINT [FK_OrganizationInvite_RecievingPhaseDefinition] FOREIGN KEY ([RecievingPhaseDefinitionID]) REFERENCES [dbo].[PhaseDefinition] ([PhaseDefinitionID])
GO
ALTER TABLE [dbo].[OrganizationInvite] ADD CONSTRAINT [FK_OrganizationInvite_SendingPhaseDefinition] FOREIGN KEY ([SendingPhaseDefinitionID]) REFERENCES [dbo].[PhaseDefinition] ([PhaseDefinitionID])
GO
