CREATE TABLE [dbo].[FlowType]
(
[FlowTypeID] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmployeeID] [int] NULL,
[Timestamp] [datetime] NULL,
[ScreenName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FlowType] ADD CONSTRAINT [PK_dbo.FlowType] PRIMARY KEY CLUSTERED  ([FlowTypeID]) ON [PRIMARY]
GO
