CREATE TABLE [dbo].[DepartmentCapacity]
(
[DepartmentCapacityID] [int] NOT NULL IDENTITY(1, 1),
[DepartmentID] [int] NULL,
[DepartmentName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CapacityDepartment] [int] NULL,
[CapacityDepartmentDate] [datetime] NULL,
[CapacityDepartmentNext] [int] NULL,
[CapacityDepartmentNextDate] [datetime] NULL,
[Information] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AdjustmentCapacityDate] [datetime] NULL,
[UserID] [int] NULL,
[CapacityDepartment3] [int] NULL,
[CapacityDepartment3Date] [datetime] NULL,
[CapacityDepartment4] [int] NULL,
[CapacityDepartment4Date] [datetime] NULL,
[CapacityDepartmentHomeCare] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CapacityDepartmentNextHomeCare] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CapacityDepartment3HomeCare] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CapacityDepartment4HomeCare] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DepartmentCapacity] ADD CONSTRAINT [PK_DepartmentCapacity] PRIMARY KEY CLUSTERED  ([DepartmentCapacityID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_DepartmentCapacity_DepartmentAndDate] ON [dbo].[DepartmentCapacity] ([DepartmentID], [CapacityDepartmentDate]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DepartmentCapacity] ADD CONSTRAINT [FK_DepartmentCapacity_Department] FOREIGN KEY ([DepartmentID]) REFERENCES [dbo].[Department] ([DepartmentID])
GO
