CREATE TABLE [dbo].[AfterCareCategory]
(
[AfterCareCategoryID] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Abbreviation] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Sortorder] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AfterCareCategory] ADD CONSTRAINT [PK_AfterCareCategory] PRIMARY KEY CLUSTERED  ([AfterCareCategoryID]) ON [PRIMARY]
GO
