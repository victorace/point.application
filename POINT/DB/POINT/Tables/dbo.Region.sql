CREATE TABLE [dbo].[Region]
(
[RegionID] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CapacityBeds] [bit] NOT NULL CONSTRAINT [DF__Region__Capacity__267B85D1] DEFAULT ((0)),
[ModifiedTimeStamp] [datetime] NULL,
[CapacityBedsPublic] [bit] NOT NULL CONSTRAINT [DF__Region__Capacity__76390C80] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Region] ADD CONSTRAINT [PK_Region] PRIMARY KEY CLUSTERED  ([RegionID]) ON [PRIMARY]
GO
