/*
This is the script that will be used to migrate the database from revision 15273 to revision 15281.

You can customize the script, and your edits will be used in deployment.
The following objects will be affected:
  dbo.FlowInstance, dbo.PhaseInstance
*/

SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
PRINT N'Dropping foreign keys from [dbo].[PhaseInstance]'
GO
ALTER TABLE [dbo].[PhaseInstance] DROP CONSTRAINT [FK_dbo.PhaseInstance_dbo.Department_DepartmentID]
GO
ALTER TABLE [dbo].[PhaseInstance] DROP CONSTRAINT [FK_dbo.PhaseInstance_dbo.FlowInstance_FlowInstanceID]
GO
ALTER TABLE [dbo].[PhaseInstance] DROP CONSTRAINT [FK_dbo.PhaseInstance_dbo.Organization_OrganizationID]
GO
ALTER TABLE [dbo].[PhaseInstance] DROP CONSTRAINT [FK_dbo.PhaseInstance_dbo.PhaseDefinition_PhaseDefinitionID]
GO
ALTER TABLE [dbo].[PhaseInstance] DROP CONSTRAINT [FK_dbo.PhaseInstance_dbo.Employee_RealizedByEmployeeID]
GO
ALTER TABLE [dbo].[PhaseInstance] DROP CONSTRAINT [FK_dbo.PhaseInstance_dbo.Employee_RequestedByEmployeeID]
GO
ALTER TABLE [dbo].[PhaseInstance] DROP CONSTRAINT [FK_dbo.PhaseInstance_dbo.Employee_StartedByEmployeeID]
GO
PRINT N'Dropping foreign keys from [dbo].[FormSetVersion]'
GO
ALTER TABLE [dbo].[FormSetVersion] DROP CONSTRAINT [FK_FormSetVersion_PhaseInstance]
GO
PRINT N'Dropping constraints from [dbo].[PhaseInstance]'
GO
ALTER TABLE [dbo].[PhaseInstance] DROP CONSTRAINT [PK_dbo.PhaseInstance]
GO
PRINT N'Dropping constraints from [dbo].[PhaseInstance]'
GO
ALTER TABLE [dbo].[PhaseInstance] DROP CONSTRAINT [DF_PhaseInstance_Inactive]
GO
PRINT N'Dropping index [IX_DepartmentID] from [dbo].[PhaseInstance]'
GO
DROP INDEX [IX_DepartmentID] ON [dbo].[PhaseInstance]
GO
PRINT N'Dropping index [IX_FlowInstanceIDCancelled_PhaseInstanceID] from [dbo].[PhaseInstance]'
GO
DROP INDEX [IX_FlowInstanceIDCancelled_PhaseInstanceID] ON [dbo].[PhaseInstance]
GO
PRINT N'Dropping index [IX_Status_PhaseInstanceID] from [dbo].[PhaseInstance]'
GO
DROP INDEX [IX_Status_PhaseInstanceID] ON [dbo].[PhaseInstance]
GO
PRINT N'Dropping index [IX_PhaseDefinitionID] from [dbo].[PhaseInstance]'
GO
DROP INDEX [IX_PhaseDefinitionID] ON [dbo].[PhaseInstance]
GO
PRINT N'Dropping index [DepartmentIDCancelled_FlowInstanceID] from [dbo].[PhaseInstance]'
GO
DROP INDEX [DepartmentIDCancelled_FlowInstanceID] ON [dbo].[PhaseInstance]
GO
PRINT N'Dropping index [IX_FlowInstanceID] from [dbo].[PhaseInstance]'
GO
DROP INDEX [IX_FlowInstanceID] ON [dbo].[PhaseInstance]
GO
PRINT N'Dropping index [IX_FlowInstanceIDDepartmentIDCancelled_OrganizationID] from [dbo].[PhaseInstance]'
GO
DROP INDEX [IX_FlowInstanceIDDepartmentIDCancelled_OrganizationID] ON [dbo].[PhaseInstance]
GO
PRINT N'Dropping index [IX_OrganizationID] from [dbo].[PhaseInstance]'
GO
DROP INDEX [IX_OrganizationID] ON [dbo].[PhaseInstance]
GO
PRINT N'Dropping index [IX_StartedByEmployeeID] from [dbo].[PhaseInstance]'
GO
DROP INDEX [IX_StartedByEmployeeID] ON [dbo].[PhaseInstance]
GO
PRINT N'Dropping index [IX_RequestedByEmployeeID] from [dbo].[PhaseInstance]'
GO
DROP INDEX [IX_RequestedByEmployeeID] ON [dbo].[PhaseInstance]
GO
PRINT N'Dropping index [IX_RealizedByEmployeeID] from [dbo].[PhaseInstance]'
GO
DROP INDEX [IX_RealizedByEmployeeID] ON [dbo].[PhaseInstance]
GO
PRINT N'Altering [dbo].[FlowInstance]'
GO
ALTER TABLE [dbo].[FlowInstance] ADD
[StartedByDepartmentID] [int] NULL,
[StartedByOrganizationID] [int] NULL,
[StartedByEmployeeID] [int] NULL
GO
PRINT N'Creating index [FlowInstance_FlowDefinitionIDDeletedStartedByDepartmentID] on [dbo].[FlowInstance]'
GO
CREATE NONCLUSTERED INDEX [FlowInstance_FlowDefinitionIDDeletedStartedByDepartmentID] ON [dbo].[FlowInstance] ([FlowDefinitionID], [StartedByDepartmentID], [Deleted]) ON [PRIMARY]
GO
PRINT N'Creating index [FlowInstance_FlowDefinitionIDDeletedStartedByOrganizationID] on [dbo].[FlowInstance]'
GO
CREATE NONCLUSTERED INDEX [FlowInstance_FlowDefinitionIDDeletedStartedByOrganizationID] ON [dbo].[FlowInstance] ([FlowDefinitionID], [StartedByOrganizationID], [Deleted]) ON [PRIMARY]
GO
PRINT N'Creating index [FlowInstance_FlowDefinitionIDDeletedStartedByEmployeeID] on [dbo].[FlowInstance]'
GO
CREATE NONCLUSTERED INDEX [FlowInstance_FlowDefinitionIDDeletedStartedByEmployeeID] ON [dbo].[FlowInstance] ([FlowDefinitionID], [StartedByEmployeeID], [Deleted]) ON [PRIMARY]
GO
PRINT N'Rebuilding [dbo].[PhaseInstance]'
GO
CREATE TABLE [dbo].[RG_Recovery_1_PhaseInstance]
(
[PhaseInstanceID] [int] NOT NULL IDENTITY(1, 1),
[PhaseDefinitionID] [int] NOT NULL,
[FlowInstanceID] [int] NOT NULL,
[Status] [int] NOT NULL,
[PhaseActiveInPreviousStep] [int] NOT NULL,
[ReceivingOrganizationID] [int] NULL,
[ReceivingDepartmentID] [int] NULL,
[StartProcessDate] [datetime] NOT NULL,
[StartedByEmployeeID] [int] NOT NULL,
[RequestDate] [datetime] NULL,
[RequestedByEmployeeID] [int] NULL,
[RealizedDate] [datetime] NULL,
[RealizedByEmployeeID] [int] NULL,
[EmployeeID] [int] NULL,
[Timestamp] [datetime] NULL,
[ScreenName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Cancelled] [bit] NOT NULL CONSTRAINT [DF_PhaseInstance_Inactive] DEFAULT ((0))
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_1_PhaseInstance] ON
GO
INSERT INTO [dbo].[RG_Recovery_1_PhaseInstance]([PhaseInstanceID], [PhaseDefinitionID], [FlowInstanceID], [Status], [PhaseActiveInPreviousStep], [StartProcessDate], [StartedByEmployeeID], [RequestDate], [RequestedByEmployeeID], [RealizedDate], [RealizedByEmployeeID], [EmployeeID], [Timestamp], [ScreenName], [Cancelled], [ReceivingOrganizationID], [ReceivingDepartmentID]) SELECT [PhaseInstanceID], [PhaseDefinitionID], [FlowInstanceID], [Status], [PhaseActiveInPreviousStep], [StartProcessDate], [StartedByEmployeeID], [RequestDate], [RequestedByEmployeeID], [RealizedDate], [RealizedByEmployeeID], [EmployeeID], [Timestamp], [ScreenName], [Cancelled], [OrganizationID], [DepartmentID] FROM [dbo].[PhaseInstance]
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_1_PhaseInstance] OFF
GO
DECLARE @idVal BIGINT
SELECT @idVal = IDENT_CURRENT(N'[dbo].[PhaseInstance]')
IF @idVal IS NOT NULL
    DBCC CHECKIDENT(N'[dbo].[RG_Recovery_1_PhaseInstance]', RESEED, @idVal)
GO
DROP TABLE [dbo].[PhaseInstance]
GO
EXEC sp_rename N'[dbo].[RG_Recovery_1_PhaseInstance]', N'PhaseInstance', N'OBJECT'
GO
PRINT N'Creating primary key [PK_dbo.PhaseInstance] on [dbo].[PhaseInstance]'
GO
ALTER TABLE [dbo].[PhaseInstance] ADD CONSTRAINT [PK_dbo.PhaseInstance] PRIMARY KEY CLUSTERED  ([PhaseInstanceID]) ON [PRIMARY]
GO
PRINT N'Creating index [IX_FlowInstanceIDCancelled_PhaseInstanceID] on [dbo].[PhaseInstance]'
GO
CREATE NONCLUSTERED INDEX [IX_FlowInstanceIDCancelled_PhaseInstanceID] ON [dbo].[PhaseInstance] ([FlowInstanceID], [Cancelled]) INCLUDE ([PhaseInstanceID]) ON [PRIMARY]
GO
PRINT N'Creating index [IX_DepartmentID] on [dbo].[PhaseInstance]'
GO
CREATE NONCLUSTERED INDEX [IX_DepartmentID] ON [dbo].[PhaseInstance] ([ReceivingDepartmentID]) INCLUDE ([PhaseInstanceID]) ON [PRIMARY]
GO
PRINT N'Creating index [IX_OrganizationIDCancelled_PhaseInstanceID] on [dbo].[PhaseInstance]'
GO
CREATE NONCLUSTERED INDEX [IX_OrganizationIDCancelled_PhaseInstanceID] ON [dbo].[PhaseInstance] ([ReceivingOrganizationID], [Cancelled]) INCLUDE ([PhaseInstanceID]) ON [PRIMARY]
GO
PRINT N'Creating index [IX_Status_PhaseInstanceID] on [dbo].[PhaseInstance]'
GO
CREATE NONCLUSTERED INDEX [IX_Status_PhaseInstanceID] ON [dbo].[PhaseInstance] ([Status]) INCLUDE ([PhaseInstanceID]) ON [PRIMARY]
GO
PRINT N'Creating index [IX_PhaseDefinitionID] on [dbo].[PhaseInstance]'
GO
CREATE NONCLUSTERED INDEX [IX_PhaseDefinitionID] ON [dbo].[PhaseInstance] ([PhaseDefinitionID]) ON [PRIMARY]
GO
PRINT N'Creating index [IX_FlowInstanceID] on [dbo].[PhaseInstance]'
GO
CREATE NONCLUSTERED INDEX [IX_FlowInstanceID] ON [dbo].[PhaseInstance] ([FlowInstanceID]) ON [PRIMARY]
GO
PRINT N'Creating index [IX_FlowInstanceIDDepartmentIDCancelled_OrganizationID] on [dbo].[PhaseInstance]'
GO
CREATE NONCLUSTERED INDEX [IX_FlowInstanceIDDepartmentIDCancelled_OrganizationID] ON [dbo].[PhaseInstance] ([FlowInstanceID], [ReceivingDepartmentID], [Cancelled]) INCLUDE ([ReceivingOrganizationID]) ON [PRIMARY]
GO
PRINT N'Creating index [DepartmentIDCancelled_FlowInstanceID] on [dbo].[PhaseInstance]'
GO
CREATE NONCLUSTERED INDEX [DepartmentIDCancelled_FlowInstanceID] ON [dbo].[PhaseInstance] ([ReceivingDepartmentID], [Cancelled]) INCLUDE ([FlowInstanceID]) ON [PRIMARY]
GO
PRINT N'Creating index [IX_StartedByEmployeeID] on [dbo].[PhaseInstance]'
GO
CREATE NONCLUSTERED INDEX [IX_StartedByEmployeeID] ON [dbo].[PhaseInstance] ([StartedByEmployeeID]) ON [PRIMARY]
GO
PRINT N'Creating index [IX_RequestedByEmployeeID] on [dbo].[PhaseInstance]'
GO
CREATE NONCLUSTERED INDEX [IX_RequestedByEmployeeID] ON [dbo].[PhaseInstance] ([RequestedByEmployeeID]) ON [PRIMARY]
GO
PRINT N'Creating index [IX_RealizedByEmployeeID] on [dbo].[PhaseInstance]'
GO
CREATE NONCLUSTERED INDEX [IX_RealizedByEmployeeID] ON [dbo].[PhaseInstance] ([RealizedByEmployeeID]) ON [PRIMARY]
GO
PRINT N'Adding foreign keys to [dbo].[PhaseInstance]'
GO
ALTER TABLE [dbo].[PhaseInstance] ADD CONSTRAINT [FK_dbo.PhaseInstance_dbo.FlowInstance_FlowInstanceID] FOREIGN KEY ([FlowInstanceID]) REFERENCES [dbo].[FlowInstance] ([FlowInstanceID])
GO
ALTER TABLE [dbo].[PhaseInstance] ADD CONSTRAINT [FK_dbo.PhaseInstance_dbo.PhaseDefinition_PhaseDefinitionID] FOREIGN KEY ([PhaseDefinitionID]) REFERENCES [dbo].[PhaseDefinition] ([PhaseDefinitionID])
GO
ALTER TABLE [dbo].[PhaseInstance] ADD CONSTRAINT [FK_dbo.PhaseInstance_dbo.Employee_RealizedByEmployeeID] FOREIGN KEY ([RealizedByEmployeeID]) REFERENCES [dbo].[Employee] ([EmployeeID])
GO
ALTER TABLE [dbo].[PhaseInstance] ADD CONSTRAINT [FK_dbo.PhaseInstance_dbo.Department_DepartmentID] FOREIGN KEY ([ReceivingDepartmentID]) REFERENCES [dbo].[Department] ([DepartmentID])
GO
ALTER TABLE [dbo].[PhaseInstance] ADD CONSTRAINT [FK_dbo.PhaseInstance_dbo.Organization_OrganizationID] FOREIGN KEY ([ReceivingOrganizationID]) REFERENCES [dbo].[Organization] ([OrganizationID])
GO
ALTER TABLE [dbo].[PhaseInstance] ADD CONSTRAINT [FK_dbo.PhaseInstance_dbo.Employee_RequestedByEmployeeID] FOREIGN KEY ([RequestedByEmployeeID]) REFERENCES [dbo].[Employee] ([EmployeeID])
GO
ALTER TABLE [dbo].[PhaseInstance] ADD CONSTRAINT [FK_dbo.PhaseInstance_dbo.Employee_StartedByEmployeeID] FOREIGN KEY ([StartedByEmployeeID]) REFERENCES [dbo].[Employee] ([EmployeeID])
GO
PRINT N'Adding foreign keys to [dbo].[FormSetVersion]'
GO
ALTER TABLE [dbo].[FormSetVersion] ADD CONSTRAINT [FK_FormSetVersion_PhaseInstance] FOREIGN KEY ([PhaseInstanceID]) REFERENCES [dbo].[PhaseInstance] ([PhaseInstanceID])
