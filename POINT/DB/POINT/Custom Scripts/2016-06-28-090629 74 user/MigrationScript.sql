/*
This is the migration script to update the database with the set of uncommitted changes you selected.

You can customize the script, and your edits will be used in deployment.
The following objects will be affected:
  dbo.PointException
*/

SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
PRINT N'Dropping constraints from [dbo].[PointException]'
GO
ALTER TABLE [dbo].[PointException] DROP CONSTRAINT [PK_PointException]
GO
PRINT N'Rebuilding [dbo].[PointException]'
GO
CREATE TABLE [dbo].[RG_Recovery_1_PointException]
(
[PointExceptionID] [int] NOT NULL IDENTITY(1, 1),
[TransferID] [int] NULL,
[EmployeeID] [int] NULL,
[EmployeeUserName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MachineName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WindowsIdentity] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CurrentUrl] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QueryString] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StackTrace] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Message] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[InnerException] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Title] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PostData] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TimeStamp] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_1_PointException] ON
GO
INSERT INTO [dbo].[RG_Recovery_1_PointException]([PointExceptionID], [TransferID], [EmployeeID], [EmployeeUserName], [MachineName], [WindowsIdentity], [CurrentUrl], [QueryString], [StackTrace], [Message], [InnerException], [Title]) SELECT [PointExceptionID], [TransferID], [EmployeeID], [EmployeeUserName], [MachineName], [WindowsIdentity], [CurrentUrl], [QueryString], [StackTrace], [Message], [InnerException], [Title] FROM [dbo].[PointException]
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_1_PointException] OFF
GO
DECLARE @idVal BIGINT
SELECT @idVal = IDENT_CURRENT(N'[dbo].[PointException]')
IF @idVal IS NOT NULL
    DBCC CHECKIDENT(N'[dbo].[RG_Recovery_1_PointException]', RESEED, @idVal)
GO
DROP TABLE [dbo].[PointException]
GO
EXEC sp_rename N'[dbo].[RG_Recovery_1_PointException]', N'PointException', N'OBJECT'
GO
PRINT N'Creating primary key [PK_PointException] on [dbo].[PointException]'
GO
UPDATE dbo.PointException SET [TimeStamp] = GETDATE() WHERE [TimeStamp] IS NULL
ALTER TABLE dbo.PointException ALTER COLUMN [TimeStamp] [datetime] NOT NULL

ALTER TABLE [dbo].[PointException] ADD CONSTRAINT [PK_PointException] PRIMARY KEY CLUSTERED  ([PointExceptionID]) ON [PRIMARY]
