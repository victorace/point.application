ALTER TABLE dbo.FlowFieldValue 
	ADD FlowInstanceID INT NULL
GO
UPDATE
    dbo.FlowFieldValue
SET
    dbo.FlowFieldValue.FlowInstanceID = fi.FlowInstanceID
FROM
    dbo.FlowFieldValue v
INNER JOIN
    dbo.FlowInstance fi
ON
    v.TransferID = fi.TransferID
GO
ALTER TABLE dbo.FlowFieldValue 
	ALTER COLUMN FlowInstanceID INT NOT NULL
