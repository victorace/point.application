SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[InsertClientQRY]
             @MESSAGE_ID                       varchar(50),
             @PARENT_ID                        varchar(50),
             @PatientID						   varchar(50),
			 @MsgID_1						   varchar(50),
			 @InterfaceNumber                  int
AS
BEGIN
	SET NOCOUNT ON;

    DECLARE @MsgID AS VARCHAR(50)
    DECLARE @Status AS VARCHAR(1)
    DECLARE @tableNameQRY AS VARCHAR(50)
    DECLARE @tableNameQuery AS VARCHAR(50)
    DECLARE @SQLString nvarchar(500)

    SET @Status = 'W'

    SET @tableNameQRY = N'ClientQRY_' + LTRIM(STR(@InterfaceNumber))
    SET @tableNameQuery = N'ClientQuery_' + LTRIM(STR(@InterfaceNumber))

    SET @SQLString = N'INSERT INTO [' + @tableNameQRY + '] (MESSAGE_ID, PARENT_ID, PatientID, MsgID, MsgID_1) VALUES (@MESSAGE_ID, @PARENT_ID, @PatientID, @MESSAGE_ID, @MsgID_1);'

    EXECUTE sp_executesql @SQLString, N'@MESSAGE_ID VARCHAR(50), @PARENT_ID VARCHAR(50), @PatientID VARCHAR(50), @MsgID VARCHAR(50), @MsgID_1 VARCHAR(50)', @MESSAGE_ID, @PARENT_ID, @PatientID, @MsgID, @MsgID_1

    SET @SQLString = N'INSERT INTO [' + @tableNameQuery + '] (MESSAGE_ID, STATUS) VALUES (@MESSAGE_ID, @Status);'

    EXECUTE sp_executesql @SQLString, N'@MESSAGE_ID VARCHAR(50), @Status VARCHAR(1)', @MESSAGE_ID, @Status

END
GO
