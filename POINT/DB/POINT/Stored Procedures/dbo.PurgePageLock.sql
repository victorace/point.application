SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[PurgePageLock] 
AS 
BEGIN 
	-- SET NOCOUNT ON added to prevent extra result sets from 
	-- interfering with SELECT statements. 
	SET NOCOUNT ON; 
 
	DECLARE @TimeoutInMinutes As INT 
	SET @TimeoutInMinutes = 5 
 
	DELETE PageLock FROM PageLock p 
		JOIN Employee ON p.EmployeeID = Employee.EmployeeID 
		JOIN Department ON Employee.Departmentid=Department.DepartmentID 
		JOIN Location ON Department.Locationid=Location.LocationID 
		JOIN Organization ON Location.Organizationid=Organization.OrganizationID 
		WHERE DATEDIFF(minute, DATEADD(minute, ISNULL(Organization.PageLockTimeout, 0), p.TimeStamp), GETDATE()) > @TimeoutInMinutes 
 
	IF @@ERROR <> 0    
		BEGIN   
			-- Return 0 to the calling program to indicate failure.   
			RETURN 0;   
		END   
	ELSE   
		BEGIN   
			-- Return 1 to the calling program to indicate success.   
			RETURN 1;   
		END;  
END 
GO
