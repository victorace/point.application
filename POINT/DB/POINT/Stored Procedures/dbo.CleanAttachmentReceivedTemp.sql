SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

 
CREATE procedure [dbo].[CleanAttachmentReceivedTemp] 
as 
begin 
 
	set nocount on; 

	delete from dbo.AttachmentReceivedTemp where datediff(dd, [Timestamp], getdate()) > 7; 
 
	if @@error <> 0    
		begin   
			-- Return 0 to the calling program to indicate failure.   
			return 0;   
		end;   
	else   
		begin   
			-- Return 1 to the calling program to indicate success.   
			return 1;   
		end;   
end; 

GO
