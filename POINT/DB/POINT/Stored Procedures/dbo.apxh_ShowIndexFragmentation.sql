SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[apxh_ShowIndexFragmentation]
	@level float = 30.0
AS
BEGIN
	SET NOCOUNT ON;

	SELECT convert(nvarchar(50),t.name) As TableName,
	-- s.*
	s.IndexDescription, s.IndexDepth, s.IndexLevel, s.AverageFragmentation, s.FragmentCount, s.AverageFragmentSize, s.TablePageCount
	FROM sys.tables t
	CROSS APPLY apxh_sys_PhysicalIndexStatistics_Wrapper(DB_ID(), object_id, NULL, NULL, NULL) s  
	WHERE AverageFragmentation > @level
	ORDER BY AverageFragmentation

END
GO
