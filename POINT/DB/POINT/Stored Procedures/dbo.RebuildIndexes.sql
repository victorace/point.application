SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
CREATE PROCEDURE [dbo].[RebuildIndexes] 
AS 
BEGIN  
	SET NOCOUNT ON 
	
	DECLARE @tableName NVARCHAR(500); 
	DECLARE @indexName NVARCHAR(500); 
	DECLARE @indexType NVARCHAR(55); 
	DECLARE @minimumfragmentation INT = 10; 
	DECLARE @percentFragment DECIMAL(11, 2); 
 
	DECLARE FragmentedTableList CURSOR
	FOR
		SELECT  OBJECT_NAME(ind.object_id) AS TableName ,
				ind.name AS IndexName ,
				indexstats.index_type_desc AS IndexType ,
				indexstats.avg_fragmentation_in_percent
		FROM    sys.dm_db_index_physical_stats(DB_ID(), NULL, NULL, NULL, NULL) indexstats
				INNER JOIN sys.indexes ind ON ind.object_id = indexstats.object_id
											  AND ind.index_id = indexstats.index_id
		WHERE   indexstats.avg_fragmentation_in_percent > @minimumfragmentation
				AND ind.name IS NOT NULL
		ORDER BY indexstats.avg_fragmentation_in_percent DESC; 
 
	OPEN FragmentedTableList; 
	FETCH NEXT FROM FragmentedTableList  
		INTO @tableName, @indexName, @indexType, @percentFragment; 
 
	WHILE @@FETCH_STATUS = 0
		BEGIN 
			IF ( @percentFragment <= 30 )
				BEGIN 
					PRINT 'Reorganizing ' + @indexName + ' on table ' + @tableName + ' (' + CAST(@percentFragment AS NVARCHAR(50)) + '% fragmented)'; 

					EXEC( 'ALTER INDEX [' +  @indexName + '] ON ' + @tableName + ' REBUILD; '); 
				END; 
			ELSE
				BEGIN 
					PRINT 'Rebuilding ' + @indexName + ' on table ' + @tableName + ' (' + CAST(@percentFragment AS NVARCHAR(50)) + '% fragmented)'; 
					EXEC( 'ALTER INDEX [' +  @indexName + '] ON ' + @tableName + ' REORGANIZE; '); 
				END;  
			FETCH NEXT FROM FragmentedTableList  
			INTO @tableName, @indexName, @indexType, @percentFragment; 
		END; 

	CLOSE FragmentedTableList; 
	DEALLOCATE FragmentedTableList;
 
	IF @@ERROR <> 0    
		BEGIN   
			-- Return 0 to the calling program to indicate failure.   
			RETURN 0;   
		END   
	ELSE   
		BEGIN   
			-- Return 1 to the calling program to indicate success.   
			RETURN 1;   
		END;  
END 
 
 
GO
