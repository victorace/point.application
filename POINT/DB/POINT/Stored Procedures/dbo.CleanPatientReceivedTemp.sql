SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

 
CREATE procedure [dbo].[CleanPatientReceivedTemp] 
as 
begin 
 
	set nocount on; 

	delete from dbo.PatientReceivedTemp where datediff(dd, [Timestamp], getdate()) > 31; 
 
	if @@error <> 0    
		begin   
			-- Return 0 to the calling program to indicate failure.   
			return 0;   
		end;   
	else   
		begin   
			-- Return 1 to the calling program to indicate success.   
			return 1;   
		end;   
end; 


GO
