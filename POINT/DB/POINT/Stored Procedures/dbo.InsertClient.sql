SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[InsertClient]
	@ClientID							INT OUTPUT,
	@ClientCreatedBy					INT,
	@ClientCreatedDate					datetime,
	@CivilServiceNumber					VARCHAR(9),
	@PatientNumber						VARCHAR(50),
	@Gender								CHAR(1),
	@Salutation							VARCHAR(255),
	@Initials							VARCHAR(50),
	@FirstName							VARCHAR(255),
	@MiddleName							VARCHAR(50),
	@LastName							VARCHAR(255),
	@MaidenName							VARCHAR(255),
	@BirthDate							datetime,
	@NationalityID                      INT,
	@LanguageID                         INT,
	@HealthInsuranceCompanyID			INT,
	@InsuranceNumber					VARCHAR(50),
	@StreetName							VARCHAR(255),
	@Number								VARCHAR(50),
	@PostalCode							CHAR(6),
	@City								VARCHAR(255),
	@Country							VARCHAR(255),
	@PhoneNumberGeneral					VARCHAR(50),
	@PhoneNumberMobile					VARCHAR(50),
	@PhoneNumberWork					VARCHAR(50),
	@Email                              VARCHAR(255),	
	@ContactPersonName					VARCHAR(255),
	@ContactPersonRelationType			VARCHAR(255),
	@ContactPersonBirthDate				datetime,
	@ContactPersonPhoneNumberGeneral	VARCHAR(50),
	@ContactPersonPhoneNumberMobile		VARCHAR(50),
	@ContactPersonPhoneWork				VARCHAR(50),
	@ContactPersonEmail					VARCHAR(255),
	
	@GeneralPractitionerName			VARCHAR(255),
	@GeneralPractitionerPhoneNumber		VARCHAR(50),
	@PharmacyName                       VARCHAR(255),
	@PharmacyPhoneNumber                VARCHAR(50),
	@CivilClass							VARCHAR(255),
	@CompositionHousekeeping			VARCHAR(255),
	@ChildrenInHousekeeping				INT,
	@HousingType						VARCHAR(255),
	@HealthCareProvider					VARCHAR(255),

	@ContactPersonName2					VARCHAR(255),
	@ContactPersonRelationType2			VARCHAR(255),
	@ContactPersonBirthDate2			datetime,
	@ContactPersonPhoneNumberGeneral2	VARCHAR(50),
	@ContactPersonPhoneNumberMobile2	VARCHAR(50),
	@ContactPersonPhoneNumberWork2		VARCHAR(50),
	@ContactPersonEmail2				VARCHAR(255),
	
	@ContactPersonName3					VARCHAR(255),
	@ContactPersonRelationType3			VARCHAR(255),
	@ContactPersonBirthDate3			datetime,
	@ContactPersonPhoneNumberGeneral3	VARCHAR(50),
	@ContactPersonPhoneNumberMobile3	VARCHAR(50),
	@ContactPersonPhoneNumberWork3		VARCHAR(50),
	@ContactPersonEmail3				VARCHAR(255),
	@UsesPatientPortal                  BIT,
	@AddressGPForZorgmail				VARCHAR(255) = NULL,
	
	@TimeStamp							AS DATETIME,
	@EmployeeID							INT,	
	@ScreenID							AS INT

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO [dbo].[Client]
			   ([ClientCreatedBy]
			   ,[ClientCreatedDate]
			   ,[CivilServiceNumber]
			   ,[PatientNumber]
			   ,[Salutation]
			   ,[Gender]
			   ,[Initials]
			   ,[FirstName]
			   ,[MiddleName]
			   ,[LastName]
			   ,[MaidenName]
			   ,[BirthDate]
			   ,[NationalityID]
			   ,[LanguageID]
			   ,[HealthInsuranceCompanyID]
			   ,[InsuranceNumber]
			   ,[StreetName]
			   ,[Number]
			   ,[PostalCode]
			   ,[City]
			   ,[Country]
			   ,[PhoneNumberGeneral]
			   ,[PhoneNumberMobile]
			   ,[PhoneNumberWork]
			   ,[Email]
			   ,[ContactPersonName]
			   ,[ContactPersonRelationType]
			   ,[ContactPersonBirthDate]
			   ,[ContactPersonPhoneNumberGeneral]
			   ,[ContactPersonPhoneNumberMobile]
			   ,[ContactPersonPhoneWork]
			   ,[ContactPersonEmail]
			   ,[GeneralPractitionerName]
			   ,[GeneralPractitionerPhoneNumber]
			   ,[PharmacyName]
			   ,[PharmacyPhoneNumber]
			   ,[CivilClass]
			   ,[CompositionHousekeeping]
			   ,[ChildrenInHousekeeping]
			   ,[HousingType]
			   ,[HealthCareProvider]
	  		   ,[ContactPersonName2]		
			   ,[ContactPersonRelationType2]	
			   ,[ContactPersonBirthDate2]
			   ,[ContactPersonPhoneNumberGeneral2]
			   ,[ContactPersonPhoneNumberMobile2]
			   ,[ContactPersonPhoneNumberWork2]	
			   ,[ContactPersonEmail2]		
			   ,[ContactPersonName3]
			   ,[ContactPersonRelationType3]
			   ,[ContactPersonBirthDate3]
			   ,[ContactPersonPhoneNumberGeneral3]
			   ,[ContactPersonPhoneNumberMobile3]
			   ,[ContactPersonPhoneNumberWork3]
			   ,[ContactPersonEmail3]
			   ,[UsesPatientPortal]
			   ,[AddressGPForZorgmail] 
			   ,TimeStamp
			   ,EmployeeID	
	           ,ScreenID)	

		 VALUES
			   (@ClientCreatedBy,
			    @ClientCreatedDate,
				@CivilServiceNumber,
			    @PatientNumber,
			    @Salutation,
				@Gender,
				@Initials,
				@FirstName,
				@MiddleName,	
				@LastName,
			    @MaidenName,
			    @BirthDate,
				@NationalityID,
				@LanguageID,
			    @HealthInsuranceCompanyID,
			    @InsuranceNumber,
			    @StreetName,
			    @Number,
			    @PostalCode,
			    @City,
			    @Country,
			    @PhoneNumberGeneral,
			    @PhoneNumberMobile,
			    @PhoneNumberWork,
			    @Email,
			    @ContactPersonName,
			    @ContactPersonRelationType,
			    @ContactPersonBirthDate,
			    @ContactPersonPhoneNumberGeneral,
			    @ContactPersonPhoneNumberMobile,
			    @ContactPersonPhoneWork,
			    @ContactPersonEmail,
			    @GeneralPractitionerName,
			    @GeneralPractitionerPhoneNumber,
			    @PharmacyName,
			    @PharmacyPhoneNumber,
			    @CivilClass,
			    @CompositionHousekeeping,
			    @ChildrenInHousekeeping,
			    @HousingType,
				@HealthCareProvider,
				@ContactPersonName2,					
				@ContactPersonRelationType2,			
				@ContactPersonBirthDate2,			
				@ContactPersonPhoneNumberGeneral2,	
				@ContactPersonPhoneNumberMobile2,	
				@ContactPersonPhoneNumberWork2,			
				@ContactPersonEmail2,				
				@ContactPersonName3,						
				@ContactPersonRelationType3,			
				@ContactPersonBirthDate3,			
				@ContactPersonPhoneNumberGeneral3,	
				@ContactPersonPhoneNumberMobile3,	
				@ContactPersonPhoneNumberWork3,			
				@ContactPersonEmail3,	
				@UsesPatientPortal,
				@AddressGPForZorgmail,				
				@TimeStamp,
			    @EmployeeID	,
	            @ScreenID)

	SET @ClientID = SCOPE_IDENTITY();
END
GO
