SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[CleanViewTransferHistory]
AS
BEGIN

	SET NOCOUNT ON; 
  
	DELETE FROM ViewTransferHistory
		WHERE ViewDateTime < DATEADD(day , -7, GETDATE())
  
	IF @@ERROR <> 0    
		BEGIN   
			-- Return 0 to the calling program to indicate failure.   
			RETURN 0;   
		END   
	ELSE   
		BEGIN   
			-- Return 1 to the calling program to indicate success.   
			RETURN 1;   
		END;   

END
GO
