SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
 
CREATE PROCEDURE [dbo].[CleanSignals] 
AS 
BEGIN 
 
	SET NOCOUNT ON; 
 
	delete  
		from SignaleringDestination 
		where SignaleringID in 
		(Select SignaleringID from Signalering where Created < DATEADD(day , -4, GETDATE())); 
 
	delete  
		from Signalering where Created < DATEADD(day , -4, GETDATE()); 
 
	IF @@ERROR <> 0    
		BEGIN   
			-- Return 0 to the calling program to indicate failure.   
			RETURN 0;   
		END   
	ELSE   
		BEGIN   
			-- Return 1 to the calling program to indicate success.   
			RETURN 1;   
		END;   
END 
 
GO
