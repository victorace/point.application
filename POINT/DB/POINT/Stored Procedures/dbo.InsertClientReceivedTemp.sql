SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[InsertClientReceivedTemp]
             @GUID                             uniqueidentifier,
             @OrganisationID                   int,
             @SenderUserName                   varchar(255),
             @SendDate                         datetime,
             @PatientNumber                    varchar(50),
             @Hash                             varchar(255),
             @CivilServiceNumber               varchar(9),
             @Gender                           varchar(1),
             @Salutation                       varchar(255),
             @Initials                         varchar(50),
             @FirstName                        varchar(255),
             @MiddleName                       varchar(50),
             @LastName                         varchar(255),
             @MaidenName                       varchar(255),
             @BirthDate                        datetime,
             @StreetName                       varchar(255),
             @Number                           varchar(50),
             @PostalCode                       char(6),
             @City                             varchar(255),
             @Country                          varchar(255),
             @PhoneNumberGeneral               varchar(50),
             @PhoneNumberMobile                varchar(50),
             @PhoneNumberWork                  varchar(50),
             @ContactPersonName                varchar(255),
             @ContactPersonRelationType        varchar(255),
             @ContactPersonBirthDate           datetime,
             @ContactPersonPhoneNumberGeneral  varchar(50),
             @ContactPersonPhoneNumberMobile   varchar(50),
             @ContactPersonPhoneNumberWork     varchar(50),
             @HealthInsuranceCompanyUZOVICode  int,
             @InsuranceNumber                  varchar(50),
             @GeneralPractitionerName          varchar(255),
             @GeneralPractitionerPhoneNumber   varchar(50),
             @CivilClass                       varchar(255),
             @CompositionHousekeeping          varchar(255),
             @ChildrenInHousekeeping           int,
             @HousingType                      varchar(255),
             @HealthCareProvider               varchar(255),
             @AddressGPForZorgmail	           VARCHAR(255) = NULL
AS
BEGIN
	SET NOCOUNT ON;

	IF EXISTS (SELECT 1 FROM [dbo].[ClientReceivedTemp]
		WHERE OrganisationID = @OrganisationID and PatientNumber = @PatientNumber)
		BEGIN
		DELETE FROM [dbo].[ClientReceivedTemp]
			WHERE OrganisationID = @OrganisationID and PatientNumber = @PatientNumber
	END

INSERT INTO [dbo].[ClientReceivedTemp]
           ([GUID]
           ,[OrganisationID]
           ,[SenderUserName]
           ,[SendDate]
           ,[PatientNumber]
           ,[Hash]
           ,[CivilServiceNumber]
           ,[Gender]
           ,[Salutation]
           ,[Initials]
           ,[FirstName]
           ,[MiddleName]
           ,[LastName]
           ,[MaidenName]
           ,[BirthDate]
           ,[StreetName]
           ,[Number]
           ,[PostalCode]
           ,[City]
           ,[Country]
           ,[PhoneNumberGeneral]
           ,[PhoneNumberMobile]
           ,[PhoneNumberWork]
           ,[ContactPersonName]
           ,[ContactPersonRelationType]
           ,[ContactPersonBirthDate]
           ,[ContactPersonPhoneNumberGeneral]
           ,[ContactPersonPhoneNumberMobile]
           ,[ContactPersonPhoneNumberWork]
           ,[HealthInsuranceCompanyUZOVICode]
           ,[InsuranceNumber]
           ,[GeneralPractitionerName]
           ,[GeneralPractitionerPhoneNumber]
           ,[CivilClass]
           ,[CompositionHousekeeping]
           ,[ChildrenInHousekeeping]
           ,[HousingType]
           ,[HealthCareProvider]
           ,[AddressGPForZorgmail])
     VALUES
             (@GUID,
             @OrganisationID,
             @SenderUserName,
             @SendDate,
             @PatientNumber,
             @Hash,
             @CivilServiceNumber,
             @Gender,
             @Salutation,
             @Initials,
             @FirstName,
             @MiddleName,
             @LastName,
             @MaidenName,
             @BirthDate,
             @StreetName,
             @Number,
             @PostalCode,
             @City,
             @Country,
             @PhoneNumberGeneral,
             @PhoneNumberMobile,
             @PhoneNumberWork,
             @ContactPersonName,
             @ContactPersonRelationType,
             @ContactPersonBirthDate,
             @ContactPersonPhoneNumberGeneral,
             @ContactPersonPhoneNumberMobile,
             @ContactPersonPhoneNumberWork,
             @HealthInsuranceCompanyUZOVICode,
             @InsuranceNumber,
             @GeneralPractitionerName,
             @GeneralPractitionerPhoneNumber,
             @CivilClass,
             @CompositionHousekeeping,
             @ChildrenInHousekeeping,
             @HousingType,
             @HealthCareProvider,
             @AddressGPForZorgmail)
END
GO
