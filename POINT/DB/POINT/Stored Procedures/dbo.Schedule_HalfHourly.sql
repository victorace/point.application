SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
 
CREATE PROCEDURE [dbo].[Schedule_HalfHourly] 
AS 
 
BEGIN 
	SET NOCOUNT ON; 
 
	DECLARE @ReturnValue INT 
	SET @ReturnValue = 0 
	 
	-- Tasks to do 
	PRINT CONVERT(VARCHAR(19),GETDATE(),121) + ' PurgePageLock' 
	EXEC @ReturnValue = PurgePageLock 
	IF(@ReturnValue = 0) RETURN @ReturnValue; 
	 
	RETURN @ReturnValue 
END 
 
 
GO
