SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [dbo].[Schedule_Daily]  
as  

begin  
	set nocount on;  

	declare @ReturnValue int  
	set @ReturnValue = 0  

	-- Tasks to do  
	print convert(varchar(19),getdate(),121) + ' CleanHL7Temp'  
	exec @ReturnValue = CleanHL7Temp  
	if(@ReturnValue = 0) return @ReturnValue;  

	print convert(varchar(19),getdate(),121) + ' CleanSignals'  
	exec @ReturnValue = CleanSignals  
	if(@ReturnValue = 0) return @ReturnValue;  

	print convert(varchar(19),getdate(),121) + ' CleanAttachmentReceivedTemp'  
	exec @ReturnValue = CleanAttachmentReceivedTemp  
	if(@ReturnValue = 0) return @ReturnValue; 

	print convert(varchar(19),getdate(),121) + ' CleanPatientReceivedTemp'  
	exec @ReturnValue = CleanPatientReceivedTemp  
	if(@ReturnValue = 0) return @ReturnValue; 

	print convert(varchar(19),getdate(),121) + ' CleanServiceBusLog'  
	exec @ReturnValue = CleanServiceBusLog  
	if(@ReturnValue = 0) return @ReturnValue; 
	
	print convert(varchar(19),getdate(),121) + ' CleanUsedHashCodes'   
	exec @ReturnValue = CleanUsedHashCodes   
	if(@ReturnValue = 0) return @ReturnValue;	

	print convert(varchar(19),getdate(),121) + ' CleanViewTransferHistory'   
	exec @ReturnValue = CleanViewTransferHistory   
	if(@ReturnValue = 0) return @ReturnValue;

	return @ReturnValue  
end  




GO
