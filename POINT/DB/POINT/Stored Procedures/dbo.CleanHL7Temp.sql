SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
CREATE PROCEDURE [dbo].[CleanHL7Temp] 
AS 
BEGIN 
 
	SET NOCOUNT ON; 
 
	DECLARE @num int = 1; 
 
	WHILE @num <= 30 
	BEGIN 
		DECLARE @tableClientRecieved NVARCHAR(255) = 'ClientReceivedTempHL7_' + CAST(@num AS NVARCHAR(255)); 
		DECLARE @tableClientQuery NVARCHAR(255) = 'ClientQuery_' + CAST(@num AS NVARCHAR(255)); 
		DECLARE @tableClientQRY NVARCHAR(255) = 'ClientQRY_' + CAST(@num AS NVARCHAR(255)); 
 
		IF NOT (OBJECT_ID(@tableClientRecieved, 'U') IS NULL) 
		BEGIN 
			PRINT 'Cleaning ' + @tableClientRecieved 
			EXEC('DELETE FROM [' + @tableClientRecieved + '] WHERE (DATEDIFF(hh,[TimeStamp],GETDATE()) >= 24)') 
		END 
		IF NOT (OBJECT_ID(@tableClientQuery, 'U') IS NULL) 
		BEGIN 
			PRINT 'Cleaning ' + @tableClientQuery 
			EXEC('DELETE [' + @tableClientQuery + '] FROM [' + @tableClientQuery + '] 
			INNER JOIN [' + @tableClientQRY + '] ON  
			' + @tableClientQuery + '.MESSAGE_ID = ' + @tableClientQRY + '.MESSAGE_ID 
			WHERE (DATEDIFF(hh,[TimeStamp],GETDATE()) >= 24)') 
		END 
 
		IF NOT (OBJECT_ID(@tableClientQRY, 'U') IS NULL) 
		BEGIN 
			PRINT 'Cleaning ' + @tableClientQRY 
			EXEC('DELETE ' + @tableClientQRY + ' FROM ' + @tableClientQRY + '  
			WHERE (DATEDIFF(hh,[TimeStamp],GETDATE()) >= 24) ') 
		END 
		  
		SET @num = @num + 1; 
    END 
 
	IF @@ERROR <> 0    
		BEGIN   
			-- Return 0 to the calling program to indicate failure.   
			RETURN 0;   
		END   
	ELSE   
		BEGIN   
			-- Return 1 to the calling program to indicate success.   
			RETURN 1;   
		END;   
END 
GO
