SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


 
CREATE PROCEDURE [dbo].[CleanServiceBusLog] 
AS 
BEGIN 
 
	SET NOCOUNT ON; 

	DELETE FROM dbo.ServiceBusLog WHERE DATEDIFF(dd, SentDateTime, GETDATE()) > 31; 
 
	IF @@error <> 0    
		BEGIN   
			-- Return 0 to the calling program to indicate failure.   
			RETURN 0;   
		END;   
	ELSE   
		BEGIN   
			-- Return 1 to the calling program to indicate success.   
			RETURN 1;   
		END;   
END; 



GO
