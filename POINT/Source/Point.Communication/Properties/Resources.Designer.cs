﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Point.Communication.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Point.Communication.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;?xml version=&quot;1.0&quot; encoding=&quot;utf-8&quot; ?&gt;
        ///&lt;!-- 
        ///01-01-2018: ZV7001 toegevoegd
        ///18-09-2017: LengteTekst en GewichtTekst Uitgeschakeld voor Point 
        ///--&gt;
        ///
        ///&lt;!--
        ///Achmea:			ZV1001 t/m ZV1010
        ///ASR*:			ZV5004, ZV5005, ZV5008, ZV5009, ZV5010, ZV5011, ZV5012
        ///CZ*:			ZV2001
        ///DSW*:			ZV6001 t/m ZV6003
        ///Friesland*:		ZV1011
        ///Iptiq/Caresq*:	ZV7001
        ///Menzis*:		ZV3001 t/m ZV3003
        ///Multizorg*:		ZV5001, ZV5002, ZV5003, ZV5006, ZV5007
        ///VGZ:			ZV4001 t/m ZV4013
        ///
        ///*ASR, CZ, DSW, FRIESLAND, IPTIQ, MENZIS en MULTIZORG zijn gelijk [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string InsurerQuestions_supplierID_1 {
            get {
                return ResourceManager.GetString("InsurerQuestions_supplierID_1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;?xml version=&quot;1.0&quot; encoding=&quot;utf-8&quot; ?&gt;
        ///
        ///&lt;!--
        ///Iptiq/Caresq*:	MP7001   (TEMP FOR TEST PURPOSES ONLY!)
        ///--&gt;
        ///
        ///&lt;insurers&gt;
        ///  &lt;insurer name=&quot;MP7001&quot;&gt;
        ///    &lt;productGroup id=&quot;U-AD2&quot;&gt;
        ///      &lt;questionGroup name=&quot;Indicaties voor product 203&quot;&gt;
        ///        &lt;question type=&quot;text&quot; ipid=&quot;CAREGIVER_PHONENUMBER&quot;&gt;
        ///          &lt;questionDescription&gt;Telefoonnummer aanvrager:&lt;/questionDescription&gt;
        ///        &lt;/question&gt;
        ///        &lt;question type=&quot;radio&quot; ipid=&quot;DISEASE_TYPE&quot;&gt;
        ///          &lt;questionDescription&gt;Ziektebeeld&lt;/questionDescr [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string InsurerQuestions_supplierID_3 {
            get {
                return ResourceManager.GetString("InsurerQuestions_supplierID_3", resourceCulture);
            }
        }
    }
}
