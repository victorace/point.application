﻿using Point.Business.Logic;
using Point.Communication.Infrastructure.Helpers;
using Point.Communication.Infrastructure.Utils;
using Point.Communication.ZorgmailMailwebserviceInline;
using Point.Database.Context;
using Point.Database.Extensions;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure;
using Point.Models.Enums;
using Point.Models.LDM.EOverdracht30.Enums;
using System;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace Point.Communication
{
    public class Zorgmail
    {
        public enum ZorgmailUserType
        {
            SendAsLinkassist = 0,
            SendForConnectedOrganization = 1
        }

        private static string UsernameLinkAssist = ConfigurationManager.AppSettings["Zorgmail.Username"].ToString(); 
        private static string PasswordLinkAssist = ConfigurationManager.AppSettings["Zorgmail.Password"].ToString();
        private static string UsernameConnectedOrganizations = ConfigurationManager.AppSettings["Zorgmail.GP.Username"].ToString();
        private static string PasswordConnectedOrganizations = ConfigurationManager.AppSettings["Zorgmail.GP.Password"].ToString();

        private string timestamp = DateTime.Now.ToString("yyyyMMddHHmmss"); // Need a timestamp which is the same for the entire lifetime of this Zorgmail-instance

        private static string errorMessage(string methodName)
        {
            return string.Format("Fout bij communicatie in Zorgmail. method {0}", methodName);
        }

        private static string errorMessage(string methodName, string username, ZorgmailUserType initusertype)
        {
            return string.Format("Fout bij communicatie in Zorgmail. method {0}, type {1}, username {2}", methodName, initusertype, username);
        }

        static MailServiceClient _zorgmailMailServiceClient;
        protected static MailServiceClient ZorgmailMailServiceClient(string username, string password)
        {
            if (_zorgmailMailServiceClient == null)
            {
                _zorgmailMailServiceClient = new MailServiceClient("ZorgmailMailwebserviceInline");

                _zorgmailMailServiceClient.ClientCredentials.UserName.UserName = username;
                _zorgmailMailServiceClient.ClientCredentials.UserName.Password = password;
            }
            return _zorgmailMailServiceClient;
        }

        public Zorgmail() {  }

        public static bool IsValidZorgmailDepartment(Department department)
        {
            return (department != null && department.RecieveZorgmailTransferSend == true && !String.IsNullOrEmpty(department.ZorgmailTransferSend));
        }

        public delegate bool SendPDFDelegate(int transferId, Recipient recipient, byte[] content, string documentDescription, bool isFlowInstance); 

        public bool SendPDF(int transferId, Recipient recipient, byte[] content, string documentDescription, bool isFlowInstance)
        {

            try
            {
                using (var uow = new UnitOfWork<PointContext>())
                {
                    var client = ClientBL.GetByTransferID(uow, transferId);
                    string filename = createB02Filename(transferId, recipient, client, documentDescription, "pdf");

                    SendMessage sendmessage = new SendMessage()
                    {
                        messageId = "<" + createMessageId(transferId) + ">",
                        sender = UsernameLinkAssist + "@lms.lifeline.nl",
                        recipient = recipient.AccountId + "@lms.lifeline.nl",
                        subject = filename,
                        contentType = String.Format("application/pdf;name=\"{0}\";", HttpUtility.UrlEncode(filename)),
                        content = content
                    };
                    ZorgmailMailServiceClient(UsernameLinkAssist, PasswordLinkAssist).Send(sendmessage);

                    var communicationlog = new CommunicationLog()
                    {
                        ClientID = client.ClientID,
                        Content = new ASCIIEncoding().GetString(content),
                        Description = "Zorgmail.SendPDF()",
                        FromAddress = sendmessage.sender,
                        Subject = filename,
                        Timestamp = DateTime.Now,
                        ToAddress = sendmessage.recipient,
                        ToName = recipient.Name,
                        TransferID = transferId,
                        MailType = CommunicationLogType.ZorgmailSendPDF
                    };
                    uow.CommunicationLogRepository.Insert(communicationlog);
                    uow.Save();
                }
            }
            catch (Exception ex)
            {
                ExceptionHandling.HandleException(ex, errorMessage("SendPDF()"), transferId);
                return false;
            }
            return true;
            
        }

        public delegate bool SendMedvriDelegate(ZorgmailUserType initusertype, int transferId, int clientId, int sendingOrganizationID, int sendingDepartmentID, Recipient recipient, string addressGPForZorgmail, string usern, string passw, int employeeID, int screenID, DateTime timeStamp, PointUserInfo pointuserinfo, TemplateTypeID templatetypeid); 

        public bool SendMedvri(ZorgmailUserType initusertype, int transferId, int clientId, int sendingOrganizationID, int sendingDepartmentID, Recipient recipient, string addressGPForZorgmail, string usern, string passw, int employeeID, int screenID, DateTime timeStamp, PointUserInfo pointuserinfo, TemplateTypeID templatetypeid)
        {
            try
            {
                using (var uow = new UnitOfWork<PointContext>())
                {
                    var department = DepartmentBL.GetByDepartmentID(uow, sendingDepartmentID);
                    if(department == null)
                    {
                        return false;
                    }

                    Medvri gmv = new Medvri(transferId, employeeID);
                    string medvriContents = gmv.GetMedvri(recipient, usern, addressGPForZorgmail, pointuserinfo, templatetypeid, department);

                    //var client = ClientBL.GetByTransferID(uow, transferId);
                    var transfer = TransferBL.GetByTransferID(uow, transferId);

                    SendMessage sendmessage = new SendMessage()
                    {
                        messageId = "<" + createMessageId(transferId) + ">",
                        sender = usern + "@lms.lifeline.nl",
                        // Send to gateway:
                        recipient = "gateway@lms.lifeline.nl",
                        // Send to Web Service:
                        // sendmessage.recipient = recipient.AccountId + "@lms.lifeline.nl"; 

                        //Nooit anoniem (huisarts)
                        subject = transfer.Client.FullName(),
                        contentType = String.Format("application/edifact;name=\"{0}\";", HttpUtility.UrlEncode("medvri.xml"))
                    };

                    sendmessage.content = Encoding.UTF8.GetBytes(medvriContents);

                    string username = Zorgmail.UsernameLinkAssist;
                    string password = Zorgmail.PasswordLinkAssist;

                    switch (initusertype)
                    {
                        case ZorgmailUserType.SendForConnectedOrganization:
                            username = Zorgmail.UsernameConnectedOrganizations;
                            password = Zorgmail.PasswordConnectedOrganizations;
                            break;
                        default:
                            break;
                    }

                    try
                    {
                        ZorgmailMailServiceClient(username, password).Send(sendmessage);
                    }
                    catch
                    {
                        // try again, it sometimes needs time to startup 
                        System.Threading.Thread.Sleep(2000);
                        ZorgmailMailServiceClient(username, password).Send(sendmessage);
                    }

                    var communicationlog = new CommunicationLog()
                    {
                        ClientID = clientId,
                        Content = medvriContents,
                        Description = "Zorgmail.SendMedvri()",
                        FromAddress = sendmessage.sender,

                        //Hier wel anoniem (zichtbaar in dossier)
                        Subject = transfer.Client.ClientIsAnonymous ? transfer.ClientAnonymousName() : transfer.Client.FullName(),
                        Timestamp = DateTime.Now,
                        ToAddress = sendmessage.recipient,
                        ToName = recipient.Name,
                        TransferID = transferId,
                        EmployeeID = employeeID,
                        OrganizationID = sendingOrganizationID,
                        MailType = CommunicationLogType.ZorgmailMedvri
                        
                    };
                    uow.CommunicationLogRepository.Insert(communicationlog);
                    uow.Save();
                }
            }
            catch (Exception ex)
            {
                ExceptionHelper.SendMessageException(ex, employeeid: employeeID);

                ExceptionHandling.HandleException(ex, errorMessage("SendMedvri()", usern, initusertype), transferId);
                return false;
            }
            return true;

        }
        
        private string createMessageId(int transferId)
        {
            //Our convention is:
            //<timestamp yyyyMMddHHmmss>-<transferid>@point.nl
            //F.e.: 20121231145522-123654@point.nl

            return timestamp + "-" + transferId.ToString() + "@point.nl";
        }

        private string createB02Filename(int transferId, Recipient recipient, Client client, string documentDescription, string extension)
        {
            //Convention b02 is:
            //Filename = <convention>_<sender-name>_<recipient-name>_<recipient-customerno>_<pat-name>_<pat-birthdate>_<pat-gender>_<pat-bsn>_<doc-id>_<unique-id>.<extension>
            //F.e.: B02_POINT_UNIT4-Test_500060457_VAN DER TECHXX_05-12-1957_M_182937281_OverdrachtVerkort_20121231145522-123654.pdf
            string b02Name = "B02_{0}_{1}_{2}_{3}_{4}_{5}_{6}_{7}_{8}.{9}";

            string gender = "O";
            switch (client.Gender)
            {
                case Geslacht.OUN :
                    gender = "O";
                    break;
                case Geslacht.OM :
                    gender = "M";
                    break;
                case Geslacht.OF :
                    gender = "V";
                    break;
                default:
                    gender = "O";
                    break;
            }

            return String.Format(b02Name, 
                                 "POINT",
                                 Regex.Replace(recipient.Name, "[^a-zA-Z0-9]",""),
                                 recipient.AccountId,
                                 Regex.Replace(client.LastName, "[^a-zA-Z0-9]",""),
                                 client.BirthDate.GetValueOrDefault().ToString("dd-MM-yyyy"),
                                 gender,
                                 Regex.Replace(client.CivilServiceNumber, "[^a-zA-Z0-9]", ""),
                                 documentDescription.Replace("_",""),
                                 timestamp + "-" + transferId.ToString(),
                                 extension);

        }
    }

    public class Recipient
    {
        public string AccountId { get; set; }
        public string Name { get; set; }
    }
    
}
