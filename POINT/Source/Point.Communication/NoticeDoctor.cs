﻿using ENovation.Lifeline.MEDVRI;
using Point.Business;
using Point.Business.Helper;
using Point.Business.Logic;
using Point.Business.Logic.TemplateDataSets;
using Point.Database.Context;
using Point.Database.Extensions;
using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Extensions;
using System;

namespace Point.Communication
{
    public class NoticeDoctor
    {
        private int transferID;
        private int employeeID;

        public NoticeDoctor(int transferID, int employeeID)
        {
            this.transferID = transferID;
            this.employeeID = employeeID;
        }

        public string GetExampleNoticeDG(IUnitOfWork uow, PointUserInfo pointuserinfo, int organizationid = -1, string example = null)
        {
            var flowinstance = FlowInstanceBL.GetByTransferID(uow, transferID);
            var employee = EmployeeBL.GetByID(uow, employeeID);

            return GetNoticeDocGen(uow, flowinstance, pointuserinfo, TemplateTypeID.ZorgMailZHVVTJa, organizationid, example);
        }

        public string GetNoticeDocGen(IUnitOfWork uow, FlowInstance flowinstance, PointUserInfo pointuserinfo, TemplateTypeID templatetypeid = TemplateTypeID.ZorgMailZHVVTJa, int? overrideorganizationid = null, string example = null)
        {
            var templatetext = example;

            if (string.IsNullOrEmpty(templatetext))
            {
                int templateorganizationid = (overrideorganizationid.HasValue) ? overrideorganizationid.Value : flowinstance.StartedByOrganizationID.GetValueOrDefault();
                templatetext = TemplateBL.GetTextOrDefault(uow, templateorganizationid, (int)templatetypeid);
                if (string.IsNullOrEmpty(templatetext))
                {
                    throw new Exception(String.Format("No Template with TemplateTypeID {0} for OrganizationID {1}", templatetypeid, templateorganizationid));
                }
            }

            var zorgmaildataset = new ZorgMail(uow, flowinstance, pointuserinfo);
            var templatePaser = new TemplateParser(templatetext, zorgmaildataset.Get());
            return templatePaser.Parse();
        }

        public void SendURO(IUnitOfWork uow, PointUserInfo pointUserInfo, int organizationID, TemplateTypeID templateTypeID)
        {
            var hl7servicedata = new HL7ServiceData();
            var flowinstance = FlowInstanceBL.GetByTransferID(uow, transferID);
            var medvri = new Infrastructure.Helpers.Medvri(pointUserInfo.Employee.EmployeeID, transferID);
            string contents = GetNoticeDocGen(uow, flowinstance, pointUserInfo, templateTypeID, organizationID, null);
            hl7servicedata.CreateORU(organizationID, null, transferID, contents);


            var transfer = TransferBL.GetByTransferID(uow, transferID);
            var communicationlog = new CommunicationLog()
            {
                ClientID = transfer?.ClientID,
                Content = contents,
                Description = "NoticeDoctor.SendURO()",
                FromAddress = "HL7Service",

                //Hier wel anoniem (zichtbaar in dossier)
                Subject = transfer.Client.ClientIsAnonymous ? transfer.ClientAnonymousName() : transfer.Client.FullName(),
                Timestamp = DateTime.Now,
                ToAddress = "",
                ToName = "",
                TransferID = transferID,
                EmployeeID = employeeID,
                OrganizationID = organizationID,
                MailType = CommunicationLogType.ORUNoticeDoctor

            };
            uow.CommunicationLogRepository.Insert(communicationlog);
            uow.Save();
        }

    }
}
