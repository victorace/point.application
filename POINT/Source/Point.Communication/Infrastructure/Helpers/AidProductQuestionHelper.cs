﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using Point.Business.Logic;
using Point.Communication.Resources.Supplier.Questions;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Models.Enums;
using VegroIDs = Point.Communication.Resources.Supplier.Constants.InsurerQuestionsConstants.VegroQuestionIDs; 

namespace Point.Communication.Infrastructure.Helpers
{
    public static class AidProductQuestionHelper
    {
        private static readonly Dictionary<int, Insurers> _insurers = new Dictionary<int, Insurers>();
        private static Insurers GetInsurersData(int supplierID)
        {
            Insurers insurers;

            if (_insurers.Any())
            {
                if (_insurers.TryGetValue(supplierID, out insurers))
                {
                    return insurers;
                }
            }

            string xmlContent = null;

            switch (supplierID)
            {
                case (int)SupplierID.Vegro:
                    xmlContent = Properties.Resources.InsurerQuestions_supplierID_1;
                    break;
                case (int)SupplierID.MediPoint:
                    xmlContent = Properties.Resources.InsurerQuestions_supplierID_3;
                    break;
            }

            if (string.IsNullOrEmpty(xmlContent))
            {
                return null;
            }

            using (TextReader reader = new StringReader(xmlContent))
            {
                var xmlSerializer = new XmlSerializer(typeof(Insurers));
                insurers = xmlSerializer.Deserialize(reader) as Insurers;
            }

            _insurers.Add(supplierID, insurers);

            return insurers;
        }

        public static ProductGroup GetProductGroupQuestions(int supplierID, string insurerCode, string productGroupCode)
        {
            if (insurerCode == null) { return null; }

            var insurerData = GetInsurersData(supplierID);
            var insurer = insurerData?.Insurer.FirstOrDefault(ins => ins.Name.Contains(insurerCode));

            return insurer?.ProductGroup.FirstOrDefault(ins => ins.Id == productGroupCode);
        }

        public static List<AidProductQuestionViewModel> GetQuestions(IUnitOfWork uow, int supplierID, int transferID, int[] productIDs, string insurerCode, int uniqueFormKey, int? formSetVersionID)
        {
            var questions = new List<AidProductQuestionViewModel>();
            if (string.IsNullOrEmpty(insurerCode))
            {
                return questions;
            }

            var productGroups = AidProductBL.GetGroupsByProductIDs(uow, productIDs);
            var productIdStringList = productIDs.Select(it => it.ToString()).ToList();

            foreach (var productGroup in productGroups.Where(it => it.Code != null))
            {
                var groupQuestions = GetProductGroupQuestions(supplierID, insurerCode, productGroup.Code);
                if (groupQuestions == null)
                {
                    // TODO: LOG
                    continue;
                }
                foreach (var questionGroup in groupQuestions.QuestionGroup)
                {
                    if (string.IsNullOrEmpty(questionGroup.ShowOnlyFor) == false)
                    {
                        var onlyFor = questionGroup.ShowOnlyFor.Split(' ').Where(it => !string.IsNullOrEmpty(it)).ToList();
                        if (productIdStringList.Any(it => onlyFor.Contains(it)) == false)
                        {
                            continue;
                        }
                    }

                    var dependentOnIDs = new List<string>();

                    foreach (var item in questionGroup.Question)
                    {
                        AidProductQuestionType type = Enum.TryParse(item.Type, out type) ? type : AidProductQuestionType.text;

                        if (questions.Any(q => q.QuestionID == item.Ipid))
                        {
                            var existingQuestion = questions.FirstOrDefault(it => it.QuestionID == item.Ipid);

                            if (existingQuestion != null && !existingQuestion.ProductGroupCode.Contains(productGroup.Code))
                            {
                                existingQuestion.ProductGroupName += " / " + productGroup.Name;
                                existingQuestion.ProductGroupCode += "," + productGroup.Code;
                            }

                            if (item.Labels != null)
                            {
                                foreach (var newLabel in item.Labels.Label.Where(it => it.ExtraItems))
                                {
                                    var answerID = string.IsNullOrEmpty(newLabel.Ipid) ? newLabel.Ipv : newLabel.Ipid;
                                    var existingPossibleAnswer = existingQuestion.PossibleAnswers.FirstOrDefault(it => it.AnswerID == answerID);
                                    if (existingPossibleAnswer == null)
                                    {
                                        continue;
                                    }

                                    existingPossibleAnswer.ProductGroupCode += "," + productGroup.Code;
                                    if (existingPossibleAnswer.ExtraItems)
                                    {
                                        continue;
                                    }

                                    existingPossibleAnswer.ExtraItems = true;
                                    existingPossibleAnswer.ExtraItemsGroupCode = string.IsNullOrEmpty(existingPossibleAnswer.ExtraItemsGroupCode) ? productGroup.Code : (existingPossibleAnswer.ExtraItemsGroupCode + "," + productGroup.Code);
                                }
                            }

                            continue;
                        }

                        var newQuestion = new AidProductQuestionViewModel
                        {
                            AidProductQuestionType = type,
                            ProductGroupName = productGroup.Name,
                            ProductGroupCode = productGroup.Code,
                            Question = item.QuestionDescription,
                            QuestionID = item.Ipid,
                            Required = true,
                            PossibleAnswers = new List<AidProductAnswerViewModel>()
                        };

                        if (type == AidProductQuestionType.check)
                        {
                            //Checkboxes niet required per default
                            newQuestion.Required = false;
                            newQuestion.QuestionID = "CheckBoxGroup_" + questions.Count;
                        }

                        if (dependentOnIDs.Count > 0)
                        {
                            //Find the previous ones to fill the id of this question
                            var lastQuestion = questions.LastOrDefault();
                            if (lastQuestion != null)
                            {
                                newQuestion.Invisible = true;
                                newQuestion.Required = false;

                                lastQuestion.PossibleAnswers.ForEach(it => it.NextQuestionID = newQuestion.QuestionID);
                            }

                            //Only for the next Question in the group
                            dependentOnIDs = new List<string>();
                        }

                        if (item.Labels != null)
                        {
                            foreach (var label in item.Labels.Label)
                            {
                                var answerID = String.IsNullOrEmpty(label.Ipid) ? label.Ipv : label.Ipid;

                                //Vegro doet iets geks met Checkboxes
                                //Checkboxes gebruiken answerid als QuestionID en "1" als AnswerID
                                //Anders krijgen we dit niet goed in de order
                                newQuestion.PossibleAnswers.Add(new AidProductAnswerViewModel
                                {
                                    ProductGroupCode = productGroup.Code,
                                    ExtraItems = label.ExtraItems,
                                    ExtraItemsGroupCode = label.ExtraItems ? productGroup.Code : null,
                                    Answer = label.Text,
                                    QuestionID = type == AidProductQuestionType.check ? answerID : newQuestion.QuestionID,
                                    AnswerID = type == AidProductQuestionType.check ? "1" : answerID
                                });

                                if (label.ExtraItems)
                                {
                                    dependentOnIDs.Add(answerID);
                                }
                            }
                        }

                        if (supplierID == (int)SupplierID.MediPoint && type == AidProductQuestionType.text)
                        {
                            if (newQuestion.QuestionID.EndsWith("REMARK"))
                            {
                                newQuestion.Required = false;
                            }
                        }


                        questions.Add(newQuestion);
                    }
                }
            }

            var answers = AidProductBL.GetAnswers(uow, uniqueFormKey, formSetVersionID);
            foreach (var question in questions)
            {
                if (question.AidProductQuestionType == AidProductQuestionType.text)
                {
                    var answer = answers.FirstOrDefault(a => a.QuestionID == question.QuestionID);
                    if (answer == null)
                    {
                        continue;
                    }
                    //Altijd zichtbaar als er een waarde in staat
                    question.Invisible = false;
                    question.CurrentValue = answer.Value;
                }
                else
                {
                    foreach (var possibleAnswer in question.PossibleAnswers)
                    {
                        var answer = answers.FirstOrDefault(it => it.QuestionID == possibleAnswer.QuestionID && it.Value == possibleAnswer.AnswerID);
                        if (answer == null)
                        {
                            continue;
                        }
                        possibleAnswer.Checked = true;
                        if (!possibleAnswer.ExtraItems)
                        {
                            continue;
                        }
                        var nextQuestion = questions.FirstOrDefault(q => q.QuestionID == possibleAnswer.NextQuestionID);
                        if (nextQuestion == null)
                        {
                            continue;
                        }
                        nextQuestion.Invisible = false;
                        nextQuestion.Required = true;
                    }
                }
            }

            ApplySupplierSpecificActionsForVegro(uow, supplierID, transferID, uniqueFormKey, questions, formSetVersionID);

            return questions;
        }

        private static void ApplySupplierSpecificActionsForVegro(IUnitOfWork uow, int supplierID, int transferID, int uniqueFormKey, IReadOnlyCollection<AidProductQuestionViewModel> questions, int? formSetVersionID)
        {
            if (supplierID != (int)SupplierID.Vegro || !questions.Any())
            {
                return;
            }
            var gewicht = FlowFieldValueBL.GetByTransferIDAndFieldID(uow, transferID, FlowFieldConstants.ID_ObesitasGewicht).FirstOrDefault();
            var lengte = FlowFieldValueBL.GetByTransferIDAndFieldID(uow, transferID, FlowFieldConstants.ID_ObesitasLengte).FirstOrDefault();
            int gewichtInt = int.TryParse(gewicht?.Value, out gewichtInt) ? gewichtInt : 0;
            int lengteInt = int.TryParse(lengte?.Value, out lengteInt) ? lengteInt : 0;
            var gewichtQuestion = questions.FirstOrDefault(q => q.QuestionID == VegroIDs.Gewicht);
            var gewichtTextQuestion = questions.FirstOrDefault(q => q.QuestionID == VegroIDs.GewichtTekst);
            var lengteQuestion = questions.FirstOrDefault(q => q.QuestionID == VegroIDs.Lengte);
            var lengteTextQuestion = questions.FirstOrDefault(q => q.QuestionID == VegroIDs.LengteTekst);

            if (gewichtInt > 0 && gewichtQuestion != null && gewichtQuestion.PossibleAnswers != null && !gewichtQuestion.PossibleAnswers.Any(pa => pa.Checked))
            {
                var answerIdCode = "";
                if (gewichtInt < 80)
                {
                    answerIdCode = VegroIDs.GewichtValues.LessThan80;;
                }
                else if (gewichtInt <= 120)
                {
                    answerIdCode = VegroIDs.GewichtValues.EqualOrLessThan120;
                }
                else if (gewichtInt > 121)
                {
                    answerIdCode = VegroIDs.GewichtValues.GreaterThan120;
                }

                var answer = gewichtQuestion.PossibleAnswers.FirstOrDefault(a => a.AnswerID == answerIdCode);
                if (answer != null)
                {
                    answer.Checked = true;
                    answer.Value = answer.AnswerID;
                    AidProductBL.SaveAnswer(uow, answer, uniqueFormKey, formSetVersionID);
                }
            }
            if (gewichtInt > 0 && gewichtTextQuestion != null && string.IsNullOrEmpty(gewichtTextQuestion?.CurrentValue))
            {
                gewichtTextQuestion.CurrentValue = gewichtInt.ToString();
                AidProductBL.SaveAnswer(uow, gewichtTextQuestion, uniqueFormKey, formSetVersionID);
            }
            if (lengteInt > 0 && lengteQuestion?.PossibleAnswers != null && lengteQuestion.PossibleAnswers.Any(pa => pa.Checked) == false)
            {
                var answerIdCode = "";
                if (lengteInt < 150)
                {
                    answerIdCode = VegroIDs.LengteValues.LessThan150;
                }
                else if (lengteInt <= 175)
                {
                    answerIdCode = VegroIDs.LengteValues.EqualOrLessThan175;
                }
                else if (lengteInt <= 190)
                {
                    answerIdCode = VegroIDs.LengteValues.EqualOrLessThan190;
                }
                else if (lengteInt > 190)
                {
                    answerIdCode = VegroIDs.LengteValues.GreaterThan190;
                }

                var answer = lengteQuestion.PossibleAnswers.FirstOrDefault(a => a.AnswerID == answerIdCode);
                if (answer != null)
                {
                    answer.Checked = true;
                    answer.Value = answer.AnswerID;
                    AidProductBL.SaveAnswer(uow, answer, uniqueFormKey, formSetVersionID);
                }
            }

            if (lengteInt <= 0 || lengteTextQuestion == null || !string.IsNullOrEmpty(lengteTextQuestion?.CurrentValue))
            {
                return;
            }

            lengteTextQuestion.CurrentValue = lengteInt.ToString();
            AidProductBL.SaveAnswer(uow, lengteTextQuestion, uniqueFormKey, formSetVersionID);
        }
    }
}
