﻿using Point.Business.Logic;
using Point.Database.Context;
using Point.Models.Enums;
using Point.Database.Repository;
using System;
using System.Net.Mail;
using Point.Business.Helper;

namespace Point.Communication.Infrastructure.Helpers
{
    public class ResetPassword
    {
        public static MailMessage SendPasswordResetMail(string emailadress, string new_password, string employee_lastname)
        {
            using (var uow = new UnitOfWork<PointContext>())
            {               
                var template = TemplateDefaultBL.GetByTemplateTypeID(uow, (int)TemplateTypeID.PasswordResetMail)?.TemplateDefaultText;
                if (!string.IsNullOrEmpty(template))
                {
                    var resetPasswordDataset = new Point.Business.Logic.TemplateDataSets.ResetPassword(new_password);
                    var templatePaser = new TemplateParser(template, resetPasswordDataset.Get(), "<br />");
                    MailMessage mailmessage = Email.SendEmail(emailadress, "Nieuw wachtwoord aangevraagd", templatePaser.Parse());
                    return mailmessage;

                }
                else
                {
                    throw new Exception("Cant load Template with TemplateTypeID:" + (int)TemplateTypeID.PasswordResetMail + " (PasswordResetEmail)");
                }
            }
        }
    }
}
