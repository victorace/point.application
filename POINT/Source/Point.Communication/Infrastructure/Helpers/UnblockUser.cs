﻿using Point.Business.Logic;
using Point.Database.Context;
using Point.Models.Enums;
using Point.Database.Repository;
using Point.Business.Helper;
using System;
using System.Net.Mail;
using Point.Database.Models;
using System.Collections.Generic;
using System.Linq;


namespace Point.Communication.Infrastructure.Helpers
{
    public class UnblockUser
    {
        public static string SendUnblockEmailToAdmins(List<Employee> admins, Employee employee )
        {
            var result = "Email kan niet verzenden worden naar beheerder(s).";
            using (var uow = new UnitOfWork<PointContext>())
            {                
                var template = TemplateDefaultBL.GetByTemplateTypeID(uow, (int)TemplateTypeID.UnblockUserMail)?.TemplateDefaultText;
                if (string.IsNullOrEmpty(template))
                {
                    throw new Exception(string.Format("Can't load Template with TemplateTypeID: {0}" + (int)TemplateTypeID.UnblockUserMail));
                }

                var adminEmails = admins?.Where(adm => !string.IsNullOrEmpty(adm.EmailAddress));
                if (!adminEmails.Any())
                {
                    throw new Exception("Admin email not found.");
                }
                
                foreach (var admin in admins)
                {
                    // get email of other admins. ( we have maximum 2 admins here.)
                    var otherAdminEmails = string.Join(",", admins?.Where(adm => adm.EmailAddress != admin.EmailAddress)?.Select(adm => adm.EmailAddress)?.ToArray());
                    var unblockDataset = new Point.Business.Logic.TemplateDataSets.UnblockUserMail(employee, admin, otherAdminEmails);
                    var templatePaser = new TemplateParser(template, unblockDataset.Get(), "<br />");
                    MailMessage mailmessage = Email.SendEmail(admin.EmailAddress, "De-blokkeren aangevraagd", templatePaser.Parse());


                    if (mailmessage != null)
                    {
                        result = "Uw verzoek is verstuurd naar uw beheerder(s).";
                    }
                }              
            }
            return result;
        }

        public static string SendUnblockEmailToOrganizationEmails(List<string> emails, Employee employee)
        {
            var result = "Email kan niet verzonden worden naar het e-mailadres ingesteld bij uw organisatie.";
            using (var uow = new UnitOfWork<PointContext>())
            {
                var template = TemplateDefaultBL.GetByTemplateTypeID(uow, (int)TemplateTypeID.UnblockUserMail)?.TemplateDefaultText;
                if (string.IsNullOrEmpty(template))
                {
                    throw new Exception(string.Format("Can't load Template with TemplateTypeID: {0}" + (int)TemplateTypeID.UnblockUserMail));
                }

                foreach (var email in emails)
                {                  
                    var otherEmails = string.Join(",", emails?.Where(otherEmail => otherEmail != email)?.ToArray());
                    var unblockDataset = new Point.Business.Logic.TemplateDataSets.UnblockUserMail(employee, null, otherEmails);
                    var templatePaser = new TemplateParser(template, unblockDataset.Get(), "<br />");
                    MailMessage mailmessage = Email.SendEmail(email, "De-blokkeren aangevraagd", templatePaser.Parse());

                    if (mailmessage != null)
                    {
                        result = "Uw verzoek is verstuurd naar het e-mailadres ingesteld bij uw organisatie.";
                    }
                }
            }
            return result;
        }
    }
}