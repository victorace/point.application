﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using Point.Business.Logic;
using Point.Communication.Resources.Supplier.Helpers;
using Point.Communication.Resources.Supplier.Ordering;
using Point.Database.Helpers;
using Point.Database.Models;
using Point.Database.Repository;
using Point.Infrastructure.Helpers;
using Point.ServiceBus.Models;

namespace Point.Communication.Infrastructure.Helpers
{
    public class ExternalSupplierXML
    {
        public string XMLResult = "";
        public Exception Exception;
        public int _formSetVersionID = -1;
        private IEnumerable<FlowFieldValue> _flowFieldValues = Enumerable.Empty<FlowFieldValue>();
        public ExternalSupplierXML(int formSetVersionID)
        {
            _formSetVersionID = formSetVersionID;
        }

        public bool IsSentBefore(IUnitOfWork uow)
        {
            string currentOrderStatus = ExternalSupplierXmlHelper.GetValue(GetFlowFieldValues(uow), FlowFieldConstants.ID_OrderStatus);

            return new[] { FlowFieldConstants.Value_OrderBevestigd, FlowFieldConstants.Value_OrderMislukt, FlowFieldConstants.Value_OrderVerstuurd }.Contains(currentOrderStatus);
        }

        public class ResultXml
        {
            public int ClientID { get; set; } = -1;
            public string OrderName { get; set; }
            public bool Success { get; set; }
        }


        public ResultXml CreateXML(IUnitOfWork uow, SendExternalSupplierOrder sendExternalSupplierOrder)
        {
            var resultXml = new ResultXml();

            try
            {
                var cxmlOrder = GetCxmlOrder(uow, sendExternalSupplierOrder);
                var xmlSerializer = new XmlSerializer(typeof(ExternalSupplierOrder.CXML));

                using (var stringWriter = new Utf8StringWriter())
                {
                    xmlSerializer.Serialize(stringWriter, cxmlOrder);
                    XMLResult = stringWriter.ToString();
                }

                resultXml.Success = !string.IsNullOrEmpty(XMLResult);
                resultXml.ClientID = cxmlOrder.ClientID;
                resultXml.OrderName = cxmlOrder.OrderName;
            }
            catch (Exception ex)
            {
                Exception = ex;
            }

            return resultXml;
        }
        
        public ExternalSupplierOrder.CXML GetCxmlOrder(IUnitOfWork uow, SendExternalSupplierOrder sendExternalSupplierOrder)
        {
            if (!SupplierHelper.IsExternalSupplier(sendExternalSupplierOrder.SupplierID))
            {
                return null;
            }

            try
            {
                var externalSupplierOrder = new ExternalSupplierOrder
                {
                    OrderDate = DateTime.Now,
                    TransferID = sendExternalSupplierOrder.TransferID,
                    FormSetVersionID = sendExternalSupplierOrder.FormSetVersionID,
                    EmployeeID = sendExternalSupplierOrder.SendingEmployeeID,
                    uow = uow,
                    OrganizationID = sendExternalSupplierOrder.SendingOrganizationID,
                    SupplierID = sendExternalSupplierOrder.SupplierID,
                    SupplierName = sendExternalSupplierOrder.SupplierName,
                    OrganizationSettingSupplierCode = sendExternalSupplierOrder.OrganizationSettingSupplierCode,
                    FlowFieldValues = GetFlowFieldValues(uow)
                };

                return ExternalSupplierXmlHelper.GetCXML(externalSupplierOrder);
            }
            catch (Exception ex)
            {
                Exception = ex;
                return null;
            }

        }

        private IEnumerable<FlowFieldValue> GetFlowFieldValues(IUnitOfWork uow)
        {
            if (!_flowFieldValues.Any())
            {
                _flowFieldValues = FlowFieldValueBL.GetByFormSetVersionID(uow, _formSetVersionID).ToList();
            }

            return _flowFieldValues;
        }

    }

}
