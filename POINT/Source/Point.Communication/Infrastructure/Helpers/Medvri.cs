﻿using ENovation.Lifeline.MEDVRI;
using Point.Business;
using Point.Business.Helper;
using Point.Business.Logic;
using Point.Business.Logic.TemplateDataSets;
using Point.Database.Context;
using Point.Database.Extensions;
using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Extensions;
using System;
using Point.Models.LDM.EOverdracht30.Enums;

namespace Point.Communication.Infrastructure.Helpers
{
    public class Medvri
    {
        private int transferID;
        private int employeeID;
        
        public Medvri(int transferID, int employeeID)
        {
            this.transferID = transferID;
            this.employeeID = employeeID;
        }

        public string GetMedvri(Recipient recipient, string username, string addressGPForZorgmail, PointUserInfo pointuserinfo, TemplateTypeID templatetypeid, Department sendingdepartment)
        {
            using (var uow = new UnitOfWork<PointContext>())
            {
                MEDVRI_1 mv = new MEDVRI_1();

                var flowinstance = FlowInstanceBL.GetByTransferID(uow, transferID);
                var employee = EmployeeBL.GetByID(uow, employeeID);
                var sendinglocation = sendingdepartment.Location;
                var sendingorganization = sendinglocation.Organization;

                
                if (flowinstance?.Transfer?.Client != null)
                {
                    mv.email.emailsubject = flowinstance.Transfer.Client.FullName();
                }

                mv.envelope.envelopeedisyntax = envelopeEnvelopeedisyntax.EDIFACT;
                mv.envelope.envelopelastofinterchange = envelopeEnvelopelastofinterchange.Yes;
                mv.envelope.envelopesyntaxid = "UNOC";
                mv.envelope.envelopesyntaxversion = "1";
                mv.envelope.envelopecontrolreference = transferID.ToString();//tranferid o.i.d.

                mv.envelope.envelopesenderidentifier.Value = username;
                mv.envelope.enveloperecipientidentifier.Value = flowinstance.Transfer.Client.AddressGPForZorgmail;
                mv.envelope.envelopedatetimeofpreparation = DateTime.Now.ToString("o");

                mv.header.headerreference = transferID.ToString();//tranferid o.i.d.
                mv.header.headermessageidentifier.messageidentifiertype = "MEDVRI";
                mv.header.headermessageidentifier.messageidentifierversion = "1";
                mv.header.headercommonaccessreference.Value = "(MEDVRI 1)";

                mv.afzender.afzenderpersoonsnaam.Value = employee.FullName();
                mv.afzender.afzenderafdeling.Value = sendingdepartment.Name;
                mv.afzender.afzendertelefoonnummer.Value = sendingdepartment.PhoneNumber;
                mv.afzender.afzenderadres.adresstraatnaam.Value = sendinglocation.StreetName;
                mv.afzender.afzenderadres.adreshuisnummer.Value = sendinglocation.Number;
                mv.afzender.afzenderadres.adrespostcode.Value = sendinglocation.PostalCode;
                mv.afzender.afzenderadres.adreswoonplaats.Value = sendinglocation.City;
                mv.afzender.afzenderinstellingsnaam.Value = sendingorganization.Name;


                mv.datumtijd.Value = DateTime.Now.ToString("yyyy-MM-dd");

                switch (flowinstance.Transfer.Client.Gender)
                {
                    case Geslacht.OM :
                        mv.patientidentificatie.patientidentificatiegeslacht = patientidentificatiePatientidentificatiegeslacht.Man; break;
                    case Geslacht.OF:
                        mv.patientidentificatie.patientidentificatiegeslacht = patientidentificatiePatientidentificatiegeslacht.Vrouw; break;
                    default:
                        mv.patientidentificatie.patientidentificatiegeslacht = patientidentificatiePatientidentificatiegeslacht.Onbekend; break;
                }

                if (flowinstance.Transfer.Client.BirthDate.HasValue)
                {
                    mv.patientidentificatie.patientidentificatiegeboortedatum.Value = flowinstance.Transfer.Client.BirthDate.Value.ToString("yyyy-MM-dd");
                }

                mv.patientidentificatie.patientidentificatiepatientnaam.patientnaamachternaam.Value = flowinstance.Transfer.Client.LastName;

                mv.patientidentificatie.patientidentificatiepatientnaam.patientnaammeisjesnaam.Value = flowinstance.Transfer.Client.MaidenName;
                mv.patientidentificatie.patientidentificatiepatientnaam.patientnaamvoorletters.Value = flowinstance.Transfer.Client.Initials;
                mv.patientidentificatie.patientidentificatiepatientnaam.patientnaamvoornaam.Value = flowinstance.Transfer.Client.FirstName;

                mv.patientidentificatie.patientidentificatieidentificatienummer1.Value = flowinstance.Transfer.Client.CivilServiceNumber;

                mv.patientadresgegevens.patientadresgegevensadres.adresstraatnaam.Value = flowinstance.Transfer.Client.StreetName;
                mv.patientadresgegevens.patientadresgegevensadres.adreshuisnummer.Value = flowinstance.Transfer.Client.Number;
                mv.patientadresgegevens.patientadresgegevensadres.adreswoonplaats.Value = flowinstance.Transfer.Client.City;
                mv.patientadresgegevens.patientadresgegevensadres.adrespostcode.Value = flowinstance.Transfer.Client.PostalCode;

                mv.notitie.Value = new NoticeDoctor(transferID, employeeID).GetNoticeDocGen(uow, flowinstance, pointuserinfo, templatetypeid);

                mv.ontvanger.ontvangerpersoonsnaam.Value = recipient.Name;

                return mv.Serialize().FilterTags(new[] { "EDI-%" }).InnerXml;
            }
        }

    }
}