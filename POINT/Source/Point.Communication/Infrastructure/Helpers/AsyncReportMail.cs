﻿using Point.Business.Logic;
using Point.Models.Enums;
using Point.Database.Repository;
using Point.Infrastructure.Helpers;
using System;
using System.Net.Mail;
using Point.Database.Context;
using Point.Business.Helper;
using Point.Database.Models;

namespace Point.Communication.Infrastructure.Helpers
{
    public class AsyncReportMail
    {
        public static MailMessage SendAsyncReportMail(int employeeId, string reportLink, string emailSubject, TemplateTypeID templateTypeID)
        {
            using (var uow = new UnitOfWork<PointContext>())
            {
                var template = TemplateDefaultBL.GetByTemplateTypeID(uow, (int)templateTypeID)?.TemplateDefaultText;
                if(template == null)
                {
                    throw new Exception("Cant load Template with TemplateTypeID:" + templateTypeID);
                }

                var employee = EmployeeBL.GetByID(uow, employeeId);
                if (employee == null || string.IsNullOrEmpty(employee.EmailAddress))
                {
                    throw new Exception("Medewerkergegevens zijn niet beschikbaar.");
                }

                if(!String.IsNullOrEmpty(employee.EmailAddress))
                {
                    return SendEmail(employee.EmailAddress, template, reportLink, emailSubject);
                }

                return null;
            }
        }

        private static MailMessage SendEmail(string emailadres, string template, string downloadLink, string emailSubject)
        {  
            var dbset = new Business.Logic.TemplateDataSets.AsyncReportMail(downloadLink);
            var templatePaser = new TemplateParser(template, dbset.Get(), "<br />");
            MailMessage mailmessage = Email.SendEmail(emailadres, emailSubject, templatePaser.Parse());

            return mailmessage;
        }

        public static MailMessage SendErrorEmail(int employeeId,string emailSubject, TemplateTypeID templateTypeID)
        {
            using (var uow = new UnitOfWork<PointContext>())
            {
                var template = TemplateDefaultBL.GetByTemplateTypeID(uow, (int)templateTypeID)?.TemplateDefaultText;
                if (template == null)
                {
                    throw new Exception("Cant load Template with TemplateTypeID:" + templateTypeID);
                }

                var employee = EmployeeBL.GetByID(uow, employeeId);
                if (employee == null || string.IsNullOrEmpty(employee.EmailAddress))
                {
                    throw new Exception("Medewerkergegevens zijn niet beschikbaar.");
                }

                if (!String.IsNullOrEmpty(employee.EmailAddress))
                {
                    var templatePaser = new TemplateParser(template, null, "<br />");
                    return Email.SendEmail(employee.EmailAddress, emailSubject, templatePaser.Parse());
                }

                return null;
            }
        }

    }
}
