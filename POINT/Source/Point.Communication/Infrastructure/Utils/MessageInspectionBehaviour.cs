﻿using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;

namespace Point.Communication.Infrastructure.Utils
{

    class MessageInspectionBehavior : IClientMessageInspector, IEndpointBehavior
    {
        public string LastRequest;
        public MessageInspectionBehavior()
        {
        }

        public void Validate(ServiceEndpoint endpoint)
        {
        }

        public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
        {
        }

        public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
        {
        }

        public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {
            clientRuntime.MessageInspectors.Add(this);
        }

        public object BeforeSendRequest(ref System.ServiceModel.Channels.Message request, IClientChannel channel)
        {
            LastRequest = request.ToString();
            return null;
        }

        public void AfterReceiveReply(ref System.ServiceModel.Channels.Message reply, object correlationState)
        {
            // Write out http headers
            //foreach (var property in reply.Properties)
            //{
            //    if (!(property.Value is HttpResponseMessageProperty)) continue;
            //    var httpProperties = (HttpResponseMessageProperty)property.Value;
            //    foreach (KeyValuePair<object, object> kvp in httpProperties.Headers)
            //    {
            //        Console.WriteLine(kvp.Key + ":" + kvp.Value);
            //    }
            //}
            //// Write result message
            //Console.WriteLine(reply.ToString());
        }
    }
}
