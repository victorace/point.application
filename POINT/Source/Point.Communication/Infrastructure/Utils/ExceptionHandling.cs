﻿using System;
using System.Configuration;
using System.Net.Mail;
using System.Text;

namespace Point.Communication.Infrastructure.Utils
{
    public static class ExceptionHandling
    {

        public static void HandleException(Exception ex, string subject = null, int transferID = -1)
        {
            try
            {
                string emailAddress = ConfigurationManager.AppSettings["addressSupport"] as string;
                string fromAddress = ConfigurationManager.AppSettings["fromAddressPoint"] as string;
                if (!String.IsNullOrEmpty(emailAddress) && !String.IsNullOrEmpty(fromAddress))
                {
                    using (SmtpClient client = new SmtpClient(ConfigurationManager.AppSettings["SmtpServer"] as string))
                    {
                        MailMessage message = new MailMessage();
                        message.IsBodyHtml = true;
                        message.From = new MailAddress(fromAddress);
                        message.To.Add(emailAddress);
                        message.Subject = (subject ?? "Fout in Point.Communication");

                        var emailbody = new StringBuilder();

                        //emailbody.AppendLine("Exception occured in Point.Communication");
                        if (transferID > 0)
                        {
                            emailbody.AppendFormat("TransferID: {0}\r\n", transferID);
                        }
                        emailbody.AppendFormat("Applicationpool: {0}\r\n", Environment.UserName);
                        emailbody.AppendFormat("Machinename: {0}\r\n", Environment.MachineName);
                        emailbody.AppendLine();
                        emailbody.AppendFormat("Exception:\r\n{0}\r\n", ex.Message);
                        if (ex.StackTrace != null)
                        {
                            emailbody.AppendFormat("Stacktrace:\r\n{0}\r\n", ex.StackTrace);
                        }

                        message.Body = @"<html><body style=""font-family: Calibri,Candara,Segoe,Segoe UI,Optima,Arial,sans-serif;"">" + emailbody.ToString().Replace("\r\n", "<br />") + "</body></html>";

                        client.Send(message);
                    }
                }
            }
            catch { /* ignore */ }
        }

    }
}
