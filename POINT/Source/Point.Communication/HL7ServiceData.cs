﻿using Point.Communication.HL7Service;

namespace Point.Communication
{
    public class HL7ServiceData
    {
        public HL7Client GetPatient(int organizationID, int? locationID, string patientNumber)
        {
            HL7ServiceClient hl7serviceclient = new HL7ServiceClient();

            HL7Client hl7client = hl7serviceclient.GetPatient(organizationID, locationID, patientNumber);

            return hl7client;
        }

        public void CreateORU(int organizationID, int? locationID, int transferID, string contents)
        {
            HL7ServiceClient hl7serviceclient = new HL7ServiceClient();
            hl7serviceclient.CreateORUAsync(organizationID, locationID, transferID, contents);
        }

        public void UpdateDirectories()
        {
            HL7ServiceClient hl7serviceclient = new HL7ServiceClient();
            hl7serviceclient.UpdateDirectories();
        }
    }
}