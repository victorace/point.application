﻿using Point.Business.Helper;
using Point.Business.Logic;
using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure;
using RazorEngine.Configuration;
using RazorEngine.Templating;
using System;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Collections.Generic;

namespace Point.Communication
{
    public class Email
    {
        private static IRazorEngineService getRazorEngine()
        {
            var config = new TemplateServiceConfiguration();
            var service = RazorEngineService.Create(config);
            config.CachingProvider = new DefaultCachingProvider(t => { });

            return RazorEngineService.Create(config);
        }

        public static MailMessage SendRazorMail(IUnitOfWork uow, int? transferid, string templatecode, object model, string email)
        {
            var template = RazorTemplateBL.GetByCode(uow, templatecode);
            if (template == null)
            {               
                throw new Exception("No RazorTemplate with code: " + templatecode);
            }

            var service = getRazorEngine();
            
            var emailbody = service.RunCompile(template.Contents, templatecode, model: model);

            var emailSubject = template.TemplateName;
            if (model is EmailSignaleringReminder)
            {
                var reminder = model as EmailSignaleringReminder;

                emailSubject = string.Format(template.TemplateName, reminder.LatestSignal, reminder.SignalCount);
            }
            var message = Email.SendEmail(email, emailSubject, emailbody);

            if (model is IScheduleHistory)
            {
                var schedulehistory = model as IScheduleHistory;
                try
                {
                    ScheduleHistoryBL.Create(uow, schedulehistory.FrequencyID, schedulehistory.Timestamp);
                }
                catch (Exception exc)
                {
                    ExceptionHelper.SendMessageException(exc);
                }
            }
            if (model is ICommunicationLog)
            {
                var communicationlog = model as ICommunicationLog;
                try
                {
                    CommunicationLogBL.Save(uow, new CommunicationLog()
                    {
                        TransferID = transferid,
                        Content = emailbody,
                        ToAddress = String.Join(";", message.To),
                        FromAddress = message.From.Address,
                        FromName = message.From.DisplayName,
                        BCCAddress = String.Join(";", message.Bcc),
                        CCAddress = String.Join(";", message.CC),
                        Subject = message.Subject,
                        Timestamp = DateTime.Now,
                        MailType = template.CommunicationLogType,
                        EmployeeID = communicationlog.EmployeeID,
                        OrganizationID = communicationlog.OrganizationID
                    });
                }
                catch (Exception exc)
                {
                    ExceptionHelper.SendMessageException(exc);
                }
            }

            return message;
        }

        public static MailMessage SendEmail(string to, string subject, string body)
        {
            var message = new MailMessage();
            message.From = new MailAddress(ConfigurationManager.AppSettings["fromAddressPoint"]);

            string[] toParts = to.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            if (toParts.Length == 0) return null;

            foreach (string email in toParts)
            {
                if (EmailHelper.IsEmailAddressValid(email))
                {
                    message.To.Add(email);
                }
            }

            if (!message.To.Any())
            {
                return null;
            }

            var addressBCC = ConfigurationManager.AppSettings["addressBCC"];

            if (!String.IsNullOrWhiteSpace(addressBCC))
            {
                string[] bccParts = addressBCC.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string email in bccParts)
                {
                    if (EmailHelper.IsEmailAddressValid(email))
                    {
                        message.Bcc.Add(email);
                    }
                }
            }

            message.Subject = subject;
            message.Body = body;
            message.IsBodyHtml = true;

            try
            {
                using (SmtpClient client = new SmtpClient(ConfigurationManager.AppSettings["SmtpServer"]))
                {
                    client.Send(message);
                }
            }
            catch (Exception ex)
            {
                ExceptionHelper.SendMessageException(ex);
            }

            return message;
        }

        public static void SendEmailAsync(string to, string subject, string body)
        {
            SendEmailDelegate caller = SendEmail;

            caller.BeginInvoke(to, subject, body, null, null);
        }

        public static void SendPDF(int transferID, Recipient recipient, string attachmentName, byte[] fileContents)
        {
            if (transferID <= -1) return;

            Zorgmail.SendPDFDelegate caller = new Zorgmail.SendPDFDelegate(new Zorgmail().SendPDF);
            IAsyncResult asyncresult = caller.BeginInvoke(transferID, recipient, fileContents, attachmentName, true, null, null); 
        }

        public static void SendPDF(int transferID, string accountId, string name, string attachmentName, byte[] fileContents)
        {
            SendPDF(transferID, new Recipient() { AccountId = accountId, Name = name }, attachmentName, fileContents);
        }

        public static void SendMedvri(Zorgmail.ZorgmailUserType initusertype, int transferID, int clientID, int sendingOrganizationID, int sendingDepartmentID, Recipient recipient, string addressGPForZorgmail, string usern, string passw, int employeeID, int screenID, DateTime timeStamp, PointUserInfo pointuserinfo, TemplateTypeID templatetypeid)
        {
            Zorgmail.SendMedvriDelegate caller = new Zorgmail.SendMedvriDelegate(new Zorgmail().SendMedvri);
            IAsyncResult asyncresult = caller.BeginInvoke(initusertype, transferID, clientID, sendingOrganizationID, sendingDepartmentID, recipient, addressGPForZorgmail, usern, passw, employeeID, screenID, timeStamp, pointuserinfo, templatetypeid, null, null); 
        }

        public delegate MailMessage SendEmailDelegate(string to, string subject, string body);
    }
}