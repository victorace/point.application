﻿using System;
using MessageBird;
using MessageBird.Exceptions;
using MessageBird.Net.ProxyConfigurationInjector;
using MessageBird.Objects;
using System.Configuration;

namespace Point.Communication
{
    public class MessageBird
    {

        public void SendMessage(string number, string message)
        {
            string accessKey = ConfigurationManager.AppSettings["MessageBirdKey"] as string;
            if ( String.IsNullOrEmpty(accessKey))
            {
                throw new Exception("MessageBirdKey not set!");
            }

            long receipient = -1;
            if (number.StartsWith("06"))
            {
                receipient = long.Parse("31" + number.Substring(1));
            }
            else
            {
                receipient = long.Parse(number);
            }

            IProxyConfigurationInjector proxyConfigurationInjector = null;
            var messagebirdClient = Client.CreateDefault(accessKey, proxyConfigurationInjector);

            try
            {
                Message messagebirdMessage = messagebirdClient.SendMessage("POINT", message, new[] { receipient });
            }
            catch (ErrorException eex)
            {
                throw new Exception("Error with sending messagebird - " + eex.Message, eex);
            }
            catch(Exception ex)
            {
                throw new Exception("Error with sending messagebird - " + ex.Message, ex);
            }

        }

    }
}
