﻿using Point.Business.Logic;
using Point.Communication.Infrastructure.Utils;
using Point.Communication.PlanbordWebservicePointImport;
using Point.Database.Context;
using Point.Database.Extensions;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography;
using System.Text;

namespace Point.Communication
{
    public class Planbord
    {
        public bool IgnoreCertificateError { get; set; }

        public delegate bool SendStatusDelegate(string organizationSystemCode, string privateKey, List<FlowInstanceSearchViewModel> flowinstancesearchvalues); 

        public bool SendStatus(string organizationSystemCode, string privateKey, List<FlowInstanceSearchViewModel> flowinstancesearchvalues)
        {
            try
            {
                DateTime dateTimeSent = DateTime.Now;

                ServicePointManager.ServerCertificateValidationCallback += new RemoteCertificateValidationCallback((sender, certificate, chain, sslPolicyErrors) => { return IgnoreCertificateError; });

                ImportPointSoapClient client = new ImportPointSoapClient();
                MessageInspectionBehavior mib = new MessageInspectionBehavior();
                client.Endpoint.Behaviors.Add(mib);

                using (var uow = new UnitOfWork<PointContext>())
                {
                    var header = getPointHeader(organizationSystemCode, privateKey, dateTimeSent);
                    var details = getPointDetails(uow, flowinstancesearchvalues).ToArray<PointDetail>();

                    string result = client.ImportDetails(header, details);

                    var communicationlog = new Point.Database.Models.CommunicationLog()
                    {
                        Content = mib.LastRequest.Truncate(1024),
                        Description = "Planbord.SendStatus()",
                        FromName = organizationSystemCode,
                        Subject = "Subject: [], Result: [" + result + "]",
                        Timestamp = dateTimeSent,
                        ToAddress = client.Endpoint.Address.ToString()
                    };
                    CommunicationLogBL.Save(uow, communicationlog);
                }
            }
            catch (Exception ex)
            {
                ExceptionHandling.HandleException(ex, "Fout bij aanroep webservice Planbord, method SendStatus()");
                return false;
            }

            return true;
        }

        private PointHeader getPointHeader(string organizationSystemCode, string privateKey, DateTime dateTimeSent)
        {
            return new PointHeader()
            {
                DateTimeSent = dateTimeSent,
                IdentificationHash = getHashString(organizationSystemCode, privateKey, dateTimeSent),
                Organization_SystemCode = organizationSystemCode
            };
        }

        private List<PointDetail> getPointDetails(IUnitOfWork uow, List<FlowInstanceSearchViewModel> flowinstancesearchvalues)
        {
            List<PointDetail> pointdetails = new List<PointDetail>();

            foreach (var searchvalues in flowinstancesearchvalues.OrderBy(fi => fi.FlowInstanceSearchValues.TransferCreatedDate).Select(fi => fi.FlowInstanceSearchValues)) // .FlowInstance.Transfer.CreatedDate))
            {
                try
                {
                    var pointdetail = new PointDetail();
                    pointdetail.Patient = new Patient()
                    {
                        CivilServiceNumber = searchvalues.ClientCivilServiceNumber,
                        PatientNumber = searchvalues.PatientNumber
                    };

                    var flowinstance = FlowInstanceBL.GetByID(uow, searchvalues.FlowInstanceID);
                    if (flowinstance == null)
                    {
                        continue;
                    }
                    var activephaseinstances = PhaseInstanceBL.GetActiveByFlowInstanceID(uow, searchvalues.FlowInstanceID);
                    if (activephaseinstances == null || activephaseinstances.Count() == 0)
                    {
                        continue;
                    }

                    bool isclosed = FlowInstanceBL.IsFlowClosed(uow, searchvalues.FlowInstanceID);
                    var activephase = activephaseinstances.ToActivePhase(flowinstance, isclosed);

                    if (activephase == null || activephase.Count() == 0)
                    {
                        continue;
                    }

                    var codes = new Code[5];

                    string activephasenumber = activephase.LastOrDefault().Item1;
                    if (double.TryParse(activephasenumber, out double activephasenumberdouble))
                    {
                        activephasenumber = activephasenumberdouble.ToString("F0");
                    }

                    codes[0] = new Code()
                    {
                        CodeMainType = CodeMainType.PointStatus,
                        CodeType = "ActieveFase",
                        CodeValue = activephasenumber
                    };

                    codes[1] = new Code()
                    {
                        CodeMainType = CodeMainType.PointStatus,
                        CodeType = "ActieveFaseBeschrijving",
                        CodeValue = (activephasenumber == "-1" ? activephase.LastOrDefault().Item2 : activephase.LastOrDefault().Item3)
                    };

                    codes[2] = new Code()
                    {
                        CodeMainType = CodeMainType.PointStatus,
                        CodeType = "OntslagGeaccepteerd",
                        CodeValue = flowinstance.FlowInstanceSearchValues?.DischargePatientAcceptedYNByVVT
                    };

                    codes[3] = new Code()
                    {
                        CodeMainType = CodeMainType.PointStatus,
                        CodeType = "TransferID",
                        CodeValue = searchvalues.TransferID.ToString()
                    };

                    codes[4] = new Code()
                    {
                        CodeMainType = CodeMainType.PointStatus,
                        CodeType = "FlowDefinitionID",
                        CodeValue = ((int)flowinstance.FlowDefinitionID).ToString()
                    };

                    pointdetail.Codes = codes;

                    pointdetails.Add(pointdetail);
                }
                catch { /* ignore */ }
            }

            return pointdetails;
        }
        
        private byte[] getHash(string inputString)
        {
            HashAlgorithm algorithm = SHA1.Create();
            return algorithm.ComputeHash(Encoding.UTF8.GetBytes(inputString));
        }

        private string getHashString(string organizationSystemCode, string privateKey, DateTime dateTimeSent)
        {
            string inputstring = String.Format("{0}_{1}_{2}", organizationSystemCode, privateKey, dateTimeSent.ToString("yyyyMMddHHmm"));

            StringBuilder sb = new StringBuilder();
            foreach (byte b in getHash(inputstring))
            {
                sb.Append(b.ToString("X2"));
            }
            return sb.ToString();
        }

    }
}
