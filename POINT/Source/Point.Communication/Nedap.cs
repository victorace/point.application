﻿using Newtonsoft.Json;
using Point.Business.BusinessObjects;
using Point.Business.Logic;
using Point.Communication.Models;
using Point.Database.Models;
using Point.Database.Repository;
using Point.Infrastructure;
using Point.Infrastructure.Extensions;
using Point.Infrastructure.Helpers;
using Point.Models.Enums;
using Point.Models.LDM.EOverdracht30.Enums;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography.X509Certificates;
using System.Xml.Linq;

namespace Point.Communication
{
    public class Nedap
    {
        public Department ReceivingDepartment = new Department();
        public Organization ReceivingOrganization = new Organization();
        private int transferID = -1;
        private int employeeID = -1;
        private int? formSetVersionID = null;
        private int? transferAttachmentID = null;
        private NedapClientData nedapClientData = new NedapClientData();
        private NedapEmployeeData nedapEmployeeData = new NedapEmployeeData();
        private CommunicationLogType communicationLogType = CommunicationLogType.None;
        private Client client = new Client();

        private string errormessage = "";
        private X509Certificate x509certificate = null;

        public bool SendAttachment(IUnitOfWork uow, int transferID, int employeeID, int transferAttachmentID, Organization organization, Department department)
        {
            resetVars();
            communicationLogType = CommunicationLogType.NedapAttachment;
            this.transferAttachmentID = transferAttachmentID;

            if (!initializeNedap(uow, transferID, employeeID, organization, department) || !String.IsNullOrEmpty(errormessage))
            {
                setUnhandledToQueue(uow);
                return false;
            }

            try
            {
                var transferattachment = TransferAttachmentBL.GetByID(uow, transferAttachmentID);
                if (transferattachment?.GenericFile == null || transferattachment?.GenericFile?.FileData == null || transferattachment?.GenericFile?.FileData.Length == 0)
                {
                    errormessage = $"Could not load filedata for transferAttachmentID {transferAttachmentID}";
                    setUnhandledToQueue(uow);
                    return false;
                }

                if (transferattachment.Deleted == true)
                {
                    CommunicationQueueBL.SetHandledByTransferAttachmentID(uow, transferAttachmentID);
                    return true;
                }

                string filename = FileHelper.GetValidFileName(transferattachment.GenericFile.FileName);
                string description = $"Bijlage '{filename}'{getDescriptionSuffix()}";
                string attachmenttype = transferattachment.AttachmentTypeID.GetDescription();
                string postfilename = $"POINT_{attachmenttype}_{filename}";

                bool postresult = postDocument(uow, description, postfilename, attachmenttype, transferattachment.GenericFile.FileData);
                if (!postresult || !String.IsNullOrEmpty(errormessage))
                {
                    setUnhandledToQueue(uow);
                    return false;
                }
                CommunicationQueueBL.SetHandledByTransferAttachmentID(uow, transferAttachmentID);
                return true;
            }
            catch (Exception ex)
            {
                ExceptionHelper.SendMessageException(ex, employeeid: employeeID, transferid: transferID);
                return false;
            }
        }

        public bool SendPDF(IUnitOfWork uow, int transferID, int employeeID, int formSetVersionID, bool isAltered, Organization organization, Department department, bool isVO = true)
        {
            resetVars();
            communicationLogType = isVO ? CommunicationLogType.NedapVO : CommunicationLogType.NedapForm;
            this.formSetVersionID = formSetVersionID;
            if (!initializeNedap(uow, transferID, employeeID, organization, department) || !String.IsNullOrEmpty(errormessage))
            {
                setUnhandledToQueue(uow);
                return false;
            }

            try
            {
                var formsetversion = FormSetVersionBL.GetByID(uow, formSetVersionID);
                if (formsetversion == null)
                {
                    errormessage = $"Could not retrieve formsetversion with formsetversionid {formSetVersionID}";
                    setUnhandledToQueue(uow);
                    return false;
                }

                if (formsetversion.Deleted == true)
                {
                    CommunicationQueueBL.SetHandledByFormSetVersionID(uow, formSetVersionID);
                    return true;
                }

                byte[] filecontents = null;
                var flowinstance = FlowInstanceBL.GetByTransferID(uow, transferID);
                var attachmentid = VerpleegkundigeOverdrachtBO.GetAttachmentID(uow, flowinstance.FlowInstanceID);
                if (attachmentid.HasValue)
                {
                    filecontents = TransferAttachmentBL.GetFileByID(uow, attachmentid.Value)?.FileData;
                }
                else
                {
                    filecontents = FormsetVersionPdfHelper.GetPDFData(formSetVersionID, employeeID);
                }

                if (filecontents == null || filecontents.Length == 0)
                {
                    errormessage = $"Could not create valid PDF for formSetVersionID {formSetVersionID}";
                    setUnhandledToQueue(uow);
                    return false;
                }

                string description = $"{(isAltered ? "Gewijzigde " : "")}{formsetversion.FormType.Name}" + getDescriptionSuffix();
                string filename = $"POINT_{formsetversion.FormType.Name.Replace(" ", "_").Replace(".", "")}_{DateTime.Now.ToString("yyyyMMddHHmm")}.pdf";
                bool postresult = postDocument(uow, description, filename, formsetversion.FormType.Name, filecontents);
                if (!postresult || !String.IsNullOrEmpty(errormessage))
                {
                    setUnhandledToQueue(uow);
                    return false;
                }
                CommunicationQueueBL.SetHandledByFormSetVersionID(uow, formSetVersionID);
                return true;

            }
            catch (Exception exc)
            {
                ExceptionHelper.SendMessageException(exc, employeeid: employeeID, transferid: transferID);
                return false;
            }
        }

        public NedapClientData GetNedapClientFromBSN(IUnitOfWork uow, string bsn)
        {
            var url = getFullUrl("clients", bsn ?? "");
            string response = doRequest(uow, url, $"No 'client' found for BSN [{bsn ?? ""}]");

            var nedapclientdata = JsonConvert.DeserializeObject<NedapClientData>(response, new JsonSerializerSettings
            {
                Error =
                delegate (object sender, Newtonsoft.Json.Serialization.ErrorEventArgs args) {
                    errormessage = $"No 'client' found for BSN [{bsn ?? ""}]. Response [{response}]. Error [{args.ErrorContext.Error.Message}]";
                    args.ErrorContext.Handled = true;
                }
            });

            return nedapclientdata ?? new NedapClientData();
        }

        public Client GetClientFromNedapClientData(NedapClientData nedapClientData, string bsn = null)
        {
            Client client = new Client();
            if (bsn != null)
            {
                client.CivilServiceNumber = bsn;
            }

            switch (nedapClientData.Gender)
            {
                case "M":
                    client.Gender = Geslacht.OM;
                    break;
                case "F":
                case "V":
                    client.Gender = Geslacht.OF;
                    break;
                default:
                    client.Gender = Geslacht.OUN;
                    break;
            }

            client.Initials = nedapClientData.Initials;
            client.FirstName = nedapClientData.FirstName;
            client.LastName = nedapClientData.LastName;
            client.MaidenName = nedapClientData.PartnerName;
            client.BirthDate = nedapClientData.DateOfBirth;
            client.StreetName = nedapClientData.AddressString;
            client.Email = nedapClientData.EmailAddress;
            client.PhoneNumberMobile = nedapClientData.MobilePhone;
            client.City = nedapClientData.Hometown;
            return client;
        }

        public NedapEmployeeData GetNedapEmployeeFromMedewerkerNummer(IUnitOfWork uow, string medewerkerNummer)
        {
            if (String.IsNullOrEmpty(medewerkerNummer))
            {
                errormessage = $"No medewerkernummer supplied in Nedap.GetNedapEmployeeFromMedewerkerNummer()";
                return new NedapEmployeeData();
            }
            var url = getFullUrl("employees", medewerkerNummer);
            string response = doRequest(uow, url, $"No 'employee' found with medewerkernummer [{medewerkerNummer}]");

            var nedapemployeedata = JsonConvert.DeserializeObject<NedapEmployeeData>(response, new JsonSerializerSettings { Error = delegate (object sender, Newtonsoft.Json.Serialization.ErrorEventArgs args) { errormessage = $"No 'employee' found for medewerkernummer [{medewerkerNummer}]. Response [{response}]. Error [{args.ErrorContext.Error.Message}]"; args.ErrorContext.Handled = true; } });

            return nedapemployeedata ?? new NedapEmployeeData();
        }

        private string getFullUrl(string action, int id)
        {
            return getFullUrl("f", action, id);
        }

        private string getFullUrl(string action, string id)
        {
            return getFullUrl("f", action, id);
        }

        private string getFullUrl(string apitype, string action, int id)
        {
            return getFullUrl(apitype, action, id.ToString());
        }

        private string getFullUrl(string apitype, string action, string id)
        {
            var baseurl = ConfigHelper.GetAppSettingByName<string>("NedapBaseUrl");
            return System.IO.Path.Combine(baseurl, apitype, action, id??"").Replace("\\", "/").TrimEnd('/');
        }

        private string getDescriptionSuffix()
        {
            return $" verzonden vanuit POINT op {DateTime.Now.ToString("dd-MM-yyyy")} om {DateTime.Now.ToString("HH:mm")}";
        }

        private void resetVars()
        {
            formSetVersionID = null;
            transferAttachmentID = null;
            communicationLogType = CommunicationLogType.None;
        }

        private bool initializeNedap(IUnitOfWork uow, int transferID, int employeeID, Organization organization, Department department)
        {
            if (organization == null || department == null)
            {
                errormessage = "Organization is NULL or Department is NULL";
                return false;
            }

            this.transferID = transferID;
            this.employeeID = employeeID;
            ReceivingOrganization = organization;
            ReceivingDepartment = department;

            if (!setCertificate(uow))
            {
                return false;
            }

            client = ClientBL.GetByTransferID(uow, transferID);
            if (String.IsNullOrEmpty(client?.CivilServiceNumber))
            {
                errormessage = $"No CivilServiceNumber specified for Client for TransferID {transferID}";
                return false;
            }

            nedapClientData = GetNedapClientFromBSN(uow, client.CivilServiceNumber);
            if (nedapClientData.ID <= 0 || !String.IsNullOrEmpty(errormessage))
            {
                return false;
            }

            nedapEmployeeData = GetNedapEmployeeFromMedewerkerNummer(uow, organization.NedapMedewerkernummer);
            if (nedapEmployeeData.ID <= 0 || !String.IsNullOrEmpty(errormessage))
            {
                return false;
            }

            return true;
        }

        private bool setCertificate(IUnitOfWork uow)
        {
            if (ReceivingOrganization?.OrganizationID <= 0)
            {
                errormessage = $"No Organization specified in Nedap()";
                return false;
            }

            var cryptocertificate = CryptoCertificateBL.GetByID(uow, ReceivingOrganization.NedapCryptoCertificateID.ConvertTo<int>());
            if (cryptocertificate == null)
            {
                errormessage = $"No CryptoCertificate found for organizationID {ReceivingOrganization.OrganizationID}";
                return false;
            }
            x509certificate = CryptoCertificateBL.GetX509FromCryptoCertificate(cryptocertificate);
            return true;
        }

        private void setUnhandledToQueue(IUnitOfWork uow)
        {
            // Set previously other records to handled before creating a new one in the queue
            if (formSetVersionID > 0)
            {
                CommunicationQueueBL.SetHandledByFormSetVersionID(uow, formSetVersionID.ConvertTo<int>());
            }
            else if (transferAttachmentID > 0)
            {
                CommunicationQueueBL.SetHandledByTransferAttachmentID(uow, transferAttachmentID.ConvertTo<int>());
            }

            CommunicationQueue communicationqueue = new CommunicationQueue() {
                MailType = communicationLogType,
                FormSetVersionID = formSetVersionID,
                TransferAttachmentID = transferAttachmentID,
                Timestamp = DateTime.Now,
                TransferID = transferID,
                ClientID = client.ClientID,
                EmployeeID = employeeID,
                ReceivingDepartmentID = ReceivingDepartment.DepartmentID,
                ReceivingOrganizationID = ReceivingOrganization.OrganizationID,
                ReasonNotSend = errormessage,
                Handled = false 
            };

            CommunicationQueueBL.Save(uow, communicationqueue);
        }

        private void saveHandledToLog(IUnitOfWork uow, string content, string subject, string toAddress)
        {
            CommunicationLogBL.Save(uow, new CommunicationLog()
            {
                BCCAddress = "",
                CCAddress = "",
                ClientID = client.ClientID,
                Content = content,
                EmployeeID = employeeID,
                FromName = $"Nedap employeeID: {nedapEmployeeData.ID}, identificationNo: {nedapEmployeeData.IdentificationNo}",
                MailType = communicationLogType,
                Subject = subject,
                Timestamp = DateTime.Now,
                ToAddress = toAddress,
                TransferID = transferID
            });
        }

        private string doRequest(IUnitOfWork uow, string url, string error)
        {
            string response = "";

            if (x509certificate == null && !setCertificate(uow))
            { 
                errormessage += $"{error} No valid x509certificate specified.";
            }
            else
            {
                try
                {
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    var request = (HttpWebRequest)WebRequest.Create(url);
                    request.Method = "GET";
                    request.Accept = "application/json";
                    request.ClientCertificates.Add(x509certificate);

                    var webresponse = request.GetResponse();

                    var streamreader = new StreamReader(webresponse.GetResponseStream());
                    response = streamreader.ReadToEnd();
                }
                catch (WebException wex)
                {
                    var httpwebresponse = wex.Response as HttpWebResponse;
                    if (httpwebresponse != null)
                    {
                        errormessage = $"{error} WebException statuscode [{httpwebresponse.StatusCode}], message [{wex.Message}]";
                    }
                    else
                    {
                        errormessage = $"{error} Exception [{wex.Message}]";
                    }
                }
                catch (Exception ex)
                {
                    errormessage = $"{error} Exception [{ex.Message}]";
                }
            }

            try
            {
                new WebRequestLogBL(uow).LogWebRequest($"GET {url}", response, errormessage);
            }
            catch { /* ignore */ }

            return response;
        }

        private bool postDocument(IUnitOfWork uow, string description, string filename, string tag, byte[] fileContents)
        {

            if (x509certificate == null && !setCertificate(uow))
            {
                errormessage = $"No valid x509certificate specified in Nedap.postDocument()";
                return false;
            }

            if (nedapClientData == default(NedapClientData))
            {
                errormessage = $"No valid nedapClientData specified in Nedap.postDocument()";
                return false;
            }

            if (nedapEmployeeData == default(NedapEmployeeData))
            {
                errormessage = $"No valid nedapEmployeeData specified in Nedap.postDocument()";
                return false;
            }

            WebRequestHandler handler = new WebRequestHandler();
            handler.ClientCertificates.Add(x509certificate);

            try
            {
                using (var httpclient = new HttpClient(handler))
                {
                    using (var content = new MultipartFormDataContent())
                    {
                        var url = getFullUrl("t", "documents", null);
                        var requesturi = String.Concat("?client_id=", nedapClientData.ID);

                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                        var request = (HttpWebRequest)WebRequest.Create(url);

                        httpclient.BaseAddress = new Uri(url);

                        XDocument xdoc = new XDocument(
                                             new XDeclaration("1.0", "utf-8", "yes"),
                                             new XElement("document",
                                                 new XElement("clientObjectId", nedapClientData.ID),
                                                 new XElement("employeeObjectId", nedapEmployeeData.ID),
                                                 new XElement("description", description),
                                                 new XElement("rightSelection", "All"),
                                                 new XElement("status", "1"),
                                                 new XElement("fileName", filename),
                                                 new XElement("tags", new XElement("tag", tag))
                                             )
                                         );

                        var fileContentXML = new StringContent(xdoc.Declaration.ToString() + Environment.NewLine + xdoc.ToString());
                        fileContentXML.Headers.ContentDisposition = new ContentDispositionHeaderValue("metadata") { DispositionType = "form-data", Name = "metadata" };
                        fileContentXML.Headers.ContentType = new MediaTypeHeaderValue("application/xml");

                        var fileContentPDF = new ByteArrayContent(fileContents);
                        fileContentPDF.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment") { FileName = filename, DispositionType = "form-data", Name = "attachment" };
                        fileContentPDF.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");

                        content.Add(fileContentXML);
                        content.Add(fileContentPDF);

                        var response = httpclient.PostAsync(requesturi, content).Result;
                        var result = response.Content.ReadAsStringAsync().Result;

                        saveHandledToLog(uow, result, filename, (url + requesturi));
                    }
                }
            }
            catch (WebException wex)
            {
                var httpwebresponse = wex.Response as HttpWebResponse;
                if (httpwebresponse != null)
                {
                    errormessage = $"WebException in Nedap.postDocument(). Statuscode: [{httpwebresponse.StatusCode}]. Message: [{wex.Message}]";
                }
                else
                {
                    errormessage = $"Exception in Nedap.postDocument(). Exception: [{wex.Message}]";
                }
                return false;
            }
            catch (Exception ex)
            {
                errormessage = $"Exception in Nedap.postDocument(). Exception: [{ex.Message}]";
                return false;
            }

            return true;
        }
    }
}