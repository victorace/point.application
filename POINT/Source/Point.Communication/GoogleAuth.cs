﻿using Google.Authenticator;
using Point.Database.Models.ViewModels;
using System;

namespace Point.Communication
{
    public class GoogleAuth
    {
        public void FillMFAViewModel(MFAViewModel mfaViewModel, string username, string environment)
        {
            var authenticator = new TwoFactorAuthenticator();
            if (!environment.Equals("POINT", StringComparison.InvariantCultureIgnoreCase))
            {
                environment = $"POINT - {environment}";
            }
            var generated = authenticator.GenerateSetupCode(environment, username.Replace(" ",""), mfaViewModel.MFAKey, 300, 300, true);
            mfaViewModel.ImageURL = generated.QrCodeSetupImageUrl; 
            mfaViewModel.SetupCode = generated.ManualEntryKey;
        }

        public bool CheckMFAViewModel(MFAViewModel mfaViewModel)
        {
            var authenticator = new TwoFactorAuthenticator();
            return authenticator.ValidateTwoFactorPIN(mfaViewModel.MFAKey, mfaViewModel.MFACode);
        }
    }
}
