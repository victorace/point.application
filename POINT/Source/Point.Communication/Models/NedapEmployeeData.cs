﻿using System;

namespace Point.Communication.Models
{
    public class NedapEmployeeData
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string BirthName { get; set; }
        public string Name { get; set; }
        public string IdentificationNo { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string PreferredNameType { get; set; }
        public string LastName { get; set; }
        public string PartnerName { get; set; }
        public string Initials { get; set; }
        public string Prefix { get; set; }
        public string BirthNamePrefix { get; set; }
        public string PartnerNamePrefix { get; set; }
        public string Gender { get; set; }
        public string ContractId { get; set; }
        public string MobilePhone { get; set; }
        public string MobilePhoneNumber { get; set; }
        public string AuthenticationMobilePhone { get; set; }
        public string WorkMobilePhone { get; set; }
        public string PrivateMobilePhone { get; set; }
        public string HomePhone { get; set; }
        public string EmailAddress { get; set; }
        public string FixedHoursPerWeek { get; set; }
        public string VarHoursPerWeek { get; set; }
        public bool UnknownEmployee { get; set; }
        public DateTime VerifiedUntilDate { get; set; }
        public bool LimitedEmployee { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}
