﻿using System;


namespace Point.Communication.Models
{
    public class NedapClientData
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string IdentificationNo { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string PreferredNameType { get; set; }
        public string LastName { get; set; }
        public string BirthName { get; set; }
        public string PartnerName { get; set; }
        public string Initials { get; set; }
        public string Prefix { get; set; }
        public string BirthNamePrefix { get; set; }
        public string PartnerNamePrefix { get; set; }
        public string Gender { get; set; }
        public string EmailAddress { get; set; }
        public string MobilePhone { get; set; }
        public DateTime UpdatedAt { get; set; }
        public DateTime CreatedAt { get; set; }
        public string GivenName { get; set; }
        public int CivilStatus { get; set; }
        public DateTime? CivilStatusDate { get; set; }
        public DateTime? DeathDate { get; set; }
        public string Hometown { get; set; }
        public string Nationality { get; set; }
        public string Language { get; set; }
        public string Religion { get; set; }
        public string AddressString { get; set; }
        public string Match { get; set; }
        public string SecretClient { get; set; }
        public Guid UUid { get; set; }
    }
}
