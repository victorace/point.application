﻿using System;
using System.Collections.Generic;
using System.Linq;
using Point.Business.Logic;
using Point.Communication.Resources.Supplier.Ordering;
using Point.Database.Extensions;
using Point.Database.Models;
using Point.Infrastructure.Extensions;
using Point.Models.Enums;

namespace Point.Communication.Resources.Supplier.Helpers
{
    public class ExternalSupplierXmlHelper
    {
        private const string _countryNL = "NL";
        private const string _orderNamePrefix = "Order_POINT_";

        public static ExternalSupplierOrder.CXML GetCXML(ExternalSupplierOrder externalSupplierOrder)
        {          
            var now = DateTime.Now;

           var cXml = new ExternalSupplierOrder.CXML
            {
                PayloadID = now.ToNumericDateTime(),
                Timestamp = now.ToXMLDateTime(),
                Header = FillHeader(externalSupplierOrder),
                Request = new ExternalSupplierOrder.Request {OrderRequest = FillOrderRequest(externalSupplierOrder)}
            };

            cXml.ClientID = externalSupplierOrder.ClientID;
            cXml.OrderName = externalSupplierOrder.OrderName;
            cXml.Success = true;

            return cXml;
        }

        public static string GetValue(IEnumerable<FlowFieldValue> flowFieldValues, int flowFieldID)
        {
            var flowFieldValue = flowFieldValues.FirstOrDefault(ffv => ffv.FlowFieldID == flowFieldID);
            return flowFieldValue == null ? "" : flowFieldValue.Value;
        }

        public static string GetValueOrDefault(IEnumerable<FlowFieldValue> flowFieldValues, int flowFieldID, string defaultValue)
        {
            var value = GetValue(flowFieldValues, flowFieldID);
            if (string.IsNullOrEmpty(value))
            {
                value = defaultValue;
            }

            return value;
        }

        public static bool GetValueBoolean(IEnumerable<FlowFieldValue> flowFieldValues, int flowFieldID)
        {
            var val = GetValue(flowFieldValues, flowFieldID);
            return !string.IsNullOrEmpty(val) && val.Equals("true", StringComparison.InvariantCultureIgnoreCase);
        }

        public static string GetValueBooleanString(IEnumerable<FlowFieldValue> flowFieldValues, int flowFieldID)
        {
            return GetValueBoolean(flowFieldValues, flowFieldID).ToString().ToLowerInvariant();
        }

        public static DateTime? GetValueDate(IEnumerable<FlowFieldValue> flowFieldValues, int flowFieldID)
        {
            var value = GetValue(flowFieldValues, flowFieldID);
            return string.IsNullOrEmpty(value) ? null : value.FromPointDateTimeDatabase();
        }

        public static ExternalSupplierOrder.Header FillHeader(ExternalSupplierOrder externalSupplierOrder)
        {
            var header = new ExternalSupplierOrder.Header
            {
                From = new ExternalSupplierOrder.From
                {
                    Credential = FillCredential("Supplier", "9999")
                },
                To = new ExternalSupplierOrder.To { Credential = FillCredential($"{externalSupplierOrder.SupplierName.ToUpperInvariant()} BV", "1") },
                Sender = new ExternalSupplierOrder.Sender
                {
                    Credential = FillCredential("NetworkId", "POINT"),
                    UserAgent = "POINT"
                }

            };
            return header;
        }
        
        private static ExternalSupplierOrder.Credential FillCredential(string domain, string identity)
        {
            var credential = new ExternalSupplierOrder.Credential
            {
                Domain = domain,
                Identity = identity
            };
            return credential;
        }

        internal static void FillOrderName(ExternalSupplierOrder externalSupplierOrder)
        {
            // NO PREFIX FOR MEDIPOINT
            var prefix = externalSupplierOrder.SupplierID == (int) SupplierID.MediPoint ? "" : _orderNamePrefix;
            externalSupplierOrder.OrderName = $"{prefix}{externalSupplierOrder.FormSetVersionID}";
        }

        internal static void FillShipToInformation(ExternalSupplierOrder externalSupplierOrder)
        {
            var formsetVersion = FormSetVersionBL.GetByID(externalSupplierOrder.uow, externalSupplierOrder.FormSetVersionID);

            if (formsetVersion == null)
            {
                return;
            }

            externalSupplierOrder.TransferID = formsetVersion.TransferID;

            var client = ClientBL.GetByTransferID(externalSupplierOrder.uow, formsetVersion.TransferID);
            if (client?.HealthInsuranceCompanyID == null)
            {
                return;
            }

            externalSupplierOrder.Client = client;

            externalSupplierOrder.ClientID = client.ClientID;
            var healthInsurer = HealthInsurerBL.GetByID(externalSupplierOrder.uow, client.HealthInsuranceCompanyID.Value);

            if (healthInsurer != null)
            {
                externalSupplierOrder.InsurerSupplierCode = HealthInsurerBL.GetExternalSupplierCode(healthInsurer, externalSupplierOrder.SupplierID);
            }

            externalSupplierOrder.HealthInsurer = healthInsurer;
            //var employee = EmployeeBL.GetByID(uow, EmployeeID);
        }

        internal static ExternalSupplierOrder.Questions FillQuestions(ExternalSupplierOrder externalSupplierOrder)
        {
            var questions = new ExternalSupplierOrder.Questions { Question = new List<ExternalSupplierOrder.Question>() };
            var answers = AidProductBL.GetAnswersByFormSetVersionID(externalSupplierOrder.uow, externalSupplierOrder.FormSetVersionID);
            if (answers == null || answers.Count == 0)
            {
                return questions;
            }
            foreach (var answer in answers)
            {
                if (string.IsNullOrEmpty(answer.QuestionID))
                {
                    continue;
                }

                var question = new ExternalSupplierOrder.Question
                {
                    Ipid = answer.QuestionID,
                    Text = answer.Value
                };

                if (answer.ExtraItems)
                {
                    question.Extraitems = "true";
                    question.Categoryid = answer.ExtraItemsGroupCode;
                }

                questions.Question.Add(question);
            }

            return questions;
        }
        
        internal static List<ExternalSupplierOrder.ItemOut> FillItems(ExternalSupplierOrder externalSupplierOrder)
        {
            var itemsOut = new List<ExternalSupplierOrder.ItemOut>();
            var aidProductOrderItems = AidProductBL.GetOrderItemsByFormSetVersionID(externalSupplierOrder.uow, externalSupplierOrder.FormSetVersionID);
            if (aidProductOrderItems == null || aidProductOrderItems.Count == 0)
            {
                return itemsOut;
            }

            foreach (var aidProductOrderItem in aidProductOrderItems)
            {
                var itemOut = new ExternalSupplierOrder.ItemOut
                {
                    Quantity = aidProductOrderItem.Quantity.ToString(),
                    LineNumber = (itemsOut.Count + 1).ToString(),
                    ItemID = new ExternalSupplierOrder.ItemID { SupplierPartID = aidProductOrderItem.AidProduct.Code },//aidProductOrderItem.AidProductID.ToString() },
                    ItemDetail = new ExternalSupplierOrder.ItemDetail { UnitPrice = new ExternalSupplierOrder.UnitPrice { Money = new ExternalSupplierOrder.Money { Currency = "", Text = "1,00" } }, Description = new ExternalSupplierOrder.Description { Lang = "", Text = aidProductOrderItem.AidProduct.Name }, UnitOfMeasure = "STUKS", Classification = new ExternalSupplierOrder.Classification { Domain = "" } }
                };
                itemsOut.Add(itemOut);
            }

            return itemsOut;
        }

        public static ExternalSupplierOrder.OrderRequest FillOrderRequest(ExternalSupplierOrder externalSupplierOrder)
        {
            FillOrderName(externalSupplierOrder);

            var orderRequest = new ExternalSupplierOrder.OrderRequest();
            var orderRequestHeader = new ExternalSupplierOrder.OrderRequestHeader
            {
                OrderID = externalSupplierOrder.OrderName,
                OrderDateRaw = externalSupplierOrder.OrderDate,
                OrderDate = externalSupplierOrder.OrderDate.ToXMLDateTime(),
                Type = "new",
                Total = new ExternalSupplierOrder.Total { Money = new ExternalSupplierOrder.Money() },
                ShipTo = new ExternalSupplierOrder.ShipTo { Address = FillShipToAddress(externalSupplierOrder) },
                BillTo = new ExternalSupplierOrder.BillTo { Address = new ExternalSupplierOrder.Address { Name = new ExternalSupplierOrder.Name { Lang = "", Text = "" } } },
                Comments = "",
                Extrinsic = new ExternalSupplierOrder.Extrinsic { Name = "Custaccount", Text = "" }
            };
            // bewust hardcoded leeg. Vegro/Sana regelt dit zelf
            FillDeliveryInfo(externalSupplierOrder, orderRequestHeader);
            orderRequestHeader.Questions = FillQuestions(externalSupplierOrder);

            orderRequest.OrderRequestHeader = orderRequestHeader;

            orderRequest.ItemOut = FillItems(externalSupplierOrder);

            var authorizationFormAttachmentID = GetValue(externalSupplierOrder.FlowFieldValues, FlowFieldConstants.ID_AuthorizationFormAttachmentID);
            if (!string.IsNullOrEmpty(authorizationFormAttachmentID))
            {
                var transferAttachment = TransferAttachmentBL.GetByID(externalSupplierOrder.uow, authorizationFormAttachmentID.ConvertTo<int>());
                if (transferAttachment?.GenericFile?.FileData != null)
                {
                    orderRequest.AuthorizationForm = Convert.ToBase64String(transferAttachment?.GenericFile?.FileData);
                }
            }

            orderRequest.PatientAuthorizationForRentalItems = GetValueBooleanString(externalSupplierOrder.FlowFieldValues, FlowFieldConstants.ID_ToestemmingPatientHuurartikelen);

            return orderRequest;
        }

        private static void FillDeliveryInfo(ExternalSupplierOrder externalSupplierOrder, ExternalSupplierOrder.OrderRequestHeader orderRequestHeader)
        {
            orderRequestHeader.RequestedDeliveryDateRaw = GetValueDate(externalSupplierOrder.FlowFieldValues, FlowFieldConstants.ID_AfleverDatum);
            orderRequestHeader.RequestedDeliveryDate = orderRequestHeader.RequestedDeliveryDateRaw.ToXMLDateTime();
            
            var isSupplierMediPoint = externalSupplierOrder.SupplierID == (int) SupplierID.MediPoint;

            if (isSupplierMediPoint)
            {
                orderRequestHeader.BlockTimeCode = GetValue(externalSupplierOrder.FlowFieldValues, FlowFieldConstants.ID_AfleverTijdMediPoint);
                orderRequestHeader.Transport1 = GetValueOrDefault(externalSupplierOrder.FlowFieldValues, FlowFieldConstants.ID_MediPointTransportAddressRemark, string.Empty);
            }
            else
            {
                orderRequestHeader.BlockTimeCode = GetValue(externalSupplierOrder.FlowFieldValues, FlowFieldConstants.ID_AfleverTijd);

                if (GetValueBoolean(externalSupplierOrder.FlowFieldValues, FlowFieldConstants.ID_TransportGeduld))
                {
                    orderRequestHeader.Transport1 = "Geduld bij deur";
                }
                if (!string.IsNullOrEmpty(GetValue(externalSupplierOrder.FlowFieldValues, FlowFieldConstants.ID_TransportHuisnummer)))
                {
                    orderRequestHeader.Transport2 = $"Afleveren bij: {GetValue(externalSupplierOrder.FlowFieldValues, FlowFieldConstants.ID_TransportHuisnummer)}";
                }
                if (!string.IsNullOrEmpty(GetValue(externalSupplierOrder.FlowFieldValues, FlowFieldConstants.ID_TransportBellenVanTeVoren)))
                {
                    orderRequestHeader.Transport3 = $"Vooraf bellen: {GetValue(externalSupplierOrder.FlowFieldValues, FlowFieldConstants.ID_TransportBellenVanTeVoren)}";
                }
            }
            
            var employee = EmployeeBL.GetByID(externalSupplierOrder.uow, externalSupplierOrder.EmployeeID);
            if (employee != null)
            {
                var email = isSupplierMediPoint ? GetValueOrDefault(externalSupplierOrder.FlowFieldValues, FlowFieldConstants.ID_OntvangerBevestigingsEmailAdresTransferpunt, employee.EmailAddress) : employee.EmailAddress;

                orderRequestHeader.BIG_Number = employee.BIG;
                orderRequestHeader.PointEmployeeEmail = email;
                orderRequestHeader.PointEmployeeName = employee.FullName().Truncate(50);
                orderRequestHeader.PointEmployeePhoneNumber = employee.PhoneNumber;
            }

            orderRequestHeader.PartnerCode = OrganizationSettingBL.GetValue<string>(externalSupplierOrder.uow, externalSupplierOrder.OrganizationID, externalSupplierOrder.OrganizationSettingSupplierCode);

            var organization = OrganizationBL.GetByOrganizationID(externalSupplierOrder.uow, externalSupplierOrder.OrganizationID);
            if (!string.IsNullOrEmpty(organization?.AGB))
            {
                orderRequestHeader.AGBCode = organization.AGB;
            }
        }

        private static ExternalSupplierOrder.Address FillShipToAddress(ExternalSupplierOrder externalSupplierOrder)
        {
            FillShipToInformation(externalSupplierOrder);

            if (externalSupplierOrder.Client == null)
            {
                return new ExternalSupplierOrder.Address();
            }
            
            var emailFieldID = externalSupplierOrder.SupplierID == (int)SupplierID.MediPoint ? FlowFieldConstants.ID_OntvangerBevestigingsEmailAdresPatient : FlowFieldConstants.ID_OntvangerBevestigingsEmailAdres;

            return new ExternalSupplierOrder.Address
            {
                BSN = externalSupplierOrder.Client.CivilServiceNumber,
                BirthdateRaw = externalSupplierOrder.Client.BirthDate,
                Birthdate = externalSupplierOrder.Client.BirthDate.ToPointDateDisplay(),
                PolicyNumber = externalSupplierOrder.Client.InsuranceNumber,

                FirstName = string.IsNullOrEmpty(externalSupplierOrder.Client.FirstName) ? externalSupplierOrder.Client.Initials : externalSupplierOrder.Client.FirstName,
                LastName = externalSupplierOrder.Client.LastName,

                Street = GetValue(externalSupplierOrder.FlowFieldValues, FlowFieldConstants.ID_OntvangerStraat),
                HouseNumber = GetValue(externalSupplierOrder.FlowFieldValues, FlowFieldConstants.ID_OntvangerHuisnummer),
                HouseNumberAdditional = GetValue(externalSupplierOrder.FlowFieldValues, FlowFieldConstants.ID_OntvangerHuisnummerExtra),
                Postal = GetValue(externalSupplierOrder.FlowFieldValues, FlowFieldConstants.ID_OntvangerPostcode),
                City = GetValue(externalSupplierOrder.FlowFieldValues, FlowFieldConstants.ID_OntvangerPlaats),
                Country = _countryNL,

                Name = new ExternalSupplierOrder.Name { Lang = "", Text = externalSupplierOrder.Client.FullName() },
                PhoneNumber = externalSupplierOrder.Client.PhoneNumberGeneral,
                Email = GetValueOrDefault(externalSupplierOrder.FlowFieldValues, emailFieldID, externalSupplierOrder.Client.Email),

                InsuranceCompany = externalSupplierOrder.InsurerSupplierCode,
                InsuranceCompanyName = externalSupplierOrder.HealthInsurer?.Name
            };
        }

    }
}
