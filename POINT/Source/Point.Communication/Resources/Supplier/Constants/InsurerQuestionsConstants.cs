﻿
namespace Point.Communication.Resources.Supplier.Constants
{
    public static class InsurerQuestionsConstants
    {
        public static class VegroQuestionIDs
        {
            public const string Gewicht = "Gewicht";
            public const string GewichtTekst = "GewichtTekst";
            public const string Lengte = "Lengte";
            public const string LengteTekst = "LengteTekst";

            public static class GewichtValues
            {
                public const string LessThan80 = "1";
                public const string EqualOrLessThan120 = "2";
                public const string GreaterThan120 = "3";
            }

            public static class LengteValues
            {
                public const string LessThan150 = "1";
                public const string EqualOrLessThan175 = "2";
                public const string EqualOrLessThan190 = "2_";
                public const string GreaterThan190 = "3";
            }
        }

    }
}
