﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Point.Database.Models;
using Point.Database.Repository;

namespace Point.Communication.Resources.Supplier.Ordering
{
    public class ExternalSupplierOrder
    {
        public ExternalSupplierOrder()
        {
            FlowFieldValues = new List<FlowFieldValue>();
        }

        public int ClientID { get; set; } = -1;
        public int FormSetVersionID { get; set; } = -1;
        public int TransferID { get; set; } = -1;
        public int SupplierID { get; set; }
        public DateTime OrderDate { get; set; }
        public IUnitOfWork uow { get; set; }
        public IEnumerable<FlowFieldValue> FlowFieldValues { get; set; }
        public int EmployeeID { get; set; } = -1;
        public int OrganizationID { get; set; } = -1;
        public string OrganizationSettingSupplierCode { get; set; }
        public string SupplierName { get; set; }
        public string OrderName { get; set; }
        public string InsurerSupplierCode { get; set; }
        public Client Client { get; set; }
        public HealthInsurer HealthInsurer { get; set; }


        [XmlRoot(ElementName = "Credential")]
        public class Credential
        {
            [XmlElement(ElementName = "Identity")] public string Identity { get; set; }

            [XmlAttribute(AttributeName = "domain")]
            public string Domain { get; set; }
        }

        [XmlRoot(ElementName = "From")]
        public class From
        {
            [XmlElement(ElementName = "Credential")]
            public Credential Credential { get; set; }
        }

        [XmlRoot(ElementName = "To")]
        public class To
        {
            [XmlElement(ElementName = "Credential")]
            public Credential Credential { get; set; }
        }

        [XmlRoot(ElementName = "Header")]
        public class Header
        {
            [XmlElement(ElementName = "From")] public From From { get; set; }
            [XmlElement(ElementName = "To")] public To To { get; set; }
            [XmlElement(ElementName = "Sender")] public Sender Sender { get; set; }
        }

        [XmlRoot(ElementName = "Money")]
        public class Money
        {
            [XmlAttribute(AttributeName = "currency")]
            public string Currency { get; set; }

            [XmlText] public string Text { get; set; }
        }

        [XmlRoot(ElementName = "Total")]
        public class Total
        {
            [XmlElement(ElementName = "Money")] public Money Money { get; set; }
        }

        [XmlRoot(ElementName = "Name")]
        public class Name
        {
            [XmlAttribute(AttributeName = "lang", Namespace = "http://www.w3.org/XML/1998/namespace")]
            public string Lang { get; set; }

            [XmlText] public string Text { get; set; }
        }

        [XmlRoot(ElementName = "Address")]
        public class Address
        {
            [XmlElement(ElementName = "BSN")] public string BSN { get; set; }

            [XmlElement(ElementName = "Birthdate")]
            public string Birthdate { get; set; }
            public DateTime? BirthdateRaw { get; set; }

            [XmlElement(ElementName = "PolicyNumber")]
            public string PolicyNumber { get; set; }

            [XmlElement(ElementName = "TariffGroupCode")]
            public string TariffGroupCode { get; set; }

            [XmlElement(ElementName = "Name")] public Name Name { get; set; }

            [XmlElement(ElementName = "FirstName")]
            public string FirstName { get; set; }

            [XmlElement(ElementName = "LastName")] public string LastName { get; set; }
            [XmlElement(ElementName = "Street")] public string Street { get; set; }
            [XmlElement(ElementName = "Email")] public string Email { get; set; }

            [XmlElement(ElementName = "HouseNumber")]
            public string HouseNumber { get; set; }

            [XmlElement(ElementName = "HouseNumberAdditional")]
            public string HouseNumberAdditional { get; set; }

            [XmlElement(ElementName = "PhoneNumber")]
            public string PhoneNumber { get; set; }

            [XmlElement(ElementName = "Postal")] public string Postal { get; set; }
            [XmlElement(ElementName = "City")] public string City { get; set; }
            [XmlElement(ElementName = "Country")] public string Country { get; set; }

            [XmlElement(ElementName = "InsuranceCompany")]
            public string InsuranceCompany { get; set; }

            [XmlElement(ElementName = "InsuranceCompanyName")]
            public string InsuranceCompanyName { get; set; }
        }

        [XmlRoot(ElementName = "ShipTo")]
        public class ShipTo
        {
            [XmlElement(ElementName = "Address")] public Address Address { get; set; }
        }

        [XmlRoot(ElementName = "BillTo")]
        public class BillTo
        {
            [XmlElement(ElementName = "Address")] public Address Address { get; set; }
        }

        [XmlRoot(ElementName = "Extrinsic")]
        public class Extrinsic
        {
            [XmlAttribute(AttributeName = "name")] public string Name { get; set; }
            [XmlText] public string Text { get; set; }
        }

        [XmlRoot(ElementName = "Sender")]
        public class Sender
        {
            [XmlElement(ElementName = "Credential")]
            public Credential Credential { get; set; }

            [XmlElement(ElementName = "UserAgent")]
            public string UserAgent { get; set; }
        }

        [XmlRoot(ElementName = "Question")]
        public class Question
        {
            [XmlAttribute(AttributeName = "ipid")] public string Ipid { get; set; }

            [XmlAttribute(AttributeName = "extraitems")]
            public string Extraitems { get; set; }

            [XmlAttribute(AttributeName = "categoryid")]
            public string Categoryid { get; set; }

            [XmlText] public string Text { get; set; }
        }

        [XmlRoot(ElementName = "Questions")]
        public class Questions
        {
            [XmlElement(ElementName = "Question")] public List<Question> Question { get; set; }
        }

        [XmlRoot(ElementName = "ItemOut")]
        public class ItemOut
        {
            [XmlElement(ElementName = "ItemID")] public ItemID ItemID { get; set; }

            [XmlElement(ElementName = "ItemDetail")]
            public ItemDetail ItemDetail { get; set; }

            [XmlAttribute(AttributeName = "quantity")]
            public string Quantity { get; set; }

            [XmlAttribute(AttributeName = "lineNumber")]
            public string LineNumber { get; set; }
        }

        [XmlRoot(ElementName = "OrderRequestHeader")]
        public class OrderRequestHeader
        {
            [XmlElement(ElementName = "Total")] public Total Total { get; set; }
            [XmlElement(ElementName = "ShipTo")] public ShipTo ShipTo { get; set; }
            [XmlElement(ElementName = "BillTo")] public BillTo BillTo { get; set; }
            [XmlElement(ElementName = "Comments")] public string Comments { get; set; }

            [XmlElement(ElementName = "Extrinsic")]
            public Extrinsic Extrinsic { get; set; }

            [XmlElement(ElementName = "RequestedDeliveryDate")]
            public string RequestedDeliveryDate { get; set; }
            public DateTime? RequestedDeliveryDateRaw { get; set; }

            [XmlElement(ElementName = "BlockTimeCode")]
            public string BlockTimeCode { get; set; }

            [XmlElement(ElementName = "Transport1")]
            public string Transport1 { get; set; }

            [XmlElement(ElementName = "Transport2")]
            public string Transport2 { get; set; }

            [XmlElement(ElementName = "Transport3")]
            public string Transport3 { get; set; }

            [XmlElement(ElementName = "BIG_Number")]
            public string BIG_Number { get; set; }

            [XmlElement(ElementName = "PartnerCode")]
            public string PartnerCode { get; set; }

            [XmlElement(ElementName = "AGBCode")] public string AGBCode { get; set; }

            [XmlElement(ElementName = "DocumentType")]
            public string DocumentType { get; set; }

            [XmlElement(ElementName = "PointEmployeeName")]
            public string PointEmployeeName { get; set; }

            [XmlElement(ElementName = "PointEmployeePhoneNumber")]
            public string PointEmployeePhoneNumber { get; set; }

            [XmlElement(ElementName = "PointEmployeeEmail")]
            public string PointEmployeeEmail { get; set; }

            [XmlElement(ElementName = "Questions")]
            public Questions Questions { get; set; }

            [XmlAttribute(AttributeName = "orderID")]
            public string OrderID { get; set; }

            [XmlAttribute(AttributeName = "orderDate")]
            public string OrderDate { get; set; }
            public DateTime? OrderDateRaw { get; set; }

            [XmlAttribute(AttributeName = "type")] public string Type { get; set; }
        }

        [XmlRoot(ElementName = "ItemID")]
        public class ItemID
        {
            [XmlElement(ElementName = "SupplierPartID")]
            public string SupplierPartID { get; set; }
        }

        [XmlRoot(ElementName = "UnitPrice")]
        public class UnitPrice
        {
            [XmlElement(ElementName = "Money")] public Money Money { get; set; }
        }

        [XmlRoot(ElementName = "Description")]
        public class Description
        {
            [XmlAttribute(AttributeName = "lang", Namespace = "http://www.w3.org/XML/1998/namespace")]
            public string Lang { get; set; }

            [XmlText] public string Text { get; set; }
        }

        [XmlRoot(ElementName = "Classification")]
        public class Classification
        {
            [XmlAttribute(AttributeName = "domain")]
            public string Domain { get; set; }
        }

        [XmlRoot(ElementName = "ItemDetail")]
        public class ItemDetail
        {
            [XmlElement(ElementName = "UnitPrice")]
            public UnitPrice UnitPrice { get; set; }

            [XmlElement(ElementName = "Description")]
            public Description Description { get; set; }

            [XmlElement(ElementName = "UnitOfMeasure")]
            public string UnitOfMeasure { get; set; }

            [XmlElement(ElementName = "Classification")]
            public Classification Classification { get; set; }
        }

        [XmlRoot(ElementName = "OrderRequest")]
        public class OrderRequest
        {
            [XmlElement(ElementName = "OrderRequestHeader")]
            public OrderRequestHeader OrderRequestHeader { get; set; }

            [XmlElement(ElementName = "ItemOut")] public List<ItemOut> ItemOut { get; set; }

            [XmlElement(ElementName = "AuthorizationForm")]
            public string AuthorizationForm { get; set; }

            [XmlElement(ElementName = "PatientAuthorizationForRentalItems")]
            public string PatientAuthorizationForRentalItems { get; set; }
        }

        [XmlRoot(ElementName = "Request")]
        public class Request
        {
            [XmlElement(ElementName = "OrderRequest")]
            public OrderRequest OrderRequest { get; set; }
        }

        [XmlRoot(ElementName = "cXML")]
        public class CXML
        {
            [XmlElement(ElementName = "Header")] public Header Header { get; set; }
            [XmlElement(ElementName = "Request")] public Request Request { get; set; }

            [XmlAttribute(AttributeName = "version")]
            public string Version { get; set; }

            [XmlAttribute(AttributeName = "payloadID")]
            public string PayloadID { get; set; }

            [XmlAttribute(AttributeName = "timestamp")]
            public string Timestamp { get; set; }

            public int ClientID { get; set; }
            public string OrderName { get; set; }
            public bool Success { get; set; }
        }

    }
}
