﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Point.Communication.Resources.Supplier.Questions
{
    [XmlRoot(ElementName = "label")]
    public class Label
    {
        [XmlAttribute(AttributeName = "ipv")]
        public string Ipv { get; set; }
        [XmlText]
        public string Text { get; set; }
        [XmlAttribute(AttributeName = "extraItems")]
        public bool ExtraItems { get; set; }

        [XmlAttribute(AttributeName = "ipid")]
        public string Ipid { get; set; }
    }

    [XmlRoot(ElementName = "labels")]
    public class Labels
    {
        [XmlElement(ElementName = "label")]
        public List<Label> Label { get; set; }
    }

    [XmlRoot(ElementName = "question")]
    public class Question
    {
        [XmlElement(ElementName = "questionDescription")]
        public string QuestionDescription { get; set; }
        [XmlElement(ElementName = "labels")]
        public Labels Labels { get; set; }
        [XmlAttribute(AttributeName = "type")]
        public string Type { get; set; }
        [XmlAttribute(AttributeName = "ipid")]
        public string Ipid { get; set; }
        [XmlAttribute(AttributeName = "format")]
        public string Format { get; set; }
    }

    [XmlRoot(ElementName = "questionGroup")]
    public class QuestionGroup
    {
        [XmlElement(ElementName = "question")]
        public List<Question> Question { get; set; }
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }
        [XmlAttribute(AttributeName = "showOnlyFor")]
        public string ShowOnlyFor { get; set; }
    }

    [XmlRoot(ElementName = "productGroup")]
    public class ProductGroup
    {
        [XmlElement(ElementName = "questionGroup")]
        public List<QuestionGroup> QuestionGroup { get; set; }
        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }
        [XmlAttribute(AttributeName = "category")]
        public string Category { get; set; }
    }

    [XmlRoot(ElementName = "insurer")]
    public class Insurer
    {
        [XmlElement(ElementName = "productGroup")]
        public List<ProductGroup> ProductGroup { get; set; }
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }
    }

    [XmlRoot(ElementName = "insurers")]
    public class Insurers
    {
        [XmlElement(ElementName = "insurer")]
        public List<Insurer> Insurer { get; set; }
    }
}
