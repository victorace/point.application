﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Web.Script.Serialization;
using Point.Business.Helper;
using Point.Business.Logic;
using Point.Communication.Infrastructure.Helpers;
using Point.Communication.MediPointService;
using Point.Communication.Resources.Supplier.Ordering;
using Point.Database.Helpers;
using Point.Database.Models;
using Point.Database.Repository;
using Point.Infrastructure.Helpers;
using Point.Models.Enums;
using Point.ServiceBus.Models;

namespace Point.Communication
{
    public class SupplierSendOrder
    {
        public bool SendOrder(IUnitOfWork uow, SendExternalSupplierOrder objectData)
        {
            if (!SupplierHelper.IsExternalSupplier(objectData.SupplierID))
            {
                return false;
            }
            
            var externalSupplierXml = new ExternalSupplierXML(objectData.FormSetVersionID);

            if (externalSupplierXml.IsSentBefore(uow))
            {
                return false;
            }

            var resultXml = externalSupplierXml.CreateXML(uow, objectData);

            if (!resultXml.Success)
            {
                // TODO: This should be logged in log4Net
                new WebRequestLogBL(uow).LogWebRequest("", "", externalSupplierXml.Exception.Message);
                return false;
            }

            OrderRequestResult requestResult;
            switch (objectData.SupplierID)
            {
                case (int)SupplierID.Vegro:
                    requestResult = SendOrderVegro(uow, externalSupplierXml, objectData);
                    break;
                case (int)SupplierID.MediPoint:
                    var cXmlOrder = externalSupplierXml.GetCxmlOrder(uow, objectData);
                    requestResult = SendOrderMediPoint(uow, cXmlOrder, objectData);
                    break;
                default:
                    return false;
            }

            SetOrderStatus(uow, objectData, requestResult);
            SaveToLog(uow, externalSupplierXml, objectData, resultXml, requestResult.ErrorMessage);

            return requestResult.IsSuccessful;
        }

        private void SetOrderStatus(IUnitOfWork uow, SendExternalSupplierOrder objectData, OrderRequestResult requestResult)
        {
            switch (requestResult.RequestStatus)
            {
                case OrderRequestStatus.Sent:
                    UpdateOrderStatus(uow, objectData.FormSetVersionID, FlowFieldConstants.Value_OrderVerstuurd);
                    break;
                case OrderRequestStatus.SentSuccess:
                    UpdateOrderStatus(uow, objectData.FormSetVersionID, FlowFieldConstants.Value_OrderBevestigd);
                    break;
                case OrderRequestStatus.NotSentFailed:
                case OrderRequestStatus.SentFailed:
                    UpdateOrderStatus(uow, objectData.FormSetVersionID, FlowFieldConstants.Value_OrderMislukt);
                    break;
                case OrderRequestStatus.NotSent:
                    UpdateOrderStatus(uow, objectData.FormSetVersionID, FlowFieldConstants.Value_InBehandeling);
                    break;
            }

            if (requestResult.IsSuccessful)
            {
                var phaseInstance = PhaseInstanceBL.GetByFormsetVersionID(uow, objectData.FormSetVersionID);
                if (phaseInstance != null)
                {
                    var logEntry = LogEntryBL.Create((FlowFormType)objectData.ScreenID, objectData.SendingEmployeeID);
                    PhaseInstanceBL.SetStatusDone(uow, phaseInstance, logEntry);
                }
            }
            else //FAILED
            {
                var systemEmployee = PointUserInfoHelper.GetSystemEmployee(uow);
                var supplierName = objectData.SupplierName;
                var flowInstance = FlowInstanceBL.GetByTransferID(uow, objectData.TransferID);
                var message = $"De hulpmiddelen konden niet worden besteld, neem contact op met {supplierName} (zie formulier)";
                SignaleringBL.HandleSignalByFlowDefinitionFormTypeAction(uow, flowInstance, (int)FlowFormType.Hulpmiddelen, $"{supplierName}Error", systemEmployee, message);
            }
        }

        public void UpdateOrderStatus(IUnitOfWork uow, int formsetVersionID, string orderStatus)
        {
            var flowFieldValue = FlowFieldValueBL.GetByFlowFieldIDAndFormSetVersionID(uow, FlowFieldConstants.ID_OrderStatus, formsetVersionID).FirstOrDefault();
            if (flowFieldValue == null)
            {
                return;
            }
            flowFieldValue.Value = orderStatus;
            uow.Save();
        }

        private OrderRequestResult SendOrderVegro(IUnitOfWork uow, ExternalSupplierXML supplierXml, SendExternalSupplierOrder objectData)
        {
            if (!objectData.HasLoginInfo)
            {
                return GetOrderRequestResult(OrderRequestStatus.NotSentFailed, "Missing Url");
            }

            var url = ConfigHelper.GetAppSettingByName<string>(objectData.RequestUrlConfigSettingName);
            OrderRequestStatus requestStatus = OrderRequestStatus.NotSent;
            var errorMessage = string.Empty;
            var xml = supplierXml.XMLResult;
            var result = string.Empty;

            if (!string.IsNullOrEmpty(xml))
            {
                try
                {
                    using (var httpClient = new HttpClient())
                    {
                        using (var content = new StringContent(xml))
                        {
                            content.Headers.Clear();
                            content.Headers.Add("Content-Type", "application/x-www-form-urlencoded");

                            HttpResponseMessage response = httpClient.PostAsync(url, content).Result;

                            result = response.Content.ReadAsStringAsync().Result;
                            var success = result.StartsWith("SUCCESS", StringComparison.InvariantCultureIgnoreCase);
                            requestStatus = success ? OrderRequestStatus.SentSuccess : OrderRequestStatus.SentFailed;
                        }
                    }
                }
                catch (Exception ex)
                {
                    // TODO: Add error (log4net)
                    requestStatus = OrderRequestStatus.NotSentFailed;
                    errorMessage = ex.Message;
                }

                new WebRequestLogBL(uow).LogWebRequest(xml, result, errorMessage);
            }
            
            return GetOrderRequestResult(requestStatus, errorMessage);
        }

        private OrderRequestResult GetOrderRequestResult(OrderRequestStatus requestStatus, string errorMessage)
        {
            return new OrderRequestResult {RequestStatus = requestStatus, ErrorMessage = errorMessage};
        }

        private OrderRequestResult SendOrderMediPoint(IUnitOfWork uow, ExternalSupplierOrder.CXML cxmlOrder, SendExternalSupplierOrder objectData)
        {
            if (!objectData.HasLoginInfo)
            {
                return GetOrderRequestResult(OrderRequestStatus.NotSentFailed, "Missing login info (url/username/pw)");
            }
            
            var url = ConfigHelper.GetAppSettingByName<string>(objectData.RequestUrlConfigSettingName);
            var userName = ConfigHelper.GetAppSettingByName<string>(objectData.UserName);
            var password = ConfigHelper.GetAppSettingByName<string>(objectData.Password);
            var dateEmpty = DateTime.MinValue;

            DateTime birthDate = cxmlOrder.Request.OrderRequest.OrderRequestHeader.ShipTo.Address.BirthdateRaw ?? dateEmpty;
            DateTime orderDate = cxmlOrder.Request.OrderRequest.OrderRequestHeader.OrderDateRaw ?? dateEmpty;
            DateTime requestedDeliveryDate = cxmlOrder.Request.OrderRequest.OrderRequestHeader.RequestedDeliveryDateRaw ?? dateEmpty;

            if (string.IsNullOrEmpty(cxmlOrder.Request.OrderRequest.OrderRequestHeader.ShipTo.Address.HouseNumber))
            {
                return GetOrderRequestResult(OrderRequestStatus.NotSentFailed, "ShipTo.Address.HouseNumber is NOT filled");
            }

            OrderRequestStatus requestStatus;
            var errorMessage = string.Empty;
            var currentSecurityProtocol = ServicePointManager.SecurityProtocol;

            var requestBody = string.Empty;
            var responseBody = string.Empty;

            try
            {
                var basicAuthBinding = new BasicHttpBinding(BasicHttpSecurityMode.Transport);
                basicAuthBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
                var basicAuthEndpoint = new EndpointAddress(url);

                var client = new MisaPortClient(basicAuthBinding, basicAuthEndpoint);
                client.ClientCredentials.UserName.UserName = userName;
                client.ClientCredentials.UserName.Password = password;

                var requestType = new RequestType
                {
                    OrderRequest = new RequestTypeOrderRequest { OrderRequestHeader = new OrderRequestHeaderType() }
                };

                requestType.OrderRequest.OrderRequestHeader.BlockTimeCode = cxmlOrder.Request.OrderRequest.OrderRequestHeader.BlockTimeCode;
                requestType.OrderRequest.OrderRequestHeader.OrderDate = orderDate;
                requestType.OrderRequest.OrderRequestHeader.OrderId = cxmlOrder.Request.OrderRequest.OrderRequestHeader.OrderID;
                requestType.OrderRequest.OrderRequestHeader.CaregiverEmail = cxmlOrder.Request.OrderRequest.OrderRequestHeader.PointEmployeeEmail;
                requestType.OrderRequest.OrderRequestHeader.CaregiverName = cxmlOrder.Request.OrderRequest.OrderRequestHeader.PointEmployeeName;
                requestType.OrderRequest.OrderRequestHeader.CaregiverPhoneNumber = cxmlOrder.Request.OrderRequest.OrderRequestHeader.PointEmployeePhoneNumber;
                requestType.OrderRequest.OrderRequestHeader.RequestedDeliveryDate = requestedDeliveryDate;

                requestType.OrderRequest.OrderRequestHeader.ShipTo = new OrderRequestHeaderTypeShipTo
                {
                    Address = new AddressType
                    {
                        FirstName = cxmlOrder.Request.OrderRequest.OrderRequestHeader.ShipTo.Address.FirstName,
                        LastName = cxmlOrder.Request.OrderRequest.OrderRequestHeader.ShipTo.Address.LastName,
                        Bsn = cxmlOrder.Request.OrderRequest.OrderRequestHeader.ShipTo.Address.BSN,
                        Birthdate = birthDate,
                        Street = cxmlOrder.Request.OrderRequest.OrderRequestHeader.ShipTo.Address.Street,
                        Postal = CleanPostalCode(cxmlOrder.Request.OrderRequest.OrderRequestHeader.ShipTo.Address.Postal),
                        City = cxmlOrder.Request.OrderRequest.OrderRequestHeader.ShipTo.Address.City,
                        Country = cxmlOrder.Request.OrderRequest.OrderRequestHeader.ShipTo.Address.Country,
                        HouseNumber = cxmlOrder.Request.OrderRequest.OrderRequestHeader.ShipTo.Address.HouseNumber,
                        HouseNumberAdditional = cxmlOrder.Request.OrderRequest.OrderRequestHeader.ShipTo.Address.HouseNumberAdditional,
                        InsuranceCompany = cxmlOrder.Request.OrderRequest.OrderRequestHeader.ShipTo.Address.InsuranceCompany,
                        InsuranceCompanyName = cxmlOrder.Request.OrderRequest.OrderRequestHeader.ShipTo.Address.InsuranceCompanyName,
                        InsuredPersonNumber = cxmlOrder.Request.OrderRequest.OrderRequestHeader.ShipTo.Address.PolicyNumber,
                        PhoneNumber = cxmlOrder.Request.OrderRequest.OrderRequestHeader.ShipTo.Address.PhoneNumber,
                        AddressRemark = cxmlOrder.Request.OrderRequest.OrderRequestHeader.Transport1
                    }
                };

                var questions = new QuestionTypeQuestion[cxmlOrder.Request.OrderRequest.OrderRequestHeader.Questions.Question.Count];

                for (var i = 0; i < cxmlOrder.Request.OrderRequest.OrderRequestHeader.Questions.Question.Count; i++)
                {
                    var item = cxmlOrder.Request.OrderRequest.OrderRequestHeader.Questions.Question[i];

                    questions[i] = new QuestionTypeQuestion
                    {
                        id = item.Ipid,
                        ipv = item.Text
                    };
                }

                requestType.OrderRequest.OrderRequestHeader.Questions = questions;

                var products = new ProductTypeProduct[cxmlOrder.Request.OrderRequest.ItemOut.Count];

                for (var i = 0; i < cxmlOrder.Request.OrderRequest.ItemOut.Count; i++)
                {
                    var item = cxmlOrder.Request.OrderRequest.ItemOut[i];

                    products[i] = new ProductTypeProduct
                    {
                        ProductId = item.ItemID.SupplierPartID,
                        Quantity = Convert.ToInt32(item.Quantity)
                    };
                }

                requestType.OrderRequest.Products = products;

                string errorType;
                string errorText;
                int orderID = 0;

                const SecurityProtocolType MediPointSecurityProtocolType = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls; // comparable to modern browsers
                ServicePointManager.SecurityProtocol = MediPointSecurityProtocolType;

                // Add a HTTP Header to an outgoing request
                using (new OperationContextScope(client.InnerChannel))
                {
                    // Add a HTTP Header to an outgoing request
                    string auth = "Basic " + Convert.ToBase64String(Encoding.Default.GetBytes(client.ClientCredentials.UserName.UserName + ":" + client.ClientCredentials.UserName.Password));
                    var requestMessage = new HttpRequestMessageProperty();
                    requestMessage.Headers["Authorization"] = auth;
                    OperationContext.Current.OutgoingMessageProperties[HttpRequestMessageProperty.Name] = requestMessage;

                    var headerType = new HeaderType
                    {
                        From = new HeaderTypeFrom { Credential = new CredentialType { Identity = "POINT" } },
                        Sender = new HeaderTypeSender { Credential = new CredentialType { Identity = "POINT" } }
                    };

                    requestBody = new JavaScriptSerializer().Serialize(requestType);
                    
                    var response = client.SubmitRequest(headerType, requestType, out orderID, out errorType, out errorText);

                    if (response == (int)HttpStatusCode.OK)
                    {
                        requestStatus = OrderRequestStatus.SentSuccess;
                    }
                    else
                    {
                        errorMessage = "Error on order submit";
                        requestStatus = OrderRequestStatus.SentFailed;
                    }

                    responseBody = response.ToString();
                }

            }
            // TODO: Do something with the error message!
            catch (WebException ex)
            {
                requestStatus = OrderRequestStatus.NotSentFailed;
                errorMessage = ex.Message;
            }
            catch (Exception ex)
            {
                requestStatus = OrderRequestStatus.NotSentFailed;
                errorMessage = ex.Message;
            }

            // Restore default
            ServicePointManager.SecurityProtocol = currentSecurityProtocol;

            new WebRequestLogBL(uow).LogWebRequest(requestBody, responseBody, errorMessage);

            return GetOrderRequestResult(requestStatus, errorMessage);
        }

        // Intended/Expected format 1111AA
        private string CleanPostalCode(string postalCode)
        {
            return string.IsNullOrEmpty(postalCode) ? string.Empty : postalCode.Replace(" ", "").ToUpperInvariant();
        }

        private void SaveToLog(IUnitOfWork uow, ExternalSupplierXML externalSupplierXML, SendExternalSupplierOrder objectData, ExternalSupplierXML.ResultXml resultXml, string errorMessage)
        {
            CommunicationLogBL.Save(uow, new CommunicationLog
            {
                BCCAddress = "",
                CCAddress = "",
                ClientID = resultXml.ClientID,
                Content = externalSupplierXML.XMLResult,
                EmployeeID = objectData.SendingEmployeeID,
                OrganizationID = objectData.SendingOrganizationID,
                FromName = "",
                MailType = objectData.CommunicationLogType,
                Subject = resultXml.OrderName,
                Timestamp = DateTime.Now,
                ToAddress = ConfigHelper.GetAppSettingByName<string>(objectData.RequestUrlConfigSettingName),       //"VegroURL/MediPointURL"),
                TransferID = objectData.TransferID,
                ErrorMessage = errorMessage
            });

        }       

    }

    internal enum OrderRequestStatus
    {
        NotSent,
        NotSentFailed,
        Sent,
        SentFailed,
        SentSuccess
    }

    internal class OrderRequestResult
    {
        public string ErrorMessage { get; set; }
        public bool HasError => !string.IsNullOrEmpty(ErrorMessage);
        public bool IsSuccessful => RequestStatus == OrderRequestStatus.SentSuccess || RequestStatus == OrderRequestStatus.Sent;
        public OrderRequestStatus RequestStatus { get; set; }
    }

}
