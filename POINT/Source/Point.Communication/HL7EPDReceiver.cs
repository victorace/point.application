﻿using Point.Communication.HL7Service;
using Point.Database.Models;
using Point.Infrastructure.Extensions;
using Point.Log4Net;
using Point.Models.Enums;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Point.Communication
{
    public class HL7EPDReceiver
    {
        private static readonly ILogger logger = LogManager.GetLogger(typeof(HL7EPDReceiver));

        public Dictionary<string, string> Get(EndpointConfiguration endpointconfiguration, EPDRequest epdrequest)
        {
            if (endpointconfiguration == null)
            {
                throw new ArgumentNullException(nameof(endpointconfiguration));
            }

            if (epdrequest == null)
            {
                throw new ArgumentNullException(nameof(epdrequest));
            }

            var data = new Dictionary<string, string>();
            try
            {
                data = fetchdata(endpointconfiguration, epdrequest);
            }
            catch (Exception e)
            {
                logger.Fatal($"EndpointConfiguration({endpointconfiguration.EndpointConfigurationID}) - EPDRequest(BirthDate:{epdrequest.BirthDate},CivilServiceNumber:{epdrequest.CivilServiceNumber},Gender:{epdrequest.Gender},PatientNumber:{epdrequest.PatientNumber},VisitNumber:{epdrequest.VisitNumber})).", e);
            }

            return data;
        }

        public async Task<Dictionary<string, string>> GetAsync(EndpointConfiguration endpointconfiguration, EPDRequest epdrequest)
        {
            if (endpointconfiguration == null)
            {
                throw new ArgumentNullException(nameof(endpointconfiguration));
            }

            if (epdrequest == null)
            {
                throw new ArgumentNullException(nameof(epdrequest));
            }

            var data = new Dictionary<string, string>();
            try
            {
                await Task.Run(() =>
                {
                    data = fetchdata(endpointconfiguration, epdrequest);
                });
            }
            catch (Exception e)
            {
                logger.Fatal($"EndpointConfiguration({endpointconfiguration.EndpointConfigurationID}) - EPDRequest(BirthDate:{epdrequest.BirthDate},CivilServiceNumber:{epdrequest.CivilServiceNumber},Gender:{epdrequest.Gender},PatientNumber:{epdrequest.PatientNumber},VisitNumber:{epdrequest.VisitNumber})).", e);
            }

            return data;
        }

        private Dictionary<string, string> fetchdata(EndpointConfiguration endpointconfiguration, EPDRequest epdrequest)
        {
            var data = new Dictionary<string, string>();
            var hl7serviceclient = new HL7ServiceClient();

            // TODO: if..else won't scale well when we have lots of EndpointConfigurations - PP
            if (endpointconfiguration.FormTypeID == (int)FlowFormType.VerpleegkundigeOverdrachtFlow)
            {
                data = hl7serviceclient.GetEOverdracht(endpointconfiguration.OrganizationID, epdrequest);
            }
            else if (endpointconfiguration.FormTypeID == (int)FlowFormType.AanvraagFormulierZHVVT)
            {
                data = hl7serviceclient.GetRequestZHVVT(endpointconfiguration.OrganizationID, epdrequest);
            }

            if (!data.ContainsKey("source"))
            {
                // TODO: This should be handled server side - PP
                data.Add("source", ((int)Source.WEBSERVICE).ToString());
            }

            return data;
        }
    }
}
