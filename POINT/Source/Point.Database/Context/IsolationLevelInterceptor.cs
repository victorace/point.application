﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.Entity.Infrastructure.Interception;

namespace Point.Database.Context
{
    public class IsolationLevelInterceptor : DbCommandInterceptor
    {
        private IsolationLevel? _isolationLevel;

        public IsolationLevelInterceptor(bool useReadUncommited = false)
        {
            if (useReadUncommited)
            {
                _isolationLevel = IsolationLevel.ReadUncommitted;
            }
        }

        public override void ReaderExecuting(DbCommand command, DbCommandInterceptionContext<DbDataReader> interceptionContext)
        {
            SetTransaction(command);
        }

        private void SetTransaction(DbCommand command)
        {
            if(!String.IsNullOrEmpty(command?.CommandText) && _isolationLevel.HasValue && _isolationLevel.Value == IsolationLevel.ReadUncommitted)
            {
                command.CommandText = "SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;" + command.CommandText;
            }
            
        }
    }
}
