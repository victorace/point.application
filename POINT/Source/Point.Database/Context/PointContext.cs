using Point.Database.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

// ----------------------------------------------------------------------------------------
// ADDING A REPOSITORY? THEN PLEASE KEEP THE ORDER CORRECT (ALPHABETICALLY) - Thank you.
// ADDING A REPOSITORY? THEN PLEASE KEEP THE ORDER CORRECT (ALPHABETICALLY) - Thank you.
// ADDING A REPOSITORY? THEN PLEASE KEEP THE ORDER CORRECT (ALPHABETICALLY) - Thank you.
// ADDING A REPOSITORY? THEN PLEASE KEEP THE ORDER CORRECT (ALPHABETICALLY) - Thank you.
// ----------------------------------------------------------------------------------------

namespace Point.Database.Context
{
    public class PointContext : DbContext
    {
        public PointContext() : base("name=PointContext")
        {
        }

        public virtual DbSet<ActionCodeHealthInsurer> ActionCodeHealthInsurer { get; set; }
        public virtual DbSet<ActionCodeHealthInsurerFormType> ActionCodeHealthInsurerFormType { get; set; }
        public virtual DbSet<ActionCodeHealthInsurerList> ActionCodeHealthInsurerList { get; set; }
        public virtual DbSet<ActionCodeHealthInsurerRegio> ActionCodeHealthInsurerRegio { get; set; }
        public virtual DbSet<ActionGroupHealthInsurer> ActionGroupHealthInsurer { get; set; }
        public virtual DbSet<ActionHealthInsurer> ActionHealthInsurer { get; set; }
        public virtual DbSet<AfterCareCategory> AfterCareCategory { get; set; }
        public virtual DbSet<AfterCareFinancing> AfterCareFinancing { get; set; }
        public virtual DbSet<AfterCareGroup> AfterCareGroup { get; set; }
        public virtual DbSet<AfterCareTileGroup> AfterCareTileGroup { get; set; }
        public virtual DbSet<AfterCareType> AfterCareType { get; set; }
        public virtual DbSet<AidProduct> AidProduct { get; set; }
        public virtual DbSet<AidProductGroup> AidProductGroup { get; set; }
        public virtual DbSet<AidProductOrderAnswer> AidProductOrderAnswer { get; set; }
        public virtual DbSet<AidProductOrderItem> AidProductOrderItem { get; set; }
        public virtual DbSet<Aspnet_Applications> aspnet_Applications { get; set; }
        public virtual DbSet<Aspnet_Membership> aspnet_Membership { get; set; }
        public virtual DbSet<Aspnet_Roles> aspnet_Roles { get; set; }
        public virtual DbSet<Aspnet_SchemaVersions> aspnet_SchemaVersions { get; set; }
        public virtual DbSet<Aspnet_Users> aspnet_Users { get; set; }
        public virtual DbSet<AttachmentReceivedTemp> AttachmentReceivedTemp { get; set; }
        public virtual DbSet<AutoCreateSet> AutoCreateSet { get; set; }
        public virtual DbSet<AutoCreateSetDepartment> AutoCreateSetDepartment { get; set; }
        public virtual DbSet<CIZRelation> CIZRelation { get; set; }
        public virtual DbSet<Client> Client { get; set; }
        public virtual DbSet<ClientReceivedTemp> ClientReceivedTemp { get; set; }
        public virtual DbSet<ClientReceivedTempHL7> ClientReceivedTempHL7 { get; set; }
        public virtual DbSet<CommunicationLog> CommunicationLog { get; set; }
        public virtual DbSet<CommunicationQueue> CommunicationQueue { get; set; }
        public virtual DbSet<ContactPerson> ContactPerson { get; set; }
        public virtual DbSet<CryptoCertificate> CryptoCertificate { get; set; }
        public virtual DbSet<Department> Department { get; set; }
        public virtual DbSet<DepartmentCapacity> DepartmentCapacity { get; set; }
        public virtual DbSet<DepartmentCapacityPublic> DepartmentCapacityPublic { get; set; }
        public virtual DbSet<DepartmentType> DepartmentType { get; set; }
        public virtual DbSet<DigitalSignature> DigitalSignature { get; set; }
        public virtual DbSet<Doctor> Doctor { get; set; }
        public virtual DbSet<Employee> Employee { get; set; }
        public virtual DbSet<EmployeeDepartment> EmployeeDepartment { get; set; }
        public virtual DbSet<EndpointConfiguration> EndpointConfiguration { get; set; }
        public virtual DbSet<FieldReceivedTemp> FieldReceivedTemp { get; set; }
        public virtual DbSet<FieldReceivedValueMapping> FieldReceivedValueMappings { get; set; }
        public virtual DbSet<FlowDefinition> FlowDefinition { get; set; }
        public virtual DbSet<FlowDefinitionParticipation> FlowDefinitionParticipation { get; set; }
        public virtual DbSet<FlowField> FlowField { get; set; }
        public virtual DbSet<FlowFieldAttribute> FlowFieldAttribute { get; set; }
        public virtual DbSet<FlowFieldAttributeException> FlowFieldAttributeException { get; set; }
        public virtual DbSet<FlowFieldDashboard> FlowFieldDashboard { get; set; }
        public virtual DbSet<FlowFieldDataSource> FlowFieldDataSource { get; set; }
        public virtual DbSet<FlowFieldValue> FlowFieldValue { get; set; }
        public virtual DbSet<FlowInstance> FlowInstance { get; set; }
        public virtual DbSet<FlowDefinitionFormTypeFlowField> FlowDefinitionFormTypeFlowField { get; set; }
        public virtual DbSet<FlowInstanceDoorlopendDossierSearchValues> FlowInstanceDoorlopendDossierSearchValues { get; set; }
        public virtual DbSet<FlowInstanceOrganization> FlowInstanceOrganization { get; set; }
        public virtual DbSet<FlowInstanceReportValues> FlowInstanceReportValues { get; set; }
        public virtual DbSet<FlowInstanceReportValuesDump> FlowInstanceReportValuesDump { get; set; }
        public virtual DbSet<FlowInstanceReportValuesAttachmentDetail> FlowInstanceReportValuesAttachmentDetail { get; set; }
        public virtual DbSet<FlowInstanceReportValuesProjects> FlowInstanceReportValuesProjects { get; set; }
        public virtual DbSet<IguanaChannel> IguanaChannel { get; set; }
        public virtual DbSet<FlowInstanceSearchValues> FlowInstanceSearchValues { get; set; }
        public virtual DbSet<FlowInstanceStatus> FlowInstanceStatus { get; set; }
        public virtual DbSet<FlowInstanceTag> FlowInstanceTag { get; set; }
        public virtual DbSet<FlowPhaseAttribute> FlowPhaseAttribute { get; set; }
        public virtual DbSet<FlowType> FlowType { get; set; }
        public virtual DbSet<FormSetVersion> FormSetVersion { get; set; }
        public virtual DbSet<FormType> FormType { get; set; }
        public virtual DbSet<FormTypeOrganization> FormTypeOrganization { get; set; }      
        public virtual DbSet<FormTypeRegion> FormTypeRegion { get; set; }
        public virtual DbSet<FormTypeSelection> FormTypeSelection { get; set; }
        public virtual DbSet<FormTypeSelectionOrganization> FormTypeSelectionOrganization { get; set; }
        public virtual DbSet<FlowDefinitionFormType> FlowDefinitionFormType { get; set; }
        public virtual DbSet<Frequency> Frequency { get; set; }
        public virtual DbSet<FrequencyTransferAttachment> FrequencyTransferAttachment { get; set; }
        public virtual DbSet<FrequencyTransferMemo> FrequencyTransferMemo { get; set; }
        public virtual DbSet<FunctionRole> FunctionRole { get; set; }
        public virtual DbSet<GeneralAction> GeneralAction { get; set; }
        public virtual DbSet<GeneralActionPhase> GeneralActionPhase { get; set; }
        public virtual DbSet<GeneralActionType> GeneralActionType { get; set; }
        public virtual DbSet<GenericFile> GenericFile { get; set; }
        public virtual DbSet<GPSCoordinaat> GPSCoordinaat { get; set; }
        public virtual DbSet<GPSGemeente> GPSGemeente { get; set; }
        public virtual DbSet<GPSPlaats> GPSPlaats { get; set; }
        public virtual DbSet<GPSProvincie> GPSProvincie { get; set; }
        public virtual DbSet<HealthAidDevice> HealthAidDevice { get; set; }
        public virtual DbSet<HealthInsurer> HealthInsurer { get; set; }
        public virtual DbSet<HtmlText> HtmlText { get; set; }
        public virtual DbSet<Location> Location { get; set; }
        public virtual DbSet<LocationRegion> LocationRegion { get; set; }
        public virtual DbSet<LogCreateUser> LogCreateUser { get; set; }
        public virtual DbSet<LogDepartment> LogDepartment { get; set; }
        public virtual DbSet<Point.Database.Models.Logging> Logging { get; set; }
        public virtual DbSet<LoginHistory> LoginHistory { get; set; }
        public virtual DbSet<LogLocation> LogLocation { get; set; }
        public virtual DbSet<LogOrganization> LogOrganization { get; set; }
        public virtual DbSet<LogPointFormService> LogPointFormService { get; set; }
        public virtual DbSet<LogPrivacy> LogPrivacy { get; set; }
        public virtual DbSet<LogRead> LogRead { get; set; }
        public virtual DbSet<Medicine> Medicine { get; set; }
        public virtual DbSet<MedicineCard> MedicineCard { get; set; }
        public virtual DbSet<MessageReceivedTemp> MessageReceivedTemp { get; set; }
        public virtual DbSet<MutActionHealthInsurer> MutActionHealthInsurer { get; set; }
        public virtual DbSet<MutClient> MutClient { get; set; }
        public virtual DbSet<MutDepartment> MutDepartment { get; set; }
        public virtual DbSet<MutEmployee> MutEmployee { get; set; }
        public virtual DbSet<MutFlowFieldValue> MutFlowFieldValue { get; set; }
        public virtual DbSet<MutFormSetVersion> MutFormSetVersion { get; set; }
        public virtual DbSet<MutLocation> MutLocation { get; set; }
        public virtual DbSet<MutMedicineCard> MutMedicineCard { get; set; }
        public virtual DbSet<MutOrganization> MutOrganization { get; set; }
        public virtual DbSet<MutTransferTask> MutTransferTask { get; set; }
        public virtual DbSet<Nationality> Nationality { get; set; }
        public virtual DbSet<Organization> Organization { get; set; }
        public virtual DbSet<OrganizationInvite> OrganizationInvite { get; set; }
        public virtual DbSet<OrganizationLogo> OrganizationLogo { get; set; }
        public virtual DbSet<OrganizationOutsidePoint> OrganizationOutsidePoint { get; set; }
        public virtual DbSet<OrganizationProject> OrganizationProject { get; set; }
        public virtual DbSet<OrganizationSearch> OrganizationSearch { get; set; }
        public virtual DbSet<OrganizationSearchParticipation> OrganizationSearchParticipation { get; set; }
        public virtual DbSet<OrganizationSearchPostalCode> OrganizationSearchPostalCode { get; set; }
        public virtual DbSet<OrganizationSearchRegion> OrganizationSearchRegion { get; set; }
        public virtual DbSet<OrganizationSetting> OrganizationSetting { get; set; }
        public virtual DbSet<OrganizationCSVTemplate> OrganizationCSVTemplate { get; set; }
        public virtual DbSet<OrganizationCSVTemplateColumn> OrganizationCSVTemplateColumn { get; set; }
        public virtual DbSet<OrganizationType> OrganizationType { get; set; }
        public virtual DbSet<PageLock> PageLock { get; set; }
        public virtual DbSet<PatientRequestTemp> PatienRequestTemp { get; set; }
        public virtual DbSet<PatientReceivedTemp> PatientReceivedTemp { get; set; }
        public virtual DbSet<PersonData> PersonData { get; set; }
        public virtual DbSet<PhaseDefinition> PhaseDefinition { get; set; }
        public virtual DbSet<PhaseDefinitionNavigation> PhaseDefinitionNavigation { get; set; }
        public virtual DbSet<PhaseDefinitionRights> PhaseDefinitionRights { get; set; }
        public virtual DbSet<PhaseInstance> PhaseInstance { get; set; }
        public virtual DbSet<PostalCode> PostalCode { get; set; }
        public virtual DbSet<PreferredLocation> PreferredLocation { get; set; }
        public virtual DbSet<RazorTemplate> RazorTemplate { get; set; }
        public virtual DbSet<Region> Region { get; set; }

        public virtual DbSet<RegionAfterCareType> RegionAfterCareType { get; set; }
        public virtual DbSet<ReportDefinition> ReportDefinition { get; set; }
        public virtual DbSet<ScheduleHistory> ScheduleHistory { get; set; }
        public virtual DbSet<Screen> Screen { get; set; }
        public virtual DbSet<SearchUIConfiguration> SearchUIConfigurations { get; set; }
        public virtual DbSet<SendAttachment> SendAttachment { get; set; }
        public virtual DbSet<SendFormType> SendFormType { get; set; }
        public virtual DbSet<ServiceArea> ServiceArea { get; set; }
        public virtual DbSet<ServiceAreaPostalCode> ServiceAreaPostalCode { get; set; }
        public virtual DbSet<ServiceBusLog> ServiceBusLog { get; set; }
        public virtual DbSet<Signalering> Signalering { get; set; }
        public virtual DbSet<SignaleringDestination> SignaleringDestination { get; set; }
        public virtual DbSet<SignaleringTarget> SignaleringTarget { get; set; }
        public virtual DbSet<SignaleringTrigger> SignaleringTrigger { get; set; }
        public virtual DbSet<SignedPhaseInstance> SignedPhaseInstance { get; set; }
        public virtual DbSet<Specialism> Specialism { get; set; }
        public virtual DbSet<Supplier> Supplier { get; set; }
        public virtual DbSet<SystemMessage> SystemMessage { get; set; }
        public virtual DbSet<SystemMessageRead> SystemMessageRead { get; set; }
        public virtual DbSet<Template> Template { get; set; }
        public virtual DbSet<TemplateDefault> TemplateDefault { get; set; }
        public virtual DbSet<TemplateType> TemplateType { get; set; }
        public virtual DbSet<Templet> Templet { get; set; }
        public virtual DbSet<Transfer> Transfer { get; set; }
        public virtual DbSet<TransferAttachment> TransferAttachment { get; set; }
        public virtual DbSet<TransferMemo> TransferMemo { get; set; }
        public virtual DbSet<TransferMemoChanges> TransferMemoChanges { get; set; }
        public virtual DbSet<TransferMemoTemplate> TransferMemoTemplate { get; set; }
        public virtual DbSet<TransferTask> TransferTask { get; set; }
        public virtual DbSet<UsedHashCodes> UsedHashCodes { get; set; }
        public virtual DbSet<UsedMFACode> UsedMFACode { get; set; }
        public virtual DbSet<Validation> Validation { get; set; }
        public virtual DbSet<ViewTransferHistory> ViewTransferHistory { get; set; }
        public virtual DbSet<WebRequestLog> WebRequestLog { get; set; }
        public virtual DbSet<ZorgAdviesItem> ZorgAdviesItem { get; set; }
        public virtual DbSet<ZorgAdviesItemGroup> ZorgAdviesItemGroup { get; set; }
        public virtual DbSet<ZorgAdviesItemValue> ZorgAdviesItemValue { get; set; }
        public void ModelCreatedFromDatabase(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ActionCodeHealthInsurer>()
                .Property(e => e.Complex)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ActionCodeHealthInsurer>()
                .Property(e => e.ActionCodeName)
                .IsUnicode(false);

            modelBuilder.Entity<ActionCodeHealthInsurer>()
                .Property(e => e.ActionCodeHealthInsurerNumber)
                .IsUnicode(false);

            modelBuilder.Entity<ActionCodeHealthInsurer>()
                .HasMany(e => e.ActionCodeHealthInsurerFormType)
                .WithRequired(e => e.ActionCodeHealthInsurer)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ActionCodeHealthInsurerRegio>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<ActionGroupHealthInsurer>()
                .Property(e => e.ActionGroupName)
                .IsUnicode(false);

            modelBuilder.Entity<ActionHealthInsurer>()
                .Property(e => e.Unit)
                .IsUnicode(false);

            modelBuilder.Entity<ActionHealthInsurer>()
                .Property(e => e.Definition)
                .IsUnicode(false);

            modelBuilder.Entity<Aspnet_Applications>()
                .HasMany(e => e.aspnet_Membership)
                .WithRequired(e => e.aspnet_Applications)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Aspnet_Applications>()
                .HasMany(e => e.aspnet_Roles)
                .WithRequired(e => e.aspnet_Applications)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<FlowInstance>()
                .HasOptional(e => e.FlowInstanceSearchValues)
                .WithRequired(e => e.FlowInstance);

            modelBuilder.Entity<Aspnet_Applications>()
                .HasMany(e => e.aspnet_Users)
                .WithRequired(e => e.aspnet_Applications)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Aspnet_Roles>()
                .HasMany(e => e.aspnet_Users)
                .WithMany(e => e.aspnet_Roles)
                .Map(m => m.ToTable("aspnet_UsersInRoles").MapLeftKey("RoleId").MapRightKey("UserId"));

            modelBuilder.Entity<Aspnet_Users>()
                .HasOptional(e => e.aspnet_Membership)
                .WithRequired(e => e.aspnet_Users);

            modelBuilder.Entity<CIZRelation>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<Client>()
                .Property(e => e.CivilServiceNumber)
                .IsUnicode(false);

            modelBuilder.Entity<Client>()
                .Property(e => e.PatientNumber)
                .IsUnicode(false);

            modelBuilder.Entity<Client>()
                .Property(e => e.Salutation)
                .IsUnicode(false);

            modelBuilder.Entity<Client>()
                .Property(e => e.Initials)
                .IsUnicode(false);

            modelBuilder.Entity<Client>()
                .Property(e => e.FirstName)
                .IsUnicode(false);

            modelBuilder.Entity<Client>()
                .Property(e => e.MiddleName)
                .IsUnicode(false);

            modelBuilder.Entity<Client>()
                .Property(e => e.LastName)
                .IsUnicode(false);

            modelBuilder.Entity<Client>()
                .Property(e => e.MaidenName)
                .IsUnicode(false);

            modelBuilder.Entity<Client>()
                .Property(e => e.InsuranceNumber)
                .IsUnicode(false);

            modelBuilder.Entity<Client>()
                .Property(e => e.StreetName)
                .IsOptional()
                .IsUnicode(false);

            modelBuilder.Entity<Client>()
                .Property(e => e.Number)
                .IsUnicode(false);

            modelBuilder.Entity<Client>()
                .Property(e => e.PostalCode)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Client>()
                .Property(e => e.City)
                .IsUnicode(false);

            modelBuilder.Entity<Client>()
                .Property(e => e.Country)
                .IsUnicode(false);

            modelBuilder.Entity<Client>()
                .Property(e => e.PhoneNumberGeneral)
                .IsUnicode(false);

            modelBuilder.Entity<Client>()
                .Property(e => e.PhoneNumberMobile)
                .IsUnicode(false);

            modelBuilder.Entity<Client>()
                .Property(e => e.PhoneNumberWork)
                .IsUnicode(false);

            modelBuilder.Entity<Client>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<Client>()
                .Property(e => e.ContactPersonName)
                .IsUnicode(false);

            modelBuilder.Entity<Client>()
                .Property(e => e.ContactPersonRelationType)
                .IsUnicode(false);

            modelBuilder.Entity<Client>()
                .Property(e => e.ContactPersonPhoneNumberGeneral)
                .IsUnicode(false);

            modelBuilder.Entity<Client>()
                .Property(e => e.ContactPersonPhoneNumberMobile)
                .IsUnicode(false);

            modelBuilder.Entity<Client>()
                .Property(e => e.ContactPersonPhoneWork)
                .IsUnicode(false);

            modelBuilder.Entity<Client>()
                .Property(e => e.GeneralPractitionerName)
                .IsUnicode(false);

            modelBuilder.Entity<Client>()
                .Property(e => e.GeneralPractitionerPhoneNumber)
                .IsUnicode(false);

            modelBuilder.Entity<Client>()
                .Property(e => e.PharmacyName)
                .IsUnicode(false);

            modelBuilder.Entity<Client>()
                .Property(e => e.PharmacyPhoneNumber)
                .IsUnicode(false);

            modelBuilder.Entity<Client>()
                .Property(e => e.HealthCareProvider)
                .IsUnicode(false);

            modelBuilder.Entity<Client>()
                .Property(e => e.ContactPersonEmail)
                .IsUnicode(false);

            modelBuilder.Entity<Client>()
                .Property(e => e.ContactPersonName2)
                .IsUnicode(false);

            modelBuilder.Entity<Client>()
                .Property(e => e.ContactPersonRelationType2)
                .IsUnicode(false);

            modelBuilder.Entity<Client>()
                .Property(e => e.ContactPersonPhoneNumberGeneral2)
                .IsUnicode(false);

            modelBuilder.Entity<Client>()
                .Property(e => e.ContactPersonPhoneNumberMobile2)
                .IsUnicode(false);

            modelBuilder.Entity<Client>()
                .Property(e => e.ContactPersonPhoneNumberWork2)
                .IsUnicode(false);

            modelBuilder.Entity<Client>()
                .Property(e => e.ContactPersonEmail2)
                .IsUnicode(false);

            modelBuilder.Entity<Client>()
                .Property(e => e.ContactPersonName3)
                .IsUnicode(false);

            modelBuilder.Entity<Client>()
                .Property(e => e.ContactPersonRelationType3)
                .IsUnicode(false);

            modelBuilder.Entity<Client>()
                .Property(e => e.ContactPersonPhoneNumberGeneral3)
                .IsUnicode(false);

            modelBuilder.Entity<Client>()
                .Property(e => e.ContactPersonPhoneNumberMobile3)
                .IsUnicode(false);

            modelBuilder.Entity<Client>()
                .Property(e => e.ContactPersonPhoneNumberWork3)
                .IsUnicode(false);

            modelBuilder.Entity<Client>()
                .Property(e => e.ContactPersonEmail3)
                .IsUnicode(false);

            modelBuilder.Entity<Client>()
                .Property(e => e.AddressGPForZorgmail)
                .IsUnicode(false);

            modelBuilder.Entity<CommunicationLog>()
                .Property(e => e.FromAddress)
                .IsUnicode(false);

            modelBuilder.Entity<CommunicationLog>()
                .Property(e => e.FromName)
                .IsUnicode(false);

            modelBuilder.Entity<CommunicationLog>()
                .Property(e => e.ToAddress)
                .IsUnicode(false);

            modelBuilder.Entity<CommunicationLog>()
                .Property(e => e.ToName)
                .IsUnicode(false);

            modelBuilder.Entity<CommunicationLog>()
                .Property(e => e.CCAddress)
                .IsUnicode(false);

            modelBuilder.Entity<CommunicationLog>()
                .Property(e => e.BCCAddress)
                .IsUnicode(false);

            modelBuilder.Entity<CommunicationLog>()
                .Property(e => e.Subject)
                .IsUnicode(false);

            modelBuilder.Entity<CommunicationLog>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<Department>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Department>()
                .Property(e => e.PhoneNumber)
                .IsUnicode(false);

            modelBuilder.Entity<Department>()
                .Property(e => e.FaxNumber)
                .IsUnicode(false);

            modelBuilder.Entity<Department>()
                .Property(e => e.EmailAddress)
                .IsUnicode(false);

            modelBuilder.Entity<Department>()
                .Property(e => e.EmailAddressTransferSend)
                .IsUnicode(false);

            modelBuilder.Entity<Department>()
                .Property(e => e.ZorgmailTransferSend)
                .IsUnicode(false);

            modelBuilder.Entity<Department>()
                .Property(e => e.Information)
                .IsUnicode(false);
            
            modelBuilder.Entity<Department>()
                .HasMany(e => e.Employee1)
                .WithRequired(e => e.DefaultDepartment)
                .HasForeignKey(e => e.DepartmentID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DepartmentCapacity>()
                .Property(e => e.DepartmentName)
                .IsUnicode(false);

            modelBuilder.Entity<DepartmentCapacity>()
                .Property(e => e.Information)
                .IsUnicode(false);

            modelBuilder.Entity<DepartmentCapacityPublic>()
                .Property(e => e.DepartmentName)
                .IsUnicode(false);

            modelBuilder.Entity<DepartmentCapacityPublic>()
                .Property(e => e.Information)
                .IsUnicode(false);

            modelBuilder.Entity<DepartmentType>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<DepartmentType>()
                .HasMany(e => e.Department)
                .WithRequired(e => e.DepartmentType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Doctor>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Doctor>()
                .Property(e => e.SearchName)
                .IsUnicode(false);

            modelBuilder.Entity<Doctor>()
                .Property(e => e.AGB)
                .IsUnicode(false);

            modelBuilder.Entity<Doctor>()
                .Property(e => e.BIG)
                .IsUnicode(false);

            modelBuilder.Entity<Doctor>()
                .Property(e => e.PhoneNumber)
                .IsUnicode(false);

            modelBuilder.Entity<Employee>()
                .Property(e => e.FirstName)
                .IsUnicode(false);

            modelBuilder.Entity<Employee>()
                .Property(e => e.MiddleName)
                .IsUnicode(false);

            modelBuilder.Entity<Employee>()
                .Property(e => e.LastName)
                .IsUnicode(false);

            modelBuilder.Entity<Employee>()
                .Property(e => e.Position)
                .IsUnicode(false);

            modelBuilder.Entity<Employee>()
                .Property(e => e.PhoneNumber)
                .IsUnicode(false);

            modelBuilder.Entity<Employee>()
                .Property(e => e.EmailAddress)
                .IsUnicode(false);

            modelBuilder.Entity<Employee>()
                .Property(e => e.ExternID)
                .IsUnicode(false);

            modelBuilder.Entity<Employee>()
                .Property(e => e.BIG)
                .IsUnicode(false);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.CapacityDepartments)
                .WithRequired(e => e.AdjustmentCapacityEmployee)
                .HasForeignKey(e => e.UserID);

            modelBuilder.Entity<FlowDefinition>()
                .HasMany(e => e.ReportDefinitionFlowDefinition)
                .WithRequired(e => e.FlowDefinition)
                .HasForeignKey(e => e.FlowDefinitionID);

            modelBuilder.Entity<ReportDefinition>()
                .HasMany(e => e.ReportDefinitionFlowDefinition)
                .WithRequired(e => e.ReportDefinition)
                .HasForeignKey(e => e.ReportDefinitionID);

            modelBuilder.Entity<ZorgAdviesItemGroup>()
                .HasMany(e => e.ZorgAdviesItemGroupChildren)
                .WithOptional(e => e.ZorgAdviesItemGroupParent)
                .HasForeignKey(e => e.ParentZorgAdviesItemGroupID);

            modelBuilder.Entity<PhaseDefinition>()
                .HasMany(e => e.PhaseDefinitionNavigation)
                .WithRequired(e => e.PhaseDefinition)
                .HasForeignKey(e => e.PhaseDefinitionID);

            modelBuilder.Entity<PhaseDefinition>()
                .HasMany(e => e.PreviousPhaseDefinitionNavigation)
                .WithRequired(e => e.NextPhaseDefinition)
                .HasForeignKey(e => e.NextPhaseDefinitionID);

            modelBuilder.Entity<FlowField>()
                .HasMany(e => e.RequiredForFlowFieldAttribute)
                .WithOptional(e => e.RequiredByFlowField)
                .HasForeignKey(e => e.RequiredByFlowFieldID);

            modelBuilder.Entity<FlowField>()
                .HasMany(e => e.RequiredForFlowFieldAttributeException)
                .WithOptional(e => e.RequiredByFlowField)
                .HasForeignKey(e => e.RequiredByFlowFieldID);

            modelBuilder.Entity<FlowInstance>()
                .HasOptional(e => e.FlowInstanceReportValues)
                .WithRequired(e => e.FlowInstance);

            modelBuilder.Entity<FlowInstanceReportValues>()
                .HasOptional(e => e.FlowInstanceReportValuesDump)
                .WithRequired(e => e.FlowInstanceReportValues);

            modelBuilder.Entity<FormSetVersion>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<FormType>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<FormType>()
                .Property(e => e.URL)
                .IsUnicode(false);

            modelBuilder.Entity<FormType>()
                .HasMany(e => e.ActionCodeHealthInsurerFormType)
                .WithRequired(e => e.FormType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<FormType>()
                .HasMany(e => e.FormTypeOrganization)
                .WithRequired(e => e.FormType)
                .WillCascadeOnDelete(false);

            //Additional information: The ForeignKeyAttribute on property 'TransferAttachment' on type 
            //'Point.Database.Models.FrequencyTransferAttachment' is not valid. 
            //The foreign key name 'AttachmentID' was not found on the dependent type 'Point.Database.Models.FrequencyTransferAttachment'.
            //The Name value should be a comma separated list of foreign key property names.

            modelBuilder.Entity<HealthInsurer>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Location>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Location>()
                .Property(e => e.StreetName)
                .IsUnicode(false);

            modelBuilder.Entity<Location>()
                .Property(e => e.Number)
                .IsUnicode(false);

            modelBuilder.Entity<Location>()
                .Property(e => e.Country)
                .IsUnicode(false);

            modelBuilder.Entity<Location>()
                .Property(e => e.PostalCode)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Location>()
                .Property(e => e.City)
                .IsUnicode(false);

            modelBuilder.Entity<Location>()
                .Property(e => e.CapacityInfo)
                .IsUnicode(false);

            modelBuilder.Entity<Location>()
                .Property(e => e.IPAddress)
                .IsUnicode(false);

            modelBuilder.Entity<Location>()
                .Property(e => e.Area)
                .IsUnicode(false);

            modelBuilder.Entity<Location>()
                .HasMany(e => e.Department)
                .WithRequired(e => e.Location)
                .HasForeignKey(e => e.LocationID)
                .WillCascadeOnDelete(false);
            
            modelBuilder.Entity<LoginHistory>()
                .Property(e => e.LoginInfo)
                .IsUnicode(false);

            modelBuilder.Entity<LoginHistory>()
                .Property(e => e.LoginBrowser)
                .IsUnicode(false);

            modelBuilder.Entity<LoginHistory>()
                .Property(e => e.ClientIP)
                .IsUnicode(false);

            modelBuilder.Entity<LoginHistory>()
                .Property(e => e.LocalAddress)
                .IsUnicode(false);

            modelBuilder.Entity<Medicine>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Medicine>()
                .Property(e => e.Code)
                .IsUnicode(false);

            modelBuilder.Entity<Medicine>()
                .HasMany(e => e.MedicineCard)
                .WithRequired(e => e.Medicine)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<MedicineCard>()
                .Property(e => e.NotRegisteredMedicine)
                .IsUnicode(false);

            modelBuilder.Entity<MedicineCard>()
                .Property(e => e.Dosage)
                .IsUnicode(false);

            modelBuilder.Entity<MedicineCard>()
                .Property(e => e.Frequence)
                .IsUnicode(false);

            modelBuilder.Entity<MedicineCard>()
                .Property(e => e.Prescription)
                .IsUnicode(false);

            modelBuilder.Entity<Nationality>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Organization>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Organization>()
                .Property(e => e.AGB)
                .IsUnicode(false);

            modelBuilder.Entity<Organization>()
                .Property(e => e.URL)
                .IsUnicode(false);

            modelBuilder.Entity<Organization>()
                .Property(e => e.ImageName)
                .IsUnicode(false);

            modelBuilder.Entity<Organization>()
                .Property(e => e.SSOKey)
                .IsUnicode(false);

            modelBuilder.Entity<Organization>()
                .Property(e => e.ZorgmailUsername)
                .IsUnicode(false);

            modelBuilder.Entity<Organization>()
                .Property(e => e.ZorgmailPassword)
                .IsUnicode(false);

            modelBuilder.Entity<Organization>()
                .Property(e => e.HashPrefix)
                .IsUnicode(false);

            modelBuilder.Entity<Organization>()
                .HasMany(e => e.FormTypeOrganization)
                .WithRequired(e => e.Organization)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OrganizationType>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<PageLock>()
                .Property(e => e.URL)
                .IsUnicode(false);

            modelBuilder.Entity<PageLock>()
                .Property(e => e.SessionID)
                .IsUnicode(false);

            modelBuilder.Entity<PostalCode>()
                .Property(e => e.StartPostalCode)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<PostalCode>()
                .Property(e => e.EndPostalCode)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<PostalCode>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<Region>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<ServiceArea>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<Specialism>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<AfterCareType>()
                .HasMany(e => e.Department1)
                .WithOptional(e => e.AfterCareType1)
                .HasForeignKey(e => e.AfterCareType1ID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SystemMessage>()
                .Property(e => e.Title)
                .IsUnicode(false);

            modelBuilder.Entity<SystemMessage>()
                .Property(e => e.Message)
                .IsUnicode(false);

            modelBuilder.Entity<SystemMessage>()
                .Property(e => e.FunctionCode)
                .IsUnicode(false);

            modelBuilder.Entity<Template>()
                .Property(e => e.TemplateText)
                .IsUnicode(false);

            modelBuilder.Entity<TemplateDefault>()
                .Property(e => e.TemplateDefaultText)
                .IsUnicode(false);

            modelBuilder.Entity<TemplateType>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Templet>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<TransferMemo>()
                .Property(e => e.EmployeeName)
                .IsUnicode(false);

            modelBuilder.Entity<TransferMemo>()
                .Property(e => e.MemoContent)
                .IsUnicode(false);

            modelBuilder.Entity<TransferMemo>()
                .Property(e => e.Target)
                .IsUnicode(false);

            modelBuilder.Entity<TransferMemoChanges>()
                .Property(e => e.OriginalContent)
                .IsUnicode(false);

            modelBuilder.Entity<TransferMemoChanges>()
                .Property(e => e.EmployeeName)
                .IsUnicode(false);

            modelBuilder.Entity<UsedHashCodes>()
                .Property(e => e.HashCode)
                .IsUnicode(false);

            modelBuilder.Entity<OrganizationProject>()
                .Property(e => e.Name)
                .IsUnicode(false);
        }

        /// <summary>
        /// Overrules the 'required'-attribute set on some of the entities, to keep the original columns nullable
        /// </summary>
        /// <param name="modelBuilder"></param>
        public void ModelCreatedOverrideRequired(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Client>()
              .Property(e => e.CivilServiceNumber)
              .IsOptional();

            modelBuilder.Entity<Client>()
              .Property(e => e.Gender)
              .IsOptional();

            modelBuilder.Entity<Client>()
              .Property(e => e.Initials)
              .IsOptional();

            modelBuilder.Entity<Client>()
              .Property(e => e.LastName)
              .IsOptional();

            modelBuilder.Entity<Client>()
              .Property(e => e.BirthDate)
              .IsOptional();

            modelBuilder.Entity<Client>()
              .Property(e => e.HealthInsuranceCompanyID)
              .IsOptional();

            modelBuilder.Entity<Client>()
              .Property(e => e.InsuranceNumber)
              .IsOptional();

            modelBuilder.Entity<Client>()
              .Property(e => e.StreetName)
              .IsOptional();

            modelBuilder.Entity<Client>()
              .Property(e => e.Number)
              .IsOptional();

            modelBuilder.Entity<Client>()
              .Property(e => e.PostalCode)
              .IsOptional();

            modelBuilder.Entity<Client>()
              .Property(e => e.City)
              .IsOptional();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            ModelCreatedFromDatabase(modelBuilder);
            ModelCreatedOverrideRequired(modelBuilder);
        }
    }

    public class PointSearchContext : PointContext
    {
        public PointSearchContext()
        {
            base.Configuration.AutoDetectChangesEnabled = false;
            base.Database.CommandTimeout = 180;
        }
    }
}