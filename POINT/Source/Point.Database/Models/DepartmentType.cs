using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Point.Database.Repository;

namespace Point.Database.Models
{
    [Table("DepartmentType")]
    public partial class DepartmentType : BaseEntity
    {
        public DepartmentType()
        {
            Department = new HashSet<Department>();
        }

        public int DepartmentTypeID { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        public virtual ICollection<Department> Department { get; set; }
        
    }
}
