﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Point.Database.Repository;

namespace Point.Database.Models
{
    public class FlowType : BaseEntity, ILogEntry 
    {
        public int FlowTypeID { get; set; }
        [StringLength(255)]
        public string Name { get; set; }
        [StringLength(4000)]
        public string Description { get; set; }

        public int? EmployeeID { get; set; }
        public DateTime? Timestamp { get; set; }
        [StringLength(255)]
        public string ScreenName { get; set; }
        
    }

}
