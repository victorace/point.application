﻿using Point.Models.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Point.Database.Models
{
    public class FlowInstanceOrganization
    {
        [Key, Column(Order = 0)]
        public int FlowInstanceID { get; set; }
        [Key, Column(Order = 1)]
        public FlowDirection FlowDirection { get; set; }
        public int OrganizationID { get; set; }
        public int LocationID { get; set; }
        [Key, Column(Order = 2)]
        public int DepartmentID { get; set; }
        [Key, Column(Order = 3)]
        public InviteType InviteType { get; set; }
        [Key, Column(Order = 4)]
        public InviteStatus InviteStatus { get; set; }
        public DateTime? InviteSend { get; set; }
        public bool ClientIsAnonymous { get; set; }
        public string HandlingBy { get; set; }
        public string HandlingRemark { get; set; }
        public DateTime? HandlingDateTime { get; set; }

        public virtual FlowInstanceReportValues FlowInstanceReportValues { get; set; }
        public virtual FlowInstanceSearchValues FlowInstanceSearchValues { get; set; }
        public virtual Organization Organization { get; set; }
        public virtual Location Location { get; set; }
        public virtual Department Department { get; set; }
    }
}
