﻿using Point.Log.Attributes;
using Point.Log.Interfaces;
using Point.Models.LDM.EOverdracht30.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Point.Database.Models
{
    [Description("Autocreate account")]
    public class AutoCreateSet : ILoggable
    {
        public AutoCreateSet()
        {
            AutoCreateSetDepartment = new HashSet<AutoCreateSetDepartment>();
        }

        [SkipLog]
        public int AutoCreateSetID { get; set; }

        [SkipLog]
        public int LocationID { get; set; }

        [DisplayName("Unieke Code")]
        public string AutoCreateCode { get; set; }

        [DisplayName("Naam")]
        public string Name { get; set; }

        [DisplayName("Default voor organisatie")]
        public bool IsDefaultForOrganization { get; set; }

        [DisplayName("Prefix")]
        public string UsernamePrefix { get; set; }

        [DisplayName("Standaard geslacht")]
        public Geslacht DefaultGender { get; set; }

        [DisplayName("Standaard telefoon")]
        public string DefaultPhoneNumber { get; set; }

        [DisplayName("Standaard email")]
        public string DefaultEmailAddress { get; set; }

        [DisplayName("Standaard wachtwoord (gecodeerd)")]
        [CustomLog(CustomLogValue = "*verborgen*")]
        public string DefaultPassword { get; set; }

        [DisplayName("Standaard dossier niveau")]
        public string DefaultDossierLevel { get; set; }

        [DisplayName("Wachtwoord aanpassen na 1e aanmelding")]
        public bool ChangePasswordByEmployee { get; set; }

        [DisplayName("MSVT bewerken")]
        public bool MSVTEdit { get; set; }

        public DateTime? ModifiedTimeStamp { get; set; }

        [DisplayName("Alleen lezen bij Versturen")]
        public bool ReadOnlySender { get; set; }

        [DisplayName("Alleen lezen bij Ontvangen")]
        public bool ReadOnlyReciever { get; set; }

        public virtual ICollection<AutoCreateSetDepartment> AutoCreateSetDepartment { get; set; }
        public virtual Location Location { get; set; }
    }
}
