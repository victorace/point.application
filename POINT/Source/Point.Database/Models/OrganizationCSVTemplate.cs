﻿using Point.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Point.Database.Models
{
    public class OrganizationCSVTemplate
    {
        public OrganizationCSVTemplate()
        {
            OrganizationCSVTemplateColumn = new HashSet<OrganizationCSVTemplateColumn>();
        }

        public int OrganizationCSVTemplateID { get; set; }
        public int OrganizationID { get; set; }
        public CSVTemplateTypeID CSVTemplateTypeID { get; set; }

        public virtual ICollection<OrganizationCSVTemplateColumn> OrganizationCSVTemplateColumn { get; set; }
        public virtual Organization Organization { get; set; }
    }
}
