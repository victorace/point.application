﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Point.Database.Repository;

namespace Point.Database.Models
{
    [Table("AfterCareCategory")]
    public class AfterCareCategory : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AfterCareCategoryID { get; set; }
        [StringLength(255), Required]
        public string Name { get; set; }
        [StringLength(10), Required]
        public string Abbreviation { get; set; }
        [Required]
        public int Sortorder { get; set; }
    }
}
