﻿using System.ComponentModel.DataAnnotations.Schema;
using Point.Database.Repository;

namespace Point.Database.Models
{
    public class AfterCareTypeFormType : BaseEntity
    {
        public int AfterCareTypeFormTypeID { get; set; }
        public int AfterCareTypeID { get; set; }
        public int FormTypeID { get; set; }
    }
}
