﻿using Point.Database.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Point.Database.Models
{
    public class LogCreateUser
    {
        public int LogCreateUserID { get; set; }
        public int? OrganizationID{ get; set; }
        public int EmployeeID { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public string Position { get; set; }
        public string PhoneNumber { get; set; }
        public string EmailAddress { get; set; }
        public string ExternID { get; set; }
        public string UserName { get; set; }
        public string UserNamePrefix { get; set; }
        public string RoleName { get; set; }
        public bool ChangePasswordByEmployee{ get; set; }
        public string DepartmentID { get; set; }
        public string LocationID { get; set; }
        public string LocationName { get; set; }
        public string DepartmentName { get; set; }
        public string UrlInlog { get; set; }
        public string ExtraParametersUrl { get; set; }
        public string HashCode { get; set; }
        public string IP { get; set; }
        public string Comment { get; set; }
        public bool UserCreated { get; set; }
        public DateTime? TimeStamp { get; set; }
    }
}
