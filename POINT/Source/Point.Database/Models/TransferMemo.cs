using Point.Database.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Point.Database.Models
{
    [Table("TransferMemo")]
    public partial class TransferMemo : BaseEntity
    {
        public int TransferMemoID { get; set; }

        public int? EmployeeID { get; set; }

        [StringLength(255)]
        public string EmployeeName { get; set; }

        public DateTime? MemoDateTime { get; set; }

        [Column(TypeName = "text")]
        public string MemoContent { get; set; }

        [StringLength(255)]
        public string Target { get; set; }

        public int? TransferID { get; set; }

        public bool Deleted { get; set; }
        public int? RelatedMemoID { get; set; }
        public virtual ICollection<FrequencyTransferMemo> FrequencyTransferMemo { get; set; }
        
    }
}
