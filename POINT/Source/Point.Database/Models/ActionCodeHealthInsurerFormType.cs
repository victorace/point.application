using System.ComponentModel.DataAnnotations.Schema;
using Point.Database.Repository;

namespace Point.Database.Models
{
    [Table("ActionCodeHealthInsurerFormType")]
    public partial class ActionCodeHealthInsurerFormType : BaseEntity
    {
        public int ActionCodeHealthInsurerFormTypeID { get; set; }

        public int ActionCodeHealthInsurerID { get; set; }

        public int FormTypeID { get; set; }

        public virtual ActionCodeHealthInsurer ActionCodeHealthInsurer { get; set; }

        public virtual FormType FormType { get; set; }
    }
}
