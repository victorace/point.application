﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Point.Database.Models
{
    public class FlowInstanceTag
    {
        [Column(Order = 0), Key, ForeignKey("FlowInstance")]
        public int FlowInstanceID { get; set; }
        [Column(Order = 1), Key]
        public string Tag { get; set; }

        public virtual FlowInstance FlowInstance { get; set; }
    }
}
