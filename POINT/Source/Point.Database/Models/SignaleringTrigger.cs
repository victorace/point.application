﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Point.Models.Enums;

namespace Point.Database.Models
{
    public class SignaleringTrigger
    {
        public int SignaleringTriggerID { get; set; }
        public FlowDefinitionID FlowDefinitionID { get; set; }
        public int? FormTypeID { get; set; }
        public int? GeneralActionID { get; set; }
        [ForeignKey("GeneralActionID")]
        public virtual GeneralAction GeneralAction { get; set; }
        
        //what needs to be done
        public string Name { get; set; }
        //The full description of what needs to be done
        public string Description { get; set; }
        public bool ShowPrompt { get; set; }
        public int? MinActivePhase { get; set; }
        public int? MaxActivePhase { get; set; }

        public ICollection<SignaleringTarget> SignaleringTarget { get; set; }
        
    }
}
