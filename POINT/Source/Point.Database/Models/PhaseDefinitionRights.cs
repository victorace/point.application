﻿using System.ComponentModel.DataAnnotations.Schema;
using Point.Database.Repository;
using Point.Models.Enums;

namespace Point.Database.Models
{
    public class PhaseDefinitionRights : BaseEntity
    {
        public int PhaseDefinitionRightsID { get; set; }

        public int PhaseDefinitionID { get; set; }
        [ForeignKey("PhaseDefinitionID")]
        public virtual PhaseDefinition PhaseDefinition { get; set; }

        public int DepartmentTypeID { get; set; }
        [ForeignKey("DepartmentTypeID")]
        public virtual DepartmentType DepartmentType { get; set; }

        public int? OrganizationTypeID { get; set; }
        [ForeignKey("OrganizationTypeID")]
        public virtual OrganizationType OrganizationType { get; set; }

        public FlowDirection FlowDirection { get; set; }

        public string MaxRights { get; set; }
        
    }

}
