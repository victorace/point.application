﻿using Point.Database.Repository;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Point.Database.Models
{
    public class LogOrganization : BaseEntity, ILogEntryWithID
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int LogOrganizationID { get; set; }

        public int OrganizationID { get; set; }

        public DateTime? TimeStamp { get; set; }

        public int? EmployeeID { get; set; }
        
        [Required]
        [StringLength(1)]    
        public string Action { get; set; }

        public int? ScreenID { get; set; }

        public int? Version { get; set; }

        public virtual Organization Organization { get; set; }

        public virtual Employee Employee { get; set; }
        
    }
}
