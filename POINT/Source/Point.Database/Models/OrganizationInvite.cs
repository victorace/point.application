﻿using Point.Models.Enums;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Point.Database.Models
{
    public class OrganizationInvite
    {
        public int OrganizationInviteID { get; set; }
        public int FlowInstanceID { get; set; }
        public int? SendingPhaseDefinitionID { get; set; }
        public int RecievingPhaseDefinitionID { get; set; }
        public int OrganizationID { get; set; }
        public int DepartmentID { get; set; }
        public InviteStatus InviteStatus { get; set; }
        public int InviteOrder { get; set; }
        public DateTime InviteCreated { get; set; }
        public DateTime? InviteSend { get; set; }
        public bool NeedsAcknowledgement { get; set; }
        public InviteType InviteType { get; set; }
        public int CreatedByID { get; set; }
        public string HandlingBy { get; set; }
        public string HandlingRemark { get; set; }
        public DateTime? HandlingDateTime { get; set; }

        public FlowInstanceReportValues FlowInstanceReportValues { get; set; }
        public virtual FlowInstance FlowInstance { get; set; }
        [ForeignKey("SendingPhaseDefinitionID")]
        public virtual PhaseDefinition SendingPhaseDefinition { get; set; }
        [ForeignKey("RecievingPhaseDefinitionID")]
        public virtual PhaseDefinition RecievingPhaseDefinition { get; set; }
        public virtual Organization Organization { get; set; }
        public virtual Department Department { get; set; }
    }
}
