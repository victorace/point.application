﻿namespace Point.Database.Models
{
    public class AidProductOrderAnswer
    {
        public int AidProductOrderAnswerID { get; set; }
        public string QuestionID { get; set; }
        public string Value { get; set; }
        public int UniqueFormKey { get; set; }
        public int? FormSetVersionID { get; set; }
        public string ProductGroupCode { get; set; }
        public string ExtraItemsGroupCode { get; set; }
        public bool ExtraItems { get; set; }
    }
}
