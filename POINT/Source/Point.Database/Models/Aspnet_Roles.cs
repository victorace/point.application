using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Point.Database.Repository;

namespace Point.Database.Models
{
    public partial class Aspnet_Roles : BaseEntity 
    {
        public Aspnet_Roles()
        {
            aspnet_Users = new HashSet<Aspnet_Users>();
        }

        public Guid ApplicationId { get; set; }

        [Key]
        public Guid RoleId { get; set; }

        [Required]
        [StringLength(256)]
        public string RoleName { get; set; }

        [Required]
        [StringLength(256)]
        public string LoweredRoleName { get; set; }

        [StringLength(256)]
        public string Description { get; set; }

        public virtual Aspnet_Applications aspnet_Applications { get; set; }

        public virtual ICollection<Aspnet_Users> aspnet_Users { get; set; }
    }
}
