﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Point.Models.Enums;
using Point.Database.Repository;

namespace Point.Database.Models
{
    public class FlowInstance : BaseEntity, ILogEntry
    {
        public int FlowInstanceID { get; set; }

        public FlowDefinitionID FlowDefinitionID { get; set; }
        [ForeignKey("FlowDefinitionID")]
        public virtual FlowDefinition FlowDefinition { get; set; }

        public int TransferID { get; set; }
        [ForeignKey("TransferID")]
        public virtual Transfer Transfer { get; set; }

        public DateTime CreatedDate { get; set; }

        public int StartedByDepartmentID { get; set; }
        [ForeignKey("StartedByDepartmentID")]
        public virtual Department StartedByDepartment { get; set; }

        public int? StartedByOrganizationID { get; set; }
        [ForeignKey("StartedByOrganizationID")]
        public virtual Organization StartedByOrganization { get; set; }

        public int? StartedByEmployeeID { get; set; }
        [ForeignKey("StartedByEmployeeID")]
        public virtual Employee StartedByEmployee { get; set; }

        public int? EmployeeID { get; set; }
        public DateTime? Timestamp { get; set; }
        [StringLength(255)]
        public string ScreenName { get; set; }

        public int? OriginalFlowInstanceID { get; set; }
        public int? RelatedTransferID { get; set; }
        public bool Deleted { get; set; }
        public bool Interrupted { get; set; }
        public DateTime? InterruptedDate { get; set; }
        public int? InterruptedByEmployeeID { get; set; }

        public DateTime? DeletedDate { get; set; }

        public int? DeletedByEmployeeID { get; set; }

        public bool IsAcute { get; set; }

        public virtual ICollection<FlowFieldValue> FlowFieldValues { get; set; }

        public virtual ICollection<PhaseInstance> PhaseInstances { get; set; }

        public virtual FlowInstanceSearchValues FlowInstanceSearchValues { get; set; }
        public virtual FlowInstanceReportValues FlowInstanceReportValues { get; set; }

        public virtual ICollection<FlowInstanceStatus> FlowInstanceStatus { get; set; }
        public virtual ICollection<OrganizationInvite> OrganizationInvite { get; set; }
        public virtual ICollection<FlowInstanceTag> FlowInstanceTag { get; set; }
    }
}
