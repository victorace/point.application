using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Point.Database.Repository;

namespace Point.Database.Models
{
    [Table("DepartmentCapacity")]
    public partial class DepartmentCapacity : BaseEntity
    {
        public int DepartmentCapacityID { get; set; }

        public int? DepartmentID { get; set; }

        [StringLength(255)]
        public string DepartmentName { get; set; }

        public int? CapacityDepartment { get; set; }

        public DateTime? CapacityDepartmentDate { get; set; }

        public int? CapacityDepartmentNext { get; set; }

        public DateTime? CapacityDepartmentNextDate { get; set; }

        [StringLength(255)]
        public string Information { get; set; }

        public DateTime? AdjustmentCapacityDate { get; set; }

        public int? UserID { get; set; }

        //[StringLength(255)]
        //public string UserName { get; set; }

        public int? CapacityDepartment3 { get; set; }

        public DateTime? CapacityDepartment3Date { get; set; }

        public int? CapacityDepartment4 { get; set; }

        public DateTime? CapacityDepartment4Date { get; set; }

        [StringLength(3)]
        public string CapacityDepartmentHomeCare { get; set; }

        [StringLength(3)]
        public string CapacityDepartmentNextHomeCare { get; set; }

        [StringLength(3)]
        public string CapacityDepartment3HomeCare { get; set; }

        [StringLength(3)]
        public string CapacityDepartment4HomeCare { get; set; }

        public virtual Department Department { get; set; }
    }
}
