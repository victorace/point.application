using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Point.Database.Repository;

namespace Point.Database.Models
{
    public partial class Aspnet_SchemaVersions : BaseEntity 
    {
        [Key]
        [Column(Order = 0)]
        public string Feature { get; set; }

        [Key]
        [Column(Order = 1)]
        public string CompatibleSchemaVersion { get; set; }

        public bool IsCurrentVersion { get; set; }
    }
}
