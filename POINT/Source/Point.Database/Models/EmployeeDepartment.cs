﻿using Point.Log.Attributes;
using Point.Log.Interfaces;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Point.Database.Models
{
    [Description("Overige afdelingen")]
    public class EmployeeDepartment : ILoggable
    {
        [Key, Column(Order = 0)]
        [SkipLog]
        public int EmployeeID { get; set; }

        [Key, Column(Order = 1)]
        [DisplayName("Afdeling")]
        [LookUp("/Lookup/DepartmentInfo", "departmentid")]
        public int DepartmentID { get; set; }

        public DateTime? ModifiedTimeStamp { get; set; }

        public virtual Employee Employee { get; set; }
        public virtual Department Department { get; set; }
    }
}
