using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Point.Database.Repository;

namespace Point.Database.Models
{
    [Table("OrganizationType")]
    public partial class OrganizationType : BaseEntity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int OrganizationTypeID { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        public bool CanAddTransferPoint { get; set; }
        
    }
}
