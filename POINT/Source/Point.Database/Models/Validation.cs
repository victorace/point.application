﻿using System.ComponentModel.DataAnnotations;

namespace Point.Database.Models
{
    /// <summary>
    /// Defines a validation method (js) for one or more field attributes.
    /// The validation will raise a warning and/or error,
    /// triggered by some/all of the events: load, change=blur, save, next=definitive
    /// </summary>
    public class Validation
    {
        public int ValidationID { get; set; }

        [StringLength(100)]
        public string Method { get; set; }

        [StringLength(100)]
        public string FieldAttributes { get; set; }

        [StringLength(100)]
        public string WarningTriggers { get; set; }

        [StringLength(100)]
        public string ErrorTriggers { get; set; }

        [StringLength(1000)]
        public string Messages { get; set; }
    }
}
