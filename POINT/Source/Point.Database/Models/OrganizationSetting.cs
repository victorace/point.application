﻿using Point.Log.Attributes;
using Point.Log.Interfaces;
using System;
using System.ComponentModel;

namespace Point.Database.Models
{
    [Description("Organisatie instellingen")]
    public class OrganizationSetting : ILoggable
    {
        [SkipLog]
        public int OrganizationSettingID { get; set; }

        [DisplayName("Organisatie")]
        [LookUp("/Lookup/OrganizationInfo", "organizationid")]
        public int OrganizationID { get; set; }

        [DisplayName("Locatie")]
        [LookUp("/Lookup/LocationInfo", "locationid")]
        public int? LocationID { get; set; }

        [DisplayName("Afdeling")]
        [LookUp("/Lookup/DepartmentInfo", "departmentid")]
        public int? DepartmentID { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public DateTime? ModifiedTimeStamp { get; set; }
    }
}
