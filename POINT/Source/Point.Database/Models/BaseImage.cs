﻿namespace Point.Database.Models
{
    public class BaseDatabaseImage
    {
        public int ContentLength { get; set; }
        public string ContentType { get; set; }
        public string FileName { get; set; }
        public byte[] FileData { get; set; }
    }
}
