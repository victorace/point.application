﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Point.Database.Repository;

namespace Point.Database.Models
{
    public class ScheduleHistory : BaseEntity
    {
        public int ScheduleHistoryID { get; set; }

        public int FrequencyID { get; set; }
        [ForeignKey("FrequencyID")]
        public virtual Frequency Frequency { get; set; }

        public DateTime Timestamp { get; set; }

    }
}
