﻿using Point.Database.Repository;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Point.Database.Models
{
    public class SignedPhaseInstance: ILogEntry
    {
        public int SignedPhaseInstanceID { get; set; }

        public int PhaseInstanceID { get; set; }
        [ForeignKey("PhaseInstanceID")]
        public virtual PhaseInstance PhaseInstance { get; set; }

        public int SignedByID { get; set; }
        [ForeignKey("SignedByID")]
        public virtual Employee SignedBy { get; set; }

        public int DigitalSignatureID { get; set; }
        [ForeignKey("DigitalSignatureID")]
        public virtual DigitalSignature DigitalSignature { get; set; }

        public int? EmployeeID { get; set; }

        public DateTime? Timestamp { get; set; }

        public string ScreenName { get; set; }
        
    }
}
