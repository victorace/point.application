﻿using Point.Models.Enums;
using Point.Log.Attributes;
using Point.Log.Interfaces;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Point.Database.Models
{
    [Description("FormType per Regio")]
    public class FormTypeRegion : ILoggable
    {
        [Key, Column(Order = 0)]
        [LookUp("/Lookup/FormTypeInfo", "formtypeid")]
        public int FormTypeID { get; set; }

        [Key, Column(Order = 1)]
        [SkipLog]
        public int RegionID { get; set; }

        [Key, Column(Order = 2)]
        public FlowDefinitionID FlowDefinitionID { get; set; }

        public virtual Region Region { get; set; }
        public virtual FormType FormType { get; set; }
        public virtual FlowDefinition FlowDefinition { get; set; }
        public DateTime? ModifiedTimeStamp { get; set; }
    }
}
