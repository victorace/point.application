﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Point.Database.Models
{
    public class GenericFile
    {
        public Guid GenericFileID { get; set; }
        public string FileName { get; set; }
        public int FileSize { get; set; }
        public byte[] FileData { get; set; }
        public string ContentType { get; set; }
        public DateTime TimeStamp { get; set; }
        public bool Deleted { get; set; }
    }
}
