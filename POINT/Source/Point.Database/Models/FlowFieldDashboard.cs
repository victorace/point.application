﻿using System.ComponentModel.DataAnnotations.Schema;
using Point.Models.Enums;

namespace Point.Database.Models
{
    [Table("FlowFieldDashboard")]
    public class FlowFieldDashboard : BaseEntity
    {
        public int FlowFieldDashboardID { get; set; }
        public FlowDefinitionID FlowDefinitionID { get; set; }

        public int FlowFieldID { get; set; }
        public int FormTypeID { get; set; }
    }
}
