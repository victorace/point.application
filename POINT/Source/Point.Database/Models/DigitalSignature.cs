﻿using Point.Database.Repository;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Point.Database.Models
{
    public class DigitalSignature: ILogEntry, IInActive
    {
        public int DigitalSignatureID { get; set; }

        [Required]
        public int ContentLength { get; set; }

        [Required, StringLength(255)]
        public string ContentType { get; set; }

        [Required, StringLength(255)]
        public string FileName { get; set; }

        [Required, Column(TypeName = "image")]
        public byte[] FileData { get; set; }

        public bool InActive { get; set; }

        public int? EmployeeID { get; set; }
        public DateTime? Timestamp { get; set; }
        [StringLength(255)]
        public string ScreenName { get; set; }
        
    }
}
