using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Point.Database.Repository;

namespace Point.Database.Models
{
    [Table("FormSetVersion")]
    public partial class FormSetVersion : BaseEntity, ILogEntryWithID
    {
        public FormSetVersion()
        {
            ActionHealthInsurer = new HashSet<ActionHealthInsurer>();
        }

        public int FormSetVersionID { get; set; }

        public int? PhaseInstanceID { get; set; }

        public int TransferID { get; set; }

        public int FormTypeID { get; set; }

        public DateTime CreateDate { get; set; }

        [StringLength(400)]
        public string CreatedBy { get; set; }

        public int CreatedByID { get; set; }

        public DateTime? UpdateDate { get; set; }

        [StringLength(400)]
        public string UpdatedBy { get; set; }

        [StringLength(400)]
        public string Description { get; set; }

        public bool Deleted { get; set; }

        public int? UpdatedByID { get; set; }

        public int? ScreenID { get; set; }

        public virtual ICollection<ActionHealthInsurer> ActionHealthInsurer { get; set; }

        public virtual ICollection<TransferAttachment> TransferAttachment { get; set; }

        public virtual PhaseInstance PhaseInstance { get; set; }

        public virtual FormType FormType { get; set; }

        [ForeignKey("TransferID")]
        public virtual Transfer Transfer { get; set; }

        [NotMapped]
        public int? EmployeeID { get => UpdatedByID; set => UpdatedByID = value; }
        [NotMapped]
        public DateTime? TimeStamp { get => UpdateDate; set => UpdateDate = value; }
    }
}
