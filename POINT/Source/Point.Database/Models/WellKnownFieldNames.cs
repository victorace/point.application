﻿namespace Point.Database.Models
{
    public static class WellKnownFieldNames
    {
        public const string TransferID = "TransferID";
        public const string ClientFullname = "ClientFullname";
        public const string ClientCivilServiceNumber = "ClientCivilServiceNumber";
        public const string ClientBirthDate = "ClientBirthDate";
        public const string ClientGender = "ClientGender";
        public const string ClientInsuranceNumber = "ClientInsuranceNumber";
        public const string PatientNumber = "PatientNumber";
        public const string ReadModelTimeStamp = "ReadModelTimeStamp";

        public const string RequestFormZHVVTType = "RequestFormZHVVTType";
        public const string DatumEindeBehandelingMedischSpecialist = "DatumEindeBehandelingMedischSpecialist";
        public const string RequestTransferPointIntakeDate = "RequestTransferPointIntakeDate";
        public const string AcceptedBy = "AcceptedBy";
        public const string AcceptedByII = "AcceptedByII";
        public const string AcceptedByVVT = "AcceptedByVVT";
        public const string AcceptedDateTP = "AcceptedDateTP";
        public const string CareBeginDate = "CareBeginDate";
        public const string BehandelaarSpecialisme = "BehandelaarSpecialisme";
        public const string TransferDateVVT = "TransferDateVVT";
        public const string DischargeProposedStartDate = "DischargeProposedStartDate";
        public const string AcceptedByTelephoneNumber = "AcceptedByTelephoneNumber";
        public const string DischargePatientAcceptedYNByVVT = "DischargePatientAcceptedYNByVVT";
        public const string TransferDate = "TransferDate";
        public const string GewensteIngangsdatum = "GewensteIngangsdatum";
        public const string DestinationHospital = "DestinationHospital";
        public const string DestinationHospitalLocation = "DestinationHospitalLocation";
        public const string DestinationHospitalDepartment = "DestinationHospitalDepartment";
        public const string TransferCreatedByID = "TransferCreatedByID";
        public const string TransferCreatedBy = "TransferCreatedBy";
        public const string DossierOwnerID = "DossierOwnerID";
        public const string DossierOwnerName = "DossierOwnerName";
        public const string BehandelaarNaam = "BehandelaarNaam";
        public const string MedischeSituatieDatumOpname = "MedischeSituatieDatumOpname";
        public const string AcceptedByTelephoneVVT = "AcceptedByTelephoneVVT";
        public const string ZorgInZorginstellingVoorkeurPatient1 = "ZorgInZorginstellingVoorkeurPatient1";
        public const string HealthInsuranceCompany = "HealthInsuranceCompany";
        public const string HealthInsuranceCompanyID = "HealthInsuranceCompanyID";
        public const string HealthInsuranceCompanyUZOVICode = "HealthInsuranceCompanyUZOVICode";
        public const string MSVTActions = "MSVTActions";
        public const string MSVTMedication = "MSVTMedication";
        public const string GebruikPalliatieveSectie = "GebruikPalliatieveSectie";

        public const string LastTransferMemoID = "LastTransferMemoID";
        public const string LastTransferMemoDateTime = "LastTransferMemoDateTime";
        public const string LastTransferMemoEmployeeName = "LastTransferMemoEmployeeName";
        public const string LastTransferMemoContent = "LastTransferMemoContent";
        public const string TransferMemoCount = "TransferMemoCount";

        public const string LastAdditionalTPMemoID = "LastAdditionalTPMemoID";
        public const string LastAdditionalTPMemoDateTime = "LastAdditionalTPMemoDateTime";
        public const string LastAdditionalTPMemoEmployeeName = "LastAdditionalTPMemoEmployeeName";
        public const string LastAdditionalTPMemoContent = "LastAdditionalTPMemoContent";
        public const string AdditionalTPMemoCount = "AdditionalTPMemoCount";

        public const string LastFrequencyTransferMemoID = "LastFrequencyTransferMemoID";
        public const string LastFrequencyTransferMemoDateTime = "LastFrequencyTransferMemoDateTime";
        public const string LastFrequencyTransferMemoEmployeeName = "LastFrequencyTransferMemoEmployeeName";
        public const string LastFrequencyTransferMemoContent = "LastFrequencyTransferMemoContent";
        public const string LastFrequencyTransferMemoTarget = "LastFrequencyTransferMemoTarget";
        public const string FrequencyTransferMemoCount = "FrequencyTransferMemoCount";

        
        public const string LastFrequencyAttachmentID = "LastFrequencyAttachmentID";
        public const string LastFrequencyAttachmentUploadDate = "LastFrequencyAttachmentUploadDate";
        public const string LastFrequencyAttachmentSource = "LastFrequencyAttachmentSource";
        public const string FrequencyAttachmentCount = "FrequencyAttachmentCount";

        public const string FrequencyType = "FrequencyType";
        public const string FrequencyStartDate = "FrequencyStartDate";
        public const string FrequencyEndDate = "FrequencyEndDate";
        public const string FrequencyName = "FrequencyName";
        public const string FrequencyScopeType = "FrequencyScopeType";

        public const string LastGrzFormDateTime = "LastGrzFormDateTime";

        public const string LastTransferAttachmentID = "LastTransferAttachmentID";
        public const string LastTransferAttachmentUploadDate = "LastTransferAttachmentUploadDate";
        public const string LastTransferAttachmentType = "LastTransferAttachmentType";
        public const string TransferAttachmentCount = "TransferAttachmentCount";

        public const string TransferCreatedDate = "TransferCreatedDate";
        public const string CopyOfTransferID = "CopyOfTransferID";
        public const string FlowDefinitionName = "FlowDefinitionName";
        public const string FlowDefinitionID = "FlowDefinitionID";
        public const string DepartmentName = "DepartmentName";
        public const string FullDepartmentName = "FullDepartmentName";
        public const string LocationName = "LocationName";
        public const string OrganizationName = "OrganizationName";

        public const string LastPhaseInstanceID = "LastPhaseInstanceID";
        public const string LastPhaseStatus = "LastPhaseStatus";
        public const string LastPhaseEndStatus = "LastPhaseEndStatus";
        public const string LastPhaseRealizedDate = "LastPhaseRealizedDate";
        public const string LastPhaseDefinitionID = "LastPhaseDefinitionID";

        public const string Deleted = "Deleted";
        public const string IsInterrupted = "IsInterrupted";
        public const string ActivePhaseText = "ActivePhaseText";
        public const string VOStatus = "VOStatus";
        public const string VVTResultStatus = "VVTResultStatus";

        public const string HuisartsNaamAdres = "HuisartsNaamAdres";

        public const string DesiredHealthCareProvider = "DesiredHealthCareProvider";
        public const string FirstDesiredHealthCareProvider = "FirstDesiredHealthCareProvider";

        public const string TyperingNazorgCombined = "TyperingNazorgCombined";

        public const string TransferTaskDueDate = "TransferTaskDueDate";

        // Need the original (employee)ID for these fields
        public const string AcceptedByID = "AcceptedByID";
        public const string AcceptedByVVTID = "AcceptedByVVTID";
        public const string AcceptedByIIID = "AcceptedByIIID";
    }
}