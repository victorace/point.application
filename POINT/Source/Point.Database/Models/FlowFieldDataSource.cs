﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Point.Database.Models
{
    public class FlowFieldDataSource
    {
        public int FlowFieldDataSourceID { get; set; }
        public string JsonData { get; set; }
    }
}
