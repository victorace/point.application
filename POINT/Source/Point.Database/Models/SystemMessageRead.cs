using System;
using System.ComponentModel.DataAnnotations.Schema;
using Point.Database.Repository;

namespace Point.Database.Models
{
    public partial class SystemMessageRead : BaseEntity 
    {
        public int SystemMessageReadID { get; set; }

        public int EmployeeID { get; set; }

        public int SystemMessageID { get; set; }

        public DateTime DateRead { get; set; }
        
    }
}
