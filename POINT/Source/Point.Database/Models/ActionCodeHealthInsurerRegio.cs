using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Point.Database.Repository;

namespace Point.Database.Models
{
    [Table("ActionCodeHealthInsurerRegio")]
    public partial class ActionCodeHealthInsurerRegio : BaseEntity 
    {
        public ActionCodeHealthInsurerRegio()
        {
            ActionCodeHealthInsurerList = new HashSet<ActionCodeHealthInsurerList>();
        }

        [Key]
        public int ActionCodeHealthInsurerRegionID { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        public int? RegionID { get; set; }

        public DateTime? DateBegin { get; set; }

        public DateTime? DateEnd { get; set; }

        public virtual ICollection<ActionCodeHealthInsurerList> ActionCodeHealthInsurerList { get; set; }

        public virtual Region Region { get; set; }
    }
}
