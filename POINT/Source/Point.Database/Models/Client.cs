using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using Point.Database.Models.ViewModels;
using Point.Models.LDM.EOverdracht30.Enums;

namespace Point.Database.Models
{
    
    [Table("Client")]
    public partial class Client : BaseEntity
    {
        public Client()
        {
            ContactPerson = new HashSet<ContactPerson>();
        }

        [Key]
        public int ClientID { get; set; }

        [StringLength(9)]
        [DisplayName("Burger Service Nummer")]
        public string CivilServiceNumber { get; set; }

        [DisplayName("Heeft (nog) geen BSN")]
        public bool HasNoCivilServiceNumber { get; set; }

        [DisplayName("Pati�ntnummer")]
        [StringLength(50, ErrorMessage=errorMaxLength)]
        public string PatientNumber { get; set; }

        [DisplayName("Opnamenr.")]
        [StringLength(50, ErrorMessage = errorMaxLength)]
        public string VisitNumber { get; set; }

        [DisplayName("Geslacht")]
        [Required(ErrorMessage = errorRequired)]
        [RegularExpression("OM|OF|OUN", ErrorMessage= "Geslacht ('Man', 'Vrouw' of 'Ongedifferentieerd') is verplicht")]
        public Geslacht Gender { get; set; }

        [StringLength(255)]
        [DisplayName("Aanhef")]
        public string Salutation { get; set; }

        [StringLength(50)]
        [DisplayName("Voorletters")]
        [Required(ErrorMessage = errorRequired)]
        public string Initials { get; set; }

        [StringLength(255)]
        [DisplayName("Voornaam")]
        public string FirstName { get; set; }

        [StringLength(50)]
        [DisplayName("Voorvoegsel")]
        public string MiddleName { get; set; }

        [StringLength(255)]
        [DisplayName("Achternaam (volledig)")]
        [Required(ErrorMessage = errorRequired)]
        public string LastName { get; set; }

        [StringLength(255)]
        [DisplayName("Geboortenaam")]
        public string MaidenName { get; set; }

        [DisplayName("Geboortedatum")]
        [DataType(DataType.Date, ErrorMessage=errorDate), DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}")]
        [Required(ErrorMessage = errorRequired)]
        public DateTime? BirthDate { get; set; }

        [DisplayName("Zorgverzekeraar")]
        public int? HealthInsuranceCompanyID { get; set; }

        [StringLength(50)]
        [DisplayName("Polisnummer")]
        public string InsuranceNumber { get; set; }

        [StringLength(255)]
        [DisplayName("Straat")]
        public string StreetName { get; set; }

        [StringLength(50)]
        [DisplayName("Huisnummer")]
        public string Number { get; set; }

        [StringLength(50)]
        [DisplayName("Huisnummer toevoeging")]
        public string HuisnummerToevoeging { get; set; }

        [StringLength(10)]
        [DisplayName("Postcode")]
        public string PostalCode { get; set; }

        [StringLength(50)]
        [DisplayName("Postbus")]
        public string Postbus { get; set; }

        [StringLength(255)]
        [DisplayName("Plaats")]
        public string City { get; set; }

        [StringLength(255)]
        [DisplayName("Land")]
        public string Country { get; set; }

        [StringLength(50)]
        [DisplayName("Telefoonnummer 1")]
        public string PhoneNumberGeneral { get; set; }

        [StringLength(50)]
        [DisplayName("Telefoonnummer 2")]
        public string PhoneNumberMobile { get; set; }

        [StringLength(50)]
        [DisplayName("Telefoonnummer 3")]
        public string PhoneNumberWork { get; set; }

        [StringLength(255)]
        [DisplayName("E-mail")]
        public string Email { get; set; }

        [StringLength(255)]
        [DisplayName("Naam")]
        public string ContactPersonName { get; set; }

        [StringLength(255)]
        [DisplayName("Relatie tot de pati�nt")]
        public string ContactPersonRelationType { get; set; }

        [DisplayName("Geboortedatum")]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}")]
        public DateTime? ContactPersonBirthDate { get; set; }

        [StringLength(50)]
        [DisplayName("Telefoonnummer 1")]
        public string ContactPersonPhoneNumberGeneral { get; set; }

        [StringLength(50)]
        [DisplayName("Telefoonnummer 2")]
        public string ContactPersonPhoneNumberMobile { get; set; }

        [StringLength(50)]
        [DisplayName("Telefoonnummer 3")]
        public string ContactPersonPhoneWork { get; set; }

        [StringLength(255)]
        [DisplayName("Huisarts")]
        public string GeneralPractitionerName { get; set; }

        [StringLength(50)]
        [DisplayName("Telefoonnummer huisarts")]
        public string GeneralPractitionerPhoneNumber { get; set; }

        [StringLength(255)]
        [DisplayName("Apotheek")]
        public string PharmacyName { get; set; }

        [StringLength(50)]
        [DisplayName("Telefoonnummer apotheek")]
        public string PharmacyPhoneNumber { get; set; }

        [DisplayName("Burgerlijke staat")]
        public BurgerlijkeStaat? CivilClass { get; set; }

        [DisplayName("Samenstelling huishouding")]
        public Gezinssamenstelling? CompositionHousekeeping { get; set; }

        [DisplayName("Aantal kinderen in het gezin")]
        public int? ChildrenInHousekeeping { get; set; }

        [DisplayName("Woonsituatie")]
        public WoningType? HousingType { get; set; }
        public string HousingTypeComment { get; set; }

        [StringLength(255)]
        [DisplayName("Verzorgings- / verpleeginstelling")]
        public string HealthCareProvider { get; set; }

        public int? ClientCreatedBy { get; set; }

        public DateTime? ClientCreatedDate { get; set; }

        public int? ClientModifiedBy { get; set; }

        public DateTime? ClientModifiedDate { get; set; }

        [DisplayName("Nationaliteit")]
        public int? NationalityID { get; set; }

        [DisplayName("Taal")]
        public CommunicatieTaal? LanguageID { get; set; }

        public DateTime? TimeStamp { get; set; }

        public int? EmployeeID { get; set; }

        public int? ScreenID { get; set; }

        [StringLength(255)]
        [DisplayName("E-mail")]
        public string ContactPersonEmail { get; set; }

        [StringLength(255)]
        [DisplayName("Naam")]
        public string ContactPersonName2 { get; set; }

        [StringLength(255)]
        [DisplayName("Relatie tot de pati�nt")]
        public string ContactPersonRelationType2 { get; set; }

        [DisplayName("Geboortedatum")]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}")]
        public DateTime? ContactPersonBirthDate2 { get; set; }

        [StringLength(50)]
        [DisplayName("Telefoonnummer 1")]
        public string ContactPersonPhoneNumberGeneral2 { get; set; }

        [StringLength(50)]
        [DisplayName("Telefoonnummer 2")]
        public string ContactPersonPhoneNumberMobile2 { get; set; }

        [StringLength(50)]
        [DisplayName("Telefoonnummer 3")]
        public string ContactPersonPhoneNumberWork2 { get; set; }

        [StringLength(255)]
        [DisplayName("E-mail")]
        public string ContactPersonEmail2 { get; set; }

        [StringLength(255)]
        [DisplayName("Naam")]
        public string ContactPersonName3 { get; set; }

        [StringLength(255)]
        [DisplayName("Relatie tot de pati�nt")]
        public string ContactPersonRelationType3 { get; set; }

        [DisplayName("Geboortedatum")]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}")]
        public DateTime? ContactPersonBirthDate3 { get; set; }

        [StringLength(50)]
        [DisplayName("Telefoonnummer 1")]
        public string ContactPersonPhoneNumberGeneral3 { get; set; }

        [StringLength(50)]
        [DisplayName("Telefoonnummer 2")]
        public string ContactPersonPhoneNumberMobile3 { get; set; }

        [StringLength(50)]
        [DisplayName("Telefoonnummer 3")]
        public string ContactPersonPhoneNumberWork3 { get; set; }

        [StringLength(255)]
        [DisplayName("Email")]
        public string ContactPersonEmail3 { get; set; }

        public bool UsesPatientPortal { get; set; }

        public int? OriginalClientID { get; set; }

        [StringLength(255)]
        [DisplayName("Zorgmail adres huisarts")]
        public string AddressGPForZorgmail { get; set; }

        [StringLength(255)]
        [DisplayName("Straat")]
        //[Required(ErrorMessage = errorRequired)]
        public string TemporaryStreetName { get; set; }

        [StringLength(50)]
        [DisplayName("Huisnummer")]
        //[Required(ErrorMessage = errorRequired)]
        public string TemporaryNumber { get; set; }

        [StringLength(10)]
        [DisplayName("Postcode")]
        public string TemporaryPostalCode { get; set; }

        [StringLength(255)]
        [DisplayName("Plaats")]
        //[Required(ErrorMessage = errorRequired)]
        public string TemporaryCity { get; set; }

        [StringLength(255)]
        [DisplayName("Land")]
        public string TemporaryCountry { get; set; }

        [StringLength(255)]
        [DisplayName("Partnernaam")]
        public string PartnerName { get; set; }

        [StringLength(50)]
        [DisplayName("Voorvoegsel")]
        public string PartnerMiddleName { get; set; }

        [DisplayName("Cli�nt is anoniem")]
        public bool ClientIsAnonymous { get; set; }

        [JsonIgnore]
        public virtual ICollection<ContactPerson> ContactPerson { get; set; }

        [JsonIgnore]
        public virtual ICollection<Transfer> Transfer { get; set; }

        public AddressViewModel FullAdress()
        {
            return new AddressViewModel { Street = StreetName, HouseNumber = Number, HouseNumberExtra = HuisnummerToevoeging, Postbox = Postbus, PostalCode = PostalCode, City = City, Country = Country };
        }
    }
}
