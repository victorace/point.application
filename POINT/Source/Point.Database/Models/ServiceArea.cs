using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Point.Log.Attributes;
using Point.Log.Interfaces;

namespace Point.Database.Models
{
    [Table("ServiceArea"), Description("Bedieningsgebied")]
    public partial class ServiceArea : BaseEntity, ILoggable
    {
        public ServiceArea()
        {
            ServiceAreaPostalCode = new HashSet<ServiceAreaPostalCode>();
        }

        [SkipLog]
        public int ServiceAreaID { get; set; }

        [SkipLog]
        public int? OrganizationID { get; set; }

        [Required]
        [StringLength(255)]
        [DisplayName("Beschrijving")]
        public string Description { get; set; }

        [DisplayName("Inactief")]
        public bool Inactive { get; set; }

        public DateTime? ModifiedTimeStamp { get; set; }

        public virtual ICollection<ServiceAreaPostalCode> ServiceAreaPostalCode { get; set; }
        public virtual Organization Organization { get; set; }
    }
}
