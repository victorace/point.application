﻿

namespace Point.Database.Models
{
    public class RegionAfterCareType
    {
        public int RegionAfterCareTypeID { get; set; }
        public int RegionID { get; set; }
        public int AfterCareTypeID { get; set; }        
        public virtual AfterCareType AfterCareType { get; set; }
        public virtual Region Region { get; set; }
    }
}
