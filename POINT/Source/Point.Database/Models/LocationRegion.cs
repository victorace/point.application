﻿using Point.Log.Attributes;
using Point.Log.Interfaces;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Point.Database.Models
{
    [Description("Afwijkende regios")]
    public class LocationRegion : ILoggable
    {
        [Key, Column(Order = 0)]
        [SkipLog]
        public int LocationID { get; set; }

        [Key, Column(Order = 1)]
        [DisplayName("Regio")]
        [LookUp("/Lookup/RegionInfo", "regionid")]
        public int RegionID { get; set; }
        public DateTime? ModifiedTimeStamp { get; set; }

        public virtual Location Location { get; set; }
        public virtual Region Region { get; set; }
    }
}
