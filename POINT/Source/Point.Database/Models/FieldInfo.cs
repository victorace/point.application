using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Point.Database.Models
{
    /// <summary>
    /// Used by SearchUIConfiguration
    /// </summary>
    [ComplexType]
    public class FieldInfo
    {
        public bool IsWellKnown { get; set; }

        [StringLength(255)] // WellKnownFieldName / FlowField.Name
        public string Name { get; set; }
    }
}
