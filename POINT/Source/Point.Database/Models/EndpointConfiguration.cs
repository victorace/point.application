﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Point.Database.Models
{
    public class EndpointConfiguration
    {
        public int EndpointConfigurationID { get; set; }

        public int OrganizationID { get; set; }
        
        public int FormTypeID { get; set; }
        [ForeignKey("FormTypeID")]
        public virtual FormType FormType { get; set; }

        public string Model { get; set; }

        public string Method { get; set; }

        public string Address { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public string Label { get; set; }

        public bool Inactive { get; set; }

        public bool SkipCertificateCheck { get; set; }
    }
}