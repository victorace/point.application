﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Point.Database.Repository;

namespace Point.Database.Models
{
    public class AfterCareFinancing : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AfterCareFinancingID { get; set; }
        [StringLength(255), Required]
        public string Name { get; set; }
        [StringLength(10), Required]
        public string Abbreviation { get; set; }
        [Required]
        public bool ShowInCapacity { get; set; }
        [Required]
        public DateTime StartDate { get; set; }
        [Required]
        public DateTime EndDate { get; set; }
        [Required]
        public int Sortorder { get; set; }
        public string ColorCode { get; set; }
        public string Description { get; set; }
        public virtual ICollection<AfterCareTypeAfterCareFinancing> AfterCareTypeAfterCareFinancing { get; set; }
    }
}