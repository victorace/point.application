using System.ComponentModel.DataAnnotations.Schema;

namespace Point.Database.Models
{
    [Table("Template")]
    public partial class Template : BaseEntity
    {
        public int TemplateID { get; set; }

        public int? TemplateTypeID { get; set; }

        public int? OrganizationID { get; set; }

        [Column(TypeName = "text")]
        public string TemplateText { get; set; }

        public virtual Organization Organization { get; set; }

        public virtual TemplateType TemplateType { get; set; }
    }
}
