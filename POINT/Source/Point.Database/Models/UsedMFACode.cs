using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Point.Database.Repository;
using System;
using Point.Models.Enums;

namespace Point.Database.Models
{
    [Table("UsedMFACode")]
    public partial class UsedMFACode : BaseEntity
    {
        public int UsedMFACodeID { get; set; }
        public MFACodeType MFACodeType { get; set; }
        public int EmployeeID { get; set; }
        public int OrganizationID { get; set; }
        [StringLength(50)]
        public string MFANumber { get; set; }
        [StringLength(50)]
        public string MFAKey { get; set; }
        public DateTime SentDateTime { get; set; }
        [StringLength(10)]
        public string MFACode { get; set; }
        public DateTime? ClaimDateTime { get; set; }
        public virtual Employee Employee {get; set;}
        public virtual Organization Organization { get; set; }
        
    }
}
