using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Point.Database.Repository;

namespace Point.Database.Models
{
    [Table("Templet")]
    public partial class Templet : BaseEntity
    {
        public int TempletID { get; set; }

        public int? RegionID { get; set; }
        public int? TempletTypeID { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        [Column(TypeName = "text")]
        public string Description { get; set; }

        public DateTime? UploadDate { get; set; }

        [Column(TypeName = "image")]
        public byte[] FileData { get; set; }

        public int? FileSize { get; set; }

        [StringLength(255)]
        public string FileName { get; set; }

        [StringLength(255)]
        public string ContentType { get; set; }

        public int? EmployeeID { get; set; }

        public bool Deleted { get; set; }
        public int? SortOrder { get; set; }

        public virtual Employee Employee { get; set; }

        public virtual Region Region { get; set; }
        
    }
}
