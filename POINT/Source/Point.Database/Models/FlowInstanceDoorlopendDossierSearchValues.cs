﻿using Point.Models.Enums;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Point.Database.Models
{
    public class FlowInstanceDoorlopendDossierSearchValues
    {
        [Column(Order = 0), Key, ForeignKey("FlowInstanceSearchValues")]
        public int FlowInstanceID { get; set; }

        [Key, Column(Order = 1)]
        public int FrequencyID { get; set; }

        public FrequencyType FrequencyType { get; set; }
        public DateTime? FrequencyStartDate { get; set; }
        public DateTime? FrequencyEndDate { get; set; }
        public string FrequencyName { get; set; }
        public int? LastFrequencyTransferMemoID { get; set; }
        public DateTime? LastFrequencyTransferMemoDateTime { get; set; }
        public string LastFrequencyTransferMemoEmployeeName { get; set; }
        public TransferMemoTypeID? LastFrequencyTransferMemoTarget { get; set; }
        public int? LastFrequencyAttachmentID { get; set; }
        public DateTime? LastFrequencyAttachmentUploadDate { get; set; }
        public AttachmentSource? LastFrequencyAttachmentSource { get; set; }
        public int FrequencyScopeType { get; set; }

        [ForeignKey("FrequencyID")]
        public virtual Frequency Frequency { get; set; }

        [ForeignKey("FlowInstanceID")]
        public virtual FlowInstanceSearchValues FlowInstanceSearchValues { get; set; }
    }
}
