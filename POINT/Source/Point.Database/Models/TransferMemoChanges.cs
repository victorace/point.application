using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Point.Database.Repository;

namespace Point.Database.Models
{
    public partial class TransferMemoChanges : BaseEntity
    {
        public int TransferMemoChangesID { get; set; }

        public int TransferMemoID { get; set; }

        [Column(TypeName = "text")]
        public string OriginalContent { get; set; }

        public int? EmployeeID { get; set; }

        [StringLength(255)]
        public string EmployeeName { get; set; }

        public DateTime? DateTimeChanged { get; set; }
        
    }
}
