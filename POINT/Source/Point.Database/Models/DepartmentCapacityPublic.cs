﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Point.Database.Models
{
    [Table("DepartmentCapacityPublic")]
    public partial class DepartmentCapacityPublic : BaseEntity
    {

        public int DepartmentCapacityPublicID { get; set; }
        public int? AfterCareTypeID { get; set; }
        public string AfterCareTypeName { get; set; }
        public int? AfterCareGroupID { get; set; }
        public int RegionID { get; set; }
        public string RegionName { get; set; }
        public int OrganizationID { get; set; }
        public string OrganizationName { get; set; }
        public int LocationID { get; set; }
        public string LocationName { get; set; }
        public string LocationStreet { get; set; }
        public string LocationNumber { get; set; }
        public string LocationPostalCode { get; set; }
        public string LocationCity { get; set; }
        public string LocationEmailAddress { get; set; }
        public int DepartmentID { get; set; }
        public string DepartmentName { get; set; }
        public string DepartmentPhoneNumber { get; set; }
        public string DepartmentFaxNumber { get; set; }
        public string DepartmentEmailAddress { get; set; }
        public bool IsActive { get; set; }
        public DateTime? AdjustmentCapacityDate { get; set; }
        public int? Capacity1 { get; set; }
        public int? Capacity2 { get; set; }
        public int? Capacity3 { get; set; }
        public int? Capacity4 { get; set; }
        public string Information { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public bool CapacityFunctionality { get; set; }
        //public List<int> AlternativeRegionIDs { get; set; }
        // TODO: XXX
        // IMPLEMENT EVERYWHERE!
        public int OrganizationTypeID { get; set; }     // Default 0 (TODO: IMPLEMENT!)

        public double? Latitude { get; set; }
        public double? Longitude { get; set; }

        public virtual Location Location { get; set; }
    }
}
