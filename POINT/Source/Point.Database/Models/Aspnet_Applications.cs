using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Point.Database.Repository;

namespace Point.Database.Models
{
    public partial class Aspnet_Applications : BaseEntity 
    {
        public Aspnet_Applications()
        {
            aspnet_Membership = new HashSet<Aspnet_Membership>();
            aspnet_Roles = new HashSet<Aspnet_Roles>();
            aspnet_Users = new HashSet<Aspnet_Users>();
        }

        [Required]
        [StringLength(256)]
        public string ApplicationName { get; set; }

        [Required]
        [StringLength(256)]
        public string LoweredApplicationName { get; set; }

        [Key]
        public Guid ApplicationId { get; set; }

        [StringLength(256)]
        public string Description { get; set; }

        public virtual ICollection<Aspnet_Membership> aspnet_Membership { get; set; }

        public virtual ICollection<Aspnet_Roles> aspnet_Roles { get; set; }

        public virtual ICollection<Aspnet_Users> aspnet_Users { get; set; }
    }
}
