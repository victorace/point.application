using System;
using System.ComponentModel.DataAnnotations;

namespace Point.Database.Models
{
    public class SystemMessage : BaseEntity
    {
        public int SystemMessageID { get; set; }

        [Required]
        [StringLength(2000)]
        public string Title { get; set; }

        [Required]
        public string Message { get; set; }

        [StringLength(200)]
        public string LinkReference { get; set; }

        [StringLength(100)]
        public string LinkText { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        [Required(AllowEmptyStrings = true)]
        [StringLength(50)]
        public string FunctionCode { get; set; }

        public bool IsPermanent { get; set; }

        public bool IsActive { get; set; }
    }
}
