using Point.Models.Enums;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Point.Database.Models
{
    public partial class AttachmentReceivedTemp : BaseEntity
    {
        public int AttachmentReceivedTempID { get; set;}

        public DateTime TimeStamp {get;set;}

        public Guid GUID {get; set;}

        public int OrganisationID {get; set;}

        [StringLength(255)]
        [DisplayName("Verzender")]
        public string SenderUserName {get; set;}

        [DisplayName("Verzenddatum")]
        public DateTime SendDate {get; set;}

        [StringLength(50)]
        [DisplayName("Patientnummer")]
        public string PatientNumber {get; set;}

        [StringLength(9)]
        [DisplayName("Burger Service Nummer")]
        public string CivilServiceNumber { get; set; }

        [StringLength(50)]
        [DisplayName("Opnamenr.")]
        public string VisitNumber { get; set; }

        [DisplayName("Geboortenaam")]
        public DateTime BirthDate { get; set; }

        [StringLength(255)]
        public string FileName { get; set; }

        [StringLength(50)]
        [DisplayName("Naam")]
        public string Name { get; set; }

        [DisplayName("Beschrijving")]
        [Column(TypeName = "text")]
        public string Description { get; set; }

        [DisplayName("Type")]
        public AttachmentTypeID AttachmentTypeID { get; set; }

        public string Base64EncodedData { get; set; }

        [NotMapped]
        public bool ShowInAttachmentList { get; set; } = false;

        [StringLength(255)]
        public string Hash {get; set;}
    }
}
