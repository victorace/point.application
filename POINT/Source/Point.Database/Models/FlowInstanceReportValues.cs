﻿using Point.Models.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Point.Database.Models
{
    public class FlowInstanceReportValues
    {
        public FlowInstanceReportValues()
        {
            FlowInstanceTag = new HashSet<FlowInstanceTag>();
            FlowInstanceReportValuesAttachmentDetail = new HashSet<FlowInstanceReportValuesAttachmentDetail>();
            FlowInstanceOrganization = new HashSet<FlowInstanceOrganization>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FlowInstanceID { get; set; }
        public ScopeType ScopeType { get; set; }
        public FlowDefinitionID FlowDefinitionID { get; set; }
        public string FlowDefinitionName { get; set; }
        public string ClientName { get; set; }
        public DateTime? ClientBirthDate { get; set; }
        public string ClientBSN { get; set; }
        public string ClientHealthCareInsurer { get; set; }
        public string ClientGender { get; set; }
        public string ClientPatientNumber { get; set; }
        public string ClientVisitNumber { get; set; }
        public string ClientPostalCode { get; set; }
        public DateTime TransferCreatedDate { get; set; }
        public string TransferCreatedBy { get; set; }
        public string AfterCareCategoryDossier { get; set; }
        public string AfterCareTypeDossier { get; set; }
        public string AfterCareFinancing { get; set; }
        public string SpecialismeBehandelaar { get; set; }
        public string PrognoseVerwachteOntwikkeling { get; set; }
        public string ActivePhaseText { get; set; }
        public string AcceptedBy { get; set; }
        public string RequestFormZHVVTType { get; set; }
        public bool? PatientOntvingZorgVoorOpnameJaNee { get; set; }
        public string ZorginzetVoorDezeOpnameTypeZorginstelling { get; set; }
        public bool? KwetsbareOudereJaNee { get; set; }
        public double? InterruptionDays { get; set; }
        public bool? RegistrationEndedWithoutTransfer { get; set; }
        public string ReasonEndRegistration { get; set; }
        public string AdjustmentStandingCare { get; set; }
        public string AcceptedByVVT { get; set; }
        public string AcceptedByII { get; set; }
        public AcceptanceState? PatientAccepted { get; set; }
        public bool ZorgAdviesToevoegen { get; set; }
        public bool IsVoorrang { get; set; }
        public string DestinationHospitalDepartment { get; set; }

        public DateTime? FirstInitialDischargeDate { get; set; }
        public DateTime? InitialDischargeDate { get; set; }
        public DateTime? RealizedDischargeDate { get; set; }
        public DateTime? DischargeProposedStartDate { get; set; }
        public DateTime? DatumOpname { get; set; }
        public DateTime? CareBeginDate { get; set; }
        public DateTime? DateIndicationDecision { get; set; }
        public DateTime? FirstReceiverDate { get; set; }
        public DateTime? VOSendDate { get; set; }
        public DateTime? PatientWordtOpgenomenDatum { get; set; }
        public DateTime? TimeStamp { get; set; }

        [ForeignKey("FlowInstanceID")]
        public virtual FlowInstance FlowInstance { get; set; }

        public virtual FlowInstanceReportValuesDump FlowInstanceReportValuesDump { get; set; }
        public virtual ICollection<FlowInstanceTag> FlowInstanceTag { get; set; }
        public virtual ICollection<FlowInstanceReportValuesAttachmentDetail> FlowInstanceReportValuesAttachmentDetail { get; set; }
        public virtual ICollection<FlowInstanceOrganization> FlowInstanceOrganization { get; set; }
    }
}
