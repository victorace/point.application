using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Point.Database.Attributes;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Log.Attributes;
using Point.Log.Interfaces;

namespace Point.Database.Models
{
    [Table("Location"), Description("Locatie gegevens")]
    public partial class Location : BaseEntity, ILoggable
    {
        public Location()
        {
            Department = new HashSet<Department>();
            LocationRegion = new HashSet<LocationRegion>();
            FlowDefinitionParticipation = new HashSet<FlowDefinitionParticipation>();
        }

        [SkipLog]
        public int LocationID { get; set; }

        [DisplayName("Organisatie")]
        [LookUp("/Lookup/OrganizationInfo", "organizationid")]
        public int OrganizationID { get; set; }

        public virtual Organization Organization { get; set; }

        [Required]
        [StringLength(255)]
        [DisplayName("Naam")]
        public string Name { get; set; }


        [StringLength(255)]
        [DisplayName("Straat")]
        public string StreetName { get; set; }

        [StringLength(50)]
        [DisplayName("Straatnummer")]
        public string Number { get; set; }

        [StringLength(255)]
        [DisplayName("Land")]
        public string Country { get; set; }

        [StringLength(6)]
        [DisplayName("Postcode")]
        public string PostalCode { get; set; }

        [StringLength(512)]
        [DisplayName("Stad")]
        public string City { get; set; }

        [DisplayName("Inactief")]
        public bool Inactive { get; set; }

        [StringLength(255)]
        [DisplayName("Capaciteit info")]
        public string CapacityInfo { get; set; }

        [DisplayName("Controle IP")]
        public bool IPCheck { get; set; }

        [StringLength(2000)]
        [DisplayName("IP adres")]
        public string IPAddress { get; set; }

        [StringLength(255)]
        [DisplayName("Gebied (rapportage)")]
        public string Area { get; set; }

        public virtual ICollection<Department> Department { get; set; }

        public virtual ICollection<FlowDefinitionParticipation> FlowDefinitionParticipation { get; set; }

        public virtual ICollection<LocationRegion> LocationRegion { get; set; }
        public virtual ICollection<AutoCreateSet> AutoCreateSet { get; set; }
        public DateTime? ModifiedTimeStamp { get; set; }

        public string ShortName()
        {
            if (Name == "Locatie 1")
                return Organization?.Name;
            else
                return string.Format("{0}", Name);
        }

        public string FullName()
        {
            if (Name == "Locatie 1")
                return Organization?.Name;
            else
                return string.Format("{0} - {1}", Organization?.Name, Name);
        }

        public string FullNameProp
        {
            get
            {
                return FullName();
            }
        }

        public string ShortAdressString()
        {
            if(String.IsNullOrEmpty(StreetName))
            {
                return "";
            }

            return string.Format("{0} {1}, {2}", StreetName, Number, City);
        }

        public AddressViewModel FullAdress()
        {
            return new AddressViewModel { Street = StreetName, HouseNumber = Number, PostalCode = PostalCode, City = City, Country = Country };
        }
    }
}
