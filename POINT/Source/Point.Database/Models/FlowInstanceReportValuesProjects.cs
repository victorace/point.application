﻿using Point.Models.Enums;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Point.Database.Models
{
    public class FlowInstanceReportValuesProjects
    {
        [Column(Order = 0), Key, ForeignKey("FlowInstanceReportValues")]
        public int FlowInstanceID { get; set; }
        public virtual FlowInstanceReportValues FlowInstanceReportValues { get; set; }

        [Column(Order = 1), Key]
        public int OrganizationProjectID { get; set; }
        public virtual OrganizationProject OrganizationProject { get; set; }
    }
}
