﻿using Point.Log.Attributes;
using Point.Log.Interfaces;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Point.Database.Models
{

    [Description("Autocreate account koppeling")]
    public class AutoCreateSetDepartment : ILoggable
    {
        [Key]
        [Column(Order = 1)]
        [SkipLog]
        public int AutoCreateSetID { get; set; }

        [Key]
        [Column(Order = 2)]
        [DisplayName("Afdeling")]
        [LookUp("/Lookup/DepartmentInfo", "departmentid")]
        public int DepartmentID { get; set; }
        public DateTime? ModifiedTimeStamp { get; set; }

        public virtual AutoCreateSet AutoCreateSet { get; set; }
        public virtual Department Department { get; set; }
    }
}
