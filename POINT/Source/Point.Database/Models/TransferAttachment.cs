using Point.Database.Repository;
using Point.Models.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Point.Database.Models
{
    [Table("TransferAttachment")]
    public partial class TransferAttachment : BaseEntity, ILogEntryWithID
    {
        public TransferAttachment()
        {
            FrequencyTransferAttachment = new HashSet<FrequencyTransferAttachment>();
        }

        [Key]
        public int AttachmentID { get; set; }

        [StringLength(50)]
        [DisplayName("Naam")]
        [Required(ErrorMessage = errorRequired)]
        public string Name { get; set; }

        [DisplayName("Beschrijving")]
        [Column(TypeName = "text")]
        public string Description { get; set; }

        public AttachmentSource AttachmentSource { get; set; } = AttachmentSource.None;

        [DisplayName("Type")]
        [Required(ErrorMessage = errorRequired)]
        public AttachmentTypeID AttachmentTypeID { get; set; } = AttachmentTypeID.None;

        public DateTime UploadDate { get; set; }

        public int? EmployeeID { get; set; }

        [ForeignKey("EmployeeID")]
        public virtual Employee Employee { get; set; }

        public int? TransferID { get; set; }

        public virtual Transfer Transfer { get; set; }

        public bool Deleted { get; set; }

        public DateTime? TimeStamp { get; set; }

        public int? ScreenID { get; set; }

        public bool ShowInAttachmentList { get; set; } = true;

        public int? FormSetVersionID { get; set; }

        public virtual FormSetVersion FormSetVersion { get; set; }

        public Guid GenericFileID { get; set; }
        public int? RelatedAttachmentID { get; set; }

        public virtual GenericFile GenericFile { get; set; }
        
        public virtual ICollection<FrequencyTransferAttachment> FrequencyTransferAttachment { get; set; }
    }
}