using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Point.Database.Models
{
    [Table("MedicineCard")]
    public partial class MedicineCard : BaseEntity
    {
        public int MedicineCardID { get; set; }

        public int TransferID { get; set; }
        public int? FormSetVersionID { get; set; }

        public int MedicineID { get; set; }

        [StringLength(100)]
        public string NotRegisteredMedicine { get; set; }

        [StringLength(50)]
        public string Dosage { get; set; }

        [StringLength(50)]
        public string Frequence { get; set; }

        public DateTime? FinishDate { get; set; }

        public string Prescription { get; set; }

        public DateTime? TimeStamp { get; set; }

        public int? EmployeeID { get; set; }

        public int? ScreenID { get; set; }

        public virtual Medicine Medicine { get; set; }

        public virtual Transfer Transfer { get; set; }
        
    }
}
