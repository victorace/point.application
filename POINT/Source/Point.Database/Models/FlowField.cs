﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Point.Database.Models.ViewModels;
using Point.Models.Enums;

namespace Point.Database.Models
{
    [Table("FlowField")]
    public partial class FlowField : BaseEntity
    {
        public FlowField()
        {
            this.FlowFieldAttribute = new HashSet<FlowFieldAttribute>();
            this.FlowFieldValue = new HashSet<FlowFieldValue>();
        }

        public int FlowFieldID { get; set; }

        [Required]
        public string Name { get; set; }

        public string Label { get; set; }

        public string Description { get; set; }

        [Required]
        public FlowFieldType Type { get; set; }
        
        public string Format { get; set; }

        public string Regexp { get; set; }

        public int? MaxLength { get; set; }
        
        public int? FlowFieldDataSourceID { get; set; }
        public bool UseInReadModel { get; set; }

        public virtual FlowFieldDataSource FlowFieldDataSource { get; set; }

        public virtual ICollection<FlowFieldAttribute> FlowFieldAttribute { get; set; }
        public virtual ICollection<FlowFieldAttribute> RequiredForFlowFieldAttribute { get; set; }
        public virtual ICollection<FlowFieldAttributeException> RequiredForFlowFieldAttributeException { get; set; }
        public virtual ICollection<FlowFieldValue> FlowFieldValue { get; set; }
        public virtual ICollection<FlowFieldDashboard> FlowFieldDashboard { get; set; }
    }
}
