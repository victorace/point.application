using System.ComponentModel.DataAnnotations.Schema;
using Point.Database.Repository;

namespace Point.Database.Models
{
    [Table("Logging")]
    public partial class Logging : BaseEntity 
    {
        public bool? Enabled { get; set; }

        public int ID { get; set; }
        
    }
}
