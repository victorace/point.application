﻿using Point.Models.Enums;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Point.Database.Models
{
    [Table("SendAttachment")]
    public class SendAttachment : BaseEntity
    {
        [Key]
        public int SendAttachmentID { get; set; }

        public SendDestinationType SendDestinationType { get; set; }

        public int? OrganizationID { get; set; }

        public AttachmentTypeID AttachmentTypeID { get; set; }

        public bool Allowed { get; set; }
    }
}
