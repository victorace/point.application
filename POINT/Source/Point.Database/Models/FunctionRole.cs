﻿using System;
using System.ComponentModel;
using Point.Models.Enums;
using Point.Log.Attributes;
using Point.Log.Interfaces;

namespace Point.Database.Models
{
    [Description("Functies")]
    public class FunctionRole : ILoggable
    {
        [SkipLog]
        public int FunctionRoleID { get; set; }

        [SkipLog]
        public int? EmployeeID { get; set; }

        [DisplayName("Functie")]
        public FunctionRoleTypeID FunctionRoleTypeID { get; set; }

        [DisplayName("Niveau")]
        public FunctionRoleLevelID FunctionRoleLevelID { get; set; }
        public DateTime? ModifiedTimeStamp { get; set; }

        public virtual Employee Employee { get; set; }
    }
}
