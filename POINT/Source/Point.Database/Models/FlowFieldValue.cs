﻿using Point.Models.Enums;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Point.Database.Repository;
using Point.Models.Interfaces;

namespace Point.Database.Models
{
    [Table("FlowFieldValue")]
    public partial class FlowFieldValue : BaseEntity, IMutation, ILogEntry, IFlowFieldValue
    {
        public int FlowFieldValueID { get; set; }

        public int FlowInstanceID { get; set; }
        [ForeignKey("FlowInstanceID")]
        public virtual FlowInstance FlowInstance { get; set; }

        [Required]
        public int FlowFieldID { get; set; }
        [ForeignKey("FlowFieldID")]
        public virtual FlowField FlowField { get; set; }

        public Nullable<int> FormSetVersionID { get; set; }
        [ForeignKey("FormSetVersionID")]
        public virtual FormSetVersion FormSetVersion { get; set;  }

        public string Value { get; set; }

        public int? EmployeeID { get; set; }

        public DateTime? Timestamp { get; set; }

        [StringLength(255)]
        public string ScreenName { get; set; }

        public Source Source { get; set; }

        [NotMapped]
        public Type MutationEntity
        {
            get;
            private set;
        }

        public FlowFieldValue()
        {
            MutationEntity = typeof(MutFlowFieldValue);
        }
    }
}