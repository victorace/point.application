﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Point.Database.Repository;

namespace Point.Database.Models
{
    public class PhaseInstance : BaseEntity, ILogEntry 
    {
        public PhaseInstance()
        {
            FormSetVersion = new HashSet<FormSetVersion>();
        }

        public int PhaseInstanceID { get; set; }

        public int PhaseDefinitionID { get; set; }
        [ForeignKey("PhaseDefinitionID")]
        public virtual PhaseDefinition PhaseDefinition { get; set; }

        public int FlowInstanceID { get; set; }
        [ForeignKey("FlowInstanceID")]
        public virtual FlowInstance FlowInstance { get; set; }

        public int Status { get; set; }

        public DateTime StartProcessDate { get; set; }
        public int StartedByEmployeeID { get; set; }
        [ForeignKey("StartedByEmployeeID")]
        public virtual Employee StartedByEmployee { get; set; }

        public DateTime? RequestDate { get; set; }
        public DateTime? RealizedDate { get; set; }
        public int? RealizedByEmployeeID { get; set; }
        [ForeignKey("RealizedByEmployeeID")]
        public virtual Employee RealizedByEmployee { get; set; }

        public int? EmployeeID { get; set; }
        public DateTime? Timestamp { get; set; }
        [StringLength(255)]
        public string ScreenName { get; set; }

        public bool Cancelled { get; set; }

        public virtual ICollection<FormSetVersion> FormSetVersion { get; set; }
    }
}
