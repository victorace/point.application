﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Point.Models.Enums;
using Point.Database.Repository;

namespace Point.Database.Models
{
    public class FlowInstanceStatus : BaseEntity, ILogEntry
    {
        public int FlowInstanceStatusID { get; set; }
        public int FlowInstanceID { get; set; }
        public FlowInstanceStatusTypeID FlowInstanceStatusTypeID { get; set; }
        public DateTime StatusDate { get; set; }
        public string Comment { get; set; }
        public int? EmployeeID { get; set; }
        public virtual Employee Employee { get; set; }
        public DateTime? Timestamp { get; set; }
        public string ScreenName { get; set; }
        
    }
}
