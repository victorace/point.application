﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Point.Models.Enums;
using Point.Database.Repository;

namespace Point.Database.Models
{

    public class FlowDefinition : BaseEntity , ILogEntry 
    {

        public FlowDefinitionID FlowDefinitionID { get; set; }

        public int FlowTypeID { get; set; }
        [ForeignKey("FlowTypeID")]
        public virtual FlowType FlowType { get; set; }

        [StringLength(255)]
        public string Name { get; set; }

        [StringLength(4000)]
        public string Description { get; set; }

        [DefaultValue(false)]
        public bool InActive { get; set; }
        public DateTime DateBegin { get; set; }
        public DateTime DateEnd { get; set; }

        public int? EmployeeID { get; set; }
        public DateTime? Timestamp { get; set; }
        [StringLength(255)]
        public string ScreenName { get; set; }

        public bool HasFormTab { get; set; }

        public int? SortOrder { get; set; }
        public virtual ICollection<PhaseDefinition> PhaseDefinitions { get; set; }
        public virtual ICollection<FlowInstance> FlowInstance { get; set; }
        public virtual ICollection<ReportDefinitionFlowDefinition> ReportDefinitionFlowDefinition { get; set; }
        public virtual ICollection<FlowDefinitionFormType> FlowDefinitionFormType { get; set; }
        public virtual ICollection<FormTypeSelection> FormTypeSelection { get; set; }
    }

}
