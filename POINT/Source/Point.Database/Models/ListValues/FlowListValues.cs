﻿using Point.Models.Enums;
using Point.Models.ViewModels;
using System.Collections.Generic;
using System.Linq;

namespace Point.Database.Models
{
    public class FlowListValues
    {
        private const string thuiszorgcode = "T2";
        private const string verpleeghuiscode = "R5";

        public static ListValue GetHealthCareOrganizationTypeByCode(string code)
        {
            var allorganizationtypes = HealthCareOrganizationType();
            return allorganizationtypes.FirstOrDefault(it => it.Value == code);
        }

        public static ListValue GetDefaultHealthCareOrganizationType(AfterCareCategoryID? aftercarecategory)
        {
            var allorganizationtypes = HealthCareOrganizationType();

            if (aftercarecategory == AfterCareCategoryID.Thuiszorg)
            {
                //Thuiszorg
                return GetHealthCareOrganizationTypeByCode(thuiszorgcode);
            }
            else
            {
                //Verpleeghuis
                return GetHealthCareOrganizationTypeByCode(verpleeghuiscode);
            }
        }

        public static List<ListValue> GRZIntensity()
        {
            string[][] data = new string[][] {
                new string[] {"1-14-dagen-intensief", "1-14 dagen intensief"},
                new string[] { "1-14-dagen-niet intensief", "1-14 dagen niet intensief"},
                new string[] { "15-28-dagen-intensief", "15-28 dagen intensief"},
                new string[] { "15-28-dagen-niet intensief", "15-28 dagen niet intensief"},
                new string[] { "29-56-dagen-intensief", "29-56 dagen intensief"},
                new string[] { "29-56-dagen-niet intensief", "29-56 dagen niet intensief"},
                new string[] { "57-92-dagen", "57-92 dagen"},
                new string[] { "Meer dan 92", "Meer dan 92 dagen" }
            };

            return data.Cast<string[]>().Select(str => new ListValue() { Value = str[0], Text = str[1] }).ToList();
        }

        public static List<ListValue> HealthCareOrganizationType()
        {
            string[][] organizationtypedata = new string[][] {
                                                    new string[] {"Z5", "Diagnostisch centrum"},
                                                    new string[] {"Z3", "Huisartspraktijk"},
                                                    new string[] {"P4", "Polikliniek"},
                                                    new string[] {"R8", "Revalidatie centrum"},
                                                    new string[] {"P6", "Bevolkingsonderzoek"},
                                                    new string[] {"V5", "Universitair Medisch Centrum"},
                                                    new string[] {verpleeghuiscode, "Verpleeghuis/verzorgingstehuis (Verpleeghuis)"},
                                                    new string[] {"M1", "Verpleeghuis/verzorgingstehuis (Verzorghuis)"},
                                                    new string[] {"Z4", "Zelfstandig Behandel Centrum"},
                                                    new string[] {"V4", "Ziekenhuis"},
                                                    new string[] {"G3", "Verloskundigenpraktijk"},
                                                    new string[] {"J8", "Openbare apotheek (Apotheken)"},
                                                    new string[] {"L1", "Laboratorium"},
                                                    new string[] {"N6", "Huisartsenpost"},
                                                    new string[] {"G5", "GGZ"},
                                                    new string[] {thuiszorgcode, "Thuiszorg"},
                                                    new string[] {"G6", "Verstandelijk gehandicaptenzorg"},
                                                    new string[] {"V4", "Algemeen ziekenhuis"},
                                                    new string[] {"B1", "Beeldvormende diagnostie"},
                                                    new string[] {"B2", "Echocentrum"},
                                                    new string[] {"K9", "Zelfstand opererende ziekenhuisapotheek"},
                                                    new string[] {"K3", "Apotheekhoudende huisartspraktijk"},
                                                    new string[] {"JGZ", "Jeugdgezondszorg"}
                                                };
            return organizationtypedata.Cast<string[]>().Select(str => new ListValue() { Value = str[0], Text = str[1] }).ToList();
        }

        public static List<ListValue> RelationsType()
        {
            string[][] relationsdata = new string[][] { new string[] {"1", "Eerste relatie/contactpersoon"}, 
                                                        new string[] {"2", "Tweede relatie/contactpersoon"},
                                                        new string[] {"3", "Curator (juridisch)"},
                                                        new string[] {"4", "Financieel (gemachtigd)"},
                                                        new string[] {"5", "Financieel (toetsing)"},
                                                        new string[] {"6", "Leefeenheid"},
                                                        new string[] {"7", "Hulpverlener"},
                                                        new string[] {"8", "Specialist"},
                                                        new string[] {"9", "Anders"},
                                                        new string[] {"10", "Ouder"},
                                                        new string[] {"11", "Voogd"},
                                                        new string[] {"12", "Partner/echtgeno(o)t(e)"},
                                                        new string[] {"13", "Pleegouder"},
                                                        new string[] {"14", "Bewindvoerder"},
                                                        new string[] {"15", "Mentor"},
                                                        new string[] {"16", "Zoon/dochter"},
                                                        new string[] {"17", "Familielid"},
                                                        new string[] {"18", "Gezinslid"},
                                                        new string[] {"19", "Buur"},
                                                        new string[] {"20", "Vriend(in)/kennis"},
                                                        new string[] {"21", "Cliëntondersteuner"},
                                                        new string[] {"22", "Huisarts"},
                                                        new string[] {"99", "Niet van toepassing"}
                                                };

            return relationsdata.Cast<string[]>().Select(str => new ListValue() { Value = str[0], Text = str[1] }).ToList();

        }

        public static List<ListValue> YesNoUnknown()
        {
            string[][] yesnodata = new string[][] {
                                            new string[] {"Ja", "Ja"},
                                            new string[] {"Nee", "Nee"},
                                            new string[] {"Onbekend", "Onbekend"}
                                       };

            return yesnodata.Cast<string[]>().Select(str => new ListValue() { Value = str[0], Text = str[1] }).ToList();

        }

        public static List<ListValue> OndervoedingMeetMethode()
        {
            string[][] methodes = new string[][] {
                new string[] {"SNAQ", "SNAQ"},
                new string[] {"414648004", "MUST"}
            };

            return methodes.Cast<string[]>().Select(str => new ListValue() { Value = str[0], Text = str[1] }).ToList();
        }

        public static List<ListValue> OndervoedingsScoreMUST()
        {
            string[][] scores = new string[][] {
                new string[] {"M0", "0"},
                new string[] {"M1", "1"},
                new string[] {"M2", "2 of meer"}
            };

            return scores.Cast<string[]>().Select(str => new ListValue() { Value = str[0], Text = str[1] }).ToList();
        }

        public static List<ListValue> OndervoedingsScoreSNAQ()
        {
            string[][] scores = new string[][] {
                new string[] {"S1", "0-1"},
                new string[] {"S2", "2"},
                new string[] {"S3", "3 of meer"}
            };

            return scores.Cast<string[]>().Select(str => new ListValue() { Value = str[0], Text = str[1] }).ToList();
        }

        public static List<ListValue> Decubitus()
        {
            string[][] decubitustypes = new string[][] {
                new string[] {"421076008", "1"},
                new string[] {"420324007", "2"},
                new string[] {"421927004", "3"},
                new string[] {"420597008", "4"}
            };

            return decubitustypes.Cast<string[]>().Select(str => new ListValue() { Value = str[0], Text = str[1] }).ToList();
        }

        public static List<ListValue> WondSoort()
        {
            string[][] wondtypes = new string[][] {
                new string[] {"112633009", "Chirurgische wond"},
                new string[] {"2", "Traumatische wond"},
                new string[] {"48333001", "Brandwond"},
                new string[] {"95344007", "Ulcus cruris"},
                new string[] {"OTH", "Anders"}
            };

            return wondtypes.Cast<string[]>().Select(str => new ListValue() { Value = str[0], Text = str[1] }).ToList();
        }

        public static List<ListValue> Stoornis()
        {
            string [][] stoornissen = new string[][]{
                new string[] {"functie.0", "Geen stoornis"},
                new string[] {"functie.1", "Lichte stoornis"},
                new string[] {"functie.2", "Matige stoornis"},
                new string[] {"functie.3", "Ernstige stoornis"},
                new string[] {"functie.4", "Volledige stoornis"}
            };

            return stoornissen.Cast<string[]>().Select(str => new ListValue() { Value = str[0], Text = str[1] }).ToList();
        }

        public static List<ListValue> UrineKetheterSoort()
        {
            string [][] kathetersoorten = new string[][]{
                new string[] {"23973005", "Verblijfskatheter"},
                new string[] {"225103006", "Intermitterende katheterisatie"},
                new string[] {"337636000", "Condoomkatheter"},
                new string[] {"OTH", "Anders"}
            };

            return kathetersoorten.Cast<string[]>().Select(str => new ListValue() { Value = str[0], Text = str[1] }).ToList();
        }

        public static List<ListValue> Beperkingen()
        {
            string [][] beperkingen = new string[][]{
                new string[] {"functie.0", "Geen beperking"},
                new string[] {"functie.1", "Lichte beperking"},
                new string[] {"functie.2", "Matige beperking"},
                new string[] {"functie.3", "Ernstige beperking"},
                new string[] {"functie.4", "Volledige beperking"}
            };

            return beperkingen.Cast<string[]>().Select(str => new ListValue() { Value = str[0], Text = str[1] }).ToList();
        }

        public static List<ListValue> HulpmiddelenZien()
        {
            string [][] hulpmiddelen = new string[][]{
                new string[] {"0", "Geen visuele hulpmiddelen"},
                new string[] {"423013001", "Leesbril"},
                new string[] {"50121007", "Bril"},
                new string[] {"57368009", "Contactlenzen"},
                new string[] {"264865009", "Loep"},
                new string[] {"OTH", "Anders"}
            };

            return hulpmiddelen.Cast<string[]>().Select(str => new ListValue() { Value = str[0], Text = str[1] }).ToList();
        }

        public static List<ListValue> HulpmiddelenGehoor()
        {
            string [][] hulpmiddelen = new string[][]{
                new string[] {"0", "Geen"},
                new string[] {"1", "Gehoorapparaat links"},
                new string[] {"2", "Gehoorapparaat rechts"},
                new string[] {"3", "Gehoorapparaat beiderzijds"}
            };

            return hulpmiddelen.Cast<string[]>().Select(str => new ListValue() { Value = str[0], Text = str[1] }).ToList();
        }

        public static List<ListValue> Pijn()
        {
            string [][] pijnlevels = new string[][]{
                new string[] {"0", "0"},
                new string[] {"1", "1"},
                new string[] {"2", "2"},
                new string[] {"3", "3"},
                new string[] {"4", "4"},
                new string[] {"5", "5"},
                new string[] {"6", "6"},
                new string[] {"7", "7"},
                new string[] {"8", "8"},
                new string[] {"9", "9"},
                new string[] {"10", "10"}
            };

            return pijnlevels.Cast<string[]>().Select(str => new ListValue() { Value = str[0], Text = str[1] }).ToList();
        }

        public static List<ListValue> JuridischeStatus()
        {
            string [][] statussen = new string[][]{
                new string[] {"1", "Rechterlijke machtiging (rm) voorlopig / voortgezette machtiging"},
                new string[] {"2", "RM op eigen verzoek"},
                new string[] {"3", "RM met voorwaardelijk ontslag"},
                new string[] {"4", "Onder toezichtstelling (ots)"},
                new string[] {"5", "In bewaring stelling (ibs)"},
                new string[] {"6", "Voogdij"},
                new string[] {"7", "Bewindvoering"},
                new string[] {"8", "Mentorschap"},
                new string[] {"9", "Onder curatele stelling"},
                new string[] {"10", "Zaakwaarneming"},
                new string[] {"11", "Strafrechterlijke justitiële contacten: tbs"},
                new string[] {"12", "Strafrechterlijke justitiële contacten: overig"}
            };

            return statussen.Cast<string[]>().Select(str => new ListValue() { Value = str[0], Text = str[1] }).ToList();
        }

        public static List<ListValue> DwangEnDrang()
        {
            string [][] dwangendrangtypes = new string[][]{
                new string[] {"A1", "Separatie"},
                new string[] {"A2", "Afzondering"},
                new string[] {"A3", "Fixatie"},
                new string[] {"A4", "Gedwongen medicatie"},
                new string[] {"A5", "Gedwongen voeding en/of vocht"},
                new string[] {"A6", "Andere interventies"}
            };

            return dwangendrangtypes.Cast<string[]>().Select(str => new ListValue() { Value = str[0], Text = str[1] }).ToList();
        }
        
        public static List<ListValue> Instemming()
        {
            string [][] instemmingtypes = new string[][]{
                new string[] {"E1", "Op eigen verzoek"},
                new string[] {"E2", "Geen verzet"},
                new string[] {"E3", "Verzet"}
            };

            return instemmingtypes.Cast<string[]>().Select(str => new ListValue() { Value = str[0], Text = str[1] }).ToList();
        }

        //public static List<ListValue> HousingType()
        //{
        //    string [][] housingtypes = new string[][]{
        //        new string[] {"Benedenwoning", "Benedenwoning"},
        //        new string[] {"Portiekwoning", "Portiekwoning"},
        //        new string[] {"Woning met trap", "Woning met trap"},
        //        new string[] {"Woning met lift", "Woning met lift"},
        //        new string[] {"Pension/aanleunwoning", "Pension/aanleunwoning"},
        //        new string[] {"Verzorgingstehuis", "Verzorgingstehuis"},
        //        new string[] {"Verpleeghuis", "Verpleeghuis"},
        //        new string[] {"Zwervend/dakloos", "Zwervend/dakloos"},
        //        new string[] {"Eengezinswoning", "Eengezinswoning"},
        //        new string[] {"Eengezinswoning bovenwoning", "Eengezinswoning bovenwoning"},
        //        new string[] {"Anders", "Anders"}
        //    };

        //    return housingtypes.Cast<string[]>().Select(str => new ListValue() { Value = str[0], Text = str[1] }).ToList();
        //}

        public static List<ListValue> CompositionHousekeeping()
        {
            string[][] comphousekeepings = new string[][]{
                new string[] {"Alleenstaand", "Alleenstaand"},
                new string[] {"Uitsluitend met partner", "Uitsluitend met partner"},
                new string[] {"Met partner en kinderen", "Met partner en kinderen"},
                new string[] {"Zonder partner met kinderen", "Zonder partner met kinderen"},
                new string[] {"Woont als kind samen met ouders in ouderlijk huis", "Woont als kind samen met ouders in ouderlijk huis"},
                new string[] {"Heeft huishouden van volwassenen met 1 of meer kinderen", "Heeft huishouden van volwassenen met 1 of meer kinderen"},
                new string[] {"Heeft een ander meerpersoonshuishouden", "Heeft een ander meerpersoonshuishouden"},
                new string[] {"Woont in zorginstelling met verblijf", "Woont in zorginstelling met verblijf"},
                new string[] {"Anders", "Anders"}
            };

            return comphousekeepings.Cast<string[]>().Select(str => new ListValue() { Value = str[0], Text = str[1] }).ToList();
        }

        public static List<ListValue> InfuusSoort()
        {
            string [][] infuussoorten = new string[][]{
                new string[] {"82449006", "Perifeer infuus"},
                new string[] {"52124006", "Centrale veneuze katheter"},
                new string[] {"30610008", "Epiduraal katheter"},
                new string[] {"303727009", "Arterieel katheter"}
            };

            return infuussoorten.Cast<string[]>().Select(str => new ListValue() { Value = str[0], Text = str[1] }).ToList();
        }
    }
}
