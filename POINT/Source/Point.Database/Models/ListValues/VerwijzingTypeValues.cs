﻿using Point.Database.Models.ViewModels;
using Point.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Point.Database.Models.ListValues
{
    public class VerwijzingTypeValues
    {
        public FlowFormType FlowFormType { get; set; }
        public string Text { get; set; }
        public string Value { get; set; }
        public bool DefaultSelected { get; set; } = false;

        public VerwijzingTypeValues() { }
        public VerwijzingTypeValues(FlowFormType flowFormType, string text, string value)
        {
            FlowFormType = flowFormType;
            Text = text;
            Value = value;
        }

        private static List<VerwijzingTypeValues> GetList()
        {
            var list = new List<VerwijzingTypeValues>();

            list.AddRange(new[] {
                    new VerwijzingTypeValues(FlowFormType.RequestFormHAVVT_Thuiszorg, "Huishoudelijke verzorging", "huishoudelijkeverzorging"),
                    new VerwijzingTypeValues(FlowFormType.RequestFormHAVVT_Thuiszorg, "Verpleging / verzorging", "verplegingverzorging"),
                    new VerwijzingTypeValues(FlowFormType.RequestFormHAVVT_Thuiszorg, "(Gespecialiseerde) Verpleging met toevoeging Uitvoeringsverzoek (vul UVV in)", "gespverplmetuvv"),
                    new VerwijzingTypeValues(FlowFormType.RequestFormHAVVT_Thuiszorg, "Anders", "andersthuisz") { DefaultSelected = true },
                    new VerwijzingTypeValues(FlowFormType.RequestFormHAVVT_Opname, "EersteLijns Verblijf [ELV]", "elv"),
                    new VerwijzingTypeValues(FlowFormType.RequestFormHAVVT_Opname, "Geriatrische Revalidatie Zorg [GRZ]", "grz"),
                    new VerwijzingTypeValues(FlowFormType.RequestFormHAVVT_Opname, "Wlz-crisis", "wlzcrisis"),
                    new VerwijzingTypeValues(FlowFormType.RequestFormHAVVT_Opname, "Wlz", "wlz"),
                    new VerwijzingTypeValues(FlowFormType.RequestFormHAVVT_Opname, "Anders", "andersopname") { DefaultSelected = true },
                    new VerwijzingTypeValues(FlowFormType.RequestFormHAVVT_Consultatie, "Specialist Ouderen geneeskunde", "specouderengen"),
                    new VerwijzingTypeValues(FlowFormType.RequestFormHAVVT_Consultatie, "Psycholoog", "psycholoog"),
                    new VerwijzingTypeValues(FlowFormType.RequestFormHAVVT_Consultatie, "Ergotherapeut", "ergotherapeut"),
                    new VerwijzingTypeValues(FlowFormType.RequestFormHAVVT_Consultatie, "Logopedist", "logopedist"),
                    new VerwijzingTypeValues(FlowFormType.RequestFormHAVVT_Consultatie, "Anders", "andersconsulatie") { DefaultSelected = true },
                    new VerwijzingTypeValues(FlowFormType.RequestFormHAVVT_Kort, "Geen", "geenkortelijst") { DefaultSelected = true}

                });

            return list;
        }

        public static List<Option> GetOptionList(FlowFormType flowFormType)
        {
            var optionlist = GetList().Where(vt => vt.FlowFormType == flowFormType).Select(vt => new Option(vt.Text, vt.Value, vt.DefaultSelected)).ToList();
            return optionlist;
        }

        public static List<Option> GetOptionList(string currentvalue = "")
        {
            var currentverwijzingtype = GetList().FirstOrDefault(vt => vt.Value.Equals(currentvalue, StringComparison.InvariantCultureIgnoreCase));
            if (currentverwijzingtype == null)
            {
                return new List<Option>();
            }
            else
            {
                var optionlist = GetOptionList(currentverwijzingtype.FlowFormType);
                optionlist.ForEach(opt => opt.Selected = opt.Value.Equals(currentvalue, StringComparison.InvariantCultureIgnoreCase));
                return optionlist;
            }
        }
    }
}
