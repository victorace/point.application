﻿using Point.Database.Repository;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Point.Database.Models
{
    public class LogDepartment : BaseEntity, ILogEntryWithID
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int LogDepartmentID { get; set; }

        public int? DepartmentID { get; set; }

        public DateTime? TimeStamp { get; set; }

        public int? EmployeeID { get; set; }

        [StringLength(1)]
        public string Action { get; set; }

        public int? ScreenID { get; set; }

        public int? Version { get; set; }

        public virtual Department Department { get; set; }
        
        public virtual Employee Employee { get; set; }
        
    }
}
