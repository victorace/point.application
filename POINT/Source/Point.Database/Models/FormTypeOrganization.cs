using Point.Models.Enums;
using Point.Log.Attributes;
using Point.Log.Interfaces;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Point.Database.Models
{
    [Table("FormTypeOrganization"), Description("Formulieren per organisatie")]
    public partial class FormTypeOrganization : BaseEntity, ILoggable
    {
        [SkipLog]
        public int FormTypeOrganizationID { get; set; }

        [LookUp("/Lookup/FormTypeInfo", "formtypeid")]
        [DisplayName("Formulier")]
        public int FormTypeID { get; set; }

        [SkipLog]
        public int OrganizationID { get; set; }

        [DisplayName("Flow")]
        public FlowDefinitionID FlowDefinitionID { get; set; }

        public DateTime? ModifiedTimeStamp { get; set; }

        public virtual FormType FormType { get; set; }

        public virtual Organization Organization { get; set; }
        
    }
}
