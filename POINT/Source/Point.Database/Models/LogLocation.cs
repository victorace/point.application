﻿using Point.Database.Repository;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Point.Database.Models
{
    public class LogLocation : BaseEntity, ILogEntryWithID
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]

        public int LogLocationID { get; set; }

        public int? LocationID { get; set; }

        public DateTime? TimeStamp { get; set; }

        public int? EmployeeID { get; set; }

        [StringLength(1)]
        public string Action { get; set; }

        public int? ScreenID { get; set; }

        public int? Version { get; set; }

        public Location Location { get; set; }

        public Employee Employee { get; set; }
        
    }
}
