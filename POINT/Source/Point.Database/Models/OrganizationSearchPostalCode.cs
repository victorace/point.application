﻿using System.ComponentModel.DataAnnotations.Schema;
using Point.Database.Repository;

namespace Point.Database.Models
{
    public class OrganizationSearchPostalCode
    {
        public int OrganizationSearchPostalCodeID { get; set; }
        public int OrganizationSearchID { get; set; }
        public string StartPostalCode { get; set; }
        public string EndPostalCode { get; set; }

        public virtual OrganizationSearch OrganizationSearch { get; set; }
        
    }
}
