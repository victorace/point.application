using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Point.Database.Repository;
using Point.Log.Attributes;
using Point.Log.Interfaces;

namespace Point.Database.Models
{
    [Table("PostalCode"), Description("Postcodes")]
    public partial class PostalCode : BaseEntity, ILoggable
    {
        public PostalCode()
        {
            ServiceAreaPostalCode = new HashSet<ServiceAreaPostalCode>();
        }

        [SkipLog]
        public int PostalCodeID { get; set; }

        [StringLength(6)]
        [DisplayName("Postcode start")]
        public string StartPostalCode { get; set; }

        [StringLength(6)]
        [DisplayName("Postcode stop")]
        public string EndPostalCode { get; set; }

        [StringLength(255)]
        [DisplayName("Beschrijving")]
        public string Description { get; set; }

        public DateTime? ModifiedTimeStamp { get; set; }

        public virtual ICollection<ServiceAreaPostalCode> ServiceAreaPostalCode { get; set; }
        
    }
}
