﻿using Point.Models.Enums;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Point.Database.Models
{
    public class ReportDefinitionFlowDefinition
    {
        [Column(Order = 0), Key]
        public int ReportDefinitionID { get; set; }
        [Column(Order = 1), Key]
        public FlowDefinitionID FlowDefinitionID { get; set; }

        public virtual ReportDefinition ReportDefinition { get; set; }
        public virtual FlowDefinition FlowDefinition { get; set; }
    }
}
