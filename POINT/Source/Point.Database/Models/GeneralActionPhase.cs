﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Point.Database.Repository;

namespace Point.Database.Models
{
    public class GeneralActionPhase : BaseEntity
    {
        [Key]
        public int GeneralActionPhaseID { get; set; }

        public int PhaseDefinitionID { get; set; }
        [ForeignKey("PhaseDefinitionID")]
        public virtual PhaseDefinition PhaseDefinition { get; set; }

        public int GeneralActionID { get; set; }
        [ForeignKey("GeneralActionID")]
        public virtual GeneralAction GeneralAction { get; set; }

        public int? PhaseStatusBegin { get; set; }
        public int? PhaseStatusEnd { get; set; }

        public int? OrganizationTypeID { get; set; }
        
    }
}
