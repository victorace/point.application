using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Point.Database.Repository;

namespace Point.Database.Models
{
    [Table("ZorgAdviesItemGroup")]
    public partial class ZorgAdviesItemGroup : BaseEntity
    {
        public int ZorgAdviesItemGroupID { get; set; }
        public int? ParentZorgAdviesItemGroupID { get; set; }
        public int? OrderNumber { get; set; }
        public string GroupName { get; set; }


        public virtual ICollection<ZorgAdviesItem> ZorgAdviesItem { get; set; }
        public virtual ZorgAdviesItemGroup ZorgAdviesItemGroupParent { get; set; }
        public virtual ICollection<ZorgAdviesItemGroup> ZorgAdviesItemGroupChildren { get; set; }
        
    }
}
