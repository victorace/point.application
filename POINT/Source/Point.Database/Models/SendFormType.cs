﻿using Point.Models.Enums;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Point.Database.Models
{
    [Table("SendFormType")]
    public class SendFormType : BaseEntity
    {
        [Key]
        public int SendFormTypeID { get; set; }

        public SendDestinationType SendDestinationType { get; set; }

        public int? OrganizationID { get; set; }

        public FlowFormType FormTypeID { get; set; }

        public bool Allowed { get; set; }
    }
}
