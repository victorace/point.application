﻿using Point.Database.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Point.Database.Models
{
    public class GPSCoordinaat : BaseEntity
    {
        public int GPSCoordinaatID { get; set; }
        public string Straat { get; set; }
        public int? HuisNrVan { get; set; }
        public int? HuisNrTot { get; set; }
        public string Postcode { get; set; }

        public int? GPSPlaatsID { get; set; }
        public int? GPSGemeenteID { get; set; }
        public int? GPSProvincieID { get; set; }

        public virtual GPSPlaats GPSPlaats { get; set; }
        public virtual GPSGemeente GPSGemeente { get; set; }
        public virtual GPSProvincie GPSProvincie { get; set; }

        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        
    }
}
