using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Point.Database.Attributes;
using Point.Models.Enums;
using Point.Log.Attributes;
using Point.Log.Interfaces;

namespace Point.Database.Models
{
    [Table("Organization"), Description("Organisatie")]
    public class Organization : BaseEntity, ILoggable
    {
        public Organization()
        {
            FormTypeOrganization = new HashSet<FormTypeOrganization>();
            Template = new HashSet<Template>();
            OrganizationProjects = new HashSet<OrganizationProject>();
            OrganizationSetting = new HashSet<OrganizationSetting>();
            OrganizationCSVTemplate = new HashSet<OrganizationCSVTemplate>();
        }

        [SkipLog]
        public int OrganizationID { get; set; }

        [StringLength(500)]
        [DisplayName("Naam")]
        public string Name { get; set; }
        [DisplayName("Type")]
        public int OrganizationTypeID { get; set; }
        [ForeignKey("OrganizationTypeID")]
        public virtual OrganizationType OrganizationType { get; set; }
        [DisplayName("Inactief")]
        public bool Inactive { get; set; }
        [DisplayName("HL7 interface")]
        public bool HL7Interface { get; set; }
        [DisplayName("WS interface")]
        public bool WSInterface { get; set; }
        [DisplayName("Interface nr")]
        public int? InterfaceNumber { get; set; }
        [DisplayName("Regio")]
        public int? RegionID { get; set; }
        [DisplayName("Verander wachtwoord na")]
        public int? ChangePasswordPeriod { get; set; }
        [DisplayName("Korte connectie ID")]
        public bool ShortConnectionID { get; set; }

        [StringLength(20)]
        [DisplayName("AGB code")]
        public string AGB { get; set; }
        [DisplayName("Url")]
        [StringLength(255)]
        public string URL { get; set; }
        [DisplayName("HL7 extra velden")]
        public bool HL7InterfaceExtraFields { get; set; }
        [DisplayName("Verberg knop 'Alle Dossiers' bij patientfocus")]
        public bool HL7ConnectionDifferentMenu { get; set; }
        [DisplayName("HL7 vanuit Point")]
        public bool HL7ConnectionCallFromPoint { get; set; }
        
        [DisplayName("Bericht bij niet-opgeslagen data")]
        public bool ShowUnloadMessage { get; set; }
        
        [DisplayName("Afbeelding/logo")]
        public bool Image { get; set; }

        [StringLength(50)]
        [DisplayName("Afbeelding/logo naam")]
        public string ImageName { get; set; }

        [DisplayName("Gebruik SSO")]
        public bool SSO { get; set; }

        [StringLength(50)]
        [DisplayName("SSO key")]
        public string SSOKey { get; set; }

        public DateTime? ModifiedTimeStamp { get; set; }

        [DisplayName("Bericht naar huisarts bij afsluiten")]
        public bool MessageToGPByClose { get; set; }

        [DisplayName("Bericht naar huisarts bij definitief maken")]
        public bool MessageToGPByDefinitive { get; set; }

        [StringLength(255)]
        [DisplayName("Zorgmail user")]
        public string ZorgmailUsername { get; set; }

        [StringLength(255)]
        [DisplayName("Zorgmail wachtwoord")]
        public string ZorgmailPassword { get; set; }
        [DisplayName("WS interface extra velden")]
        public bool WSInterfaceExtraFields { get; set; }

        [StringLength(50)]
        [DisplayName("Hash prefix")]
        public string HashPrefix { get; set; }
        [DisplayName("Pagina lock timeout")]
        public int? PageLockTimeout { get; set; }
        [DisplayName("Aanmaken gebruikers via SSO")]
        public bool CreateUserViaSSO { get; set; }
        
        [StringLength(50)]
        [DisplayName("SSO Hash type")]
        public string SSOHashType { get; set; }
        [DisplayName("Gebruik palliatieve zorg")]
        public bool UsesPalliativeCareInVO { get; set; }

        public bool MFARequired { get; set; }

        public MFAAuthType? MFAAuthType { get; set; }

        public int? MFATimeout { get; set; }

        public bool UseAnonymousClient { get; set; }

        public bool UseDigitalSignature { get; set; }

        public bool UseTasks { get; set; }

        public string IPAddresses { get; set; }
        
        public string DeBlockEmails { get; set; }

        public bool DeBlockEmailsToOrganization { get; set; }
        public virtual ICollection<FormTypeOrganization> FormTypeOrganization { get; set; }

        public virtual Region Region { get; set; }

        public virtual ICollection<Template> Template { get; set; }

        public virtual ICollection<Location> Location { get; set; }

        public virtual ICollection<ServiceArea> ServiceArea { get; set; }

        public virtual ICollection<OrganizationProject> OrganizationProjects { get; set; }

        public virtual ICollection<OrganizationSetting> OrganizationSetting { get; set; }

        public virtual ICollection<OrganizationCSVTemplate> OrganizationCSVTemplate { get; set; }

        public virtual ICollection<FormTypeSelectionOrganization> FormTypeSelectionOrganization { get; set; }

        public int? NedapCryptoCertificateID { get; set; }
        [ForeignKey("NedapCryptoCertificateID")]
        public virtual CryptoCertificate NedapCryptoCertificate { get; set; }

        public string NedapMedewerkernummer { get; set; }

        public int? OrganizationLogoID { get; set; }
        public virtual OrganizationLogo OrganizationLogo { get; set; }

    }
}
