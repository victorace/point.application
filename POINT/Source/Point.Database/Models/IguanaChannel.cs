﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Point.Database.Models
{
    public class IguanaChannel
    {
        [Key]
        public string ChannelID { get; set; }
        public string ChannelName { get; set; }
        public bool ChannelOn { get; set; }
        public string Source { get; set; }
        public bool SourceOn { get; set; }
        public string Destination { get; set; }
        public bool DestinationOn { get; set; }
        public int MessagesQueued { get; set; }
        public bool Monitor { get; set; }
        public DateTime TimeStamp { get; set; }
    }
}
