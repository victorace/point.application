using Point.Models.Enums;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Point.Database.Models
{
    [Table("CommunicationQueue")]
    public partial class CommunicationQueue : BaseEntity
    {
        public int CommunicationQueueID { get; set; }

        public int? TransferID { get; set; }

        public int? ClientID { get; set; }

        public DateTime? Timestamp { get; set; }

        public int? EmployeeID { get; set; }

        public int? OrganizationID { get; set; }

        private CommunicationLogType? _MailType;
        public CommunicationLogType? MailType
        {
            get { return _MailType; }
            set { _MailType = value; }
        }

        public int? FormSetVersionID { get; set; }
        public int? TransferAttachmentID { get; set; }

        public int? ReceivingOrganizationID { get; set; }
        public int? ReceivingDepartmentID { get; set; }

        [StringLength(4000)]
        public string ReasonNotSend { get; set; }

        public bool Handled { get; set; }
    }
}
