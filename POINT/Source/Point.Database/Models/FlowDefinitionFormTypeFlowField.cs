﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Point.Database.Models
{
    [Table("FlowDefinitionFormTypeFlowField")]
    public partial class FlowDefinitionFormTypeFlowField : BaseEntity
    {
        public int FlowDefinitionFormTypeFlowFieldID { get; set; }
        public int FlowDefinitionID { get; set; }
        public int FormTypeID { get; set; }
        public int FlowFieldID { get; set; }
        public bool TakeOver { get; set; }
    }
}
