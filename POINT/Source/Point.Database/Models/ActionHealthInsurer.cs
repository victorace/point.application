using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Point.Database.Repository;

namespace Point.Database.Models
{
    [Table("ActionHealthInsurer")]
    public partial class ActionHealthInsurer : BaseEntity
    {
        public int ActionHealthInsurerID { get; set; }

        public int? ActionCodeHealthInsurerID { get; set; }

        public int? TransferID { get; set; }

        public int? FormSetVersionID { get; set; }

        public int? Amount { get; set; }
        public int? UnitFrequency { get; set; }
        public string CustomFrequency { get; set; }

        [StringLength(10)]
        public string Unit { get; set; }

        public int? ExpectedTime { get; set; }

        [StringLength(4000)]
        public string Definition { get; set; }

        public DateTime? TimeStamp { get; set; }

        public int? EmployeeID { get; set; }

        public int? ScreenID { get; set; }

        public virtual ActionCodeHealthInsurer ActionCodeHealthInsurer { get; set; }

        public virtual FormSetVersion FormSetVersion { get; set; }

        public virtual Transfer Transfer { get; set; }
    }
}
