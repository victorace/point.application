using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Point.Database.Repository;

namespace Point.Database.Models
{
    [Table("Doctor")]
    public partial class Doctor : BaseEntity
    {
        public int DoctorID { get; set; }

        [StringLength(255)]
        [Required]
        public string Name { get; set; }

        [StringLength(255)]
        [Required]
        public string SearchName { get; set; }

        public int? SpecialismID { get; set; }

        public int? OrganizationID { get; set; }
        public virtual Organization Organization { get; set; }

        [StringLength(50)]
        public string AGB { get; set; }

        [StringLength(50)]
        public string BIG { get; set; }

        [StringLength(50)]
        public string PhoneNumber { get; set; }

        public bool Inactive { get; set; }

        public virtual Specialism Specialism { get; set; }
        public virtual ICollection<DoctorLocation> DoctorLocation { get; set; } = new HashSet<DoctorLocation>();
        
    }
}
