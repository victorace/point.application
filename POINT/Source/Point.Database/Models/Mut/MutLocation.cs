﻿using Point.Database.Repository;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Point.Database.Models
{
    public class MutLocation : BaseEntity, ILogAction, ILogEntryWithID
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MutLocationID { get; set; }

        public int? LogLocationID { get; set; }

        [Required]
        public int LocationID { get; set; }

        [Required]
        public int OrganizationID { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        [StringLength(255)]
        public string StreetName { get; set; }

        [StringLength(255)]
        public string Number { get; set; }

        [StringLength(255)]
        public string Country { get; set; }

        [StringLength(255)]
        public string PostalCode { get; set; }

        [StringLength(255)]
        public string City { get; set; }

        public int? TransferPointID { get; set; }

        public bool? Inactive { get; set; }

        [StringLength(255)]
        public string PhoneNumber { get; set; }

        [StringLength(255)]
        public string FaxNumber { get; set; }

        [StringLength(255)]
        public string EmailAddress { get; set; }

        [StringLength(255)]
        public string FlowParticipation { get; set; }

        public DateTime? CapacityNotAvailbleTill { get; set; }

        [Required]
        public bool RecieveEmail { get; set; }

        [Required]
        public bool Capacity { get; set; }

        [Required]
        public int NumberPlaces { get; set; }

        [StringLength(255)]
        public string CapacityInfo { get; set; }

        public DateTime? CapacityInfoChanged { get; set; }

        public bool? IPCheck { get; set; }

        [StringLength(255)]
        public string IPAddress { get; set; }

        [StringLength(255)]
        public string Area { get; set; }

        public DateTime? TimeStamp { get; set; }

        public int? EmployeeID { get; set; }

        public int? ScreenID { get; set; }

        public virtual LogLocation LogLocation { get; set; }

        public virtual Location Location { get; set; }

        public virtual Organization Organization { get; set; }

        public virtual Employee Employee { get; set; }

        [NotMapped]
        public string Action
        {
            get
            {
                return LogLocation?.Action;
            }
        }
    }
}
