﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Point.Database.Repository;

namespace Point.Database.Models
{
    public class MutActionHealthInsurer : BaseEntity
    {
        public int MutActionHealthInsurerID { get; set; }
        public int? LogActionHealthInsurerID {get;set;}
        public int ActionHealthInsurerID  {get;set;}
        public int? ActionCodeHealthInsurerID { get; set; }
        public int? TransferID { get; set; }
        public int? FormSetVersionID { get; set; }
        public int? Amount { get; set; }
	    public string Unit { get; set; }
	    public int? ExpectedTime { get; set; }
	    public string Definition { get; set; }
	    public DateTime? TimeStamp { get; set; }
	    public int? EmployeeID { get; set; }
	    public int? ScreenID { get; set; }

        [ForeignKey("ActionCodeHealthInsurerID")]
        public virtual ActionCodeHealthInsurer ActionCodeHealthInsurer { get; set; }

        [ForeignKey("FormSetVersionID")]
        public virtual FormSetVersion FormSetVersion { get; set; }

        [ForeignKey("LogActionHealthInsurerID")]
        public virtual LogActionHealthInsurer LogActionHealthInsurer { get; set; }

        [ForeignKey("EmployeeID")]
        public virtual Employee Employee { get; set; }
        
    }
}
