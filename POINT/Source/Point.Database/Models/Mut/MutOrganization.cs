﻿using Point.Database.Repository;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Point.Database.Models
{
    public class MutOrganization : BaseEntity, ILogAction, ILogEntryWithID
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MutOrganizationID { get; set; }

        public int? LogOrganizationID { get; set; }

        [Required]
        public int OrganizationID { get; set; }

        [StringLength(500)]
        public string Name { get; set; }

        [Required]
        public int OrganizationTypeID { get; set; }

        public bool? Inactive { get; set; }

        public bool? HL7Interface { get; set; }

        public bool? WSInterface { get; set; }

        public int? InterfaceNumber { get; set; }

        public int? RegionID { get; set; }

        public int? ChangePasswordPeriod { get; set; }

        public bool? ShortConnectionID { get; set; }

        [StringLength(20)]
        public string AGB { get; set; }

        [StringLength(255)]
        public string URL { get; set; }

        public bool? ConnectionZorgkantoor { get; set; }

        public bool? HL7InterfaceExtraFields { get; set; }

        public int? OrganizationSubType { get; set; }

        public bool? HL7ConnectionDifferentMenu { get; set; }

        public bool? HL7ConnectionCallFromPoint { get; set; }

        [Required]
        public bool UsesRadius { get; set; }

        public bool? UsesPatientPortal { get; set; }

        public bool? ShowUnloadMessage { get; set; }

        public bool? ButtonRedirectOrganization { get; set; }

        [StringLength(25)]
        public string ButtonRedirectOrganizationName { get; set; }

        [StringLength(255)]
        public string URLRedirectOrganization { get; set; }

        public bool? Image { get; set; }

        [StringLength(50)]
        public string ImageName { get; set; }

        public bool? SSO { get; set; }

        [StringLength(50)]
        public string SSOKey { get; set; }

        public DateTime? TimeStamp { get; set; }

        public int? EmployeeID { get; set; }

        public int? ScreenID { get; set; }

        public bool? MessageToGPByClose { get; set; }

        public bool? MessageToGPByDefinitive { get; set; }

        public string ZorgmailUsername { get; set; }

        [StringLength(255)]
        public string ZorgmailPassword { get; set; }

        public bool? WSInterfaceExtraFields { get; set; }

        [StringLength(50)]
        public string HashPrefix { get; set; }

        public int? PageLockTimeout { get; set; }

        public bool? CreateUserViaSSO { get; set; }

        public bool? NVZReport { get; set; }

        [StringLength(50)]
        public string SSOHashType { get; set; }

        [Required]
        public bool UsesPalliativeCareInVO { get; set; }

        [Required]
        public bool IsAdminOrganization { get; set; }

        public bool UseAnonymousClient { get; set; }
        public int? OrganizationLogoID { get; set; }

        public virtual LogOrganization LogOrganization { get; set; }

        public virtual Organization Organization { get; set; }

        public virtual OrganizationType OrganizationType { get; set; }

        public virtual Region Region { get; set; }

        public virtual Employee Employee { get; set; }
        public virtual OrganizationLogo OrganizationLogo { get; set; }

        [NotMapped]
        public string Action
        {
            get
            {
                return LogOrganization?.Action;
            }
        }
        
    }
}
