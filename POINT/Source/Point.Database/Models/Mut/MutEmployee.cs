﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Point.Database.Attributes;

namespace Point.Database.Models
{
    public class MutEmployee
    {
        public int MutEmployeeID { get; set; }

        public int LogEmployeeID { get; set; }

        public int EmployeeID { get; set; }

        public int DepartmentID { get; set; }

        [StringLength(255)]
        public string FirstName { get; set; }

        [StringLength(50)]
        public string MiddleName { get; set; }

        [Required]
        [StringLength(255)]
        public string LastName { get; set; }

        [Required]
        [StringLength(1)]
        public string Gender { get; set; }

        [StringLength(255)]
        public string Position { get; set; }

        [StringLength(50)]
        public string PhoneNumber { get; set; }

        public Guid? UserId { get; set; }

        [StringLength(255)]
		[EmailAddressValidator]
        public string EmailAddress { get; set; }

        public bool? InActive { get; set; }

        [StringLength(50)]
        public string ExternID { get; set; }

        public bool? ChangePasswordByEmployee { get; set; }

        [StringLength(50)]
        public string BIG { get; set; }

        public bool? ViewRights { get; set; }

        public DateTime? TimeStamp { get; set; }

        public int? ModifiedByEmployeeID { get; set; }
        [ForeignKey("ModifiedByEmployeeID")]
        public virtual Employee ModifiedByEmployee { get; set; }

        public int? ScreenID { get; set; }

        public string MembershipRolesAction { get; set; }

        public string UserLogin { get; set; }

        public bool? AllOrganizationDepartmentsLinked { get; set; }

        public bool? MSVTFunctionality { get; set; }

        public virtual Aspnet_Users aspnet_Users { get; set; }

        public virtual Department Department { get; set; }

        public string DossierLevel { get; set; }

        public string FunctionRoles { get; set; }
        
    }
}
