﻿using Point.Database.Repository;
using Point.Models.Enums;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Point.Database.Models
{
    public class MutFlowFieldValue : BaseEntity, ILogEntry  
    {
        public int MutFlowFieldValueID { get; set; }

        [Required]
        public int FlowFieldValueID { get; set; }
        [ForeignKey("FlowFieldValueID")]
        public virtual FlowFieldValue FlowFieldValue { get; set; }

        [Required]
        public int FlowFieldID { get; set; }
        [ForeignKey("FlowFieldID")]
        public virtual FlowField FlowField { get; set; }

        public Nullable<int> FormSetVersionID { get; set; }
        [ForeignKey("FormSetVersionID")]
        public virtual FormSetVersion FormSetVersion { get; set; }

        public string Value { get; set; }

        public int? EmployeeID { get; set; }
        [ForeignKey("EmployeeID")]
        public virtual Employee Employee { get; set; }

        public DateTime? Timestamp { get; set; }

        [StringLength(255)]
        public string ScreenName { get; set; }

        public Source Source { get; set; }
    }
}