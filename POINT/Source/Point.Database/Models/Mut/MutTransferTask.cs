﻿using Point.Models.Enums;
using Point.Database.Repository;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Point.Database.Models
{
    public class MutTransferTask : BaseEntity, ILogEntry  
    {
        public int MutTransferTaskID { get; set; }

        public int TransferTaskID { get; set; }

        public int TransferID { get; set; }

        [StringLength(255)]
        public string Description { get; set; }

        [StringLength(4000)]
        public string Comments { get; set; }

        public DateTime CreatedDate { get; set; }

        public int CreatedByEmployeeID { get; set; }
        [ForeignKey("CreatedByEmployeeID")]
        public virtual Employee CreatedByEmployee { get; set; }

        public DateTime DueDate { get; set; }

        public DateTime? CompletedDate { get; set; }

        public int? CompletedByEmployeeID { get; set; }
        [ForeignKey("CompletedByEmployeeID")]
        public virtual Employee CompletedByEmployee { get; set; }

        public Status Status { get; set; }

        public bool Inactive { get; set; }
        
        public DateTime? Timestamp { get; set; }
       
        public int? EmployeeID { get; set; }
        [ForeignKey("EmployeeID")]
        public virtual Employee Employee { get; set; }

        [StringLength(255)]
        public string ScreenName { get; set; }
        
    }
}
