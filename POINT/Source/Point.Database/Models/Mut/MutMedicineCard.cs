﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Point.Database.Repository;

namespace Point.Database.Models
{
    public class MutMedicineCard : BaseEntity
    {
        public int MutMedicineCardID { get; set; }
        public int LogMedicineCardID { get; set; }
        public int MedicineCardID { get; set; }
        public int TransferID { get; set; }
        public int MedicineID { get; set; }
        public string NotRegisteredMedicine { get; set; }
        public string Dosage { get; set; }
        public string Frequence { get; set; }
        public DateTime? FinishDate { get; set; }
        public string Prescription { get; set; }
        public DateTime? TimeStamp { get; set; }
        public int? EmployeeID { get; set; }
        public int? ScreenID { get; set; }
        public int? FormSetVersionID { get; set; }

        [ForeignKey("EmployeeID")]
        public virtual Employee Employee { get; set; }
        
    }
}
