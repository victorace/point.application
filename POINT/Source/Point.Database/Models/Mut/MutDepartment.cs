﻿using Point.Database.Repository;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Point.Database.Models
{
    public class MutDepartment : BaseEntity, ILogAction, ILogEntryWithID
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MutDepartmentID { get; set; }

        public int? LogDepartmentID { get; set; }

        [Required]
        public int LocationID { get; set; }

        [Required]
        public int DepartmentID { get; set; }

        [Required]
        public int DepartmentTypeID { get; set; }

        public int? SpecialismID { get; set; }
        public int? AfterCareType1ID { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        [StringLength(255)]
        public string PhoneNumber { get; set; }

        [StringLength(255)]
        public string FaxNumber { get; set; }

        [StringLength(255)]
        public string EmailAddress { get; set; }

        [Required]
        public bool RecieveEmail { get; set; }

        public int? MainContactID { get; set; }

        public bool? Inactive { get; set; }

        public int? SpecialismID2 { get; set; }
        
        public bool? RehabilitationCenter { get; set; }

        public bool? IntensiveRehabilitationNursinghome { get; set; }

        [StringLength(255)]
        public string EmailAddressTransferSend { get; set; }

        public bool? RecieveEmailTransferSend { get; set; }

        [StringLength(255)]
        public string ZorgmailTransferSend { get; set; }

        public bool? RecieveZorgmailTransferSend { get; set; }

        public DateTime? TimeStamp { get; set; }

        public int? EmployeeID { get; set; }

        public int? ScreenID { get; set; }

        public bool? CapacityFunctionality { get; set; }

        public bool? PointParticipatorCareAccess { get; set; }

        public bool? RecieveCDATransferSend { get; set; }

        [StringLength(10)]
        public string CapacityDepartmentHomeCare { get; set; }

        [StringLength(10)]
        public string CapacityDepartmentNextHomeCare { get; set; }

        [StringLength(10)]
        public string CapacityDepartment3HomeCare { get; set; }

        [StringLength(10)]
        public string CapacityDepartment4HomeCare { get; set; }

        public virtual LogDepartment LogDepartment { get; set; }

        public virtual Location Location { get; set; }

        public virtual DepartmentType DepartmentType { get; set; }

        public virtual Department Department { get; set; }

        [ForeignKey("AfterCareType1ID")]
        public virtual AfterCareType AfterCareType1 { get; set; }

        public virtual Employee Employee { get; set; }

        [NotMapped]
        public string Action
        {
            get
            {
                return LogDepartment?.Action;
            }
        }
        
    }
}
