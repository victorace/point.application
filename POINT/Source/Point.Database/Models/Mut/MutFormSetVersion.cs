﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Point.Database.Repository;

namespace Point.Database.Models
{
    public class MutFormSetVersion : BaseEntity, ILogEntryWithID  
    {
        public MutFormSetVersion()
        {
        }

        public int MutFormSetVersionID { get; set; }

        public int LogFormSetVersionID { get; set; }

        public int TransferID { get; set; }

        public int FormTypeID { get; set; }

        public int FormSetVersionID { get; set; }

        public DateTime? CreateDate { get; set; }

        [StringLength(400)]
        public string CreatedBy { get; set; }

        public int? CreatedByID { get; set; }

        public DateTime? UpdateDate { get; set; }

        [StringLength(400)]
        public string UpdatedBy { get; set; }

        [StringLength(400)]
        public string UserDataName { get; set; }

        public int? UserDataID { get; set; }

        [StringLength(50)]
        public string UserDataPhoneNumber { get; set; }

        [StringLength(400)]
        public string Description { get; set; }

        public bool? Definitive { get; set; }

        public DateTime? DefinitiveDate { get; set; }

        public bool? Deleted { get; set; }

        public DateTime? DeletedDate { get; set; }

        public int? ParentFormSetVersionID { get; set; }

        public DateTime? TimeStamp { get; set; }

        public int? EmployeeID { get; set; }

        public int? ScreenID { get; set; }

        public virtual FormType FormType { get; set; }

        public virtual FormSetVersion FormSetVersion { get; set; }
        
    }
}
