﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Point.Database.Models
{
    public class FormTypeSelectionOrganization
    {
        [Column(Order = 0), Key]
        public int FormTypeSelectionID { get; set; }
        [Column(Order = 1), Key]
        public int OrganizationID { get; set; }

        public virtual FormTypeSelection FormTypeSelection { get; set; }
        public virtual Organization Organization { get; set; }
    }
}
