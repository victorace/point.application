﻿using System.Collections.Generic;
using System.Linq;
using Point.Models.Enums;

namespace Point.Database.Models
{
    public class OrganizationSearch
    {
        private List<FlowDirection> _receivingFlowDirections = new List<FlowDirection> { FlowDirection.Any, FlowDirection.Receiving };
        private List<FlowDirection>  _sendingFlowDirections = new List<FlowDirection> { FlowDirection.Any, FlowDirection.Sending };
        public OrganizationSearch()
        {
            OrganizationSearchPostalCode = new HashSet<OrganizationSearchPostalCode>();
            OrganizationSearchRegion = new HashSet<OrganizationSearchRegion>();
            OrganizationSearchParticipation = new HashSet<OrganizationSearchParticipation>();
        }

        public int OrganizationSearchID { get; set; }
        public int OrganizationID { get; set; }
        public string OrganizationName { get; set; }
        public int OrganizationTypeID { get; set; }
        public string OrganizationTypeName { get; set; }
        public int LocationID { get; set; }
        public string LocationName { get; set; }
        public string LocationPostcode { get; set; }
        public int DepartmentID { get; set; }
        public int DepartmentTypeID { get; set; }
        public string DepartmentName { get; set; }
        public string City { get; set; }
        public bool IsRehabilitationCenter { get; set; }
        public bool IsIntensiveRehabilitationNursingHome { get; set; }
        public string AfterCareType { get; set; }
        public string AfterCareCategory { get; set; }
        public int? Capacity0 { get; set; }
        public int? Capacity1 { get; set; }
        public int? Capacity2 { get; set; }
        public int? Capacity3 { get; set; }

        public virtual ICollection<OrganizationSearchPostalCode> OrganizationSearchPostalCode { get; set; }
        public virtual ICollection<OrganizationSearchRegion> OrganizationSearchRegion { get; set; }
        public virtual ICollection<OrganizationSearchParticipation> OrganizationSearchParticipation { get; set; }
        public bool IsReceivingParty => OrganizationSearchParticipation.Any(it => (it.Participation == Participation.Active || it.Participation == Participation.ActiveButInvisibleInSearch) && _receivingFlowDirections.Contains(it.FlowDirection));
        public bool IsSendingParty => OrganizationSearchParticipation.Any(it => (it.Participation == Participation.Active || it.Participation == Participation.ActiveButInvisibleInSearch) && _sendingFlowDirections.Contains(it.FlowDirection));
        public string RegionNames => string.Join(", ", OrganizationSearchRegion.Select(it => it.RegionName));

    }
}
