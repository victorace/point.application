﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Point.Database.Repository;

namespace Point.Database.Models
{
    [Table("WebRequestLog")]
    public class WebRequestLog : BaseEntity
    {
        public int WebRequestLogID { get; set; }

        public DateTime TimeStamp { get; set; }
    
        public string Sent { get; set; }
        public string Received { get; set; }
        public string ExceptionMessage { get; set; }
        
    }
}
