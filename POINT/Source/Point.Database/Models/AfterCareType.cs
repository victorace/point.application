﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Point.Database.Repository;

namespace Point.Database.Models
{
    public class AfterCareType : BaseEntity
    {
        public AfterCareType()
        {
            Department1 = new HashSet<Department>();
        }

        public virtual ICollection<Department> Department1 { get; set; }

        //public virtual ICollection<DepartmentCapacityPublic> DepartmentCapacityPublic1 { get; set; }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AfterCareTypeID { get; set; }
        [StringLength(255), Required]
        public string Name { get; set; }
        [StringLength(10), Required]
        public string Abbreviation { get; set; }
        [Required]
        public DateTime StartDate { get; set; }
        [Required]
        public DateTime EndDate { get; set; }

        [StringLength(50), Required]
        public string ReportCode { get; set; }
        [Required]
        public int Sortorder { get; set; }

        public int? AfterCareCategoryID { get; set; }
        public virtual AfterCareCategory AfterCareCategory { get; set; }
        
        public int? AfterCareGroupID { get; set; }
        public virtual AfterCareGroup AfterCareGroup { get; set; }

        public int OrganizationTypeID { get; set; }
        public bool Inactive { get; set; }
        public string Description { get; set; }

        public virtual ICollection<AfterCareTypeAfterCareFinancing> AfterCareTypeAfterCareFinancing { get; set; }
        public virtual ICollection<AfterCareTypeFormType> AfterCareTypeFormType { get; set; }
    }
}
