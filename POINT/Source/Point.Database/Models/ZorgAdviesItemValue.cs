using System.ComponentModel.DataAnnotations.Schema;
using Point.Database.Repository;

namespace Point.Database.Models
{
    [Table("ZorgAdviesItemValue")]
    public partial class ZorgAdviesItemValue : BaseEntity
    {
        

        public int ZorgAdviesItemValueID { get; set; }
        public int FlowInstanceID { get; set; }
        public int ZorgAdviesItemID { get; set; }
        public string Text { get; set; }
        public string ActionBy { get; set; }
        public int? ActionFrequency { get; set; }
        public int? ActionTimespan { get; set; }
        public int? ActionTime { get; set; }

        public virtual FlowInstance FlowInstance { get; set; }
        public virtual ZorgAdviesItem ZorgAdviesItem { get; set; }
        
    }
}
