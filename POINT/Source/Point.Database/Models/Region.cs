using Point.Log.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Point.Database.Models
{
    [Table("Region"), Description("Regio")]
    public partial class Region : BaseEntity, ILoggable
    {
        public Region()
        {
            ActionCodeHealthInsurerRegio = new HashSet<ActionCodeHealthInsurerRegio>();
            Organization = new HashSet<Organization>();
            Templet = new HashSet<Templet>();
            FormTypeRegion = new HashSet<FormTypeRegion>();
            LocationRegion = new HashSet<LocationRegion>();
        }

        public int RegionID { get; set; }

        [StringLength(255)]
        public string Name { get; set; }

        public bool CapacityBeds { get; set; }

        public DateTime? ModifiedTimeStamp { get; set; }

        public virtual ICollection<ActionCodeHealthInsurerRegio> ActionCodeHealthInsurerRegio { get; set; }

        public virtual ICollection<Organization> Organization { get; set; }

        public virtual ICollection<Templet> Templet { get; set; }

        public virtual ICollection<FormTypeRegion> FormTypeRegion { get; set; }

        public virtual ICollection<LocationRegion> LocationRegion { get; set; }
        public bool CapacityBedsPublic { get; set; }

    }
}
