﻿using Point.Models.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Point.Database.Models
{
    public class PhaseDefinitionNavigation
    {
        [Column(Order = 0), Key, ForeignKey("PhaseDefinition")]
        public int PhaseDefinitionID { get; set; }

        [Column(Order = 1), Key, ForeignKey("NextPhaseDefinition")]
        public int NextPhaseDefinitionID { get; set; }

        [Column(Order = 2), Key]
        public ActionTypeID ActionTypeID { get; set; }
        public string ActionLabel { get; set; }
        public bool Validate { get; set; }
        public bool Visible { get; set; }

        public virtual PhaseDefinition PhaseDefinition { get; set; }
        public virtual PhaseDefinition NextPhaseDefinition { get; set; }
    }
}
