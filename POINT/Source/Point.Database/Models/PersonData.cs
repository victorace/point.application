﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Point.Database.Attributes;
using Point.Models.LDM.EOverdracht30.Enums;

namespace Point.Database.Models
{
    public class PersonData : BaseEntity
    {
        public int PersonDataID { get; set; }

        [DisplayName("Geslacht")]
        public Geslacht? Gender { get; set; }

        [StringLength(255)]
        [DisplayName("Aanhef")]
        public string Salutation { get; set; }

        [StringLength(50)]
        [DisplayName("Voorletters")]
        public string Initials { get; set; }

        [StringLength(255)]
        [DisplayName("Voornaam")]
        public string FirstName { get; set; }

        [StringLength(50)]
        [DisplayName("Voorvoegsel")]
        public string MiddleName { get; set; }

        [StringLength(255)]
        [DisplayName("Achternaam (volledig)")]
        public string LastName { get; set; }

        [StringLength(255)]
        [DisplayName("Geboortenaam")]
        public string MaidenName { get; set; }

        [StringLength(255)]
        [DisplayName("Partnernaam")]
        public string PartnerName { get; set; }

        [StringLength(50)]
        [DisplayName("Voorvoegsel")]
        public string PartnerMiddleName { get; set; }

        [DisplayName("Geboortedatum")]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}")]
        public DateTime? BirthDate { get; set; }

        [StringLength(50)]
        [DisplayName("Telefoonnummer 1")]
        public string PhoneNumberGeneral { get; set; }

        [StringLength(50)]
        [DisplayName("Telefoonnummer 2")]
        public string PhoneNumberMobile { get; set; }

        [StringLength(50)]
        [DisplayName("Telefoonnummer 3")]
        public string PhoneNumberWork { get; set; }

        [StringLength(255)]
        [DisplayName("E-mail")]
        public string Email { get; set; }
        
    }
}
