﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Point.Models.Enums;
using Point.Database.Repository;

namespace Point.Database.Models
{
    public partial class Frequency : BaseEntity, ILogEntry
    {
        public int FrequencyID { get; set; }

        public int TransferID { get; set; }
        [ForeignKey("TransferID")]
        public virtual Transfer Transfer { get; set; }

        [Required(ErrorMessage = "Naam is verplicht.")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Frequentie is verplicht.")]
        [Range(1, int.MaxValue, ErrorMessage = "Frequentie moet een positief getal zijn")]
        public int Quantity { get; set; }

        [Required]
        public FrequencyType Type { get; set; }

        [Required(ErrorMessage = "Start datum is verplicht.")]
        public DateTime StartDate { get; set; }

        [Required(ErrorMessage = "Eind datum is verplicht.")]
        public DateTime EndDate { get; set; }

        public FrequencyStatus Status { get; set; }

        public FrequencyFeedbackType FeedbackType { get; set; }

        public int? EmployeeID { get; set; }
        [ForeignKey("EmployeeID")]
        public virtual Employee Employee { get; set; }

        public DateTime? Timestamp { get; set; }

        [StringLength(255)]
        public string ScreenName { get; set; }

        public bool Cancelled { get; set; }
        
    }
}
