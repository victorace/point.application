using System.ComponentModel.DataAnnotations.Schema;

namespace Point.Database.Models
{
    [Table("TemplateDefault")]
    public partial class TemplateDefault : BaseEntity
    {
        public int TemplateDefaultID { get; set; }

        public int? TemplateTypeID { get; set; }

        [Column(TypeName = "text")]
        public string TemplateDefaultText { get; set; }

        public bool Inactive { get; set; }

        public int? OrganizationTypeID { get; set; }

        public virtual TemplateType TemplateType { get; set; }
    }
}
