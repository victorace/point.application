﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using Point.Database.Repository;

namespace Point.Database.Models
{
    [Table("FlowFieldAttributeException")]
    public partial class FlowFieldAttributeException : BaseEntity 
    {
        public int FlowFieldAttributeExceptionID { get; set; }
        public int FlowFieldAttributeID { get; set; }
        public virtual FlowFieldAttribute FlowFieldAttribute { get; set; }
        public int? RegionID { get; set; }
        public int? OrganizationID { get; set; }

        [DefaultValue(false)]
        public bool ReadOnly { get; set; }

        [DefaultValue(true)]
        public bool Visible { get; set; }

        [DefaultValue(false)]
        public bool Required { get; set; }

        [DefaultValue(false)]
        public bool SignalOnChange { get; set; }
        
        public int? PhaseDefinitionID { get; set; }

        public int? RequiredByFlowFieldID { get; set; }

        public string RequiredByFlowFieldValue { get; set; }

        public virtual FlowField RequiredByFlowField { get; set; }

        public virtual Organization Organization { get; set; }
        public virtual Region Region { get; set; }
        public virtual PhaseDefinition PhaseDefinition { get; set; }
    }
}
