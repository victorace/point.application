﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Point.Database.Repository;

namespace Point.Database.Models
{
    public class AfterCareGroup : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AfterCareGroupID { get; set; }
        [StringLength(255), Required]
        public string Name { get; set; }

        [Required]
        public int Sortorder { get; set; }

        public int? AfterCareTileGroupID { get; set; }
        public virtual AfterCareTileGroup AfterCareTileGroup { get; set; }

        public virtual ICollection<AfterCareType> AfterCareTypes { get; set; }

        // XXX (AfterCareTypes)
        public virtual ICollection<DepartmentCapacityPublic> DepartmentCapacityPublics { get; set; }
    }
}
