using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Point.Database.Repository;

namespace Point.Database.Models
{
    [Table("ZorgAdviesItem")]
    public partial class ZorgAdviesItem : BaseEntity
    {
        public int ZorgAdviesItemID { get; set; }
        public int ZorgAdviesItemGroupID { get; set; }

        public int? OrderNumber { get; set; }
        public string SubCategory { get; set; }
        public string Label { get; set; }
        public bool NeedsFrequencyAndTime { get; set; }

        public virtual ICollection<ZorgAdviesItemValue> ZorgAdviesItemValue { get; set; }
        public virtual ZorgAdviesItemGroup ZorgAdviesItemGroup { get; set; }
        
    }
}
