﻿using System.ComponentModel.DataAnnotations.Schema;
using Point.Database.Repository;

namespace Point.Database.Models
{
    public class FrequencyTransferMemo : BaseEntity
    {
        public int FrequencyTransferMemoID { get; set; }

        public int FrequencyID { get; set; }
        [ForeignKey("FrequencyID")]
        public virtual Frequency Frequency { get; set; }

        public int TransferMemoID { get; set; }
        [ForeignKey("TransferMemoID")]
        public virtual TransferMemo TransferMemo { get; set; }
        
    }
}
