﻿
namespace Point.Database.Models.Constants
{
    public static class MenuTypes
    {
        public const string Main = "Main";
        public const string Filter = "Filter";
        public const string Zoeken = "Zoeken";
        public const string Proces = "Proces";
        public const string Aanvullend = "Aanvullend";
        public const string Actie = "Actie";
        public const string ExtReference = "ExtReference";
    }
}
