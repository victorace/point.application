﻿
namespace Point.Database.Models.Constants
{
    public class RegularExpressions
    {
        public const string EmailAddress = @"\A(?:[a-z|A-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z|A-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z|A-Z0-9](?:[a-z|A-Z0-9-]*[a-z|A-Z0-9])?\.)+[a-z|A-Z0-9](?:[a-z|A-Z0-9-]*[a-z|A-Z0-9])?)\Z";

    }
}
