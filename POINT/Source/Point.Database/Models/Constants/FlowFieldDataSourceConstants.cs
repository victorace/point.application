﻿
namespace Point.Database.Models.Constants
{
    public static class FlowFieldDataSourceConstants
    {
        // FlowFieldID 191 - FlowFieldDataSourceID 71
        public static class PrognoseVerwachteOntwikkeling
        {
            public const string DeteriorationWithin1Year = "Er wordt een verslechtering verwacht binnen een jaar";
            public const string FullRecoveryWithin3Months = "Uitzicht op volledig herstel binnen 3 maanden";
            public const string LifeExpectancyLessThan3Months = "Verwachte levensduur minder dan 3 maanden";
            public const string PartialRecoveryWithin1Year = "Uitzicht op gedeeltelijk herstel/verbetering binnen een jaar";
            public const string RecoveryIn3To6Months = "Herstel 3 tot 6 maanden";
            public const string Unchanged = "De gezondheidstoestand blijft hetzelfde";
            public const string Unknown = "Onbekend: prognose niet beschikbaar of niet mogelijk";
        }

        // FlowFieldID 1806 - FlowFieldDataSourceID 72
        public static class PrognoseRHD
        {
            public const string DeteriorationWithin1Year = "Er wordt een verslechtering verwacht binnen een jaar";
            public const string FullRecoveryWithin3Months = "Uitzicht op volledig herstel binnen 3 maanden";
            public const string LifeExpectancyLessThan1Year = "Verwachte levensduur minder dan 1 jaar";
            public const string LifeExpectancyLessThan3Months = "Verwachte levensduur minder dan 3 maanden";
            public const string PartialRecoveryWithin1Year = "Uitzicht op gedeeltelijk herstel/verbetering binnen een jaar";
            public const string RecoveryIn3To6Months = "Herstel 3 tot 6 maanden";
            public const string Unchanged = "De gezondheidstoestand blijft hetzelfde";
            public const string Unknown = "Onbekend: prognose niet beschikbaar of niet mogelijk";
        }

        // FlowFieldID 642 - FlowFieldDataSourceID 121
        public static class VOFormulierType
        {
            public const string Pdf = "Pdf";
            public const string Standard = "Standaard";
            public const string ToPatient = "Aan patient";
        }
    }
}
