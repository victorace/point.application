﻿
using Point.Models.Enums;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Point.Database.Models
{
    public class FlowDefinitionFormType
    {
        [Key , Column(Order = 0)]
        public FlowDefinitionID FlowDefinitionID { get; set; }
        [Key, Column(Order = 1)]
        public int FormTypeID { get; set; }

        public virtual FlowDefinition FlowDefinition { get; set; }
        public virtual FormType FormType { get; set; }        
    }
}
