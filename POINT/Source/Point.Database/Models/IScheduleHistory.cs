﻿using System;

namespace Point.Database.Models
{
    public interface IScheduleHistory : ICommunicationLog
    {
        int FrequencyID { get; set; }
        DateTime Timestamp { get; set; }
    }
}
