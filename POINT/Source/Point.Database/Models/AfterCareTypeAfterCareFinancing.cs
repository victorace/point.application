﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Point.Database.Models
{
    public class AfterCareTypeAfterCareFinancing
    {
        [Key]
        [Column(Order = 1)]
        public int AfterCareTypeID { get; set; }
        public AfterCareType AfterCareType { get; set; }

        [Key]
        [Column(Order = 2)]
        public int AfterCareFinancingID { get; set; }
        public AfterCareFinancing AfterCareFinancing { get; set; }
    }
}
