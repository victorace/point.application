﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Point.Database.Models
{
    [Table("FlowFieldAttribute")]
    public class FlowFieldAttribute
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FlowFieldAttributeID { get; set; }

        [Required]
        public int FlowFieldID { get; set; }
        [ForeignKey("FlowFieldID")]
        public virtual FlowField FlowField { get; set; }

        [Required]
        public int FormTypeID { get; set; }
        [ForeignKey("FormTypeID")]
        public virtual FormType FormType { get; set; }

        [DefaultValue(true)]
        public bool Required { get; set; }

        [DefaultValue(true)]
        public bool Visible { get; set; }

        [DefaultValue(false)]
        public bool ReadOnly { get; set; }

        [DefaultValue(false)]
        public bool SignalOnChange { get; set; }

        [DefaultValue(true)]
        public bool IsActive { get; set; }

        public int? OriginFormTypeID { get; set; }

        public int? OriginFlowFieldID { get; set; }

        public bool OriginCopyToAll { get; set; }

        public int? RequiredByFlowFieldID { get; set; }

        public string RequiredByFlowFieldValue { get; set; }

        public virtual FlowField RequiredByFlowField { get; set; }

        public virtual ICollection<FlowFieldAttributeException> FlowFieldAttributeException { get; set; }
        
    }
}
