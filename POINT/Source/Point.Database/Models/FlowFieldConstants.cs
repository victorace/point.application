﻿namespace Point.Database.Models
{
    public static class FlowFieldConstants
    {
        public const string Value_Ja = "Ja";
        public const string Value_Nee = "Nee";
        public const string Value_True = "true";
        public const string Value_False = "false";

        public const string Value_BeloopOpname_Patientisontslagen = "1";
        public const string Value_BeloopOpname_Patientisoverleden = "2";
        public const string Value_Ontslagbestemming_Huis = "1";
        public const string Value_Ontslagbestemming_Revalidatiecentrum = "2";
        public const string Value_Ontslagbestemming_GRZCVAverpleeghuis = "3";
        public const string Value_Ontslagbestemming_Verpleeghuis = "4";
        public const string Value_Ontslagbestemming_Anderziekenhuis = "5";
        public const string Value_Ontslagbestemming_Buitenland = "6";
        public const string Value_Ontslagbestemming_Overig = "7";
        public const string Value_Ontslagbestemming_Dossiersluiten = "8";
        public const string Value_OntslagbestemmingThuis_Thuismetzorg = "2";
        public const string Value_OntslagbestemmingThuis_Thuismetzorgenbehandeling = "4";

        public static string Value_InBehandeling = "In behandeling";
        public static string Value_OrderMislukt = "Order mislukt";
        public static string Value_OrderBevestigd = "Order bevestigd";
        public static string Value_OrderVerstuurd = "Order verstuurd";

        public static string Value_PrognoseVerwachteOntwikkeling_ExpectedLifeLessThan3Months = "Verwachte levensduur minder dan 3 maanden";
        public static string Value_PrognoseVerwachteOntwikkeling_ExpectedLifeLessThan1Year = "Verwachte levensduur minder dan 1 jaar";


        // 'Telefoonnummer afdeling' (1: String)
        public const int ID_CreatedByDepartmentTelephoneNumber = 1;
        public const string Name_CreatedByDepartmentTelephoneNumber = "CreatedByDepartmentTelephoneNumber";

        // 'In behandeling door' (7: DropDownList)
        public const int ID_DossierOwner = 2;
        public const string Name_DossierOwner = "DossierOwner";

        // 'Telefoonnummer' (1: String)
        public const int ID_DossierOwnerTelephoneNumber = 3;
        public const string Name_DossierOwnerTelephoneNumber = "DossierOwnerTelephoneNumber";

        // 'Datum transfer (gepland)' (9: Date)
        public const int ID_TransferDate = 4;
        public const string Name_TransferDate = "TransferDate";

        // 'Datum verzonden naar ZH' (2: DateTime)
        public const int ID_SendToHospitalTimestamp = 5;
        public const string Name_SendToHospitalTimestamp = "SendToHospitalTimestamp";

        // 'Toelichting' (1: String)
        public const int ID_SendToHospitalComments = 6;
        public const string Name_SendToHospitalComments = "SendToHospitalComments";

        // 'Ziekenhuis' (8: IDText)
        public const int ID_DestinationHospital = 7;
        public const string Name_DestinationHospital = "DestinationHospital";

        // 'Ziekenhuis locatie' (8: IDText)
        public const int ID_DestinationHospitalLocation = 8;
        public const string Name_DestinationHospitalLocation = "DestinationHospitalLocation";

        // 'Afdeling ZH' (8: IDText)
        public const int ID_DestinationHospitalDepartment = 9;
        public const string Name_DestinationHospitalDepartment = "DestinationHospitalDepartment";

        // 'Aanvraag in behandeling genomen door' (7: DropDownList)
        public const int ID_AcceptedBy = 10;
        public const string Name_AcceptedBy = "AcceptedBy";

        // 'Telefoonnummer' (1: String)
        public const int ID_AcceptedByTelephoneNumber = 11;
        public const string Name_AcceptedByTelephoneNumber = "AcceptedByTelephoneNumber";

        // 'Datum aanvraag in behandeling genomen' (2: DateTime)
        public const int ID_AcceptedDateTP = 12;
        public const string Name_AcceptedDateTP = "AcceptedDateTP";

        // 'Toelichting' (1: String)
        public const int ID_DestinationHospitalDepartmentComments = 13;
        public const string Name_DestinationHospitalDepartmentComments = "DestinationHospitalDepartmentComments";

        // 'Versturende zorginstelling' (8: IDText)
        public const int ID_SourceHealthCareProvider = 14;
        public const string Name_SourceHealthCareProvider = "SourceHealthCareProvider";

        // 'Aanpassen' (6: Checkbox)
        public const int ID_AanpassingVanBestaandeZorg = 15;
        public const string Name_AanpassingVanBestaandeZorg = "AanpassingVanBestaandeZorg";

        // 'Aanvraag voor' (5: RadioButton)
        public const int ID_AanvraagVoor = 16;
        public const string Name_AanvraagVoor = "AanvraagVoor";

        // 'Toelichting' (1: String)
        public const int ID_AanwezigheidContactpersoonBijTransfergesprekGewenst = 17;
        public const string Name_AanwezigheidContactpersoonBijTransfergesprekGewenst = "AanwezigheidContactpersoonBijTransfergesprekGewenst";

        // 'Aanwezigheid Contactpersoon bij transfergesprek gewenst' (5: RadioButton)
        public const int ID_AanwezigheidContactpersoonBijTransfergesprekGewenstJaNee = 18;
        public const string Name_AanwezigheidContactpersoonBijTransfergesprekGewenstJaNee = "AanwezigheidContactpersoonBijTransfergesprekGewenstJaNee";

        // 'Eten en drinken' (6: Checkbox)
        public const int ID_BasiszorgEtenEnDrinken = 19;
        public const string Name_BasiszorgEtenEnDrinken = "BasiszorgEtenEnDrinken";

        // 'Per dag' (4: Number)
        public const int ID_BasiszorgEtenEnDrinkenKerenPerDag = 20;
        public const string Name_BasiszorgEtenEnDrinkenKerenPerDag = "BasiszorgEtenEnDrinkenKerenPerDag";

        // 'Per week' (4: Number)
        public const int ID_BasiszorgEtenEnDrinkenKerenPerWeek = 21;
        public const string Name_BasiszorgEtenEnDrinkenKerenPerWeek = "BasiszorgEtenEnDrinkenKerenPerWeek";

        // 'Toelichting' (1: String)
        public const int ID_BasiszorgEtenEnDrinkenToelichting = 22;
        public const string Name_BasiszorgEtenEnDrinkenToelichting = "BasiszorgEtenEnDrinkenToelichting";

        // 'Mobiliteit' (6: Checkbox)
        public const int ID_BasiszorgMobiliteit = 23;
        public const string Name_BasiszorgMobiliteit = "BasiszorgMobiliteit";

        // 'Per dag' (4: Number)
        public const int ID_BasiszorgMobiliteitKerenPerDag = 24;
        public const string Name_BasiszorgMobiliteitKerenPerDag = "BasiszorgMobiliteitKerenPerDag";

        // 'Per week' (4: Number)
        public const int ID_BasiszorgMobiliteitKerenPerWeek = 25;
        public const string Name_BasiszorgMobiliteitKerenPerWeek = "BasiszorgMobiliteitKerenPerWeek";

        // 'Toelichting' (1: String)
        public const int ID_BasiszorgMobiliteitToelichting = 26;
        public const string Name_BasiszorgMobiliteitToelichting = "BasiszorgMobiliteitToelichting";

        // 'Overig' (6: Checkbox)
        public const int ID_BasiszorgOverig = 27;
        public const string Name_BasiszorgOverig = "BasiszorgOverig";

        // 'Per dag' (4: Number)
        public const int ID_BasiszorgOverigKerenPerDag = 28;
        public const string Name_BasiszorgOverigKerenPerDag = "BasiszorgOverigKerenPerDag";

        // 'Per week' (4: Number)
        public const int ID_BasiszorgOverigKerenPerWeek = 29;
        public const string Name_BasiszorgOverigKerenPerWeek = "BasiszorgOverigKerenPerWeek";

        // 'Toelichting' (1: String)
        public const int ID_BasiszorgOverigToelichting = 30;
        public const string Name_BasiszorgOverigToelichting = "BasiszorgOverigToelichting";

        // 'Persoonlijke verzorging' (6: Checkbox)
        public const int ID_BasiszorgPersoonlijkeVerzorging = 31;
        public const string Name_BasiszorgPersoonlijkeVerzorging = "BasiszorgPersoonlijkeVerzorging";

        // 'Per dag' (4: Number)
        public const int ID_BasiszorgPersoonlijkeVerzorgingKerenPerDag = 32;
        public const string Name_BasiszorgPersoonlijkeVerzorgingKerenPerDag = "BasiszorgPersoonlijkeVerzorgingKerenPerDag";

        // 'Per week' (4: Number)
        public const int ID_BasiszorgPersoonlijkeVerzorgingKerenPerWeek = 33;
        public const string Name_BasiszorgPersoonlijkeVerzorgingKerenPerWeek = "BasiszorgPersoonlijkeVerzorgingKerenPerWeek";

        // 'Toelichting' (1: String)
        public const int ID_BasiszorgPersoonlijkeVerzorgingToelichting = 34;
        public const string Name_BasiszorgPersoonlijkeVerzorgingToelichting = "BasiszorgPersoonlijkeVerzorgingToelichting";

        // 'Toiletgang' (6: Checkbox)
        public const int ID_BasiszorgToiletgang = 35;
        public const string Name_BasiszorgToiletgang = "BasiszorgToiletgang";

        // 'Per dag' (4: Number)
        public const int ID_BasiszorgToiletgangKerenPerDag = 36;
        public const string Name_BasiszorgToiletgangKerenPerDag = "BasiszorgToiletgangKerenPerDag";

        // 'Per week' (4: Number)
        public const int ID_BasiszorgToiletgangKerenPerWeek = 37;
        public const string Name_BasiszorgToiletgangKerenPerWeek = "BasiszorgToiletgangKerenPerWeek";

        // 'Toelichting' (1: String)
        public const int ID_BasiszorgToiletgangToelichting = 38;
        public const string Name_BasiszorgToiletgangToelichting = "BasiszorgToiletgangToelichting";

        // 'Transfer bed/stoel' (6: Checkbox)
        public const int ID_BasiszorgTransferBedStoel = 39;
        public const string Name_BasiszorgTransferBedStoel = "BasiszorgTransferBedStoel";

        // 'Per dag' (4: Number)
        public const int ID_BasiszorgTransferBedStoelKerenPerDag = 40;
        public const string Name_BasiszorgTransferBedStoelKerenPerDag = "BasiszorgTransferBedStoelKerenPerDag";

        // 'Per week' (4: Number)
        public const int ID_BasiszorgTransferBedStoelKerenPerWeek = 41;
        public const string Name_BasiszorgTransferBedStoelKerenPerWeek = "BasiszorgTransferBedStoelKerenPerWeek";

        // 'Toelichting' (1: String)
        public const int ID_BasiszorgTransferBedStoelToelichting = 42;
        public const string Name_BasiszorgTransferBedStoelToelichting = "BasiszorgTransferBedStoelToelichting";

        // 'AGB-code' (1: String)
        public const int ID_BehandelaarAGB = 43;
        public const string Name_BehandelaarAGB = "BehandelaarAGB";

        // 'BIG-code' (1: String)
        public const int ID_BehandelaarBIG = 44;
        public const string Name_BehandelaarBIG = "BehandelaarBIG";

        // 'Behandelend arts' (1: String)
        public const int ID_BehandelaarNaam = 45;
        public const string Name_BehandelaarNaam = "BehandelaarNaam";

        // 'Opmerkingen' (1: String)
        public const int ID_BehandelaarOpmerkingen = 46;
        public const string Name_BehandelaarOpmerkingen = "BehandelaarOpmerkingen";

        // 'Specialisme' (7: DropDownList)
        public const int ID_BehandelaarSpecialisme = 47;
        public const string Name_BehandelaarSpecialisme = "BehandelaarSpecialisme";

        // 'Telefoon/pieper' (1: String)
        public const int ID_BehandelaarTelefoonPieper = 48;
        public const string Name_BehandelaarTelefoonPieper = "BehandelaarTelefoonPieper";

        // 'Catheter verzorging' (6: Checkbox)
        public const int ID_CatheterVerzorging = 49;
        public const string Name_CatheterVerzorging = "CatheterVerzorging";

        // 'Blaas catheter' (6: Checkbox)
        public const int ID_CatheterVerzorgingBlaascatheter = 50;
        public const string Name_CatheterVerzorgingBlaascatheter = "CatheterVerzorgingBlaascatheter";

        // 'Per dag' (4: Number)
        public const int ID_CatheterVerzorgingBlaascatheterKerenPerDag = 51;
        public const string Name_CatheterVerzorgingBlaascatheterKerenPerDag = "CatheterVerzorgingBlaascatheterKerenPerDag";

        // 'Per week' (4: Number)
        public const int ID_CatheterVerzorgingBlaascatheterKerenPerWeek = 52;
        public const string Name_CatheterVerzorgingBlaascatheterKerenPerWeek = "CatheterVerzorgingBlaascatheterKerenPerWeek";

        // 'Toelichting' (1: String)
        public const int ID_CatheterVerzorgingBlaascatheterToelichting = 53;
        public const string Name_CatheterVerzorgingBlaascatheterToelichting = "CatheterVerzorgingBlaascatheterToelichting";

        // 'Suprapubis catheter' (6: Checkbox)
        public const int ID_CatheterVerzorgingSuprapubisCatheter = 54;
        public const string Name_CatheterVerzorgingSuprapubisCatheter = "CatheterVerzorgingSuprapubisCatheter";

        // 'Per dag' (4: Number)
        public const int ID_CatheterVerzorgingSuprapubisCatheterKerenPerDag = 55;
        public const string Name_CatheterVerzorgingSuprapubisCatheterKerenPerDag = "CatheterVerzorgingSuprapubisCatheterKerenPerDag";

        // 'Per week' (4: Number)
        public const int ID_CatheterVerzorgingSuprapubisCatheterKerenPerWeek = 56;
        public const string Name_CatheterVerzorgingSuprapubisCatheterKerenPerWeek = "CatheterVerzorgingSuprapubisCatheterKerenPerWeek";

        // 'Toelichting' (1: String)
        public const int ID_CatheterVerzorgingSuprapubisCatheterToelichting = 57;
        public const string Name_CatheterVerzorgingSuprapubisCatheterToelichting = "CatheterVerzorgingSuprapubisCatheterToelichting";

        // 'Continueren' (6: Checkbox)
        public const int ID_ContinueringVanBestaandeZorg = 58;
        public const string Name_ContinueringVanBestaandeZorg = "ContinueringVanBestaandeZorg";

        // 'Continuiteitsbezoek' (1: String)
        public const int ID_ContinuiteitsbezoekToelichting = 59;
        public const string Name_ContinuiteitsbezoekToelichting = "ContinuiteitsbezoekToelichting";

        // 'Controle lichaamsfuncties' (6: Checkbox)
        public const int ID_ControleLichaamsfuncties = 60;
        public const string Name_ControleLichaamsfuncties = "ControleLichaamsfuncties";

        // 'Anders' (6: Checkbox)
        public const int ID_ControleLichaamsfunctiesAnders = 61;
        public const string Name_ControleLichaamsfunctiesAnders = "ControleLichaamsfunctiesAnders";

        // 'Per dag' (4: Number)
        public const int ID_ControleLichaamsfunctiesAndersKerenPerDag = 62;
        public const string Name_ControleLichaamsfunctiesAndersKerenPerDag = "ControleLichaamsfunctiesAndersKerenPerDag";

        // 'Per week' (4: Number)
        public const int ID_ControleLichaamsfunctiesAndersKerenPerWeek = 63;
        public const string Name_ControleLichaamsfunctiesAndersKerenPerWeek = "ControleLichaamsfunctiesAndersKerenPerWeek";

        // 'Toelichting' (1: String)
        public const int ID_ControleLichaamsfunctiesAndersToelichting = 64;
        public const string Name_ControleLichaamsfunctiesAndersToelichting = "ControleLichaamsfunctiesAndersToelichting";

        // 'Bloedsuiker controle' (6: Checkbox)
        public const int ID_ControleLichaamsfunctiesBloedsuikerControle = 65;
        public const string Name_ControleLichaamsfunctiesBloedsuikerControle = "ControleLichaamsfunctiesBloedsuikerControle";

        // 'Per dag' (4: Number)
        public const int ID_ControleLichaamsfunctiesBloedsuikerControleKerenPerDag = 66;
        public const string Name_ControleLichaamsfunctiesBloedsuikerControleKerenPerDag = "ControleLichaamsfunctiesBloedsuikerControleKerenPerDag";

        // 'Per week' (4: Number)
        public const int ID_ControleLichaamsfunctiesBloedsuikerControleKerenPerWeek = 67;
        public const string Name_ControleLichaamsfunctiesBloedsuikerControleKerenPerWeek = "ControleLichaamsfunctiesBloedsuikerControleKerenPerWeek";

        // 'Toelichting' (1: String)
        public const int ID_ControleLichaamsfunctiesBloedsuikerControleToelichting = 68;
        public const string Name_ControleLichaamsfunctiesBloedsuikerControleToelichting = "ControleLichaamsfunctiesBloedsuikerControleToelichting";

        // 'Zuurstoftoediening' (6: Checkbox)
        public const int ID_ControleLichaamsfunctiesZuurstoftoediening = 69;
        public const string Name_ControleLichaamsfunctiesZuurstoftoediening = "ControleLichaamsfunctiesZuurstoftoediening";

        // 'Per dag' (4: Number)
        public const int ID_ControleLichaamsfunctiesZuurstoftoedieningKerenPerDag = 70;
        public const string Name_ControleLichaamsfunctiesZuurstoftoedieningKerenPerDag = "ControleLichaamsfunctiesZuurstoftoedieningKerenPerDag";

        // 'Per week' (4: Number)
        public const int ID_ControleLichaamsfunctiesZuurstoftoedieningKerenPerWeek = 71;
        public const string Name_ControleLichaamsfunctiesZuurstoftoedieningKerenPerWeek = "ControleLichaamsfunctiesZuurstoftoedieningKerenPerWeek";

        // 'Toelichting' (1: String)
        public const int ID_ControleLichaamsfunctiesZuurstoftoedieningToelichting = 72;
        public const string Name_ControleLichaamsfunctiesZuurstoftoedieningToelichting = "ControleLichaamsfunctiesZuurstoftoedieningToelichting";

        // 'Gewenste ontslagdatum' (9: Date)
        public const int ID_DatumEindeBehandelingMedischSpecialist = 73;
        public const string Name_DatumEindeBehandelingMedischSpecialist = "DatumEindeBehandelingMedischSpecialist";

        // 'Toelichting' (1: String)
        public const int ID_DatumEindeBehandelingMedischSpecialistToelichting = 74;
        public const string Name_DatumEindeBehandelingMedischSpecialistToelichting = "DatumEindeBehandelingMedischSpecialistToelichting";

        // 'Welke zorg is noodzakelijk als de patient uit het ziekenhuis is ontslagen wordt?' (6: Checkbox)
        public const int ID_DeMogelijkheidVanHulpmiddelen = 75;
        public const string Name_DeMogelijkheidVanHulpmiddelen = "DeMogelijkheidVanHulpmiddelen";

        // 'Drainverzorging' (6: Checkbox)
        public const int ID_Drainverzorging = 76;
        public const string Name_Drainverzorging = "Drainverzorging";

        // 'Per dag' (4: Number)
        public const int ID_DrainverzorgingKerenPerDag = 77;
        public const string Name_DrainverzorgingKerenPerDag = "DrainverzorgingKerenPerDag";

        // 'Per week' (4: Number)
        public const int ID_DrainverzorgingKerenPerWeek = 78;
        public const string Name_DrainverzorgingKerenPerWeek = "DrainverzorgingKerenPerWeek";

        // 'Toelichting' (1: String)
        public const int ID_DrainverzorgingToelichting = 79;
        public const string Name_DrainverzorgingToelichting = "DrainverzorgingToelichting";

        // 'Naar huis' (7: DropDownList)
        public const int ID_ExtramuraalTyperingNazorg = 80;
        public const string Name_ExtramuraalTyperingNazorg = "ExtramuraalTyperingNazorg";

        // 'Hospice' (7: DropDownList)
        public const int ID_HospiceTyperingNazorg = 81;
        public const string Name_HospiceTyperingNazorg = "HospiceTyperingNazorg";

        // 'MRSA of anders' (1: String)
        public const int ID_InfectieMRSA = 82;
        public const string Name_InfectieMRSA = "InfectieMRSA";

        // 'Noodzakelijke barrière-maatregelen' (1: String)
        public const int ID_InfectieNoodzakelijke = 83;
        public const string Name_InfectieNoodzakelijke = "InfectieNoodzakelijke";

        // 'Infusietherapie' (6: Checkbox)
        public const int ID_Infusietherapie = 84;
        public const string Name_Infusietherapie = "Infusietherapie";

        // 'Centraal Veneuze Lijn' (6: Checkbox)
        public const int ID_InfusietherapieCentraalVeneuzeLijn = 85;
        public const string Name_InfusietherapieCentraalVeneuzeLijn = "InfusietherapieCentraalVeneuzeLijn";

        // 'Per dag' (4: Number)
        public const int ID_InfusietherapieCentraalVeneuzeLijnKerenPerDag = 86;
        public const string Name_InfusietherapieCentraalVeneuzeLijnKerenPerDag = "InfusietherapieCentraalVeneuzeLijnKerenPerDag";

        // 'Per week' (4: Number)
        public const int ID_InfusietherapieCentraalVeneuzeLijnKerenPerWeek = 87;
        public const string Name_InfusietherapieCentraalVeneuzeLijnKerenPerWeek = "InfusietherapieCentraalVeneuzeLijnKerenPerWeek";

        // 'Toelichting' (1: String)
        public const int ID_InfusietherapieCentraalVeneuzeLijnToelichting = 88;
        public const string Name_InfusietherapieCentraalVeneuzeLijnToelichting = "InfusietherapieCentraalVeneuzeLijnToelichting";

        // 'Parenterale toediening' (6: Checkbox)
        public const int ID_InfusietherapieParenteraleToedieningMedicatie = 89;
        public const string Name_InfusietherapieParenteraleToedieningMedicatie = "InfusietherapieParenteraleToedieningMedicatie";

        // 'Per dag' (4: Number)
        public const int ID_InfusietherapieParenteraleToedieningMedicatieKerenPerDag = 90;
        public const string Name_InfusietherapieParenteraleToedieningMedicatieKerenPerDag = "InfusietherapieParenteraleToedieningMedicatieKerenPerDag";

        // 'Per week' (4: Number)
        public const int ID_InfusietherapieParenteraleToedieningMedicatieKerenPerWeek = 91;
        public const string Name_InfusietherapieParenteraleToedieningMedicatieKerenPerWeek = "InfusietherapieParenteraleToedieningMedicatieKerenPerWeek";

        // 'Toelichting' (1: String)
        public const int ID_InfusietherapieParenteraleToedieningMedicatieToelichting = 92;
        public const string Name_InfusietherapieParenteraleToedieningMedicatieToelichting = "InfusietherapieParenteraleToedieningMedicatieToelichting";

        // 'Parenterale voeding' (6: Checkbox)
        public const int ID_InfusietherapieParenteraleVoeding = 93;
        public const string Name_InfusietherapieParenteraleVoeding = "InfusietherapieParenteraleVoeding";

        // 'Per dag' (4: Number)
        public const int ID_InfusietherapieParenteraleVoedingKerenPerDag = 94;
        public const string Name_InfusietherapieParenteraleVoedingKerenPerDag = "InfusietherapieParenteraleVoedingKerenPerDag";

        // 'Per week' (4: Number)
        public const int ID_InfusietherapieParenteraleVoedingKerenPerWeek = 95;
        public const string Name_InfusietherapieParenteraleVoedingKerenPerWeek = "InfusietherapieParenteraleVoedingKerenPerWeek";

        // 'Toelichting' (1: String)
        public const int ID_InfusietherapieParenteraleVoedingToelichting = 96;
        public const string Name_InfusietherapieParenteraleVoedingToelichting = "InfusietherapieParenteraleVoedingToelichting";

        // 'PICC' (6: Checkbox)
        public const int ID_InfusietherapiePICC = 97;
        public const string Name_InfusietherapiePICC = "InfusietherapiePICC";

        // 'Per dag' (4: Number)
        public const int ID_InfusietherapiePICCKerenPerDag = 98;
        public const string Name_InfusietherapiePICCKerenPerDag = "InfusietherapiePICCKerenPerDag";

        // 'Per week' (4: Number)
        public const int ID_InfusietherapiePICCKerenPerWeek = 99;
        public const string Name_InfusietherapiePICCKerenPerWeek = "InfusietherapiePICCKerenPerWeek";

        // 'Toelichting' (1: String)
        public const int ID_InfusietherapiePICCToelichting = 100;
        public const string Name_InfusietherapiePICCToelichting = "InfusietherapiePICCToelichting";

        // 'Anders' (6: Checkbox)
        public const int ID_IngeschakeldeOndersteunendeDienstenAnders = 101;
        public const string Name_IngeschakeldeOndersteunendeDienstenAnders = "IngeschakeldeOndersteunendeDienstenAnders";

        // 'Diëtist' (6: Checkbox)
        public const int ID_IngeschakeldeOndersteunendeDienstenDietist = 102;
        public const string Name_IngeschakeldeOndersteunendeDienstenDietist = "IngeschakeldeOndersteunendeDienstenDietist";

        // 'Ergotherapeut' (6: Checkbox)
        public const int ID_IngeschakeldeOndersteunendeDienstenErgotherapeut = 103;
        public const string Name_IngeschakeldeOndersteunendeDienstenErgotherapeut = "IngeschakeldeOndersteunendeDienstenErgotherapeut";

        // 'Fysiotherapeut' (6: Checkbox)
        public const int ID_IngeschakeldeOndersteunendeDienstenFysiotherapeut = 104;
        public const string Name_IngeschakeldeOndersteunendeDienstenFysiotherapeut = "IngeschakeldeOndersteunendeDienstenFysiotherapeut";

        // 'Geriater' (6: Checkbox)
        public const int ID_IngeschakeldeOndersteunendeDienstenGeriater = 105;
        public const string Name_IngeschakeldeOndersteunendeDienstenGeriater = "IngeschakeldeOndersteunendeDienstenGeriater";

        // 'Gespecialiseerd verpleegkundige' (6: Checkbox)
        public const int ID_IngeschakeldeOndersteunendeDienstenGespecialiseerdVerpleegkundige = 106;
        public const string Name_IngeschakeldeOndersteunendeDienstenGespecialiseerdVerpleegkundige = "IngeschakeldeOndersteunendeDienstenGespecialiseerdVerpleegkundige";

        // 'Logopedist' (6: Checkbox)
        public const int ID_IngeschakeldeOndersteunendeDienstenLogopedist = 107;
        public const string Name_IngeschakeldeOndersteunendeDienstenLogopedist = "IngeschakeldeOndersteunendeDienstenLogopedist";

        // 'Medisch maatschappelijk werk' (6: Checkbox)
        public const int ID_IngeschakeldeOndersteunendeDienstenMedischMaatschappelijkWerk = 108;
        public const string Name_IngeschakeldeOndersteunendeDienstenMedischMaatschappelijkWerk = "IngeschakeldeOndersteunendeDienstenMedischMaatschappelijkWerk";

        // 'Pedagogisch medewerker' (6: Checkbox)
        public const int ID_IngeschakeldeOndersteunendeDienstenPedagogischMedewerker = 109;
        public const string Name_IngeschakeldeOndersteunendeDienstenPedagogischMedewerker = "IngeschakeldeOndersteunendeDienstenPedagogischMedewerker";

        // 'Psychiater' (6: Checkbox)
        public const int ID_IngeschakeldeOndersteunendeDienstenPsychiater = 110;
        public const string Name_IngeschakeldeOndersteunendeDienstenPsychiater = "IngeschakeldeOndersteunendeDienstenPsychiater";

        // 'Revalidatiearts' (6: Checkbox)
        public const int ID_IngeschakeldeOndersteunendeDienstenRevalidatiearts = 111;
        public const string Name_IngeschakeldeOndersteunendeDienstenRevalidatiearts = "IngeschakeldeOndersteunendeDienstenRevalidatiearts";

        // 'Toelichting' (1: String)
        public const int ID_IngeschakeldeOndersteunendeDienstenToelichting = 112;
        public const string Name_IngeschakeldeOndersteunendeDienstenToelichting = "IngeschakeldeOndersteunendeDienstenToelichting";

        // 'Met zorg naar huis' (6: Checkbox)
        public const int ID_InschattingZorgverleningNaOntslagMetZorgNaarHuis = 113;
        public const string Name_InschattingZorgverleningNaOntslagMetZorgNaarHuis = "InschattingZorgverleningNaOntslagMetZorgNaarHuis";

        // 'Nieuwe aanvraag' (5: RadioButton)
        public const int ID_InschattingZorgverleningNaOntslagNieuweAanvraag = 114;
        public const string Name_InschattingZorgverleningNaOntslagNieuweAanvraag = "InschattingZorgverleningNaOntslagNieuweAanvraag";

        // 'Zorg in zorginstelling' (6: Checkbox)
        public const int ID_InschattingZorgverleningNaOntslagZorgInZorginstelling = 115;
        public const string Name_InschattingZorgverleningNaOntslagZorgInZorginstelling = "InschattingZorgverleningNaOntslagZorgInZorginstelling";

        // 'Zorginstelling voor' (7: DropDownList)
        public const int ID_IntramuraalTyperingNazorg = 116;
        public const string Name_IntramuraalTyperingNazorg = "IntramuraalTyperingNazorg";

        // 'Eénpersoons kamer med. noodzakelijk' (6: Checkbox)
        public const int ID_KamerNoodzakelijk = 117;
        public const string Name_KamerNoodzakelijk = "KamerNoodzakelijk";

        // 'Kamernummer' (1: String)
        public const int ID_Kamernummer = 118;
        public const string Name_Kamernummer = "Kamernummer";

        // 'Medicatie toediening' (6: Checkbox)
        public const int ID_MedicatieToediening = 119;
        public const string Name_MedicatieToediening = "MedicatieToediening";

        // 'Anders' (6: Checkbox)
        public const int ID_MedicatieToedieningAnders = 120;
        public const string Name_MedicatieToedieningAnders = "MedicatieToedieningAnders";

        // 'Per dag' (4: Number)
        public const int ID_MedicatieToedieningAndersKerenPerDag = 121;
        public const string Name_MedicatieToedieningAndersKerenPerDag = "MedicatieToedieningAndersKerenPerDag";

        // 'Per week' (4: Number)
        public const int ID_MedicatieToedieningAndersKerenPerWeek = 122;
        public const string Name_MedicatieToedieningAndersKerenPerWeek = "MedicatieToedieningAndersKerenPerWeek";

        // 'Toelichting' (1: String)
        public const int ID_MedicatieToedieningAndersToelichting = 123;
        public const string Name_MedicatieToedieningAndersToelichting = "MedicatieToedieningAndersToelichting";

        // 'Druppelen' (6: Checkbox)
        public const int ID_MedicatieToedieningDruppelen = 124;
        public const string Name_MedicatieToedieningDruppelen = "MedicatieToedieningDruppelen";

        // 'Per dag' (4: Number)
        public const int ID_MedicatieToedieningDruppelenKerenPerDag = 125;
        public const string Name_MedicatieToedieningDruppelenKerenPerDag = "MedicatieToedieningDruppelenKerenPerDag";

        // 'Per week' (4: Number)
        public const int ID_MedicatieToedieningDruppelenKerenPerWeek = 126;
        public const string Name_MedicatieToedieningDruppelenKerenPerWeek = "MedicatieToedieningDruppelenKerenPerWeek";

        // 'Toelichting' (1: String)
        public const int ID_MedicatieToedieningDruppelenToelichting = 127;
        public const string Name_MedicatieToedieningDruppelenToelichting = "MedicatieToedieningDruppelenToelichting";

        // 'Inhalatie/verneveling' (6: Checkbox)
        public const int ID_MedicatieToedieningInhalatieVerneveling = 128;
        public const string Name_MedicatieToedieningInhalatieVerneveling = "MedicatieToedieningInhalatieVerneveling";

        // 'Per dag' (4: Number)
        public const int ID_MedicatieToedieningInhalatieVernevelingKerenPerDag = 129;
        public const string Name_MedicatieToedieningInhalatieVernevelingKerenPerDag = "MedicatieToedieningInhalatieVernevelingKerenPerDag";

        // 'Per week' (4: Number)
        public const int ID_MedicatieToedieningInhalatieVernevelingKerenPerWeek = 130;
        public const string Name_MedicatieToedieningInhalatieVernevelingKerenPerWeek = "MedicatieToedieningInhalatieVernevelingKerenPerWeek";

        // 'Toelichting' (1: String)
        public const int ID_MedicatieToedieningInhalatieVernevelingToelichting = 131;
        public const string Name_MedicatieToedieningInhalatieVernevelingToelichting = "MedicatieToedieningInhalatieVernevelingToelichting";

        // 'Per injectie' (6: Checkbox)
        public const int ID_MedicatieToedieningPerInjectie = 132;
        public const string Name_MedicatieToedieningPerInjectie = "MedicatieToedieningPerInjectie";

        // 'Per dag' (4: Number)
        public const int ID_MedicatieToedieningPerInjectieKerenPerDag = 133;
        public const string Name_MedicatieToedieningPerInjectieKerenPerDag = "MedicatieToedieningPerInjectieKerenPerDag";

        // 'Per week' (4: Number)
        public const int ID_MedicatieToedieningPerInjectieKerenPerWeek = 134;
        public const string Name_MedicatieToedieningPerInjectieKerenPerWeek = "MedicatieToedieningPerInjectieKerenPerWeek";

        // 'Toelichting' (1: String)
        public const int ID_MedicatieToedieningPerInjectieToelichting = 135;
        public const string Name_MedicatieToedieningPerInjectieToelichting = "MedicatieToedieningPerInjectieToelichting";

        // 'Per os' (6: Checkbox)
        public const int ID_MedicatieToedieningPerOs = 136;
        public const string Name_MedicatieToedieningPerOs = "MedicatieToedieningPerOs";

        // 'Per dag' (4: Number)
        public const int ID_MedicatieToedieningPerOsKerenPerDag = 137;
        public const string Name_MedicatieToedieningPerOsKerenPerDag = "MedicatieToedieningPerOsKerenPerDag";

        // 'Per week' (4: Number)
        public const int ID_MedicatieToedieningPerOsKerenPerWeek = 138;
        public const string Name_MedicatieToedieningPerOsKerenPerWeek = "MedicatieToedieningPerOsKerenPerWeek";

        // 'Toelichting' (1: String)
        public const int ID_MedicatieToedieningPerOsToelichting = 139;
        public const string Name_MedicatieToedieningPerOsToelichting = "MedicatieToedieningPerOsToelichting";

        // 'Zalven' (6: Checkbox)
        public const int ID_MedicatieToedieningZalven = 140;
        public const string Name_MedicatieToedieningZalven = "MedicatieToedieningZalven";

        // 'Per dag' (4: Number)
        public const int ID_MedicatieToedieningZalvenKerenPerDag = 141;
        public const string Name_MedicatieToedieningZalvenKerenPerDag = "MedicatieToedieningZalvenKerenPerDag";

        // 'Per week' (4: Number)
        public const int ID_MedicatieToedieningZalvenKerenPerWeek = 142;
        public const string Name_MedicatieToedieningZalvenKerenPerWeek = "MedicatieToedieningZalvenKerenPerWeek";

        // 'Toelichting' (1: String)
        public const int ID_MedicatieToedieningZalvenToelichting = 143;
        public const string Name_MedicatieToedieningZalvenToelichting = "MedicatieToedieningZalvenToelichting";

        // 'Medicatie; toezicht op inname' (6: Checkbox)
        public const int ID_MedicatieToezichtOpInname = 144;
        public const string Name_MedicatieToezichtOpInname = "MedicatieToezichtOpInname";

        // 'Per dag' (4: Number)
        public const int ID_MedicatieToezichtOpInnameKerenPerDag = 145;
        public const string Name_MedicatieToezichtOpInnameKerenPerDag = "MedicatieToezichtOpInnameKerenPerDag";

        // 'Per week' (4: Number)
        public const int ID_MedicatieToezichtOpInnameKerenPerWeek = 146;
        public const string Name_MedicatieToezichtOpInnameKerenPerWeek = "MedicatieToezichtOpInnameKerenPerWeek";

        // 'Toelichting' (1: String)
        public const int ID_MedicatieToezichtOpInnameToelichting = 147;
        public const string Name_MedicatieToezichtOpInnameToelichting = "MedicatieToezichtOpInnameToelichting";

        // 'Behandeling' (1: String)
        public const int ID_MedischeSituatieBehandeling = 148;
        public const string Name_MedischeSituatieBehandeling = "MedischeSituatieBehandeling";

        // 'Datum opname' (9: Date)
        public const int ID_MedischeSituatieDatumOpname = 149;
        public const string Name_MedischeSituatieDatumOpname = "MedischeSituatieDatumOpname";

        // 'Diagnose/Reden opname/aanvraag' (1: String)
        public const int ID_MedischeSituatieRedenOpname = 150;
        public const string Name_MedischeSituatieRedenOpname = "MedischeSituatieRedenOpname";

        // 'Voorgeschiedenis' (1: String)
        public const int ID_MedischeSituatieVoorgeschiedenis = 151;
        public const string Name_MedischeSituatieVoorgeschiedenis = "MedischeSituatieVoorgeschiedenis";

        // 'Aanpassing/uitbreiding bestaande zorg' (6: Checkbox)
        public const int ID_MetZorgNaarHuisAanpassingVanBestaandeZorg = 152;
        public const string Name_MetZorgNaarHuisAanpassingVanBestaandeZorg = "MetZorgNaarHuisAanpassingVanBestaandeZorg";

        // 'Continuering van bestaande zorg' (6: Checkbox)
        public const int ID_MetZorgNaarHuisContinueringVanBestaandeZorg = 153;
        public const string Name_MetZorgNaarHuisContinueringVanBestaandeZorg = "MetZorgNaarHuisContinueringVanBestaandeZorg";

        // 'Alarmering' (6: Checkbox)
        public const int ID_MetZorgNaarHuisNieuweAanvraagAlarmering = 154;
        public const string Name_MetZorgNaarHuisNieuweAanvraagAlarmering = "MetZorgNaarHuisNieuweAanvraagAlarmering";

        // 'Anders nl' (6: Checkbox)
        public const int ID_MetZorgNaarHuisNieuweAanvraagAndersNl = 155;
        public const string Name_MetZorgNaarHuisNieuweAanvraagAndersNl = "MetZorgNaarHuisNieuweAanvraagAndersNl";

        // 'Anders nl' (1: String)
        public const int ID_MetZorgNaarHuisNieuweAanvraagAndersNlText = 156;
        public const string Name_MetZorgNaarHuisNieuweAanvraagAndersNlText = "MetZorgNaarHuisNieuweAanvraagAndersNlText";

        // 'Dagopvang/dagbehandeling' (6: Checkbox)
        public const int ID_MetZorgNaarHuisNieuweAanvraagDagopvangDagbehandeling = 157;
        public const string Name_MetZorgNaarHuisNieuweAanvraagDagopvangDagbehandeling = "MetZorgNaarHuisNieuweAanvraagDagopvangDagbehandeling";

        // 'Hulp bij huishouding' (6: Checkbox)
        public const int ID_MetZorgNaarHuisNieuweAanvraagHulpBijHuishouding = 158;
        public const string Name_MetZorgNaarHuisNieuweAanvraagHulpBijHuishouding = "MetZorgNaarHuisNieuweAanvraagHulpBijHuishouding";

        // 'Maaltijdvoorziening' (6: Checkbox)
        public const int ID_MetZorgNaarHuisNieuweAanvraagMaaltijdvoorziening = 159;
        public const string Name_MetZorgNaarHuisNieuweAanvraagMaaltijdvoorziening = "MetZorgNaarHuisNieuweAanvraagMaaltijdvoorziening";

        // 'Palliatieve Terminale Zorg Thuis' (6: Checkbox)
        public const int ID_MetZorgNaarHuisNieuweAanvraagPalliatieveTerminaleZorgThuis = 160;
        public const string Name_MetZorgNaarHuisNieuweAanvraagPalliatieveTerminaleZorgThuis = "MetZorgNaarHuisNieuweAanvraagPalliatieveTerminaleZorgThuis";

        // 'Persoonlijke verzorging' (6: Checkbox)
        public const int ID_MetZorgNaarHuisNieuweAanvraagPersoonlijkeVerzorging = 161;
        public const string Name_MetZorgNaarHuisNieuweAanvraagPersoonlijkeVerzorging = "MetZorgNaarHuisNieuweAanvraagPersoonlijkeVerzorging";

        // 'Verpleging' (6: Checkbox)
        public const int ID_MetZorgNaarHuisNieuweAanvraagVerpleging = 162;
        public const string Name_MetZorgNaarHuisNieuweAanvraagVerpleging = "MetZorgNaarHuisNieuweAanvraagVerpleging";

        // 'Nierfunctievervangende therapie' (6: Checkbox)
        public const int ID_NierfunctievervangendeTherapie = 163;
        public const string Name_NierfunctievervangendeTherapie = "NierfunctievervangendeTherapie";

        // 'Per dag' (1: String)
        public const int ID_NierfunctievervangendeTherapieKerenPerDag = 164;
        public const string Name_NierfunctievervangendeTherapieKerenPerDag = "NierfunctievervangendeTherapieKerenPerDag";

        // 'Per week' (1: String)
        public const int ID_NierfunctievervangendeTherapieKerenPerWeek = 165;
        public const string Name_NierfunctievervangendeTherapieKerenPerWeek = "NierfunctievervangendeTherapieKerenPerWeek";

        // 'Toelichting' (1: String)
        public const int ID_NierfunctievervangendeTherapieToelichting = 166;
        public const string Name_NierfunctievervangendeTherapieToelichting = "NierfunctievervangendeTherapieToelichting";

        // 'Regelen hulpmiddelen noodzakelijk' (6: Checkbox)
        public const int ID_NoodzakelijkTeRegelenHulpmiddelen = 167;
        public const string Name_NoodzakelijkTeRegelenHulpmiddelen = "NoodzakelijkTeRegelenHulpmiddelen";

        // 'Ander adres, nl.' (6: Checkbox)
        public const int ID_NoodzakelijkTeRegelenHulpmiddelenAfleveringAnderAdres = 168;
        public const string Name_NoodzakelijkTeRegelenHulpmiddelenAfleveringAnderAdres = "NoodzakelijkTeRegelenHulpmiddelenAfleveringAnderAdres";

        // 'Ander adres' (1: String)
        public const int ID_NoodzakelijkTeRegelenHulpmiddelenAfleveringAnderAdresText = 169;
        public const string Name_NoodzakelijkTeRegelenHulpmiddelenAfleveringAnderAdresText = "NoodzakelijkTeRegelenHulpmiddelenAfleveringAnderAdresText";

        // 'Thuis' (6: Checkbox)
        public const int ID_NoodzakelijkTeRegelenHulpmiddelenAfleveringThuis = 170;
        public const string Name_NoodzakelijkTeRegelenHulpmiddelenAfleveringThuis = "NoodzakelijkTeRegelenHulpmiddelenAfleveringThuis";

        // 'Ziekenhuis' (6: Checkbox)
        public const int ID_NoodzakelijkTeRegelenHulpmiddelenAfleveringZiekenhuis = 171;
        public const string Name_NoodzakelijkTeRegelenHulpmiddelenAfleveringZiekenhuis = "NoodzakelijkTeRegelenHulpmiddelenAfleveringZiekenhuis";

        // 'Anders' (6: Checkbox)
        public const int ID_NoodzakelijkTeRegelenHulpmiddelenAnders = 172;
        public const string Name_NoodzakelijkTeRegelenHulpmiddelenAnders = "NoodzakelijkTeRegelenHulpmiddelenAnders";

        // 'Anders' (1: String)
        public const int ID_NoodzakelijkTeRegelenHulpmiddelenAndersText = 173;
        public const string Name_NoodzakelijkTeRegelenHulpmiddelenAndersText = "NoodzakelijkTeRegelenHulpmiddelenAndersText";

        // 'Anti decubitus matras' (6: Checkbox)
        public const int ID_NoodzakelijkTeRegelenHulpmiddelenAntiDecubitusMatras = 174;
        public const string Name_NoodzakelijkTeRegelenHulpmiddelenAntiDecubitusMatras = "NoodzakelijkTeRegelenHulpmiddelenAntiDecubitusMatras";

        // 'Draaischijf' (6: Checkbox)
        public const int ID_NoodzakelijkTeRegelenHulpmiddelenDraaischijf = 175;
        public const string Name_NoodzakelijkTeRegelenHulpmiddelenDraaischijf = "NoodzakelijkTeRegelenHulpmiddelenDraaischijf";

        // 'Hoog/laagbed' (6: Checkbox)
        public const int ID_NoodzakelijkTeRegelenHulpmiddelenHoogLaagbed = 176;
        public const string Name_NoodzakelijkTeRegelenHulpmiddelenHoogLaagbed = "NoodzakelijkTeRegelenHulpmiddelenHoogLaagbed";

        // 'Krukken/stok' (6: Checkbox)
        public const int ID_NoodzakelijkTeRegelenHulpmiddelenKrukkenStok = 177;
        public const string Name_NoodzakelijkTeRegelenHulpmiddelenKrukkenStok = "NoodzakelijkTeRegelenHulpmiddelenKrukkenStok";

        // 'Postoel' (6: Checkbox)
        public const int ID_NoodzakelijkTeRegelenHulpmiddelenPostoel = 178;
        public const string Name_NoodzakelijkTeRegelenHulpmiddelenPostoel = "NoodzakelijkTeRegelenHulpmiddelenPostoel";

        // 'Rollator' (6: Checkbox)
        public const int ID_NoodzakelijkTeRegelenHulpmiddelenRollator = 179;
        public const string Name_NoodzakelijkTeRegelenHulpmiddelenRollator = "NoodzakelijkTeRegelenHulpmiddelenRollator";

        // 'Rolstoel' (6: Checkbox)
        public const int ID_NoodzakelijkTeRegelenHulpmiddelenRolstoel = 180;
        public const string Name_NoodzakelijkTeRegelenHulpmiddelenRolstoel = "NoodzakelijkTeRegelenHulpmiddelenRolstoel";

        // 'Tillift' (6: Checkbox)
        public const int ID_NoodzakelijkTeRegelenHulpmiddelenTillift = 181;
        public const string Name_NoodzakelijkTeRegelenHulpmiddelenTillift = "NoodzakelijkTeRegelenHulpmiddelenTillift";

        // 'Toelichting' (1: String)
        public const int ID_NoodzakelijkTeRegelenHulpmiddelenToelichting = 182;
        public const string Name_NoodzakelijkTeRegelenHulpmiddelenToelichting = "NoodzakelijkTeRegelenHulpmiddelenToelichting";

        // 'BMI (= resultaat kg/lengte2)' (1: String)
        public const int ID_ObesitasBMI = 183;
        public const string Name_ObesitasBMI = "ObesitasBMI";

        // 'Gewicht (kg)' (4: Number)
        public const int ID_ObesitasGewicht = 184;
        public const string Name_ObesitasGewicht = "ObesitasGewicht";

        // 'Lengte (cm)' (4: Number)
        public const int ID_ObesitasLengte = 185;
        public const string Name_ObesitasLengte = "ObesitasLengte";

        // 'Overige verrichtingen' (6: Checkbox)
        public const int ID_OverigeVerrichtingen = 186;
        public const string Name_OverigeVerrichtingen = "OverigeVerrichtingen";

        // 'Per dag' (1: String)
        public const int ID_OverigeVerrichtingenKerenPerDag = 187;
        public const string Name_OverigeVerrichtingenKerenPerDag = "OverigeVerrichtingenKerenPerDag";

        // 'Per week' (1: String)
        public const int ID_OverigeVerrichtingenKerenPerWeek = 188;
        public const string Name_OverigeVerrichtingenKerenPerWeek = "OverigeVerrichtingenKerenPerWeek";

        // 'Toelichting' (1: String)
        public const int ID_OverigeVerrichtingenToelichting = 189;
        public const string Name_OverigeVerrichtingenToelichting = "OverigeVerrichtingenToelichting";

        // 'Patient ontving zorg voor opname' (5: RadioButton)
        public const int ID_PatientOntvingZorgVoorOpnameJaNee = 190;
        public const string Name_PatientOntvingZorgVoorOpnameJaNee = "PatientOntvingZorgVoorOpnameJaNee";

        // 'Prognose verwachte ontwikkeling t.a.v de ziekte of aandoening' (7: DropDownList)
        public const int ID_PrognoseVerwachteOntwikkeling = 191;
        public const string Name_PrognoseVerwachteOntwikkeling = "PrognoseVerwachteOntwikkeling";

        // 'Denk-/waarnemingsstoornissen' (5: RadioButton)
        public const int ID_PsychischFunctionerenDenkWaarnemingsstroornissen = 192;
        public const string Name_PsychischFunctionerenDenkWaarnemingsstroornissen = "PsychischFunctionerenDenkWaarnemingsstroornissen";

        // '' (5: RadioButton)
        public const int ID_PsychischFunctionerenDenkWaarnemingsstroornissen_Geen = 193;
        public const string Name_PsychischFunctionerenDenkWaarnemingsstroornissen_Geen = "PsychischFunctionerenDenkWaarnemingsstroornissen_Geen";

        // '' (5: RadioButton)
        public const int ID_PsychischFunctionerenDenkWaarnemingsstroornissen_Matig = 194;
        public const string Name_PsychischFunctionerenDenkWaarnemingsstroornissen_Matig = "PsychischFunctionerenDenkWaarnemingsstroornissen_Matig";

        // 'Desorientatie' (5: RadioButton)
        public const int ID_PsychischFunctionerenDesorientatie = 195;
        public const string Name_PsychischFunctionerenDesorientatie = "PsychischFunctionerenDesorientatie";

        // '' (5: RadioButton)
        public const int ID_PsychischFunctionerenDesorientatie_Geen = 196;
        public const string Name_PsychischFunctionerenDesorientatie_Geen = "PsychischFunctionerenDesorientatie_Geen";

        // '' (5: RadioButton)
        public const int ID_PsychischFunctionerenDesorientatie_Matig = 197;
        public const string Name_PsychischFunctionerenDesorientatie_Matig = "PsychischFunctionerenDesorientatie_Matig";

        // 'Gedragstoornissen' (5: RadioButton)
        public const int ID_PsychischFunctionerenGedragsstoornissen = 198;
        public const string Name_PsychischFunctionerenGedragsstoornissen = "PsychischFunctionerenGedragsstoornissen";

        // '' (5: RadioButton)
        public const int ID_PsychischFunctionerenGedragsstoornissen_Geen = 199;
        public const string Name_PsychischFunctionerenGedragsstoornissen_Geen = "PsychischFunctionerenGedragsstoornissen_Geen";

        // '' (5: RadioButton)
        public const int ID_PsychischFunctionerenGedragsstoornissen_Matig = 200;
        public const string Name_PsychischFunctionerenGedragsstoornissen_Matig = "PsychischFunctionerenGedragsstoornissen_Matig";

        // 'Geheugenverlies' (5: RadioButton)
        public const int ID_PsychischFunctionerenGeheugenverlies = 201;
        public const string Name_PsychischFunctionerenGeheugenverlies = "PsychischFunctionerenGeheugenverlies";

        // '' (5: RadioButton)
        public const int ID_PsychischFunctionerenGeheugenverlies_Geen = 202;
        public const string Name_PsychischFunctionerenGeheugenverlies_Geen = "PsychischFunctionerenGeheugenverlies_Geen";

        // '' (5: RadioButton)
        public const int ID_PsychischFunctionerenGeheugenverlies_Matig = 203;
        public const string Name_PsychischFunctionerenGeheugenverlies_Matig = "PsychischFunctionerenGeheugenverlies_Matig";

        // 'Toelichting' (1: String)
        public const int ID_PsychischFunctionerenItemIngevuldToelichting = 204;
        public const string Name_PsychischFunctionerenItemIngevuldToelichting = "PsychischFunctionerenItemIngevuldToelichting";

        // 'Psychisch functioneren goed' (5: RadioButton)
        public const int ID_PsychischFunctionerenJaNee = 205;
        public const string Name_PsychischFunctionerenJaNee = "PsychischFunctionerenJaNee";

        // 'Stemmingsstoornissen' (5: RadioButton)
        public const int ID_PsychischFunctionerenStemmingsstoornissen = 206;
        public const string Name_PsychischFunctionerenStemmingsstoornissen = "PsychischFunctionerenStemmingsstoornissen";

        // '' (5: RadioButton)
        public const int ID_PsychischFunctionerenStemmingsstoornissen_Geen = 207;
        public const string Name_PsychischFunctionerenStemmingsstoornissen_Geen = "PsychischFunctionerenStemmingsstoornissen_Geen";

        // '' (5: RadioButton)
        public const int ID_PsychischFunctionerenStemmingsstoornissen_Matig = 208;
        public const string Name_PsychischFunctionerenStemmingsstoornissen_Matig = "PsychischFunctionerenStemmingsstoornissen_Matig";

        // 'Ziekte inzicht' (5: RadioButton)
        public const int ID_PsychischFunctionerenZiekteInzichtJaNee = 209;
        public const string Name_PsychischFunctionerenZiekteInzichtJaNee = "PsychischFunctionerenZiekteInzichtJaNee";

        // '' (6: Checkbox)
        public const int ID_PsychischFunctionerenZiekteInzicht_Nee = 210;
        public const string Name_PsychischFunctionerenZiekteInzicht_Nee = "PsychischFunctionerenZiekteInzicht_Nee";

        // 'Anders nl' (6: Checkbox)
        public const int ID_RelevanteAspectenVoorZorgverleningAnders = 211;
        public const string Name_RelevanteAspectenVoorZorgverleningAnders = "RelevanteAspectenVoorZorgverleningAnders";

        // 'Toelichting' (1: String)
        public const int ID_RelevanteAspectenVoorZorgverleningAndersText = 212;
        public const string Name_RelevanteAspectenVoorZorgverleningAndersText = "RelevanteAspectenVoorZorgverleningAndersText";

        // 'Dwaal/zwerfgedrag' (6: Checkbox)
        public const int ID_RelevanteAspectenVoorZorgverleningDwaalZwerfgedrag = 213;
        public const string Name_RelevanteAspectenVoorZorgverleningDwaalZwerfgedrag = "RelevanteAspectenVoorZorgverleningDwaalZwerfgedrag";

        // 'Gehoorproblemen' (6: Checkbox)
        public const int ID_RelevanteAspectenVoorZorgverleningGehoorproblemen = 214;
        public const string Name_RelevanteAspectenVoorZorgverleningGehoorproblemen = "RelevanteAspectenVoorZorgverleningGehoorproblemen";

        // 'Incontinentie' (6: Checkbox)
        public const int ID_RelevanteAspectenVoorZorgverleningIncontinentie = 215;
        public const string Name_RelevanteAspectenVoorZorgverleningIncontinentie = "RelevanteAspectenVoorZorgverleningIncontinentie";

        // 'Incontinentie Type' (5: RadioButton)
        public const int ID_RelevanteAspectenVoorZorgverleningIncontinentieKeus = 216;
        public const string Name_RelevanteAspectenVoorZorgverleningIncontinentieKeus = "RelevanteAspectenVoorZorgverleningIncontinentieKeus";

        // 'Spraakproblemen' (6: Checkbox)
        public const int ID_RelevanteAspectenVoorZorgverleningSpraakproblemen = 217;
        public const string Name_RelevanteAspectenVoorZorgverleningSpraakproblemen = "RelevanteAspectenVoorZorgverleningSpraakproblemen";

        // 'Verwaarlozing' (6: Checkbox)
        public const int ID_RelevanteAspectenVoorZorgverleningVerwaarlozing = 218;
        public const string Name_RelevanteAspectenVoorZorgverleningVerwaarlozing = "RelevanteAspectenVoorZorgverleningVerwaarlozing";

        // 'Visusproblemen' (6: Checkbox)
        public const int ID_RelevanteAspectenVoorZorgverleningVisusproblemen = 219;
        public const string Name_RelevanteAspectenVoorZorgverleningVisusproblemen = "RelevanteAspectenVoorZorgverleningVisusproblemen";

        // 'Zwak/ontbrekend mantelzorgnetwerk' (6: Checkbox)
        public const int ID_RelevanteAspectenVoorZorgverleningZwakOntbrekendMantelzorgnetwerk = 220;
        public const string Name_RelevanteAspectenVoorZorgverleningZwakOntbrekendMantelzorgnetwerk = "RelevanteAspectenVoorZorgverleningZwakOntbrekendMantelzorgnetwerk";

        // 'Toelichting' (1: String)
        public const int ID_RevalidatiedoelenToelichting = 221;
        public const string Name_RevalidatiedoelenToelichting = "RevalidatiedoelenToelichting";

        // 'Sondevoeding' (6: Checkbox)
        public const int ID_Sondevoeding = 222;
        public const string Name_Sondevoeding = "Sondevoeding";

        // 'Per dag' (1: String)
        public const int ID_SondevoedingKerenPerDag = 223;
        public const string Name_SondevoedingKerenPerDag = "SondevoedingKerenPerDag";

        // 'Per week' (1: String)
        public const int ID_SondevoedingKerenPerWeek = 224;
        public const string Name_SondevoedingKerenPerWeek = "SondevoedingKerenPerWeek";

        // 'Toelichting' (1: String)
        public const int ID_SondevoedingToelichting = 225;
        public const string Name_SondevoedingToelichting = "SondevoedingToelichting";

        // 'Stomaverzorging' (6: Checkbox)
        public const int ID_Stomaverzorging = 226;
        public const string Name_Stomaverzorging = "Stomaverzorging";

        // 'Per dag' (1: String)
        public const int ID_StomaverzorgingKerenPerDag = 227;
        public const string Name_StomaverzorgingKerenPerDag = "StomaverzorgingKerenPerDag";

        // 'Per week' (1: String)
        public const int ID_StomaverzorgingKerenPerWeek = 228;
        public const string Name_StomaverzorgingKerenPerWeek = "StomaverzorgingKerenPerWeek";

        // 'Toelichting' (1: String)
        public const int ID_StomaverzorgingToelichting = 229;
        public const string Name_StomaverzorgingToelichting = "StomaverzorgingToelichting";

        // 'Handtekening ouder niet gegeven' (6: Checkbox)
        public const int ID_ToestemmingOuderPatientNietGegeven = 230;
        public const string Name_ToestemmingOuderPatientNietGegeven = "ToestemmingOuderPatientNietGegeven";

        // 'Handtekening vastgelegd' (6: Checkbox)
        public const int ID_ToestemmingPatientHandtekeningVastgelegd = 231;
        public const string Name_ToestemmingPatientHandtekeningVastgelegd = "ToestemmingPatientHandtekeningVastgelegd";

        // 'Toestemming kon niet verkregen worden' (6: Checkbox)
        public const int ID_ToestemmingPatientKonNietVerkregenWorden = 232;
        public const string Name_ToestemmingPatientKonNietVerkregenWorden = "ToestemmingPatientKonNietVerkregenWorden";

        // 'Toestemming patient niet gegeven' (6: Checkbox)
        public const int ID_ToestemmingPatientNietGegeven = 233;
        public const string Name_ToestemmingPatientNietGegeven = "ToestemmingPatientNietGegeven";

        // 'Handtekening ouder vastgelegd' (6: Checkbox)
        public const int ID_ToestemmingPatientOuderHandtekeningGewenst = 234;
        public const string Name_ToestemmingPatientOuderHandtekeningGewenst = "ToestemmingPatientOuderHandtekeningGewenst";

        // 'Reden' (1: String)
        public const int ID_ToestemmingPatientRedenGeenToestemming = 235;
        public const string Name_ToestemmingPatientRedenGeenToestemming = "ToestemmingPatientRedenGeenToestemming";

        // 'Toestemming van ouder of diens verzorger' (6: Checkbox)
        public const int ID_ToestemmingPatientVanOuderOfDiensVertegenwoordiger = 236;
        public const string Name_ToestemmingPatientVanOuderOfDiensVertegenwoordiger = "ToestemmingPatientVanOuderOfDiensVertegenwoordiger";

        // 'Toestemming van patient' (6: Checkbox)
        public const int ID_ToestemmingPatientVanPatient = 237;
        public const string Name_ToestemmingPatientVanPatient = "ToestemmingPatientVanPatient";

        // 'Adres vertegenwoordiger' (1: String)
        public const int ID_ToestemmingPatientVanVertegenwoordigerAdres = 238;
        public const string Name_ToestemmingPatientVanVertegenwoordigerAdres = "ToestemmingPatientVanVertegenwoordigerAdres";

        // 'Naam vertegenwoordiger' (1: String)
        public const int ID_ToestemmingPatientVanVertegenwoordigerNaam = 239;
        public const string Name_ToestemmingPatientVanVertegenwoordigerNaam = "ToestemmingPatientVanVertegenwoordigerNaam";

        // 'Transfusies bloed' (6: Checkbox)
        public const int ID_TransfusiesBloed = 240;
        public const string Name_TransfusiesBloed = "TransfusiesBloed";

        // 'Per dag' (1: String)
        public const int ID_TransfusiesBloedKerenPerDag = 241;
        public const string Name_TransfusiesBloedKerenPerDag = "TransfusiesBloedKerenPerDag";

        // 'Per week' (1: String)
        public const int ID_TransfusiesBloedKerenPerWeek = 242;
        public const string Name_TransfusiesBloedKerenPerWeek = "TransfusiesBloedKerenPerWeek";

        // 'Toelichting' (1: String)
        public const int ID_TransfusiesBloedToelichting = 243;
        public const string Name_TransfusiesBloedToelichting = "TransfusiesBloedToelichting";

        // 'Drank' (6: Checkbox)
        public const int ID_VerslavingDrank = 244;
        public const string Name_VerslavingDrank = "VerslavingDrank";

        // 'Drugs' (6: Checkbox)
        public const int ID_VerslavingDrugs = 245;
        public const string Name_VerslavingDrugs = "VerslavingDrugs";

        // 'Medicatie' (6: Checkbox)
        public const int ID_VerslavingMedicatie = 246;
        public const string Name_VerslavingMedicatie = "VerslavingMedicatie";

        // 'Wondverzorging' (6: Checkbox)
        public const int ID_Wondverzorging = 247;
        public const string Name_Wondverzorging = "Wondverzorging";

        // 'Per dag' (1: String)
        public const int ID_WondverzorgingKerenPerDag = 248;
        public const string Name_WondverzorgingKerenPerDag = "WondverzorgingKerenPerDag";

        // 'Per week' (1: String)
        public const int ID_WondverzorgingKerenPerWeek = 249;
        public const string Name_WondverzorgingKerenPerWeek = "WondverzorgingKerenPerWeek";

        // 'Toelichting' (1: String)
        public const int ID_WondverzorgingToelichting = 250;
        public const string Name_WondverzorgingToelichting = "WondverzorgingToelichting";

        // 'Type zorginstelling' (7: DropDownList)
        public const int ID_ZorginzetVoorDezeOpnameTypeZorginstelling = 251;
        public const string Name_ZorginzetVoorDezeOpnameTypeZorginstelling = "ZorginzetVoorDezeOpnameTypeZorginstelling";

        // 'Of' (1: String)
        public const int ID_ZorginzetVoorDezeOpnameTypeZorginstellingOf = 252;
        public const string Name_ZorginzetVoorDezeOpnameTypeZorginstellingOf = "ZorginzetVoorDezeOpnameTypeZorginstellingOf";

        // 'Toelichting' (1: String)
        public const int ID_ZorginzetVoorDezeOpnameTypeZorginstellingToelichting = 253;
        public const string Name_ZorginzetVoorDezeOpnameTypeZorginstellingToelichting = "ZorginzetVoorDezeOpnameTypeZorginstellingToelichting";

        // 'Aanpassing van bestaande zorg' (5: RadioButton)
        public const int ID_ZorgInZorginstellingAanpassingVanBestaandeZorg = 254;
        public const string Name_ZorgInZorginstellingAanpassingVanBestaandeZorg = "ZorgInZorginstellingAanpassingVanBestaandeZorg";

        // 'Continuering van bestaande zorg' (5: RadioButton)
        public const int ID_ZorgInZorginstellingContinueringVanBestaandeZorg = 255;
        public const string Name_ZorgInZorginstellingContinueringVanBestaandeZorg = "ZorgInZorginstellingContinueringVanBestaandeZorg";

        // 'Hospice/palliatieve terminale zorgunit' (5: RadioButton)
        public const int ID_ZorgInZorginstellingHospicePalliatieveTerminaleZorgunit = 256;
        public const string Name_ZorgInZorginstellingHospicePalliatieveTerminaleZorgunit = "ZorgInZorginstellingHospicePalliatieveTerminaleZorgunit";

        // 'Nieuwe aanvraag' (5: RadioButton)
        public const int ID_ZorgInZorginstellingNieuweAanvraag = 257;
        public const string Name_ZorgInZorginstellingNieuweAanvraag = "ZorgInZorginstellingNieuweAanvraag";

        // 'Palliatieve/terminale zorg' (5: RadioButton)
        public const int ID_ZorgInZorginstellingPalliatieveTerminaleZorg = 258;
        public const string Name_ZorgInZorginstellingPalliatieveTerminaleZorg = "ZorgInZorginstellingPalliatieveTerminaleZorg";

        // 'Permanent verblijf' (5: RadioButton)
        public const int ID_ZorgInZorginstellingPermanentVerblijf = 259;
        public const string Name_ZorgInZorginstellingPermanentVerblijf = "ZorgInZorginstellingPermanentVerblijf";

        // 'Anders, nl' (5: RadioButton)
        public const int ID_ZorgInZorginstellingPermanentVerblijfAnders = 260;
        public const string Name_ZorgInZorginstellingPermanentVerblijfAnders = "ZorgInZorginstellingPermanentVerblijfAnders";

        // 'Anders, nl' (1: String)
        public const int ID_ZorgInZorginstellingPermanentVerblijfAndersText = 261;
        public const string Name_ZorgInZorginstellingPermanentVerblijfAndersText = "ZorgInZorginstellingPermanentVerblijfAndersText";

        // 'PG' (5: RadioButton)
        public const int ID_ZorgInZorginstellingPermanentVerblijfPG = 262;
        public const string Name_ZorgInZorginstellingPermanentVerblijfPG = "ZorgInZorginstellingPermanentVerblijfPG";

        // 'Somatiek' (5: RadioButton)
        public const int ID_ZorgInZorginstellingPermanentVerblijfSomatiek = 263;
        public const string Name_ZorgInZorginstellingPermanentVerblijfSomatiek = "ZorgInZorginstellingPermanentVerblijfSomatiek";

        // 'Revalidatie en terugkeer naar huis' (5: RadioButton)
        public const int ID_ZorgInZorginstellingRevalidatieEnTerugkeerNaarHuis = 264;
        public const string Name_ZorgInZorginstellingRevalidatieEnTerugkeerNaarHuis = "ZorgInZorginstellingRevalidatieEnTerugkeerNaarHuis";

        // 'Screenen wonen' (5: RadioButton)
        public const int ID_ZorgInZorginstellingScreenenWonen = 265;
        public const string Name_ZorgInZorginstellingScreenenWonen = "ZorgInZorginstellingScreenenWonen";

        // 'Screeningprogramma' (5: RadioButton)
        public const int ID_ZorgInZorginstellingScreeningprogramma = 266;
        public const string Name_ZorgInZorginstellingScreeningprogramma = "ZorgInZorginstellingScreeningprogramma";

        // 'Tijdelijk verblijf/revalidatie' (5: RadioButton)
        public const int ID_ZorgInZorginstellingTijdelijkVerblijfRevalidatie = 267;
        public const string Name_ZorgInZorginstellingTijdelijkVerblijfRevalidatie = "ZorgInZorginstellingTijdelijkVerblijfRevalidatie";

        // 'PG' (5: RadioButton)
        public const int ID_ZorgInZorginstellingTijdelijkVerblijfRevalidatiePG = 268;
        public const string Name_ZorgInZorginstellingTijdelijkVerblijfRevalidatiePG = "ZorgInZorginstellingTijdelijkVerblijfRevalidatiePG";

        // 'Somatiek' (5: RadioButton)
        public const int ID_ZorgInZorginstellingTijdelijkVerblijfRevalidatieSomatiek = 269;
        public const string Name_ZorgInZorginstellingTijdelijkVerblijfRevalidatieSomatiek = "ZorgInZorginstellingTijdelijkVerblijfRevalidatieSomatiek";

        // 'Verpleeghuiszorg' (5: RadioButton)
        public const int ID_ZorgInZorginstellingVerpleeghuiszorg = 270;
        public const string Name_ZorgInZorginstellingVerpleeghuiszorg = "ZorgInZorginstellingVerpleeghuiszorg";

        // 'Verzorgingshuis; Kortdurende opname' (5: RadioButton)
        public const int ID_ZorgInZorginstellingVerzorgingshuisKortdurendeOpname = 271;
        public const string Name_ZorgInZorginstellingVerzorgingshuisKortdurendeOpname = "ZorgInZorginstellingVerzorgingshuisKortdurendeOpname";

        // 'Toelichting voorkeur patiënt' (1: String)
        public const int ID_ZorgInZorginstellingVoorkeurPatientZorgInGemeenteText = 272;
        public const string Name_ZorgInZorginstellingVoorkeurPatientZorgInGemeenteText = "ZorgInZorginstellingVoorkeurPatientZorgInGemeenteText";

        // 'Zorghotel/herstellingsoord' (5: RadioButton)
        public const int ID_ZorgInZorginstellingZorghotelHerstellingsoord = 273;
        public const string Name_ZorgInZorginstellingZorghotelHerstellingsoord = "ZorgInZorginstellingZorghotelHerstellingsoord";

        // 'Toelichting' (1: String)
        public const int ID_ZorgInZorginstellingZorghotelHerstellingsoordToelichting = 274;
        public const string Name_ZorgInZorginstellingZorghotelHerstellingsoordToelichting = "ZorgInZorginstellingZorghotelHerstellingsoordToelichting";

        // 'Aanvraag type' (7: DropDownList)
        public const int ID_RequestFormZHVVTType = 275;
        public const string Name_RequestFormZHVVTType = "RequestFormZHVVTType";

        // 'Versturende organisatie' (8: IDText)
        public const int ID_SourceOrganization = 276;
        public const string Name_SourceOrganization = "SourceOrganization";

        // 'Behandelend Arts' (7: DropDownList)
        public const int ID_BehandelendArtsID = 277;
        public const string Name_BehandelendArtsID = "BehandelendArtsID";

        // 'Specialisme' (7: DropDownList)
        public const int ID_SpecialismeHospital = 278;
        public const string Name_SpecialismeHospital = "SpecialismeHospital";

        // 'Toestemming van' (5: RadioButton)
        public const int ID_ToestemmingPatientVan = 279;
        public const string Name_ToestemmingPatientVan = "ToestemmingPatientVan";

        // 'Geen toestemming van' (5: RadioButton)
        public const int ID_GeenToestemmingPatientVan = 280;
        public const string Name_GeenToestemmingPatientVan = "GeenToestemmingPatientVan";

        // 'Reden' (1: String)
        public const int ID_GeenToestemmingPatientVanReden = 281;
        public const string Name_GeenToestemmingPatientVanReden = "GeenToestemmingPatientVanReden";

        // 'Toestemming verkregen' (5: RadioButton)
        public const int ID_ToestemmingPatient = 282;
        public const string Name_ToestemmingPatient = "ToestemmingPatient";

        // 'Ingevuld door' (7: DropDownList)
        public const int ID_ToestemmingPatientIngevuldDoor = 283;
        public const string Name_ToestemmingPatientIngevuldDoor = "ToestemmingPatientIngevuldDoor";

        // 'Infectie' (6: Checkbox)
        public const int ID_Infectie = 284;
        public const string Name_Infectie = "Infectie";

        // 'Verslaving' (6: Checkbox)
        public const int ID_Verslaving = 285;
        public const string Name_Verslaving = "Verslaving";

        // 'Obesitas' (6: Checkbox)
        public const int ID_Obesitas = 286;
        public const string Name_Obesitas = "Obesitas";

        // 'Typering nazorg' (5: RadioButton)
        public const int ID_TyperingNazorg = 287;
        public const string Name_TyperingNazorg = "TyperingNazorg";

        // 'Patiënt moet zorg ontvangen vanaf datum' (9: Date)
        public const int ID_CareBeginDate = 288;
        public const string Name_CareBeginDate = "CareBeginDate";

        // 'Patiënt moet zorg ontvangen tussen(uur)' (3: Time)
        public const int ID_CareBeginTime = 289;
        public const string Name_CareBeginTime = "CareBeginTime";

        // 'En(uur)' (3: Time)
        public const int ID_CareEndTime = 290;
        public const string Name_CareEndTime = "CareEndTime";

        // 'COPD' (6: Checkbox)
        public const int ID_SpecificAftercareCOPD = 291;
        public const string Name_SpecificAftercareCOPD = "SpecificAftercareCOPD";

        // 'MSVT' (6: Checkbox)
        public const int ID_SpecificAftercareMSVT = 292;
        public const string Name_SpecificAftercareMSVT = "SpecificAftercareMSVT";

        // 'Warme Overdracht' (6: Checkbox)
        public const int ID_SpecificAftercareWO = 293;
        public const string Name_SpecificAftercareWO = "SpecificAftercareWO";

        // 'Patiënt wil' (5: RadioButton)
        public const int ID_AfterCarePreference = 294;
        public const string Name_AfterCarePreference = "AfterCarePreference";

        // 'VVT 1e voorkeur' (8: IDText)
        public const int ID_ZorgInZorginstellingVoorkeurPatient1 = 295;
        public const string Name_ZorgInZorginstellingVoorkeurPatient1 = "ZorgInZorginstellingVoorkeurPatient1";

        // 'VVT 2e voorkeur' (8: IDText)
        public const int ID_ZorgInZorginstellingVoorkeurPatient2 = 296;
        public const string Name_ZorgInZorginstellingVoorkeurPatient2 = "ZorgInZorginstellingVoorkeurPatient2";

        // 'VVT 3e voorkeur' (8: IDText)
        public const int ID_ZorgInZorginstellingVoorkeurPatient3 = 297;
        public const string Name_ZorgInZorginstellingVoorkeurPatient3 = "ZorgInZorginstellingVoorkeurPatient3";

        // 'Te versturen naar VVT' (8: IDText)
        public const int ID_SelectedHealthCareProvider = 298;
        public const string Name_SelectedHealthCareProvider = "SelectedHealthCareProvider";

        // 'Te versturen naar ind. CIZ organisatie' (8: IDText)
        public const int ID_SelectedII = 300;
        public const string Name_SelectedII = "SelectedII";

        // 'Aanvraag bij indicatiesteller' (2: DateTime)
        public const int ID_TransferDateII = 301;
        public const string Name_TransferDateII = "TransferDateII";

        // 'Aanvraag in behandeling genomen' (2: DateTime)
        public const int ID_AcceptedDateII = 302;
        public const string Name_AcceptedDateII = "AcceptedDateII";

        // 'In behandeling genomen door' (7: DropDownList)
        public const int ID_AcceptedByII = 303;
        public const string Name_AcceptedByII = "AcceptedByII";

        // 'Telefoon nummer' (1: String)
        public const int ID_AcceptedByTelephoneII = 304;
        public const string Name_AcceptedByTelephoneII = "AcceptedByTelephoneII";

        // 'Aanvraag bij VVT' (2: DateTime)
        public const int ID_TransferDateVVT = 305;
        public const string Name_TransferDateVVT = "TransferDateVVT";

        // 'Aanvraag in behandeling genomen' (2: DateTime)
        public const int ID_AcceptedDateVVT = 306;
        public const string Name_AcceptedDateVVT = "AcceptedDateVVT";

        // 'In behandeling genomen door' (7: DropDownList)
        public const int ID_AcceptedByVVT = 307;
        public const string Name_AcceptedByVVT = "AcceptedByVVT";

        // 'Telefoon nummer' (1: String)
        public const int ID_AcceptedByTelephoneVVT = 308;
        public const string Name_AcceptedByTelephoneVVT = "AcceptedByTelephoneVVT";

        // 'Kan gevraagde zorg geboden worden?' (5: RadioButton)
        public const int ID_DischargePatientAcceptedYNByVVT = 309;
        public const string Name_DischargePatientAcceptedYNByVVT = "DischargePatientAcceptedYNByVVT";

        // 'Patient kan door VVT in zorg worden genomen vanaf' (9: Date)
        public const int ID_DischargeProposedStartDate = 310;
        public const string Name_DischargeProposedStartDate = "DischargeProposedStartDate";

        // 'Thuiszorg komt voor eerste zorgmoment langs tussen (uur)' (3: Time)
        public const int ID_DischargeHomecareBeginTime = 311;
        public const string Name_DischargeHomecareBeginTime = "DischargeHomecareBeginTime";

        // 'en (uur)' (3: Time)
        public const int ID_DischargeHomecareEndTime = 312;
        public const string Name_DischargeHomecareEndTime = "DischargeHomecareEndTime";

        // 'Opmerkingen' (1: String)
        public const int ID_DischargeRemarks = 313;
        public const string Name_DischargeRemarks = "DischargeRemarks";

        // 'Patient wordt verwacht om (uur)' (3: Time)
        public const int ID_DischargePatientExpectedTime = 314;
        public const string Name_DischargePatientExpectedTime = "DischargePatientExpectedTime";

        // 'Reden' (1: String)
        public const int ID_DischargeReason = 315;
        public const string Name_DischargeReason = "DischargeReason";

        // 'Te versturen naar ind. GRZ organisatie' (8: IDText)
        public const int ID_SelectedGRZ = 316;
        public const string Name_SelectedGRZ = "SelectedGRZ";

        // 'Patiënt kan overgenomen worden op gewenste startdatum/tijd?' (5: RadioButton)
        public const int ID_PatientAcceptedAtDateTimeYN = 317;
        public const string Name_PatientAcceptedAtDateTimeYN = "PatientAcceptedAtDateTimeYN";

        // 'Contra-indicatie' (11: Hidden)
        public const int ID_GRZFormContraIndicationStatus = 318;
        public const string Name_GRZFormContraIndicationStatus = "GRZFormContraIndicationStatus";

        // 'Meldingen zijn afgehandeld' (6: Checkbox)
        public const int ID_GRZFormContraIndicationOverride = 319;
        public const string Name_GRZFormContraIndicationOverride = "GRZFormContraIndicationOverride";

        // 'Apotheek' (1: String)
        public const int ID_PharmacyName = 398;
        public const string Name_PharmacyName = "PharmacyName";

        // 'Burger Service Nummer' (4: Number)
        public const int ID_CivilServiceNumber = 399;
        public const string Name_CivilServiceNumber = "CivilServiceNumber";

        // 'Aanspreekpersoon voor client' (1: String)
        public const int ID_AanspreekpersoonVoorClient = 400;
        public const string Name_AanspreekpersoonVoorClient = "AanspreekpersoonVoorClient";

        // 'Afspraken cliënt/naasten' (1: String)
        public const int ID_AfsprakenClient = 401;
        public const string Name_AfsprakenClient = "AfsprakenClient";

        // 'Stoornis bij bewustzijn' (7: DropDownList)
        public const int ID_AlgemeneMentaleFunctieBewustzijn = 402;
        public const string Name_AlgemeneMentaleFunctieBewustzijn = "AlgemeneMentaleFunctieBewustzijn";

        // 'Intellectuele functies' (7: DropDownList)
        public const int ID_AlgemeneMentaleFunctieIntellectueleFuncties = 403;
        public const string Name_AlgemeneMentaleFunctieIntellectueleFuncties = "AlgemeneMentaleFunctieIntellectueleFuncties";

        // 'Persoon' (7: DropDownList)
        public const int ID_AlgemeneMentaleFunctieOrientatiePersoon = 404;
        public const string Name_AlgemeneMentaleFunctieOrientatiePersoon = "AlgemeneMentaleFunctieOrientatiePersoon";

        // 'Plaats' (7: DropDownList)
        public const int ID_AlgemeneMentaleFunctieOrientatiePlaats = 405;
        public const string Name_AlgemeneMentaleFunctieOrientatiePlaats = "AlgemeneMentaleFunctieOrientatiePlaats";

        // 'Tijd' (7: DropDownList)
        public const int ID_AlgemeneMentaleFunctieOrientatieTijd = 406;
        public const string Name_AlgemeneMentaleFunctieOrientatieTijd = "AlgemeneMentaleFunctieOrientatieTijd";

        // 'Toelichting' (1: String)
        public const int ID_AlgemeneMentaleFunctieToelichting = 407;
        public const string Name_AlgemeneMentaleFunctieToelichting = "AlgemeneMentaleFunctieToelichting";

        // 'Verpleegkundige interventies' (1: String)
        public const int ID_AlgemeneMentaleFunctieVerpleegkundigeInterventies = 408;
        public const string Name_AlgemeneMentaleFunctieVerpleegkundigeInterventies = "AlgemeneMentaleFunctieVerpleegkundigeInterventies";

        // 'Bron bewering' (1: String)
        public const int ID_AllergieBronBewering = 409;
        public const string Name_AllergieBronBewering = "AllergieBronBewering";

        // 'Soort en reactie' (1: String)
        public const int ID_AllergieSoortEnReactie = 410;
        public const string Name_AllergieSoortEnReactie = "AllergieSoortEnReactie";

        // 'Andere relevante medische aandoening' (1: String)
        public const int ID_AndereRelevanteMedischeAandoening = 411;
        public const string Name_AndereRelevanteMedischeAandoening = "AndereRelevanteMedischeAandoening";

        // 'Begin in zorg/behandeling' (9: Date)
        public const int ID_BeginInZorgBehandeling = 412;
        public const string Name_BeginInZorgBehandeling = "BeginInZorgBehandeling";

        // 'Begin klachten' (1: String)
        public const int ID_BeginKlachten = 413;
        public const string Name_BeginKlachten = "BeginKlachten";

        // 'Toelichting' (1: String)
        public const int ID_BloeddrukToelichting = 414;
        public const string Name_BloeddrukToelichting = "BloeddrukToelichting";

        // 'Burgerlijke staat' (7: DropDownList)
        public const int ID_BurgerlijkeStaat = 415;
        public const string Name_BurgerlijkeStaat = "BurgerlijkeStaat";

        // 'CIZ-indicatie' (1: String)
        public const int ID_CIZIndicatie = 416;
        public const string Name_CIZIndicatie = "CIZIndicatie";

        // 'Non verbale communicatie' (7: DropDownList)
        public const int ID_CommunicatieBegrijpenNonVerbale = 417;
        public const string Name_CommunicatieBegrijpenNonVerbale = "CommunicatieBegrijpenNonVerbale";

        // 'Verbale communicatie' (7: DropDownList)
        public const int ID_CommunicatieBegrijpenVerbale = 418;
        public const string Name_CommunicatieBegrijpenVerbale = "CommunicatieBegrijpenVerbale";

        // 'Spreekt Nederlandse taal' (1: String)
        public const int ID_CommunicatieSpreektNederlandse = 419;
        public const string Name_CommunicatieSpreektNederlandse = "CommunicatieSpreektNederlandse";

        // 'Toelichting' (1: String)
        public const int ID_CommunicatieToelichting = 420;
        public const string Name_CommunicatieToelichting = "CommunicatieToelichting";

        // 'Verpleegkundige interventies' (1: String)
        public const int ID_CommunicatieVerpleegkundigeInterventies = 421;
        public const string Name_CommunicatieVerpleegkundigeInterventies = "CommunicatieVerpleegkundigeInterventies";

        // 'Non verbale communicatie' (7: DropDownList)
        public const int ID_CommunicatieZichKunnenUitenNonVerbale = 422;
        public const string Name_CommunicatieZichKunnenUitenNonVerbale = "CommunicatieZichKunnenUitenNonVerbale";

        // 'Verbale communicatie' (7: DropDownList)
        public const int ID_CommunicatieZichKunnenUitenVerbale = 423;
        public const string Name_CommunicatieZichKunnenUitenVerbale = "CommunicatieZichKunnenUitenVerbale";

        // 'Datum bloeddruk' (9: Date)
        public const int ID_DatumBloeddruk = 424;
        public const string Name_DatumBloeddruk = "DatumBloeddruk";

        // 'Datum frequentie' (9: Date)
        public const int ID_DatumFrequentie = 425;
        public const string Name_DatumFrequentie = "DatumFrequentie";

        // 'Datum gewicht' (9: Date)
        public const int ID_DatumGewicht = 426;
        public const string Name_DatumGewicht = "DatumGewicht";

        // 'Datum lengte' (9: Date)
        public const int ID_DatumLengte = 427;
        public const string Name_DatumLengte = "DatumLengte";

        // 'Datum meting' (9: Date)
        public const int ID_DatumMeting = 428;
        public const string Name_DatumMeting = "DatumMeting";

        // 'Datum temperatuur' (9: Date)
        public const int ID_DatumTemperatuur = 429;
        public const string Name_DatumTemperatuur = "DatumTemperatuur";

        // 'Datum zuurstofgebruik' (9: Date)
        public const int ID_DatumZuurstofgebruik = 430;
        public const string Name_DatumZuurstofgebruik = "DatumZuurstofgebruik";

        // 'Diastole' (1: String)
        public const int ID_Diastole = 431;
        public const string Name_Diastole = "Diastole";

        // 'Discipline mede behandelaars' (1: String)
        public const int ID_DisciplineMedeBehandelaars = 432;
        public const string Name_DisciplineMedeBehandelaars = "DisciplineMedeBehandelaars";

        // 'Bereiden (openen/ uitschenken)' (7: DropDownList)
        public const int ID_EtenDrinkenDrinkenBereiden = 433;
        public const string Name_EtenDrinkenDrinkenBereiden = "EtenDrinkenDrinkenBereiden";

        // 'Naar mond brengen' (7: DropDownList)
        public const int ID_EtenDrinkenDrinkenNaarMondBrengen = 434;
        public const string Name_EtenDrinkenDrinkenNaarMondBrengen = "EtenDrinkenDrinkenNaarMondBrengen";

        // 'Vastpakken' (7: DropDownList)
        public const int ID_EtenDrinkenDrinkenVastpakken = 435;
        public const string Name_EtenDrinkenDrinkenVastpakken = "EtenDrinkenDrinkenVastpakken";

        // 'Eetgerei hanteren' (7: DropDownList)
        public const int ID_EtenDrinkenEatenEetgereiHanteren = 436;
        public const string Name_EtenDrinkenEatenEetgereiHanteren = "EtenDrinkenEatenEetgereiHanteren";

        // 'Naar mond brengen' (7: DropDownList)
        public const int ID_EtenDrinkenEatenNaarMondBrengen = 437;
        public const string Name_EtenDrinkenEatenNaarMondBrengen = "EtenDrinkenEatenNaarMondBrengen";

        // 'Snijden / openen' (7: DropDownList)
        public const int ID_EtenDrinkenEatenSnijdenOpenen = 438;
        public const string Name_EtenDrinkenEatenSnijdenOpenen = "EtenDrinkenEatenSnijdenOpenen";

        // 'Verpleegkundige interventies' (1: String)
        public const int ID_EtenDrinkenVerpleegkundigeInterventies = 439;
        public const string Name_EtenDrinkenVerpleegkundigeInterventies = "EtenDrinkenVerpleegkundigeInterventies";

        // 'Lichaamsgewicht' (1: String)
        public const int ID_Gewicht = 440;
        public const string Name_Gewicht = "Gewicht";

        // 'Aanwezig' (5: RadioButton)
        public const int ID_HuidDecubitusAanwezig = 441;
        public const string Name_HuidDecubitusAanwezig = "HuidDecubitusAanwezig";

        // 'Behandelplan' (1: String)
        public const int ID_HuidDecubitusBehandelplan = 442;
        public const string Name_HuidDecubitusBehandelplan = "HuidDecubitusBehandelplan";

        // 'Categorie' (7: DropDownList)
        public const int ID_HuidDecubitusCategorie = 443;
        public const string Name_HuidDecubitusCategorie = "HuidDecubitusCategorie";

        // 'Locatie' (1: String)
        public const int ID_HuidDecubitusLocatie = 444;
        public const string Name_HuidDecubitusLocatie = "HuidDecubitusLocatie";

        // 'Preventie' (1: String)
        public const int ID_HuidDecubitusPreventie = 445;
        public const string Name_HuidDecubitusPreventie = "HuidDecubitusPreventie";

        // 'Aanwezig' (5: RadioButton)
        public const int ID_HuidHuidletselAanwezig = 446;
        public const string Name_HuidHuidletselAanwezig = "HuidHuidletselAanwezig";

        // 'Behandelplan' (1: String)
        public const int ID_HuidHuidletselBehandelplan = 447;
        public const string Name_HuidHuidletselBehandelplan = "HuidHuidletselBehandelplan";

        // 'Toelichting' (1: String)
        public const int ID_HuidHuidletselFaseBeschrijving = 448;
        public const string Name_HuidHuidletselFaseBeschrijving = "HuidHuidletselFaseBeschrijving";

        // 'Locatie' (1: String)
        public const int ID_HuidHuidletselLocatie = 449;
        public const string Name_HuidHuidletselLocatie = "HuidHuidletselLocatie";

        // 'Preventie' (1: String)
        public const int ID_HuidHuidletselPreventie = 450;
        public const string Name_HuidHuidletselPreventie = "HuidHuidletselPreventie";

        // 'Aanwezig' (5: RadioButton)
        public const int ID_HuidOverigeAanwezig = 451;
        public const string Name_HuidOverigeAanwezig = "HuidOverigeAanwezig";

        // 'Behandelplan' (1: String)
        public const int ID_HuidOverigeBehandelplan = 452;
        public const string Name_HuidOverigeBehandelplan = "HuidOverigeBehandelplan";

        // 'Locatie' (1: String)
        public const int ID_HuidOverigeLocatie = 453;
        public const string Name_HuidOverigeLocatie = "HuidOverigeLocatie";

        // 'Soort' (1: String)
        public const int ID_HuidOverigeSoort = 454;
        public const string Name_HuidOverigeSoort = "HuidOverigeSoort";

        // 'Aanwezig' (5: RadioButton)
        public const int ID_HuidWondAanwezig = 455;
        public const string Name_HuidWondAanwezig = "HuidWondAanwezig";

        // 'Behandeling' (1: String)
        public const int ID_HuidWondBehandeling = 456;
        public const string Name_HuidWondBehandeling = "HuidWondBehandeling";

        // 'Locatie' (1: String)
        public const int ID_HuidWondLocatie = 457;
        public const string Name_HuidWondLocatie = "HuidWondLocatie";

        // 'Soort' (7: DropDownList)
        public const int ID_HuidWondSoort = 458;
        public const string Name_HuidWondSoort = "HuidWondSoort";

        // 'Huisnummertoevoeging' (1: String)
        public const int ID_Huisnummertoevoeging = 459;
        public const string Name_Huisnummertoevoeging = "Huisnummertoevoeging";

        // 'Aard' (1: String)
        public const int ID_HulpVanAnderenMantelzorgAard = 460;
        public const string Name_HulpVanAnderenMantelzorgAard = "HulpVanAnderenMantelzorgAard";

        // 'Frequentie' (1: String)
        public const int ID_HulpVanAnderenMantelzorgFrequentie = 461;
        public const string Name_HulpVanAnderenMantelzorgFrequentie = "HulpVanAnderenMantelzorgFrequentie";

        // 'Aard' (1: String)
        public const int ID_HulpVanAnderenProfessioneleAard = 462;
        public const string Name_HulpVanAnderenProfessioneleAard = "HulpVanAnderenProfessioneleAard";

        // 'Frequentie' (1: String)
        public const int ID_HulpVanAnderenProfessioneleFrequentie = 463;
        public const string Name_HulpVanAnderenProfessioneleFrequentie = "HulpVanAnderenProfessioneleFrequentie";

        // 'Toelichting' (1: String)
        public const int ID_HulpVanAnderenToelichting = 464;
        public const string Name_HulpVanAnderenToelichting = "HulpVanAnderenToelichting";

        // 'Aanwezig' (5: RadioButton)
        public const int ID_InfectierisicoAanwezig = 465;
        public const string Name_InfectierisicoAanwezig = "InfectierisicoAanwezig";

        // 'Toelichting' (1: String)
        public const int ID_InfectierisicoAanwezigToelichting = 466;
        public const string Name_InfectierisicoAanwezigToelichting = "InfectierisicoAanwezigToelichting";

        // 'Aantal kinderen inwonend' (4: Number)
        public const int ID_KinderenAantalKinderenInwonend = 467;
        public const string Name_KinderenAantalKinderenInwonend = "KinderenAantalKinderenInwonend";

        // 'Aanwezig' (5: RadioButton)
        public const int ID_KinderenAanwezig = 468;
        public const string Name_KinderenAanwezig = "KinderenAanwezig";

        // 'Aantal' (4: Number)
        public const int ID_KinderenAanwezigAantal = 469;
        public const string Name_KinderenAanwezigAantal = "KinderenAanwezigAantal";

        // 'Leeftijd' (1: String)
        public const int ID_KinderenLeeftijd = 470;
        public const string Name_KinderenLeeftijd = "KinderenLeeftijd";

        // 'Lichaamslengte' (1: String)
        public const int ID_Lengte = 471;
        public const string Name_Lengte = "Lengte";

        // 'Cultuur' (1: String)
        public const int ID_LevensOvertuigingCultuur = 472;
        public const string Name_LevensOvertuigingCultuur = "LevensOvertuigingCultuur";

        // 'Geregistreerd als donor' (5: RadioButton)
        public const int ID_LevensOvertuigingGeregistreerdAlsDonor = 473;
        public const string Name_LevensOvertuigingGeregistreerdAlsDonor = "LevensOvertuigingGeregistreerdAlsDonor";

        // 'Religie' (1: String)
        public const int ID_LevensOvertuigingReligie = 474;
        public const string Name_LevensOvertuigingReligie = "LevensOvertuigingReligie";

        // 'Verpleegkundige interventies' (1: String)
        public const int ID_LevensOvertuigingVerpleegkundigeInterventies = 475;
        public const string Name_LevensOvertuigingVerpleegkundigeInterventies = "LevensOvertuigingVerpleegkundigeInterventies";

        // 'Aanwezig' (5: RadioButton)
        public const int ID_LevensOvertuigingWilsverklaringAanwezig = 476;
        public const string Name_LevensOvertuigingWilsverklaringAanwezig = "LevensOvertuigingWilsverklaringAanwezig";

        // 'Waar te vinden' (1: String)
        public const int ID_LevensOvertuigingWilsverklaringWaarTeVinden = 477;
        public const string Name_LevensOvertuigingWilsverklaringWaarTeVinden = "LevensOvertuigingWilsverklaringWaarTeVinden";

        // 'Lichaamstemperatuur' (1: String)
        public const int ID_Lichaamstemperatuur = 478;
        public const string Name_Lichaamstemperatuur = "Lichaamstemperatuur";

        // 'Gebruik' (5: RadioButton)
        public const int ID_MedicatieGebruik = 479;
        public const string Name_MedicatieGebruik = "MedicatieGebruik";

        // 'Hulp bij toediening' (1: String)
        public const int ID_MedicatieGebruikToediening = 480;
        public const string Name_MedicatieGebruikToediening = "MedicatieGebruikToediening";

        // 'Mening client t.a.v. overplaatsing' (1: String)
        public const int ID_MeningClientOverplaatsing = 481;
        public const string Name_MeningClientOverplaatsing = "MeningClientOverplaatsing";

        // 'Liggen' (7: DropDownList)
        public const int ID_MobiliteitHoudingHandhavenLiggen = 482;
        public const string Name_MobiliteitHoudingHandhavenLiggen = "MobiliteitHoudingHandhavenLiggen";

        // 'Staan' (7: DropDownList)
        public const int ID_MobiliteitHoudingHandhavenStaan = 483;
        public const string Name_MobiliteitHoudingHandhavenStaan = "MobiliteitHoudingHandhavenStaan";

        // 'Zitten' (7: DropDownList)
        public const int ID_MobiliteitHoudingHandhavenZitten = 484;
        public const string Name_MobiliteitHoudingHandhavenZitten = "MobiliteitHoudingHandhavenZitten";

        // 'Liggen' (7: DropDownList)
        public const int ID_MobiliteitHoudingVeranderenLiggen = 485;
        public const string Name_MobiliteitHoudingVeranderenLiggen = "MobiliteitHoudingVeranderenLiggen";

        // 'Staan' (7: DropDownList)
        public const int ID_MobiliteitHoudingVeranderenStaan = 486;
        public const string Name_MobiliteitHoudingVeranderenStaan = "MobiliteitHoudingVeranderenStaan";

        // 'Zitten' (7: DropDownList)
        public const int ID_MobiliteitHoudingVeranderenZitten = 487;
        public const string Name_MobiliteitHoudingVeranderenZitten = "MobiliteitHoudingVeranderenZitten";

        // 'Transfers in lig' (7: DropDownList)
        public const int ID_MobiliteitTransfersInLig = 488;
        public const string Name_MobiliteitTransfersInLig = "MobiliteitTransfersInLig";

        // 'Transfers in zit' (7: DropDownList)
        public const int ID_MobiliteitTransfersInZit = 489;
        public const string Name_MobiliteitTransfersInZit = "MobiliteitTransfersInZit";

        // 'Verhoogd valrisico aanwezig' (5: RadioButton)
        public const int ID_MobiliteitValrisicoAanwezig = 490;
        public const string Name_MobiliteitValrisicoAanwezig = "MobiliteitValrisicoAanwezig";

        // 'Toelichting' (1: String)
        public const int ID_MobiliteitValrisicoToelichting = 491;
        public const string Name_MobiliteitValrisicoToelichting = "MobiliteitValrisicoToelichting";

        // 'Verpleegkundige interventies' (1: String)
        public const int ID_MobiliteitVerpleegkundigeInterventies = 492;
        public const string Name_MobiliteitVerpleegkundigeInterventies = "MobiliteitVerpleegkundigeInterventies";

        // 'Hulpmiddelen' (1: String)
        public const int ID_MobiliteitVoortbewegenHulpmiddelen = 493;
        public const string Name_MobiliteitVoortbewegenHulpmiddelen = "MobiliteitVoortbewegenHulpmiddelen";

        // 'Lopen' (7: DropDownList)
        public const int ID_MobiliteitVoortbewegenLopen = 494;
        public const string Name_MobiliteitVoortbewegenLopen = "MobiliteitVoortbewegenLopen";

        // 'Orthothese aanwezig' (5: RadioButton)
        public const int ID_MondVerzorgingOrthotheseAanwezig = 495;
        public const string Name_MondVerzorgingOrthotheseAanwezig = "MondVerzorgingOrthotheseAanwezig";

        // 'Prothese aanwezig' (5: RadioButton)
        public const int ID_MondVerzorgingProtheseAanwezig = 496;
        public const string Name_MondVerzorgingProtheseAanwezig = "MondVerzorgingProtheseAanwezig";

        // 'Soort ondersteuning tandverzorging' (1: String)
        public const int ID_MondVerzorgingSoortOndersteuningTandverzorging = 497;
        public const string Name_MondVerzorgingSoortOndersteuningTandverzorging = "MondVerzorgingSoortOndersteuningTandverzorging";

        // 'Verpleegkundige interventies' (1: String)
        public const int ID_MondVerzorgingVerpleegkundigeInterventies = 498;
        public const string Name_MondVerzorgingVerpleegkundigeInterventies = "MondVerzorgingVerpleegkundigeInterventies";

        // 'Contactpersoon' (8: IDText)
        public const int ID_OntvangendeContactpersoon = 499;
        public const string Name_OntvangendeContactpersoon = "OntvangendeContactpersoon";

        // 'Datum overplaatsing' (9: Date)
        public const int ID_OntvangendeDatumOverplaatsing = 500;
        public const string Name_OntvangendeDatumOverplaatsing = "OntvangendeDatumOverplaatsing";

        // 'Soort organisatie' (7: DropDownList)
        public const int ID_OntvangendeSoortOrganisatie = 501;
        public const string Name_OntvangendeSoortOrganisatie = "OntvangendeSoortOrganisatie";

        // 'E-mail adres' (1: String)
        public const int ID_OrganisatieEmail = 502;
        public const string Name_OrganisatieEmail = "OrganisatieEmail";

        // 'Contactpersoon' (7: DropDownList)
        public const int ID_OrganisatieNaamContactpersoon = 503;
        public const string Name_OrganisatieNaamContactpersoon = "OrganisatieNaamContactpersoon";

        // 'Opsteller overdracht' (7: DropDownList)
        public const int ID_OrganisatieOpstellerOverdracht = 504;
        public const string Name_OrganisatieOpstellerOverdracht = "OrganisatieOpstellerOverdracht";

        // 'Telefoonnummer' (1: String)
        public const int ID_OrganisatieTelefoonnummer = 505;
        public const string Name_OrganisatieTelefoonnummer = "OrganisatieTelefoonnummer";

        // 'Aanvraag hulpmiddelen/aanpassingen' (1: String)
        public const int ID_OverigAanvraagHulpmiddelen = 506;
        public const string Name_OverigAanvraagHulpmiddelen = "OverigAanvraagHulpmiddelen";

        // 'E-health voorzieningen' (1: String)
        public const int ID_OverigEHealthVoorzieningen = 507;
        public const string Name_OverigEHealthVoorzieningen = "OverigEHealthVoorzieningen";

        // 'Meetwaarde' (1: String)
        public const int ID_OverigeMeetwaarden = 508;
        public const string Name_OverigeMeetwaarden = "OverigeMeetwaarden";

        // 'Meegegeven documentatie' (1: String)
        public const int ID_OverigMeegegevenDocumentatie = 509;
        public const string Name_OverigMeegegevenDocumentatie = "OverigMeegegevenDocumentatie";

        // 'Hobby' (1: String)
        public const int ID_ParticipatieInMaatschappijParticipatieHobby = 510;
        public const string Name_ParticipatieInMaatschappijParticipatieHobby = "ParticipatieInMaatschappijParticipatieHobby";

        // 'Sociaal netwerk' (1: String)
        public const int ID_ParticipatieInMaatschappijParticipatieSociaalNetwerk = 511;
        public const string Name_ParticipatieInMaatschappijParticipatieSociaalNetwerk = "ParticipatieInMaatschappijParticipatieSociaalNetwerk";

        // 'Toelichting' (1: String)
        public const int ID_ParticipatieInMaatschappijParticipatieToelichting = 512;
        public const string Name_ParticipatieInMaatschappijParticipatieToelichting = "ParticipatieInMaatschappijParticipatieToelichting";

        // 'Werk / beroep' (1: String)
        public const int ID_ParticipatieInMaatschappijParticipatieWerkOfBeroep = 513;
        public const string Name_ParticipatieInMaatschappijParticipatieWerkOfBeroep = "ParticipatieInMaatschappijParticipatieWerkOfBeroep";

        // 'Verpleegkundige interventies' (1: String)
        public const int ID_ParticipatieInMaatschappijVerpleegkundigeInterventies = 514;
        public const string Name_ParticipatieInMaatschappijVerpleegkundigeInterventies = "ParticipatieInMaatschappijVerpleegkundigeInterventies";

        // 'Pijn bij beweging' (7: DropDownList)
        public const int ID_PijnBijBeweging = 515;
        public const string Name_PijnBijBeweging = "PijnBijBeweging";

        // 'Pijn in rust' (7: DropDownList)
        public const int ID_PijnInRust = 516;
        public const string Name_PijnInRust = "PijnInRust";

        // 'Locatie pijn' (1: String)
        public const int ID_PijnLocatie = 517;
        public const string Name_PijnLocatie = "PijnLocatie";

        // 'Mate van pijn' (7: DropDownList)
        public const int ID_PijnMateVan = 518;
        public const string Name_PijnMateVan = "PijnMateVan";

        // 'Toelichting' (1: String)
        public const int ID_PijnToelichting = 519;
        public const string Name_PijnToelichting = "PijnToelichting";

        // 'Verpleegkundige interventies' (1: String)
        public const int ID_PijnVerpleegkundigeInterventies = 520;
        public const string Name_PijnVerpleegkundigeInterventies = "PijnVerpleegkundigeInterventies";

        // 'Frequentie' (1: String)
        public const int ID_PolsFrequentie = 521;
        public const string Name_PolsFrequentie = "PolsFrequentie";

        // 'Toelichting' (1: String)
        public const int ID_PolsToelichting = 522;
        public const string Name_PolsToelichting = "PolsToelichting";

        // 'Postbus' (1: String)
        public const int ID_Postbus = 523;
        public const string Name_Postbus = "Postbus";

        // 'Prognose' (1: String)
        public const int ID_Prognose = 524;
        public const string Name_Prognose = "Prognose";

        // 'Aanvang episode' (9: Date)
        public const int ID_PsychischFunctionerenDwangEnDrangAanvangEpisode = 525;
        public const string Name_PsychischFunctionerenDwangEnDrangAanvangEpisode = "PsychischFunctionerenDwangEnDrangAanvangEpisode";

        // 'Einde episode' (9: Date)
        public const int ID_PsychischFunctionerenDwangEnDrangEindeEpisode = 526;
        public const string Name_PsychischFunctionerenDwangEnDrangEindeEpisode = "PsychischFunctionerenDwangEnDrangEindeEpisode";

        // 'Instemming' (7: DropDownList)
        public const int ID_PsychischFunctionerenDwangEnDrangInstemming = 527;
        public const string Name_PsychischFunctionerenDwangEnDrangInstemming = "PsychischFunctionerenDwangEnDrangInstemming";

        // 'Soort Interventie' (7: DropDownList)
        public const int ID_PsychischFunctionerenDwangEnDrangInterventie = 528;
        public const string Name_PsychischFunctionerenDwangEnDrangInterventie = "PsychischFunctionerenDwangEnDrangInterventie";

        // 'Juridische status (soort)' (7: DropDownList)
        public const int ID_PsychischFunctionerenJuridischeStatus = 529;
        public const string Name_PsychischFunctionerenJuridischeStatus = "PsychischFunctionerenJuridischeStatus";

        // 'Middelengebruik (soort en hoeveelheid)' (1: String)
        public const int ID_PsychischFunctionerenMiddelengebruik = 530;
        public const string Name_PsychischFunctionerenMiddelengebruik = "PsychischFunctionerenMiddelengebruik";

        // 'Verpleegkundige interventies' (1: String)
        public const int ID_PsychischFunctionerenVerpleegkundigeInterventies = 531;
        public const string Name_PsychischFunctionerenVerpleegkundigeInterventies = "PsychischFunctionerenVerpleegkundigeInterventies";

        // 'Aanwezig' (5: RadioButton)
        public const int ID_PsychischFunctionerenVrijheidsbeperkendeInterventiesAanwezig = 532;
        public const string Name_PsychischFunctionerenVrijheidsbeperkendeInterventiesAanwezig = "PsychischFunctionerenVrijheidsbeperkendeInterventiesAanwezig";

        // 'Besproken met wettelijke vertegenwoordiger' (5: RadioButton)
        public const int ID_PsychischFunctionerenVrijheidsbeperkendeInterventiesBesproken = 533;
        public const string Name_PsychischFunctionerenVrijheidsbeperkendeInterventiesBesproken = "PsychischFunctionerenVrijheidsbeperkendeInterventiesBesproken";

        // 'Soort interventie' (1: String)
        public const int ID_PsychischFunctionerenVrijheidsbeperkendeInterventiesSoort = 534;
        public const string Name_PsychischFunctionerenVrijheidsbeperkendeInterventiesSoort = "PsychischFunctionerenVrijheidsbeperkendeInterventiesSoort";

        // 'Is er sprake van' (5: RadioButton)
        public const int ID_PsychischFunctionerenWilsbekwaamSprakeVan = 535;
        public const string Name_PsychischFunctionerenWilsbekwaamSprakeVan = "PsychischFunctionerenWilsbekwaamSprakeVan";

        // 'Wilsonbekwaam t.a.v ...' (1: String)
        public const int ID_PsychischFunctionerenWilsbekwaamWilsonbekwaam = 536;
        public const string Name_PsychischFunctionerenWilsbekwaamWilsonbekwaam = "PsychischFunctionerenWilsbekwaamWilsonbekwaam";

        // 'Reden in zorg' (1: String)
        public const int ID_RedenInZorg = 537;
        public const string Name_RedenInZorg = "RedenInZorg";

        // 'Reden overdragen zorg' (1: String)
        public const int ID_RedenOverdragenZorg = 538;
        public const string Name_RedenOverdragenZorg = "RedenOverdragenZorg";

        // 'Toelichting seksualiteit' (1: String)
        public const int ID_SeksualiteitToelichting = 539;
        public const string Name_SeksualiteitToelichting = "SeksualiteitToelichting";

        // 'Verpleegkundige interventies' (1: String)
        public const int ID_SeksualiteitVerpleegkundigeInterventies = 540;
        public const string Name_SeksualiteitVerpleegkundigeInterventies = "SeksualiteitVerpleegkundigeInterventies";

        // 'Voortplanting' (1: String)
        public const int ID_SeksualiteitVoortplanting = 541;
        public const string Name_SeksualiteitVoortplanting = "SeksualiteitVoortplanting";

        // 'Aantal weken' (1: String)
        public const int ID_SeksualiteitZwangerschapAantalWeken = 542;
        public const string Name_SeksualiteitZwangerschapAantalWeken = "SeksualiteitZwangerschapAantalWeken";

        // 'Toelichting' (1: String)
        public const int ID_SeksualiteitZwangerschapToelichting = 543;
        public const string Name_SeksualiteitZwangerschapToelichting = "SeksualiteitZwangerschapToelichting";

        // 'Zwanger' (5: RadioButton)
        public const int ID_SeksualiteitZwangerschapZwanger = 544;
        public const string Name_SeksualiteitZwangerschapZwanger = "SeksualiteitZwangerschapZwanger";

        // 'Doorslapen' (7: DropDownList)
        public const int ID_SlaapDoorslapen = 545;
        public const string Name_SlaapDoorslapen = "SlaapDoorslapen";

        // 'Inslapen' (7: DropDownList)
        public const int ID_SlaapInslapen = 546;
        public const string Name_SlaapInslapen = "SlaapInslapen";

        // 'Slaapcyclus' (7: DropDownList)
        public const int ID_SlaapSlaapcyclus = 547;
        public const string Name_SlaapSlaapcyclus = "SlaapSlaapcyclus";

        // 'Toelichting' (1: String)
        public const int ID_SlaapToelichting = 548;
        public const string Name_SlaapToelichting = "SlaapToelichting";

        // 'Verpleegkundige interventies' (1: String)
        public const int ID_SlaapVerpleegkundigeInterventies = 549;
        public const string Name_SlaapVerpleegkundigeInterventies = "SlaapVerpleegkundigeInterventies";

        // 'Soort organisatie' (7: DropDownList)
        public const int ID_SoortOrganisatie = 550;
        public const string Name_SoortOrganisatie = "SoortOrganisatie";

        // 'Soort woning' (1: String)
        public const int ID_SoortWoning = 551;
        public const string Name_SoortWoning = "SoortWoning";

        // 'Stoornis bij aandacht' (7: DropDownList)
        public const int ID_SpecifiekeMentaleFunctieAandacht = 552;
        public const string Name_SpecifiekeMentaleFunctieAandacht = "SpecifiekeMentaleFunctieAandacht";

        // 'Coherentie' (7: DropDownList)
        public const int ID_SpecifiekeMentaleFunctieDenkenCoherentie = 553;
        public const string Name_SpecifiekeMentaleFunctieDenkenCoherentie = "SpecifiekeMentaleFunctieDenkenCoherentie";

        // 'Controle' (7: DropDownList)
        public const int ID_SpecifiekeMentaleFunctieDenkenControle = 554;
        public const string Name_SpecifiekeMentaleFunctieDenkenControle = "SpecifiekeMentaleFunctieDenkenControle";

        // 'Inhoud (werkelijkheidszin)' (7: DropDownList)
        public const int ID_SpecifiekeMentaleFunctieDenkenInhoud = 555;
        public const string Name_SpecifiekeMentaleFunctieDenkenInhoud = "SpecifiekeMentaleFunctieDenkenInhoud";

        // 'Tempo (snelheid gedachten wisselen)' (7: DropDownList)
        public const int ID_SpecifiekeMentaleFunctieDenkenTempo = 556;
        public const string Name_SpecifiekeMentaleFunctieDenkenTempo = "SpecifiekeMentaleFunctieDenkenTempo";

        // 'Korte termijn' (7: DropDownList)
        public const int ID_SpecifiekeMentaleFunctieGeheugenKorteTermijn = 557;
        public const string Name_SpecifiekeMentaleFunctieGeheugenKorteTermijn = "SpecifiekeMentaleFunctieGeheugenKorteTermijn";

        // 'Lange termijn' (7: DropDownList)
        public const int ID_SpecifiekeMentaleFunctieGeheugenLangeTermijn = 558;
        public const string Name_SpecifiekeMentaleFunctieGeheugenLangeTermijn = "SpecifiekeMentaleFunctieGeheugenLangeTermijn";

        // 'Adequaatheid' (7: DropDownList)
        public const int ID_SpecifiekeMentaleFunctieStemmingAdequaatheid = 559;
        public const string Name_SpecifiekeMentaleFunctieStemmingAdequaatheid = "SpecifiekeMentaleFunctieStemmingAdequaatheid";

        // 'Regulering' (7: DropDownList)
        public const int ID_SpecifiekeMentaleFunctieStemmingRegulering = 560;
        public const string Name_SpecifiekeMentaleFunctieStemmingRegulering = "SpecifiekeMentaleFunctieStemmingRegulering";

        // 'Systole' (1: String)
        public const int ID_Systole = 561;
        public const string Name_Systole = "Systole";

        // 'Toelichting' (1: String)
        public const int ID_TemperatuurToelichting = 562;
        public const string Name_TemperatuurToelichting = "TemperatuurToelichting";

        // 'Datum plaatsing sonde' (9: Date)
        public const int ID_ToedieningDatumPlaatsingSonde = 563;
        public const string Name_ToedieningDatumPlaatsingSonde = "ToedieningDatumPlaatsingSonde";

        // 'Datum plaatsing infuus' (9: Date)
        public const int ID_ToedieningInfuusDatumPlaatsing = 564;
        public const string Name_ToedieningInfuusDatumPlaatsing = "ToedieningInfuusDatumPlaatsing";

        // 'Locatie infuus' (1: String)
        public const int ID_ToedieningInfuusLocatie = 565;
        public const string Name_ToedieningInfuusLocatie = "ToedieningInfuusLocatie";

        // 'Soort' (7: DropDownList)
        public const int ID_ToedieningInfuusSoort = 566;
        public const string Name_ToedieningInfuusSoort = "ToedieningInfuusSoort";

        // 'Vloeistof (toe te dienen)' (1: String)
        public const int ID_ToedieningInfuusVloeistof = 567;
        public const string Name_ToedieningInfuusVloeistof = "ToedieningInfuusVloeistof";

        // 'Vloeistof dosering (%)' (1: String)
        public const int ID_ToedieningInfuusVloeistofDosering = 568;
        public const string Name_ToedieningInfuusVloeistofDosering = "ToedieningInfuusVloeistofDosering";

        // 'Vloeistof hoeveelheid (l)' (1: String)
        public const int ID_ToedieningInfuusVloeistofHoeveelheid = 569;
        public const string Name_ToedieningInfuusVloeistofHoeveelheid = "ToedieningInfuusVloeistofHoeveelheid";

        // 'Soort sonde' (1: String)
        public const int ID_ToedieningSoortSonde = 570;
        public const string Name_ToedieningSoortSonde = "ToedieningSoortSonde";

        // 'Verpleegkundige interventies' (1: String)
        public const int ID_ToedieningVerpleegkundigeInterventies = 571;
        public const string Name_ToedieningVerpleegkundigeInterventies = "ToedieningVerpleegkundigeInterventies";

        // 'Vloeistof (toe te dienen)' (1: String)
        public const int ID_ToedieningVloeistof = 572;
        public const string Name_ToedieningVloeistof = "ToedieningVloeistof";

        // 'Toelichting beloop' (1: String)
        public const int ID_ToelichtingBeloop = 573;
        public const string Name_ToelichtingBeloop = "ToelichtingBeloop";

        // 'Defecatie (zorgdragen voor)' (7: DropDownList)
        public const int ID_ToiletgangDefecatie = 574;
        public const string Name_ToiletgangDefecatie = "ToiletgangDefecatie";

        // 'Menstruatie (zorgdragen voor)' (7: DropDownList)
        public const int ID_ToiletgangMenstruatie = 575;
        public const string Name_ToiletgangMenstruatie = "ToiletgangMenstruatie";

        // 'Mictie (zorgdragen voor)' (7: DropDownList)
        public const int ID_ToiletgangMictie = 576;
        public const string Name_ToiletgangMictie = "ToiletgangMictie";

        // 'Verpleegkundige interventies' (1: String)
        public const int ID_ToiletgangVerpleegkundigeInterventies = 577;
        public const string Name_ToiletgangVerpleegkundigeInterventies = "ToiletgangVerpleegkundigeInterventies";

        // 'Kleden bovenlichaam' (7: DropDownList)
        public const int ID_UitkledenBovenlichaamBeperking = 578;
        public const string Name_UitkledenBovenlichaamBeperking = "UitkledenBovenlichaamBeperking";

        // 'Kleden onderlichaam' (7: DropDownList)
        public const int ID_UitkledenOnderlichaamBeperking = 579;
        public const string Name_UitkledenOnderlichaamBeperking = "UitkledenOnderlichaamBeperking";

        // 'Verpleegkundige interventies' (1: String)
        public const int ID_UitkledenVerpleegkundigeInterventies = 580;
        public const string Name_UitkledenVerpleegkundigeInterventies = "UitkledenVerpleegkundigeInterventies";

        // 'Datum laatste menstruatie' (9: Date)
        public const int ID_UitscheidingDatumLaatsteMenstruatie = 581;
        public const string Name_UitscheidingDatumLaatsteMenstruatie = "UitscheidingDatumLaatsteMenstruatie";

        // 'Continentie' (7: DropDownList)
        public const int ID_UitscheidingDefecatieContinentie = 582;
        public const string Name_UitscheidingDefecatieContinentie = "UitscheidingDefecatieContinentie";

        // 'Datum laatste defecatie' (9: Date)
        public const int ID_UitscheidingDefecatieDatumLaatste = 583;
        public const string Name_UitscheidingDefecatieDatumLaatste = "UitscheidingDefecatieDatumLaatste";

        // 'Frequentie' (1: String)
        public const int ID_UitscheidingDefecatieFrequentie = 584;
        public const string Name_UitscheidingDefecatieFrequentie = "UitscheidingDefecatieFrequentie";

        // 'Incontinentiemateriaal' (1: String)
        public const int ID_UitscheidingIncontinentiemateriaal = 585;
        public const string Name_UitscheidingIncontinentiemateriaal = "UitscheidingIncontinentiemateriaal";

        // 'Continentie' (7: DropDownList)
        public const int ID_UitscheidingMictieContinentie = 586;
        public const string Name_UitscheidingMictieContinentie = "UitscheidingMictieContinentie";

        // 'Soort incontinentie' (1: String)
        public const int ID_UitscheidingMictieContinentieSoort = 587;
        public const string Name_UitscheidingMictieContinentieSoort = "UitscheidingMictieContinentieSoort";

        // 'Plaats stoma' (1: String)
        public const int ID_UitscheidingStomaPlaats = 588;
        public const string Name_UitscheidingStomaPlaats = "UitscheidingStomaPlaats";

        // 'Stoma sinds' (9: Date)
        public const int ID_UitscheidingStomaSinds = 589;
        public const string Name_UitscheidingStomaSinds = "UitscheidingStomaSinds";

        // 'Soort stoma' (1: String)
        public const int ID_UitscheidingStomaSoort = 590;
        public const string Name_UitscheidingStomaSoort = "UitscheidingStomaSoort";

        // 'Toelichting' (1: String)
        public const int ID_UitscheidingStomaToelichting = 591;
        public const string Name_UitscheidingStomaToelichting = "UitscheidingStomaToelichting";

        // 'Toelichting' (1: String)
        public const int ID_UitscheidingToelichting = 592;
        public const string Name_UitscheidingToelichting = "UitscheidingToelichting";

        // 'Soort katheter' (7: DropDownList)
        public const int ID_UitscheidingUrineKatheterSoort = 593;
        public const string Name_UitscheidingUrineKatheterSoort = "UitscheidingUrineKatheterSoort";

        // 'Toelichting' (1: String)
        public const int ID_UitscheidingUrineKatheterToelichting = 594;
        public const string Name_UitscheidingUrineKatheterToelichting = "UitscheidingUrineKatheterToelichting";

        // 'Urostoma sinds' (9: Date)
        public const int ID_UitscheidingUrostomaSinds = 595;
        public const string Name_UitscheidingUrostomaSinds = "UitscheidingUrostomaSinds";

        // 'Toelichting' (1: String)
        public const int ID_UitscheidingUrostomaToelichting = 596;
        public const string Name_UitscheidingUrostomaToelichting = "UitscheidingUrostomaToelichting";

        // 'Verpleegkundige interventies' (1: String)
        public const int ID_UitscheidingVerpleegkundigeInterventies = 597;
        public const string Name_UitscheidingVerpleegkundigeInterventies = "UitscheidingVerpleegkundigeInterventies";

        // 'Consistentie' (1: String)
        public const int ID_VoedingConsistentie = 598;
        public const string Name_VoedingConsistentie = "VoedingConsistentie";

        // 'Aanwezig' (5: RadioButton)
        public const int ID_VoedingDieetAanwezig = 599;
        public const string Name_VoedingDieetAanwezig = "VoedingDieetAanwezig";

        // 'Soort dieet' (1: String)
        public const int ID_VoedingDieetSoort = 600;
        public const string Name_VoedingDieetSoort = "VoedingDieetSoort";

        // 'Aanwezig' (5: RadioButton)
        public const int ID_VoedingOndervoedingAanwezig = 601;
        public const string Name_VoedingOndervoedingAanwezig = "VoedingOndervoedingAanwezig";

        // 'Meetmethode' (5: RadioButton)
        public const int ID_VoedingOndervoedingAanwezigMeetmethode = 602;
        public const string Name_VoedingOndervoedingAanwezigMeetmethode = "VoedingOndervoedingAanwezigMeetmethode";

        // 'Score' (7: DropDownList)
        public const int ID_VoedingOndervoedingAanwezigScore = 603;
        public const string Name_VoedingOndervoedingAanwezigScore = "VoedingOndervoedingAanwezigScore";

        // 'Sprake van' (5: RadioButton)
        public const int ID_VoedingSprake = 604;
        public const string Name_VoedingSprake = "VoedingSprake";

        // 'Soort' (1: String)
        public const int ID_VoedingSprakeSoort = 605;
        public const string Name_VoedingSprakeSoort = "VoedingSprakeSoort";

        // 'Verpleegkundige interventies' (1: String)
        public const int ID_VoedingVerpleegkundigeInterventies = 606;
        public const string Name_VoedingVerpleegkundigeInterventies = "VoedingVerpleegkundigeInterventies";

        // 'Wassen bovenlichaam' (7: DropDownList)
        public const int ID_WassenBovenlichaamBeperking = 607;
        public const string Name_WassenBovenlichaamBeperking = "WassenBovenlichaamBeperking";

        // 'Wassen gezicht' (7: DropDownList)
        public const int ID_WassenGezichtBeperking = 608;
        public const string Name_WassenGezichtBeperking = "WassenGezichtBeperking";

        // 'Haarverzorging' (7: DropDownList)
        public const int ID_WassenHaarverzorging = 609;
        public const string Name_WassenHaarverzorging = "WassenHaarverzorging";

        // 'Wassen onderlichaam' (7: DropDownList)
        public const int ID_WassenOnderlichaamBeperking = 610;
        public const string Name_WassenOnderlichaamBeperking = "WassenOnderlichaamBeperking";

        // 'Verpleegkundige interventies' (1: String)
        public const int ID_WassenVerpleegkundigeInterventies = 611;
        public const string Name_WassenVerpleegkundigeInterventies = "WassenVerpleegkundigeInterventies";

        // 'Welke aanpassingen nodig' (1: String)
        public const int ID_WelkeAanpassingenNodig = 612;
        public const string Name_WelkeAanpassingenNodig = "WelkeAanpassingenNodig";

        // 'E-mail adres' (1: String)
        public const int ID_WettelijkeVertegenwoordigerEmail = 613;
        public const string Name_WettelijkeVertegenwoordigerEmail = "WettelijkeVertegenwoordigerEmail";

        // 'Persoonnaam' (1: String)
        public const int ID_WettelijkeVertegenwoordigerNaam = 614;
        public const string Name_WettelijkeVertegenwoordigerNaam = "WettelijkeVertegenwoordigerNaam";

        // 'Soort relatie client' (7: DropDownList)
        public const int ID_WettelijkeVertegenwoordigerSoortRelatieClient = 615;
        public const string Name_WettelijkeVertegenwoordigerSoortRelatieClient = "WettelijkeVertegenwoordigerSoortRelatieClient";

        // 'Telefoonnummer' (1: String)
        public const int ID_WettelijkeVertegenwoordigerTelefoonnummer = 616;
        public const string Name_WettelijkeVertegenwoordigerTelefoonnummer = "WettelijkeVertegenwoordigerTelefoonnummer";

        // 'Naasten' (1: String)
        public const int ID_ZiekteBelevingOmgaanMetZiekteprocesNaasten = 617;
        public const string Name_ZiekteBelevingOmgaanMetZiekteprocesNaasten = "ZiekteBelevingOmgaanMetZiekteprocesNaasten";

        // 'Patient' (1: String)
        public const int ID_ZiekteBelevingOmgaanMetZiekteprocesPatient = 618;
        public const string Name_ZiekteBelevingOmgaanMetZiekteprocesPatient = "ZiekteBelevingOmgaanMetZiekteprocesPatient";

        // 'Verpleegkundige interventies' (1: String)
        public const int ID_ZiekteBelevingVerpleegkundigeInterventies = 619;
        public const string Name_ZiekteBelevingVerpleegkundigeInterventies = "ZiekteBelevingVerpleegkundigeInterventies";

        // 'Ziekte-inzicht' (1: String)
        public const int ID_ZiekteBelevingZiekteInzicht = 620;
        public const string Name_ZiekteBelevingZiekteInzicht = "ZiekteBelevingZiekteInzicht";

        // 'Zijn er mede behandelaars' (5: RadioButton)
        public const int ID_ZijnMedeBehandelaars = 621;
        public const string Name_ZijnMedeBehandelaars = "ZijnMedeBehandelaars";

        // 'Stoornis bij horen' (7: DropDownList)
        public const int ID_ZintuigenHoren = 622;
        public const string Name_ZintuigenHoren = "ZintuigenHoren";

        // 'Hulpmiddelen bij gehoor' (7: DropDownList)
        public const int ID_ZintuigenHorenHulpmiddelen = 623;
        public const string Name_ZintuigenHorenHulpmiddelen = "ZintuigenHorenHulpmiddelen";

        // 'Overig (tast , reuk, smaak)' (1: String)
        public const int ID_ZintuigenOverig = 624;
        public const string Name_ZintuigenOverig = "ZintuigenOverig";

        // 'Verpleegkundige interventies' (1: String)
        public const int ID_ZintuigenVerpleegkundigeInterventies = 625;
        public const string Name_ZintuigenVerpleegkundigeInterventies = "ZintuigenVerpleegkundigeInterventies";

        // 'Stoornis bij zien' (7: DropDownList)
        public const int ID_ZintuigenZien = 626;
        public const string Name_ZintuigenZien = "ZintuigenZien";

        // 'Hulpmiddelen bij zien' (7: DropDownList)
        public const int ID_ZintuigenZienHulpmiddelen = 627;
        public const string Name_ZintuigenZienHulpmiddelen = "ZintuigenZienHulpmiddelen";

        // 'Zorgvragen, interventies en resultaten tijdens verblijf/zorg' (1: String)
        public const int ID_Zorgvragen = 628;
        public const string Name_Zorgvragen = "Zorgvragen";

        // 'Zuurstofgebruik' (5: RadioButton)
        public const int ID_Zuurstofgebruik = 629;
        public const string Name_Zuurstofgebruik = "Zuurstofgebruik";

        // 'Toelichting' (1: String)
        public const int ID_ZuurstofgebruikToelichting = 630;
        public const string Name_ZuurstofgebruikToelichting = "ZuurstofgebruikToelichting";

        // 'Locatie' (8: IDText)
        public const int ID_OntvangendeLocatie = 631;
        public const string Name_OntvangendeLocatie = "OntvangendeLocatie";

        // 'Algemeen' (1: String)
        public const int ID_KindAlgemeen = 632;
        public const string Name_KindAlgemeen = "KindAlgemeen";

        // 'Specifieke handelingen' (1: String)
        public const int ID_KindSpecifiekeHandelingen = 633;
        public const string Name_KindSpecifiekeHandelingen = "KindSpecifiekeHandelingen";

        // 'Rol van de mantelzorg, familieleden en anderen' (1: String)
        public const int ID_KindRolMantelzorgFamilieAnderen = 634;
        public const string Name_KindRolMantelzorgFamilieAnderen = "KindRolMantelzorgFamilieAnderen";

        // 'Gedrag' (1: String)
        public const int ID_KindGedrag = 635;
        public const string Name_KindGedrag = "KindGedrag";

        // 'Ontwikkeling' (1: String)
        public const int ID_KindOntwikkeling = 636;
        public const string Name_KindOntwikkeling = "KindOntwikkeling";

        // 'Hobby's, spel en interesses' (1: String)
        public const int ID_KindHobbySpelInteresse = 637;
        public const string Name_KindHobbySpelInteresse = "KindHobbySpelInteresse";

        // 'Hoe beleeft het kind de gevolgen van zijn zorgsituatie' (1: String)
        public const int ID_KindBelevingGevolgenZorgsituatie = 638;
        public const string Name_KindBelevingGevolgenZorgsituatie = "KindBelevingGevolgenZorgsituatie";

        // 'Bijzonderheden' (1: String)
        public const int ID_KindBijzonderheden = 639;
        public const string Name_KindBijzonderheden = "KindBijzonderheden";

        // 'Palliatieve sectie toevoegen in Verpl. Overdracht?' (5: RadioButton)
        public const int ID_GebruikPalliatieveSectie = 640;
        public const string Name_GebruikPalliatieveSectie = "GebruikPalliatieveSectie";

        // 'Reden' (1: String)
        public const int ID_ToestemmingPatientNietMogelijkReden = 641;
        public const string Name_ToestemmingPatientNietMogelijkReden = "ToestemmingPatientNietMogelijkReden";

        // 'Type overdracht formulier' (5: RadioButton)
        public const int ID_VOFormulierType = 642;
        public const string Name_VOFormulierType = "VOFormulierType";

        // 'Verpleegkundige overdracht bijlage' (11: Hidden)
        public const int ID_VOFormulierTransferAttachmentID = 643;
        public const string Name_VOFormulierTransferAttachmentID = "VOFormulierTransferAttachmentID";

        // 'Meegegeven aan' (1: String)
        public const int ID_VOFormulierHandedTo = 644;
        public const string Name_VOFormulierHandedTo = "VOFormulierHandedTo";

        // 'Soort relatie client' (7: DropDownList)
        public const int ID_VOFormulierHandedToSoortRelatieClient = 645;
        public const string Name_VOFormulierHandedToSoortRelatieClient = "VOFormulierHandedToSoortRelatieClient";

        // 'Datum meegegeven' (9: Date)
        public const int ID_VOFormulierHandedToDateTime = 646;
        public const string Name_VOFormulierHandedToDateTime = "VOFormulierHandedToDateTime";

        // 'Bijlagen bij VO' (1: String)
        public const int ID_VOFormulierHandedToBijlagen = 647;
        public const string Name_VOFormulierHandedToBijlagen = "VOFormulierHandedToBijlagen";

        // 'Naam bestand' (1: String)
        public const int ID_VOFormulierTransferFileName = 648;
        public const string Name_VOFormulierTransferFileName = "VOFormulierTransferFileName";

        // 'Datum toegevoegd' (9: Date)
        public const int ID_VOFormulierUploadDate = 649;
        public const string Name_VOFormulierUploadDate = "VOFormulierUploadDate";

        // 'Naam medewerker' (1: String)
        public const int ID_VOFormulierEmployeeName = 650;
        public const string Name_VOFormulierEmployeeName = "VOFormulierEmployeeName";

        // 'Geslachtsnaam' (1: String)
        public const int ID_GeslachtsNaam = 700;
        public const string Name_GeslachtsNaam = "GeslachtsNaam";

        // 'Geslachtsnaam partner' (1: String)
        public const int ID_GeslachtsNaamPartner = 701;
        public const string Name_GeslachtsNaamPartner = "GeslachtsNaamPartner";

        // 'Voorletters' (1: String)
        public const int ID_Voorletters = 702;
        public const string Name_Voorletters = "Voorletters";

        // 'Voornamen' (1: String)
        public const int ID_Voornamen = 703;
        public const string Name_Voornamen = "Voornamen";

        // 'Patientidentificatie' (1: String)
        public const int ID_PatientIdentificatie = 704;
        public const string Name_PatientIdentificatie = "PatientIdentificatie";

        // 'Geslacht' (5: RadioButton)
        public const int ID_Geslacht = 705;
        public const string Name_Geslacht = "Geslacht";

        // 'Geboortedatum' (9: Date)
        public const int ID_Geboortedatum = 706;
        public const string Name_Geboortedatum = "Geboortedatum";

        // 'Telefoonnummer' (1: String)
        public const int ID_Telefoonnummer = 707;
        public const string Name_Telefoonnummer = "Telefoonnummer";

        // 'E-mail adres' (1: String)
        public const int ID_Email = 708;
        public const string Name_Email = "Email";

        // 'Straat' (1: String)
        public const int ID_Straat = 709;
        public const string Name_Straat = "Straat";

        // 'Huisnummer' (1: String)
        public const int ID_Huisnummer = 710;
        public const string Name_Huisnummer = "Huisnummer";

        // 'Postcode' (1: String)
        public const int ID_Postcode = 711;
        public const string Name_Postcode = "Postcode";

        // 'Plaatsnaam' (1: String)
        public const int ID_Plaatsnaam = 712;
        public const string Name_Plaatsnaam = "Plaatsnaam";

        // 'Naam verzekeringsmaatschappij' (7: DropDownList)
        public const int ID_NaamVerzekeringmaatschappij = 713;
        public const string Name_NaamVerzekeringmaatschappij = "NaamVerzekeringmaatschappij";

        // 'Polisnummer' (1: String)
        public const int ID_Polisnummer = 714;
        public const string Name_Polisnummer = "Polisnummer";

        // 'Persoonnaam' (1: String)
        public const int ID_Contactpersoonnaam = 715;
        public const string Name_Contactpersoonnaam = "Contactpersoonnaam";

        // 'Type contactpersoon' (7: DropDownList)
        public const int ID_Contactpersoontype = 716;
        public const string Name_Contactpersoontype = "Contactpersoontype";

        // 'Telefoonnummer' (1: String)
        public const int ID_Contactpersoontelefoonnummer = 717;
        public const string Name_Contactpersoontelefoonnummer = "Contactpersoontelefoonnummer";

        // 'E-mail adres' (1: String)
        public const int ID_Contactpersoonemail = 718;
        public const string Name_Contactpersoonemail = "Contactpersoonemail";

        // 'Naam instelling' (8: IDText)
        public const int ID_NaamInstelling = 719;
        public const string Name_NaamInstelling = "NaamInstelling";

        // 'Locatie' (8: IDText)
        public const int ID_OrganisatieLocatie = 720;
        public const string Name_OrganisatieLocatie = "OrganisatieLocatie";

        // 'Afdeling' (8: IDText)
        public const int ID_OrganisatieAfdeling = 721;
        public const string Name_OrganisatieAfdeling = "OrganisatieAfdeling";

        // 'Naam instelling' (8: IDText)
        public const int ID_OntvangendeNaam = 722;
        public const string Name_OntvangendeNaam = "OntvangendeNaam";

        // 'Gewenste ingangsdatum zorg' (9: Date)
        public const int ID_GewensteIngangsdatum = 723;
        public const string Name_GewensteIngangsdatum = "GewensteIngangsdatum";

        // 'Mantelzorg' (1: String)
        public const int ID_Mantelzorg = 724;
        public const string Name_Mantelzorg = "Mantelzorg";

        // 'Gewenste zorg' (1: String)
        public const int ID_GewensteZorg = 725;
        public const string Name_GewensteZorg = "GewensteZorg";

        // 'Frequentie' (1: String)
        public const int ID_GewensteFrequentie = 726;
        public const string Name_GewensteFrequentie = "GewensteFrequentie";

        // 'Afwijkend afleveradres' (1: String)
        public const int ID_AfwijkendLeveringAdres = 727;
        public const string Name_AfwijkendLeveringAdres = "AfwijkendLeveringAdres";

        // 'Medisch' (1: String)
        public const int ID_InventarisatieMedisch = 728;
        public const string Name_InventarisatieMedisch = "InventarisatieMedisch";

        // 'Sociaal' (1: String)
        public const int ID_InventarisatieSociaal = 729;
        public const string Name_InventarisatieSociaal = "InventarisatieSociaal";

        // 'Ontwikkeling' (1: String)
        public const int ID_InventarisatieOntwikkeling = 730;
        public const string Name_InventarisatieOntwikkeling = "InventarisatieOntwikkeling";

        // 'Veiligheid' (1: String)
        public const int ID_InventarisatieVeiligheid = 731;
        public const string Name_InventarisatieVeiligheid = "InventarisatieVeiligheid";

        // 'Opmerking vanuit afd.vpk' (1: String)
        public const int ID_InventarisatieOpmerkingAfdVPK = 732;
        public const string Name_InventarisatieOpmerkingAfdVPK = "InventarisatieOpmerkingAfdVPK";

        // 'Medische zorg na ontslag' (5: RadioButton)
        public const int ID_MedischeZorgNaOntslag = 733;
        public const string Name_MedischeZorgNaOntslag = "MedischeZorgNaOntslag";

        // 'Levensverwachting' (5: RadioButton)
        public const int ID_LevensVerwachting = 734;
        public const string Name_LevensVerwachting = "LevensVerwachting";

        // 'Slecht nieuwsgesprek (kopieer verslag)' (1: String)
        public const int ID_SlechtNieuwsGesprekVerslag = 735;
        public const string Name_SlechtNieuwsGesprekVerslag = "SlechtNieuwsGesprekVerslag";

        // 'Voorkeursplaats van sterven' (1: String)
        public const int ID_VoorkeursPlaatsSterven = 736;
        public const string Name_VoorkeursPlaatsSterven = "VoorkeursPlaatsSterven";

        // 'Nog op ziekte gerichte behandelingen mogelijk?' (5: RadioButton)
        public const int ID_OpZiekteGerichteBehandelingenMogelijk = 737;
        public const string Name_OpZiekteGerichteBehandelingenMogelijk = "OpZiekteGerichteBehandelingenMogelijk";

        // 'Op ziekte gerichte behandelingen toelichting' (1: String)
        public const int ID_OpZiekteGerichteBehandelingenAfspraken = 738;
        public const string Name_OpZiekteGerichteBehandelingenAfspraken = "OpZiekteGerichteBehandelingenAfspraken";

        // 'Wensen patiënt / naasten t.a.v. ziektegerichte behandeling' (5: RadioButton)
        public const int ID_WensenPatientNaastenBehandelingEnZiekte = 739;
        public const string Name_WensenPatientNaastenBehandelingEnZiekte = "WensenPatientNaastenBehandelingEnZiekte";

        // 'Wensen patiënt / naasten t.a.v. ziektegerichte behandeling toelichting' (1: String)
        public const int ID_WensenPatientNaastenBehandelingEnZiekteAfspraken = 740;
        public const string Name_WensenPatientNaastenBehandelingEnZiekteAfspraken = "WensenPatientNaastenBehandelingEnZiekteAfspraken";

        // 'Het reanimatiebeleid' (5: RadioButton)
        public const int ID_BesprokenReanimatiebeleid = 741;
        public const string Name_BesprokenReanimatiebeleid = "BesprokenReanimatiebeleid";

        // 'Reanimatiebeleid toelichting' (1: String)
        public const int ID_BesprokenReanimatiebeleidToelichting = 742;
        public const string Name_BesprokenReanimatiebeleidToelichting = "BesprokenReanimatiebeleidToelichting";

        // 'Palliatieve sedatie' (5: RadioButton)
        public const int ID_BesprokenPalliatieveSedatie = 743;
        public const string Name_BesprokenPalliatieveSedatie = "BesprokenPalliatieveSedatie";

        // 'Palliatieve sedatie toelichting' (1: String)
        public const int ID_BesprokenPalliatieveSedatieToelichting = 744;
        public const string Name_BesprokenPalliatieveSedatieToelichting = "BesprokenPalliatieveSedatieToelichting";

        // 'Euthanasie' (5: RadioButton)
        public const int ID_BesprokenEuthenasie = 745;
        public const string Name_BesprokenEuthenasie = "BesprokenEuthenasie";

        // 'Euthanasie toelichting' (1: String)
        public const int ID_BesprokenEuthenasieToelichting = 746;
        public const string Name_BesprokenEuthenasieToelichting = "BesprokenEuthenasieToelichting";

        // 'Afbouwen/Stoppen met sonde en/of parenterale voeding' (5: RadioButton)
        public const int ID_BesprokenStoppenSondeParenteraleVoeding = 747;
        public const string Name_BesprokenStoppenSondeParenteraleVoeding = "BesprokenStoppenSondeParenteraleVoeding";

        // 'Afbouwen/Stoppen met sonde en/of parenterale voeding toelichting' (1: String)
        public const int ID_BesprokenStoppenSondeParenteraleVoedingToelichting = 748;
        public const string Name_BesprokenStoppenSondeParenteraleVoedingToelichting = "BesprokenStoppenSondeParenteraleVoedingToelichting";

        // 'Stoppen met vochttoediening via infuus' (5: RadioButton)
        public const int ID_BesprokenStoppenParenteraleVochttoediening = 749;
        public const string Name_BesprokenStoppenParenteraleVochttoediening = "BesprokenStoppenParenteraleVochttoediening";

        // 'Stoppen met parenterale vochttoediening toelichting' (1: String)
        public const int ID_BesprokenStoppenParenteraleVochttoedieningToelichting = 750;
        public const string Name_BesprokenStoppenParenteraleVochttoedieningToelichting = "BesprokenStoppenParenteraleVochttoedieningToelichting";

        // 'Toedienen bloedproducten' (5: RadioButton)
        public const int ID_BesprokenToedieningBloedtransfusie = 751;
        public const string Name_BesprokenToedieningBloedtransfusie = "BesprokenToedieningBloedtransfusie";

        // 'Toedienen bloedproducten toelichting' (1: String)
        public const int ID_BesprokenToedieningBloedtransfusieToelichting = 752;
        public const string Name_BesprokenToedieningBloedtransfusieToelichting = "BesprokenToedieningBloedtransfusieToelichting";

        // 'Toedienen antibiotica' (5: RadioButton)
        public const int ID_BesprokenToedieningAntibiotica = 753;
        public const string Name_BesprokenToedieningAntibiotica = "BesprokenToedieningAntibiotica";

        // 'Toedienen antibiotica toelichting' (1: String)
        public const int ID_BesprokenToedieningAntibioticaToelichting = 754;
        public const string Name_BesprokenToedieningAntibioticaToelichting = "BesprokenToedieningAntibioticaToelichting";

        // 'Ziekenhuisopname' (5: RadioButton)
        public const int ID_BesprokenWensZHopnameVsNietOpnemenZHOfSEH = 755;
        public const string Name_BesprokenWensZHopnameVsNietOpnemenZHOfSEH = "BesprokenWensZHopnameVsNietOpnemenZHOfSEH";

        // 'Ziekenhuisopname toelichting' (1: String)
        public const int ID_BesprokenWensZHopnameVsNietOpnemenZHOfSEHToelichting = 756;
        public const string Name_BesprokenWensZHopnameVsNietOpnemenZHOfSEHToelichting = "BesprokenWensZHopnameVsNietOpnemenZHOfSEHToelichting";

        // 'Uitzetten van de ICD' (5: RadioButton)
        public const int ID_BesprokenUitzettenICDOfPacemaker = 757;
        public const string Name_BesprokenUitzettenICDOfPacemaker = "BesprokenUitzettenICDOfPacemaker";

        // 'Uitzetten van de ICD toelichting' (1: String)
        public const int ID_BesprokenUitzettenICDOfPacemakerToelichting = 758;
        public const string Name_BesprokenUitzettenICDOfPacemakerToelichting = "BesprokenUitzettenICDOfPacemakerToelichting";

        // 'Uitvoering van diagnostisch onderzoek' (5: RadioButton)
        public const int ID_BesprokenUitvoerenDiagnostischOnderzoek = 759;
        public const string Name_BesprokenUitvoerenDiagnostischOnderzoek = "BesprokenUitvoerenDiagnostischOnderzoek";

        // 'Uitvoering van diagnostisch onderzoek toelichting' (1: String)
        public const int ID_BesprokenUitvoerenDiagnostischOnderzoekToelichting = 760;
        public const string Name_BesprokenUitvoerenDiagnostischOnderzoekToelichting = "BesprokenUitvoerenDiagnostischOnderzoekToelichting";

        // 'Benauwdheid toelichting' (1: String)
        public const int ID_VerwachtingBenauwdheidToelichting = 761;
        public const string Name_VerwachtingBenauwdheidToelichting = "VerwachtingBenauwdheidToelichting";

        // 'Koorts toelichting' (1: String)
        public const int ID_VerwachtingKoortsToelichting = 762;
        public const string Name_VerwachtingKoortsToelichting = "VerwachtingKoortsToelichting";

        // 'Levensbedreigende bloeding toelichting' (1: String)
        public const int ID_VerwachtingBloedingToelichting = 763;
        public const string Name_VerwachtingBloedingToelichting = "VerwachtingBloedingToelichting";

        // 'Insulten toelichting' (1: String)
        public const int ID_VerwachtingInsultenToelichting = 764;
        public const string Name_VerwachtingInsultenToelichting = "VerwachtingInsultenToelichting";

        // 'Laag HB / lage trombo's' (1: String)
        public const int ID_VerwachtingLaagHBTrombo = 765;
        public const string Name_VerwachtingLaagHBTrombo = "VerwachtingLaagHBTrombo";

        // 'Ascites toelichting' (1: String)
        public const int ID_VerwachtingAscitesToelichting = 766;
        public const string Name_VerwachtingAscitesToelichting = "VerwachtingAscitesToelichting";

        // 'Pleuravocht toelichting' (1: String)
        public const int ID_VerwachtingPleuravochtToelichting = 767;
        public const string Name_VerwachtingPleuravochtToelichting = "VerwachtingPleuravochtToelichting";

        // 'Toename van (invullen klachten)' (1: String)
        public const int ID_VerwachtingToenameOverig = 768;
        public const string Name_VerwachtingToenameOverig = "VerwachtingToenameOverig";

        // 'Anders toelichting' (1: String)
        public const int ID_VerwachtingAndersToelichting = 769;
        public const string Name_VerwachtingAndersToelichting = "VerwachtingAndersToelichting";

        // 'Obstipatie toelichting' (1: String)
        public const int ID_VerwachtingObstipatieToelichting = 770;
        public const string Name_VerwachtingObstipatieToelichting = "VerwachtingObstipatieToelichting";

        // 'Misselijkheid toelichting' (1: String)
        public const int ID_VerwachtingMisselijkheidToelichting = 771;
        public const string Name_VerwachtingMisselijkheidToelichting = "VerwachtingMisselijkheidToelichting";

        // 'Obstipatie' (5: RadioButton)
        public const int ID_VerwachtingObstipatie = 772;
        public const string Name_VerwachtingObstipatie = "VerwachtingObstipatie";

        // 'Misselijkheid' (5: RadioButton)
        public const int ID_VerwachtingMisselijkheid = 773;
        public const string Name_VerwachtingMisselijkheid = "VerwachtingMisselijkheid";

        // 'Benauwdheid' (5: RadioButton)
        public const int ID_VerwachtingBenauwdheid = 774;
        public const string Name_VerwachtingBenauwdheid = "VerwachtingBenauwdheid";

        // 'Koorts' (5: RadioButton)
        public const int ID_VerwachtingKoorts = 775;
        public const string Name_VerwachtingKoorts = "VerwachtingKoorts";

        // 'Levensbedreigende bloeding' (5: RadioButton)
        public const int ID_VerwachtingBloeding = 776;
        public const string Name_VerwachtingBloeding = "VerwachtingBloeding";

        // 'Insulten' (5: RadioButton)
        public const int ID_VerwachtingInsulten = 777;
        public const string Name_VerwachtingInsulten = "VerwachtingInsulten";

        // 'Ascites' (5: RadioButton)
        public const int ID_VerwachtingAscites = 778;
        public const string Name_VerwachtingAscites = "VerwachtingAscites";

        // 'Pleuravocht' (5: RadioButton)
        public const int ID_VerwachtingPleuravocht = 779;
        public const string Name_VerwachtingPleuravocht = "VerwachtingPleuravocht";

        // 'Anders' (5: RadioButton)
        public const int ID_VerwachtingAnders = 780;
        public const string Name_VerwachtingAnders = "VerwachtingAnders";

        // 'Is er sprake van palliatieve zorg?' (5: RadioButton)
        public const int ID_SprakeVanPalliatieveZorg = 781;
        public const string Name_SprakeVanPalliatieveZorg = "SprakeVanPalliatieveZorg";

        // 'Benodigde hulpmiddelen' (1: String)
        public const int ID_BenodigdeHulpmiddelen_1 = 803;
        public const string Name_BenodigdeHulpmiddelen_1 = "BenodigdeHulpmiddelen_1";

        // 'Evt complicaties, bijzonderheden, bijwerkingen' (1: String)
        public const int ID_EvtComplicatiesBijzonderheden_1 = 804;
        public const string Name_EvtComplicatiesBijzonderheden_1 = "EvtComplicatiesBijzonderheden_1";

        // 'Startdatum' (9: Date)
        public const int ID_VerwachteDuurStartdatum = 805;
        public const string Name_VerwachteDuurStartdatum = "VerwachteDuurStartdatum";

        // 'Einddatum' (9: Date)
        public const int ID_VerwachteDuurEinddatum = 806;
        public const string Name_VerwachteDuurEinddatum = "VerwachteDuurEinddatum";

        // 'Datum eerstvolgend polibezoek' (9: Date)
        public const int ID_DatumEerstVolgendPolibezoek = 807;
        public const string Name_DatumEerstVolgendPolibezoek = "DatumEerstVolgendPolibezoek";

        // 'Extra informatie' (1: String)
        public const int ID_ExtraInfo2eSpecialist = 808;
        public const string Name_ExtraInfo2eSpecialist = "ExtraInfo2eSpecialist";

        // 'Diagnose of indicatie voor de handeling' (1: String)
        public const int ID_MedischeSituatieRedenOpnameMSVT = 809;
        public const string Name_MedischeSituatieRedenOpnameMSVT = "MedischeSituatieRedenOpnameMSVT";

        // 'Aflevering' (5: RadioButton)
        public const int ID_NoodzakelijkTeRegelenHulpmiddelenAflevering = 810;
        public const string Name_NoodzakelijkTeRegelenHulpmiddelenAflevering = "NoodzakelijkTeRegelenHulpmiddelenAflevering";

        // '' (5: RadioButton)
        public const int ID_OudersEnKindDiagnose = 811;
        public const string Name_OudersEnKindDiagnose = "OudersEnKindDiagnose";

        // 'Verwoording ouders van gestelde diagnose bij kind' (1: String)
        public const int ID_OudersEnKindDiagnoseWelke = 812;
        public const string Name_OudersEnKindDiagnoseWelke = "OudersEnKindDiagnoseWelke";

        // '' (5: RadioButton)
        public const int ID_OudersEnKindMeerInformatie = 813;
        public const string Name_OudersEnKindMeerInformatie = "OudersEnKindMeerInformatie";

        // 'Behoefte van ouders aan meer informatie' (1: String)
        public const int ID_OudersEnKindMeerInformatieWaarover = 814;
        public const string Name_OudersEnKindMeerInformatieWaarover = "OudersEnKindMeerInformatieWaarover";

        // 'Kinderverpleegkundige handelingen die ouders/kind denken nodig te hebben buiten ziekenhuis ' (1: String)
        public const int ID_OudersEnKindHandelingenBuitenHetZiekenhuis = 815;
        public const string Name_OudersEnKindHandelingenBuitenHetZiekenhuis = "OudersEnKindHandelingenBuitenHetZiekenhuis";

        // 'Mate waarin zij (of andere gezinsleden) deze handelingen zelf kunnen uitvoeren of bereid zijn deze aan te leren ' (1: String)
        public const int ID_OudersEnKindHandelingenBuitenHetZiekenhuisZelf = 816;
        public const string Name_OudersEnKindHandelingenBuitenHetZiekenhuisZelf = "OudersEnKindHandelingenBuitenHetZiekenhuisZelf";

        // 'Moment waarop ouders ondersteuning of overname van zorg  denken nodig te hebben bij bepaalde kinderverpleegkundige handelingen ' (1: String)
        public const int ID_OudersEnKindHandelingenBuitenHetZiekenhuisVanaf = 817;
        public const string Name_OudersEnKindHandelingenBuitenHetZiekenhuisVanaf = "OudersEnKindHandelingenBuitenHetZiekenhuisVanaf";

        // 'Behoefte aan hulpmiddelen van ouders/kind' (1: String)
        public const int ID_OudersEnKindHulpmiddelen = 818;
        public const string Name_OudersEnKindHulpmiddelen = "OudersEnKindHulpmiddelen";

        // 'Behoefte aan ondersteuning bij persoonlijke verzorging kind' (1: String)
        public const int ID_OudersEnKindOndersteuningPersoonlijkeVerzorging = 819;
        public const string Name_OudersEnKindOndersteuningPersoonlijkeVerzorging = "OudersEnKindOndersteuningPersoonlijkeVerzorging";

        // 'Organisatie zorg als kind gezond zou zijn' (1: String)
        public const int ID_OudersEnKindZorgEnOpvang = 820;
        public const string Name_OudersEnKindZorgEnOpvang = "OudersEnKindZorgEnOpvang";

        // 'Verblijfplaats kind anders dan thuis' (1: String)
        public const int ID_OudersEnKindVerblijf = 821;
        public const string Name_OudersEnKindVerblijf = "OudersEnKindVerblijf";

        // 'Kinderverpleegkundige handelingen die volgens ouders/kind dan nodig zijn' (1: String)
        public const int ID_OudersEnKindVerblijfHandelingenNodig = 822;
        public const string Name_OudersEnKindVerblijfHandelingenNodig = "OudersEnKindVerblijfHandelingenNodig";

        // 'Motorische/lichamelijke ontwikkeling' (1: String)
        public const int ID_OudersEnKindOntwikkelingMotorische = 823;
        public const string Name_OudersEnKindOntwikkelingMotorische = "OudersEnKindOntwikkelingMotorische";

        // 'Spraak / taal ontwikkeling' (1: String)
        public const int ID_OudersEnKindOntwikkelingSpraak = 824;
        public const string Name_OudersEnKindOntwikkelingSpraak = "OudersEnKindOntwikkelingSpraak";

        // 'Cognitieve ontwikkeling' (1: String)
        public const int ID_OudersEnKindOntwikkelingCognitieve = 825;
        public const string Name_OudersEnKindOntwikkelingCognitieve = "OudersEnKindOntwikkelingCognitieve";

        // 'Sociaal emotionele ontwikkeling' (1: String)
        public const int ID_OudersEnKindOntwikkelingSociaalEmotionele = 826;
        public const string Name_OudersEnKindOntwikkelingSociaalEmotionele = "OudersEnKindOntwikkelingSociaalEmotionele";

        // 'Uitgesproken gedrag' (1: String)
        public const int ID_OudersEnKindOntwikkelingUitgesprokenGedrag = 827;
        public const string Name_OudersEnKindOntwikkelingUitgesprokenGedrag = "OudersEnKindOntwikkelingUitgesprokenGedrag";

        // 'Professionele ondersteuning ingezet op het gebied van ontwikkeling' (1: String)
        public const int ID_OudersEnKindOntwikkelingProfessioneleOndersteuning = 828;
        public const string Name_OudersEnKindOntwikkelingProfessioneleOndersteuning = "OudersEnKindOntwikkelingProfessioneleOndersteuning";

        // 'Gevolgen aandoening/ziekte en bijbehorende zorg voor  sociale emotionele ontwikkeling van kind en broers/zussen ' (1: String)
        public const int ID_OudersEnKindOntwikkelingGevolgen = 829;
        public const string Name_OudersEnKindOntwikkelingGevolgen = "OudersEnKindOntwikkelingGevolgen";

        // 'Gevolgen ziekte kind voor eigen (on)betaalde werkzaamheden' (1: String)
        public const int ID_OudersEnKindSociaalWerkzaamheden = 830;
        public const string Name_OudersEnKindSociaalWerkzaamheden = "OudersEnKindSociaalWerkzaamheden";

        // 'Inzet van het sociale netwerk van ouders' (1: String)
        public const int ID_OudersEnKindSociaalSocialeNetwerk = 831;
        public const string Name_OudersEnKindSociaalSocialeNetwerk = "OudersEnKindSociaalSocialeNetwerk";

        // 'Personen waarop ouders/kind een beroep kunnen doen voor ondersteuning gezin' (1: String)
        public const int ID_OudersEnKindSociaalOndersteunen = 832;
        public const string Name_OudersEnKindSociaalOndersteunen = "OudersEnKindSociaalOndersteunen";

        // 'Cijfer van ouders voor hun draagkracht op dit moment' (4: Number)
        public const int ID_OudersEnKindSociaalDraagkracht = 833;
        public const string Name_OudersEnKindSociaalDraagkracht = "OudersEnKindSociaalDraagkracht";

        // 'Cijfer van ouders voor hun draaglast op dit moment' (4: Number)
        public const int ID_OudersEnKindSociaalDraaglast = 834;
        public const string Name_OudersEnKindSociaalDraaglast = "OudersEnKindSociaalDraaglast";

        // 'Mate waarin zorg voor het kind in het ouderlijk huis/woonomgeving veilig kan worden  uitgevoerd ' (1: String)
        public const int ID_OudersEnKindVeiligheidZorg = 835;
        public const string Name_OudersEnKindVeiligheidZorg = "OudersEnKindVeiligheidZorg";

        // 'Invloed staat van woning op de gezondheid kind' (1: String)
        public const int ID_OudersEnKindVeiligheidGezondheid = 836;
        public const string Name_OudersEnKindVeiligheidGezondheid = "OudersEnKindVeiligheidGezondheid";

        // 'Andere betrokken instanties waarmee contact is geweest' (1: String)
        public const int ID_OudersEnKindVeiligheidContactMetAndereBetrokkenInstanties = 837;
        public const string Name_OudersEnKindVeiligheidContactMetAndereBetrokkenInstanties = "OudersEnKindVeiligheidContactMetAndereBetrokkenInstanties";

        // 'Wens van ouders/kind om met iemand te praten over de situatie' (1: String)
        public const int ID_OudersEnKindVeiligheidPratenOverDeSituatie = 838;
        public const string Name_OudersEnKindVeiligheidPratenOverDeSituatie = "OudersEnKindVeiligheidPratenOverDeSituatie";

        // 'Naar huis' (7: DropDownList)
        public const int ID_ExtramuraalTyperingNazorgTP = 839;
        public const string Name_ExtramuraalTyperingNazorgTP = "ExtramuraalTyperingNazorgTP";

        // 'Zorginstelling voor' (7: DropDownList)
        public const int ID_IntramuraalTyperingNazorgTP = 840;
        public const string Name_IntramuraalTyperingNazorgTP = "IntramuraalTyperingNazorgTP";

        // 'Typering nazorg TP' (5: RadioButton)
        public const int ID_TyperingNazorgTP = 841;
        public const string Name_TyperingNazorgTP = "TyperingNazorgTP";

        // 'Naar huis' (8: IDText)
        public const int ID_ExtramuraalTyperingNazorgRequest = 842;
        public const string Name_ExtramuraalTyperingNazorgRequest = "ExtramuraalTyperingNazorgRequest";

        // 'Zorginstelling voor' (8: IDText)
        public const int ID_IntramuraalTyperingNazorgRequest = 843;
        public const string Name_IntramuraalTyperingNazorgRequest = "IntramuraalTyperingNazorgRequest";

        // 'Typering nazorg TP' (7: DropDownList)
        public const int ID_AfterCareCategoryDefinitiveDDL = 844;
        public const string Name_AfterCareCategoryDefinitiveDDL = "AfterCareCategoryDefinitiveDDL";

        // 'Bestaande zorg' (7: DropDownList)
        public const int ID_AdjustmentStandingCareDDL = 845;
        public const string Name_AdjustmentStandingCareDDL = "AdjustmentStandingCareDDL";

        // 'Naam' (8: IDText)
        public const int ID_ZorginzetVoorDezeOpnameTypeZorginstellingNaam = 900;
        public const string Name_ZorginzetVoorDezeOpnameTypeZorginstellingNaam = "ZorginzetVoorDezeOpnameTypeZorginstellingNaam";

        // 'Aanvraag type' (8: IDText)
        public const int ID_CloseTransferRequestFormType = 1000;
        public const string Name_CloseTransferRequestFormType = "CloseTransferRequestFormType";

        // '' (1: String)
        public const int ID_FilledOutBy = 1050;
        public const string Name_FilledOutBy = "FilledOutBy";

        // 'Typering nazorg aanvraag' (5: RadioButton)
        public const int ID_AfterCareCategory = 1051;
        public const string Name_AfterCareCategory = "AfterCareCategory";

        // 'Nazorg type' (7: DropDownList)
        public const int ID_AfterCare = 1052;
        public const string Name_AfterCare = "AfterCare";

        // 'Nazorg type' (7: DropDownList)
        public const int ID_AfterCareDefinitive = 1053;
        public const string Name_AfterCareDefinitive = "AfterCareDefinitive";

        // 'Typering nazorg definitief' (5: RadioButton)
        public const int ID_AfterCareCategoryDefinitive = 1054;
        public const string Name_AfterCareCategoryDefinitive = "AfterCareCategoryDefinitive";

        // 'Datum indicatie/triage/afweging' (9: Date)
        public const int ID_DateIndicationDecision = 1100;
        public const string Name_DateIndicationDecision = "DateIndicationDecision";

        // 'Indicatie gesteld door' (7: DropDownList)
        public const int ID_IndicationGivenBy = 1101;
        public const string Name_IndicationGivenBy = "IndicationGivenBy";

        // 'Functie' (5: RadioButton)
        public const int ID_EmployeeIIFunction = 1102;
        public const string Name_EmployeeIIFunction = "EmployeeIIFunction";

        // 'Anders, nl.' (1: String)
        public const int ID_EmployeeIIFunctionOther = 1103;
        public const string Name_EmployeeIIFunctionOther = "EmployeeIIFunctionOther";

        // 'Nieuw' (6: Checkbox)
        public const int ID_IndicationsForHomecareNew = 1104;
        public const string Name_IndicationsForHomecareNew = "IndicationsForHomecareNew";

        // 'Uitbreiding' (6: Checkbox)
        public const int ID_IndicationsForHomecareExpansion = 1105;
        public const string Name_IndicationsForHomecareExpansion = "IndicationsForHomecareExpansion";

        // '' (6: Checkbox)
        public const int ID_IndicationsForHomecareNewAttachment = 1106;
        public const string Name_IndicationsForHomecareNewAttachment = "IndicationsForHomecareNewAttachment";

        // '' (6: Checkbox)
        public const int ID_IndicationsForHomecareExpansionAttachment = 1107;
        public const string Name_IndicationsForHomecareExpansionAttachment = "IndicationsForHomecareExpansionAttachment";

        // 'Geldig van' (9: Date)
        public const int ID_HomecareNewFrom = 1108;
        public const string Name_HomecareNewFrom = "HomecareNewFrom";

        // 'Geldig tot' (9: Date)
        public const int ID_HomecareNewUntill = 1109;
        public const string Name_HomecareNewUntill = "HomecareNewUntill";

        // 'Geldig van' (9: Date)
        public const int ID_HomecareExpansionFrom = 1110;
        public const string Name_HomecareExpansionFrom = "HomecareExpansionFrom";

        // 'Geldig tot' (9: Date)
        public const int ID_HomecareExpansionUntill = 1111;
        public const string Name_HomecareExpansionUntill = "HomecareExpansionUntill";

        // 'Basis' (6: Checkbox)
        public const int ID_PrimaryStayBasis = 1112;
        public const string Name_PrimaryStayBasis = "PrimaryStayBasis";

        // '' (6: Checkbox)
        public const int ID_PrimaryStayBasisAttachment = 1113;
        public const string Name_PrimaryStayBasisAttachment = "PrimaryStayBasisAttachment";

        // 'Geldig van' (9: Date)
        public const int ID_PrimaryStayBasisFrom = 1114;
        public const string Name_PrimaryStayBasisFrom = "PrimaryStayBasisFrom";

        // 'Geldig tot' (9: Date)
        public const int ID_PrimaryStayBasisUntill = 1115;
        public const string Name_PrimaryStayBasisUntill = "PrimaryStayBasisUntill";

        // 'Intensief SOM' (6: Checkbox)
        public const int ID_PrimaryStaySom = 1116;
        public const string Name_PrimaryStaySom = "PrimaryStaySom";

        // '' (6: Checkbox)
        public const int ID_PrimaryStaySomAttachment = 1117;
        public const string Name_PrimaryStaySomAttachment = "PrimaryStaySomAttachment";

        // 'Geldig van' (9: Date)
        public const int ID_PrimaryStaySomFrom = 1118;
        public const string Name_PrimaryStaySomFrom = "PrimaryStaySomFrom";

        // 'Geldig tot' (9: Date)
        public const int ID_PrimaryStaySomUntill = 1119;
        public const string Name_PrimaryStaySomUntill = "PrimaryStaySomUntill";

        // 'Intensief PG' (6: Checkbox)
        public const int ID_PrimaryStayPG = 1120;
        public const string Name_PrimaryStayPG = "PrimaryStayPG";

        // '' (6: Checkbox)
        public const int ID_PrimaryStayPGAttachment = 1121;
        public const string Name_PrimaryStayPGAttachment = "PrimaryStayPGAttachment";

        // 'Geldig van' (9: Date)
        public const int ID_PrimaryStayPGFrom = 1122;
        public const string Name_PrimaryStayPGFrom = "PrimaryStayPGFrom";

        // 'Geldig tot' (9: Date)
        public const int ID_PrimaryStayPGUntill = 1123;
        public const string Name_PrimaryStayPGUntill = "PrimaryStayPGUntill";

        // 'Palliatief' (6: Checkbox)
        public const int ID_PrimaryStayPalliative = 1124;
        public const string Name_PrimaryStayPalliative = "PrimaryStayPalliative";

        // '' (6: Checkbox)
        public const int ID_PrimaryStayPalliativeAttachment = 1125;
        public const string Name_PrimaryStayPalliativeAttachment = "PrimaryStayPalliativeAttachment";

        // 'Geldig van' (9: Date)
        public const int ID_PrimaryStayPalliativeFrom = 1126;
        public const string Name_PrimaryStayPalliativeFrom = "PrimaryStayPalliativeFrom";

        // 'Geldig tot' (9: Date)
        public const int ID_PrimaryStayPalliativeUntill = 1127;
        public const string Name_PrimaryStayPalliativeUntill = "PrimaryStayPalliativeUntill";

        // 'Zorgprofiel' (6: Checkbox)
        public const int ID_WlzProfiel = 1128;
        public const string Name_WlzProfiel = "WlzProfiel";

        // '' (6: Checkbox)
        public const int ID_WlzProfielAttachment = 1129;
        public const string Name_WlzProfielAttachment = "WlzProfielAttachment";

        // 'Geldig van' (9: Date)
        public const int ID_WlzProfielFrom = 1130;
        public const string Name_WlzProfielFrom = "WlzProfielFrom";

        // 'Geldig tot' (9: Date)
        public const int ID_WlzProfielUntill = 1131;
        public const string Name_WlzProfielUntill = "WlzProfielUntill";

        // '' (1: String)
        public const int ID_WlzProfielOpmerkingen = 1132;
        public const string Name_WlzProfielOpmerkingen = "WlzProfielOpmerkingen";

        // 'Anders' (6: Checkbox)
        public const int ID_WlzAnders = 1133;
        public const string Name_WlzAnders = "WlzAnders";

        // '' (6: Checkbox)
        public const int ID_WlzAndersAttachment = 1134;
        public const string Name_WlzAndersAttachment = "WlzAndersAttachment";

        // 'Geldig van' (9: Date)
        public const int ID_WlzAndersFrom = 1135;
        public const string Name_WlzAndersFrom = "WlzAndersFrom";

        // 'Geldig tot' (9: Date)
        public const int ID_WlzAndersUntill = 1136;
        public const string Name_WlzAndersUntill = "WlzAndersUntill";

        // '' (1: String)
        public const int ID_WlzAndersOpmerkingen = 1137;
        public const string Name_WlzAndersOpmerkingen = "WlzAndersOpmerkingen";

        // 'Op basis van art. 2' (6: Checkbox)
        public const int ID_BOPZobvArt2 = 1138;
        public const string Name_BOPZobvArt2 = "BOPZobvArt2";

        // 'Op basis van art. 60' (6: Checkbox)
        public const int ID_BOPZobvArt60 = 1139;
        public const string Name_BOPZobvArt60 = "BOPZobvArt60";

        // 'Datum' (9: Date)
        public const int ID_BOPZDate = 1140;
        public const string Name_BOPZDate = "BOPZDate";

        // 'Opmerkingen' (1: String)
        public const int ID_GRZOtherText = 1141;
        public const string Name_GRZOtherText = "GRZOtherText";

        // 'Som' (6: Checkbox)
        public const int ID_WlzGrondslagSom = 1142;
        public const string Name_WlzGrondslagSom = "WlzGrondslagSom";

        // '' (6: Checkbox)
        public const int ID_WlzGrondslagSomAttachment = 1143;
        public const string Name_WlzGrondslagSomAttachment = "WlzGrondslagSomAttachment";

        // 'Geldig van' (9: Date)
        public const int ID_WlzGrondslagSomFrom = 1144;
        public const string Name_WlzGrondslagSomFrom = "WlzGrondslagSomFrom";

        // 'Geldig tot' (9: Date)
        public const int ID_WlzGrondslagSomUntill = 1145;
        public const string Name_WlzGrondslagSomUntill = "WlzGrondslagSomUntill";

        // 'PG' (6: Checkbox)
        public const int ID_WlzGrondslagPG = 1146;
        public const string Name_WlzGrondslagPG = "WlzGrondslagPG";

        // '' (6: Checkbox)
        public const int ID_WlzGrondslagPGAttachment = 1147;
        public const string Name_WlzGrondslagPGAttachment = "WlzGrondslagPGAttachment";

        // 'Geldig van' (9: Date)
        public const int ID_WlzGrondslagPGFrom = 1148;
        public const string Name_WlzGrondslagPGFrom = "WlzGrondslagPGFrom";

        // 'Geldig tot' (9: Date)
        public const int ID_WlzGrondslagPGUntill = 1149;
        public const string Name_WlzGrondslagPGUntill = "WlzGrondslagPGUntill";

        // 'Opmerkingen' (1: String)
        public const int ID_IIFormOpmerkingen = 1150;
        public const string Name_IIFormOpmerkingen = "IIFormOpmerkingen";

        // 'BOPZ afgegeven' (5: RadioButton)
        public const int ID_AanvraagBOPZ = 1151;
        public const string Name_AanvraagBOPZ = "AanvraagBOPZ";

        // 'Toelichting' (1: String)
        public const int ID_AcceptedByTPExplanation = 1152;
        public const string Name_AcceptedByTPExplanation = "AcceptedByTPExplanation";

        // 'Intercollegiale communicatie' (1: String)
        public const int ID_Remarks = 1153;
        public const string Name_Remarks = "Remarks";

        // 'Persoons Gebonden Budget' (6: Checkbox)
        public const int ID_AfterCarePreferencePGB = 1154;
        public const string Name_AfterCarePreferencePGB = "AfterCarePreferencePGB";

        // 'Zorg in Natura' (6: Checkbox)
        public const int ID_AfterCarePreferenceZIN = 1155;
        public const string Name_AfterCarePreferenceZIN = "AfterCarePreferenceZIN";

        // 'Gerealiseerde VVT instelling' (8: IDText)
        public const int ID_RealizedHCPInstitution = 1156;
        public const string Name_RealizedHCPInstitution = "RealizedHCPInstitution";

        // 'Gerealiseerde ontslagdatum' (9: Date)
        public const int ID_RealizedDischargeDate = 1157;
        public const string Name_RealizedDischargeDate = "RealizedDischargeDate";

        // 'Opnieuw medische behandeling' (6: Checkbox)
        public const int ID_OnceAgainMedicalTreatment = 1158;
        public const string Name_OnceAgainMedicalTreatment = "OnceAgainMedicalTreatment";

        // 'Vertraging bij transferpunt' (6: Checkbox)
        public const int ID_DelayInTransferAgency = 1159;
        public const string Name_DelayInTransferAgency = "DelayInTransferAgency";

        // 'Vertraging bij de verpleegafdeling/polikliniek' (6: Checkbox)
        public const int ID_DelayInNursingDepartmentPolyclinic = 1160;
        public const string Name_DelayInNursingDepartmentPolyclinic = "DelayInNursingDepartmentPolyclinic";

        // 'Vertraging bij VVT instelling' (6: Checkbox)
        public const int ID_DelayInInstitutionVVT = 1161;
        public const string Name_DelayInInstitutionVVT = "DelayInInstitutionVVT";

        // 'Vertraging bij andere partijen (CIZ, Verzekeraar, gemeente, leveranciers)' (6: Checkbox)
        public const int ID_DelayInOthers = 1162;
        public const string Name_DelayInOthers = "DelayInOthers";

        // 'Registratie beeindigd zonder transfer naar vervolgvoorziening' (6: Checkbox)
        public const int ID_RegistrationEndedWithoutTransfer = 1163;
        public const string Name_RegistrationEndedWithoutTransfer = "RegistrationEndedWithoutTransfer";

        // 'Reden einde registratie' (7: DropDownList)
        public const int ID_ReasonEndRegistration = 1164;
        public const string Name_ReasonEndRegistration = "ReasonEndRegistration";

        // 'VVT instelling eerste voorkeur' (8: IDText)
        public const int ID_ZorgInZorginstellingVoorkeur1 = 1165;
        public const string Name_ZorgInZorginstellingVoorkeur1 = "ZorgInZorginstellingVoorkeur1";

        // 'Alarmering' (6: Checkbox)
        public const int ID_NoodzakelijkTeRegelenHulpmiddelenAlarmering = 1166;
        public const string Name_NoodzakelijkTeRegelenHulpmiddelenAlarmering = "NoodzakelijkTeRegelenHulpmiddelenAlarmering";

        // 'Bestaande zorg' (5: RadioButton)
        public const int ID_AdjustmentStandingCare = 1167;
        public const string Name_AdjustmentStandingCare = "AdjustmentStandingCare";

        // 'Patient kan WEL overgenomen/in zorg genomen worden met ingang van' (9: Date)
        public const int ID_DischargeProposedStartDateByNo = 1168;
        public const string Name_DischargeProposedStartDateByNo = "DischargeProposedStartDateByNo";

        // 'Reden einde registratie' (10: CheckboxList)
        public const int ID_ReasonEndRegistrationHA = 1170;
        public const string Name_ReasonEndRegistrationHA = "ReasonEndRegistrationHA";

        // 'Anders' (1: String)
        public const int ID_AndersSoortMeting = 1200;
        public const string Name_AndersSoortMeting = "AndersSoortMeting";

        // 'Datum' (9: Date)
        public const int ID_BarthelIndexDatum = 1201;
        public const string Name_BarthelIndexDatum = "BarthelIndexDatum";

        // 'Resultaat' (1: String)
        public const int ID_BarthelIndexResultaat = 1202;
        public const string Name_BarthelIndexResultaat = "BarthelIndexResultaat";

        // 'Resultaat voor opname' (1: String)
        public const int ID_BarthelIndexVoorOpname = 1203;
        public const string Name_BarthelIndexVoorOpname = "BarthelIndexVoorOpname";

        // 'Aankleden' (6: Checkbox)
        public const int ID_BasiszorgAankleden = 1204;
        public const string Name_BasiszorgAankleden = "BasiszorgAankleden";

        // 'Baden of Douchen' (6: Checkbox)
        public const int ID_BasiszorgBadenOfDouchen = 1205;
        public const string Name_BasiszorgBadenOfDouchen = "BasiszorgBadenOfDouchen";

        // 'Gebruik Incontinentiemateriaal' (6: Checkbox)
        public const int ID_BasiszorgGebruikIncontinentiemateriaal = 1207;
        public const string Name_BasiszorgGebruikIncontinentiemateriaal = "BasiszorgGebruikIncontinentiemateriaal";

        // 'Patient is wakker en kan vertellen wat hem/haar overkomen is ' (6: Checkbox)
        public const int ID_Bewustzijn = 1211;
        public const string Name_Bewustzijn = "Bewustzijn";

        // 'Huishouden' (6: Checkbox)
        public const int ID_BijdrageMantelzorgerHuishouden = 1212;
        public const string Name_BijdrageMantelzorgerHuishouden = "BijdrageMantelzorgerHuishouden";

        // 'Overig' (6: Checkbox)
        public const int ID_BijdrageMantelzorgerOverig = 1213;
        public const string Name_BijdrageMantelzorgerOverig = "BijdrageMantelzorgerOverig";

        // 'Persoonlijke verzorging' (6: Checkbox)
        public const int ID_BijdrageMantelzorgerPersoonlijkeVerzorging = 1214;
        public const string Name_BijdrageMantelzorgerPersoonlijkeVerzorging = "BijdrageMantelzorgerPersoonlijkeVerzorging";

        // 'Vervoer' (6: Checkbox)
        public const int ID_BijdrageMantelzorgerVervoer = 1215;
        public const string Name_BijdrageMantelzorgerVervoer = "BijdrageMantelzorgerVervoer";

        // 'Patiënt geeft er blijk van informatie te kunnen opnemen en te reproduceren ' (6: Checkbox)
        public const int ID_Cognitie = 1216;
        public const string Name_Cognitie = "Cognitie";

        // 'Gehoorproblemen' (6: Checkbox)
        public const int ID_CommunicatieGehoorproblemen = 1217;
        public const string Name_CommunicatieGehoorproblemen = "CommunicatieGehoorproblemen";

        // 'Spraakproblemen' (6: Checkbox)
        public const int ID_CommunicatieSpraakproblemen = 1218;
        public const string Name_CommunicatieSpraakproblemen = "CommunicatieSpraakproblemen";

        // 'Visusproblemen' (6: Checkbox)
        public const int ID_CommunicatieVisusproblemen = 1219;
        public const string Name_CommunicatieVisusproblemen = "CommunicatieVisusproblemen";

        // 'Datum ingevuld' (9: Date)
        public const int ID_DatumIngevuld = 1220;
        public const string Name_DatumIngevuld = "DatumIngevuld";

        // 'Delier' (5: RadioButton)
        public const int ID_Delier = 1221;
        public const string Name_Delier = "Delier";

        // 'datum' (9: Date)
        public const int ID_DelierAanwezigDatum = 1222;
        public const string Name_DelierAanwezigDatum = "DelierAanwezigDatum";

        // 'Laatste DOS-Score' (1: String)
        public const int ID_DelierAanwezigDOSScore = 1223;
        public const string Name_DelierAanwezigDOSScore = "DelierAanwezigDOSScore";

        // 'Denk / waarnemingsstoornissen' (5: RadioButton)
        public const int ID_Denk = 1225;
        public const string Name_Denk = "Denk";

        // 'Desoriëntatie' (5: RadioButton)
        public const int ID_Desorientatie = 1228;
        public const string Name_Desorientatie = "Desorientatie";

        // 'Datum' (9: Date)
        public const int ID_DOSDatum = 1231;
        public const string Name_DOSDatum = "DOSDatum";

        // 'Resultaat' (1: String)
        public const int ID_DOSResultaat = 1232;
        public const string Name_DOSResultaat = "DOSResultaat";

        // 'Resultaat voor opname' (1: String)
        public const int ID_DOSVoorOpname = 1233;
        public const string Name_DOSVoorOpname = "DOSVoorOpname";

        // 'Datum' (9: Date)
        public const int ID_FACDatum = 1234;
        public const string Name_FACDatum = "FACDatum";

        // 'Resultaat' (1: String)
        public const int ID_FACResultaat = 1235;
        public const string Name_FACResultaat = "FACResultaat";

        // 'Resultaat voor opname' (1: String)
        public const int ID_FACVoorOpname = 1236;
        public const string Name_FACVoorOpname = "FACVoorOpname";

        // 'Fysieke belastbaarheid' (5: RadioButton)
        public const int ID_FysiekeBelastbaarheid = 1237;
        public const string Name_FysiekeBelastbaarheid = "FysiekeBelastbaarheid";

        // 'Aankleden' (6: Checkbox)
        public const int ID_FysiekFuncVoorOpnameAankleden = 1238;
        public const string Name_FysiekFuncVoorOpnameAankleden = "FysiekFuncVoorOpnameAankleden";

        // 'Baden of Douchen' (6: Checkbox)
        public const int ID_FysiekFuncVoorOpnameBadenOfDouchen = 1239;
        public const string Name_FysiekFuncVoorOpnameBadenOfDouchen = "FysiekFuncVoorOpnameBadenOfDouchen";

        // 'Eten en Drinken' (6: Checkbox)
        public const int ID_FysiekFuncVoorOpnameEtenEnDrinken = 1240;
        public const string Name_FysiekFuncVoorOpnameEtenEnDrinken = "FysiekFuncVoorOpnameEtenEnDrinken";

        // 'Gebruik Incontinentiemateriaal' (6: Checkbox)
        public const int ID_FysiekFuncVoorOpnameGebruikIncontinentiemateriaal = 1241;
        public const string Name_FysiekFuncVoorOpnameGebruikIncontinentiemateriaal = "FysiekFuncVoorOpnameGebruikIncontinentiemateriaal";

        // 'Mobiliteit' (6: Checkbox)
        public const int ID_FysiekFuncVoorOpnameMobiliteit = 1242;
        public const string Name_FysiekFuncVoorOpnameMobiliteit = "FysiekFuncVoorOpnameMobiliteit";

        // 'Toiletgang' (6: Checkbox)
        public const int ID_FysiekFuncVoorOpnameToiletgang = 1243;
        public const string Name_FysiekFuncVoorOpnameToiletgang = "FysiekFuncVoorOpnameToiletgang";

        // 'Transfer Bed/Stoel' (6: Checkbox)
        public const int ID_FysiekFuncVoorOpnameTransferBedStoel = 1244;
        public const string Name_FysiekFuncVoorOpnameTransferBedStoel = "FysiekFuncVoorOpnameTransferBedStoel";

        // 'Gedragsstoornissen' (5: RadioButton)
        public const int ID_Gedragsstoornissen = 1245;
        public const string Name_Gedragsstoornissen = "Gedragsstoornissen";

        // 'Geheugenverlies' (5: RadioButton)
        public const int ID_GeheugenVerlies = 1248;
        public const string Name_GeheugenVerlies = "GeheugenVerlies";

        // 'herstel na delier gedurende deze opname' (5: RadioButton)
        public const int ID_HerstelNaDelierGedurendeDezeOpname = 1251;
        public const string Name_HerstelNaDelierGedurendeDezeOpname = "HerstelNaDelierGedurendeDezeOpname";

        // 'Gehoorproblemen' (6: Checkbox)
        public const int ID_RequestFormCommunicatieGehoorproblemen = 1252;
        public const string Name_RequestFormCommunicatieGehoorproblemen = "RequestFormCommunicatieGehoorproblemen";

        // 'Spraakproblemen' (6: Checkbox)
        public const int ID_RequestFormCommunicatieSpraakproblemen = 1253;
        public const string Name_RequestFormCommunicatieSpraakproblemen = "RequestFormCommunicatieSpraakproblemen";

        // 'Visusproblemen' (6: Checkbox)
        public const int ID_RequestFormCommunicatieVisusproblemen = 1254;
        public const string Name_RequestFormCommunicatieVisusproblemen = "RequestFormCommunicatieVisusproblemen";

        // 'Inschatting terugkeer naar huis na GRZ' (5: RadioButton)
        public const int ID_InschattingTerugkeerNaarHuis = 1266;
        public const string Name_InschattingTerugkeerNaarHuis = "InschattingTerugkeerNaarHuis";

        // 'Datum' (9: Date)
        public const int ID_KatzADLDatum = 1268;
        public const string Name_KatzADLDatum = "KatzADLDatum";

        // 'Resultaat' (1: String)
        public const int ID_KatzADLResultaat = 1269;
        public const string Name_KatzADLResultaat = "KatzADLResultaat";

        // 'Resultaat voor opname' (1: String)
        public const int ID_KatzADLVoorOpname = 1270;
        public const string Name_KatzADLVoorOpname = "KatzADLVoorOpname";

        // 'Mantelzorg wordt geboden' (5: RadioButton)
        public const int ID_MantelzorgerAanwezig = 1271;
        public const string Name_MantelzorgerAanwezig = "MantelzorgerAanwezig";

        // 'Datum' (9: Date)
        public const int ID_MIDatum = 1272;
        public const string Name_MIDatum = "MIDatum";

        // 'Resultaat' (1: String)
        public const int ID_MIResultaat = 1273;
        public const string Name_MIResultaat = "MIResultaat";

        // 'Resultaat voor opname' (1: String)
        public const int ID_MIVoorOpname = 1274;
        public const string Name_MIVoorOpname = "MIVoorOpname";

        // 'Datum' (9: Date)
        public const int ID_MMSEDatum = 1275;
        public const string Name_MMSEDatum = "MMSEDatum";

        // 'Resultaat' (1: String)
        public const int ID_MMSEResultaat = 1276;
        public const string Name_MMSEResultaat = "MMSEResultaat";

        // 'Resultaat voor opname' (1: String)
        public const int ID_MMSEVoorOpname = 1277;
        public const string Name_MMSEVoorOpname = "MMSEVoorOpname";

        // 'Krukken/stok' (6: Checkbox)
        public const int ID_MobiliteitVoorOpnameKrukkenStok = 1278;
        public const string Name_MobiliteitVoorOpnameKrukkenStok = "MobiliteitVoorOpnameKrukkenStok";

        // 'Looprekje' (6: Checkbox)
        public const int ID_MobiliteitVoorOpnameLooprekje = 1279;
        public const string Name_MobiliteitVoorOpnameLooprekje = "MobiliteitVoorOpnameLooprekje";

        // 'Looprekje met elleboog schalen' (6: Checkbox)
        public const int ID_MobiliteitVoorOpnameLooprekjeMetElleboogSchalen = 1280;
        public const string Name_MobiliteitVoorOpnameLooprekjeMetElleboogSchalen = "MobiliteitVoorOpnameLooprekjeMetElleboogSchalen";

        // 'Rollator' (6: Checkbox)
        public const int ID_MobiliteitVoorOpnameRollator = 1281;
        public const string Name_MobiliteitVoorOpnameRollator = "MobiliteitVoorOpnameRollator";

        // 'Rolstoel' (6: Checkbox)
        public const int ID_MobiliteitVoorOpnameRolstoel = 1282;
        public const string Name_MobiliteitVoorOpnameRolstoel = "MobiliteitVoorOpnameRolstoel";

        // 'Patiënt is gemotiveerd om te gaan revalideren' (6: Checkbox)
        public const int ID_MotivatieVoorRevalidatie = 1283;
        public const string Name_MotivatieVoorRevalidatie = "MotivatieVoorRevalidatie";

        // 'Toelichting op Motivatie revalidatie' (1: String)
        public const int ID_MotivatieVoorRevalidatieToelichting = 1284;
        public const string Name_MotivatieVoorRevalidatieToelichting = "MotivatieVoorRevalidatieToelichting";

        // 'Looprekje' (6: Checkbox)
        public const int ID_NoodzakelijkTeRegelenHulpmiddelenLooprekje = 1286;
        public const string Name_NoodzakelijkTeRegelenHulpmiddelenLooprekje = "NoodzakelijkTeRegelenHulpmiddelenLooprekje";

        // 'Looprekje met elleboog schalen' (6: Checkbox)
        public const int ID_NoodzakelijkTeRegelenHulpmiddelenLooprekjeMetElleboogSchalen = 1287;
        public const string Name_NoodzakelijkTeRegelenHulpmiddelenLooprekjeMetElleboogSchalen = "NoodzakelijkTeRegelenHulpmiddelenLooprekjeMetElleboogSchalen";

        // 'Advieszorgplan TP toevoegen' (5: RadioButton)
        public const int ID_ZorgAdviesToevoegen = 1288;
        public const string Name_ZorgAdviesToevoegen = "ZorgAdviesToevoegen";

        // 'Datum' (9: Date)
        public const int ID_PRPPDatum = 1294;
        public const string Name_PRPPDatum = "PRPPDatum";

        // 'Resultaat' (1: String)
        public const int ID_PRPPResultaat = 1295;
        public const string Name_PRPPResultaat = "PRPPResultaat";

        // 'Resultaat voor opname' (1: String)
        public const int ID_PRPPVoorOpname = 1296;
        public const string Name_PRPPVoorOpname = "PRPPVoorOpname";

        // 'Toelichting' (1: String)
        public const int ID_FysiekFuncToelichting = 1297;
        public const string Name_FysiekFuncToelichting = "FysiekFuncToelichting";

        // 'Verkeersdeelname' (10: CheckboxList)
        public const int ID_VerkeersDeelname = 1298;
        public const string Name_VerkeersDeelname = "VerkeersDeelname";

        // 'Toelichting' (1: String)
        public const int ID_GRZCommunicatieToelichting = 1299;
        public const string Name_GRZCommunicatieToelichting = "GRZCommunicatieToelichting";

        // 'Toelichting' (1: String)
        public const int ID_GRZMobiliteitToelichting = 1300;
        public const string Name_GRZMobiliteitToelichting = "GRZMobiliteitToelichting";

        // 'Datum' (9: Date)
        public const int ID_SANDatum = 1303;
        public const string Name_SANDatum = "SANDatum";

        // 'Resultaat' (1: String)
        public const int ID_SANResultaat = 1304;
        public const string Name_SANResultaat = "SANResultaat";

        // 'Resultaat voor opname' (1: String)
        public const int ID_SANVoorOpname = 1305;
        public const string Name_SANVoorOpname = "SANVoorOpname";

        // 'Datum' (9: Date)
        public const int ID_SnaqDatum = 1306;
        public const string Name_SnaqDatum = "SnaqDatum";

        // 'Resultaat' (1: String)
        public const int ID_SnaqResultaat = 1307;
        public const string Name_SnaqResultaat = "SnaqResultaat";

        // 'Resultaat voor opname' (1: String)
        public const int ID_SnaqVoorOpname = 1308;
        public const string Name_SnaqVoorOpname = "SnaqVoorOpname";

        // 'Toelichting' (1: String)
        public const int ID_SocialeSituatieHuishoudenToelichting = 1309;
        public const string Name_SocialeSituatieHuishoudenToelichting = "SocialeSituatieHuishoudenToelichting";

        // 'Stemmingsstoornissen' (5: RadioButton)
        public const int ID_Stemmingsstoornissen = 1310;
        public const string Name_Stemmingsstoornissen = "Stemmingsstoornissen";

        // 'Toelichting' (1: String)
        public const int ID_ToegangGRZToelichting = 1313;
        public const string Name_ToegangGRZToelichting = "ToegangGRZToelichting";

        // 'Datum' (9: Date)
        public const int ID_WatersliktestDatum = 1317;
        public const string Name_WatersliktestDatum = "WatersliktestDatum";

        // 'Resultaat' (1: String)
        public const int ID_WatersliktestResultaat = 1318;
        public const string Name_WatersliktestResultaat = "WatersliktestResultaat";

        // 'Resultaat voor opname' (1: String)
        public const int ID_WatersliktestVoorOpname = 1319;
        public const string Name_WatersliktestVoorOpname = "WatersliktestVoorOpname";

        // 'Datum' (9: Date)
        public const int ID_WOLCDatum = 1320;
        public const string Name_WOLCDatum = "WOLCDatum";

        // 'Resultaat' (1: String)
        public const int ID_WOLCResultaat = 1321;
        public const string Name_WOLCResultaat = "WOLCResultaat";

        // 'Resultaat voor opname' (1: String)
        public const int ID_WOLCVoorOpname = 1322;
        public const string Name_WOLCVoorOpname = "WOLCVoorOpname";

        // 'Ziekte inzicht' (5: RadioButton)
        public const int ID_ZiekteInzicht = 1323;
        public const string Name_ZiekteInzicht = "ZiekteInzicht";

        // 'Samenstelling huishouden' (7: DropDownList)
        public const int ID_CompositionHousekeeping = 1324;
        public const string Name_CompositionHousekeeping = "CompositionHousekeeping";

        // 'Woonsituatie' (7: DropDownList)
        public const int ID_HousingType = 1339;
        public const string Name_HousingType = "HousingType";

        // 'Verzorgings- / verpleeginstelling' (8: IDText)
        public const int ID_HealthCareProvider = 1340;
        public const string Name_HealthCareProvider = "HealthCareProvider";

        // 'Door' (1: String)
        public const int ID_MantelzorgDoor = 1344;
        public const string Name_MantelzorgDoor = "MantelzorgDoor";

        // 'Per dag' (1: String)
        public const int ID_MantelzorgKeerPerDag = 1345;
        public const string Name_MantelzorgKeerPerDag = "MantelzorgKeerPerDag";

        // 'Of per week' (1: String)
        public const int ID_MantelzorgKeerPerWeek = 1346;
        public const string Name_MantelzorgKeerPerWeek = "MantelzorgKeerPerWeek";

        // 'Mantelzorg bijdrage toelichting' (1: String)
        public const int ID_BijdrageMantelzorgerOverigToelichting = 1347;
        public const string Name_BijdrageMantelzorgerOverigToelichting = "BijdrageMantelzorgerOverigToelichting";

        // 'Toelichting' (1: String)
        public const int ID_MantelzorgToelichting = 1348;
        public const string Name_MantelzorgToelichting = "MantelzorgToelichting";

        // 'Toelichting afwijkende tijd' (1: String)
        public const int ID_RedenAfwijkendeTijd = 1403;
        public const string Name_RedenAfwijkendeTijd = "RedenAfwijkendeTijd";

        // 'Startdatum' (9: Date)
        public const int ID_GeldigheidsDuurStartdatum = 1404;
        public const string Name_GeldigheidsDuurStartdatum = "GeldigheidsDuurStartdatum";

        // 'Einddatum' (9: Date)
        public const int ID_GeldigheidsDuurEinddatum = 1405;
        public const string Name_GeldigheidsDuurEinddatum = "GeldigheidsDuurEinddatum";

        // 'Toelichting' (1: String)
        public const int ID_GeldigheidsDuurToelichting = 1406;
        public const string Name_GeldigheidsDuurToelichting = "GeldigheidsDuurToelichting";

        // 'Totale verwachte benodigde tijd van de behandeling' (1: String)
        public const int ID_IndicatieBenodigdeTijd = 1407;
        public const string Name_IndicatieBenodigdeTijd = "IndicatieBenodigdeTijd";

        // 'Leverende organisatie' (8: IDText)
        public const int ID_MSVTLeverendeOrganisatie = 1414;
        public const string Name_MSVTLeverendeOrganisatie = "MSVTLeverendeOrganisatie";

        // 'Medewerker' (1: String)
        public const int ID_MSVTLeverendeMedewerker = 1415;
        public const string Name_MSVTLeverendeMedewerker = "MSVTLeverendeMedewerker";

        // 'BIG nr' (1: String)
        public const int ID_MSVTLeverendeMedewerkerBIG = 1416;
        public const string Name_MSVTLeverendeMedewerkerBIG = "MSVTLeverendeMedewerkerBIG";

        // 'Emailadres' (1: String)
        public const int ID_MSVTLeverendeMedewerkerEmail = 1417;
        public const string Name_MSVTLeverendeMedewerkerEmail = "MSVTLeverendeMedewerkerEmail";

        // 'Telefoon nr' (1: String)
        public const int ID_MSVTLeverendeMedewerkerTelefoon = 1418;
        public const string Name_MSVTLeverendeMedewerkerTelefoon = "MSVTLeverendeMedewerkerTelefoon";

        // 'Datum Triage' (9: Date)
        public const int ID_ResultaatDatumTriage = 1501;
        public const string Name_ResultaatDatumTriage = "ResultaatDatumTriage";

        // 'Naam SO / Transferverpleegkundige' (1: String)
        public const int ID_ResultaatTriageDoorSOG = 1502;
        public const string Name_ResultaatTriageDoorSOG = "ResultaatTriageDoorSOG";

        // '' (6: Checkbox)
        public const int ID_PatientBezochtInZiekenhuis = 1503;
        public const string Name_PatientBezochtInZiekenhuis = "PatientBezochtInZiekenhuis";

        // 'Kernprobleem' (1: String)
        public const int ID_KernProbleem = 1504;
        public const string Name_KernProbleem = "KernProbleem";

        // 'Functionele prognose' (1: String)
        public const int ID_FunctionelePrognose = 1505;
        public const string Name_FunctionelePrognose = "FunctionelePrognose";

        // 'Patiënt is gemotiveerd voor revalidatie' (5: RadioButton)
        public const int ID_ResultaatPatientIsGemotiveerdVoorRevalidatie = 1506;
        public const string Name_ResultaatPatientIsGemotiveerdVoorRevalidatie = "ResultaatPatientIsGemotiveerdVoorRevalidatie";

        // 'Patiënt is leerbaar' (5: RadioButton)
        public const int ID_ResultaatPatientIsLeerbaar = 1507;
        public const string Name_ResultaatPatientIsLeerbaar = "ResultaatPatientIsLeerbaar";

        // 'Patiënt is trainbaar' (5: RadioButton)
        public const int ID_ResultaatPatientIsFysiekBelastbaar = 1508;
        public const string Name_ResultaatPatientIsFysiekBelastbaar = "ResultaatPatientIsFysiekBelastbaar";

        // 'Patiënt kan na de revalidatie naar huis (of verzorgingshuis)' (5: RadioButton)
        public const int ID_ResultaatPatientKanNaarHuis = 1509;
        public const string Name_ResultaatPatientKanNaarHuis = "ResultaatPatientKanNaarHuis";

        // 'Geschatte kans op 'terug naar huis' (w.o. verzorgingshuis)' (1: String)
        public const int ID_ResultaatKansOpTerugNaarHuis = 1510;
        public const string Name_ResultaatKansOpTerugNaarHuis = "ResultaatKansOpTerugNaarHuis";

        // 'Patiënt komt in aanmerking voor in medisch-specialistische revalidatie' (5: RadioButton)
        public const int ID_ResultaatPatientKomtInAanmerkingVoorMSR = 1511;
        public const string Name_ResultaatPatientKomtInAanmerkingVoorMSR = "ResultaatPatientKomtInAanmerkingVoorMSR";

        // 'Conclusie uit triage' (5: RadioButton)
        public const int ID_ConclusieWelGeenGRZ = 1512;
        public const string Name_ConclusieWelGeenGRZ = "ConclusieWelGeenGRZ";

        // '' (1: String)
        public const int ID_ConclusieOverigBeschrijving = 1513;
        public const string Name_ConclusieOverigBeschrijving = "ConclusieOverigBeschrijving";

        // '' (6: Checkbox)
        public const int ID_AdviesGeenGRZ_ZZP9b = 1514;
        public const string Name_AdviesGeenGRZ_ZZP9b = "AdviesGeenGRZ_ZZP9b";

        // '' (6: Checkbox)
        public const int ID_AdviesGeenGRZ_ZZP5678 = 1515;
        public const string Name_AdviesGeenGRZ_ZZP5678 = "AdviesGeenGRZ_ZZP5678";

        // '' (6: Checkbox)
        public const int ID_AdviesGeenGRZ_ZZP34 = 1516;
        public const string Name_AdviesGeenGRZ_ZZP34 = "AdviesGeenGRZ_ZZP34";

        // 'Opmerkingen' (1: String)
        public const int ID_ResultaatEvtAfsprakenOntslag = 1517;
        public const string Name_ResultaatEvtAfsprakenOntslag = "ResultaatEvtAfsprakenOntslag";

        // 'GRZ type' (5: RadioButton)
        public const int ID_ConclusieTypeGRZ = 1518;
        public const string Name_ConclusieTypeGRZ = "ConclusieTypeGRZ";

        // 'Door' (5: RadioButton)
        public const int ID_ResultaatTriageDoor = 1519;
        public const string Name_ResultaatTriageDoor = "ResultaatTriageDoor";

        // 'Specifieke (nazorg) projecten' (10: CheckboxList)
        public const int ID_AftercareProjects = 1520;
        public const string Name_AftercareProjects = "AftercareProjects";

        // 'Toelichting' (1: String)
        public const int ID_DelayComments = 1521;
        public const string Name_DelayComments = "DelayComments";

        // 'Hervatten zorg type' (5: RadioButton)
        public const int ID_HervattenZorgType = 1522;
        public const string Name_HervattenZorgType = "HervattenZorgType";

        // 'Toelichting' (1: String)
        public const int ID_HervattenZorgToelichting = 1523;
        public const string Name_HervattenZorgToelichting = "HervattenZorgToelichting";

        // 'Print "voorwaarden voor het uitvoeren van medische handelingen in de thuissituatie in opdracht van de  medisch specialist" (Zie: <a href="/Documents/Voorwaarden MSVT.pdf" target="_blank">document</a>)' (6: Checkbox)
        public const int ID_PrintVoorwaardenUVV = 1524;
        public const string Name_PrintVoorwaardenUVV = "PrintVoorwaardenUVV";

        // 'Versturende locatie' (8: IDText)
        public const int ID_SourceOrganizationLocation = 1525;
        public const string Name_SourceOrganizationLocation = "SourceOrganizationLocation";

        // 'Patiënt is belastbaar voor revalidatie' (5: RadioButton)
        public const int ID_ResultaatPatientBelastbaarVoorRevalidatie = 1526;
        public const string Name_ResultaatPatientBelastbaarVoorRevalidatie = "ResultaatPatientBelastbaarVoorRevalidatie";

        // 'Verwachte opnameduur en intensiteit behandeling' (5: RadioButton)
        public const int ID_GRZVerwachtteOpnameDuur = 1530;
        public const string Name_GRZVerwachtteOpnameDuur = "GRZVerwachtteOpnameDuur";

        // 'CVA' (10: CheckboxList)
        public const int ID_GRZVoorlopigeDiagnoseCVA = 1531;
        public const string Name_GRZVoorlopigeDiagnoseCVA = "GRZVoorlopigeDiagnoseCVA";

        // 'Electieve orthopedie' (10: CheckboxList)
        public const int ID_GRZVoorlopigeDiagnoseEO = 1532;
        public const string Name_GRZVoorlopigeDiagnoseEO = "GRZVoorlopigeDiagnoseEO";

        // 'Trauma' (10: CheckboxList)
        public const int ID_GRZVoorlopigeDiagnoseTrauma = 1533;
        public const string Name_GRZVoorlopigeDiagnoseTrauma = "GRZVoorlopigeDiagnoseTrauma";

        // 'Amputatie' (10: CheckboxList)
        public const int ID_GRZVoorlopigeDiagnoseAmputatie = 1534;
        public const string Name_GRZVoorlopigeDiagnoseAmputatie = "GRZVoorlopigeDiagnoseAmputatie";

        // 'Overig' (10: CheckboxList)
        public const int ID_GRZVoorlopigeDiagnoseOverig = 1535;
        public const string Name_GRZVoorlopigeDiagnoseOverig = "GRZVoorlopigeDiagnoseOverig";

        // 'Bij 504' (10: CheckboxList)
        public const int ID_GRZVoorlopigeDiagnoseBij504 = 1536;
        public const string Name_GRZVoorlopigeDiagnoseBij504 = "GRZVoorlopigeDiagnoseBij504";

        // 'Diagnose' (5: RadioButton)
        public const int ID_Diagnose = 1600;
        public const string Name_Diagnose = "Diagnose";

        // 'Behandelwijze' (5: RadioButton)
        public const int ID_Behandelwijze = 1601;
        public const string Name_Behandelwijze = "Behandelwijze";

        // 'Datum opname' (9: Date)
        public const int ID_PatientWordtOpgenomenDatum = 1602;
        public const string Name_PatientWordtOpgenomenDatum = "PatientWordtOpgenomenDatum";

        // 'Patient wordt opgenomen op stroke-unit' (5: RadioButton)
        public const int ID_PatientWordtOpgenomenOpStrokeUnit = 1603;
        public const string Name_PatientWordtOpgenomenOpStrokeUnit = "PatientWordtOpgenomenOpStrokeUnit";

        // 'Datum overplaatsing op Normal care' (9: Date)
        public const int ID_OverplaatsingNaarNormalCareDatum = 1604;
        public const string Name_OverplaatsingNaarNormalCareDatum = "OverplaatsingNaarNormalCareDatum";

        // 'Datum/tijd eerste klacht' (9: Date)
        public const int ID_DiagnoseEersteKlachtDatum = 1605;
        public const string Name_DiagnoseEersteKlachtDatum = "DiagnoseEersteKlachtDatum";

        // 'Tijd eerste klacht' (3: Time)
        public const int ID_DiagnoseEersteKlachtTijd = 1606;
        public const string Name_DiagnoseEersteKlachtTijd = "DiagnoseEersteKlachtTijd";

        // 'Datum aankomst ziekenhuis met klachten i.r.t. herseninfarct/hersenbloeding' (9: Date)
        public const int ID_DiagnoseAankomstSEHDatum = 1607;
        public const string Name_DiagnoseAankomstSEHDatum = "DiagnoseAankomstSEHDatum";

        // 'Tijd aankomst SEH' (3: Time)
        public const int ID_DiagnoseAankomstSEHTijd = 1608;
        public const string Name_DiagnoseAankomstSEHTijd = "DiagnoseAankomstSEHTijd";

        // 'Patient is in ziekenhuis intraveneus getrombolyseerd' (5: RadioButton)
        public const int ID_InZHIntraveneusGetrombolyseerd = 1609;
        public const string Name_InZHIntraveneusGetrombolyseerd = "InZHIntraveneusGetrombolyseerd";

        // 'Geringe uitval' (6: Checkbox)
        public const int ID_RedenTrombolyseNietUitgevoerdGeringeUitval = 1610;
        public const string Name_RedenTrombolyseNietUitgevoerdGeringeUitval = "RedenTrombolyseNietUitgevoerdGeringeUitval";

        // 'Verbetering' (6: Checkbox)
        public const int ID_RedenTrombolyseNietUitgevoerdVerbetering = 1611;
        public const string Name_RedenTrombolyseNietUitgevoerdVerbetering = "RedenTrombolyseNietUitgevoerdVerbetering";

        // 'Te hoge bloeddruk' (6: Checkbox)
        public const int ID_RedenTrombolyseNietUitgevoerdTeHogeBloeddruk = 1612;
        public const string Name_RedenTrombolyseNietUitgevoerdTeHogeBloeddruk = "RedenTrombolyseNietUitgevoerdTeHogeBloeddruk";

        // 'Antistolling' (6: Checkbox)
        public const int ID_RedenTrombolyseNietUitgevoerdAntistolling = 1613;
        public const string Name_RedenTrombolyseNietUitgevoerdAntistolling = "RedenTrombolyseNietUitgevoerdAntistolling";

        // 'Te laat gekomen' (6: Checkbox)
        public const int ID_RedenTrombolyseNietUitgevoerdTeLaatGekomen = 1614;
        public const string Name_RedenTrombolyseNietUitgevoerdTeLaatGekomen = "RedenTrombolyseNietUitgevoerdTeLaatGekomen";

        // 'Tijdstip CVA onbekend' (6: Checkbox)
        public const int ID_RedenTrombolyseNietUitgevoerdTijdstipCVAOnbekend = 1615;
        public const string Name_RedenTrombolyseNietUitgevoerdTijdstipCVAOnbekend = "RedenTrombolyseNietUitgevoerdTijdstipCVAOnbekend";

        // 'Anders' (6: Checkbox)
        public const int ID_RedenTrombolyseNietUitgevoerdAnders = 1616;
        public const string Name_RedenTrombolyseNietUitgevoerdAnders = "RedenTrombolyseNietUitgevoerdAnders";

        // 'Namelijk' (1: String)
        public const int ID_RedenTrombolyseNietUitgevoerdNamelijk = 1617;
        public const string Name_RedenTrombolyseNietUitgevoerdNamelijk = "RedenTrombolyseNietUitgevoerdNamelijk";

        // 'Datum/tijd trombolyse intraveneus' (9: Date)
        public const int ID_TrombolyseIntraveneusDatum = 1618;
        public const string Name_TrombolyseIntraveneusDatum = "TrombolyseIntraveneusDatum";

        // 'Tijd trombolyse intraveneus' (3: Time)
        public const int ID_TrombolyseIntraveneusTijd = 1619;
        public const string Name_TrombolyseIntraveneusTijd = "TrombolyseIntraveneusTijd";

        // 'Vertraging intraveneuze trombolyse' (5: RadioButton)
        public const int ID_VertragingIntraveneuzeTrombolyse = 1620;
        public const string Name_VertragingIntraveneuzeTrombolyse = "VertragingIntraveneuzeTrombolyse";

        // 'Reden trombolyse vertraagd uitgevoerd' (5: RadioButton)
        public const int ID_RedenTrombolyseVerlaatUitgevoerd = 1621;
        public const string Name_RedenTrombolyseVerlaatUitgevoerd = "RedenTrombolyseVerlaatUitgevoerd";

        // 'Patient is in ziekenhuis met intra-arteriële trombectomie behandeld' (5: RadioButton)
        public const int ID_InZHIntraArterieelBehandeld = 1622;
        public const string Name_InZHIntraArterieelBehandeld = "InZHIntraArterieelBehandeld";

        // 'Datum/tijdstip intra-arteriële trombectomie' (9: Date)
        public const int ID_TrombolyseArterieelDatum = 1623;
        public const string Name_TrombolyseArterieelDatum = "TrombolyseArterieelDatum";

        // 'Tijd trombolyse arterieel' (3: Time)
        public const int ID_TrombolyseArterieelTijd = 1624;
        public const string Name_TrombolyseArterieelTijd = "TrombolyseArterieelTijd";

        // 'Datum aanmelding patient voor TIA diagnostiek' (9: Date)
        public const int ID_AanmeldingTIADiagnostiekDatum = 1625;
        public const string Name_AanmeldingTIADiagnostiekDatum = "AanmeldingTIADiagnostiekDatum";

        // 'Datum start TIA diagnostiek' (9: Date)
        public const int ID_StartTIADiagnostiekDatum = 1626;
        public const string Name_StartTIADiagnostiekDatum = "StartTIADiagnostiekDatum";

        // 'Barthel index op dag vier (of eerder) van de opname' (7: DropDownList)
        public const int ID_BarthelIndexOpDagVierOfEerder = 1627;
        public const string Name_BarthelIndexOpDagVierOfEerder = "BarthelIndexOpDagVierOfEerder";

        // 'Slikscreening uitgevoerd direct na opname' (5: RadioButton)
        public const int ID_SlikscreeningUitgevoerdDirectNaOpname = 1628;
        public const string Name_SlikscreeningUitgevoerdDirectNaOpname = "SlikscreeningUitgevoerdDirectNaOpname";

        // 'IO2-saturatie meting gedaan gedurende de slikscreening' (5: RadioButton)
        public const int ID_IO2SaturatieGedurendeSlikscreening = 1629;
        public const string Name_IO2SaturatieGedurendeSlikscreening = "IO2SaturatieGedurendeSlikscreening";

        // 'Beloop van de opname' (5: RadioButton)
        public const int ID_BeloopOpname = 1630;
        public const string Name_BeloopOpname = "BeloopOpname";

        // 'Datum ontslag/overlijden patient' (9: Date)
        public const int ID_PatientOntslagenDatum = 1631;
        public const string Name_PatientOntslagenDatum = "PatientOntslagenDatum";

        // 'Patient ontslagen naar' (7: DropDownList)
        public const int ID_PatientOntslagenNaar = 1633;
        public const string Name_PatientOntslagenNaar = "PatientOntslagenNaar";

        // 'Naam ziekenhuis' (7: DropDownList)
        public const int ID_PatientOntslagenNaarNaamZH = 1634;
        public const string Name_PatientOntslagenNaarNaamZH = "PatientOntslagenNaarNaamZH";

        // 'Anders nl.' (1: String)
        public const int ID_PatientOntslagenNaarAnders = 1635;
        public const string Name_PatientOntslagenNaarAnders = "PatientOntslagenNaarAnders";

        // 'Opname datum' (9: Date)
        public const int ID_DatumOpnameRevalidatieCentrum = 1640;
        public const string Name_DatumOpnameRevalidatieCentrum = "DatumOpnameRevalidatieCentrum";

        // 'Barthel index bij opname' (7: DropDownList)
        public const int ID_BarthelIndexBijOpnameVVT = 1641;
        public const string Name_BarthelIndexBijOpnameVVT = "BarthelIndexBijOpnameVVT";

        // 'Beloop van de opname' (5: RadioButton)
        public const int ID_BeloopOpnameVVT = 1642;
        public const string Name_BeloopOpnameVVT = "BeloopOpnameVVT";

        // 'Datum ontslag/overlijden patient' (9: Date)
        public const int ID_PatientOntslagenOverledenDatumVVT = 1643;
        public const string Name_PatientOntslagenOverledenDatumVVT = "PatientOntslagenOverledenDatumVVT";

        // 'Barthel index bij ontslag' (7: DropDownList)
        public const int ID_BarthelIndexBijOntslagVVT = 1644;
        public const string Name_BarthelIndexBijOntslagVVT = "BarthelIndexBijOntslagVVT";

        // 'Patient ontslagen naar' (7: DropDownList)
        public const int ID_PatientOntslagenVVTNaar = 1645;
        public const string Name_PatientOntslagenVVTNaar = "PatientOntslagenVVTNaar";

        // 'Naam ziekenhuis' (7: DropDownList)
        public const int ID_PatientOntslagenVVTNaarNaamZH = 1646;
        public const string Name_PatientOntslagenVVTNaarNaamZH = "PatientOntslagenVVTNaarNaamZH";

        // 'Anders nl.' (1: String)
        public const int ID_PatientOntslagenVVTNaarAnders = 1647;
        public const string Name_PatientOntslagenVVTNaarAnders = "PatientOntslagenVVTNaarAnders";

        // 'Zorginstelling waar de patient naar wordt verwezen' (7: DropDownList)
        public const int ID_ZorginstellingPatientNaarVerwezen = 1648;
        public const string Name_ZorginstellingPatientNaarVerwezen = "ZorginstellingPatientNaarVerwezen";

        // 'VVT' (8: IDText)
        public const int ID_PatientOntslagenVVTNaarVVT = 1649;
        public const string Name_PatientOntslagenVVTNaarVVT = "PatientOntslagenVVTNaarVVT";

        // 'Verwezen uit ander ziekenhuis' (5: RadioButton)
        public const int ID_VerwezenUitAnderZiekenhuis = 1650;
        public const string Name_VerwezenUitAnderZiekenhuis = "VerwezenUitAnderZiekenhuis";

        // 'Follow-up datum' (9: Date)
        public const int ID_FollowUpDatum = 1660;
        public const string Name_FollowUpDatum = "FollowUpDatum";

        // 'Modified Rankin Scale drie maanden na optreden van het herseninfarct/de hersenbloeding' (7: DropDownList)
        public const int ID_ModifiedRankingScale = 1661;
        public const string Name_ModifiedRankingScale = "ModifiedRankingScale";

        // 'Verblijfplaats drie maanden na optreden van het CVA' (7: DropDownList)
        public const int ID_VerblijfplaatsNa3mnd = 1662;
        public const string Name_VerblijfplaatsNa3mnd = "VerblijfplaatsNa3mnd";

        // 'Naam ziekenhuis' (7: DropDownList)
        public const int ID_VerblijfplaatsNa3mndNaamZH = 1663;
        public const string Name_VerblijfplaatsNa3mndNaamZH = "VerblijfplaatsNa3mndNaamZH";

        // 'Anders nl.' (1: String)
        public const int ID_VerblijfplaatsNa3mndAnders = 1664;
        public const string Name_VerblijfplaatsNa3mndAnders = "VerblijfplaatsNa3mndAnders";

        // 'Postcode van verblijfplaats na 3 maanden' (1: String)
        public const int ID_VerblijfplaatsNa3mndPostcode = 1665;
        public const string Name_VerblijfplaatsNa3mndPostcode = "VerblijfplaatsNa3mndPostcode";

        // 'Datum van overlijden?' (9: Date)
        public const int ID_DatumVanOverlijdenNa3mnd = 1666;
        public const string Name_DatumVanOverlijdenNa3mnd = "DatumVanOverlijdenNa3mnd";

        // 'Barthelscore voor het CVA' (7: DropDownList)
        public const int ID_BarthelscoreVoorHetCVA = 1667;
        public const string Name_BarthelscoreVoorHetCVA = "BarthelscoreVoorHetCVA";

        // 'NIHSS bij opname ('-1' als niet bekend)' (1: String)
        public const int ID_MedischeSituatieNIHSSBijOpname = 1668;
        public const string Name_MedischeSituatieNIHSSBijOpname = "MedischeSituatieNIHSSBijOpname";

        // 'Modified rankin scale op datum ontstaan neurologische uitval' (7: DropDownList)
        public const int ID_ModifiedRankingScaleOpDatumOntstaanNeurologischeUitval = 1669;
        public const string Name_ModifiedRankingScaleOpDatumOntstaanNeurologischeUitval = "ModifiedRankingScaleOpDatumOntstaanNeurologischeUitval";

        // 'Mobilisatie binnen 24 uur' (5: RadioButton)
        public const int ID_MobilisatieBinnen24Uur = 1670;
        public const string Name_MobilisatieBinnen24Uur = "MobilisatieBinnen24Uur";

        // 'NIHSS bij ontslag ('-1' als niet bekend)' (1: String)
        public const int ID_MedischeSituatieNIHSSBijOntslag = 1671;
        public const string Name_MedischeSituatieNIHSSBijOntslag = "MedischeSituatieNIHSSBijOntslag";

        // 'Barthelscore bij ontslag' (7: DropDownList)
        public const int ID_BarthelscoreBijOntslag = 1672;
        public const string Name_BarthelscoreBijOntslag = "BarthelscoreBijOntslag";

        // 'Reden waarom geen follow up afgenomen' (7: DropDownList)
        public const int ID_FollowUpNietGedaan = 1673;
        public const string Name_FollowUpNietGedaan = "FollowUpNietGedaan";

        // 'Patient gaat naar' (5: RadioButton)
        public const int ID_Ontslagbestemming = 1700;
        public const string Name_Ontslagbestemming = "Ontslagbestemming";

        // 'Huis' (5: RadioButton)
        public const int ID_OntslagbestemmingThuis = 1701;
        public const string Name_OntslagbestemmingThuis = "OntslagbestemmingThuis";

        // 'Buitenland' (5: RadioButton)
        public const int ID_OntslagbestemmingBuitenland = 1702;
        public const string Name_OntslagbestemmingBuitenland = "OntslagbestemmingBuitenland";

        // '' (1: String)
        public const int ID_OntslagbestemmingVerpleeghuis = 1703;
        public const string Name_OntslagbestemmingVerpleeghuis = "OntslagbestemmingVerpleeghuis";

        // '' (1: String)
        public const int ID_OntslagbestemmingAnderZiekenhuis = 1704;
        public const string Name_OntslagbestemmingAnderZiekenhuis = "OntslagbestemmingAnderZiekenhuis";

        // '' (1: String)
        public const int ID_OntslagbestemmingOverig = 1705;
        public const string Name_OntslagbestemmingOverig = "OntslagbestemmingOverig";

        // 'Opmerkingen' (1: String)
        public const int ID_OpmerkingenRHD = 1800;
        public const string Name_OpmerkingenRHD = "OpmerkingenRHD";

        // 'Startdatum' (9: Date)
        public const int ID_StartDatumRHD = 1801;
        public const string Name_StartDatumRHD = "StartDatumRHD";

        // 'Verwachte einddatum' (9: Date)
        public const int ID_EindDatumRHD = 1802;
        public const string Name_EindDatumRHD = "EindDatumRHD";

        // 'Medische diagnosen / Medisch beeld / Reden opname' (1: String)
        public const int ID_DiagnoseRHD = 1803;
        public const string Name_DiagnoseRHD = "DiagnoseRHD";

        // 'Voorgeschiedenis' (1: String)
        public const int ID_VoorgeschiedenisRHD = 1804;
        public const string Name_VoorgeschiedenisRHD = "VoorgeschiedenisRHD";

        // 'Behandeling' (1: String)
        public const int ID_BehandelingRHD = 1805;
        public const string Name_BehandelingRHD = "BehandelingRHD";

        // 'Prognose verwachte ontwikkeling t.a.v. de ziekte of aandoening' (7: DropDownList)
        public const int ID_PrognoseRHD = 1806;
        public const string Name_PrognoseRHD = "PrognoseRHD";

        // 'Noodzakelijk te regelen hulpmiddelen' (10: CheckboxList)
        public const int ID_HulpmiddelenRHD = 1807;
        public const string Name_HulpmiddelenRHD = "HulpmiddelenRHD";

        // 'Toelichting' (1: String)
        public const int ID_ToelichtingRHD = 1808;
        public const string Name_ToelichtingRHD = "ToelichtingRHD";

        // 'Aflevering' (10: CheckboxList)
        public const int ID_AfleveringRHD = 1809;
        public const string Name_AfleveringRHD = "AfleveringRHD";

        // 'Nadere toelichting op de aanvraag' (1: String)
        public const int ID_AanvraagToelichtingRHD = 1810;
        public const string Name_AanvraagToelichtingRHD = "AanvraagToelichtingRHD";

        // '' (1: String)
        public const int ID_HulpmiddelenAndersRHD = 1811;
        public const string Name_HulpmiddelenAndersRHD = "HulpmiddelenAndersRHD";

        // '' (1: String)
        public const int ID_AfleveringAndersRHD = 1812;
        public const string Name_AfleveringAndersRHD = "AfleveringAndersRHD";

        // 'Wettelijk kader / financieringsregeling' (7: DropDownList)
        public const int ID_AfterCareFinancing = 1813;
        public const string Name_AfterCareFinancing = "AfterCareFinancing";

        // 'Huishoudelijke onderst.' (6: Checkbox)
        public const int ID_WMOHuishoudelijkeondersteuning = 1900;
        public const string Name_WMOHuishoudelijkeondersteuning = "WMOHuishoudelijkeondersteuning";

        // '' (6: Checkbox)
        public const int ID_WMOHuishoudelijkeondersteuningAttachment = 1901;
        public const string Name_WMOHuishoudelijkeondersteuningAttachment = "WMOHuishoudelijkeondersteuningAttachment";

        // 'Geldig van' (9: Date)
        public const int ID_WMOHuishoudelijkeondersteuningFrom = 1902;
        public const string Name_WMOHuishoudelijkeondersteuningFrom = "WMOHuishoudelijkeondersteuningFrom";

        // 'Geldig tot' (9: Date)
        public const int ID_WMOHuishoudelijkeondersteuningUntil = 1903;
        public const string Name_WMOHuishoudelijkeondersteuningUntil = "WMOHuishoudelijkeondersteuningUntil";

        // 'Begeleiding' (6: Checkbox)
        public const int ID_WMOBegeleiding = 1904;
        public const string Name_WMOBegeleiding = "WMOBegeleiding";

        // '' (6: Checkbox)
        public const int ID_WMOBegeleidingAttachment = 1905;
        public const string Name_WMOBegeleidingAttachment = "WMOBegeleidingAttachment";

        // 'Geldig van' (9: Date)
        public const int ID_WMOBegeleidingFrom = 1906;
        public const string Name_WMOBegeleidingFrom = "WMOBegeleidingFrom";

        // 'Geldig tot' (9: Date)
        public const int ID_WMOBegeleidingUntil = 1907;
        public const string Name_WMOBegeleidingUntil = "WMOBegeleidingUntil";

        // 'Jeugdzorg' (6: Checkbox)
        public const int ID_WMOJeugdzorg = 1908;
        public const string Name_WMOJeugdzorg = "WMOJeugdzorg";

        // '' (6: Checkbox)
        public const int ID_WMOJeugdzorgAttachment = 1909;
        public const string Name_WMOJeugdzorgAttachment = "WMOJeugdzorgAttachment";

        // 'Geldig van' (9: Date)
        public const int ID_WMOJeugdzorgFrom = 1910;
        public const string Name_WMOJeugdzorgFrom = "WMOJeugdzorgFrom";

        // 'Geldig tot' (9: Date)
        public const int ID_WMOJeugdzorgUntil = 1911;
        public const string Name_WMOJeugdzorgUntil = "WMOJeugdzorgUntil";

        // 'Bestaande zorg' (5: RadioButton)
        public const int ID_ZorgsoortBestaandeZorg = 1912;
        public const string Name_ZorgsoortBestaandeZorg = "ZorgsoortBestaandeZorg";

        // 'Versturende afdeling' (8: IDText)
        public const int ID_SourceOrganizationDepartment = 1913;
        public const string Name_SourceOrganizationDepartment = "SourceOrganizationDepartment";

        // 'Actuele VVT' (8: IDText)
        public const int ID_DestinationHealthCareProvider = 1914;
        public const string Name_DestinationHealthCareProvider = "DestinationHealthCareProvider";

        // 'Actuele ind. CIZ organisatie' (8: IDText)
        public const int ID_DestinationII = 1915;
        public const string Name_DestinationII = "DestinationII";

        // 'VVT vrije invoer' (1: String)
        public const int ID_ZorgInZorginstellingVoorkeurPatientOverig = 1916;
        public const string Name_ZorgInZorginstellingVoorkeurPatientOverig = "ZorgInZorginstellingVoorkeurPatientOverig";

        // 'Informatie tbv Huisarts-bericht' (1: String)
        public const int ID_InformatieHuisartsenbericht = 1917;
        public const string Name_InformatieHuisartsenbericht = "InformatieHuisartsenbericht";

        // 'Geplande OK datum' (9: Date)
        public const int ID_DatumEindeBehandelingMedischSpecialistOK = 1918;
        public const string Name_DatumEindeBehandelingMedischSpecialistOK = "DatumEindeBehandelingMedischSpecialistOK";

        // 'Preoperatieve screening datum' (9: Date)
        public const int ID_PreoperatieveScreeningDatum = 1919;
        public const string Name_PreoperatieveScreeningDatum = "PreoperatieveScreeningDatum";

        // 'VVT in wachtrij plaatsen' (8: IDText)
        public const int ID_QueuedHealthCareProvider = 1920;
        public const string Name_QueuedHealthCareProvider = "QueuedHealthCareProvider";

        // 'Actuele ind. GRZ organisatie' (8: IDText)
        public const int ID_DestinationGRZ = 1921;
        public const string Name_DestinationGRZ = "DestinationGRZ";

        // 'Medische en/of verpleegkundige zorg na opname is noodzakelijk wegens' (10: CheckboxList)
        public const int ID_ELVZorgNaOpnameNoodzakelijkWegens = 2000;
        public const string Name_ELVZorgNaOpnameNoodzakelijkWegens = "ELVZorgNaOpnameNoodzakelijkWegens";

        // 'Deze (medische) zorg is thuis niet mogelijk wegens' (10: CheckboxList)
        public const int ID_ELVZorgThuisNietMogelijkWegens = 2001;
        public const string Name_ELVZorgThuisNietMogelijkWegens = "ELVZorgThuisNietMogelijkWegens";

        // 'Anders' (1: String)
        public const int ID_ELVZorgThuisNietMogelijkWegensVrijeTekst = 2002;
        public const string Name_ELVZorgThuisNietMogelijkWegensVrijeTekst = "ELVZorgThuisNietMogelijkWegensVrijeTekst";

        // 'De cliënt heeft blijvend permanent toezicht of 24 uur per dag zorg in de nabijheid nodig' (5: RadioButton)
        public const int ID_ELVZorgBehoefteVerwachting = 2003;
        public const string Name_ELVZorgBehoefteVerwachting = "ELVZorgBehoefteVerwachting";

        // 'Er is onvoldoende sprake van haalbare revalidatie doelen wegens tekort aan' (10: CheckboxList)
        public const int ID_ELVOnvoldoendeRevalidatieDoelenVanwegeTekort = 2004;
        public const string Name_ELVOnvoldoendeRevalidatieDoelenVanwegeTekort = "ELVOnvoldoendeRevalidatieDoelenVanwegeTekort";

        // 'Is sprake van een enkelvoudige of meervoudige aandoening of beperking' (5: RadioButton)
        public const int ID_ELVBeinvloedendeMeervoudigeProblematiek = 2005;
        public const string Name_ELVBeinvloedendeMeervoudigeProblematiek = "ELVBeinvloedendeMeervoudigeProblematiek";

        // 'Patiënt heeft een WLZ indicatie (verzilverd / niet verzilverd), of een WMO  beschikking beschermd wonen' (5: RadioButton)
        public const int ID_ELVWlzIndicatieAanwezig = 2006;
        public const string Name_ELVWlzIndicatieAanwezig = "ELVWlzIndicatieAanwezig";

        // 'Conclusie van de afweging ELV' (5: RadioButton)
        public const int ID_ELVConclusieAfweging = 2007;
        public const string Name_ELVConclusieAfweging = "ELVConclusieAfweging";

        // 'Datum afweging' (9: Date)
        public const int ID_ELVDatumAfweging = 2008;
        public const string Name_ELVDatumAfweging = "ELVDatumAfweging";

        // 'Ingevuld door' (7: DropDownList)
        public const int ID_ELVIngevuldDoor = 2009;
        public const string Name_ELVIngevuldDoor = "ELVIngevuldDoor";

        // 'Anders' (1: String)
        public const int ID_ELVOnvoldoendeRevalidatieDoelenVanwegeTekortVrijeTekst = 2010;
        public const string Name_ELVOnvoldoendeRevalidatieDoelenVanwegeTekortVrijeTekst = "ELVOnvoldoendeRevalidatieDoelenVanwegeTekortVrijeTekst";

        // 'Patiënt  heeft volgens behandelend arts een levensverwachting van minder dan 3 maanden' (5: RadioButton)
        public const int ID_ELVLevensVerwachtingMinderDan3Mnd = 2011;
        public const string Name_ELVLevensVerwachtingMinderDan3Mnd = "ELVLevensVerwachtingMinderDan3Mnd";

        // 'Land' (7: DropDownList)
        public const int ID_DICALand = 2012;
        public const string Name_DICALand = "DICALand";

        // 'Indicatie is verzilverd voor een WLZ zorgzwaartepakket / zorgprofiel' (5: RadioButton)
        public const int ID_ELVIndicatieVerzilverd = 2013;
        public const string Name_ELVIndicatieVerzilverd = "ELVIndicatieVerzilverd";

        // 'WLZ indicatie is voor een laag (1-3) ZZP of hoog ZZP/zorgprofiel en verblijft thuis' (5: RadioButton)
        public const int ID_ELVIndicatieZZPProfiel = 2014;
        public const string Name_ELVIndicatieZZPProfiel = "ELVIndicatieZZPProfiel";

        // 'Is de huisarts van cliënt geïnformeerd over de afweging ELV' (5: RadioButton)
        public const int ID_ELVHuisartsGeinformeerd = 2015;
        public const string Name_ELVHuisartsGeinformeerd = "ELVHuisartsGeinformeerd";

        // 'Algemene toelichting' (1: String)
        public const int ID_ELVAlgemeneToelichting = 2016;
        public const string Name_ELVAlgemeneToelichting = "ELVAlgemeneToelichting";

        // '' (11: Hidden)
        public const int ID_ValuesRetrievedFromWebservice = 2100;
        public const string Name_ValuesRetrievedFromWebservice = "ValuesRetrievedFromWebservice";

        // 'Kwetsbare oudere' (5: RadioButton)
        public const int ID_KwetsbareOudereJaNee = 2101;
        public const string Name_KwetsbareOudereJaNee = "KwetsbareOudereJaNee";

        // 'Toelichting' (1: String)
        public const int ID_KwetsbareOudereToelichting = 2102;
        public const string Name_KwetsbareOudereToelichting = "KwetsbareOudereToelichting";

        // 'Initieel gewenste ontslagdatum' (9: Date)
        public const int ID_DatumEindeBehandelingInitieel = 2106;
        public const string Name_DatumEindeBehandelingInitieel = "DatumEindeBehandelingInitieel";

        // 'Infectie/Isolatie' (5: RadioButton)
        public const int ID_InfectieIsolatie = 2107;
        public const string Name_InfectieIsolatie = "InfectieIsolatie";

        // 'Soort infectie' (10: CheckboxList)
        public const int ID_InfectieIsolatieSoortInfectie = 2108;
        public const string Name_InfectieIsolatieSoortInfectie = "InfectieIsolatieSoortInfectie";

        // 'Anders' (6: Checkbox)
        public const int ID_InfectieIsolatieAnders = 2109;
        public const string Name_InfectieIsolatieAnders = "InfectieIsolatieAnders";

        // '' (1: String)
        public const int ID_InfectieIsolatieAndersTekst = 2110;
        public const string Name_InfectieIsolatieAndersTekst = "InfectieIsolatieAndersTekst";

        // 'Soort isolatie' (10: CheckboxList)
        public const int ID_InfectieIsolatieSoortIsolatie = 2111;
        public const string Name_InfectieIsolatieSoortIsolatie = "InfectieIsolatieSoortIsolatie";

        // 'Anders' (6: Checkbox)
        public const int ID_InfectieIsolatieInfectieAnders = 2112;
        public const string Name_InfectieIsolatieInfectieAnders = "InfectieIsolatieInfectieAnders";

        // '' (1: String)
        public const int ID_InfectieIsolatieInfectieAndersTekst = 2113;
        public const string Name_InfectieIsolatieInfectieAndersTekst = "InfectieIsolatieInfectieAndersTekst";

        // 'Oorspronkelijk gewenste ontslagdatum' (9: Date)
        public const int ID_OorspronkelijkeDatumEindeBehandelingMedischSpecialist = 2114;
        public const string Name_OorspronkelijkeDatumEindeBehandelingMedischSpecialist = "OorspronkelijkeDatumEindeBehandelingMedischSpecialist";

        // 'Datum' (9: Date)
        public const int ID_MOCADatum = 2269;
        public const string Name_MOCADatum = "MOCADatum";

        // 'Resultaat' (1: String)
        public const int ID_MOCAResultaat = 2270;
        public const string Name_MOCAResultaat = "MOCAResultaat";

        // 'Resultaat voor opname' (1: String)
        public const int ID_MOCAVoorOpname = 2271;
        public const string Name_MOCAVoorOpname = "MOCAVoorOpname";

        // 'Datum' (9: Date)
        public const int ID_MRCDatum = 2272;
        public const string Name_MRCDatum = "MRCDatum";

        // 'Resultaat' (1: String)
        public const int ID_MRCResultaat = 2273;
        public const string Name_MRCResultaat = "MRCResultaat";

        // 'Resultaat voor opname' (1: String)
        public const int ID_MRCVoorOpname = 2274;
        public const string Name_MRCVoorOpname = "MRCVoorOpname";

        // 'Huisarts' (1: String)
        public const int ID_HuisartsNaamAdres = 2401;
        public const string Name_HuisartsNaamAdres = "HuisartsNaamAdres";

        // 'Functie' (1: String)
        public const int ID_DossierOwnerPosition = 2402;
        public const string Name_DossierOwnerPosition = "DossierOwnerPosition";

        // 'Ingestelde behandeling' (1: String)
        public const int ID_IngesteldeBehandeling = 2404;
        public const string Name_IngesteldeBehandeling = "IngesteldeBehandeling";

        // 'Opdracht naar thuiszorg' (1: String)
        public const int ID_OpdrachtThuiszorg = 2408;
        public const string Name_OpdrachtThuiszorg = "OpdrachtThuiszorg";

        // 'Algemene toelichting' (1: String)
        public const int ID_AlgemeneToelichting = 2409;
        public const string Name_AlgemeneToelichting = "AlgemeneToelichting";

        // 'Woonvorm / steunsysteem' (1: String)
        public const int ID_PsychosocialeAnamnese = 2413;
        public const string Name_PsychosocialeAnamnese = "PsychosocialeAnamnese";

        // 'Huidige zorgcoördinator' (5: RadioButton)
        public const int ID_ZorgCoordinator = 2414;
        public const string Name_ZorgCoordinator = "ZorgCoordinator";

        // 'Zorgorganisatie / toelichting' (1: String)
        public const int ID_ZorgOrganisatieToelichting = 2415;
        public const string Name_ZorgOrganisatieToelichting = "ZorgOrganisatieToelichting";

        // '' (5: RadioButton)
        public const int ID_TerugkoppelingGewenst = 2416;
        public const string Name_TerugkoppelingGewenst = "TerugkoppelingGewenst";

        // 'Terugkoppeling toelichting' (1: String)
        public const int ID_TerugkoppelingGewenstToelichting = 2417;
        public const string Name_TerugkoppelingGewenstToelichting = "TerugkoppelingGewenstToelichting";

        // '' (6: Checkbox)
        public const int ID_HISOvernemenAanvullendOnderzoek = 2418;
        public const string Name_HISOvernemenAanvullendOnderzoek = "HISOvernemenAanvullendOnderzoek";

        // 'Aanvullend onderzoek' (1: String)
        public const int ID_HISAanvullendOnderzoek = 2419;
        public const string Name_HISAanvullendOnderzoek = "HISAanvullendOnderzoek";

        // 'Journaal' (1: String)
        public const int ID_HISEpisodeLijst = 2420;
        public const string Name_HISEpisodeLijst = "HISEpisodeLijst";

        // '' (6: Checkbox)
        public const int ID_HISOvernemenMedicatie = 2421;
        public const string Name_HISOvernemenMedicatie = "HISOvernemenMedicatie";

        // 'Medicatie' (1: String)
        public const int ID_HISMedicatie = 2422;
        public const string Name_HISMedicatie = "HISMedicatie";

        // 'Aanvraag aan Thuiszorg / Verpleeghuis te versturen door' (5: RadioButton)
        public const int ID_AanvraagVersturenType = 2423;
        public const string Name_AanvraagVersturenType = "AanvraagVersturenType";

        // '(Regionaal) coördinatiepunt' (8: IDText)
        public const int ID_AanvraagVersturenDoorTussenOrganisatie = 2424;
        public const string Name_AanvraagVersturenDoorTussenOrganisatie = "AanvraagVersturenDoorTussenOrganisatie";

        // 'Verwijzing type' (7: DropDownList)
        public const int ID_VerwijzingType = 2425;
        public const string Name_VerwijzingType = "VerwijzingType";

        // 'Gezondheidsplan' (1: String)
        public const int ID_HISGezondheidsplan = 2426;
        public const string Name_HISGezondheidsplan = "HISGezondheidsplan";

        // 'Katheter verzorging' (6: Checkbox)
        public const int ID_KatheterVerzorging = 2429;
        public const string Name_KatheterVerzorging = "KatheterVerzorging";

        // 'Toelichting' (1: String)
        public const int ID_KatheterVerzorgingToelichting = 2430;
        public const string Name_KatheterVerzorgingToelichting = "KatheterVerzorgingToelichting";

        // 'Type ontvanger' (5: RadioButton)
        public const int ID_OntvangerType = 2500;
        public const string Name_OntvangerType = "OntvangerType";

        // 'Bestellende organisatie' (1: String)
        public const int ID_BestellerAfdeling = 2501;
        public const string Name_BestellerAfdeling = "BestellerAfdeling";

        // 'Straat' (1: String)
        public const int ID_OntvangerStraat = 2502;
        public const string Name_OntvangerStraat = "OntvangerStraat";

        // 'Bevestiging e-mail van bestelling naar' (1: String)
        public const int ID_OntvangerBevestigingsEmailAdres = 2503;
        public const string Name_OntvangerBevestigingsEmailAdres = "OntvangerBevestigingsEmailAdres";

        // 'Bevestigingsemail van bestelling naar' (5: RadioButton)
        public const int ID_OntvangerBevestigingsEmailTarget = 2504;
        public const string Name_OntvangerBevestigingsEmailTarget = "OntvangerBevestigingsEmailTarget";

        // 'Hulpmiddelen via:' (5: RadioButton)
        public const int ID_SupplierID = 2506;
        public const string Name_SupplierID = "SupplierID";

        // 'Huisnummer' (1: String)
        public const int ID_OntvangerHuisnummer = 2507;
        public const string Name_OntvangerHuisnummer = "OntvangerHuisnummer";

        // 'Toevoeging' (1: String)
        public const int ID_OntvangerHuisnummerExtra = 2508;
        public const string Name_OntvangerHuisnummerExtra = "OntvangerHuisnummerExtra";

        // 'Postcode' (1: String)
        public const int ID_OntvangerPostcode = 2509;
        public const string Name_OntvangerPostcode = "OntvangerPostcode";

        // 'Plaats' (1: String)
        public const int ID_OntvangerPlaats = 2510;
        public const string Name_OntvangerPlaats = "OntvangerPlaats";

        // 'Aflever datum' (9: Date)
        public const int ID_AfleverDatum = 2511;
        public const string Name_AfleverDatum = "AfleverDatum";

        // 'Aflever tijd' (12: ???)
        public const int ID_AfleverTijd = 2512;
        public const string Name_AfleverTijd = "AfleverTijd";

        // '' (6: Checkbox)
        public const int ID_TransportGeduld = 2513;
        public const string Name_TransportGeduld = "TransportGeduld";

        // 'Afleveren bij huisnummer' (1: String)
        public const int ID_TransportHuisnummer = 2514;
        public const string Name_TransportHuisnummer = "TransportHuisnummer";

        // 'Graag half uur van te voren bellen' (1: String)
        public const int ID_TransportBellenVanTeVoren = 2515;
        public const string Name_TransportBellenVanTeVoren = "TransportBellenVanTeVoren";

        // 'Vragen beantwoorden (zie vragenlijst)' (5: RadioButton)
        public const int ID_QuestionsAnswered = 2516;
        public const string Name_QuestionsAnswered = "QuestionsAnswered";

        // 'Voor Zondagen of spoed: Bel vegro of kies een andere datum' (5: RadioButton)
        public const int ID_DateTimeRequirementsMet = 2517;
        public const string Name_DateTimeRequirementsMet = "DateTimeRequirementsMet";

        // 'Orderstatus' (11: Hidden)
        public const int ID_OrderStatus = 2518;
        public const string Name_OrderStatus = "OrderStatus";

        // 'Verzendkosten' (1: String)
        public const int ID_TransportKosten = 2519;
        public const string Name_TransportKosten = "TransportKosten";

        // 'Patientbrief' (1: String)
        public const int ID_Patientbrief = 2520;
        public const string Name_Patientbrief = "Patientbrief";

        // 'Machtigingsformulier' (11: Hidden)
        public const int ID_AuthorizationFormAttachmentID = 2521;
        public const string Name_AuthorizationFormAttachmentID = "AuthorizationFormAttachmentID";

        // 'Toelichting/Extra hulpmiddelen' (1: String)
        public const int ID_ToelichtingHulpmiddelen = 2522;
        public const string Name_ToelichtingHulpmiddelen = "ToelichtingHulpmiddelen";

        // '' (6: Checkbox)
        public const int ID_ToestemmingPatientHuurartikelen = 2523;
        public const string Name_ToestemmingPatientHuurartikelen = "ToestemmingPatientHuurartikelen";

        // 'Burgerlijke staat' (7: DropDownList)
        public const int ID_GezinsSituatieBurgerlijkeStaat = 3000;
        public const string Name_GezinsSituatieBurgerlijkeStaat = "GezinsSituatieBurgerlijkeStaat";

        // 'Gezinssamenstelling' (7: DropDownList)
        public const int ID_GezinsSituatieGezinssamenstelling = 3001;
        public const string Name_GezinsSituatieGezinssamenstelling = "GezinsSituatieGezinssamenstelling";

        // 'Aantal kinderen' (1: String)
        public const int ID_GezinsSituatieAantalKinderen = 3002;
        public const string Name_GezinsSituatieAantalKinderen = "GezinsSituatieAantalKinderen";

        // 'Woning type' (7: DropDownList)
        public const int ID_WoonomgevingWoningType = 3003;
        public const string Name_WoonomgevingWoningType = "WoonomgevingWoningType";

        // 'Toelichting' (1: String)
        public const int ID_WoonomgevingToelichting = 3004;
        public const string Name_WoonomgevingToelichting = "WoonomgevingToelichting";

        // 'Levensovertuiging' (7: DropDownList)
        public const int ID_LevensovertuigingLevensovertuiging = 3005;
        public const string Name_LevensovertuigingLevensovertuiging = "LevensovertuigingLevensovertuiging";

        // 'Sociaal netwerk' (1: String)
        public const int ID_ParticipatieInMaatschappijSociaalNetwerk = 3006;
        public const string Name_ParticipatieInMaatschappijSociaalNetwerk = "ParticipatieInMaatschappijSociaalNetwerk";

        // 'Vrijetijdsbesteding' (1: String)
        public const int ID_ParticipatieInMaatschappijVrijetijdsbesteding = 3007;
        public const string Name_ParticipatieInMaatschappijVrijetijdsbesteding = "ParticipatieInMaatschappijVrijetijdsbesteding";

        // 'Arbeidssituatie' (1: String)
        public const int ID_ParticipatieInMaatschappijArbeidssituatie = 3008;
        public const string Name_ParticipatieInMaatschappijArbeidssituatie = "ParticipatieInMaatschappijArbeidssituatie";

        // 'Toelichting' (1: String)
        public const int ID_ParticipatieInMaatschappijToelichting = 3009;
        public const string Name_ParticipatieInMaatschappijToelichting = "ParticipatieInMaatschappijToelichting";

        // 'Zorgverlener (achternaam)' (1: String)
        public const int ID_HulpVanAnderenHulpverlenerZorgverlener = 3010;
        public const string Name_HulpVanAnderenHulpverlenerZorgverlener = "HulpVanAnderenHulpverlenerZorgverlener";

        // 'Mantelzorger (achternaam)' (1: String)
        public const int ID_HulpVanAnderenHulpverlenerMantelzorger = 3011;
        public const string Name_HulpVanAnderenHulpverlenerMantelzorger = "HulpVanAnderenHulpverlenerMantelzorger";

        // 'Zorgaanbieder' (1: String)
        public const int ID_HulpVanAnderenHulpverlenerZorgaanbieder = 3012;
        public const string Name_HulpVanAnderenHulpverlenerZorgaanbieder = "HulpVanAnderenHulpverlenerZorgaanbieder";

        // 'Aard' (1: String)
        public const int ID_HulpVanAnderenAard = 3013;
        public const string Name_HulpVanAnderenAard = "HulpVanAnderenAard";

        // 'Frequentie' (1: String)
        public const int ID_HulpVanAnderenFrequentie = 3014;
        public const string Name_HulpVanAnderenFrequentie = "HulpVanAnderenFrequentie";

        // 'Soort hulp' (7: DropDownList)
        public const int ID_HulpVanAnderenSoortHulp = 3015;
        public const string Name_HulpVanAnderenSoortHulp = "HulpVanAnderenSoortHulp";

        // 'Communicatie taal' (7: DropDownList)
        public const int ID_CommunicatieCommunicatieTaal = 3017;
        public const string Name_CommunicatieCommunicatieTaal = "CommunicatieCommunicatieTaal";

        // 'Taalvaardigheid begrijpen' (5: RadioButton)
        public const int ID_CommunicatieTaalvaardigheidBegrijpen = 3018;
        public const string Name_CommunicatieTaalvaardigheidBegrijpen = "CommunicatieTaalvaardigheidBegrijpen";

        // 'Taalvaardigheid spreken' (5: RadioButton)
        public const int ID_CommunicatieTaalvaardigheidSpreken = 3019;
        public const string Name_CommunicatieTaalvaardigheidSpreken = "CommunicatieTaalvaardigheidSpreken";

        // 'Taalvaardigheid lezen' (5: RadioButton)
        public const int ID_CommunicatieTaalvaardigheidLezen = 3020;
        public const string Name_CommunicatieTaalvaardigheidLezen = "CommunicatieTaalvaardigheidLezen";

        // 'Voornamen' (1: String)
        public const int ID_PatientVoornamen = 3022;
        public const string Name_PatientVoornamen = "PatientVoornamen";

        // 'Initialen' (1: String)
        public const int ID_PatientInitialen = 3023;
        public const string Name_PatientInitialen = "PatientInitialen";

        // 'Voorvoegsels' (1: String)
        public const int ID_PatientVoorvoegsels = 3024;
        public const string Name_PatientVoorvoegsels = "PatientVoorvoegsels";

        // 'Achternaam' (1: String)
        public const int ID_PatientAchternaam = 3025;
        public const string Name_PatientAchternaam = "PatientAchternaam";

        // 'Voorvoegsels partner' (1: String)
        public const int ID_PatientVoorvoegselsPartner = 3026;
        public const string Name_PatientVoorvoegselsPartner = "PatientVoorvoegselsPartner";

        // 'Achternaam partner' (1: String)
        public const int ID_PatientAchternaamPartner = 3027;
        public const string Name_PatientAchternaamPartner = "PatientAchternaamPartner";

        // 'Straat' (1: String)
        public const int ID_PatientStraat = 3028;
        public const string Name_PatientStraat = "PatientStraat";

        // 'Huisnummer' (1: String)
        public const int ID_PatientHuisnummer = 3029;
        public const string Name_PatientHuisnummer = "PatientHuisnummer";

        // 'Huisnummer toevoeging' (1: String)
        public const int ID_PatientHuisnummertoevoeging = 3030;
        public const string Name_PatientHuisnummertoevoeging = "PatientHuisnummertoevoeging";

        // 'Postcode' (1: String)
        public const int ID_PatientPostcode = 3031;
        public const string Name_PatientPostcode = "PatientPostcode";

        // 'Woonplaats' (1: String)
        public const int ID_PatientWoonplaats = 3032;
        public const string Name_PatientWoonplaats = "PatientWoonplaats";

        // 'Land' (1: String)
        public const int ID_PatientLand = 3033;
        public const string Name_PatientLand = "PatientLand";

        // 'Straat (tijdelijk)' (1: String)
        public const int ID_PatientTijdelijkVerblijfStraat = 3034;
        public const string Name_PatientTijdelijkVerblijfStraat = "PatientTijdelijkVerblijfStraat";

        // 'Huisnummer (tijdelijk)' (1: String)
        public const int ID_PatientTijdelijkVerblijfHuisnummer = 3035;
        public const string Name_PatientTijdelijkVerblijfHuisnummer = "PatientTijdelijkVerblijfHuisnummer";

        // 'Huisnummer toevoeging (tijdelijk)' (1: String)
        public const int ID_PatientTijdelijkVerblijfHuisnummertoevoeging = 3036;
        public const string Name_PatientTijdelijkVerblijfHuisnummertoevoeging = "PatientTijdelijkVerblijfHuisnummertoevoeging";

        // 'Postcode (tijdelijk)' (1: String)
        public const int ID_PatientTijdelijkVerblijfPostcode = 3037;
        public const string Name_PatientTijdelijkVerblijfPostcode = "PatientTijdelijkVerblijfPostcode";

        // 'Woonplaats (tijdelijk)' (1: String)
        public const int ID_PatientTijdelijkVerblijfWoonplaats = 3038;
        public const string Name_PatientTijdelijkVerblijfWoonplaats = "PatientTijdelijkVerblijfWoonplaats";

        // 'Land (tijdelijk)' (1: String)
        public const int ID_PatientTijdelijkVerblijfLand = 3039;
        public const string Name_PatientTijdelijkVerblijfLand = "PatientTijdelijkVerblijfLand";

        // 'Telefoonnummer' (1: String)
        public const int ID_PatientTelefoonnummer = 3040;
        public const string Name_PatientTelefoonnummer = "PatientTelefoonnummer";

        // 'Emailadres' (1: String)
        public const int ID_PatientEmailAdres = 3041;
        public const string Name_PatientEmailAdres = "PatientEmailAdres";

        // 'BSN' (1: String)
        public const int ID_PatientIdentificatienummer = 3042;
        public const string Name_PatientIdentificatienummer = "PatientIdentificatienummer";

        // 'Geboorte datum' (9: Date)
        public const int ID_PatientGeboortedatum = 3043;
        public const string Name_PatientGeboortedatum = "PatientGeboortedatum";

        // 'Geslacht' (5: RadioButton)
        public const int ID_PatientGeslacht = 3044;
        public const string Name_PatientGeslacht = "PatientGeslacht";

        // 'Versturende organisatie' (8: IDText)
        public const int ID_VersturendeAfdeling = 3047;
        public const string Name_VersturendeAfdeling = "VersturendeAfdeling";

        // 'Ontvangende organisatie' (8: IDText)
        public const int ID_OntvangendeAfdeling = 3050;
        public const string Name_OntvangendeAfdeling = "OntvangendeAfdeling";

        // 'Telefoonnummer' (1: String)
        public const int ID_VersturendeContactgegevensTelefoonnummer = 3051;
        public const string Name_VersturendeContactgegevensTelefoonnummer = "VersturendeContactgegevensTelefoonnummer";

        // 'Emailadres' (1: String)
        public const int ID_VersturendeContactgegevensEmailAdres = 3052;
        public const string Name_VersturendeContactgegevensEmailAdres = "VersturendeContactgegevensEmailAdres";

        // 'Telefoonnummer' (1: String)
        public const int ID_VersturendeAdresgegevensStraat = 3053;
        public const string Name_VersturendeAdresgegevensStraat = "VersturendeAdresgegevensStraat";

        // 'Emailadres' (1: String)
        public const int ID_VersturendeAdresgegevensHuisnummer = 3054;
        public const string Name_VersturendeAdresgegevensHuisnummer = "VersturendeAdresgegevensHuisnummer";

        // 'Postcode' (1: String)
        public const int ID_VersturendeAdresgegevensPostcode = 3055;
        public const string Name_VersturendeAdresgegevensPostcode = "VersturendeAdresgegevensPostcode";

        // 'Woonplaats' (1: String)
        public const int ID_VersturendeAdresgegevensWoonplaats = 3056;
        public const string Name_VersturendeAdresgegevensWoonplaats = "VersturendeAdresgegevensWoonplaats";

        // 'Land' (1: String)
        public const int ID_VersturendeAdresgegevensLand = 3057;
        public const string Name_VersturendeAdresgegevensLand = "VersturendeAdresgegevensLand";

        // 'Telefoonnummer' (1: String)
        public const int ID_OntvangendeContactgegevensTelefoonnummer = 3058;
        public const string Name_OntvangendeContactgegevensTelefoonnummer = "OntvangendeContactgegevensTelefoonnummer";

        // 'Emailadres' (1: String)
        public const int ID_OntvangendeContactgegevensEmailAdres = 3059;
        public const string Name_OntvangendeContactgegevensEmailAdres = "OntvangendeContactgegevensEmailAdres";

        // 'Telefoonnummer' (1: String)
        public const int ID_OntvangendeAdresgegevensStraat = 3060;
        public const string Name_OntvangendeAdresgegevensStraat = "OntvangendeAdresgegevensStraat";

        // 'Emailadres' (1: String)
        public const int ID_OntvangendeAdresgegevensHuisnummer = 3061;
        public const string Name_OntvangendeAdresgegevensHuisnummer = "OntvangendeAdresgegevensHuisnummer";

        // 'Postcode' (1: String)
        public const int ID_OntvangendeAdresgegevensPostcode = 3062;
        public const string Name_OntvangendeAdresgegevensPostcode = "OntvangendeAdresgegevensPostcode";

        // 'Woonplaats' (1: String)
        public const int ID_OntvangendeAdresgegevensWoonplaats = 3063;
        public const string Name_OntvangendeAdresgegevensWoonplaats = "OntvangendeAdresgegevensWoonplaats";

        // 'Land' (1: String)
        public const int ID_OntvangendeAdresgegevensLand = 3064;
        public const string Name_OntvangendeAdresgegevensLand = "OntvangendeAdresgegevensLand";

        // 'Organisatie type' (8: IDText)
        public const int ID_VersturendeOrganisatieType = 3065;
        public const string Name_VersturendeOrganisatieType = "VersturendeOrganisatieType";

        // 'Organisatie type' (8: IDText)
        public const int ID_OntvangendeOrganisatieType = 3066;
        public const string Name_OntvangendeOrganisatieType = "OntvangendeOrganisatieType";

        // 'Zorgverlener naam' (8: IDText)
        public const int ID_VersturendeZorgverlenerNaam = 3068;
        public const string Name_VersturendeZorgverlenerNaam = "VersturendeZorgverlenerNaam";

        // 'Zorgverlener telefoonnummer' (1: String)
        public const int ID_VersturendeZorgverlenerTelefoonnummer = 3071;
        public const string Name_VersturendeZorgverlenerTelefoonnummer = "VersturendeZorgverlenerTelefoonnummer";

        // 'Zorgverlener emailadres' (1: String)
        public const int ID_VersturendeZorgverlenerEmailAdres = 3072;
        public const string Name_VersturendeZorgverlenerEmailAdres = "VersturendeZorgverlenerEmailAdres";

        // 'Zorgverlener naam' (8: IDText)
        public const int ID_OntvangendeZorgverlenerNaam = 3075;
        public const string Name_OntvangendeZorgverlenerNaam = "OntvangendeZorgverlenerNaam";

        // 'Zorgverlener telefoonnummer' (1: String)
        public const int ID_OntvangendeZorgverlenerTelefoonnummer = 3078;
        public const string Name_OntvangendeZorgverlenerTelefoonnummer = "OntvangendeZorgverlenerTelefoonnummer";

        // 'Zorgverlener emailadres' (1: String)
        public const int ID_OntvangendeZorgverlenerEmailAdres = 3079;
        public const string Name_OntvangendeZorgverlenerEmailAdres = "OntvangendeZorgverlenerEmailAdres";

        // 'Datum/tijd eerste klacht bekend?' (5: RadioButton)
        public const int ID_DiagnoseEersteKlachtDatumBekend = 3080;
        public const string Name_DiagnoseEersteKlachtDatumBekend = "DiagnoseEersteKlachtDatumBekend";

        // 'Bijkomende problematiek' (10: CheckboxList)
        public const int ID_BijkomendeProblematiek = 3081;
        public const string Name_BijkomendeProblematiek = "BijkomendeProblematiek";

        // 'Bijkomende problematiek toelichting' (1: String)
        public const int ID_BijkomendeProblematiekVrijeTekst = 3082;
        public const string Name_BijkomendeProblematiekVrijeTekst = "BijkomendeProblematiekVrijeTekst";

        // 'Bijzonderheden wat betreft de medische zorg' (10: CheckboxList)
        public const int ID_BijzonderhedenWbtMedischeZorg = 3083;
        public const string Name_BijzonderhedenWbtMedischeZorg = "BijzonderhedenWbtMedischeZorg";

        // 'Heeft de patiënt 24 uurs toezicht nodig dat niet in de thuissituatie kan worden geleverd?' (5: RadioButton)
        public const int ID_PatientHeeft24UurToezichtNodig = 3084;
        public const string Name_PatientHeeft24UurToezichtNodig = "PatientHeeft24UurToezichtNodig";

        // 'Heeft de patiënt een elkaar beïnvloedende aandoening/beperking of meervoudige problematiek?' (5: RadioButton)
        public const int ID_PatientHeeftMeervoudigeProblematiek = 3085;
        public const string Name_PatientHeeftMeervoudigeProblematiek = "PatientHeeftMeervoudigeProblematiek";

        // 'Blijft de medische zorg onder uw verantwoordelijkheid tijdens het eerstelijns verblijf?' (5: RadioButton)
        public const int ID_MedischeZorgOnderUwVerantwoordelijkheid = 3086;
        public const string Name_MedischeZorgOnderUwVerantwoordelijkheid = "MedischeZorgOnderUwVerantwoordelijkheid";

        // 'Wat is de prognose van de patiënt m.b.t. herstel/terugkeer naar huis?' (10: CheckboxList)
        public const int ID_PatientPrognoseHerstelTerugkeerStatus = 3087;
        public const string Name_PatientPrognoseHerstelTerugkeerStatus = "PatientPrognoseHerstelTerugkeerStatus";

        // 'Bij crisisopnameplaatsing' (10: CheckboxList)
        public const int ID_ClientOpDeHoogteBijCrisisopnameplaatsing = 3088;
        public const string Name_ClientOpDeHoogteBijCrisisopnameplaatsing = "ClientOpDeHoogteBijCrisisopnameplaatsing";

        // 'Welke zorg heeft cliënt nodig?' (5: RadioButton)
        public const int ID_ELVClientZorgType = 3089;
        public const string Name_ELVClientZorgType = "ELVClientZorgType";

        // 'Heeft de patient een WLZ zorgprofiel?' (5: RadioButton)
        public const int ID_PatientHeeftWLZZorgprofiel = 3090;
        public const string Name_PatientHeeftWLZZorgprofiel = "PatientHeeftWLZZorgprofiel";

        // '' (5: RadioButton)
        public const int ID_PatientHeeftBezwaarTegenZorgprofielVerificatie = 3091;
        public const string Name_PatientHeeftBezwaarTegenZorgprofielVerificatie = "PatientHeeftBezwaarTegenZorgprofielVerificatie";

        // 'Heeft de patiënt BLIJVEND permanent toezicht en/of 24 uur per dag zorg in de nabijheid nodig omdat terugkeer naar huis niet meer mogelijk is?' (5: RadioButton)
        public const int ID_ELVHAZorgBehoefteVerwachting = 3092;
        public const string Name_ELVHAZorgBehoefteVerwachting = "ELVHAZorgBehoefteVerwachting";

        // 'Toelichting' (1: String)
        public const int ID_ELVHABezwaarTekstWLZZorgProfiel = 3093;
        public const string Name_ELVHABezwaarTekstWLZZorgProfiel = "ELVHABezwaarTekstWLZZorgProfiel";

        // 'Ter beoordeling MEV indien' (5: RadioButton)
        public const int ID_ELVBeoordelingMEV = 3094;
        public const string Name_ELVBeoordelingMEV = "ELVBeoordelingMEV";

        // 'ELV type' (5: RadioButton)
        public const int ID_ELVType = 3095;
        public const string Name_ELVType = "ELVType";

        // 'ZZP nummer' (1: String)
        public const int ID_ZZPNummer = 3096;
        public const string Name_ZZPNummer = "ZZPNummer";

        // 'Dossierhouder' (1: String)
        public const int ID_Dossierhouder = 3097;
        public const string Name_Dossierhouder = "Dossierhouder";

        // 'Medische en/of verpleegkundige zorg is noodzakelijk wegens' (10: CheckboxList)
        public const int ID_MedischeZorgNoodzakelijk = 3098;
        public const string Name_MedischeZorgNoodzakelijk = "MedischeZorgNoodzakelijk";

        // 'De patiënt heeft BLIJVEND permanent toezicht en/of 24 uur per dag zorg in de nabijheid nodig (en er dient direct een WLZ indicatie aangevraagd te worden omdat terugkeer naar huis absoluut niet meer mogelijk is)' (5: RadioButton)
        public const int ID_BlijvendPermanentToezicht = 3099;
        public const string Name_BlijvendPermanentToezicht = "BlijvendPermanentToezicht";

        // 'Conclusie van de afweging betreft een verwijzing naar' (5: RadioButton)
        public const int ID_ConclusieVerwijzing = 3100;
        public const string Name_ConclusieVerwijzing = "ConclusieVerwijzing";

        // 'GRZ (Consult geriater is noodzakelijk)' (5: RadioButton)
        public const int ID_ELVZorgTypeGRZ = 3101;
        public const string Name_ELVZorgTypeGRZ = "ELVZorgTypeGRZ";

        // 'ELV' (5: RadioButton)
        public const int ID_ELVZorgTypeELV = 3102;
        public const string Name_ELVZorgTypeELV = "ELVZorgTypeELV";

        // 'WLZ SOM' (5: RadioButton)
        public const int ID_ELVZorgTypeWLZSOM = 3103;
        public const string Name_ELVZorgTypeWLZSOM = "ELVZorgTypeWLZSOM";

        // 'WLZ PG' (5: RadioButton)
        public const int ID_ELVZorgTypeWLZPG = 3104;
        public const string Name_ELVZorgTypeWLZPG = "ELVZorgTypeWLZPG";

        // '' (5: RadioButton)
        public const int ID_PatientHeeftWLZZorgprofielJa = 3105;
        public const string Name_PatientHeeftWLZZorgprofielJa = "PatientHeeftWLZZorgprofielJa";

        // '' (5: RadioButton)
        public const int ID_PatientHeeftWLZZorgprofielNee = 3106;
        public const string Name_PatientHeeftWLZZorgprofielNee = "PatientHeeftWLZZorgprofielNee";

        // '' (5: RadioButton)
        public const int ID_PatientHeeftWLZZorgprofielOnbekend = 3107;
        public const string Name_PatientHeeftWLZZorgprofielOnbekend = "PatientHeeftWLZZorgprofielOnbekend";


        // 'Voor Zondagen of spoed: Bel MediPoint of kies een andere datum' (7: DropdonwList)
        public const int ID_MediPointTransportAddressRemark = 3108;
        public const string Name_MediPointTransportAddressRemark = "MediPointTransportAddressRemark";

        // 'Voor Zondagen of spoed: Bel MediPoint of kies een andere datum' (7: DropdonwList)
        public const int ID_AfleverTijdMediPoint = 3109;
        public const string Name_AfleverTijdMediPoint = "AfleverTijdMediPoint";
        
        // 'Bevestiging e-mail van bestelling naar Transferpunt'
        public const int ID_OntvangerBevestigingsEmailAdresTransferpunt = 3110;
        public const string Name_OntvangerBevestigingsEmailAdresTransferpunt = "OntvangerBevestigingsEmailAdresTransferpunt";

        // 'Bevestiging e-mail van bestelling naar Patient'
        public const int ID_OntvangerBevestigingsEmailAdresPatient = 3111;
        public const string Name_OntvangerBevestigingsEmailAdresPatient = "OntvangerBevestigingsEmailAdresPatient";

    }

}

