using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Point.Database.Repository;

namespace Point.Database.Models
{
    
    public partial class ClientReceivedTempHL7 : BaseEntity
    {

        // PK staat op 3 kolommen

        [Key, Column(Order=0)]
        public DateTime TimeStamp { get; set; }

        [Key, Column(Order = 1)]
        public string MessageID {get; set;}

        [Key, Column(Order = 2)]
        public int OrganisationID {get; set;}


        public string SenderUserName {get; set;}
        public DateTime? SendDate {get; set;}
        public string PatientNumber {get; set;}
        public string PatientNumberMerge {get; set;}
        public string CivilServiceNumber {get; set;}
        public string Gender {get; set;}
        public string Salutation {get; set;}
        public string Initials {get; set;}
        public string FirstName {get; set;}
        public string MiddleName {get; set;}
        public string LastName {get; set;}
        public string MaidenName {get; set;}
        public DateTime? BirthDate {get; set;}
        public string StreetName {get; set;}
        public string Number {get; set;}
        public string PostalCode {get; set;}
        public string City {get; set;}
        public string Country {get; set;}
        public string PhoneNumberGeneral {get; set;}
        public string PhoneNumberMobile {get; set;}
        public string PhoneNumberWork {get; set;}
        public string ContactPersonName {get; set;}
        public string ContactPersonRelationType {get; set;}
        public string ContactPersonBirthDate {get; set;}
        public string ContactPersonPhoneNumberGeneral {get; set;}
        public string ContactPersonPhoneNumberMobile {get; set;}
        public string ContactPersonPhoneNumberWork {get; set;}
        public string HealthInsuranceCompanyUZOVICode {get; set;}
        public string InsuranceNumber {get; set;}
        public string GeneralPractitionerName {get; set;}
        public string GeneralPractitionerPhoneNumber {get; set;}
        public string HealthCareProvider {get; set;}
        public string MessageType {get; set;}
        public string AddressGPForZorgmail { get; set; }
        public string VisitNumber { get; set; }

        [NotMapped]
        public int? HealthInsuranceCompanyID { get; set; }

        [NotMapped]
        public int ClientReceivedTempHL7ID {get; set;}
    }
}
