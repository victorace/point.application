using Point.Database.Repository;
using Point.Log.Attributes;
using Point.Log.Interfaces;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Point.Database.Models
{
    [Table("OrganizationProject"), Description("Projecten")]
    public partial class OrganizationProject : BaseEntity, IInActive, ILoggable
    {
        [SkipLog]
        public int OrganizationProjectID { get; set; }

        [StringLength(255)]
        [DisplayName("Naam")]
        public string Name { get; set; }

        [SkipLog]
        public int OrganizationID { get; set; }

        [DisplayName("Inactief")]
        public bool InActive { get; set; }
                         
        public virtual Organization Organization { get; set; }
        public DateTime? ModifiedTimeStamp { get; set; }
    }
}
