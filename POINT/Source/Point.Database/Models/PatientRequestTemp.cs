﻿using System;

namespace Point.Database.Models
{
    public class PatientRequestTemp : BaseEntity
    {
        public int PatientRequestTempID { get; set; }
        public DateTime Timestamp { get; set; }
        public Guid GUID { get; set; }
        public string InterfaceCode { get; set; }
        public int OrganizationID { get; set; }
        public string PatientNumber { get; set; }
        public bool Handled { get; set; }
    }
}
