﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Point.Database.Models
{
    public class AidProductOrderItem
    {
        public int AidProductOrderItemID { get; set; }
        public int FormSetVersionID { get; set; }
        public int AidProductID { get; set; }
        public int Quantity { get; set; }

        public virtual FormSetVersion FormSetVersion { get; set; }
        public virtual AidProduct AidProduct { get; set; }
    }
}
