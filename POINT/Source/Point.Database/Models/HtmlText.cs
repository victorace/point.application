using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Point.Database.Repository;

namespace Point.Database.Models
{
    [Table("HtmlText")]
    public partial class HtmlText : BaseEntity 
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int RegionID { get; set; }

        [Column("HtmlText", TypeName = "ntext")]
        public string HtmlText1 { get; set; }

        public int? CreatedByEmployeeID { get; set; }

        public DateTime? CreatedDate { get; set; }

        public int? ModifiedByEmployeeID { get; set; }

        public DateTime? ModifiedDate { get; set; }

    }
}
