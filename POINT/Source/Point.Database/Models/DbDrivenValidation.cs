﻿using System.Collections.Generic;

namespace Point.Database.Models
{
    /// <summary>
    /// Related to Validation table in database.
    /// </summary>
    public class DbDrivenValidation
    {
        public string Method { get; set; }

        public List<int> FieldAttributes { get; set; }

        public List<string> WarningTriggers { get; set; }

        public List<string> Messages { get; set; }

        public int FieldAttributeIndexWithinValidation { get; set; }
    }
}
