﻿using System.ComponentModel.DataAnnotations.Schema;
using Point.Database.Repository;

namespace Point.Database.Models
{
    public class SignaleringDestination
    {
        public int SignaleringDestinationID { get; set; }

        public int LocationID { get; set; }
        [ForeignKey("LocationID")]
        public virtual Location Location { get; set; }

        public int? DepartmentID { get; set; }
        [ForeignKey("DepartmentID")]
        public virtual Department Department { get; set; }
        public int SignaleringID { get; set; }
        [ForeignKey("SignaleringID")]
        public virtual Signalering Signalering { get; set; }
        
    }
}
