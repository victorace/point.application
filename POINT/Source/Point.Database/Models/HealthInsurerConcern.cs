﻿namespace Point.Database.Models
{
    public class HealthInsurerConcern
    {
        public int HealthInsurerConcernID { get; set; }
        public string Name { get; set; }
    }
}
