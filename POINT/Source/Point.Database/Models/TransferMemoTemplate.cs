﻿using Point.Models.Enums;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Point.Database.Models
{
    public class TransferMemoTemplate
    {
        public int TransferMemoTemplateID { get; set; }
        public int OrganizationID { get; set; }

        [DisplayName("Type")]
        public TransferMemoTypeID TransferMemoTypeID { get; set; }

        [Required(ErrorMessage = BaseEntity.errorRequired)]
        [DisplayName("Template naam")]
        public string Name { get; set; }

        [Required(ErrorMessage = BaseEntity.errorRequired)]
        public string TemplateText { get; set; }
    }
}
