﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Point.Models.Enums;

namespace Point.Database.Models
{
    public class PhaseDefinition : BaseEntity
    {
        public int PhaseDefinitionID { get; set; }

        /// <summary>
        /// Check FK
        /// </summary>
        public FlowDefinitionID FlowDefinitionID { get; set; }
        [ForeignKey("FlowDefinitionID")]
        public virtual FlowDefinition FlowDefinition { get; set; }
        public decimal Phase { get; set; }
        [StringLength(255)]
        public string PhaseName { get; set; }
        public int FormTypeID { get; set; }
        [DefaultValue(false)]
        public bool BeginFlow { get; set; }
        [DefaultValue(false)]
        public bool EndFlow { get; set; }
        [StringLength(4000)]
        public string Description { get; set; }
        [StringLength(50)]
        public string Executor { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(100)]
        public string Subcategory { get; set; }
        [StringLength(100)]
        public string Maincategory { get; set; }
        [StringLength(25)]
        public string CodeGroup { get; set; }
        public bool? HasPrintButton { get; set; }
        public bool? ReadOnlyOnDone { get; set; }
        public bool ShowReadOnlyAndEmpty { get; set; }
        public MenuItemType DashboardMenuItemType { get; set; }
        public MenuItemType MenuItemType { get; set; }
        public int OrderNumber { get; set; }
        public int? RedirectToPhaseDefinitionIDOnNext { get; set; }
        public string AccessGroup { get; set; }
        public bool CanResend { get; set; }
        public FlowHandling? FlowHandling { get; set; }

        public virtual ICollection<PhaseDefinitionNavigation> PreviousPhaseDefinitionNavigation { get; set; }
        public virtual ICollection<PhaseDefinitionNavigation> PhaseDefinitionNavigation { get; set; }
        public virtual ICollection<PhaseInstance> PhaseInstance { get; set; }

        [ForeignKey("FormTypeID")]
        public virtual FormType FormType { get; set; }
        
    }
}
