﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Point.Database.Models
{
    [Table("Log4Net")]
    public class Log4Net
    {
        public int ID { get; set; }
        public string Thread { get; set; }
        public int? TransferID { get; set; }
        public string StackTrace { get; set; }
        public DateTime Date { get; set; }
        public string Level { get; set; }
        public string Message { get; set; }
        public string Exception { get; set; }
        public string Url { get; set; }
        public string QueryString { get; set; }
        public string PostData { get; set; }
        public string Cookies { get; set; }
        public string UserAgent { get; set; }
        public string IsAjaxRequest { get; set; }
        public int? EmployeeID { get; set; }
        public string EmployeeUserName { get; set; }
        public string MachineName { get; set; }
        public string WindowsIdentity { get; set; }
        public string ClassName { get; set; }
        public string MethodName { get; set; }
        public int? LineNumber { get; set; }
        public int? ErrorCode { get; set; }
    }
}
