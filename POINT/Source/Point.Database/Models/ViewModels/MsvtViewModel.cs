﻿using System;
using System.Collections.Generic;
using System.Linq;
using Point.Models.Enums;

namespace Point.Database.Models.ViewModels
{
    public class MsvtViewModel : FormTypeSharedViewModel
    {
        public string FrequencyUrl { get; set; }
        public string GenoemdeArtsMedischSpecialist { get; set; }
        public string LoggedInEmployeeFunction { get; set; }
        public string LoggedInEmployeeName { get; set; }
        public bool PrintUVVConditions => Convert.ToBoolean(Global.FlowWebFields.FirstOrDefault(x => x.Name == "PrintVoorwaardenUVV").Value);
        public Rights Rights { get; set; }
        public List<MsvtTreatmentItemViewModel> TreatmentItems { get; set; }
    }
}
