﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Point.Database.Models.ViewModels
{
    public class SecretQuestionViewModel
    {
        public int EmployeeID { get; set; }

        [DisplayName("Emailadres")]
        [Required(ErrorMessage = BaseEntity.errorRequired)]
        public string EmailAdress { get; set; }

        [DisplayName("Vraag")]
        [Required(ErrorMessage = BaseEntity.errorRequired)]
        public string SecretQuestion { get; set; }

        [DisplayName("Antwoord")]
        [Required(ErrorMessage = BaseEntity.errorRequired)]
        public string SecretQuestionAnswer { get; set; }

        [DisplayName("Uw huidige wachtwoord (ter controle)")]
        [Required(ErrorMessage = BaseEntity.errorRequired)]
        public string PasswordCheck { get; set; }

        public bool SecretQuestionEnabled { get; set; }
    }
}
