﻿using System.Collections.Generic;
using System.Linq;

namespace Point.Database.Models.ViewModels
{
    public class RequestFormZHVVTViewModel : FormTypeBaseViewModel
    {
        public RequestFormZHVVTViewModel()
        {
            CurrentSection = new List<string>();
            Sections = new Dictionary<string, List<string>>();
        }
        public int AutoSelectFormTypeID { get; set; } = 0;
        public List<string> CurrentSection { get; set; }
        public string DepartmentName { get; set; }
        public string FormTransferType { get; set; }
        public bool HasActiveNextPhase { get; set; }
        public string LocationName { get; set; }
        public Dictionary<string, List<string>> Sections { get; set; }
        public string SerializedSectionData { get; set; }
        public bool ShowRelatedTransfers { get; set; }
        public bool TakeRelatedTransfer { get; set; }
        public bool ShowResetVVT { get; set; }
        public int Status { get; set; } = (int)Point.Models.Enums.Status.Active;
        public string ZorginzetVoorDezeOpnameTypeZorginstelling { get; set; }
        public string OorspronkelijkeOntslagDatum => Global.FlowWebFields.FirstOrDefault(m => m.Name == "OorspronkelijkeDatumEindeBehandelingMedischSpecialist").Value;
    }
}
