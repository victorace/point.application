﻿using System.Collections.Generic;

namespace Point.Database.Models.ViewModels
{
    public class SignalListViewModel
    {
        public enum ListType
        {
            Global = 0,
            Dossier = 1
        }
        public int OrganizationID{ get; set; }

        public int? FlowInstanceID { get; set; }

        public string DepartmentFilter { get; set; }

        public IEnumerable<Signalering> SignaleringItems { get; set; }
        public ListType SignalListType { get; set; }

        public IEnumerable<SignaleringEmployee> SignaleringEmployees { get; set; }

        public IEnumerable<SignaleringClient> SignaleringClients { get; set; }
    }


    public class SignaleringEmployee
    {
        public Employee Employee { get; set; }
        public string OrganizationName { get; set; }
    }

    public class SignaleringClient 
    {
        public Client Client { get; set; }

        public FlowInstance FlowInstance { get; set; }
    }
}
