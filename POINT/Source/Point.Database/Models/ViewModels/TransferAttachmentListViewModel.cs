﻿using System.Collections.Generic;
using Point.Models.Enums;

namespace Point.Database.Models.ViewModels
{
    public class TransferAttachmentListViewModel
    {
        public IEnumerable<TransferAttachmentViewModel> Items { get; set; }
        public AttachmentSource? AttachmentSource { get; set; }
        public int TransferID { get; set; }
        public int PhaseDefinitionID { get; set; }
        public int PhaseInstanceID { get; set; }
        public int FormSetVersionID { get; set; }
        public int? FrequencyID { get; set; }
        public bool ReadOnly { get; set; }
        public bool ShowTakeOverMessage { get; set; } = false;
    }
}