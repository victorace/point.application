﻿using System.ComponentModel;

namespace Point.Database.Models.ViewModels
{
    public class ClientSearchViewModel
    {
        [DisplayName("Zoeken")]
        public string Search { get; set; }
    }
}
