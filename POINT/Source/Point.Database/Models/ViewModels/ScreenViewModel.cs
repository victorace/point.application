﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Point.Models.Enums;

namespace Point.Database.Models.ViewModels
{
    public class ScreenTypesLog
    {
        public int ScreenTypeID { get; set; }
        public string Name { get; set; }
        public bool IsSelected { get; set; }

    }
    public class SimpleObject
    {
        public int ID { get; set; }
        public string Text { get; set; }
    }
    public class ScreenViewModel
    {
        [DisplayName("Schermid")]
        public int ScreenID { get; set; }

        [DisplayName("Formtype")]
        public int? FormTypeID { get; set; }
        public virtual FormType FormType { get; set; }

        [DisplayName("Generalaction")]
        public int? GeneralActionID { get; set; }
        public virtual GeneralAction GeneralAction { get; set; }
        [StringLength(255)]
        [DisplayName("URL")]
        public string URL { get; set; }
        [StringLength(50)]
        [DisplayName("Schermtype")]
        public string ScreenTypes { get; set; }

        [DisplayName("title")]
        public string ScreenTitle { get; set; }             
        public List<ScreenTypesLog> ScreenTypeLogs { get; set; }
        public string ScreenTypeNames() {
            return string.Join(", ", ScreenTypeLogs?.Where(st => st.IsSelected)?.Select(st => st.Name));            
        }
        public static ScreenViewModel GetViewModel(Screen model)
        {
            return FromModel(model);
        }
        public static List<ScreenViewModel> GetViewModelList(IEnumerable<Screen> modelList)
        {
            var viewmodelScreens = new List<ScreenViewModel>();

            viewmodelScreens=  modelList?.Select(scr => FromModel(scr)).ToList();

            return viewmodelScreens;

        }
        private static ScreenViewModel FromModel(Screen model)
        {
            var viewModel = new ScreenViewModel();
            if (model == null)
            {
                viewModel.ScreenTypeLogs = getScreenTypes("");
            }
            else
            {
                viewModel.ScreenID = model.ScreenID;
                viewModel.URL = model.URL;
                viewModel.ScreenTypes = model.ScreenTypes;
                viewModel.ScreenTitle = model.ScreenTitle;
                viewModel.FormTypeID = model.FormTypeID;
                viewModel.FormType = model.FormType;
                viewModel.GeneralAction = model.GeneralAction;
                viewModel.GeneralActionID = model.GeneralActionID;
                viewModel.ScreenTypeLogs = getScreenTypes(model.ScreenTypes);
            }
            return viewModel;
        }
        private static List<ScreenTypesLog> getScreenTypes(string screenTypes)
        {
            var lst = new List<ScreenTypesLog>();
            var sTypes = screenTypes.Split(',');

            foreach (ScreenType st in Enum.GetValues(typeof(ScreenType)))
            {
                lst.Add(new ScreenTypesLog { ScreenTypeID = (int)st, Name = st.ToString(), IsSelected = (sTypes != null ? sTypes.Contains(((int)st).ToString()) : false) });
            }

            return lst;
        }
    }
}
