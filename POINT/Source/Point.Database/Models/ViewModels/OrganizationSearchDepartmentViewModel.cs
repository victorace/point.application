﻿using Point.Models.Enums;
using System.Collections.Generic;
using System.Linq;

namespace Point.Database.Models.ViewModels
{
    public class OrganizationSearchDepartmentViewModel
    {
        public string RegionName { get; set; }
        public int OrganizationTypeID { get; set; }
        public int OrganizationID { get; set; }
        public int DepartmentID { get; set; }
        public string DepartmentName { get; set; }
        public string City { get; set; }

        public Participation Participation { get; set; }
        public FlowDirection FlowDirection { get; set; }

        public bool IsFavorite { get; set; }
        public bool UseCapacity { get; set; }

        public int? Capacity0 { get; set; }
        public int? Capacity1 { get; set; }
        public int? Capacity2 { get; set; }
        public int? Capacity3 { get; set; }
        

        public static OrganizationSearchDepartmentViewModel FromModel(OrganizationSearch orgsearch, FlowDefinitionID? flowdefinitionid, int[] favoritelocations)
        {
            bool showlocation = !string.IsNullOrEmpty(orgsearch.LocationName) && orgsearch.LocationName != "Locatie 1";
            bool showdepartment = !string.IsNullOrEmpty(orgsearch.DepartmentName) && orgsearch.DepartmentName != "Afdeling 1";
            var participation = orgsearch.OrganizationSearchParticipation.FirstOrDefault(it => it.FlowDefinitionID == flowdefinitionid);

            var viewmodel = new OrganizationSearchDepartmentViewModel();
            viewmodel.OrganizationID = orgsearch.OrganizationID;
            viewmodel.DepartmentID = orgsearch.DepartmentID;
            viewmodel.DepartmentName = orgsearch.OrganizationName
                                         + (showlocation ? (" - " + orgsearch.LocationName) : "")
                                         + (showdepartment ? (" - " + orgsearch.DepartmentName) : "");
            viewmodel.City = orgsearch.City;

            viewmodel.OrganizationTypeID = orgsearch.OrganizationTypeID;
            viewmodel.RegionName = string.Join(", ", orgsearch.OrganizationSearchRegion.Select(it => it.RegionName));
            viewmodel.Participation = (participation?.Participation).GetValueOrDefault();
            viewmodel.IsFavorite = favoritelocations.Contains(orgsearch.LocationID);
            viewmodel.FlowDirection = (participation?.FlowDirection).GetValueOrDefault();

            viewmodel.Capacity0 = orgsearch.Capacity0;
            viewmodel.Capacity1 = orgsearch.Capacity1;
            viewmodel.Capacity2 = orgsearch.Capacity2;
            viewmodel.Capacity3 = orgsearch.Capacity3;

            return viewmodel;
        }


        public static List<OrganizationSearchDepartmentViewModel> FromModelList(IEnumerable<OrganizationSearch> orgsearch, FlowDefinitionID? flowdefinitionid, int[] favoritelocations)
        {
            var viewmodellist = new List<OrganizationSearchDepartmentViewModel>();
            foreach (var item in orgsearch)
            {
                viewmodellist.Add(FromModel(item, flowdefinitionid, favoritelocations));
            }

            return viewmodellist;
        }
    }
}
