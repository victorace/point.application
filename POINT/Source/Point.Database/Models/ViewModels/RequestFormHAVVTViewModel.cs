﻿

namespace Point.Database.Models.ViewModels
{
    public class RequestFormHAVVTViewModel : FormTypeBaseViewModel
    {
        public string DepartmentName { get; set; }
        public string LocationName { get; set; }
    }
}
