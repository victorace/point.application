﻿using System.Collections.Generic;

namespace Point.Database.Models.ViewModels
{
    public class PrintList
    {
        public PrintList()
        {
            ErrorMessage = "";
        }
        public int TransferID { get; set; }

        public Client ClientData { get; set; }
        public List<PrintListType> PrintListTypes { get; set; }
        public string ErrorMessage { get; set; }
    }

    public class PrintListType
    {
        public FormType FormType { get; set; }
        public List<FormSetVersion> FormsetVersions { get; set; }
        
        //Voor bijvoorbeld transferdossier
        public string CombinedFormName { get; set; }
        public List<FormSetVersion> CombinedForms { get; set; }
        public TransferAttachment TransferAttachment { get; set; }

        //...Flowdata
        //...Communicatiejournaal
    }

    public class CombinedForm
    {
        public List<FormSetVersion> FormsetVersions { get; set; }
    }
}
