﻿using System;

namespace Point.Database.Models.ViewModels
{
    public class LoginHistoryItemViewModel
    {
        public string ClientIP { get; set; }
        public string LoginDateTime { get; set; }
    }
}
