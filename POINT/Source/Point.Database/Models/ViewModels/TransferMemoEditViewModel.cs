﻿using Point.Models.Enums;

namespace Point.Database.Models.ViewModels
{
    public class TransferMemoEditViewModel
    {
        public int TransferMemoID { get; set; }
        public string MemoContent { get; set; }
        public TransferMemoTypeID? Target { get; set; }
        public int TransferID { get; set; }
        public int? FrequencyID { get; set; }
        public bool EnableSignalering { get; set; }
        public bool SignaleringChecked { get; set; }
        public int FormTypeID { get; set; }
    }
}