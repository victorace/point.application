﻿using System.Collections.Generic;
using System.Linq;

namespace Point.Database.Models.ViewModels
{
    public class DropdownMultiSelectViewModel
    {
        private static string _fieldName;
        private static string _selectedNoneText;
        internal static string[] _trimSegmentsForShort;
        private const string PrefixForField = "flowField";
        private static bool _isRequired;
        private static string _classNames;
        public DropdownMultiSelectViewModel(string fieldName, string selectedNoneText, string[] trimSegmentsForShort, bool isRequired, string classNames)
        {
            _fieldName = fieldName;
            _selectedNoneText = selectedNoneText;
            _trimSegmentsForShort = trimSegmentsForShort;
            _isRequired = isRequired;
            _classNames = classNames;
            Items = new List<DropdownMultiSelectItem>();
        }

        // The name used for the field which gets posted
        public string FieldName => $"{PrefixForField}{_fieldName}";

        public bool IsRequired => _isRequired;
        public string ClassString => _classNames;
        public List<DropdownMultiSelectItem> Items { get; set; }

        // The comma-separated list of ALL items' DisplayTextShort
        public string DisplayTextShort
        {
            get
            {
                var text = "";
                foreach (var item in Items)
                {
                    text += item.DisplayTextShort + ",";
                }

                return text.TrimEnd(',');
            }
        }


        // To override the default FlowWebField.Name
        public void SetFieldName(string text)
        {
            _fieldName = text;
        }
        public string SelectedNoneText => _selectedNoneText;

        // The text displayed on the control when no selection has been made
        public void SetSelectedNoneText(string text)
        {
            _selectedNoneText = text;
        }
        
        // Accept a set of strings which are to be removed from the DisplayTextFull in the DisplayTextShort
        public void SetTrimSegmentsForShort(string[] segments)
        {
            _trimSegmentsForShort = segments;
        }
        

        public class DropdownMultiSelectItem
        {
            // The full text used to display item in the dropdown segment
            public string DisplayTextFull { get; set; }

            // The item's text used to display all (comma separated) selections in the control
            public string DisplayTextShort
            {
                get
                {
                    return string.IsNullOrEmpty(DisplayTextFull) ? string.Empty : _trimSegmentsForShort.Aggregate(DisplayTextFull, (current, segment) => current.Replace(segment, ""));
                }
            }

            public bool IsSelected { get; set; }

            // Holds the KeyValue of the item as defined in FlowField.DataSource
            public string KeyValue { get; set; }
        }
    }
}
