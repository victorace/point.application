﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace Point.Database.Models.ViewModels
{
    public class MsvtTreatmentItemViewModel
    {
        public static readonly string NAME_PREFIX = "MSVTTI_";
        public static readonly string NAME_FORMAT_REGEXP = @"\[(?<id>\d+?)\]_(?<complex>\w*)_(?<fieldName>\w*)";

        public enum ComplexType
        {
            H = 0,
            L = 1,
            O = 2
        }

        public enum DayWeekMonth
        {
            [Description("Dag")]
            Day = 1,
            [Description("Week")]
            Week = 7,
            [Description("Maand")]
            Month = 30
        }

        public int Id { get; set; }
        public string GroupName { get; set; }
        public int GroupID { get; set; }

        [Description("Freq")]
        public int? Frequency { get; set; }

        public int? AmountUnitFrequency { get; set; }

        [Description("Eenheid")]
        public DayWeekMonth? FrequencyUnit { get; set; }        
        [Description("Handeling")]
        public string Action {get; set;}

        public bool IsCustomAction { get; set; }

        [Description("Definitie")]
        public string Definition { get; set; }
        [Description("Norm tijd")]
        public string NormTime { get; set; }

        public int? AverageNormTime { get; set; }

        [Description("Schatting tijd")]
        public int? ExpectedTime { get; set; }
        [Description("Complex type")]
        public ComplexType Complex { get; set; }
        public bool HasHistory { get; set; }
        public int? FormsetVersionID { get; set; }
        public string FrequencyName => $"{NAME_PREFIX}{GroupID}_[{Id}]_{Complex}_frequency";
        public string DefinitionName => $"{NAME_PREFIX}{GroupID}_[{Id}]_{Complex}_definition";
        public string AmountUnitFrequencyName => $"{NAME_PREFIX}{GroupID}_[{Id}]_{Complex}_amountunitfrequency";
        public string FrequencyUnitName => $"{NAME_PREFIX}{GroupID}_[{Id}]_{Complex}_freqencyunit";
        public string ExpectedTimeName => $"{NAME_PREFIX}{GroupID}_[{Id}]_{Complex}_expectedtime";

        public static List<MsvtTreatmentItemViewModel> GetByModelList(IEnumerable<ActionCodeHealthInsurer> actions, List<ActionHealthInsurer> values, List<MutActionHealthInsurer> history = null )
        {
            var list = new List<MsvtTreatmentItemViewModel>();
            foreach (var action in actions)
            {
                var treatmentitem = new MsvtTreatmentItemViewModel();
                treatmentitem.Id = action.ActionCodeHealthInsurerID;
                treatmentitem.Action = action.ActionCodeName;
                treatmentitem.IsCustomAction = String.IsNullOrEmpty(action.ActionCodeName);
                treatmentitem.GroupName = action.ActionGroupHealthInsurer.ActionGroupName;
                treatmentitem.GroupID = action.ActionGroupHealthInsurerID.GetValueOrDefault(0);

                if (action.Complex == "H")
                    treatmentitem.Complex = ComplexType.H;
                if (action.Complex == "L")
                    treatmentitem.Complex = ComplexType.L;
                if (action.Complex == "O")
                    treatmentitem.Complex = ComplexType.O;

                if (action.TimeLimitMin.HasValue)
                {
                    treatmentitem.NormTime = action.TimeLimitMin.Value.ToString();
                    if (action.TimeLimitMax.HasValue && action.TimeLimitMax > action.TimeLimitMin)
                        treatmentitem.NormTime += "-" + action.TimeLimitMax;

                    treatmentitem.AverageNormTime = action.TimeLimitMin;
                    if (action.TimeLimitMax.HasValue)
                        treatmentitem.AverageNormTime += (action.TimeLimitMax - action.TimeLimitMin) / 2;
                }

                var actionvalue = values?.FirstOrDefault(val => val.ActionCodeHealthInsurerID == action.ActionCodeHealthInsurerID);
                if (actionvalue != null)
                {
                    treatmentitem.Frequency = actionvalue.Amount;
                    treatmentitem.AmountUnitFrequency = actionvalue.UnitFrequency;
                    treatmentitem.ExpectedTime = actionvalue.ExpectedTime;
                    treatmentitem.Definition = actionvalue.Definition;

                    if (actionvalue.Unit == "dag")
                        treatmentitem.FrequencyUnit = DayWeekMonth.Day;
                    if (actionvalue.Unit == "week")
                        treatmentitem.FrequencyUnit = DayWeekMonth.Week;
                    if (actionvalue.Unit == "maand")
                        treatmentitem.FrequencyUnit = DayWeekMonth.Month;

                    if (history != null)
                    {
                        treatmentitem.HasHistory = history.Any(h => 
                            h.ActionCodeHealthInsurerID == treatmentitem.Id && h.Amount != null);
                        if (treatmentitem.HasHistory)
                            treatmentitem.FormsetVersionID = actionvalue.FormSetVersionID;
                    }
                }

                list.Add(treatmentitem);
            }

            return list;
        }
    }

    
}
