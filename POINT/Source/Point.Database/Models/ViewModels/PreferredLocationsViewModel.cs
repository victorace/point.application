﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Point.Database.Models.ViewModels
{
    public class PreferredLocationsViewModel
    {
        public List<Option> Regions { get; set; }
        public List<Option> Locations { get; set; }
        public List<PreferredLocationViewModel> PreferredLocations { get; set; }

        [Display(Name = "Geselecteerde regio")]
        public int SelectedRegionID { get; set; }
        [Display(Name = "Geselecteerde locatie")]
        public int SelectedLocationID { get; set; }
        [Display(Name = "Favoriete locaties")]
        public int[] FavoriteLocationIDs { get; set; }
    }

    public class PreferredLocationViewModel
    {
        public int LocationID { get; set; }

        public int PreferredLocationID { get; set; }

        public string FullName { get; set; }

        public bool Inactive { get; set; } // Organization and/or Location is marked as Inactive
    }
}
