﻿
using System;
using System.Collections.Generic;

namespace Point.Database.Models.ViewModels
{
    public class LoginHistoryViewModel
    {
        public LoginHistoryViewModel()
        {
            Items = new List<LoginHistoryItemViewModel>();
        }

        public IList<LoginHistoryItemViewModel> Items { get; set; }
        public DateTime? LastLogin { get; set; }
        public string Title { get; set; }
        
    }

}
