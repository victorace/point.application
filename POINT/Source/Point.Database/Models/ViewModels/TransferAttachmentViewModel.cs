﻿using Point.Models.Enums;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Point.Database.Models.ViewModels
{
    public class TransferAttachmentViewModel : IEditable
    {
        public int AttachmentID { get; set; }
        public int TransferID { get; set; }
        public int EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public int OrganizationID { get; set; }
        public string OrganizationName { get; set; }
        public DateTime DateTime { get; set; }        
        public string FileName { get; set; }
        public int? FrequencyID { get; set; }
        [StringLength(50), DisplayName("Naam"), Required]
        public string Name { get; set; }
        [DisplayName("Beschrijving"), Column(TypeName = "text")]
        public string Description { get; set; }
        public bool ShowInAttachmentList { get; set; }
        public AttachmentSource AttachmentSource { get; set; }
        [DisplayName("Type"), Required]
        public AttachmentTypeID AttachmentTypeID { get; set; }
        public bool IsEditable { get; set; }
        public bool AttachedAfterClose { get; set; } = false;
        public bool SignalOnSave { get; set; }
        public bool OnlyPDF { get; set; } = false;
        public int? RelatedAttachmentID { get; set; }        
        public string RelatedAttachmentOwner { get; set; }
    }
}