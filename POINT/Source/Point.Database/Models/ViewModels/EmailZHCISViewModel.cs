﻿using Point.Models.Enums;

namespace Point.Database.Models.ViewModels
{
    public class EmailZHCISViewModel : ICommunicationLog
    {
        public bool IsAltered { get; set; }

        public string RecievingOrganization { get; set; }
        public string RecievingEmailAdress { get; set; }
        
        public string SendingUser { get; set; }
        public string SendingLocation { get; set; }
        public string SendingTelephoneNumber { get; set; }

        public int TransferID { get; set; }
        public string DossierUrl { get; set; }
        public string PatientName { get; set; }

        public int? EmployeeID { get; set; }
        public int? OrganizationID { get; set; }

        public CommunicationLogType MailType { get; set; }
    }
}
