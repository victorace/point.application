﻿using System.ComponentModel;

namespace Point.Database.Models.ViewModels
{
    public class ContactInformationViewModel
    {
        [DisplayName("Naam")]
        public string Name { get; set; }

        [DisplayName("Naam")]
        public string FullName { get; set; }

        [DisplayName("Straat")]
        public string StreetName { get; set; }

        [DisplayName("Huisnummer")]
        public string Number { get; set; }

        [DisplayName("Postcode")]
        public string PostalCode { get; set; }

        [DisplayName("Plaats")]
        public string City { get; set; }

        [DisplayName("Telefoonnummer")]
        public string PhoneNumber { get; set; }

        [DisplayName("Fax")]
        public string FaxNumber { get; set; }

        [DisplayName("E-mail")]
        public string EmailAddress { get; set; }
    }
}
