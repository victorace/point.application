﻿namespace Point.Database.Models.ViewModels
{
    public class FlowInstanceSearchViewModel
    {
        public FlowInstanceSearchValues FlowInstanceSearchValues { get; set; }
        public FlowInstanceDoorlopendDossierSearchValues FlowInstanceDoorlopendDossierSearchValues { get; set; }
        public bool Anonymous { get; set; }
    }
}
