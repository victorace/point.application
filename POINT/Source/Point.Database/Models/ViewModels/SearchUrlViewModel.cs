using Point.Models.Enums;
using System.Collections.Generic;

namespace Point.Database.Models.ViewModels
{
    public class SearchUrlViewModel
    {
        public SearchUrlViewModel()
        {
            Status = DossierStatus.Active;
            Type = DossierType.All;
            DepartmentIDs = new int[] { };
            SearchType = SearchType.Transfer;
            FrequencyStatus = FrequencyDossierStatus.Active;
            Handling = DossierHandling.All;
        }

        public bool Reset { get; set; }
        public FlowDirection FlowDirection { get; set; }
        public FlowDefinitionID[] FlowDefinitionsSender { get; set; }
        public FlowDefinitionID[] FlowDefinitionsReceiver { get; set; }
        public DossierStatus Status { get; set; }
        public DossierType Type { get; set; }
        public DossierHandling Handling { get; set; }
        public int[] DepartmentIDs { get; set; }
        public SearchType SearchType { get; set; }
        public FrequencyDossierStatus FrequencyStatus { get; set; }

        public FlowDefinitionID[] FlowDefinitionIDs
        {
            get
            {
                var flowdefinitionids = new List<FlowDefinitionID>();

                if (FlowDefinitionsReceiver != null)
                    flowdefinitionids.AddRange(FlowDefinitionsReceiver);
                if (FlowDefinitionsSender != null)
                    flowdefinitionids.AddRange(FlowDefinitionsSender);

                return flowdefinitionids.ToArray();
            }
        }
    }
}
