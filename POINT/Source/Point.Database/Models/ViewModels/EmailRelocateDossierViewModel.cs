﻿namespace Point.Database.Models.ViewModels
{
    public class EmailRelocateDossierViewModel : ICommunicationLog
    {
        public string ReceivingEmailAdress { get; set; }
        public string ReceivingTransferPointEmailAdress { get; set; }

        public string SourceDepartment { get; set; }
        public string DestinationDepartment { get; set; }
        public string RelocateReason { get; set; }

        public string ClientName { get; set; }
        public string DossierUrl { get; set; }

        public string ResponsibleEmployee { get; set; }
        public string ResponsibleDepartment { get; set; }
        public string ResponsibleTelephoneNumber { get; set; }

        public int? EmployeeID { get; set; }
        public int? OrganizationID { get; set; }
    }
}
