﻿namespace Point.Database.Models.ViewModels
{
    public class LocalMenuItem
    {
        public string Action { get; set; }
        public string Controller { get; set; }
        public string Url { get; set; }
        public string Label { get; set; }
        public string MenuType { get; set; }
        public string Identifier { get; set; }
        public string Description { get; set; }
        public int? OrderNumber { get; set; }
        public string Subcategory { get; set; }
        public bool Disabled { get; set; }
        public bool Hidden { get; set; }
        public bool IsCurrent { get; set; }
        public string Glyphicon { get; set; }
        public string CssClass { get; set; } = "";
        public string OnClick { get; set; } = "";
        public bool OpenNewWindow { get; set; }
    }
}
