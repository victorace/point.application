namespace Point.Database.Models
{
    public enum ReadValueType
    {
        Unspecified = 0,
        WellKnown,
        LookedUp,
        Copied
    }
}
