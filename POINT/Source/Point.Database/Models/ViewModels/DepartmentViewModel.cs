﻿using Point.Database.Attributes;
using Point.Models.Enums;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Point.Database.Models.ViewModels
{
    public class DepartmentViewModel
    {
        public DepartmentViewModel()
        {
            AutoCreateSetIDs = new List<int>();
            LocationAutoCreateSets = new List<AutoCreateSet>();
        }

        public int DepartmentID { get; set; }

        [Required(ErrorMessage = BaseEntity.errorRequired)]
        [StringLength(255)]
        [DisplayName("Naam")]
        public string Name { get; set; }

        [Required(ErrorMessage = BaseEntity.errorRequired)]
        [DisplayName("Organisatie")]
        public int OrganizationID { get; set; }

        [Required(ErrorMessage = BaseEntity.errorRequired)]
        [DisplayName("Locatie")]
        public int LocationID { get; set; }

        [Required(ErrorMessage = BaseEntity.errorRequired)]
        [DisplayName("Afdeling type")]
        public DepartmentTypeID DepartmentTypeID { get; set; }

        [Required(ErrorMessage = BaseEntity.errorRequired)]
        [DisplayName("Specialisme / Nazorgtype 1")]
        public int AfterCareType1ID { get; set; }

        [StringLength(50)]
        [DisplayName("Telefoonnummer")]
        public string PhoneNumber { get; set; }

        [StringLength(50)]
        [DisplayName("Faxnummer")]
        public string FaxNumber { get; set; }

        [StringLength(1024)]
        [DisplayName("E-mailadres t.b.v. aanvraag")]
        [EmailAddressValidator]
        public string EmailAddress { get; set; }
        public bool RecieveEmail { get; set; }

        [DisplayName("Revalidatie Centrum")]
        public bool RehabilitationCenter { get; set; }

        [DisplayName("Geriatrisch Revalidatie Centrum")]
        public bool IntensiveRehabilitationNursinghome { get; set; }

        [StringLength(1024)]
        [DisplayName("E-mailadres t.b.v. VO-notificatie:")]
        [EmailAddressValidator]
        public string EmailAddressTransferSend { get; set; }
        public bool RecieveEmailTransferSend { get; set; }

        [StringLength(1024)]
        [DisplayName("Digitaal versturen VO")]
        [WriteAccess(FunctionRoleLevelID.Global, FunctionRoleTypeID.ManageOrganization)]
        public string ZorgmailTransferSend { get; set; }

        [DisplayName("Digitaal versturen VO Aan/Uit")]
        [WriteAccess(FunctionRoleLevelID.Global, FunctionRoleTypeID.ManageOrganization)]
        public bool RecieveZorgmailTransferSend { get; set; }

        [DisplayName("Digitaal versturen CDA")]
        [WriteAccess(FunctionRoleLevelID.Global, FunctionRoleTypeID.ManageOrganization)]
        public bool RecieveCDATransferSend { get; set; }

        [DisplayName("Tonen op capaciteitscherm")]
        public bool CapacityFunctionality { get; set; }

        [DisplayName("Inactief")]
        public bool Inactive { get; set; }

        public bool EnableServiceAreas { get; set; }

        [StringLength(12)]
        [DisplayName("Zorgmail adres")]
        public string AddressGPForZorgmail { get; set; }

        [DisplayName("AGB code")]
        public string AGB { get; set; }

        [DisplayName("Sorteervolgorde")]
        public int? SortOrder { get; set; }
        [DisplayName("Extern ID")]
        public string ExternID { get; set; }

        [DisplayName("Autocreate Set(s)")]
        public List<int> AutoCreateSetIDs { get; set; }
        public List<AutoCreateSet> LocationAutoCreateSets { get; set; }

        public FunctionRoleTypeID FunctionRoleTypeID = FunctionRoleTypeID.ManageOrganization;

        public virtual Organization Organization { get; set; }
        public virtual Location Location { get; set; }

        public virtual AfterCareType AfterCareType1 { get; set; }

        public List<FlowParticipationViewModel> FlowParticipation { get; set; }
        public bool ShowCapacityFunctionality { get; set; }
        public List<Option> Organizations { get; set; } = new List<Option>();
        public List<Option> Locations { get; set; } = new List<Option>();
        public List<Option> DepartmentTypes { get; set; } = new List<Option>();
        public List<Option> AfterCareTypes1 { get; set; } = new List<Option>();
        public FunctionRoleLevelID FunctionRoleLevelID { get; set; }
    }
}