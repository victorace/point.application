﻿using System.ComponentModel.DataAnnotations;

namespace Point.Database.Models.ViewModels
{
    public class LocationSearch
    {
        [Required]
        public int OrganisationID { get; set; }
        [Required]
        public string OrganisationName { get; set; }

        [Required]
        public int LocationID { get; set; }
        [Required]
        public string LocationName { get; set; }

        public int DepartmentID { get; set; }
        public string DepartmentName { get; set; }
    }
}
