﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Point.Database.Models.ViewModels
{
    public class LastFeedbackMemoViewModel
    {
        public string DateTimeString { get; set; }
        public string MemoContent { get; set; }
        public string EmployeeName { get; set; }
    }
}
