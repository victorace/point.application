﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Point.Database.Models.ViewModels
{
    public class CopyOfTransferViewModel
    {
        public int TransferID { get; set; }
        public string DateTimeCreated { get; set; }
    }
}
