﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Point.Database.Models.ViewModels
{
	public class MedicationItemViewModel
	{
		public static readonly string NAME_PREFIX = "MEDITEM_";
		public static readonly string NAME_FORMAT_REGEXP = @"(?<nr>\d+?)_(?<fieldName>\w*)";

		public enum DayWeekMonth
		{
			[Description("Dag")]
			Day = 0,
			[Description("Week")]
			Week = 1,
			[Description("Maand")]
			Month = 2
		}

		[Description("Benodigde medicatie")]
        [StringLength(100)]
		public string Medication { get; set; }

		[Description("Dosering")]
        [StringLength(50)]
		public string Dosage { get; set; }

		[Description("Toediening per")]
		public string DeliveryMethod { get; set; }

		[Description("Frequentie / tijden")]
        [StringLength(50)]
		public string Frequency { get; set; }

        private int Index { get; set; }
        public string MedicationName => $"{NAME_PREFIX}{Index}_medication";
	    public string DosageName => $"{NAME_PREFIX}{Index}_dosage";
	    public string FrequencyName => $"{NAME_PREFIX}{Index}_frequency";
	    public string DeliveryMethodName => $"{NAME_PREFIX}{Index}_deliverymethod";

        public static List<MedicationItemViewModel> GetByModelList(List<MedicineCard> values, int totalamount = 5)
		{
			var medicationList = new List<MedicationItemViewModel>();
		    var index = 0;
            for (var mi = 0; mi < totalamount; mi++)
            {
                medicationList.Add(new MedicationItemViewModel {Index = mi});
                index++;
            }

		    if (values == null)
		    {
		        return medicationList;
		    }
		    for (var i = 0; i < values.Count; i++)
		    {
		        var value = values[i];
		        if (medicationList.Count == i)
		        {
		            medicationList.Add(new MedicationItemViewModel {Index = index});
		            index++;
		        }                       

		        var item = medicationList[i];
		        item.Dosage = value.Dosage;
		        item.Frequency = value.Frequence;
		        item.Medication = value.NotRegisteredMedicine;
		        item.DeliveryMethod = value.Prescription;
		    }

		    return medicationList;
		}
	}
}
