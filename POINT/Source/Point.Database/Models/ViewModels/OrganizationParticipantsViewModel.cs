﻿using Point.Models.Enums;
using System.Collections.Generic;

namespace Point.Database.Models.ViewModels
{
    public class OrganizationParticipantsViewModel
    {
        public bool DoRegionSearch { get; set; }
        public string OrderBy { get; set; } = "OrganizationName asc";
        public Pager Pager { get; set; } = new Pager() { PageNo = 1 };
        public IEnumerable<OrganizationSearch> Participants { get; set; }
        public int RegionID { get; set; }

        public OrganizationTypeID OrganizationTypeID { get; set; } = OrganizationTypeID.HealthCareProvider;
        public string Search { get; set; }

        public bool IsOrderDescending => OrderBy.ToLowerInvariant().EndsWith("desc");
        public bool IsOrderByAnyFlowReceiving => OrderBy.Contains("AnyFlowReceiving");
        public bool IsOrderByAnyFlowSending => OrderBy.Contains("AnyFlowSending");
        public bool IsOrderByRegionName => OrderBy.Contains("RegionName");
    }
    
}
