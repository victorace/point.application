﻿using System.ComponentModel.DataAnnotations;

namespace Point.Database.Models.ViewModels
{
    public class TemplateExampleViewModel
    {
        [Required]
        public int TransferID { get; set; }
        [Required]
        public int TemplateTypeID { get; set; }
        public string TemplateText { get; set; }
    }
}