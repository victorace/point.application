﻿using System.ComponentModel;

namespace Point.Database.Models.ViewModels
{
    public class ClientCareFormViewModel : FormTypeBaseViewModel
    {
        [DisplayName("Patient/Cliënt")]
        public Client Client { get; set; }
        public ContactPerson ContactPerson { get; set; }
        public string Patientbrief { get; set; }
    }
}