﻿using Point.Models.Enums;
using System;

namespace Point.Database.Models.ViewModels
{

    public class FlowPhaseViewModel
    {
        public FlowDefinitionID FlowDefinitionID { get; set; }
        public int PhaseDefinitionID { get; set; }
        public int? PhaseInstanceID { get; set; }
        public bool PhaseDefinitionEndFlow { get; set; }
        public enum PhaseState { NotActive, Active, Done }
        public decimal PhaseNum { get; set; }
        public string PhaseName { get; set; }
        public string PhaseDescription { get; set; }
        public PhaseState State { get; set; }
        public DateTime? StateDate { get; set; }
        public string LastUpdateBy { get; set; }
        public bool Inactive { get; set; }
        public string Executor { get; set; }
        public int FormTypeID { get; set; }
    }


}
