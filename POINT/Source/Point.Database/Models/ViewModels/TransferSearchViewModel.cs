﻿using System.Collections.Generic;
using System.Linq;

namespace Point.Database.Models.ViewModels
{
    public class TransferSearchViewModel
    {
        public int AutoRefreshMilliseconds { get; set; }
        public List<FlowDefinition> ReceiverFlowDefinitions { get; set; }
        public List<FlowDefinition> SenderFlowDefinitions { get; set; }
        public List<Option> ActivePhases { get; set; }
        public List<SearchUIFieldConfiguration> SearchProperties { get; set; }
        public IEnumerable<string> NoneSortableColumns { get; set; }
        public bool NoFilterOptions { get; set; }
        public string PatExternalReference { get; set; }
        public string PatExternalReferenceBSN { get; set; }
        public SearchControl SearchControl { get; set; }
        public bool MatchType1Equal => SearchControl.matchtype.Any() && SearchControl.matchtype[0] == (int) SearchCritera.MatchType.equal;
        public bool MatchType1Lesser => SearchControl.matchtype.Any() && SearchControl.matchtype[0] == (int)SearchCritera.MatchType.lesser;
        public bool MatchType1Greater => SearchControl.matchtype.Any() && SearchControl.matchtype[0] == (int)SearchCritera.MatchType.greater;
        public bool MatchType2Equal => SearchControl.matchtype.Length > 1 && SearchControl.matchtype[1] == (int)SearchCritera.MatchType.equal;
        public bool MatchType2Lesser => SearchControl.matchtype.Length > 1 && SearchControl.matchtype[1] == (int)SearchCritera.MatchType.lesser;
        public bool MatchType2Greater => SearchControl.matchtype.Length > 1 && SearchControl.matchtype[1] == (int)SearchCritera.MatchType.greater;
    }
}
