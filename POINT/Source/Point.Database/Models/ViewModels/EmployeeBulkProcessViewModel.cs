﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Point.Database.Models.ViewModels
{
    public class EmployeeBulkProcessViewModel
    {
        public int EmployeeID { get; set; }

        [DisplayName("Inactief")]
        public bool Inactive { get; set; }

        [DisplayName("Voornaam")]
        public string FirstName { get; set; }

        [DisplayName("Tussenvoegsel")]
        public string MiddleName { get; set; }

        [Required(ErrorMessage = BaseEntity.errorRequired)]
        [DisplayName("Achternaam")]
        public string LastName { get; set; }

        [DisplayName("Laatste inlogdatum")]
        public DateTime? LastLoginDate { get; set; }

        [DisplayName("Gebruikersnaam")]
        [Required(ErrorMessage = BaseEntity.errorRequired)]
        public string UserName { get; set; }

        [DisplayName("Medewerker is geblokkeerd")]
        public bool? EmployeeIsLocked { get; set; }

        public bool EmployeeSelected { get; set; } = false;
    }   
}
