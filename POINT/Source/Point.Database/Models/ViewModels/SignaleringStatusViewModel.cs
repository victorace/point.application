﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Point.Database.Models.ViewModels
{
    public class SignaleringStatusViewModel
    {
        public int? FlowInstanceID { get; set; }
        public int SignaleringCount { get; set; }
    }
}
