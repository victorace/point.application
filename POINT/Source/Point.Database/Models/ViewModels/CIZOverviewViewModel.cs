﻿
using System.Collections.Generic;

namespace Point.Database.Models.ViewModels
{
    public class CIZOverviewViewModel : FormTypeBaseViewModel
    {
        public CIZOverviewViewModel()
        {
            GvpDuration = new List<GvpDuration>();
        }
        public List<GvpDuration> GvpDuration { get; set; }
        public bool ShowAttachment { get; set; }
        public bool ShowGrondslag { get; set; }
    }
    
}
