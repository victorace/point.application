﻿using System.Collections.Generic;
using System.Linq;
using Point.Database.Attributes;

namespace Point.Database.Models.ViewModels
{

    public class HulpmiddelenViewModel : FormTypeBaseViewModel
    {
        public DropdownMultiSelectViewModel AfleverTijden { get; set; } = new DropdownMultiSelectViewModel(null, null, null, false, string.Empty);
        public string AidProductIDs { get; set; }
        public TransferAttachment AuthorizationFormAttachment { get; set; }
        public List<Supplier> Suppliers { get; set; } = new List<Supplier>();

        public List<AidProductOrderItemViewModel> OrderItems { get; set; } = new List<AidProductOrderItemViewModel>();
        public List<AidProductQuestionViewModel> Questions { get; set; } = new List<AidProductQuestionViewModel>();
        public string OrderStatus { get; set; }
        public List<FormSetVersionViewModel> FormSetVersions { get; set; } = new List<FormSetVersionViewModel>();
        public bool HideNewButtons { get; set; }
        public string HospitalAdres { get; set; }
        public string PatientAdres { get; set; }
        public string PatientBSN { get; set; }
        public string PatientEmail { get; set; }
        public string TransferPuntEmail { get; set; }
        [EmailAddressValidator]
        public string ConfirmationEmail { get; set; }
        public int UniqueFormKey { get; set; }
        public string VegroInsurerCode { get; set; }
        public string ZorginstellingAdres { get; set; }
        public string QuestionsAnsweredAll { get; set; }


        public int SupplierID { get; set; } = (int)Point.Models.Enums.SupplierID.Point;
        public bool IsExternalSupplier => SupplierID != (int)Point.Models.Enums.SupplierID.Point;
        public string CurrentSupplierName { get; set; }
        public string CurrentSupplierPhoneNumber { get; set; }

        public string VegroCode { get; set; }
        public string MediPointCode { get; set; }

        public bool HasVegroAccount => !string.IsNullOrEmpty(VegroCode);
        public bool HasMediPointAccount => !string.IsNullOrEmpty(MediPointCode);
        public bool ShowQuestionWarning { get; set; }
        public bool HasQuestionList => IsExternalSupplier && OrderItems != null && OrderItems.Any();
        public bool HastransportInfo { get; set; }
        public bool ShowBsnWarning => IsExternalSupplier && (string.IsNullOrEmpty(PatientBSN) || PatientBSN.Length < 8 || PatientBSN.Length > 9);
        public bool ShowExternalSupplierWarning => IsExternalSupplier && string.IsNullOrEmpty(ExternalSupplierInsurerCode);
        public string CurrentFormSetversionDescription { get; set; }
        public string SupplierNameVegro { get; set; }
        public string SupplierPhoneNumberVegro { get; set; }
        public string SupplierNameMediPoint { get; set; }
        public string SupplierPhoneNumberMediPoint { get; set; }
        public string SupplierNamePoint { get; set; }
        public string SupplierPhoneNumberPoint { get; set; }

        public int HealthInsurerUzovi { get; set; }
        public object FlowForm { get; set; }

        public string ExternalSupplierInsurerCode
        {
            get
            {
                switch (SupplierID)
                {
                    case (int)Point.Models.Enums.SupplierID.Vegro:
                        return VegroCode ?? string.Empty;
                    case (int)Point.Models.Enums.SupplierID.MediPoint:
                        return MediPointCode ?? string.Empty;
                }
                return null;
            }
        }

        public bool IsCurrentOrderDone { get; set; }
        public bool ShowOrderStatusMessage { get; set; }
    }

    public static class AidProductQuestionsAnsweredAll
    {
        public const string Yes = "1";
        public const string NotNecessary = "2";
    }
}
