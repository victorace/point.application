﻿using Point.Database.Attributes;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Point.Models.LDM.EOverdracht30.Enums;

namespace Point.Database.Models.ViewModels
{
    public enum ClientTab
    {
        General = 1,
        Address = 2,
        Contact = 3,
        Misc = 4
    }

    public class ClientViewModelBase : GlobalViewModel
    {
        private class TabNames
        {
            internal const string Address = "address";
            internal const string General = "general";
            internal const string Contact = "contact";
            internal const string Miscellaneous = "misc";
        }

        public int ClientID { get; set; }

        public string PatientReceiveURL { get; set; }
        public string PatientRequestURL { get; set; }
        public string PatExternalReference { get; set; }
        public bool HavePatExternalReference { get; set; }

        public string ActiveTabName { get; set; } = string.Empty;
        public bool IsGeneralTabActive => IsTabActive(TabNames.General, true);
        public bool IsAddressTabActive => IsTabActive(TabNames.Address);
        public bool IsContactTabActive => IsTabActive(TabNames.Contact);
        public bool IsMiscTabActive => IsTabActive(TabNames.Miscellaneous);

        public ClientLookupsViewModel ClientLookups { get; set; }
        public bool EnableGetBSN => !(HavePatExternalReference && (!IsInstanced || ClientID <= 0));

        internal bool IsTabActive(string tabName, bool isDefault=false)
        {
            if (string.IsNullOrEmpty(ActiveTabName))
            {
                return isDefault;
            }
            return ActiveTabName.Equals(tabName, StringComparison.InvariantCultureIgnoreCase);
        }
    }

    public class ClientViewModel : ClientViewModelBase
    {
        public ClientTab[] Tabs { get; set; } = new ClientTab[] { ClientTab.General, ClientTab.Address, ClientTab.Contact, ClientTab.Misc };
        public ClientTab ActiveTab { get; set; } = ClientTab.Address;

        public bool BSNRequired { get; set; } = true;
        public bool GenderRequired { get; set; } = true;
        public bool InitialsRequired { get; set; } = true;
        public bool LastNameRequired { get; set; } = true;
        public bool GeboortedatumRequired { get; set; } = true;
        public bool PolisnummerRequired { get; set; } = true;
        public bool StraatRequired { get; set; } = true;
        public bool HuisnummerRequired { get; set; } = true;
        public bool PostcodeRequired { get; set; } = true;
        public bool PlaatsRequired { get; set; } = true;
        public bool Telefoonnummer1Required { get; set; } = true;
        public bool HuisartsRequired { get; set; } = true;
        public bool PharmacyRequired { get; set; } = false;

        [StringLength(9)]
        [DisplayName("Burger Service Nummer")]
        [RequiredIf("BSNRequired", true)]
        public string CivilServiceNumber { get; set; }

        [DisplayName("Patiëntnummer ZH")]
        [StringLength(50, ErrorMessage = BaseEntity.errorMaxLength)]
        public string PatientNumber { get; set; }

        [DisplayName("Opnamenr.")]
        [StringLength(50, ErrorMessage = BaseEntity.errorMaxLength)]
        public string VisitNumber { get; set; }

        [DisplayName("Geslacht")]
        [Required(ErrorMessage = BaseEntity.errorRequired)]
        [RegularExpression("OM|OF|OUN", ErrorMessage = "Geslacht ('Man', 'Vrouw' of 'Ongedifferentieerd') is verplicht")]
        public Geslacht Gender { get; set; }

        [StringLength(255)]
        [DisplayName("Aanhef")]
        public string Salutation { get; set; }

        [StringLength(50)]
        [DisplayName("Voorletters")]
        [RequiredIf("InitialsRequired", true)]
        public string Initials { get; set; }

        [StringLength(255)]
        [DisplayName("Voornaam")]
        public string FirstName { get; set; }

        [StringLength(50)]
        [DisplayName("Tussenvoegsel")]
        public string MiddleName { get; set; }

        [StringLength(255)]
        [DisplayName("Achternaam (volledig)")]
        [RequiredIf("LastNameRequired", true)]
        public string LastName { get; set; }

        [StringLength(255)]
        [DisplayName("Geboortenaam")]
        public string MaidenName { get; set; }

        [DisplayName("Geboortedatum")]
        [DataType(DataType.Date, ErrorMessage = BaseEntity.errorDate), DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}")]
        [RequiredIf("GeboortedatumRequired", true)]
        public DateTime? BirthDate { get; set; }

        [DisplayName("Zorgverzekeraar")]
        [Required(ErrorMessage = BaseEntity.errorRequired)]
        public int? HealthInsuranceCompanyID { get; set; }

        [StringLength(50)]
        [DisplayName("Polisnummer")]
        [RequiredIf("PolisnummerRequired", true)]
        public string InsuranceNumber { get; set; }

        [StringLength(255)]
        [DisplayName("Straat")]
        [RequiredIf("StraatRequired", true)]
        public string StreetName { get; set; }

        [StringLength(50)]
        [DisplayName("Huisnummer")]
        [RequiredIf("HuisnummerRequired", true)]
        public string Number { get; set; }

        [StringLength(50)]
        [DisplayName("Huisnummer toevoeging")]
        public string HuisnummerToevoeging { get; set; }

        [StringLength(10)]
        [DisplayName("Postcode")]
        [RequiredIf("PostcodeRequired", true)]
        public string PostalCode { get; set; }

        [StringLength(50)]
        [DisplayName("Postbus")]
        public string Postbus { get; set; }

        [StringLength(255)]
        [DisplayName("Plaats")]
        [RequiredIf("PlaatsRequired", true)]
        public string City { get; set; }

        [StringLength(255)]
        [DisplayName("Land")]
        public string Country { get; set; }

        [StringLength(50)]
        [DisplayName("Telefoonnummer 1")]
        [RequiredIf("Telefoonnummer1Required", true)]
        public string PhoneNumberGeneral { get; set; }

        [StringLength(50)]
        [DisplayName("Telefoonnummer 2")]
        public string PhoneNumberMobile { get; set; }

        [StringLength(50)]
        [DisplayName("Telefoonnummer 3")]
        public string PhoneNumberWork { get; set; }

        [StringLength(255)]
        [DisplayName("E-mail")]
        [EmailAddressValidator]
        public string Email { get; set; }

        [StringLength(255)]
        [DisplayName("Huisarts")]
        [RequiredIf("HuisartsRequired", true)]
        public string GeneralPractitionerName { get; set; }

        [StringLength(50)]
        [DisplayName("Telefoonnummer huisarts")]
        public string GeneralPractitionerPhoneNumber { get; set; }

        [StringLength(255)]
        [DisplayName("Apotheek")]
        [RequiredIf("PharmacyRequired", true)]
        public string PharmacyName { get; set; }

        [StringLength(50)]
        [DisplayName("Telefoonnummer apotheek")]
        public string PharmacyPhoneNumber { get; set; }

        [DisplayName("Burgerlijke staat")]
        public BurgerlijkeStaat? CivilClass { get; set; }

        [DisplayName("Samenstelling huishouding")]
        public Gezinssamenstelling? CompositionHousekeeping { get; set; }

        [DisplayName("Aantal kinderen in het gezin")]
        public int? ChildrenInHousekeeping { get; set; }

        [DisplayName("Woonsituatie")]
        public WoningType? HousingType { get; set; }

        [StringLength(255)]
        [DisplayName("Verzorgings- / verpleeginstelling")]
        public string HealthCareProvider { get; set; }

        public int? ClientCreatedBy { get; set; }

        public DateTime? ClientCreatedDate { get; set; }

        public int? ClientModifiedBy { get; set; }

        public DateTime? ClientModifiedDate { get; set; }

        [DisplayName("Nationaliteit")]
        public int? NationalityID { get; set; }

        [DisplayName("Taal")]
        public CommunicatieTaal? LanguageID { get; set; }

        public DateTime? TimeStamp { get; set; }

        public int? EmployeeID { get; set; }

        public int? ScreenID { get; set; }

        [StringLength(255)]
        [DisplayName("Zorgmail adres huisarts")]
        public string AddressGPForZorgmail { get; set; }

        [StringLength(255)]
        [DisplayName("Straat")]
        //[Required(ErrorMessage = BaseEntity.errorRequired)]
        public string TemporaryStreetName { get; set; }

        [StringLength(50)]
        [DisplayName("Huisnummer")]
        //[Required(ErrorMessage = BaseEntity.errorRequired)]
        public string TemporaryNumber { get; set; }

        [StringLength(6)]
        [DisplayName("Postcode")]
        //[Required(ErrorMessage = BaseEntity.errorRequired)]
        [RegularExpression(@"\d{4}[A-Za-z]{2}", ErrorMessage = "{0} moet bestaan uit 4 cijfers direct gevolgd door 2 letters")]
        public string TemporaryPostalCode { get; set; }

        [StringLength(255)]
        [DisplayName("Plaats")]
        //[Required(ErrorMessage = BaseEntity.errorRequired)]
        public string TemporaryCity { get; set; }

        [StringLength(255)]
        [DisplayName("Land")]
        public string TemporaryCountry { get; set; }

        [StringLength(255)]
        [DisplayName("Partnernaam")]
        public string PartnerName { get; set; }

        [StringLength(50)]
        [DisplayName("Tussenvoegsel")]
        public string PartnerMiddleName { get; set; }

        [DisplayName("Cliënt is anoniem")]
        public bool ClientIsAnonymous { get; set; }
        public bool ClientShowAnonymous { get; set; }

        public bool ShowSearchBSN { get; set; }
        public bool ShowSearchComplete { get; set; }

        public ContactPersonViewModel ContactPerson { get; set; }
        public bool ForceContactPersonCreation { get; set; }
        public int? DepartmentID { get; set; }
    }
}
