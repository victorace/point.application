﻿namespace Point.Database.Models.ViewModels
{
    public class DepartmentSearch
    {
 
        public int DepartmentID { get; set; }
        public string DepartmentName { get; set; }
        public int? DepartmentTypeID { get; set; }
        public int? LocationID { get; set; }
        public string LocationName { get; set; }
        public int? OrganizationID { get; set; }
        public string OrganizationName { get; set; }
        
    }
}
