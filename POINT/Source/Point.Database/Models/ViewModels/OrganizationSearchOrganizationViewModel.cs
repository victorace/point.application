﻿using Point.Models.Enums;
using System.Collections.Generic;
using System.Linq;

namespace Point.Database.Models.ViewModels
{
    public class OrganizationSearchOrganizationViewModel
    {
        public string RegionName { get; set; }
        public int OrganizationTypeID { get; set; }
        public int OrganizationID { get; set; }
        public string OrganizationName { get; set; }

        public int? CapacityTotal0 { get; set; }
        public int? CapacityTotal1 { get; set; }
        public int? CapacityTotal2 { get; set; }
        public int? CapacityTotal3 { get; set; }

        public Participation Participation { get; set; }
        public FlowDirection FlowDirection { get; set; }

        public bool IsFavorite { get; set; }

        public static OrganizationSearchOrganizationViewModel FromModel(OrganizationSearch orgsearch, FlowDefinitionID? flowdefinitionid, int[] favoritelocations)
        {
            bool showlocation = !string.IsNullOrEmpty(orgsearch.LocationName) && orgsearch.LocationName != "Locatie 1";
            var participation = orgsearch.OrganizationSearchParticipation.FirstOrDefault(it => it.FlowDefinitionID == flowdefinitionid);

            var viewmodel = new OrganizationSearchOrganizationViewModel();
            viewmodel.OrganizationID = orgsearch.OrganizationID;
            viewmodel.OrganizationName = orgsearch.OrganizationName;

            viewmodel.OrganizationTypeID = orgsearch.OrganizationTypeID;
            viewmodel.RegionName = string.Join(", ", orgsearch.OrganizationSearchRegion.Select(it => it.RegionName));
            viewmodel.Participation = (participation?.Participation).GetValueOrDefault();
            viewmodel.IsFavorite = favoritelocations.Contains(orgsearch.LocationID);
            viewmodel.FlowDirection = (participation?.FlowDirection).GetValueOrDefault();
            viewmodel.CapacityTotal0 = orgsearch.Capacity0;
            viewmodel.CapacityTotal1 = orgsearch.Capacity1;
            viewmodel.CapacityTotal2 = orgsearch.Capacity2;
            viewmodel.CapacityTotal3 = orgsearch.Capacity3;

            return viewmodel;
        }


        public static List<OrganizationSearchOrganizationViewModel> FromModelList(IEnumerable<OrganizationSearch> orgsearch, FlowDefinitionID? flowdefinitionid, int[] favoritelocations)
        {
            var viewmodellist = new List<OrganizationSearchOrganizationViewModel>();
            foreach (var item in orgsearch)
            {
                var existing = viewmodellist.FirstOrDefault(it => it.OrganizationID == item.OrganizationID);
                if (existing == null)
                {
                    viewmodellist.Add(FromModel(item, flowdefinitionid, favoritelocations));
                }
                else
                {
                    existing.CapacityTotal0 = item.Capacity0.HasValue
                                                ? existing.CapacityTotal0.GetValueOrDefault() + item.Capacity0
                                                : existing.CapacityTotal0;
                    existing.CapacityTotal1 = item.Capacity1.HasValue
                                                ? existing.CapacityTotal1.GetValueOrDefault() + item.Capacity1
                                                : existing.CapacityTotal1;
                    existing.CapacityTotal2 = item.Capacity2.HasValue
                                                ? existing.CapacityTotal2.GetValueOrDefault() + item.Capacity2
                                                : existing.CapacityTotal2;
                    existing.CapacityTotal3 = item.Capacity3.HasValue
                                                ? existing.CapacityTotal3.GetValueOrDefault() + item.Capacity3
                                                : existing.CapacityTotal3;

                    existing.IsFavorite = favoritelocations.Contains(item.LocationID) ? true : existing.IsFavorite;
                }
            }

            return viewmodellist;
        }
    }
}
