﻿using System;
using System.Collections.Generic;

namespace Point.Database.Models.ViewModels
{
    public class ClientHeaderViewModel : GlobalViewModel
    {
        private const string _GenderMan = "man";

        public int ClientID { get; set; }

        public FlowInstance FlowInstance { get; set; }

        public string Name { get; set; }

        public string Gender { get; set; }

        public DateTime? BirthDate { get; set; }

        public string CivilServiceNumber { get; set; }

        public string Logo { get; set; }

        public bool IsCopy { get; set; }
        public bool HasRelatedTransfer { get; set; }
        public int? OriginalTransferID { get; set; }
        public int? RelatedTransferID { get; set; }
        public IEnumerable<LocalMenuItem> ActionsMenu { get; set; }
        public FormSetVersion FormSetVersion { get; set; }
        public string OriginalTransferUrl { get; set; }
        public string RelatedTransferUrl { get; set; }
        public bool IsMFAScreen { get; set; }
        public int? FlowInstanceID { get; set; }
        public bool DesktopForced { get; set; }
        public string MobileUrl { get; set; }
        public bool IsGenderMan => Gender.ToLower() == _GenderMan;
        public string UserName { get; set; }
        public bool HasUserName => !string.IsNullOrEmpty(UserName);
        public int OpenSignalItemsCount { get; set; }
        public bool ExternalPatientIsAuth { get; set; }
        public string ExternalPatientReference { get; set; }
        public string ExternalPatientReferenceBSN { get; set; }
        public bool HasExternalPatientReference => !string.IsNullOrEmpty(ExternalPatientReference);
        public bool HasExternalPatientReferenceBSN => !string.IsNullOrEmpty(ExternalPatientReferenceBSN);
        public List<MenuItemViewModel> ServiceMenuItems { get; set; } = new List<MenuItemViewModel>();
        public SignaleringStatusViewModel SignaleringStatusViewModel { get; set; }
    }
}
