﻿using Point.Database.Extensions;

namespace Point.Database.Models.ViewModels
{
    public class SignedPhaseInstanceViewModel
    {
        bool _useNameBIG;
        public SignedPhaseInstanceViewModel(bool useNameBIG=false)
        {
            _useNameBIG = useNameBIG;
        }
        public SignedPhaseInstance ObjectInstance { get; set; }
        public bool HasSignedPhaseInstance { get { return ObjectInstance != null; } }
        public string SignedByEmployeeName
        {
            get
            {
                if (_useNameBIG && !string.IsNullOrWhiteSpace(ObjectInstance?.SignedBy?.BIG))
                {
                    return " (BIG: " + ObjectInstance?.SignedBy?.BIG + ")";
                }
                return ObjectInstance?.SignedBy?.FullName(); ;
            }
        }
        public string SignedByEmployeeFunction { get { return ObjectInstance?.SignedBy?.Position; } }
        public string SignedByEmployeeBIG { get { return ObjectInstance?.SignedBy?.BIG; } }
        public System.DateTime? Date { get { return ObjectInstance?.Timestamp; } }
        public int PhaseInstanceID { get { return ObjectInstance?.PhaseInstanceID ?? 0; } }

    }
}
