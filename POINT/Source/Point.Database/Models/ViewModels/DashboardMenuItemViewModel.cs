﻿using Point.Models.Enums;
using System;

namespace Point.Database.Models.ViewModels
{
    public class DashboardMenuItemViewModel
    {
        public int TransferID { get; set; }
        public int PhaseDefinitionID { get; set; }
        public int FormTypeID { get; set; }
        public int? PhaseInstanceID { get; set; }
        public string Url { get; set; }
        public MenuItemType DashboardMenuItemType { get; set; }
        public int Status { get; set; }
        public string Label { get; set; }
        public string Text { get; set; }
        public DateTime? ProcessDateTime { get; set; } // PhaseInstance.StartProcessDate or PhaseInstance.RealizedDate
        public DateTime? LastModifiedOn { get; set; } // FormSetVersion.CreateDate or FormSetVersion.UpdateDate
        public string LastModifiedBy { get; set; } // FormSetVersion.CreatedBy or FormSetVersion.UpdatedBy
        public bool HasPhaseInstance { get; set; }
        public int Order { get; set; }
    }
}
