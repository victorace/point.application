﻿using System.Collections.Generic;

namespace Point.Database.Models.ViewModels
{
    public class AfterCareFinancingViewModel
    {
        public string Name { get; set; }
        public string ColorCode { get; set; }
        public IEnumerable<AfterCareGroupViewModel> AfterCareGroups { get; set; } = new List<AfterCareGroupViewModel>();
        public int Sortorder { get; set; }

        public int AfterCareTileGroupID { get; set; }
        public bool IsHospital => AfterCareTileGroupID == (int)Point.Models.Enums.AfterCareTileGroupID.Ziekenhuis;
    }
}
