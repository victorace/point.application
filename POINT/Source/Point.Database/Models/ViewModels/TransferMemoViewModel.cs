﻿using System.Collections.Generic;

namespace Point.Database.Models.ViewModels
{
    public class TransferMemoViewModel : IEditable
    {
        public TransferMemo TransferMemo { get; set; }
        public string DepartmentFullName { get; set; }
        public string DepartmentContactInfo { get; set; }
        public int EmployeeID { get; set; }
        public int OrganizationID { get; set; }
        public bool IsEditable { get; set; }
        public bool CanCreateTask { get; set; }
        public bool HasNotification { get; set; }
        public string EmployeeContactInfo { get; set; }
        public string LastChangedInfo { get; set; }
        public string RelatedMemoOwner { get; set; }
        public IEnumerable<TransferMemoChanges> TransferMemoChanges { get; set; }
    }
}
