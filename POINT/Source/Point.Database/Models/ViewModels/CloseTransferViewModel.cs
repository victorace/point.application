﻿using System;
using System.Collections.Generic;
using Point.Models.Enums;

namespace Point.Database.Models.ViewModels
{
    public class CloseTransferViewModel : GlobalViewModel
    {
        public enum VOActions
        {
            None = 0,
            Missing = 1,
            Active = 2
        }

        public class ActiveIndicatieMSVT
        {
            public string Name { get; set; }
            public string Url { get; set; }
        }

        public class CloseTransferRule
        {
            public string Name { get; set; }
            public string Description { get; set; }

            public CloseTransferRule(string name, string description)
            {
                Name = name;
                Description = description;
            }
        }

        public IEnumerable<FlowWebField> FlowWebFields { get; set; }
        public FlowDefinitionID FlowDefinitionID { get; set; }
        public bool IsAllowed { get; set; }
        public bool IsFlowClosed { get; set; }

        public bool IsFlowInterrupted { get; set; }
        public DateTime? InterruptionDate { get; set; }
        public string InterruptionReason { get; set; }
        
        public string ActivePhases { get; set; }
        public bool ShowButtonCloseTransfer => IsAllowed && !IsFlowClosed;
    }

    public class CloseTransferHAViewModel : CloseTransferViewModel
    {
        public int DestinationHealthCareProviderDepartmentID { get; set; }
        public string DestinationHealthCareProviderDepartmentName { get; set; }
        public bool HasDestinationHealthCareProvider { get; set; }
        public bool HasReceivingDepartment => !string.IsNullOrEmpty(DestinationHealthCareProviderDepartmentName);
    }
    
    public class CloseTransferVVTViewModel : GlobalViewModel
    {
        public bool IsAllowed { get; set; }
    }

    public class CloseTransferZHVVTViewModel : CloseTransferViewModel
    {
        public VOActions VOAction { get; set; }
        public string VOUrl { get; set; }

        public bool HasOrganizationDefinedTags { get; set; }
        public bool HasRegistrationEndedWithoutTransfer { get; set; }
        public bool HasRegistrationEndedWithoutReason { get; set; }

        public bool HasRouterenPhaseInstance { get; set; }
        public bool HasDestinationHealthCareProvider { get; set; }
        public int DestinationHealthCareProviderDepartmentID { get; set; }
        public string DestinationHealthCareProviderDepartmentName { get; set; }
        public bool ValidAftercareClassification { get; set; }
        public IEnumerable<ActiveIndicatieMSVT> ActiveIndicatieMSVTs { get; set; }

        public CloseTransferZHVVTViewModel()
        {
            ActiveIndicatieMSVTs = new List<ActiveIndicatieMSVT>();
        }
    }
}
