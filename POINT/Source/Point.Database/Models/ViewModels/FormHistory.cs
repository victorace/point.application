﻿using Point.Models.Enums;
using System;

namespace Point.Database.Models.ViewModels
{
    public class FormHistoryForm
    {
        public int FormTypeID { get; set; }
        public int? FormSetVersionID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime? CreationDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? ModificationDate { get; set; }
        public int ModifiedByID { get; set; }
        public string ModifiedBy { get; set; }
        public string DataType { get; set; }        // FormHistoryDataTypes
        public string FieldsUrl { get; set; }
        public string HistoryUrl { get; set; }
    }

    public static class FormHistoryDataTypes
    {
        public const string Client = "clientdata";
        public const string CommJournal = "commjournal";
        public const string FlowForm = "flowform";
    }

    public class FormHistoryField
    {
        public string CustomField { get; set; }
        public int? FlowFieldID { get; set; }
        public string Name { get; set; }
        public string Label { get; set; }
    }

    public class FormHistoryFieldValue
    {
        public DateTime Timestamp;
        public string EmployeeFullName;
        public int FlowFieldID;
        public FlowField FlowField;
        public string Value;
        public string Name;
        public string Label;
        public Source Source;
    }
}