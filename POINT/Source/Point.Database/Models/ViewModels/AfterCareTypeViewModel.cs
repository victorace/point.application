﻿

namespace Point.Database.Models.ViewModels
{
    public class AfterCareTypeViewModel
    {
        public int AfterCareTypeID { get; set; }
        public string Name { get; set; }
        public int? Capacity { get; set; }
        public int Sortorder { get; set; }
        public bool HasCapacity => Capacity.HasValue && Capacity.Value > 0;
        public int OrganizationTypeID { get; set; }

        // TODO:
        public int? AfterCareGroupID { get; set; }
        public int? RegionID { get; set; }

        // MODAL
        public string Description { get; set; }
        public bool IsActiveInSelectedRegions { get; set; }
    }
}
