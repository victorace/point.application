﻿namespace Point.Database.Models.ViewModels
{
    public class PagingViewModel
    {
        public int PageSize => Pager.PageSize; //30;
        public int PageNo => Pager.PageNo;
        //public int PageIndex => Pager.PageIndex;
        public int TotalCount => Pager.TotalCount;

        //public PagingViewModel(int pageIndex, int totalCount)
        public PagingViewModel(Pager pager)
        {
            //PageIndex = pageIndex;
            //TotalCount = totalCount;
            Pager = pager;
        }

        public Pager Pager { get; set; }

        public int PageItemsStartNo => Pager.PageIndex * Pager.PageSize + 1;

        public int PageItemsEndNo
        {
            get
            {
                var endIndex = Pager.PageNo * Pager.PageSize;
                return endIndex < Pager.TotalCount ? endIndex : Pager.TotalCount;
            }
        }
        
    }
    
}
