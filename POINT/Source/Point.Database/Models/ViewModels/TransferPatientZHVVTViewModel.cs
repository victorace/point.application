﻿

namespace Point.Database.Models.ViewModels
{
    public class TransferPatientZHVVTViewModel : FormTypeBaseViewModel
    {
        public int? NextPhaseJa { get; set; }
        public int? NextPhaseJaMaar { get; set; }
        public int? NextPhaseNee { get; set; }
        public bool HasAfterCareCategoryDefinitive { get; set; }
        public bool HasCareBeginDate { get; set; }
    }
}
