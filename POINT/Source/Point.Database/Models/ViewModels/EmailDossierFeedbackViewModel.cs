﻿using System;
using System.Collections.Generic;
using Point.Models.Enums;

namespace Point.Database.Models.ViewModels
{
    public class EmailDossierFeedbackViewModel : IScheduleHistory
    {
        public int TransferID { get; set; }
        public string ClientName { get; set; }
        public int FormTypeID { get; set; }
        public string Type { get;  set;}
        public string Title { get; set; }
        public string SendingEmployeeName { get; set; }
        public string SendingEmployeeTelephoneNumber { get; set; }
        public string SendingLocationName { get; set; }
        public string SendingOrganizationName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public List<DateTime> Dates { get; set; }
        public int Quantity { get; set; }
        public string RecievingEmailAdress { get; set; }
        public int RecievingDepartmentID { get; set; }
        public string DossierUrl { get; set; }


        public int FrequencyID { get; set; }
        public DateTime Timestamp { get; set; }


        public int? EmployeeID { get; set; }
        public int? OrganizationID { get; set; }
        public CommunicationLogType MailType { get; set; }

        public EmailDossierFeedbackViewModel()
        {
            Dates = new List<DateTime>();
        }
    }
}
