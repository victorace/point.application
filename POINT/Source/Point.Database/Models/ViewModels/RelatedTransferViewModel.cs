﻿using System;

namespace Point.Database.Models.ViewModels
{
    public class RelatedTransferViewModel
    {
        public int TransferID { get; set; }
        public string ClientName { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime TransferDate { get; set; }

        public int? HospitalID { get; set; }
        public string Hospital { get; set; }

        public int? PreferredVVTID { get; set; }
        public string PreferredVVT { get; set; }

        public int? SendingVVTID { get; set; }
        public string SendingVVT { get; set; }

        public string CivilServiceNumber { get; set; }
        public string TypeFlow { get; set; }
        public bool PointParticipator { get; set; }
    }
}
