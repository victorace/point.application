﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Point.Database.Models.ViewModels
{
    public class LocationDepartmentSelection
    {
        public LocationDepartmentSelection(int locationID, int departmentID, string locationName, string departmentName)
        {
            this.LocationID = locationID;
            this.DepartmentID = departmentID;
            this.LocationName = locationName;
            this.DepartmentName = departmentName;
        }

        public int LocationID { get; set; }
        public int DepartmentID { get; set; }
        public string LocationName { get; set; }
        public string DepartmentName { get; set; }
    }
}
