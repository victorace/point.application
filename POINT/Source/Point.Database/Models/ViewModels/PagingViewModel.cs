﻿namespace Point.Database.Models.ViewModels
{
    public class PagingViewModel
    {
        public PagingViewModel(Pager pager)
        {
            //PageIndex = pageIndex;
            //TotalCount = totalCount;
            Pager = pager;
        }
        public int PageSize => Pager.PageSize; //30;
        public int PageNo => Pager.PageNo;
        //public int PageIndex => Pager.PageIndex;
        public int TotalCount => Pager.TotalCount;
        public bool HasPreviousPage => PageNo > 1;
        public bool HasNextPage => PageNo < Pager.PageCount;
        public Pager Pager { get; set; }
        public int PageItemsStartNo => Pager.PageIndex * Pager.PageSize + 1;

        public int PageItemsEndNo
        {
            get
            {
                var endIndex = Pager.PageNo * Pager.PageSize;
                return endIndex < Pager.TotalCount ? endIndex : Pager.TotalCount;
            }
        }

    }

}
