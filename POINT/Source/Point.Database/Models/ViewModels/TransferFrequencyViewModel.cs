﻿namespace Point.Database.Models.ViewModels
{
    public class TransferFrequencyViewModel
    {
        public string Name { get; set; }
        public int TransferID { get; set; }
        public int FrequencyID { get; set; }
        public int FormSetVersionID { get; set; }
        public int PhaseDefinitionID { get; set; }
        public int PhaseInstanceID { get; set; }
        public string URL { get; set; }
    }
}
