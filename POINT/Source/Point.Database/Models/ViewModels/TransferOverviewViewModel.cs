﻿
namespace Point.Database.Models.ViewModels
{
    public class TransferOverviewViewModel
    {
        public int NotHandled { get; set; }
        public int Handled { get; set; }
        public int SignalCount { get; set; }
    }
}
