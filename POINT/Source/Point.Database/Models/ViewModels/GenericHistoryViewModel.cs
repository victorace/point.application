﻿namespace Point.Database.Models.ViewModels
{
    public class GenericHistoryViewModel
    {
        public string TableName { get; set; }
        
        public string PrimaryKey { get; set; }
        
    }
}
