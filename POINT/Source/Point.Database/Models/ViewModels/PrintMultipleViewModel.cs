﻿using System.Collections.Generic;

namespace Point.Database.Models.ViewModels
{
    public class PrintMultipleViewModel
    {
        public int TransferID { get; set; }
        public List<FormSetVersionContainer> FormSetVersions { get; set; }
        public List<TransferAttachment> Attachments { get; set; }
    }

    public class FormSetVersionContainer
    {
        public string PrintName { get; set; }
        public int FormsetVersionID { get; set; }
        public int PhaseDefinitionID { get; set; }
        public int PhaseInstanceID { get; set; }
        public int FormTypeID { get; set; }
        public int EmployeeID { get; set; }
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
        public bool IsCombined { get; set; }
        public bool PrintOnNewPage { get; set; }
    }
    
}
