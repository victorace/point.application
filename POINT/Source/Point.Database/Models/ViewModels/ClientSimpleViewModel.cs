﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Point.Database.Models.ViewModels
{
    public class ClientSimpleViewModel : GlobalViewModel
    {
        public int ClientID { get; set; }

        [StringLength(9)]
        [DisplayName("Burger Service Nummer")]
        [Required(ErrorMessage = BaseEntity.errorRequired)] 
        public string CivilServiceNumber { get; set; }

        [DisplayName("Heeft (nog) geen BSN")]
        public bool HasNoCivilServiceNumber { get; set; }

        [StringLength(1)]
        [DisplayName("Geslacht")]
        [Required(ErrorMessage = BaseEntity.errorRequired)]
        [RegularExpression("m|v", ErrorMessage="Geslacht ('m' of 'v') is verplicht")]
        public string Gender { get; set; }

        [StringLength(50)]
        [DisplayName("Voorletters")]
        [Required(ErrorMessage = BaseEntity.errorRequired)]
        public string Initials { get; set; }

        [StringLength(255)]
        [DisplayName("Voornaam")]
        public string FirstName { get; set; }

        [StringLength(50)]
        [DisplayName("Voorvoegsel")]
        public string MiddleName { get; set; }

        [StringLength(255)]
        [DisplayName("Achternaam (volledig)")]
        [Required(ErrorMessage = BaseEntity.errorRequired)]
        public string LastName { get; set; }

        [StringLength(255)]
        [DisplayName("Geboortenaam")]
        public string MaidenName { get; set; }

        [DisplayName("Geboortedatum")]
        [DataType(DataType.Date, ErrorMessage = BaseEntity.errorDate), DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}")]
        [Required(ErrorMessage = BaseEntity.errorRequired)]
        public DateTime? BirthDate { get; set; }

        [StringLength(255)]
        [DisplayName("Huisarts")]
        [Required(ErrorMessage = BaseEntity.errorRequired)]
        public string GeneralPractitionerName { get; set; }

        [StringLength(50)]
        [DisplayName("Telefoonnummer huisarts")]
        public string GeneralPractitionerPhoneNumber { get; set; }

        [StringLength(255)]
        [DisplayName("Zorgmail adres huisarts")]
        public string AddressGPForZorgmail { get; set; }

        [DisplayName("Cliënt is anoniem")]
        public bool ClientIsAnonymous { get; set; }
        public bool ClientShowAnonymous { get; set; }

        public bool ShowSearchBSN { get; set; }
        public bool ShowSearchComplete { get; set; }
        public int? DepartmentID { get; set; }
        public bool ShowNedapRetrieve { get; set; }
        public bool BSNRequired { get; set; }
    }
}
