﻿using System.Collections.Generic;

namespace Point.Database.Models.ViewModels
{
    public class OrganizationMigrateResultsViewModel
    {
       public OrganizationMigrateResultsViewModel()
        {
            ResultLines = new List<OrganizationMigrateResultLineViewModel>();
        }

        public Dictionary<string, string> HtmlReplaces { get; set; }    
        public string Title { get; set; }

        public List<OrganizationMigrateResultLineViewModel> ResultLines { get; set; }
        public bool HasErrors
        {
            get { return ResultLines.Exists(x => x.IsError); }
        }
        public bool HasLineHeaders
        {
            get { return ResultLines.Exists(x => x.IsLineHeader); }
        }
        public bool HasWarnings
        {
            get { return ResultLines.Exists(x => x.IsWarning); }
        }
    }

    public class OrganizationMigrateResultLineViewModel
    {
        public string Step { get; set; }
        public string Message { get; set; }
        public bool IsLineHeader { get; set; }
        public bool IsWarning { get; set; }
        public bool IsError { get; set; }
        public bool IsEnd { get; set; }
    }
}
