﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Point.Database.Models.ViewModels
{
    public class LogReadExtendedViewModel
    {
        [Required(ErrorMessage = BaseEntity.errorRequired)]
        public DateTime DateFrom { get; set; } = DateTime.Now;

        [Required(ErrorMessage = BaseEntity.errorRequired)]
        public DateTime DateTm { get; set; } = DateTime.Now;
        public int? TransferID { get; set; }
        public string BSN { get; set; }
        public string EmployeeName { get; set; }
        public bool ReportCommandSent { get; set; }

        public int? OrganizationID { get; set; }
        public int? EmployeeID { get; set; }

        public string Message { get; set; }

        public List<TransferAttachmentViewModel> LogReadReportList = new List<TransferAttachmentViewModel>();
    }
}
