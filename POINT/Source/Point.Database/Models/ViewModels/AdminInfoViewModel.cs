﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Point.Database.Models.ViewModels
{
    public class AdminInfoViewModel
    {
        public int EmployeeID { get; set; }
        public string FullName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }

        private static AdminInfoViewModel FromModel(Employee employee)
        {
            var vm = new AdminInfoViewModel();
            vm.EmployeeID = employee.EmployeeID;
            vm.FullName = employee.FirstName + ' ' + employee.MiddleName + ' ' + employee.LastName;
            vm.PhoneNumber = employee.PhoneNumber;
            vm.Email = employee.EmailAddress;
            return vm;
        }
        public static List<AdminInfoViewModel> FromModelList(IQueryable<Employee> employees)
        {
            var vmList = new List<AdminInfoViewModel>();
            foreach (var emp in employees)
            {
                vmList.Add(FromModel(emp));
            }
            return vmList;
        }
    }
}
