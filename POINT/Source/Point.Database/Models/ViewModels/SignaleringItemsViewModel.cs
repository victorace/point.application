﻿namespace Point.Database.Models.ViewModels
{
    public class SignaleringItemsViewModel
    {
        public string[] SignaleringItems { get; set; }
        public bool HasSignalering { get; set; }
        public bool? SendSignal { get; set; }
    }
}
