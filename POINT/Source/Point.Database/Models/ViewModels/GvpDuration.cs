﻿namespace Point.Database.Models.ViewModels
{
    public class GvpDuration
    {
        public string Name { get; set; }
        public string DateBegin { get; set; }
        public string DateEnd { get; set; }
    }
}
