﻿using Point.Database.Attributes;
using Point.Models.Enums;
using Point.Models.LDM.EOverdracht30.Enums;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Point.Database.Models.ViewModels
{
    public class AutoCreateSetViewModel
    {
        public AutoCreateSetViewModel()
        {
            AllowedDossierLevels = new HashSet<Role>();
        }

        public int AutoCreateSetID { get; set; }

        [DisplayName("Locatie")]
        public int LocationID { get; set; }

        [DisplayName("Unieke Code")]
        [Required(ErrorMessage = BaseEntity.errorRequired)]
        public string AutoCreateCode { get; set; }

        [DisplayName("Set naam")]
        [Required(ErrorMessage = BaseEntity.errorRequired)]
        public string Name { get; set; }

        [DisplayName("Default voor organisatie")]
        public bool IsDefaultForOrganization { get; set; }

        [DisplayName("Username Prefix")]
        public string UsernamePrefix { get; set; }

        [DisplayName("Standaard Wachtwoord")]
        [Required(ErrorMessage = BaseEntity.errorRequired)]
        [WriteAccess(FunctionRoleLevelID.Organization, FunctionRoleTypeID.ManageOrganization)]
        [ReadAccess(FunctionRoleLevelID.Organization, FunctionRoleTypeID.ManageOrganization)]
        public string DefaultPassword { get; set; }

        [DisplayName("Standaard Telefoonnummer")]
        public string DefaultPhoneNumber { get; set; }

        [DisplayName("Standaard Emailadres")]
        public string DefaultEmailAddress { get; set; }

        [Required(ErrorMessage = BaseEntity.errorRequired)]
        [DisplayName("Standaard Dossier niveau")]
        public Role DefaultDossierLevel { get; set; }
        [DisplayName("Standaard Dossier niveau")]
        public string DefaultDossierLevelString { get { return DefaultDossierLevel.ToString(); } }

        public IEnumerable<Role> AllowedDossierLevels { get; set; }

        [DisplayName("Standaard Geslacht")]
        public Geslacht DefaultGender { get; set; }

        [DisplayName("Wachtwoord aanpassen na 1e aanmelding")]
        public bool ChangePasswordByEmployee { get; set; }

        [Required(ErrorMessage = BaseEntity.errorRequired)]
        [DisplayName("Standaard Afdeling(en)")]
        public List<int> DefaultDepartmentIDs { get; set; }
        public List<Department> LocationDepartments { get; set; }

        [DisplayName("Alleen lezen bij Versturen")]
        public bool ReadOnlySender { get; set; }

        [DisplayName("Alleen lezen bij Ontvangen")]
        public bool ReadOnlyReciever { get; set; }


    }
}
