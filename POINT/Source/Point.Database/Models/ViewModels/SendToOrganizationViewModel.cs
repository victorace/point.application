﻿using Point.Models.Enums;
using System.Collections.Generic;

namespace Point.Database.Models.ViewModels
{
    public class DepartmentData
    {
        public int? OrganizationInviteID { get; set; }
        public int DepartmentID { get; set; }
        public InviteStatus? InviteStatus { get; set; }
        public bool Deleted { get; set; }
    }

    public class SendToOrganizationViewModel
    {
        public int FlowInstanceID { get; set; }
        public int NextPhaseDefinitionID { get; set; }
        //public int[] DepartmentIDs { get; set; }
        public int FormTypeID { get; set; } = (int)FlowFormType.NoScreen;
        public InviteType InviteType { get; set; } = InviteType.Regular;
        public bool DeleteExisting { get; set; }

        public List<DepartmentData> Departments { get; set; }
    }
}
