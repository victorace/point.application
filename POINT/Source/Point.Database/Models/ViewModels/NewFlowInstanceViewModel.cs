﻿
using System.Collections.Generic;

namespace Point.Database.Models.ViewModels
{
    public class NewFlowInstanceViewModel
    {   
        public List<Option> FlowDefinitions { get; set; } = new List<Option>();
        public List<Option> MultipleDepartments { get; set; } = new List<Option>();
        public bool HasMultipleDepartments { get; set; }
        public string DepartmentIDReference { get; set; }
        public bool HasMultipleFlowDefinitions { get; set; }
    }
}