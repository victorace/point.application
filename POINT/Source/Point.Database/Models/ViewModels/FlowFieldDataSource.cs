﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Point.Models.Enums;
using System.Collections.Generic;

namespace Point.Database.Models.ViewModels
{
    public class FlowFieldDataSourceViewModel
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public FlowFieldDataSourceType Type { get; set; }

        public List<Option> Options { get; set; }

        public string MethodName { get; set; }

        public FlowFieldDataSourceViewModel()
        {
            Options = new List<Option>();
        }   
    }

    public class Option
    {
        public string Text { get; set; }
        public string Value { get; set; }
        public bool Selected { get; set; }
        public string Digest { get; set; }
        public bool Inactive { get; set; }

        public Option() { }

        public Option(string text) : this(text, text, selected: false, digest: "", inactive: false) { }

        public Option(string text, int value, bool selected = false, string digest = "", bool inactive = false) 
            : this(text, value.ToString(), selected, digest, inactive) { }

        public Option(string text, string value, bool selected = false, string digest = "", bool inactive = false)
        {
            Inactive = inactive;
            if (inactive)
                Text = $"{text} (verwijderd)";
            else
                Text = text;

            Value = value;
            Selected = selected;
            Digest = digest;
        }

        public Option(Option src)
        {
            Text = src.Text;
            Value = src.Value;
            Selected = src.Selected;
            Digest = src.Digest;
            Inactive = src.Inactive;
        }
    }
}