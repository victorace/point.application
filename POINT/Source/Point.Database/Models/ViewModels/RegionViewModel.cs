﻿using Point.Database.Attributes;
using Point.Models.Enums;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;


namespace Point.Database.Models.ViewModels
{
    public class FormTypeForRegion {
        public int FormTypeID { get; set; }
        public string Name {get;set;}
        public bool IsSelected { get; set; }

    }

    public class RegionViewModel
    {
        public int RegionID { get; set; }

        [Required(ErrorMessage = BaseEntity.errorRequired)]
        [DisplayName("Naam van regio")]
        [WriteAccess(FunctionRoleLevelID.Global, FunctionRoleTypeID.ManageRegion)]
        public string Name { get; set; }

        [DisplayName("Capaciteit")]
        [WriteAccess(FunctionRoleLevelID.Global, FunctionRoleTypeID.ManageRegion)]
        public bool CapacityBeds { get; set; }

        [DisplayName("Capaciteit Extern")]
        [WriteAccess(FunctionRoleLevelID.Global, FunctionRoleTypeID.ManageRegion)]
        public bool CapacityBedsPublic { get; set; }

        [DisplayName("Flow")]
        public FlowDefinitionID DefaultFlowDefinitionID { get; set; } = FlowDefinitionID.ZH_VVT;

        public List<FormTypeForRegion> FormTypesForRegion { get; set; }
        public Dictionary <FlowDefinitionID, List<FormTypeForRegion>> AllFormTypesOfRegion { get; set; }
    }
}
