﻿

namespace Point.Database.Models.ViewModels
{
    public class AdditionalInfoRequestTPViewModel : FormTypeBaseViewModel
    {
        public bool AllowCreateTask => UseTasks & (!Global.IsReadMode && !Global.IsPageLock);
        public bool DisplayCommunication => Global.PointUserInfo.IsTransferEmployee || Global.PointUserInfo.Employee.EditAllForms;
        public bool UseTasks => Global.PointUserInfo.Organization.UseTasks;
        public bool ShowInformatieHuisartsenBericht { get; set; }
    }
}
