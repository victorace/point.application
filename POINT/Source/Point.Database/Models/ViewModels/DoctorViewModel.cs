﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Point.Database.Models.ViewModels
{
    public class DoctorViewModel
    {
        public int DoctorID { get; set; }
        [Required(ErrorMessage = BaseEntity.errorRequired)]
        [DisplayName("Naam van arts")]
        public string Name { get; set; }
        [Required(ErrorMessage = BaseEntity.errorRequired)]
        [DisplayName("Zoeknaam")]
        public string SearchName { get; set; }
        [DisplayName("Specialisme")]
        [Required(ErrorMessage = BaseEntity.errorRequired)]
        public int? SpecialismID { get; set; }
        [DisplayName("AGB code")]
        public string AGB { get; set; }
        [DisplayName("BIG code")]
        public string BIG { get; set; }
        [DisplayName("Telefoon")]
        public string PhoneNumber { get; set; }
        [DisplayName("Inactief")]
        public bool Inactive { get; set; }
        [DisplayName("Organisatie")]
        public int OrganizationID { get; set; }
        [DisplayName("Locatie(s)")]
        public int[] LocationIDS { get; set; }

        public static DoctorViewModel FromModel(Doctor model)
        {
            return new DoctorViewModel()
            {
                DoctorID = model.DoctorID,
                AGB = model.AGB,
                BIG = model.BIG,
                Inactive = model.Inactive,
                LocationIDS = model.DoctorLocation.Select(it => it.LocationID).ToArray(),
                Name = model.Name,
                OrganizationID = model.OrganizationID.GetValueOrDefault(),
                SpecialismID = model.SpecialismID.GetValueOrDefault(),
                PhoneNumber = model.PhoneNumber,
                SearchName = model.SearchName
            };
        }
        
    }
}
