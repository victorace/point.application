﻿using System.Collections.Generic;

namespace Point.Database.Models.ViewModels
{
    public class ZorgAdviesViewModel : FormTypeBaseViewModel
    {
        public List<ZorgAdviesItemViewModel> ZorgAdviesItems { get; set; }
        public bool HideZorgAdvies { get; set; }
    }
}
