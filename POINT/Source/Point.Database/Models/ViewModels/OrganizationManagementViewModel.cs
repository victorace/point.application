﻿using System.Collections.Generic;
using Point.Models.Enums;

namespace Point.Database.Models.ViewModels
{
    public class OrganizationManagementViewModel
    {
        public FunctionRoleLevelID FunctionRoleLevelID { get; set; }
        public bool HasDepartmentEditRights => FunctionRoleLevelID >= FunctionRoleLevelID.Department;
        public bool HasLocationEditRights => FunctionRoleLevelID >= FunctionRoleLevelID.Location;
        public bool HasOrganizationEditRights => FunctionRoleLevelID >= FunctionRoleLevelID.Organization;
        public bool HasRegionEditRights => FunctionRoleLevelID >= FunctionRoleLevelID.Region;
        public bool OnlyOrganizations { get; set; }
        public List<OrganizationLocationDepartmentViewModel> OrganizationLocationDepartments { get; set; } = new List<OrganizationLocationDepartmentViewModel>();
        public string Search { get; set; }
        public bool SearchDeleted { get; set; }

    }
}