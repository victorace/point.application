﻿

namespace Point.Database.Models.ViewModels
{
    public class AidProductViewModel
    {
        public int AidProductID { get; set; }
        public int AidProductGroupID { get; set; }
        public string Name { get; set; }

        public string FullName
        {
            get
            {
                var priceText = "";

                if (PricePerDay > 0)
                {
                    priceText = "€ " + PricePerDay + " per dag";
                }

                if (Price > 0)
                {
                    if (priceText != "")
                    {
                        priceText += " + ";
                    }

                    priceText += "€ " + Price + " bij levering";
                }

                return priceText != "" ? $"{Name} ({priceText})" : Name;
            }
        }

        public string Description { get; set; }
        public decimal Price { get; set; }
        public decimal PricePerDay { get; set; }
        public byte[] ImageData { get; set; }
    }
}
