﻿using Point.Database.Extensions;
using Point.Database.Repository;
using Point.Log.Enums;
using Point.Log.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Point.Database.Models.ViewModels
{
    public class GenericLogDetailsViewModel
    {
        public string EmployeeName { get; set; }
        public DateTime TimeStamp { get; set; }
        public ChangeType ChangeType { get; set; }
        public string TableName { get; set; }
        public string PrimaryKeys { get; set; }
        public string FieldName { get; set; }
        public string OldValue { get; set; }
        public string NewValue { get; set; }

        public static List<GenericLogDetailsViewModel> FromModelList(IUnitOfWork uow, IEnumerable<EntityLog> modellist)
        {
            var employeeids = modellist.Select(it => it.EntityLogSet.EmployeeID).Distinct().ToArray();
            var employees = uow.EmployeeRepository.Get(e => employeeids.Contains(e.EmployeeID)).ToList();

            return modellist.Select(m => new GenericLogDetailsViewModel()
            {
                EmployeeName = employees.FirstOrDefault(e => e.EmployeeID == m.EntityLogSet.EmployeeID).FullName(),
                ChangeType = m.ChangeType,
                TimeStamp = m.EntityLogSet.TimeStamp,
                TableName = m.TableName,
                PrimaryKeys = m.PrimaryKeys,
                FieldName = m.FieldName,
                OldValue = m.OldValue,
                NewValue = m.NewValue
            }).ToList();
        }
    }

    public class GenericLogViewModel
    {
        public string TableName { get; set; }
        public string PrimaryKey { get; set; }
    }
}
