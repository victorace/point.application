﻿using System.Collections.Generic;

namespace Point.Database.Models.ViewModels
{
    public class EmailSignaleringReminder
    {
        public string Url { get; set; }
        public int SignalCount { get; set; }
        public string LatestSignal { get; set; }
        public List<SenderSignalering> SenderSignalering;

        public EmailSignaleringReminder()
        {
            SenderSignalering = new List<SenderSignalering>();
        }
    }
    

    public class SenderSignalering
    {
        public int SignleringID { get; set; }
        public string SignaleringDateTime { get; set; }
        public string OrganizationName { get; set; }
        public string SignaleringTypeComment { get; set; }
    }   

}
