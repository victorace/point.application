﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Point.Database.Attributes;

namespace Point.Database.Models.ViewModels
{
    public class AccountDetailsViewModel
    {
        public int EmployeeID { get; set; }

        [DisplayName("Voornaam")]
        public string FirstName { get; set; }

        [DisplayName("Tussenvoegsel")]
        public string MiddleName { get; set; }

        [DisplayName("Achternaam")]
        [Required(ErrorMessage = BaseEntity.errorRequired)]
        public string LastName { get; set; }

        [DisplayName("Functie")]
        public string Position { get; set; }

        [DisplayName("Telefoonnummer/Piepernummer")]
        [Required(ErrorMessage = BaseEntity.errorRequired)]
        public string TelephoneNumber { get; set; }

        [DisplayName("Emailadres")]
        [Required(ErrorMessage = BaseEntity.errorRequired)]
        [EmailAddressValidator]
        public string EmailAdress { get; set; }

        [DisplayName("BIG-nummer")]
        public string BIG { get; set; }

        public bool NotAutoLogin { get; set; }

        [DisplayName("Controlevraag instellen")]
        public bool SetSecretQuestion { get; set; }

        [DisplayName("Vraag")]
        [Required(ErrorMessage = BaseEntity.errorRequired)]
        public string SecretQuestion { get; set; }

        [DisplayName("Antwoord")]
        [Required(ErrorMessage = BaseEntity.errorRequired)]
        public string SecretQuestionAnswer { get; set; }

        [DisplayName("Uw huidige wachtwoord (ter controle)")]
        [Required(ErrorMessage = BaseEntity.errorRequired)]
        public string PasswordCheck { get; set; }

        [DisplayName("E-mail versturen openstaande signaleringen")]
        public bool AutoEmailSignalering { get; set; }
    }
}
