﻿using Point.Models.Enums;

namespace Point.Database.Models.ViewModels
{
    public class SearchStatement
    {
        public SortOrder SortOrder { get; set; }
        public string SortColumn { get; set; }
        public SearchFieldOwningType SearchFieldOwningType { get; set; }
        public int Page { get; set; }
    }
}