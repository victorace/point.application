﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Point.Database.Models.ViewModels
{
    public class FlowInterruptionViewModel : BaseEntity
    {
        public int TransferID { get; set; }
        public bool SignalOnSave { get; set; }

        [DisplayName("Datum")]
        [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}")]
        [Required(ErrorMessage = errorRequired)]
        public DateTime InterruptionDate { get; set; }

        [DisplayName("Reden")]
        public string InterruptionReason { get; set; }
    }

    public class FlowResumeViewModel : BaseEntity
    {
        public int TransferID { get; set; }
        public int? RedirectFormTypeID { get; set; }
        public bool SignalOnSave { get; set; }

        [DisplayName("Datum")]
        [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}")]
        [Required(ErrorMessage = errorRequired)]
        public DateTime ResumeDate { get; set; }

        [DisplayName("Reden")]
        public string ResumeReason { get; set; }

        public bool HasActiveReciever { get; set; }
        public string ActiveRecieverName { get; set; }
    }
}
