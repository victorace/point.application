﻿
using Point.Models.Enums;

namespace Point.Database.Models.ViewModels
{
    public class InvullenCVARegistratie3MndViewModel : FormTypeBaseViewModel
    {
        public int FrequencyID { get; set; } = -1;
        public FrequencyStatus FrequencyStatus { get; set; } = FrequencyStatus.NotActive;
        public bool ShowReopenButton => FrequencyStatus == FrequencyStatus.Done;
    }
}
