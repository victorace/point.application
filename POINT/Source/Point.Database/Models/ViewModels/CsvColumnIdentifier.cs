﻿
namespace Point.Database.Models.ViewModels
{
    public class CsvColumnIdentifier
    {
        public string Name { get; set; }
        public string NameUpper => Name.ToUpperInvariant();
        public bool IsRequired { get; set; }

    }
}
