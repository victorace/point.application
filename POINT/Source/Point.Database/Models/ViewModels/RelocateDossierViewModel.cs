﻿using Point.Models.Enums;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Point.Database.Models.ViewModels
{
    public class RelocateDossierViewModel
    {
        public int OrganizationTypeID { get; set; }
        public FlowDefinitionID FlowDefinitionID { get; set; }

        public int FlowInstanceID { get; set; }
        public int TransferID { get; set; }

        [DisplayName("Organisatie")]
        public string SourceOrganizationName { get; set; }
        public int SourceOrganizationID { get; set; }

        [DisplayName("Locatie")]
        public string SourceLocationName { get; set; }
        public int SourceLocationID { get; set; }

        [DisplayName("Afdeling")]
        public string SourceDepartmentName { get; set; }
        public int SourceDepartmentID { get; set; }

        [DisplayName("Kamernummer")]
        public string SourceRoomNumber { get; set; }

        [DisplayName("Nieuwe organisatie")]
        [Required(ErrorMessage = BaseEntity.errorRequired)]
        public string DestinationOrganizationName { get; set; }
        
        [Required(ErrorMessage = BaseEntity.errorRequired)]
        public int? DestinationOrganizationID { get; set; }

        [DisplayName("Nieuwe locatie")]
        [Required(ErrorMessage = BaseEntity.errorRequired)]
        public int DestinationLocationID { get; set; }

        [DisplayName("Nieuwe afdeling")]
        [Required(ErrorMessage = BaseEntity.errorRequired)]
        public int DestinationDepartmentID { get; set; }

        [DisplayName("Nieuwe kamernummer")]
        public string DestinationRoomNumber { get; set; }
        public bool CanHaveNewDestinationRoomNumber { get; set; }

        [DisplayName("Reden voor verplaatsing")]
        public string ReasonForRelocation { get; set; }
    }
}
