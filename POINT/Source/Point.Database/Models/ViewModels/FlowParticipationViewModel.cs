﻿using Point.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Point.Database.Models.ViewModels
{
    public class FlowParticipationViewModel
    {
        public FlowDefinitionID FlowDefinitionID { get; set; }
        public string Name { get; set; }
        public Participation Participation { get; set; }
        public FlowDirection FlowDirection { get; set; }

        public bool DisableByDepartment { get; set; }
        /// <summary>
        /// to hold original participation of a department, inherited from location
        /// </summary>
        public Participation DepartmentLocationParticipation {get;set;}
        public List<Participation> Participations { get; set; }

        public int DefaultSendingOrganizationTypeID {
            get
            {
                switch (FlowDefinitionID)
                {
                    case FlowDefinitionID.VVT_ZH:
                    case FlowDefinitionID.VVT_VVT:
                        return (int)OrganizationTypeID.HealthCareProvider;

                    case FlowDefinitionID.CVA:
                    case FlowDefinitionID.ZH_VVT:
                    case FlowDefinitionID.RzTP:
                        return (int)OrganizationTypeID.Hospital;

                    case FlowDefinitionID.HA_VVT:
                        return (int)OrganizationTypeID.GeneralPracticioner;

                    default:
                        return 0;
                }
            }
        }
        public int DefaultRecievingOrganizationTypeID
        {
            get
            {
                switch (FlowDefinitionID)
                {
                    case FlowDefinitionID.VVT_ZH:
                        return (int)OrganizationTypeID.Hospital;

                    case FlowDefinitionID.CVA:
                    case FlowDefinitionID.ZH_VVT:
                    case FlowDefinitionID.RzTP:
                    case FlowDefinitionID.HA_VVT:
                    case FlowDefinitionID.VVT_VVT:
                        return (int)OrganizationTypeID.HealthCareProvider;

                    default:
                        return 0;
                }
            }
        }

        public static List<FlowParticipationViewModel> FromModelList(Department department, List<FlowDefinition> flowdefinitions)
        {
            var viewmodellist = new List<FlowParticipationViewModel>();
            var participationList = Enum.GetValues(typeof(Participation)).Cast<Participation>().ToList();
            //First the default (from the location)
            foreach (var flowdefinition in flowdefinitions)
            {
                var viewmodel = FromModel(participationList, flowdefinition, department?.Location?.FlowDefinitionParticipation.
                    FirstOrDefault(it => it.FlowDefinitionID == flowdefinition.FlowDefinitionID && 
                    it.DepartmentID == null && it.Participation != Participation.None), 
                    FunctionRoleLevelID.None);
                if (viewmodel.Participation == Participation.None)
                {
                    continue;
                }
                var departmentoverride = department?.FlowDefinitionParticipation?.FirstOrDefault(it => it.FlowDefinitionID == flowdefinition.FlowDefinitionID);
                if (departmentoverride != null)
                {
                    viewmodel.Participation = departmentoverride.Participation;
                }                
                viewmodellist.Add(viewmodel);
            }

            return viewmodellist;
        }

        public static List<FlowParticipationViewModel> FromModelList(Location location, List<FlowDefinition> flowdefinitions, FunctionRoleLevelID maxFunctionRoleLevelID)
        {
            var viewmodellist = new List<FlowParticipationViewModel>();
            var participationList = Enum.GetValues(typeof(Participation)).Cast<Participation>().ToList();

            foreach (var flowdefinition in flowdefinitions)
            {
                viewmodellist.Add(FromModel(participationList, flowdefinition, location?.FlowDefinitionParticipation.FirstOrDefault(it => it.FlowDefinitionID == flowdefinition.FlowDefinitionID && it.DepartmentID == null), maxFunctionRoleLevelID));
            }

            return viewmodellist;
        }      

        public static FlowParticipationViewModel FromModel(List<Participation> participationList, FlowDefinition flowdefinition, FlowDefinitionParticipation flowDefinitionParticipation, FunctionRoleLevelID maxFunctionRoleLevelID)
        {
            var viewmodel = new FlowParticipationViewModel
            {
                FlowDefinitionID = flowdefinition.FlowDefinitionID,
                Name = flowdefinition.Description,
                Participation = flowDefinitionParticipation?.Participation ?? Participation.None,                
                DepartmentLocationParticipation = flowDefinitionParticipation?.Participation ?? Participation.None,
                FlowDirection = flowDefinitionParticipation?.FlowDirection ?? FlowDirection.None
            };
            if (flowDefinitionParticipation == null || maxFunctionRoleLevelID == FunctionRoleLevelID.None)      // maxFunctionRoleLevelID = null from Department
            {
                viewmodel.Participations = participationList;
                return viewmodel;
            }
            if (maxFunctionRoleLevelID < FunctionRoleLevelID.Region && flowDefinitionParticipation.Participation < Participation.Active)
            {
                viewmodel.Participations = participationList.Where(p => p != Participation.Active && p!=Participation.ActiveButInvisibleInSearch).ToList();
            }
            else if (maxFunctionRoleLevelID < FunctionRoleLevelID.Global && flowDefinitionParticipation.Participation != Participation.FaxMail && flowDefinitionParticipation.FlowDirection == FlowDirection.Sending)
            {
                viewmodel.Participations = participationList.Where(p => p != Participation.FaxMail).ToList();
            }
            else
            {
                viewmodel.Participations = participationList;
            }

            return viewmodel;
        }
    }
}
