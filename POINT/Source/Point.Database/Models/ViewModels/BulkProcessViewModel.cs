﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Point.Database.Models.ViewModels
{
    public class BulkProcessViewModel
    {
        public string ProcessType { get; set; }
        public DateTime? LastLoginDate { get; set; }
        public List<EmployeeBulkProcessViewModel> Employees { get; set; }
    }
}
