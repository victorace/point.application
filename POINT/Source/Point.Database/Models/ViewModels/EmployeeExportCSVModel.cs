﻿using Point.Models.Enums;
using Point.Models.LDM.EOverdracht30.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Point.Database.Models.ViewModels
{
    public class EmployeeExportCSVModel
    {
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public Geslacht? Gender { get; set; }
        public string Position { get; set; }
        public string PhoneNumber { get; set; }
        public string EmailAddress { get; set; }
        public string ExternID { get; set; }
        public string BIG { get; set; }
        public string Organization { get; set; }
        public string Location { get; set; }
        public string DefaultDepartment { get; set; }

        public List<string> _EmployeeDepartments;
        public string EmployeeDepartments { get { return string.Join(";", _EmployeeDepartments.OrderBy(d => d)); } }

        public string UserName { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? LastLoginDate { get; set; }
        public bool IsLockedOut { get; set; }
        public bool InActive { get; set; }
        public bool ChangePasswordByEmployee { get; set; }
        public string Role { get; set; }
        public FunctionRoleLevelID? ReportingLevel { get; set; }
        public FunctionRoleLevelID? FunctionLevel { get; set; }
        public bool ManageDoctors { get; set; }
        public bool ManageOrganization { get; set; }
        public bool ManageEmployees { get; set; }
        public bool ManageFavorites { get; set; }
        public bool ManageSearch { get; set; }
        public bool ManageDocuments { get; set; }
        public bool ManageForms { get; set; }
        public bool ManageCapacity { get; set; }
        public bool ManageServicePage { get; set; }
        public bool ManageTransferMemoTemplates { get; set; }
        public bool ManageLogs { get; set; }
    }
}
