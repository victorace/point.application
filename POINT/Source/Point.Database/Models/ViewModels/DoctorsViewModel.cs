﻿using System.Collections.Generic;

namespace Point.Database.Models.ViewModels
{
    public class DoctorsViewModel
    {
        public List<Doctor> Doctors { get; set; } = new List<Doctor>();
        public int PointUserRegionID { get; set; }
        public int? OrganizationID { get; set; }
        public List<Option> Organizations { get; set; } = new List<Option>();
        public int? RegionID { get; set; }
        public List<Option> Regions { get; set; } = new List<Option>();
    }
}