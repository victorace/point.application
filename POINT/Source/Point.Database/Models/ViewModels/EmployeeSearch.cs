﻿using System.ComponentModel.DataAnnotations;

namespace Point.Database.Models.ViewModels
{
    public class EmployeeSearch
    {
        public int OrganizationID { get; set; }
        public int LocationID { get; set; }
        public int DepartmentID { get; set; }

        [Required]
        public int EmployeeID { get; set; }
        [Required]
        public string EmployeeName { get; set; }
    }
}
