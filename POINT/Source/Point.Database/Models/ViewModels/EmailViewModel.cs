﻿namespace Point.Database.Models.ViewModels
{
    public class EmailViewModel : ICommunicationLog
    {
        public string ReceivingDepartment{ get; set; }
        public string ReceivingLocation { get; set; }
        public string ReceivingOrganization { get; set; }
        public string ReceivingEmailAdress { get; set; }
        
        public string SendingUser { get; set; }
        public string SendingOrganization { get; set; }
        public string SendingLocation { get; set; }
        public string SendingDepartment { get; set; }
        public string SendingTelephoneNumber { get; set; }

        public int TransferID { get; set; }
        public string DossierUrl { get; set; }
        public string PatientName { get; set; }

        public int? EmployeeID { get; set; }
        public int? OrganizationID { get; set; }
    }
}
