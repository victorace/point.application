﻿

namespace Point.Database.Models.ViewModels
{
    public class MoveToDepartmentViewModel : FormTypeBaseViewModel
    {
        public int CurrentLocationID { get; set; } = -1;
    }
}
