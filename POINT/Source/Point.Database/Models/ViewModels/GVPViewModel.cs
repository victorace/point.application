﻿using System.Collections.Generic;
using System.Linq;

namespace Point.Database.Models.ViewModels
{
    public class GVPViewModel : FormTypeSharedViewModel
    {
        public static List<int> RisicovolleGroupIds => new List<int> {17, 18, 22, 23, 24, 32};
        public static List<int> UitvoeringsVerzoekGroupIds => new List<int> {33, 34, 35, 36, 37, 38, 39, 40};

        public bool HasRisicoVolle { get; set; }
        public bool HasUitvoeringsVerzoek { get; set; }

        public bool DisplayHighRiskActionsInfo => !string.IsNullOrEmpty(HighRiskActionsInfo) && !Global.IsReadMode;
        public bool DisplayUvvActionsInfo => !string.IsNullOrEmpty(UvvActionsInfo) && !Global.IsReadMode;
        public List<GVPHandelingViewModel> GVPHandelingItems { get; set; }
        public string HighRiskActionsInfo { get; set; }
        public bool IsRisicoVolle { get; set; }
        public bool IsUitvoeringsVerzoek { get; set; }
        public string UvvActionsInfo { get; set; }
        public int GVPNumber
        {
            get
            {
                {
                    var gvpNumber = 1;
                    if (!FormSetVersions.Any())
                    {
                        return gvpNumber;
                    }

                    gvpNumber = FormSetVersions.Count();
                    
                    if (Global.FormSetVersionID > 0)
                    {
                        var index = FormSetVersions.ToList().FindIndex(f => f.FormSetVersionID == Global.FormSetVersionID);
                        if (index > -1)
                        {
                            // IndexOf is zero based
                            gvpNumber = index+1;
                        }
                    }
                    else
                    {
                        gvpNumber++;
                    }

                    return gvpNumber;
                }
            }

        }
    }
}
