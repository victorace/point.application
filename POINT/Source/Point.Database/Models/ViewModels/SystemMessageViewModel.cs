﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Point.Database.Models.ViewModels
{
    public class SystemMessageViewModel
    {
        public int SystemMessageID { get; set; }

        [Required(ErrorMessage = BaseEntity.errorRequired)]
        [StringLength(2000)]
        [DisplayName("Titel")]
        public string Title { get; set; }

        [Required(ErrorMessage = BaseEntity.errorRequired)]
        [DisplayName("Bericht")]
        public string Message { get; set; }

        // For: <a href"LinkReference">LinkText<a>
        [StringLength(200)]
        [DisplayName("Link-ref")]
        public string LinkReference { get; set; }

        // For: <a href"LinkReference">LinkText<a>
        [StringLength(100)]
        [DisplayName("Link-tekst")]
        public string LinkText { get; set; }

        [Required(ErrorMessage = BaseEntity.errorRequired)]
        [DisplayName("Melding startdatum")]
        public DateTime StartDate { get; set; }

        [Required(ErrorMessage = BaseEntity.errorRequired)]
        [DisplayName("Melding starttijd")]
        public TimeSpan StartTime { get; set; }

        [Required(ErrorMessage = BaseEntity.errorRequired)]
        [DisplayName("Melding einddatum")]
        public DateTime EndDate { get; set; }

        [Required(ErrorMessage = BaseEntity.errorRequired)]
        [DisplayName("Melding eindtijd")]
        public TimeSpan EndTime { get; set; }

        [StringLength(50)]
        public string FunctionCode { get; set; }

        [DisplayName("Permanent")]
        public bool IsPermanent { get; set; }

        [DisplayName("Aktief")]
        public bool IsActive { get; set; }
    }
}
