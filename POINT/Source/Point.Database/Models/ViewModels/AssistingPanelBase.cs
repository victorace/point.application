﻿using System.Collections.Generic;
using Point.Models.Enums;

namespace Point.Database.Models.ViewModels
{
    public class AssistingPanelBase
    {
        private FunctionRoleLevelID _functionRoleLevelId;
        public AssistingPanelBase()
        {
            _functionRoleLevelId = FunctionRoleLevelID.None;
        }
        public void SetMaximumFunctionRoleLevel(FunctionRoleLevelID maxLevel)
        {
            _functionRoleLevelId = maxLevel;
        }
        public bool HasDepartmentRights => _functionRoleLevelId >= FunctionRoleLevelID.Department;
        public bool HasGlobalRights => _functionRoleLevelId >= FunctionRoleLevelID.Global;
        public bool HasLocationRights => _functionRoleLevelId >= FunctionRoleLevelID.Location;
        public bool HasOrganizationRights => _functionRoleLevelId >= FunctionRoleLevelID.Organization;
        public bool HasRegionRights => _functionRoleLevelId >= FunctionRoleLevelID.Region;        
    }
}
