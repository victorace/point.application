﻿using System.Collections.Generic;

namespace Point.Database.Models.ViewModels
{
    public class RequiredByValue
    {
        public IList<string> Values { get; set; }
    }
}
