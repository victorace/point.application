﻿

namespace Point.Database.Models.ViewModels
{
    public class RealizedCareViewModel : FormTypeBaseViewModel
    {
        public string ReceivingDepartmentID { get; set; }
        public string ReceivingDepartmentName { get; set; }

    }
}
