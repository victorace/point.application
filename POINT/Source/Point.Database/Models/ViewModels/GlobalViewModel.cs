﻿

namespace Point.Database.Models.ViewModels
{
    public class GlobalViewModel
    {
        private bool _isSet;

        // This is just merely to make sure the GlobalProperties are passed to the model
        public void SetGlobalProperties(GlobalProperties globalProps = null)
        {
            if (globalProps == null)
            {
                globalProps = new GlobalProperties();
            }
            Global = globalProps;
            _isSet = true;
        }
        
        public bool IsGlobalPropertiesSet => _isSet && Global != null;
        public string ErrorMessage { get; set; }
        public GlobalProperties Global { get; private set; }

        public bool HasErrors => !string.IsNullOrEmpty(ErrorMessage);
        public bool IsInstanced { get; set; } = true;       // Set manually to false when we want to 'act' as NULL in the view (for eventual/needed checks)
    }
}
