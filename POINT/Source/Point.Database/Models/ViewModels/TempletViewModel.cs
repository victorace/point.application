﻿
using System;
using System.ComponentModel.DataAnnotations;
using Point.Models.Enums;
using System.ComponentModel;

namespace Point.Database.Models.ViewModels
{
    public class TempletViewModel
    {
        public int TempletID { get; set; }

        public int? RegionID { get; set; }
        public TempletTypeID TempletTypeID { get; set; }

        public DateTime? UploadDate { get; set; }

        [StringLength(50)]
        [DisplayName("Naam bestand")]
        public string Name { get; set; }

        [StringLength(200)]
        public string OrganizationName { get; set; }

        [StringLength(200)]
        [DisplayName("Omschrijving")]
        public string Description { get; set; }

        public int? FileSize { get; set; }
        
        [StringLength(255)]
        public string FileName { get; set; }

        public byte[] FileData { get; set; }

        [StringLength(255)]
        public string ContentType { get; set; }

        public int? EmployeeID { get; set; }
        public string EmployeeName { get; set; }

        public bool Deleted { get; set; }

        [DisplayName("Sorteerpositie")]
        public int? SortOrder { get; set; }
    }
}

