﻿using Newtonsoft.Json;
using Point.Models.Enums;
using System.Collections.Generic;

namespace Point.Database.Models.ViewModels
{
    public class SearchControl
    {
        public FlowDirection FlowDirection { get; set; }
        public SearchType SearchType { get; set; }
        public FlowDefinitionID[] FlowDefinitionsSender { get; set; }
        public FlowDefinitionID[] FlowDefinitionsReceiver { get; set; }
        public IEnumerable<LocationDepartmentSelection> Departments { get; set; }

        [JsonIgnore]
        public int Page { get; set; }
        public int Count { get; set; }
        public SortOrder SortOrder { get; set; }
        public string SortColumn { get; set; }
        public DossierType type { get; set; }
        public DossierHandling handling { get; set; }
        public string[] phases { get; set; }
        public int[] DepartmentIDs { get; set; }
        public SearchLogicOperatorType connector { get; set; }
        public string[] date { get; set; }
        public string[] input { get; set; }
        public string[] number { get; set; }
        public string[] select { get; set; }
        public string[] propertyList { get; set; }
        public int[] matchtype { get; set; }
        public string[] flowfield { get; set; }
        public DossierStatus statusFilter { get; set; }
        public DossierStatus status { get; set; }
        public FrequencyDossierStatus frequencyStatusFilter { get; set; }
        public FrequencyDossierStatus frequencyStatus { get; set; }
        public SearchFieldOwningType SearchFieldOwningType { get; set; }

        public FlowDefinitionID[] FlowDefinitionIDs
        {
            get
            {
                var flowdefinitionids = new List<FlowDefinitionID>();

                if (FlowDefinitionsReceiver != null)
                {
                    flowdefinitionids.AddRange(FlowDefinitionsReceiver);
                }
                if (FlowDefinitionsSender != null)
                {
                    flowdefinitionids.AddRange(FlowDefinitionsSender);
                }

                return flowdefinitionids.ToArray();
            }
        }

        [JsonIgnore]
        public SearchStatement SearchStatement
        {
            get
            {
                return new SearchStatement()
                {
                    SortColumn = SortColumn,
                    SortOrder = SortOrder,
                    Page = Page,
                    SearchFieldOwningType = SearchFieldOwningType
                };
            }
        }
    }
}