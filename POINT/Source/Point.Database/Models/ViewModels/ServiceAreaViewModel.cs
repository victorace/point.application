﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Point.Database.Models.ViewModels
{

    public class DepartmentServiceAreaViewModel
    {
        public List<ServiceArea> ServiceAreas { get; set; }

        public List<int?> ServiceAreasSelected { get; set; }

        public List<ServiceAreaPostalCodeViewModel> ServiceAreaPostalCodes { get; set; }

        public static DepartmentServiceAreaViewModel FromModel(Department department)
        {
            var departmentServiceAreaViewModel = new DepartmentServiceAreaViewModel();
            var allserviceareapostalcodes = department.ServiceAreaPostalCode.ToList();
            var serviceareas = department.Location?.Organization.ServiceArea.Where(it => it.Inactive != true).ToList();
            var serviceareapostalcodes = allserviceareapostalcodes.Where(it => it.ServiceAreaID == null).ToList();

            departmentServiceAreaViewModel.ServiceAreas = serviceareas;
            departmentServiceAreaViewModel.ServiceAreasSelected = allserviceareapostalcodes.Select(sap => sap.ServiceAreaID).ToList();
            departmentServiceAreaViewModel.ServiceAreaPostalCodes = ServiceAreaPostalCodeViewModel.FromModelList(serviceareapostalcodes);

            return departmentServiceAreaViewModel;
        }
    }

    public class ServiceAreaViewModel
    {
        public int ServiceAreaID { get; set; }
        public int OrganizationID { get; set; }
        public string Description { get; set; }

        public List<ServiceAreaPostalCodeViewModel> ServiceAreaPostalCodes { get; set; }

        public static ServiceAreaViewModel FromModel(ServiceArea model)
        {
            var viewmodel = new ServiceAreaViewModel
            {
                Description = model.Description,
                OrganizationID = model.OrganizationID.GetValueOrDefault(0),
                ServiceAreaID = model.ServiceAreaID,
                ServiceAreaPostalCodes = ServiceAreaPostalCodeViewModel.FromModelList(model.ServiceAreaPostalCode.Where(it => it.DepartmentID == null).OrderByDescending(it => it.ServiceAreaPostalCodeID))
            };

            return viewmodel;
        }
    }

    public class ServiceAreaPostalCodeViewModel
    {
        public static List<ServiceAreaPostalCodeViewModel> FromModelList(IEnumerable<ServiceAreaPostalCode> modellist)
        {
            var viewmodellist = new List<ServiceAreaPostalCodeViewModel>();
            foreach (var item in modellist.ToList())
            {
                viewmodellist.Add(FromModel(item));
            }

            return viewmodellist;
        }

        public static ServiceAreaPostalCodeViewModel FromModel(ServiceAreaPostalCode model)
        {
            var viewmodel = new ServiceAreaPostalCodeViewModel
            {
                OrganizationID = model.ServiceArea?.OrganizationID,
                DepartmentID = model.DepartmentID,
                Description = model.PostalCode?.Description,
                EndPostalCode = model.PostalCode?.EndPostalCode,
                StartPostalCode = model.PostalCode?.StartPostalCode,
                PostalCodeID = model.PostalCodeID,
                ServiceAreaID = model.ServiceAreaID,
                ServiceAreaPostalCodeID = model.ServiceAreaPostalCodeID
            };

            return viewmodel;
        }

        public int ServiceAreaPostalCodeID { get; set; }

        //Set to null if its a pre department linked postcode (not a organization servicearea)
        public int? ServiceAreaID { get; set; }

        //Set this if its a organization linked postcode
        public int? OrganizationID { get; set; }
        //Set this if its a department linked postcode
        public int? DepartmentID { get; set; }

        //Set to null if its a department using a organization servicearea
        public int? PostalCodeID { get; set; }

        [MaxLength(6)]
        public string StartPostalCode { get; set; }

        [MaxLength(6)]
        public string EndPostalCode { get; set; }

        [MaxLength(255)]
        public string Description { get; set; }
    }
}
