﻿using Point.Database.Attributes;
using Point.Models.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Point.Database.Models.ViewModels
{
    public class FormTypePerOrganization 
    {
        public int FormTypeID { get; set; }
        public int RegionID { get; set; }
        public string Name { get; set; }
        public FlowDefinitionID FlowDefinitionID { get; set; }
        public bool IsSelected { get; set; }
    }

    public class OrganizationViewModel
    {
        public OrganizationViewModel()
        {
            OrganizationProjects = new List<OrganizationProject>();
            FormTypesOfOrganization = new List<FormTypePerOrganization>();
        }

        [DisplayName("Uniek nummer")]
        public int OrganizationID { get; set; }

        [Required(ErrorMessage = BaseEntity.errorRequired)]
        [StringLength(500)]
        [DisplayName("Naam")]
        public string Name { get; set; }

        [Required(ErrorMessage = BaseEntity.errorRequired)]
        [DisplayName("Type")]
        public int OrganizationTypeID { get; set; }
        [ForeignKey("OrganizationTypeID")]
        public virtual OrganizationType OrganizationType { get; set; }

        [Required(ErrorMessage = BaseEntity.errorRequired)]
        [DisplayName("Regio")]
        public int RegionID { get; set; }
        [ForeignKey("RegionID")]
        public virtual Region Region { get; set; }

        [DisplayName("Periode geldigheid wachtwoord (in maanden)")]
        public int? ChangePasswordPeriod { get; set; }

        [DisplayName("Page locking timeout (in minuten)")]
        public int? PageLockTimeout { get; set; }

        [DisplayName("HL7 koppeling info")]
        public string HL7Description { get; set; }

        [DisplayName("Web service koppeling")]
        public bool WSInterface { get; set; }

        [DisplayName("Toon waarschuwing bij 'niet opslaan'")]
        public bool ShowUnloadMessage { get; set; }

        [DisplayName("Bericht naar huisarts bij VO definitief")]
        [WriteAccess(FunctionRoleLevelID.Global, FunctionRoleTypeID.ManageOrganization)]
        public bool MessageToGPByDefinitive { get; set; }

        [DisplayName("Bericht naar huisarts bij afsluiten van transfer")]
        [WriteAccess(FunctionRoleLevelID.Global, FunctionRoleTypeID.ManageOrganization)]
        public bool MessageToGPByClose { get; set; }

        [StringLength(20)]
        [DisplayName("AGB code")]
        public string AGB { get; set; }

        [DisplayName("Inactief")]
        public bool Inactive { get; set; }

        public bool SaveAdminSettings { get; set; }
        public bool SaveCouplingSettings { get; set; }

        [DisplayName("Gebruikt SSO")]
        [WriteAccess(FunctionRoleLevelID.Global, FunctionRoleTypeID.ManageOrganization)]
        public bool UsesSSO { get; set; }

        [DisplayName("Gebruikt Auth")]
        [WriteAccess(FunctionRoleLevelID.Global, FunctionRoleTypeID.ManageOrganization)]
        public bool UsesAuth { get; set; }

        [DisplayName("Gebruikt AutoCreateAccount")]
        [WriteAccess(FunctionRoleLevelID.Global, FunctionRoleTypeID.ManageOrganization)]
        public bool SSOUsesAutoCeateAccount { get; set; }

        [DisplayName("SSO Key")]
        [WriteAccess(FunctionRoleLevelID.Global, FunctionRoleTypeID.ManageOrganization)]
        [ReadAccess(FunctionRoleLevelID.Global, FunctionRoleTypeID.ManageOrganization)]
        public string SSOKey { get; set; }

        [DisplayName("SSO Hashtype")]
        [WriteAccess(FunctionRoleLevelID.Global, FunctionRoleTypeID.ManageOrganization)]
        [ReadAccess(FunctionRoleLevelID.Global, FunctionRoleTypeID.ManageOrganization)]
        public HashAlgorythm? SSOHashType { get; set; }

        [DisplayName("SSO Hashtype")]
        [WriteAccess(FunctionRoleLevelID.Global, FunctionRoleTypeID.ManageOrganization)]
        [ReadAccess(FunctionRoleLevelID.Global, FunctionRoleTypeID.ManageOrganization)]
        public string SSOHashTypeName { get { return SSOHashType.HasValue ? Enum.GetName(typeof(HashAlgorythm), SSOHashType) : null; } }

        [DisplayName("Multi factor authenticatie")]
        [WriteAccess(FunctionRoleLevelID.Organization, FunctionRoleTypeID.ManageOrganization)]
        public bool MFARequired { get; set; }

        [DisplayName("MFA - Type")]
        [WriteAccess(FunctionRoleLevelID.Organization, FunctionRoleTypeID.ManageOrganization)]
        public MFAAuthType? MFAAuthType { get; set; }

        [DisplayName("MFA - Timeout")]
        [WriteAccess(FunctionRoleLevelID.Organization, FunctionRoleTypeID.ManageOrganization)]
        public int? MFATimeout { get; set; }

        [DisplayName("Client Anoniem (ZHVVT/CVA)")]
        public bool UseAnonymousClient { get; set; }

        [DisplayName("Digitale handtekening")]
        public bool UseDigitalSignature { get; set; }

        [DisplayName("Projecten")]
        public List<OrganizationProject> OrganizationProjects { get; set; }

        public bool EnableServiceAreas { get; set; }

        [DisplayName("Palliatieve zorg in VO")]
        [WriteAccess(FunctionRoleLevelID.Region, FunctionRoleTypeID.ManageOrganization)]
        public bool UsesPalliativeCareInVO { get; set; }

        [DisplayName("Taken")]
        [WriteAccess(FunctionRoleLevelID.Region, FunctionRoleTypeID.ManageOrganization)]
        public bool UseTasks { get; set; }

        [DisplayName("MFA - IP adressen")]
        [WriteAccess(FunctionRoleLevelID.Organization, FunctionRoleTypeID.ManageOrganization)]
        public string IPAddresses { get; set; }

        [DisplayName("E-mail adressen voor deblokkering")]
        [WriteAccess(FunctionRoleLevelID.Organization, FunctionRoleTypeID.ManageOrganization)]
        public string DeBlockEmails { get; set; }
        [DisplayName("E-mail deblokkering aan vaste adressen")]
        [WriteAccess(FunctionRoleLevelID.Organization, FunctionRoleTypeID.ManageOrganization)]
        public bool DeBlockEmailsToOrganization { get; set; }

        [DisplayName("Nedap Crypto certificaat")]
        public int? NedapCryptoCertificateID { get; set; }

        [DisplayName("Nedap Medewerkersnummer")]
        [WriteAccess(FunctionRoleLevelID.Global, FunctionRoleTypeID.ManageOrganization)]
        public string NedapMedewerkerNummer { get; set; }

        [DisplayName("Zorgmail usernaam")]
        [WriteAccess(FunctionRoleLevelID.Global, FunctionRoleTypeID.ManageOrganization)]
        public string ZorgmailUsername { get; set; }

        [DisplayName("Zorgmail wachtwoord")]
        [WriteAccess(FunctionRoleLevelID.Global, FunctionRoleTypeID.ManageOrganization)]
        public string ZorgmailPassword { get; set; }

        [DisplayName("Verberg knop 'Alle Dossiers' bij patientfocus")]
        [WriteAccess(FunctionRoleLevelID.Region, FunctionRoleTypeID.ManageOrganization)]
        public bool HL7ConnectionDifferentMenu { get; set; }

        [DisplayName("HL7 koppeling vanuit POINT initieren")]
        [WriteAccess(FunctionRoleLevelID.Global, FunctionRoleTypeID.ManageOrganization)]
        
        public bool HL7ConnectionCallFromPoint { get; set; }

        public FlowDefinitionID SelectedFlowDefinitionID { get; set; } = FlowDefinitionID.ZH_VVT;
        public bool EnableFormTypeOrganizationTab { get; set; }
        public List<FormTypePerOrganization> FormTypesOfOrganization { get; set; }
        
        public int? AutoCloseTransferAfterDays { get; set; }

        [DisplayName("Knop 'overzicht patienten' bij patientfocus")]
        [WriteAccess(FunctionRoleLevelID.Region, FunctionRoleTypeID.ManageOrganization)]
        public bool? PatientOverview { get; set; }

        [DisplayName("Organisatie heeft een logo")]
        public bool Image { get; set; }

        [DisplayName("Organisatie logo")]
        public int? OrganizationLogoID { get; set; }

        public List<Option> OrganizationTypeOptions { get; set; } = new List<Option>();
        public List<Option> RegionOptions { get; set; } = new List<Option>();
        public List<Option> CryptoCertificateOptions { get; set; } = new List<Option>();

    }
}
