﻿using System;
using System.Collections.Generic;

namespace Point.Database.Models.ViewModels
{
    public class FrequencyPrintViewModel
    {
        public int TransferID { get; set; }
        public int ClientID { get; set; }
        public string ClientFullName { get; set; }
        public string ClientFullGender{ get; set; }
        public DateTime? ClientBirthDate { get; set; }
        public string ClientCivilServiceNumber { get; set; }
        public IEnumerable<FrequencyTransferMemoViewModel> FrequencyTransferMemos { get; set; }
    }

    public class FrequencyTransferMemoViewModel
    {
        public int FrequencyID { get; set; }
        public int FormSetVersionID { get; set; }
        public string Name { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int Frequency { get; set; }
        public IEnumerable<TransferMemoViewModel> TransferMemos { get; set; }
    }
}
