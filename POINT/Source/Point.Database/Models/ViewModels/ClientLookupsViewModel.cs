﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Point.Database.Models.ViewModels
{
    public class ClientLookupsViewModel
    {
        public IList<Option> Nationalities { get; set; } = new List<Option>();
        public IList<Option> HealthInsurers { get; set; } = new List<Option>();
        public IList<int> HealthInsurerIDsNotInsured { get; set; } = new List<int>();
        public IList<Option> HousingType { get; set; } = new List<Option>();
        public IEnumerable<CIZRelation> CizRelations { get; set; } = Enumerable.Empty<CIZRelation>();

    }
}
