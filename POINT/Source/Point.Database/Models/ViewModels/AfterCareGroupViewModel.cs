﻿using System.Collections.Generic;
using System.Linq;

namespace Point.Database.Models.ViewModels
{
    public class AfterCareGroupViewModel
    {
        public int AfterCareGroupID { get; set; }
        public string Name { get; set; }
        public IEnumerable<AfterCareTypeViewModel> AfterCareTypes { get; set; } = new List<AfterCareTypeViewModel>();
        public int Sortorder { get; set; }
        public int TotalCapacity => AfterCareTypes?.Sum(x => x.Capacity) ?? 0;
        public bool HasCapacity => TotalCapacity > 0;

        public int? AfterCareTileGroupID { get; set; }
        public bool IsActiveInSelectedRegions { get; set; }
    }
}
