﻿namespace Point.Database.Models.ViewModels
{
    public class CompanyLogoViewModel
    {
        public string LogoPath { get; set; }
    }
}
