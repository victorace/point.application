﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Point.Database.Models.ViewModels
{
    public class ForgotPasswordViewModel
    {
        [DisplayName("E-mailadres")]
        [Required(ErrorMessage = "{0} is verplicht")]
        public string EmailAddress { get; set; }
        [DisplayName("Gebruikersnaam")]
        [Required(ErrorMessage = "{0} is verplicht")]
        public string Username { get; set; }
        [DisplayName("Geheime vraag")]
        public string SecretQuestion { get; set; }
        [DisplayName("Antwoord")]
        [Required(ErrorMessage = "{0} is verplicht")]
        public string SecretAnswer { get; set; }
    }
}