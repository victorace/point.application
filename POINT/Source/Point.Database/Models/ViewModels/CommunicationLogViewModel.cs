﻿
using Point.Models.Enums;

namespace Point.Database.Models.ViewModels
{
    public class CommunicationLogViewModel
    {
        public CommunicationLog CommunicationLog { get; set; }
        public CommunicationLogType CommunicationLogType { get; set; }
        public string OrganizationName { get; set; }
        public string EmployeeName { get; set; }
    }
}
