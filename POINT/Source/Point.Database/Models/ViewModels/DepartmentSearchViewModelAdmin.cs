﻿
using System.Collections.Generic;
using System.Linq;

namespace Point.Database.Models.ViewModels
{
    public class DepartmentSearchViewModelAdmin
    {
        public int OrganizationID { get; set; }
        public int LocationId { get; set; }
        public int DepartmentID { get; set; }

        public string OrganizationName { get; set; }
        public string LocationName { get; set; }
        public string DepartmentName { get; set; }

        public bool Selected { get; set; }

        public static DepartmentSearchViewModelAdmin FromModel(OrganizationSearch orgsearch, int[] selecteddepartmentids)
        {
            var viewmodel = new DepartmentSearchViewModelAdmin();
            viewmodel.OrganizationID = orgsearch.OrganizationID;
            viewmodel.LocationId = orgsearch.LocationID;
            viewmodel.DepartmentID = orgsearch.DepartmentID;
            viewmodel.OrganizationName = orgsearch.OrganizationName;
            viewmodel.LocationName = orgsearch.LocationName;
            viewmodel.DepartmentName = orgsearch.DepartmentName;
            viewmodel.Selected = selecteddepartmentids.Contains(orgsearch.DepartmentID);

            return viewmodel;
        }


        public static List<DepartmentSearchViewModelAdmin> FromModelList(IEnumerable<OrganizationSearch> orgsearch, int[] selecteddepartmentids)
        {
            var viewmodellist = new List<DepartmentSearchViewModelAdmin>();
            foreach (var item in orgsearch)
                viewmodellist.Add(FromModel(item, selecteddepartmentids));

            return viewmodellist;
        }
    }
    
}