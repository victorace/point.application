﻿using System.Collections.Generic;
using System.Linq;
using Point.Models.Enums;

namespace Point.Database.Models.ViewModels
{
    public class FlowDefinitionViewModel
    {
        public string Name { get; set; }
        public FlowDefinitionID FlowDefinitionID { get; set; }
        public FlowDirection FlowDirection { get; set; }
        public Participation Participation { get; set; }
    }

    public class OrganizationLocationDepartmentViewModel
    {
        private List<FlowDirection> _sendingFlowDirection = new List<FlowDirection> { FlowDirection.Any, FlowDirection.Sending };
        private List<FlowDirection>  _receivingFlowDirection = new List<FlowDirection> { FlowDirection.Any, FlowDirection.Receiving };

        public int OrganizationID { get; set; }

        public string OrganizationName { get; set; }

        public int OrganizationTypeID { get; set; }

        public string OrganizationType { get; set; }

        public bool OrganizationInactive { get; set; }

        public int? RegionID { get; set; }

        public string RegionName { get; set; }

        public bool RegionInactive { get; set; }

        public int? LocationID { get; set; }

        public string LocationName { get; set; }

        public bool LocationInactive { get; set; }

        public int? DepartmentID { get; set; }

        public string DepartmentName { get; set; }

        public bool DepartmentInactive { get; set; }

        public List<FlowDefinitionViewModel> FlowDefinitions { get; set; }
        public string FlowDefinitionNames => string.Join(", ", FlowDefinitions.Select(it => it.Name));
        public bool IsDepartmentEditAllowed { get; set; }
        public bool IsDepartmentInsertAllowed { get; set; }
        public bool IsLocationEditAllowed { get; set; }
        public bool IsLocationInsertAllowed { get; set; }
        public bool IsReceivingParty => FlowDefinitions.Any(it => (it.Participation == Participation.Active || it.Participation == Participation.ActiveButInvisibleInSearch) && _receivingFlowDirection.Contains(it.FlowDirection));
        public bool IsSendingParty => FlowDefinitions.Any(it => (it.Participation == Participation.Active || it.Participation == Participation.ActiveButInvisibleInSearch) && _sendingFlowDirection.Contains(it.FlowDirection));
        public bool ShowLocation { get; set; }
        public bool ShowOrganization { get; set; }
    }
}