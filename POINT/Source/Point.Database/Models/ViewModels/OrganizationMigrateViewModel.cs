﻿
using System.Collections.Generic;

namespace Point.Database.Models.ViewModels
{
    public class OrganizationMigrateViewModel
    {
        public OrganizationMigrateViewModel()
        {
            Locations = new List<OrganizationMigrateItemViewModel>();
            OrganizationSource = new OrganizationMigrateOrganizationViewModel(OrganizationSourceOrTarget.Source);
            OrganizationTarget = new OrganizationMigrateOrganizationViewModel(OrganizationSourceOrTarget.Target);
        }

        public string OrganizationNameOrID { get; set; }
        public string SourceOrTarget { get; set; }

        public OrganizationMigrateOrganizationViewModel OrganizationSource { get; set; }
        public OrganizationMigrateOrganizationViewModel OrganizationTarget { get; set; }
        public List<OrganizationMigrateItemViewModel> Locations { get; set; }       // For Source Organization

        //public List<OrganizationMigrateItemViewModel> Organizations { get; set; }
    }

    public class OrganizationMigrateItemViewModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public bool IsSelected { get; set; }
    }

    public static class OrganizationSourceOrTarget
    {
        public const string Source = "source";
        public const string Target = "target";
    }

    public class OrganizationMigrateOrganizationViewModel
    {
        public OrganizationMigrateOrganizationViewModel(string sourceOrTarget)
        {
            if (sourceOrTarget == OrganizationSourceOrTarget.Source || sourceOrTarget == OrganizationSourceOrTarget.Target)
            {
                SourceOrTarget = sourceOrTarget;
            }
            
            Organizations = new List<OrganizationMigrateItemViewModel>();
        }
        public string SourceOrTarget { get; set; }
        public List<OrganizationMigrateItemViewModel> Organizations { get; set; }
    }


}
