﻿using Point.Database.Extensions;
using Point.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Point.Database.Models.ViewModels
{
    public class PointUserInfo
    {
        public class PointUserInfoEmployee
        {
            public string FirstName { get; set; }
            public string MiddleName { get; set; }
            public string LastName { get; set; }

            public string EmailAddress { get; set; }
            public string PhoneNumber { get; set; }
            public string Position { get; set; }
            public string BIG { get; set; }
            public int? DigitalSignatureID { get; set; }
            public int EmployeeID { get; set; }
            public int DepartmentID { get; set; }
            public Guid? UserID { get; set; }
            public bool MFARequired { get; set; }
            public string MFAKey { get; set; }
            public string MFANumber { get; set; }
            public DateTime? MFAConfirmedDate { get; set; }
            public bool ChangePasswordByEmployee { get; set; }
            public bool EditAllForms { get; set; }
            public bool ReadOnlyReciever { get; set; }
            public bool ReadOnlySender { get; set; }
            public string IPAddresses { get; set; }
            public string UserName { get; set; }
            public List<FunctionRole> FunctionRoles { get; set; }
            public Role PointRole { get; set; }
            public bool IsLockedOut { get; set; }
            public string FullName()
            {
                return new NameComponents
                {
                    Initials = "",
                    FirstName = FirstName,
                    MiddleName = MiddleName,
                    LastName = LastName,
                    MaidenName = ""
                }.FullName();
            }
        }

        public PointUserInfoEmployee Employee { get; set; }
        public Department Department { get; set; }
        public Location Location { get; set; }
        public Organization Organization { get; set; }
        public Region Region { get; set; }
                
        public List<int> EmployeeOrganizationIDs { get; set; }
        public List<int> EmployeeLocationIDs { get; set; }
        public List<int> EmployeeDepartmentIDs { get; set; }

        public List<int> DepartmentTypeIDS { get; set; }

        public FunctionRoleLevelID GetReportFunctionRoleLevelID()
        {
            var functionrole = Employee.FunctionRoles
                .OrderBy(it => it.FunctionRoleTypeID == FunctionRoleTypeID.Reporting)
                .FirstOrDefault(it => it.FunctionRoleTypeID == FunctionRoleTypeID.Reporting);
            return functionrole?.FunctionRoleLevelID ?? FunctionRoleLevelID.None;
        }

        public FunctionRoleLevelID GetMaxDossierLevelAdminLevelID()
        {
            var functionrole = Employee.FunctionRoles
                .OrderBy(it => it.FunctionRoleTypeID == FunctionRoleTypeID.MaxDossierLevelAdmin)
                .FirstOrDefault(it => it.FunctionRoleTypeID == FunctionRoleTypeID.MaxDossierLevelAdmin);

            return functionrole?.FunctionRoleLevelID ?? FunctionRoleLevelID.None;
        }

        public FunctionRoleLevelID GetFunctionRoleLevelID(FunctionRoleTypeID functionroletypeid)
        {
            var functionrole = Employee.FunctionRoles
                .Where(it => it.FunctionRoleTypeID == FunctionRoleTypeID.ManageAll || it.FunctionRoleTypeID == functionroletypeid)?
                .Max(it => it.FunctionRoleLevelID) ?? FunctionRoleLevelID.None;

            return functionrole;
        }

        public bool IsGlobalAdmin
        { 
            get 
            {
                return Employee.FunctionRoles.Any(it => it.FunctionRoleTypeID == FunctionRoleTypeID.ManageAll && it.FunctionRoleLevelID == FunctionRoleLevelID.Global);
            } 
        }

        public bool IsRegioAdmin
        {
            get
            {
                return Employee.FunctionRoles.Any(it => it.FunctionRoleTypeID == FunctionRoleTypeID.ManageAll && it.FunctionRoleLevelID >= FunctionRoleLevelID.Region);
            }
        }

        private static readonly IEnumerable<FunctionRoleTypeID> managementFunctions = new List<FunctionRoleTypeID>
        {
            FunctionRoleTypeID.ManageDoctors,
            FunctionRoleTypeID.ManageOrganization,
            FunctionRoleTypeID.ManageEmployees,
            FunctionRoleTypeID.ManageFavorites,
            FunctionRoleTypeID.ManageSearch,
            FunctionRoleTypeID.ManageDocuments,
            FunctionRoleTypeID.ManageForms,
            FunctionRoleTypeID.ManageCapacity,
            FunctionRoleTypeID.ManageServicePage,
            FunctionRoleTypeID.ManageLogs,
            FunctionRoleTypeID.ManageTransferMemoTemplates,
            FunctionRoleTypeID.ManageRegion,
            FunctionRoleTypeID.ManageAll
        };

        public bool HasFunctionRole(FunctionRoleTypeID functionroletypeid)
        {
            return Employee.FunctionRoles.Any(fr => fr.FunctionRoleTypeID == functionroletypeid);
        }

        public bool HasFunctionRole(params FunctionRoleTypeID[] functionroletypeids)
        {
            return Employee.FunctionRoles.Any(fr => functionroletypeids.ToList().Contains(fr.FunctionRoleTypeID)); 
        }

        public bool HasManagementFunctions
        {
            get
            {
                return Employee.FunctionRoles.Any(fr => managementFunctions.Contains(fr.FunctionRoleTypeID));
            }
        }
        
        public bool IsEmployee => DepartmentTypeIDS.Contains((int)DepartmentTypeID.Afdeling);

        public bool IsTransferEmployee => DepartmentTypeIDS.Contains((int)DepartmentTypeID.Transferpunt);
    }
}
