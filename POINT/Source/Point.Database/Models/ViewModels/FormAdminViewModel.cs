﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Point.Database.Models.ViewModels
{
    public class FormAdminViewModel
    {
        public List<Item> Items { get; set; }

        public class Item
        {
            public string Name { get; set; }
            public int FormTypeID { get; set; }
        }
    }

    public class FormAdminFormDetailsViewModel
    {
        public string Name { get; set; }
        public int FormTypeID { get; set; }
        public List<Item> Items { get; set; }

        public class Item
        {
            public int? RegionID { get; set; }
            public int? OrganizationID { get; set; }
            public int? PhaseDefinitionID { get; set; }
            public string RegionName { get; set; }
            public string OrganizationName { get; set; }
            public string FlowName { get; set; }
            public string PhaseName { get; set; }
        }
    }

    public class AttributeEditViewModel
    {
        public int FlowFieldAttributeExceptionID { get; set; }
        public int FlowFieldAttributeID { get; set; }
        public List<Option> FlowFieldAttributeOptions { get; set; }

        public string Name { get; set; }
        public string Label { get; set; }
        public bool ReadOnly { get; set; }
        public bool Visible { get; set; }
        public bool Required { get; set; }
        public bool SignalOnChange { get; set; }
        public int? RequiredByFlowFieldID { get; set; }
        public string RequiredByFlowFieldValue { get; set; }

        public List<Option> RequiredByFlowFieldOptions { get; set; }
    }

    public class AttributeAddViewModel
    {
        public int FormTypeID { get; set; }
        public int? PhaseDefinitionID { get; set; }
        public int? RegionID { get; set; }
        public int? OrganizationID { get; set; }

        public int FlowFieldAttributeID { get; set; }
        public List<Option> FlowFieldAttributeOptions { get; set; }

        public bool ReadOnly { get; set; }
        public bool Visible { get; set; }
        public bool Required { get; set; }
        public bool SignalOnChange { get; set; }
        public int? RequiredByFlowFieldID { get; set; }
        public string RequiredByFlowFieldValue { get; set; }

        public List<Option> RequiredByFlowFieldOptions { get; set; }
    }

    public class FormAdminEditViewModel
    {
        public int FormTypeID { get; set; }
        public int? PhaseDefinitionID { get; set; }
        public int? RegionID { get; set; }
        public int? OrganizationID { get; set; }
        
        public List<Item> Items { get; set; }

        public class Item
        {
            public int FlowFieldAttributeExceptionID { get; set; }
            public int FlowFieldAttributeID { get; set; }

            public string Name { get; set; }
            public string Label { get; set; }

            public bool OriginalReadOnly { get; set; }
            public bool ReadOnly { get; set; }

            public bool OriginalVisible { get; set; }
            public bool Visible { get; set; }

            public bool OriginalRequired { get; set; }
            public bool Required { get; set; }

            public bool OriginalSignalOnChange { get; set; }
            public bool SignalOnChange { get; set; }

            public int? OriginalRequiredByFlowFieldID { get; set; }
            public int? RequiredByFlowFieldID { get; set; }

            public string OriginalRequiredByFlowFieldValue { get; set; }
            public string RequiredByFlowFieldValue { get; set; }
        }
    }

    public class AddFormDetailsViewModel
    {
        [Required]
        public int FormTypeID { get; set; }
        [Required]
        public int[] FlowFieldAttributeID { get; set; }
        public List<Option> FlowFieldAttributeOptions { get; set; }

        public int? PhaseDefinitionID { get; set; }
        public List<Option> PhaseDefinitionOptions { get; set; }
        public int? RegionID { get; set; }
        public List<Option> RegionOptions { get; set; }
        public int? OrganizationID { get; set; }
        public List<Option> OrganizationOptions { get; set; }
    }
}
