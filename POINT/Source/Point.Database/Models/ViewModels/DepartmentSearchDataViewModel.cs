﻿
using System.Collections.Generic;

namespace Point.Database.Models.ViewModels
{
    public class DepartmentSearchDataViewModel
    {
        public List<DepartmentSearchViewModelAdmin>  Departments { get; set; }

    }
}
