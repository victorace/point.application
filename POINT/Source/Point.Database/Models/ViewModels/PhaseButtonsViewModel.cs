﻿
using System.Collections.Generic;
using Point.Models.Enums;

namespace Point.Database.Models.ViewModels
{
    public class PhaseButtonsViewModel : GlobalViewModel
    {
        public PhaseButtonsViewModel()
        {
            SavePhasebuttons = new List<PhaseDefinitionNavigation>();
            NextPhaseButtons = new List<PhaseDefinitionNavigation>();
            DefinitivePhaseButtons = new List<PhaseDefinitionNavigation>();
        }

        public int CurrentPhaseInstanceID { get; set; }
        public bool IsFlowClosed { get; set; }
        public bool DisableDoubleClickProtection { get; set; }
        public Rights Rights { get; set; }
        public bool IsCurrentPhaseActive { get; set; }
        public bool IsNewFlowInstance { get; set; }
        public bool IsDossierForm { get; set; }
        public bool IsInterrupted { get; set; }
        public List<PhaseDefinitionNavigation> SavePhasebuttons { get; set; }
        public List<PhaseDefinitionNavigation> NextPhaseButtons { get; set; }
        public List<PhaseDefinitionNavigation> DefinitivePhaseButtons { get; set; }
        public bool ShowSavePhaseButtons { get; set; }
        public bool ShowNextPhaseButtons { get; set; }
        public bool ShowDefinitivePhaseButtons { get; set; }
        public bool IsCurrentPhaseDone { get; set; }
        public string LastSaved { get; set; }
        public string LastSavedBy { get; set; }
        public List<GeneralAction> ExtraActions { get; set; }
        public bool HasDigitalSignature { get; set; }
        public bool ShowPrintButton { get; set; }
        public bool IsReadOnly => IsReadMode || IsPageLocked || IsFlowClosed;
        public bool HasFlowInstance { get; set; }
        public bool HasSignedPhaseInstance { get; set; }
        public bool IsReadMode { get; set; }
        public bool IsPageLocked { get; set; }
    }
}
