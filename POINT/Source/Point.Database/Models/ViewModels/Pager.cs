﻿
namespace Point.Database.Models.ViewModels
{
    public class Pager
    {
        public int PageNo { get; set; }
        public int PageIndex => PageNo - 1;
        public int PageCount { get; set; }
        public int PageSize { get; set; } = 10;
        public int TotalCount { get; set; }
    }

}
