﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Point.Database.Models.ViewModels
{
    public class CopyTransferViewModel
    {
        public static readonly string NAME_PREFIX = "CTF_";
        public static readonly string NAME_FORMAT_REGEXP = @"(?<formsetversionid>\d+?)_(?<formtypeid>\d+?)";

        public int TransferID { get; set; }

        public List<CopyTransferFormViewModel> CopyTransferForms { get; set; }

        [DisplayName("Communicatie journaal")]
        public bool CopyTransferMemos { get; set; }

        [DisplayName("Bijlagen")]
        public bool CopyTransferAttachments { get; set; }

        [DisplayName("Kenmerken")]
        public bool CopyDossierTags { get; set; }

        public CopyTransferViewModel()
        {
            CopyTransferForms = new List<CopyTransferFormViewModel>();
        }
    }

    public class CopyTransferFormViewModel
    {
        public int FormSetVersionID { get; set; }
        public int FormTypeID { get; set; }
        public int TypeID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Timestamp { get; set; }
        public int EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public bool Selected { get; set; }
    }
}