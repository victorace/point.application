﻿namespace Point.Database.Models.ViewModels
{
    public class MenuItemViewModel
    {
        public string Title { get; set; }
        public string URL { get; set; }
        public string Target { get; set; }

        public MenuItemViewModel(string title, string url, string target)
        {
            Title = title;
            URL = url;
            Target = target;
        }
    }
}
