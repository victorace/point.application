﻿using System.Collections.Generic;
using System.Linq;
using Point.Database.Models.Constants;

namespace Point.Database.Models.ViewModels
{
    public class MenuViewModel
    {
        public IEnumerable<LocalMenuItem> MenuItems { get; set; } = new List<LocalMenuItem>();
        public IEnumerable<LocalMenuItem> MainForms
        {
            get { return MenuItems.Where(mi => mi.MenuType == MenuTypes.Main); }
        }

        public IEnumerable<LocalMenuItem> ExtReferenceItems
        {
            get { return MenuItems.Where(mi => mi.MenuType == MenuTypes.ExtReference); }
        }

        public IEnumerable<LocalMenuItem> FilterForms
        {
            get { return MenuItems.Where(mi => mi.MenuType == MenuTypes.Filter); }
        }
        public IEnumerable<LocalMenuItem> SearchForms
        {
            get { return MenuItems.Where(mi => mi.MenuType == MenuTypes.Zoeken); }
        }
        public IEnumerable<LocalMenuItem> ProcessForms
        {
            get { return MenuItems.Where(mi => mi.MenuType == MenuTypes.Proces); }
        }
        public IEnumerable<LocalMenuItem> AanvullendForms
        {
            get { return MenuItems.Where(mi => mi.MenuType == MenuTypes.Aanvullend); }
        }

        public IEnumerable<LocalMenuItem> Actions
        {
            get { return MenuItems.Where(mi => mi.MenuType == MenuTypes.Actie); }
        }

        public bool IsSearch { get; set; }
        public bool IsError { get; set; }
    }
}
