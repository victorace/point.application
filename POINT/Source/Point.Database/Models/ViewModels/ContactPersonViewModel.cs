﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Point.Database.Attributes;
using Point.Models.LDM.EOverdracht30.Enums;

namespace Point.Database.Models.ViewModels
{
    public class ContactPersonViewModel
    {
        [Required]
        public int ContactPersonID { get; set; }

        [Required]
        public int ClientID { get; set; }

        public int PersonDataID { get; set; }

        [DisplayName("Geslacht")]
        public Geslacht? Gender { get; set; }

        [StringLength(50)]
        [DisplayName("Voorletters")]
        public string Initials { get; set; }

        [StringLength(255)]
        [DisplayName("Voornaam")]
        public string FirstName { get; set; }

        [StringLength(255)]
        [DisplayName("Achternaam (volledig)")]
        [Required(ErrorMessage = "Contactpersoon achternaam is verplicht")]
        public string LastName { get; set; }

        [StringLength(50)]
        [DisplayName("Telefoonnummer 1")]
        [Required(ErrorMessage = "Contactpersoon Telefoonnummer 1 is verplicht")]
        public string PhoneNumberGeneral { get; set; }

        [StringLength(50)]
        [DisplayName("Telefoonnummer 2")]
        public string PhoneNumberMobile { get; set; }

        [StringLength(50)]
        [DisplayName("Telefoonnummer 3")]
        public string PhoneNumberWork { get; set; }

        [StringLength(255)]
        [DisplayName("E-mail")]
        [EmailAddressValidator]
        public string Email { get; set; }

        [DisplayName("Relatie tot de patiënt")]
        public int? CIZRelationID { get; set; }

        [DisplayName("Relatie tot de patiënt")]
        public string CIZDescription { get; set; }
    }
}
