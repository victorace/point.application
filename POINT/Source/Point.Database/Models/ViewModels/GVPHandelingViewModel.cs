﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace Point.Database.Models.ViewModels
{
    public class GVPHandelingViewModel
    {
        public static readonly string NAME_PREFIX = "GVPTI_";
        public static readonly string NAME_FORMAT_REGEXP = @"\[(?<id>\d+?)\]_(?<complex>\w*)_(?<fieldName>\w*)";

        public enum ComplexType
        {
            H = 0,
            L = 1,
            O = 2
        }


        public int Id { get; set; }
        public string GroupName { get; set; }
        public int GroupID { get; set; }

        [Description("Frequentie")]
        public string Frequency { get; set; }

        [Description("Handeling")]
        public string Action {get; set;}

        public bool IsCustomAction { get; set; }

        [Description("Definitie")]
        public string Definition { get; set; }
        
        [Description("Complex type")]
        public ComplexType Complex { get; set; }
        public bool HasHistory { get; set; }
        public int? FormsetVersionID { get; set; }
        public string FrequencyName => $"{NAME_PREFIX}{GroupID}_[{Id}]_{Complex}_frequency";
        public string DefinitionName => $"{NAME_PREFIX}{GroupID}_[{Id}]_{Complex}_definition";
        public bool IsUitvoeringsVerzoekItem => GVPViewModel.UitvoeringsVerzoekGroupIds.Contains(GroupID);
        public bool isRisicovolItem => GVPViewModel.RisicovolleGroupIds.Contains(GroupID);

        public static List<GVPHandelingViewModel> GetByModelList(IEnumerable<ActionCodeHealthInsurer> actions, List<ActionHealthInsurer> values, List<MutActionHealthInsurer> history = null )
        {
            var list = new List<GVPHandelingViewModel>();
            foreach (var action in actions)
            {
                var treatmentitem = new GVPHandelingViewModel
                {
                    Id = action.ActionCodeHealthInsurerID,
                    Action = action.ActionCodeName,
                    IsCustomAction = string.IsNullOrEmpty(action.ActionCodeName),
                    GroupName = action.ActionGroupHealthInsurer.ActionGroupName,
                    GroupID = action.ActionGroupHealthInsurerID.GetValueOrDefault(0)
                };

                if (action.Complex == "H")
                {
                    treatmentitem.Complex = ComplexType.H;
                }

                if (action.Complex == "L")
                {
                    treatmentitem.Complex = ComplexType.L;
                }

                if (action.Complex == "O")
                {
                    treatmentitem.Complex = ComplexType.O;
                }

                var actionValue = values?.FirstOrDefault(val => val.ActionCodeHealthInsurerID == action.ActionCodeHealthInsurerID);
                if (actionValue != null)
                {
                    treatmentitem.Frequency = actionValue.CustomFrequency;
                    treatmentitem.Definition = actionValue.Definition;

                    if (history != null)
                    {
                        treatmentitem.HasHistory = history.Any(h => h.ActionCodeHealthInsurerID == treatmentitem.Id && h.Amount != null);
                        if (treatmentitem.HasHistory)
                        {
                            treatmentitem.FormsetVersionID = actionValue.FormSetVersionID;
                        }
                    }
                }
                list.Add(treatmentitem);
            }

            return list;
        }
    }

    
}
