﻿

namespace Point.Database.Models.ViewModels
{
    public class RequestFormZHZHViewModel : FormTypeBaseViewModel
    {
        public string DepartmentName { get; set; }
        public string LocationName { get; set; }
    }
}
