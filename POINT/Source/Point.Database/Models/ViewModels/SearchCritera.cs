﻿using Point.Models.Enums;

namespace Point.Database.Models.ViewModels
{
    public class SearchCritera
    {
        public enum MatchType
        {
            equal = 0,
            lesser = 1,
            greater = 2
        }

        public string SearchFor { get; set; }
        public MatchType SearchMatchType { get; set; }
        public string SearchIn { get; set; }
        public SearchPropertyType SearchPropertyType { get; set; }
    }
}