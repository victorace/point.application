﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Point.Database.Models.ViewModels
{
    public class LastGrzFormViewModel
    {
        public string DateTimeString { get; set; }
        public string FormTypeName { get; set; }
        public string Description { get; set; }
    }
}
