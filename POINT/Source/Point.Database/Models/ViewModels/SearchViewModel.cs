using System.Collections.Generic;
using System.Linq;
using Point.Models.Enums;

namespace Point.Database.Models.ViewModels
{
    public class SearchViewModel
    {
        public IEnumerable<FlowInstanceSearchViewModel> ItemsTable { get; set; }
        public bool UsePaging { get; set; }
        public int ResultCount { get; set; }
        public int PageCount { get; set; }
        public int CurrentPage { get; set; }

        public string SortColumn { get; set; }
        public string SortOrder { get; set; }
        public bool HavePatExternalReference { get; set; } = false;
        public SearchType CurrentSearchType { get; set; }
        public bool IsClickable { get; set; }
        public IOrderedEnumerable<SearchUIFieldConfiguration> Fields { get; set; }
        public int StartPage { get; set; }
        public int EndPage { get; set; }

    }
}
