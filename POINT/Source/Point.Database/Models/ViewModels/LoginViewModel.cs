﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Point.Database.Models.ViewModels
{
    public class LoginViewModel
    {
        [DisplayName("Gebruikersnaam")]
        [Required(AllowEmptyStrings = false, ErrorMessage ="{0} is verplicht")]
        public string Username { get; set; }
        [DisplayName("Wachtwoord")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "{0} is verplicht")]
        public string Password { get; set; }
        public string Resolution { get; set; }
        public string DeblockMessage { get; set; }
        public bool SendDeblockEmail { get; set; } = false;
        public bool? SSOSucceeded { get; set; }
        public string ReturnUrl { get; set; }
    }
}