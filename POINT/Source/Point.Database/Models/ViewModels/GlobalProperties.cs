﻿
using System;
using System.Collections.Generic;

namespace Point.Database.Models.ViewModels
{
    public class GlobalProperties
    {
        public GlobalProperties()
        {
            FlowWebFields = new List<FlowWebField>();
        }

        public IDictionary<string, object> ViewData { get; set; }

        public string ActionName { get; set; }
        public bool AsPartial { get; set; }
        public string ControllerName { get; set; }
        public FlowInstance FlowInstance { get; set; }
        public List<FlowWebField> FlowWebFields { get; set; }
        public string FormRequestUrl { get; set; }
        public int FormSetVersionID { get; set; }
        public int FormTypeID { get; set; }
        public int GeneralActionID { get; set; }
        public bool HasFormSetVersion => FormSetVersionID > 0;
        public DateTime? HistoryFromDate { get; set; }
        public bool IsClosed { get; set; }
        public bool IsPageLock { get; set; }
        public bool IsPrintRequest { get; set; }
        public bool IsReadMode { get; set; }
        public string NewFormRequestUrl { get; set; }
        public int PhaseDefinitionID { get; set; }
        public int PhaseInstanceID { get; set; }
        public PointUserInfo PointUserInfo { get; set; }
        public int TransferID { get; set; }
        public PhaseInstance PhaseInstance { get; set; }
        public Transfer Transfer { get; set; }
    }
}
