﻿using System.Collections.Generic;

namespace Point.Database.Models.ViewModels
{
    public class HistoryViewModel
    {
        public HistoryViewModel()
        {
            FormHistoryForms = new List<FormHistoryForm>();
        }
     //   public IList<CommunicationJournalViewModel> CommunicationJournals { get; set; }
        public IList<FormHistoryForm> FormHistoryForms { get; set; }

    }
}
