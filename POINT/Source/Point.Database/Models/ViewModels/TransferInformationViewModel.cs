﻿using System.ComponentModel;

namespace Point.Database.Models.ViewModels
{
    public class TransferInformationViewModel
    {
        [DisplayName("Naam instelling")]
        public string SourceOrganizationName { get; set; }
        [DisplayName("Soort organisatie")]
        public string SourceOrganizationType { get; set; }
        [DisplayName("Locatie")]
        public string SourceLocationName { get; set; }
        [DisplayName("Afdeling")]
        public string SourceDepartmentName { get; set; }
        [DisplayName("E-mail adres")]
        public string SourceDepartmentEmail { get; set; }
        [DisplayName("Telefoonnummer")]
        public string SourceDepartmentTelephonenumber { get; set; }
        [DisplayName("Naam")]
        public string SourceDepartmentFullName { get; set; }

        [DisplayName("Naam instelling")]
        public string DestinationOrganizationName { get; set; }
        [DisplayName("Soort organisatie")]
        public string DestinationOrganizationType { get; set; }
        [DisplayName("Locatie")]
        public string DestinationLocationName { get; set; }
        [DisplayName("Afdeling")]
        public string DestinationDepartmentName { get; set; }

        [DisplayName("E-mail adres")]
        public string DestinationDepartmentEmail { get; set; }
        [DisplayName("Telefoonnummer")]
        public string DestinationDepartmentTelephonenumber { get; set; }
        [DisplayName("Naam")]
        public string DestinationDepartmentFullName { get; set; }
    }
}
