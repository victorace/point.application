﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Point.Database.Models.ViewModels
{
    public class LastTransferAttachmentViewModel
    {
        public string DateTimeString { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int TransferID { get; set; }
    }
}
