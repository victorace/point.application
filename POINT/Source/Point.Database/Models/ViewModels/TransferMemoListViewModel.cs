﻿using System.Collections.Generic;
using Point.Models.Enums;

namespace Point.Database.Models.ViewModels
{
    public class TransferMemoListViewModel
    {
        public TransferMemoListViewModel()
        {
            Templates = new List<TransferMemoTemplate>();
        }

        public IEnumerable<TransferMemoViewModel> Items { get; set; }
        public IEnumerable<TransferMemoTemplate> Templates { get; set; }
        public bool IsCommunicationJournal { get; set; }
        public TransferMemoTypeID? Target { get; set; }
        public int TransferID { get; set; }
        public int PhaseDefinitionID { get; set; }
        public int PhaseInstanceID { get; set; }
        public int FormSetVersionID { get; set; }
        public int? FrequencyID { get; set; }
        public bool ForcePartial { get; set; }
        public bool WithoutHeader { get; set; }
        public bool ReadOnly { get; set; }
        public string Action { get; set; }
        public bool ShowTakeOverMessage { get; set; } = false;
        public bool EnableSignalering { get; set; } = true;
    }
}
