﻿using Point.Models.Interfaces;

using Point.Models.Enums;
using System.Collections.Generic;

namespace Point.Database.Models.ViewModels
{
    public class FlowWebField : IFlowFieldValue
    {
        public static readonly string DEFAULT_DATETIME_FORMAT = "dd-MM-yyyy HH:mm";
        public static readonly string DEFAULT_DATE_FORMAT = "dd-MM-yyyy";
        public static readonly string NAME_PREFIX = "f_";
        public static readonly string NAME_FORMAT = "[{0}]_[{1}]_{2}_{3}_{4}";
        public static readonly string NAME_FORMAT_REGEXP = @"\[(?<id>\d+?)\]_\[(?<attributeid>\d+?)\]_(?<fieldName>\w*)_((?<type>\d*))_(?<signalonchange>\w*)";

        public int FlowFieldID { get; set; }
        public int FlowFieldAttributeID { get; set; }
        public string Name { get; set; }
        public string RequiredBy { get; set; }
        public string RequiredByValue { get; set; }
        public string RequiredByLabel { get; set; }
        public string Label { get; set; }
        public string Description { get; set; }
        public FlowFieldType Type { get; set; }
        public bool Required { get; set; }
        public bool Visible { get; set; }
        public bool ReadOnly { get; set; }
        public bool SignalOnChange { get; set; }
        public bool HasHistory { get; set; }
        public int? FormSetVersionID { get; set; }
        public string Format { get; set; }
        public string Regexp { get; set; }
        public FlowFieldDataSourceViewModel FlowFieldDataSource { get; set; }
        public int? MaxLength { get; set; }
        public string Value { get; set; }
        public string DisplayValue { get; set; }
        public DbDrivenValidation DbDrivenValidation { get; set; }
        public Source Source { get; set; }

        public override string ToString()
        {
            return $"{FlowFieldID} {FlowFieldAttributeID} {Label}: {Value}";
        }
    }
}
