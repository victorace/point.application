﻿
namespace Point.Database.Models.ViewModels
{
    public class SendOverdrachtViewModel : GlobalViewModel
    {
        public bool VOIsDone { get; set; }
        public bool HasAccepted { get; set; }
        public bool DisplayHasAccepted { get; set; }
        public string VersturenVoRealizedEmployee { get; set; } = string.Empty;
        public string VersturenVoRealizedDate { get; set; } = null;
        public int? VOPhaseDefinitionID { get; set; } = -1;
    }
}
