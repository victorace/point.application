﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Point.Models.Enums;

namespace Point.Database.Models.ViewModels
{
    public class FrequencyViewModel : FormTypeBaseViewModel
    {
        public int TransferID { get; set; }

        public int FrequencyID { get; set; }

        [DisplayName("Naam")]
        [Required(ErrorMessage = "Naam is verplicht.")]
        [StringLength(50)]
        public string Name { get; set; }

        [Required(ErrorMessage = "Frequentie is verplicht.")]
        [Range(1, int.MaxValue, ErrorMessage = "Frequentie moet een positief getal zijn")]
        [DisplayName("Frequentie")]
        public int Quantity { get; set; }

        [Required(ErrorMessage = "Begin datum is verplicht.")]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}")]
        [DataType(DataType.Date)]
        [DisplayName("Begin datum")]
        public DateTime? StartDate { get; set; }

        [Required(ErrorMessage = "Eind datum is verplicht.")]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}")]
        [DataType(DataType.Date)]
        [DisplayName("Eind datum")]
        public DateTime? EndDate { get; set; }

        public FrequencyType Type { get; set; }

        public FrequencyStatus Status { get; set; }

        [DisplayName("Ontvanger wordt genotificeerd per")]
        public FrequencyFeedbackType FeedbackType { get; set; }


        public string IndicatiestellingFormName { get; set; }
        public string IndicatiestellingURL { get; set; }
        public bool ShowReopenButton => (Status == FrequencyStatus.Done);
    }
}
