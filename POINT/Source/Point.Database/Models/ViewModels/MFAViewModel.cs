﻿using Point.Models.Enums;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Point.Database.Models.ViewModels
{
    public class MFAViewModel
    {
        public int EmployeeID { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "{0} is verplicht")]
        [MinLength(10), MaxLength(10)]
        [DisplayName("Uw mobiele nummer")]
        public string MFANumber { get; set; }

        public string MFAKey { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "{0} is verplicht")]
        [DisplayName("Bevestingscode")]
        public string MFACode { get; set; }

        public MFACodeType MFACodeType { get; set; }

        public DateTime? MFAConfirmedDate { get; set; }

        public string ImageURL { get; set; }
        public string SetupCode { get; set; }
    }

}