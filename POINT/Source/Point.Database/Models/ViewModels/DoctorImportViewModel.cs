﻿using System.Collections.Generic;
using System.Linq;

namespace Point.Database.Models.ViewModels
{
    public class DoctorImportViewModel : DoctorViewModel
    {
        public const string FlagEmpty = "X";
        public DoctorImportViewModel()
        {
            AGBItem = new DoctorImportItemViewModel();
            NameItem = new DoctorImportItemViewModel();
            SearchNameItem = new DoctorImportItemViewModel();
            SpecialismItem = new DoctorImportItemViewModel();
            Locations = new List<DoctorImportItemViewModel>();
        }

        public bool CanImport => !IsDuplicate && AGBItem.Value != FlagEmpty && NameItem.Value != FlagEmpty 
                                 && SearchNameItem.Value != FlagEmpty && SpecialismID.HasValue;
        public bool IsDuplicate { get; set; }
        public DoctorImportItemViewModel AGBItem { get; set; }
        public DoctorImportItemViewModel NameItem { get; set; }
        public DoctorImportItemViewModel SearchNameItem { get; set; }
        public DoctorImportItemViewModel SpecialismItem { get; set; }
        public List<DoctorImportItemViewModel> Locations { get; set; }
    }

    public class DoctorImportItemViewModel
    {
        public int? ID { get; set; }
        public string ErrorMessage { get; set; }
        public bool HasError => !string.IsNullOrEmpty(ErrorMessage);
        public string Value { get; set; }
    }

    public class DoctorImportListViewModel
    {
        public DoctorImportListViewModel()
        {
            ColumnIdentifiers = new List<CsvColumnIdentifier>();
            Doctors = new List<DoctorImportViewModel>();
            Errors = new Dictionary<string, string>();
        }

        public List<CsvColumnIdentifier> ColumnIdentifiers { get; set; }
        public List<DoctorImportViewModel> Doctors { get; set; }
        public string ErrorMessage { get; set; }
        public Dictionary<string, string> Errors { get; set; }
        public bool HasError => Errors.Any();

        public void AddColumnIdentifier(string columnName, bool isRequired = false)
        {
            ColumnIdentifiers.Add(new CsvColumnIdentifier {Name = columnName, IsRequired = isRequired});
        }
    }

    public class DoctorImportDbItemViewModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string NameUpper => Name.ToUpperInvariant();
    }

    public static class DoctorImportColumnIdentifiers
    {
        public const string AGB_LABEL = "AGB";
        public static string AGB_ID => AGB_LABEL.ToUpperInvariant();
        public const string BIG_LABEL = "BIG";
        public static string BIG_ID => BIG_LABEL.ToUpperInvariant();
        public const string Locations_LABEL = "Locaties";
        public const string Locations_LABEL1 = "Locatie";
        public static string Locations_ID => Locations_LABEL.ToUpperInvariant();
        public const string Name_LABEL = "Naam";
        public static string Name_ID => Name_LABEL.ToUpperInvariant();
        public const string PhoneNumber_LABEL = "Telefoonnummer";
        public static string PhoneNumber_ID => PhoneNumber_LABEL.ToUpperInvariant();
        public const string SearchName_LABEL = "Zoeknaam";
        public static string SearchName_ID => SearchName_LABEL.ToUpperInvariant();
        public const string Specialism_LABEL = "Specialisme";
        public static string Specialism_ID => Specialism_LABEL.ToUpperInvariant();
    }

}
