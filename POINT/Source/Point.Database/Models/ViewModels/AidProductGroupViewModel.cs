﻿
namespace Point.Database.Models.ViewModels
{
    public class AidProductGroupViewModel
    {
        private int _TypeIndex = 0;     
        public int AidProductGroupID { get; set; }
        public string Name { get; set; }
        // GroupType =  "Algemeen", "Huurartikelen" (at the moment)
        public bool IsGroupType => AidProductGroupID == (int)Point.Models.Enums.AidProductGroupID.VegroPoint_Algemeen || AidProductGroupID == (int)Point.Models.Enums.AidProductGroupID.Vegro_Huurartikelen;
    }

}
