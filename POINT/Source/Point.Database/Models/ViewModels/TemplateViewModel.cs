﻿using Point.Models.Enums;
using System.Collections.Generic;

namespace Point.Database.Models.ViewModels
{
    public class TemplateViewModel
    {
        public IEnumerable<TemplateType> TemplateTypes { get; set; }
        public TemplateTypeID TemplateTypeID { get; set; }
        public int TemplateID { get; set; }
        public int TemplateDefaultID { get; set; }
        public int? OrganizationTypeID { get; set; }
        public IEnumerable<Organization> Organizations { get; set; }
        public int? OrganizationID { get; set; }
        public bool IsDefault { get; set; }
        public string TemplateText { get; set; }
        public bool CanBeOrganizationSpecific { get; set; }
        //public bool Inactive { get; set; }
    }
}