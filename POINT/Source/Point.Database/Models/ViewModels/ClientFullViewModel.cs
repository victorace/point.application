﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Point.Database.Attributes;
using Point.Models.LDM.EOverdracht30.Enums;

namespace Point.Database.Models.ViewModels
{
    public class ClientFullViewModel : ClientViewModelBase
    {
        [StringLength(9)]
        [DisplayName("Burger Service Nummer")]
        //[Required(ErrorMessage = BaseEntity.errorRequired)] 
        public string CivilServiceNumber { get; set; }
        [DisplayName("Heeft (nog) geen BSN")]
        public bool HasNoCivilServiceNumber { get; set; }

        [DisplayName("Patiëntnummer ZH")]
        [StringLength(50, ErrorMessage = BaseEntity.errorMaxLength)]
        public string PatientNumber { get; set; }

        [DisplayName("Opnamenr.")]
        [StringLength(50, ErrorMessage = BaseEntity.errorMaxLength)]
        public string VisitNumber { get; set; }

        [DisplayName("Geslacht")]
        [Required(ErrorMessage = BaseEntity.errorRequired)]
        [RegularExpression("OM|OF|OUN", ErrorMessage = "Geslacht ('Man', 'Vrouw' of 'Ongedifferentieerd') is verplicht")]
        public Geslacht Gender { get; set; }

        [StringLength(255)]
        [DisplayName("Aanhef")]
        public string Salutation { get; set; }

        [StringLength(50)]
        [DisplayName("Voorletters")]
        [Required(ErrorMessage = BaseEntity.errorRequired)]
        public string Initials { get; set; }

        [StringLength(255)]
        [DisplayName("Voornaam")]
        public string FirstName { get; set; }

        [StringLength(50)]
        [DisplayName("Tussenvoegsel")]
        public string MiddleName { get; set; }

        [StringLength(255)]
        [DisplayName("Achternaam (volledig)")]
        [Required(ErrorMessage = BaseEntity.errorRequired)]
        public string LastName { get; set; }

        [StringLength(255)]
        [DisplayName("Geboortenaam")]
        public string MaidenName { get; set; }

        [DisplayName("Geboortedatum")]
        [DataType(DataType.Date, ErrorMessage = BaseEntity.errorDate), DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}")]
        [Required(ErrorMessage = BaseEntity.errorRequired)]
        public DateTime? BirthDate { get; set; }

        [DisplayName("Zorgverzekeraar")]
        [Required(ErrorMessage = BaseEntity.errorRequired)]
        public int? HealthInsuranceCompanyID { get; set; }

        [StringLength(50)]
        [DisplayName("Polisnummer")]
        [Required(ErrorMessage = BaseEntity.errorRequired)]
        public string InsuranceNumber { get; set; }

        [StringLength(255)]
        [DisplayName("Straat")]
        [Required(ErrorMessage = BaseEntity.errorRequired)]
        public string StreetName { get; set; }

        [StringLength(50)]
        [DisplayName("Huisnummer")]
        [Required(ErrorMessage = BaseEntity.errorRequired)]
        public string Number { get; set; }

        [StringLength(50)]
        [DisplayName("Huisnummer toevoeging")]
        public string HuisnummerToevoeging { get; set; }

        [StringLength(10)]
        [DisplayName("Postcode")]
        [Required(ErrorMessage = BaseEntity.errorRequired)]
        public string PostalCode { get; set; }

        [StringLength(50)]
        [DisplayName("Postbus")]
        public string Postbus { get; set; }

        [StringLength(255)]
        [DisplayName("Plaats")]
        [Required(ErrorMessage = BaseEntity.errorRequired)]
        public string City { get; set; }

        [StringLength(255)]
        [DisplayName("Land")]
        public string Country { get; set; }

        [StringLength(50)]
        [Required(ErrorMessage = "Patient Telefoonnummer 1 is verplicht")]
        [DisplayName("Telefoonnummer 1")]
        public string PhoneNumberGeneral { get; set; }

        [StringLength(50)]
        [DisplayName("Telefoonnummer 2")]
        public string PhoneNumberMobile { get; set; }

        [StringLength(50)]
        [DisplayName("Telefoonnummer 3")]
        public string PhoneNumberWork { get; set; }

        [StringLength(255)]
        [DisplayName("E-mail")]
        [EmailAddressValidator]
        public string Email { get; set; }

        [StringLength(255)]
        [DisplayName("Huisarts")]
        [Required(ErrorMessage = BaseEntity.errorRequired)]
        public string GeneralPractitionerName { get; set; }

        [StringLength(50)]
        [DisplayName("Telefoonnummer huisarts")]
        public string GeneralPractitionerPhoneNumber { get; set; }

        [StringLength(255)]
        [DisplayName("Apotheek")]
        public string PharmacyName { get; set; }

        [StringLength(50)]
        [DisplayName("Telefoonnummer apotheek")]
        public string PharmacyPhoneNumber { get; set; }

        [DisplayName("Burgerlijke staat")]
        public BurgerlijkeStaat? CivilClass { get; set; }

        [DisplayName("Samenstelling huishouding")]
        public Gezinssamenstelling? CompositionHousekeeping { get; set; }

        [DisplayName("Aantal kinderen in het gezin")]
        public int? ChildrenInHousekeeping { get; set; }

        [DisplayName("Woonsituatie")]
        public WoningType? HousingType { get; set; }

        [DisplayName("Toelichting")]
        [StringLength(255)]
        public string HousingTypeComment { get; set; }

        [StringLength(255)]
        [DisplayName("Verzorgings- / verpleeginstelling")]
        public string HealthCareProvider { get; set; }

        public int? ClientCreatedBy { get; set; }

        public DateTime? ClientCreatedDate { get; set; }

        public int? ClientModifiedBy { get; set; }

        public DateTime? ClientModifiedDate { get; set; }

        [DisplayName("Nationaliteit")]
        public int? NationalityID { get; set; }

        [DisplayName("Taal")]
        public CommunicatieTaal? LanguageID { get; set; }

        public DateTime? TimeStamp { get; set; }

        public int? EmployeeID { get; set; }

        public int? ScreenID { get; set; }

        [StringLength(255)]
        [DisplayName("Zorgmail adres huisarts")]
        public string AddressGPForZorgmail { get; set; }

        [StringLength(255)]
        [DisplayName("Straat")]
        //[Required(ErrorMessage = BaseEntity.errorRequired)]
        public string TemporaryStreetName { get; set; }

        [StringLength(50)]
        [DisplayName("Huisnummer")]
        //[Required(ErrorMessage = BaseEntity.errorRequired)]
        public string TemporaryNumber { get; set; }

        [StringLength(10)]
        [DisplayName("Postcode")]
        //[Required(ErrorMessage = BaseEntity.errorRequired)]
        public string TemporaryPostalCode { get; set; }

        [StringLength(255)]
        [DisplayName("Plaats")]
        //[Required(ErrorMessage = BaseEntity.errorRequired)]
        public string TemporaryCity { get; set; }

        [StringLength(255)]
        [DisplayName("Land")]
        public string TemporaryCountry { get; set; }

        [StringLength(255)]
        [DisplayName("Partnernaam")]
        public string PartnerName { get; set; }

        [StringLength(50)]
        [DisplayName("Tussenvoegsel")]
        public string PartnerMiddleName { get; set; }

        [DisplayName("Cliënt is anoniem")]
        public bool ClientIsAnonymous { get; set; }
        public bool ClientShowAnonymous { get; set; }

 //       public bool ShowSearchBSN { get; set; }
 //       public bool ShowSearchComplete { get; set; }
        public ContactPersonViewModel ContactPerson { get; set; }
        public bool PharmacyRequired { get; set; }
        public bool BSNRequired { get; set; }
        public bool ShowNedapRetrieve { get; set; }
        public bool HideHospitalFields { get; set; }
        public bool ShowContactPersonUnknown { get; set; }
        public bool ForceContactPersonCreation { get; set; }
        public int? DepartmentID { get; set; }
        public string HealthInsurerUZOVI { get; set; } = string.Empty;
        public string HealthInsurerName { get; set; } = string.Empty;
        public bool HasHealthInsurerName => !string.IsNullOrEmpty(HealthInsurerName);
    }
}
