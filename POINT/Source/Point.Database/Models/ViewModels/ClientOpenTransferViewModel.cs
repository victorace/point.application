﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Point.Database.Models.ViewModels
{
    public class ClientOpenTransferViewModel
    {
        public int TransferID { get; set; }
        public string ClientName { get; set; }
        public string FlowType { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }

        public static ClientOpenTransferViewModel ToViewModel(FlowInstanceSearchValues searchModel)
        {
            return new ClientOpenTransferViewModel
            {
                TransferID = searchModel.TransferID,
                ClientName = searchModel.ClientFullname,
                FlowType = searchModel.FlowDefinitionName,
                CreateDate = searchModel.TransferCreatedDate?? DateTime.MinValue,
                CreateBy = searchModel.TransferCreatedBy
            };
        }
    }
}
