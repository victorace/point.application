﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Point.Database.Models.ViewModels
{ 
    //TO DO: is this view model still needed?
    public class CommunicationJournalViewModel
    {
        public int TransferMemoID { get; set; }

        public int? EmployeeID { get; set; }

        public string EmployeeName { get; set; }

        public DateTime? MemoDateTime { get; set; }

        public string MemoContent { get; set; }

        public int? TransferID { get; set; }  
    }
}
