﻿using Point.Models.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Point.Database.Models.ViewModels
{
    public class TransferTaskListViewModel
    {
        public TransferTaskListViewModel()
        {
            TransferTasks = new List<TransferTaskViewModel>();
        }

        public int TransferID { get; set; }
        public bool IsReadOnly { get; set; }
        public bool IsPartial { get; set; }
        public List<TransferTaskViewModel> TransferTasks { get; set; }
    }

    public class TransferTaskViewModel
    {
        public int TransferTaskID { get; set; }
        public int TransferID { get; set; }
        public int FormTypeID { get; set; }

        [Required, DisplayName("Omschrijving")]
        public string Description { get; set; }

        [DisplayName("Opmerkingen")]
        public string Comments { get; set; }

        [DisplayName("Datum/tijdstip aangemaakt")]
        public DateTime CreatedDate { get; set; }

        [DisplayName("Aangemaakt door")]
        public string CreatedByEmployee { get; set; }

        [Required, DisplayName("Datum/tijdstip uitvoering"), DisplayFormat(DataFormatString = "{0:dd-MM-yyyy HH:mm}")]
        public DateTime? DueDate { get; set; }

        [DisplayName("Datum/tijdstip afgehandeld")]
        public DateTime? CompletedDate { get; set; }

        [DisplayName("Afgehandeld door")]
        public string CompletedByEmployee { get; set; }

        [DisplayName("Status")]
        public Status Status { get; set; } = Status.Active;

        [DisplayName("Status")]
        public String StatusDescription { get; set; }

        [DisplayName("Afgehandeld")]
        public bool Completed { get; set; }

        public bool IsReadOnly { get; set; }
    }
}