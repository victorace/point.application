﻿using Point.Database.Attributes;
using Point.Models.Enums;
using Point.Models.LDM.EOverdracht30.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Point.Database.Models.ViewModels
{
    public class EmployeeViewModel
    {
        public EmployeeViewModel()
        {
            OtherDepartments = new List<Department>();
            OtherDepartmentID = new int[0];
            FunctionRoleTypeIDs = new FunctionRoleTypeID[0];
        }

        public Department DefaultDepartment { get; set; }

        public int EmployeeID { get; set; }

        [DisplayName("Inactief")]
        public bool Inactive { get; set; }

        [Required(ErrorMessage = BaseEntity.errorRequired)]
        [DisplayName("Organisatie")]
        public int OrganizationID { get; set; }

        [Required(ErrorMessage = BaseEntity.errorRequired)]
        [DisplayName("Locatie")]
        public int LocationID { get; set; }

        [Required(ErrorMessage = BaseEntity.errorRequired)]
        [DisplayName("Primaire afdeling")]
        public int DepartmentID { get; set; }

        [DisplayName("Overige afdelingen")]
        public int[] OtherDepartmentID { get; set; }
        public List<Department> OtherDepartments { get; set; }

        [DisplayName("Extra afdelingen")]
        public string OtherDepartmentsToShow { get; set; }

        [DisplayName("Voornaam")]
        public string FirstName { get; set; }

        [DisplayName("Tussenvoegsel")]
        public string MiddleName { get; set; }

        [Required(ErrorMessage = BaseEntity.errorRequired)]
        [DisplayName("Achternaam")]
        public string LastName { get; set; }

        [Required(ErrorMessage = BaseEntity.errorRequired)]
        [DisplayName("Geslacht")]
        public Geslacht? Gender { get; set; }

        [DisplayName("Functie")]
        public string Function { get; set; }

        [Required(ErrorMessage = BaseEntity.errorRequired)]
        [DisplayName("Telefoonnummer / Piepernummer")]
        public string PhoneNumber { get; set; }

        [DisplayName("Emailadres")]
        [EmailAddressValidator]
        public string EmailAddress { get; set; }

        [DisplayName("ZIS ID")]
        public string ZISID { get; set; }

        [DisplayName("BIG nummer")]
        public string BIGNR { get; set; }

        [DisplayName("Moet wachtwoord wijzigen bij volgende login")]
        public bool ChangePasswordOnNextLogin { get; set; }

        [Required(ErrorMessage = BaseEntity.errorRequired)]
        [DisplayName("Dossier toegangsniveau")]
        public FunctionRoleLevelID DossierLevelID { get; set; }

        [DisplayName("Te beheren dossier toegangsniveau")]
        public FunctionRoleLevelID MaxDossierLevelAdminLevelID { get; set; }

        [DisplayName("Functies")]
        public FunctionRoleTypeID[] FunctionRoleTypeIDs { get; set; }

        [DisplayName("Beheerfuncties en beheer niveau")]
        public FunctionRoleLevelID FunctionRoleLevelID { get; set; }

        [DisplayName("Rapportage")]
        public bool ReportingFunction { get; set; }

        [DisplayName("Rapportage niveau")]
        public FunctionRoleLevelID ReportingLevelID { get; set; }

        [DisplayName("Aanmaakdatum")]
        public DateTime? CreateDate { get; set; }
        [DisplayName("Laatste inlogdatum")]
        public DateTime? LastLoginDate { get; set; }

        [DisplayName("Account blokkeren na")]
        public DateTime? AutoLockAccountDate { get; set; }

        [DisplayName("Gebruikersnaam")]
        [Required(ErrorMessage = BaseEntity.errorRequired)]
        public string UserName { get; set; }

        [DisplayName("Wachtwoord")]
        [Required(ErrorMessage = BaseEntity.errorRequired)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DisplayName("Herhaal wachtwoord")]
        [DataType(DataType.Password)]
        [Required(ErrorMessage = BaseEntity.errorRequired)]
        [Compare("Password", ErrorMessage = "Wachtwoorden zijn niet hetzelfde")]
        public string Password2 { get; set; }

        [DisplayName("Medewerker is geblokkeerd")]
        public bool EmployeeIsLocked { get; set; }

        [DisplayName("Transferpunt rechten")]
        [WriteAccess(FunctionRoleLevelID.Location, FunctionRoleTypeID.ManageEmployees)]
        public bool EditAllForms { get; set; }

        [DisplayName("Alleen lezen bij Versturen")]
        [WriteAccess(FunctionRoleLevelID.Location, FunctionRoleTypeID.ManageEmployees)]
        public bool ReadOnlySender { get; set; }

        [DisplayName("Alleen lezen bij Ontvangen")]
        [WriteAccess(FunctionRoleLevelID.Location, FunctionRoleTypeID.ManageEmployees)]
        public bool ReadOnlyReciever { get; set; }

        public bool ShowMFANumber { get; set; }
        [DisplayName("Mobiel nummer t.b.v. authenticatie")]
        public string MFANumber { get; set; }
        public DateTime? MFAConfirmedDate { get { return String.IsNullOrEmpty(MFANumber) ? (DateTime?)null: DateTime.Now; } }
        [DisplayName("Google AuthID t.b.v. authenticatie")]
        public string MFAKey { get; set; }

        public bool EnableDigitalSignature { get; set; }

        [DisplayName("Digitale handtekening")]
        public int? DigitalSignatureID { get; set; }

        [DisplayName("Multi factor authenticatie")]    
        public bool MFARequired { get; set; }

        [MaxLength(300)]
        [DisplayName("Multi factor authenticatie - IP adressen")]
        public string IPAddresses { get; set; }

        [DisplayName("E-mail versturen openstaande signaleringen")]
        public bool AutoEmailSignalering { get; set; }
        public List<FunctionRoleLevelID> AllowedDossierLevelIDs { get; set; }
        public List<FunctionRoleLevelID> AllowedDossierAdminLevelsIDs { get; set; }
        public List<FunctionRoleLevelID> AllowedFunctionRoleLevelIDs { get; set; }
        public List<FunctionRoleLevelID> AllowedReportingLevelIDs { get; set; }
        public List<FunctionRoleTypeID> AllowedFunctionRoleTypeIDs { get; set; }

        public string[] HiddenFunctions { get; set; }

        public bool ShowAlterDossierAdminLevel {
            get {
                return FunctionRoleTypeIDs.Contains(FunctionRoleTypeID.ManageAll) || FunctionRoleTypeIDs.Contains(FunctionRoleTypeID.ManageEmployees);
            }
        }
    }
}