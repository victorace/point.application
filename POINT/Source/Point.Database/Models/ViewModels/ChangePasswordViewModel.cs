﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Point.Database.Models.ViewModels
{
    public class ChangePasswordViewModel
    {
        public int EmployeeID { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "{0} is verplicht")]
        [DisplayName("Huidig wachtwoord")]
        public string OldPassword { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "{0} is verplicht")]
        [DisplayName("Nieuw wachtwoord")]
        public string NewPassword { get; set; }
        [Compare("NewPassword", ErrorMessage = "Nieuwe wachtwoorden komen niet overeen")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "{0} is verplicht")]
        [DisplayName("Herhaal nieuw wachtwoord")]
        public string ConfirmPassword { get; set; }
    }
}