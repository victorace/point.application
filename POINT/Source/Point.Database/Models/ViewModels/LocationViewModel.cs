﻿using Point.Database.Attributes;
using Point.Models.Enums;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Point.Database.Models.ViewModels
{
    public class LocationViewModel
    {
        public LocationViewModel()
        {
            AutoCreateSets = new List<AutoCreateSetViewModel>();
            Assisting = new AssistingPanel();
        }

        public int LocationID { get; set; }

        [Required(ErrorMessage = BaseEntity.errorRequired)]
        [DisplayName("Organisatie")]
        public int OrganizationID { get; set; }
        public int? OrganizationTypeID { get; set; }
        public string OrganizationName { get; set; }

        [Required(ErrorMessage = BaseEntity.errorRequired)]
        [StringLength(255)]
        [DisplayName("Naam")]
        public string Name { get; set; }

        [StringLength(255)]
        [DisplayName("Straat")]
        public string StreetName { get; set; }

        [StringLength(50)]
        [DisplayName("Huisnummer")]
        public string Number { get; set; }

        [StringLength(6)]
        [DisplayName("Postcode")]
        public string PostalCode { get; set; }

        [Required(ErrorMessage = BaseEntity.errorRequired)]
        [StringLength(255)]
        [DisplayName("Plaats")]
        public string City { get; set; }

        [DisplayName("Inactive")]
        public bool Inactive { get; set; }

        [DisplayName("IP adressen controleren")]
        [WriteAccess(FunctionRoleLevelID.Organization, FunctionRoleTypeID.ManageOrganization)]
        [ReadAccess(FunctionRoleLevelID.Organization, FunctionRoleTypeID.ManageOrganization)]
        public bool IPCheck { get; set; }

        [StringLength(2000)]
        [DisplayName("IP adressen")]
        [WriteAccess(FunctionRoleLevelID.Organization, FunctionRoleTypeID.ManageOrganization)]
        [ReadAccess(FunctionRoleLevelID.Organization, FunctionRoleTypeID.ManageOrganization)]
        public string IPAddress { get; set; }

        [StringLength(255)]
        [DisplayName("Gebied (t.b.v. rapportage)")]
        public string Area { get; set; }

        [DisplayName("Organisatie regio")]
        public string OrganizationRegionName { get; set; }

        [DisplayName("Afwijkende regio")]
        public int[] OtherRegionID { get; set; }
        public string OtherRegionIDs { get; set; }

        public List<FlowParticipationViewModel> FlowParticipation { get; set; }
        
        #region sso extra settings techxxadmin
        [DisplayName("Autocreate sets")]
        public List<AutoCreateSetViewModel> AutoCreateSets { get; set; }
        #endregion

        public AssistingPanel Assisting { get; set; }
    }

    public class AssistingPanel : AssistingPanelBase
    {
        public AssistingPanel()
        {
            OrganizationList = new List<Organization>();
            OtherRegions = new List<Region>();
            RegionsMinusOtherRegions = new List<Region>();
        }

        public bool EnableAutoCreate { get; set; }
        public bool EnableOtherRegions { get; set; }
        public List<Organization> OrganizationList { get; set; }
        public List<Region> OtherRegions { get; set; }
        public List<Region> RegionsMinusOtherRegions { get; set; }
    }


}
