﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Point.Database.Models.ViewModels
{
    public class AddressViewModel
    {
        public string Street { get; set; }
        public string HouseNumber { get; set; }
        public string HouseNumberExtra { get; set; }
        public string Postbox { get; set; }
        public string PostalCode { get; set; }
        public string City { get; set; }
        public string Country { get; set; }

        public string ConcatName()
        {
            return (Street + " " + HouseNumber + " " + HouseNumberExtra + " " + Postbox + " " + PostalCode + " " + City).Replace("  ", " ").Replace("  ", " ");
        }
    }
}
