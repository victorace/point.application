﻿using System.Collections.Generic;
using System.Linq;
using Point.Models.Enums;

namespace Point.Database.Models.ViewModels
{
    public class TransferRouteViewModel: FormTypeBaseViewModel
    {
        public TransferRouteViewModel()
        {
            CIZOrganizations = new List<OrganizationInvite>();
            GRZOrganizations = new List<OrganizationInvite>();
            HealthCareProviders = new List<OrganizationInvite>();
        }

        public List<OrganizationInvite> CIZOrganizations { get; set; }
        public static List<InviteStatus> CurrentStatuses => new List<InviteStatus> { InviteStatus.Realized, InviteStatus.Active, InviteStatus.Acknowledged, InviteStatus.Queued, InviteStatus.AcknowledgedPartially };
        public string DateTimeIND { get; set; }
        public string DateTimeGRZ { get; set; }
        public bool DisableGRZ => PhaseDefinitionGRZID == null;
        public bool DisableII => PhaseDefinitionIIID == null;
        public bool DisplayAfterCareSuggestion => !Global.IsPrintRequest && Global.FlowWebFields.Any(ff => ff.Name == "AfterCare" && ff.Visible);
        public bool GRZCanBeSent { get; set; }
        public bool GRZAlreadySent => GRZOrganizations.Any(it => it.InviteStatus == InviteStatus.Active);
        public List<OrganizationInvite> GRZOrganizations { get; set; }
        public bool HasGewensteVVT { get; set; }
        public bool HasToestemmingSection => (RequestFormZHVVTType != FlowFormType.RequestFormZHVVT_FormulierRetourVPHTZ && FlowDefinitionID != FlowDefinitionID.HA_VVT);
        public List<OrganizationInvite> HealthCareProviders { get; set; }
        public List<OrganizationInvite> HistoryInvites => HealthCareProviders.Where(o => !CurrentStatuses.Contains(o.InviteStatus)).ToList();
        public bool IIAlreadySent => CIZOrganizations.Any(it => it.InviteStatus == InviteStatus.Active);
        public bool IsQueuedSelected { get; set; }
        public bool IsVVTSelected { get; set; }
        public int? PhaseDefinitionGRZID { get; set; }
        public int? PhaseDefinitionIIID { get; set; }
        public int? PhaseDefinitionMultiZAID { get; set; }
        public int? PhaseDefinitionZAID { get; set; }
        public FlowFormType RequestFormZHVVTType { get; set; }
        public bool ShowButtonCancelVVT { get; set; }
        public bool ShowPointParticipatorWarning { get; set; }
        public bool ShowResetVVT { get; set; }
        public bool VVTAlreadySent => HealthCareProviders.Any(it => it.InviteStatus == InviteStatus.Active);
        // TransferRouteHAVVT specific
        public bool ShowTussenorgWarning { get; set; }
        // TransferRouteRzTP specific
        public bool ShowWarningMessage => !Global.IsReadMode && HealthCareProviders.Any() && HealthCareProviders.All(it => it.InviteStatus != InviteStatus.Queued);
        public bool ShowAddInfoMessage => !Global.IsReadMode && !HealthCareProviders.Any();
        public TransferRouteInvitesViewModel TransferRouteInvitesViewModel { get; set; }

        public bool HideThuiszorg => FlowDefinitionID == FlowDefinitionID.HA_VVT;
        public int VVTPreferenceParticipation { get; set; } = (int)Participation.None;
    }
}
