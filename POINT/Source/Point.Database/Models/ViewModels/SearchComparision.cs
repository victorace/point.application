﻿using Point.Models.Enums;

namespace Point.Database.Models.ViewModels
{
    public class SearchComparision
    {
        public string Text { get; set; }
        public SearchComparisonOperatorType SearchComparisonOperatorType { get; set; }
        public SearchPropertyType SearchPropertyType { get; set; }
    }
}
