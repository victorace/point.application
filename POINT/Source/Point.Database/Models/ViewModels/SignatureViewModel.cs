﻿
using Point.Models.LDM.EOverdracht30.Enums;

namespace Point.Database.Models.ViewModels
{
    public class SignatureViewModel : FormTypeBaseViewModel
    {
        public string FullName { get; set; }
        public string BirthDate { get; set; }
        public Geslacht Gender { get; set; }
        public Location Location { get; set; }
        public Department Department { get; set; }
        public Employee MdwTP { get; set; }
        public Employee MdwCIZ { get; set; }
        public Employee MdwVVT { get; set; }
        public string GeenToestemmingPatientVanReden { get; set; }
        public string KamerNummer { get; set; }
    }
}
