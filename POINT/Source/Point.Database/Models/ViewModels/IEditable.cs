﻿namespace Point.Database.Models.ViewModels
{
    /// <summary>
    /// Helps assign modify/delete rights of:
    /// - attachments,
    /// - communication journals.
    /// </summary>
    public interface IEditable
    {
        int EmployeeID { get; set; }
        int OrganizationID { get; set; }
        bool IsEditable { get; set; }
    }
}