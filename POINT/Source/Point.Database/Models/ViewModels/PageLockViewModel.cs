﻿using System;
using System.Collections.Generic;

namespace Point.Database.Models.ViewModels
{
    public class PageLockViewModel
    {
        public int PageLockID { get; set; }
        public string Owner { get; set; }
        public DateTime Timestamp { get; set; }
        public string FormName { get; set; }
        public string Url { get; set; }
        public int PageLockTimeout { get; set; }
        public List<Employee> PageUnlockEmployees { get; set; }
        public bool CanRemovePageLock { get; set; } = false;
    }
}
