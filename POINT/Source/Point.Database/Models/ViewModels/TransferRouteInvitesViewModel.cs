﻿using System.Collections.Generic;

namespace Point.Database.Models.ViewModels
{
    public class TransferRouteInvitesViewModel
    {
        public TransferRouteInvitesViewModel()
        {
            CurrentInvites = new List<OrganizationInvite>();
        }
        public List<OrganizationInvite> CurrentInvites { get; set; }
        public string SearchUrl { get; set; }
        public bool IsReadMode { get; set; }
        public bool CanAdd { get; set; } = true;
        public int TransferID { get; set; }
    }
}
