﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Point.Database.Models.ViewModels
{
    public class DossierTagsViewModel
    { 
        public int TransferID { get; set; }
        public bool Editable { get; set; }
    }
}
