﻿using System.Collections.Generic;
using Point.Database.Extensions;
using Point.Models.Enums;

namespace Point.Database.Models.ViewModels
{
    public class FormTypeBaseViewModel : GlobalViewModel 
    {
        public int? ClientID { get; set; }
        public FlowDefinitionID FlowDefinitionID { get; set; }
        public int FlowInstanceID { get; set; }
        public bool Prefill => (Global.FormSetVersionID == -1 || !Global.FlowWebFields.HasValues()) && !Global.IsReadMode;
        public bool HasEndpoint { get; set; }
    }

    public class FormTypeSharedViewModel : FormTypeBaseViewModel
    {
        public IEnumerable<FormSetVersion> FormSetVersions { get; set; }
        public bool HideNewButtons { get; set; }
        public List<MedicationItemViewModel> MedicationItems { get; set; }
        public string ReceivingOrganization { get; set; }
        public string SendingOrganization { get; set; }
        public bool ShowLeverendeMedewerker => VVTDepartmentID.HasValue && (!VVTDepartmentIDMSVT.HasValue || VVTDepartmentIDMSVT == VVTDepartmentID);
        public int? VVTDepartmentID { get; set; }
        public int? VVTDepartmentIDMSVT { get; set; }
        // SignedPhaseInstance
        public bool HasSignedPhaseInstance => SignedPhaseInstancePhaseInstanceID.HasValue;
        public string SignedPhaseInstanceDate { get; set; }
        public string SignedByEmployeeFunction { get; set; }
        public string SignedByEmployeeName { get; set; }
        public int? SignedPhaseInstancePhaseInstanceID { get; set; }

    }
}
