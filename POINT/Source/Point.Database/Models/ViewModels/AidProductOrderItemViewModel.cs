﻿using Point.Database.Extensions;
using System.Collections.Generic;
using System.Linq;

namespace Point.Database.Models.ViewModels
{
    public class AidProductOrderItemViewModel
    {
        public static readonly string NAME_PREFIX = "HMI_";
        public static readonly string NAME_FORMAT_REGEXP = @"(?<productid>\d+?)_(?<fieldname>\w*)";

        public int AidProductOrderItemID { get; set; }
        public int AidProductID { get; set; }
        public int AidProductGroupID { get; set; }
        public int Quantity { get; set; }
        public string Name { get; set; }

        public static List<AidProductOrderItemViewModel> FromModelList(List<AidProductOrderItem> modellist)
        {
            return modellist.Select(it => FromModel(it)).ToList();
        }
        public static AidProductOrderItemViewModel FromModel(AidProduct model)
        {
            return new AidProductOrderItemViewModel()
            {
                AidProductOrderItemID = 0,
                AidProductID = model.AidProductID,
                AidProductGroupID = model.AidProductGroupID,
                Quantity = 1,
                Name = model.FullName()
            };
        }

        public static AidProductOrderItemViewModel FromModel(AidProductOrderItem model)
        {
            var viewmodel = FromModel(model.AidProduct);
            viewmodel.AidProductOrderItemID = model.AidProductOrderItemID;
            viewmodel.Quantity = model.Quantity;
            viewmodel.AidProductGroupID = model.AidProduct.AidProductGroupID;

            return viewmodel;
        }
    }
}
