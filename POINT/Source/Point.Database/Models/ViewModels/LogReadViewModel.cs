﻿using Point.Database.Extensions;
using Point.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Point.Database.Models.ViewModels
{

    public class LogReadViewModel
    {

        public LogRead LogRead { get; set; }
        public string RoleName { get; set; }
        public string OrganizationType { get; set; }
        public string EmployeeName { get; set; }
        public string ScreenTitle { get; set; }
        public List<ScreenType> ScreenTypes { get; set; }

        public static LogReadViewModel FromModel(LogRead logread, IEnumerable<Employee> employees, IEnumerable<Screen> screens)
        {
            var viewmodel = new LogReadViewModel()
            {
                LogRead = logread,
                RoleName = logread.RoleName,
                OrganizationType = getOrganizationType(employees.FirstOrDefault(emp => emp.EmployeeID == logread.EmployeeID)),
                EmployeeName = getEmployeeName(employees.FirstOrDefault(emp => emp.EmployeeID == logread.EmployeeID)),
                ScreenTitle = getScreenTitle(logread, screens.FirstOrDefault(sc => sc.ScreenID == logread.ScreenID)),
                ScreenTypes = getScreenTypes(logread, screens.FirstOrDefault(sc => sc.ScreenID == logread.ScreenID)).ToList()

                //RoleName = logread.Role?.Description,   //roles.FirstOrDefault(r => r.RoleId == logread.RoleID).Description,
                //OrganizationType = getOrganizationType(logread.Employee), // getOrganizationType(employees.FirstOrDefault(emp => emp.EmployeeID == logread.EmployeeID)),
                //EmployeeName = logread.Employee?.FullName(), // getEmployeeName(employees.FirstOrDefault(emp => emp.EmployeeID == logread.EmployeeID)),
                //ScreenTitle = logread.Screen?.ScreenTitle, // getScreenTitle(logread, screens.FirstOrDefault(sc => sc.ScreenID == logread.ScreenID)),
                //ScreenTypes = getScreenTypes(logread).ToList()

            };

            return viewmodel;
        }

        public static IEnumerable<LogReadViewModel> FromModelList(IEnumerable<LogRead> logReadlist, IEnumerable<Employee> employees, IEnumerable<Screen> screens)
        {
            return logReadlist?.Select(it => FromModel(it, employees, screens));
        }



        private static string getOrganizationType(Employee employee)
        {
            return employee?.DefaultDepartment?.Location?.Organization?.OrganizationType?.Name;
        }

        private static string getScreenTitle(LogRead logread, Screen screen)
        {
            string screentitle = "";
            if (screen != null)
            {
                if (!string.IsNullOrEmpty(screen.ScreenTitle))
                {
                    screentitle = screen.ScreenTitle;
                }
                else if (screen.FormTypeID != null)
                {
                    screentitle = screen.FormType?.Name;
                }
                else if (screen.GeneralActionID != null)
                {
                    screentitle = screen.GeneralAction?.Name;
                }

            }

            if (String.IsNullOrEmpty(screentitle))
            {
                string[] components = logread.Url.Split(new[] { '/', }, StringSplitOptions.RemoveEmptyEntries);

                string controller = "";
                string action = "";
                if (components.Length >= 2)
                {
                    controller = components[components.Length - 2];
                    action = components[components.Length - 1];
                }
                else if (components.Length >= 1)
                {
                    controller = components[components.Length - 1];
                }


                if (logread.Url.IndexOf("Transfer/Search", StringComparison.InvariantCultureIgnoreCase) >= 0)
                {
                    screentitle = "Zoekscherm";
                }
                else if (logread.Url.IndexOf("/Dashboard", StringComparison.InvariantCultureIgnoreCase) >= 0 || logread.Url.IndexOf("/OverviewFlow", StringComparison.InvariantCultureIgnoreCase) >= 0)
                {
                    screentitle = "Dashboard";
                }
                else if (logread.Url.IndexOf("/LogRead", StringComparison.InvariantCultureIgnoreCase) >= 0)
                {
                    screentitle = "Bekijk leesacties";
                }
                else if (logread.Url.IndexOf("/Management", StringComparison.InvariantCultureIgnoreCase) >= 0)
                {
                    screentitle = String.Format("Beheerpagina ({0}/{1})", controller, action);
                }
                else if (logread.Url.IndexOf("CommunicationLog", StringComparison.InvariantCultureIgnoreCase) >= 0)
                {
                    screentitle = "Communicatielog";
                }
                else if (logread.Url.IndexOf("FlowMain", StringComparison.InvariantCultureIgnoreCase) >= 0)
                {
                    screentitle = "Flowmain";
                }
                else if (logread.Url.IndexOf("History", StringComparison.InvariantCultureIgnoreCase) >= 0)
                {
                    screentitle = "History";
                }
                else
                {
                    screentitle = String.Format("({0}/{1})", controller, action);
                }
            }

            return screentitle;
        }


        //private static IEnumerable<ScreenType> getScreenTypes(LogRead logread)
        //{
        //    List<ScreenType> screentypeslist = new List<ScreenType>();
        //    var screen = logread.Screen;

        //    if (screen != null)
        //    {
        //        var screentypes = screen.ScreenTypes;
        //        foreach (string st in screentypes.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
        //        {
        //            if (Enum.TryParse<ScreenType>(st, out ScreenType screentype))
        //            {
        //                screentypeslist.Add(screentype);
        //            }
        //        }
        //    }

        //    return screentypeslist;
        //}


        private static IEnumerable<ScreenType> getScreenTypes(LogRead logread, Screen screen)
        {
            List<ScreenType> screentypeslist = new List<ScreenType>();

            if (screen != null)
            {
                var screentypes = screen.ScreenTypes;
                foreach (string st in screentypes.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    ScreenType screentype;
                    if (Enum.TryParse<ScreenType>(st, out screentype))
                    {
                        screentypeslist.Add(screentype);
                    }
                }
            }

            return screentypeslist;
        }

        private static string getEmployeeName(Employee employee)
        {
            if (employee == null)
            {
                return "";
            }
            else
            {
                return employee.FullName();
            }
        }
    }
}
