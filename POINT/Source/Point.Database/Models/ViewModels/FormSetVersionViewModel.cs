﻿

namespace Point.Database.Models.ViewModels
{
    public class FormSetVersionViewModel
    {
        public string Description { get; set; }
        public int FormSetVersionID { get; set; }

        public bool IsSelected { get; set; }
        public string Url { get; set; }

    }
}
