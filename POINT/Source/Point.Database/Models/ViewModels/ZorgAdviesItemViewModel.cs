﻿using System.ComponentModel;

namespace Point.Database.Models.ViewModels
{
    public class ZorgAdviesItemViewModel
    {
        public static readonly string NAME_PREFIX = "ZPITEM";
        public static readonly string NAME_FORMAT_REGEXP = @"(?<itemid>\d+?)_(?<itemvalueid>\d+?)_(?<needsfreq>\w*)_(?<fieldName>\w*)";

        public enum By
        {
            [Description("Patiënt")]
            Patient = 1,
            [Description("Mantelzorg")]
            Mantelzorg = 2,
            [Description("Welzijn/vrijw.")]
            WelzijnVrijw = 3,
            [Description("Professional")]
            Professional = 4
        }

        public enum Timespan
        {
            [Description("Dag")]
            Dag = 1,
            [Description("Week")]
            Week = 7,
            [Description("Maand")]
            Maand = 28
        }

        public int ZorgAdviesItemID { get; set; }
        public int ZorgAdviesItemValueID { get; set; }

        public string Section { get; set; }
        public string Category { get; set; }
        public string SubCategory { get; set; }
        public string Label { get; set; }
        public bool NeedsFrequencyAndTime { get; set; }
        public string Text { get; set; }
        
        public int? ActionFrequency { get; set; }
        public int? ActionTime { get; set; }
        
        public By[] ActionBy { get; set; }
        public Timespan? ActionTimespan { get; set; }
    }
}
