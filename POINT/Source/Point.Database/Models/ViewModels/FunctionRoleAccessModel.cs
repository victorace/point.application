﻿using Point.Models.Enums;

namespace Point.Database.Models.ViewModels
{
    public class FunctionRoleAccessModel
    {
        public FunctionRoleLevelID MaxLevel { get; set; }
        public int[] RegionIDs { get; set; }
        public int[] OrganizationIDs { get; set; }
        public int[] LocationIDs { get; set; }
    }
}
