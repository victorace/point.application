﻿using Point.Database.Extensions;
using Point.Database.Models.Constants;
using Point.Infrastructure.Extensions;
using System.Linq;

namespace Point.Database.Models.ViewModels
{
    public class EOverdrachtFlowViewModel : FormTypeBaseViewModel
    {
        public bool ShowResendQuestion { get; set; }
        public bool ShowPalliatief { get; set; }
        public bool ValidateOnDefinitive { get; set; }

        public bool VOFormulierTypeIsVisible {
            get {
                return Global.FlowWebFields.IsVisible(FlowFieldConstants.Name_VOFormulierType);
            }
        }
        public string VOFormulierTypeValue {
            get {
                return Global.FlowWebFields.GetValueOrDefault(FlowFieldConstants.Name_VOFormulierType, "");
            }
        }
        public bool VOFormulierTypeIsPdf => VOFormulierTypeValue == FlowFieldDataSourceConstants.VOFormulierType.Pdf;
        public bool VOFormulierTypeIsStandard => VOFormulierTypeValue == FlowFieldDataSourceConstants.VOFormulierType.Standard;
        public bool VOFormulierTypeIsToPatient => VOFormulierTypeValue == FlowFieldDataSourceConstants.VOFormulierType.ToPatient;
        public bool vOFormulierTransferHasAttachment { get; set; }
        public int vOFormulierTransferAttachmentID { get; set; } = 0;

    }

}
