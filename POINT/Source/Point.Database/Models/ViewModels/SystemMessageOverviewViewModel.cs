﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Point.Database.Models.ViewModels
{
    public class SystemMessageOverviewViewModel
    {
        public bool ShowPopup { get; set; }
        public int? LastSystemMessageID { get; set; }
        public string LastSystemMessage { get; set; }
    }
}
