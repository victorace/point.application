﻿

using System.Collections.Generic;
using Point.Models.Enums;

namespace Point.Database.Models.ViewModels
{
    public class SendToHospitalViewModel : FormTypeBaseViewModel
    {
        public SendToHospitalViewModel()
        {
            Historyinvites = new List<OrganizationInvite>();
        }
        public bool HasDestinationHospitalDepartment { get; set; }
        public bool HasDestinationHospitalLocation { get; set; }
        public List<OrganizationInvite> Historyinvites { get; set; }
        public int LastSentToHospitalLocationID { get; set; } = -1;
        public int LastSentToHospitalDepartmentID { get; set; } = -1;
        public int PhaseInstanceStatus { get; set; }
        public string ReceivingOrganization { get; set; }
        public bool SelectDepartment => Global.FlowInstance?.FlowDefinition?.FlowTypeID == (int)FlowTypeID.ZH_ZH;
        public int SenderOrganizationTypeID { get; set; }

        public bool SenderOrganizationTypeIsHospital => SenderOrganizationTypeID == (int)Point.Models.Enums.OrganizationTypeID.Hospital;
        public bool ShowButtonCancelDepartment => HasDestinationHospitalDepartment && !Global.IsReadMode;
        public bool ShowButtonCancelLocation => HasDestinationHospitalLocation && !Global.IsReadMode;
        public bool ShowResetZH => HasDestinationHospitalDepartment || HasDestinationHospitalLocation;

    }
}
