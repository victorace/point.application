﻿namespace Point.Database.Models.ViewModels
{
    public class FlowFieldDashboardViewModel
    {
        public string Name { get; set; }
        public string Label { get; set; }
        public string DisplayValue { get; set; }
        public string LookupUrl { get; set; }
    }
}
