﻿using System.Collections.Generic;
using System.ComponentModel;

namespace Point.Database.Models.ViewModels
{
    public class AidProductSearchViewModel
    {
        public List<AidProductGroupViewModel> AidProductGroups { get; set; }

        [Description("Productgroep")]
        public int? AidProductGroupID { get; set; }

        [Description("Zoeken")]
        public string SearchString { get; set; }

        public int SupplierID { get; set; }

        public List<AidProductViewModel> AidProducts { get; set; } = new List<AidProductViewModel>();
    }
}
