﻿
namespace Point.Database.Models.ViewModels
{
    public class DepartmentSearchViewModel
    {
        public string Search { get; set; }
        public int? RegionID { get; set; }
        public string[] OrganizationIDs { get; set; }
        public string[] LocationIDs { get; set; }
        public string[] DepartmentIDs { get; set; }
        public string[] SelectedDepartmentIDs { get; set; }
    }
}
