﻿using Point.Models.Enums;

namespace Point.Database.Models.ViewModels
{
    public class OrganizationSearchViewModel
    {
        public string AfterCareCategory { get; set; }
        public int? TransferID { get; set; }
        public FlowDefinitionID? FlowDefinitionID { get; set; }
        public FlowDirection FlowDirection { get; set; } = FlowDirection.Receiving;
        public OrganizationTypeID? OrganizationTypeID { get; set; }
        public RehabilitationCenterType? RehabilitationCenterType { get; set; }

        public string SortBy { get; set; }
        public bool ShowAll { get; set; } = false;
        public bool ShowRegionTab { get; set; } = true;
        public bool ShowPostalCodeTab { get; set; } = true;
        public bool ShowPostalCodeRangeTab { get; set; } = true;
        public bool ShowOnLoad { get; set; } = true;
        public int MinLength { get; set; } = 3;

        public string SearchType { get; set; } = "OwnRegion";
        public string PostalCode { get; set; }
        public string NaamPlaats { get; set; }

        public string PostalCodeRange { get; set; }
        public int? MaxRange { get; set; }

        public int[] OrganizationIDs { get; set; }
        public int[] LocationIDs { get; set; }
        public string TemporaryPostalCode { get; set; }
        public string PermanentPostalCode { get; set; }      
    }
}
