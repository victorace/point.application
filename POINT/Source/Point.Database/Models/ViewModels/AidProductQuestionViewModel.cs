﻿using System.Collections.Generic;

namespace Point.Database.Models.ViewModels
{
    public enum AidProductQuestionType
    {
        check,
        radio,
        text
    }

    public class AidProductQuestionViewModel
    {
        public string ProductGroupName { get; set; }
        public string ProductGroupCode { get; set; }
        public string QuestionID { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
        public bool Invisible { get; set; }
        public bool Required { get; set; }
        public string CurrentValue { get; set; }
        public AidProductQuestionType AidProductQuestionType { get; set; }

        public List<AidProductAnswerViewModel> PossibleAnswers { get; set; }
    }

    public class AidProductAnswerViewModel
    {
        public string ProductGroupCode { get; set; }
        public string ExtraItemsGroupCode { get; set; }
        public string QuestionID { get; set; }
        public string AnswerID { get; set; }
        public string Answer { get; set; }
        public string NextQuestionID { get; set; }
        public string Value { get; set; }
        public bool Checked { get; set; }
        public bool Delete { get; set; }
        public bool ExtraItems { get; set; }
    }
}
