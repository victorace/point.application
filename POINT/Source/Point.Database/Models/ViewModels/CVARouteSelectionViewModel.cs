﻿
using System.Collections.Generic;

namespace Point.Database.Models.ViewModels
{
    public class CVARouteSelectionViewModel : FormTypeBaseViewModel
    {
        public CVARouteSelectionViewModel()
        {
            HealthCareProviders = new List<OrganizationInvite>();
        }
        public IEnumerable<OrganizationInvite> HealthCareProviders { get; set; }
        public int? PhaseAfdAanvraag { get; set; }
        public int? PhaseAfdVO { get; set; }
        public int? PhaseAfsluiten { get; set; }
        public bool ResendAvailable { get; set; }
        public bool ShowPointParticipatorWarning { get; set; }
    }
}
