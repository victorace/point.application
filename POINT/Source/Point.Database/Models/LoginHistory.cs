using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Point.Database.Repository;

namespace Point.Database.Models
{
    [Table("LoginHistory")]
    public partial class LoginHistory : BaseEntity 
    {
        [Key]
        public int LoginID { get; set; }

        [StringLength(50)]
        public string LoginUsername { get; set; }

        [StringLength(50)]
        public string LoginOrganizationName { get; set; }

        public DateTime? LoginDateTime { get; set; }

        [StringLength(50)]
        public string LoginResolution { get; set; }

        [Column(TypeName = "text")]
        public string LoginInfo { get; set; }

        [StringLength(50)]
        public string LoginIP { get; set; }

        [Column(TypeName = "text")]
        public string LoginBrowser { get; set; }

        [StringLength(50)]
        public string ClientIP { get; set; }

        [StringLength(50)]
        public string LocalAddress { get; set; }

        public bool LoginFailed { get; set; }
        
    }
}
