﻿using Point.Database.Repository;
using Point.Log.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Point.Database.Models
{
    public class OrganizationLogo : IInActive, ILoggable
    {
        public int OrganizationLogoID { get; set; }

        [Required]
        public int ContentLength { get; set; }

        [Required, StringLength(255)]
        public string ContentType { get; set; }

        [Required, StringLength(255)]
        public string FileName { get; set; }

        [Required, Column(TypeName = "image")]
        public byte[] FileData { get; set; }

        public bool InActive { get; set; }
        public DateTime? ModifiedTimeStamp { get; set; }
    }
}
