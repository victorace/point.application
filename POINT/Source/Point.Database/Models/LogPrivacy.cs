﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Point.Database.Models
{
    public class LogPrivacy
    {
        public int LogPrivacyID { get; set; }
        public int OrganizationID { get; set; }
        public int? ClientID { get; set; }
        public int? AttachmentID { get; set; }
        public string Action { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
