﻿using Point.Models.Enums;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Point.Database.Models
{
    public class FlowInstanceReportValuesAttachmentDetail
    {
        [Column(Order = 0), Key, ForeignKey("FlowInstanceReportValues")]
        public int FlowInstanceID { get; set; }

        [Column(Order = 1), Key]
        public AttachmentTypeID AttachmentTypeID { get; set; }

        public virtual FlowInstanceReportValues FlowInstanceReportValues { get; set; }
    }
}
