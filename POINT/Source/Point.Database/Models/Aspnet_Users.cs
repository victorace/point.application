using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Point.Database.Repository;

namespace Point.Database.Models
{
    public partial class Aspnet_Users : BaseEntity 
    {
        public Aspnet_Users()
        {
            Employee = new HashSet<Employee>();
            aspnet_Roles = new HashSet<Aspnet_Roles>();
        }

        public Guid ApplicationId { get; set; }

        [Key]
        public Guid UserId { get; set; }

        [Required]
        [StringLength(256)]
        public string UserName { get; set; }

        [Required]
        [StringLength(256)]
        public string LoweredUserName { get; set; }

        [StringLength(16)]
        public string MobileAlias { get; set; }

        public bool IsAnonymous { get; set; }

        public DateTime LastActivityDate { get; set; }

        public virtual Aspnet_Applications aspnet_Applications { get; set; }

        public virtual Aspnet_Membership aspnet_Membership { get; set; }

        public virtual ICollection<Employee> Employee { get; set; }

        public virtual ICollection<Aspnet_Roles> aspnet_Roles { get; set; }
    }
}
