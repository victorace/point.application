using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Point.Database.Repository;

namespace Point.Database.Models
{
    public partial class ContactPerson : BaseEntity
    {
        public int ContactPersonID { get; set; }

        [Required]
        public int ClientID { get; set; }
        public virtual Client Client { get; set; }

        [Required]
        public int PersonDataID { get; set; }
        public virtual PersonData PersonData { get; set; }

        public int? CIZRelationID { get; set; }
        public virtual CIZRelation CIZRelation { get; set; }
    }
}