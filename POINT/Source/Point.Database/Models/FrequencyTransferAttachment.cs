﻿using System.ComponentModel.DataAnnotations.Schema;
using Point.Database.Repository;

namespace Point.Database.Models
{
    public class FrequencyTransferAttachment : BaseEntity
    {
        public int FrequencyTransferAttachmentID { get; set; }

        public int FrequencyID { get; set; }
        [ForeignKey("FrequencyID")]
        public virtual Frequency Frequency { get; set; }

        public int AttachmentID { get; set; }
        [ForeignKey("AttachmentID")]
        public virtual TransferAttachment TransferAttachment { get; set; }
        
    }
}
