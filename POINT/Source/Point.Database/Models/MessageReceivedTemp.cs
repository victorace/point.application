using Point.Models.Enums;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Point.Database.Models
{
    public partial class MessageReceivedTemp : BaseEntity
    {
        [DisplayName("Berichtnummer")]
        public int MessageReceivedTempID {get; set;}

        public DateTime TimeStamp {get;set;}

        public Guid GUID {get; set;}

        public int OrganisationID {get; set;}

        [StringLength(255)]
        [DisplayName("Verzender")]
        public string SenderUserName {get; set;}

        [DisplayName("Verzenddatum")]
        public DateTime SendDate {get; set;}

        [StringLength(50)]
        [DisplayName("Patientnummer")]
        public string PatientNumber {get; set;}

        [StringLength(50)]
        [DisplayName("Opnamenummer")]
        public string VisitNumber { get; set; }

        [StringLength(255)]
        public string Hash {get; set;}

        [StringLength(50)]
        public string Type {get; set;}

        [DisplayName("Status")]
        public string Status {get; set;}

        [StringLength(255)]
        public string MessageID { get; set; }

        [DisplayName("Bron")]
        public Source Source { get; set; }
    }
}