using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Point.Log.Attributes;
using Point.Log.Interfaces;

namespace Point.Database.Models
{
    [Table("Department"), Description("Afdeling")]
    public partial class Department : BaseEntity, ILoggable
    {
        public Department()
        {
            Employee1 = new HashSet<Employee>();
            ServiceAreaPostalCode = new HashSet<ServiceAreaPostalCode>();
            FlowDefinitionParticipation = new HashSet<FlowDefinitionParticipation>();
            AutoCreateSetDepartment = new HashSet<AutoCreateSetDepartment>();
            DepartmentCapacity = new HashSet<DepartmentCapacity>();
        }

        [SkipLog]
        public int DepartmentID { get; set; }

        [DisplayName("Locatie")]
        [LookUp("/Lookup/LocationInfo", "locationid")]
        public int LocationID { get; set; }

        [DisplayName("Afdeling type")]
        public int DepartmentTypeID { get; set; }

        [DisplayName("Specialisme/nazorgtype")]
        public int? AfterCareType1ID { get; set; }

        [Required]
        [StringLength(255)]
        [DisplayName("Naam")]
        public string Name { get; set; }

        [StringLength(50)]
        [DisplayName("Telefoon")]
        public string PhoneNumber { get; set; }

        [StringLength(50)]
        [DisplayName("Fax")]
        public string FaxNumber { get; set; }

        [StringLength(1024)]
        [DisplayName("Email")]
        public string EmailAddress { get; set; }

        [DisplayName("Ontvang email")]
        public bool RecieveEmail { get; set; }

        [DisplayName("Inactief")]
        public bool Inactive { get; set; }

        [DisplayName("Revalidatiecentrum")]
        public bool RehabilitationCenter { get; set; }

        [DisplayName("Intensief revalidatiecentrum")]
        public bool IntensiveRehabilitationNursinghome { get; set; }

        [StringLength(1024)]
        [DisplayName("Email versturen overdracht")]
        public string EmailAddressTransferSend { get; set; }

        [DisplayName("Email ontvangen overdracht")]
        public bool RecieveEmailTransferSend { get; set; }

        [StringLength(1024)]
        [DisplayName("Zorgmail versturen overdracht")]
        public string ZorgmailTransferSend { get; set; }

        [DisplayName("Zorgmail ontvangen overdracht")]
        public bool RecieveZorgmailTransferSend { get; set; }

        [DisplayName("Acute afdeling")]
        public bool IsAcute { get; set; }

        public DateTime? ModifiedTimeStamp { get; set; }

        [DisplayName("Capaciteit")]
        public int? CapacityDepartment { get; set; }

        [DisplayName("Capaciteit dag2")]
        public int? CapacityDepartmentNext { get; set; }

        [StringLength(255)]
        [DisplayName("Capaciteit info")]
        public string Information { get; set; }

        [DisplayName("Capaciteit info")]
        public DateTime? AdjustmentCapacityDate { get; set; }

        [DisplayName("Capaciteit bijgewerkt")]
        public DateTime? CapacityDepartmentDate { get; set; }

        [DisplayName("Capaciteit dag2 bijgewerkt")]
        public DateTime? CapacityDepartmentNextDate { get; set; }

        public int? UserID { get; set; }
        [ForeignKey("UserID")]
        public virtual Employee AdjustmentCapacityEmployee { get; set; }

        [DisplayName("Capaciteit dag3")]
        public int? CapacityDepartment3 { get; set; }

        [DisplayName("Capaciteit dag4")]
        public int? CapacityDepartment4 { get; set; }

        [DisplayName("Capaciteit dag3 bijgewerkt")]
        public DateTime? CapacityDepartment3Date { get; set; }

        [DisplayName("Capaciteit dag4 bijgewerkt")]
        public DateTime? CapacityDepartment4Date { get; set; }

        [DisplayName("Gebruikt capaciteit")]
        public bool CapacityFunctionality { get; set; }

        [DisplayName("Zorgmailadres t.b.v. huisarts")]
        public string AddressGPForZorgmail { get; set; }

        [StringLength(20)]
        [DisplayName("AGB code")]
        public string AGB { get; set; }

        public int? SortOrder { get; set; }

        [StringLength(50)]
        [DisplayName("Extern ID")]
        public string ExternID { get; set; }

        public virtual DepartmentType DepartmentType { get; set; }

        public virtual Location Location { get; set; }

        public virtual AfterCareType AfterCareType1 { get;set;}

        public virtual ICollection<Employee> Employee1 { get; set; }

        public virtual ICollection<ServiceAreaPostalCode> ServiceAreaPostalCode { get; set; }

        public virtual ICollection<FlowDefinitionParticipation> FlowDefinitionParticipation { get; set; }
        
        public virtual ICollection<AutoCreateSetDepartment> AutoCreateSetDepartment { get; set; }

        public virtual ICollection<EmployeeDepartment> EmployeeDepartment { get; set; }
        public virtual ICollection<DepartmentCapacity> DepartmentCapacity { get; set; }

        [DisplayName("Digitaal versturen CDA")]
        public bool RecieveCDATransferSend { get; set; }

        public string FullName()
        {
            string locationname = Location?.FullName();

            if (Name == "Afdeling 1")
                return locationname;
            else
                return string.Format("{0} - {1}", locationname, Name);
        }

        [NotMapped]
        public string FullNameProp
        {
            get
            {
                return FullName();
            }
        }

        public string ShortName()
        {
            string locationname = Location?.ShortName();

            if (Name == "Afdeling 1")
                return locationname;
            else
                return string.Format("{0}", Name);
        }

        public string NameAndAdress()
        {
            string shortname = ShortName();
            
            return string.Format("{0}, {1}", shortname, Location?.ShortAdressString());
        }
    }
}
