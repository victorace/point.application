﻿using Point.Models.Enums;

namespace Point.Database.Models
{
    public interface ICommunicationLog
    {
        int? EmployeeID { get; set; }
        int? OrganizationID { get; set; }
    }
}
