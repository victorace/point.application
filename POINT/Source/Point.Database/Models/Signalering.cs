﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Point.Database.Repository;

namespace Point.Database.Models
{
    public enum SignaleringStatus
    {
        Open = 0,
        Closed = 1,
        Expired = 2
    }

    public class Signalering
    {
        public Signalering()
        {
            SignaleringDestination = new HashSet<SignaleringDestination>();
        }

        public int SignaleringID { get; set; }
        public int FlowInstanceID { get; set; }
        [ForeignKey("FlowInstanceID")]
        public virtual FlowInstance FlowInstance { get; set; }

        public DateTime Created { get; set; }
        public int CreatedByEmployeeID { get; set; }
        [ForeignKey("CreatedByEmployeeID")]
        public virtual Employee CreatedByEmployee { get; set; }
        public int? CreatedByDepartmentID { get; set; }
        public DateTime? LastUpdated { get; set; }
        public int? LastUpdatedByEmployeeID { get; set; }

        public DateTime? Completed { get; set; }
        public int? CompletedByEmployeeID { get; set; }

        public SignaleringStatus Status { get; set; }
        public string Message { get; set; }
        public string Comment { get; set; }

        public int SignaleringTargetID { get; set; }
        public virtual SignaleringTarget SignaleringTarget { get; set; }
        public virtual ICollection<SignaleringDestination> SignaleringDestination { get; set; }
        
    }
}
