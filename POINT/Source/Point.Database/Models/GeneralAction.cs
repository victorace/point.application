﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Point.Database.Repository;

namespace Point.Database.Models
{
    public class GeneralAction : BaseEntity
    {
        public int GeneralActionID { get; set; }

        public int GeneralActionTypeID { get; set; }
        [ForeignKey("GeneralActionTypeID")]
        public virtual GeneralActionType GeneralActionType { get; set; }

        [StringLength(255)]
        public string MethodName { get; set; }

        [StringLength(100)]
        public string ClassNameMenuItem { get; set; }

        [StringLength(100)]
        public string ClassNameGlyphicon { get; set; }

        [StringLength(255)]
        public string Name { get; set; }

        [StringLength(4000)]
        public string Description { get; set; }

        [StringLength(1)]
        public string Rights { get; set; }

        public int? OrderNumber { get; set; }

        [StringLength(100)]
        public string Subcategory { get; set; }

        public bool? OnFormSetVersionOnly { get; set; }
     
        public bool HidePhaseButtons { get; set; }
    }
}
