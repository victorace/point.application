using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Point.Database.Repository;

namespace Point.Database.Models
{
    [Table("CIZRelation")]
    public partial class CIZRelation : BaseEntity
    {
        public int CIZRelationID { get; set; }

        [Required]
        [StringLength(255)]
        public string Description { get; set; }

        public int EnumerationValue { get; set; }
    }
}
