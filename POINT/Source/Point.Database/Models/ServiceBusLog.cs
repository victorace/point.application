﻿using Point.Database.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Point.Database.Models
{
    public class ServiceBusLog
    {
        public int ServiceBusLogID { get; set; }
        public string ObjectData { get; set; }
        public string ObjectType { get; set; }
        public DateTime SentDateTime { get; set; }
        public string Queue { get; set; }
        public string Label { get; set; }
        public bool Processed { get; set; }
        public bool Expired { get; set; }
        public DateTime? ProcessedDateTime { get; set; }
        public long UniqueID { get; set; }
        
    }
}
