using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Point.Database.Repository;

namespace Point.Database.Models
{
    public partial class Specialism : BaseEntity
    {
        public Specialism()
        {
            Doctor = new HashSet<Doctor>();
        }

        public int SpecialismID { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        public int? OrganizationTypeID { get; set; }

        public bool Inactive { get; set; }

        public string SpecialismType { get; set; }

        public virtual ICollection<Doctor> Doctor { get; set; }
       
        public int? AfterCareTypeID { get; set; }

        [ForeignKey("AfterCareTypeID")]
        public AfterCareType AfterCareType { get; set; }
    }
}
