﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Point.Database.Repository;

namespace Point.Database.Models
{
    public class FlowPhaseAttribute : BaseEntity
    {
        public int FlowPhaseAttributeID { get; set; }

        public int PhaseDefinitionID { get; set; }
        [ForeignKey("PhaseDefinitionID")]
        public virtual PhaseDefinition PhaseDefinition { get; set; }

        public string RoleID { get; set; }

        public int? OrganizationTypeID { get; set; }
        [ForeignKey("OrganizationTypeID")]
        public virtual OrganizationType OrganizationType { get; set; }


        public string ClassNameMenuItem { get; set; }
        
    }

}
