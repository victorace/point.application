using System.ComponentModel.DataAnnotations.Schema;
using Point.Database.Repository;

namespace Point.Database.Models
{
    [Table("ActionCodeHealthInsurerList")]
    public partial class ActionCodeHealthInsurerList : BaseEntity
    {
        public int ActionCodeHealthInsurerListID { get; set; }

        public int? ActionCodeHealthInsurerRegionID { get; set; }

        public int? ActionCodeHealthInsurerID { get; set; }

        public virtual ActionCodeHealthInsurer ActionCodeHealthInsurer { get; set; }

        public virtual ActionCodeHealthInsurerRegio ActionCodeHealthInsurerRegio { get; set; }

    }
}
