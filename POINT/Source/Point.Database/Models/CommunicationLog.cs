using Point.Models.Enums;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Point.Database.Models
{
    [Table("CommunicationLog")]
    public partial class CommunicationLog : BaseEntity
    {
        public int CommunicationLogID { get; set; }

        [StringLength(1024)]
        public string FromAddress { get; set; }

        [StringLength(1024)]
        public string FromName { get; set; }

        [StringLength(1024)]
        public string ToAddress { get; set; }

        [StringLength(1024)]
        public string ToName { get; set; }

        [StringLength(1024)]
        public string CCAddress { get; set; }

        [StringLength(1024)]
        public string BCCAddress { get; set; }

        [StringLength(1024)]
        public string Subject { get; set; }

        public string Content { get; set; }

        public DateTime? Timestamp { get; set; }

        [StringLength(255)]
        public string Description { get; set; }

        public int? TransferID { get; set; }

        public int? ClientID { get; set; }

        public int? EmployeeID { get; set; }

        public int? OrganizationID { get; set; }

        private CommunicationLogType? _MailType;
        public CommunicationLogType? MailType
        {
            get { return _MailType; }
            set { _MailType = value; }
        }
        public string ErrorMessage { get; set; }
    }
}
