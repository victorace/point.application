using System;

namespace Point.Database.Models
{
    public partial class LogEntry 
    {
        public int ScreenID { get; set; }
        
        public string Screenname { get; set; }
        
        public int EmployeeID { get; set; }
        
        public DateTime Timestamp { get; set; }
    }
}
