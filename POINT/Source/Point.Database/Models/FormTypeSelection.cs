﻿using Point.Models.Enums;
using System.Collections.Generic;

namespace Point.Database.Models
{
    public class FormTypeSelection
    {
        public int FormTypeSelectionID { get; set; }
        public FlowDefinitionID FlowDefinitionID { get; set; }
        public TypeID TypeID { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }

        public virtual FlowDefinition FlowDefinition { get; set; }
        public virtual ICollection<FormTypeSelectionOrganization> FormTypeSelectionOrganization { get; set; }
    }
}
