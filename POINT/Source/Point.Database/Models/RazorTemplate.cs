using System.ComponentModel.DataAnnotations.Schema;
using Point.Models.Enums;
using Point.Database.Repository;

namespace Point.Database.Models
{
    [Table("RazorTemplate")]
    public partial class RazorTemplate
    {
        public int RazorTemplateID { get; set; }
        public string TemplateName { get; set; }
        public string TemplateCode { get; set; }
        public string Contents { get; set; }
        public CommunicationLogType CommunicationLogType { get; set; }
    }
}
