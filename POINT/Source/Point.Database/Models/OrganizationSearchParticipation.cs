﻿using Point.Models.Enums;
using Point.Database.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Point.Database.Models
{
    public class OrganizationSearchParticipation
    {
        public int OrganizationSearchParticipationID { get; set; }
        public int OrganizationSearchID { get; set; }
        public FlowDefinitionID FlowDefinitionID { get; set; }
        public FlowDirection FlowDirection { get; set; }
        public Participation Participation { get; set; }

        public virtual OrganizationSearch OrganizationSearch { get; set; }
        
    }
}
