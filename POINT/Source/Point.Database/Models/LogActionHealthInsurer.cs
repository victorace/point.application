﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Point.Database.Models
{
    public class LogActionHealthInsurer
    {
        public int LogActionHealthInsurerID { get; set; }
        public int? ActionHealthInsurerID { get; set; }
        public DateTime? TimeStamp { get; set; }
        public int? EmployeeID { get; set; }
        public string Action { get; set; }
        public int? ScreenID { get; set; }
        public int? Version { get; set; }

        [ForeignKey("EmployeeID")]
        public virtual Employee Employee { get;set;}
    }
}
