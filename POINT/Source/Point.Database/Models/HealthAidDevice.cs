﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Point.Database.Repository;

namespace Point.Database.Models
{
    [Table("HealthAidDevice")]
    public class HealthAidDevice : BaseEntity
    {
        public int HealthAidDeviceId { get; set; }

        [StringLength(255)]
        [Required]
        public string Name { get; set; }

        public int Sortorder { get; set; }
        
    }
}
