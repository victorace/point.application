﻿using Point.Models.Enums;
using System.Collections.Generic;

namespace Point.Database.Models
{
    public class ReportDefinition
    {
        public int ReportDefinitionID { get; set; }
        public string FileName { get; set; }
        public string ExcelFriendlyFileName { get; set; }
        public string ReportName { get; set; }
        public string ReportActionName { get; set; }
        public string Category { get; set; }
        public string Description { get; set; }
        public string FilterPartial { get; set; }

        public FlowDirection FlowDirection { get; set; }
        public bool NeedsOrganizationID { get; set; }
        public bool Visible { get; set; }

        public virtual ICollection<ReportDefinitionFlowDefinition> ReportDefinitionFlowDefinition { get; set; }
    }
}
