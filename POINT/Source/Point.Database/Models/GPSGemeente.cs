﻿using Point.Database.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Point.Database.Models
{
    public class GPSGemeente : BaseEntity
    {
        public int GPSGemeenteID { get; set; }
        public string Naam { get; set; }
        public int? GPSProvincieID { get; set; }
        
        public virtual GPSProvincie GPSProvincie { get; set; }
        
    }
}
