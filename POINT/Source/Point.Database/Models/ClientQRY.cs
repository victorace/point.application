using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Point.Database.Models
{
    
    /// <summary>
    /// NOTE: This model maps the individual tables ClientQRY_##, NOT the ClientQRY-table itself (which isn't used)
    /// </summary>
    public partial class ClientQRY //: BaseEntity
    {
        [Key, Column(Order = 0)]
        public string MESSAGE_ID {get; set;}

        [Key, Column(Order = 1)]
        public string PARENT_ID {get; set;}

        [Key, Column(Order = 2)]
        public string PatientID {get; set;}

        public string MsgID {get; set;}

        public string MsgID_1 {get; set;}

        public string TimeStamp { get; set; }
    }
}
