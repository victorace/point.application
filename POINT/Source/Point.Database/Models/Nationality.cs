using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Point.Database.Repository;

namespace Point.Database.Models
{
    [Table("Nationality")]
    public partial class Nationality : BaseEntity
    {
        public int NationalityID { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        public bool InActive { get; set; }
        
    }
}
