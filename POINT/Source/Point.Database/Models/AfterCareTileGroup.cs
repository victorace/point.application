﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Point.Database.Repository;

namespace Point.Database.Models
{
    public class AfterCareTileGroup : BaseEntity
    {
        public int AfterCareTileGroupID { get; set; }
        public string Name { get; set; }
        public string ColorCode { get; set; }
        public int Sortorder { get; set; }

        public virtual ICollection<AfterCareGroup> AfterCareGroup { get; set; }
    }
}
