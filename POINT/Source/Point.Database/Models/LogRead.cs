using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Point.Database.Repository;
using System;

namespace Point.Database.Models
{
    [Table("PointLog_LogRead")]
    public partial class LogRead : BaseEntity
    {
        public int LogReadID { get; set; }

        public int? ClientID { get; set; }

        public int? TransferID { get; set; }

        public DateTime? TimeStamp { get; set; }

        public int? EmployeeID { get; set; }
        [StringLength(1)]
        public string Action { get; set; }

        public int? ScreenID { get; set; }
        public string RoleName { get; set; }
        [StringLength(2048)]
        public string Url { get; set; }
        [StringLength(2048)]
        public string QueryString { get; set; }
    }
}
