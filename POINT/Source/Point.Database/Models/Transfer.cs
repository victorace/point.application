using Point.Database.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Point.Database.Models
{
    [Table("Transfer")]
    public partial class Transfer : BaseEntity, ILogEntryWithID 
    {
        public Transfer()
        {
            TransferAttachments = new HashSet<TransferAttachment>();
            FlowInstances = new HashSet<FlowInstance>();
            FormSetVersions = new HashSet<FormSetVersion>();
            TransferMemos = new HashSet<TransferMemo>();
            Frequencies = new HashSet<Frequency>();
            TransferTasks = new HashSet<TransferTask>();
        }

        public int TransferID { get; set; }

        public int ClientID { get; set; }

        public DateTime? TimeStamp { get; set; }

        public int? EmployeeID { get; set; }

        public int? ScreenID { get; set; }

        public int? OriginalTransferID { get; set; }

        public virtual Client Client { get; set; }
        
        public virtual ICollection<FormSetVersion> FormSetVersions { get; set; }
        
        public virtual ICollection<TransferAttachment> TransferAttachments { get; set; }

        public virtual ICollection<FlowInstance> FlowInstances { get; set; } // NOTE due to UNIQUE INDEX in DB, this is actually 1:1 (i.e. there can be 0..1 FlowInstance in this collection)

        public virtual ICollection<TransferMemo> TransferMemos { get; set; }

        public virtual ICollection<TransferTask> TransferTasks { get; set; }

        public virtual ICollection<Frequency> Frequencies { get; set; }
        
    }
}
