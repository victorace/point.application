﻿using Point.Models.Enums;
using Point.Database.Repository;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Point.Database.Models
{
    public class TransferTask : BaseEntity
    {
        public int TransferTaskID { get; set; }

        public int TransferID { get; set; }

        [DisplayName("Omschrijving")]
        [StringLength(255)]
        public string Description { get; set; }

        [DisplayName("Opmerkingen")]
        [StringLength(4000)]
        public string Comments { get; set; }

        [DisplayName("Datum/tijdstip aangemaakt")]
        public DateTime CreatedDate { get; set; }

        public int CreatedByEmployeeID { get; set; }
        [ForeignKey("CreatedByEmployeeID")]
        public virtual Employee CreatedByEmployee { get; set; }

        [DisplayName("Datum/tijdstip uitvoering")]
        public DateTime DueDate { get; set; }

        [DisplayName("Datum/tijdstip afgehandeld")]
        public DateTime? CompletedDate { get; set; }

        public int? CompletedByEmployeeID { get; set; }
        [ForeignKey("CompletedByEmployeeID")]
        public virtual Employee CompletedByEmployee { get; set; }

        public Status Status { get; set; }

        public bool Inactive { get; set; }
        
    }
}
