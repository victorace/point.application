using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Point.Database.Repository;

namespace Point.Database.Models
{
    [Table("PageLock")]
    public partial class PageLock : BaseEntity
    {
        public int PageLockID { get; set; }

        public int TransferID { get; set; }

        public int EmployeeID { get; set; }

        public virtual Employee Employee { get; set; }

        [Required]
        [StringLength(1024)]
        public string URL { get; set; }

        [Required]
        [Column(TypeName = "varchar")]
        [StringLength(32)]
        public string SessionID { get; set; }

        public DateTime TimeStamp { get; set; }
        
    }
}
