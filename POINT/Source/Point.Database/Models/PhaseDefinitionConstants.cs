﻿namespace Point.Database.Models
{
    public static class PhaseDefinitionConstants
    {
        // FlowDefinitionID 2
        public const string ZHVVT_SUBCATEGORY_DOORLOPENDDOSSIER         = "Doorlopend dossier";
        public const string ZHVVT_SUBCATEGORY_INDICATIESTELLING         = "Indicatiestelling";
        public const string ZHVVT_SUBCATEGORY_TRANSFERPUNT              = "Transferpunt";
        public const string ZHVVT_SUBCATEGORY_VERPLEEGKUNDIGEOVERDRACHT = "Verpl.overdracht";
        public const string ZHVVT_SUBCATEGORY_VVTBEHANDELING            = "VVT Behandeling";

        // FlowDefinitionID 3
        public const string RZTP_SUBCATEGORY_VERPLEEGKUNDIGEOVERDRACHT  = "Verpl.overdracht";

        // FlowDefinitionID 4
        public const string CVA_SUBCATEGORY_DOORLOPENDDOSSIER           = "Doorlopend dossier";
        public const string CVA_SUBCATEGORY_INDICATIESTELLING           = "Indicatiestelling";
        public const string CVA_SUBCATEGORY_TRANSFERPUNT                = "Transferpunt";
        public const string CVA_SUBCATEGORY_VERPLEEGKUNDIGEOVERDRACHT   = "Verpl.overdracht";
        public const string CVA_SUBCATEGORY_VVTBEHANDELING              = "VVT Behandeling";
    }
}
