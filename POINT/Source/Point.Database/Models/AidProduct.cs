﻿namespace Point.Database.Models
{
    public class AidProduct
    {
        public int AidProductID { get; set; }
        public int AidProductGroupID { get; set; }
        public int SupplierID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public byte[] ImageData { get; set; }

        public decimal Price { get; set; }
        public decimal PricePerDay { get; set; }
        public decimal TransportCosts { get; set; }

        public bool AuthorizationForm { get; set; }
        public string Code { get; set; }
        public string InsurersUZOVIList { get; set; }
        public virtual Supplier Supplier { get; set; }
        public virtual AidProductGroup AidProductGroup { get; set; }
    }
}
