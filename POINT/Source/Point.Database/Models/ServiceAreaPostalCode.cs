using System;
using System.ComponentModel.DataAnnotations.Schema;
using Point.Log.Interfaces;
using Point.Log.Attributes;
using System.ComponentModel;

namespace Point.Database.Models
{
    [Table("ServiceAreaPostalCode"), Description("Bedieningsgebied postcodes")]
    public partial class ServiceAreaPostalCode : BaseEntity, ILoggable
    {
        [SkipLog]
        public int ServiceAreaPostalCodeID { get; set; }

        [DisplayName("Afdeling")]
        [LookUp("/Lookup/DepartmentInfo", "departmentid")]
        public int? DepartmentID { get; set; }

        [DisplayName("Bedieningsgebied")]
        [LookUp("/Lookup/ServiceAreaInfo", "serviceareaid")]
        public int? ServiceAreaID { get; set; }

        [DisplayName("Postcode")]
        [LookUp("/Lookup/PostalCodeInfo", "postalcodeid")]
        public int? PostalCodeID { get; set; }

        public DateTime? ModifiedTimeStamp { get; set; }

        public virtual Department Department { get; set; }

        public virtual PostalCode PostalCode { get; set; }

        public virtual ServiceArea ServiceArea { get; set; }
    }
}
