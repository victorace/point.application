using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Point.Database.Repository;

namespace Point.Database.Models
{
    public partial class UsedHashCodes : BaseEntity 
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid UsedHashCodeId { get; set; }
        public string HashCode { get; set; }

        public int? EmployeeID { get; set; }

        public DateTime CreatedDate { get; set; }

        public bool IsAuth { get; set; }

        public virtual Employee Employee { get; set; }
        
    }
}
