﻿using Point.Database.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Point.Database.Models
{
    public class GPSProvincie : BaseEntity
    {
        public int GPSProvincieID { get; set; }
        public string Naam { get; set; }
        
    }
}
