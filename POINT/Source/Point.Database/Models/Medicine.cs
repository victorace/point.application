using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Point.Database.Repository;

namespace Point.Database.Models
{
    [Table("Medicine")]
    public partial class Medicine : BaseEntity
    {
        public Medicine()
        {
            MedicineCard = new HashSet<MedicineCard>();
        }

        public int MedicineID { get; set; }

        [StringLength(255)]
        public string Name { get; set; }

        [StringLength(100)]
        public string Code { get; set; }

        public int? SortOrder { get; set; }

        public virtual ICollection<MedicineCard> MedicineCard { get; set; }
        
    }
}
