﻿using Point.Models.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Point.Database.Models
{
    [Table("FlowInstanceSearchValues")]
    public class FlowInstanceSearchValues
    {
        public FlowInstanceSearchValues()
        {
            FlowInstanceOrganization = new HashSet<FlowInstanceOrganization>();
            FlowInstanceDoorlopendDossierSearchValues = new HashSet<FlowInstanceDoorlopendDossierSearchValues>();
        }

        [Key]
        public int FlowInstanceID { get; set; }

        public int TransferID { get; set; }
        public FlowDefinitionID FlowDefinitionID { get; set; }

        public bool IsInterrupted { get; set; }
        public int? CopyOfTransferID { get; set; }
        public string ActivePhaseText { get; set; }
        public int ScopeType { get; set; }

        public string AcceptedBy { get; set; }
        public int? AcceptedByID { get; set; }
        public string AcceptedByII { get; set; }
        public int? AcceptedByIIID { get; set; }
        public string AcceptedByVVT { get; set; }
        public int? AcceptedByVVTID { get; set; }
        public string AcceptedByTelephoneNumber { get; set; }
        public DateTime? AcceptedDateTP { get; set; }

        public int? TransferCreatedByID { get; set; }
        public string TransferCreatedBy { get; set; }

        public string BehandelaarSpecialisme { get; set; }
        public DateTime? CareBeginDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}")]
        public DateTime? ClientBirthDate { get; set; }
        public string ClientFullname { get; set; }
        public string ClientGender { get; set; }
        public string PatientNumber { get; set; }

        public DateTime? DatumEindeBehandelingMedischSpecialist { get; set; }
        public string DepartmentName { get; set; }
        public string FullDepartmentName { get; set; }

        public string DesiredHealthCareProvider { get; set; }
        public string DestinationHospital { get; set; }
        public string DestinationHospitalLocation { get; set; }
        public string DestinationHospitalDepartment { get; set; }
        public string DischargePatientAcceptedYNByVVT { get; set; }
        public DateTime? DischargeProposedStartDate { get; set; }
        public DateTime? LastAdditionalTPMemoDateTime { get; set; }
        public DateTime? LastGrzFormDateTime { get; set; }
        public DateTime? LastTransferAttachmentUploadDate { get; set; }
        public DateTime? LastTransferMemoDateTime { get; set; }
        public string LocationName { get; set; }
        public string OrganizationName { get; set; }
        public string RequestFormZHVVTType { get; set; }
        public DateTime? RequestTransferPointIntakeDate { get; set; }
        public DateTime? TransferCreatedDate { get; set; }
        public DateTime? TransferDate { get; set; }
        public DateTime? TransferDateVVT { get; set; }
        public DateTime? GewensteIngangsdatum { get; set; }
        public string TyperingNazorgCombined { get; set; }
        public string FlowDefinitionName { get; set; }
        public string ClientCivilServiceNumber { get; set; }
        public string BehandelaarNaam { get; set; }
        public DateTime? MedischeSituatieDatumOpname { get; set; }
        public string AcceptedByTelephoneVVT { get; set; }
        public string ZorgInZorginstellingVoorkeurPatient1 { get; set; }

        public string HealthInsuranceCompany { get; set; }
        public int? HealthInsuranceCompanyID { get; set; }

        public DateTime? ReadModelTimeStamp { get; set; }

        public DateTime? TransferTaskDueDate { get; set; }

        [ForeignKey("FlowInstanceID")]
        public virtual FlowInstance FlowInstance { get; set; }

        public virtual ICollection<FlowInstanceOrganization> FlowInstanceOrganization { get; set; }
        public virtual ICollection<FlowInstanceDoorlopendDossierSearchValues> FlowInstanceDoorlopendDossierSearchValues { get; set; }
    }
}
