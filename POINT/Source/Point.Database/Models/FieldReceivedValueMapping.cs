﻿namespace Point.Database.Models
{
    public partial class FieldReceivedValueMapping : BaseEntity
    {
        public int FieldReceivedValueMappingID { get; set; }

        public string FieldName { get; set; }

        public string SourceValue { get; set; }

        public string TargetValue { get; set; }
        
    }
}
