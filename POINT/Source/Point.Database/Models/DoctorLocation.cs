﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Point.Database.Models
{
    public class DoctorLocation
    {
        [Key]
        [Column(Order = 1)]
        public int DoctorID { get; set; }
        public virtual Doctor Doctor { get; set; }

        [Key]
        [Column(Order = 2)]
        public int LocationID { get; set; }
        public virtual Location Location { get; set; }
    }
}
