using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Point.Database.Models
{
    [Table("FormType")]
    public partial class FormType : BaseEntity
    {
        public FormType()
        {
            ActionCodeHealthInsurerFormType = new HashSet<ActionCodeHealthInsurerFormType>();
            FormTypeOrganization = new HashSet<FormTypeOrganization>();
            FormTypeRegion = new HashSet<FormTypeRegion>();
            FlowFieldAttribute = new HashSet<FlowFieldAttribute>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FormTypeID { get; set; }

        [StringLength(255)]
        public string Name { get; set; }

        [StringLength(255)]
        public string PrintName { get; set; }

        [StringLength(255)]
        public string URL { get; set; }

        public int? TypeID { get; set; }

        public bool Remove { get; set; }

        public int? Order { get; set; }

        public bool IsPrintable { get; set; }

        public bool HasPageLocking { get; set; }
        public bool IsVisibleInHistory { get; set; }

        public virtual ICollection<FlowFieldAttribute> FlowFieldAttribute { get; set; }

        public virtual ICollection<ActionCodeHealthInsurerFormType> ActionCodeHealthInsurerFormType { get; set; }

        public virtual ICollection<PhaseDefinition> PhaseDefinition { get; set; }

        public virtual ICollection<FormTypeOrganization> FormTypeOrganization { get; set; }

        public virtual ICollection<FormTypeRegion> FormTypeRegion { get; set; }

        public bool? PrintOnNewPage { get; set; }

        public bool HasDigitalSignature { get; set; }
        
    }
}
