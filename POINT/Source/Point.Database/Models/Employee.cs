using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Point.Log.Attributes;
using Point.Log.Interfaces;
using Point.Models.LDM.EOverdracht30.Enums;

namespace Point.Database.Models
{
    [Table("Employee"), Description("Medewerker")]
    public partial class Employee : BaseEntity, ILoggable
    {
        public Employee()
        {
            EmployeeDepartment = new HashSet<EmployeeDepartment>();
            FunctionRole = new HashSet<FunctionRole>();
        }

        [SkipLog]
        public int EmployeeID { get; set; }

        [DisplayName("Afdeling")]
        [LookUp("/Lookup/DepartmentInfo", "departmentid")]
        public int DepartmentID { get; set; }

        [StringLength(255), DisplayName("Voornaam")]
        public string FirstName { get; set; }

        [StringLength(50), DisplayName("Tussenvoegsel")]
        public string MiddleName { get; set; }

        [Required]
        [StringLength(255), DisplayName("Achternaam")]
        public string LastName { get; set; }

        [Required]
        public Geslacht? Gender { get; set; }

        [StringLength(255)]
        public string Position { get; set; }

        [StringLength(50)]
        public string PhoneNumber { get; set; }

        public Guid? UserId { get; set; }

        [StringLength(255)]
        public string EmailAddress { get; set; }

        public bool InActive { get; set; }

        [StringLength(50)]
        public string ExternID { get; set; }

        public bool ChangePasswordByEmployee { get; set; }

        [StringLength(50)]
        public string BIG { get; set; }

        public DateTime? ModifiedTimeStamp { get; set; }

        public int? CreatedWithAutoCreateSetID { get; set; }

        public bool EditAllForms { get; set; }

        public bool ReadOnlySender { get; set; }

        public bool ReadOnlyReciever { get; set; }

        [StringLength(50)]
        public string MFANumber { get; set; }
        [StringLength(50)]
        [SkipLog]
        public string MFAKey { get; set; }

        public DateTime? MFAConfirmedDate { get; set; }

        public int? DigitalSignatureID { get; set; }
        public bool AutoEmailSignalering { get; set; }
        public virtual DigitalSignature DigitalSignature { get; set; }

        public virtual Aspnet_Users aspnet_Users { get; set; }

        public virtual Department DefaultDepartment { get; set; }

        public virtual ICollection<FunctionRole> FunctionRole { get; set; }

        public virtual ICollection<Department> CapacityDepartments { get; set; }

        public virtual ICollection<EmployeeDepartment> EmployeeDepartment { get; set; }

        public string LockAction { get; set; }
        public bool MFARequired { get; set; }

        [StringLength(300)]
        public string IPAddresses { get; set; }

        public DateTime? LastRealLoginDate { get; set; }
        public DateTime? AutoLockAccountDate { get; set; }
    }
}
