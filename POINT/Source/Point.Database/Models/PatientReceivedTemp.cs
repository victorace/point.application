﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Point.Database.Models
{
    public class PatientReceivedTemp : BaseEntity
    {
        public int PatientReceivedTempID { get; set; }

        public string PatientNumber { get; set; }
        public string CivilServiceNumber { get; set; }
        public string Gender { get; set; }
        public string Salutation { get; set; }
        public string Initials { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string MaidenName { get; set; }
        public DateTime? BirthDate { get; set; }
        public string StreetName { get; set; }
        public string Number { get; set; }
        public string PostalCode { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string PhoneNumberGeneral { get; set; }
        public string PhoneNumberMobile { get; set; }
        public string PhoneNumberWork { get; set; }
        public string ContactPersonName { get; set; }
        public DateTime? ContactPersonBirthDate { get; set; }
        public string ContactPersonRelationType { get; set; }
        public string ContactPersonPhoneNumberGeneral { get; set; }
        public string ContactPersonPhoneNumberMobile { get; set; }
        public string ContactPersonPhoneNumberWork { get; set; }
        public int HealthInsuranceCompanyUZOVICode { get; set; }
        public string InsuranceNumber { get; set; }
        public string GeneralPractitionerName { get; set; }
        public string GeneralPractitionerPhoneNumber { get; set; }
        public string PharmacyName { get; set; }
        public string PharmacyPhoneNumber { get; set; }
        public string CivilClass { get; set; }
        public string CompositionHousekeeping { get; set; }
        public int ChildrenInHousekeeping { get; set; }
        public string HousingType { get; set; }
        public string HealthCareProvider { get; set; }
        public string AddressGPForZorgmail { get; set; }
        public string VisitNumber { get; set; }
        public DateTime TimeStamp { get; set; }
        public Guid GUID { get; set; }
        public int OrganisationID { get; set; }
        public string SenderUserName { get; set; }
        public DateTime SendDate { get; set; }
        public string Hash { get; set; }
        public string InterfaceCode { get; set; }
        public string MessageType { get; set; }

        // mapping between HealthInsuranceCompanyUZOVICode and HealthInsuranceCompanyID
        [NotMapped]
        public int HealthInsuranceCompanyID { get; set; }
    }
}
