using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Point.Database.Repository;

namespace Point.Database.Models
{
    [Table("Screen")]
    public partial class Screen : BaseEntity
    {

        //Because ScreenID is not auto increment in SQL, and Id must be generated in the code:
        
        public int ScreenID { get; set; }

        public int? FormTypeID { get; set; }

        public virtual FormType FormType { get; set; }

        public int? GeneralActionID { get; set; }
        public virtual GeneralAction GeneralAction { get; set; }
        [StringLength(255)]
        public string URL { get; set; }
        [StringLength(50)]
        public string ScreenTypes {get; set; }

        public string ScreenTitle{ get; set; }

    }
}
