using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Point.Database.Repository;

namespace Point.Database.Models
{
    [Table("OrganizationOutsidePoint")]
    public partial class OrganizationOutsidePoint : BaseEntity
    {
        public int OrganizationOutsidePointID { get; set; }

        [StringLength(255)]
        public string Name { get; set; }

        //Not officialy referencing the OrganizationType-table
        public int? OrganizationTypeID { get; set; }

        [StringLength(6)]
        public string PostalCode { get; set; }

        public int? Code { get; set; }

        //Not officialy referencing the Organization-table
        public int? OrganizationID { get; set; }
        
    }
}
