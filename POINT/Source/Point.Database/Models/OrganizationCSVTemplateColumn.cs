﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Point.Database.Models
{
    public class OrganizationCSVTemplateColumn
    {
        [Column(Order = 0), Key]
        public int OrganizationCSVTemplateID { get; set; }
        [Column(Order = 1), Key]
        public string ColumnSource { get; set; }
        public int Position { get; set; }

        public virtual OrganizationCSVTemplate OrganizationCSVTemplate { get; set; }
    }
}
