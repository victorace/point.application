﻿using Point.Models.Enums;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Point.Database.Models
{
    public class FlowInstanceReportValuesDump
    {
        [Key]
        public int FlowInstanceID { get; set; }
        public int ReceiveCount { get; set; }
        public DateTime? DecisionFormStartDate { get; set; }
        public DateTime? DecisionFormRealizedDate { get; set; }
        public DateTime? OpstellenVODate { get; set; }
        public Status? OpstellenVOStatus { get; set; }
        public DateTime? MSVTUVVDate { get; set; }
        public int MSVTUVVCount { get; set; }
        public DateTime? MSVTINDDate { get; set; }
        public DateTime? GRZDate { get; set; }
        public DateTime? ELVDate { get; set; }
        public int DoorlDossierCount { get; set; }
        public DateTime? AanvullenFirstDate { get; set; }
        public DateTime? AanvullenLastDate { get; set; }
        public DateTime? RequestInBehandelingStartDate { get; set; }
        public DateTime? RequestInBehandelingRealizeDate { get; set; }
        public Status? RequestInBehandelingStatus { get; set; }
        public DateTime? ForwardDate { get; set; }
        public DateTime? TransferClosedDate { get; set; }
        public DateTime? IndicationCIZReceiveFormStartDate { get; set; }
        public DateTime? IndicationCIZReceiveFormRealizeDate { get; set; }
        public DateTime? IndicationCIZDecisionFormStartDate { get; set; }
        public DateTime? IndicationCIZDecisionFormRealizeDate { get; set; }
        public DateTime? IndicationGRZReceiveFormStartDate { get; set; }
        public DateTime? IndicationGRZReceiveFormRealizeDate { get; set; }
        public DateTime? IndicationGRZDecisionFormStartDate { get; set; }
        public DateTime? IndicationGRZDecisionFormRealizeDate { get; set; }
        public DateTime? ReceiveFormStartDate { get; set; }
        public DateTime? ReceiveFormRealizedDate { get; set; }
        public DateTime? RequestFormStartDate { get; set; }
        public DateTime? RequestFormRealizedDate { get; set; }
        public DateTime? ReceivingDate { get; set; }
        public string ReceivingRegionName { get; set; }
        public string ReceivingOrganizationName { get; set; }
        public string ReceivingLocationName { get; set; }
        public string ReceivingDepartmentName { get; set; }
        public string ReceivingArea { get; set; }
        public string ReceivingPostalCode { get; set; }
        public string IndiceringCIZOrganizationName { get; set; }
        public string IndiceringCIZLocationName { get; set; }
        public string IndiceringCIZDepartmentName { get; set; }
        public string IndiceringGRZOrganizationName { get; set; }
        public string IndiceringGRZLocationName { get; set; }
        public string IndiceringGRZDepartmentName { get; set; }
        public string DoorlopendDossierOrganizationName { get; set; }
        public string DoorlopendDossierLocationName { get; set; }
        public string DoorlopendDossierDepartmentName { get; set; }

        [ForeignKey("FlowInstanceID")]
        public virtual FlowInstanceReportValues FlowInstanceReportValues { get; set; }
    }
}
