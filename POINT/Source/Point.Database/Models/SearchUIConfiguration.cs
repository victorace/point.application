﻿using Point.Models.Enums;
using Point.Database.Repository;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Point.Database.Models
{
    [Table("SearchUIConfiguration")]
    public class SearchUIConfiguration : BaseEntity
    {
        #region Fields
        private DossierMenus _dossierMenus;
        private SortOrderInfo _sortOrder;
        #endregion

        [Key]
        public int SearchUIConfigurationID { get; set; }

        public int? DepartmentID { get; set; }
        [ForeignKey("DepartmentID")]
        public virtual Department Department { get; set; }

        public int? OrganizationID { get; set; }
        [ForeignKey("OrganizationID")]
        public virtual Organization Organization { get; set; }

        public FlowDefinitionID FlowDefinitionID { get; set; }
        [ForeignKey("FlowDefinitionID")]
        public virtual FlowDefinition FlowDefinition { get; set; }

        [Index]
        public string SearchType { get; set; } // NOTE Enums.SearchType.ToString()

        public SortOrderInfo SortOrder
        {
            get
            {
                return _sortOrder ?? (_sortOrder = new SortOrderInfo());
            }
            set
            {
                _sortOrder = value;
            }
        }

        public int AutoRefreshSeconds { get; set; } // 0 = disabled

        public DossierMenus DossierMenus
        {
            get
            {
                return _dossierMenus ?? (_dossierMenus = new DossierMenus());
            }
            set
            {
                _dossierMenus = value;
            }
        }

        public int PageSize { get; set; } // 0 = code default

        public virtual ICollection<SearchUIFieldConfiguration> Fields { get; set; }

    }

    [ComplexType]
    public class SortOrderInfo
    {
        private FieldInfo _field;

        public FieldInfo Field
        {
            get
            {
                return _field ?? (_field = new FieldInfo());
            }
            set
            {
                _field = value;
            }
        }

        public bool OrderByDescending { get; set; }
    }

    [ComplexType]
    public class DossierMenus
    {
        public bool OnlyCVA { get; set; }
        public bool OnlyMSVT { get; set; }
        public bool AllAcceptCVA { get; set; }
        public bool AllAcceptMSVT { get; set; }
        public bool AllAcceptCVAAndMSVT { get; set; }
    }

    [ComplexType]
    public class ColumnInfo
    {
        public bool IsVisible { get; set; }
        [Range(0, int.MaxValue)]
        public int Index { get; set; }
        public string FullName { get; set; }
        public string HeaderName { get; set; }
        public bool IsSortable { get; set; }
        public string CssClass { get; set; }
        public void AssignFrom(ColumnInfo other)
        {
            this.Index = other.Index;
            this.IsVisible = other.IsVisible;
        }
    }

    [ComplexType]
    public class IconInfo
    {
        public bool ShowAsIcon { get; set; }

        [StringLength(255)] // Bootstrap Glyph Name e.g. "glyphicon glyphicon-paperclip", "glyphicon glyphicon-ok"
        public string GlyphName { get; set; }

        [StringLength(255)] // Bootstrap Glyph Name e.g. "glyphicon glyphicon-remove"
        public string GlyphNameFalse { get; set; }

        public bool TreatEmptyAsNull { get; set; }

        public bool TreatNullAsFalse { get; set; }

        public IconInfo Clone()
        {
            var res = new IconInfo()
            {
                ShowAsIcon = this.ShowAsIcon,
                GlyphName = this.GlyphName,
                GlyphNameFalse = this.GlyphNameFalse,
                TreatEmptyAsNull = this.TreatEmptyAsNull,
                TreatNullAsFalse = this.TreatNullAsFalse
            };

            return res;
        }

        public void AssignFrom(IconInfo other)
        {
            this.ShowAsIcon = other.ShowAsIcon;
            this.GlyphName = other.GlyphName;
            this.GlyphNameFalse = other.GlyphNameFalse;
            this.TreatEmptyAsNull = other.TreatEmptyAsNull;
            this.TreatNullAsFalse = this.TreatNullAsFalse;
        }
    }
}
