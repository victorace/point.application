﻿using System;

namespace Point.Database.Models
{
    public abstract class BaseEntity
    {
        //Kinda lonely out here. 
        //Well maybe some code in the near future...

        public const string errorRequired = "{0} is verplicht.";
        public const string errorRequiredBy = "{0} is verplicht voor {1}.";
        public const string errorMaxLength = "{0} mag maximaal {1} tekens lang zijn.";
        public const string errorNumber = "{0} moet een getal zijn.";
        public const string errorRegexp = "{0} bevat ongeldig tekens.";
        public const string errorDate = "{0} moet een geldig datum zijn ({1}).";
        public const string errorTime = "{0} moet een geldig tijd zijn ({1}).";
        public const string errorNotInDataSet = "{0} bevat een ongeldige waarde.";
        public const string errorSelection = "{0} moet een geldige keuze bevatten.";

        public static string ErrorMessage(string errorMessage, object[] args)
        {
            return String.Format(errorMessage, args);
        }
    }
}
