﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Point.Database.Models.ReportModels
{
    public class DICAReportModel
    {
        public int FlowInstanceID { get; set; }
        public int? OrganizationID { get; set; }
        public string Land { get; set; }
        public int? Diagnoseid { get; set; }
        public string IDCode { get; set; }
        public string Voorl { get; set; }
        public string Tussen { get; set; }
        public string Naam { get; set; }
        public int Upn { get; set; }
        public DateTime? Gebdat { get; set; }
        public string Geslacht { get; set; }
        public string Pcode { get; set; }
        public string Diagnose_type { get; set; }
        public DateTime Presseh { get; set; }
        public string Pressehuur { get; set; }
        public string Presseminuut { get; set; }
        public int Tromiv { get; set; }
        public DateTime? Tromven { get; set; }
        public string Tromvenuur { get; set; }
        public string Tromvenminuut { get; set; }
        public int Tromia { get; set; }
        public DateTime? Tromart { get; set; }
        public string Tromartuur { get; set; }
        public string Tromartminuut { get; set; }
        public string Redenlatetrom { get; set; }
        public DateTime? Datumfu { get; set; }
        public string Mrs3maandfu { get; set; }
        public string Funietgedaan { get; set; }
        public string Nihss_totaal { get; set; }
        public string Door_to_groin_time { get; set; }
        public string Door_to_needle_time { get; set; }
        public int Verwezen { get; set; }
        public DateTime Datum1eklacht { get; set; }
        public string Datum1eklachtuur { get; set; }
        public string Datum1eklachtminuut { get; set; }
        public DateTime? DatumOverleden { get; set; }
        public string Patientnummer{ get; set; }
    }
}