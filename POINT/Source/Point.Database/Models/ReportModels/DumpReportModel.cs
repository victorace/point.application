﻿using Point.Database.Attributes;
using Point.Models.Enums;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Point.Database.Models.ReportModels
{
    public class DumpReportModelList
    {
        [XmlElement("DumpReportModel")]
        public List<DumpReportModel> Items { get; set; }
    }

    public class DumpReportModel
    {
        public int TransferID { get; set; }
        public int? OriginalTransferID { get; set; }
        public ScopeType ScopeType { get; set; }
        public string FlowDefinitionName { get; set; }
        public DateTime? ClientBirthDate { get; set; }
        public string ClientHealthCareInsurer { get; set; }
        public string ClientGender { get; set; }
        public string ClientPatientNumber { get; set; }
        public string ClientVisitNumber { get; set; }
        public string ClientCity { get; set; }
        public string ClientPostalCode { get; set; }
        public DateTime TransferCreatedDate { get; set; }
        public int TransferCreatedYear { get { return TransferCreatedDate.Year; } }
        public int TransferCreatedMonth { get { return TransferCreatedDate.Month; } }
        public string TransferCreatedBy { get; set; }
        public DateTime? TransferClosedDate { get; set; }
        public string AfterCareCategoryDossier { get; set; }
        public string AfterCareTypeDossier { get; set; }
        public string AfterCareFinancing { get; set; }
        public string SpecialismeBehandelaar { get; set; }
        public string PrognoseVerwachteOntwikkeling { get; set; }
        public string ActivePhaseText { get; set; }
        public string AcceptedBy { get; set; }
        public string RequestFormZHVVTType { get; set; }
        public bool? PatientOntvingZorgVoorOpnameJaNee { get; set; }
        public string ZorginzetVoorDezeOpnameTypeZorginstelling { get; set; }
        public bool? KwetsbareOudereJaNee { get; set; }
        public double? InterruptionDays { get; set; }
        public int? ReceiveCount { get; set; }
        public bool RegistrationEndedWithoutTransfer { get; set; }
        public string ReasonEndRegistration { get; set; }
        public string AdjustmentStandingCare { get; set; }
        public string AcceptedByVVT { get; set; }
        public string AcceptedByII { get; set; }
        public bool? PatientAccepted { get; set; }
        public string DestinationHospitalDepartment { get; set; }
        public DateTime? VOSendDate { get; set; }
        public DateTime? FirstReceiverDate { get; set; }
        public DateTime? IndicationCIZReceiveFormStartDate { get; set; }
        public DateTime? IndicationCIZReceiveFormRealizeDate { get; set; }
        public DateTime? IndicationCIZDecisionFormStartDate { get; set; }
        public DateTime? IndicationCIZDecisionFormRealizeDate { get; set; }
        public DateTime? DateIndicationDecision { get; set; }
        public DateTime? IndicationGRZReceiveFormStartDate { get; set; }
        public DateTime? IndicationGRZReceiveFormRealizeDate { get; set; }
        public DateTime? IndicationGRZDecisionFormStartDate { get; set; }
        public DateTime? IndicationGRZDecisionFormRealizeDate { get; set; }
        public DateTime? DischargeProposedStartDate { get; set; }
        public DateTime? FirstInitialDischargeDate { get; set; }
        public DateTime? InitialDischargeDate { get; set; }
        public DateTime? RealizedDischargeDate { get; set; }
        public DateTime? DatumOpname { get; set; }
        public DateTime? CareBeginDate { get; set; }
        public DateTime? RequestFormStartDate { get; set; }
        public DateTime? RequestFormRealizedDate { get; set; }
        public DateTime? ReceiveFormStartDate { get; set; }
        public DateTime? ReceiveFormRealizedDate { get; set; }
        public DateTime? DecisionFormStartDate { get; set; }
        public DateTime? DecisionFormRealizedDate { get; set; }
        public DateTime? AanvullenFirstDate { get; set; }
        public DateTime? AanvullenLastDate { get; set; }
        public DateTime? OpstellenVODate { get; set; }
        public Status? OpstellenVOStatus { get; set; }
        public DateTime? MSVTUVVDate { get; set; }
        public int? MSVTUVVCount { get; set; }
        public DateTime? MSVTINDDate { get; set; }
        public DateTime? GRZDate { get; set; }
        public DateTime? ELVDate { get; set; }
        public bool ZorgAdviesToevoegen { get; set; }
        public int? DoorlDossierCount { get; set; }
        public DateTime? RequestInBehandelingStartDate { get; set; }
        public DateTime? RequestInBehandelingRealizeDate { get; set; }
        public Status? RequestInBehandelingStatus { get; set; }
        public DateTime? ForwardDate { get; set; }
        public string SendingRegionName { get; set; }
        public string SendingOrganizationName { get; set; }
        public string SendingLocationName { get; set; }
        public string SendingDepartmentName { get; set; }
        public DateTime? ReceivingDate { get; set; }
        public string ReceivingRegionName { get; set; }
        public string ReceivingOrganizationName { get; set; }
        public string ReceivingLocationName { get; set; }
        public string ReceivingDepartmentName { get; set; }
        public string ReceivingPostalCode { get; set; }
        public string ReceivingArea { get; set; }
        public string IndiceringCIZOrganizationName { get; set; }
        public string IndiceringCIZLocationName { get; set; }
        public string IndiceringCIZDepartmentName { get; set; }
        public string IndiceringGRZOrganizationName { get; set; }
        public string IndiceringGRZLocationName { get; set; }
        public string IndiceringGRZDepartmentName { get; set; }
        public string DoorlopendDossierOrganizationName { get; set; }
        public string DoorlopendDossierLocationName { get; set; }
        public string DoorlopendDossierDepartmentName { get; set; }

        [XmlIgnore, CSVIgnore]
        public List<string> Projects { get; set; }
        public string ProjectsString { get { return string.Join(",", Projects); } }

        [XmlIgnore, CSVIgnore]
        public List<AttachmentTypeID> AttachmentTypes { get; set; }
        public string AttachmentTypeString { get { return string.Join(",", AttachmentTypes); } }
    }
}
