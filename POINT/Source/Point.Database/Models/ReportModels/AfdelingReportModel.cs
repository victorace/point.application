﻿using Point.Infrastructure.Extensions;
using Point.Models.Enums;
using System.Collections.Generic;
using System.Linq;

namespace Point.Database.Models.ReportModels
{
    public class AfdelingReportModel
    {
        public string Organisatie { get; set; }
        public string Locatie { get; set; }
        public string Afdeling { get; set; }
        public string Postcode { get; set; }
        public string Plaatsnaam { get; set; }
        public string Specialisme { get; set; }
        public string Telefoonnummer { get; set; }
        public string Faxnummer { get; set; }

        public string EmailadresTbvAanvraagJaNee { get; set; }
        public string EmailadresTbvAanvraag { get; set; }
        public string EmailadresTbvVONotificatieJaNee { get; set; }
        public string EmailadresTbvVONotificatie { get; set; }
        public string DigitaalVersturenVOJaNee { get; set; }
        public string DigitaalVersturenVO { get; set; }
        public string DigitaalVersturenCDAJaNee { get; set; }
        public List<FlowDefinitionID> FlowsActiefList { get; set; }
        public string FlowsActiefString { get { return string.Join(" ", FlowsActiefList.OrderBy(f => f).Select(f => f.GetDescription())); } }
        public List<FlowDefinitionID> FlowsFaxMailList { get; set; }
        public string FlowsFaxMailString { get { return string.Join(" ", FlowsFaxMailList.OrderBy(f => f).Select(f => f.GetDescription())); } }
        public string TonenOpCapaciteitSchermJaNee { get; set; }
        public string RevalidatieCentrumJaNee { get; set; }
        public string IntensieveRevalidatieVerpleeghuisJaNee { get; set; }
        public string Sortering { get; set; }
        public List<string> AfdelingBedieningsgebiedList { get; set; }
        public string AfdelingBedieningsgebiedString { get { return string.Join(" ", AfdelingBedieningsgebiedList); } }
    }
}