﻿using System;

namespace Point.Database.Models.ReportModels
{
    public class AfdelingCapacityReportModel
    {
        public int DepartmentID { get; set; }
        public string Organisatie { get; set; }
        public string Locatie { get; set; }
        public string Afdeling { get; set; }
        public string Specialisme { get; set; }
        public string SpecialismeCategorie { get; set; }

        public DateTime CapacityDateTime { get; set; }

        public int Year { get { return CapacityDateTime.Year; } }
        public int Month { get { return CapacityDateTime.Month; } }

        public int Capacity { get; set; }
        public int MinCapacity { get; set; }
        public int MaxCapacity { get; set; }

    }
}