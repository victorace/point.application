﻿using Point.Database.Helpers;
using System;

namespace Point.Database.Models.ReportModels
{
    public class StroomTrendModel
    {
        public string PeriodeNaam { get; set; } = "Alle";
        public string SetName { get; set; }
        public string SetDetails { get; set; }
        public DateTime TransferCreatedDate { get; set; }
        public int Jaar { get; set; }
        public int Maand { get; set; }
        public int Week { get; set; }
        public string VersturendeOrganisatie { get; set; }
        public string VersturendeLocatie { get; set; }
        public string VersturendeAfdeling { get; set; }
        public string OntvangendeOrganisatie { get; set; }
        public string OntvangendeLocatie { get; set; }
        public string OntvangendeAfdeling { get; set; }
        public string SpecialismeBehandelaar { get; set; }
        public int? OntslagDatumVerschilDagen { get; set; }
        public bool? OntslagDatumBehaald { get; set; }
        public double? PreTransferFaseDagen { get; set; }
        public double? TransferFaseDagen { get; set; }
        public bool IsVoorrang { get; set; }
        public double? OntvangFaseDagen { get; set; }
        public double? BesluitFaseDagen { get; set; }
        public double? OntvangBesluitFaseDagen { get; set; }
        public double? OverdrachtFaseDagen { get; set; }
        public string DossierPhaseType { get; set; }
        public string NazorgType { get; set; }
        public string NazorgCategorie { get; set; }
        public string NazorgCategorieLumc { get; set; }
        
        //Voor KPI vanwege andere berekening (werkuren etc)
        public DateTime? OntvangFaseStart { get; set; }
        public DateTime? OntvangFaseEnd { get; set; }
        public DateTime? BesluitFaseStart { get; set; }
        public DateTime? BesluitFaseEnd { get; set; }
        
        public double? NormTijd { get; set; } = 2;

        public string NormTijdString {
            get {
                if(NormTijd == null)
                {
                    return "";
                }
                return TimeSpan.FromHours(NormTijd.Value).ToString(@"hh\:mm");
            }
        }
        
        public int? NormGroupAverage { get; set; } = 75;
        public int BinnenNormOntvangFase { get { return DateHelper.DiffWorkingHours(OntvangFaseStart, OntvangFaseEnd) <= NormTijd ? 1 : 0; } }
        public int BinnenNormBesluitFase { get { return DateHelper.DiffWorkingHours(BesluitFaseStart, BesluitFaseEnd) <= NormTijd ? 1 : 0; } }
        public int TransferID { get; set; }
    }
}