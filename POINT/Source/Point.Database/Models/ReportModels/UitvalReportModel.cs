﻿using Point.Infrastructure.Extensions;
using Point.Models.Enums;
using System;

namespace Point.Database.Models.ReportModels
{
    public class UitvalReportModel
    {
        public int TransferID { get; set; }
        public string FlowDefinitionName { get; set; }
        public DateTime DateTimeCreated { get; set; }
        public string ClientName { get; set; }
        public DateTime? ClientBirthDate { get; set; }
        public string ClientBSN { get; set; }

        public string VersturendeOrganisatie { get; set; }
        public string VersturendeLocatie { get; set; }
        public string VersturendeAfdeling { get; set; }

        public string OntvangendeRegio { get; set; }
        public string OntvangendeOrganisatie { get; set; }
        public string OntvangendeLocatie { get; set; }
        public string OntvangendeAfdeling { get; set; }

        public string MyOrganizationName { get; set; }
        public string MyLocationName { get; set; }
        public string MyDepartmentName { get; set; }

        public int Different { get; set; }

        public InviteStatus? MyInviteStatus { get; set; }
        public string MyInviteStatusName => MyInviteStatus.GetDescription();

        public InviteStatus? InviteStatus { get; set; }
        public string InviteStatusName => InviteStatus.GetDescription();

        public string Medewerker { get; set; }
        public string Opmerking { get; set; }
        public DateTime? DatumIngaveNee { get; set; }

        public int DossierJaar { get { return DateTimeCreated.Year; } }
        public int DossierMaand { get { return DateTimeCreated.Month; } }
        public string DossierMaandNaam { get { return DateTimeCreated.ToString("MMMM", new System.Globalization.CultureInfo("nl")); } }

    }
}