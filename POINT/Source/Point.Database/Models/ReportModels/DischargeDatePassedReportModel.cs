﻿using System;

namespace Point.Database.Models.ReportModels
{
    public class DischargeDatePassedReportModel
    {
        public string VersturendeOrganisatie { get; set; }
        public string VersturendeLocatie { get; set; }
        public string VersturendeAfdeling { get; set; }
        public string OntvangendeOrganisatie { get; set; }
        public string OntvangendeLocatie { get; set; }
        public string OntvangendeAfdeling { get; set; }
        public string Patient { get; set; }
        public DateTime? GewensteOntslagDatum { get; set; } 
        public int? AantalKalenderDagen { get; set; }
        public int? AantalWerkDagen { get; set; }

    }
}