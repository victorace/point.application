﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Point.Database.Models.ReportModels
{
    public class StroomReportModel
    {
        [DisplayName("TransferID")]
        public int TransferID { get; set; }
        [DisplayName("Flow naam")]
        public string FlowDefinitionName { get; set; }
        [DisplayName("Datum aangemaakt")]
        public DateTime DateTimeCreated { get; set; }
        [DisplayName("Jaar")]
        public int Year { get { return DateTimeCreated.Year; } }
        [DisplayName("Maand")]
        public int Month { get { return DateTimeCreated.Month; } }
        [DisplayName("Patient")]
        public string ClientName { get; set; }
        [DisplayName("Geb. Datum")]
        public DateTime? ClientBirthDate { get; set; }
        [DisplayName("BSN")]
        public string ClientBSN { get; set; }

        [DisplayName("Versturende Organisatie")]
        public string SendingOrganizationName { get; set; }
        [DisplayName("Versturende Locatie")]
        public string SendingLocationName { get; set; }
        [DisplayName("Versturende Afdeling")]
        public string SendingDepartmentName { get; set; }
        [DisplayName("Versturende Afdeling Specialisme")]
        public string SendingDepartmentAfterCareType { get; set; }

        [DisplayName("Ontvangende Organisatie")]
        public string ReceivingOrganizationName { get; set; }
        [DisplayName("Ontvangende Locatie")]
        public string ReceivingLocationName { get; set; }
        [DisplayName("Ontvangende Afdeling")]
        public string ReceivingDepartmentName { get; set; }
        [DisplayName("Ontvangende Afdeling Specialisme")]
        public string ReceivingDepartmentAfterCareType { get; set; }

        [DisplayName("Indicerende CIZ Organisatie")]
        public string IndiceringCIZOrganizationName { get; set; }
        [DisplayName("Indicerende CIZ Locatie")]
        public string IndiceringCIZLocationName { get; set; }
        [DisplayName("Indicerende CIZ Afdeling")]
        public string IndiceringCIZDepartmentName { get; set; }
        [DisplayName("Indicerende CIZ Afdeling Specialisme")]
        public string IndiceringCIZDepartmentAfterCareType { get; set; }

        [DisplayName("Indicerende GRZ Organisatie")]
        public string IndiceringGRZOrganizationName { get; set; }
        [DisplayName("Indicerende GRZ Locatie")]
        public string IndiceringGRZLocationName { get; set; }
        [DisplayName("Indicerende GRZ Afdeling")]
        public string IndiceringGRZDepartmentName { get; set; }
        [DisplayName("Indicerende GRZ Afdeling Specialisme")]
        public string IndiceringGRZDepartmentAfterCareType { get; set; }

        [DisplayName("Doorl.dossier Organisatie")]
        public string DoorlopendDossierOrganizationName { get; set; }
        [DisplayName("Doorl.dossier Locatie")]
        public string DoorlopendDossierLocationName { get; set; }
        [DisplayName("Doorl.dossier Afdeling")]
        public string DoorlopendDossierDepartmentName { get; set; }
        [DisplayName("Doorl.dossier Afdeling Specialisme")]
        public string DoorlopendDossierDepartmentAfterCareType { get; set; }

        public List<string> Projects { get; set; }
        public string ProjectsString { get { return string.Join(",", Projects); } }
        [DisplayName("Nazorgtype dossier")]
        public string AfterCareTypeDossier { get; set; }
        [DisplayName("Specialisme behandelaar")]
        public string SpecialismeBehandelaar { get; set; }
        [DisplayName("Actieve fase")]
        public string ActivePhaseName { get; set; }
    }
}
