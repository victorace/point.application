﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Point.Database.Models.ReportModels
{
    public class ControlInfoReportModel
    {
        public int TransferID { get; set; }
        public string VersturendeOrganisatie { get; set; }
        public string VersturendeLocatie { get; set; }
        public string VersturendeAfdeling { get; set; }
        public string OntvangendeOrganisatie { get; set; }
        public string OntvangendeLocatie { get; set; }
        public string OntvangendeAfdeling { get; set; }
        public string IndicerendeCIZOrganisatie { get; set; }

        public string NazorgType { get; set; }
        public string NazorgCategorie { get; set; }
        public string SpecialismeBehandelaar { get; set; }
        public string ClientName { get; set; }
        public DateTime? ClientBirthDate { get; set; }
        public DateTime DateTimeCreated { get; set; }
        public DateTime? DatumOpname { get; set; }
        public DateTime? GewensteOntslagDatum { get; set; }
        public DateTime? GerealiseerdeOntslagDatum { get; set; }

        public int? OptimaleOpnameDuur { get; set; }
        public DateTime? RequestInBehandelingStartDate { get; set; }
        public double? BeschikbaarGesteldeDagenVoorOntslag { get; set; }
        public double? BewerkingduurTransferPunt { get; set; }
        public double? BewerkingduurIndicerende { get; set; }
        public double? BewerkingduurOntvangFase { get; set; }
        public double? BewerkingduurBesluitFase { get; set; }
        public double? BewerkingduurOverdracht { get; set; }
        public double? GerealiseerdeOpnameDuur { get; set; }
        public int? VerkeerdeBedDagen { get; set; }
        public int? VerkeerdeBedWeekDagen { get; set; }
        public double? DuurOnderbroken { get; set; }

        public DateTime? IndicerendeStartDate { get; set; }
        public DateTime? IndicerendeEndDate { get; set; }
        public DateTime? OntvangStartDate { get; set; }
        public DateTime? OntvangEndDate { get; set; }
        public DateTime? OntvangBesluitStartDate { get; set; }
        public DateTime? OntvangBesluitEndDate { get; set; }
        public DateTime? DischargeProposedStartDate { get; set; }
        public DateTime? DateIndicationDecision { get; set; }

    }
}