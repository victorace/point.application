﻿namespace Point.Database.Models.ReportModels
{
    public class Top10ReportModel
    {
        public string Organization { get; set; }
        public int Count { get; set; }
        public bool IsOthers { get; set; }
    }
}