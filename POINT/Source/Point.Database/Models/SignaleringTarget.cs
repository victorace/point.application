﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Point.Database.Repository;
using Point.Models.Enums;

namespace Point.Database.Models
{
    public class SignaleringTarget
    {
        public int SignaleringTargetID { get; set; }
        public string Action { get; set; }
        public int SignaleringTriggerID { get; set; }
        public int OrganizationTypeID { get; set; }
        public int? DepartmentTypeID { get; set; }
        public int? RegionID { get; set; }
        public int? OrganizationID { get; set; }
        public int? MinActivePhase { get; set; }
        public int? MaxActivePhase { get; set; }
        public bool TargetDepartment { get; set; }
        public FlowDirection FlowDirection { get; set; }

        public virtual SignaleringTrigger SignaleringTrigger { get; set; }
        
    }
}
