using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Point.Database.Models
{
    [Table("HealthInsurer")]
    public partial class HealthInsurer : BaseEntity
    {
        public int HealthInsurerID { get; set; }

        [StringLength(255)]
        public string Name { get; set; }

        public int? SortOrder { get; set; }

        public int? UZOVI { get; set; }

        public string MediPointCode { get; set; }
        public string VegroCode { get; set; }
        public bool InActive { get; set; }
        public int? HealthInsurerConcernID { get; set; }

        public virtual HealthInsurerConcern HealthInsurerConcern { get; set; }

    }
}
