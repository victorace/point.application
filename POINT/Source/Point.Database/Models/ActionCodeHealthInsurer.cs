using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Point.Database.Repository;

namespace Point.Database.Models
{
    [Table("ActionCodeHealthInsurer")]
    public partial class ActionCodeHealthInsurer : BaseEntity
    {
        public ActionCodeHealthInsurer()
        {
            ActionCodeHealthInsurerFormType = new HashSet<ActionCodeHealthInsurerFormType>();
            ActionCodeHealthInsurerList = new HashSet<ActionCodeHealthInsurerList>();
        }

        public int ActionCodeHealthInsurerID { get; set; }

        [StringLength(1)]
        public string Complex { get; set; }

        public int? ActionGroupHealthInsurerID { get; set; }

        [StringLength(4000)]
        public string ActionCodeName { get; set; }

        public int? TimeLimitMin { get; set; }

        public int? TimeLimitMax { get; set; }

        public DateTime? DateBegin { get; set; }

        public DateTime? DateEnd { get; set; }

        [StringLength(5)]
        public string ActionCodeHealthInsurerNumber { get; set; }

        public int? ActionCodeHealthInsurerNumberOnScreen { get; set; }

        public virtual ActionGroupHealthInsurer ActionGroupHealthInsurer { get; set; }

        public virtual ICollection<ActionCodeHealthInsurerFormType> ActionCodeHealthInsurerFormType { get; set; }

        public virtual ICollection<ActionCodeHealthInsurerList> ActionCodeHealthInsurerList { get; set; }

        public virtual ICollection<ActionHealthInsurer> ActionHealthInsurer { get; set; }
    }
}
