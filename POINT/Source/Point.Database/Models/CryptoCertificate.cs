﻿using Point.Database.Repository;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Point.Database.Models
{
    [Table("CryptoCertificate")]
    public class CryptoCertificate : BaseEntity
    {

        public int CryptoCertificateID { get; set; }

        [StringLength(100)]
        public string FriendlyName { get; set; }
        [StringLength(100)]
        public string FileName { get; set; }
        /// <summary>
        /// PrivateKey: Stored in the database via MachineKey.Protect as 
        /// </summary>
        [StringLength(255)]
        public string PrivateKey { get; set; }

        public DateTime ExpiryDate { get; set; }
        [StringLength(100)]
        public string CertificateType { get; set; }
        [StringLength(255)]
        public string Description { get; set; }

        [Column(TypeName = "image")]
        public byte[] FileData { get; set; }

    }
}
