﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Point.Database.Models
{
    public class LogPointFormService
    {
        public int LogPointFormServiceID { get; set; }
        public DateTime Timestamp { get; set; }
        public int? OrganizationID { get; set; }
        [StringLength(255)]
        public string Hash { get; set; }
        public string URL { get; set; }
        public string Request { get; set; }
    }
}
