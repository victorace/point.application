using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Point.Database.Models
{
    [Table("ViewTransferHistory")]
    public class ViewTransferHistory : BaseEntity
    {
        public int ViewTransferHistoryID { get; set; }
        public int EmployeeID { get; set; }
        public int TransferID { get; set; }
        public DateTime ViewDateTime { get; set; }
        
    }
}
