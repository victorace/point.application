using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Point.Database.Repository;

namespace Point.Database.Models
{
    [Table("ActionGroupHealthInsurer")]
    public partial class ActionGroupHealthInsurer : BaseEntity
    {
        public ActionGroupHealthInsurer()
        {
            ActionCodeHealthInsurer = new HashSet<ActionCodeHealthInsurer>();
        }

        public int ActionGroupHealthInsurerID { get; set; }

        [StringLength(400)]
        public string ActionGroupName { get; set; }

        public virtual ICollection<ActionCodeHealthInsurer> ActionCodeHealthInsurer { get; set; }
    }
}
