﻿using Point.Models.Enums;
using Point.Database.Repository;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Point.Database.Models
{
    [Table("SearchUIFieldConfiguration")]
    public class SearchUIFieldConfiguration : BaseEntity
    {
        private IconInfo _icon;
        private ColumnInfo _column;
        private FieldInfo _field;

        [Key]
        public int SearchUIFieldConfigurationID { get; set; }

        public int SearchUIConfigurationID { get; set; }
        [ForeignKey("SearchUIConfigurationID")]
        public virtual SearchUIConfiguration SearchUIConfiguration { get; set; }

        public FieldInfo Field
        {
            get
            {
                return _field ?? (_field = new FieldInfo());
            }
            set
            {
                _field = value;
            }
        }

        public ColumnInfo Column
        {
            get
            {
                return _column ?? (_column = new ColumnInfo());
            }
            set
            {
                _column = value;
            }
        }

        public IconInfo Icon
        {
            get
            {
                return _icon ?? (_icon = new IconInfo());
            }
            set
            {
                _icon = value;
            }
        }

        public bool UseInSearchFilter { get; set; }
        public bool SelectedInSearchFilter { get; set; }
        public SearchPropertyType SearchPropertyType { get; set; }
        public SearchDestination SearchDestination { get; set; }
        public SearchFieldOwningType SearchFieldOwningType { get; set; }
        

        public void AssignFrom(SearchUIFieldConfiguration other)
        {
            this.Column.AssignFrom(other.Column);
            this.Icon.AssignFrom(other.Icon);
            this.UseInSearchFilter = other.UseInSearchFilter;
        }
    }
}
