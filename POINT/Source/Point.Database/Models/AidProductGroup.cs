﻿using System.Collections.Generic;

namespace Point.Database.Models
{
    public class AidProductGroup
    {
        public int AidProductGroupID { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public virtual ICollection<AidProduct> AidProducts { get; set; }
    }
}
