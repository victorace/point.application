﻿using System;
using System.ComponentModel;
using Point.Models.Enums;
using Point.Log.Attributes;
using Point.Log.Interfaces;

namespace Point.Database.Models
{
    [Description("Flows")]
    public class FlowDefinitionParticipation : BaseEntity, ILoggable
    {
        [SkipLog]
        public int FlowDefinitionParticipationID { get; set; }

        [DisplayName("Flow")]
        public FlowDefinitionID FlowDefinitionID { get; set; }
        [SkipLog]
        public int LocationID { get; set; }

        [DisplayName("Afdeling")]
        [LookUp("/Lookup/DepartmentInfo", "departmentid")]
        public int? DepartmentID { get; set; }

        [DisplayName("Deelname")]
        public Participation Participation { get; set; }

        [DisplayName("Flowrichting")]
        public FlowDirection FlowDirection { get; set; }

        public DateTime? ModifiedTimeStamp { get; set; }

        public virtual FlowDefinition FlowDefinition { get; set; }
        public virtual Location Location { get; set; }
        public virtual Department Department { get; set; }
    }
}
