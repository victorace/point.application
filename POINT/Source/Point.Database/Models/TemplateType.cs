using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Point.Database.Models
{
    [Table("TemplateType")]
    public partial class TemplateType : BaseEntity
    {
        public TemplateType()
        {
            Template = new HashSet<Template>();
            TemplateDefault = new HashSet<TemplateDefault>();
        }

        public int TemplateTypeID { get; set; }

        [StringLength(255)]
        public string Name { get; set; }

        [StringLength(255)]
        public string Description { get; set; }

        public bool CanBeOrganizationSpecific  { get; set; }

        public virtual ICollection<Template> Template { get; set; }

        public virtual ICollection<TemplateDefault> TemplateDefault { get; set; }
        
    }
}
