using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Point.Database.Models
{
    public partial class FieldReceivedTemp : BaseEntity
    {
        public int FieldReceivedTempID {get; set;}

        public int MessageReceivedTempID {get; set;}

        [DisplayName("Verwerkingsdatum")]
        public DateTime Timestamp {get; set;}

        [StringLength(100)]
        [DisplayName("Veldnaam")]
        public string FieldName {get; set;}

        [DisplayName("Waarde")]
        public string FieldValue {get; set;}

        [DisplayName("Status")]
        public string Status {get; set;}
    }
}
