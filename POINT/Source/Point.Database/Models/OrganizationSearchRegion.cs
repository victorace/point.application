﻿using System.ComponentModel.DataAnnotations.Schema;
using Point.Database.Repository;

namespace Point.Database.Models
{
    public class OrganizationSearchRegion
    {
        public int OrganizationSearchRegionID { get; set; }
        public int OrganizationSearchID { get; set; }
        public int RegionID { get; set; }
        public string RegionName { get; set; }

        public virtual OrganizationSearch OrganizationSearch { get; set; }
        
    }
}
