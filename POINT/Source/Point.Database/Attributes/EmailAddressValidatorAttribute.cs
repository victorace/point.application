﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Point.Database.Models.Constants;

namespace Point.Database.Attributes
{
    public class EmailAddressValidatorAttribute : RegularExpressionAttribute
    {
        private char[] _allowedSeparators = { ';', ',', ':' };

        public EmailAddressValidatorAttribute() : base(RegularExpressions.EmailAddress)
        {
            ErrorMessage = "E-mailadres is niet geldig";
        }

        public override bool IsValid(object value)
        {
            // TODO: DISABLE TEMPORARILY. ENABLE UNTIL A PROPER EMAILADDRESS FORM VALIDATION HAS BEN IMPLEMENTED.
            return true;

            //var emailValue = (string) value;
            //if (string.IsNullOrEmpty(emailValue))
            //{
            //    return true;
            //}

            //var sep = GetSeparator(emailValue);
            //var emails = ((string) value).Split(new []{sep} , StringSplitOptions.RemoveEmptyEntries);

            //return emails.All(email => base.IsValid(email.Trim()));
        }

        private char GetSeparator(string emailValue)
        {
            foreach (var sep in _allowedSeparators)
            {
                if (emailValue.IndexOf(sep) >= 0)
                {
                    return sep;
                }
            }

            return _allowedSeparators.First();
        }

    }
    
}
