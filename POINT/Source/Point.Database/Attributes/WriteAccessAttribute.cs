﻿using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using System;

namespace Point.Database.Attributes
{
    public class WriteAccessAttribute : Attribute
    {
        public FunctionRoleLevelID? MinimumFunctionRoleLevelID { get; set; }
        public FunctionRoleTypeID[] FunctionRoleTypeIDs { get; set; }

        public WriteAccessAttribute(FunctionRoleLevelID minimumFunctionRoleLevelID, params FunctionRoleTypeID[] functionRoleTypeIDs)
        {
            MinimumFunctionRoleLevelID = minimumFunctionRoleLevelID;
            FunctionRoleTypeIDs = functionRoleTypeIDs;
        }

        public bool HasWriteAccess(PointUserInfo pointUserInfo)
        {
            foreach (var functionRoleTypeID in FunctionRoleTypeIDs)
            {
                var level = pointUserInfo.GetFunctionRoleLevelID(functionRoleTypeID);
                if (level >= MinimumFunctionRoleLevelID)
                {
                    return true;
                }
            }

            return false;
        }
    }
}