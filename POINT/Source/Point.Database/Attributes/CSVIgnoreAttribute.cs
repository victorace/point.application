﻿using System;

namespace Point.Database.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class CSVIgnoreAttribute : Attribute
    {
    }
}
