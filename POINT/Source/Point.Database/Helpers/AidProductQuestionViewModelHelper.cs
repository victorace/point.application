﻿using System;
using System.Collections.Generic;
using System.Linq;
using Point.Database.Models.ViewModels;
using Point.Models.Enums;

namespace Point.Database.Helpers
{
    public static class AidProductQuestionViewModelHelper
    {
        private static bool IsQuestionExempt(int supplierID, List<AidProductQuestionViewModel> questions, AidProductQuestionViewModel question)
        {
			const string suffixOther = "_OTHER";
			const string suffixRemark = "REMARK";
		
            if (string.IsNullOrEmpty(question.QuestionID))
            {
                // TODO: Log error
                return true;
            }

            // This is only for MediPoint (at the moment)
            if (supplierID != (int)SupplierID.MediPoint)
            {
                return false;
            }			
			
			if (question.AidProductQuestionType != AidProductQuestionType.text)
            {
                return false;
            }
			
            if (question.QuestionID.EndsWith(suffixRemark))
            {
				// ending in _REMARK are not mandatory
                return true;
            }			
			
            if (!question.QuestionID.EndsWith(suffixOther))
            {
                return false;
            }

            // Select options (IDs) which were not selected and require additional text/specific answer.
            // [Type=Radio].PossibleAnswers.Item.AnswerID == [Type=Text].QuestionID
            return questions.Any(q => q.AidProductQuestionType == AidProductQuestionType.radio && q.Required && q.PossibleAnswers != null && q.PossibleAnswers.Any(p => p.AnswerID == question.QuestionID && !p.Checked));
        }

        public static bool AreAllQuestionsAnswered(int supplierID, List<AidProductQuestionViewModel> questions)
        {
            return questions.All(q => !q.Required || (!string.IsNullOrEmpty(q.CurrentValue) || (q.PossibleAnswers != null && q.PossibleAnswers.Any(pa => pa.Checked)) || IsQuestionExempt(supplierID, questions, q)));
        }

        public static List<String> GetUnansweredQuestions(int supplierID, List<AidProductQuestionViewModel> questions)
        {
            if (!questions.Any())
            {
                return new List<string>();
            }
            return questions.Where(q => q.Required && string.IsNullOrEmpty(q.CurrentValue) && (q.PossibleAnswers != null && q.PossibleAnswers.All(pa => !pa.Checked)) && !IsQuestionExempt(supplierID, questions, q)).Select(it => it.Question).ToList();
        }


    }

}
