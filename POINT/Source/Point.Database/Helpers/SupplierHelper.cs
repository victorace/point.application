﻿
using Point.Models.Enums;

namespace Point.Database.Helpers
{
    public static class SupplierHelper
    {
        public static bool IsExternalSupplier(int supplierID)
        {
            switch (supplierID)
            {
                case (int)SupplierID.MediPoint:
                    return true;
                case (int)SupplierID.Vegro:
                    return true;
            }

            return false;
        }

    }
}
