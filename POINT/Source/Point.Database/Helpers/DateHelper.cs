﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Point.Database.Helpers
{
    public class DateHelper
    {
        //http://alecpojidaev.wordpress.com/2009/10/29/work-days-calculation-with-c/
        public static int? GetBusinessDays(DateTime? startD, DateTime? endD)
        {
            if(startD == null || endD == null)
            {
                return null;
            }

            var calcBusinessDays = (1 + ((endD.Value - startD.Value).TotalDays * 5 - (startD.Value.DayOfWeek - endD.Value.DayOfWeek) * 2) / 7);

            if (endD.Value.DayOfWeek == DayOfWeek.Saturday) calcBusinessDays--;
            if (startD.Value.DayOfWeek == DayOfWeek.Sunday) calcBusinessDays--;

            return (int)calcBusinessDays;
        }

        public static double? DiffWorkingHours(DateTime? startD, DateTime? endD)
        {
            return DiffWorkingHours(startD, endD, new TimeSpan(8, 30, 0), new TimeSpan(17, 0, 0));
        }

        public static double? DiffWorkingHours(DateTime? startD, DateTime? endD, TimeSpan startOfDay, TimeSpan endOfDay)
        {
            if (startD == null || endD == null)
            {
                return null;
            }

            if(startD.Value.DayOfYear == endD.Value.DayOfYear)
            {
                //Zelfde dag (99.99% zeker)
                //Hoef niet te checken voor volgende dag.
                return (endD.Value - startD.Value).TotalHours;
            }

            var firstdayhours = endOfDay - startD.Value.TimeOfDay;
            var lastdayhours = endD.Value.TimeOfDay - startOfDay;

            var totalhours = firstdayhours.Add(lastdayhours);

            var totaldays = GetBusinessDays(startD, endD);
            if (totaldays.HasValue && totaldays > 0 && endD.Value.TimeOfDay < startD.Value.TimeOfDay)
            {
                totalhours = totalhours.Add(new TimeSpan(totaldays.Value * (endOfDay - startOfDay).Ticks));
            }

            return totalhours.TotalHours;
        }

        public static int? DiffHours(DateTime? startD, DateTime? endD)
        {
            if (startD == null || endD == null)
            {
                return null;
            }

            return (int)(endD.Value - startD.Value).TotalHours;
        }

        public static int? DiffMinutes(DateTime? startD, DateTime? endD)
        {
            if (startD == null || endD == null)
            {
                return null;
            }

            return (int)(endD.Value - startD.Value).TotalMinutes;
        }

        public static int? DiffDays(DateTime? startD, DateTime? endD)
        {
            if (startD == null || endD == null)
            {
                return null;
            }

            return (int)(endD.Value - startD.Value).TotalDays;
        }
    }
}
