﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using Point.Database.Models.ViewModels;

namespace Point.Database.Helpers
{
    public static class PagingHelper<T>
    {
        public static IQueryable<T> GetPage(Pager pager, IQueryable<T> input)
        {
            var pageSize = Math.Max(pager.PageSize, 10);
            var pageIndex = Math.Max(pager.PageNo, 1)-1;

            pager.TotalCount = input.Count();

            pager.PageCount = (int)Math.Ceiling(pager.TotalCount / (decimal)pageSize);

            //pager.TotalCount = pagedResult.Count;       // input.Count();   // count from input not reliable!

            //var items = input.ToList();
            //var afterSkip = items.Skip(pageIndex * pageSize).ToList();
            //var finalItems = afterSkip.Take(pageSize).ToList();

            //var x = input.ToList().Count;
            //if (x > 1000)
            //{
            //    Debugger.Break();
            //}

            //var pagedResult = input.Skip(pageIndex * pageSize).Take(pageSize).ToList();

            //pager.TotalCount = pagedResult.Count + (pagedResult.Any() ? pageIndex * pageSize : 0);



            //pager.TotalCount = (pagedResult.Count + (pagedResult.Any() ? pager.PageCount * pageSize : 0));

            var ret = input.Skip(pageIndex * pageSize).Take(pageSize);

            //return input.Skip(pageIndex * pageSize).Take(pageSize);

            return ret;

            //return pagedResult;
        }
    }
}

/*
  var totalCount = participants.Count;

            viewModel.Pager.PageNo = page;
            viewModel.Pager.PageCount = (int)Math.Ceiling(totalCount / (decimal)_maxPageLen);
            viewModel.Pager.TotalCount = totalCount;
            viewModel.Participants = participants.Skip(_maxPageLen * (page - 1)).Take(_maxPageLen);
 */
