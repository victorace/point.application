﻿using Point.Models.Enums;
using System;
using System.Linq;

namespace Point.Database.CacheModels
{
    public class PhaseDefinitionCache
    {
        public FlowDefinitionID FlowDefinitionID { get; set; }
        public int PhaseDefinitionID { get; set; }
        public string PhaseName { get; set; }
        public string Description { get; set; }
        public decimal Phase { get; set; }
        public int FormTypeID { get; set; }
        public string FormTypeName { get; set; }
        public string FormTypeURL { get; set; }
        public bool BeginFlow { get; set; }
        public bool EndFlow { get; set; }
        public int OrderNumber { get; set; }
        public string CodeGroup { get; set; }
        public string Maincategory { get; set; }
        public string Subcategory { get; set; }
        public string AccessGroup { get; set; }
        public string Executor { get; set; }
        public bool ShowReadOnlyAndEmpty { get; set; }
        public MenuItemType MenuItemType { get; set; }
        public MenuItemType DashboardMenuItemType { get; set; }
        public FlowHandling? FlowHandling { get; set; }

        public string GetControllerName()
        {
            if (String.IsNullOrWhiteSpace(FormTypeURL)) return "";
            string[] components = FormTypeURL.Split('/');

            if (components.Count() >= 1)
                return FormTypeURL.Split('/')[0];
            else
                return "";
        }

        public string GetActionName()
        {
            if (String.IsNullOrWhiteSpace(this.FormTypeURL)) return "";
            string[] components = FormTypeURL.Split('/');

            if (components.Count() >= 2)
                return FormTypeURL.Split('/')[1];
            else
                return "";
        }
    }
}
