﻿using Point.Models.Enums;

namespace Point.Database.CacheModels
{
    public class SendAttachmentCache
    {
        public int SendAttachmentID { get; set; }

        public SendDestinationType SendDestinationType { get; set; }

        public int? OrganizationID { get; set; }

        public AttachmentTypeID AttachmentTypeID { get; set; }

        public bool Allowed { get; set; }

    }
}
