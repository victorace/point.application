﻿using Point.Models.Enums;

namespace Point.Database.CacheModels
{
    public class SendFormTypeCache
    {
        public int SendFormTypeID { get; set; }

        public SendDestinationType SendDestinationType { get; set; }

        public int? OrganizationID { get; set; }

        public FlowFormType FormTypeID { get; set; }

        public bool Allowed { get; set; }
    }
}
