﻿using Point.Database.Attributes;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Models.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Collections.Specialized;
using Point.Models.Interfaces;
using Point.Models.LDM.EOverdracht30.Enums;
using Point.Infrastructure.Extensions;

namespace Point.Database.Extensions
{
    public static class ModelExtensions
    {
        public static bool IsEqual(this Object target, Object source)
        {
            if (target == null && source == null)
            {
                return true;
            }

            if (target?.Equals(source) == true)
            {
                return true;
            }

            return false;
        }

        public static FunctionRoleLevelID ToFunctionRoleLevelID(this Role role)
        {
            FunctionRoleLevelID returnvalue = Enum.TryParse(role.ToString(), out returnvalue) ? returnvalue : FunctionRoleLevelID.None;
            return returnvalue;
        }

        public static void WriteSecureValue<Target, TT, Source, ST>(this Target targetObject, Expression<Func<Target, TT>> targetExpression, Source sourceObject, Expression<Func<Source, ST>> sourceExpression, PointUserInfo pointUserInfo)
        {
            if (targetExpression?.Body is MemberExpression targetMemberExp && sourceExpression?.Body is MemberExpression sourceMemberExp)
            {
                var targetPropertyInfo = (PropertyInfo)targetMemberExp.Member;
                var targetValue = targetPropertyInfo.GetValue(targetObject);

                var sourcePropertyInfo = (PropertyInfo)sourceMemberExp.Member;
                var sourceValue = sourcePropertyInfo.GetValue(sourceObject);

                if (targetValue.IsEqual(sourceValue)) { return; }

                if (!targetObject.CanReadValue(targetExpression, pointUserInfo))
                {
                    return;
                };
                if (!sourceObject.CanReadValue(sourceExpression, pointUserInfo))
                {
                    return;
                };

                if (targetObject.CanWriteValue(targetExpression, pointUserInfo) && 
                    sourceObject.CanWriteValue(sourceExpression, pointUserInfo))
                {
                    targetPropertyInfo.SetValue(targetObject, sourceValue);
                }
                else
                {
                    var displayname = sourcePropertyInfo.GetCustomAttributes<DisplayNameAttribute>().FirstOrDefault();
                    throw new Exception($"Mag nieuwe waarde niet schrijven in {displayname?.DisplayName ?? sourcePropertyInfo.Name}, onvoldoende rechten");
                }
            }
            else
            {
                throw new Exception("propertyExpression is null of incorrect");
            }
        }

        public static bool CanReadValue<Target, TT>(this Target objectinstance, Expression<Func<Target, TT>> targetExpression, PointUserInfo pointUserInfo)
        {
            if (targetExpression?.Body is MemberExpression targetMemberExp)
            {
                var propertyInfo = (PropertyInfo)targetMemberExp.Member;

                var attribute = propertyInfo.GetCustomAttributes<ReadAccessAttribute>().FirstOrDefault();
                if (attribute == null)
                {
                    return true;
                }

                return attribute.HasReadAccess(pointUserInfo);
            }
            else
            {
                throw new Exception("propertyInfo is null");
            }
        }

        public static bool CanWriteValue<Target, TT>(this Target objectinstance, Expression<Func<Target, TT>> targetExpression, PointUserInfo pointUserInfo)
        {
            if (targetExpression?.Body is MemberExpression targetMemberExp)
            {
                var propertyInfo = (PropertyInfo)targetMemberExp.Member;

                var attribute = propertyInfo.GetCustomAttributes<WriteAccessAttribute>().FirstOrDefault();
                if (attribute == null)
                {
                    return true;
                }

                return attribute.HasWriteAccess(pointUserInfo);
            }
            else
            {
                throw new Exception("propertyInfo is null");
            }
        }

        public static string GetDescription(this Type type)
        {
            if (type == null)
            {
                return "";
            }

            DescriptionAttribute[] attributes = (DescriptionAttribute[])type.GetCustomAttributes(typeof(DescriptionAttribute), false);

            if (attributes.Length > 0)
            {
                return attributes[0].Description;
            }

            return type.ToString();
        }

        public static bool HasDefinitiveButton(this PhaseDefinition phaseDefinition)
        {
            return phaseDefinition.PhaseDefinitionNavigation.Any(it => it.ActionTypeID == ActionTypeID.Definitive);
        }

        public static bool IsSender(this FlowInstance flowinstance, PointUserInfo userinfo)
        {
            if (flowinstance == null)
            {
                return false;
            }

            return userinfo.EmployeeOrganizationIDs.Contains(flowinstance.StartedByOrganizationID.GetValueOrDefault());
        }

        public static bool IsReceiver(this FlowInstance flowinstance, PointUserInfo userinfo)
        {
            if (flowinstance == null)
            {
                return false;
            }

            var activestatuses = new InviteStatus[] { InviteStatus.Acknowledged, InviteStatus.Active, InviteStatus.Realized };
            return flowinstance.OrganizationInvite.Any(inv => userinfo.EmployeeOrganizationIDs.Contains(inv.OrganizationID) && activestatuses.Contains(inv.InviteStatus));
        }

        public static string AsQueryString(this NameValueCollection collection)
        {
            return string.Join("&",
            (from string name in collection
             select string.Concat(name, "=", collection[name])).ToArray());
        }

        public static string PrivacyInitials(this Client client)
        {
            string privacyinitials = "";

            //First letter:
            if (!String.IsNullOrEmpty(client.FirstName))
            {
                privacyinitials += client.FirstName.Substring(0, 1).ToUpper() + ".";
            }
            else if(!String.IsNullOrEmpty(client.Initials))
            {
                privacyinitials += client.Initials.Substring(0, 1).ToUpper() + ".";
            }
            else
            {
                privacyinitials += "-.";
            }

            //Second letter:
            if (!String.IsNullOrEmpty(client.LastName))
            {
                privacyinitials += client.LastName.Substring(0, 1).ToUpper() + ".";
            }
            else
            {
                privacyinitials += "-.";
            }

            return privacyinitials;
        }

        public static string FullName(this ClientFullViewModel client)
        {
            if (client == null)
            {
                return "";
            }

            return new NameComponents
            {
                Initials = client.Initials,
                FirstName = client.FirstName,
                MiddleName = client.MiddleName,
                LastName = client.LastName,
                MaidenName = client.MaidenName
            }.FullName();
        }

        public static string FullName(this Client client)
        {
            if (client == null)
            {
                return "";
            }

            return new NameComponents
            {
                Initials = client.Initials,
                FirstName = client.FirstName,
                MiddleName = client.MiddleName,
                LastName = client.LastName,
                MaidenName = client.MaidenName
            }.FullName();
        }

        public static string FullName(this AidProduct aidproduct)
        {
            if (aidproduct == null)
            {
                return "";
            }

            var name = aidproduct.Name;
            string pricetext = "";

            if (aidproduct.PricePerDay > 0)
            {
                pricetext = "€ " + aidproduct.PricePerDay + " per dag";
            }
            if (aidproduct.Price > 0)
            {
                if (pricetext != "")
                {
                    pricetext += " + ";
                }
                pricetext += "€ " + aidproduct.Price + " bij levering";
            }

            if (pricetext != "")
            {
                return String.Format("{0} ({1})",  name, pricetext);
            }
            else
            {
                return name;
            }
        }

        public static string Salutation(this ContactPerson contactperson)
        {
            if (contactperson == null)
            {
                return "";
            }

            return new NameComponents
            {
                Gender = contactperson.PersonData.Gender,
                Initials = contactperson.PersonData.Initials,
                FirstName = contactperson.PersonData.FirstName,
                MiddleName = contactperson.PersonData.MiddleName,
                LastName = contactperson.PersonData.LastName,
                MaidenName = contactperson.PersonData.MaidenName
            }.Salutation();
        }

        public static string FullName(this ContactPerson contactperson)
        {
            if (contactperson == null)
            {
                return "";
            }

            return new NameComponents
            {
                Gender = contactperson.PersonData.Gender,
                Initials = contactperson.PersonData.Initials,
                FirstName = contactperson.PersonData.FirstName,
                MiddleName = contactperson.PersonData.MiddleName,
                LastName = contactperson.PersonData.LastName,
                MaidenName = contactperson.PersonData.MaidenName,
                PhoneNumber = contactperson.PersonData.PhoneNumberGeneral,
                Email = contactperson.PersonData.Email
            }.FullNameWithTelephoneAndEmail();
        }

        public static string Salutation(this Client client)
        {
            if (client == null)
            {
                return "";
            }

            return new NameComponents
            {
                Gender = client.Gender,
                Initials = client.Initials,
                FirstName = client.FirstName,
                MiddleName = client.MiddleName,
                LastName = client.LastName,
                MaidenName = client.MaidenName
            }.Salutation();
        }

        public static string ClientAnonymousName(this Transfer transfer)
        {
            if (transfer == null)
            {
                return "";
            }

            return transfer.TransferID.ToString();
        }

        public static string FullName(this Client client, FlowInstance flowinstance, int? organizationid)
        {
            if (client == null)
            {
                return "";
            }

            if (client.ClientIsAnonymous)
            {
                if (organizationid == null || flowinstance.StartedByOrganizationID != organizationid)
                {
                    return flowinstance.Transfer.ClientAnonymousName();
                }
            }

            return FullName(client);
        }

        public static string EPDRequestGender(this Geslacht geslacht)
        {
            switch (geslacht)
            {
                case Geslacht.OF:
                    return "v";
                case Geslacht.OM:
                    return "m";
                default:
                    return "o";
            }
        }

        public static string FullGender(this Client client)
        {
            if (client?.Gender == null)
            {
                return "";
            }

            return client.Gender.GetDescription();
        }

        public static string FullName(this Employee employee)
        {
            if (employee == null)
            {
                return "";
            }

            return new NameComponents
            {
                Initials = "",
                FirstName = employee.FirstName,
                MiddleName = employee.MiddleName,
                LastName = employee.LastName,
                MaidenName = ""
            }.FullName();
        }

        public static string FullNameWithTelephone(this Employee employee)
        {
            if (employee == null)
            {
                return "";
            }

            return new NameComponents
            {
                Initials = "",
                FirstName = employee.FirstName,
                MiddleName = employee.MiddleName,
                LastName = employee.LastName,
                MaidenName = "",
                PhoneNumber = employee.PhoneNumber ?? employee.DefaultDepartment.PhoneNumber
            }.FullNameWithTelePhone();
        }

        public static bool IsInRole(this Employee employee, string role)
        {
            if (employee?.aspnet_Users?.aspnet_Roles == null)
            {
                return false;
            }

            var roles = employee.aspnet_Users.aspnet_Roles.Select(it => it.LoweredRoleName);
            return roles.Contains(role.ToLower());
        }

        public static bool IsHospital(this Organization organization)
        {
            return organization?.OrganizationTypeID == (int)OrganizationTypeID.Hospital;
        }

        public static bool IsHealthCareProvider(this Organization organization)
        {
            return organization?.OrganizationTypeID == (int)OrganizationTypeID.HealthCareProvider;
        }

        public static bool IsGeneralPracticioner(this Organization organization)
        {
            return organization?.OrganizationTypeID == (int)OrganizationTypeID.GeneralPracticioner; 
        }

        public static bool IsRegionaalCoordinatiepunt(this Organization organization)
        {
            return organization?.OrganizationTypeID == (int)OrganizationTypeID.RegionaalCoordinatiepunt;
        }

        public static bool IsCIZ(this Organization organization)
        {
            return organization?.OrganizationTypeID == (int)OrganizationTypeID.CIZ;
        }

        public static bool IsGRZ(this Organization organization)
        {
            return organization?.OrganizationTypeID == (int)OrganizationTypeID.GRZ;
        }

        public static bool IsEndFlowActive(this IEnumerable<PhaseInstance> phaseInstances)
        {
            return phaseInstances.OrderByDescending(phi => phi.Timestamp).FirstOrDefault().IsEndFlowActive();
        }

        public static bool IsEndFlowActive(this PhaseInstance phaseInstance)
        {
            if (phaseInstance?.PhaseDefinition == null)
            {
                return false;
            }

            if (phaseInstance.PhaseDefinition.EndFlow && phaseInstance.Status == (int)Status.Active)
            {
                return true;
            }

            return false;
        }

        public static string GenerateWebName(this FlowWebField flowWebField)
        {
            if (flowWebField == null)
            {
                throw new ArgumentNullException();
            }

            return string.Format(FlowWebField.NAME_FORMAT, flowWebField.FlowFieldID, flowWebField.FlowFieldAttributeID,
                                flowWebField.Name, (int)flowWebField.Type, flowWebField.SignalOnChange, 
                                (int)flowWebField.Source);
        }

        public static FlowFieldAttributeException FilterSingleByPointUser(this IEnumerable<FlowFieldAttributeException> flowfieldattributes, PointUserInfo userinfo)
        {
            return flowfieldattributes.OrderByDescending(it => it.OrganizationID)
                                      .FirstOrDefault(it => it.RegionID == userinfo.Region.RegionID || it.OrganizationID == userinfo.Organization.OrganizationID);
        }

        public static FlowFieldAttributeException FilterSingleByFlowFieldAttributeID(
            this IEnumerable<FlowFieldAttributeException> flowfieldattributes, int flowfieldattributeid)
        {
            return flowfieldattributes.OrderByDescending(it => it.OrganizationID)
                                      .FirstOrDefault(it => it.FlowFieldAttributeID == flowfieldattributeid);
        }

        public static SearchPropertyType ToSearchPropertyType(this FlowField flowField)
        {
            if (flowField == null)
            {
                throw new ArgumentNullException();
            }

            var searchPropertyType = SearchPropertyType.String;
            switch (flowField.Type)
            {
                case FlowFieldType.None:
                case FlowFieldType.String:
                case FlowFieldType.Time:
                case FlowFieldType.Checkbox:
                    searchPropertyType = SearchPropertyType.String;
                    break;
                case FlowFieldType.Number:
                    searchPropertyType = SearchPropertyType.Numeric;
                    break;
                case FlowFieldType.Date:
                    searchPropertyType = SearchPropertyType.Date;
                    break;
                case FlowFieldType.DateTime:
                    searchPropertyType = SearchPropertyType.DateTime;
                    break;
                case FlowFieldType.RadioButton:
                case FlowFieldType.DropDownList:
                case FlowFieldType.IDText:
                    searchPropertyType = SearchPropertyType.Lookup;
                    break;
            }


            return searchPropertyType;
        }

        public static List<Tuple<string, string, string>> ToActivePhase(this IEnumerable<PhaseInstance> PhaseInstances, FlowInstance flowInstance, bool isClosed = false)
        {
            var active = new List<Tuple<string, string, string>>();
            var isinterrupted = flowInstance.Interrupted;

            foreach (var pi in PhaseInstances)
            {
                if (!pi.Cancelled)
                {
                    if (isClosed)
                    {
                        //Dont add any
                    }
                    else if (!isinterrupted)
                    {
                        if (pi.PhaseDefinition != null && pi.PhaseDefinition.Phase >= 0)
                        {
                            if (pi.Status == (int)Status.Active)
                            {
                                var entry = new Tuple<string, string, string>(pi.PhaseDefinition.Phase.ToString(), pi.PhaseDefinition.PhaseName, pi.PhaseDefinition.Description);
                                if (!active.Contains(entry))
                                {
                                    active.Insert(0, entry);
                                }
                            }
                        }
                    }
                }
            }

            if (isClosed)
            {
                active.Insert(0, new Tuple<string, string, string>("-1", "Afgesloten", ""));
            }
            else if (isinterrupted)
            {
                active.Insert(0, new Tuple<string, string, string>("-1", $"TO ({ToPointDateDisplay(flowInstance.InterruptedDate)})", ""));
            }

            return active;
        }

        public static FlowInstanceStatus GetInterruptionStatus(this IEnumerable<FlowInstanceStatus> statuses)
        {
            var laststatus = statuses?.LastOrDefault();

            return (laststatus?.FlowInstanceStatusTypeID == FlowInstanceStatusTypeID.Onderbreken) ? laststatus : null;
        }

        public static string GetActionName(this ILogAction logaction)
        {
            string action = logaction?.Action;
            if (action == "I")
            {
                return "Aanmaken";
            }
            else if (action == "U")
            {
                return "Bewerken";
            }
            else if (action == "D")
            {
                return "Verwijderen";
            }
            else
            {
                return "Onbekend";
            }
        }

        public static bool HasValues(this IEnumerable<FlowWebField> flowWebFields)
        {
            if (flowWebFields == null || flowWebFields.All(fwf => string.IsNullOrWhiteSpace(fwf.Value)))
            {
                return false;
            }
            return true;
        }

        public static string GetValueOrDefault(this IEnumerable<FlowFieldValue> flowfields, int flowfieldid, string defaultvalue)
        {
            var value = defaultvalue;

            if (flowfields == null)
            {
                return value;
            }
            var flowwebfield = flowfields.FirstOrDefault(fwf => fwf.FlowFieldID == flowfieldid);
            if (flowwebfield != null)
            {
                value = flowwebfield.Value;
            }

            return value;
        }

        public static string GetValueOrDefault(this IEnumerable<FlowFieldValue> flowfields, string name, string defaultvalue)
        {
            var value = defaultvalue;

            if (flowfields == null)
            {
                return value;
            }
            var flowwebfield = flowfields.FirstOrDefault(fwf => fwf.FlowField.Name == name);
            if (flowwebfield != null)
            {
                value = flowwebfield.Value;
            }

            return value;
        }

        public static FlowWebField GetByName(this IEnumerable<FlowWebField> flowwebfields, string name)
        {
            return flowwebfields.FirstOrDefault(fwf => fwf.Name == name);
        }

        public static string GetValueOrDefault(this IEnumerable<FlowWebField> flowwebfields, string name, string defaultvalue)
        {
            var value = defaultvalue;

            if (flowwebfields == null)
            {
                return value;
            }
            var flowwebfield = flowwebfields.FirstOrDefault(fwf => fwf.Name == name);
            if (flowwebfield != null)
            {
                value = flowwebfield.Value;
            }

            return value;
        }

        public static bool IsVisible(this IEnumerable<FlowWebField> flowwebfields, string name)
        {
            if (flowwebfields == null)
            {
                return false;
            }

            return flowwebfields.FirstOrDefault(fwf => fwf.Name == name)?.Visible == true;
        }

        public static string GetValueOrDefault(this FlowWebField flowwebfield, string defaultvalue)
        {
            if (flowwebfield == null) return defaultvalue;
            if (string.IsNullOrWhiteSpace(flowwebfield.Value)) return defaultvalue;
            return flowwebfield.Value;
        }

        public static string GetValueOrDefault(this FlowFieldValue flowfieldvalue, string defaultvalue)
        {
            if (flowfieldvalue == null) return defaultvalue;
            if (string.IsNullOrWhiteSpace(flowfieldvalue.Value)) return defaultvalue;
            return flowfieldvalue.Value;
        }

        [Obsolete("Use FlowWebFieldBL.SetValue.")]
        public static void SetValue(this IEnumerable<FlowWebField> flowwebfields, string name, string value)
        {
            if (flowwebfields == null) return;

            var flowwebfield = flowwebfields.FirstOrDefault(fwf => fwf.Name == name);
            if (flowwebfield == null)
                return;

            flowwebfield.DisplayValue = flowwebfield.Value = value;
        }

       public static string GetFlowFieldValue(this IEnumerable<IFlowFieldValue> flowFieldValues, int flowfieldid)
        {
            return flowFieldValues.FirstOrDefault(ffv => ffv.FlowFieldID == flowfieldid && !string.IsNullOrEmpty(ffv.Value))?.Value;
        }

        public static string GetFlowFieldDisplayValue(this IEnumerable<FlowWebField> flowFieldValues, int flowfieldid)
        {
            return flowFieldValues.FirstOrDefault(ffv => ffv.FlowFieldID == flowfieldid && !string.IsNullOrEmpty(ffv.Value))?.DisplayValue;
        }

        public static DateTime? GetFlowFieldDateTimeValue(this IEnumerable<FlowWebField> flowFieldValues, int flowfieldid)
        {
            var value = flowFieldValues.GetFlowFieldValue(flowfieldid);
            if (DateTime.TryParse(value, out var result))
            {
                return result;
            }

            return null;
        }

        public static List<Option> AsClone(this List<Option> source)
        {
            return source?.Select(src => new Option
            {
                Selected = src.Selected,
                Text = src.Text,
                Value = src.Value
            }).ToList();
        }

        public static IList<Option> AsClone(this IList<Option> source)
        {
            return source?.Select(src => new Option
            {
                Selected = src.Selected,
                Text = src.Text,
                Value = src.Value
            }).ToList();
        }

        public static List<DateTime> GetPeriodDates(this Frequency frequency)
        {
            var dates = new List<DateTime>();

            if (frequency == null)
            {
                return dates;
            }

            if (frequency.Type == FrequencyType.CVA)
            {
                // Product Backlog Item 13338 - A CVA Registratie 3 mnd doorlopend dossier has an end date of Today + 5 months
                //dates.Add(frequency.EndDate);
            }
            else
            {
                var quantity = frequency.Quantity;

                // TODO: This is a quickfix to cap the days / quantity of a frequency period.
                // Organizations in production are setting the enddate to 2048+ with a 
                // quantity of 1000000000, causing an OutOfRangeExceptoin in DateTime.AddDays.
                var daysintheyear = DateTime.IsLeapYear(frequency.StartDate.Year) || DateTime.IsLeapYear(frequency.EndDate.Year) ? 366 : 365;
                var totaldays = Math.Abs((frequency.EndDate - frequency.StartDate).TotalDays + 1);
                if (totaldays > daysintheyear) { totaldays = daysintheyear; }
                // everyday 
                if (quantity > daysintheyear) { quantity = 1; }

                if (quantity > 0)
                {
                    var start = frequency.StartDate;
                    while (totaldays > 0)
                    {
                        start = start.AddDays(quantity);
                        if (start > frequency.EndDate)
                        {
                            break;
                        }
                        dates.Add(start.Date);
                        totaldays--;
                    }
                }
            }

            return dates;
        }

        public static FlowFormType GetFlowFormType(this Frequency frequency)
        {
            var formtype = FlowFormType.NoScreen;

            if (frequency == null) return formtype;

            switch (frequency.Type)
            {
                case FrequencyType.General:
                    formtype = FlowFormType.DoorlopendDossier;
                    break;
                case FrequencyType.MSVT:
                    formtype = FlowFormType.MSVTIndDoorldossier;
                    break;
                case FrequencyType.CVA:
                    formtype = FlowFormType.InvullenCVARegistratie3mnd;
                    break;
                default:
                    formtype = FlowFormType.NoScreen;
                    break;
            }

            return formtype;
        }

        public static bool IsResendAvailable(this PhaseInstance phaseinstance)
        {
            return phaseinstance?.PhaseDefinition?.CanResend == true && phaseinstance.Status == (int)Status.Done;
        }

        public static DateTime? GetProcessDateTime(this PhaseInstance phaseinstance)
        {
            if (phaseinstance == null) return null;

            DateTime? processdatetime = phaseinstance.RealizedDate;
            if (processdatetime == null)
            {
                processdatetime = phaseinstance.StartProcessDate;
            }
            return processdatetime;
        }

        public static string CombinedName(this CopyTransferFormViewModel copytransferform)
        {
            if (copytransferform == null) return string.Empty;

            var combinedname = copytransferform.Name;

            if (!string.IsNullOrWhiteSpace(copytransferform.Description))
            {
                combinedname = $"{combinedname} ({copytransferform.Description})";
            }

            return combinedname;
        }

        public static bool IsNullOrEmpty(this FlowFieldValue flowfieldvalue)
        {
            if (flowfieldvalue == null)
            {
                return true;
            }

            if (string.IsNullOrWhiteSpace(flowfieldvalue.Value))
            {
                return true;
            }

            return false;
        }

        public static bool IsDone(this PhaseInstance phaseinstance)
        {
            return phaseinstance != null && phaseinstance.Status == (int)Status.Done;
        }

        //Note: These extensions are duplicated from Point.Infrastructure.
        public static string ToPointDateDisplay(DateTime dt)
        {
            return ToPointDateDisplay(((DateTime?)dt));
        }

        public static string ToPointDateDisplay(DateTime? dt)
        {
            if (IsOutOfScope(dt))
            {
                return "";
            }
            return ToStringEx(dt, "dd-MM-yyyy");
        }

        public static bool IsOutOfScope(DateTime? dt)
        {
            if (dt == null || !dt.HasValue)
            {
                return true;
            }

            var dtscope = dt.GetValueOrDefault();
            return (dtscope.Year < 1900 || dtscope.Year > 2100);
        }

        public static string ToPointDateHourDisplay(DateTime dt)
        {
            return ToPointDateHourDisplay((DateTime?)dt);
        }

        public static string ToPointDateHourDisplay(DateTime? dt)
        {
            if (IsOutOfScope(dt))
            {
                return "";
            }
            return ToStringEx(dt, "d-M-yyyy HH:mm");
        }

        public static string ToStringEx(DateTime? dt, string format)
        {
            if (dt == null || !dt.HasValue)
            {
                return "";
            }

            return dt.Value.ToString(format);
        }

        public static DateTime UTCToLocalTime(this Aspnet_Membership aspmembership)
        {
            return aspmembership.LastLoginDate.ToLocalTime();
        }
    }

    public class NameComponents
    {
        public Geslacht? Gender { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string MaidenName { get; set; }
        public string Initials { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Salutation()
        {
            var salutation = "";

            if (Gender == Geslacht.OM)
            {
                salutation += "Dhr ";
            }
            else if (Gender == Geslacht.OF)
            {
                salutation += "Mevr ";
            }
            else
            {
                salutation += "Dhr/mevr ";
            }

            return salutation;
        }
        public string FullName()
        {
            var fullname = "";

            if (!string.IsNullOrWhiteSpace(LastName))
            {
                fullname += UppercaseFirst(LastName);
            }

            if (fullname != "")
            {
                fullname += ", ";
            }
            if (Initials != null || FirstName != null)
            {
                fullname += ((Initials ?? "").ToUpper() + " " + UppercaseFirst(FirstName ?? "")).Trim() + ", ";
            }

            return fullname.Trim(',', ' ');
        }
        public string FullNameWithTelePhone()
        {
            var fullname = FullName();
            if (String.IsNullOrEmpty(PhoneNumber))
            {
                return fullname;
            }

            return $"{fullname} (Tel: {PhoneNumber})";
        }
        public string FullNameWithTelephoneAndEmail()
        {
            var fullNameWithTel = FullNameWithTelePhone();
            if (string.IsNullOrEmpty(Email))
            {
                return fullNameWithTel;
            }
            return $"{fullNameWithTel} (Email: {Email})";
        }
        private static string UppercaseFirst(string s)
        {
            if (String.IsNullOrEmpty(s))
            {
                return "";
            }

            char[] a = s.ToCharArray();
            a[0] = char.ToUpper(a[0]);
            return new string(a);
        }
    }
}