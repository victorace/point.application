﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace Point.Database.Repository
{
    public class GenericRepository<TEntity> : IRepository<TEntity> where TEntity : class, new()
    {
        internal DbContext context;
        internal DbSet<TEntity> dbSet;

        public enum Multiplicity
        {
            ZeroOne,
            ZeroMany
        }

        public GenericRepository(DbContext context)
        {
            this.context = context;
            this.dbSet = context.Set<TEntity>();
        }

        public virtual IQueryable<TEntity> GetAll()
        {
            if (!context.Configuration.ProxyCreationEnabled)
            {
                var props = GetNavigationalProperties(Multiplicity.ZeroOne);

                string includes = string.Join(",", props);

                return Get(x => true, null, includes);
            }

            return dbSet;
        }

        public virtual IQueryable<TEntity> AsNoTracking()
        {
            return dbSet.AsNoTracking<TEntity>();
        }

        public virtual bool Any(Expression<Func<TEntity, bool>> where)
        {
            return dbSet.Any(where);
        }

        public virtual bool Exists(Expression<Func<TEntity, bool>> where = null)
        {
            IQueryable<TEntity> query = dbSet;
            if (where != null)
            {
                query = query.Where(where);
            }
            return query.Any();
        }

        public virtual TEntity FirstOrDefault(Expression<Func<TEntity, bool>> where,
            string includeProperties = null)
        {
            IQueryable<TEntity> query = dbSet;

            var includes = new List<string>(includeProperties.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries));
            foreach (var includeProperty in includes)
            {
                query = query.Include(includeProperty);
            }

            return query.FirstOrDefault(where);
        }

        public virtual IQueryable<TEntity> Get(
            Expression<Func<TEntity, bool>> where = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = null,
            bool asNoTracking = false)
        {
            IQueryable<TEntity> query = dbSet;

            if (asNoTracking)
            {
                query = query.AsNoTracking<TEntity>();
            }

            if (where != null)
            {
                query = query.Where(where);
            }

            var includes = new List<string>(includeProperties.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries));

            if (!context.Configuration.ProxyCreationEnabled)
            {
                var props = GetNavigationalProperties(Multiplicity.ZeroOne);

                foreach (var prop in props)
                {
                    if (!includes.Contains(prop))
                    {
                        includes.Add(prop);
                    }
                }
            }

            foreach (var includeProperty in includes)
            {
                query = query.Include(includeProperty);
            }

            if (orderBy != null)
            {
                return orderBy(query);
            }
            else
            {
                return query;
            }
        }

        private IList<string> GetNavigationalProperties(Multiplicity multiplicity)
        {
            Type entityType = typeof(TEntity);
            var elementType = ((System.Data.Entity.Infrastructure.IObjectContextAdapter)context).ObjectContext.CreateObjectSet<TEntity>().EntitySet.ElementType;
            var res = elementType.NavigationProperties.Where(np => 
                                                            {
                                                                if (multiplicity == Multiplicity.ZeroOne)
                                                                {
                                                                    return np.ToEndMember.RelationshipMultiplicity != System.Data.Entity.Core.Metadata.Edm.RelationshipMultiplicity.Many;
                                                                }
                                                                else if (multiplicity == Multiplicity.ZeroMany)
                                                                {
                                                                    return np.ToEndMember.RelationshipMultiplicity == System.Data.Entity.Core.Metadata.Edm.RelationshipMultiplicity.Many;
                                                                }
                                                                else
                                                                {
                                                                    return false;
                                                                }
                                                            })
                                                        .Select(np => entityType.GetProperty(np.Name).Name).ToList();
            return res;
        }

        public virtual TEntity GetByID(object id)
        {
            return dbSet.Find(id);
        }

        public virtual TEntity GetByID(params object[] ids)
        {
            return dbSet.Find(ids);
        }

        public virtual void Insert(TEntity entity)
        {
            dbSet.Add(entity);
        }

        public virtual TEntity Attach(TEntity entity)
        {
            return dbSet.Attach(entity);
        }

        public virtual void Delete(object id)
        {
            TEntity entityToDelete = dbSet.Find(id);
            Delete(entityToDelete);
        }

        public virtual void Delete(Expression<Func<TEntity, bool>> where)
        {
            if(typeof(TEntity).GetInterfaces().Contains(typeof(IInActive)))
            {
                var entities = dbSet.Where(where).ToList().Cast<IInActive>().ToList();
                entities.ForEach(e => e.InActive = true);
            } else
            {
                dbSet.RemoveRange(dbSet.Where(where));
            }
        }

        public virtual void Delete(TEntity entityToDelete)
        {
            if (entityToDelete is IInActive inActiveEntity)
            {
                inActiveEntity.InActive = true;
            } else
            {
                dbSet.Remove(entityToDelete);
            }
        }
        
        public virtual void Reload(TEntity entityToReload)
        {
            context.Entry(entityToReload).Reload();
        }

        public virtual bool HasChanges(TEntity entitytocheck)
        {
            return context.Entry(entitytocheck).State != EntityState.Unchanged;
        }

        public virtual int Count()
        {
            return dbSet.Count();
        }

        public virtual int Count(Expression<Func<TEntity, bool>> where)
        {
            return dbSet.Count(where);
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }

            disposed = true;
        }

        public virtual void Dispose()
        {
            Dispose(true);
        }
    }
}