﻿using Point.Database.Models;
using System;
using System.Linq;
using System.Linq.Expressions;

// ADDING A REPOSITORY? THEN PLEASE KEEP THE ORDER CORRECT (ALPHABETICALLY) - Thank you.
// ADDING A REPOSITORY? THEN PLEASE KEEP THE ORDER CORRECT (ALPHABETICALLY) - Thank you.
// ADDING A REPOSITORY? THEN PLEASE KEEP THE ORDER CORRECT (ALPHABETICALLY) - Thank you.
// ADDING A REPOSITORY? THEN PLEASE KEEP THE ORDER CORRECT (ALPHABETICALLY) - Thank you.

namespace Point.Database.Repository
{
    public interface ILogAction
    {
        string Action { get; }
    }

    public interface IInActive
    {
        bool InActive { get; set; }
    }

    public interface ILogEntry
    {
        int? EmployeeID { get; set; }
        string ScreenName { get; set; }
        DateTime? Timestamp { get; set; }
    }

    //Used for the existing POINT-entities
    public interface ILogEntryWithID
    {
        int? EmployeeID { get; set; }
        int? ScreenID { get; set; }
        DateTime? TimeStamp { get; set; }
    }

    public interface IMutation
    {
        Type MutationEntity { get; }
    }

    public interface IActionCodeHealthInsurerFormTypeRepository : IRepository<ActionCodeHealthInsurerFormType> { }

    public interface IActionCodeHealthInsurerListRepository : IRepository<ActionCodeHealthInsurerList> { }

    public interface IActionCodeHealthInsurerRegioRepository : IRepository<ActionCodeHealthInsurerRegio> { }

    public interface IActionCodeHealthInsurerRepository : IRepository<ActionCodeHealthInsurer> { }

    public interface IActionGroupHealthInsurerRepository : IRepository<ActionGroupHealthInsurer> { }

    public interface IActionHealthInsurerRepository : IRepository<ActionHealthInsurer> { }

    public interface IAfterCareCategoryRepository : IRepository<AfterCareCategory> { }

    public interface IAfterCareFinancingRepository : IRepository<AfterCareFinancing> { }

    public interface IAfterCareGroupRepository : IRepository<AfterCareGroup> { }

    public interface IAfterCareTileGroupRepository : IRepository<AfterCareTileGroup> { }

    public interface IAfterCareTypeRepository : IRepository<AfterCareType> { }

    public interface IAidProductGroupRepository : IRepository<AidProductGroup> { }

    public interface IAidProductOrderAnswerRepository : IRepository<AidProductOrderAnswer> { }

    public interface IAidProductOrderItemRepository : IRepository<AidProductOrderItem> { }

    public interface IAidProductRepository : IRepository<AidProduct> { }

    public interface IAspnet_ApplicationsRepository : IRepository<Aspnet_Applications> { }

    public interface IAspnet_MembershipRepository : IRepository<Aspnet_Membership> { }

    public interface IAspnet_RolesRepository : IRepository<Aspnet_Roles> { }

    public interface IAspnet_SchemaVersionsRepository : IRepository<Aspnet_SchemaVersions> { }

    public interface IAspnet_UsersRepository : IRepository<Aspnet_Users> { }

    public interface IAttachmentReceivedTempRepository : IRepository<AttachmentReceivedTemp> { }

    public interface IEndpointConfigurationRepository : IRepository<EndpointConfiguration> { }

    public interface ICIZRelationRepository : IRepository<CIZRelation> { }

    public interface IClientReceivedTempHL7Repository : IRepository<ClientReceivedTempHL7> { }

    public interface IClientReceivedTempRepository : IRepository<ClientReceivedTemp> { }

    public interface IClientRepository : IRepository<Client> { }

    public interface ICommunicationLogRepository : IRepository<CommunicationLog> { }

    public interface ICommunicationQueueRepository : IRepository<CommunicationQueue> { }

    public interface IContactPersonRepository : IRepository<ContactPerson> { }

    public interface ICryptoCertificateRepository : IRepository<CryptoCertificate> { }

    public interface IDepartmentCapacityRepository : IRepository<DepartmentCapacity> { }

    public interface IDepartmentCapacityPublicRepository : IRepository<DepartmentCapacityPublic> { }

    public interface IDepartmentRepository : IRepository<Department> { }

    public interface IDepartmentTypeRepository : IRepository<DepartmentType> { }

    public interface IDigitalSignatureRepository : IRepository<DigitalSignature> { }    

    public interface IDoctorRepository : IRepository<Doctor> { }

    public interface IEmployeeRepository : IRepository<Employee> { }

    public interface IEmployeeDepartmentRepository : IRepository<EmployeeDepartment> { }

    public interface IFieldReceivedTempRepository : IRepository<FieldReceivedTemp> { }

    public interface IFieldReceivedValueMappingRepository : IRepository<FieldReceivedValueMapping> { }

    public interface IFlowDefinitionParticipationRepository : IRepository<FlowDefinitionParticipation> { }

    public interface IFlowDefinitionRepository : IRepository<FlowDefinition> { }

    public interface IFlowFieldAttributeExceptionRepository : IRepository<FlowFieldAttributeException> { }

    public interface IFlowFieldAttributeRepository : IRepository<FlowFieldAttribute> { }

    public interface IFlowFieldDashboardRepository : IRepository<FlowFieldDashboard> { }

    public interface IFlowFieldRepository : IRepository<FlowField> { }

    public interface IFlowFieldDataSourceRepository : IRepository<FlowFieldDataSource> { }

    public interface IFlowFieldValueRepository : IRepository<FlowFieldValue> { }

    public interface IFlowInstanceDoorlopendDossierSearchValuesRepository : IRepository<FlowInstanceDoorlopendDossierSearchValues> { }

    public interface IFlowInstanceRepository : IRepository<FlowInstance> { }
    public interface IFlowDefinitionFormTypeFlowFieldRepository : IRepository<FlowDefinitionFormTypeFlowField> { }

    public interface IFlowInstanceOrganizationRepository : IRepository<FlowInstanceOrganization> { }

    public interface IFlowInstanceReportValuesProjectsRepository : IRepository<FlowInstanceReportValuesProjects> { }

    public interface IIguanaChannelRepository : IRepository<IguanaChannel> { }

    public interface IFlowInstanceReportValuesAttachmentDetailRepository : IRepository<FlowInstanceReportValuesAttachmentDetail> { }

    public interface IFlowInstanceReportValuesRepository : IRepository<FlowInstanceReportValues> { }

    public interface IFlowInstanceReportValuesDumpRepository : IRepository<FlowInstanceReportValuesDump> { }

    public interface IFlowInstanceSearchValuesRepository : IRepository<FlowInstanceSearchValues> { }

    public interface IFlowInstanceStatusRepository : IRepository<FlowInstanceStatus> { }
    public interface IFlowInstanceTagRepository : IRepository<FlowInstanceTag> { }

    public interface IFlowPhaseAttributeRepository : IRepository<FlowPhaseAttribute> { }

    public interface IFlowTypeRepository : IRepository<FlowType> { }

    public interface IFormSetVersionRepository : IRepository<FormSetVersion> { }

    public interface IFormTypeOrganizationRepository : IRepository<FormTypeOrganization> { }
   
    public interface IFormTypeRegionRepository : IRepository<FormTypeRegion> { }
    public interface IFormTypeSelectionRepository : IRepository<FormTypeSelection> { }
    public interface IFormTypeSelectionOrganizationRepository : IRepository<FormTypeSelectionOrganization> { }
    public interface IFlowDefinitionFormTypeRepository : IRepository<FlowDefinitionFormType> { }
    public interface IFormTypeRepository : IRepository<FormType> { }

    public interface IFrequencyRepository : IRepository<Frequency> { }

    public interface IFrequencyTransferAttachmentRepository : IRepository<FrequencyTransferAttachment> { }

    public interface IFrequencyTransferMemoRepository : IRepository<FrequencyTransferMemo> { }

    public interface IFunctionRoleRepository : IRepository<FunctionRole> { }

    public interface IGeneralActionPhaseRepository : IRepository<GeneralActionPhase> { }

    public interface IGeneralActionRepository : IRepository<GeneralAction> { }

    public interface IGeneralActionTypeRepository : IRepository<GeneralActionType> { }

    public interface IGPSCoordinaatRepository : IRepository<GPSCoordinaat> { }

    public interface IGPSGemeenteRepository : IRepository<GPSGemeente> { }

    public interface IGPSPlaatsRepository : IRepository<GPSPlaats> { }

    public interface IGPSProvincieRepository : IRepository<GPSProvincie> { }

    public interface IHealthAidDeviceRepository : IRepository<HealthAidDevice> { }

    public interface IHealthInsurerRepository : IRepository<HealthInsurer> { }

    public interface IHtmlTextRepository : IRepository<HtmlText> { }

    public interface ILocationRepository : IRepository<Location> { }

    public interface ILocationRegionRepository : IRepository<LocationRegion> { }

    public interface ILogCreateUserRepository : IRepository<LogCreateUser> { }

    public interface ILogDepartmentRepository : IRepository<LogDepartment> { }

    public interface ILoggingRepository : IRepository<Logging> { }

    public interface ILoginHistoryRepository : IRepository<LoginHistory> { }

    public interface ILogLocationRepository : IRepository<LogLocation> { }

    //public interface IOrganizationSearchParticipationRepository : IRepository<OrganizationSearchParticipation> { }
    public interface ILogOrganizationRepository : IRepository<LogOrganization> { }

    public interface ILogPointFormServiceRepository : IRepository<LogPointFormService> { }

    public interface ILogPrivacyRepository : IRepository<LogPrivacy> { }

    public interface ILogReadRepository : IRepository<LogRead> { }

    public interface IMedicineCardRepository : IRepository<MedicineCard> { }

    public interface IMedicineRepository : IRepository<Medicine> { }

    public interface IMessageReceivedTempRepository : IRepository<MessageReceivedTemp> { }

    public interface IMutActionHealthInsurerRepository : IRepository<MutActionHealthInsurer> { }

    public interface IMutClientRepository : IRepository<MutClient> { }

    public interface IMutDepartmentRepository : IRepository<MutDepartment> { }

    public interface IMutEmployeeRepository : IRepository<MutEmployee> { }

    public interface IMutFlowFieldValueRepository : IRepository<MutFlowFieldValue> { }

    public interface IMutFormSetVersionRepository : IRepository<MutFormSetVersion> { }

    public interface IMutLocationRepository : IRepository<MutLocation> { }

    public interface IMutMedicineCardRepository : IRepository<MutMedicineCard> { }

    public interface IMutOrganizationRepository : IRepository<MutOrganization> { }

    public interface IMutTransferTaskRepository : IRepository<MutTransferTask> { }

    public interface INationalityRepository : IRepository<Nationality> { }

    public interface IOrganizationInviteRepository : IRepository<OrganizationInvite> { }

    public interface IOrganizationCSVTemplateRepository : IRepository<OrganizationCSVTemplate> { }

    public interface IOrganizationCSVTemplateColumnRepository : IRepository<OrganizationCSVTemplateColumn> { }
    
    public interface IOrganizationLogoRepository : IRepository<OrganizationLogo> { }

    public interface IOrganizationOutsidePointRepository : IRepository<OrganizationOutsidePoint> { }

    public interface IOrganizationProjectRepository : IRepository<OrganizationProject> { }

    public interface IOrganizationRepository : IRepository<Organization> { }

    public interface IOrganizationSearchParticipationRepository : IRepository<OrganizationSearchParticipation> { }

    public interface IOrganizationSearchPostalCodeRepository : IRepository<OrganizationSearchPostalCode> { }

    public interface IOrganizationSearchRegionRepository : IRepository<OrganizationSearchRegion> { }

    public interface IOrganizationSearchRepository : IRepository<OrganizationSearch> { }

    public interface IOrganizationSettingRepository : IRepository<OrganizationSetting> { }

    public interface IOrganizationTypeRepository : IRepository<OrganizationType> { }

    public interface IPageLockRepository : IRepository<PageLock> { }

    public interface IPatientReceivedTempRepository : IRepository<PatientReceivedTemp> { }

    public interface IPatientRequestTempRepository : IRepository<PatientRequestTemp> { }

    public interface IPersonDataRepository : IRepository<PersonData> { }

    public interface IPhaseDefinitionRepository : IRepository<PhaseDefinition> { }

    public interface IPhaseDefinitionNavigationRepository : IRepository<PhaseDefinitionNavigation> { }

    public interface IPhaseDefinitionRightsRepository : IRepository<PhaseDefinitionRights> { }

    public interface IPhaseInstanceRepository : IRepository<PhaseInstance> { }

    public interface IPostalCodeRepository : IRepository<PostalCode> { }

    public interface IPreferredLocationRepository : IRepository<PreferredLocation> { }

    public interface IRazorTemplateRepository : IRepository<RazorTemplate> { }

    public interface IRegionRepository : IRepository<Region> { }

    public interface IRegionAfterCareTypeRepository : IRepository<RegionAfterCareType> { }

    public interface IReportDefinitionRepository : IRepository<ReportDefinition> { }

    public interface IScheduleHistoryRepository : IRepository<ScheduleHistory> { }

    public interface IScreenRepository : IRepository<Screen> { }

    public interface ISearchUIConfigurationRepository : IRepository<SearchUIConfiguration> { }

    public interface ISearchUIFieldConfigurationRepository : IRepository<SearchUIFieldConfiguration> { }

    public interface ISendAttachmentRepository : IRepository<SendAttachment> { }

    public interface ISendFormTypeRepository : IRepository<SendFormType> { }

    public interface IServiceAreaPostalCodeRepository : IRepository<ServiceAreaPostalCode> { }

    public interface IServiceAreaRepository : IRepository<ServiceArea> { }

    public interface IServiceBusLogRepository : IRepository<ServiceBusLog> { }

    public interface IAutoCreateSetRepository : IRepository<AutoCreateSet> { }
    public interface IAutoCreateSetDepartmentRepository : IRepository<AutoCreateSetDepartment> { }

    public interface ISignaleringDestinationRepository : IRepository<SignaleringDestination> { }

    public interface ISignaleringRepository : IRepository<Signalering> { }

    public interface ISignaleringTargetRepository : IRepository<SignaleringTarget> { }

    public interface ISignaleringTriggerRepository : IRepository<SignaleringTrigger> { }

    public interface ISignedPhaseInstanceRepository : IRepository<SignedPhaseInstance> { }

    public interface ISpecialismRepository : IRepository<Specialism> { }

    public interface ISupplierRepository : IRepository<Supplier> { }

    public interface ISystemMessageReadRepository : IRepository<SystemMessageRead> { }

    public interface ISystemMessageRepository : IRepository<SystemMessage> { }

    public interface ITemplateDefaultRepository : IRepository<TemplateDefault> { }

    public interface ITemplateRepository : IRepository<Template> { }

    public interface ITemplateTypeRepository : IRepository<TemplateType> { }

    public interface ITempletRepository : IRepository<Templet> { }

    public interface ITransferAttachmentRepository : IRepository<TransferAttachment> { }

    public interface ITransferMemoChangesRepository : IRepository<TransferMemoChanges> { }

    public interface ITransferMemoRepository : IRepository<TransferMemo> { }

    public interface ITransferMemoTemplateRepository : IRepository<TransferMemoTemplate> { }

    public interface ITransferRepository : IRepository<Transfer> { }

    public interface ITransferTaskRepository : IRepository<TransferTask> { }

    public interface IUsedHashCodesRepository : IRepository<UsedHashCodes> { }

    public interface IUsedMFACodeRepository : IRepository<UsedMFACode> { }

    public interface IValidationRepository : IRepository<Validation> { }

    public interface IViewTransferHistory : IRepository<ViewTransferHistory> { }

    public interface IWebRequestLogRepository : IRepository<WebRequestLog> { }

    public interface IZorgAdviesItemGroupRepository : IRepository<ZorgAdviesItemGroup> { }

    public interface IZorgAdviesItemRepository : IRepository<ZorgAdviesItem> { }

    public interface IZorgAdviesItemValueRepository : IRepository<ZorgAdviesItemValue> { }

    public interface IRepository<T> : IDisposable where T : class, new()
    {
        bool Any(Expression<Func<T, bool>> where);

        T Attach(T entity);

        int Count();

        int Count(Expression<Func<T, bool>> where);

        void Delete(object id);

        void Delete(T entityToDelete);

        void Delete(Expression<Func<T, bool>> where);

        bool Exists(Expression<Func<T, bool>> where = null);

        T FirstOrDefault(Expression<Func<T, bool>> where, string includeProperties = "");

        IQueryable<T> Get(Expression<Func<T, bool>> where = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, string includeProperties = "", bool asNoTracking = false);

        IQueryable<T> GetAll();

        IQueryable<T> AsNoTracking();

        T GetByID(object id);

        T GetByID(params object[] ids);

        bool HasChanges(T entitytocheck);

        void Insert(T entity);

        void Reload(T entityToReload);
    }
}