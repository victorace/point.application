﻿using Point.Database.Models;
using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;

/// <summary>
/// ADDING A REPOSITORY? THEN PLEASE KEEP THE ORDER CORRECT (ALPHABETICALLY) - Thank you.
/// ADDING A REPOSITORY? THEN PLEASE KEEP THE ORDER CORRECT (ALPHABETICALLY) - Thank you.
/// ADDING A REPOSITORY? THEN PLEASE KEEP THE ORDER CORRECT (ALPHABETICALLY) - Thank you.
/// ADDING A REPOSITORY? THEN PLEASE KEEP THE ORDER CORRECT (ALPHABETICALLY) - Thank you.
/// </summary>

namespace Point.Database.Repository
{
    public interface IUnitOfWork
    {
        DbContextConfiguration DatabaseConfiguration { get; }
        DbContext DatabaseContext { get; }
        void Dispose();
        void Save();
        void Save(LogEntry logentry);

        IActionCodeHealthInsurerFormTypeRepository ActionCodeHealthInsurerFormTypeRepository { get; }
        IActionCodeHealthInsurerListRepository ActionCodeHealthInsurerListRepository { get; }
        IActionCodeHealthInsurerRegioRepository ActionCodeHealthInsurerRegioRepository { get; }
        IActionCodeHealthInsurerRepository ActionCodeHealthInsurerRepository { get; }
        IActionGroupHealthInsurerRepository ActionGroupHealthInsurerRepository { get; }
        IActionHealthInsurerRepository ActionHealthInsurerRepository { get; }
        IAfterCareCategoryRepository AfterCareCategoryRepository { get; }
        IAfterCareFinancingRepository AfterCareFinancingRepository { get; }
        IAfterCareGroupRepository AfterCareGroupRepository { get; }
        IAfterCareTileGroupRepository AfterCareTileGroupRepository { get; }
        IAfterCareTypeRepository AfterCareTypeRepository { get; }
        IAidProductGroupRepository AidProductGroupRepository { get; }
        IAidProductOrderAnswerRepository AidProductOrderAnswerRepository { get; }
        IAidProductOrderItemRepository AidProductOrderItemRepository { get; }
        IAidProductRepository AidProductRepository { get; }
        IAspnet_ApplicationsRepository Aspnet_ApplicationsRepository { get; }
        IAspnet_MembershipRepository Aspnet_MembershipRepository { get; }
        IAspnet_RolesRepository Aspnet_RolesRepository { get; }
        IAspnet_SchemaVersionsRepository Aspnet_SchemaVersionsRepository { get; }
        IAspnet_UsersRepository Aspnet_UsersRepository { get; }
        IAttachmentReceivedTempRepository AttachmentReceivedTempRepository { get; }
        IAutoCreateSetRepository AutoCreateSetRepository { get; }
        IAutoCreateSetDepartmentRepository AutoCreateSetDepartmentRepository { get; }
        ICIZRelationRepository CIZRelationRepository { get; }
        IClientReceivedTempHL7Repository ClientReceivedTempHL7Repository { get; }
        IClientReceivedTempRepository ClientReceivedTempRepository { get; }
        IClientRepository ClientRepository { get; }
        ICommunicationLogRepository CommunicationLogRepository { get; }
        ICommunicationQueueRepository CommunicationQueueRepository { get; }
        IContactPersonRepository ContactPersonRepository { get; }
        ICryptoCertificateRepository CryptoCertificateRepository { get; }
        IDepartmentCapacityRepository DepartmentCapacityRepository { get; }
        IDepartmentCapacityPublicRepository DepartmentCapacityPublicRepository { get; }
        IDepartmentRepository DepartmentRepository { get; }
        IDepartmentTypeRepository DepartmentTypeRepository { get; }
        IDigitalSignatureRepository DigitalSignatureRepository { get; }
        IDoctorRepository DoctorRepository { get; }
        IEmployeeRepository EmployeeRepository { get; }
        IEmployeeDepartmentRepository EmployeeDepartmentRepository { get; }
        IEndpointConfigurationRepository EndpointConfigurationRepository { get; }
        IFieldReceivedTempRepository FieldReceivedTempRepository { get; }
        IFieldReceivedValueMappingRepository FieldReceivedValueMappingRepository { get; }
        IFlowDefinitionParticipationRepository FlowDefinitionParticipationRepository { get; }
        IFlowDefinitionRepository FlowDefinitionRepository { get; }
        IFlowFieldAttributeExceptionRepository FlowFieldAttributeExceptionRepository { get; }
        IFlowFieldAttributeRepository FlowFieldAttributeRepository { get; }
        IFlowFieldDashboardRepository FlowFieldDashboardRepository { get; }
        IFlowFieldDataSourceRepository FlowFieldDataSourceRepository { get; }
        IFlowFieldRepository FlowFieldRepository { get; }
        IFlowFieldValueRepository FlowFieldValueRepository { get; }
        IFlowInstanceDoorlopendDossierSearchValuesRepository FlowInstanceDoorlopendDossierSearchValuesRepository { get; }
        IFlowInstanceOrganizationRepository FlowInstanceOrganizationRepository { get; }
        IFlowInstanceRepository FlowInstanceRepository { get; }
        IFlowInstanceTagRepository FlowInstanceTagRepository { get; }
        IFlowDefinitionFormTypeFlowFieldRepository FlowDefinitionFormTypeFlowFieldRepository { get; }
        IFlowInstanceReportValuesProjectsRepository FlowInstanceReportValuesProjectsRepository { get; }
        IIguanaChannelRepository IguanaChannelRepository { get; }
        IFlowInstanceReportValuesAttachmentDetailRepository FlowInstanceReportValuesAttachmentDetailRepository { get; }
        IFlowInstanceReportValuesRepository FlowInstanceReportValuesRepository { get; }
        IFlowInstanceReportValuesDumpRepository FlowInstanceReportValuesDumpRepository { get; }
        IFlowInstanceSearchValuesRepository FlowInstanceSearchValuesRepository { get; }
        IFlowInstanceStatusRepository FlowInstanceStatusRepository { get; }
        IFlowPhaseAttributeRepository FlowPhaseAttributeRepository { get; }
        IFlowTypeRepository FlowTypeRepository { get; }
        IFormSetVersionRepository FormSetVersionRepository { get; }
        IFormTypeOrganizationRepository FormTypeOrganizationRepository { get; }
        IFormTypeRegionRepository FormTypeRegionRepository { get; }
        IFormTypeSelectionRepository FormTypeSelectionRepository { get; }
        IFormTypeSelectionOrganizationRepository FormTypeSelectionOrganizationRepository { get; }
        IFlowDefinitionFormTypeRepository FlowDefinitionFormTypeRepository { get; }
        IFormTypeRepository FormTypeRepository { get; }
        IFrequencyRepository FrequencyRepository { get; }
        IFrequencyTransferAttachmentRepository FrequencyTransferAttachmentRepository { get; }
        IFrequencyTransferMemoRepository FrequencyTransferMemoRepository { get; }
        IFunctionRoleRepository FunctionRoleRepository { get; }
        IGeneralActionPhaseRepository GeneralActionPhaseRepository { get; }
        IGeneralActionRepository GeneralActionRepository { get; }
        IGeneralActionTypeRepository GeneralActionTypeRepository { get; }
        IGPSCoordinaatRepository GPSCoordinaatRepository { get; }
        IGPSGemeenteRepository GPSGemeenteRepository { get; }
        IGPSPlaatsRepository GPSPlaatsRepository { get; }
        IGPSProvincieRepository GPSProvincieRepository { get; }
        IHealthAidDeviceRepository HealthAidDeviceRepository { get; }
        IHealthInsurerRepository HealthInsurerRepository { get; }
        IHtmlTextRepository HtmlTextRepository { get; }
        ILocationRepository LocationRepository { get; }
        ILocationRegionRepository LocationRegionRepository { get; }
        ILogCreateUserRepository LogCreateUserRepository { get; }
        ILogDepartmentRepository LogDepartmentRepository { get; }
        ILoggingRepository LoggingRepository { get; }
        ILoginHistoryRepository LoginHistoryRepository { get; }
        ILogLocationRepository LogLocationRepository { get; }
        ILogOrganizationRepository LogOrganizationRepository { get; }
        ILogPointFormServiceRepository LogPointFormServiceRepository { get; }
        ILogPrivacyRepository LogPrivacyRepository { get; }
        ILogReadRepository LogReadRepository { get; }
        IMedicineCardRepository MedicineCardRepository { get; }
        IMedicineRepository MedicineRepository { get; }
        IMessageReceivedTempRepository MessageReceivedTempRepository { get; }
        IMutActionHealthInsurerRepository MutActionHealthInsurerRepository { get; }
        IMutClientRepository MutClientRepository { get; }
        IMutDepartmentRepository MutDepartmentRepository { get; }
        IMutEmployeeRepository MutEmployeeRepository { get; }
        IMutFlowFieldValueRepository MutFlowFieldValueRepository { get; }
        IMutFormSetVersionRepository MutFormSetVersionRepository { get; }
        IMutLocationRepository MutLocationRepository { get; }
        IMutMedicineCardRepository MutMedicineCardRepository { get; }
        IMutOrganizationRepository MutOrganizationRepository { get; }
        IMutTransferTaskRepository MutTransferTaskRepository { get; }
        INationalityRepository NationalityRepository { get; }
        IOrganizationCSVTemplateRepository OrganizationCSVTemplateRepository { get; }
        IOrganizationCSVTemplateColumnRepository OrganizationCSVTemplateColumnRepository { get; }
        IOrganizationInviteRepository OrganizationInviteRepository { get; }
        IOrganizationLogoRepository OrganizationLogoRepository { get; }
        IOrganizationOutsidePointRepository OrganizationOutsidePointRepository { get; }
        IOrganizationProjectRepository OrganizationProjectRepository { get; }
        IOrganizationRepository OrganizationRepository { get; }
        IOrganizationSearchParticipationRepository OrganizationSearchParticipationRepository { get; }
        IOrganizationSearchPostalCodeRepository OrganizationSearchPostalCodeRepository { get; }
        IOrganizationSearchRegionRepository OrganizationSearchRegionRepository { get; }
        IOrganizationSearchRepository OrganizationSearchRepository { get; }
        IOrganizationSettingRepository OrganizationSettingRepository { get; }
        IOrganizationTypeRepository OrganizationTypeRepository { get; }
        IPageLockRepository PageLockRepository { get; }
        IPatientReceivedTempRepository PatientReceivedTempRepository { get; }
        IPatientRequestTempRepository PatientRequestTempRepository { get; }
        IPersonDataRepository PersonDataRepository { get; }
        IPhaseDefinitionRepository PhaseDefinitionRepository { get; }
        IPhaseDefinitionNavigationRepository PhaseDefinitionNavigationRepository { get; }
        IPhaseDefinitionRightsRepository PhaseDefinitionRightsRepository { get; }
        IPhaseInstanceRepository PhaseInstanceRepository { get; }
        IPostalCodeRepository PostalCodeRepository { get; }
        IPreferredLocationRepository PreferredLocationRepository { get; }
        IRazorTemplateRepository RazorTemplateRepository { get; }
        IRegionRepository RegionRepository { get; }
        IRegionAfterCareTypeRepository RegionAfterCareTypeRepository { get; }
        IReportDefinitionRepository ReportDefinitionRepository { get; }
        IScheduleHistoryRepository ScheduleHistoryRepository { get; }
        IScreenRepository ScreenRepository { get; }
        ISearchUIConfigurationRepository SearchUIConfigurationRepository { get; }
        ISearchUIFieldConfigurationRepository SearchUIFieldConfigurationRepository { get; }
        ISendAttachmentRepository SendAttachmentRepository { get; }
        ISendFormTypeRepository SendFormTypeRepository { get; }
        IServiceAreaPostalCodeRepository ServiceAreaPostalCodeRepository { get; }
        IServiceAreaRepository ServiceAreaRepository { get; }
        IServiceBusLogRepository ServiceBusLogRepository { get; }
        ISignaleringDestinationRepository SignaleringDestinationRepository { get; }
        ISignaleringRepository SignaleringRepository { get; }
        ISignaleringTargetRepository SignaleringTargetRepository { get; }
        ISignaleringTriggerRepository SignaleringTriggerRepository { get; }
        ISignedPhaseInstanceRepository SignedPhaseInstanceRepository { get; }
        ISpecialismRepository SpecialismRepository { get; }
        ISupplierRepository SupplierRepository { get; }
        ISystemMessageReadRepository SystemMessageReadRepository { get; }
        ISystemMessageRepository SystemMessageRepository { get; }
        ITemplateDefaultRepository TemplateDefaultRepository { get; }
        ITemplateRepository TemplateRepository { get; }
        ITemplateTypeRepository TemplateTypeRepository { get; }
        ITempletRepository TempletRepository { get; }
        ITransferAttachmentRepository TransferAttachmentRepository { get; }
        ITransferMemoChangesRepository TransferMemoChangesRepository { get; }
        ITransferMemoRepository TransferMemoRepository { get; }
        ITransferMemoTemplateRepository TransferMemoTemplateRepository { get; }
        ITransferRepository TransferRepository { get; }
        ITransferTaskRepository TransferTaskRepository { get; }
        IUsedHashCodesRepository UsedHashCodesRepository { get; }
        IUsedMFACodeRepository UsedMFACodeRepository { get; }
        IValidationRepository ValidationRepository { get; }
        IViewTransferHistory ViewTransferHistoryRepository { get; }
        IWebRequestLogRepository WebRequestLogRepository { get; }
        IZorgAdviesItemGroupRepository ZorgAdviesItemGroupRepository { get; }
        IZorgAdviesItemRepository ZorgAdviesItemRepository { get; }
        IZorgAdviesItemValueRepository ZorgAdviesItemValueRepository { get; }
    }
    public partial class UnitOfWork<TDbContext> : IUnitOfWork, IDisposable where TDbContext : DbContext, new()
    {
        public DbContextConfiguration DatabaseConfiguration { get { return this.DbContext.Configuration; } }
        public DbContext DatabaseContext { get { return this.DbContext; } }

        private IAfterCareCategoryRepository afterCareCategoryRepository;
        private IAfterCareFinancingRepository afterCareFinancingRepository;
        private IAfterCareGroupRepository afterCareGroupRepository;
        private IAfterCareTileGroupRepository afterCareTileGroupRepository;
        private IAfterCareTypeRepository afterCareTypeRepository;
        private IActionCodeHealthInsurerFormTypeRepository actioncodehealthinsurerformtypeRepository;
        private IActionCodeHealthInsurerListRepository actioncodehealthinsurerlistRepository;
        private IActionCodeHealthInsurerRegioRepository actioncodehealthinsurerregioRepository;
        private IActionCodeHealthInsurerRepository actioncodehealthinsurerRepository;
        private IActionGroupHealthInsurerRepository actiongrouphealthinsurerRepository;
        private IActionHealthInsurerRepository actionhealthinsurerRepository;
        private IAidProductGroupRepository aidProductGroupRepository;
        private IAidProductOrderAnswerRepository aidProductOrderAnswerRepository;
        private IAidProductOrderItemRepository aidProductOrderItemRepository;
        private IAidProductRepository aidProductRepository;
        private IAspnet_ApplicationsRepository aspnet_applicationsRepository;
        private IAspnet_MembershipRepository aspnet_membershipRepository;
        private IAspnet_RolesRepository aspnet_rolesRepository;
        private IAspnet_SchemaVersionsRepository aspnet_schemaversionsRepository;
        private IAspnet_UsersRepository aspnet_usersRepository;
        private IAttachmentReceivedTempRepository attachmentreceivedtemprepository;
        private IAutoCreateSetRepository autocreatesetrepository;
        private IAutoCreateSetDepartmentRepository autocreatesetdepartmentrepository;
        private ICIZRelationRepository cizrelationRepository;
        private IClientReceivedTempHL7Repository clientreceivedtemphl7Repository;
        private IClientReceivedTempRepository clientReceivedTempRepository;
        private IClientRepository clientRepository;
        private ICommunicationLogRepository communicationlogRepository;
        private ICommunicationQueueRepository communicationqueueRepository;
        private IContactPersonRepository contactPersonRepository;
        private ICryptoCertificateRepository cryptoCertificateRepository;
        private IDepartmentCapacityRepository departmentcapacityRepository;
        private IDepartmentCapacityPublicRepository departmentcapacityPublicRepository;
        private IDepartmentRepository departmentRepository;
        private IDepartmentTypeRepository departmenttypeRepository;
        private IDigitalSignatureRepository digitalsignaturerepository;
        private IDoctorRepository doctorRepository;
        private IEmployeeRepository employeeRepository;
        private IEndpointConfigurationRepository endpointconfigurationrepository;
        private IEmployeeDepartmentRepository employeeDepartmentRepository;
        private IFieldReceivedTempRepository fieldReceivedTempRepository;
        private IFieldReceivedValueMappingRepository fieldReceivedValueMappingRepository;
        private IFlowDefinitionParticipationRepository flowDefinitionParticipationRepository;
        private IFlowDefinitionRepository flowdefinitionRepository;     
        private IFlowFieldAttributeExceptionRepository flowfieldattributeExceptionRepository;
        private IFlowFieldAttributeRepository flowfieldattributeRepository;
        private IFlowFieldDashboardRepository flowFieldDashboardRepository;
        private IFlowFieldRepository flowfieldRepository;
        private IFlowFieldDataSourceRepository flowFieldDataSourceRepository;
        private IFlowFieldValueRepository flowfieldvalueRepository;
        private IFlowInstanceDoorlopendDossierSearchValuesRepository flowInstanceDoorlopendDossierSearchValuesRepository;
        private IFlowInstanceOrganizationRepository flowinstanceOrganizationRepository;
        private IFlowInstanceRepository flowinstanceRepository;
        private IFlowInstanceTagRepository flowinstanceTagRepository;
        private IFlowDefinitionFormTypeFlowFieldRepository flowDefinitionFormTypeFlowFieldRepository;
        private IFlowInstanceReportValuesProjectsRepository flowinstanceReportValuesProjectsRepository;
        private IIguanaChannelRepository iguanaChannelRepository;
        private IFlowInstanceReportValuesAttachmentDetailRepository flowinstanceReportValuesAttachmentDetailRepository;
        private IFlowInstanceReportValuesRepository flowinstanceReportValuesRepository;
        private IFlowInstanceReportValuesDumpRepository flowinstanceReportValuesDumpRepository;
        private IFlowInstanceSearchValuesRepository flowinstanceSearchValuesRepository;
        private IFlowInstanceStatusRepository flowInstanceStatusRepository;
        private IFlowPhaseAttributeRepository flowphaseattributeRepository;
        private IFlowTypeRepository flowtypeRepository;
        private IFormSetVersionRepository formsetversionRepository;
        private IFormTypeOrganizationRepository formtypeorganizationRepository;     
        private IFormTypeRegionRepository formtypeRegionRepository;
        private IFormTypeSelectionRepository formtypeSelectionRepository;
        private IFormTypeSelectionOrganizationRepository formtypeSelectionOrganizationRepository;
        private IFlowDefinitionFormTypeRepository flowDefinitionFormTypeRepository;
        private IFormTypeRepository formtypeRepository;
        private IFrequencyRepository frequencyRepository;
        private IFrequencyTransferAttachmentRepository frequencyTransferAttachmentRepository;
        private IFrequencyTransferMemoRepository frequencyTransferMemoRepository;
        private IFunctionRoleRepository functionRoleRepository;
        private IGeneralActionPhaseRepository generalactionphaseRepository;
        private IGeneralActionRepository generalactionRepository;
        private IGeneralActionTypeRepository generalactiontypeRepository;
        private IGPSCoordinaatRepository gpsCoordinaatRepository;
        private IGPSGemeenteRepository gpsGemeenteRepository;
        private IGPSPlaatsRepository gpsPlaatsRepository;
        private IGPSProvincieRepository gpsProvincieRepository;
        private HealthAidDeviceRepository healthAidDeviceRepository;
        private IHealthInsurerRepository healthinsurerRepository;
        private IHtmlTextRepository htmltextRepository;
        private ILocationRepository locationRepository;
        private ILocationRegionRepository locationRegionRepository;
        private ILogCreateUserRepository logCreateUserRepository;
        private ILogDepartmentRepository logDepartmentRepository;
        private ILoggingRepository loggingRepository;
        private ILoginHistoryRepository loginhistoryRepository;
        private ILogLocationRepository logLocationRepository;
        private ILogOrganizationRepository logOrganizationRepository;
        private ILogPointFormServiceRepository logPointFormServiceRepository;
        private ILogPrivacyRepository logPrivacyRepository;
        private ILogReadRepository logreadRepository;
        private IMedicineCardRepository medicinecardRepository;
        private IMedicineRepository medicineRepository;
        private IMessageReceivedTempRepository messageReceivedTempRepository;
        private IMutActionHealthInsurerRepository mutActionHealthInsurerRepository;
        private MutClientRepository mutClientRepository;
        private IMutDepartmentRepository mutDepartmentRepository;
        private IMutEmployeeRepository mutEmployeeRepository;
        private IMutFlowFieldValueRepository mutFlowFieldValueRepository;
        private IMutFormSetVersionRepository mutFormSetVersionRepository;
        private IMutLocationRepository mutLocationRepository;
        private IMutMedicineCardRepository mutMedicineCardRepository;
        private IMutOrganizationRepository mutOrganizationRepository;
        private IMutTransferTaskRepository muttransfertaskrepository;
        private INationalityRepository nationalityRepository;
        private IOrganizationCSVTemplateRepository organizationCSVTemplateRepository;
        private IOrganizationCSVTemplateColumnRepository organizationCSVTemplateColumnRepository;
        private IOrganizationInviteRepository organizationInviteRepository;
        private IOrganizationLogoRepository organizationLogoRepository;
        private IOrganizationOutsidePointRepository organizationoutsidepointRepository;
        private IOrganizationProjectRepository organizationProjectRepository;
        private IOrganizationRepository organizationRepository;
        private IOrganizationSearchParticipationRepository organizationSearchParticipationRepository;
        private IOrganizationSearchPostalCodeRepository organizationSearchPostalCodeRepository;
        private IOrganizationSearchRegionRepository organizationSearchRegionRepository;
        private IOrganizationSearchRepository organizationSearchRepository;
        private IOrganizationSettingRepository organizationSettingRepository;
        private IOrganizationTypeRepository organizationtypeRepository;
        private IPageLockRepository pagelockRepository;
        private IPatientReceivedTempRepository patientReceivedTempRepository;
        private IPatientRequestTempRepository patientrequesttemprepository;
        private IPersonDataRepository personDataRepository;
        private IPhaseDefinitionRepository phasedefinitionRepository;
        private IPhaseDefinitionNavigationRepository phasedefinitionNavigationRepository;
        private IPhaseDefinitionRightsRepository phaseDefinitionRightsRepositoryRepository;
        private IPhaseInstanceRepository phaseinstanceRepository;
        private IPostalCodeRepository postalcodeRepository;
        private IPreferredLocationRepository preferredLocationRepository;
        private IRazorTemplateRepository razorTemplateRepository;
        private IRegionRepository regionRepository;
        private IRegionAfterCareTypeRepository regionAfterCareTypeRepository;
        private IReportDefinitionRepository reportDefinitionRepository;
        private IScheduleHistoryRepository scheduleHistoryRepository;
        private IScreenRepository screenRepository;
        private ISearchUIConfigurationRepository searchUIConfigurationRepository;
        private ISearchUIFieldConfigurationRepository searchUIFieldConfigurationRepository;
        private ISendAttachmentRepository sendAttachmentRepository;
        private ISendFormTypeRepository sendFormTypeRepository;
        private IServiceAreaPostalCodeRepository serviceareapostalcodeRepository;
        private IServiceAreaRepository serviceareaRepository;
        private IServiceBusLogRepository serviceBusLogRepository;
        private ISignaleringDestinationRepository signaleringDestinationRepository;
        private ISignaleringRepository signaleringRepository;
        private ISignaleringTargetRepository signaleringTargetRepository;
        private ISignaleringTriggerRepository signaleringTriggerRepository;
        private ISignedPhaseInstanceRepository signedphaseinstancerepository;
        private ISpecialismRepository specialismRepository;
        private ISupplierRepository supplierRepository;
        private ISystemMessageReadRepository systemmessagereadRepository;
        private ISystemMessageRepository systemmessageRepository;
        private ITemplateDefaultRepository templatedefaultRepository;
        private ITemplateRepository templateRepository;
        private ITemplateTypeRepository templatetypeRepository;
        private ITempletRepository templetRepository;
        private ITransferAttachmentRepository transferattachmentRepository;
        private ITransferMemoChangesRepository transfermemochangesRepository;
        private ITransferMemoRepository transfermemoRepository;
        private ITransferMemoTemplateRepository transfermemoTemplateRepository;
        private ITransferRepository transferRepository;
        private ITransferTaskRepository transfertaskrepository;
        private IUsedHashCodesRepository usedhashcodesRepository;
        private IUsedMFACodeRepository usedmfacodeRepository;
        private IValidationRepository validationRepository;
        private IViewTransferHistory viewtransferhistoryRepository;
        private IWebRequestLogRepository webRequestLogRepository;
        private IZorgAdviesItemGroupRepository zorgadviesitemgroupRepository;
        private IZorgAdviesItemRepository zorgadviesitemRepository;
        private IZorgAdviesItemValueRepository zorgadviesitemvalueRepository;

        public IActionCodeHealthInsurerFormTypeRepository ActionCodeHealthInsurerFormTypeRepository
        {
            get
            {
                if (this.actioncodehealthinsurerformtypeRepository == null)
                {
                    this.actioncodehealthinsurerformtypeRepository = new ActionCodeHealthInsurerFormTypeRepository(DbContext);
                }
                return this.actioncodehealthinsurerformtypeRepository;
            }
        }

        public IActionCodeHealthInsurerListRepository ActionCodeHealthInsurerListRepository
        {
            get
            {
                if (this.actioncodehealthinsurerlistRepository == null)
                {
                    this.actioncodehealthinsurerlistRepository = new ActionCodeHealthInsurerListRepository(DbContext);
                }
                return this.actioncodehealthinsurerlistRepository;
            }
        }

        public IActionCodeHealthInsurerRegioRepository ActionCodeHealthInsurerRegioRepository
        {
            get
            {
                if (this.actioncodehealthinsurerregioRepository == null)
                {
                    this.actioncodehealthinsurerregioRepository = new ActionCodeHealthInsurerRegioRepository(DbContext);
                }
                return this.actioncodehealthinsurerregioRepository;
            }
        }

        public IActionCodeHealthInsurerRepository ActionCodeHealthInsurerRepository
        {
            get
            {
                if (this.actioncodehealthinsurerRepository == null)
                {
                    this.actioncodehealthinsurerRepository = new ActionCodeHealthInsurerRepository(DbContext);
                }
                return this.actioncodehealthinsurerRepository;
            }
        }

        public IActionGroupHealthInsurerRepository ActionGroupHealthInsurerRepository
        {
            get
            {
                if (this.actiongrouphealthinsurerRepository == null)
                {
                    this.actiongrouphealthinsurerRepository = new ActionGroupHealthInsurerRepository(DbContext);
                }
                return this.actiongrouphealthinsurerRepository;
            }
        }

        public IActionHealthInsurerRepository ActionHealthInsurerRepository
        {
            get
            {
                if (this.actionhealthinsurerRepository == null)
                {
                    this.actionhealthinsurerRepository = new ActionHealthInsurerRepository(DbContext);
                }
                return this.actionhealthinsurerRepository;
            }
        }

        public IAfterCareCategoryRepository AfterCareCategoryRepository
        {
            get
            {
                if (this.afterCareCategoryRepository == null)
                {
                    this.afterCareCategoryRepository = new AfterCareCategoryRepository(DbContext);
                }
                return this.afterCareCategoryRepository;
            }
        }

        public IAfterCareFinancingRepository AfterCareFinancingRepository
        {
            get
            {
                if (this.afterCareFinancingRepository == null)
                {
                    this.afterCareFinancingRepository = new AfterCareFinancingRepository(DbContext);
                }
                return this.afterCareFinancingRepository;
            }
        }

        public IAfterCareGroupRepository AfterCareGroupRepository
        {
            get
            {
                if (this.afterCareGroupRepository == null)
                {
                    this.afterCareGroupRepository = new AfterCareGroupRepository(DbContext);
                }
                return this.afterCareGroupRepository;
            }
        }

        public IAfterCareTileGroupRepository AfterCareTileGroupRepository
        {
            get
            {
                if (this.afterCareTileGroupRepository == null)
                {
                    this.afterCareTileGroupRepository = new AfterCareTileGroupRepository(DbContext);
                }
                return this.afterCareTileGroupRepository;
            }
        }

        public IAfterCareTypeRepository AfterCareTypeRepository
        {
            get
            {
                if (this.afterCareTypeRepository == null)
                {
                    this.afterCareTypeRepository = new AfterCareTypeRepository(DbContext);
                }
                return this.afterCareTypeRepository;
            }
        }

        public IAidProductGroupRepository AidProductGroupRepository
        {
            get
            {
                if (this.aidProductGroupRepository == null)
                {
                    this.aidProductGroupRepository = new AidProductGroupRepository(DbContext);
                }
                return this.aidProductGroupRepository;
            }
        }

        public IAidProductOrderAnswerRepository AidProductOrderAnswerRepository
        {
            get
            {
                if (this.aidProductOrderAnswerRepository == null)
                {
                    this.aidProductOrderAnswerRepository = new AidProductOrderAnswerRepository(DbContext);
                }
                return this.aidProductOrderAnswerRepository;
            }
        }

        public IAidProductOrderItemRepository AidProductOrderItemRepository
        {
            get
            {
                if (this.aidProductOrderItemRepository == null)
                {
                    this.aidProductOrderItemRepository = new AidProductOrderItemRepository(DbContext);
                }
                return this.aidProductOrderItemRepository;
            }
        }

        public IAidProductRepository AidProductRepository
        {
            get
            {
                if (this.aidProductRepository == null)
                {
                    this.aidProductRepository = new AidProductRepository(DbContext);
                }
                return this.aidProductRepository;
            }
        }

        public IAspnet_ApplicationsRepository Aspnet_ApplicationsRepository
        {
            get
            {
                if (this.aspnet_applicationsRepository == null)
                {
                    this.aspnet_applicationsRepository = new Aspnet_ApplicationsRepository(DbContext);
                }
                return this.aspnet_applicationsRepository;
            }
        }

        public IAspnet_MembershipRepository Aspnet_MembershipRepository
        {
            get
            {
                if (this.aspnet_membershipRepository == null)
                {
                    this.aspnet_membershipRepository = new Aspnet_MembershipRepository(DbContext);
                }
                return this.aspnet_membershipRepository;
            }
        }

        public IAspnet_RolesRepository Aspnet_RolesRepository
        {
            get
            {
                if (this.aspnet_rolesRepository == null)
                {
                    this.aspnet_rolesRepository = new Aspnet_RolesRepository(DbContext);
                }
                return this.aspnet_rolesRepository;
            }
        }

        public IAspnet_SchemaVersionsRepository Aspnet_SchemaVersionsRepository
        {
            get
            {
                if (this.aspnet_schemaversionsRepository == null)
                {
                    this.aspnet_schemaversionsRepository = new Aspnet_SchemaVersionsRepository(DbContext);
                }
                return this.aspnet_schemaversionsRepository;
            }
        }

        public IAspnet_UsersRepository Aspnet_UsersRepository
        {
            get
            {
                if (this.aspnet_usersRepository == null)
                {
                    this.aspnet_usersRepository = new Aspnet_UsersRepository(DbContext);
                }
                return this.aspnet_usersRepository;
            }
        }

        public IAttachmentReceivedTempRepository AttachmentReceivedTempRepository
        {
            get
            {
                if (this.attachmentreceivedtemprepository == null)
                {
                    this.attachmentreceivedtemprepository = new AttachmentReceivedTempRepository(DbContext);
                }
                return this.attachmentreceivedtemprepository;
            }
        }

        public IAutoCreateSetRepository AutoCreateSetRepository
        {
            get
            {
                if (this.autocreatesetrepository == null)
                {
                    this.autocreatesetrepository = new AutoCreateSetRepository(DbContext);
                }
                return this.autocreatesetrepository;
            }
        }

        public IAutoCreateSetDepartmentRepository AutoCreateSetDepartmentRepository
        {
            get
            {
                if (this.autocreatesetdepartmentrepository == null)
                {
                    this.autocreatesetdepartmentrepository = new AutoCreateSetDepartmentRepository(DbContext);
                }
                return this.autocreatesetdepartmentrepository;
            }
        }

        public ICIZRelationRepository CIZRelationRepository
        {
            get
            {
                if (this.cizrelationRepository == null)
                {
                    this.cizrelationRepository = new CIZRelationRepository(DbContext);
                }
                return this.cizrelationRepository;
            }
        }

        public IClientReceivedTempHL7Repository ClientReceivedTempHL7Repository
        {
            get
            {
                if (this.clientreceivedtemphl7Repository == null)
                {
                    this.clientreceivedtemphl7Repository = new ClientReceivedTempHL7Repository(DbContext);
                }
                return this.clientreceivedtemphl7Repository;
            }
        }

        public IClientReceivedTempRepository ClientReceivedTempRepository
        {
            get
            {
                if (this.clientReceivedTempRepository == null)
                    this.clientReceivedTempRepository = new ClientReceivedTempRepository(DbContext);

                return this.clientReceivedTempRepository;
            }
        }

        public IClientRepository ClientRepository
        {
            get
            {
                if (this.clientRepository == null)
                {
                    this.clientRepository = new ClientRepository(DbContext);
                }
                return this.clientRepository;
            }
        }

        public ICommunicationLogRepository CommunicationLogRepository
        {
            get
            {
                if (this.communicationlogRepository == null)
                {
                    this.communicationlogRepository = new CommunicationLogRepository(DbContext);
                }
                return this.communicationlogRepository;
            }
        }

        public ICommunicationQueueRepository CommunicationQueueRepository
        {
            get
            {
                if (this.communicationqueueRepository == null)
                {
                    this.communicationqueueRepository = new CommunicationQueueRepository(DbContext);
                }
                return this.communicationqueueRepository;
            }
        }

        public IContactPersonRepository ContactPersonRepository
        {
            get
            {
                if (this.contactPersonRepository == null)
                {
                    this.contactPersonRepository = new ContactPersonRepository(DbContext);
                }
                return this.contactPersonRepository;
            }
        }

        public ICryptoCertificateRepository CryptoCertificateRepository
        {
            get
            {
                if (this.cryptoCertificateRepository == null)
                {
                    this.cryptoCertificateRepository = new CryptoCertificateRepository(DbContext);
                }
                return this.cryptoCertificateRepository;
            }
        }

        public IDepartmentCapacityRepository DepartmentCapacityRepository
        {
            get
            {
                if (this.departmentcapacityRepository == null)
                {
                    this.departmentcapacityRepository = new DepartmentCapacityRepository(DbContext);
                }
                return this.departmentcapacityRepository;
            }
        }

        public IDepartmentCapacityPublicRepository DepartmentCapacityPublicRepository
        {
            get
            {
                if (this.departmentcapacityPublicRepository == null)
                {
                    this.departmentcapacityPublicRepository = new DepartmentCapacityPublicRepository(DbContext);
                }
                return this.departmentcapacityPublicRepository;
            }
        }

        public IDepartmentRepository DepartmentRepository
        {
            get
            {
                if (this.departmentRepository == null)
                {
                    this.departmentRepository = new DepartmentRepository(DbContext);
                }
                return this.departmentRepository;
            }
        }

        public IDepartmentTypeRepository DepartmentTypeRepository
        {
            get
            {
                if (this.departmenttypeRepository == null)
                {
                    this.departmenttypeRepository = new DepartmentTypeRepository(DbContext);
                }
                return this.departmenttypeRepository;
            }
        }

        public IDigitalSignatureRepository DigitalSignatureRepository
        {
            get
            {
                if (this.digitalsignaturerepository == null)
                {
                    this.digitalsignaturerepository = new DigitalSignatureRepository(DbContext);
                }
                return this.digitalsignaturerepository;
            }
        }

        public IDoctorRepository DoctorRepository
        {
            get
            {
                if (this.doctorRepository == null)
                {
                    this.doctorRepository = new DoctorRepository(DbContext);
                }
                return this.doctorRepository;
            }
        }

        public IEmployeeRepository EmployeeRepository
        {
            get
            {
                if (this.employeeRepository == null)
                {
                    this.employeeRepository = new EmployeeRepository(DbContext);
                }
                return this.employeeRepository;
            }
        }

        public IEmployeeDepartmentRepository EmployeeDepartmentRepository
        {
            get
            {
                if (this.employeeDepartmentRepository == null)
                {
                    this.employeeDepartmentRepository = new EmployeeDepartmentRepository(DbContext);
                }
                return this.employeeDepartmentRepository;
            }
        }

        public IEndpointConfigurationRepository EndpointConfigurationRepository
        {
            get
            {
                if (this.endpointconfigurationrepository == null)
                {
                    this.endpointconfigurationrepository = new EndpointConfigurationRepository(DbContext);
                }
                return this.endpointconfigurationrepository;
            }
        }

        public IFieldReceivedTempRepository FieldReceivedTempRepository
        {
            get
            {
                if (this.fieldReceivedTempRepository == null)
                {
                    this.fieldReceivedTempRepository = new FieldReceivedTempRepository(DbContext);
                }
                return this.fieldReceivedTempRepository;
            }
        }

        public IFieldReceivedValueMappingRepository FieldReceivedValueMappingRepository
        {
            get
            {
                if (this.fieldReceivedValueMappingRepository == null)
                    this.fieldReceivedValueMappingRepository = new FieldReceivedValueMappingRepository(DbContext);

                return this.fieldReceivedValueMappingRepository;
            }
        }

        public IFlowDefinitionParticipationRepository FlowDefinitionParticipationRepository
        {
            get
            {
                if (this.flowDefinitionParticipationRepository == null)
                {
                    this.flowDefinitionParticipationRepository = new FlowDefinitionParticipationRepository(DbContext);
                }
                return this.flowDefinitionParticipationRepository;
            }
        }

        public IFlowDefinitionRepository FlowDefinitionRepository
        {
            get
            {
                if (this.flowdefinitionRepository == null)
                {
                    this.flowdefinitionRepository = new FlowDefinitionRepository(DbContext);
                }
                return this.flowdefinitionRepository;
            }
        }

        public IFlowFieldAttributeExceptionRepository FlowFieldAttributeExceptionRepository
        {
            get
            {
                if (this.flowfieldattributeExceptionRepository == null)
                {
                    this.flowfieldattributeExceptionRepository = new FlowFieldAttributeExceptionRepository(DbContext);
                }
                return this.flowfieldattributeExceptionRepository;
            }
        }

        public IFlowFieldAttributeRepository FlowFieldAttributeRepository
        {
            get
            {
                if (this.flowfieldattributeRepository == null)
                {
                    this.flowfieldattributeRepository = new FlowFieldAttributeRepository(DbContext);
                }
                return this.flowfieldattributeRepository;
            }
        }

        public IFlowFieldDashboardRepository FlowFieldDashboardRepository
        {
            get
            {
                if (this.flowFieldDashboardRepository == null)
                {
                    this.flowFieldDashboardRepository = new FlowFieldDashboardRepository(DbContext);
                }

                return this.flowFieldDashboardRepository;
            }
        }

        public IFlowFieldDataSourceRepository FlowFieldDataSourceRepository
        {
            get
            {
                if (this.flowFieldDataSourceRepository == null)
                {
                    this.flowFieldDataSourceRepository = new FlowFieldDataSourceRepository(DbContext);
                }

                return this.flowFieldDataSourceRepository;
            }
        }

        public IFlowFieldRepository FlowFieldRepository
        {
            get
            {
                if (this.flowfieldRepository == null)
                {
                    this.flowfieldRepository = new FlowFieldRepository(DbContext);
                }
                return this.flowfieldRepository;
            }
        }

        public IFlowFieldValueRepository FlowFieldValueRepository
        {
            get
            {
                if (this.flowfieldvalueRepository == null)
                {
                    this.flowfieldvalueRepository = new FlowFieldValueRepository(DbContext);
                }
                return this.flowfieldvalueRepository;
            }
        }

        public IFlowInstanceDoorlopendDossierSearchValuesRepository FlowInstanceDoorlopendDossierSearchValuesRepository
        {
            get
            {
                if (this.flowInstanceDoorlopendDossierSearchValuesRepository == null)
                {
                    this.flowInstanceDoorlopendDossierSearchValuesRepository = new FlowInstanceDoorlopendDossierSearchValuesRepository(DbContext);
                }
                return this.flowInstanceDoorlopendDossierSearchValuesRepository;
            }
        }

        public IFlowInstanceOrganizationRepository FlowInstanceOrganizationRepository
        {
            get
            {
                if (this.flowinstanceOrganizationRepository == null)
                {
                    this.flowinstanceOrganizationRepository = new FlowInstanceOrganizationRepository(DbContext);
                }
                return this.flowinstanceOrganizationRepository;
            }
        }

        public IFlowInstanceTagRepository FlowInstanceTagRepository
        {
            get
            {
                if (this.flowinstanceTagRepository == null)
                {
                    this.flowinstanceTagRepository = new FlowInstanceTagRepository(DbContext);
                }
                return this.flowinstanceTagRepository;
            }
        }

        public IFlowInstanceRepository FlowInstanceRepository
        {
            get
            {
                if (this.flowinstanceRepository == null)
                {
                    this.flowinstanceRepository = new FlowInstanceRepository(DbContext);
                }
                return this.flowinstanceRepository;
            }
        }

        public IFlowDefinitionFormTypeFlowFieldRepository FlowDefinitionFormTypeFlowFieldRepository
        {
            get
            {
                if (this.flowDefinitionFormTypeFlowFieldRepository == null)
                {
                    this.flowDefinitionFormTypeFlowFieldRepository = new FlowDefinitionFormTypeFlowFieldRepository(DbContext);
                }
                return this.flowDefinitionFormTypeFlowFieldRepository;
            }
        }
        public IFlowInstanceReportValuesProjectsRepository FlowInstanceReportValuesProjectsRepository
        {
            get
            {
                if (this.flowinstanceReportValuesProjectsRepository == null)
                {
                    this.flowinstanceReportValuesProjectsRepository = new FlowInstanceReportValuesProjectsRepository(DbContext);
                }
                return this.flowinstanceReportValuesProjectsRepository;
            }
        }

        public IIguanaChannelRepository IguanaChannelRepository
        {
            get
            {
                if (this.iguanaChannelRepository == null)
                {
                    this.iguanaChannelRepository = new IguanaChannelRepository(DbContext);
                }
                return this.iguanaChannelRepository;
            }
        }

        public IFlowInstanceReportValuesAttachmentDetailRepository FlowInstanceReportValuesAttachmentDetailRepository
        {
            get
            {
                if (this.flowinstanceReportValuesAttachmentDetailRepository == null)
                {
                    this.flowinstanceReportValuesAttachmentDetailRepository = new FlowInstanceReportValuesAttachmentDetailRepository(DbContext);
                }
                return this.flowinstanceReportValuesAttachmentDetailRepository;
            }
        }

        public IFlowInstanceReportValuesRepository FlowInstanceReportValuesRepository
        {
            get
            {
                if (this.flowinstanceReportValuesRepository == null)
                {
                    this.flowinstanceReportValuesRepository = new FlowInstanceReportValuesRepository(DbContext);
                }
                return this.flowinstanceReportValuesRepository;
            }
        }

        public IFlowInstanceReportValuesDumpRepository FlowInstanceReportValuesDumpRepository
        {
            get
            {
                if (this.flowinstanceReportValuesDumpRepository == null)
                {
                    this.flowinstanceReportValuesDumpRepository = new FlowInstanceReportValuesDumpRepository(DbContext);
                }
                return this.flowinstanceReportValuesDumpRepository;
            }
        }

        public IFlowInstanceSearchValuesRepository FlowInstanceSearchValuesRepository
        {
            get
            {
                if (this.flowinstanceSearchValuesRepository == null)
                {
                    this.flowinstanceSearchValuesRepository = new FlowInstanceSearchValuesRepository(DbContext);
                }
                return this.flowinstanceSearchValuesRepository;
            }
        }

        public IFlowInstanceStatusRepository FlowInstanceStatusRepository
        {
            get
            {
                if (this.flowInstanceStatusRepository == null)
                {
                    this.flowInstanceStatusRepository = new FlowInstanceStatusRepository(DbContext);
                }

                return this.flowInstanceStatusRepository;
            }
        }

        public IFlowPhaseAttributeRepository FlowPhaseAttributeRepository
        {
            get
            {
                if (this.flowphaseattributeRepository == null)
                {
                    this.flowphaseattributeRepository = new FlowPhaseAttributeRepository(DbContext);
                }
                return this.flowphaseattributeRepository;
            }
        }

        public IFlowTypeRepository FlowTypeRepository
        {
            get
            {
                if (this.flowtypeRepository == null)
                {
                    this.flowtypeRepository = new FlowTypeRepository(DbContext);
                }
                return this.flowtypeRepository;
            }
        }

        public IFormSetVersionRepository FormSetVersionRepository
        {
            get
            {
                if (this.formsetversionRepository == null)
                {
                    this.formsetversionRepository = new FormSetVersionRepository(DbContext);
                }
                return this.formsetversionRepository;
            }
        }

        public IFormTypeOrganizationRepository FormTypeOrganizationRepository
        {
            get
            {
                if (this.formtypeorganizationRepository == null)
                {
                    this.formtypeorganizationRepository = new FormTypeOrganizationRepository(DbContext);
                }
                return this.formtypeorganizationRepository;
            }
        }
        public IFormTypeRegionRepository FormTypeRegionRepository
        {
            get
            {
                if (this.formtypeRegionRepository == null)
                {
                    this.formtypeRegionRepository = new FormTypeRegionRepository(DbContext);
                }
                return this.formtypeRegionRepository;
            }
        }

        public IFormTypeSelectionRepository FormTypeSelectionRepository
        {
            get
            {
                if (this.formtypeSelectionRepository == null)
                {
                    this.formtypeSelectionRepository = new FormTypeSelectionRepository(DbContext);
                }
                return this.formtypeSelectionRepository;
            }
        }

        public IFormTypeSelectionOrganizationRepository FormTypeSelectionOrganizationRepository
        {
            get
            {
                if (this.formtypeSelectionOrganizationRepository == null)
                {
                    this.formtypeSelectionOrganizationRepository = new FormTypeSelectionOrganizationRepository(DbContext);
                }
                return this.formtypeSelectionOrganizationRepository;
            }
        }

        public IFlowDefinitionFormTypeRepository FlowDefinitionFormTypeRepository
        {
            get
            {
                if (this.flowDefinitionFormTypeRepository == null)
                {
                    this.flowDefinitionFormTypeRepository = new FlowDefinitionFormTypeRepository(DbContext);
                }
                return this.flowDefinitionFormTypeRepository;
            }
        }

        public IFormTypeRepository FormTypeRepository
        {
            get
            {
                if (this.formtypeRepository == null)
                {
                    this.formtypeRepository = new FormTypeRepository(DbContext);
                }
                return this.formtypeRepository;
            }
        }

        public IFrequencyRepository FrequencyRepository
        {
            get
            {
                if (this.frequencyRepository == null)
                {
                    this.frequencyRepository = new FrequencyRepository(DbContext);
                }
                return this.frequencyRepository;
            }
        }

        public IFrequencyTransferAttachmentRepository FrequencyTransferAttachmentRepository
        {
            get
            {
                if (this.frequencyTransferAttachmentRepository == null)
                {
                    this.frequencyTransferAttachmentRepository = new FrequencyTransferAttachmentRepository(DbContext);
                }
                return this.frequencyTransferAttachmentRepository;
            }
        }

        public IFrequencyTransferMemoRepository FrequencyTransferMemoRepository
        {
            get
            {
                if (this.frequencyTransferMemoRepository == null)
                {
                    this.frequencyTransferMemoRepository = new FrequencyTransferMemoRepository(DbContext);
                }
                return this.frequencyTransferMemoRepository;
            }
        }

        public IFunctionRoleRepository FunctionRoleRepository
        {
            get
            {
                if (this.functionRoleRepository == null)
                {
                    this.functionRoleRepository = new FunctionRoleRepository(DbContext);
                }
                return this.functionRoleRepository;
            }
        }

        public IGeneralActionPhaseRepository GeneralActionPhaseRepository
        {
            get
            {
                if (this.generalactionphaseRepository == null)
                {
                    this.generalactionphaseRepository = new GeneralActionPhaseRepository(DbContext);
                }
                return this.generalactionphaseRepository;
            }
        }

        public IGeneralActionRepository GeneralActionRepository
        {
            get
            {
                if (this.generalactionRepository == null)
                {
                    this.generalactionRepository = new GeneralActionRepository(DbContext);
                }
                return this.generalactionRepository;
            }
        }

        public IGeneralActionTypeRepository GeneralActionTypeRepository
        {
            get
            {
                if (this.generalactiontypeRepository == null)
                {
                    this.generalactiontypeRepository = new GeneralActionTypeRepository(DbContext);
                }
                return this.generalactiontypeRepository;
            }
        }

        public IGPSCoordinaatRepository GPSCoordinaatRepository
        {
            get
            {
                if (this.gpsCoordinaatRepository == null)
                {
                    this.gpsCoordinaatRepository = new GPSCoordinaatRepository(DbContext);
                }
                return this.gpsCoordinaatRepository;
            }
        }

        public IGPSGemeenteRepository GPSGemeenteRepository
        {
            get
            {
                if (this.gpsGemeenteRepository == null)
                {
                    this.gpsGemeenteRepository = new GPSGemeenteRepository(DbContext);
                }
                return this.gpsGemeenteRepository;
            }
        }

        public IGPSPlaatsRepository GPSPlaatsRepository
        {
            get
            {
                if (this.gpsPlaatsRepository == null)
                {
                    this.gpsPlaatsRepository = new GPSPlaatsRepository(DbContext);
                }
                return this.gpsPlaatsRepository;
            }
        }

        public IGPSProvincieRepository GPSProvincieRepository
        {
            get
            {
                if (this.gpsProvincieRepository == null)
                {
                    this.gpsProvincieRepository = new GPSProvincieRepository(DbContext);
                }
                return this.gpsProvincieRepository;
            }
        }

        public IHealthAidDeviceRepository HealthAidDeviceRepository
        {
            get
            {
                if (this.healthAidDeviceRepository == null)
                    this.healthAidDeviceRepository = new HealthAidDeviceRepository(DbContext);

                return this.healthAidDeviceRepository;
            }
        }

        public IHealthInsurerRepository HealthInsurerRepository
        {
            get
            {
                if (this.healthinsurerRepository == null)
                {
                    this.healthinsurerRepository = new HealthInsurerRepository(DbContext);
                }
                return this.healthinsurerRepository;
            }
        }

        public IHtmlTextRepository HtmlTextRepository
        {
            get
            {
                if (this.htmltextRepository == null)
                {
                    this.htmltextRepository = new HtmlTextRepository(DbContext);
                }
                return this.htmltextRepository;
            }
        }

        public ILocationRepository LocationRepository
        {
            get
            {
                if (this.locationRepository == null)
                {
                    this.locationRepository = new LocationRepository(DbContext);
                }
                return this.locationRepository;
            }
        }

        public ILocationRegionRepository LocationRegionRepository
        {
            get
            {
                if (this.locationRegionRepository == null)
                {
                    this.locationRegionRepository = new LocationRegionRepository(DbContext);
                }
                return this.locationRegionRepository;
            }
        }

        public ILogCreateUserRepository LogCreateUserRepository
        {
            get
            {
                if (this.logCreateUserRepository == null)
                {
                    this.logCreateUserRepository = new LogCreateUserRepository(DbContext);
                }
                return this.logCreateUserRepository;
            }
        }

        public ILogDepartmentRepository LogDepartmentRepository
        {
            get
            {
                if (this.logDepartmentRepository == null)
                {
                    this.logDepartmentRepository = new LogDepartmentRepository(DbContext);
                }

                return this.logDepartmentRepository;
            }
        }

        public ILoggingRepository LoggingRepository
        {
            get
            {
                if (this.loggingRepository == null)
                {
                    this.loggingRepository = new LoggingRepository(DbContext);
                }
                return this.loggingRepository;
            }
        }

        public ILoginHistoryRepository LoginHistoryRepository
        {
            get
            {
                if (this.loginhistoryRepository == null)
                {
                    this.loginhistoryRepository = new LoginHistoryRepository(DbContext);
                }
                return this.loginhistoryRepository;
            }
        }

        public ILogLocationRepository LogLocationRepository
        {
            get
            {
                if (this.logLocationRepository == null)
                {
                    this.logLocationRepository = new LogLocationRepository(DbContext);
                }

                return this.logLocationRepository;
            }
        }

        public ILogOrganizationRepository LogOrganizationRepository
        {
            get
            {
                if (this.logOrganizationRepository == null)
                {
                    this.logOrganizationRepository = new LogOrganizationRepository(DbContext);
                }

                return this.logOrganizationRepository;
            }
        }

        public ILogPointFormServiceRepository LogPointFormServiceRepository
        {
            get
            {
                if (this.logPointFormServiceRepository == null)
                {
                    this.logPointFormServiceRepository = new LogPointFormServiceRepository(DbContext);
                }

                return this.logPointFormServiceRepository;
            }
        }

        public ILogPrivacyRepository LogPrivacyRepository
        {
            get
            {
                if (this.logPrivacyRepository == null)
                {
                    this.logPrivacyRepository = new LogPrivacyRepository(DbContext);
                }
                return this.logPrivacyRepository;
            }
        }
        

        public ILogReadRepository LogReadRepository
        {
            get
            {
                if (this.logreadRepository == null)
                {
                    this.logreadRepository = new LogReadRepository(DbContext);
                }
                return this.logreadRepository;
            }
        }

        public IMedicineCardRepository MedicineCardRepository
        {
            get
            {
                if (this.medicinecardRepository == null)
                {
                    this.medicinecardRepository = new MedicineCardRepository(DbContext);
                }
                return this.medicinecardRepository;
            }
        }

        public IMedicineRepository MedicineRepository
        {
            get
            {
                if (this.medicineRepository == null)
                {
                    this.medicineRepository = new MedicineRepository(DbContext);
                }
                return this.medicineRepository;
            }
        }

        public IMessageReceivedTempRepository MessageReceivedTempRepository
        {
            get
            {
                if (this.messageReceivedTempRepository == null)
                {
                    this.messageReceivedTempRepository = new MessageReceivedTempRepository(DbContext);
                }
                return this.messageReceivedTempRepository;
            }
        }

        public IMutActionHealthInsurerRepository MutActionHealthInsurerRepository
        {
            get
            {
                if (this.mutActionHealthInsurerRepository == null)
                {
                    this.mutActionHealthInsurerRepository = new MutActionHealthInsurerRepository(DbContext);
                }
                return this.mutActionHealthInsurerRepository;
            }
        }

        public IMutClientRepository MutClientRepository
        {
            get
            {
                if (this.mutClientRepository == null)
                    this.mutClientRepository = new MutClientRepository(DbContext);

                return this.mutClientRepository;
            }
        }

        public IMutDepartmentRepository MutDepartmentRepository
        {
            get
            {
                if (this.mutDepartmentRepository == null)
                {
                    this.mutDepartmentRepository = new MutDepartmentRepository(DbContext);
                }

                return this.mutDepartmentRepository;
            }
        }

        public IMutEmployeeRepository MutEmployeeRepository
        {
            get
            {
                if (this.mutEmployeeRepository == null)
                {
                    this.mutEmployeeRepository = new MutEmployeeRepository(DbContext);
                }

                return this.mutEmployeeRepository;
            }
        }

        public IMutFlowFieldValueRepository MutFlowFieldValueRepository
        {
            get
            {
                if (this.mutFlowFieldValueRepository == null)
                {
                    this.mutFlowFieldValueRepository = new MutFlowFieldValueRepository(DbContext);
                }
                return this.mutFlowFieldValueRepository;
            }
        }

        public IMutFormSetVersionRepository MutFormSetVersionRepository
        {
            get
            {
                if (this.mutFormSetVersionRepository == null)
                {
                    this.mutFormSetVersionRepository = new MutFormSetVersionRepository(DbContext);
                }
                return this.mutFormSetVersionRepository;
            }
        }

        public IMutLocationRepository MutLocationRepository
        {
            get
            {
                if (this.mutLocationRepository == null)
                {
                    this.mutLocationRepository = new MutLocationRepository(DbContext);
                }

                return this.mutLocationRepository;
            }
        }

        public IMutMedicineCardRepository MutMedicineCardRepository
        {
            get
            {
                if (this.mutMedicineCardRepository == null)
                {
                    this.mutMedicineCardRepository = new MutMedicineCardRepository(DbContext);
                }
                return this.mutMedicineCardRepository;
            }
        }

        public IMutOrganizationRepository MutOrganizationRepository
        {
            get
            {
                if (this.mutOrganizationRepository == null)
                {
                    this.mutOrganizationRepository = new MutOrganizationRepository(DbContext);
                }

                return this.mutOrganizationRepository;
            }
        }

        public IMutTransferTaskRepository MutTransferTaskRepository
        {
            get
            {
                if (this.muttransfertaskrepository == null)
                {
                    this.muttransfertaskrepository = new MutTransferTaskRepository(DbContext);
                }
                return this.muttransfertaskrepository;
            }
        }

        public INationalityRepository NationalityRepository
        {
            get
            {
                if (this.nationalityRepository == null)
                {
                    this.nationalityRepository = new NationalityRepository(DbContext);
                }
                return this.nationalityRepository;
            }
        }

        public IOrganizationCSVTemplateRepository OrganizationCSVTemplateRepository
        {
            get
            {
                if (this.organizationCSVTemplateRepository == null)
                {
                    this.organizationCSVTemplateRepository = new OrganizationCSVTemplateRepository(DbContext);
                }
                return this.organizationCSVTemplateRepository;
            }
        }

        public IOrganizationCSVTemplateColumnRepository OrganizationCSVTemplateColumnRepository
        {
            get
            {
                if (this.organizationCSVTemplateColumnRepository == null)
                {
                    this.organizationCSVTemplateColumnRepository = new OrganizationCSVTemplateColumnRepository(DbContext);
                }
                return this.organizationCSVTemplateColumnRepository;
            }
        }

        public IOrganizationInviteRepository OrganizationInviteRepository
        {
            get
            {
                if (this.organizationInviteRepository == null)
                {
                    this.organizationInviteRepository = new OrganizationInviteRepository(DbContext);
                }
                return this.organizationInviteRepository;
            }
        }

        public IOrganizationLogoRepository OrganizationLogoRepository
        {
            get
            {
                if (this.organizationLogoRepository == null)
                {
                    this.organizationLogoRepository = new OrganizationLogoRepository(DbContext);
                }
                return this.organizationLogoRepository;
            }
        }

        public IOrganizationOutsidePointRepository OrganizationOutsidePointRepository
        {
            get
            {
                if (this.organizationoutsidepointRepository == null)
                {
                    this.organizationoutsidepointRepository = new OrganizationOutsidePointRepository(DbContext);
                }
                return this.organizationoutsidepointRepository;
            }
        }

        public IOrganizationProjectRepository OrganizationProjectRepository
        {
            get
            {
                if (this.organizationProjectRepository == null)
                {
                    this.organizationProjectRepository = new OrganizationProjectRepository(DbContext);
                }
                return this.organizationProjectRepository;
            }
        }

        public IOrganizationRepository OrganizationRepository
        {
            get
            {
                if (this.organizationRepository == null)
                {
                    this.organizationRepository = new OrganizationRepository(DbContext);
                }
                return this.organizationRepository;
            }
        }

        public IOrganizationSearchParticipationRepository OrganizationSearchParticipationRepository
        {
            get
            {
                if (this.organizationSearchParticipationRepository == null)
                {
                    this.organizationSearchParticipationRepository = new OrganizationSearchParticipationRepository(DbContext);
                }
                return this.organizationSearchParticipationRepository;
            }
        }

        public IOrganizationSearchPostalCodeRepository OrganizationSearchPostalCodeRepository
        {
            get
            {
                if (this.organizationSearchPostalCodeRepository == null)
                {
                    this.organizationSearchPostalCodeRepository = new OrganizationSearchPostalCodeRepository(DbContext);
                }
                return this.organizationSearchPostalCodeRepository;
            }
        }

        public IOrganizationSearchRegionRepository OrganizationSearchRegionRepository
        {
            get
            {
                if (this.organizationSearchRegionRepository == null)
                {
                    this.organizationSearchRegionRepository = new OrganizationSearchRegionRepository(DbContext);
                }
                return this.organizationSearchRegionRepository;
            }
        }

        public IOrganizationSearchRepository OrganizationSearchRepository
        {
            get
            {
                if (this.organizationSearchRepository == null)
                {
                    this.organizationSearchRepository = new OrganizationSearchRepository(DbContext);
                }
                return this.organizationSearchRepository;
            }
        }

        public IOrganizationSettingRepository OrganizationSettingRepository
        {
            get
            {
                if (this.organizationSettingRepository == null)
                {
                    this.organizationSettingRepository = new OrganizationSettingRepository(DbContext);
                }
                return this.organizationSettingRepository;
            }
        }

        public IOrganizationTypeRepository OrganizationTypeRepository
        {
            get
            {
                if (this.organizationtypeRepository == null)
                {
                    this.organizationtypeRepository = new OrganizationTypeRepository(DbContext);
                }
                return this.organizationtypeRepository;
            }
        }

        public IPageLockRepository PageLockRepository
        {
            get
            {
                if (this.pagelockRepository == null)
                {
                    this.pagelockRepository = new PageLockRepository(DbContext);
                }
                return this.pagelockRepository;
            }
        }

        public IPatientReceivedTempRepository PatientReceivedTempRepository
        {
            get
            {
                if (this.patientReceivedTempRepository == null)
                    this.patientReceivedTempRepository = new PatientReceivedTempRepository(DbContext);

                return this.patientReceivedTempRepository;
            }
        }

        public IPatientRequestTempRepository PatientRequestTempRepository
        {
            get
            {
                if (this.patientrequesttemprepository == null)
                {
                    this.patientrequesttemprepository = new PatientRequestTempRepository(DbContext);
                }
                return this.patientrequesttemprepository;
            }
        }

        public IPersonDataRepository PersonDataRepository
        {
            get
            {
                if (this.personDataRepository == null)
                {
                    this.personDataRepository = new PersonDataRepository(DbContext);
                }
                return this.personDataRepository;
            }
        }

        public IPhaseDefinitionRepository PhaseDefinitionRepository
        {
            get
            {
                if (this.phasedefinitionRepository == null)
                {
                    this.phasedefinitionRepository = new PhaseDefinitionRepository(DbContext);
                }
                return this.phasedefinitionRepository;
            }
        }

        public IPhaseDefinitionNavigationRepository PhaseDefinitionNavigationRepository
        {
            get
            {
                if (this.phasedefinitionNavigationRepository == null)
                {
                    this.phasedefinitionNavigationRepository = new PhaseDefinitionNavigationRepository(DbContext);
                }
                return this.phasedefinitionNavigationRepository;
            }
        }

        public IPhaseDefinitionRightsRepository PhaseDefinitionRightsRepository
        {
            get
            {
                if (this.phaseDefinitionRightsRepositoryRepository == null)
                {
                    this.phaseDefinitionRightsRepositoryRepository = new PhaseDefinitionRightsRepository(DbContext);
                }
                return this.phaseDefinitionRightsRepositoryRepository;
            }
        }

        public IPhaseInstanceRepository PhaseInstanceRepository
        {
            get
            {
                if (this.phaseinstanceRepository == null)
                {
                    this.phaseinstanceRepository = new PhaseInstanceRepository(DbContext);
                }
                return this.phaseinstanceRepository;
            }
        }

        public IPostalCodeRepository PostalCodeRepository
        {
            get
            {
                if (this.postalcodeRepository == null)
                {
                    this.postalcodeRepository = new PostalCodeRepository(DbContext);
                }
                return this.postalcodeRepository;
            }
        }

        public virtual IPreferredLocationRepository PreferredLocationRepository
        {
            get
            {
                if (this.preferredLocationRepository == null)
                {
                    this.preferredLocationRepository = new PreferredLocationRepository(DbContext);
                }
                return this.preferredLocationRepository;
            }
        }

        public IRazorTemplateRepository RazorTemplateRepository
        {
            get
            {
                if (this.razorTemplateRepository == null)
                {
                    this.razorTemplateRepository = new RazorTemplateRepository(DbContext);
                }
                return this.razorTemplateRepository;
            }
        }

        public IRegionRepository RegionRepository
        {
            get
            {
                if (this.regionRepository == null)
                {
                    this.regionRepository = new RegionRepository(DbContext);
                }
                return this.regionRepository;
            }
        }

        public IRegionAfterCareTypeRepository RegionAfterCareTypeRepository
        {
            get
            {
                if (this.regionAfterCareTypeRepository == null)
                {
                    this.regionAfterCareTypeRepository = new RegionAfterCareTypeRepository(DbContext);
                }
                return this.regionAfterCareTypeRepository;
            }
        }

        public IReportDefinitionRepository ReportDefinitionRepository
        {
            get
            {
                if (this.reportDefinitionRepository == null)
                    this.reportDefinitionRepository = new ReportDefinitionRepository(DbContext);

                return this.reportDefinitionRepository;
            }
        }

        public IScheduleHistoryRepository ScheduleHistoryRepository
        {
            get
            {
                if (this.scheduleHistoryRepository == null)
                {
                    this.scheduleHistoryRepository = new ScheduleHistoryRepository(DbContext);
                }
                return this.scheduleHistoryRepository;
            }
        }

        public IScreenRepository ScreenRepository
        {
            get
            {
                if (this.screenRepository == null)
                {
                    this.screenRepository = new ScreenRepository(DbContext);
                }
                return this.screenRepository;
            }
        }

        public ISearchUIConfigurationRepository SearchUIConfigurationRepository
        {
            get
            {
                if (this.searchUIConfigurationRepository == null)
                {
                    this.searchUIConfigurationRepository = new SearchUIConfigurationRepository(DbContext);
                }
                return this.searchUIConfigurationRepository;
            }
        }

        public ISearchUIFieldConfigurationRepository SearchUIFieldConfigurationRepository
        {
            get
            {
                if (this.searchUIFieldConfigurationRepository == null)
                {
                    this.searchUIFieldConfigurationRepository = new SearchUIFieldConfigurationRepository(DbContext);
                }
                return this.searchUIFieldConfigurationRepository;
            }
        }

        public ISendAttachmentRepository SendAttachmentRepository
        {
            get
            {
                if (this.sendAttachmentRepository == null)
                {
                    this.sendAttachmentRepository = new SendAttachmentRepository(DbContext);
                }
                return this.sendAttachmentRepository;
            }
        }

        public ISendFormTypeRepository SendFormTypeRepository
        {
            get
            {
                if (this.sendFormTypeRepository == null)
                {
                    this.sendFormTypeRepository = new SendFormTypeRepository(DbContext);
                }
                return this.sendFormTypeRepository;
            }
        }

        public IServiceAreaPostalCodeRepository ServiceAreaPostalCodeRepository
        {
            get
            {
                if (this.serviceareapostalcodeRepository == null)
                {
                    this.serviceareapostalcodeRepository = new ServiceAreaPostalCodeRepository(DbContext);
                }
                return this.serviceareapostalcodeRepository;
            }
        }

        public IServiceAreaRepository ServiceAreaRepository
        {
            get
            {
                if (this.serviceareaRepository == null)
                {
                    this.serviceareaRepository = new ServiceAreaRepository(DbContext);
                }
                return this.serviceareaRepository;
            }
        }

        public IServiceBusLogRepository ServiceBusLogRepository
        {
            get
            {
                if (this.serviceBusLogRepository == null)
                {
                    this.serviceBusLogRepository = new ServiceBusLogRepository(DbContext);
                }

                return this.serviceBusLogRepository;
            }
        }

        public ISignaleringDestinationRepository SignaleringDestinationRepository
        {
            get
            {
                if (this.signaleringDestinationRepository == null)
                {
                    this.signaleringDestinationRepository = new SignaleringDestinationRepository(DbContext);
                }

                return this.signaleringDestinationRepository;
            }
        }

        public ISignaleringRepository SignaleringRepository
        {
            get
            {
                if (this.signaleringRepository == null)
                {
                    this.signaleringRepository = new SignaleringRepository(DbContext);
                }

                return this.signaleringRepository;
            }
        }

        public ISignaleringTargetRepository SignaleringTargetRepository
        {
            get
            {
                if (this.signaleringTargetRepository == null)
                {
                    this.signaleringTargetRepository = new SignaleringTargetRepository(DbContext);
                }

                return this.signaleringTargetRepository;
            }
        }

        public ISignaleringTriggerRepository SignaleringTriggerRepository
        {
            get
            {
                if (this.signaleringTriggerRepository == null)
                {
                    this.signaleringTriggerRepository = new SignaleringTriggerRepository(DbContext);
                }

                return this.signaleringTriggerRepository;
            }
        }

        public ISignedPhaseInstanceRepository SignedPhaseInstanceRepository
        {
            get
            {
                if (this.signedphaseinstancerepository == null)
                {
                    this.signedphaseinstancerepository = new SignedPhaseInstanceRepository(DbContext);
                }
                return this.signedphaseinstancerepository;
            }
        }

        public ISpecialismRepository SpecialismRepository
        {
            get
            {
                if (this.specialismRepository == null)
                {
                    this.specialismRepository = new SpecialismRepository(DbContext);
                }
                return this.specialismRepository;
            }
        }

        public ISupplierRepository SupplierRepository
        {
            get
            {
                if (this.supplierRepository == null)
                {
                    this.supplierRepository = new SupplierRepository(DbContext);
                }
                return this.supplierRepository;
            }
        }
        public ISystemMessageReadRepository SystemMessageReadRepository
        {
            get
            {
                if (this.systemmessagereadRepository == null)
                {
                    this.systemmessagereadRepository = new SystemMessageReadRepository(DbContext);
                }
                return this.systemmessagereadRepository;
            }
        }

        public ISystemMessageRepository SystemMessageRepository
        {
            get
            {
                if (this.systemmessageRepository == null)
                {
                    this.systemmessageRepository = new SystemMessageRepository(DbContext);
                }
                return this.systemmessageRepository;
            }
        }
        public ITemplateDefaultRepository TemplateDefaultRepository
        {
            get
            {
                if (this.templatedefaultRepository == null)
                {
                    this.templatedefaultRepository = new TemplateDefaultRepository(DbContext);
                }
                return this.templatedefaultRepository;
            }
        }

        public ITemplateRepository TemplateRepository
        {
            get
            {
                if (this.templateRepository == null)
                {
                    this.templateRepository = new TemplateRepository(DbContext);
                }
                return this.templateRepository;
            }
        }
        public ITemplateTypeRepository TemplateTypeRepository
        {
            get
            {
                if (this.templatetypeRepository == null)
                {
                    this.templatetypeRepository = new TemplateTypeRepository(DbContext);
                }
                return this.templatetypeRepository;
            }
        }
        public ITempletRepository TempletRepository
        {
            get
            {
                if (this.templetRepository == null)
                {
                    this.templetRepository = new TempletRepository(DbContext);
                }
                return this.templetRepository;
            }
        }
        public ITransferAttachmentRepository TransferAttachmentRepository
        {
            get
            {
                if (this.transferattachmentRepository == null)
                {
                    this.transferattachmentRepository = new TransferAttachmentRepository(DbContext);
                }
                return this.transferattachmentRepository;
            }
        }

        public ITransferMemoChangesRepository TransferMemoChangesRepository
        {
            get
            {
                if (this.transfermemochangesRepository == null)
                {
                    this.transfermemochangesRepository = new TransferMemoChangesRepository(DbContext);
                }
                return this.transfermemochangesRepository;
            }
        }

        public ITransferMemoRepository TransferMemoRepository
        {
            get
            {
                if (this.transfermemoRepository == null)
                {
                    this.transfermemoRepository = new TransferMemoRepository(DbContext);
                }
                return this.transfermemoRepository;
            }
        }

        public ITransferMemoTemplateRepository TransferMemoTemplateRepository
        {
            get
            {
                if (this.transfermemoTemplateRepository == null)
                {
                    this.transfermemoTemplateRepository = new TransferMemoTemplateRepository(DbContext);
                }
                return this.transfermemoTemplateRepository;
            }
        }

        public ITransferRepository TransferRepository
        {
            get
            {
                if (this.transferRepository == null)
                {
                    this.transferRepository = new TransferRepository(DbContext);
                }
                return this.transferRepository;
            }
        }
        public ITransferTaskRepository TransferTaskRepository
        {
            get
            {
                if (this.transfertaskrepository == null)
                {
                    this.transfertaskrepository = new TransferTaskRepository(DbContext);
                }
                return this.transfertaskrepository;
            }
        }

        public IUsedHashCodesRepository UsedHashCodesRepository
        {
            get
            {
                if (this.usedhashcodesRepository == null)
                {
                    this.usedhashcodesRepository = new UsedHashCodesRepository(DbContext);
                }
                return this.usedhashcodesRepository;
            }
        }
        public IUsedMFACodeRepository UsedMFACodeRepository
        {
            get
            {
                if (this.usedmfacodeRepository == null)
                {
                    this.usedmfacodeRepository = new UsedMFACodeRepository(DbContext);
                }
                return this.usedmfacodeRepository;
            }
        }

        public IValidationRepository ValidationRepository
        {
            get
            {
                if (this.validationRepository == null)
                {
                    this.validationRepository = new ValidationRepository(DbContext);
                }
                return this.validationRepository;
            }
        }

        public IViewTransferHistory ViewTransferHistoryRepository
        {
            get
            {
                if (this.viewtransferhistoryRepository == null)
                {
                    this.viewtransferhistoryRepository = new ViewTransferHistoryRepository(DbContext);
                }
                return this.viewtransferhistoryRepository;
            }
        }


        public IWebRequestLogRepository WebRequestLogRepository
        {
            get
            {
                if (this.webRequestLogRepository == null)
                {
                    this.webRequestLogRepository = new WebRequestLogRepository(DbContext);
                }

                return this.webRequestLogRepository;
            }
        }

        public IZorgAdviesItemGroupRepository ZorgAdviesItemGroupRepository
        {
            get
            {
                if (this.zorgadviesitemgroupRepository == null)
                {
                    this.zorgadviesitemgroupRepository = new ZorgAdviesItemGroupRepository(DbContext);
                }
                return this.zorgadviesitemgroupRepository;
            }
        }

        public IZorgAdviesItemRepository ZorgAdviesItemRepository
        {
            get
            {
                if (this.zorgadviesitemRepository == null)
                {
                    this.zorgadviesitemRepository = new ZorgAdviesItemRepository(DbContext);
                }
                return this.zorgadviesitemRepository;
            }
        }
        public IZorgAdviesItemValueRepository ZorgAdviesItemValueRepository
        {
            get
            {
                if (this.zorgadviesitemvalueRepository == null)
                {
                    this.zorgadviesitemvalueRepository = new ZorgAdviesItemValueRepository(DbContext);
                }
                return this.zorgadviesitemvalueRepository;
            }
        }

        public UnitOfWork(TDbContext context = null)
        {
            if (context == null)
            {
                DbContext = new TDbContext();
            }
            else
            {
                DbContext = context;
            }
        }
        public TDbContext DbContext { get; private set; }

        public void Save(LogEntry logentry)
        {
            var modified = DbContext.ChangeTracker.Entries().Where(p => p.State == EntityState.Modified).ToList();
            var inserted = DbContext.ChangeTracker.Entries().Where(p => p.State == EntityState.Added).ToList();
            var deleted = DbContext.ChangeTracker.Entries().Where(p => p.State == EntityState.Deleted).ToList();

            var logger = new Log.LogBL(logentry.ScreenID, logentry.EmployeeID); ;

            logger.LogDBChanges(deleted, ((IObjectContextAdapter)DbContext).ObjectContext, Log.Enums.ChangeType.Deleted);
            logger.LogDBChanges(modified, ((IObjectContextAdapter)DbContext).ObjectContext, Log.Enums.ChangeType.Modified);
            Save();
            logger.LogDBChanges(inserted, ((IObjectContextAdapter)DbContext).ObjectContext, Log.Enums.ChangeType.Added);

        }
        public void Save()
        {
            try
            {
                DbContext.SaveChanges();
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = String.Format("{0}:{1}", validationErrors.Entry.Entity.ToString(), validationError.ErrorMessage);
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
            catch (System.Data.SqlClient.SqlException sqlEx)
            {
                Exception raise = sqlEx;
                foreach (var errors in sqlEx.Errors)
                {
                    string message = String.Format("{0}:{1}", "SQL-", errors.ToString());
                    raise = new InvalidOperationException(message, raise);
                }
                throw raise;
            }
            catch (Exception ex)
            {
                Exception raise = ex;
                string message = "";

                while (ex.InnerException != null && message == "")
                {
                    ex = ex.InnerException;
                    if (ex.GetType() == typeof(System.Data.SqlClient.SqlException))
                    {
                        if (((System.Data.SqlClient.SqlException)ex).Number == 242)
                        {
                            message = "Verkeerd datum-formaat of datum ligt buiten het geldige bereik";
                        }
                    }
                }
                if (message != "")
                {
                    raise = new InvalidOperationException(message, raise);
                }
                throw raise;
            }
        }
        public void Dispose()
        {
            Dispose(true);
        }
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    this.disposed = true;
                    DbContext.Dispose();
                }
            }
        }
        ~UnitOfWork()
        {
            Dispose(true);
        }
    }
}
