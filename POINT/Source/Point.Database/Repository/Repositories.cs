﻿using Point.Database.Models;
using System.Data.Entity;
using System.Linq;

// ADDING A REPOSITORY? THEN PLEASE KEEP THE ORDER CORRECT (ALPHABETICALLY) - Thank you.
// ADDING A REPOSITORY? THEN PLEASE KEEP THE ORDER CORRECT (ALPHABETICALLY) - Thank you.
// ADDING A REPOSITORY? THEN PLEASE KEEP THE ORDER CORRECT (ALPHABETICALLY) - Thank you.
// ADDING A REPOSITORY? THEN PLEASE KEEP THE ORDER CORRECT (ALPHABETICALLY) - Thank you.

namespace Point.Database.Repository
{
    public class ActionCodeHealthInsurerFormTypeRepository : GenericRepository<ActionCodeHealthInsurerFormType>, IActionCodeHealthInsurerFormTypeRepository
    {
        public ActionCodeHealthInsurerFormTypeRepository(DbContext context) : base(context)
        {
        }
    }

    public class ActionCodeHealthInsurerListRepository : GenericRepository<ActionCodeHealthInsurerList>, IActionCodeHealthInsurerListRepository
    {
        public ActionCodeHealthInsurerListRepository(DbContext context) : base(context)
        {
        }
    }

    public class ActionCodeHealthInsurerRegioRepository : GenericRepository<ActionCodeHealthInsurerRegio>, IActionCodeHealthInsurerRegioRepository
    {
        public ActionCodeHealthInsurerRegioRepository(DbContext context) : base(context)
        {
        }
    }

    public class ActionCodeHealthInsurerRepository : GenericRepository<ActionCodeHealthInsurer>, IActionCodeHealthInsurerRepository
    {
        public ActionCodeHealthInsurerRepository(DbContext context) : base(context)
        {
        }
    }

    public class ActionGroupHealthInsurerRepository : GenericRepository<ActionGroupHealthInsurer>, IActionGroupHealthInsurerRepository
    {
        public ActionGroupHealthInsurerRepository(DbContext context) : base(context)
        {
        }
    }

    public class ActionHealthInsurerRepository : GenericRepository<ActionHealthInsurer>, IActionHealthInsurerRepository
    {
        public ActionHealthInsurerRepository(DbContext context) : base(context)
        {
        }
    }

    public class AfterCareCategoryRepository : GenericRepository<AfterCareCategory>, IAfterCareCategoryRepository
    {
        public AfterCareCategoryRepository(DbContext context) : base(context)
        {
        }
    }

    public class AfterCareFinancingRepository : GenericRepository<AfterCareFinancing>, IAfterCareFinancingRepository
    {
        public AfterCareFinancingRepository(DbContext context) : base(context)
        {
        }
    }

    public class AfterCareGroupRepository : GenericRepository<AfterCareGroup>, IAfterCareGroupRepository
    {
        public AfterCareGroupRepository(DbContext context) : base(context)
        {
        }
    }

    public class AfterCareTileGroupRepository : GenericRepository<AfterCareTileGroup>, IAfterCareTileGroupRepository
    {
        public AfterCareTileGroupRepository(DbContext context) : base(context)
        {
        }
    }

    public class AfterCareTypeRepository : GenericRepository<AfterCareType>, IAfterCareTypeRepository
    {
        public AfterCareTypeRepository(DbContext context) : base(context)
        {
        }
    }

    public class AidProductGroupRepository : GenericRepository<AidProductGroup>, IAidProductGroupRepository
    {
        public AidProductGroupRepository(DbContext context) : base(context)
        {
        }
    }

    public class AidProductOrderAnswerRepository : GenericRepository<AidProductOrderAnswer>, IAidProductOrderAnswerRepository
    {
        public AidProductOrderAnswerRepository(DbContext context) : base(context)
        {
        }
    }

    public class AidProductOrderItemRepository : GenericRepository<AidProductOrderItem>, IAidProductOrderItemRepository
    {
        public AidProductOrderItemRepository(DbContext context) : base(context)
        {
        }
    }

    public class AidProductRepository : GenericRepository<AidProduct>, IAidProductRepository
    {
        public AidProductRepository(DbContext context) : base(context)
        {
        }
    }

    public class Aspnet_ApplicationsRepository : GenericRepository<Aspnet_Applications>, IAspnet_ApplicationsRepository
    {
        public Aspnet_ApplicationsRepository(DbContext context) : base(context)
        {
        }
    }

    public class Aspnet_MembershipRepository : GenericRepository<Aspnet_Membership>, IAspnet_MembershipRepository
    {
        public Aspnet_MembershipRepository(DbContext context) : base(context)
        {
        }
    }

    public class Aspnet_RolesRepository : GenericRepository<Aspnet_Roles>, IAspnet_RolesRepository
    {
        public Aspnet_RolesRepository(DbContext context) : base(context)
        {
        }
    }

    public class Aspnet_SchemaVersionsRepository : GenericRepository<Aspnet_SchemaVersions>, IAspnet_SchemaVersionsRepository
    {
        public Aspnet_SchemaVersionsRepository(DbContext context) : base(context)
        {
        }
    }

    public class Aspnet_UsersRepository : GenericRepository<Aspnet_Users>, IAspnet_UsersRepository
    {
        public Aspnet_UsersRepository(DbContext context) : base(context)
        {
        }
    }

    public class AttachmentReceivedTempRepository : GenericRepository<AttachmentReceivedTemp>, IAttachmentReceivedTempRepository
    {
        public AttachmentReceivedTempRepository(DbContext context) : base(context)
        {
        }
    }

    public class AutoCreateSetRepository : GenericRepository<AutoCreateSet>, IAutoCreateSetRepository
    {
        public AutoCreateSetRepository(DbContext context) : base(context)
        {
        }
    }

    public class AutoCreateSetDepartmentRepository : GenericRepository<AutoCreateSetDepartment>, IAutoCreateSetDepartmentRepository
    {
        public AutoCreateSetDepartmentRepository(DbContext context) : base(context)
        {
        }
    }

    

    public class CIZRelationRepository : GenericRepository<CIZRelation>, ICIZRelationRepository
    {
        public CIZRelationRepository(DbContext context) : base(context)
        {
        }
    }

    public class ClientReceivedTempHL7Repository : GenericRepository<ClientReceivedTempHL7>, IClientReceivedTempHL7Repository
    {
        public ClientReceivedTempHL7Repository(DbContext context) : base(context)
        {
        }
    }

    public class ClientReceivedTempRepository : GenericRepository<ClientReceivedTemp>, IClientReceivedTempRepository
    {
        public ClientReceivedTempRepository(DbContext context) : base(context)
        {
        }
    }

    public class ClientRepository : GenericRepository<Client>, IClientRepository
    {
        public ClientRepository(DbContext context) : base(context)
        {
        }
    }

    public class CommunicationLogRepository : GenericRepository<CommunicationLog>, ICommunicationLogRepository
    {
        public CommunicationLogRepository(DbContext context) : base(context)
        {
        }
    }

    public class CommunicationQueueRepository : GenericRepository<CommunicationQueue>, ICommunicationQueueRepository
    {
        public CommunicationQueueRepository(DbContext context) : base(context)
        {
        }
    }

    public class ContactPersonRepository : GenericRepository<ContactPerson>, IContactPersonRepository
    {
        public ContactPersonRepository(DbContext context) : base(context)
        {
        }
    }

    public class CryptoCertificateRepository : GenericRepository<CryptoCertificate>, ICryptoCertificateRepository
    {
        public CryptoCertificateRepository(DbContext context) : base(context)
        {
        }
    }

    public class DepartmentCapacityRepository : GenericRepository<DepartmentCapacity>, IDepartmentCapacityRepository
    {
        public DepartmentCapacityRepository(DbContext context) : base(context)
        {
        }
    }

    public class DepartmentCapacityPublicRepository : GenericRepository<DepartmentCapacityPublic>, IDepartmentCapacityPublicRepository
    {
        public DepartmentCapacityPublicRepository(DbContext context) : base(context)
        {
        }
    }

    public class DepartmentRepository : GenericRepository<Department>, IDepartmentRepository
    {
        public DepartmentRepository(DbContext context) : base(context)
        {
        }
    }

    public class DepartmentTypeRepository : GenericRepository<DepartmentType>, IDepartmentTypeRepository
    {
        public DepartmentTypeRepository(DbContext context) : base(context)
        {
        }
    }

    public class DigitalSignatureRepository : GenericRepository<DigitalSignature>, IDigitalSignatureRepository
    {
        public DigitalSignatureRepository(DbContext context) : base(context)
        {
        }
    }

    public class DoctorRepository : GenericRepository<Doctor>, IDoctorRepository
    {
        public DoctorRepository(DbContext context) : base(context)
        {
        }
    }

    public class EmployeeRepository : GenericRepository<Employee>, IEmployeeRepository
    {
        public EmployeeRepository(DbContext context) : base(context)
        {
        }
    }

    public class EmployeeDepartmentRepository : GenericRepository<EmployeeDepartment>, IEmployeeDepartmentRepository
    {
        public EmployeeDepartmentRepository(DbContext context) : base(context)
        {
        }
    }

    public class EndpointConfigurationRepository : GenericRepository<EndpointConfiguration>, IEndpointConfigurationRepository
    {
        public EndpointConfigurationRepository(DbContext context) : base(context)
        {
        }
    }

    public class FieldReceivedTempRepository : GenericRepository<FieldReceivedTemp>, IFieldReceivedTempRepository
    {
        public FieldReceivedTempRepository(DbContext context) : base(context)
        {
        }
    }

    public class FieldReceivedValueMappingRepository : GenericRepository<FieldReceivedValueMapping>, IFieldReceivedValueMappingRepository
    {
        public FieldReceivedValueMappingRepository(DbContext context) : base(context)
        {
        }
    }

    public class FlowDefinitionParticipationRepository : GenericRepository<FlowDefinitionParticipation>, IFlowDefinitionParticipationRepository
    {
        public FlowDefinitionParticipationRepository(DbContext context) : base(context)
        {
        }
    }

    public class FlowDefinitionRepository : GenericRepository<FlowDefinition>, IFlowDefinitionRepository
    {
        public FlowDefinitionRepository(DbContext context) : base(context)
        {
        }
    }

    public class FlowFieldAttributeExceptionRepository : GenericRepository<FlowFieldAttributeException>, IFlowFieldAttributeExceptionRepository
    {
        public FlowFieldAttributeExceptionRepository(DbContext context) : base(context)
        {
        }
    }

    public class FlowFieldAttributeRepository : GenericRepository<FlowFieldAttribute>, IFlowFieldAttributeRepository
    {
        public FlowFieldAttributeRepository(DbContext context) : base(context)
        {
        }
    }

    public class FlowFieldDashboardRepository : GenericRepository<FlowFieldDashboard>, IFlowFieldDashboardRepository
    {
        public FlowFieldDashboardRepository(DbContext context) : base(context)
        {
        }
    }

    public class FlowFieldDataSourceRepository : GenericRepository<FlowFieldDataSource>, IFlowFieldDataSourceRepository
    {
        public FlowFieldDataSourceRepository(DbContext context) : base(context)
        {
        }
    }

    public class FlowFieldRepository : GenericRepository<FlowField>, IFlowFieldRepository
    {
        public FlowFieldRepository(DbContext context) : base(context)
        {
        }
    }

    public class FlowFieldValueRepository : GenericRepository<FlowFieldValue>, IFlowFieldValueRepository
    {
        public FlowFieldValueRepository(DbContext context) : base(context)
        {
        }
    }

    public class FlowInstanceDoorlopendDossierSearchValuesRepository : GenericRepository<FlowInstanceDoorlopendDossierSearchValues>, IFlowInstanceDoorlopendDossierSearchValuesRepository
    {
        public FlowInstanceDoorlopendDossierSearchValuesRepository(DbContext context) : base(context)
        {
        }
    }

    public class FlowInstanceOrganizationRepository : GenericRepository<FlowInstanceOrganization>, IFlowInstanceOrganizationRepository
    {
        public FlowInstanceOrganizationRepository(DbContext context) : base(context)
        {
        }
    }

    public class FlowInstanceRepository : GenericRepository<FlowInstance>, IFlowInstanceRepository
    {
        public FlowInstanceRepository(DbContext context) : base(context)
        {
        }
    }

    public class FlowInstanceTagRepository : GenericRepository<FlowInstanceTag>, IFlowInstanceTagRepository
    {
        public FlowInstanceTagRepository(DbContext context) : base(context)
        {
        }
    }

    public class FlowDefinitionFormTypeFlowFieldRepository : GenericRepository<FlowDefinitionFormTypeFlowField>, IFlowDefinitionFormTypeFlowFieldRepository
    {
        public FlowDefinitionFormTypeFlowFieldRepository(DbContext context) : base(context)
        {
        }
    }
    
    public class FlowInstanceReportValuesProjectsRepository : GenericRepository<FlowInstanceReportValuesProjects>, IFlowInstanceReportValuesProjectsRepository
    {
        public FlowInstanceReportValuesProjectsRepository(DbContext context) : base(context)
        {
        }
    }

    public class IguanaChannelRepository : GenericRepository<IguanaChannel>, IIguanaChannelRepository
    {
        public IguanaChannelRepository(DbContext context) : base(context)
        {
        }
    }

    public class FlowInstanceReportValuesAttachmentDetailRepository : GenericRepository<FlowInstanceReportValuesAttachmentDetail>, IFlowInstanceReportValuesAttachmentDetailRepository
    {
        public FlowInstanceReportValuesAttachmentDetailRepository(DbContext context) : base(context)
        {
        }
    }

    public class FlowInstanceReportValuesRepository : GenericRepository<FlowInstanceReportValues>, IFlowInstanceReportValuesRepository
    {
        public FlowInstanceReportValuesRepository(DbContext context) : base(context)
        {
        }
    }

    public class FlowInstanceReportValuesDumpRepository : GenericRepository<FlowInstanceReportValuesDump>, IFlowInstanceReportValuesDumpRepository
    {
        public FlowInstanceReportValuesDumpRepository(DbContext context) : base(context)
        {
        }
    }

    public class FlowInstanceSearchValuesRepository : GenericRepository<FlowInstanceSearchValues>, IFlowInstanceSearchValuesRepository
    {
        public FlowInstanceSearchValuesRepository(DbContext context) : base(context)
        {
        }
    }

    public class FlowInstanceStatusRepository : GenericRepository<FlowInstanceStatus>, IFlowInstanceStatusRepository
    {
        public FlowInstanceStatusRepository(DbContext context) : base(context)
        {
        }
    }

    public class FlowPhaseAttributeRepository : GenericRepository<FlowPhaseAttribute>, IFlowPhaseAttributeRepository
    {
        public FlowPhaseAttributeRepository(DbContext context) : base(context)
        {
        }
    }

    public class FlowTypeRepository : GenericRepository<FlowType>, IFlowTypeRepository
    {
        public FlowTypeRepository(DbContext context) : base(context)
        {
        }
    }

    public class FormSetVersionRepository : GenericRepository<FormSetVersion>, IFormSetVersionRepository
    {
        public FormSetVersionRepository(DbContext context) : base(context)
        {
        }
    }

    public class FormTypeOrganizationRepository : GenericRepository<FormTypeOrganization>, IFormTypeOrganizationRepository
    {
        public FormTypeOrganizationRepository(DbContext context) : base(context)
        {
        }
    }
    public class FormTypeRegionRepository : GenericRepository<FormTypeRegion>, IFormTypeRegionRepository
    {
        public FormTypeRegionRepository(DbContext context) : base(context)
        {
        }
    }

    public class FormTypeSelectionRepository : GenericRepository<FormTypeSelection>, IFormTypeSelectionRepository
    {
        public FormTypeSelectionRepository(DbContext context) : base(context)
        {
        }
    }

    public class FormTypeSelectionOrganizationRepository : GenericRepository<FormTypeSelectionOrganization>, IFormTypeSelectionOrganizationRepository
    {
        public FormTypeSelectionOrganizationRepository(DbContext context) : base(context)
        {
        }
    }

    public class FlowDefinitionFormTypeRepository : GenericRepository<FlowDefinitionFormType>, IFlowDefinitionFormTypeRepository
    {
        public FlowDefinitionFormTypeRepository(DbContext context) : base(context)
        {
        }
    }

    public class FormTypeRepository : GenericRepository<FormType>, IFormTypeRepository
    {
        public FormTypeRepository(DbContext context) : base(context)
        {
        }
    }

    public class FrequencyRepository : GenericRepository<Frequency>, IFrequencyRepository
    {
        public FrequencyRepository(DbContext context) : base(context)
        {
        }
    }

    public class FrequencyTransferAttachmentRepository : GenericRepository<FrequencyTransferAttachment>, IFrequencyTransferAttachmentRepository
    {
        public FrequencyTransferAttachmentRepository(DbContext context) : base(context)
        {
        }
    }

    public class FrequencyTransferMemoRepository : GenericRepository<FrequencyTransferMemo>, IFrequencyTransferMemoRepository
    {
        public FrequencyTransferMemoRepository(DbContext context) : base(context)
        {
        }
    }

    public class FunctionRoleRepository : GenericRepository<FunctionRole>, IFunctionRoleRepository
    {
        public FunctionRoleRepository(DbContext context) : base(context)
        {
        }
    }

    public class GeneralActionPhaseRepository : GenericRepository<GeneralActionPhase>, IGeneralActionPhaseRepository
    {
        public GeneralActionPhaseRepository(DbContext context) : base(context)
        {
        }
    }

    public class GeneralActionRepository : GenericRepository<GeneralAction>, IGeneralActionRepository
    {
        public GeneralActionRepository(DbContext context) : base(context)
        {
        }
    }

    public class GeneralActionTypeRepository : GenericRepository<GeneralActionType>, IGeneralActionTypeRepository
    {
        public GeneralActionTypeRepository(DbContext context) : base(context)
        {
        }
    }

    public class GPSCoordinaatRepository : GenericRepository<GPSCoordinaat>, IGPSCoordinaatRepository
    {
        public GPSCoordinaatRepository(DbContext context) : base(context)
        {
        }
    }

    public class GPSGemeenteRepository : GenericRepository<GPSGemeente>, IGPSGemeenteRepository
    {
        public GPSGemeenteRepository(DbContext context) : base(context)
        {
        }
    }

    public class GPSPlaatsRepository : GenericRepository<GPSPlaats>, IGPSPlaatsRepository
    {
        public GPSPlaatsRepository(DbContext context) : base(context)
        {
        }
    }

    public class GPSProvincieRepository : GenericRepository<GPSProvincie>, IGPSProvincieRepository
    {
        public GPSProvincieRepository(DbContext context) : base(context)
        {
        }
    }

    public class HealthAidDeviceRepository : GenericRepository<HealthAidDevice>, IHealthAidDeviceRepository
    {
        public HealthAidDeviceRepository(DbContext context) : base(context)
        {
        }
    }

    public class HealthInsurerRepository : GenericRepository<HealthInsurer>, IHealthInsurerRepository
    {
        public HealthInsurerRepository(DbContext context) : base(context)
        {
        }
    }

    public class HtmlTextRepository : GenericRepository<HtmlText>, IHtmlTextRepository
    {
        public HtmlTextRepository(DbContext context) : base(context)
        {
        }
    }
    
    public class LocationRepository : GenericRepository<Location>, ILocationRepository
    {
        public LocationRepository(DbContext context) : base(context)
        {
        }
    }

    public class LocationRegionRepository : GenericRepository<LocationRegion>, ILocationRegionRepository
    {
        public LocationRegionRepository(DbContext context) : base(context)
        {
        }
    }

    public class LogCreateUserRepository : GenericRepository<LogCreateUser>, ILogCreateUserRepository
    {
        public LogCreateUserRepository(DbContext context) : base(context)
        {
        }
    }

    public class LogDepartmentRepository : GenericRepository<LogDepartment>, ILogDepartmentRepository
    {
        public LogDepartmentRepository(DbContext context) : base(context)
        {
        }
    }

    public class LoggingRepository : GenericRepository<Logging>, ILoggingRepository
    {
        public LoggingRepository(DbContext context) : base(context)
        {
        }
    }

    public class LoginHistoryRepository : GenericRepository<LoginHistory>, ILoginHistoryRepository
    {
        public LoginHistoryRepository(DbContext context) : base(context)
        {
        }
    }

    public class LogLocationRepository : GenericRepository<LogLocation>, ILogLocationRepository
    {
        public LogLocationRepository(DbContext context) : base(context)
        {
        }
    }

    public class LogOrganizationRepository : GenericRepository<LogOrganization>, ILogOrganizationRepository
    {
        public LogOrganizationRepository(DbContext context) : base(context)
        {
        }
    }

    public class LogPointFormServiceRepository : GenericRepository<LogPointFormService>, ILogPointFormServiceRepository
    {
        public LogPointFormServiceRepository(DbContext context) : base(context) { }
    }

    public class LogPrivacyRepository : GenericRepository<LogPrivacy>, ILogPrivacyRepository
    {
        public LogPrivacyRepository(DbContext context) : base(context) { }
    }

    public class LogReadRepository : GenericRepository<LogRead>, ILogReadRepository
    {
        public LogReadRepository(DbContext context) : base(context)
        {
        }
    }

    public class MedicineCardRepository : GenericRepository<MedicineCard>, IMedicineCardRepository
    {
        public MedicineCardRepository(DbContext context) : base(context)
        {
        }
    }

    public class MedicineRepository : GenericRepository<Medicine>, IMedicineRepository
    {
        public MedicineRepository(DbContext context) : base(context)
        {
        }
    }

    public class MessageReceivedTempRepository : GenericRepository<MessageReceivedTemp>, IMessageReceivedTempRepository
    {
        public MessageReceivedTempRepository(DbContext context) : base(context)
        {
        }
    }

    public class MutActionHealthInsurerRepository : GenericRepository<MutActionHealthInsurer>, IMutActionHealthInsurerRepository
    {
        public MutActionHealthInsurerRepository(DbContext context) : base(context)
        {
        }
    }

    public class MutClientRepository : GenericRepository<MutClient>, IMutClientRepository
    {
        public MutClientRepository(DbContext context) : base(context)
        {
        }
    }

    public class MutDepartmentRepository : GenericRepository<MutDepartment>, IMutDepartmentRepository
    {
        public MutDepartmentRepository(DbContext context) : base(context)
        {
        }
    }

    public class MutEmployeeRepository : GenericRepository<MutEmployee>, IMutEmployeeRepository
    {
        public MutEmployeeRepository(DbContext context) : base(context)
        {
        }
    }

    public class MutFlowFieldValueRepository : GenericRepository<MutFlowFieldValue>, IMutFlowFieldValueRepository
    {
        public MutFlowFieldValueRepository(DbContext context) : base(context)
        {
        }
    }

    public class MutFormSetVersionRepository : GenericRepository<MutFormSetVersion>, IMutFormSetVersionRepository
    {
        public MutFormSetVersionRepository(DbContext context) : base(context)
        {
        }
    }

    public class MutLocationRepository : GenericRepository<MutLocation>, IMutLocationRepository
    {
        public MutLocationRepository(DbContext context) : base(context)
        {
        }
    }

    public class MutMedicineCardRepository : GenericRepository<MutMedicineCard>, IMutMedicineCardRepository
    {
        public MutMedicineCardRepository(DbContext context) : base(context)
        {
        }
    }

    public class MutOrganizationRepository : GenericRepository<MutOrganization>, IMutOrganizationRepository
    {
        public MutOrganizationRepository(DbContext context) : base(context)
        {
        }
    }

    public class MutTransferTaskRepository : GenericRepository<MutTransferTask>, IMutTransferTaskRepository
    {
        public MutTransferTaskRepository(DbContext context) : base(context)
        {
        }
    }

    public class NationalityRepository : GenericRepository<Nationality>, INationalityRepository
    {
        public NationalityRepository(DbContext context) : base(context)
        {
        }
    }

    public class OrganizationInviteRepository : GenericRepository<OrganizationInvite>, IOrganizationInviteRepository
    {
        public OrganizationInviteRepository(DbContext context) : base(context)
        {
        }
    }

    public class OrganizationCSVTemplateRepository : GenericRepository<OrganizationCSVTemplate>, IOrganizationCSVTemplateRepository
    {
        public OrganizationCSVTemplateRepository(DbContext context) : base(context)
        {
        }
    }

    public class OrganizationCSVTemplateColumnRepository : GenericRepository<OrganizationCSVTemplateColumn>, IOrganizationCSVTemplateColumnRepository
    {
        public OrganizationCSVTemplateColumnRepository(DbContext context) : base(context)
        {
        }
    }

    public class OrganizationLogoRepository : GenericRepository<OrganizationLogo>, IOrganizationLogoRepository
    {
        public OrganizationLogoRepository(DbContext context) : base(context)
        {
        }
    }


    public class OrganizationOutsidePointRepository : GenericRepository<OrganizationOutsidePoint>, IOrganizationOutsidePointRepository
    {
        public OrganizationOutsidePointRepository(DbContext content) : base(content)
        {
        }
    }

    public class OrganizationProjectRepository : GenericRepository<OrganizationProject>, IOrganizationProjectRepository
    {
        public OrganizationProjectRepository(DbContext context) : base(context)
        {
        }
    }

    public class OrganizationRepository : GenericRepository<Organization>, IOrganizationRepository
    {
        public OrganizationRepository(DbContext context) : base(context)
        {
        }
    }

    public class OrganizationSearchParticipationRepository : GenericRepository<OrganizationSearchParticipation>, IOrganizationSearchParticipationRepository
    {
        public OrganizationSearchParticipationRepository(DbContext context) : base(context)
        {
        }
    }

    public class OrganizationSearchPostalCodeRepository : GenericRepository<OrganizationSearchPostalCode>, IOrganizationSearchPostalCodeRepository
    {
        public OrganizationSearchPostalCodeRepository(DbContext context) : base(context)
        {
        }
    }

    public class OrganizationSearchRegionRepository : GenericRepository<OrganizationSearchRegion>, IOrganizationSearchRegionRepository
    {
        public OrganizationSearchRegionRepository(DbContext context) : base(context)
        {
        }
    }

    public class OrganizationSearchRepository : GenericRepository<OrganizationSearch>, IOrganizationSearchRepository
    {
        public OrganizationSearchRepository(DbContext context) : base(context)
        {
        }
    }

    public class OrganizationSettingRepository : GenericRepository<OrganizationSetting>, IOrganizationSettingRepository
    {
        public OrganizationSettingRepository(DbContext context) : base(context)
        {
        }
    }

    public class OrganizationTypeRepository : GenericRepository<OrganizationType>, IOrganizationTypeRepository
    {
        public OrganizationTypeRepository(DbContext context) : base(context)
        {
        }
    }

    public class PageLockRepository : GenericRepository<PageLock>, IPageLockRepository
    {
        public PageLockRepository(DbContext context) : base(context)
        {
        }
    }

    public class PatientReceivedTempRepository : GenericRepository<PatientReceivedTemp>, IPatientReceivedTempRepository
    {
        public PatientReceivedTempRepository(DbContext context) : base(context)
        {
        }
    }

    public class PatientRequestTempRepository : GenericRepository<PatientRequestTemp>, IPatientRequestTempRepository
    {
        public PatientRequestTempRepository(DbContext context) : base(context)
        {
        }
    }

    public class PersonDataRepository : GenericRepository<PersonData>, IPersonDataRepository
    {
        public PersonDataRepository(DbContext context) : base(context)
        {
        }
    }

    public class PhaseDefinitionRepository : GenericRepository<PhaseDefinition>, IPhaseDefinitionRepository
    {
        public PhaseDefinitionRepository(DbContext context) : base(context)
        {
        }
    }

    public class PhaseDefinitionNavigationRepository : GenericRepository<PhaseDefinitionNavigation>, IPhaseDefinitionNavigationRepository
    {
        public PhaseDefinitionNavigationRepository(DbContext context) : base(context)
        {
        }
    }

    public class PhaseDefinitionRightsRepository : GenericRepository<PhaseDefinitionRights>, IPhaseDefinitionRightsRepository
    {
        public PhaseDefinitionRightsRepository(DbContext context) : base(context)
        {
        }
    }

    public class PhaseInstanceRepository : GenericRepository<PhaseInstance>, IPhaseInstanceRepository
    {
        public PhaseInstanceRepository(DbContext context) : base(context)
        {
        }
    }

    public class PostalCodeRepository : GenericRepository<PostalCode>, IPostalCodeRepository
    {
        public PostalCodeRepository(DbContext context) : base(context)
        {
        }
    }

    public class PreferredLocationRepository : GenericRepository<PreferredLocation>, IPreferredLocationRepository
    {
        public PreferredLocationRepository(DbContext context) : base(context)
        {
        }
    }

    public class RazorTemplateRepository : GenericRepository<RazorTemplate>, IRazorTemplateRepository
    {
        public RazorTemplateRepository(DbContext context) : base(context)
        {
        }
    }

    public class RegionRepository : GenericRepository<Region>, IRegionRepository
    {
        public RegionRepository(DbContext context) : base(context)
        {
        }
    }

    public class RegionAfterCareTypeRepository : GenericRepository<RegionAfterCareType>, IRegionAfterCareTypeRepository
    {
        public RegionAfterCareTypeRepository(DbContext context) : base(context)
        {
        }
    }

    public class ReportDefinitionRepository : GenericRepository<ReportDefinition>, IReportDefinitionRepository
    {
        public ReportDefinitionRepository(DbContext context) : base(context)
        {
        }
    }

    public class ScheduleHistoryRepository : GenericRepository<ScheduleHistory>, IScheduleHistoryRepository
    {
        public ScheduleHistoryRepository(DbContext context) : base(context)
        {
        }
    }

    public class ScreenRepository : GenericRepository<Screen>, IScreenRepository
    {
        public ScreenRepository(DbContext context) : base(context)
        {
        }
    }

    public class SearchUIConfigurationRepository : GenericRepository<SearchUIConfiguration>, ISearchUIConfigurationRepository
    {
        public SearchUIConfigurationRepository(DbContext context) : base(context)
        {
        }
    }

    public class SearchUIFieldConfigurationRepository : GenericRepository<SearchUIFieldConfiguration>, ISearchUIFieldConfigurationRepository
    {
        public SearchUIFieldConfigurationRepository(DbContext context) : base(context)
        {
        }
    }

    public class SendAttachmentRepository : GenericRepository<SendAttachment>, ISendAttachmentRepository
    {
        public SendAttachmentRepository(DbContext context) : base(context)
        {
        }
    }

    public class SendFormTypeRepository : GenericRepository<SendFormType>, ISendFormTypeRepository
    {
        public SendFormTypeRepository(DbContext context) : base(context)
        {
        }
    }

    public class ServiceAreaPostalCodeRepository : GenericRepository<ServiceAreaPostalCode>, IServiceAreaPostalCodeRepository
    {
        public ServiceAreaPostalCodeRepository(DbContext context) : base(context)
        {
        }
    }

    public class ServiceAreaRepository : GenericRepository<ServiceArea>, IServiceAreaRepository
    {
        public ServiceAreaRepository(DbContext context) : base(context)
        {
        }
    }

    public class ServiceBusLogRepository : GenericRepository<ServiceBusLog>, IServiceBusLogRepository
    {
        public ServiceBusLogRepository(DbContext context) : base(context)
        {
        }
    }

    public class SignaleringDestinationRepository : GenericRepository<SignaleringDestination>, ISignaleringDestinationRepository
    {
        public SignaleringDestinationRepository(DbContext context) : base(context)
        {
        }
    }

    public class SignaleringRepository : GenericRepository<Signalering>, ISignaleringRepository
    {
        public SignaleringRepository(DbContext context) : base(context)
        {
        }
    }

    public class SignaleringTargetRepository : GenericRepository<SignaleringTarget>, ISignaleringTargetRepository
    {
        public SignaleringTargetRepository(DbContext context) : base(context)
        {
        }
    }

    public class SignaleringTriggerRepository : GenericRepository<SignaleringTrigger>, ISignaleringTriggerRepository
    {
        public SignaleringTriggerRepository(DbContext context) : base(context)
        {
        }
    }

    public class SignedPhaseInstanceRepository : GenericRepository<SignedPhaseInstance>, ISignedPhaseInstanceRepository
    {
        public SignedPhaseInstanceRepository(DbContext context) : base(context)
        {
        }
    }

    public class SpecialismRepository : GenericRepository<Specialism>, ISpecialismRepository
    {
        public SpecialismRepository(DbContext context) : base(context)
        {
        }
    }

    public class SupplierRepository : GenericRepository<Supplier>, ISupplierRepository
    {
        public SupplierRepository(DbContext context) : base(context)
        {
        }
    }
    public class SystemMessageReadRepository : GenericRepository<SystemMessageRead>, ISystemMessageReadRepository
    {
        public SystemMessageReadRepository(DbContext context) : base(context)
        {
        }
    }

    public class SystemMessageRepository : GenericRepository<SystemMessage>, ISystemMessageRepository
    {
        public SystemMessageRepository(DbContext context) : base(context)
        {
        }
    }

    public class TemplateDefaultRepository : GenericRepository<TemplateDefault>, ITemplateDefaultRepository
    {
        public TemplateDefaultRepository(DbContext context) : base(context)
        {
        }
    }

    public class TemplateRepository : GenericRepository<Template>, ITemplateRepository
    {
        public TemplateRepository(DbContext context) : base(context)
        {
        }
    }

    public class TemplateTypeRepository : GenericRepository<TemplateType>, ITemplateTypeRepository
    {
        public TemplateTypeRepository(DbContext context) : base(context)
        {
        }
    }

    public class TempletRepository : GenericRepository<Templet>, ITempletRepository
    {
        public TempletRepository(DbContext context) : base(context)
        {
        }
    }

    public class TransferAttachmentRepository : GenericRepository<TransferAttachment>, ITransferAttachmentRepository
    {
        public TransferAttachmentRepository(DbContext context) : base(context)
        {
        }
    }

    public class TransferMemoChangesRepository : GenericRepository<TransferMemoChanges>, ITransferMemoChangesRepository
    {
        public TransferMemoChangesRepository(DbContext context) : base(context)
        {
        }
    }

    public class TransferMemoRepository : GenericRepository<TransferMemo>, ITransferMemoRepository
    {
        public TransferMemoRepository(DbContext context) : base(context)
        {
        }
    }

    public class TransferMemoTemplateRepository : GenericRepository<TransferMemoTemplate>, ITransferMemoTemplateRepository
    {
        public TransferMemoTemplateRepository(DbContext context) : base(context)
        {
        }
    }
    public class TransferRepository : GenericRepository<Transfer>, ITransferRepository
    {
        public TransferRepository(DbContext context) : base(context)
        {
        }
    }
    public class TransferTaskRepository : GenericRepository<TransferTask>, ITransferTaskRepository
    {
        public TransferTaskRepository(DbContext context) : base(context)
        {
        }
    }

    public class UsedHashCodesRepository : GenericRepository<UsedHashCodes>, IUsedHashCodesRepository
    {
        public UsedHashCodesRepository(DbContext context) : base(context)
        {
        }
    }

    public class UsedMFACodeRepository : GenericRepository<UsedMFACode>, IUsedMFACodeRepository
    {
        public UsedMFACodeRepository(DbContext context) : base(context)
        {
        }
    }

    public class ValidationRepository : GenericRepository<Validation>, IValidationRepository
    {
        public ValidationRepository(DbContext context) : base(context)
        {
        }
    }

    public class ViewTransferHistoryRepository : GenericRepository<ViewTransferHistory>, IViewTransferHistory
    {
        public ViewTransferHistoryRepository(DbContext context) : base(context)
        {
        }
    }

    public class WebRequestLogRepository : GenericRepository<WebRequestLog>, IWebRequestLogRepository
    {
        public WebRequestLogRepository(DbContext context) : base(context)
        {
        }
    }

    public class ZorgAdviesItemGroupRepository : GenericRepository<ZorgAdviesItemGroup>, IZorgAdviesItemGroupRepository
    {
        public ZorgAdviesItemGroupRepository(DbContext context) : base(context)
        {
        }
    }

    public class ZorgAdviesItemRepository : GenericRepository<ZorgAdviesItem>, IZorgAdviesItemRepository
    {
        public ZorgAdviesItemRepository(DbContext context) : base(context)
        {
        }
    }
    public class ZorgAdviesItemValueRepository : GenericRepository<ZorgAdviesItemValue>, IZorgAdviesItemValueRepository
    {
        public ZorgAdviesItemValueRepository(DbContext context) : base(context)
        {
        }
    }
}