﻿using Point.Business.BusinessObjects;
using Point.Business.Helper;
using Point.Business.Logic;
using Point.Database.Context;
using Point.Database.Repository;
using Point.FormService.Logic;
using Point.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace Point.FormService
{
    [WebService(Namespace = "http://verzorgdeoverdracht.nl/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    public class FormFields : WebService
    {
        // Field has been renamed and moved to Business.BusinessObject.FieldBO and
        // in order to minimize issues raised by this for the customer, a simple
        // 'alias' structure has been implemented
        public sealed class Field : Point.Business.BusinessObjects.FieldBO {
            public Field() { }
            public Field(string name, string value) : base(name, value) { }
        };

        private const string OK = "OK";
        private const string NOK = "NOK";

        private string Authenticate(Guid GUID, int OrganisationID, string SenderUserName, DateTime SendDate, string PatientNumber, string Hash)
        {
            string response = OK;
            if (!OrganizationBO.AuthenticateHash(GUID, OrganisationID, SenderUserName, SendDate, PatientNumber, Hash))
            {
                response = $"{NOK} Hash GUID=[{GUID}] OrganizationID=[{OrganisationID}] SenderUserName=[{SenderUserName}] SendDate=[{SendDate}] PatientNumber=[{PatientNumber}] Hash=[{Hash}]";
            }
            return response;
        }

        [WebMethod]
        public string PointFormFields(Guid GUID, int OrganisationID, string SenderUserName, DateTime SendDate, string PatientNumber, string VisitNumber, string Hash, List<Field> Fields)
        {
            LogPointFormServiceHelper.Log(OrganisationID, Hash, HttpContext.Current?.Request);

            string response = Authenticate(GUID, OrganisationID, SenderUserName, SendDate, PatientNumber, Hash);

            if (response == OK)
            {
                using (var uow = new UnitOfWork<PointContext>())
                {
                    try
                    {
                        var messagereceivedtemp = MessageReceivedTempBL.Insert(uow, GUID, OrganisationID, SenderUserName, SendDate, 
                            PatientNumber, VisitNumber, Hash, "FormFields", Source.WEBSERVICE);
                        FieldReceivedTempBL.Insert(uow, messagereceivedtemp, Fields.Cast<FieldBO>().ToList());
                    }
                    catch (Exception ex)
                    {
                        response = $"{NOK} {ex.InnerException}";
                        ExceptionsBL.HandleException(ex, Context);
                    }
                }
            }

            return response;
        }

        // Patient has been renamed and moved to Business.BusinessObject.PatientBO and
        // in order to minimize issues raised by this for the customer, a simple
        // 'alias' structure has been implemented
        public sealed class Patient : Point.Business.BusinessObjects.PatientBO { };

        [WebMethod]
        public string AddTransferData(Guid GUID, int OrganisationID, string SenderUserName, DateTime SendDate, string PatientNumber, string Hash, Patient Patient)
        {
            LogPointFormServiceHelper.Log(OrganisationID, Hash, HttpContext.Current?.Request);

            string response = Authenticate(GUID, OrganisationID, SenderUserName, SendDate, PatientNumber, Hash);

            if (response == OK)
            {
                using (var uow = new UnitOfWork<PointContext>())
                {
                    try
                    {
                        PatientReceivedTempBL.Delete(uow, OrganisationID, PatientNumber);
                        PatientReceivedTempBL.Insert(uow, GUID, OrganisationID, SenderUserName, SendDate, PatientNumber, Hash, Patient);
                    }
                    catch (Exception ex)
                    {
                        response = $"{NOK} {ex.InnerException}";
                        ExceptionsBL.HandleException(ex, Context);
                    }
                }
            }

            return response;
        }

        [WebMethod]
        public string AddAndCreateTransferData(Guid GUID, int OrganisationID, string SenderUserName, DateTime SendDate, string PatientNumber, string Hash, Patient Patient)
        {
            string response = AddTransferData(GUID, OrganisationID, SenderUserName, SendDate, PatientNumber, Hash, Patient);

            if (response == OK)
            {
                using (var uow = new UnitOfWork<PointContext>())
                {
                    try
                    {
                        if (response == OK)
                        {
                            var flowRequestBL = new FlowRequestBL();
                            flowRequestBL.LogRequest();

                            flowRequestBL.CreateFlowZHVVTSimpleFromWS(SenderUserName, OrganisationID, Patient.CivilServiceNumber, PatientNumber);
                            if (flowRequestBL.HaveErrors)
                            {
                                response = $"{NOK} {String.Join(",", flowRequestBL.ErrorMessages)}";
                            }
                            else if (flowRequestBL.TransferID > 0)
                            {
                                response = $"{OK} Created TransferID {flowRequestBL.TransferID}";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        response = $"{NOK} {ex.InnerException}";
                        ExceptionsBL.HandleException(ex, Context);
                    }
                }
            }

            return response;
        }

    }
}