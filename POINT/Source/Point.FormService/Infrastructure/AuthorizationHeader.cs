﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Dispatcher;
using Point.Business.Helper;

namespace Point.FormService.Infrastructure
{
    public class AuthorizationHeaderHandler : DelegatingHandler
    {
        public string Key { get; set; }

        public AuthorizationHeaderHandler(HttpConfiguration httpConfiguration)
        {
            InnerHandler = new HttpControllerDispatcher(httpConfiguration);
        }

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage requestMessage, CancellationToken cancellationToken)
        {
            List<string> errormessages = new List<string>();
            if (requestMessage.RequestUri.Scheme != Uri.UriSchemeHttps)
            {
                errormessages.Add("HTTPS required.");
            }

            if (!validateApiKey(requestMessage))
            {
                errormessages.Add("No or invalid apiKey specified.");
            }

            if (errormessages.Any())
            {
                var responsemessage = new HttpResponseMessage(HttpStatusCode.Forbidden)
                {
                    Content = new StringContent(String.Join(" ", errormessages))
                };
                var taskcompletionsource = new TaskCompletionSource<HttpResponseMessage>();
                taskcompletionsource.SetResult(responsemessage);
                return taskcompletionsource.Task;
            }

            return base.SendAsync(requestMessage, cancellationToken);

        }

        private bool validateApiKey(HttpRequestMessage requestMessage)
        {
            string apikey = null;

            if (String.IsNullOrEmpty(apikey))
            {
                IEnumerable<string> headervalues;
                if (requestMessage.Headers.TryGetValues("apiKey", out headervalues))
                {
                    apikey = headervalues.FirstOrDefault();
                }
            }

            if (String.IsNullOrEmpty(apikey))
            {
                return false;
            }
            
            var config = ApiKeyConfiguration.GetConfiguration();
            if (config == null)
            {
                return false;
            }
            var apikeyelement = config.ApiKeys.GetApiKeyElement(apikey);

            return apikeyelement != null && IPAddressHelper.IsIPAddressValid(requestMessage, apikeyelement.GetAllowedReferrers);
        }


    }
}