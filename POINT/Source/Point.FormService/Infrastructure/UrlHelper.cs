﻿using Point.Infrastructure.Helpers;

namespace Point.FormService.Infrastructure
{
    public class UrlHelper
    {
        public static string DispatchUrl(int transferID, int formTypeID)
        {
            string webserver = ConfigHelper.GetAppSettingByName<string>("WebServer");
            if (!webserver.EndsWith("/"))
            {
                webserver += "/";
            }
            
            return $"{webserver}DispatchToForm?TransferID={transferID}&FormType={formTypeID}";
        }
    }
}