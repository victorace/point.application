﻿using Point.FormService.Models;
using Point.Infrastructure.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Point.FormService.Infrastructure
{
    public class VerwijzingHelper
    {
        public string EpisodesToString(List<Episode> episodes)
        {
            if (episodes == null || !episodes.Any())
            {
                return "";
            }

            string result = "";
            foreach (var episode in episodes.OrderByDescending(ep => ep.BeginDatum))
            {
                result += $"{episode.ICPCCode} {episode.ICPCOmschrijving}".Trim() + Environment.NewLine;
                result += $"Begindatum: {episode.BeginDatum.ToPointDateDisplay()} {(episode.Probleem == true ? "Probleem!" : "")} {(episode.Attentie == true ? "Attentie!" : "")}".Trim() + Environment.NewLine;
                result += Environment.NewLine;
            }
            return result.TrimEnd(Environment.NewLine.ToCharArray());

        }

        public string MeasurementsToString(List<Measurement> measurements)
        {
            if (measurements == null || !measurements.Any())
            {
                return "";
            }

            string result = "";
            foreach (var measurementnaam in measurements.Select(m => m.Naam).Distinct().OrderBy(m => m))
            {
                result += $"{measurementnaam}:" + Environment.NewLine;
                foreach (var measurement in measurements.Where(m => m.Naam == measurementnaam).OrderByDescending(m => m.Datum))
                {
                    result += $"Meetdatum: {measurement.Datum.ToPointDateDisplay()} Meetwaarde: {measurement.Waarde} {measurement.Eenheid}" + Environment.NewLine;
                }
                result += Environment.NewLine;
            }
            return result.TrimEnd(Environment.NewLine.ToCharArray());
        }

        public string MedicationsToString(List<Medication> medications)
        {
            if (medications == null || !medications.Any())
            {
                return "";
            }

            string result = "";
            foreach(var medication in medications.OrderByDescending(m => m.VoorschrijfDatum))
            {
                result += $"{medication.Omschrijving}".Trim() + Environment.NewLine;
                if (medication.Hoeveelheid != null)
                {
                    result += $"Hoeveelheid: {medication.Hoeveelheid}".Trim() + Environment.NewLine;
                }
                if (!string.IsNullOrEmpty(medication.Gebruiksvoorschrift))
                {
                    result += $"Voorschrift: {medication.Gebruiksvoorschrift}".Trim() + Environment.NewLine;
                }
                result += $"Voorschrijfdatum: {medication.VoorschrijfDatum.ToPointDateDisplay()} Einddatum: {medication.EindDatum.ToPointDateDisplay()}".Trim() + Environment.NewLine;
                result += Environment.NewLine;
            }
            return result.TrimEnd(Environment.NewLine.ToCharArray());
        }

        public string GezondheidsplanToString(Gezondheidsplan gezondheidsplan)
        {
            if (gezondheidsplan == null || !gezondheidsplan.GezondheidsplanProblemen.Any())
            {
                return "";
            }

            string result = $"Aanmaakdatum: {gezondheidsplan.AanmaakDatum.ToPointDateDisplay()}" + Environment.NewLine + Environment.NewLine;
            foreach(var gezondheidsplanprobleem in gezondheidsplan.GezondheidsplanProblemen.OrderBy(gp => gp.Probleem).ThenByDescending(gp => gp.Prioriteit??0))
            {
                result += $"Probleem: {gezondheidsplanprobleem.Probleem}, prioriteit: {gezondheidsplanprobleem.Prioriteit}".Trim() + Environment.NewLine;
                if (!string.IsNullOrEmpty(gezondheidsplanprobleem.Doel))
                {
                    result += $"Doel: {gezondheidsplanprobleem.Doel}".Trim() + Environment.NewLine;
                }
                if (gezondheidsplanprobleem.Domeinen != null && gezondheidsplanprobleem.Domeinen.Any())
                {
                    result += string.Join(", ", gezondheidsplanprobleem.Domeinen) + Environment.NewLine;
                }
                if (gezondheidsplanprobleem.Actiepunten != null && gezondheidsplanprobleem.Actiepunten.Any())
                {
                    result += "Actiepunten:" + Environment.NewLine;
                    foreach(var actiepunt in gezondheidsplanprobleem.Actiepunten)
                    {
                        result += $"- {actiepunt}".Trim() + Environment.NewLine;
                    }
                }
                result += Environment.NewLine;
            }
            return result.TrimEnd(Environment.NewLine.ToCharArray());
        }

    }
}