﻿using System;
using System.Configuration;
using System.Runtime.Caching;

namespace Point.FormService.Infrastructure
{
    public class ApiKeyConfiguration : ConfigurationSection
    {
        public static ApiKeyConfiguration GetConfiguration(bool useCache = true)
        {
            ApiKeyConfiguration apikeyconfiguration = null;
            string cachekey = "ApiKeyConfiguration";

            if (useCache)
            {
                apikeyconfiguration = MemoryCache.Default.Get(cachekey) as ApiKeyConfiguration;
            }

            if (apikeyconfiguration == null)
            {
                apikeyconfiguration = ConfigurationManager.GetSection("apiKeyConfiguration") as ApiKeyConfiguration;
                if (apikeyconfiguration != null)
                {
                    MemoryCache.Default.Set(cachekey, apikeyconfiguration, new DateTimeOffset(DateTime.Now.AddHours(8)));
                }
            }

            return apikeyconfiguration;
        }

        [ConfigurationProperty("apiKeys", IsRequired = true)]
        public ApiKeyCollection ApiKeys
        {
            get
            {
                return this["apiKeys"] as ApiKeyCollection;
            }
        }

    }

    public class ApiKeyElement : ConfigurationElement
    {
        [ConfigurationProperty("name", IsRequired = true)]
        public string Name
        {
            get
            {
                return this["name"] as string;
            }
        }

        [ConfigurationProperty("apiKey", IsRequired = true)]
        public string ApiKey
        {
            get
            {
                return this["apiKey"] as string;
            }
        }

        // Comma-separated list of IP-addresses/IP-address-ranges
        [ConfigurationProperty("allowedReferrers", IsRequired = false)]
        public string AllowedReferrers
        {
            get
            {
                return this["allowedReferrers"] as string;
            }
        }

        public string[] GetAllowedReferrers
        {
            get
            {
                if (String.IsNullOrEmpty(AllowedReferrers))
                {
                    return new[] { "*" };
                }
                if (AllowedReferrers.Contains(","))
                {
                    return AllowedReferrers.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                }
                else if (AllowedReferrers.Contains(";"))
                {
                    return AllowedReferrers.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                }
                else
                {
                    return new[] { AllowedReferrers };
                }
            }
        }
    }

    public class ApiKeyCollection : ConfigurationElementCollection
    {
        public ApiKeyElement this[int index]
        {
            get
            {
                return BaseGet(index) as ApiKeyElement;
            }
            set
            {
                if (BaseGet(index) != null)
                {
                    BaseRemoveAt(index);
                }
                BaseAdd(index, value);
            }
        }

        public ApiKeyCollection GetAll()
        {
            return this;
        }

        public ApiKeyElement GetApiKeyElement(string apiKey)
        {
            foreach(ApiKeyElement apiKeyElement in this)
            {
                if (apiKeyElement.ApiKey.Equals(apiKey, StringComparison.InvariantCultureIgnoreCase))
                {
                    return apiKeyElement;
                }
            }
            return null;
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new ApiKeyElement();
        }
        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((ApiKeyElement)element).Name;
        }

    }
}