﻿using Point.Business.BusinessObjects;
using Point.Business.Helper;
using Point.Business.Logic;
using Point.Database.Context;
using Point.Database.Repository;
using Point.FormService.Logic;
using Point.Models.Enums;
using System;
using System.Web;
using System.Web.Services;

namespace Point.FormService
{
    [WebService(Namespace = "http://verzorgdeoverdracht.nl/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    public class Attachment : WebService
    {
        [WebMethod]
        public string AddAttachment(Guid GUID, int OrganisationID, string SenderUsername, DateTime SendDate, string PatientNumber,
            string VisitNumber, string CivilServiceNumber, DateTime Birthdate, AttachmentReceivedTempBO AttachmentReceivedTemp, string Hash)
        {
            LogPointFormServiceHelper.Log(OrganisationID, Hash, HttpContext.Current?.Request);

            var responseid = errorresponse(GUID, OrganisationID, SenderUsername, SendDate, PatientNumber, Hash);

            var response = "OK";

            if (!OrganizationBO.AuthenticateHash(GUID, OrganisationID, SenderUsername, SendDate, PatientNumber, Hash))
            {
                return $"NOK Hash {responseid}";
            }
            else
            {
                if (OrganisationID <= 0)
                {
                    return $"NOK Missing/Invalid {nameof(OrganisationID)} {responseid}";
                }

                if (string.IsNullOrWhiteSpace(PatientNumber))
                {
                    return $"NOK Missing/Invalid {nameof(PatientNumber)} {responseid}";
                }

                if (string.IsNullOrWhiteSpace(VisitNumber))
                {
                    return $"NOK Missing/Invalid {nameof(VisitNumber)} {responseid}";
                }

                if (AttachmentReceivedTemp == null)
                {
                    return $"NOK Missing/Invalid {nameof(AttachmentReceivedTemp)} {responseid}";
                }

                if (string.IsNullOrWhiteSpace(AttachmentReceivedTemp.Name) || AttachmentReceivedTemp.AttachmentTypeID == AttachmentTypeID.None
                    || string.IsNullOrWhiteSpace(AttachmentReceivedTemp.Base64EncodedData))
                {
                    return $"NOK Missing/Invalid {nameof(AttachmentReceivedTemp)} {responseid}";
                }

                using (var uow = new UnitOfWork<PointContext>())
                {
                    try
                    {
                        var systememployee = PointUserInfoHelper.GetSystemEmployee(uow);
                        var logentry = LogEntryBL.Create(FlowFormType.FormServiceAttachment, systememployee);

                        var flowinstance = FlowInstanceBL.GetByClientDetails(uow, OrganisationID, PatientNumber, VisitNumber, Birthdate);
                        var attachmentreceivedtemp = AttachmentReceivedTempBL.Insert(uow, GUID, OrganisationID, SenderUsername, SendDate, PatientNumber, VisitNumber,
                            CivilServiceNumber, Birthdate, AttachmentReceivedTemp, Hash);

                        if (attachmentreceivedtemp == null)
                        {
                            return $"NOK Insert failed {responseid}";
                        }

                        if (flowinstance != null)
                        {
                            // TODO: we don't and have never checked here if the Organization
                            // is actually configured for VO als bijlage - PP
                            if (AttachmentReceivedTemp.AttachmentTypeID == AttachmentTypeID.ExternVODocument)
                            {
                                AttachmentReceivedTempBL.HandleAsVO(uow, flowinstance, attachmentreceivedtemp, systememployee, logentry);
                            }
                            else
                            {
                                var exists = TransferAttachmentBL.ExistsAsAttachment(uow, flowinstance.TransferID, attachmentreceivedtemp);
                                TransferAttachmentBL.Upsert(uow, flowinstance.TransferID, attachmentreceivedtemp, logentry);
                                SignaleringBL.HandleSignalByFlowDefinitionAction(uow, flowinstance, "WebserviceAttachment",
                                    systememployee, $"Bijlage '{attachmentreceivedtemp.Name}' {(exists ? "gewijzigd" : "toegevoegd")}");
                            }

                            FlowInstanceSearchValuesBL.CalculateReadValuesByTransferIDAsync(uow, flowinstance.TransferID);
                        }
                    }
                    catch (Exception ex)
                    {
                        response = $"NOK Internal error {responseid}";
                        ExceptionsBL.HandleException(ex, Context);
                    }
                };
            }

            return response;
        }

        private string errorresponse(Guid GUID, int OrganisationID, string SenderUsername, DateTime SendDate, string PatientNumber, string Hash)
        {
            return $"- GUID={GUID.ToString()} OrganizationID={OrganisationID.ToString()} SenderUserName={SenderUsername} SendDate={SendDate} PatientNumber={PatientNumber} Hash={Hash}";
        }
    }
}