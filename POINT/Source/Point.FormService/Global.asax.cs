﻿using Point.Database.Context;
using Point.Infrastructure;
using System;

namespace Point.FormService
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            System.Data.Entity.Database.SetInitializer<PointContext>(null);
            System.Data.Entity.Database.SetInitializer<PointSearchContext>(null);

            RouteConfig.RegisterRoutes(System.Web.Routing.RouteTable.Routes);
            WebApiConfig.Register(System.Web.Http.GlobalConfiguration.Configuration);
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            Utils.ClearHeaders(Response);
        }
    }
}