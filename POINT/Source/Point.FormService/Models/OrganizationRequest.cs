﻿namespace Point.FormService.Models
{
    public class OrganizationRequest
    {
        public int? RegionID { get; set; }
        public string RegionName { get; set; }
        public string Name { get; set; }
    }
}