﻿using System;
using System.Collections.Generic;

namespace Point.FormService.Models
{
    public class CapacityResponse
    {
        public CapacityOrganization CapacityOrganization { get; set; } = new CapacityOrganization();
        public CapacityInfo CapacityInfo { get; set; } = new CapacityInfo();
        public CapacityAddress CapacityAddress { get; set; } = new CapacityAddress();
    }

    public class CapacityOrganization
    {
        public int OrganizationID { get; set; }
        public string OrganizationName { get; set; }
        public int LocationID { get; set; }
        public string LocationName { get; set; }
        public int DepartmentID { get; set; }
        public string DepartmentName { get; set; }
        public List<Region> Regions { get; set; }
        public string FullName
        {
            get
            {
                string fullname = OrganizationName;
                if (!LocationName.Equals("Locatie 1", StringComparison.InvariantCultureIgnoreCase))
                {
                    fullname += " " + LocationName;
                }
                if (!DepartmentName.Equals("Afdeling 1", StringComparison.InvariantCultureIgnoreCase))
                {
                    fullname += " " + DepartmentName;
                }
                return fullname;
            }
        }

        public string AfterCareType { get; set; }

        public int? CentralDepartmentID { get; set; }
    }

    public class CapacityAddress
    {
        public string City { get; set; }
        public string Postcode { get; set; }
    }

    public class CapacityInfo
    {
        public int? Day1 { get; set; }
        public int? Day2 { get; set; }
        public int? Day3 { get; set; }
        public int? Day4 { get; set; }
    }
}