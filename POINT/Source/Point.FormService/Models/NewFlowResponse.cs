﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Point.FormService.Models
{
    public class NewFlowResponse
    {
        public int TransferID { get; set; }
        public int FlowInstanceID { get; set; }
    }
}