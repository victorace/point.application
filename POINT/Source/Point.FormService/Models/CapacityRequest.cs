﻿using System;
using System.Collections.Generic;

namespace Point.FormService.Models
{
    public class CapacityRequest
    {
        public int? RegionID { get; set; }
        public string RegionName { get; set; }

        public string Name { get; set; }

        public int? AfterCareTypeID { get; set; }
        public string AfterCareTypeName { get; set; }

        public string Postcode { get; set; }
        public int? Radius { get; set; }

        public List<int> DepartmentIDs { get; set; }

        public DateTime StartDate { get; set; } // Not really used yet (10-04-'18) Since we only have 4 days
    }
}