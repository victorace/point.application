﻿namespace Point.FormService.Models
{
    public class DepartmentInfo
    {
        public int DepartmentID { get; set; }
        public string DepartmentName { get; set; }
        public string PhoneNumber { get; set; }
        public string FaxNumber { get; set; }
        public string EmailAddress { get; set; }
        public string AfterCareType { get; set; }
    }
}