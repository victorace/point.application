﻿namespace Point.FormService.Models
{
    public class Region
    {
        public int RegionID { get; set; }
        public string Name { get; set; }
        public bool CapacityBeds { get; set; }
    }
}