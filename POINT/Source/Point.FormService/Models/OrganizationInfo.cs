﻿using System.Collections.Generic;

namespace Point.FormService.Models
{
    public class OrganizationInfo
    {
        public int OrganizationID { get; set; }
        public string OrganizationName { get; set; }
        public int OrganizationTypeID { get; set; }
        public string OrganizationType { get; set; }
        public int? RegionID { get; set; }
        public List<Region> Regions { get; set; } = new List<Region>();
    }
}