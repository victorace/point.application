﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Point.FormService.Models
{
    public class EmployeeDepartmentRequest
    {
        [Required]
        public int EmployeeID { get; set; }
        [Required]
        public int OrganizationID { get; set; }
        [Required]
        public List<string> DepartmentExternIDs { get; set; } = new List<string>();
    }
}