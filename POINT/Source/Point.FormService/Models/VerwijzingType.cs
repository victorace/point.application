﻿namespace Point.FormService.Models
{
    public class VerwijzingType
    {
        public string Verwijzing { get; set; }
        public string VerwijzingTypeText { get; set; }
        public string VerwijzingTypeValue { get; set; }
        public int VerwijzingID { get; set; } // = FormTypeID
    }
}