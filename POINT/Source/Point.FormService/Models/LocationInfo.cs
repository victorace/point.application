﻿namespace Point.FormService.Models
{
    public class LocationInfo
    {
        public int LocationID { get; set; }
        public string Street { get; set; }
        public string Number { get; set; }
        public string Postcode { get; set; }
        public string City { get; set; }
    }
}