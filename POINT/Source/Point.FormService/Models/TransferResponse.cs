﻿using Point.Models.Enums;
using System.Collections.Generic;

namespace Point.FormService.Models
{
    public class TransferResponse
    {
        public FlowDefinitionID FlowDefinitionID { get; set; }
        public List<ActivePhase> AllActivePhases { get; set; }
        public bool IsInterrupted { get; set; }
        public bool IsClosed { get; set; }
        public string Owner { get; set; }
        public string AcceptedBy { get; set; }
    }
}