﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Point.FormService.Models
{
    public class NewFlowRequest
    {
        [Required]
        public int OrganizationID { get; set; }
        [Required]
        public int DepartmentID { get; set; }
        [Required]
        public int EmployeeID { get; set; }
        [Required]
        public VerwijzingDetails VerwijzingDetails { get; set; }
        public int FlowDefinitionID { get; set; } = (int)Point.Models.Enums.FlowDefinitionID.HA_VVT; 
        [Required]
        public Patient Patient { get; set; }
        public List<Episode> Episodes { get; set; } = new List<Episode>();
        public List<Journal> Journals { get; set; } = new List<Journal>();
        public Gezondheidsplan Gezondheidsplan { get; set; }
        public List<Medication> Medications { get; set; } = new List<Medication>();
        public List<Measurement> Measurements { get; set; } = new List<Measurement>();

    }

    public class VerwijzingDetails
    {

        [Required]
        public List<int> SendToDepartmentIDs { get; set; } = new List<int>();
        [Required]
        public int VerwijzingID { get; set; }
        public string VerwijzingType { get; set; }
        public string MedischeSituatieRedenOpname { get; set; } //Verwijzing reden
        [Required]
        public DateTime ZorgVanafDatum { get; set; }
        public bool? KwetsbareOudere { get; set; }
        public string PsychosocialeAnamnese { get; set; } //Woonvorm/Steunsysteem
        public string ZorgCoordinator { get; set; } // Waarden: huisarts, poh_ouderen, casemanager, wijk_vpk, anders
        public string ZorgOrganisatieToelichting { get; set; }
        public string AlgemeneToelichting { get; set; }
        public string AanvraagVersturenVia { get; set; } = "huisarts"; // Waarden: huisarts, coordinatiepunt

    }

    public class Patient : Database.Models.PatientReceivedTemp
    {
       
    }

    public class Episode
    {
        public string ICPCCode { get; set; }
        public string ICPCOmschrijving { get; set; }
        public DateTime? BeginDatum { get; set; }
        public bool? Probleem { get; set; }
        public bool? Attentie { get; set; }
    }

    public class Journal
    {
        public int JournalID { get; set; }
        public int EpisodeID { get; set; }
        public List<JournalLine> JournalLines { get; set; } = new List<JournalLine>();
        public string ContactSoort { get; set; }
        public string RegistratieDatum { get; set; }
        public string ICPCCode { get; set; }
    }

    public class JournalLine
    {
        public int JournalLineID { get; set; }
        public string SOEPCode { get; set; }
        public string ICPCCode { get; set; }
        public string Tekst { get; set; }
    }

    public class Medication
    {
        public string ATCCode { get; set; }
        public string Omschrijving { get; set; }
        public DateTime? VoorschrijfDatum { get; set; }
        public double? Hoeveelheid { get; set; }
        public string Gebruiksvoorschrift { get; set; }
        public DateTime? EindDatum { get; set; }
    }

    public class Measurement
    {
        public string Naam { get; set; }
        public string Waarde { get; set; }
        public string Eenheid { get; set; }
        public DateTime? Datum { get; set; }
    }

    public class Gezondheidsplan
    {
        public DateTime AanmaakDatum { get; set; }
        public List<GezondheidsplanProbleem> GezondheidsplanProblemen { get; set; }
    }

    public class GezondheidsplanProbleem
    {
        public string Probleem { get; set; }
        public string Doel { get; set; }
        public int? Prioriteit { get; set; }
        public List<string> Domeinen { get; set; }
        public List<string> Actiepunten { get; set; }
    }

}