﻿namespace Point.FormService.Models
{
    public class EmployeeResponse
    {
        public int EmployeeID { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string ExternID { get; set; }
        public int DepartmentID { get; set; }
        public string DepartmentExternID { get; set; }
        public string DepartmentAGB { get; set; }
        public int LocationID { get; set; }
        public int OrganizationID { get; set; }
        public string OrganizationAGB { get; set; }
    }
}