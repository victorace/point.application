﻿namespace Point.FormService.Models
{
    public class EmployeeRequest
    {
        public int? OrganizationID { get; set; }
        public string OrganizationAGB { get; set; }
        public int? DepartmentID { get; set; }
        public string DepartmentAGB { get; set; }
        public int? EmployeeID { get; set; }
        public string EmployeeExternID { get; set; }
        public string EmployeeBIG { get; set; }
    }
}