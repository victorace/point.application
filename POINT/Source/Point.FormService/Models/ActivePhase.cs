﻿namespace Point.FormService.Models
{
    public class ActivePhase
    {
        public decimal Phase { get; set; }
        public string PhaseName { get; set; }
    }
}