﻿namespace Point.FormService.Models
{
    public class AfterCareType
    {
        public int AfterCareTypeID { get; set; }
        public string AfterCareTypeAbbreviation { get; set; }
        public string AfterCareTypeName { get; set; }
        public int AfterCareTypeSortOrder { get; set; }
        public int AfterCareGroupID { get; set; }
        public string AfterCareGroupName { get; set; }
        public int AfterCareGroupSortOrder { get; set; }
        public int AfterCareTileId { get; set; }
        public string AfterCareTileName { get; set; }
        public int AfterCareTileSortOrder { get; set; }

    }
}