﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Point.FormService.Models
{
    public class TransferRequest
    {
        [Required]
        public int OrganizationID { get; set; }
        public string PatientNumber { get; set; }
        public string VisitNumber { get; set; }
        public DateTime DateOfBirth { get; set; }
    }
}