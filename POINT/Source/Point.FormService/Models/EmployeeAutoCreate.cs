﻿using System.ComponentModel.DataAnnotations;

namespace Point.FormService.Models
{
    public class EmployeeAutoCreate
    {
        [Required]
        public int OrganizationID { get; set; }
        [Required, StringLength(50)]
        public string AutoCreateCode { get; set; }
        [Required, StringLength(50)]
        public string ExternID { get; set; }
        [StringLength(255)]
        public string FirstName { get; set; }
        [StringLength(50)]
        public string MiddleName { get; set; }
        [Required, StringLength(255)]
        public string LastName { get; set; }
        [StringLength(1)]
        public string Gender { get; set; }
        [StringLength(255)]
        public string Function { get; set; }
        [StringLength(50)]
        public string PhoneNumber { get; set; }
        [StringLength(255)]
        public string Email { get; set; }
        [StringLength(50)]
        public string BIG { get; set; }
    }
}