﻿using Point.FormService.Logic;
using Point.FormService.Models;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Point.FormService.Controllers.API
{
    public class LookupController : ApiController
    {
        [HttpGet]
        public async Task<HttpResponseMessage> Regions()
        {
            var regionbl = new RegionBL();

            var regions = await regionbl.GetRegions();

            return Request.CreateResponse(HttpStatusCode.OK, regions);


        }

        [HttpGet]
        public async Task<HttpResponseMessage> AfterCareTypes()
        {
            var aftercaretypebl = new AfterCareTypeBL();

            var aftercaretypes = await aftercaretypebl.GetAfterCareTypes();

            return Request.CreateResponse(HttpStatusCode.OK, aftercaretypes);
        }

        [HttpGet]
        public async Task<HttpResponseMessage> DepartmentInfo(int departmentID)
        {
            var departmentbl = new DepartmentBL();

            var departmentinfo = await departmentbl.GetDepartmentInfoByID(departmentID);

            return Request.CreateResponse(HttpStatusCode.OK, departmentinfo);
        }

        [HttpGet]
        public async Task<HttpResponseMessage> LocationInfo(int locationID)
        {
            var locationbl = new LocationBL();

            var locationinfo = await locationbl.GetLocationByID(locationID);

            return Request.CreateResponse(HttpStatusCode.OK, locationinfo);
        }

        [HttpGet]
        public async Task<HttpResponseMessage> OrganizationInfo(int organizationID)
        {
            var organizationbl = new OrganizationBL();

            var organizationinfo = await organizationbl.GetOrganizationByID(organizationID);

            return Request.CreateResponse(HttpStatusCode.OK, organizationinfo);
        }

        [HttpGet]
        public async Task<HttpResponseMessage> VerwijzingTypes()
        {
            var lookupbl = new LookupBL();

            var verwijzingtypes = await lookupbl.GetVerwijzingTypes();

            return Request.CreateResponse(HttpStatusCode.OK, verwijzingtypes);
        }
    }
}