﻿using Point.FormService.Logic;
using Point.FormService.Models;
using System;
using System.Threading.Tasks;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Point.Infrastructure.Extensions;

namespace Point.FormService.Controllers.API
{
    public class FlowController : ApiController
    {

        private FlowRequestBL FlowRequestBL = new FlowRequestBL();

        [HttpPost]
        public async Task<HttpResponseMessage> NewFlow([FromBody]NewFlowRequest newFlowRequest)
        {
            if (!ModelState.IsValid)
            {
                string invalids = String.Join(",", ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage + e.Exception?.Message));
                return new HttpResponseMessage(HttpStatusCode.BadRequest) { ReasonPhrase = $"Field wrong or missing: {invalids.Truncate(200)}" }; 
            }

            FlowRequestBL = new FlowRequestBL() { NewFlowRequest = newFlowRequest };
            FlowRequestBL.LogRequest();

            var result = await FlowRequestBL.CreateFlow();
            if (!result)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest) { ReasonPhrase = "Error: " + String.Join(",", FlowRequestBL.ErrorMessages) };
            }

            var newflowresponse = new NewFlowResponse() { TransferID = FlowRequestBL.TransferID , FlowInstanceID = FlowRequestBL.FlowInstanceID };

            HttpResponseMessage responsemessage = Request.CreateResponse(HttpStatusCode.OK, newflowresponse);

            return responsemessage;
        }
    }
}
