﻿using Point.FormService.Logic;
using Point.FormService.Models;
using Point.Infrastructure.Extensions;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Point.FormService.Controllers.API
{
    public class TransferController : ApiController
    {
        [HttpGet]
        public async Task<HttpResponseMessage> Status([FromUri]TransferRequest transferrequest)
        {
            if (transferrequest.IsNewed())
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest) { ReasonPhrase = $"No values specified in {nameof(transferrequest)}" };
            }

            try
            {
                var request = new TransferRequestBL();
                var transferresponse = await request.Status(transferrequest);
                if (transferresponse != null)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, transferresponse);
                }
                else
                {
                    return new HttpResponseMessage(HttpStatusCode.NotFound)
                    {
                        ReasonPhrase = $"Niet gevonden"
                    };
                }
            }
            catch (Exception)
            {
                return new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    ReasonPhrase = $"Fatal error"
                };
            }
        }
    }
}