﻿using Point.Models.Enums;
using Point.FormService.Logic;
using Point.FormService.Models;
using Point.Infrastructure.Extensions;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Point.FormService.Controllers.API
{
    public class OrganizationController : ApiController
    {
        [HttpGet]
        public async Task<HttpResponseMessage> RegionalCoordinators([FromUri]OrganizationRequest organizationRequest)
        {
            if (organizationRequest.IsNewed())
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest) { ReasonPhrase = "No values specified in organizationRequest" };
            }

            var capacityrequestbl = new CapacityRequestBL()
            {
                FlowDefinitionIDs = new List<int>() { (int)FlowDefinitionID.HA_VVT },
                FlowDirections = new List<int>() { (int)FlowDirection.Receiving, (int)FlowDirection.Any },
                OrganizationTypeID = (int)OrganizationTypeID.RegionaalCoordinatiepunt
            };
            var capacity = await capacityrequestbl.GetRegionalCoordinators(organizationRequest.RegionID.Value, organizationRequest.RegionName);

            return Request.CreateResponse(HttpStatusCode.OK, capacity);
        }

    }
}
