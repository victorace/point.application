﻿using Point.FormService.Logic;
using Point.FormService.Models;
using Point.Infrastructure.Extensions;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Point.FormService.Controllers.API
{
    public class EmployeeController : ApiController
    {
        private EmployeeRequestBL employeeRequestBL = new EmployeeRequestBL();

        [HttpGet]
        public async Task<HttpResponseMessage> Exists([FromUri]EmployeeRequest employeeRequest)
        {
            if (employeeRequest.IsNewed())
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest) { ReasonPhrase = "No values specified in employeeRequest" };
            }

            var employeeid = await employeeRequestBL.GetEmployeeID(employeeRequest);
            var employeeresponse = new EmployeeResponseYN() { Result = employeeid > 0 };

            return Request.CreateResponse(HttpStatusCode.OK, employeeresponse);  
        }
        
        [HttpGet]
        public async Task<HttpResponseMessage> EmployeeInfo([FromUri] EmployeeRequest employeeRequest)
        {
            if (employeeRequest.IsNewed())
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest) { ReasonPhrase = "No values specified in employeeRequest" };
            }

            var employeeresponse = await employeeRequestBL.GetEmployeeResponse(employeeRequest);

            return Request.CreateResponse(HttpStatusCode.OK, employeeresponse);
        }
        
        [HttpPost]
        public async Task<HttpResponseMessage> AutoCreate([FromBody]EmployeeAutoCreate employeeAutoCreate)
        {
            if (!ModelState.IsValid)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest) { ReasonPhrase = "Field missing: " + string.Join(",", ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage + e.Exception?.Message)) };
            }

            var employeeResponse = await employeeRequestBL.AutoCreate(employeeAutoCreate);

            return Request.CreateResponse(HttpStatusCode.OK, employeeResponse);
        }

        [HttpPost]
        public async Task<HttpResponseMessage> AddToDepartment([FromBody]EmployeeDepartmentRequest employeedepartmenrequest)
        {
            if (!ModelState.IsValid)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest) { ReasonPhrase = "Field missing: " + string.Join(",", ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage + e.Exception?.Message)) };
            }

            var reponse = await employeeRequestBL.AddToDepartment(employeedepartmenrequest);

            return Request.CreateResponse(HttpStatusCode.OK, reponse);
        }
    }
}