﻿using Point.Models.Enums;
using Point.FormService.Logic;
using Point.FormService.Models;
using Point.Infrastructure.Extensions;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Point.FormService.Controllers.API
{
    public class CapacityController : ApiController
    {
        [HttpGet]
        public async Task<HttpResponseMessage> CapacityDetails([FromUri]CapacityRequest capacityRequest)
        {
            if (capacityRequest.IsNewed())
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest) { ReasonPhrase = "No values specified in capacityRequest" };
            }

            var capacityrequestbl = new CapacityRequestBL()
            {
                FlowDefinitionIDs = new List<int>() { (int)FlowDefinitionID.HA_VVT },
                FlowDirections = new List<int>() { (int)FlowDirection.Receiving, (int)FlowDirection.Any },
                OrganizationTypeID = (int)OrganizationTypeID.HealthCareProvider
            };
            var capacity = await capacityrequestbl.GetCapacity(capacityRequest);

            return Request.CreateResponse(HttpStatusCode.OK, capacity);
        }
    }
}
