﻿using Point.Business.Helper;
using Point.Business.Logic;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.FormService.Infrastructure;
using Point.FormService.Models;
using Point.Infrastructure.Extensions;
using Point.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Point.FormService.Logic
{
    public partial class FlowRequestBL : BaseBL
    {
        public const string AanvraagVersturenViaHuisarts = "huisarts";
        public const string AanvraagVersturenViaCoordinatiepunt = "coordinatiepunt";

        private string GetAanvraagVersturenVia()
        {
            if (NewFlowRequest.VerwijzingDetails.AanvraagVersturenVia.Equals(AanvraagVersturenViaCoordinatiepunt, StringComparison.InvariantCultureIgnoreCase))
            {
                return AanvraagVersturenViaCoordinatiepunt;
            }
            else
            {
                return AanvraagVersturenViaHuisarts;
            }
        }

        private bool IsAanvraagVersturenViaCoordinatiepunt()
        {
            return GetAanvraagVersturenVia() == AanvraagVersturenViaCoordinatiepunt
                   && NewFlowRequest.VerwijzingDetails.SendToDepartmentIDs.Any();
        }

        private bool ValidateFlowRequestHAVVT()
        {
            if (NewFlowRequest.VerwijzingDetails.SendToDepartmentIDs == null || !NewFlowRequest.VerwijzingDetails.SendToDepartmentIDs.Any())
            {
                ErrorMessages.Add("Error in FillTransferRouteHAVVT. No SendToDepartmentIDs specified.");
                return false;
            }
            return true;
        }

        public async Task<bool> CreateFlowHAVVT()
        {
            try
            {
                if(!ValidateFlowRequestHAVVT())
                {
                    return false;
                }

                if (!SavePatientReceivedTemp())
                {
                    return false;
                }

                if (!CreateTransferAndClient())
                {
                    return false;
                }
                var phasedefinitionbeginflow = PhaseDefinitionCacheBL.GetPhaseDefinitionBeginFlow(uow, flowdefinitionid);

                int previousphasedefinitionid = phasedefinitionbeginflow.PhaseDefinitionID;
                var phasedefinition = PhaseDefinitionBL.GetNextPhaseDefinitions(uow, phasedefinitionbeginflow.PhaseDefinitionID).FirstOrDefault();
                //PhaseInstanceBL.HandleNextPhase(... keep the structure of handling the phase, not needed on the first phase ...
                var phaseinstance = PhaseInstanceBL.GetByFlowInstanceAndPhase(uow, FlowInstanceID, phasedefinition.Phase).FirstOrDefault();
                var viewdata = new Dictionary<string, object> { { "TransferID", TransferID } };

                if (!await HandleFlowWebFields(phasedefinition, phaseinstance, viewdata))
                {
                    return false;
                }

                previousphasedefinitionid = phasedefinition.PhaseDefinitionID;
                phasedefinition = PhaseDefinitionBL.GetNextPhaseDefinitions(uow, phaseinstance.PhaseDefinitionID).FirstOrDefault();
                PhaseInstanceBL.HandleNextPhase(uow, FlowInstanceID, phaseinstance.PhaseInstanceID, phasedefinition.PhaseDefinitionID, employee.EmployeeID, logentry);
                phaseinstance = PhaseInstanceBL.GetByFlowInstanceAndPhase(uow, FlowInstanceID, phasedefinition.Phase).FirstOrDefault();
                if (!await HandleFlowWebFields(phasedefinition, phaseinstance, viewdata))
                {
                    return false;
                }

                if (phasedefinition.FormTypeID == (int)FlowFormType.TransferRouteHAVVT && !IsAanvraagVersturenViaCoordinatiepunt())
                {
                    previousphasedefinitionid = phasedefinition.PhaseDefinitionID;
                    phasedefinition = PhaseDefinitionBL.GetNextPhaseDefinitions(uow, phaseinstance.PhaseDefinitionID).FirstOrDefault();
                    PhaseInstanceBL.HandleNextPhase(uow, FlowInstanceID, phaseinstance.PhaseInstanceID, phasedefinition.PhaseDefinitionID, employee.EmployeeID, logentry);
                    phaseinstance = PhaseInstanceBL.GetByFlowInstanceAndPhase(uow, FlowInstanceID, phasedefinition.Phase).FirstOrDefault();
                }

                if (!SendInvitation(phasedefinition, previousphasedefinitionid)) 
                {
                    return false;
                }

                FlowInstanceSearchValuesBL.CalculateReadValuesByFlowinstanceIDAsync(uow, FlowInstanceID);

            }
            catch (Exception ex)
            {
                ErrorMessages.Add("SaveData: " + ex.Message);
                return false;
            }
            return true;

        }

        private async Task<bool> FillAanvraagFormulierHAVVT(List<FlowWebField> flowWebFields)
        {
            var department = await new DepartmentBL().GetDepartmentByID(NewFlowRequest.DepartmentID);
            var organizationinfo = await new OrganizationBL().GetByLocationID(department.LocationID);
            var employee = new EmployeeBL().GetByEmployeeID(NewFlowRequest.EmployeeID);
            var verwijzinghelper = new VerwijzingHelper();


            string hismedicatie = "[ Medicatie ]" + Environment.NewLine + Environment.NewLine +
                                  verwijzinghelper.MedicationsToString(NewFlowRequest.Medications) + Environment.NewLine +
                                  Environment.NewLine +
                                  "[ Meetwaarden ]" + Environment.NewLine + Environment.NewLine +
                                  verwijzinghelper.MeasurementsToString(NewFlowRequest.Measurements);

            string hisepisodelijst = verwijzinghelper.EpisodesToString(NewFlowRequest.Episodes);

            string hisgezondheidsplan = verwijzinghelper.GezondheidsplanToString(NewFlowRequest.Gezondheidsplan);

            FlowWebFieldBL.SetValue(flowWebFields, FlowFieldConstants.ID_HuisartsNaamAdres,
                                                   department.Name);
            FlowWebFieldBL.SetValue(flowWebFields, FlowFieldConstants.ID_DossierOwner,
                                                   employee.EmployeeID);
            FlowWebFieldBL.SetValue(flowWebFields, FlowFieldConstants.ID_DossierOwnerPosition,
                                                   employee.Position);
            FlowWebFieldBL.SetValue(flowWebFields, FlowFieldConstants.ID_DossierOwnerTelephoneNumber,
                                                   employee.PhoneNumber);
            FlowWebFieldBL.SetValue(flowWebFields, FlowFieldConstants.ID_RequestFormZHVVTType,
                                                   NewFlowRequest.VerwijzingDetails.VerwijzingID);
            FlowWebFieldBL.SetValue(flowWebFields, FlowFieldConstants.ID_VerwijzingType,
                                                   NewFlowRequest.VerwijzingDetails.VerwijzingType);
            FlowWebFieldBL.SetValue(flowWebFields, FlowFieldConstants.ID_MedischeSituatieRedenOpname,
                                                   NewFlowRequest.VerwijzingDetails.MedischeSituatieRedenOpname);
            FlowWebFieldBL.SetValue(flowWebFields, FlowFieldConstants.ID_CareBeginDate, 
                                                   NewFlowRequest.VerwijzingDetails.ZorgVanafDatum);
            FlowWebFieldBL.SetValue(flowWebFields, FlowFieldConstants.ID_KwetsbareOudereJaNee, 
                                                   NewFlowRequest.VerwijzingDetails.KwetsbareOudere.BoolToTrueFalseNull());
            FlowWebFieldBL.SetValue(flowWebFields, FlowFieldConstants.ID_PsychosocialeAnamnese,
                                                   NewFlowRequest.VerwijzingDetails.PsychosocialeAnamnese);
            FlowWebFieldBL.SetValue(flowWebFields, FlowFieldConstants.ID_ZorgCoordinator,
                                                   NewFlowRequest.VerwijzingDetails.ZorgCoordinator);
            FlowWebFieldBL.SetValue(flowWebFields, FlowFieldConstants.ID_ZorgOrganisatieToelichting,
                                                   NewFlowRequest.VerwijzingDetails.ZorgOrganisatieToelichting);
            FlowWebFieldBL.SetValue(flowWebFields, FlowFieldConstants.ID_AlgemeneToelichting,
                                                   NewFlowRequest.VerwijzingDetails.AlgemeneToelichting);
            FlowWebFieldBL.SetValue(flowWebFields, FlowFieldConstants.ID_HISEpisodeLijst, 
                                                   verwijzinghelper.EpisodesToString(NewFlowRequest.Episodes)); 
            FlowWebFieldBL.SetValue(flowWebFields, FlowFieldConstants.ID_HISOvernemenMedicatie, 
                                                   FlowFieldConstants.Value_True);
            FlowWebFieldBL.SetValue(flowWebFields, FlowFieldConstants.ID_HISMedicatie,
                                                   hismedicatie);
            FlowWebFieldBL.SetValue(flowWebFields, FlowFieldConstants.ID_HISGezondheidsplan, 
                                                   hisgezondheidsplan);
            FlowWebFieldBL.SetValue(flowWebFields, FlowFieldConstants.ID_AanvraagVersturenType,
                                                   GetAanvraagVersturenVia());
            FlowWebFieldBL.SetValue(flowWebFields, FlowFieldConstants.ID_SourceOrganization, 
                                                   organizationinfo.OrganizationID);
            FlowWebFieldBL.SetValue(flowWebFields, FlowFieldConstants.ID_SourceOrganizationLocation, 
                                                   department.LocationID);
            FlowWebFieldBL.SetValue(flowWebFields, FlowFieldConstants.ID_SourceOrganizationDepartment, 
                                                   department.DepartmentID);
            if (IsAanvraagVersturenViaCoordinatiepunt())
            {
                FlowWebFieldBL.SetValue(flowWebFields, FlowFieldConstants.ID_AanvraagVersturenDoorTussenOrganisatie,
                                                       NewFlowRequest.VerwijzingDetails.SendToDepartmentIDs.First()); 
            }
            return true;
        }


        private async Task<bool> FillTransferRouteHAVVT(List<FlowWebField> flowWebFields)
        {
            var department = await new DepartmentBL().GetDepartmentByID(NewFlowRequest.VerwijzingDetails.SendToDepartmentIDs.First());
            if (!IsAanvraagVersturenViaCoordinatiepunt())
            {
                FlowWebFieldBL.SetValue(flowWebFields, FlowFieldConstants.ID_DestinationHealthCareProvider, department.DepartmentID);
            }
            
            return true;
        }

        private bool SendInvitation(PhaseDefinition phaseDefinition, int sendingPhaseDefinitionID)
        {
            if (IsAanvraagVersturenViaCoordinatiepunt())
            {
                return SendInvitationReCo(phaseDefinition, sendingPhaseDefinitionID);
            }
            else
            {
                return SendInvitationsVVT(phaseDefinition, sendingPhaseDefinitionID);
            }
        }

        private bool SendInvitationReCo(PhaseDefinition phaseDefinition, int sendingPhaseDefinitionID) 
        {
            var departmentbl = new DepartmentBL();
            try
            {
                bool needsacknowledgement = OrganizationHelper.NeedsAcknowledgement(phaseDefinition.FormTypeID);
                var destinationurl = UrlHelper.DispatchUrl(TransferID, phaseDefinition.FormTypeID);

                var recodepartmentid = NewFlowRequest.VerwijzingDetails.SendToDepartmentIDs.First();
                OrganizationHelper.Invite(uow, FlowInstanceID, sendingPhaseDefinitionID, phaseDefinition.PhaseDefinitionID, recodepartmentid, needsacknowledgement, InviteType.Mediating, logentry, destinationurl);
            }
            catch (Exception ex)
            {
                ErrorMessages.Add($"Error in {nameof(SendInvitationReCo)}, phaseDefinitionID {phaseDefinition.PhaseDefinitionID}, flowInstanceID {FlowInstanceID}: {ex.Message}");
                return false;
            }

            return true;
        }
        private bool SendInvitationsVVT(PhaseDefinition phaseDefinition, int sendingPhaseDefinitionID)
        {
            var departmentbl = new DepartmentBL();
            try
            {
                bool needsacknowledgement = OrganizationHelper.NeedsAcknowledgement(phaseDefinition.FormTypeID);
                var destinationurl = UrlHelper.DispatchUrl(TransferID, phaseDefinition.FormTypeID);

                int firstdepartmentid =  NewFlowRequest.VerwijzingDetails.SendToDepartmentIDs.First();
                OrganizationHelper.Invite(uow, FlowInstanceID, sendingPhaseDefinitionID, phaseDefinition.PhaseDefinitionID, firstdepartmentid, needsacknowledgement, InviteType.Regular, logentry, destinationurl);

                foreach (var queuedepartmentid in NewFlowRequest.VerwijzingDetails.SendToDepartmentIDs.Skip(1))
                {
                    OrganizationHelper.Queue(uow, FlowInstanceID, sendingPhaseDefinitionID, phaseDefinition.PhaseDefinitionID, queuedepartmentid, needsacknowledgement, InviteType.Regular, logentry);
                }

            }
            catch (Exception ex)
            {
                ErrorMessages.Add($"Error in {nameof(SendInvitationsVVT)}, phaseDefinitionID {phaseDefinition.PhaseDefinitionID}, flowInstanceID {FlowInstanceID}: {ex.Message}");
                return false;
            }
            return true;
        }



    }
}