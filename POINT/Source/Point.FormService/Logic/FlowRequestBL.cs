﻿using Point.Business.BusinessObjects;
using Point.Business.Helper;
using Point.Business.Logic;
using Point.Database.Extensions;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.FormService.Infrastructure;
using Point.FormService.Models;
using Point.Infrastructure.Extensions;
using Point.Infrastructure.Helpers;
using Point.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Point.FormService.Logic
{
    public partial class FlowRequestBL : BaseBL
    {
        public int TransferID { get; private set; }
        public int FlowInstanceID { get; private set; }

        public NewFlowRequest NewFlowRequest { get; set; }

        private DateTime now = DateTime.Now;
        private Employee employee = new Employee();
        private LogEntry logentry = new LogEntry();
        private PointUserInfo pointuserinfo = null;
        private FlowDefinitionID flowdefinitionid;

        public async Task<bool> CreateFlow()
        {
            if (NewFlowRequest == null)
            {
                ErrorMessages.Add("No NewFlowRequest specified.");
                return false;
            }

            if (!InitVariables())
            {
                return false;
            }

            bool createflow = false;
            switch (flowdefinitionid)
            {
                case FlowDefinitionID.HA_VVT:
                    createflow = await CreateFlowHAVVT();
                    break;
                default:
                    ErrorMessages.Add($"FlowDefinitionID [{flowdefinitionid}] is not implemented.");
                    createflow = false;
                    break;
            }

            if (!createflow)
            {
                Cleanup(); 
            }

            return createflow;
        }

        private void Cleanup()
        {
            try
            {
                if (FlowInstanceID > 0)
                {
                    int systememployeeid = ConfigHelper.GetAppSettingByName<int>("SystemEmployeeID");
                    var flowinstance = FlowInstanceBL.GetByID(uow, FlowInstanceID);
                    if (flowinstance != null && systememployeeid >= 1)
                    {
                        flowinstance.Deleted = true;
                        flowinstance.DeletedByEmployeeID = systememployeeid;
                        flowinstance.DeletedDate = DateTime.Now;
                        uow.Save();
                    }
                }
            }
            catch { /* ignore */ }
        }

        public bool LogRequest()
        {
            if (NewFlowRequest == null)
            {
                return false;
            }
            try
            {
                new WebRequestLogBL(uow).LogWebRequestJson(NewFlowRequest, null, null);
                return true;
            }
            catch { /* ignore */ }
            return false;
        }

        private bool InitVariables()
        {

            if (!Enum.IsDefined(typeof(FlowDefinitionID), NewFlowRequest.FlowDefinitionID))
            {
                ErrorMessages.Add($"Invalid FlowDefinitionID {NewFlowRequest.FlowDefinitionID}");
                return false;
            }
            flowdefinitionid = (FlowDefinitionID)NewFlowRequest.FlowDefinitionID;

            employee = new EmployeeBL().GetByEmployeeID(NewFlowRequest.EmployeeID);
            if (employee == null)
            {
                ErrorMessages.Add($"Error in InitEmployeeVariables: No employee found with ID {NewFlowRequest.EmployeeID}");
                return false;
            }

            logentry = new LogEntry() { EmployeeID = employee.EmployeeID, ScreenID = -1, Screenname = "FormService", Timestamp = now };

            pointuserinfo = PointUserInfoHelper.GetPointUserInfoByEmployeeID(uow, employee.EmployeeID);
            if (pointuserinfo == null)
            {
                ErrorMessages.Add($"Error in InitEmployeeVariables: No valid PointUserInfo-object for employeeID {employee.EmployeeID}");
                return false;
            }
            return true;
        }

        private bool SavePatientReceivedTemp()
        {
            var patientreceivedtemp = new PatientReceivedTemp();
            patientreceivedtemp.Merge(NewFlowRequest.Patient);
            try
            {
                PatientReceivedTempBL.Insert(uow, Guid.NewGuid(), NewFlowRequest.OrganizationID, employee.FullName(), now, NewFlowRequest.Patient.PatientNumber, "", patientreceivedtemp);
            }
            catch (Exception ex)
            {
                ErrorMessages.Add($"Error in SavePatientReceivedTemp: {ex.Message}");
                return false;
            }
            return true;
        }

        private bool CreateTransferAndClient()
        {
            try
            {
                var dossierbo = new DossierBO(employee.EmployeeID, NewFlowRequest.Patient.CivilServiceNumber, NewFlowRequest.DepartmentID, NewFlowRequest.Patient.PatientNumber );
                var dossierparameters = dossierbo.CreateDossierAPI((FlowDefinitionID)NewFlowRequest.FlowDefinitionID, logentry);

                if (dossierparameters.TransferID == -1)
                {
                    ErrorMessages.Add("Error in CreateTransferAndClient. Could not create new transfer: " + dossierbo.Error);
                    return false;
                }

                TransferID = dossierparameters.TransferID;
                FlowInstanceID = dossierparameters.FlowInstanceID;
            }
            catch (Exception ex)
            {
                ErrorMessages.Add($"Error in CreateTransferAndClient: {ex.Message}");
                return false;
            }
            FlowInstanceSearchValuesBL.CalculateReadValuesByFlowinstanceIDAsync(uow, FlowInstanceID);
            return true;
        }

        private async Task<bool> HandleFlowWebFields(PhaseDefinition phaseDefinition, PhaseInstance phaseInstance, IDictionary<string, object> viewData)
        {
            if (phaseDefinition == null || phaseInstance == null)
            {
                return false;
            }
            try
            {
                var flowwebfields = FlowWebFieldBL.GetFieldsFromFormTypeID(uow, phaseDefinition.FormTypeID, phaseDefinition.PhaseDefinitionID, TransferID, null, null, viewData).ToList();
                await FillFlowWebFields(phaseDefinition.FormTypeID, flowwebfields);

                var formsetversion = FormSetVersionBL.CreateFormSetVersion(uow, TransferID, phaseDefinition.FormTypeID, phaseInstance.PhaseInstanceID, pointuserinfo, logentry);
                uow.Save();

                flowwebfields = FlowWebFieldBL.HandleOriginFormTypeFields(uow, flowwebfields, formsetversion.FormTypeID);
                FlowWebFieldBL.SaveValues(uow, FlowInstanceID, flowwebfields, formsetversion.FormSetVersionID, logentry);
                var validfields = flowwebfields.Where(fwf => !fwf.ReadOnly).ToList();
                FlowWebFieldBL.SetValuesOriginCopyToAll(uow, validfields, TransferID, formsetversion.FormTypeID, logentry, pointuserinfo, viewData);
            }
            catch (Exception ex)
            {
                ErrorMessages.Add($"Error in HandleFlowWebFields, phaseDefinitionID {phaseDefinition.PhaseDefinitionID}, phaseInstanceID {phaseInstance.PhaseInstanceID}: {ex.Message}");
                return false;
            }
            return true;

        }

        private async Task<bool> FillFlowWebFields(int formTypeID, List<FlowWebField> flowWebFields)
        {
            if (!Enum.IsDefined(typeof(FlowFormType), formTypeID))
            {
                ErrorMessages.Add($"Invalid FormTypeID {formTypeID}");
                return false;
            }

            var formtypeid = (FlowFormType)formTypeID;

            switch (formtypeid)
            {
                case FlowFormType.AanvraagFormulierHAVVT:
                    await FillAanvraagFormulierHAVVT(flowWebFields);
                    break;
                case FlowFormType.TransferRouteHAVVT:
                    await FillTransferRouteHAVVT(flowWebFields);
                    break;
            }
            return true;
        }





    }
}