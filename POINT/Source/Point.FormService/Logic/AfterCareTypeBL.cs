﻿using Point.FormService.Models;
using Point.Models.Enums;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Runtime.Caching;
using System.Threading.Tasks;
using AfterCareType = Point.FormService.Models.AfterCareType;

namespace Point.FormService.Logic
{
    public class AfterCareTypeBL :BaseBL
    {
        public async Task<IEnumerable<AfterCareType>> GetAfterCareTypes()
        {
            string cachekey = "GetAfterCareTypes";

            var result = MemoryCache.Default.Get(cachekey) as List<AfterCareType>;

            if (result == null || !result.Any())
            {
                result = new List<AfterCareType>();
                int organizationtypeid = (int)OrganizationTypeID.HealthCareProvider;

                var aftercaretypes = await uow.AfterCareTypeRepository.Get(act => act.OrganizationTypeID == organizationtypeid && !act.Inactive, includeProperties: "AfterCareGroup,AfterCareGroup.AfterCareTileGroup").ToListAsync();

                foreach (var aftercaretype in aftercaretypes)
                {
                    result.Add(new AfterCareType
                    {
                        AfterCareGroupID = aftercaretype.AfterCareGroup.AfterCareGroupID,
                        AfterCareGroupName = aftercaretype.AfterCareGroup.Name,
                        AfterCareGroupSortOrder = aftercaretype.AfterCareGroup.Sortorder,
                        AfterCareTileId = aftercaretype.AfterCareGroup.AfterCareTileGroup.AfterCareTileGroupID,
                        AfterCareTileName = aftercaretype.AfterCareGroup.AfterCareTileGroup.Name,
                        AfterCareTileSortOrder = aftercaretype.AfterCareGroup.AfterCareTileGroup.Sortorder,
                        AfterCareTypeID = aftercaretype.AfterCareTypeID,
                        AfterCareTypeAbbreviation = aftercaretype.Abbreviation,
                        AfterCareTypeName = aftercaretype.Name,
                        AfterCareTypeSortOrder = aftercaretype.Sortorder
                    });
                }

                if (result != null)
                {
                    MemoryCache.Default.Set(cachekey, result, new System.DateTimeOffset(DateTime.Now.AddHours(8)));
                }
            }


            return result;
        }

        public async Task<AfterCareType> GetAfterCareTypeByName(string name)
        {
            var result = await GetAfterCareTypes();
            return result.FirstOrDefault(act => act.AfterCareTypeName.Equals(name, StringComparison.InvariantCultureIgnoreCase));
        }

        public async Task<AfterCareType> GetAfterCareTypeByID(int id)
        {
            var result = await GetAfterCareTypes();
            return result.FirstOrDefault(act => act.AfterCareTypeID == id);
        }
    }
}