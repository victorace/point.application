﻿using Point.FormService.Models;
using System.Data.Entity;
using System.Threading.Tasks;

namespace Point.FormService.Logic
{
    public class LocationBL : BaseBL 
    {
        public async Task<LocationInfo> GetLocationByID(int locationID)
        {
            var locationinfo = new LocationInfo();

            var location = await uow.LocationRepository.Get(loc =>loc.LocationID == locationID).FirstOrDefaultAsync();
            if (location != null)
            {
                locationinfo.City = location.City;
                locationinfo.LocationID = location.LocationID;
                locationinfo.Number = location.Number;
                locationinfo.Postcode = location.PostalCode;
                locationinfo.Street = location.StreetName;
            }

            return locationinfo;
        }

    }
}