﻿using Point.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Point.FormService.Logic
{
    public class EmployeeBL : BaseBL 
    {
        public Employee GetBySenderUserName(string senderUserName, int organizationID)
        {
            if (string.IsNullOrEmpty(senderUserName) || organizationID < 0)
            {
                return null;
            }

            var employee = Business.Logic.EmployeeBL.GetByExternID(uow, senderUserName, organizationID);
            if (employee == null)
            {
                employee = Business.Logic.EmployeeBL.GetByUserName(uow, senderUserName, organizationID);
            }
            return employee;
        }

        public  Employee GetByEmployeeID(int employeeID)
        {
            if (employeeID <= 0)
            {
                return null;
            }
            return Business.Logic.EmployeeBL.GetByID(uow, employeeID);
        }

        public IEnumerable<int> AddToDepartment(int employeeid, IEnumerable<int> departmentids)
        {
            var timestamp = DateTime.Now;

            var newdepartmentids = Enumerable.Empty<int>();

            var employee = GetByEmployeeID(employeeid);
            if (employee != null)
            {
                var currentemployeedepartmentids = employee.EmployeeDepartment.Select(d => d.DepartmentID);
                newdepartmentids = departmentids.Except(currentemployeedepartmentids).ToList();

                foreach (var departmentid in newdepartmentids)
                {
                    uow.EmployeeDepartmentRepository.Insert(new EmployeeDepartment()
                    {
                        EmployeeID = employeeid,
                        DepartmentID = departmentid,
                        ModifiedTimeStamp = timestamp
                    });
                }

                uow.Save();
            }

            return newdepartmentids;
        }
    }
}