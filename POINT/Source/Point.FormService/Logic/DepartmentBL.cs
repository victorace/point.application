﻿using Point.Database.Models;
using Point.FormService.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Point.FormService.Logic
{
    public class DepartmentBL : BaseBL
    {
        public async Task<DepartmentInfo> GetDepartmentInfoByID(int departmentID)
        {
            var departmentinfo = new DepartmentInfo();

            var department = await uow.DepartmentRepository.Get(dep => dep.DepartmentID == departmentID, includeProperties:"AfterCareType1").FirstOrDefaultAsync();
            if (department != null)
            {
                departmentinfo.AfterCareType = department.AfterCareType1.Name;
                departmentinfo.DepartmentID = department.DepartmentID;
                departmentinfo.DepartmentName = department.Name;
                departmentinfo.EmailAddress = department.EmailAddress;
                departmentinfo.FaxNumber = department.FaxNumber;
                departmentinfo.PhoneNumber = departmentinfo.PhoneNumber;
            }

            return departmentinfo;
        }

        public async Task<Department> GetDepartmentByID(int departmentID)
        {
            var department = await uow.DepartmentRepository.Get(dep => dep.DepartmentID == departmentID).FirstOrDefaultAsync();
            return department;
        }

        public async Task<int> GetOrganizationID(int departmentID)
        {
            int organizationid = -1;
            var department = await uow.DepartmentRepository.Get(dep => dep.DepartmentID == departmentID, includeProperties: "Location").FirstOrDefaultAsync();
            if (department != null && department.Location != null)
            {
                organizationid = department.Location.OrganizationID;
            }
            return organizationid;
        }

        public async Task<IEnumerable<Department>> GetByOrganizationID(int organizationid)
        {
            return await (from organizations in uow.OrganizationRepository.Get()
                          join locations in uow.LocationRepository.Get() on organizations.OrganizationID equals locations.OrganizationID
                          join departments in uow.DepartmentRepository.Get() on locations.LocationID equals departments.LocationID
                          where organizations.OrganizationID == organizationid
                          select departments).ToListAsync();
        }
    }
}