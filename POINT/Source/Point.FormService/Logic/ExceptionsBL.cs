﻿using Point.Infrastructure;
using Point.Infrastructure.Extensions;
using System;
using System.IO;
using System.Web;

namespace Point.FormService.Logic
{
    public class ExceptionsBL
    {
        private static string RemovePlainPassword(string postData = null)
        {
            if (string.IsNullOrEmpty(postData)) return postData;

            var passwordSign = "Password=";
            var postParams = postData.Split('&');
            foreach (var param in postParams)
            {
                // if postdata contains (exactly) 'Password' param : 
                if (param.IndexOf(passwordSign, StringComparison.InvariantCultureIgnoreCase) == 0)
                {
                    postData = postData.Replace("&" + param, "");
                    break;
                }
            }
            return postData;
        }

        public static int HandleException(Exception exception, HttpContext httpcontext)
        {
            string postdata = null;
            if (httpcontext?.Request?.InputStream != null)
            {
                var streamreader = new StreamReader(httpcontext.Request.InputStream);
                postdata = RemovePlainPassword(streamreader.ReadToEnd());
            }

            var cookies = "";
            if ((httpcontext?.Request?.Cookies != null))
            {
                foreach (var cookiekey in httpcontext.Request.Cookies.AllKeys)
                {
                    if (cookiekey == ".POINTAUTH" || cookiekey == ".POINTROLES")
                    {
                        continue;
                    }

                    var httpCookie = httpcontext.Request.Cookies[cookiekey];
                    if (httpCookie != null)
                    {
                        cookies += $"{cookiekey}={httpCookie.Value}&";
                    }
                }
            }

            var transferID = httpcontext?.Request?.GetRequestVariable<int?>("transferid", null);

            return ExceptionHelper.SendMessageException(exception, httpcontext?.Request?.Url, null, transferID, httpcontext?.User?.Identity?.Name, postdata, cookies, httpcontext?.Request?.UserAgent, httpcontext?.Request?.IsAjaxRequest());
        }
    }
}