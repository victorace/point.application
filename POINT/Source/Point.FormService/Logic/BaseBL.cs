﻿using Point.Database.Context;
using Point.Database.Repository;
using System.Collections.Generic;
using System.Linq;

namespace Point.FormService.Logic
{
    public class BaseBL
    {
        public List<string> ErrorMessages { get; set; } = new List<string>();

        public bool HaveErrors { get { return ErrorMessages.Any(); } }

        protected readonly UnitOfWork<PointSearchContext> uow = new UnitOfWork<PointSearchContext>();
    }
}