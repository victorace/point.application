﻿using Point.Business.Logic;
using Point.Database.Extensions;
using Point.Database.Models;
using Point.FormService.Models;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Point.FormService.Logic
{
    public class TransferRequestBL : BaseBL
    {
        public async Task<TransferResponse> Status(TransferRequest transferrequest)
        {
            var flowinstance = FlowInstanceBL.GetByClientDetails(uow, transferrequest.OrganizationID, transferrequest.PatientNumber,
                transferrequest.VisitNumber, transferrequest.DateOfBirth);
            if (flowinstance == null)
            {
                return null;
            }

            int flowinstanceid = flowinstance.FlowInstanceID;

            var isclosed = FlowInstanceBL.IsFlowClosed(uow, flowinstance.FlowInstanceID);

            var flowfieldvalues = FlowFieldValueBL.GetByFlowInstanceIDAndFieldIDs(uow, flowinstanceid, new int[]
                { FlowFieldConstants.ID_DossierOwner, FlowFieldConstants.ID_AcceptedBy });

            var flowfieldvalue = flowfieldvalues.FirstOrDefault(ffv => ffv.FlowFieldID == FlowFieldConstants.ID_DossierOwner);
            string dossierowner = string.Empty;
            if (int.TryParse(flowfieldvalue?.Value, out int employeeid))
            {
                var employee = new EmployeeBL().GetByEmployeeID(employeeid);
                dossierowner = employee?.FullName();
            }

            flowfieldvalue = flowfieldvalues.FirstOrDefault(ffv => ffv.FlowFieldID == FlowFieldConstants.ID_AcceptedBy);
            string acceptedby = string.Empty;
            if (int.TryParse(flowfieldvalue?.Value, out employeeid))
            {
                var employee = new EmployeeBL().GetByEmployeeID(employeeid);
                acceptedby = employee?.FullName();
            }

            var allactivephases = await PhaseInstanceBL.GetActiveByFlowInstanceID(uow, flowinstanceid)
                    .Where(p => p.PhaseDefinition.Phase >= 0)
                    .ToListAsync();

            var transferresponse = new TransferResponse()
            {
                FlowDefinitionID = flowinstance.FlowDefinitionID,
                AllActivePhases = allactivephases.Select(p => new ActivePhase()
                {
                    Phase = (p.PhaseDefinition.Phase % 1 == 0) ? decimal.Round(p.PhaseDefinition.Phase) : p.PhaseDefinition.Phase,
                    PhaseName = p.PhaseDefinition.PhaseName,
                }).ToList(),
                IsInterrupted = flowinstance.Interrupted,
                IsClosed = isclosed,
                Owner = dossierowner,
                AcceptedBy = acceptedby
            };

            return transferresponse;
        }
    }
}