﻿using Point.Business.BusinessObjects;
using Point.Business.Logic;
using Point.Database.Models;
using Point.Models.Enums;

namespace Point.FormService.Logic
{
    public partial class FlowRequestBL : BaseBL
    {
        //Only using a simple form of creating the ZH-VVT flow. Create client + first phase (AF) active
        public bool CreateFlowZHVVTSimpleFromWS(string senderUserName, int organizationID, string civilServiceNumber, string patientNumber ) 
        {
            employee = new Point.FormService.Logic.EmployeeBL().GetBySenderUserName(senderUserName, organizationID);

            if (employee == null)
            {
                ErrorMessages.Add($"No valid employee for senderUserName [{senderUserName}]");
                return false;
            }
           
            logentry = new LogEntry() { EmployeeID = employee.EmployeeID, ScreenID = -1, Screenname = "FormService", Timestamp = now };

            var dossierbo = new DossierBO(employee.EmployeeID, civilServiceNumber, employee.DepartmentID, patientNumber);
            var dossierparameters = dossierbo.CreateDossierAPI(FlowDefinitionID.ZH_VVT, logentry);

            if (dossierparameters.TransferID > 0)
            {
                TransferID = dossierparameters.TransferID;
                FlowInstanceID = dossierparameters.FlowInstanceID;
                FlowInstanceSearchValuesBL.CalculateReadValuesByFlowinstanceIDAsync(uow, dossierparameters.FlowInstanceID);
            }
            else
            {
                ErrorMessages.Add($"Error in {nameof(CreateFlowZHVVTSimpleFromWS)}. {dossierbo.Error}");
                return false;
            }

            return true;
        }
    }
}