﻿using Point.Database.Context;
using Point.Database.Repository;
using Point.FormService.Models;
using System.Collections.Generic;
using System;
using System.Data.Entity;
using System.Linq;
using System.Runtime.Caching;
using System.Threading.Tasks;


namespace Point.FormService.Logic
{
    public class RegionBL : BaseBL
    {
        public async Task<IEnumerable<Region>> GetRegions()
        {
            string cachekey = "GetRegions";

            var result = MemoryCache.Default.Get(cachekey) as List<Region>;

            if (result == null || !result.Any())
            {
                result = new List<Region>();
                var regions = await uow.RegionRepository.GetAll().ToListAsync();

                foreach (var region in regions)
                {
                    result.Add(new Region()
                    {
                        CapacityBeds = region.CapacityBeds == true,
                        Name = region.Name,
                        RegionID = region.RegionID
                    });
                }

                if (result != null)
                {
                    MemoryCache.Default.Set(cachekey, result, new System.DateTimeOffset(DateTime.Now.AddHours(8)));
                }
            }

            return result;
        }

        public async Task<Region> GetRegionByName(string name)
        {
            var result = await GetRegions();
            return result.FirstOrDefault(r => r.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase));
        }


        public async Task<Region> GetRegionByID(int id)
        {
            var result = await GetRegions();
            return result.FirstOrDefault(r => r.RegionID == id);
        }

        public async Task<IEnumerable<Region>> GetRegionsByIDs(int[] ids)
        {
            var result = await GetRegions();
            return result.Where(r => ids.Contains(r.RegionID));
        }

        public async Task<IEnumerable<Region>> GetRegionsByOrganizationID(int organizationID)
        {
            var regionids = await uow.OrganizationSearchRepository.Get(os => os.OrganizationID == organizationID).SelectMany(os => os.OrganizationSearchRegion.Select(osr => osr.RegionID)).ToArrayAsync();
            return await GetRegionsByIDs(regionids);
        }

    }
}