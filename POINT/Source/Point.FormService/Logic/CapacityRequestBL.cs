﻿using Point.Business.Logic;
using Point.Database.Models;
using Point.FormService.Models;
using Point.Models.Enums;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Point.FormService.Logic
{
    public class CapacityRequestBL : BaseBL
    {
        public List<int> FlowDefinitionIDs = new List<int>();
        public List<int> FlowDirections = new List<int>();
        public int OrganizationTypeID = -1;

        private List<string> SearchPostcodes = null;
        private int SearchRegionID = -1;
        private string SearchAfterCareTypeAbbreviation = null;
        private string SearchName = null;

        public async Task<IEnumerable<CapacityResponse>> GetCapacity(CapacityRequest capacityRequest)
        {

            RequestToSearchparameters(capacityRequest);

            var search = Search(); 
            if (capacityRequest.DepartmentIDs != null)
            {
                search = search.Where(os => capacityRequest.DepartmentIDs.Contains(os.DepartmentID));
            }

            var searchresults = await search.ToListAsync();
            
            if (SearchPostcodes != null)
            {
                // faster to filter afterwards, then trying to "query" it
                searchresults = searchresults.Where(os => SearchPostcodes.Contains(os.LocationPostcode)).ToList();
            }

            if (capacityRequest.Postcode != null)
            {
                var searchresultsareas = await Search().Where(os => os.OrganizationSearchPostalCode.Any(osp => capacityRequest.Postcode.CompareTo(osp.StartPostalCode) >= 0 
                                                                                                            && capacityRequest.Postcode.CompareTo(osp.EndPostalCode) <= 0)).ToListAsync();

                searchresults.AddRange(searchresultsareas);
            }

            return await SearchResultToResponse(searchresults);
        }

        public async Task<IEnumerable<CapacityResponse>> GetRegionalCoordinators(int? regionID, string regionName = null)
        {
            SetRegion(regionID, regionName);

            var searchresults = await Search().ToListAsync();
            var response = await SearchResultToResponse(searchresults);
            response.All(r => { r.CapacityInfo = null; return true; });

            return response;
        }

        public IQueryable<OrganizationSearch> Search()
        {

            var search = (from organizationsearch 
                            in uow.OrganizationSearchRepository.Get(os => os.OrganizationTypeID == OrganizationTypeID
                                                                       && os.OrganizationSearchParticipation.Any(osp => FlowDefinitionIDs.Contains((int)osp.FlowDefinitionID) && FlowDirections.Contains((int)osp.FlowDirection))
                                                                       && (SearchAfterCareTypeAbbreviation == null || os.AfterCareType == SearchAfterCareTypeAbbreviation)
                                                                       && (SearchName == null || (os.OrganizationName.Contains(SearchName) || os.LocationName.Contains(SearchName) || os.DepartmentName.Contains(SearchName)) )
                                                                       && (SearchRegionID == -1 || os.OrganizationSearchRegion.Any(osr => osr.RegionID == SearchRegionID))
                                                                   )
                        select organizationsearch);

            return search;
        }

        private async void RequestToSearchparameters(CapacityRequest capacityRequest)
        {
            if (!string.IsNullOrEmpty(capacityRequest.Postcode))
            {
                if (!validPostcode(capacityRequest.Postcode))
                {
                    ErrorMessages.Add($"Invalid PostalCode specified [{capacityRequest.Postcode}]");
                }

                SearchPostcodes = GPSCoordinaatBL.GetPostcodesByStartAndRange(uow, capacityRequest.Postcode, capacityRequest.Radius ?? 5);
            }

            SetRegion(capacityRequest.RegionID, capacityRequest.RegionName);

            if (!string.IsNullOrEmpty(capacityRequest.Name))
            {
                SearchName = capacityRequest.Name;
            }

            if (capacityRequest.AfterCareTypeID != null)
            {
                var aftercaretype = await new AfterCareTypeBL().GetAfterCareTypeByID(capacityRequest.AfterCareTypeID.Value);
                if (aftercaretype != null)
                {
                    SearchAfterCareTypeAbbreviation = aftercaretype.AfterCareTypeAbbreviation;
                }
            }
            else if (!string.IsNullOrEmpty(capacityRequest.AfterCareTypeName))
            {
                var aftercaretype = await new AfterCareTypeBL().GetAfterCareTypeByName(capacityRequest.AfterCareTypeName);
                if (aftercaretype != null)
                {
                    SearchAfterCareTypeAbbreviation = aftercaretype.AfterCareTypeAbbreviation;
                }
            }
        }
        private async void SetRegion(int? regionID, string regionName = null)
        {
            if (regionID != null)
            {
                SearchRegionID = regionID.Value;
            }
            else if (!string.IsNullOrEmpty(regionName))
            {
                var region = await new RegionBL().GetRegionByName(regionName);
                if (region != null)
                {
                    SearchRegionID = region.RegionID;
                }
            }

        }

        private async Task<IEnumerable<CapacityResponse>> SearchResultToResponse(IEnumerable<OrganizationSearch> searchresults)
        {
            var result = new List<CapacityResponse>();

            var locationids = searchresults.Select(sr => sr.LocationID).ToList();
            var centraldepartments = Point.Business.Logic.DepartmentBL.GetByLocationIDAndDepartmentTypeID(uow, locationids, DepartmentTypeID.CentraalMeldpunt).ToList();


            var regionbl = new RegionBL();

            foreach (var searchresult in searchresults.Distinct())
            {
                var regions = await regionbl.GetRegionsByIDs(searchresult.OrganizationSearchRegion.Select(osr => osr.RegionID).ToArray());
                result.Add(new CapacityResponse()
                {
                    CapacityInfo = new CapacityInfo()
                    {
                        Day1 = searchresult.Capacity0,
                        Day2 = searchresult.Capacity1,
                        Day3 = searchresult.Capacity2,
                        Day4 = searchresult.Capacity3
                    },
                    CapacityOrganization = new CapacityOrganization()
                    {
                        AfterCareType = searchresult.AfterCareType,
                        DepartmentID = searchresult.DepartmentID,
                        DepartmentName = searchresult.DepartmentName,
                        LocationID = searchresult.LocationID,
                        LocationName = searchresult.LocationName,
                        OrganizationID = searchresult.OrganizationID,
                        OrganizationName = searchresult.OrganizationName,
                        Regions = regions.ToList(),
                        CentralDepartmentID = await GetCentralDepartmentID(centraldepartments, searchresult.LocationID, searchresult.AfterCareType)
                    },
                    CapacityAddress = new CapacityAddress()
                    {
                        City = searchresult.City,
                        Postcode = searchresult.LocationPostcode
                    }
                });
            }

            return result;

        }

        private async Task<int?> GetCentralDepartmentID(IEnumerable<Department> departments, int currentLocationID, string afterCareType) 
        {
            if (departments == null || !departments.Any())
            {
                return null;
            }
            var aftercaretype = await new AfterCareTypeBL().GetAfterCareTypeByName(afterCareType);
            var department = departments.FirstOrDefault(d => d.LocationID == currentLocationID && (aftercaretype == null || d.AfterCareType1ID == aftercaretype.AfterCareTypeID));
            if (department == null)
            {
                department = departments.FirstOrDefault(d => d.LocationID == currentLocationID);
            }
            return department?.DepartmentID;
        }

        private bool validPostcode(string postcode)
        {
            if (postcode.Length < 4 || postcode.Length > 6)
            {
                return false;
            }
            if (postcode.Length == 4 && !Regex.IsMatch(postcode, @"[0-9]{4}"))
            {
                return false;
            }
            if (postcode.Length == 5 && !Regex.IsMatch(postcode, @"[0-9]{4}[A-Za-z]{1}"))
            {
                return false;
            }
            if (postcode.Length == 6 && !Regex.IsMatch(postcode, @"[0-9]{4}[A-Za-z]{2}"))
            {
                return false;
            }
            return true;
        }

    }
}