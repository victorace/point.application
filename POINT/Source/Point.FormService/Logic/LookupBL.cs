﻿using Point.Business.Logic;
using Point.FormService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Threading.Tasks;

namespace Point.FormService.Logic
{
    public class LookupBL : BaseBL
    {
        public async Task<IEnumerable<VerwijzingType>> GetVerwijzingTypes()
        {
            string cachekey = "GetVerwijzingTypes";

            var result = MemoryCache.Default.Get(cachekey) as List<VerwijzingType>;

            if (result == null || !result.Any())
            {

                result = await Task.Run(() =>
                {

                    var verwijzingtypes = new List<VerwijzingType>();

                    var lookupbl = new LookUpBL();
                    var formtypes = FormTypeBL.GetFormTypes(uow, true).Where(ft => ft.URL != null && ft.URL.Equals("FormType/RequestFormHAVVT", StringComparison.InvariantCultureIgnoreCase));

                    foreach (var formtype in formtypes)
                    {
                        foreach (var option in lookupbl.GetVerwijzingTypeByFormTypeID(formtype.FormTypeID))
                        {
                            verwijzingtypes.Add(new VerwijzingType()
                            {
                                VerwijzingID = formtype.FormTypeID,
                                Verwijzing = formtype.Name,
                                VerwijzingTypeText = option.Text,
                                VerwijzingTypeValue = option.Value
                            });
                        }
                    }

                    return verwijzingtypes;

                });

                if (result != null)
                {
                    MemoryCache.Default.Set(cachekey, result, new System.DateTimeOffset(DateTime.Now.AddHours(8)));
                }

            }

            return result;

        }
    }
}