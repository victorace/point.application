﻿using Point.Database.Models;
using Point.FormService.Models;
using Point.Infrastructure.Extensions;
using Point.Models.Enums;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Point.FormService.Logic
{
    public class OrganizationBL : BaseBL
    {

        public async Task<OrganizationInfo> GetOrganizationByID(int organizationID)
        {
            var organizationinfo = new OrganizationInfo();

            var organization = await uow.OrganizationRepository.Get(org => org.OrganizationID == organizationID).FirstOrDefaultAsync();
            if (organization != null)
            {
                organizationinfo.OrganizationID = organization.OrganizationID;
                organizationinfo.OrganizationName = organization.Name;
                organizationinfo.OrganizationTypeID = organization.OrganizationTypeID;
                organizationinfo.OrganizationType = ((OrganizationTypeID)organization.OrganizationTypeID).GetDescription();
                organizationinfo.RegionID = organization.RegionID;
                var regions = await new RegionBL().GetRegionsByOrganizationID(organization.OrganizationID);
                organizationinfo.Regions = regions.ToList();
            }

            return organizationinfo;
        }

        public async Task<OrganizationInfo> GetByLocationID(int locationID)
        {
            var organizationid = await uow.LocationRepository.Get(loc => loc.LocationID == locationID).Select(loc => loc.OrganizationID).FirstOrDefaultAsync();

            return await GetOrganizationByID(organizationid);
        }

        

    }
}