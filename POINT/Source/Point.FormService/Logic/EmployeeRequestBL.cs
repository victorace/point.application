﻿using Point.Business.Logic;
using Point.Database.Models;
using Point.FormService.Models;
using Point.Infrastructure.Extensions;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Point.FormService.Logic
{
    public class EmployeeRequestBL : BaseBL
    {
        public async Task<int> GetEmployeeID(EmployeeRequest employeeRequest)
        {
            var employeeresponse = await GetEmployeesByRequest(employeeRequest);

            if (employeeresponse == null)
            {
                return -1;
            }

            return employeeresponse.EmployeeID;
        }

        public async Task<EmployeeResponse> GetEmployeeResponse(EmployeeRequest employeeRequest)
        {
            var employeeresponse = await GetEmployeesByRequest(employeeRequest);

            if (employeeresponse == null)
            {
                return new EmployeeResponse();
            }

            return employeeresponse;
        }

        public async Task<EmployeeResponse> GetEmployeesByRequest(EmployeeRequest employeeRequest)
        {
            var matchingemployees = (from employees in uow.EmployeeRepository.Get()
                                     where employees.InActive == false
                                       && ((employeeRequest.EmployeeID != null && employeeRequest.EmployeeID.Value == employees.EmployeeID)
                                          || (!string.IsNullOrEmpty(employeeRequest.EmployeeExternID) && employeeRequest.EmployeeExternID == employees.ExternID)
                                          || (!string.IsNullOrEmpty(employeeRequest.EmployeeBIG) && employeeRequest.EmployeeBIG == employees.BIG))
                                     select employees).Include("EmployeeDepartment").ToList();

            var matchingdepartmentids = matchingemployees.Select(me => me.DepartmentID);
            matchingdepartmentids = matchingdepartmentids.Union(matchingemployees.SelectMany(me => me.EmployeeDepartment).Select(ed => ed.DepartmentID));

            var matchingorgalocadeps = (from departments in uow.DepartmentRepository.Get(d => matchingdepartmentids.Contains(d.DepartmentID)
                                                                                         && (string.IsNullOrEmpty(employeeRequest.DepartmentAGB) || employeeRequest.DepartmentAGB == d.AGB)
                                                                                         && (!employeeRequest.DepartmentID.HasValue || employeeRequest.DepartmentID == d.DepartmentID))
                                       join locations in uow.LocationRepository.Get() on departments.LocationID equals locations.LocationID
                                       join organizations in uow.OrganizationRepository.Get(o => (string.IsNullOrEmpty(employeeRequest.OrganizationAGB) || employeeRequest.OrganizationAGB == o.AGB)
                                                                                         && (!employeeRequest.OrganizationID.HasValue || employeeRequest.OrganizationID == o.OrganizationID))
                                       on locations.OrganizationID equals organizations.OrganizationID
                                       select new { organizations.OrganizationID, OrganizationAGB = organizations.AGB,
                                                    locations.LocationID,
                                                    departments.DepartmentID, DepartmentExternID = departments.ExternID,  DepartmentAGB = departments.AGB }).ToListAsync().Result;

            List<EmployeeResponse> employeeResponses = new List<EmployeeResponse>();
            foreach(var matchingorgalocadep in matchingorgalocadeps)
            {
                foreach(var matchingemployee in matchingemployees.Where(me => me.DepartmentID == matchingorgalocadep.DepartmentID || me.EmployeeDepartment.Any(ed => ed.DepartmentID == matchingorgalocadep.DepartmentID)))
                {
                    employeeResponses.Add(new EmployeeResponse()
                    {
                        DepartmentAGB = matchingorgalocadep.DepartmentAGB,
                        DepartmentExternID = matchingorgalocadep.DepartmentExternID,
                        DepartmentID = matchingorgalocadep.DepartmentID,
                        ExternID = matchingemployee.ExternID,
                        EmployeeID = matchingemployee.EmployeeID,
                        FirstName = matchingemployee.FirstName,
                        LastName = matchingemployee.LastName,
                        LocationID = matchingorgalocadep.LocationID,
                        MiddleName = matchingemployee.MiddleName,
                        OrganizationAGB = matchingorgalocadep.OrganizationAGB,
                        OrganizationID = matchingorgalocadep.OrganizationID
                    });
                }
            }

            var employeeResponse = new EmployeeResponse();
            if (employeeResponses.Count > 1)
            {
                if (string.IsNullOrEmpty(employeeRequest.OrganizationAGB) && !employeeRequest.OrganizationID.HasValue 
                    && string.IsNullOrEmpty(employeeRequest.DepartmentAGB) && !employeeRequest.DepartmentID.HasValue
                    && matchingemployees.Any())
                {
                    employeeResponse = employeeResponses.FirstOrDefault(er => er.DepartmentID == matchingemployees.FirstOrDefault().DepartmentID);
                }
                else
                {
                    employeeResponse = employeeResponses.FirstOrDefault();
                }
            }
            else if (employeeResponses.Count == 1)
            {
                employeeResponse = employeeResponses.FirstOrDefault();
            }

            return await Task.FromResult(employeeResponse);
        }



        public IQueryable<Employee> GetActiveByOrganizationID(int organizationID)
        {
            return (from organizations in uow.OrganizationRepository.Get()
                    join locations in uow.LocationRepository.Get() on organizations.OrganizationID equals locations.OrganizationID
                    join departments in uow.DepartmentRepository.Get() on locations.LocationID equals departments.LocationID
                    join employees in uow.EmployeeRepository.Get() on departments.DepartmentID equals employees.DepartmentID
                    where organizations.OrganizationID == organizationID && employees.InActive == false
                    select employees);
        }
        
        public async Task<EmployeeResponse> AutoCreate(EmployeeAutoCreate employeeAutoCreate)
        {
            var employeeReponse = new EmployeeResponse();

            var employee = SecurityBL.CreateNewAccount(uow, employeeAutoCreate.OrganizationID, employeeAutoCreate.AutoCreateCode, employeeAutoCreate.ExternID,
                employeeAutoCreate.FirstName, employeeAutoCreate.MiddleName, employeeAutoCreate.LastName, employeeAutoCreate.Gender.StringToGeslacht(), 
                employeeAutoCreate.Function, employeeAutoCreate.PhoneNumber, employeeAutoCreate.Email, employeeAutoCreate.BIG);
            
            employeeReponse.Merge(employee);

            return await Task.FromResult(employeeReponse);
        }

        public async Task<IEnumerable<int>> AddToDepartment(EmployeeDepartmentRequest employeedepatmentrequest)
        {
            var departments = await new DepartmentBL().GetByOrganizationID(employeedepatmentrequest.OrganizationID);

            var departmentids = departments.Where(d => employeedepatmentrequest.DepartmentExternIDs.Contains(d.ExternID)).Select(d => d.DepartmentID);

            return await Task.FromResult(new EmployeeBL().AddToDepartment(employeedepatmentrequest.EmployeeID, departmentids));
        }
    }
}