﻿using Point.FormService.Filters;
using Point.FormService.Infrastructure;
using System.Web.Http;

namespace Point.FormService
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.Filters.Add(new UnhandledExceptionFilterAttribute());
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional },
                constraints: null,
                handler: new AuthorizationHeaderHandler(config)
            );
        }
    }
}
