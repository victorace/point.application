﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Point.FormService
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.asmx/{*pathInfo}");
        }
    }
    
}