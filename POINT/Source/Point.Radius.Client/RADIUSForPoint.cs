﻿using System;
using System.Net.nRadius;

namespace Point.RADIUS.Client
{
    public class RADIUSForPoint
    {
        private RADIUSServerSetting _rad = null;
        private string _userName = null;
        private string _passw = null;


        public RADIUSForPoint(string username, string password, int organizationId)
        {
            _rad = DbAccess.GetServerSettings(organizationId);
            _userName = username;
            _passw = password;
        }

        public bool AuthenticatePointUser()
        {
            bool succeed = CheckAuthentication(_rad.ServerIPAddress1);
            if (!succeed && !String.IsNullOrEmpty(_rad.FallbackServerIPAddress2)) succeed = CheckAuthentication(_rad.FallbackServerIPAddress2);

            return succeed;
        }

        private bool CheckAuthentication(string serverIP)
        {
            nRadius_Client radClient = new nRadius_Client(serverIP,
                    _rad.SharedSecret, _userName, _passw);
            radClient.UDPTimeout = _rad.TimeOut;

            int counter = 1;
            int result = 0;
            do // Do until specified retries reached.
            {
                result = radClient.Authenticate();
                counter++;
            }
            while (result == -2 && counter <= _rad.Retries);

            return (result == 0); // Authentication succeed.
        }
    }
}
