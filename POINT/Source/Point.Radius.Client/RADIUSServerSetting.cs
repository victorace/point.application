namespace Point.RADIUS.Client
{
    public class RADIUSServerSetting
    {
        public int RADIUSServerSettingID;

        public int OrganizationID;

        public string ServerIPAddress1;

        public string FallbackServerIPAddress2;

        public string SharedSecret;

        public short Retries;

        public short TimeOut;
    }
}
