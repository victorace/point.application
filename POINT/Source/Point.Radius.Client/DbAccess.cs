using System;
using System.Data.SqlClient;

namespace Point.RADIUS.Client
{
    public class DbAccess
    {
        public static RADIUSServerSetting GetServerSettings(int organizationId)
        {
            using (SqlConnection sqlConnection = SqlConnectionFactory.GetSqlConnection())
            {
                sqlConnection.Open();
                SqlCommand com = new SqlCommand(String.Format("SELECT * FROM RADIUSServerSetting WHERE OrganizationID={0}", organizationId),
                    sqlConnection);

                SqlDataReader reader = com.ExecuteReader();
                RADIUSServerSetting rad = new RADIUSServerSetting();

                while (reader.Read())
                {
                    rad.RADIUSServerSettingID = Convert.ToInt32(reader[0]);
                    rad.OrganizationID = Convert.ToInt32(reader[1]);                    
                    rad.ServerIPAddress1 = reader[2].ToString();
                    if (!reader.IsDBNull(3))
                        rad.FallbackServerIPAddress2 = reader[3].ToString();
                    if (!reader.IsDBNull(4))
                        rad.SharedSecret = reader[4].ToString();
                    if (!reader.IsDBNull(5))
                        rad.Retries = (short)reader[5];
                    if (!reader.IsDBNull(6))
                        rad.TimeOut = (short)reader[6];
                }
                sqlConnection.Close();

                return rad;
            }
        }
    }
}
