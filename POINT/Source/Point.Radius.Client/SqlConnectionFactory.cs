﻿using System.Configuration;
using System.Data.SqlClient;

namespace Point.RADIUS.Client
{
    /// <summary>
    /// GoF Design Patterns: Factory Method.
    /// </summary>
    public sealed class SqlConnectionFactory
    {
        /// <summary>
        /// Returns a the initialized connection.
        /// </summary>
        /// <returns></returns>
        public static SqlConnection GetSqlConnection()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["SqlRoleAndMembershipConnection"].ConnectionString;
            return new SqlConnection(connectionString);
        }
    }
}
