﻿using Microsoft.Reporting.WebForms;
using Point.Business.Logic;
using Point.Database.Context;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Extensions;
using Point.Models.ViewModels;
using Point.MVC.Reporting.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Web;

namespace Point.MVC.Reporting.Logic
{
    public class ReportViewModelBL
    {
        public static ReportingOutputViewModel GetByFilterViewModelAndDataSource(FilterViewModel viewmodel, ReportDataSource datasource)
        {
            using (var uow = new UnitOfWork<PointContext>())
            {
                var reportdefinition = ReportDefinitionBL.GetByReportDefinitionID(uow, viewmodel.ReportDefinitionID);
                if (reportdefinition == null)
                {
                    throw new Exception("Rapport niet gevonden");
                }

                string reportpath = HttpContext.Current.Server.MapPath("~/Reports");

                var reportviewmodel = new ReportingOutputViewModel
                {
                    Filter = viewmodel,
                    UseExcel = viewmodel.UseExcel,
                    UseCSV = viewmodel.UseCSV,
                    ReportName = reportdefinition.ReportName,
                    ReportFileName = Path.Combine(reportpath, viewmodel.UseExcel ? reportdefinition.ExcelFriendlyFileName : reportdefinition.FileName),
                    ReportDataSource = datasource,
                    ReportParameters = new List<ReportParameter>() {
                        new ReportParameter("StartDate", viewmodel.StartDate.ToLongDateString()),
                        new ReportParameter("EndDate", viewmodel.EndDate.ToLongDateString()),
                        new ReportParameter("DossierStatus", viewmodel.DossierStatus == null ? "Alle dossiers" : viewmodel.DossierStatus.GetDescription()),
                    }
                };

                return reportviewmodel;
            }
        }
    }
}