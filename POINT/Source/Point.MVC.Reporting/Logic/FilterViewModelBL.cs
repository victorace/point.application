﻿using Point.Business.Helper;
using Point.Business.Logic;
using Point.Database.Context;
using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.MVC.Reporting.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ReportDefinition = Point.Database.Models.ReportDefinition;
using Point.Infrastructure.Extensions;
using Point.MVC.Reporting.Helpers;

namespace Point.MVC.Reporting.Logic
{
    public class FilterViewModelBL
    {
        private static void SetBasicFilterInfo(IUnitOfWork unitOfWork, PointUserInfo pointUserInfo, ReportingViewModel viewmodel, DossierStatus defaultstatus)
        {
            var filtercache = FilterCacheHelper.GetFilter();
            if (filtercache != null)
            {
                TypeExtensions.Merge(viewmodel, filtercache);
            }

            var accesslevel = FunctionRoleHelper.GetAccessLevel(unitOfWork, FunctionRoleTypeID.Reporting, pointUserInfo);
            var reports = new List<ReportDefinition>();
            var flowdefinitions = new List<FlowDefinition>();

            if (accesslevel.MaxLevel >= FunctionRoleLevelID.Region)
            {
                viewmodel.AvailableOrganizationTypeID = new List<SelectListItem>()
                    {
                        new SelectListItem() { Value = ((int)OrganizationTypeID.Hospital).ToString(), Text = "Ziekenhuis"},
                        new SelectListItem() { Value = ((int)OrganizationTypeID.HealthCareProvider).ToString(), Text = "VVT"},
                        new SelectListItem() { Value = ((int)OrganizationTypeID.CIZ).ToString(), Text = "Indicerende organisatie CIZ"},
                        new SelectListItem() { Value = ((int)OrganizationTypeID.GRZ).ToString(), Text = "Indicerende organisatie GRZ"},
                        new SelectListItem() { Value = ((int)OrganizationTypeID.GeneralPracticioner).ToString(), Text = "Huisarts"}
                    };
                reports = ReportDefinitionBL.GetAll(unitOfWork).ToList();
                flowdefinitions = FlowDefinitionBL.GetActiveFlowDefinitions(unitOfWork).ToList();
            }
            else if (accesslevel.MaxLevel == FunctionRoleLevelID.Organization)
            {
                reports = ReportDefinitionBL.GetByOrganizationIDs(unitOfWork, pointUserInfo.EmployeeOrganizationIDs).ToList();
                flowdefinitions = FlowDefinitionParticipationBL.GetByOrganizationIDs(unitOfWork, pointUserInfo.EmployeeOrganizationIDs).Select(it => it.FlowDefinition).Distinct().ToList();
            }
            else if (accesslevel.MaxLevel <= FunctionRoleLevelID.Location)
            {
                reports = ReportDefinitionBL.GetByLocationIDs(unitOfWork, pointUserInfo.EmployeeLocationIDs).ToList();
                flowdefinitions = FlowDefinitionParticipationBL.GetByLocationIDs(unitOfWork, pointUserInfo.EmployeeLocationIDs).Select(it => it.FlowDefinition).Distinct().ToList();
            }

            reports.GroupBy(r => r.Category).ToList()
                .ForEach(r =>
                    viewmodel.ReportGroups.Add(
                            r.Key,
                            r.Select(i => new ReportDefinitionViewModel() { ID = i.ReportDefinitionID.ToString(), Name = i.ReportName, Description = i.Description }).ToList()
                        ));

            viewmodel.AvailableFlowDefinitionIDs = flowdefinitions.Select(f => new SelectListItem() { Value = f.FlowDefinitionID.ToString(), Text = f.Name }).ToList();
            viewmodel.AvailableDossierStatuses.Add(new SelectListItem() { Value = null, Text = "Alle (geen filter)", Selected = defaultstatus == DossierStatus.All });
            viewmodel.AvailableDossierStatuses.Add(new SelectListItem() { Value = ((int)DossierStatus.Active).ToString(), Text = "Alleen actieve dossiers", Selected = defaultstatus == DossierStatus.Active });
            viewmodel.AvailableDossierStatuses.Add(new SelectListItem() { Value = ((int)DossierStatus.IsClosed).ToString(), Text = "Alleen afgesloten dossiers", Selected = defaultstatus == DossierStatus.IsClosed });

            if (accesslevel.RegionIDs.Any() && accesslevel.MaxLevel == FunctionRoleLevelID.Global)
            {
                viewmodel.AvailableRegions = new SelectList(RegionBL.GetAll(unitOfWork).OrderBy(r => r.Name), "RegionID", "Name").ToList();
                viewmodel.AvailableRegions.Insert(0, new SelectListItem() { Text = " - Maak een keuze - ", Value = "" });
            }
            else if (accesslevel.RegionIDs.Any() && accesslevel.MaxLevel == FunctionRoleLevelID.Region)
            {
                viewmodel.AvailableRegions = new SelectList(new Region[] { pointUserInfo.Region }, "RegionID", "Name").ToList();
                viewmodel.RegionID = pointUserInfo.Region?.RegionID;
            }
            else if (accesslevel.OrganizationIDs.Any() && accesslevel.MaxLevel == FunctionRoleLevelID.Organization)
            {
                viewmodel.AvailableOrganizations = new SelectList(OrganizationBL.GetByOrganizationIDs(unitOfWork, accesslevel.OrganizationIDs), "OrganizationID", "Name").ToList();
            }
            else if (accesslevel.LocationIDs.Any() && accesslevel.MaxLevel == FunctionRoleLevelID.Location)
            {
                viewmodel.AvailableLocations = new SelectList(LocationBL.GetActiveByLocationIDs(unitOfWork, accesslevel.LocationIDs), "LocationID", "Name").ToList();
            }
            else if (accesslevel.MaxLevel == FunctionRoleLevelID.Department)
            {
                viewmodel.AvailableDepartments = new SelectList(DepartmentBL.GetActiveByDepartmentIDs(unitOfWork, pointUserInfo.EmployeeDepartmentIDs.ToArray()), "DepartmentID", "Name").ToList();
            }
            if (accesslevel.OrganizationIDs.Any() && accesslevel.MaxLevel <= FunctionRoleLevelID.Organization)
            {
                var organizationprojects = FlowInstanceTagBL.GetAllUsedTags(accesslevel.OrganizationIDs);
                viewmodel.AvailableOrganizationProjects = new SelectList(organizationprojects).ToList();
            }

            if(viewmodel.RegionID.HasValue)
            {
                if(accesslevel.MaxLevel <=  FunctionRoleLevelID.Organization)
                {
                    viewmodel.AvailableOrganizations = new SelectList(OrganizationBL.GetByOrganizationIDs(unitOfWork, accesslevel.OrganizationIDs), "OrganizationID", "Name").ToList();
                } else if(viewmodel.OrganizationTypeIDs.Any())
                {
                    viewmodel.AvailableOrganizations = new SelectList(OrganizationBL.GetActiveByRegionIDAndOrganizationTypeIDs(unitOfWork, viewmodel.RegionID.Value, viewmodel.OrganizationTypeIDs), "OrganizationID", "Name").ToList();
                } else
                {
                    viewmodel.AvailableOrganizations = new SelectList(OrganizationBL.GetActiveByRegionID(unitOfWork, viewmodel.RegionID.Value), "OrganizationID", "Name").ToList();
                }
            }

            if (viewmodel.OrganizationIDs.Any())
            {
                if (accesslevel.MaxLevel <= FunctionRoleLevelID.Location)
                {
                    viewmodel.AvailableLocations = new SelectList(LocationBL.GetActiveByLocationIDs(unitOfWork, accesslevel.LocationIDs), "LocationID", "FullNameProp").OrderBy(i => i.Text).ToList();
                }
                else
                {
                    viewmodel.AvailableLocations = new SelectList(LocationBL.GetActiveByOrganisationIDs(unitOfWork, viewmodel.OrganizationIDs), "LocationID", "FullNameProp").OrderBy(i => i.Text).ToList();
                }
            }

            if (viewmodel.LocationIDs.Any())
            {
                if (accesslevel.MaxLevel <= FunctionRoleLevelID.Department)
                {
                    viewmodel.AvailableDepartments = new SelectList(DepartmentBL.GetByDepartmentIDs(unitOfWork, pointUserInfo.EmployeeDepartmentIDs.ToArray()), "DepartmentID", "FullNameProp").OrderBy(i => i.Text).ToList();
                }
                else
                {
                    viewmodel.AvailableDepartments = new SelectList(DepartmentBL.GetByLocationIDs(unitOfWork, viewmodel.LocationIDs), "DepartmentID", "FullNameProp").OrderBy(i => i.Text).ToList();
                }
            }

        }

        public static KPIFilterViewModel GetNewKPI(DateTime startDate, DateTime endDate, PointUserInfo pointUserInfo, string reportName)
        {
            using (var unitOfWork = new UnitOfWork<PointSearchContext>())
            {
                var viewmodel = new KPIFilterViewModel()
                {
                    StartDate = startDate,
                    EndDate = endDate,
                    Name = reportName
                };

                SetBasicFilterInfo(unitOfWork, pointUserInfo, viewmodel, DossierStatus.IsClosed);
                

                return viewmodel;
            }
        }

        public static CompareFilterViewModel GetNewCompareFilter(DateTime startDate, DateTime endDate, PointUserInfo pointUserInfo, string reportName)
        {
            using (var unitOfWork = new UnitOfWork<PointSearchContext>())
            {
                var viewmodel = new CompareFilterViewModel()
                {
                    StartDate = startDate,
                    EndDate = endDate,
                    Name = reportName
                };

                SetBasicFilterInfo(unitOfWork, pointUserInfo, viewmodel, DossierStatus.All);

                return viewmodel;
            }
        }

        public static ReportingViewModel GetNew(DateTime startDate, DateTime endDate, PointUserInfo pointUserInfo, string reportName)
        {
            using (var unitOfWork = new UnitOfWork<PointSearchContext>())
            {
                var viewmodel = new ReportingViewModel()
                {
                    StartDate = startDate,
                    EndDate = endDate,
                    Name = reportName
                };

                SetBasicFilterInfo(unitOfWork, pointUserInfo, viewmodel, DossierStatus.All);

                return viewmodel;
            }
        }
    }
}