﻿using Point.Business.Expressions;
using Point.Business.Helper;
using Point.Business.Logic;
using Point.Database.Context;
using Point.Database.Extensions;
using Point.Database.Helpers;
using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Models.ReportModels;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Models.ViewModels;
using Point.MVC.Reporting.Expressions;
using Point.MVC.Reporting.Extensions;
using Point.MVC.Reporting.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Point.Infrastructure.Extensions;

namespace Point.MVC.Reporting.Logic
{
    public class ReportModelBL
    {
        private const int maxphasedays = 100;

        public static List<DICAReportModel> GetDicaReportData(FilterViewModel filterviewmodel)
        {
            DateTime nulldate = new DateTime(1809, 9, 19);
            string nulltime = "99";

            using (var uow = new UnitOfWork<PointSearchContext>())
            {
                var pointUserInfo = PointUserInfoHelper.GetPointUserInfo(uow);
                var query = FlowInstanceReportValuesBL.GetQuery(uow);

                //Apart filter voor DICA
                query = query.ApplyFilterViewModel(filterviewmodel, FlowDirection.Sending, pointUserInfo, applyDateTimeFilter: false);
                query = query.Where(q =>
                        q.PatientWordtOpgenomenDatum != null && 
                        filterviewmodel.StartDate <= q.PatientWordtOpgenomenDatum && 
                        filterviewmodel.EndDate >= q.PatientWordtOpgenomenDatum
                    );

                var basicreportdata = query.Select(FlowInstanceReportValuesExpressions.DICAPartialDatafromModel).ToList();

                foreach (var reportrow in basicreportdata)
                {
                    var flowfieldvalues = FlowWebFieldBL.GetFastDICAValuesByFlowInstance(uow, reportrow.FlowInstanceID);

                    reportrow.Land = flowfieldvalues.GetFlowFieldValue(FlowFieldConstants.ID_DICALand);

                    //DiagnoseID ?????
                    var organizationoutsidepoint = reportrow.OrganizationID.HasValue ? OrganizationOutsidePointBL.GetByOrganizationID(uow, reportrow.OrganizationID.Value) : null;
                    reportrow.Diagnoseid = organizationoutsidepoint?.Code;

                    reportrow.Diagnose_type = flowfieldvalues.GetFlowFieldValue(FlowFieldConstants.ID_Diagnose);

                    var diagnoseAankomstSEHTijd = flowfieldvalues.GetFlowFieldValue(FlowFieldConstants.ID_DiagnoseAankomstSEHTijd);
                    reportrow.Presseh = flowfieldvalues.GetFlowFieldDateTimeValue(FlowFieldConstants.ID_DiagnoseAankomstSEHDatum).GetValueOrDefault(nulldate);
                    reportrow.Pressehuur = String.IsNullOrEmpty(diagnoseAankomstSEHTijd) ? nulltime : diagnoseAankomstSEHTijd.Split(':')[0];
                    reportrow.Presseminuut = String.IsNullOrEmpty(diagnoseAankomstSEHTijd) ? nulltime : diagnoseAankomstSEHTijd.Split(':')[1];

                    reportrow.Tromiv = flowfieldvalues.GetFlowFieldValue(FlowFieldConstants.ID_InZHIntraveneusGetrombolyseerd).StringToNullableBool() == true ? 1 : 0;
                    if (reportrow.Tromiv == 1)
                    {
                        var trombolyseIntraveneusTijd = flowfieldvalues.GetFlowFieldValue(FlowFieldConstants.ID_TrombolyseIntraveneusTijd);
                        reportrow.Tromven = flowfieldvalues.GetFlowFieldDateTimeValue(FlowFieldConstants.ID_TrombolyseIntraveneusDatum).GetValueOrDefault(nulldate);
                        reportrow.Tromvenuur = String.IsNullOrEmpty(trombolyseIntraveneusTijd) ? nulltime : trombolyseIntraveneusTijd.Split(':')[0];
                        reportrow.Tromvenminuut = String.IsNullOrEmpty(trombolyseIntraveneusTijd) ? nulltime : trombolyseIntraveneusTijd.Split(':')[1];
                    }

                    reportrow.Tromia = flowfieldvalues.GetFlowFieldValue(FlowFieldConstants.ID_InZHIntraArterieelBehandeld).StringToNullableBool() == true ? 1 : 0;
                    if (reportrow.Tromia == 1)
                    {
                        var trombolyseArterieelTijd = flowfieldvalues.GetFlowFieldValue(FlowFieldConstants.ID_TrombolyseArterieelTijd);
                        reportrow.Tromart = flowfieldvalues.GetFlowFieldDateTimeValue(FlowFieldConstants.ID_TrombolyseArterieelDatum).GetValueOrDefault(nulldate);
                        reportrow.Tromartuur = String.IsNullOrEmpty(trombolyseArterieelTijd) ? nulltime : trombolyseArterieelTijd.Split(':')[0];
                        reportrow.Tromartminuut = String.IsNullOrEmpty(trombolyseArterieelTijd) ? nulltime : trombolyseArterieelTijd.Split(':')[1];
                    }

                    reportrow.Redenlatetrom = flowfieldvalues.GetFlowFieldValue(FlowFieldConstants.ID_RedenTrombolyseVerlaatUitgevoerd);
                    reportrow.Datumfu = flowfieldvalues.GetFlowFieldDateTimeValue(FlowFieldConstants.ID_FollowUpDatum);

                    var modifiedRankingScale = flowfieldvalues.GetFlowFieldValue(FlowFieldConstants.ID_ModifiedRankingScale) ?? "-1";
                    reportrow.Mrs3maandfu = modifiedRankingScale == "-1" ? "" : modifiedRankingScale;

                    var redenfollowupnietgedaan = flowfieldvalues.GetFlowFieldValue(FlowFieldConstants.ID_FollowUpNietGedaan) ?? "-1";
                    reportrow.Funietgedaan = redenfollowupnietgedaan == "-1" ? "" : redenfollowupnietgedaan;

                    var medischeSituatieNIHSSBijOpname = flowfieldvalues.GetFlowFieldValue(FlowFieldConstants.ID_MedischeSituatieNIHSSBijOpname);
                    reportrow.Nihss_totaal = medischeSituatieNIHSSBijOpname == "-1" ? nulltime : medischeSituatieNIHSSBijOpname;

              
                    var Datum1eKlachtBekend = flowfieldvalues.GetFlowFieldValue(FlowFieldConstants.ID_DiagnoseEersteKlachtDatumBekend)?.ToString();
                    
                    switch (Datum1eKlachtBekend)
                    {
                        case "0": //nee
                            reportrow.Datum1eklacht = new DateTime(1809,9,9);
                            reportrow.Datum1eklachtuur = nulltime;
                            reportrow.Datum1eklachtminuut = nulltime;
                            break;
                        case "1": //ja
                            var Datum1eKlacht = flowfieldvalues.GetFlowFieldDateTimeValue(FlowFieldConstants.ID_DiagnoseEersteKlachtDatum);
                            var Datum1eKlachttijd = flowfieldvalues.GetFlowFieldValue(FlowFieldConstants.ID_DiagnoseEersteKlachtTijd);
                            reportrow.Datum1eklacht = Datum1eKlacht ?? nulldate;
                            reportrow.Datum1eklachtuur = string.IsNullOrEmpty(Datum1eKlachttijd) ? nulltime : Datum1eKlachttijd.Split(':')[0];
                            reportrow.Datum1eklachtminuut = string.IsNullOrEmpty(Datum1eKlachttijd) ? nulltime : Datum1eKlachttijd.Split(':')[1];
                            break;
                        case "2": //wakeup strok
                            reportrow.Datum1eklacht = new DateTime(1806, 6, 6);
                            reportrow.Datum1eklachtuur = nulltime;
                            reportrow.Datum1eklachtminuut = nulltime;
                            break;
                        default:
                            reportrow.Datum1eklacht = nulldate;
                            reportrow.Datum1eklachtuur = nulltime;
                            reportrow.Datum1eklachtminuut = nulltime;
                            break;
                    }                   
                    var patientOverleden = flowfieldvalues.GetFlowFieldValue(FlowFieldConstants.ID_BeloopOpname).ToNullableInt();
                    if (patientOverleden == 2)
                    {
                        reportrow.DatumOverleden = flowfieldvalues.GetFlowFieldDateTimeValue(FlowFieldConstants.ID_PatientOntslagenDatum);                                             
                    }
                    if (flowfieldvalues.GetFlowFieldDateTimeValue(FlowFieldConstants.ID_DiagnoseAankomstSEHDatum).HasValue &&
                    !String.IsNullOrEmpty(flowfieldvalues.GetFlowFieldValue(FlowFieldConstants.ID_DiagnoseAankomstSEHTijd)) &&
                    flowfieldvalues.GetFlowFieldDateTimeValue(FlowFieldConstants.ID_TrombolyseIntraveneusDatum).HasValue &&
                    !String.IsNullOrEmpty(flowfieldvalues.GetFlowFieldValue(FlowFieldConstants.ID_TrombolyseIntraveneusTijd)))
                    {
                        var aankomstdate = reportrow.Presseh;
                        if(int.TryParse(reportrow.Pressehuur, out int urenaankomst) && urenaankomst <= 24)
                        {
                            aankomstdate = aankomstdate.AddHours(urenaankomst);
                        }
                        if (int.TryParse(reportrow.Presseminuut, out int minutenaankomst) && minutenaankomst < 60)
                        {
                            aankomstdate = aankomstdate.AddMinutes(minutenaankomst);
                        }

                        var intravendate = reportrow.Tromven;
                        if(intravendate.HasValue && int.TryParse(reportrow.Tromvenuur, out int urenintraven) && urenintraven <= 24)
                        {
                            intravendate = intravendate.Value.AddHours(urenintraven);
                        }
                        if (intravendate.HasValue && int.TryParse(reportrow.Tromvenminuut, out int minutenintraven) && minutenintraven < 60)
                        {
                            intravendate = intravendate.Value.AddMinutes(minutenintraven);
                        }

                        var difference = DateHelper.DiffMinutes(aankomstdate, intravendate);
                        reportrow.Door_to_needle_time = difference == null ? "" : difference.ToString();
                    }

                    reportrow.Verwezen = flowfieldvalues.GetFlowFieldValue(FlowFieldConstants.ID_VerwezenUitAnderZiekenhuis).StringToNullableBool() == true ? 1 : 0;
                }

                return basicreportdata;
            }
        }

        public static List<StroomTrendModel> GetTransferMonitorReportData(CompareFilterViewModel filterviewmodel)
        {
            using (var uow = new UnitOfWork<PointSearchContext>())
            {
                var pointUserInfo = PointUserInfoHelper.GetPointUserInfo(uow);

                var filterA = filterviewmodel as FilterViewModel;

                var filterB = new FilterViewModel();
                filterB.Merge(filterviewmodel);
                filterB.DepartmentIDs = filterviewmodel.CompareDepartmentIDs;

                var queryA = FlowInstanceReportValuesBL.GetQuery(uow);
                queryA = queryA.ApplyFilterViewModel(filterA, FlowDirection.Sending, pointUserInfo);

                var queryB = FlowInstanceReportValuesBL.GetQuery(uow);
                queryB = queryB.ApplyFilterViewModel(filterB, FlowDirection.Sending, pointUserInfo);

                var setadepartments = DepartmentBL.GetByDepartmentIDs(uow, filterviewmodel.DepartmentIDs).Select(it => it.Name);
                var setbdepartments = DepartmentBL.GetByDepartmentIDs(uow, filterviewmodel.CompareDepartmentIDs).Select(it => it.Name);

                var setadetails = string.Join(", ", setadepartments.Any() ? setadepartments : new string[] { "Alle" });
                var setbdetails = string.Join(", ", setbdepartments.Any() ? setbdepartments : new string[] { "Alle" });

                var List = queryA.Select(FlowInstanceReportValuesExpressions.StroomTrendDataFromModelWithSetName("A", setadetails)).ToList();
                var ListB = queryB.Select(FlowInstanceReportValuesExpressions.StroomTrendDataFromModelWithSetName("B", setbdetails)).ToList();
                List.AddRange(ListB);

                var laatste7dagen = filterviewmodel.EndDate.AddDays(-7);

                List.ForEach(l => {
                    l.PeriodeNaam = l.TransferCreatedDate > laatste7dagen ? "Laatste 7 dagen" : l.PeriodeNaam;

                    if (l.OntvangBesluitFaseDagen.HasValue)
                    {
                        l.DossierPhaseType = "4 - " + DossierPhaseType.Overdracht.GetDescription();
                    }
                    else if (l.TransferFaseDagen.HasValue)
                    {
                        l.DossierPhaseType = "3 - " + DossierPhaseType.ReceiveAndDecision.GetDescription();
                    }
                    else if (l.PreTransferFaseDagen.HasValue)
                    {
                        l.DossierPhaseType = "2 - " + DossierPhaseType.Transfer.GetDescription();
                    }
                    else
                    {
                        l.DossierPhaseType = "1 - " + DossierPhaseType.PreTransfer.GetDescription();
                    }
                });

                return List;
            }
        }

        public static List<DumpReportModel> GetDumpReportData(FilterViewModel filterviewmodel, PointUserInfo pointUserInfo)
        {
            using (var uow = new UnitOfWork<PointSearchContext>())
            {
                var query = FlowInstanceReportValuesBL.GetQuery(uow);
                query = query.ApplyFilterViewModel(filterviewmodel, FlowDirection.Any, pointUserInfo);

                return query.Select(FlowInstanceReportValuesExpressions.AllDataFromModel).ToList();
            }
        }

        public static List<DumpReportModel> GetDumpReportData(FilterViewModel filterviewmodel, FlowDirection flowDirection)
        {
            using (var uow = new UnitOfWork<PointSearchContext>())
            {
                var pointUserInfo = PointUserInfoHelper.GetPointUserInfo(uow);
                var query = FlowInstanceReportValuesBL.GetQuery(uow);
                query = query.ApplyFilterViewModel(filterviewmodel, flowDirection, pointUserInfo);

                return query.Select(FlowInstanceReportValuesExpressions.AllDataFromModel).ToList();
            }
        }

        public static List<StroomReportModel> GetUitstroomReportData(FilterViewModel filterviewmodel)
        {
            using (var uow = new UnitOfWork<PointSearchContext>())
            {
                var pointUserInfo = PointUserInfoHelper.GetPointUserInfo(uow);
                var query = FlowInstanceReportValuesBL.GetQuery(uow);
                query = query.ApplyFilterViewModel(filterviewmodel, FlowDirection.Sending, pointUserInfo);

                return query
                    .Select(FlowInstanceReportValuesExpressions.StroomDataFromModel)
                    .ToList()
                    .OrderBy(it => it.SendingOrganizationName)
                    .ThenBy(it => it.SendingLocationName)
                    .ThenBy(it => it.SendingDepartmentName)
                    .ThenBy(it => it.SendingDepartmentAfterCareType)
                    .ThenBy(it => it.ReceivingOrganizationName)
                    .ThenBy(it => it.ReceivingLocationName)
                    .ThenBy(it => it.ReceivingDepartmentName)
                    .ToList();
            }
        }

        public static List<StroomReportModel> GetInstroomReportData(FilterViewModel filterviewmodel)
        {
            using (var uow = new UnitOfWork<PointSearchContext>())
            {
                var pointUserInfo = PointUserInfoHelper.GetPointUserInfo(uow);
                var query = FlowInstanceReportValuesBL.GetQuery(uow);
                query = query.ApplyFilterViewModel(filterviewmodel, FlowDirection.Receiving, pointUserInfo, receiveStateFilter: ReceiveStateFilter.Active);

                return query
                    .Select(FlowInstanceReportValuesExpressions.StroomDataFromModel)
                    .ToList()
                    .OrderBy(it => it.ReceivingOrganizationName)
                    .ThenBy(it => it.ReceivingLocationName)
                    .ThenBy(it => it.ReceivingDepartmentName)
                    .ThenBy(it => it.ReceivingDepartmentAfterCareType)
                    .ThenBy(it => it.SendingOrganizationName)
                    .ThenBy(it => it.SendingLocationName)
                    .ThenBy(it => it.SendingDepartmentName)
                    .ToList();
            }
        }
        
        public static List<AfdelingReportModel> GetAfdelingReportData(FilterViewModel filterviewmodel)
        {
            using (var uow = new UnitOfWork<PointSearchContext>())
            {
                var pointUserInfo = PointUserInfoHelper.GetPointUserInfo(uow);
                var query = DepartmentBL.GetQuery(uow);

                query = query.ApplyFilterViewModel(filterviewmodel, pointUserInfo);

                return query
                    .Select(DepartmentExpressions.AfdelingReportFromModel)
                    .ToList();
            }
        }

        public static List<AfdelingCapacityReportModel> GetAfdelingCapacityReportData(FilterViewModel filterviewmodel)
        {
            using (var uow = new UnitOfWork<PointSearchContext>())
            {
                var pointUserInfo = PointUserInfoHelper.GetPointUserInfo(uow);
                var query = DepartmentBL.GetCapacityQuery(uow);

                query = query.ApplyFilterViewModel(filterviewmodel, pointUserInfo);

                return query
                    .Select(DepartmentExpressions.AfdelingCapacityReportFromModel)
                    .ToList()
                    .GroupBy(c => new { c.DepartmentID, c.CapacityDateTime.Date })
                    .OrderBy(g => g.Key.DepartmentID).ThenBy(g => g.Key.Date)
                    .Select(g =>
                        new AfdelingCapacityReportModel()
                        {
                            Organisatie = g.FirstOrDefault().Organisatie,
                            Locatie = g.FirstOrDefault().Locatie,
                            Afdeling = g.FirstOrDefault().Afdeling,
                            Capacity = g.FirstOrDefault().Capacity,
                            CapacityDateTime = new DateTime(g.Key.Date.Year, g.Key.Date.Month, g.Key.Date.Day),
                            DepartmentID = g.Key.DepartmentID,
                            Specialisme = g.FirstOrDefault().Specialisme,
                            SpecialismeCategorie = g.FirstOrDefault().SpecialismeCategorie,
                            MaxCapacity = g.Max(i => i.Capacity),
                            MinCapacity = g.Min(i => i.Capacity)
                        }
                    ).ToList();
            }
        }

        public static List<StroomTrendModel> GetUitStroomTrendReportData(FilterViewModel filterviewmodel)
        {
            using (var uow = new UnitOfWork<PointSearchContext>())
            {
                var pointUserInfo = PointUserInfoHelper.GetPointUserInfo(uow);
                var query = FlowInstanceReportValuesBL.GetQuery(uow);
                query = query.ApplyFilterViewModel(filterviewmodel, FlowDirection.Sending, pointUserInfo);

                return query
                    .Select(FlowInstanceReportValuesExpressions.StroomTrendDataFromModel)
                    .ToList();
            }
        }

        public static List<StroomTrendModel> GetUitStroomProcesTijdenReportData(FilterViewModel filterviewmodel)
        {
            using (var uow = new UnitOfWork<PointSearchContext>())
            {
                var pointUserInfo = PointUserInfoHelper.GetPointUserInfo(uow);
                var query = FlowInstanceReportValuesBL.GetQuery(uow);
                query = query.ApplyFilterViewModel(filterviewmodel, FlowDirection.Sending, pointUserInfo);

                return query
                    .Select(FlowInstanceReportValuesExpressions.StroomTrendDataFromModel)
                    .Where(m =>
                        m.PreTransferFaseDagen >= 0 && m.PreTransferFaseDagen < maxphasedays &&
                        m.TransferFaseDagen >= 0 && m.TransferFaseDagen < maxphasedays &&
                        m.OntvangFaseDagen >= 0 && m.OntvangFaseDagen < maxphasedays &&
                        m.BesluitFaseDagen >= 0 && m.BesluitFaseDagen < maxphasedays &&
                        m.OverdrachtFaseDagen >= 0 && m.OverdrachtFaseDagen < maxphasedays
                    )
                    .ToList();
            }
        }

        public static List<StroomTrendModel> GetUitStroomKPIInBehandelingReportData(KPIFilterViewModel filterviewmodel)
        {
            using (var uow = new UnitOfWork<PointSearchContext>())
            {
                var pointUserInfo = PointUserInfoHelper.GetPointUserInfo(uow);
                var query = FlowInstanceReportValuesBL.GetQuery(uow);
                query = query.ApplyFilterViewModel(filterviewmodel, FlowDirection.Sending, pointUserInfo);
                query = query.Where(FlowInstanceReportValuesExpressions.WithReceiver);

                var fasedata = query
                    .Select(FlowInstanceReportValuesExpressions.StroomTrendDataFromModel)
                    .Where(m =>
                        //Filter afwijkende data weg
                        (m.OntvangFaseDagen >= 0 && m.OntvangFaseDagen < maxphasedays)
                    )
                    .ToList();

                double hoursExtramuraal = TimeSpan.Parse(filterviewmodel.NormTijdExtramuraal).TotalHours;
                double hoursIntramuraal = TimeSpan.Parse(filterviewmodel.NormTijdIntramuraal).TotalHours;
                double hoursHospice = TimeSpan.Parse(filterviewmodel.NormTijdHospice).TotalHours;

                fasedata.ForEach(f => {

                    f.NormTijd = f.NazorgCategorie == "Zorginstelling" ? hoursIntramuraal :
                                            f.NazorgCategorie == "Thuiszorg" ? hoursExtramuraal :
                                                f.NazorgCategorie == "Hospice" ? hoursHospice : (double?)null;

                    f.NormGroupAverage = f.NazorgCategorie == "Zorginstelling" ? filterviewmodel.NormIntramuraal :
                                            f.NazorgCategorie == "Thuiszorg" ? filterviewmodel.NormExtramuraal :
                                                f.NazorgCategorie == "Hospice" ? filterviewmodel.NormHospice : (int?)null;
                });

                return fasedata;
            }
        }

        public static List<StroomTrendModel> GetUitStroomKPITerugmeldingReportData(KPIFilterViewModel filterviewmodel)
        {
            using (var uow = new UnitOfWork<PointSearchContext>())
            {
                var pointUserInfo = PointUserInfoHelper.GetPointUserInfo(uow);
                var query = FlowInstanceReportValuesBL.GetQuery(uow);
                query = query.ApplyFilterViewModel(filterviewmodel, FlowDirection.Sending, pointUserInfo);
                query = query.Where(FlowInstanceReportValuesExpressions.WithReceiver);

                var fasedata = query
                    .Select(FlowInstanceReportValuesExpressions.StroomTrendDataFromModel)
                    .Where(m =>
                        //Filter afwijkende data weg
                        (m.BesluitFaseDagen >= 0 && m.BesluitFaseDagen < maxphasedays)
                    )
                    .ToList();

                double hoursExtramuraal = TimeSpan.Parse(filterviewmodel.NormTijdExtramuraal).TotalHours;
                double hoursIntramuraal = TimeSpan.Parse(filterviewmodel.NormTijdIntramuraal).TotalHours;
                double hoursHospice = TimeSpan.Parse(filterviewmodel.NormTijdHospice).TotalHours;

                fasedata.ForEach(f => {

                    f.NormTijd = f.NazorgCategorie == "Zorginstelling" ? hoursIntramuraal :
                                            f.NazorgCategorie == "Thuiszorg" ? hoursExtramuraal :
                                                f.NazorgCategorie == "Hospice" ? hoursHospice : (double?)null;

                    f.NormGroupAverage = f.NazorgCategorie == "Zorginstelling" ? filterviewmodel.NormIntramuraal :
                                            f.NazorgCategorie == "Thuiszorg" ? filterviewmodel.NormExtramuraal :
                                                f.NazorgCategorie == "Hospice" ? filterviewmodel.NormHospice : (int?)null;
                });

                return fasedata;
            }
        }

        public static List<ControlInfoReportModel> GetUitStroomStuurInfoZHData(FilterViewModel filterviewmodel)
        {
            using (var uow = new UnitOfWork<PointSearchContext>())
            {
                var pointUserInfo = PointUserInfoHelper.GetPointUserInfo(uow);
                var query = FlowInstanceReportValuesBL.GetQuery(uow);
                query = query.ApplyFilterViewModel(filterviewmodel, FlowDirection.Sending, pointUserInfo);

                var returndata = query
                    .Select(FlowInstanceReportValuesExpressions.StuurInfoFromModel)
                    .ToList();

                returndata.ForEach(r =>
                {
                    r.VerkeerdeBedWeekDagen = Math.Max(DateHelper.GetBusinessDays(r.GewensteOntslagDatum, r.GerealiseerdeOntslagDatum) ?? 0, 0);
                    r.VerkeerdeBedDagen = Math.Max(r.VerkeerdeBedDagen ?? 0, 0);
                });

                return returndata;
            }
        }

        public static List<StroomTrendModel> GetInStroomProcesTijdenReportData(FilterViewModel filterviewmodel)
        {
            using (var uow = new UnitOfWork<PointSearchContext>())
            {
                var pointUserInfo = PointUserInfoHelper.GetPointUserInfo(uow);
                var query = FlowInstanceReportValuesBL.GetQuery(uow);
                query = query.ApplyFilterViewModel(filterviewmodel, FlowDirection.Receiving, pointUserInfo, receiveStateFilter: ReceiveStateFilter.Active);

                return query
                    .Select(FlowInstanceReportValuesExpressions.StroomTrendDataFromModel)
                    .Where(m =>
                        m.PreTransferFaseDagen >= 0 && m.PreTransferFaseDagen < maxphasedays &&
                        m.TransferFaseDagen >= 0 && m.TransferFaseDagen < maxphasedays &&
                        m.OntvangFaseDagen >= 0 && m.OntvangFaseDagen < maxphasedays &&
                        m.OverdrachtFaseDagen >= 0 && m.OverdrachtFaseDagen < maxphasedays
                    )
                    .ToList();
            }
        }

        public static List<Top10ReportModel> GetUitStroomTop10ReportData(FilterViewModel filterviewmodel)
        {
            using (var uow = new UnitOfWork<PointSearchContext>())
            {
                var pointUserInfo = PointUserInfoHelper.GetPointUserInfo(uow);
                var query = FlowInstanceReportValuesBL.GetQuery(uow);
                query = query.ApplyFilterViewModel(filterviewmodel, FlowDirection.Sending, pointUserInfo);
                query = query.Where(FlowInstanceReportValuesExpressions.WithReceiver);

                var stroomdata = query.Select(FlowInstanceReportValuesExpressions.StroomDataFromModel).ToList();
                var top10 = stroomdata.GroupBy(s => s.ReceivingOrganizationName).OrderByDescending(g => g.Count()).Take(10).ToList();
                var top10names = top10.Select(it => it.Key);
                var others = stroomdata.Where(s => !top10names.Contains(s.ReceivingOrganizationName));

                var top10reportmodel = new List<Top10ReportModel>();
                top10.ForEach(t => top10reportmodel.Add(new Top10ReportModel() { Organization = t.Key, Count = t.Count() }));
                top10reportmodel.Add(new Top10ReportModel() { Organization = "[Overig]", Count = others.Count(), IsOthers = true });

                top10reportmodel = top10reportmodel.OrderBy(t => t.Count).ToList();

                return top10reportmodel;
            }
        }

        public static List<DischargeDatePassedReportModel> GetUitStroomOntslagDatumGepasseerdReportData(FilterViewModel filterviewmodel)
        {
            using (var uow = new UnitOfWork<PointSearchContext>())
            {
                var pointUserInfo = PointUserInfoHelper.GetPointUserInfo(uow);
                var query = FlowInstanceReportValuesBL.GetQuery(uow);
                query = query.ApplyFilterViewModel(filterviewmodel, FlowDirection.Sending, pointUserInfo);
                query = query.Where(q => 
                    q.RealizedDischargeDate == null && 
                    q.InitialDischargeDate.HasValue && q.InitialDischargeDate < DateTime.Now);

                var returndata = query
                    .Select(FlowInstanceReportValuesExpressions.DischargeDatePassedFromModel)
                    .ToList();

                returndata.ForEach(r => r.AantalWerkDagen = DateHelper.GetBusinessDays(r.GewensteOntslagDatum, DateTime.Now));

                return returndata;
            }
        }

        public static List<DischargeDatePassedReportModel> GetInStroomOntslagDatumGepasseerdReportData(FilterViewModel filterviewmodel)
        {
            using (var uow = new UnitOfWork<PointSearchContext>())
            {
                var pointUserInfo = PointUserInfoHelper.GetPointUserInfo(uow);
                var query = FlowInstanceReportValuesBL.GetQuery(uow);
                query = query.ApplyFilterViewModel(filterviewmodel, FlowDirection.Receiving, pointUserInfo, receiveStateFilter: ReceiveStateFilter.Active);
                query = query.Where(q =>
                    q.RealizedDischargeDate == null &&
                    q.InitialDischargeDate.HasValue && q.DischargeProposedStartDate < DateTime.Now);

                var returndata = query
                    .Select(FlowInstanceReportValuesExpressions.DischargeDatePassedFromModel)
                    .ToList();

                returndata.ForEach(r => r.AantalWerkDagen = DateHelper.GetBusinessDays(r.GewensteOntslagDatum, DateTime.Now));

                return returndata;
            }
        }

        public static List<Top10ReportModel> GetInStroomTop10ReportData(FilterViewModel filterviewmodel)
        {
            using (var uow = new UnitOfWork<PointSearchContext>())
            {
                var pointUserInfo = PointUserInfoHelper.GetPointUserInfo(uow);
                var query = FlowInstanceReportValuesBL.GetQuery(uow);
                query = query.ApplyFilterViewModel(filterviewmodel, FlowDirection.Receiving, pointUserInfo, receiveStateFilter: ReceiveStateFilter.Active);

                var stroomdata = query.Select(FlowInstanceReportValuesExpressions.StroomDataFromModel).ToList();
                var top10 = stroomdata.GroupBy(s => s.SendingOrganizationName).OrderByDescending(g => g.Count()).Take(10).ToList();
                var top10names = top10.Select(it => it.Key);
                var others = stroomdata.Where(s => !top10names.Contains(s.SendingOrganizationName));

                var top10reportmodel = new List<Top10ReportModel>();
                top10.ForEach(t => top10reportmodel.Add(new Top10ReportModel() { Organization = t.Key, Count = t.Count() }));
                top10reportmodel.Add(new Top10ReportModel() { Organization = "[Overig]", Count = others.Count(), IsOthers = true });

                return top10reportmodel;
            }
        }

        public static List<StroomTrendModel> GetUitStroomOntslagDatumBehaaldReportData(FilterViewModel filterviewmodel)
        {
            using (var uow = new UnitOfWork<PointSearchContext>())
            {
                var pointUserInfo = PointUserInfoHelper.GetPointUserInfo(uow);
                var query = FlowInstanceReportValuesBL.GetQuery(uow);
                query = query.ApplyFilterViewModel(filterviewmodel, FlowDirection.Sending, pointUserInfo);

                return query
                    .Select(FlowInstanceReportValuesExpressions.StroomTrendDataFromModel)
                    .Where(m => m.OntslagDatumBehaald.HasValue)
                    .ToList();
            }
        }

        public static List<StroomTrendModel> GetInStroomOntslagDatumBehaaldReportData(FilterViewModel filterviewmodel)
        {
            using (var uow = new UnitOfWork<PointSearchContext>())
            {
                var pointUserInfo = PointUserInfoHelper.GetPointUserInfo(uow);
                var query = FlowInstanceReportValuesBL.GetQuery(uow);
                query = query.ApplyFilterViewModel(filterviewmodel, FlowDirection.Receiving, pointUserInfo, receiveStateFilter: ReceiveStateFilter.Active);

                return query
                    .Select(FlowInstanceReportValuesExpressions.StroomTrendDataFromModel)
                    .Where(m => m.OntslagDatumBehaald.HasValue)
                    .ToList();
            }
        }

        public static List<StroomTrendModel> GetUitStroomVKBReportData(FilterViewModel filterviewmodel)
        {
            using (var uow = new UnitOfWork<PointSearchContext>())
            {
                var pointUserInfo = PointUserInfoHelper.GetPointUserInfo(uow);
                var query = FlowInstanceReportValuesBL.GetQuery(uow);
                query = query.ApplyFilterViewModel(filterviewmodel, FlowDirection.Sending, pointUserInfo);

                return query
                    .Select(FlowInstanceReportValuesExpressions.StroomTrendDataFromModel)
                    .Where(m => m.OntslagDatumVerschilDagen.HasValue && m.OntslagDatumVerschilDagen > (0-maxphasedays) && m.OntslagDatumVerschilDagen < maxphasedays)
                    .ToList();
            }
        }

        public static List<StroomTrendModel> GetInStroomVKBReportData(FilterViewModel filterviewmodel)
        {
            using (var uow = new UnitOfWork<PointSearchContext>())
            {
                var pointUserInfo = PointUserInfoHelper.GetPointUserInfo(uow);
                var query = FlowInstanceReportValuesBL.GetQuery(uow);
                query = query.ApplyFilterViewModel(filterviewmodel, FlowDirection.Receiving, pointUserInfo, receiveStateFilter: ReceiveStateFilter.Active);

                return query
                    .Select(FlowInstanceReportValuesExpressions.StroomTrendDataFromModel)
                    .Where(m => m.OntslagDatumVerschilDagen.HasValue && m.OntslagDatumVerschilDagen > (0 - maxphasedays) && m.OntslagDatumVerschilDagen < maxphasedays)
                    .ToList();
            }
        }

        public static List<StroomTrendModel> GetInStroomTrendReportData(FilterViewModel filterviewmodel)
        {
            using (var uow = new UnitOfWork<PointSearchContext>())
            {
                var pointUserInfo = PointUserInfoHelper.GetPointUserInfo(uow);
                var query = FlowInstanceReportValuesBL.GetQuery(uow);
                query = query.ApplyFilterViewModel(filterviewmodel, FlowDirection.Receiving, pointUserInfo, receiveStateFilter: ReceiveStateFilter.Active);

                return query
                    .Select(FlowInstanceReportValuesExpressions.StroomTrendDataFromModel)
                    .ToList();
            }
        }

        public static List<StroomTrendModel> GetInStroomKPIInBehandelingReportData(KPIFilterViewModel filterviewmodel)
        {
            using (var uow = new UnitOfWork<PointSearchContext>())
            {
                var pointUserInfo = PointUserInfoHelper.GetPointUserInfo(uow);
                var query = FlowInstanceReportValuesBL.GetQuery(uow);
                query = query.ApplyFilterViewModel(filterviewmodel, FlowDirection.Receiving, pointUserInfo, receiveStateFilter: ReceiveStateFilter.Active);
                query = query.Where(FlowInstanceReportValuesExpressions.WithReceiver);

                var fasedata = query
                    .Select(FlowInstanceReportValuesExpressions.StroomTrendDataFromModel)
                    .Where(m =>
                        //Filter afwijkende data weg
                        (m.OntvangFaseDagen >= 0 && m.OntvangFaseDagen < maxphasedays)
                    )
                    .ToList();

                double hoursExtramuraal = TimeSpan.Parse(filterviewmodel.NormTijdExtramuraal).TotalHours;
                double hoursIntramuraal = TimeSpan.Parse(filterviewmodel.NormTijdIntramuraal).TotalHours;
                double hoursHospice = TimeSpan.Parse(filterviewmodel.NormTijdHospice).TotalHours;

                fasedata.ForEach(f => {

                    f.NormTijd = f.NazorgCategorie == "Zorginstelling" ? hoursIntramuraal :
                                            f.NazorgCategorie == "Thuiszorg" ? hoursExtramuraal :
                                                f.NazorgCategorie == "Hospice" ? hoursHospice : (double?)null;

                    f.NormGroupAverage = f.NazorgCategorie == "Zorginstelling" ? filterviewmodel.NormIntramuraal :
                                            f.NazorgCategorie == "Thuiszorg" ? filterviewmodel.NormExtramuraal :
                                                f.NazorgCategorie == "Hospice" ? filterviewmodel.NormHospice : (int?)null;
                });

                return fasedata;
            }
        }

        public static List<StroomTrendModel> GetInStroomKPITerugmeldingReportData(KPIFilterViewModel filterviewmodel)
        {
            using (var uow = new UnitOfWork<PointSearchContext>())
            {
                var pointUserInfo = PointUserInfoHelper.GetPointUserInfo(uow);
                var query = FlowInstanceReportValuesBL.GetQuery(uow);
                query = query.ApplyFilterViewModel(filterviewmodel, FlowDirection.Receiving, pointUserInfo, receiveStateFilter: ReceiveStateFilter.Active);
                query = query.Where(FlowInstanceReportValuesExpressions.WithReceiver);

                var fasedata = query
                    .Select(FlowInstanceReportValuesExpressions.StroomTrendDataFromModel)
                    .Where(m =>
                        //Filter afwijkende data weg
                        (m.BesluitFaseDagen >= 0 && m.BesluitFaseDagen < maxphasedays)
                    )
                    .ToList();

                double hoursExtramuraal = TimeSpan.Parse(filterviewmodel.NormTijdExtramuraal).TotalHours;
                double hoursIntramuraal = TimeSpan.Parse(filterviewmodel.NormTijdIntramuraal).TotalHours;
                double hoursHospice = TimeSpan.Parse(filterviewmodel.NormTijdHospice).TotalHours;

                fasedata.ForEach(f => {

                    f.NormTijd = f.NazorgCategorie == "Zorginstelling" ? hoursIntramuraal :
                                            f.NazorgCategorie == "Thuiszorg" ? hoursExtramuraal :
                                                f.NazorgCategorie == "Hospice" ? hoursHospice : (double?)null;

                    f.NormGroupAverage = f.NazorgCategorie == "Zorginstelling" ? filterviewmodel.NormIntramuraal :
                                            f.NazorgCategorie == "Thuiszorg" ? filterviewmodel.NormExtramuraal :
                                                f.NazorgCategorie == "Hospice" ? filterviewmodel.NormHospice : (int?)null;
                });

                return fasedata;
            }
        }
        
        public static List<UitvalReportModel> GetInStroomUitvalReportData(FilterViewModel filterviewmodel)
        {
            using (var uow = new UnitOfWork<PointSearchContext>())
            {
                var pointUserInfo = PointUserInfoHelper.GetPointUserInfo(uow);
                var query = FlowInstanceReportValuesBL.GetQuery(uow);
                query = query.ApplyFilterViewModel(filterviewmodel, FlowDirection.Receiving, pointUserInfo, receiveStateFilter: ReceiveStateFilter.CancelledOrRefused);

                var data = query
                    .Select(FlowInstanceReportValuesExpressions.UitvalFromModel(filterviewmodel.OrganizationIDs))
                    .Where(d => d.MyOrganizationName != d.OntvangendeOrganisatie)
                    .ToList();

                return data;
            }
        }

        public static List<UitvalReportModel> GetInStroomWaarIsDePatientGebleven(FilterViewModel filterviewmodel)
        {
            using (var uow = new UnitOfWork<PointSearchContext>())
            {
                var pointUserInfo = PointUserInfoHelper.GetPointUserInfo(uow);
                var query = FlowInstanceReportValuesBL.GetQuery(uow);
                query = query.ApplyFilterViewModel(filterviewmodel, FlowDirection.Receiving, pointUserInfo, receiveStateFilter: ReceiveStateFilter.CancelledOrRefused);

                var data = query
                    .Select(FlowInstanceReportValuesExpressions.UitvalFromModel(filterviewmodel.OrganizationIDs))
                    .Where(d => d.MyOrganizationName != d.OntvangendeOrganisatie)
                    .ToList();

                return data;
            }
        }
        
        public static List<UitvalReportModel> GetInStroomTotalenReportData(FilterViewModel filterviewmodel)
        {
            using (var uow = new UnitOfWork<PointSearchContext>())
            {
                var pointUserInfo = PointUserInfoHelper.GetPointUserInfo(uow);
                var query = FlowInstanceReportValuesBL.GetQuery(uow);
                
                query = query.ApplyFilterViewModel(filterviewmodel, FlowDirection.Receiving, pointUserInfo, receiveStateFilter: ReceiveStateFilter.CancelledOrRefusedAndActive);

                var data = query
                    .Select(FlowInstanceReportValuesExpressions.UitvalFromModel(filterviewmodel.OrganizationIDs))
                    .Where(d => d.MyOrganizationName != d.OntvangendeOrganisatie)
                    .ToList();

                data.ForEach(d => d.Different = String.IsNullOrEmpty(d.MyOrganizationName) ? 0 : 1);

                return data;
            }
        }
    }
}