﻿using Microsoft.Reporting.WebForms;
using Point.Models.ViewModels;
using System.Collections.Generic;

namespace Point.MVC.Reporting.Models.ViewModels
{
    public class ReportingOutputViewModel
    {
        public bool UseExcel { get; set; }
        public bool UseCSV { get; set; }
        public string ReportName { get; set; }
        public string ReportFileName { get; set; }
        public FilterViewModel Filter { get; set; }
        public ReportDataSource ReportDataSource { get; set; }
        public List<ReportParameter> ReportParameters { get; set; } = new List<ReportParameter>();

    }
}
