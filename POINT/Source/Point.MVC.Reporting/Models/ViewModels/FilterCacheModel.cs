﻿using Point.Models.Enums;
using System;

namespace Point.MVC.Reporting.Models.ViewModels
{
    public class FilterCacheModel
    {
        public FlowDefinitionID[] FlowDefinitionIDs { get; set; }
        public DossierStatus? DossierStatus { get; set; }
        public int? RegionID { get; set; }
        public int[] OrganizationTypeIDs { get; set; }
        public int[] OrganizationIDs { get; set; }
        public int[] LocationIDs { get; set; }
        public int[] DepartmentIDs { get; set; }
        public int[] CompareDepartmentIDs { get; set; }
        public string[] OrganizationProjects { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}