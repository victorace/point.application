﻿using Point.Models.Enums;
using Point.Models.ViewModels;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.Mvc;

namespace Point.MVC.Reporting.Models.ViewModels
{
    public class CompareFilterViewModel : ReportingViewModel
    {
        public CompareFilterViewModel() : base()
        {
            CompareDepartmentIDs = new int[] { };
        }

        [DisplayName("Afdeling(en)")]
        public int[] CompareDepartmentIDs { get; set; }
    }

    public class DumpFilterViewModel : ReportingViewModel
    {
        public DumpFilterViewModel() : base()
        {
            StoredDumpList = new List<SelectListItem>();
        }

        public List<SelectListItem> StoredDumpList { get; set; }
    }

    public class KPIFilterViewModel : ReportingViewModel
    {
        [DisplayName("Normtijd Extramuraal")]
        public string NormTijdExtramuraal { get; set; }
        [DisplayName("Normtijd Intramuraal")]
        public string NormTijdIntramuraal { get; set; }
        [DisplayName("Normtijd Hospice")]
        public string NormTijdHospice { get; set; }

        [DisplayName("Norm Extramuraal (%)")]
        public int NormExtramuraal { get; set; }
        [DisplayName("Norm Intramuraal (%)")]
        public int NormIntramuraal { get; set; }
        [DisplayName("Norm Hospice (%)")]
        public int NormHospice { get; set; }

        public KPIFilterViewModel() : base()
        {
            NormTijdExtramuraal = "02:00";
            NormTijdIntramuraal = "02:00";
            NormTijdHospice = "02:00";
            NormExtramuraal = 75;
            NormHospice = 85;
            NormIntramuraal = 85;
        }
    }

    public class ReportingViewModel : FilterViewModel
    {

        public ReportingViewModel() : base()
        {
            AvailableFlowDefinitionIDs = new List<SelectListItem>();
            AvailableOrganizationTypeID = new List<SelectListItem>();
            AvailableRegions = new List<SelectListItem>();
            AvailableOrganizations = new List<SelectListItem>();
            AvailableLocations = new List<SelectListItem>();
            AvailableDepartments = new List<SelectListItem>();
            AvailableOrganizationProjects = new List<SelectListItem>();
            AvailableDossierStatuses = new List<SelectListItem>();

            ReportGroups = new Dictionary<string, List<ReportDefinitionViewModel>>();
        }

        public string Name { get; set; }

        public Dictionary<string, List<ReportDefinitionViewModel>> ReportGroups { get; set; }

        public List<SelectListItem> AvailableDossierStatuses { get; set; }
        public List<SelectListItem> AvailableOrganizationTypeID { get; set; }
        public List<SelectListItem> AvailableFlowDefinitionIDs { get; set; }
        public List<SelectListItem> AvailableRegions { get; set; }
        public List<SelectListItem> AvailableOrganizations { get; set; }
        public List<SelectListItem> AvailableLocations { get; set; }
        public List<SelectListItem> AvailableDepartments { get; set; }
        public List<SelectListItem> AvailableOrganizationProjects { get; set; }

        public bool NeedsOrganizationID { get; set; }

    }


}