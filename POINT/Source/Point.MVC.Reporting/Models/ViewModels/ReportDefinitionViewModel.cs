﻿namespace Point.MVC.Reporting.Models.ViewModels
{
    public class ReportDefinitionViewModel
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}