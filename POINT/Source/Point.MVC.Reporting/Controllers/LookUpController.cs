﻿using Point.Business.Helper;
using Point.Business.Logic;
using Point.Database.Context;
using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Repository;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Point.MVC.Reporting.Controllers
{
    [Authorize]
    public class LookUpController : Controller
    {
        private readonly UnitOfWork<PointContext> uow = new UnitOfWork<PointContext>();

        public JsonResult GetFlowDefinitions(int reportDefinitionID)
        {
            var reportdefinition = ReportDefinitionBL.GetByReportDefinitionID(uow, reportDefinitionID);
            var pointuserinfo = PointUserInfoHelper.GetPointUserInfo(uow);
            var maxlevel = pointuserinfo.GetReportFunctionRoleLevelID();

            var flowdefinitions = new List<FlowDefinition>();
            if(maxlevel > FunctionRoleLevelID.Organization)
            {
                flowdefinitions = FlowDefinitionBL.GetActiveFlowDefinitions(uow).ToList();
            } else if(maxlevel == FunctionRoleLevelID.Organization)
            {
                flowdefinitions = FlowDefinitionParticipationBL.GetByOrganizationIDs(uow, pointuserinfo.EmployeeOrganizationIDs)
                    .Where(p => reportdefinition.ReportDefinitionFlowDefinition.Any(rfd => 
                            rfd.FlowDefinitionID == p.FlowDefinitionID &&
                            reportdefinition.FlowDirection == FlowDirection.Any || p.FlowDirection == FlowDirection.Any || reportdefinition.FlowDirection == p.FlowDirection
                    ))
                    .Select(p => p.FlowDefinition)
                    .Distinct()
                    .ToList();
            }
            else if (maxlevel <= FunctionRoleLevelID.Location)
            {
                flowdefinitions = FlowDefinitionParticipationBL.GetByLocationIDs(uow, pointuserinfo.EmployeeLocationIDs)
                    .Where(p => reportdefinition.ReportDefinitionFlowDefinition.Any(rfd =>
                            rfd.FlowDefinitionID == p.FlowDefinitionID &&
                            reportdefinition.FlowDirection == FlowDirection.Any || p.FlowDirection == FlowDirection.Any || reportdefinition.FlowDirection == p.FlowDirection
                    ))
                    .Select(p => p.FlowDefinition)
                    .Distinct()
                    .ToList();
            }

            return Json(
                flowdefinitions.Select(f => new {
                    f.FlowDefinitionID,
                    f.Name
                }).OrderBy(f => f.FlowDefinitionID), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetReportDefinition(int reportDefinitionID)
        {
            var reportdefinition = ReportDefinitionBL.GetByReportDefinitionID(uow, reportDefinitionID);

            return Json(
                new {
                    reportdefinition.ReportName,
                    reportdefinition.ReportActionName,
                    reportdefinition.FileName,
                    reportdefinition.ExcelFriendlyFileName,
                    reportdefinition.FilterPartial
                }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetReportOrganizationsByRegionIDAndOrganizationTypes(int regionID, OrganizationTypeID[] organizationTypeIDs = null)
        {
            var pointuserinfo = PointUserInfoHelper.GetPointUserInfo(uow);
            var maxlevel = pointuserinfo.GetReportFunctionRoleLevelID();
            var organizationlist = OrganizationBL.GetActiveByRegionID(uow, regionID);

            if (maxlevel < FunctionRoleLevelID.Region)
            {
                organizationlist = organizationlist.Where(org => pointuserinfo.EmployeeOrganizationIDs.Contains(org.OrganizationID));
            }

            if(organizationTypeIDs != null)
            {
                organizationlist = organizationlist.Where(org => organizationTypeIDs.Contains((OrganizationTypeID)org.OrganizationTypeID));
            }

            return Json(
                organizationlist.Select(org => new {
                    org.OrganizationID,
                    org.Name
                }).OrderBy(loc => loc.Name), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetReportLocationsByOrganizationIDs(int[] organizationIDs)
        {
            var pointuserinfo = PointUserInfoHelper.GetPointUserInfo(uow);
            var maxlevel = pointuserinfo.GetReportFunctionRoleLevelID();
            IEnumerable<Location> locationlist = null;

            locationlist = LocationBL.GetActiveByOrganisationIDs(uow, organizationIDs);
            if (maxlevel <= FunctionRoleLevelID.Location && organizationIDs.All(orgid => pointuserinfo.EmployeeOrganizationIDs.Contains(orgid)))
            {
                locationlist = locationlist.Where(loc => pointuserinfo.EmployeeLocationIDs.Contains(loc.LocationID));
            }

            return Json(
                locationlist.Select(loc => new {
                    loc.LocationID,
                    Name = loc.FullName()
                }).OrderBy(loc => loc.Name), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetReportDepartmentsByLocationIDs(int[] locationIDs)
        {
            var pointuserinfo = PointUserInfoHelper.GetPointUserInfo(uow);
            var maxlevel = pointuserinfo.GetReportFunctionRoleLevelID();
            IEnumerable<Department> departmentlist = null;

            departmentlist = DepartmentBL.GetAllActiveByLocationIDs(uow, locationIDs.ToList());
            if (maxlevel < FunctionRoleLevelID.Location)
            {
                departmentlist = departmentlist.Where(dep => pointuserinfo.EmployeeDepartmentIDs.Contains(dep.DepartmentID));
            }

            return Json(
                departmentlist.Select(dep => new {
                    dep.DepartmentID,
                    Name = dep.FullName()
                }).OrderBy(loc => loc.Name), JsonRequestBehavior.AllowGet);
        }
    }
}