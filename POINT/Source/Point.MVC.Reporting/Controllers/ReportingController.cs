﻿using Microsoft.Reporting.WebForms;
using Point.Business.Helper;
using Point.Business.Logic;
using Point.Database.Context;
using Point.Database.Models.ReportModels;
using Point.Database.Repository;
using Point.Infrastructure;
using Point.Infrastructure.Extensions;
using Point.Models.Enums;
using Point.Models.ViewModels;
using Point.MVC.Reporting.DataSources;
using Point.MVC.Reporting.Helpers;
using Point.MVC.Reporting.Logic;
using Point.MVC.Reporting.Models.ViewModels;
using Point.ServiceBus.Models;
using System;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Serialization;

namespace Point.MVC.Reporting.Controllers
{
    [Authorize]
    [OutputCache(NoStore = true, Duration = 0)]
    public class ReportingController : WebApplication.Controllers.PointBaseController
    {
        DateTime defaultStartDate = new DateTime(DateTime.Now.Year, 1, 1).AddYears(-1);
        DateTime defaultEndDate = new DateTime(DateTime.Now.Year, 1, 1).AddYears(1).AddMinutes(-1);

        public ActionResult Index(int? reportDefinitionID = null)
        {
            var unitofwork = new UnitOfWork<PointContext>();
            var pointuserinfo = PointUserInfoHelper.GetPointUserInfo(unitofwork);

            ReportingViewModel viewmodel = null; 

            var view = "Index";

            if(reportDefinitionID.HasValue)
            {
                var reportdefinition = ReportDefinitionBL.GetByReportDefinitionID(uow, reportDefinitionID.Value);

                viewmodel = FilterViewModelBL.GetNew(defaultStartDate, defaultEndDate, pointuserinfo, reportdefinition.ReportName);

                view = "IndexDirect";
                viewmodel.ReportDefinitionID = reportDefinitionID.Value;
                viewmodel.ReportActionName = reportdefinition?.ReportActionName;
                viewmodel.Name = reportdefinition?.ReportName;
            } else
            {
                viewmodel = FilterViewModelBL.GetNew(defaultStartDate, defaultEndDate, pointuserinfo, "");
            }

            return View(view, viewmodel);
        }

        public ActionResult XMLDump(int? year = null)
        {
            var unitofwork = new UnitOfWork<PointContext>();
            var pointuserinfo = PointUserInfoHelper.GetPointUserInfo(unitofwork);

            FilterViewModel viewmodel = null;

            if(year.HasValue)
            {
                viewmodel = FilterViewModelBL.GetNew(new DateTime(year.Value, 1, 1), new DateTime(year.Value + 1, 1, 1).AddDays(-1), pointuserinfo, "Dump");
            } else
            {
                viewmodel = FilterViewModelBL.GetNew(defaultStartDate, defaultEndDate, pointuserinfo, "Dump");
            }

            var dumpreportmodellist = new DumpReportModelList()
            {
                Items = ReportModelBL.GetDumpReportData(viewmodel, pointuserinfo)
            };

            var builder = new StringBuilder();
            using (var xmlWriter = XmlWriter.Create(builder))
            {
                var serializer = new XmlSerializer(dumpreportmodellist.GetType());
                serializer.Serialize(xmlWriter, dumpreportmodellist);

                return Content(builder.ToString(), "text/xml");
            }
        }

        public ActionResult CSVDump(FilterViewModel viewmodel)
        {
            ServiceBusBL.SendMessage(uow, "CSVDump", "CSVDumpRequest", new CSVDumpRequest()
            {
                FlowDirection = FlowDirection.Any,
                UniqueID = DateTime.Now.Ticks,
                TimeStamp = DateTime.Now,
                EmployeeID = PointUserInfo().Employee.EmployeeID,
                Filter = viewmodel
            });

            return View("CSVAsync");
        }

        public ActionResult InStroomDumpReport(ReportingViewModel viewmodel)
        {
            return ReportView(
                "Report",
                ReportViewModelBL.GetByFilterViewModelAndDataSource(
                    viewmodel,
                    ReportDataSources.GetInStroomDumpReportData(viewmodel)
                ));
        }

        public ActionResult UitStroomDumpReport(ReportingViewModel viewmodel)
        {
            return ReportView(
                "Report",
                ReportViewModelBL.GetByFilterViewModelAndDataSource(
                    viewmodel,
                    ReportDataSources.GetUitStroomDumpReportData(viewmodel)
                ));
        }

        public ActionResult InstroomReportLocatie(ReportingViewModel viewmodel)
        {
            return ReportView(
                "Report",
                ReportViewModelBL.GetByFilterViewModelAndDataSource(
                    viewmodel,
                    ReportDataSources.GetInstroomReportData(viewmodel)
                ));
        }

        public ActionResult InstroomReportOverzicht(ReportingViewModel viewmodel)
        {
            return ReportView(
                "Report",
                ReportViewModelBL.GetByFilterViewModelAndDataSource(
                    viewmodel,
                    ReportDataSources.GetInstroomReportData(viewmodel)
                ));
        }

        public ActionResult InstroomReportIndicerend(ReportingViewModel viewmodel)
        {
            return ReportView(
                "Report",
                ReportViewModelBL.GetByFilterViewModelAndDataSource(
                    viewmodel,
                    ReportDataSources.GetInstroomReportData(viewmodel)
                ));
        }

        public ActionResult InStroomAantallenPerMaand(ReportingViewModel viewmodel)
        {
            return ReportView(
                "Report",
                ReportViewModelBL.GetByFilterViewModelAndDataSource(
                    viewmodel,
                    ReportDataSources.GetInstroomReportData(viewmodel)
                ));
        }

        public ActionResult InStroomTop10Report(ReportingViewModel viewmodel)
        {
            return ReportView(
                "Report",
                ReportViewModelBL.GetByFilterViewModelAndDataSource(
                    viewmodel,
                    ReportDataSources.GetInStroomTop10ReportData(viewmodel)
                ));
        }

        public ActionResult InStroomZorgType(ReportingViewModel viewmodel)
        {
            return ReportView(
                "Report",
                ReportViewModelBL.GetByFilterViewModelAndDataSource(
                    viewmodel,
                    ReportDataSources.GetInstroomReportData(viewmodel)
                ));
        }

        public ActionResult InStroomOntslagDatumGepasseerd(ReportingViewModel viewmodel)
        {
            return ReportView(
                "Report",
                ReportViewModelBL.GetByFilterViewModelAndDataSource(
                    viewmodel,
                    ReportDataSources.GetInStroomOntslagDatumGepasseerdReportData(viewmodel)
                ));
        }

        public ActionResult InStroomOntslagDatumBehaaldReport(ReportingViewModel viewmodel)
        {
            return ReportView(
                "Report",
                ReportViewModelBL.GetByFilterViewModelAndDataSource(
                    viewmodel,
                    ReportDataSources.GetInStroomOntslagDatumBehaaldReportData(viewmodel)
                ));
        }

        public ActionResult InStroomVKBReport(ReportingViewModel viewmodel)
        {
            return ReportView(
                "Report",
                ReportViewModelBL.GetByFilterViewModelAndDataSource(
                    viewmodel,
                    ReportDataSources.GetInStroomVKBReportData(viewmodel)
                ));
        }

        public ActionResult InStroomProcesTijdenReport(ReportingViewModel viewmodel)
        {
            return ReportView(
                "Report",
                ReportViewModelBL.GetByFilterViewModelAndDataSource(
                    viewmodel,
                    ReportDataSources.GetInStroomProcesTijdenReportData(viewmodel)
                ));
        }

        public ActionResult InStroomKPIInBehandelingReport(KPIFilterViewModel viewmodel)
        {
            return ReportView(
                "Report",
                ReportViewModelBL.GetByFilterViewModelAndDataSource(
                    viewmodel,
                    ReportDataSources.GetInStroomKPIInBehandelingReportData(viewmodel)
                ));
        }

        public ActionResult InStroomKPITerugmeldingReport(KPIFilterViewModel viewmodel)
        {
            return ReportView(
                "Report",
                ReportViewModelBL.GetByFilterViewModelAndDataSource(
                    viewmodel,
                    ReportDataSources.GetInStroomKPITerugmeldingReportData(viewmodel)
                ));
        }

        public ActionResult InStroomUitvalReport(ReportingViewModel viewmodel)
        {
            return ReportView(
                "Report",
                ReportViewModelBL.GetByFilterViewModelAndDataSource(
                    viewmodel,
                    ReportDataSources.GetInStroomUitvalReportData(viewmodel)
                ));
        }

        public ActionResult InStroomWaarIsDePatientGebleven(ReportingViewModel viewmodel)
        {
            return ReportView(
                "Report",
                ReportViewModelBL.GetByFilterViewModelAndDataSource(
                    viewmodel,
                    ReportDataSources.GetInStroomWaarIsDePatientGebleven(viewmodel)
                ));
        }

        public ActionResult InStroomTotalenReport(ReportingViewModel viewmodel)
        {
            return ReportView(
                "Report",
                ReportViewModelBL.GetByFilterViewModelAndDataSource(
                    viewmodel,
                    ReportDataSources.GetInStroomTotalenReportData(viewmodel)
                ));
        }

        public ActionResult UitstroomReportLocatie(ReportingViewModel viewmodel)
        {
            return ReportView(
                "Report",
                ReportViewModelBL.GetByFilterViewModelAndDataSource(
                    viewmodel,
                    ReportDataSources.GetUitstroomReportData(viewmodel)
                ));
        }

        public ActionResult UitstroomReportOverzicht(ReportingViewModel viewmodel)
        {
            return ReportView(
                "Report",
                ReportViewModelBL.GetByFilterViewModelAndDataSource(
                    viewmodel,
                    ReportDataSources.GetUitstroomReportData(viewmodel)
                ));
        }

        public ActionResult UitStroomAantallenPerMaand(ReportingViewModel viewmodel)
        {
            return ReportView(
                "Report",
                ReportViewModelBL.GetByFilterViewModelAndDataSource(
                    viewmodel,
                    ReportDataSources.GetUitstroomReportData(viewmodel)
                ));
        }

        public ActionResult UitStroomTop10Report(ReportingViewModel viewmodel)
        {
            return ReportView(
                "Report",
                ReportViewModelBL.GetByFilterViewModelAndDataSource(
                    viewmodel,
                    ReportDataSources.GetUitStroomTop10ReportData(viewmodel)
                ));
        }

        public ActionResult UitStroomZorgType(ReportingViewModel viewmodel)
        {
            return ReportView(
                "Report",
                ReportViewModelBL.GetByFilterViewModelAndDataSource(
                    viewmodel,
                    ReportDataSources.GetUitstroomReportData(viewmodel)
                ));
        }

        public ActionResult AfdelingReport(ReportingViewModel viewmodel)
        {
            return ReportView(
                "Report",
                ReportViewModelBL.GetByFilterViewModelAndDataSource(
                    viewmodel,
                    ReportDataSources.GetAfdelingReportData(viewmodel)
                ));
        }

        public ActionResult AfdelingCapacityReport(ReportingViewModel viewmodel)
        {
            return ReportView(
                "Report",
                ReportViewModelBL.GetByFilterViewModelAndDataSource(
                    viewmodel,
                    ReportDataSources.GetAfdelingCapacityReportData(viewmodel)
                ));
        }

        public ActionResult UitStroomOntslagDatumGepasseerd(ReportingViewModel viewmodel)
        {
            return ReportView(
                "Report",
                ReportViewModelBL.GetByFilterViewModelAndDataSource(
                    viewmodel,
                    ReportDataSources.GetUitstroomOntslagDatumGepasseerdReportData(viewmodel)
                ));
        }

        public ActionResult UitStroomOntslagDatumBehaaldReport(ReportingViewModel viewmodel)
        {
            return ReportView(
                "Report",
                ReportViewModelBL.GetByFilterViewModelAndDataSource(
                    viewmodel,
                    ReportDataSources.GetUitStroomOntslagDatumBehaaldReportData(viewmodel)
                ));
        }

        public ActionResult UitStroomVKBReport(ReportingViewModel viewmodel)
        {
            return ReportView(
                "Report",
                ReportViewModelBL.GetByFilterViewModelAndDataSource(
                    viewmodel,
                    ReportDataSources.GetUitStroomVKBReportData(viewmodel)
                ));
        }

        public ActionResult UitStroomProcesTijdenReport(ReportingViewModel viewmodel)
        {
            return ReportView(
                "Report",
                ReportViewModelBL.GetByFilterViewModelAndDataSource(
                    viewmodel,
                    ReportDataSources.GetUitStroomProcesTijdenReportData(viewmodel)
                ));
        }

        public ActionResult UitStroomKPIInBehandelingReport(KPIFilterViewModel viewmodel)
        {
            return ReportView(
                "Report",
                ReportViewModelBL.GetByFilterViewModelAndDataSource(
                    viewmodel,
                    ReportDataSources.GetUitStroomKPIInBehandelingReportData(viewmodel)
                ));
        }

        public ActionResult UitStroomKPITerugmeldingReport(KPIFilterViewModel viewmodel)
        {
            return ReportView(
                "Report",
                ReportViewModelBL.GetByFilterViewModelAndDataSource(
                    viewmodel,
                    ReportDataSources.GetUitStroomKPITerugmeldingReportData(viewmodel)
                ));
        }

        public PartialViewResult GetFilter(int reportdefinitionid)
        {
            var unitofwork = new UnitOfWork<PointContext>();
            var pointuserinfo = PointUserInfoHelper.GetPointUserInfo(unitofwork);
            var reportdefinition = ReportDefinitionBL.GetByReportDefinitionID(unitofwork, reportdefinitionid);

            ReportingViewModel model = null;

            string partialname = reportdefinition.FilterPartial;

            if(reportdefinition.FilterPartial == "_KPIFilter")
            {
                model = FilterViewModelBL.GetNewKPI(defaultStartDate, defaultEndDate, pointuserinfo, reportdefinition.ReportName);
            }

            if (reportdefinition.FilterPartial == "_KPIFilterTerugMelding")
            {
                var tempmodel = FilterViewModelBL.GetNewKPI(defaultStartDate, defaultEndDate, pointuserinfo, reportdefinition.ReportName);
                tempmodel.NormTijdExtramuraal = "04:00";
                tempmodel.NormTijdHospice = "08:00";
                tempmodel.NormTijdIntramuraal = "08:00";
                tempmodel.NormExtramuraal = 75;
                tempmodel.NormHospice = 70;
                tempmodel.NormIntramuraal = 70;

                model = tempmodel;

                partialname = "_KPIFilter";
            }

            if (reportdefinition.FilterPartial == "_CompareFilter")
            {
                defaultStartDate = DateTime.Now.AddMonths(-3);
                defaultEndDate = DateTime.Now;
                var tempmodel = FilterViewModelBL.GetNewCompareFilter(defaultStartDate, defaultEndDate, pointuserinfo, reportdefinition.ReportName);

                model = tempmodel;
            }

            if (model == null)
            {
                model = FilterViewModelBL.GetNew(defaultStartDate, defaultEndDate, pointuserinfo, reportdefinition?.ReportName);
            }

            model.NeedsOrganizationID = reportdefinition.NeedsOrganizationID;
            if (model.NeedsOrganizationID && model.AvailableOrganizations.Count() == 1)
            {
                
                model.OrganizationIDs = new int[] { Convert.ToInt32(model.AvailableOrganizations.First().Value) };
            }

            return PartialView(partialname, model);
        }

        public ActionResult UitStroomStuurInfoZH(ReportingViewModel viewmodel)
        {
            return ReportView(
                "Report",
                ReportViewModelBL.GetByFilterViewModelAndDataSource(
                    viewmodel,
                    ReportDataSources.GetUitStroomStuurInfoZHData(viewmodel)
                ));
        }

        public ActionResult UitStroomDICA(ReportingViewModel viewmodel)
        {
            return ReportView(
                "Report",
                ReportViewModelBL.GetByFilterViewModelAndDataSource(
                    viewmodel,
                    ReportDataSources.GetDicaReportData(viewmodel)
                ));
        }

        private ActionResult ReportView(string viewname, ReportingOutputViewModel reportviewmodel)
        {
            //Dont cache in browser history
            Response.DisableClientCache();

            FilterCacheHelper.SaveFilter(reportviewmodel.Filter);

            if (reportviewmodel.UseExcel)
            {
                ReportViewer viewer = new ReportViewer
                {
                    ProcessingMode = ProcessingMode.Local
                };

                viewer.LocalReport.ReportPath = reportviewmodel.ReportFileName;
                viewer.LocalReport.DataSources.Add(reportviewmodel.ReportDataSource);

                var parameters = viewer.LocalReport.GetParameters();
                foreach (var reportparameter in reportviewmodel.ReportParameters)
                {
                    if (parameters.Any(rp => rp.Name == reportparameter.Name))
                    {
                        viewer.LocalReport.SetParameters(reportparameter);
                    }
                }

                string mimeType = string.Empty;
                string encoding = string.Empty;
                string extension = string.Empty;

                byte[] bytes = viewer.LocalReport.Render("EXCELOPENXML", null, out mimeType, out encoding, out extension, out string[] streamIds, out Warning[] warnings);

                var fileDownloadName = $"{reportviewmodel.ReportName}-{DateTime.Now.ToString("yyyyMMddHHmm")}.{extension}";
                return File(bytes, mimeType, fileDownloadName);
            } 

            return View("Report", reportviewmodel);
        }

        
        public ActionResult TransferMonitor(CompareFilterViewModel viewmodel)
        {
            return ReportView(
                "Report",
                ReportViewModelBL.GetByFilterViewModelAndDataSource(
                    viewmodel,
                    ReportDataSources.GetTransferMonitorReportData(viewmodel)
                ));
        }
    }
}