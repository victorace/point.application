﻿using Point.Business.Logic;
using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.MVC.Reporting.Expressions;
using System;
using System.Linq;
using System.Security;
using Point.Models.ViewModels;

namespace Point.MVC.Reporting.Extensions
{
    public static class LINQExtensions
    {
        //Voor afdeling rapport...
        public static IQueryable<Department> ApplyFilterViewModel(this IQueryable<Department> query, FilterViewModel filterViewModel, PointUserInfo pointUserInfo)
        {
            if (FlowInstanceReportValuesBL.VerifyFilter(filterViewModel, pointUserInfo) == false)
            {
                throw new SecurityException("U heeft geen bevoegdheid om dit rapport te zien.");
            }

            //Geen inactives
            query = query.Where(d => d.Inactive != true && d.Location.Inactive != true && d.Location.Organization.Inactive != true);

            //Rapprtage niveau
            if (filterViewModel.DepartmentIDs.Any())
            {
                query = query.Where(d => filterViewModel.DepartmentIDs.Contains(d.DepartmentID));
            }
            else if (filterViewModel.LocationIDs.Any())
            {
                query = query.Where(d => filterViewModel.LocationIDs.Contains(d.LocationID));
            }
            else if (filterViewModel.OrganizationIDs.Any())
            {
                query = query.Where(d => filterViewModel.OrganizationIDs.Contains(d.Location.OrganizationID));
            }
            else if (filterViewModel.RegionID.HasValue)
            {
                query = query.Where(d =>
                    filterViewModel.OrganizationTypeIDs.Any() ?
                        (filterViewModel.RegionID == d.Location.Organization.RegionID && filterViewModel.OrganizationTypeIDs.Contains(d.Location.Organization.OrganizationTypeID)) :
                        filterViewModel.RegionID == d.Location.Organization.RegionID);
            }
            //Als alleen OrganizationTypeID is opgegeven
            else if (filterViewModel.OrganizationTypeIDs.Any())
            {
                query = query.Where(d => filterViewModel.OrganizationTypeIDs.Contains(d.Location.Organization.OrganizationTypeID));
            }
            else
            {
                //Als niks is opgegeven dan filter wat je maximaal mag zien
                var maxlevel = pointUserInfo.GetReportFunctionRoleLevelID();
                if (maxlevel == FunctionRoleLevelID.Region)
                {
                    query = query.Where(d => pointUserInfo.Region.RegionID == d.Location.Organization.RegionID);
                }
                else if (maxlevel == FunctionRoleLevelID.Organization)
                {
                    query = query.Where(d => pointUserInfo.EmployeeOrganizationIDs.Contains(d.Location.OrganizationID));
                }
                else if (maxlevel == FunctionRoleLevelID.Location)
                {
                    query = query.Where(d => pointUserInfo.EmployeeLocationIDs.Contains(d.LocationID));
                }
                else if (maxlevel == FunctionRoleLevelID.Department)
                {
                    query = query.Where(d => pointUserInfo.EmployeeDepartmentIDs.Contains(d.DepartmentID));
                }
            }
            
            return query;
        }

        public static IQueryable<DepartmentCapacity> ApplyFilterViewModel(this IQueryable<DepartmentCapacity> query, FilterViewModel filterViewModel, PointUserInfo pointUserInfo)
        {
            if (FlowInstanceReportValuesBL.VerifyFilter(filterViewModel, pointUserInfo) == false)
            {
                throw new SecurityException("U heeft geen bevoegdheid om dit rapport te zien.");
            }

            //Geen inactives
            query = query.Where(d => d.Department.Inactive != true && d.Department.Location.Inactive != true && d.Department.Location.Organization.Inactive != true);

            //Datum tijd fix (t/m verhaal....)
            var startdatetime = new DateTime(filterViewModel.StartDate.Year, filterViewModel.StartDate.Month, filterViewModel.StartDate.Day);
            var enddatetime = new DateTime(filterViewModel.EndDate.Year, filterViewModel.EndDate.Month, filterViewModel.EndDate.Day).AddDays(1).AddSeconds(-1);
            query = query.Where(DepartmentExpressions.ByDateRange(startdatetime, enddatetime));

            //Rapprtage niveau
            if (filterViewModel.DepartmentIDs.Any())
            {
                query = query.Where(d => filterViewModel.DepartmentIDs.Contains(d.Department.DepartmentID));
            }
            else if (filterViewModel.LocationIDs.Any())
            {
                query = query.Where(d => filterViewModel.LocationIDs.Contains(d.Department.LocationID));
            }
            else if (filterViewModel.OrganizationIDs.Any())
            {
                query = query.Where(d => filterViewModel.OrganizationIDs.Contains(d.Department.Location.OrganizationID));
            }
            else if (filterViewModel.RegionID.HasValue)
            {
                query = query.Where(d =>
                    filterViewModel.OrganizationTypeIDs.Any() ?
                        (filterViewModel.RegionID == d.Department.Location.Organization.RegionID && filterViewModel.OrganizationTypeIDs.Contains(d.Department.Location.Organization.OrganizationTypeID)) :
                        filterViewModel.RegionID == d.Department.Location.Organization.RegionID);
            }
            //Als alleen OrganizationTypeID is opgegeven
            else if (filterViewModel.OrganizationTypeIDs.Any())
            {
                query = query.Where(d => filterViewModel.OrganizationTypeIDs.Contains(d.Department.Location.Organization.OrganizationTypeID));
            }
            else
            {
                //Als niks is opgegeven dan filter wat je maximaal mag zien
                var maxlevel = pointUserInfo.GetReportFunctionRoleLevelID();
                if (maxlevel == FunctionRoleLevelID.Region)
                {
                    query = query.Where(d => pointUserInfo.Region.RegionID == d.Department.Location.Organization.RegionID);
                }
                else if (maxlevel == FunctionRoleLevelID.Organization)
                {
                    query = query.Where(d => pointUserInfo.EmployeeOrganizationIDs.Contains(d.Department.Location.OrganizationID));
                }
                else if (maxlevel == FunctionRoleLevelID.Location)
                {
                    query = query.Where(d => pointUserInfo.EmployeeLocationIDs.Contains(d.Department.LocationID));
                }
                else if (maxlevel == FunctionRoleLevelID.Department)
                {
                    query = query.Where(d => pointUserInfo.EmployeeDepartmentIDs.Contains(d.Department.DepartmentID));
                }
            }

            return query;
        }
    }
}