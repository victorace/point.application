﻿using Point.Database.Context;
using Point.Infrastructure;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System;
using Point.Infrastructure.Constants;

namespace Point.MVC.Reporting
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            System.Data.Entity.Database.SetInitializer<PointContext>(null);
            System.Data.Entity.Database.SetInitializer<PointSearchContext>(null);

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            Utils.ClearHeaders(Response);
        }

        protected void Session_Start(Object sender, EventArgs e)
        {
            //Dummy entry to fix session is always new see https://forums.asp.net/t/1520827.aspx
            Session[SessionIdentifiers.Point.SessionID] = Session.SessionID;
        }
    }
}
