﻿using Microsoft.Reporting.WebForms;
using Point.Models.Enums;
using Point.Database.Models.ReportModels;
using Point.Models.ViewModels;
using Point.MVC.Reporting.Logic;
using Point.MVC.Reporting.Models.ViewModels;
using System.Collections.Generic;

namespace Point.MVC.Reporting.DataSources
{
    public class ReportDataSources
    {
        //Deze zijn nodig zodat de reporteditor kan zien welk type DataSources beschikbaar zijn
        public List<DumpReportModel> DumpReportData() { return null; }
        public List<StroomReportModel> StroomReportData() { return null; }
        public List<StroomTrendModel> StroomTrendData() { return null; }
        public List<Top10ReportModel> Top10ReportData() { return null; }
        public List<DischargeDatePassedReportModel> DischargeDatePassedData() { return null; }
        public List<ControlInfoReportModel> ControlInfoData() { return null; }
        public List<DICAReportModel> DicaReportData() { return null; }
        public List<UitvalReportModel> UitvalReportData() { return null; }
        public List<AfdelingReportModel> AfdelingReportData() { return null; }
        public List<AfdelingCapacityReportModel> AfdelingCapacityReportData() { return null; }

        public static ReportDataSource GetInStroomDumpReportData(FilterViewModel filterviewmodel)
        {
            return new ReportDataSource("DumpReportData", ReportModelBL.GetDumpReportData(filterviewmodel, FlowDirection.Receiving));
        }

        public static ReportDataSource GetUitStroomDumpReportData(FilterViewModel filterviewmodel)
        {
            return new ReportDataSource("DumpReportData", ReportModelBL.GetDumpReportData(filterviewmodel, FlowDirection.Sending));
        }

        public static ReportDataSource GetUitstroomReportData(FilterViewModel filterviewmodel)
        {
            return new ReportDataSource("StroomReportData", ReportModelBL.GetUitstroomReportData(filterviewmodel));
        }

        public static ReportDataSource GetInstroomReportData(FilterViewModel filterviewmodel)
        {
            return new ReportDataSource("StroomReportData", ReportModelBL.GetInstroomReportData(filterviewmodel));
        }

        public static ReportDataSource GetInStroomKPIInBehandelingReportData(KPIFilterViewModel filterviewmodel)
        {
            return new ReportDataSource("StroomTrendData", ReportModelBL.GetInStroomKPIInBehandelingReportData(filterviewmodel));
        }

        public static ReportDataSource GetInStroomKPITerugmeldingReportData(KPIFilterViewModel filterviewmodel)
        {
            return new ReportDataSource("StroomTrendData", ReportModelBL.GetInStroomKPITerugmeldingReportData(filterviewmodel));
        }
        
        public static ReportDataSource GetInStroomUitvalReportData(FilterViewModel filterviewmodel)
        {
            return new ReportDataSource("UitvalReportData", ReportModelBL.GetInStroomUitvalReportData(filterviewmodel));
        }

        public static ReportDataSource GetInStroomWaarIsDePatientGebleven(FilterViewModel filterviewmodel)
        {
            return new ReportDataSource("UitvalReportData", ReportModelBL.GetInStroomWaarIsDePatientGebleven(filterviewmodel));
        }
        
        public static ReportDataSource GetInStroomTotalenReportData(FilterViewModel filterviewmodel)
        {
            return new ReportDataSource("UitvalReportData", ReportModelBL.GetInStroomTotalenReportData(filterviewmodel));
        }
        
        public static ReportDataSource GetAfdelingReportData(FilterViewModel filterviewmodel)
        {
            return new ReportDataSource("AfdelingReportData", ReportModelBL.GetAfdelingReportData(filterviewmodel));
        }

        public static ReportDataSource GetAfdelingCapacityReportData(FilterViewModel filterviewmodel)
        {
            return new ReportDataSource("AfdelingCapacityReportData", ReportModelBL.GetAfdelingCapacityReportData(filterviewmodel));
        }

        public static ReportDataSource GetUitStroomTrendReportData(FilterViewModel filterviewmodel)
        {
            return new ReportDataSource("StroomReportData", ReportModelBL.GetUitStroomTrendReportData(filterviewmodel));
        }

        public static ReportDataSource GetUitStroomOntslagDatumBehaaldReportData(FilterViewModel filterviewmodel)
        {
            return new ReportDataSource("StroomTrendData", ReportModelBL.GetUitStroomOntslagDatumBehaaldReportData(filterviewmodel));
        }

        public static ReportDataSource GetInStroomOntslagDatumBehaaldReportData(FilterViewModel filterviewmodel)
        {
            return new ReportDataSource("StroomTrendData", ReportModelBL.GetInStroomOntslagDatumBehaaldReportData(filterviewmodel));
        }

        public static ReportDataSource GetUitStroomVKBReportData(FilterViewModel filterviewmodel)
        {
            return new ReportDataSource("StroomTrendData", ReportModelBL.GetUitStroomVKBReportData(filterviewmodel));
        }

        public static ReportDataSource GetInStroomVKBReportData(FilterViewModel filterviewmodel)
        {
            return new ReportDataSource("StroomTrendData", ReportModelBL.GetInStroomVKBReportData(filterviewmodel));
        }

        public static ReportDataSource GetUitStroomProcesTijdenReportData(FilterViewModel filterviewmodel)
        {
            return new ReportDataSource("StroomReportData", ReportModelBL.GetUitStroomProcesTijdenReportData(filterviewmodel));
        }

        public static ReportDataSource GetUitStroomKPIInBehandelingReportData(KPIFilterViewModel filterviewmodel)
        {
            return new ReportDataSource("StroomTrendData", ReportModelBL.GetUitStroomKPIInBehandelingReportData(filterviewmodel));
        }

        public static ReportDataSource GetUitStroomKPITerugmeldingReportData(KPIFilterViewModel filterviewmodel)
        {
            return new ReportDataSource("StroomTrendData", ReportModelBL.GetUitStroomKPITerugmeldingReportData(filterviewmodel));
        }

        public static ReportDataSource GetUitStroomStuurInfoZHData(FilterViewModel filterviewmodel)
        {
            return new ReportDataSource("StuurInfoData", ReportModelBL.GetUitStroomStuurInfoZHData(filterviewmodel));
        }

        public static ReportDataSource GetDicaReportData(FilterViewModel filterviewmodel)
        {
            return new ReportDataSource("DicaReportData", ReportModelBL.GetDicaReportData(filterviewmodel));
        }

        public static ReportDataSource GetTransferMonitorReportData(CompareFilterViewModel filterviewmodel)
        {
            return new ReportDataSource("StroomTrendData", ReportModelBL.GetTransferMonitorReportData(filterviewmodel));
        }

        public static ReportDataSource GetInStroomProcesTijdenReportData(FilterViewModel filterviewmodel)
        {
            return new ReportDataSource("StroomReportData", ReportModelBL.GetInStroomProcesTijdenReportData(filterviewmodel));
        }

        public static ReportDataSource GetUitStroomTop10ReportData(FilterViewModel filterviewmodel)
        {
            return new ReportDataSource("StroomReportData", ReportModelBL.GetUitStroomTop10ReportData(filterviewmodel));
        }

        public static ReportDataSource GetUitstroomOntslagDatumGepasseerdReportData(FilterViewModel filterviewmodel)
        {
            return new ReportDataSource("StroomReportData", ReportModelBL.GetUitStroomOntslagDatumGepasseerdReportData(filterviewmodel));
        }

        public static ReportDataSource GetInStroomOntslagDatumGepasseerdReportData(FilterViewModel filterviewmodel)
        {
            return new ReportDataSource("StroomReportData", ReportModelBL.GetInStroomOntslagDatumGepasseerdReportData(filterviewmodel));
        }

        public static ReportDataSource GetInStroomTop10ReportData(FilterViewModel filterviewmodel)
        {
            return new ReportDataSource("StroomReportData", ReportModelBL.GetInStroomTop10ReportData(filterviewmodel));
        }

        public static ReportDataSource GetInStroomTrendReportData(FilterViewModel filterviewmodel)
        {
            return new ReportDataSource("StroomReportData", ReportModelBL.GetInStroomTrendReportData(filterviewmodel));
        }
    }
}