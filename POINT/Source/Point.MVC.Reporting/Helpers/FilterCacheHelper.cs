﻿using Point.Infrastructure.Extensions;
using Point.Models.ViewModels;
using Point.MVC.Reporting.Models.ViewModels;
using System.Web;

namespace Point.MVC.Reporting.Helpers
{
    public class FilterCacheHelper
    {
        public const string FILTERSESSIONNAME = "ReportFilter";
        public static void SaveFilter(FilterViewModel filter)
        {
            var cachedfilter = new FilterCacheModel();
            TypeExtensions.Merge(cachedfilter, filter);

            HttpContext.Current.Session[FILTERSESSIONNAME] = cachedfilter;
        }

        public static FilterCacheModel GetFilter()
        {
            return HttpContext.Current.Session[FILTERSESSIONNAME] as FilterCacheModel;
        }
    }
}