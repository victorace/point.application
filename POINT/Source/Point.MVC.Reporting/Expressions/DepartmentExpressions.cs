﻿using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Models.ReportModels;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace Point.MVC.Reporting.Expressions
{
    public class DepartmentExpressions
    {
        public static Expression<Func<Department, AfdelingReportModel>> AfdelingReportFromModel => d => new AfdelingReportModel()
        {
            Organisatie = d.Location.Organization.Name,
            Locatie = d.Location.Name,
            Afdeling = d.Name,
            AfdelingBedieningsgebiedList = d.ServiceAreaPostalCode.Where(sap => sap.PostalCode != null).Select(sap => sap.PostalCode.StartPostalCode + " " + sap.PostalCode.EndPostalCode).ToList(),
            DigitaalVersturenCDAJaNee = d.RecieveCDATransferSend == true ? "Ja" : "Nee",
            DigitaalVersturenVOJaNee = d.RecieveZorgmailTransferSend == true ? "Ja" : "Nee",
            DigitaalVersturenVO = d.ZorgmailTransferSend,
            EmailadresTbvAanvraag = d.EmailAddress,
            EmailadresTbvAanvraagJaNee = d.RecieveEmail == true ? "Ja" : "Nee",
            EmailadresTbvVONotificatie = d.EmailAddressTransferSend,
            EmailadresTbvVONotificatieJaNee = d.RecieveEmailTransferSend == true ? "Ja" : "Nee",
            Faxnummer = d.FaxNumber,
            FlowsActiefList = d.Location.FlowDefinitionParticipation.Where(p =>
                (p.Participation == Participation.Active || p.Participation == Participation.ActiveButInvisibleInSearch) &&
                !d.Location.FlowDefinitionParticipation.Any(pp =>
                    pp.FlowDefinitionID == p.FlowDefinitionID &&
                    pp.DepartmentID == d.DepartmentID &&
                    pp.Participation == Participation.None)
                ).Select(p => p.FlowDefinitionID).ToList(),

            FlowsFaxMailList = d.Location.FlowDefinitionParticipation.Where(p =>
                p.Participation == Participation.FaxMail &&
                !d.Location.FlowDefinitionParticipation.Any(pp =>
                    pp.FlowDefinitionID == p.FlowDefinitionID &&
                    pp.DepartmentID == d.DepartmentID &&
                    pp.Participation == Participation.None)
                ).Select(p => p.FlowDefinitionID).ToList(),

            IntensieveRevalidatieVerpleeghuisJaNee = d.IntensiveRehabilitationNursinghome == true ? "Ja" : "Nee",
            Plaatsnaam = d.Location.City,
            Postcode = d.Location.PostalCode,
            RevalidatieCentrumJaNee = d.RehabilitationCenter == true ? "Ja" : "Nee",
            Sortering = d.SortOrder == null ? "" : d.SortOrder.ToString(),
            Specialisme = d.AfterCareType1.ReportCode,
            Telefoonnummer = d.PhoneNumber,
            TonenOpCapaciteitSchermJaNee = d.CapacityFunctionality == true ? "Ja" : "Nee"
        };

        public struct DateStruct
        {
            public int Year;
            public int Month;
            public int Day;
        }

        public static Expression<Func<DepartmentCapacity, AfdelingCapacityReportModel>> AfdelingCapacityReportFromModel => d => new AfdelingCapacityReportModel()
        {
            DepartmentID = d.DepartmentID ?? 0,
            Organisatie = d.Department.Location.Organization.Name,
            Locatie = d.Department.Location.Name,
            Afdeling = d.Department.Name,
            Specialisme = d.Department.AfterCareType1.ReportCode,
            SpecialismeCategorie = d.Department.AfterCareType1.AfterCareCategory.Abbreviation,
            Capacity = d.CapacityDepartment ?? 0,
            CapacityDateTime = d.CapacityDepartmentDate.Value
        };

        public static Expression<Func<DepartmentCapacity, bool>> ByDateRange(DateTime startDate, DateTime endDate) =>
         fi => fi.CapacityDepartmentDate > startDate && fi.CapacityDepartmentDate < endDate;
    }
}
