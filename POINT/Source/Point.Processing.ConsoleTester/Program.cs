﻿using System;

namespace Point.Processing.ConsoleTester
{
    class Program
    {
        static void Main(string[] args)
        {
            var phasequeprocessor = new PointQueueProcessorList() { FromConsole = true };
            phasequeprocessor.Start();
            phasequeprocessor.AddLog($"ConsoleTester started @{DateTime.Now.ToString("HH:mm:ss")}", ConsoleColor.White);
            
            var pointScheduler = new PointScheduler();
            pointScheduler.Start();

            Console.ReadLine();
        }
    }
}
