﻿using System;
using Point.Database.Models;
using Point.Models.Enums;

namespace Point.ServiceBus.Models
{
    public class SendTransferAttachment : ILoggingServiceBusModel, ICommunicationLog, IDossierServiceBusModel
    {
        public int TransferID { get; set; }
        public int TransferAttachmentID { get; set; }
        public int RecievingOrganizationID { get; set; }
        public int? RecievingDepartmentID { get; set; }
        public int SendingEmployeeID { get; set; }
        public int? EmployeeID
        {
            get
            {
                return SendingEmployeeID;
            }
            set
            {
                if (!value.HasValue)
                {
                    throw new Exception("No Null values accepted for EmployeeID in SendTransferAttachment");
                }
                SendingEmployeeID = value.Value;
            }
        }
        public CommunicationLogType MailType { get; set; }
        public long UniqueID { get; set; }
        public int? OrganizationID { get; set; }
        public DateTime TimeStamp { get; set; }
    }
}

