﻿using System;
using Point.Models.Enums;

namespace Point.ServiceBus.Models
{
    public abstract class SendExternalSupplierOrder : ILoggingServiceBusModel, IDossierServiceBusModel
    {
        public int TransferID { get; set; }
        public int FormSetVersionID { get; set; }

        public int SendingOrganizationID { get; set; }
        //The employee sending the message
        public int SendingEmployeeID { get; set; }
        //The screen generating it
        public int ScreenID { get; set; }
        //The type of template we're sending
        //public CommunicationLogType CommunicationLogType { get; set; }

        //Reserved for logging
        public long UniqueID { get; set; }
        //Reserved for logging
        public DateTime TimeStamp { get; set; }

        public abstract string SupplierName { get; }
        public abstract int SupplierID { get; }
        public abstract CommunicationLogType CommunicationLogType { get; }
        public abstract bool UseBasicAuthentication { get; }

        public string RequestUrlConfigSettingName => $"{SupplierName}URL";
        public string OrganizationSettingSupplierCode => $"{SupplierName}Code";

        public string UserName => $"{SupplierName}Username";
        public string Password => $"{SupplierName}Password";
        public bool HasLoginInfo => !string.IsNullOrEmpty(RequestUrlConfigSettingName) && (!UseBasicAuthentication || (!string.IsNullOrEmpty(UserName) && !string.IsNullOrEmpty(Password)));
    }

    public class SendVegroOrder : SendExternalSupplierOrder
    {
        public override string SupplierName => "Vegro";
        public override int SupplierID => (int)Point.Models.Enums.SupplierID.Vegro;
        public override CommunicationLogType CommunicationLogType => CommunicationLogType.VegroOrder;
        public override bool UseBasicAuthentication => false;
    }

    public class SendMediPointOrder : SendExternalSupplierOrder
    {
        public override string SupplierName => "MediPoint";
        public override int SupplierID => (int)Point.Models.Enums.SupplierID.MediPoint;
        public override CommunicationLogType CommunicationLogType => CommunicationLogType.MediPointOrder;
        public override bool UseBasicAuthentication => true;
    }
}
