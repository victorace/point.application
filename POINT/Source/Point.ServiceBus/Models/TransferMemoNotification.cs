﻿namespace Point.ServiceBus.Models
{
    public class TransferMemoNotification : IPointServiceBusModel
    {
        public int TransferID { get; set; }
        public string FormName { get; set; }
        public int TransferMemoID { get; set; }
        public int SendingEmployeeID { get; set; }
        public string Url { get; set; }
    }
}
