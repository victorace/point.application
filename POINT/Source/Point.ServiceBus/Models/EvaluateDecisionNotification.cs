﻿using Point.Database.Models;
using Point.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Point.ServiceBus.Models
{
    public class EvaluateDecisionNotification : ILoggingServiceBusModel, IDossierServiceBusModel
    {
        public long UniqueID { get; set; }
        public int TransferID { get; set; }
        public DateTime TimeStamp { get; set; }
        public int OrganizationInviteID { get; set; }
        public int EmployeeID { get; set; }
        public string InviteUrl { get; set; }
    }
}
