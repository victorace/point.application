﻿using Point.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Point.ServiceBus.Models
{
    public class SignaleringMessage : IPointServiceBusModel
    {
        public int TransferID { get; set; }
        public int? FormTypeID { get; set; }
        public int? GeneralActionID { get; set; }
        public int EmployeeID { get; set; }
        public string Message { get; set; }
        public SignaleringType SignaleringType { get; set; }
    }
}
