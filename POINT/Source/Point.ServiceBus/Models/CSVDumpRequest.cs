﻿using Point.Models.Enums;
using Point.Models.ViewModels;
using System;

namespace Point.ServiceBus.Models
{
    public class CSVDumpRequest : ILoggingServiceBusModel
    {
        public long UniqueID { get; set; }
        public DateTime TimeStamp { get; set; }
        public int EmployeeID { get; set; }
        public FlowDirection FlowDirection { get; set; }
        public FilterViewModel Filter { get; set; }
    }
}
