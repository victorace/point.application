﻿using System;
using Point.Database.Models;
using Point.Models.Enums;

namespace Point.ServiceBus.Models
{
    public class CancelNotification : ILoggingServiceBusModel, ICommunicationLog
    {          
        public int OrganizationInviteID { get; set; }
        public int SendingEmployeeID { get; set; }

        public int? EmployeeID
        {
            get
            {
                return SendingEmployeeID;
            }
            set
            {
                if (!value.HasValue)
                    throw new Exception("No Null value allowed");
                SendingEmployeeID = value.Value;
            }
        }

        public int? OrganizationID { get; set; }

        public CommunicationLogType MailType { get; set; }

        public long UniqueID { get; set; }

        public DateTime TimeStamp { get; set; }
    }
}

