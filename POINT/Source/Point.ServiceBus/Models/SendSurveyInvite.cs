﻿using System;

namespace Point.ServiceBus.Models
{
    public class SendSurveyInvite : ILoggingServiceBusModel, IDossierServiceBusModel
    {
        public int TransferID { get; set; }
        public long UniqueID { get; set; }
        public DateTime TimeStamp { get; set; }
    }
}