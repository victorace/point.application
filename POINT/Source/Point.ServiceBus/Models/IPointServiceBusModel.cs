﻿using System;

namespace Point.ServiceBus.Models
{
    public interface ILoggingServiceBusModel : IPointServiceBusModel
    {
        long UniqueID { get; set; }
        DateTime TimeStamp { get; set; }
    }

    public interface IPointServiceBusModel
    {

    }

    public interface IDossierServiceBusModel
    {
        int TransferID { get; set; }
    }

    public interface IMailServiceBusModel
    {
        string Url { get; set; }
        bool RedirectToAuth { get; set; }
        int? AuthOrganizationID { get; set; }
    }
}
