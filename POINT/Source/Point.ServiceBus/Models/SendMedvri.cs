﻿using Point.Models.Enums;
using System;

namespace Point.ServiceBus.Models
{
    public class SendMedvri : ILoggingServiceBusModel, IDossierServiceBusModel
    {
        public int TransferID { get; set; }
        public int ClientID { get; set; }

        public int SendingOrganizationID { get; set; }
        public int SendingDepartmentID { get; set; }
        //The employee sending the message
        public int SendingEmployeeID { get; set; }
        //The screen generating it
        public int ScreenID { get; set; }
        //The type of template we're sending
        public TemplateTypeID TemplateTypeID { get; set; }

        //Reserved for logging
        public long UniqueID { get; set; }
        //Reserved for logging
        public DateTime TimeStamp { get; set; }
    }
}