﻿using Point.Database.Models;
using Point.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Point.ServiceBus.Models
{
    public class InviteNotification : ILoggingServiceBusModel, IDossierServiceBusModel, IMailServiceBusModel
    {
        public long UniqueID { get; set; }
        public int TransferID { get; set; }
        public DateTime TimeStamp { get; set; }
        public int OrganizationInviteID { get; set; }
        public int EmployeeID { get; set; }
        public string Url { get; set; }
        public bool RedirectToAuth { get; set; }
        public int? AuthOrganizationID { get; set; }
    }
}
