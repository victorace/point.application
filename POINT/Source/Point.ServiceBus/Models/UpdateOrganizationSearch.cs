﻿namespace Point.ServiceBus.Models
{
    public class UpdateOrganizationSearch : IPointServiceBusModel
    {
        public int? OrganizationID { get; set; }
        public int? LocationID { get; set; }
        public int? DepartmentID { get; set; }
    }
}
