﻿namespace Point.ServiceBus.Models
{
    public class UpdateReadModel : IPointServiceBusModel
    {
        public int FlowInstanceID { get; set; }
    }

    public class UpdateReadModelBatch : IPointServiceBusModel
    {
        public int[] FlowInstanceIDs { get; set; }
    }
}
