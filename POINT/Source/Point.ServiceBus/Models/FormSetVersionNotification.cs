﻿using System;
using Point.Database.Models;
using Point.Models.Enums;

namespace Point.ServiceBus.Models
{
    public class FormSetVersionNotification : ILoggingServiceBusModel, ICommunicationLog, IDossierServiceBusModel
    {
        public int TransferID { get; set; }
        public int FormSetVersionID { get; set; }
        public int RecievingOrganizationID { get; set; }
        public int? RecievingDepartmentID { get; set; }
        public bool IsAltered { get; set; }
        public int SendingEmployeeID { get; set; }
        public string Url { get; set; }

        public int? EmployeeID
        {
            get
            {
                return SendingEmployeeID;
            }
            set
            {
                if (!value.HasValue)
                {
                    throw new Exception("No Null values accepted for EmployeeID in FormSetVersionNotification");
                }
                SendingEmployeeID = value.Value;
            }
        }

        public int? OrganizationID { get; set; }

        public CommunicationLogType MailType { get; set; }

        public long UniqueID { get; set; }
        public DateTime TimeStamp { get; set; }

        public bool IsVO { get; set; }
    }
}

