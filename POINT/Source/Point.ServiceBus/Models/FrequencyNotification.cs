﻿using System;
using Point.Database.Models;
using Point.Models.Enums;

namespace Point.ServiceBus.Models
{
    public class FrequencyNotification : ILoggingServiceBusModel, IScheduleHistory, IDossierServiceBusModel, IMailServiceBusModel
    {
        public int TransferID { get; set; }
        public int SendingEmployeeID { get; set; }
        public string Url { get; set; }


        public int FrequencyID { get; set; }
        public DateTime Timestamp { get; set; }


        public int? EmployeeID
        {
            get
            { return SendingEmployeeID; }

            set
            {
                if (!value.HasValue)
                    throw new Exception("No Null values accepted for FrequencyNotification");
                SendingEmployeeID = value.Value;
            }
        }

        public int? OrganizationID { get; set; }

        public CommunicationLogType MailType { get; set; }

        public long UniqueID { get; set; }
        
        public DateTime TimeStamp { get; set; }
        public bool RedirectToAuth { get; set; }
        public int? AuthOrganizationID { get; set; }
    }
}
