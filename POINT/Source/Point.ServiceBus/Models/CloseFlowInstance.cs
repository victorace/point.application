﻿namespace Point.ServiceBus.Models
{
    public class CloseFlowInstance : IPointServiceBusModel
    {
        public int FlowInstanceID { get; set; }
    }
}
