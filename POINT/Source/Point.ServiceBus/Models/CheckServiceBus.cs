﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Point.ServiceBus.Models
{
    public class ServiceBusCheck : ILoggingServiceBusModel
    {
        public string MachineName { get; set; }
        public DateTime TimeStamp { get; set; }
        public long UniqueID { get; set; }
    }
}
