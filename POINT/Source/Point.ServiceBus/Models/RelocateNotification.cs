﻿using Point.Database.Models;
using System;

namespace Point.ServiceBus.Models
{
    public class RelocateNotification : IDossierServiceBusModel, ICommunicationLog, IPointServiceBusModel
    {
        public int TransferID { get; set; }
        public int? EmployeeID { get; set; }
        public int? OrganizationID { get; set; }

        public int SourceDepartmentID { get; set; }
        public int DestinationDepartmentID { get; set; }
        public string RelocateReason { get; set; }
    }
}
