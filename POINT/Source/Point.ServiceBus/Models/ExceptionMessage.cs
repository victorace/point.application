﻿using System;
using Point.Database.Models;
using Point.Models.Enums;

namespace Point.ServiceBus.Models
{
    public class ExceptionMessage : IPointServiceBusModel, ICommunicationLog
    {
        public AlertLevel AlertLevel { get; set; }
        public string Title { get; set; }
        public int ErrorNumber { get; set; }
        public DateTime TimeStamp { get; set; }

        public int? TransferID { get; set; }
        public int? EmployeeID { get; set; }
        public string EmployeeUserName { get; set; }

        public string MachineName { get; set; }
        public string WindowsIdentity { get; set; }

        public string CurrentUrl { get; set; }
        public string QueryString { get; set; }
        public string PostData { get; set; }
        public string Cookies { get; set; }
        public string UserAgent { get; set; }
        public string IsAjaxRequest { get; set; }
        public string StackTrace { get; set; }
        public string Message { get; set; }
        public string InnerException { get; set; }

        public int? OrganizationID { get; set; }

        public CommunicationLogType MailType { get; set; }
    }
}

