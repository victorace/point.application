﻿using System.Threading.Tasks;
using Point.ServiceBus.Models;

namespace Point.ServiceBus
{
    public class Topic
    {
        public static async Task SendMessage(string topictype, string label, IPointServiceBusModel serializableobject)
        {
            await Helpers.SendTopicMessage(topictype, serializableobject.GetType().Name, label, serializableobject);
        }

        public static async Task SendMessage(string topictype, string label, IPointServiceBusModel serializableobject, string subscription)
        {
            await Helpers.SendTopicMessage(topictype, subscription, label, serializableobject);
        }
    }
}
