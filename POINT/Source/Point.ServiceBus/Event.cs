﻿using Point.ServiceBus.Models;

namespace Point.ServiceBus
{
    public class Event
    {
        public enum EventHub
        {
            PointFlow = 0
        }

        public static async void SendEvent(EventHub eventhub, string label, IPointServiceBusModel serializableobject)
        {
            await Helpers.SendEvent(eventhub.ToString(), label, serializableobject);
        }
    }
}
