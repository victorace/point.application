﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.ServiceBus;
using Microsoft.ServiceBus.Messaging;
using Newtonsoft.Json;
using Point.ServiceBus.Models;

namespace Point.ServiceBus
{
    public class Helpers
    {

        public static string QueueCheck = "check";
        public static string QueueExceptions = "exceptions";
        public static string QueueFlowInstance = "flowinstance";
        public static string QueueNotification = "notification";
        public static string QueueOrganizationSearch = "organizationsearch";
        public static string QueuePhaseQueue = "phasequeue";
        public static string QueueReadmodel = "readmodel";

        public static bool IsBackupActive { get; set; }

        private static List<Type> serverFailureExceptions = new List<Type>() {
            typeof(MessagingEntityNotFoundException),
            typeof(UnauthorizedAccessException),
            typeof(ServerBusyException),
            typeof(MessagingCommunicationException),
            typeof(TimeoutException),
            typeof(MessagingException) };

        #region Privates
        private static NamespaceManager _namespacemanagercache { get; set; }
        private static NamespaceManager GetNamespaceManager()
        {
            if (_namespacemanagercache == null)
            {
                _namespacemanagercache = NamespaceManager.CreateFromConnectionString(GetServiceBusConnectionString());
            }
            return _namespacemanagercache;
        }

        private static void SwitchBackup()
        {
            IsBackupActive = !IsBackupActive;
            _namespacemanagercache = null;
            _cachedfactory = null;
        }

        private static NamespaceManager[] _namespacemanagerscache { get; set; }
        private static NamespaceManager[] GetNamespaceManagers()
        {
            if (_namespacemanagerscache == null)
            {
                _namespacemanagerscache = new NamespaceManager[] {
                    NamespaceManager.CreateFromConnectionString(GetPrimaryServiceBusConnectionString()),
                    NamespaceManager.CreateFromConnectionString(GetSecondaryServiceBusConnectionString())
                };
            }
            return _namespacemanagerscache;
        }

        private static string GetServiceBusConnectionString()
        {
            if (!IsBackupActive)
            {
                return GetPrimaryServiceBusConnectionString();
            }
            else
            {
                return GetSecondaryServiceBusConnectionString();
            }
        }

        private static string GetPrimaryServiceBusConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["ServiceBusConnectionString"].ToString();
        }

        private static string GetSecondaryServiceBusConnectionString()
        {
            if (ConfigurationManager.ConnectionStrings["ServiceBusSecondaryConnectionString"] != null)
            {
                return ConfigurationManager.ConnectionStrings["ServiceBusSecondaryConnectionString"].ToString();
            }
            throw new Exception("ServiceBusSecondaryConnectionString not present on Config");
        }

        private static MessagingFactory _cachedfactory;
        private static MessagingFactory GetFactory()
        {
            if (_cachedfactory == null)
            {
                _cachedfactory = MessagingFactory.CreateFromConnectionString(GetServiceBusConnectionString());
            }
            return _cachedfactory;
        }

        private static MessagingFactory[] _cachedfactories;
        private static MessagingFactory[] GetFactories()
        {
            if(_cachedfactories == null)
            {
                _cachedfactories = new MessagingFactory[] { MessagingFactory.CreateFromConnectionString(GetPrimaryServiceBusConnectionString()), MessagingFactory.CreateFromConnectionString(GetSecondaryServiceBusConnectionString()) };
            }
            return _cachedfactories;
        }

        private static TopicClient GetTopicClient(string topicname)
        {
            return TopicClient.CreateFromConnectionString(GetServiceBusConnectionString(), topicname);
        }

        private static EventHubClient GetEventHubClient(string eventhubname)
        {
            return EventHubClient.CreateFromConnectionString(GetServiceBusConnectionString(), eventhubname);
        }
        #endregion

        public static async Task CreateQueue(string name)
        {
            var manager = GetNamespaceManager();
            if (!await manager.QueueExistsAsync(name))
            {
                await manager.CreateQueueAsync(name);
            }
        }

        public static async Task CreateQueues(string name)
        {
            var managers = GetNamespaceManagers();
            foreach (var manager in managers)
            {
                if (!await manager.QueueExistsAsync(name))
                {
                    await manager.CreateQueueAsync(name);
                }
            }
        }

        public static async Task CreateTopic(string topicname)
        {
            //TopicDescription Enables more configuration in the future instead of just string
            var namespacemanager = GetNamespaceManager();
            if (!await namespacemanager.TopicExistsAsync(topicname))
            {
                var topicdescription = new TopicDescription(topicname);
                await namespacemanager.CreateTopicAsync(topicdescription);
            }
        }

        public static async Task CreateEventHub(string eventhubname)
        {
            var manager = GetNamespaceManager();
            if (!await manager.EventHubExistsAsync(eventhubname))
            {
                await manager.CreateEventHubAsync(eventhubname);
            }
        }

        public static async Task<MessageSender> GetMessageSender(string queuename)
        {
            var factory = GetFactory();
            MessageSender sender = await factory.CreateMessageSenderAsync(queuename);
            return sender;
        }

        public static async Task<MessageReceiver[]> GetMessageReceivers(string queuename)
        {
            var recievers = new List<MessageReceiver>();

            var factories = GetFactories();
            foreach (var factory in factories)
            {
                recievers.Add(await factory.CreateMessageReceiverAsync(queuename));
            }
            return recievers.ToArray();
        }

        public static async Task<long> GetMessageCount(string queuename)
        {
            var manager = GetNamespaceManager();
            var queue = await manager.GetQueueAsync(queuename);
            return queue.MessageCount;
        }

        public static async Task CreateSubscription(string topicname, string subscriptionname)
        {
            SqlFilter filter = new SqlFilter(String.Format("SubScription = '{0}'", subscriptionname));

            var namespacemanager = GetNamespaceManager();
            if (!await namespacemanager.SubscriptionExistsAsync(topicname, subscriptionname))
            {
                await namespacemanager.CreateSubscriptionAsync(topicname, subscriptionname, filter);
            }
        }

        public static async Task SendTopicMessage(string topicname, string subscription, string label, IPointServiceBusModel serializableobject)
        {
            await CreateTopic(topicname);
            var topicclient = GetTopicClient(topicname);

            await CreateSubscription(topicname, subscription);
                        
            var message = new BrokeredMessage(serializableobject);
            message.Label = label;
            message.Properties["Type"] = serializableobject.GetType().FullName;
            message.Properties["Label"] = label;
            message.Properties["Subscription"] = subscription;

            await topicclient.SendAsync(message);
            await topicclient.CloseAsync();
        }

        private static async Task SendActualMessage(string queue, string label, IPointServiceBusModel serializableobject) {

            await CreateQueue(queue);

            var sender = await GetMessageSender(queue);
            var message = new BrokeredMessage(serializableobject)
            {
                Label = label
            };
            message.Properties["Type"] = serializableobject.GetType().FullName;
            message.Properties["Label"] = label;

            await sender.SendAsync(message);
            await sender.CloseAsync();
        }

        public static async Task SendMessage(string queue, string label, IPointServiceBusModel serializableobject)
        {
            await sendMessage(queue, label, serializableobject);
        }

        private static async Task sendMessage(string queue, string label, IPointServiceBusModel serializableobject)
        {
            try
            {
                await SendActualMessage(queue, label, serializableobject);
            }
            catch (Exception exc)
            {
                if (serverFailureExceptions.Contains(exc.GetType()))
                {
                    SwitchBackup();
                    try
                    {
                        await SendActualMessage(queue, label, serializableobject);
                    }
                    catch (Exception excbackup)
                    {
                        throw excbackup;
                    }
                }
                else
                {
                    throw exc;
                }
            }
        }

        public static async Task SendEvent(string eventhubname, string label, IPointServiceBusModel serializableobject)
        {
            await CreateEventHub(eventhubname);
            var client = GetEventHubClient(eventhubname);
            
            var serializedobject = JsonConvert.SerializeObject(serializableobject);
            EventData eventdata = new EventData(Encoding.UTF8.GetBytes(serializedobject));
            eventdata.Properties["Type"] = serializableobject.GetType().FullName;
            eventdata.Properties["Label"] = label;

            await client.SendAsync(eventdata);
            await client.CloseAsync();
        }
    }
}
