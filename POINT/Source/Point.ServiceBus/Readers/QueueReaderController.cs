﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.ServiceBus.Messaging;
using Point.Infrastructure.Helpers;

namespace Point.ServiceBus.Readers
{
    public static class QueueReaderController
    {
        public static event EventHandler<Exception> OnError;

        private static List<IQueueReader> queues { get; set; }
        private static List<MessageReceiver> runningreaders { get; set; }

        public static void RegisterQueue(IQueueReader queuereader)
        {
            if (queues == null) queues = new List<IQueueReader>();
            queues.Add(queuereader);
        }

        public static async void Start()
        {
            if (runningreaders == null) runningreaders = new List<MessageReceiver>();

            var threadsperqueue = ConfigHelper.GetAppSettingByName<int>("ThreadsPerQueue", 4);

            foreach (var queue in queues.Select(it => it.Queue).Distinct())
            {
                await Helpers.CreateQueues(queue);

                MessageReceiver[] recievers = runningreaders.Where(cl => cl.Path == queue).ToArray();
                if (recievers == null || recievers.Count() == 0)
                {
                    recievers = await Helpers.GetMessageReceivers(queue);
                    
                    OnMessageOptions options = new OnMessageOptions();
                    options.AutoComplete = false;
                    options.MaxConcurrentCalls = threadsperqueue;
                    options.ExceptionReceived += options_ExceptionReceived;

                    foreach (var reciever in recievers)
                    {
                        reciever.OnMessageAsync(OnMessageReceivedAsync, options);
                        runningreaders.Add(reciever);
                    }
                }
            }
        }

        static void options_ExceptionReceived(object sender, ExceptionReceivedEventArgs e)
        {
            if (OnError != null)
                OnError(sender, e.Exception);
        }

        public static async Task Stop()
        {
            foreach (var reader in runningreaders)
                await reader.CloseAsync();
        }

        private static async Task OnMessageReceivedAsync(BrokeredMessage message)
        {
            var reader = queues.FirstOrDefault(rd => rd.MessageType.FullName == message.Properties["Type"].ToString());
            if (reader != null)
                await Task.Run(() => reader.MessageRecieved(message));
        }
    }
}
