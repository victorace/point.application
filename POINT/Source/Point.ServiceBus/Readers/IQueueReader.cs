﻿using System;
using Microsoft.ServiceBus.Messaging;
using Point.ServiceBus.EventTypes;

namespace Point.ServiceBus.Readers
{
    public interface IQueueReader
    {
        string Queue { get; set; }
        Type MessageType { get; set; }

        event EventHandler<IMessageEvent> OnMessageRecieved;
        void MessageRecieved(BrokeredMessage message);
    }
}
