﻿using System;
using Microsoft.ServiceBus.Messaging;
using Point.ServiceBus.EventTypes;

namespace Point.ServiceBus.Readers
{
    public class QueueReader<T> : IQueueReader
    {
        public event EventHandler<IMessageEvent> OnMessageRecieved;

        public void MessageRecieved(BrokeredMessage message)
        {
            OnMessageRecieved(this, new MessageEvent<T>(message));
        }
        
        public string Queue { get; set; }
        public Type MessageType { get; set; }

        public QueueReader(string queue)
        {
            Queue = queue;
            MessageType = typeof(T);
        }
    }
}
