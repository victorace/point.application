﻿using System;
using Microsoft.ServiceBus.Messaging;

namespace Point.ServiceBus.EventTypes
{
    public class TopicEvent<T> : EventArgs
    {
        public BrokeredMessage Message { get; set; }
        public T ObjectData { get; set; }
    }
}
