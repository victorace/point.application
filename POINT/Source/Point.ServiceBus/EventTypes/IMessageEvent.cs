﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Point.ServiceBus.EventTypes
{
    public interface IMessageEvent
    {
        object ObjectData { get; set; }
        void Complete();
        bool IsOldMessage { get; }
        string FullName { get; }
        void Error(string errormessage);
        void Invalid(string reason, string stacktrace);
        void DeadLetter(string reason, string stacktrace);
        DateTime StartTime { get; set; }
        DateTime EndTime { get; set; }
    }
}
