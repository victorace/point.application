﻿using System;
using System.Diagnostics;
using Microsoft.ServiceBus.Messaging;
using Point.Infrastructure.Helpers;

namespace Point.ServiceBus.EventTypes
{
    public class MessageEvent<T> : EventArgs, IMessageEvent
    {
        public MessageEvent(BrokeredMessage message)
        {
            Message = message;
            ObjectData = message.GetBody<T>();
            StartTime = DateTime.Now;
        }

        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        protected BrokeredMessage Message { get; set; }
        public object ObjectData { get; set; }

        public void Complete()
        {
            Message.CompleteAsync();
            EndTime = DateTime.Now;
        }

        public bool IsOldMessage
        {
            get
            {
                return Message.EnqueuedTimeUtc < DateTime.UtcNow.AddDays(-3);
            }
        }

        public string FullName
        {
            get { return Message.Label + " - " + Message.MessageId; }
        }

        public void Error(string errormessage)
        {
            Console.WriteLine("Message error: " + Message.Label + " - " + Message.MessageId);
            EventLogHelper.AddItem("Point", "MessageError", errormessage, EventLogEntryType.Error);
            EndTime = DateTime.Now;
        }

        public void Invalid(string reason, string stacktrace)
        {
            Console.WriteLine("Message DeadLetter: " + Message.MessageId);
            EventLogHelper.AddItem("Point", "MessageDeadLetter", "Message DeadLetter: " + Message.MessageId, EventLogEntryType.Information);

            Message.DeadLetterAsync(reason, stacktrace);
            EndTime = DateTime.Now;
        }

        public void DeadLetter(string reason, string stacktrace)
        {
            Console.WriteLine("Message DeadLetter: " + Message.MessageId);
            EventLogHelper.AddItem("Point", "MessageDeadLetter", "Message DeadLetter: " + Message.MessageId, EventLogEntryType.Information);

            Message.DeadLetterAsync(reason, stacktrace);
            EndTime = DateTime.Now;
        }
    }
}
