﻿using System;
using System.Globalization;
using System.Runtime.Serialization;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Point.Infrastructure.Extensions;
using System.Web.Routing;
using Point.Infrastructure.Exceptions;
using System.Linq.Expressions;

namespace Point.Infrastructure.Attributes
{

    public static class AntiFiddleInjection
    {
        public static string DigestKey = "Digest";
        public static string CreateDigest(object value)
        {
            if (value == null)
            {
                return "";
            }
            byte[] protectedbytes = MachineKey.Protect(Encoding.UTF8.GetBytes(Convert.ToString(value)), "Encryption");
            return protectedbytes.ByteArrayToHexString();
        }

        public static void AddDigest(this RouteValueDictionary routeValues, object value)
        {
            if (routeValues.ContainsKey(DigestKey))
            {
                routeValues.Remove(DigestKey);
            }
            routeValues.Add(DigestKey, CreateDigest(value));

        }

        public static MvcHtmlString AntiFiddleToken(this HtmlHelper html, object value)
        {
            return generateHiddenFormField(value);
        }

        public static MvcHtmlString AntiFiddleTokenFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression)
        {
            object modelValue = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData).Model;

            return generateHiddenFormField(modelValue);
        }

        private static MvcHtmlString generateHiddenFormField(object value)
        {
            TagBuilder tagbuilder = new TagBuilder("input");
            tagbuilder.Attributes["type"] = "hidden";
            tagbuilder.Attributes["name"] = DigestKey;
            tagbuilder.Attributes["value"] = CreateDigest(value);
            return new MvcHtmlString(tagbuilder.ToString(TagRenderMode.SelfClosing));
        }

        public static void AddDigest(this RouteValueDictionary routeValues, string routeValueKey)
        {
            if (routeValues.ContainsKey(routeValueKey))
            {
                AddDigest(routeValues, routeValues[routeValueKey]);
            }
        }

        public static string GetDigestValue(this HttpContextBase httpContextBase)
        {
            string value = httpContextBase.Request.GetParam(DigestKey);
            return value;
        }

        public static string GetPlainDigest(string digest)
        {
            byte[] plainbytes = MachineKey.Unprotect(digest.HexStringToByteArray(), "Encryption");
            string plaindigest = Encoding.UTF8.GetString(plainbytes);
            return plaindigest;
        }

        public static bool CheckDigest(object value, string digest)
        {
            if (String.IsNullOrWhiteSpace(digest) || value == null)
            {
                return false;
            }

            return GetPlainDigest(digest) == Convert.ToString(value);
        }
    }
    
    public class ValidateAntiFiddleInjectionAttribute : ActionFilterAttribute
    {
        private string propertyName { get; set; }
        private bool nullAllowed { get; set; }

        public ValidateAntiFiddleInjectionAttribute(string propertyName, bool nullAllowed = false)
        {
            this.propertyName = propertyName;
            this.nullAllowed = nullAllowed;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            bool debug = filterContext.HttpContext.IsDebuggingEnabled;

            if (String.IsNullOrWhiteSpace(propertyName))
            {
                throwException("Digest.propertyName value must be a non empty string.", debug);
            }

            string propertyvalue = filterContext.HttpContext.Request.GetParam(propertyName);
            if (!(nullAllowed && string.IsNullOrEmpty(propertyvalue)))
            {
                if (String.IsNullOrWhiteSpace(propertyvalue))
                {
                    throwException(String.Format("Digest.PropertyName [{0}] must be present and contain a value.", propertyName), debug);
                }

                string digestvalue = filterContext.HttpContext.GetDigestValue();
                if (String.IsNullOrWhiteSpace(digestvalue))
                {
                    throwException(String.Format("Digest must be present and contain a value."), debug);
                }

                if (!AntiFiddleInjection.CheckDigest(propertyvalue, digestvalue))
                {
                    throwException(String.Format("Digest check failed. digestvalue [{0}], propertyvalue [{1}], plaindigest [{2}]", digestvalue, propertyvalue, AntiFiddleInjection.GetPlainDigest(digestvalue)), debug);
                }
            }
            base.OnActionExecuting(filterContext);
        }

        private void throwException(string message, bool debugMode)
        {
            if (!debugMode)
            {
                message = "Digest check failed."; //Don't provide any more info than needed.
            }

            throw new PointJustLogException(message);
        }
    }
}
