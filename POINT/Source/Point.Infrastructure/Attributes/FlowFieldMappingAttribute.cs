﻿using System;

namespace Point.Infrastructure.Attributes
{
    public class FlowFieldMappingAttribute : Attribute
    {
        public FlowFieldMappingAttribute(string flowField)
        {
            FlowFieldName = flowField;
        }
        public string FlowFieldName { get; set; }
    }
}
