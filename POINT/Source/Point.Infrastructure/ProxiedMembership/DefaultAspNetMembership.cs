using System;
using System.Web.Security;

namespace Point.Infrastructure.ProxiedMembership
{
    public class DefaultAspNetMembership : IMembership
    {
        public User GetUser()
        {
            //var userId = Guid.Parse() // Session["ProviderUserKey"]
            var membershipUser = Membership.GetUser();
            if (membershipUser == null) return null;

            var userId = Guid.Parse(membershipUser.ProviderUserKey.ToString());

            var user = new User(userId, membershipUser.UserName);

            return user;
        }

        public User GetUser(Guid userId)
        {
            object oUserId = userId;

            var membershipUser = Membership.GetUser(oUserId);
            if (membershipUser == null) return null;

            var user = new User(userId, membershipUser.UserName);

            return user;
        }
    }
}
