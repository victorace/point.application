using System;
using System.Collections.Generic;

namespace Point.Infrastructure.ProxiedMembership
{
    public class User
    {
        private List<string> _roles = new List<string>();

        public User(Guid userId, string userName)
        {
            UserId = userId;
            UserName = userName;
        }

        public User(Guid userId, string userName, IEnumerable<string> roles)
            : this(userId, userName)
        {
            _roles.AddRange(roles);
        }

        public Guid UserId { get; private set; }

        public string UserName { get; private set; }

        public IList<string> Roles
        {
            get
            {
                return _roles;
            }
            private set
            {
                _roles.Clear();
                _roles.AddRange(value);
            }
        }
    }
}
