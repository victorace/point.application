using System;
using System.Threading;
using Point.Infrastructure.Extensions;

namespace Point.Infrastructure.ProxiedMembership
{
    /// <summary>
    /// Singleton (because of lack of IoC), to provide a (ASP.NET) Membership Proxy. Default implementation is the 'real' ASP.NET Membership (System.Web.*).
    /// </summary>
    public class MembershipProxy
    {
        #region Thread-local
        private static ThreadLocal<IMembership> _instance = new ThreadLocal<IMembership>(() => new DefaultAspNetMembership());
        #endregion

        /// <summary>
        /// Singleton instance property
        /// </summary>
        /// <returns>IMembership implementation</returns>
        public static IMembership Instance
        {
            get
            {
                return _instance.Value;
            }
        }

        protected MembershipProxy()
        {
            Reset();
        }

        /// <summary>
        /// Typically used with unit test only.
        /// </summary>
        public static void Set<T>(T instance) where T : IMembership
        {
            if (instance == null)
                throw new ArgumentNullException("Instance (implementor) cannot be null");

            var obj = Instance;
            obj.SafeDispose();

            _instance.Value = instance;
        }

        /// <summary>
        /// Just a shortcut to setting the FixedMembership implementation.
        /// </summary>
        public static void SetFixed(Guid userId, string userName)
        {
            var obj = Instance;
            obj.SafeDispose();

            _instance.Value = new FixedMembership(userId, userName);
        }

        /// <summary>
        /// Set back the default implementation.
        /// </summary>
        public static void Reset()
        {
            Set(new DefaultAspNetMembership());
        }
    }
}
