﻿using System;

namespace Point.Infrastructure.ProxiedMembership
{
    public interface IMembership
    {
        User GetUser();
        User GetUser(Guid userId);
    }

}
