using System;

namespace Point.Infrastructure.ProxiedMembership
{
    public class FixedMembership : IMembership
    {
        private readonly Guid _userId;
        private readonly string _userName;

        public FixedMembership(User user)
            : this(user.UserId, user.UserName)
        {
        }

        public FixedMembership(Guid userId, string userName)
        {
            _userId = userId;
            _userName = userName;
        }

        public User GetUser()
        {
            var res = new User(_userId, _userName);

            return res;
        }

        public User GetUser(Guid userId)
        {
            if (userId != _userId)
                throw new InvalidOperationException("Fixed User ID mismatch");

            return this.GetUser();
        }
    }
}
