﻿using Point.Models.Enums;
using Point.Models.LDM.EOverdracht30.Enums;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Point.Infrastructure
{
    public class FormSection
    {
        public string Name { get; set; }
        public string ClassName { get; set; }
        public bool Visible { get; set; }
        public bool PrintOnly { get; set; }
        public List<FormSubSection> SubSections { get; set; } = new List<FormSubSection>();
    }

    public class FormSubSection
    {
        public string Name { get; set; }
        public bool Switchable { get; set; }
        public bool Visible { get; set; }

        public List<FormFieldGroup> FormFieldGroups { get; set; } = new List<FormFieldGroup>();
    }

    public class FormFieldGroup
    {
        public string FormFieldGroupID { get; set; }
        public List<FormField> FormFields { get; set; } = new List<FormField>();

        public FormFieldGroupDisplayHandlerDelegate DisplayHandler;
    }

    public delegate void ImportHandlerDelegate(FormField formField);
    public delegate MvcHtmlString FormFieldGroupDisplayHandlerDelegate(HtmlHelper htmlhelper, FormFieldGroup formFieldGroup);

    public class ImportProperty
    {
        public ImportProperty(ImportSourceType importSourceType, ImportHandlerDelegate importHandler, bool overwriteValue = false)
        {
            ImportSourceType = importSourceType;
            ImportHandler = importHandler;
            OverwriteValue = overwriteValue;
        }

        public ImportSourceType ImportSourceType { get; set; }
        public ImportHandlerDelegate ImportHandler;
        public bool OverwriteValue { get; set; }
    }

    public class FormField
    {
        public string ImportValue { get; set; }
        public string FlowFieldName { get; set; }

        public Source Source { get; set; }
        public bool IsFilled { get; set; }

        public List<ImportProperty> ImportProperties = new List<ImportProperty>();
        public List<FormFieldTrigger> FormFieldTriggers { get; set; } = new List<FormFieldTrigger>();
    }

    public class FormFieldTrigger
    {
        public string TriggerFlowFieldValue { get; set; }
        public string TriggerFormFieldGroupID { get; set; }
    }
}