﻿using System;
using System.Collections.Generic;

namespace Point.Infrastructure.Caching
{
    public interface ICache
    {
        // FUTURE/MAYBE
        //object this[string key] { get; set; }
        //void Insert(string key, object value, CacheDependency dependencies, DateTime absoluteExpiration, TimeSpan slidingExpiration);

        T Get<T>(string key);
        void Insert(string key, object value, string profile = "CacheMedium");
        void Insert(string key, object value, TimeSpan slidingExpiration);
        object Remove(string key);
        void RemoveAll(string keymatch = null);
        IEnumerable<string> RemoveAll(bool returnKeys);
    }
}
