using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Caching;
using Point.Infrastructure.Helpers;

namespace Point.Infrastructure.Caching
{
    public class DefaultWebCache : ICache
    {
        #region Private impl.

        private IEnumerable<string> RemoveAllAndReturnKeys()
        {
            var res = new List<string>();

            var enumerator = HttpRuntime.Cache.GetEnumerator();

            while (enumerator.MoveNext())
            {
                res.Add(enumerator.Key.ToString());

                Remove(enumerator.Key.ToString());
            }

            res.Sort();

            return res.AsReadOnly();
        }

        #endregion

        private object Get(string key)
        {
            return HttpRuntime.Cache[key];
        }

        public T Get<T>(string key)
        {
            var obj = Get(key);
            
            return (T)obj;
        }

        public void Insert(string key, object value, string profile = "CacheMedium")
        {
            var expirydate = DateTime.Now.AddMinutes(ConfigHelper.GetAppSettingByName<int>(profile));
            HttpRuntime.Cache.Insert(key, value, null, expirydate, Cache.NoSlidingExpiration);
        }

        public void Insert(string key, object value, TimeSpan slidingExpiration)
        {
            HttpRuntime.Cache.Insert(key, value, null, Cache.NoAbsoluteExpiration, slidingExpiration);
        }

        public object Remove(string key)
        {
            return HttpRuntime.Cache.Remove(key);
        }

        public void RemoveAll(string keymatch = null)
        {
            var enumerator = HttpRuntime.Cache.GetEnumerator();

            while (enumerator.MoveNext())
            {
                if (keymatch == null)
                {
                    Remove(enumerator.Key.ToString());
                }
                else if (enumerator.Key.ToString().Contains(keymatch))
                {
                    Remove(enumerator.Key.ToString());
                }
            }
        }

        public IEnumerable<string> RemoveAll(bool returnKeys)
        {
            if (returnKeys)
            {
                return RemoveAllAndReturnKeys();
            }
            else
            {
                RemoveAll();

                return (new List<string>()).AsReadOnly();
            }
        }
    }
}
