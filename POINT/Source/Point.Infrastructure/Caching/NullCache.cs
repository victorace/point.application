using System;
using System.Collections.Generic;

namespace Point.Infrastructure.Caching
{
    public class NullCache : ICache
    {
        public object Get(string key)
        {
            return null;
        }

        public T Get<T>(string key)
        {
            var obj = Get(key);

            return (T)obj;
        }

        public void Insert(string key, object value, TimeSpan slidingExpiration)
        {

        }

        public void Insert(string key, object value, string profile = "CacheMedium")
        {
            // NOP
        }

        public object Remove(string key)
        {
            return null;
        }

        public void RemoveAll(string keymatch = null)
        {
            // NOP
        }

        public IEnumerable<string> RemoveAll(bool returnKeys)
        {
            return (new List<string>()).AsReadOnly();
        }
    }
}
