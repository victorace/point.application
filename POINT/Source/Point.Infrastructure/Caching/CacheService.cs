using System.Threading;
using Point.Infrastructure.Extensions;

namespace Point.Infrastructure.Caching
{
    /// <summary>
    /// Singleton (because of lack of IoC), to provide Caching functionality. Default implementation is ASP.NET web cache (System.Web.*).
    /// </summary>
    public class CacheService
    {
        #region Thread-local
        private static ThreadLocal<ICache> _instance = new ThreadLocal<ICache>(() => new DefaultWebCache());
        #endregion

        /// <summary>
        /// Singleton instance property
        /// </summary>
        /// <returns>ICache implementation</returns>
        public static ICache Instance
        {
            get
            {
                return _instance.Value;
            }
        }
                
        /// <summary>
        /// Set back the default implementation.
        /// </summary>
        public static void Reset()
        {
            _instance.Value = new DefaultWebCache();
        }
    }
}
