﻿
namespace Point.Infrastructure
{
    public class MasterPageVars
    {
        public string FocusFieldClientID = "";
        public bool FocusFieldDoFocus = true;
        public string LogFilter = null;
        public string LogSelectMethod = null;
        public string LogSelectParameters = null;
        public string PageTitle = "";
    }
}