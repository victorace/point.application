﻿
namespace Point.Infrastructure.Constants
{
    public static class SessionIdentifiers
    {
        public static class SystemMessage
        {
            public const string PopupSeen = "PopupSeen";
        }

        public static class Point
        {
            public const string SessionID = "SessionID";
            public const string ForceDesktop = "ForceDesktop";
            public const string ForceOld = "forceOld";
        }

        public static class Authorization
        {
            public const string ProviderUserKey = "ProviderUserKey";
        }

    }
}
