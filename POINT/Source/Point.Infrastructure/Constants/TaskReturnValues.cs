﻿
namespace Point.Infrastructure.Constants
{
    public static class TaskReturnValues
    {
        public const string Success = "OK";

        public const string Failed = "NOK";
    }
}
