﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Point.Infrastructure.Helpers
{
    /// <summary>
    /// Helps getting a type by its name. Uses a cache to speed up subsequent lookups.
    /// </summary>
    public static class GetTypeByNameHelper
    {
        private static Dictionary<string, Type> _dicTypes = new Dictionary<string, Type>();

        /// <summary>
        /// Looks up a type by its name. Same semantics as <code>Type.GetType()</code> but when not found, tries the assemblies
        /// loaded in the current AppDomain. Ultimately scans the current AppDomain base directory.
        /// Results are cached to speed up subsequent lookups.
        /// </summary>
        /// <param name="fullTypeName">Type name either short, full or fully qualified</param>
        /// <returns>The type found or null if not found</returns>
        public static Type GetType(string fullTypeName)
        {
            return GetType(fullTypeName, true);
        }

        /// <summary>
        /// Looks up a type by its name. Same semantics as <code>Type.GetType()</code> but when not found, tries the assemblies
        /// loaded in the current AppDomain. Optionally scans the current AppDomain base directory.
        /// Results are cached to speed up subsequent lookups.
        /// </summary>
        /// <param name="fullTypeName">Type name either short, full or fully qualified</param>
        /// <param name="allowDirectoryScan">Specifies if scanning of the current AppDomain base directory is allowed. Set true to allow scan.</param>
        /// <returns>The type found or null if not found</returns>
        public static Type GetType(string fullTypeName, bool allowDirectoryScan)
        {
            if (!_dicTypes.ContainsKey(fullTypeName))
            {
                Assembly[] currentAppDomainAssemblies = null;

                Type type = Type.GetType(fullTypeName);

                if (type == null)
                {
                    currentAppDomainAssemblies = AppDomain.CurrentDomain.GetAssemblies();

                    type = currentAppDomainAssemblies
                        .Select(a => a.GetType(fullTypeName))
                        .FirstOrDefault(t => t != null);
                }

                if (type == null && allowDirectoryScan)
                {
                    if (currentAppDomainAssemblies == null)
                    {
                        currentAppDomainAssemblies = AppDomain.CurrentDomain.GetAssemblies();
                    }

                    var path = AppDomain.CurrentDomain.BaseDirectory;

                    string[] fileNames = Directory.GetFiles(path, "*.dll");

                    foreach (string fileName in fileNames)
                    {
                        try
                        {
                            // NOTE Assembly.Location throws NotSupportedException for 'dynamic assembly', so we enclose in try-catch

                            bool bAlreadyLoaded = currentAppDomainAssemblies
                                    .FirstOrDefault(a => !a.IsDynamic &&
                                                    a.Location.Equals(fileName, StringComparison.InvariantCultureIgnoreCase)) != null;

                            if (!bAlreadyLoaded)
                            {
                                var asm = Assembly.LoadFrom(fileName); // NOTE might fail for many reasons, ignore all

                                type = asm.GetType(fullTypeName);
                                if (type != null)
                                {
                                    break;
                                }
                            }
                        }
                        catch (System.Exception ex)
                        {
                            Debug.WriteLine(ex);
                        }
                    }
                }

                _dicTypes.Add(fullTypeName, type); // NOTE this possibly caches a null, so if the type isn't found, it never found, nor found later
            }
            return _dicTypes[fullTypeName];
        }
    }
}
