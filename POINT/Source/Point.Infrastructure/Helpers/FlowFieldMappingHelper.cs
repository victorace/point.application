﻿using Point.Infrastructure.Attributes;
using Point.Infrastructure.Extensions;
using System.Collections.Generic;
using System.Linq;

namespace Point.Infrastructure.Helpers
{
    public static class FlowFieldMappingHelper
    {
        public static void Map(object obj, ref Dictionary<string, string> dictionary)
        {
            if (obj == null)
            {
                return;
            }

            var properties = obj.GetType().GetProperties();

            foreach (var property in properties)
            {
                var value = property.GetValue(obj);

                if (property.PropertyType.IsClass && !property.PropertyType.FullName.StartsWith("System."))
                {
                    Map(value, ref dictionary);
                }
                else
                {
                    var key = property.Name;
                    if (!key.Equals("Error", System.StringComparison.InvariantCultureIgnoreCase))
                    {
                        var mapping = property.GetCustomAttributes(typeof(FlowFieldMappingAttribute), true).FirstOrDefault();
                        if (mapping != null)
                        {
                            key = ((FlowFieldMappingAttribute)mapping).FlowFieldName;
                        }
                    }
                    if (!dictionary.ContainsKey(key))
                    {
                        dictionary.Add(key, value.ConvertTo<string>());
                    }
                }
            }
        }
    }
}
