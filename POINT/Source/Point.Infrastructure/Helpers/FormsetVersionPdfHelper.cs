﻿using Point.Infrastructure.Attributes;
using Point.Log4Net;
using System;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Configuration;

namespace Point.Infrastructure.Helpers
{
    public class FormsetVersionPdfHelper
    {
        private static string webserver = ConfigHelper.GetAppSettingByName<string>("WebServer").TrimEnd('/');
        private static int max_retries = ConfigHelper.GetAppSettingByName<int>("GetPDFDataMaximumRetries", 2);

        public static string GetSessionCookieName()
        {
            string sessioncookiename = "";
            try
            {
                SessionStateSection sessionStateSection = (SessionStateSection)ConfigurationManager.GetSection("system.web/sessionState");
                sessioncookiename = sessionStateSection.CookieName;
            }
            catch { /* ignore */ }

            if (String.IsNullOrEmpty(sessioncookiename))
            {
                sessioncookiename = "ASP.NET_SessionId";
            }
            return sessioncookiename;
        }

        public static byte[] GetPDFData(int formSetVersionID, int employeeID)
        {
            byte[] datapdf = null;
            int retries = 0;
            bool success = true;

            do
            {
                datapdf = null;
                success = true;

                if (formSetVersionID > 0 && employeeID > 0)
                {
                    string connecturl = String.Format("{0}/SecurePdf/Connect?FormSetVersionID={1}&Digest={2}", webserver, formSetVersionID, AntiFiddleInjection.CreateDigest(formSetVersionID));
                    string loginurl = String.Format("{0}/SecurePdf/Login?FormSetVersionID={1}&Digest={2}&EmployeeID={3}", webserver, formSetVersionID, AntiFiddleInjection.CreateDigest(formSetVersionID), employeeID);
                    string formseturl = String.Format("{0}/SecurePdf/GetFormSetVersion?FormSetVersionID={1}&Digest={2}&EmployeeID={3}", webserver, formSetVersionID, AntiFiddleInjection.CreateDigest(formSetVersionID), employeeID);
                    string disconnecturl = String.Format("{0}/SecurePdf/Disconnect", webserver);

                    using (CookieAwareWebClient client = new CookieAwareWebClient())
                    {
                        if (success)
                        {
                            // 1 - Hit the webserver to get a session with it
                            try
                            {
                                client.DownloadData(connecturl);
                            }
                            catch (Exception exception)
                            {
                                datapdf = HandleExceptionPDF(formSetVersionID, employeeID, exception);
                                success = false;
                            }
                        }

                        if (success)
                        {
                            client.Headers.Add("DigestEmployee", AntiFiddleInjection.CreateDigest(employeeID));
                            var sessioncookie = client.ResponseCookies[GetSessionCookieName()];
                            client.Headers.Add("DigestSession", AntiFiddleInjection.CreateDigest(sessioncookie.Value));

                            // 2 - Hit the login, with the 'session' secured on various ways (client: cookie, transport: https-header, server: unique sessionid)
                            try
                            {
                                client.DownloadData(loginurl);
                            }
                            catch (Exception exception)
                            {
                                datapdf = HandleExceptionPDF(formSetVersionID, employeeID, exception);
                                success = false;
                            }
                        }

                        if (success)
                        {
                            // 3 - Download the PDF('s)
                            try
                            {
                                datapdf = client.DownloadData(formseturl);
                                if (datapdf == null || datapdf?.Length == 0)
                                {
                                    success = false;
                                    datapdf = HandleExceptionPDF(formSetVersionID, employeeID, "DownloadData returned no data.");
                                }
                            }
                            catch (Exception exception)
                            {
                                datapdf = HandleExceptionPDF(formSetVersionID, employeeID, exception);
                                success = false;
                            }
                        }

                        if (success)
                        {
                            // 4 - Disconnect and clear cookies/session
                            try
                            {
                                client.DownloadData(disconnecturl);
                            }
                            catch { /* ignore */ }
                        }
                    }
                }

                success = validate_pdf(formSetVersionID, employeeID, ref datapdf);

                if (!success)
                {
                    retries++;

                    ExceptionHelper.Logger.Warning($"GetPDFData failed. Attempt:{retries}, FormSetVersionID:{formSetVersionID},EmployeeID:{employeeID}.");
                }

            } while (!success && retries < max_retries);

            return datapdf;
        }

        private static bool validate_pdf(int formsetversionid, int employeeid, ref byte[] datapdf)
        {
            bool success = true;

            if (datapdf == null)
            {
                success = false;

                datapdf = HandleExceptionPDF(formsetversionid, employeeid, "Required parameters not specified or invalid.");
            }
            else
            {
                if (datapdf.Length == 0)
                {
                    success = false;

                    datapdf = HandleExceptionPDF(formsetversionid, employeeid, "PDF contained no data.");
                }
                // Test if it's a valid PDF (i.e. starting with %PDF-) If not, it probably contains HTML and might have an errornumber in it
                else if (!(datapdf[0] == 0x25 && datapdf[1] == 0x50 && datapdf[2] == 0x44 && datapdf[3] == 0x46 && datapdf[4] == 0x2D))
                {
                    success = false;

                    string stringpdf = Encoding.Default.GetString(datapdf);
                    Match match = Regex.Match(stringpdf, @"Foutcode nummer: (\d*),");
                    if (match.Success)
                    {
                        datapdf = HandleExceptionPDF(formsetversionid, employeeid, match.Groups[1].Value);
                    }
                    else
                    {
                        datapdf = HandleExceptionPDF(formsetversionid, employeeid, "Could not create a valid pdf.");
                    }

                    try
                    {
                        string pdf_contents = Encoding.Default.GetString(datapdf);
                        ExceptionHelper.Logger.Warning($"validate_pdf failed. FormSetVersionID:{formsetversionid},EmployeeID:{employeeid}{Environment.NewLine}{pdf_contents}");
                    }
                    catch { /* ignore */ }
                }
            }

            return success;
        }

        private static byte[] HandleExceptionPDF(int formsetversionid, int employeeid, string message)
        {
            return HandleExceptionPDF(formsetversionid, employeeid, null, message);
        }

        private static byte[] HandleExceptionPDF(int formsetversionid, int employeeid, Exception exception)
        {
            return HandleExceptionPDF(formsetversionid, employeeid, exception, exception?.Message);
        }

        private static byte[] HandleExceptionPDF(int formsetversionid, int employeeid, Exception exception, string message)
        {
            message = $"{message}{Environment.NewLine}{formsetversionid} {employeeid}";

            var logparameters = new LogParameters()
            {
                Exception = exception,
                Message = message,
            };

            if (exception != null)
            {
                ExceptionHelper.Logger.Fatal(logparameters);
            }
            else
            {
                ExceptionHelper.Logger.Warning(logparameters);
            }

            string exceptionurl = String.Format("{0}/SecurePdf/GetExceptionPDF", webserver);

            byte[] datapdf = null;
            using (CookieAwareWebClient client = new CookieAwareWebClient())
            {
                var param = new System.Collections.Specialized.NameValueCollection();
                param.Add("errorcode", message);
                datapdf = client.UploadValues(exceptionurl, param);
            }
            return datapdf;
        }
    }
}
