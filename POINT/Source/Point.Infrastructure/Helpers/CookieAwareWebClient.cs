﻿using System;
using System.Net;

namespace Point.Infrastructure.Helpers
{
    public class CookieAwareWebClient : WebClient
    {
        private const int timeoutseconds = 120;

        public CookieContainer CookieContainer { get; private set; }
        public CookieCollection ResponseCookies { get; set; }
        public CookieAwareWebClient()
        {
            CookieContainer = new CookieContainer();
        }

        protected override WebRequest GetWebRequest(Uri address)
        {
            var request = (HttpWebRequest)base.GetWebRequest(address);
            request.CookieContainer = CookieContainer;
            request.Timeout = timeoutseconds * 1000;
            return request;
        }

        protected override WebResponse GetWebResponse(WebRequest request)
        {
            var response = (HttpWebResponse)base.GetWebResponse(request);
            this.ResponseCookies = response.Cookies;
            return response;
        }
    }
}
