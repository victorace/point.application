﻿using System.Diagnostics;

namespace Point.Infrastructure.Helpers
{
    public class EventLogHelper
    {
        public static void AddItem(string applicationname, string logtype, string eventbody, EventLogEntryType type)
        {
            if (!EventLog.SourceExists(applicationname))
                EventLog.CreateEventSource(applicationname, logtype);

            EventLog.WriteEntry(applicationname, eventbody, type);
        }
    }
}
