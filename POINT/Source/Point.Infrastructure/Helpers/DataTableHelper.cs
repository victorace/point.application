﻿using Point.Infrastructure.Extensions;
using System.Collections.Generic;
using System.Data;

namespace Point.Infrastructure.Helpers
{
    public static class DataTableHelper
    {
        public static DataTable CreateWithColumnList(string tablename, IEnumerable<string> columns)
        {
            var datatable = new DataTable(tablename);
            if (!columns.IsNullOrEmpty()) {
                foreach (var column in columns) {
                    datatable.Columns.Add(column);
                }
            }
            return datatable;
        }
    }
}
