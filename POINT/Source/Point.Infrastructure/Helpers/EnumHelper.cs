﻿using System;
using System.Linq;
using System.Web.Mvc;
using Point.Infrastructure.Extensions;

namespace Point.Infrastructure.Helpers
{
    public static class EnumHelper
    {
        //Creates a SelectList for an enum value
        public static SelectList SelectListFor<T>(T selected) where T : Enum
        {
            Type t = typeof(T);
            if (t.IsEnum)
            {
                var values = Enum.GetValues(t).Cast<T>().Select(e => new { Id = Convert.ToInt32(e), Name = e.GetDescription() });

                return new SelectList(values, "Id", "Name", Convert.ToInt32(selected));
            }
            return null;
        }

        //Creates a SelectList for an enum type
        public static SelectList SelectListFor<T>() where T : Enum
        {
            Type t = typeof(T);
            if (t.IsEnum)
            {
                var values = Enum.GetValues(typeof(T)).Cast<T>().Select(e => new { Id = Convert.ToInt32(e), Name = e.GetDescription() });

                return new SelectList(values, "Id", "Name");
            }
            return null;
        }
    }
}
