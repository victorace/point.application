﻿using System;
using System.Drawing;
using System.IO;
using System.Threading.Tasks;

namespace Point.Infrastructure.Helpers
{
    public class FileHelper
    {
        public static string GetValidFileName(string fileName)
        {
            if (string.IsNullOrWhiteSpace(fileName))
            {
                return "";
            }
            fileName = Path.GetFileName(fileName);
            if (string.IsNullOrWhiteSpace(fileName))
            {
                return "";
            }
            return fileName?.Replace(Path.GetInvalidFileNameChars().ToString() + Path.GetInvalidPathChars().ToString(), "_");
        }

        public static string GetImageMimeType(byte[] contents)
        {
            string mime = "";
            try
            {
                using (var ms = new MemoryStream(contents))
                {
                    var img = Image.FromStream(ms);
                    mime = new ImageFormatConverter().ConvertToString(img.RawFormat);
                }
            }
            catch { /* ignore */ }
            if (string.IsNullOrEmpty(mime))
            {
                mime = "jpeg";
            }

            return $"image/{mime}".ToLower();
        }

        public static void DeleteFileAsync(string filePath)
        {
            if (string.IsNullOrEmpty(filePath) || !File.Exists(filePath))
            {
                return;
            }

            DeleteAsync(filePath);
        }

        public static Task DeleteAsync(string filePath)
        {
            try
            {
                var fi = new FileInfo(filePath);
                return Task.Factory.StartNew(() => fi.Delete());
            }
            catch (Exception)
            {
                // Swallow it! <:O
            }

            return null;
        }

    }


}
