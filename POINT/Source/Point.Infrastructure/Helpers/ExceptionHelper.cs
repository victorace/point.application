﻿using Point.Infrastructure.Exceptions;
using System;
using Point.Log4Net;

namespace Point.Infrastructure
{
    public class ExceptionHelper
    {
        public static readonly ILogger Logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static int SendMessageException(Exception exc, Uri uri = null, int? employeeid = null, int? transferid = null, string employeeusername = null, string postdata = null, string cookies = null, string useragent = null, bool? isAjaxRequest = null)
        {
            var logParameters = new LogParameters { Exception = exc, Message = exc?.Message, EmployeeID = employeeid, Url = uri?.AbsolutePath, TransferID = transferid, EmployeeUserName = employeeusername, PostData = postdata, Cookies = cookies, UserAgent = useragent, IsAjaxRequest = isAjaxRequest ?? false};
            return SendMessageException(logParameters);
        }

        public static int SendMessageException(LogParameters logParameters)
        {
            return logParameters.Exception == null || logParameters.Exception.GetType() == typeof(PointJustLogException) ? Logger.Info(logParameters) : Logger.Error(logParameters);
        }

    }
}
