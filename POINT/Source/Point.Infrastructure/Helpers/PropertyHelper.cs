﻿using System;
using System.Reflection;
using Point.Infrastructure.Extensions;

namespace Point.Infrastructure.Helpers
{
    public static class PropertyHelper
    {
        public static PropertyInfo GetProperty(Type type, string propName)
        {
            return type.GetProperty(propName);
        }

        public static string GetPropValue(object src, string propName)
        {
            var prop = GetProperty(src.GetType(), propName);
            if (prop == null) return null;

            object objval = prop.GetValue(src);
            if (objval == null) return null;

            if (prop.PropertyType == typeof(DateTime) || prop.PropertyType == typeof(DateTime?))
                return ((DateTime)objval).ToPointDateDisplay();

            return objval.ToString();
        }

        public static void SetPropValue(object src, string propName, string value)
        {
            var prop = GetProperty(src.GetType(), propName);
            if (prop == null) return;

            if (prop.PropertyType == typeof(string))
            {
                prop.SetValue(src, value);
            }
            else if (prop.PropertyType == typeof(int))
            {
                int newint = 0;
                if (int.TryParse(value, out newint))
                    prop.SetValue(src, newint);
            }
            else if (prop.PropertyType == typeof(int?))
            {
                int newint = 0;
                if (int.TryParse(value, out newint))
                    prop.SetValue(src, newint);
                else
                    prop.SetValue(src, null);
            }
            else if (prop.PropertyType == typeof(DateTime))
            {
                var newdatetime = default(DateTime);
                DateTime.TryParse(value, out newdatetime);

                prop.SetValue(src, newdatetime);

            }
            else if (prop.PropertyType == typeof(DateTime?))
            {
                var newdatetime = default(DateTime);
                if (DateTime.TryParse(value, out newdatetime))
                    prop.SetValue(src, newdatetime);
                else
                    prop.SetValue(src, null);

            }
            else if (prop.PropertyType == typeof(bool))
            {
                var newbool = false;
                if (bool.TryParse(value, out newbool))
                    prop.SetValue(src, newbool);
                else
                    prop.SetValue(src, false);
            }
            else if (prop.PropertyType == typeof(bool?))
            {
                var newbool = false;
                if (bool.TryParse(value, out newbool))
                    prop.SetValue(src, newbool);
                else
                    prop.SetValue(src, null);
            }
        }
    }
}
