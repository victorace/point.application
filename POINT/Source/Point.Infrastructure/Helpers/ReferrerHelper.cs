﻿using Point.Infrastructure.Exceptions;
using System.Linq;
using System.Web;

namespace Point.Infrastructure.Helpers
{
    public class ReferrerHelper
    {
        public static void CheckReferrer(HttpRequestBase httpRequestBase)
        {
            // See Referrer-Policy for allowed referrerd in web.config
            // <add name="Referrer-Policy" value="strict-origin" />   
            // Refer: https://scotthelme.co.uk/a-new-security-header-referrer-policy/
            if (httpRequestBase.UrlReferrer == null)
            {
                throw new PointSecurityException($"Referer not allowed", ExceptionType.AccessDenied);
            }
        }
    }
}
