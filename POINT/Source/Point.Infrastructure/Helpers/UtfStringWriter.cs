﻿using System.IO;
using System.Text;

namespace Point.Infrastructure.Helpers
{
    public class Utf8StringWriter : StringWriter
    {
        public override Encoding Encoding { get { return Encoding.UTF8; } }
    }

    public class Utf16StringWriter : StringWriter
    {
        public override Encoding Encoding { get { return Encoding.Unicode; } }
    }

    public class Utf32StringWriter : StringWriter
    {
        public override Encoding Encoding { get { return Encoding.UTF32; } }
    }

    public class UnicodetringWriter : StringWriter
    {
        public override Encoding Encoding { get { return Encoding.Unicode; } }
    }


}
