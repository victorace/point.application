﻿using Point.Infrastructure.Extensions;
using System;
using System.Configuration;

namespace Point.Infrastructure.Helpers
{
    public static class ConfigHelper
    {
        public static T GetAppSettingByName<T>(string name, T defaultvalue = default(T))
        {
            var stringValue = ConfigurationManager.AppSettings.Get(name);
            return stringValue == null ? defaultvalue : stringValue.ConvertTo<T>();
        }
    }
}
