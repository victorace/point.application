﻿using Point.Infrastructure.Helpers;
using System;
using System.Configuration;
using System.IO;
using System.Web;

namespace Point.Infrastructure
{
    public class Utils
    {
        public static string UriScheme()
        {
            if (!bool.TryParse(ConfigurationManager.AppSettings["ForceHTTPS"], out var forceHttps))
            {
                forceHttps = true;
            }

            return forceHttps ? Uri.UriSchemeHttps : Uri.UriSchemeHttp;
        }

        public static void ClearHeaders(HttpResponse httpResponse)
        {
            string[] headers = { "Server", "X-AspNet-Version", "X-AspNetMvc-Version", "X-Powered-By", "X-OneAgent-JS-Injection", "X-ruxit-JS-Agent" };
            //Note: "Server" is rewritten on IIS itself to 'N/A', for static content and others pages not served via this webapplication

            if (!httpResponse.HeadersWritten)
            {
                httpResponse.AddOnSendingHeaders((httpcontext) =>
                {
                    if (httpcontext != null && httpcontext.Response != null && httpcontext.Response.Headers != null)
                    {
                        foreach (string header in headers)
                        {
                            if (httpcontext.Response.Headers[header] != null)
                            {
                                httpcontext.Response.Headers.Remove(header);
                            }
                        }
                    }
                });
            }
        }
    }
}
