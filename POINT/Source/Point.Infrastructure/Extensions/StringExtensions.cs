﻿using Point.Models.LDM.EOverdracht30.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Point.Infrastructure.Extensions
{
    public static class StringExtensions
    {
        public static string UppercaseFirst(this string s)
        {
            if (String.IsNullOrEmpty(s)) return "";

            char[] a = s.ToCharArray();
            a[0] = char.ToUpper(a[0]);
            return new string(a);
        }

        public static string ToTitleCase(this string s)
        {
            if (String.IsNullOrEmpty(s)) return "";

            return System.Globalization.CultureInfo.InvariantCulture.TextInfo.ToTitleCase(s);
        }

        public static string NullToEmpty(this string s)
        {
            if (String.IsNullOrEmpty(s)) return "";

            return s;
        }

        public static string EmptyToNull(this string s)
        {
            if (String.IsNullOrEmpty(s)) return null;

            return s;
        }

        public static string ReplaceDoubleToSingle(this string s, char c, bool recursive = false)
        {
            s = s.Replace("".PadLeft(2, c), c.ToString());
            if (recursive && s.IndexOf("".PadLeft(2, c)) != -1)
                s = s.ReplaceDoubleToSingle(c, recursive);

            return s;
        }

        public static string BoolToJaNee(this bool? b)
        {
            if(b == null)
            {
                return "";
            }

            return b.ToString().BoolToJaNee();
        }

        public static string BoolToJaNee(this bool b)
        {
            return b.ToString().BoolToJaNee();
        }

        public static string BoolToJSTrueFalse(this bool b)
        {
            return b == true ? "true" : "false";
        }

        public static string BoolToTrueFalseNull(this bool? b)
        {
            if (b == true)
            {
                return "true";
            }
            else if (b == false)
            {
                return "false";
            }
            else
            {
                return "null";
            }
        }

        public static string BoolToJaNee(this string s)
        {
            if (String.IsNullOrEmpty(s)) return "";

            if (s == "1" || s.ToUpper() == "Y" || s.ToUpper() == "J" || s.ToUpper() == "TRUE") return "Ja";
            if (s == "0" || s.ToUpper() == "N" || s.ToUpper() == "FALSE") return "Nee";

            return s;
        }

        public static bool? StringToNullableBool(this string s)
        {
            if (String.IsNullOrEmpty(s))
            {
                return null;
            }
            else
            {
                return StringToBool(s);
            }
        }

        public static bool StringToBool(this string s)
        {
            if (String.IsNullOrEmpty(s) || s == "0" || s == "-1" || s.ToUpper() == "NO" || s.ToUpper() == "NEE" || s.ToUpper() == "FALSE") return false;
            if (s == "1" || s.ToUpper() == "YES" || s.ToUpper() == "JA" || s.ToUpper() == "TRUE") return true;

            throw new ApplicationException(String.Format("Can't convert '{0}' to a boolean.", s));
        }

        public static string ClientStringToGender(this string s)
        {
            if (string.IsNullOrEmpty(s)) return null;
            if (s.ToUpper() == "V" || s.ToUpper() == "F" || s.ToUpper() == "VROUW" || s.ToUpper() == "FEMALE") return "v";
            if (s.ToUpper() == "M" || s.ToUpper() == "MAN" || s.ToUpper() == "MALE") return "m";
            return null;
        }

        public static Geslacht? StringToGeslacht(this string s)
        {
            if (string.IsNullOrEmpty(s)) return null;
            if (s.ToUpper() == "V" || s.ToUpper() == "F" || s.ToUpper() == "VROUW" || s.ToUpper() == "FEMALE") return Geslacht.OF;
            if (s.ToUpper() == "M" || s.ToUpper() == "MAN" || s.ToUpper() == "MALE") return Geslacht.OM;
            return Geslacht.OUN;
        }

        public static string ToAlphanumeric(this string s)
        {
            if (String.IsNullOrEmpty(s)) return "";
            return new Regex("[^a-zA-Z0-9]").Replace(s, "");
        }

        public static bool ContainsIgnoreCase(this string s, string seek)
        {
            return s.Contains(seek, StringComparison.InvariantCultureIgnoreCase);
        }

        public static bool Contains(this string s, string seek, StringComparison comparison)
        {
            return s.IndexOf(seek, comparison) >= 0;
        }

        public static IEnumerable<int> ToIntList(this string s)
        {
            if (String.IsNullOrEmpty(s))
                yield break;

            foreach (var num in s.Split(','))
            {
                int i;
                if (int.TryParse(num, out i))
                    yield return i;
            }
        }

        public static string AddEllipsis(this string s, int length)
        {
            if (String.IsNullOrEmpty(s)) return "";
            if (s.Length <= length) return s;
            return s.Substring(0, length - 1) + "...";
        }

        public static string AddExtraMailAddressCount(this string s, string seperator = ";")
        {
            if (String.IsNullOrEmpty(s)) return "";
            if (!s.Contains(seperator, StringComparison.InvariantCultureIgnoreCase)) return s;
            string[] splitted = s.Split(new [] { seperator }, StringSplitOptions.RemoveEmptyEntries);
            if (splitted.Length <= 1)
            {
                return splitted[0];
            }
            else
            {
                return String.Format("{0} [+{1}]", splitted[0], splitted.Length - 1);
            }


        }

        public static string StripHtmlSpace(this string s)
        {
            s = new Regex(@">\s+<", RegexOptions.Compiled).Replace(s, "><");
            s = new Regex(@"\n\s+", RegexOptions.Compiled).Replace(s, " ");

            return s;
        }

        public static string DefaultIfEmpty(this string s, string defaultValue)
        {
            if (String.IsNullOrWhiteSpace(s))
                return defaultValue;
            else
                return s;
        }

        public static bool IsLogical(this string s)
        {
            if (string.IsNullOrWhiteSpace(s))
            {
                return false;
            }

            return s.Equals("Ja", StringComparison.InvariantCultureIgnoreCase) || s.Equals("True", StringComparison.InvariantCultureIgnoreCase);
        }

        public static bool ToLogical(this string s)
        {
            if (string.IsNullOrWhiteSpace(s))
            {
                return false;
            }

            return s.Equals("Ja", StringComparison.InvariantCultureIgnoreCase) || s.Equals("True", StringComparison.InvariantCultureIgnoreCase);
        }

        /// <summary>
        /// Fastest methods to convert byte[] to string and vice versa
        /// http://stackoverflow.com/questions/623104/byte-to-hex-string/3974535#3974535
        /// </summary>

        public static string ByteArrayToHexString(this byte[] bytes)
        {
            char[] c = new char[bytes.Length * 2];

            byte b;

            for (int bx = 0, cx = 0; bx < bytes.Length; ++bx, ++cx)
            {
                b = ((byte)(bytes[bx] >> 4));
                c[cx] = (char)(b > 9 ? b + 0x37 + 0x20 : b + 0x30);

                b = ((byte)(bytes[bx] & 0x0F));
                c[++cx] = (char)(b > 9 ? b + 0x37 + 0x20 : b + 0x30);
            }

            return new string(c);
        }

        public static byte[] HexStringToByteArray(this string s)
        {
            if (String.IsNullOrWhiteSpace(s) || s.Length % 2 != 0)
                return new byte[0];

            byte[] buffer = new byte[s.Length / 2];
            char c;
            for (int bx = 0, sx = 0; bx < buffer.Length; ++bx, ++sx)
            {
                // Convert first half of byte
                c = s[sx];
                buffer[bx] = (byte)((c > '9' ? (c > 'Z' ? (c - 'a' + 10) : (c - 'A' + 10)) : (c - '0')) << 4);

                // Convert second half of byte
                c = s[++sx];
                buffer[bx] |= (byte)(c > '9' ? (c > 'Z' ? (c - 'a' + 10) : (c - 'A' + 10)) : (c - '0'));
            }

            return buffer;
        }

        public static bool IsBase64String(this string s)
        {
            if (String.IsNullOrEmpty(s))
            {
                return false;
            }

            s = s.Trim();
            return (s.Length % 4 == 0) && Regex.IsMatch(s, @"^[a-zA-Z0-9\+/]*={0,3}$", RegexOptions.None);
        }

        public static string Base64Decode(this string s)
        {
            if (!IsBase64String(s))
            {
                return s;
            }
            return Encoding.UTF8.GetString(Convert.FromBase64String(s));
        }

        public static string ConvertDateFormatToJQueryDatepickerDateFormat(this string format)
        {
            /*
             *  Date used in this comment : 5th - Nov - 2009 (Thursday)
             *
             *  .NET    JQueryUI        Output      Comment
             *  --------------------------------------------------------------
             *  d       d               5           day of month(No leading zero)
             *  dd      dd              05          day of month(two digit)
             *  ddd     D               Thu         day short name
             *  dddd    DD              Thursday    day long name
             *  M       m               11          month of year(No leading zero)
             *  MM      mm              11          month of year(two digit)
             *  MMM     M               Nov         month name short
             *  MMMM    MM              November    month name long.
             *  yy      y               09          Year(two digit)
             *  yyyy    yy              2009        Year(four digit)             *
             */

            string currentFormat = format;

            // Convert the date
            currentFormat = currentFormat.Replace("dddd", "DD");
            currentFormat = currentFormat.Replace("ddd", "D");

            // Convert month
            if (currentFormat.Contains("MMMM"))
            {
                currentFormat = currentFormat.Replace("MMMM", "MM");
            }
            else if (currentFormat.Contains("MMM"))
            {
                currentFormat = currentFormat.Replace("MMM", "M");
            }
            else if (currentFormat.Contains("MM"))
            {
                currentFormat = currentFormat.Replace("MM", "mm");
            }
            else
            {
                currentFormat = currentFormat.Replace("M", "m");
            }

            // Convert year
            currentFormat = currentFormat.Contains("yyyy") ? currentFormat.Replace("yyyy", "yy") : currentFormat.Replace("yy", "y");

            return currentFormat;
        }

        /// <param name="methodName">Full methodName, seperated by / (like URL)</param>
        /// <param name="position">One-based position of the splitted methodName</param>
        public static string GetURLComponent(this string s, int position)
        {
            if (String.IsNullOrWhiteSpace(s)) return "";
            string[] components = s.Split(new[] { '/', '?' }, StringSplitOptions.RemoveEmptyEntries);

            if (components.Count() >= position)
                return components[position - 1];
            else
                return "";
        }

        public static DateTime SafeToDateTime(this string value)
        {
            if (!string.IsNullOrWhiteSpace(value))
            {
                DateTime d;
                if (DateTime.TryParse(value, out d))
                {
                    return d;
                }
            }

            return default(DateTime);
        }

        public static string Truncate(this string instance, int maxLength)
        {
            if (String.IsNullOrEmpty(instance) || maxLength <= 0)
            {
                return "";
            }
            return instance.Substring(0, Math.Min(maxLength, instance.Length));
        }

        public static string TruncateWithChar(this string instance, int maxLength, char suffix)
        {
            if (string.IsNullOrEmpty(instance))
            {
                return string.Empty;
            }

            string s = instance.Trim();

            if (s.Length <= maxLength)
            {
                return s;
            }

            string res = s.Substring(0, maxLength - 1) + suffix;

            return res;
        }

        public static string TruncateWithEllipsis(this string instance, int maxLength)
        {
            string res = instance.TruncateWithChar(maxLength, '…');

            return res;
        }

        public static string StripHtmlFast(this string source)
        {
            char[] array = new char[source.Length];
            int arrayIndex = 0;
            bool inside = false;

            for (int i = 0; i < source.Length; i++)
            {
                char let = source[i];
                if (let == '<')
                {
                    inside = true;
                    continue;
                }
                if (let == '>')
                {
                    inside = false;
                    continue;
                }
                if (!inside)
                {
                    array[arrayIndex] = let;
                    arrayIndex++;
                }
            }
            return new string(array, 0, arrayIndex);
        }

        public static string UrlCombine(this string url, params string[] paths)
        {
            if (paths.Length == 0) return url;
            StringBuilder stringbuilder = new StringBuilder(url.TrimEnd('/'));
            foreach (string path in paths)
            {
                stringbuilder.Append(string.Format("/{0}", path.Trim('/')));
            }
            return stringbuilder.ToString();
        }

        public static string Right(this string value, int length)
        {
            if (String.IsNullOrEmpty(value))
            {
                return string.Empty;
            }

            return value.Length <= length ? value : value.Substring(value.Length - length);
        }

        public static int? ToNullableInt(this string value)
        {
            int i;
            if (int.TryParse(value, out i))
            {
                return i;
            }
            return null;
        }

        public static string Obfuscate(this string value)
        {
            if (value == null)
            {
                return null;
            }

            Random random = new Random(DateTime.Now.Second);
            char[] randomLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray().OrderBy(s => (random.Next())).ToArray();
            char[] randomDigits = "1234567890".ToCharArray().OrderBy(s => (random.Next())).ToArray();

            string result = "";
            foreach (var character in value.ToCharArray())
            {
                int ascii = (int)character;
                if (ascii >= 65 && ascii <= 90)
                {
                    result += randomLetters[ascii - 65];
                }
                else if (ascii >= 97 && ascii <= 122)
                {
                    result += randomLetters[ascii - 97].ToString().ToLower();
                }
                else if (ascii >= 48 && ascii <= 57)
                {
                    result += randomDigits[ascii - 48];
                }
                else
                {
                    result += character;
                }
            }
            return result;
        }

    }
}