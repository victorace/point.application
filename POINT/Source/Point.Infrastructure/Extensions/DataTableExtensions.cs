﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Reflection;

namespace Point.Infrastructure.Extensions
{
    public static class DataTableExtensions
    {
        public static DataTable ConvertToDataTable<T>(this IList<T> objects)
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));

            DataTable table = new DataTable();

            foreach (PropertyDescriptor prop in properties)
            {
                var propertytype = Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType;
                if (propertytype == typeof(string) || !propertytype.IsClass)
                {
                    if (propertytype.IsEnum) {
                        table.Columns.Add(prop.Name, typeof(string));
                        continue;
                    }

                    table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
                }
            }
            foreach (T o in objects)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                {
                    var propertytype = Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType;
                    if (propertytype == typeof(string) || !propertytype.IsClass)
                    {
                        var value = prop.GetValue(o);
                        if (value == null)
                        {
                            row[prop.Name] = DBNull.Value;
                            continue;
                        } 
                        
                        if (propertytype.IsEnum)
                        {
                            row[prop.Name] = ((Enum)value).GetDescription();
                            continue;
                        }

                        row[prop.Name] = value;
                    }
                }
                table.Rows.Add(row);
            }

            table.AcceptChanges();

            return table;
        }

        public static DataTable ConvertToDataTable<T>(this T o, string tablename)
        {
            return ConvertToDataTable(o, tablename, Enumerable.Empty<string>());
        }

        public static DataTable ConvertToDataTable<T>(this T o, string tablename, IEnumerable<string> extracolumns)
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));

            DataTable table = new DataTable(tablename);

            foreach (PropertyDescriptor prop in properties)
            {
                if (prop.PropertyType == typeof(string) || !prop.PropertyType.IsClass)
                {
                    table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
                }
            }

            foreach (var extracolumn in extracolumns)
            {
                table.Columns.Add(extracolumn);
            }

            DataRow row = table.NewRow();
            foreach (PropertyDescriptor prop in properties)
            {
                if (prop.PropertyType == typeof(string) || !prop.PropertyType.IsClass)
                {
                    row[prop.Name] = prop.GetValue(o) ?? DBNull.Value;
                }
            }
            table.Rows.Add(row);

            table.AcceptChanges();

            return table;
        }

        public static DataRow FirstRow(this DataTable datatable)
        {
            if (datatable == null)
            {
                throw new ArgumentNullException(nameof(datatable));
            }

            if (datatable.Rows.IsEmpty())
            {
                return datatable.Rows.Add();
            }

            return datatable.Rows[0];
        }

        public static bool IsEmpty(this DataRowCollection datarowcollection)
        {
            if (datarowcollection == null)
            {
                return true;
            }

            return datarowcollection.Count == 0;
        }
    }
}