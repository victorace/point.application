﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Point.Infrastructure.Extensions
{
    public static class LINQExtensions
    {
        public static T FirstOrNew<T>(this IEnumerable<T> e, Func<T, bool> where = null) where T : new()
        {
            var r = (where == null ? e.FirstOrDefault<T>() : e.FirstOrDefault<T>(where));
            return (r == null) ? new T() : r;
        }

        public static T LastOrNew<T>(this IEnumerable<T> e, Func<T, bool> where = null) where T : new()
        {
            var r = (where == null ? e.LastOrDefault<T>() : e.LastOrDefault<T>(where));
            return (r == null) ? new T() : r;
        }

        public static IEnumerable<T> OrderBySequence<T, TId>(this IEnumerable<T> source, IEnumerable<TId> order, Func<T, TId> selector)
        {
            var lookup = source.ToLookup(selector, t => t);
            foreach (var id in order)
            {
                foreach (var t in lookup[id])
                {
                    yield return t;
                }
            }
        }

        public static IOrderedEnumerable<TSource> OrderByDirection<TSource, TKey>
            (this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, bool descending = false) 
        {
            return descending ? source.OrderByDescending(keySelector)
                              : source.OrderBy(keySelector);
        }

        public static IOrderedQueryable<TSource> OrderByDirection<TSource, TKey>
            (this IQueryable<TSource> source, Expression<Func<TSource, TKey>> keySelector, bool descending = false)
        {
            return descending ? source.OrderByDescending(keySelector)
                              : source.OrderBy(keySelector);
        }

        public static IEnumerable<TSource> Except<TSource>(this IEnumerable<TSource> first, IEnumerable<TSource> second, Func<TSource, TSource, bool> comparer)
        {
            return first.Where(x => second.Count(y => comparer(x, y)) == 0);
        }

        public static IEnumerable<TSource> Intersect<TSource>(this IEnumerable<TSource> first, IEnumerable<TSource> second, Func<TSource, TSource, bool> comparer)
        {
            return first.Where(x => second.Count(y => comparer(x, y)) == 1);
        }

        public static IEnumerable<U> GetDuplicates<T, U>(this IEnumerable<T> list, Func<T, U> selector)
        {
            return list.GroupBy(selector)
                    .Where(group => group.Count() > 1)
                    .Select(group => group.Key);
        }
    }
}
