﻿using System;
using System.Linq;
using System.Reflection;

namespace Point.Infrastructure.Extensions
{
    public static class ObjectExtensions
    {
        public static void SafeDispose(this object instance)
        {
            if (instance != null)
            {
                var disposable = instance as IDisposable;

                if (disposable != null)
                {
                    disposable.Dispose();
                }
            }
        }

        public static PropertyInfo[] GetFilteredProperties(this Type type, Type skiptype)
        {
            return type.GetProperties().Where(pi => pi.GetCustomAttributes(skiptype, true).Length == 0).ToArray();
        }
    }
}
