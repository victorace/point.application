﻿using System;
using System.Globalization;
using System.Linq;
using System.Reflection;

namespace Point.Infrastructure.Extensions
{
    public static class TypeExtensions
    {
        public static void Merge<TTarget>(this TTarget copyTo, object copyFrom) where TTarget : new()
        {
            if (copyTo == null || object.Equals(copyTo, default(TTarget)))
            {
                copyTo = new TTarget();
            }

            if (copyFrom != null)
            {
                var flags = BindingFlags.Instance | BindingFlags.Public;

                var targetDicF = copyTo.GetType().GetFields(flags).ToDictionary(f => f.Name);
                foreach (FieldInfo f in copyFrom.GetType().GetFields(flags).Concat(copyFrom.GetType().BaseType.GetFields(flags)))
                {
                    if (targetDicF.ContainsKey(f.Name))
                    {
                        targetDicF[f.Name].SetValue(copyTo, f.GetValue(copyFrom));
                    }
                }

                var targetDicP = copyTo.GetType().GetProperties(flags).ToDictionary(f => f.Name);
                foreach (PropertyInfo p in copyFrom.GetType().GetProperties(flags))
                {
                    if (targetDicP.ContainsKey(p.Name) && targetDicP[p.Name].PropertyType == p.PropertyType && p.CanWrite)
                    {
                        targetDicP[p.Name].SetValue(copyTo, p.GetValue(copyFrom));
                    }
                }
            }
        }

        public static bool IsNewed<T>(this T orig) where T : new()
        {
            var newt = new T();

            Type type = typeof(T);

            if (orig != null && newt != null)
            {

                foreach (PropertyInfo pi in type.GetProperties(BindingFlags.Public | BindingFlags.Instance))
                {
                    object origValue = type.GetProperty(pi.Name).GetValue(orig, null);
                    object newValue = type.GetProperty(pi.Name).GetValue(newt, null);

                    if (origValue != newValue && (origValue == null || !origValue.Equals(newValue)))
                    {
                        return false;
                    }
                }
                return true;
            }
            return false;
        }

        public static T ConvertTo<T>(this object value, Type type)
        {
            if (value == null || value is DBNull || (value is string && string.IsNullOrEmpty((string)value)))
            {
                return default(T);
            }

            //sourcetype
            var st = value.GetType();
            if (st.IsGenericType && st.GetGenericTypeDefinition() == typeof(Nullable<>))
            {
                st = Nullable.GetUnderlyingType(st) ?? st;
            }

            //targettype
            var t = type;
            if (t.IsGenericType && t.GetGenericTypeDefinition() == typeof(Nullable<>))
            {
                t = Nullable.GetUnderlyingType(t) ?? t;
            }

            //handling of datetime to string
            if (t == typeof(string) && st == typeof(DateTime))
            {
                var datetimevalue = (DateTime?)value;
                if (datetimevalue.HasValue && datetimevalue != default(DateTime))
                {
                    if (datetimevalue.Value.TimeOfDay.Milliseconds > 0)
                    {
                        return (T)(object)datetimevalue.Value.ToString("yyyy-MM-dd HH:mm:ss");
                    }
                    else
                    {
                        return (T)(object)datetimevalue.Value.ToString("yyyy-MM-dd");
                    }
                }

                return default(T);
            }

            //handling of string to enum
            if (t.IsEnum && value is string)
            {
                return (T)Enum.Parse(t, value as string, true);
            }

            //handling of numbers to enum
            if (t.IsEnum && value.GetType().IsPrimitive &&
                !(value is bool) && !(value is char) &&
                !(value is float) && !(value is double))
            {
                return (T)Enum.ToObject(t, value);
            }

            return (T)Convert.ChangeType(value, t, CultureInfo.InvariantCulture);
        }
        public static T ConvertTo<T>(this object value)
        {
            return value.ConvertTo<T>(typeof(T));
        }
    }
}
