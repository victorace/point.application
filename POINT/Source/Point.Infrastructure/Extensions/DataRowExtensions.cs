﻿using System.Data;

namespace Point.Infrastructure.Extensions
{
    public enum DataRowExtensionFieldType
    {
        Bool,
        Char,
        Date,       // dd-MM-yyyy
        FullDate,   // dddd, dd MMMM yyyy
        DropDown
    }

    public static class DataRowExtensions
    {

        public static void SetColumnValue(this DataRow datarow, string columnname, string value)
        {
            if (datarow != null && datarow.Table.Columns.Contains(columnname))
            {
                datarow[columnname] = value;
            }
        }

    }
}
