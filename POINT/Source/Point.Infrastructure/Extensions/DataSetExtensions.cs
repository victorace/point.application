﻿using System.Data;

namespace Point.Infrastructure.Extensions
{
    public static class DataSetExtensions
    {
        public static void SafeAddTable(this DataSet dataSet, DataTable dataTable)
        {
            if (dataSet == null || dataSet.Tables == null || dataTable == null )
            {
                return;
            }

            dataSet.Tables.Add(dataTable);
        }
    }
}
