﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml;

namespace Point.Infrastructure.Extensions
{
    public static class HttpRequestBaseExtensions
    {
        public static string GetParam(this HttpRequestBase httpRequest, string keyName)
        {
            var querystringkeys = httpRequest.Unvalidated.QueryString.AllKeys;
            IDictionary<string, string> querystringvalues = querystringkeys.ToDictionary(k => k.ToLowerInvariant(), k => httpRequest.Unvalidated.QueryString[k]);

            var formkeys = httpRequest.Unvalidated.Form.AllKeys.ToArray();
            IDictionary<string, string> formvalues = formkeys.ToDictionary(k => k.ToLowerInvariant(), k => httpRequest.Unvalidated.Form[k]);

            var allvalues = formvalues.Concat(querystringvalues.Where(it => formvalues.All(qsv => qsv.Key != it.Key)))
                    .ToDictionary(pair => pair.Key, pair => pair.Value);
                
            if (allvalues.ContainsKey(keyName.ToLowerInvariant()))
            {
                return allvalues[keyName.ToLowerInvariant()];
            }

            allvalues = httpRequest.RequestContext.RouteData.Values.ToDictionary(v => v.Key.ToLowerInvariant(), v => v.Value.ToString());

            return allvalues.ContainsKey(keyName.ToLowerInvariant()) 
                ? allvalues[keyName.ToLowerInvariant()] 
                : null;
        }

        public static bool IsAjaxRequest(this HttpRequest httpRequest)
        {
            return httpRequest.Headers["X-Requested-With"] == "XMLHttpRequest";
        }

        public static bool IsPrintRequest(this HttpRequestBase httpRequestBase)
        {
            var printmode = httpRequestBase.GetParam("PrintMode");
            if (String.IsNullOrEmpty(printmode))
            {
                return false;
            }

            return Convert.ToBoolean(printmode);
        }

        public static bool IsMailRequest(this HttpRequestBase httpRequestBase)
        {
            var mailmode = httpRequestBase.GetParam("MailMode");
            if (String.IsNullOrEmpty(mailmode))
            {
                return false;
            }

            return Convert.ToBoolean(mailmode);
        }

        public static bool IsManagementArea(this HttpRequestBase httpRequestBase)
        {
            var kvp = httpRequestBase.RequestContext.RouteData.DataTokens["area"];
            if (kvp != null && kvp.ToString().Equals("management", StringComparison.InvariantCultureIgnoreCase))
            {
                return true;
            }
            
            return false;
        }

        public static T GetRequestVariable<T>(this HttpRequestBase httpRequestBase, string name, T defaultValue = default(T))
        {
            if (httpRequestBase != null)
            {
                string paramValue = httpRequestBase.GetParam(name);
                if (paramValue == null)
                {
                    return defaultValue;
                }

                if (paramValue != null && paramValue.Contains(',')) //param might be passed twice to the controller
                {
                    paramValue = paramValue.Split(',')[0];
                }

                return paramValue.ConvertTo<T>();
            }

            return defaultValue;
        }

        public static T GetRequestVariable<T>(this HttpRequest httpRequest, string name, T defaultValue = default(T))
        {
            var httpRequestBase = new HttpRequestWrapper(httpRequest);
            return httpRequestBase.GetRequestVariable<T>(name, defaultValue);
        }

        public static XmlDocument GetXmlRequest(this HttpRequest httprequest)
        {
            if (httprequest == null) return null;

            XmlDocument xmlSoapRequest = null;
            try
            {
                xmlSoapRequest = new XmlDocument();
                Stream receiveStream = httprequest.InputStream;
                receiveStream.Position = 0; // Move to begining of input stream and read
                using (StreamReader readStream = new StreamReader(receiveStream, System.Text.Encoding.UTF8))
                {
                    xmlSoapRequest.Load(readStream);
                }
            }
            catch (Exception exception)
            {
                ExceptionHelper.SendMessageException(exception, uri: httprequest?.Url);
            }

            return xmlSoapRequest;
        }
    }
}