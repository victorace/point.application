﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace Point.Infrastructure.Extensions
{
    public static class DatetimeExtensions
    {
        public static readonly List<string> Formats = new List<string>()
        {
            "yyyyMMddHHmmss", // HL7 FieldReceivedTemp format
            "dd-MM-yyyy HH:mm:ss",
            "yyyy-MM-dd HH:mm:ss.fff",
            "yyyy-MM-dd HH:mm:ss",
            "dd-MM-yyyy",
            "yyyy-MM-dd",
            "yyyy-MM-dd HH:mm"
        };

        public static string ToStringEx(this DateTime? dt, string format)
        {
            if (dt == null || !dt.HasValue)
                return "";

            return dt.Value.ToString(format);
        }

        public static string ToPointLongDisplay(this DateTime dt)
        {
            return ((DateTime?)dt).ToPointLongDisplay();
        }

        public static string ToPointLongDisplay(this DateTime? dt)
        {
            if (dt.IsOutOfScope()) return "";
            return dt.ToStringEx("ddd dd-MM HH:mm");
        }

        public static string ToPointDayMonthTime(this DateTime dt)
        {
            return ((DateTime?)dt).ToPointDayMonthTime(); 
        }

        public static string ToPointDayMonthTime(this DateTime? dt)
        {
            if (dt.IsOutOfScope()) return "";
            return dt.ToStringEx("dd-MM HH:mm");
        }

        public static string ToPointDayMonth(this DateTime dt)
        {
            return ((DateTime?)dt).ToPointDayMonth();
        }

        public static string ToPointDayMonth(this DateTime? dt)
        {
            if (dt.IsOutOfScope()) return "";
            return dt.ToStringEx("dd-MM");
        }

        public static string ToPointDateDisplay(this DateTime dt)
        {
            return ((DateTime?)dt).ToPointDateDisplay();
        }

        public static string ToPointDateDisplay(this DateTime? dt)
        {
            if (dt.IsOutOfScope()) return "";
            return dt.ToStringEx("dd-MM-yyyy");
        }

        public static string ToPointDateHourDisplay(this DateTime dt)
        {
            return ((DateTime?)dt).ToPointDateHourDisplay();
        }

        public static string ToPointDateHourDisplay(this DateTime? dt)
        {
            if (dt.IsOutOfScope()) return "";
            return dt.ToStringEx("d-M-yyyy HH:mm");
        }

        public static string ToSortablePointDateHourDisplay(this DateTime? dt)
        {
            if (dt.IsOutOfScope()) return "";
            return dt.ToStringEx("yyyy-MM-dd HH:mm");
        }

        public static string ToPointTimeDisplay(this DateTime? dt)
        {
            return dt.ToStringEx("HH:mm");
        }

        public static string ToPointTimeDisplay(this DateTime dt)
        {
            return ((DateTime?)dt).ToStringEx("HH:mm");
        }

        public static string ToPointDateTimeDatabase(this DateTime? dt)
        {
            if (dt.IsOutOfScope()) return "";
            return dt.ToStringEx("yyyy-MM-dd HH:mm:ss.fff");
        }

        public static string ToPointDateTimeDatabase(this DateTime dt)
        {
            return ((DateTime?)dt).ToPointDateTimeDatabase();
        }

        public static DateTime? FromPointDateTimeDatabase(this string s)
        {
            if (String.IsNullOrEmpty(s)) return null;
            DateTime dt = DateTime.MinValue;
            if (!DateTime.TryParseExact(s, "yyyy-MM-dd HH:mm:ss.fff", Thread.CurrentThread.CurrentCulture, System.Globalization.DateTimeStyles.None, out dt))
            {
                dt = DateTime.MinValue;
            }
            return dt;
        }

        public static string ToPointLeterDate(this DateTime dt)
        {
            return ((DateTime?)dt).ToPointLeterDate();
        }

        public static string ToPointLeterDate(this DateTime? dt)
        {
            if (dt.IsOutOfScope()) return "";
            return dt.ToStringEx("dddd, dd MMMM yyyy");
        }

        public static string ToXMLDateTime(this DateTime? dt)
        {
            if (dt == null)
            {
                return "";
            }
            else
            {
                return ((DateTime)dt).ToXMLDateTime();
            }
        }

        public static string ToXMLDateTime(this DateTime dt)
        {
            return dt.ToString("o");
        }

        public static string ToNumericDateTime(this DateTime dt)
        {
            return dt.ToString("yyyyMMddHHmm");
        }

        public static string ToStringOrDefault(this DateTime? dt, string format)
        {
            return ToStringOrDefault(dt, format, null);
        }

        public static string ToStringOrDefault(this DateTime dt, string format)
        {
            return ((DateTime?)dt).ToStringOrDefault(format);
        }
        public static string ToStringOrDefault(this DateTime dt, string format, string defaultValue)
        {
            return ((DateTime?)dt).ToStringOrDefault(format, defaultValue);
        }
        public static string ToStringOrDefault(this DateTime? dt, string format, string defaultValue)
        {
            if (dt != null)
            {
                return dt.Value.ToString(format);
            }
            else
            {
                return String.IsNullOrEmpty(defaultValue) ? String.Empty : defaultValue;
            }
        }

        public static string ToSortable(this DateTime? dt)
        {
            if (dt == null || !dt.HasValue)
                dt = new DateTime(1799, 1, 1, 0, 0, 0);

            return dt.Value.ToString("yyyyMMddHHmmss");
        }

        public static string ToSortableDate(this DateTime? dt)
        {
            if (!dt.HasValue)
                return null;

            return dt.Value.ToString("yyyy-MM-dd");
        }

        public static bool IsToday(this DateTime? dt)
        {
            if (dt == null || !dt.HasValue)
                return false;

            var dtnow = DateTime.Now;
            var dtcomp = dt.GetValueOrDefault();
            return (dtnow.Year == dtcomp.Year && dtnow.Month == dtcomp.Month && dtnow.Day == dtcomp.Day);
        }

        public static bool IsTomorrow(this DateTime? dt)
        {
            if (dt == null || !dt.HasValue)
                return false;

            var dtnow = DateTime.Now.AddDays(1);
            var dtcomp = dt.GetValueOrDefault();
            return (dtnow.Year == dtcomp.Year && dtnow.Month == dtcomp.Month && dtnow.Day == dtcomp.Day);

        }

        public static bool IsYesterday(this DateTime? dt)
        {
            if (dt == null || !dt.HasValue)
                return false;

            var dtnow = DateTime.Now.AddDays(-1);
            var dtcomp = dt.GetValueOrDefault();
            return (dtnow.Year == dtcomp.Year && dtnow.Month == dtcomp.Month && dtnow.Day == dtcomp.Day);

        }

        public static bool IsOutOfScope(this DateTime? dt)
        {
            if (dt == null || !dt.HasValue) return true;

            var dtscope = dt.GetValueOrDefault();
            return (dtscope.Year < 1900 || dtscope.Year > 2100);
        }

        public static DateTime SqlMinValue(this DateTime dt)
        {
            return new DateTime(1753, 1, 1);
        }

        public static DateTime SqlMaxValue(this DateTime dt)
        {
            return new DateTime(9999, 12, 31, 23, 59, 59, 997);
        }

        public static int GetAge(this DateTime? birthDate)
        {
            if (!birthDate.HasValue)
                return 0;

            var now = DateTime.Now;
            int age = now.Year - birthDate.Value.Year;

            if (now.Month < birthDate.Value.Month || (now.Month == birthDate.Value.Month && now.Day < birthDate.Value.Day))
                age--;

            return age;
        }
    }
}
