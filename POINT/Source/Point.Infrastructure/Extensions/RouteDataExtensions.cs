﻿using System;
using System.Web.Routing;

namespace Point.Infrastructure.Extensions
{
    public static class RouteDataExtensions
    {
        public static T GetValue<T>(this RouteData routeData, string key, object defaultValue)
        {
            if (routeData != null)
            {
                if (routeData.Values.ContainsKey(key))
                {
                    try
                    {
                        return routeData.Values[key].ConvertTo<T>();
                    }
                    catch { /* return default value */ }
                }
            }

            return defaultValue.ConvertTo<T>(); ;
        }
    }
}
