﻿using System.ComponentModel;
using System.Reflection;

namespace Point.Infrastructure.Extensions
{
    public static class PropertyExtensions
    {
        public static string GetDisplayName(this PropertyInfo property)
        {
            var attribute = property.GetCustomAttribute<DisplayNameAttribute>(true);
            if(attribute != null)
            {
                return attribute.DisplayName;
            }
            return "";
        }

    }
}
