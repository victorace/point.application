﻿using System;
using System.Linq;
using System.Text;
using System.Xml;

namespace Point.Infrastructure.Extensions
{

    public static class XmlExtensions
    {

        /// <summary>
        /// Removes all attributes specified in the filter.
        /// </summary>
        /// <param name="xml">XmlDocument as a string (innerxml)</param>
        /// <param name="filter">Use % as a wildcard, 
        /// xyz% = all starting with xyz,
        /// %xyz = all ending with xyz,
        /// xyz = exactly xyz</param>
        /// <returns>XmlDocument</returns>
        public static XmlDocument FilterTags(this string xml, string[] filter)
        {
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(xml);

            recurseXmlDocument(xmldoc.FirstChild, filter);

            return xmldoc;
        }

        public static XmlDocument RemoveEmptyNodes(this XmlDocument xmldoc)
        {
            XmlNodeList emptyElements = xmldoc.SelectNodes(@"//*[not(node())]");
            if (emptyElements.Count == 0)
            {
                return xmldoc;
            }
            else
            {
                for (int i = emptyElements.Count - 1; i >= 0; i--)
                {
                    emptyElements[i].ParentNode.RemoveChild(emptyElements[i]);
                }
                return xmldoc.RemoveEmptyNodes();
            }
        }

        private static void recurseXmlDocument(XmlNode node, string[] filter)
        {
            if (node is XmlElement)
            {
                for (int i = 0; i < node.Attributes.Count; i++)
                {
                    XmlAttribute attr = node.Attributes[i];
                    if (filter.Any(f => (f.EndsWith("%") && attr.Name.StartsWith(f.Substring(0, f.Length - 1))) ||
                                        (f.StartsWith("%") && attr.Name.EndsWith(f.Substring(1))) ||
                                        attr.Name == f))
                        node.Attributes.Remove(attr);

                }
            }

            if (node.NextSibling != null)
                recurseXmlDocument(node.NextSibling, filter);

            for (int i = 0; i < node.ChildNodes.Count; i++)
                recurseXmlDocument(node.ChildNodes[i], filter);
        }

        public static XmlDocument StringToXmlDocument(string xml)
        {
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(xml);

            return xmldoc;
        }

        public static string IndentXml(this XmlDocument doc)
        {
            StringBuilder sb = new StringBuilder();
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.IndentChars = "  ";
            settings.NewLineChars = Environment.NewLine;
            settings.NewLineHandling = NewLineHandling.Replace;
            using (XmlWriter writer = XmlWriter.Create(sb, settings))
            {
                doc.Save(writer);
            }
            return sb.ToString();
        }


    }
}
