﻿using Point.Infrastructure.Exceptions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml;

namespace Point.Infrastructure.Extensions
{
    public static class HttpResponseBaseExtensions
    {
        public static void DisableClientCache(this HttpResponseBase httpResponseBase)
        {
            httpResponseBase.AddHeader("Cache-Control", "private, max-age=0, no-cache, no-store, must-revalidate");
            httpResponseBase.AddHeader("Expires", "-1");
        }
    }
}
