﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace Point.Infrastructure.Extensions
{
    public static class EnumExtensions
    {
        public static string GetDescription(this Enum value)
        {
            if(value == null)
            {
                return null;
            }

            return
                value
                    .GetType()
                    .GetMember(value.ToString())
                    .FirstOrDefault()
                    ?.GetCustomAttribute<DescriptionAttribute>()
                    ?.Description
                ?? value.ToString();
        }

        public static TEnum SafeParse<TEnum>(this string instance) where TEnum : struct
        {
            TEnum res;

            if (Enum.TryParse<TEnum>(instance, out res))
            {
                return res;
            }

            return default(TEnum);
        }

        public static TEnum SafeParse<TEnum>(this int instance) where TEnum : struct
        {
            if (Enum.IsDefined(typeof(TEnum), instance))
            {
                TEnum res;

                Enum.TryParse<TEnum>(instance.ToString(), out res);

                return res;
            }

            return default(TEnum);
        }
    }
}
