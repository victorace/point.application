﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web.Mvc;

namespace Point.Infrastructure.Extensions
{
    public static class EnumerableExtensions
    {
        public static IEnumerable<IEnumerable<T>> Batch<T>(this IEnumerable<T> source, int size)
        {
            T[] bucket = null;
            var count = 0;

            foreach (var item in source)
            {
                if (bucket == null)
                    bucket = new T[size];

                bucket[count++] = item;

                if (count != size)
                    continue;

                yield return bucket.Select(x => x);

                bucket = null;
                count = 0;
            }

            // Return the last bucket with all remaining elements
            if (bucket != null && count > 0)
                yield return bucket.Take(count);
        }

        public static bool IsNullOrEmpty<T>(this IEnumerable<T> list)
        {
            if (list == null)
                return true;
            return (list.Count() == 0);
        }

        public static T PickRandomOne<T>(this IEnumerable<T> list)
        {
            System.Security.Cryptography.RNGCryptoServiceProvider generator = new RNGCryptoServiceProvider();
            byte[] data = new byte[4];
            generator.GetBytes(data);

            Random rnd = new Random(BitConverter.ToInt32(data, 0));


            T picked = default(T);
            int cnt = 0;
            foreach (T item in list)
            {
                if (rnd.Next(++cnt) == 0)
                {
                    picked = item;
                }
            }
            return picked;
        }

        public static IEnumerable<T> TakeLast<T>(this IEnumerable<T> collection, int n)
        {
            if (collection == null)
                throw new ArgumentNullException("collection");
            if (n < 0)
                throw new ArgumentOutOfRangeException("n", "n must be 0 or greater");

            LinkedList<T> temp = new LinkedList<T>();
            foreach (var value in collection)
            {
                temp.AddLast(value);
                if (temp.Count > n)
                    temp.RemoveFirst();
            }

            return temp;
        }

        public static IEnumerable<SelectListItem> ToSelectListItems<T>(this IEnumerable<T> items, Func<T, string> nameSelector, Func<T, string> valueSelector, Func<T, bool> selectedSelector = null, string firstItem = null, string firstItemValue = "", bool doNotTruncateOnSingleOption = false)
        {
            if (items == null) return Enumerable.Empty<SelectListItem>();

            var listitems = items  //.OrderBy(item => nameSelector(item))
                   .Select(item =>
                           new SelectListItem
                           {
                               Selected = (selectedSelector == null ? false : selectedSelector(item)),
                               Text = nameSelector(item),
                               Value = valueSelector(item)
                           });



            if ((!String.IsNullOrWhiteSpace(firstItem) && listitems.Count() > 1) || doNotTruncateOnSingleOption)
            {
                listitems = (new[] { new SelectListItem { Selected = listitems.Where(li => li.Selected).Count() == 0, Text = firstItem, Value = firstItemValue } }).Concat(listitems);
            }

            return listitems;
        }
    }
}
