﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web.Routing;

namespace Point.Infrastructure.Extensions
{
    public static class CollectionExtensions
    {
        public static void AddRange<T>(this ICollection<T> instance, IEnumerable<T> enumerable)
        {
            foreach (var cur in enumerable)
                instance.Add(cur);
        }

        public static RouteValueDictionary ToRouteValueDictionary(this NameValueCollection nv)
        {
            if (nv == null)
                throw new ArgumentNullException();

            RouteValueDictionary rv = new RouteValueDictionary();
            foreach (string key in nv.AllKeys)
                rv.Add(key, nv[key]);

            return rv;
        }

        public static RouteValueDictionary ToRouteValueDictionary(this Dictionary<string, string> dict)
        {
            if (dict == null)
                throw new ArgumentNullException();

            RouteValueDictionary rv = new RouteValueDictionary();
            foreach (string key in dict.Keys)
                rv.Add(key, dict[key]);

            return rv;
        }

        public static T GetValueOrDefault<T>(this NameValueCollection collection, string key, object defaultValue)
        {
            if (collection.Get(key) != null || collection.Keys.Cast<string>().Contains(key, StringComparer.InvariantCulture))
            {
                try
                {
                    return collection.Get(key).ConvertTo<T>();
                }
                catch { /* return default value */ }
            }

            return defaultValue.ConvertTo<T>();
        }

        public static bool ContainsKey(this NameValueCollection collection, string key)
        {
            if (collection.Get(key) == null)
            {
                return collection.AllKeys.Contains(key);
            }

            return true;
        }
    }
}
