﻿namespace Point.Infrastructure.Exceptions
{
    public class ErrorViewModel
    {
        public ExceptionType ExceptionType { get; set; }
        public int ErrorNumber { get; set; }
    }
}
