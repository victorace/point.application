﻿using System;

namespace Point.Infrastructure.Exceptions
{
    public class PointJustLogException : ApplicationException
    {
        public PointJustLogException() { }
        public PointJustLogException(string message) : base(message) { }
        public PointJustLogException(string message, Exception inner) : base(message, inner) { }
    }
}
