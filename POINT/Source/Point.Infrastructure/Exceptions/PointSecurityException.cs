﻿using System;

namespace Point.Infrastructure.Exceptions
{
    public enum ExceptionType
    {
        Default = 0,
        PageNotFound = 1,
        ChangePassword = 2,
        AccessDeniedVVT = 3,
        AccessDeniedDifferentVVT = 4,
        AccessDenied = 5,
        NoRole = 6,
        DossierDeleted = 7
    }

    public class PointSecurityException : PointJustLogException
    {
        public ExceptionType ExceptionType { get; set; }
        public PointSecurityException(ExceptionType type) : base() { this.ExceptionType = type; }

        public PointSecurityException(string message, ExceptionType type) : base(message) { this.ExceptionType = type; }

        public PointSecurityException(string message, Exception inner, ExceptionType type) : base(message, inner) { this.ExceptionType = type; }
    }
}
