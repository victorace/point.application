﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Point.Infrastructure.Exceptions
{
    public class PointDossierDeletedException : PointJustLogException
    {
        public PointDossierDeletedException(string message) : base(message) { }
    }
}
