﻿using System;
using System.Web.Mvc;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.MVC.Shared.Constants;
using Point.MVC.Shared.ViewModels.Capacity;

namespace Point.MVC.Shared.Models.Capacity
{
    public class CapacityParameters
    {
        public Controller Controller { get; set; }
        public IUnitOfWork Uow { get; set; }
        public bool ShowByRegion { get; set; }
        public PointUserInfo PointUserInfo { get; set; }
        public bool WithCapacityOnly { get; set; }
        //public int[] RegionIDs { get; set; }
        public RegionIdsSelection RegionIdsSelection { get; set; }

        public int? AfterCareTileGroupID { get; set; }
        public int? AfterCareGroupID { get; set; }
        public int? AfterCareTypeID { get; set; }
        public int? OrganizationTypeID { get; set; }

        public bool IsSpecificSearch { get; set; }

        public CapacityParametersNonSpecificSearch SearchNonSpecificParameters { get; set; } = new CapacityParametersNonSpecificSearch();

        public int PageNo { get; set; }
        public bool FillLocationCoordinates { get; set; }

        public bool IsCapacityPublicVersion { get; set; }

        public bool IsHospitalSearch { get; set; }
        public string GetResultsUrl { get; set; }
        public string GetAfterCareGroupsAndTypesUrl { get; set; }
        public string CurrentAction { get; set; } = TabMenuItems.Other;
        public bool ShowMutadedByUserFields { get; set; }

        public int? OrganizationID { get; set; }
        public int? AfterCareCategoryID { get; set; }
        public int[] RegionIDsFilter { get; set; } = new int[0];

        public string PathViews { get; set; }

    }

    public class CapacityParametersNonSpecificSearch
    {
        public string OrgLocDepName { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        public int? PostalCodeRadius { get; set; }
        public bool HasSearchParameters => !string.IsNullOrEmpty(OrgLocDepName) || !string.IsNullOrEmpty(City) || (!string.IsNullOrEmpty(PostalCode) && PostalCodeRadius != null);
    }


}
