﻿
namespace Point.MVC.Shared.Constants
{
    public static class CapacityManagementConstants
    {
        public static int RegionIdForAllSelected = 0;
        public static int RegionIdForNoneSelected = -1;
        public static int AfterCareGroupOrTypeIdNone = 0;
    }

    public static class TabMenuItems
    {
        public const string EditView = "Edit";
        public const string ListView = "List";
        public const string MapView = "Map";
        public const string TileView = "Tile";
        public const string Other = "Other";
    }
}