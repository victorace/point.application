﻿using System.Web.Mvc;
using Point.Models.Enums;

namespace Point.MVC.Shared.Attributes
{
    public class FunctionRoles : ActionFilterAttribute
    {
        private readonly FunctionRoleTypeID[] functionRoleTypeIDs;
        private readonly FunctionRoleLevelID? minimumFunctionRoleLevelID;
        private readonly string exceptionmessage = "";

        public FunctionRoles(params FunctionRoleTypeID[] functionRoleTypeIDs)
        {
            this.functionRoleTypeIDs = functionRoleTypeIDs;
        }

        public FunctionRoles(string exceptionmessage, params FunctionRoleTypeID[] functionRoleTypeIDs)
        {
            this.exceptionmessage = exceptionmessage;
            this.functionRoleTypeIDs = functionRoleTypeIDs;
        }

        public FunctionRoles(string exceptionmessage, FunctionRoleLevelID minimumFunctionRoleLevelID, params FunctionRoleTypeID[] functionRoleTypeIDs)
        {
            this.exceptionmessage = exceptionmessage;
            this.minimumFunctionRoleLevelID = minimumFunctionRoleLevelID;
            this.functionRoleTypeIDs = functionRoleTypeIDs;
        }

        public FunctionRoles(FunctionRoleLevelID minimumFunctionRoleLevelID, params FunctionRoleTypeID[] functionRoleTypeIDs)
        {
            this.minimumFunctionRoleLevelID = minimumFunctionRoleLevelID;
            this.functionRoleTypeIDs = functionRoleTypeIDs;
        }

        //public override void OnActionExecuting(ActionExecutingContext filterContext)
        //{
        //    var controller = (PointController)filterContext.Controller;
        //    if (controller == null)
        //        throw new PointSecurityException("Deze functie word alleen ondersteund op PointControllers", ExceptionType.Default);

        //    bool allowed;
        //    var userinfo = controller.PointUserInfo();

        //    allowed = userinfo.Employee.FunctionRoles.Any(fr => functionRoleTypeIDs.Contains(fr.FunctionRoleTypeID) && (minimumFunctionRoleLevelID == null || fr.FunctionRoleLevelID >= minimumFunctionRoleLevelID));

        //    if (!allowed)
        //        throw new PointSecurityException(exceptionmessage == "" ? "Deze handeling is niet toegestaan" : exceptionmessage, ExceptionType.AccessDenied);
        //}
    }
}