﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Ajax.Utilities;
using Point.Business.Logic;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.MVC.Shared.Constants;
using Point.MVC.Shared.Models.Capacity;

namespace Point.MVC.Shared.Logic
{
    public class AfterCareViewBL
    {
        private static List<AfterCareFinancingViewModel> _afterCareTileGroups;
        private static List<AfterCareGroupViewModel> _afterCareGroups;
        private static List<AfterCareTypeViewModel> _afterCareTypes;

        public static async Task<List<AfterCareFinancingViewModel>> GetAfterCareTileGroups(IUnitOfWork uow)
        {
            if (_afterCareTileGroups != null && _afterCareTileGroups.Any()) return _afterCareTileGroups;

            _afterCareTileGroups = await uow.AfterCareTileGroupRepository.Get().Select(tg => new AfterCareFinancingViewModel
            {
                AfterCareTileGroupID = tg.AfterCareTileGroupID,
                Name = tg.Name,
                Sortorder = tg.Sortorder,
                ColorCode = tg.ColorCode
            }).ToListAsync();

            return _afterCareTileGroups;
        }

        public static async Task<List<AfterCareGroupViewModel>> GetAfterCareGroups(IUnitOfWork uow)
        {
            if (_afterCareGroups != null && _afterCareGroups.Any()) return _afterCareGroups;

            _afterCareGroups = await uow.AfterCareGroupRepository.Get().OrderBy(x => x.Name).Select(g => new AfterCareGroupViewModel
            {
                AfterCareTileGroupID = g.AfterCareTileGroupID,
                AfterCareGroupID = g.AfterCareGroupID,
                Name = g.Name,
                Sortorder = g.Sortorder
            }).ToListAsync();

            return _afterCareGroups;
        }

        public static async Task<List<AfterCareTypeViewModel>> GetAfterCareTypes(CapacityParameters parameters)
        {
            if (_afterCareTypes == null || !_afterCareTypes.Any())
            {
                _afterCareTypes = await parameters.Uow.AfterCareTypeRepository.Get(x => !x.Inactive).OrderBy(x => x.AfterCareGroupID).ThenBy(x => x.Name).Select(t => new AfterCareTypeViewModel
                {
                    AfterCareTypeID = t.AfterCareTypeID,
                    AfterCareGroupID = t.AfterCareGroupID,
                    Name = t.Name,
                    Sortorder = t.Sortorder,
                    OrganizationTypeID = t.OrganizationTypeID
                }).ToListAsync();
            }
            
            if (!parameters.IsCapacityPublicVersion)
            {
                return _afterCareTypes;
            }

            // Public version only!
            var list = new List<AfterCareTypeViewModel>();
           
            if (parameters.CurrentAction != TabMenuItems.TileView && !parameters.RegionIdsSelection.HasRegionsSelected)
            {
                // TileView displays grayed-out tiles which are not applicable for selected regions
                return list;
            }

            var hasRegionFilter = parameters.RegionIdsSelection.NeedsRegionCheckOnQuery;
            var regionAfterCareTypes = RegionAfterCareTypeBL.GetAll(parameters.Uow);

            if (parameters.CurrentAction == TabMenuItems.TileView)
            {
                _afterCareTypes.ForEach(x => list.Add(new AfterCareTypeViewModel
                {
                    AfterCareTypeID = x.AfterCareTypeID,
                    AfterCareGroupID = x.AfterCareGroupID,
                    Description = x.Description,
                    Name = x.Name,
                    RegionID = x.RegionID,
                    Capacity = x.Capacity,
                    Sortorder = x.Sortorder,
                    OrganizationTypeID = x.OrganizationTypeID,
                    IsActiveInSelectedRegions = regionAfterCareTypes.Any(reg => reg.AfterCareTypeID == x.AfterCareTypeID && (!hasRegionFilter || parameters.RegionIdsSelection.SelectedRegionIds.Contains(reg.RegionID)))
                }));
            }
            else
            {
                regionAfterCareTypes = (List<RegionAfterCareType>) RegionAfterCareTypeBL.GetAllForRegionIDs(parameters.Uow, parameters.RegionIdsSelection.SelectedRegionIds); //   Where(x => parameters.RegionIdsSelection.SelectedRegionIds.Contains(x.RegionID)).ToList();

                _afterCareTypes.Where(act => !hasRegionFilter || regionAfterCareTypes.Any(reg => act.AfterCareTypeID == reg.AfterCareTypeID))
                    .ForEach(x => list.Add(new AfterCareTypeViewModel
                {
                    AfterCareTypeID = x.AfterCareTypeID,
                    AfterCareGroupID = x.AfterCareGroupID,
                    Description = x.Description,
                    Name = x.Name,
                    RegionID = x.RegionID,
                    Capacity = x.Capacity,
                    Sortorder = x.Sortorder,
                    OrganizationTypeID = x.OrganizationTypeID,
                    IsActiveInSelectedRegions = true        // Always a region selection
                }));
            }

            return list;
        }
        
        public static async Task<List<AfterCareTypeViewModel>> GetAfterCareTypeDescriptionsByCategoryIDs(IUnitOfWork uow, int[] categoryIDs)
        {
            return await AfterCareTypeBL.GetAfterCareTypeDescriptionsByCategoryIDs(uow, categoryIDs).Select(x => new AfterCareTypeViewModel
            {
                Name = x.Name,
                Description = x.Description ?? x.Name
            }).ToListAsync();
        }

        public static async Task<List<AfterCareTypeViewModel>> GetAfterCareTypeDescriptions(IUnitOfWork uow, int organizationTypeID, int afterCareTileGroupID)
        {
            return await AfterCareTypeBL.GetAfterCareTypeDescriptions(uow, organizationTypeID, afterCareTileGroupID).Select(x => new AfterCareTypeViewModel
            {
                Name = x.Name,
                Description = x.Description ?? x.Name
            }).ToListAsync();
        }
    }
}