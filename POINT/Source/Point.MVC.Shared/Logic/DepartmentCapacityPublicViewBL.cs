﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Point.Business.Logic;
using Point.Database.Helpers;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Models.Enums;
using Point.MVC.Shared.Constants;
using Point.MVC.Shared.Models.Capacity;
using Point.MVC.Shared.ViewModels.Capacity;

namespace Point.MVC.Shared.Logic
{
    public class DepartmentCapacityPublicViewBL
    {
        private const string _hospitalAfterCareTypesLabel = "Alle specialismen";
        private const int _hospitalOrganizationTypeID = (int)OrganizationTypeID.Hospital;
        private const int _hospitalAfterCareTileGroupID = (int)AfterCareTileGroupID.Ziekenhuis;
        private static string _capacityDaysStringFormat = Get4DaysOfTheWeekFormatString();

        public static List<CapacityRegionViewModel> GetDepartmentCapacityPublicTiles(List<CapacityRegionViewModel> capacityRegionViewModel, CapacityParameters parameters, IEnumerable<int> regionIDs)
        {
            var capacityRegionViewModelX = new List<CapacityRegionViewModel>();

            var afterCareTileGroupsX = Task.Run(async () => await AfterCareViewBL.GetAfterCareTileGroups(parameters.Uow)).Result;
            var afterCareGroupsX = Task.Run(async () => await AfterCareViewBL.GetAfterCareGroups(parameters.Uow)).Result;
            var afterCareTypesX = Task.Run(async () => await AfterCareViewBL.GetAfterCareTypes(parameters)).Result;


            var regionAfterCareTypes = RegionAfterCareTypeBL.GetAllForRegionIDs(parameters.Uow, regionIDs); //parameters.RegionIdsSelection.SelectedRegionIds);

            if (regionIDs != null && regionIDs.Any() && afterCareTileGroupsX.Any() && afterCareGroupsX.Any() && afterCareTypesX.Any())
            {
                var addEmptyTiles = parameters.IsCapacityPublicVersion && !parameters.WithCapacityOnly;

                var departmentCapacityPublics = GetDepartmentCapacityPublic(parameters, regionIDs, null)
                    .Select(x => new AfterCareTypeViewModel
                    {
                        RegionID = x.RegionID,
                        AfterCareGroupID = x.AfterCareGroupID,
                        AfterCareTypeID = x.AfterCareTypeID ?? CapacityManagementConstants.AfterCareGroupOrTypeIdNone,
                        Name = x.AfterCareTypeName,
                        OrganizationTypeID = x.OrganizationTypeID, // TODO: IMPLEMENT!,
                        Capacity = x.Capacity1 ?? 0
                    }).ToList();

                foreach (var cr in capacityRegionViewModel)
                {
                    var departmentCapacities = (parameters.ShowByRegion ? departmentCapacityPublics.Where(x => x.RegionID == cr.RegionID) : departmentCapacityPublics).OrderBy(x => x.AfterCareTypeID);
                    if (!parameters.IsCapacityPublicVersion && !departmentCapacities.Any() && !parameters.WithCapacityOnly) continue;

                    var afterCareTileGroups = new List<AfterCareFinancingViewModel>();
                    bool isActiveInSelectedRegions;

                    var afterCareTypesForRegionID = regionAfterCareTypes.Where(x => x.RegionID == cr.RegionID);

                    foreach (var tg in afterCareTileGroupsX)
                    {
                        var afterCareGroups = new List<AfterCareGroupViewModel>();
                        
                        if (tg.AfterCareTileGroupID == _hospitalAfterCareTileGroupID)
                        {
                            var afterCareTypes = new List<AfterCareTypeViewModel>();
                            var afterCareTypesForOrganizationType = departmentCapacities.Where(x => x.OrganizationTypeID == _hospitalOrganizationTypeID).GroupBy(x => x.AfterCareTypeID)
                                .Select(x => new AfterCareTypeViewModel { AfterCareTypeID = x.Key, Name = x.Select(n => n.Name).First(), OrganizationTypeID = _hospitalOrganizationTypeID, Capacity = x.Sum(s => s.Capacity), RegionID = cr.RegionID});

                            AddAfterCareTypes(afterCareTypesX, afterCareTypesForOrganizationType, afterCareTypes, true, true, null);

                            IEnumerable<AfterCareTypeViewModel> afterCareTypesForRegionNoCount;
                            
                            if (addEmptyTiles)
                            {
                                // AfterCareTypes without capacity for this Region
                                afterCareTypesForRegionNoCount = afterCareTypesX.Where(x => (x.OrganizationTypeID == _hospitalOrganizationTypeID && afterCareTypesForRegionID.Any(x2 => x2.AfterCareTypeID == x.AfterCareTypeID)
                                                                                            && !afterCareTypes.Any(x3 => x3.RegionID == cr.RegionID && x3.AfterCareTypeID == x.AfterCareTypeID)));
                                AddAfterCareTypes(afterCareTypesX, afterCareTypesForRegionNoCount, afterCareTypes, true, true, null);
                            }
                            
                            isActiveInSelectedRegions = !parameters.IsCapacityPublicVersion || afterCareTypes.Any();
                            
                            if (addEmptyTiles)
                            {
                                // AfterCareTypes not applicable to this Region
                                var afterCareTypesNotIncluded = afterCareTypesX.Where(x => x.OrganizationTypeID == _hospitalOrganizationTypeID && !x.IsActiveInSelectedRegions && !afterCareTypesForRegionID.Any(x2 => x2.AfterCareTypeID == x.AfterCareTypeID)
                                                                                                                                               && !afterCareTypes.Any(reg => reg.AfterCareTypeID == reg.AfterCareTypeID));
                                AddAfterCareTypes(afterCareTypesX, afterCareTypesNotIncluded, afterCareTypes, true, false, null);
                            }

                            if (!parameters.WithCapacityOnly || afterCareTypes.Any())
                            {
                                AddAfterCareGroup(afterCareGroups, afterCareTypes, isActiveInSelectedRegions, null, _hospitalAfterCareTypesLabel, tg.AfterCareTileGroupID, tg.Sortorder);
                            }                          
                        }
                        else
                        {
                            foreach (var g in afterCareGroupsX.Where(x => x.AfterCareTileGroupID == tg.AfterCareTileGroupID))
                            {
                                var afterCareTypes = new List<AfterCareTypeViewModel>();
                                var afterCareTypesForOrganizationType = departmentCapacities.Where(x => x.AfterCareGroupID == g.AfterCareGroupID && x.OrganizationTypeID != _hospitalOrganizationTypeID).GroupBy(x => x.AfterCareTypeID)
                                    .Select(x => new AfterCareTypeViewModel{ AfterCareTypeID = x.Key, Name = x.Select(n => n.Name).First(), OrganizationTypeID = x.Select(n => n.OrganizationTypeID).First(), Capacity = x.Sum(s => s.Capacity), RegionID = cr.RegionID });

                                AddAfterCareTypes(afterCareTypesX, afterCareTypesForOrganizationType, afterCareTypes, false, true, g.AfterCareGroupID);

                                IEnumerable<AfterCareTypeViewModel> afterCareTypesForRegionNoCount;

                                if (addEmptyTiles)
                                {
                                    // AfterCareTypes without capacity for this Region
                                    afterCareTypesForRegionNoCount = afterCareTypesX.Where(x => x.OrganizationTypeID != _hospitalOrganizationTypeID && x.AfterCareGroupID == g.AfterCareGroupID && afterCareTypesForRegionID.Any(x2 => x2.AfterCareTypeID == x.AfterCareTypeID)
                                                                                                                                                    && !afterCareTypes.Any(x3 => x3.RegionID == cr.RegionID && x3.AfterCareTypeID == x.AfterCareTypeID));
                                    AddAfterCareTypes(afterCareTypesX, afterCareTypesForRegionNoCount, afterCareTypes, false, true, null);
                                }


                                isActiveInSelectedRegions = !parameters.IsCapacityPublicVersion || afterCareTypes.Any();

                                if (addEmptyTiles)
                                {
                                    // AfterCareTypes not applicable to this Region
                                    var afterCareTypesNotIncluded = afterCareTypesX.Where(x => x.OrganizationTypeID != _hospitalOrganizationTypeID && !x.IsActiveInSelectedRegions && x.AfterCareGroupID == g.AfterCareGroupID
                                                                                                                                                   && !afterCareTypesForRegionID.Any(x2 => x2.AfterCareTypeID == x.AfterCareTypeID)
                                                                                                                                                   && !afterCareTypes.Any(reg => reg.AfterCareGroupID == g.AfterCareGroupID && reg.AfterCareTypeID == reg.AfterCareTypeID));
                                    AddAfterCareTypes(afterCareTypesX, afterCareTypesNotIncluded, afterCareTypes, false, false, null);
                                }

                                if (!parameters.WithCapacityOnly || afterCareTypes.Any())
                                {
                                    AddAfterCareGroup(afterCareGroups, afterCareTypes, isActiveInSelectedRegions, g.AfterCareGroupID, g.Name, g.AfterCareTileGroupID, g.Sortorder);
                                }
                            }

                            if (addEmptyTiles)
                            {
                                var afterCareGroupsNotIncluded = afterCareGroupsX.Where(x => x.AfterCareTileGroupID == tg.AfterCareTileGroupID && afterCareGroups.All(reg => reg.AfterCareGroupID != x.AfterCareGroupID));
                                AddAfterCareGroups(afterCareGroupsNotIncluded, afterCareGroups);
                            }
                        }

                        if (!parameters.WithCapacityOnly || afterCareGroups.Any())
                        {
                            AddAfterCareTileGroups(new List<AfterCareFinancingViewModel>{tg}, afterCareTileGroups, afterCareGroups);
                        }
                    }

                    if (addEmptyTiles)
                    {
                        var afterCareTileGroupsNotIncluded = afterCareTileGroupsX.Where(x => afterCareTileGroups.All(reg => reg.AfterCareTileGroupID != x.AfterCareTileGroupID));
                        AddAfterCareTileGroups(afterCareTileGroupsNotIncluded, afterCareTileGroups, null);
                    }

                    if (!parameters.WithCapacityOnly || afterCareTileGroups.Any())
                    {
                        capacityRegionViewModelX.Add(new CapacityRegionViewModel {
                            AfterCareTileGroups = afterCareTileGroups,
                            RegionID = cr.RegionID,
                            Name = cr.Name
                        });
                    }
                }
            }

            if (!capacityRegionViewModelX.Any())
            {
                capacityRegionViewModelX = GetDepartmentCapacityPublicTilesEmpty(capacityRegionViewModel, afterCareTileGroupsX, afterCareGroupsX, afterCareTypesX);
            }

            return capacityRegionViewModelX;
        }

        public static List<CapacityRegionViewModel> GetDepartmentCapacityPublicTilesEmpty(List<CapacityRegionViewModel> capacityRegionViewModel, List<AfterCareFinancingViewModel> afterCareTileGroupsX, List<AfterCareGroupViewModel> afterCareGroupsX, List<AfterCareTypeViewModel> afterCareTypesX)
        {
            var capacityRegionViewModelX = new List<CapacityRegionViewModel>();
            var afterCareTileGroups = new List<AfterCareFinancingViewModel>();

            foreach (var tg in afterCareTileGroupsX)
            {
                var afterCareGroups = new List<AfterCareGroupViewModel>();

                if (tg.AfterCareTileGroupID == _hospitalAfterCareTileGroupID)
                {
                    AddAfterCareGroup(afterCareGroups, null, false, null, _hospitalAfterCareTypesLabel, tg.AfterCareTileGroupID, tg.Sortorder);
                }
                else
                {
                    var afterCareGroupsNotIncluded = afterCareGroupsX.Where(x => x.AfterCareTileGroupID == tg.AfterCareTileGroupID);
                    AddAfterCareGroups(afterCareGroupsNotIncluded, afterCareGroups);
                }

                AddAfterCareTileGroups(new List<AfterCareFinancingViewModel> { tg }, afterCareTileGroups, afterCareGroups);
            }

            capacityRegionViewModelX.Add(new CapacityRegionViewModel
            {
                AfterCareTileGroups = afterCareTileGroups,
                RegionID = CapacityManagementConstants.RegionIdForNoneSelected
            });

            return capacityRegionViewModelX;
        }

        public static IEnumerable<DepartmentCapacityItemViewModel> GetDepartmentCapacityPublicList(CapacityParameters parameters, IEnumerable<int> regionIDs, Pager pager, string orderBy)
        {
            var departmentCapacityPublic = GetDepartmentCapacityPublic(parameters, regionIDs, orderBy);

            if (parameters.SearchNonSpecificParameters.HasSearchParameters || parameters.FillLocationCoordinates)
            {               
                if (parameters.SearchNonSpecificParameters.HasSearchParameters)
                {
                    var name = NullifyEmptyString(parameters.SearchNonSpecificParameters.OrgLocDepName);
                    var city = NullifyEmptyString(parameters.SearchNonSpecificParameters.City);
                    var postalCode = NullifyEmptyString(parameters.SearchNonSpecificParameters.PostalCode);
                    var hasGpsCheck = postalCode != null && parameters.SearchNonSpecificParameters.PostalCodeRadius != null;

                    if (city != null)
                    {
                        departmentCapacityPublic = departmentCapacityPublic.Where(dcp => dcp.LocationCity.Contains(city));
                    }

                    if (name != null)
                    {
                        departmentCapacityPublic = departmentCapacityPublic.Where(dcp => (name == null || dcp.DepartmentName.Contains(name) || dcp.LocationName.Contains(name) || dcp.OrganizationName.Contains(name)));
                    }

                    if (hasGpsCheck)
                    {
                        var startcoordinates = GPSCoordinaatBL.GetStartCoordinate(parameters.Uow, postalCode);
                        double gpsrange = (double)1 / 111 * (parameters.SearchNonSpecificParameters.PostalCodeRadius ?? 5);
                        double maxlongitude = startcoordinates.Longitude.GetValueOrDefault(0) + gpsrange;
                        double minlongitude = startcoordinates.Longitude.GetValueOrDefault(0) - gpsrange;
                        double maxlatitude = startcoordinates.Latitude.GetValueOrDefault(0) + gpsrange;
                        double minlatitude = startcoordinates.Latitude.GetValueOrDefault(0) - gpsrange;

                        departmentCapacityPublic = departmentCapacityPublic.Where(x => (x.Longitude > minlongitude && x.Longitude < maxlongitude && x.Latitude > minlatitude && x.Latitude < maxlatitude));
                    }
                }
            }

            if (pager != null)
            {
                departmentCapacityPublic = PagingHelper<DepartmentCapacityPublic>.GetPage(pager, departmentCapacityPublic);
            }

            var capacityListItems = departmentCapacityPublic.Select(dep => new DepartmentCapacityItemViewModel
            {
                OrganizationName = dep.OrganizationName,
                LocationName = dep.LocationName,
                CityName = dep.LocationCity,
                LocationCity = dep.LocationCity,
                DepartmentName = dep.DepartmentName,
                Capacity1 = dep.Capacity1 >= 0 ? dep.Capacity1 : null,
                Capacity2 = dep.Capacity2 >= 0 ? dep.Capacity2 : null,
                Capacity3 = dep.Capacity3 >= 0 ? dep.Capacity3 : null,
                Capacity4 = dep.Capacity4 >= 0 ? dep.Capacity4 : null,
                CapacityInfo = dep.Information,
                MutationDateTime = dep.AdjustmentCapacityDate,
                MutatedByName = dep.ModifiedBy,
                MutatedByPhoneNumber = dep.DepartmentPhoneNumber,
                LocationAddress = (dep.LocationStreet ?? "") + " " + (dep.LocationNumber ?? ""),
                LocationPostalCode = dep.LocationPostalCode ?? "",
                DepartmentEmailAddress = dep.DepartmentEmailAddress,
                DepartmentPhoneNumber = dep.DepartmentPhoneNumber,
                DepartmentFaxNumber = dep.DepartmentFaxNumber,
                Latitude = dep.Latitude,
                Longitude = dep.Longitude
            }).ToList();

            //Formatting
            foreach (var item in capacityListItems)
            {
                item.CapacityDaysString = string.Format(_capacityDaysStringFormat, item.Capacity1 ?? 0, item.Capacity2 ?? 0, item.Capacity3 ?? 0, item.Capacity4 ?? 0);
                item.LocationPostalCode = item.LocationPostalCode?.ToUpperInvariant();
            }

            return capacityListItems;
        }

        public static IQueryable<DepartmentCapacityPublic> GetDepartmentCapacityPublic(CapacityParameters parameters, IEnumerable<int> regionIDs, string orderBy)
        {
            var departmentCapacityPublic = DepartmentCapacityPublicBL.GetAllActiveForRegionIDs(parameters.Uow, parameters.IsCapacityPublicVersion, regionIDs);
            
            if (parameters.AfterCareGroupID != null)
            {
                departmentCapacityPublic = departmentCapacityPublic.Where(dcp => dcp.AfterCareGroupID == parameters.AfterCareGroupID);
            }

            if (parameters.AfterCareTypeID != null)
            {
                departmentCapacityPublic = departmentCapacityPublic.Where(dcp => (parameters.AfterCareTypeID == null || dcp.AfterCareTypeID == parameters.AfterCareTypeID));
            }

            if (parameters.WithCapacityOnly)
            {
                departmentCapacityPublic = departmentCapacityPublic.Where(dcp => dcp.Capacity1 > 0);
            }


            if (!string.IsNullOrEmpty(orderBy))
            {
                departmentCapacityPublic = departmentCapacityPublic.OrderBy(orderBy);
            }

            return departmentCapacityPublic;
        }

        public static List<DepartmentCapacityGPSViewModel> GetLocationMapMarkers(List<DepartmentCapacityItemViewModel> capacityItems, CapacityParameters parameters)
        {
            var departmentCapacityGpsVms = new List<DepartmentCapacityGPSViewModel>();

            if (capacityItems == null || !capacityItems.Any()) return departmentCapacityGpsVms.ToList();

            foreach (var grouped in capacityItems.GroupBy(dcgps => dcgps.LocationPostalCode))
            {
                var departmentcapacitygpsvm = new DepartmentCapacityGPSViewModel();
                var capacity = 0;
                foreach (var departmentcapacitygps in capacityItems
                    .Where(dcgps => dcgps.LocationPostalCode == grouped.Key)
                    .OrderBy(dcgps => dcgps.DepartmentName))
                {
                    if (!departmentcapacitygpsvm.DepartmentNames.Any())
                    {
                        departmentcapacitygpsvm.Latitude = departmentcapacitygps.Latitude;
                        departmentcapacitygpsvm.Longitude = departmentcapacitygps.Longitude;
                        departmentcapacitygpsvm.LocationName = departmentcapacitygps.LocationName ?? string.Empty;
                        departmentcapacitygpsvm.OrganizationName = departmentcapacitygps.OrganizationName ?? string.Empty;
                        departmentcapacitygpsvm.LocationAddress = departmentcapacitygps.LocationAddress;
                        departmentcapacitygpsvm.LocationCity = departmentcapacitygps.LocationCity;
                        departmentcapacitygpsvm.LocationPostalCode = departmentcapacitygps.LocationPostalCode;
                    }

                    var departmentCapacity = new DepartmentCapacityViewModel
                    {
                        Capacity1 = departmentcapacitygps.Capacity1 ?? 0,
                        Capacity2 = departmentcapacitygps.Capacity2 ?? 0,
                        Capacity3 = departmentcapacitygps.Capacity3 ?? 0,
                        Capacity4 = departmentcapacitygps.Capacity4 ?? 0,
                        Name = departmentcapacitygps.DepartmentName ?? string.Empty,
                        Information = departmentcapacitygps.CapacityInfo ?? string.Empty,
                        DepartmentEmailAddress = departmentcapacitygps.DepartmentEmailAddress ?? string.Empty,
                        DepartmentPhoneNumber = departmentcapacitygps.DepartmentPhoneNumber ?? string.Empty,
                        LastUpdated = departmentcapacitygps.MutationDateTime,
                        DateLastUpdated = departmentcapacitygps.MutationDateTime != null ? $"{departmentcapacitygps.MutationDateTime:dd-MM-yyyy H:mm}" : ""
                    };

                    capacity += departmentcapacitygps.Capacity1 ?? 0;
                    departmentcapacitygpsvm.Departments.Add(departmentCapacity);
                }

                departmentcapacitygpsvm.TotalCapacity = capacity;
                departmentCapacityGpsVms.Add(departmentcapacitygpsvm);
            }

            return departmentCapacityGpsVms.ToList();
        }

        private static void AddAfterCareTypes(List<AfterCareTypeViewModel> afterCareTypesAll, IEnumerable<AfterCareTypeViewModel> afterCareTypesForOrganizationType, List<AfterCareTypeViewModel> afterCareTypesTarget, bool isHospitalType, bool isActiveInSelectedRegions, int? afterCareGroupID)
        {
            if (afterCareTypesForOrganizationType == null || afterCareTypesTarget == null) return;

            foreach (var act in afterCareTypesForOrganizationType)
            {
                var vm = new AfterCareTypeViewModel
                {
                    RegionID = act.RegionID,
                    AfterCareTypeID = act.AfterCareTypeID,
                    Name = act.Name,
                    AfterCareGroupID = afterCareGroupID,
                    Capacity = isActiveInSelectedRegions ? act.Capacity : 0,
                    Sortorder = isActiveInSelectedRegions ? afterCareTypesAll.Where(x => x.AfterCareTypeID == act.AfterCareTypeID).Select(x => x.Sortorder).FirstOrDefault() : act.Sortorder,      // TODO: sortOrder
                    OrganizationTypeID = isHospitalType ? _hospitalOrganizationTypeID : act.OrganizationTypeID,
                    IsActiveInSelectedRegions = isActiveInSelectedRegions
                };

                afterCareTypesTarget.Add(vm);
            }
        }

        private static void AddAfterCareGroup(List<AfterCareGroupViewModel> afterCareGroupsTarget, List<AfterCareTypeViewModel> afterCareTypesTarget, bool isActiveInSelectedRegions, int? afterCareGroupID, string name, int? afterCareTileGroupID, int sortOrder)
        {
            var vm = new AfterCareGroupViewModel
            {
                Name = name,
                AfterCareTileGroupID = afterCareTileGroupID,
                AfterCareGroupID = afterCareGroupID ?? CapacityManagementConstants.AfterCareGroupOrTypeIdNone,
                Sortorder = sortOrder,
                AfterCareTypes = afterCareTypesTarget ?? new List<AfterCareTypeViewModel>(),
                IsActiveInSelectedRegions = isActiveInSelectedRegions
            };

            afterCareGroupsTarget.Add(vm);
        }

        private static void AddAfterCareGroups(IEnumerable<AfterCareGroupViewModel> afterCareGroupsSource, List<AfterCareGroupViewModel> afterCareGroupsTarget)
        {
            foreach (var g in afterCareGroupsSource)
            {
                AddAfterCareGroup(afterCareGroupsTarget, null, false, g.AfterCareGroupID, g.Name, g.AfterCareTileGroupID, g.Sortorder);
            }
        }

        private static void AddAfterCareTileGroups(IEnumerable<AfterCareFinancingViewModel> afterCareTileGroupsSource, List<AfterCareFinancingViewModel> afterCareTileGroupsTarget, List<AfterCareGroupViewModel> afterCareGroupsTarget)
        {
            foreach (var tg in afterCareTileGroupsSource)
            {
                var vm = new AfterCareFinancingViewModel
                {
                    AfterCareTileGroupID = tg.AfterCareTileGroupID,
                    Name = tg.Name,
                    Sortorder = tg.Sortorder,
                    ColorCode = tg.ColorCode,
                    AfterCareGroups = afterCareGroupsTarget ?? new List<AfterCareGroupViewModel>(),
                };

                afterCareTileGroupsTarget.Add(vm);
            }
        }
        private static string NullifyEmptyString(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                return null;
            }
            return value.ToLowerInvariant();
        }

        /// <summary>
        /// If today is Wed,
        /// the output will be: "wo: {0}, do: {1}, vr: {2}, za: {3}"
        /// </summary>
        /// <returns></returns>
        private static string Get4DaysOfTheWeekFormatString()
        {
            var days = new[] { "zo", "ma", "di", "wo", "do", "vr", "za", "zo", "ma", "di", "wo" };
            var day = (int)DateTime.Now.DayOfWeek;
            return $"{days[day++]}: {{0}},  {days[day++]}: {{1}},  {days[day++]}: {{2}},  {days[day]}: {{3}}";
        }

    }
}