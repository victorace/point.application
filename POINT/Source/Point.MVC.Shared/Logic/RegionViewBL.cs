﻿using System.Collections.Generic;
using System.Linq;
using Point.Database.Models;
using Point.MVC.Shared.Models.Capacity;
using Point.MVC.Shared.ViewModels.Capacity;

namespace Point.MVC.Shared.Logic
{
    public class RegionViewBL
    {
        public static List<CapacityRegionViewModel> GetSelectedCapacityRegions(CapacityParameters parameters)
        {
            var capacityRegions = GetCapacityRegionsAll(parameters);
            if (parameters.RegionIdsSelection.NeedsRegionCheckOnQuery && parameters.RegionIdsSelection.SelectedRegionIds.Any())
            {
                capacityRegions = capacityRegions.Where(x => parameters.RegionIdsSelection.SelectedRegionIds.Contains(x.RegionID));
            }

            return capacityRegions.Select(r => new CapacityRegionViewModel
            {
                RegionID = r.RegionID,
                Name = r.Name
            }).ToList();
        }

        public static List<Region> GetCapacityRegions(CapacityParameters parameters)
        {
            IQueryable<Region> capacityRegions = (from r in parameters.Uow.RegionRepository.Get()
                join o in parameters.Uow.OrganizationRepository.Get() on r.RegionID equals o.RegionID
                join l in parameters.Uow.LocationRepository.Get() on o.OrganizationID equals l.OrganizationID
                join d in parameters.Uow.DepartmentCapacityPublicRepository.Get() on l.LocationID equals d.LocationID
                where !o.Inactive && !l.Inactive && d.IsActive && d.CapacityFunctionality && d.Capacity1 > 0
                select r);
            capacityRegions = parameters.IsCapacityPublicVersion ? capacityRegions.Where(x => x.CapacityBedsPublic) : capacityRegions.Where(x => x.CapacityBeds);
            capacityRegions = capacityRegions.Distinct().OrderBy(r => r.Name);

            if (parameters.RegionIdsSelection.NeedsRegionCheckOnQuery && parameters.RegionIdsSelection.SelectedRegionIds.Any())
            {
                capacityRegions = capacityRegions.Where(x => parameters.RegionIdsSelection.SelectedRegionIds.Contains(x.RegionID));
            }

            return capacityRegions.ToList();
        }

        // Returns ALL regions where CapacityBeds or CapacityBedsPublic is enabled
        public static IQueryable<Region> GetCapacityRegionsAll(CapacityParameters parameters)
        {
            return parameters.Uow.RegionRepository.Get().Where(r => parameters.IsCapacityPublicVersion ? r.CapacityBedsPublic : r.CapacityBeds).OrderBy(r => r.Name);
        }

    }
}