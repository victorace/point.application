﻿
$(document).ready(function () {
    _CapManJs.Init();
});


// Needed for GoogleMaps api (expected and called from own functions)
function initMap() {
    _CapManJs.MapView.ResultsSetup();
}


/***********************
 _CapManJs
 Accessed/Used in views:
 - TileViewResults
 - MapView
 - CapacitySearch
***********************/

var _CapManJs = {
    HasPublicLogoSpinner: false,

    CssSelectors: {
        OrgTypeZH: ".aftercare-type-zh",
        OrgTypeVVT: ".aftercare-type-vvt",
        AfterCareDescriptionsInfoModalInvoker: ".show-info",
        TabItems: "ul.capacity-tabs > li.menuItem"
    },

    ClassNames: {
        GlyphIcon: "glyphicon",
        GlyphIconFold: "glyphicon-chevron-up",
        GlyphIconUnfold: "glyphicon-chevron-down",
        Inactive: "inactive",
        Hidden: "hidden"
    },

    DataAttributes: {
        FormUrl: "formurl"
    },

    IDs: {
        AfterCareGroupsAndTypes: "#CapacitySearchAfterCareGroupsAndTypes",
        AfterCareGroupIdAndTypeId: "#AfterCareGroupIdAndTypeId",
        AfterCareGroupsAndTypesForHospital: "#AfterCareGroupsAndTypesForHospital",
        AfterCareGroupsAndTypesForVVT: "#AfterCareGroupsAndTypesForVVT",
        AfterCareGroupIdAndTypeUpdateMessage: "#AfterCareGroupIdAndTypeUpdateMessage",
        DisplayResults: "#displayCapacity",
        OrganizationTypeID: "#OrganizationTypeID",
        RegionIDs: "#regionIDs",
        ShowByRegion: "#showByRegion",
        WithCapacityOnly: "#withCapacityOnly",
        PointPublicLogoStatic: "#capacitylogoStatic",
        PointPublicLogoSpinner: "#capacitylogoSpinner",
        CurrentAction: "#CurrentAction"
    },

    Constants: {
        OrganizationTypeIdForHospital: $("#OrganizationTypeIdForHospital").val(),
        AfterCareGroupOrTypeIdNone: $("#AfterCareGroupOrTypeIdNone").val(),
        CurrentActionTileView: $("#CurrentActionTileView").val(),
        CurrentActionListView: $("#CurrentActionListView").val(),
        CurrentActionMapView: $("#CurrentActionMapView").val(),
        CurrentActionEditView: $("#CurrentActionEditView").val(),
        RegionIdForAllSelected: $("#RegionIdForAllSelected").val()
    },

    CheckEnableControls: function () {
        var afterCareGroupAndTypeId = _CapManJs.GetAfterCareGroupAndTypeListId();
        var hasAfterGroupOrTypeSelection = $(afterCareGroupAndTypeId).length > 0;
        var selectedRegionIds = $(_CapManJs.IDs.RegionIDs).val();
        if ((hasAfterGroupOrTypeSelection && $(afterCareGroupAndTypeId).val() === "") || _CapManJs.IsUndefined(typeof selectedRegionIds) || selectedRegionIds === null || selectedRegionIds.length === 0) {
            $(_CapManJs.IDs.DisplayResults).addClass("disabled");
        } else {
            $(_CapManJs.IDs.DisplayResults).removeClass("disabled");
        }

        _CapManJs.SetNavTabUrls();
    },

    GetAfterCareGroupAndTypeListId: function() {
        return _CapManJs.OrganizationTypeIsHospital()
            ? _CapManJs.IDs.AfterCareGroupsAndTypesForHospital
            : _CapManJs.IDs.AfterCareGroupsAndTypesForVVT;
    },

    GetViewResults: function () {
        switch ($(_CapManJs.IDs.CurrentAction).val()) {
            case _CapManJs.Constants.CurrentActionTileView:
                _CapManJs.TileView.GetResults();
                break;
            case _CapManJs.Constants.CurrentActionListView:
                _CapManJs.ListView.GetResults();
                break;
            case _CapManJs.Constants.CurrentActionMapView:
                _CapManJs.MapView.GetResults();
                break;
            //case _CapManJs.Constants.CurrentActionEditView:
            //    // this.GetEditViewResults();   // TODO: IMPLEMENT!
            //    break;
            default:
        }
    },


    IsDisplayResultsButtonDisabled: function () {
        var obj = $(_CapManJs.IDs.DisplayResults);
        return obj.length && obj.hasClass("disabled");
    },

    PointPublicLogoDisplay: function (isBusy) {
        if (!_CapManJs.HasPublicLogoSpinner) return;

        if (isBusy) {
            $(_CapManJs.IDs.PointPublicLogoSpinner).show();
            $(_CapManJs.IDs.PointPublicLogoStatic).hide();
        } else {
            $(_CapManJs.IDs.PointPublicLogoStatic).show();
            $(_CapManJs.IDs.PointPublicLogoSpinner).hide();
        }
    },

    IsUndefined: function (typeOfObj) {
        return typeOfObj === "undefined";
    },

    GetClassSelector: function (className) {
        return "." + className;
    },

    GetElementValue: function (objID) {
        var obj = $(objID);
        if (obj.length) {
            var value = obj.val();
            if (value !== "") {
                return value;
            }
        }
        return null;
    },

    OrganizationTypeRefresh: function (isHospital) {

        if (_CapManJs.IsUndefined(typeof isHospital)) {
            isHospital = _CapManJs.OrganizationTypeIsHospital();
        }

        var hidden = _CapManJs.ClassNames.Hidden;

        if (isHospital) {
            $(_CapManJs.CssSelectors.OrgTypeZH).removeClass(hidden);
            $(_CapManJs.CssSelectors.OrgTypeVVT).addClass(hidden);

            $(_CapManJs.IDs.AfterCareGroupsAndTypesForHospital).removeClass(hidden);
            $(_CapManJs.IDs.AfterCareGroupsAndTypesForVVT).addClass(hidden);
        } else {
            $(_CapManJs.CssSelectors.OrgTypeVVT).removeClass(hidden);
            $(_CapManJs.CssSelectors.OrgTypeZH).addClass(hidden);

            $(_CapManJs.IDs.AfterCareGroupsAndTypesForVVT).removeClass(hidden);
            $(_CapManJs.IDs.AfterCareGroupsAndTypesForHospital).addClass(hidden);
        }
    },

    OrganizationTypeIsHospital: function () {
        var orgTypeObj = $(_CapManJs.IDs.OrganizationTypeID);
        if (orgTypeObj.length === 0) return false;
        return (orgTypeObj.val() === _CapManJs.Constants.OrganizationTypeIdForHospital);
    },

    GetAfterCareGroupsAndTypes: function () {
        var postData = _CapManJs.GetCommonParameters();
        _CapManJs.AfterCareGroupsAndTypesDisplayUpdate(true);
        _CapManJs.AjaxCapacityPost($(_CapManJs.IDs.RegionIDs).data(_CapManJs.DataAttributes.FormUrl), postData, _CapManJs.AfterCareGroupsAndTypesDisplayUpdate);
    },

    AfterCareGroupsAndTypesDisplayUpdate: function (showBusyMessage) {
        if (_CapManJs.IsUndefined(typeof showBusyMessage)) {
            showBusyMessage = false;
        }

        var afterCareGroupIdAndTypeId = _CapManJs.GetAfterCareGroupAndTypeListId();
        var hidden = _CapManJs.ClassNames.Hidden;

        if (showBusyMessage) {
            $(afterCareGroupIdAndTypeId).addClass(hidden);
            $(_CapManJs.IDs.AfterCareGroupIdAndTypeUpdateMessage).removeClass(hidden);
        } else {
            $(_CapManJs.IDs.AfterCareGroupIdAndTypeUpdateMessage).addClass(hidden);
            $(afterCareGroupIdAndTypeId).removeClass(hidden);
            _CapManJs.OrganizationTypeRefresh();
            _CapManJs.CheckEnableControls();
        }
    },

    // ----------------------------------------------

    // Navigation
    //RemoveQueryString: function () {
    //    return;
    //    if (_CapManJs.IsUndefined(typeof history)) {
    //        return;
    //    }
    //    var urlValue = document.location.href.replace(location.search, '');
    //    //history.pushState({ state: 1, rand: Math.random() }, '', urlValue);
    //    // window.history.pushState('data', "Title", "/client-testimonials.html#" + scope);
    //    if (history.pushState) {
    //        history.pushState({ state: 1, rand: Math.random() }, '', urlValue);
    //    } else {
    //        if (history.go) {
    //            history.go(urlValue);
    //        }
    //    }
    //},

    GetRegionsParameter: function() {
        var selectedRegionIds = _CapManJs.GetElementValue(_CapManJs.IDs.RegionIDs);
        if (selectedRegionIds === null) return null;

        var regionIDs = [];
        if (selectedRegionIds.length === $(_CapManJs.IDs.RegionIDs + " option").length) {
            regionIDs = [_CapManJs.Constants.RegionIdForAllSelected];
        } else {
            regionIDs = selectedRegionIds;
        }
        return regionIDs;
    },

    // Post
    GetCommonParameters: function () {

        var showByRegion = false; // TODO: DETERMINE WHICH SHOULD BE DEFAULT (performance issues)
        var showByRegionObj = $(_CapManJs.IDs.ShowByRegion);
        if (showByRegionObj.length) {
            showByRegion = showByRegionObj.find("option:selected").val();
        }

        var withCapacityOnly = $(_CapManJs.IDs.WithCapacityOnly).is(":checked");

        var parameters = { showByRegion: showByRegion, withCapacityOnly: withCapacityOnly };

        var selectedRegionIds = _CapManJs.GetRegionsParameter();
        if (selectedRegionIds !== null) {
            parameters.regionIDs = selectedRegionIds;
        }

        var organizationTypeID = _CapManJs.GetElementValue(_CapManJs.IDs.OrganizationTypeID);
        if (organizationTypeID !== null) {
            parameters.organizationTypeID = organizationTypeID;
        }

        var currentTabView = _CapManJs.GetElementValue(_CapManJs.IDs.CurrentAction);
        if (currentTabView !== null) {
            parameters.currentTabView = currentTabView;
        }

        //var afterCareGroupIdAndTypeId = _CapManJs.GetElementValue(_CapManJs.IDs.AfterCareGroupIdAndTypeId);

        var afterCareGroupIdAndTypeId = _CapManJs.GetElementValue(_CapManJs.GetAfterCareGroupAndTypeListId());
        if (afterCareGroupIdAndTypeId !== null) {
            var idNone = _CapManJs.Constants.AfterCareGroupOrTypeIdNone;
            var IDs = afterCareGroupIdAndTypeId.split(";");	// Expected 2 items. item 0 = afterCareGroupID, item 1 = afterCareTypeID ("2;6")
            if (IDs.length === 2) {
                if (IDs[0] !== idNone) {
                    parameters.aftercaregroupid = IDs[0];
                }
                if (IDs[1] !== idNone) {
                    parameters.aftercaretypeid = IDs[1];
                }
            }
        }

        return parameters;
    },

    AjaxCapacityPost: function (url, postData, postFunction) {

        _CapManJs.PointPublicLogoDisplay(true);

        $.ajax({
            cache: false,
            type: "POST",
            url: url,
            data: postData,
            dataType: "json",
            success: function (data) { _CapManJs.ReplaceData(data, postFunction); },
            error: function (xhr, status, error) { _CapManJs.ShowErrorMessage(error); },
            complete: function () { _CapManJs.PointPublicLogoDisplay(false); }
        });
    },

    ShowErrorMessage: function (error) {
        if (!_CapManJs.IsUndefined(typeof showErrorMessage)) {
            showErrorMessage(error);
        } else {
            alert(error);
        }
    },

    ReplaceData: function (data, postFunction) {

        switch ($(_CapManJs.IDs.CurrentAction).val()) {
            case _CapManJs.Constants.CurrentActionTileView:
            case _CapManJs.Constants.CurrentActionListView:
                _CapManJs.ReplaceHtml(data);
                break;
            case _CapManJs.Constants.CurrentActionMapView:
                if (data.IsResultsJson) {
                    if (!_CapManJs.IsUndefined(typeof data.Results) && !_CapManJs.IsUndefined(typeof locations)) {
                        // Variable declared in MapViewResults view
                        locations = data.Results;
                    }
                }
                else {
                    _CapManJs.ReplaceHtml(data);
                }
                break;
            default:
        }

        if (!_CapManJs.IsUndefined(typeof postFunction)) {
            postFunction();
        }
    },

    ReplaceHtml: function (data) {
        if (!_CapManJs.IsUndefined(typeof data.htmlReplaces)) {
            
            $.each(data.htmlReplaces || [],
                function (id, html) {
                    if ($(id).length) {
                        $(id).replaceWith($(html));
                    }
                });
        }
    },

    TileView: {
        CssSelectors: {
            AfterCareFoldedItems: ".aftercaresubitems",
            AfterCareTileBox: ".aftercarebox",
            TileBoxItemsActiveStatus: ".capacity-active-status",
            AfterCareSubItemsFoldActionInvoker: ".capacity-unfold"
        },

        DataAttributes: {
            AfterCareGroupID: "aftercaregroupid",
            AfterCareTileGroupID: "aftercaretilegroupid"
        },

        ResultsSetup: function () {
            _CapManJs.InfoInit();
        },

        GetResults: function () {
            if (_CapManJs.IsDisplayResultsButtonDisabled()) return;

            var postData = _CapManJs.GetCommonParameters();
            _CapManJs.AjaxCapacityPost(
                $(_CapManJs.IDs.DisplayResults).data(_CapManJs.DataAttributes.FormUrl),
                postData,
                _CapManJs.TileView.ResultsSetup);
        },

        UnfoldChildren: function (obj) {
            if (_CapManJs.IsUndefined(typeof obj)) return;
            var objJQ = $(obj);

            var aftercareTileGroupID = objJQ.data(_CapManJs.TileView.DataAttributes.AfterCareTileGroupID);
            var afterCareGroupID = objJQ.data(_CapManJs.TileView.DataAttributes.AfterCareGroupID);

            if (aftercareTileGroupID.length === 0) return;

            var afterCareGroupIDSelector = _CapManJs.TileView.GetDataByValueSegment(_CapManJs.TileView.DataAttributes.AfterCareGroupID, afterCareGroupID);
            var itemToSlide = $(_CapManJs.TileView.CssSelectors.AfterCareFoldedItems + afterCareGroupIDSelector);
            var mustShow = !itemToSlide.is(":visible"); // Visible means it will be slid up (closed)
            var arrowRef = _CapManJs.GetClassSelector(_CapManJs.ClassNames.GlyphIcon);

            $(_CapManJs.TileView.CssSelectors.AfterCareTileBox + " " + _CapManJs.TileView.CssSelectors.AfterCareSubItemsFoldActionInvoker + " > " + arrowRef)
                .removeClass(_CapManJs.ClassNames.GlyphIconFold)
                .addClass(_CapManJs.ClassNames.GlyphIconUnfold);
            $(_CapManJs.TileView.CssSelectors.AfterCareTileBox + " " + _CapManJs.TileView.CssSelectors.TileBoxItemsActiveStatus)
                .removeClass(_CapManJs.ClassNames.Inactive);

            $(_CapManJs.TileView.CssSelectors.AfterCareFoldedItems).slideUp();

            if (mustShow) {
                // Should slide open
                $(_CapManJs.TileView.CssSelectors.AfterCareTileBox + _CapManJs.TileView.GetDataByValueSegment(_CapManJs.TileView.DataAttributes.AfterCareTileGroupID, aftercareTileGroupID) + _CapManJs.TileView.CssSelectors.TileBoxItemsActiveStatus)
                    .addClass(_CapManJs.ClassNames.Inactive);
                $(_CapManJs.TileView.CssSelectors.AfterCareTileBox + afterCareGroupIDSelector + _CapManJs.TileView.CssSelectors.TileBoxItemsActiveStatus)
                    .removeClass(_CapManJs.ClassNames.Inactive);

                var iconHolder = objJQ.find(arrowRef);
                if (iconHolder.length > 0) {
                    iconHolder.removeClass(_CapManJs.ClassNames.GlyphIconUnfold).addClass(_CapManJs.ClassNames.GlyphIconFold);
                }

                itemToSlide.slideDown(300);
            }
        },

        GetDataByValueSegment: function (dataAttributeName, dataAttributeValue) {
            return "[data-" + dataAttributeName + "=" + dataAttributeValue + "] "; // returns space at the end!
        }
    },


    ListView: {
        IDs: {
            // Free search fields
            City: "#City",
            Name: "#Name",
            PostalCode: "#PostalCode",
            PostalCodeRadius: "#PostalCodeRadius",
            // Paging
            PageNo: "#pageNo", //
            NextPage: "#nextPage", //
            PreviousPage: "#previousPage", //
            ListViewResultsTable: "table#capacityListTable"

        },

        ResultsSetup: function () {
            $(_CapManJs.ListView.IDs.PreviousPage + ".enabled").on("click",
                function () {
                    _CapManJs.ListView.ChangePageAndSubmit(-1);
                });

            $(_CapManJs.ListView.IDs.NextPage + ".enabled").on("click",
                function () {
                    _CapManJs.ListView.ChangePageAndSubmit(1);
                });

            $(_CapManJs.ListView.IDs.ListViewResultsTable + " tbody tr").on("click",
                function () {
                    var modal = $(_CapManJs.Modals.CapacityInfoModal.IDs.ModalID);
                    var tr = $(this);
                    var modalRef = _CapManJs.Modals.CapacityInfoModal;

                    modalRef.Functions.SetTextForIDFromDataValue(modal, tr, modalRef.IDAndDataAttributes.OrganizationName);
                    modalRef.Functions.SetTextForIDFromDataValue(modal, tr, modalRef.IDAndDataAttributes.LocationName);
                    modalRef.Functions.SetTextForIDFromDataValue(modal, tr, modalRef.IDAndDataAttributes.DepartmentName);
                    modalRef.Functions.SetTextForIDFromDataValue(modal, tr, modalRef.IDAndDataAttributes.LocationAddress);
                    modalRef.Functions.SetTextForIDFromDataValue(modal, tr, modalRef.IDAndDataAttributes.LocationPostalCode, ' ');
                    modalRef.Functions.SetTextForIDFromDataValue(modal, tr, modalRef.IDAndDataAttributes.LocationCity, ' ');
                    //modalRef.Functions.SetTextForIDFromDataValue(modal, tr, modalRef.IDAndDataAttributes.DepartmentFaxNumber, "fax:");
                    modalRef.Functions.SetTextForIDFromDataValue(modal, tr, modalRef.IDAndDataAttributes.Capacities);
                    modalRef.Functions.SetTextForIDFromDataValue(modal, tr, modalRef.IDAndDataAttributes.DepartmentAdjustmentCapacityDate);

                    modalRef.Functions.AssignAnchorTag(modal, tr, modalRef.IDAndDataAttributes.LocationEmailAddress, "mailto:", "");
                    modalRef.Functions.AssignAnchorTag(modal, tr, modalRef.IDAndDataAttributes.DepartmentPhoneNumber, "tel:", "");
                    modalRef.Functions.AssignAnchorTag(modal, tr, modalRef.IDAndDataAttributes.DepartmentEmailAddress, "mailto:", "");

                    modal.modal("show");
                });
        },

        GetResults: function () {
            if (_CapManJs.IsDisplayResultsButtonDisabled()) return;

            var postData = _CapManJs.GetCommonParameters();
            postData.name = $(_CapManJs.ListView.IDs.Name).val();
            postData.pageNo = $(_CapManJs.ListView.IDs.PageNo).val();

            var cityVal = _CapManJs.GetElementValue(_CapManJs.ListView.IDs.City);
            if (cityVal !== null) {
                postData.city = cityVal;
            }

            var postalCodeVal = _CapManJs.GetElementValue(_CapManJs.ListView.IDs.PostalCode);
            if (postalCodeVal !== null) {
                postData.postalCode = postalCodeVal;
            }

            var postalCodeRadiusVal = _CapManJs.GetElementValue(_CapManJs.ListView.IDs.PostalCodeRadius);
            if (postalCodeRadiusVal !== null) {
                postData.postalCodeRadius = postalCodeRadiusVal;
            }

            _CapManJs.AjaxCapacityPost($(_CapManJs.IDs.DisplayResults).data(_CapManJs.DataAttributes.FormUrl), postData, _CapManJs.ListView.ResultsSetup);
        },

        // PAGING:
        ChangePageAndSubmit: function (increment) {
            var pageNo = $(_CapManJs.ListView.IDs.PageNo);
            pageNo.val(parseInt(pageNo.val()) + increment);
            _CapManJs.ListView.GetResults();
        }

    },

    MapView: {
        URLs: {
            GoogleMapsMarkerClusterer: "/images/markerclusterer/m"
        },

        ResultsSetup: function () {
            if (_CapManJs.IsUndefined(typeof google) ||
                _CapManJs.IsUndefined(typeof locations)) return;

            var myLatlng = new google.maps.LatLng(51.99, 5.12);
            var myOptions = {
                zoom: 8,
                center: myLatlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                maxWidth: 900
            };

            var map = new google.maps.Map(document.getElementById("map"), myOptions);

            var markers = [];
            locations.map(function (loc, ix) {
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(loc.Latitude, loc.Longitude),
                    label: loc.TotalCapacity.toString(),
                    title: loc.LocationName,
                    totalcapacity: loc.TotalCapacity
                });

                var html = "<div>";
                var infoWindow = new google.maps.InfoWindow({ maxWidth: 900 });

                var address = "";
                if (loc.LocationAddress.length > 1) {
                    address = loc.LocationAddress;
                }
                if (loc.LocationPostalCode.length > 3) {
                    if (address !== "") {
                        address += ", ";
                    }
                    address += loc.LocationPostalCode;
                }
                if (loc.LocationCity.length > 3) {
                    if (address !== "") {
                        address += ", ";
                    }
                    address += loc.LocationCity;
                }

                html += "<h4><table><tr><td>Organisatie:</td><td class='padding-title'>" + loc.OrganizationName + "</td></tr>";
                html += "<tr><td>Locatie:</td><td class='padding-title'>" + loc.LocationName + "</td></tr>";
                html += "<tr><td></td><td class='padding-title location-address'>" + address + "</td></tr>";
                html += "<tr><td>Totaal capaciteit:</td><td class='padding-title'>" + loc.TotalCapacity + "</td></tr></table></h4>";
                html += "<table class='table table-striped  table-bordered table-condensed table-hover'><thead><tr>" +
                    "<th>Afdeling</th>" +
                    "<th>Email</th>" +
                    "<th width='105px'>Telefoon</th>" +
                    "<th width='60px'><strong>Capaciteit</strong></th>" +
                    "<th width='110px'><strong>Mutatiedatum</strong></th>" +
                    "<th>Info</th>" +
                    "</tr></thead><tbody>";

                loc.Departments.map(function (department, ix2) {
                    html += "<tr><td>" + department.Name + "</td>" +
                        "<td>" + _CapManJs.MapView.ResultsAssignAnchorTag(department.DepartmentEmailAddress, "mailto") + "</td>" +
                        "<td>" + _CapManJs.MapView.ResultsAssignAnchorTag(department.DepartmentPhoneNumber, "tel") + "</td>" +
                        "<td class='capacity-count'>" + department.Capacity1 + "</td>" +
                        "<td>" + department.DateLastUpdated + "</td>" +
                        "<td>" + department.Information + "</td></tr>";
                });

                html += "</tbody> </table> </div>";

                _CapManJs.MapView.GoogleMapsBindInfoWindow(marker, map, infoWindow, html);
                markers.push(marker);
            });

            var markerCluster = new MarkerClusterer(map, markers, { imagePath: _CapManJs.MapView.URLs.GoogleMapsMarkerClusterer });

            markerCluster.setCalculator(function (markers2, numStyles) {
                var index = 0;
                var total = 0;

                markers2.map(function (marker, ix) {
                    total += parseInt(marker.totalcapacity, 10);
                });

                index = Math.min(index, numStyles);

                return { text: total, index: index };
            });
        },

        GetResults: function () {
            if (_CapManJs.IsDisplayResultsButtonDisabled()) return;

            var postData = _CapManJs.GetCommonParameters();
            _CapManJs.AjaxCapacityPost($(_CapManJs.IDs.DisplayResults).data(_CapManJs.DataAttributes.FormUrl), postData, _CapManJs.MapView.ResultsSetup);
        },

        // link: mailto, tel
        ResultsAssignAnchorTag: function (value, link) {
            if (value === "") {
                return value;
            }
            return "<a class='capacity-link' href='" + link + ":" + value + "'>" + value + "</a>";
        },

        GoogleMapsBindInfoWindow: function (marker, map, infoWindow, html) {
            google.maps.event.addListener(marker, 'click', function () {
                infoWindow.setOptions({ maxWidth: 900 });
                infoWindow.setContent(html);
                infoWindow.open(map, marker);
            });
        }
    },

    EditView: {

        IDs: {
            CapacityTable: "#capacityTable",
            CapacityTableDiv: "#capacityTablediv",
            AfterCareTypeSelectForm: "#careTypeSelectForm",
            RegionSelectForm: "#regionSelectForm",
            OrganizationSelectForm: "#organizationSelectForm",
            RegionDropDown: "#ddlRegions",
            OrganizationDropDown: "#ddlOrganizations"
        },

        CssSelectors: {
            TrChecked: "#capacityTable > tbody > tr input.chk:checked",
            AfterCareTypeRadio: ".careTypeSelect",
            ButtonCopyCapacity: ".button-copy",
            DataTablesScroll: ".dataTables_scrollBody",
            InputSetDateNoID: ".cap-date-updated>input"
        },

        Setup: function () {

            $(_CapManJs.EditView.CssSelectors.ButtonCopyCapacity).click(function () {
                var btn = $(this);
                var suffix = ">input";

                $(_CapManJs.EditView.CssSelectors.TrChecked).each(function () {
                    var prefix = "." + $(this).data("trid") + " ";
                    var sourceVal = $(prefix + btn.data("source") + suffix).val();
                    $(prefix + btn.data("target") + suffix).val(sourceVal);
                });
            });

            $(_CapManJs.EditView.CssSelectors.AfterCareTypeRadio).click(function () {
                $(_CapManJs.EditView.IDs.AfterCareTypeSelectForm).submit();
            });

            $(_CapManJs.EditView.IDs.RegionDropDown).change(function () {
                $(_CapManJs.EditView.IDs.RegionSelectForm).submit();
            });

            $(_CapManJs.EditView.IDs.OrganizationDropDown).change(function () {
                $(_CapManJs.EditView.IDs.OrganizationSelectForm).submit();
            });

            var capacityTable = $(_CapManJs.EditView.IDs.CapacityTable);
            if (capacityTable.length > 0) {

                table = capacityTable.DataTable({
                    "dom": "lrt",
                    "paging": false,
                    "order": [],
                    "scrollY": _CapManJs.EditView.CalculateTableHeight(),
                    "scrollX": 1600
                });
            }

            $(window).resize(function () {
                if (!_CapManJs.IsUndefined(typeof table)) {
                    table.columns.adjust().draw();
                    if (capacityTable.length > 0) {
                        capacityTable.closest(_CapManJs.EditView.CssSelectors.DataTablesScroll).height(_CapManJs.EditView.CalculateTableHeight());
                    }
                }
            });
        },

        CalculateTableHeight: function () {
            var capacityTableDiv = $(_CapManJs.EditView.IDs.CapacityTableDiv);
            if (capacityTableDiv.length === 0) {
                return 0;
            }
            return $(window).innerHeight() - capacityTableDiv.offset().top - 140;
        },

        ShowSetDateTodayWarningModal: function () {
            $(_CapManJs.Modals.EditSetDateTodayWarning.IDs.ModalID).modal('show');
        },

        SetDateLastUpdatedToToday: function() {
            var now = moment().format('DD-MM-YYYY HH:mm');

            $(_CapManJs.EditView.CssSelectors.TrChecked).each(function () {
                var rowId = $(this).data("trid");
                $(_CapManJs.EditView.CssSelectors.InputSetDateNoID +  "." + rowId).val(now);
            });
        }

    },

    // *** MODALS ***

    Modals: {
        AfterCareDescriptionsModal: {
            IDs: {
                ModalID: "#afterCareTypeDescriptionsModal"
            },

            CssSelectors: {
                ModalBody: ".modal-body",
                ModalTitleSuffix: ".modal-title-suffix"
            },

            URLs: {
                AfterCareTypeDescriptions: $("#AfterCareTypeDescriptionsUrl").val(),
                GoogleMapsMarkerClusterer: "/images/markerclusterer/m"
            }
        },

        CapacityInfoModal: {
            IDs: {
                ModalID: "#capacityInfoModal"
            },
            // Elements exist as BOTH ID and Data Attribute
            IDAndDataAttributes: {
                OrganizationName: "organizationName",
                LocationName: "locationName",
                DepartmentName: "departmentName",
                LocationAddress: "locationAddress",
                LocationPostalCode: "locationPostalCode",
                LocationCity: "locationCity",
                //DepartmentFaxNumber: "departmentFaxNumber",
                Capacities: "capacities",
                LocationEmailAddress: "locationEmailAddress",
                DepartmentPhoneNumber: "departmentPhoneNumber",
                DepartmentEmailAddress: "departmentEmailAddress",
                DepartmentAdjustmentCapacityDate: "departmentAdjustmentcapacitydate"
            },

            Functions: {
                SetTextForIDFromDataValue: function (modal, tr, idAndDataAttr, prefixValue) {

                    var dataValue = tr.data(idAndDataAttr);
                    if (dataValue !== "" && !_CapManJs.IsUndefined(typeof prefixValue)) {
                        dataValue = prefixValue + dataValue;
                    }

                    modal.find("#" + idAndDataAttr).text(dataValue);
                },

                AssignAnchorTag: function (modal, tr, idAndDataAttr, tag, prefix) {
                    var value = tr.data(idAndDataAttr);

                    if (value === "") {
                        modal.find("#" + idAndDataAttr + "Div").hide();
                    } else {
                        modal.find("#" + idAndDataAttr)
                            .attr("href", value === "" ? "" : tag + value)
                            .text(value === "" ? "" : prefix + value);
                        modal.find("#" + idAndDataAttr + "Div").show();
                    }
                }
            }
        },

        EditSetDateTodayWarning: {
            IDs: {
                ModalID: "#capacitySetDateTodayWarning"
            }
        }
    },

    ViewSetup: function () {
        switch ($(_CapManJs.IDs.CurrentAction).val()) {
            //case _CapManJs.Constants.CurrentActionTileView:
            //    break;
            case _CapManJs.Constants.CurrentActionListView:
                _CapManJs.ListView.ResultsSetup();
                break;
            //case _CapManJs.Constants.CurrentActionMapView:
            //    break;
            case _CapManJs.Constants.CurrentActionEditView:
                _CapManJs.EditView.Setup();
                break;
            default:
        }
    },

    InfoInit: function () {
        $(_CapManJs.CssSelectors.AfterCareDescriptionsInfoModalInvoker).on("click", function () {
            var obj = $(this);
            var parameters = _CapManJs.GetCommonParameters();
            var urlParams = "organizationTypeID=" + parameters.organizationTypeID;
            var setHospital = -1;
            var tileGroupName = "";

            if ($(_CapManJs.IDs.CurrentAction).val() === _CapManJs.Constants.CurrentActionTileView) {

                var afterCareTileGroupID = obj.data(_CapManJs.TileView.DataAttributes.AfterCareTileGroupID);
                if (!_CapManJs.IsUndefined(typeof afterCareTileGroupID) && afterCareTileGroupID !== "") {
                    urlParams += "&afterCareTileGroupID=" + afterCareTileGroupID;

                    setHospital = obj.closest(".financing-row").hasClass("is-hospital") ? 1 : 0;
                    tileGroupName = " " + obj.prev(".aftercaretype-tile-group").text();
                }
            }

            var url = _CapManJs.Modals.AfterCareDescriptionsModal.URLs.AfterCareTypeDescriptions + "/?" + urlParams;
            var modalRef = _CapManJs.Modals.AfterCareDescriptionsModal;
            var modalDlg = $(modalRef.IDs.ModalID);
            modalDlg.find(modalRef.CssSelectors.ModalBody).load(url, function () {
                modalDlg.modal("show");
            });

            if (setHospital !== -1) {
                _CapManJs.OrganizationTypeRefresh(setHospital === 1);
                modalDlg.find(modalRef.CssSelectors.ModalTitleSuffix).text(tileGroupName);
            }

        });
    },

    SetNavTabUrls: function() {

        $(_CapManJs.CssSelectors.TabItems).each(function () {
            var li = $(this);
            var url = li.data('url');
            var selectedRegionIds = _CapManJs.GetRegionsParameter();
            if (selectedRegionIds !== null && selectedRegionIds.length > 0) {
                var regionIdPart = "regionIDs=";
                url += "?" + regionIdPart + selectedRegionIds.join("&" + regionIdPart);
            }
            li.find('a').attr('href', url);
        });
    },

    Init: function () {

        $(_CapManJs.IDs.RegionIDs).multiselect({
            includeSelectAllOption: true,
            selectAllText: "Alles/Niets Selecteren",
            allSelectedText: "Alle regio's geselecteerd",
            nonSelectedText: "Selecteer regio(s)",
            disableIfEmpty: true,
            maxHeight: 300,
            buttonWidth: '100%',
            numberDisplayed: 1,
            nSelectedText: 'geselecteerd'
        });

        this.HasPublicLogoSpinner = $(this.IDs.PointPublicLogoSpinner).length > 0;  // Determine if Public logo spinner is available
        this.ViewSetup();

        // TODO: IMPLEMENT Shared SearchViewModel
        if ($(_CapManJs.IDs.CurrentAction).val() === _CapManJs.Constants.CurrentActionEditView) {
            // As of today the EditView does NOT implement the shared (SearchViewModel) functionality!
            return;
        }

        this.InfoInit();
        this.OrganizationTypeRefresh();
        this.CheckEnableControls();
        this.SetNavTabUrls();
        //this.RemoveQueryString();
    }

};
