﻿
namespace Point.MVC.Shared.ViewModels
{
    public class ValueAndNameOption
    {
        public string Value { get; set; }
        public string Name { get; set; }

        public ValueAndNameOption(string value, string name)
        {
            Value = value;
            Name = name;
        }
    }
}