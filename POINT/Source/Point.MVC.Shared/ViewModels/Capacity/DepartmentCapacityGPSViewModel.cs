﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Point.MVC.Shared.ViewModels.Capacity
{
    public class DepartmentCapacityGPSViewModel
    {
        public string OrganizationName { get; set; }
        public string LocationName { get; set; }
        public List<string> DepartmentNames { get; set; }
        public List<int?> DepartmentCapacities { get; set; }
        public int? TotalCapacity { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }


        // Per department
        //public string DepartmentEmailAddress { get; set; }
        //public string DepartmentPhoneNumber { get; set; }
        //public string CapacityInfo { get; set; }
        //public string DepartmentName { get; set; }
        //public int? Capacity1 { get; set; }

        public List<DepartmentCapacityViewModel> Departments { get; set; } = new List<DepartmentCapacityViewModel>();
        public string LocationAddress { get; internal set; }
        public string LocationCity { get; internal set; }
        public string LocationPostalCode { get; internal set; }

        public DepartmentCapacityGPSViewModel()
        {
            DepartmentNames = new List<string>();
            DepartmentCapacities = new List<int?>();
        }

        public string ToMapsString()
        {
            //Newtonsoft.Json.Encod
            return JsonConvert.SerializeObject(this, Formatting.Indented);

            // return $"{{ lat: {Latitude.ToString().Replace(",", ".")}, lon: {Longitude.ToString().Replace(",", ".")}, title: '{String.Join("\r\n", DepartmentNames)}' }}"; //< text >{ lat: @marker.GPSCoordinaat.Latitude.ToString().Replace(",", "."), lng: @marker.GPSCoordinaat.Longitude.ToString().Replace(",", "."), title: '@marker.DepartmentCapacity.LocationName @marker.DepartmentCapacity.DepartmentName', capacity: '@marker.DepartmentCapacity.Capacity1' },</ text >
        }
    }
}