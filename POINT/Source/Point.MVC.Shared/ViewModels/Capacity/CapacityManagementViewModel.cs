﻿
namespace Point.MVC.Shared.ViewModels.Capacity
{
    public class CapacityManagementViewModel
    {
        public string Title { get; set; }
        public TabMenu TabMenu { get; set; }
        public CapacityTileViewModel TileViewModel { get;set;}
        public CapacityListViewModel ListViewModel { get; set; }
        public CapacityMapViewModel MapViewModel { get; set; }
        public CapacityEditViewModel EditViewModel { get; set; }
        public string CurrentTabView { get; set; } 
        public string ScreenTitle { get; set; }
        public bool IsEditView { get; set; }

        public bool IsCapacityPublicVersion { get; set; }
        public string AfterCareTypeDescriptionsUrl { get; set; }

        public string MenuActiveClass(CapacityViewType view)
        {
            return view == ViewType ? "menu-active" : "";
        }


        public virtual CapacityViewType ViewType { get; set; }

    }

    public enum CapacityViewType
    {
        Edit,
        Tiles,
        List,
        Map
    }

}
