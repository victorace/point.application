﻿using System;
using System.Collections.Generic;
using Point.Database.Models.ViewModels;

namespace Point.MVC.Shared.ViewModels.Capacity
{
    public class DepartmentCapacityResultsViewModel
    {
        private string _dateFormat = "dd-MM";
        public string Day1 => DateTime.Today.ToString(_dateFormat);
        public string Day2 => DateTime.Today.AddDays(1).ToString(_dateFormat);
        public string Day3 => DateTime.Today.AddDays(2).ToString(_dateFormat);
        public string Day4 => DateTime.Today.AddDays(3).ToString(_dateFormat);
        public bool IsSpecificSearch { get; set; }
        public List<DepartmentCapacityItemViewModel> Items { get; set; } = new List<DepartmentCapacityItemViewModel>();
        public PagingViewModel Paging { get; set; }

        public bool ShowMutadedByUserFields { get; set; }
    }

    public class DepartmentCapacityItemViewModel
    {
        public string OrganizationName { get; set; }
        public string LocationName { get; set; }
        public string DepartmentName { get; set; }
        public string CityName { get; set; }
        public DateTime? MutationDateTime { get; set; }
        public string MutationDateTimeDisplay => $"{MutationDateTime:dd-MM-yyyy H:mm}";
        public string MutatedByName { get; set; }
        public string MutatedByPhoneNumber { get; set; }
        public int? Capacity1 { get; set; }
        public int? Capacity2 { get; set; }
        public int? Capacity3 { get; set; }
        public int? Capacity4 { get; set; }
        public string CapacityDaysString { get; set; }
        public string CapacityInfo { get; set; }
        public string LocationCity { get; set; }
        public string LocationAddress { get; set; }
        public string DepartmentPhoneNumber { get; set; }
        public string DepartmentFaxNumber { get; set; }
        public string DepartmentEmailAddress { get; set; }
        public string Information { get; set; }
        public string LocationPostalCode { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }

    }
}