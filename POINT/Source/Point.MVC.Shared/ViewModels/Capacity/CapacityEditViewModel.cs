﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Models.Enums;

namespace Point.MVC.Shared.ViewModels.Capacity
{
    public class CapacityEditViewModel
    {
        public string Title { get; set; }
        [DisplayName("Typering nazorg")]
        public int? AfterCareCategoryID { get; set; }
        public int? RegionID { get; set; }
        public int? OrganizationID { get; set; }
        public OrganizationTypeID? OrganizationTypeID { get; set; }
        public int? LocationID { get; set; }
        public int? DepatmentID { get; set; }
        public List<CapacityLocationViewModel> Locations { get; set; } = new List<CapacityLocationViewModel>();
        public List<Option> Organizations { get; set; } = new List<Option>();
        public List<Option> Regions { get; set; } = new List<Option>();
        public bool AllowAfterCareCategorySelection => OrganizationTypeID == Point.Models.Enums.OrganizationTypeID.HealthCareProvider;
    }

    public class TabMenu
    {
        public List<LocalMenuItem> MenuItems { get; set; } = new List<LocalMenuItem>();
        public bool UserHasViewRights { get; set; }
        public bool UserHasEditRights { get; set; }
        public bool IsAllowed { get; set; }
    }

    public class CapacityLocationViewModel
    {
        public int LocationID { get; set; }
        public string Name { get; set; }
        public List<DepartmentCapacityViewModel> Departments { get; set; }
    }

    public class DepartmentCapacityViewModel : BaseEntity
    {
        public int DepartmentID { get; set; }
        public string Name { get; set; }
        public int? Capacity1 { get; set; }
        public int? Capacity2 { get; set; }
        public int? Capacity3 { get; set; }
        public int? Capacity4 { get; set; }

        [DisplayName("Informatie")]
        [StringLength(255, ErrorMessage = BaseEntity.errorMaxLength)]
        public string Information { get; set; }
        public DateTime? LastUpdated { get; set; }

        public string DateLastUpdated { get; set; }
        // Map
        public string DepartmentEmailAddress { get; set; }
        public string DepartmentPhoneNumber { get; set; }
    }
}