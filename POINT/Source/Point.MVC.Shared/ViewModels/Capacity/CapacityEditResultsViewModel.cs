﻿using System.Collections.Generic;

namespace Point.MVC.Shared.ViewModels.Capacity
{
    public class CapacityEditResultsViewModel
    {
        public List<CapacityLocationViewModel> Locations { get; set; } = new List<CapacityLocationViewModel>();

        public string CapacitySaveUrl { get; set; }
    }
}