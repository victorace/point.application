﻿using System.Collections.Generic;
using Point.Database.Models.ViewModels;

namespace Point.MVC.Shared.ViewModels.Capacity
{
    public class CapacityAfterCareGroupsAndTypesViewModel
    {
        public List<Option> AfterCareGroupsAndTypesHospital { get; set; } = new List<Option>();

        public List<Option> AfterCareGroupsAndTypesVVT { get; set; } = new List<Option>();

    }
}