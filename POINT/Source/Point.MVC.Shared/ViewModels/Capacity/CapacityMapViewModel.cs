﻿
using System.Collections.Generic;

namespace Point.MVC.Shared.ViewModels.Capacity
{
    public class CapacityMapViewModel
    {
        //public int? AfterCareTypeID { get; set; }
        //public int? AfterCareGroupID { get; set; }
        //public bool IsHospital { get; set; }
        //public bool WithCapacityOnly { get; set; }
        //public List<Option> OrganizationTypes { get; set; } = new List<Option>();

        //public List<ValueAndNameOption> AfterCareGroupsAndTypes { get; set; } = new List<ValueAndNameOption>();
        //public string AfterCareGroupIdAndTypeId => $"{AfterCareGroupID ?? 0};{AfterCareTypeID ?? 0}";

        //public List<Option> Regions { get; set; }
        //public RegionIdsSelection RegionIdsSelection { get; set; } = new RegionIdsSelection(null, null);

        public CapacitySearchViewModel Search { get; set; } = new CapacitySearchViewModel();
        public DepartmentCapacityResultsViewModel Results { get; set; } = new DepartmentCapacityResultsViewModel();

        public List<DepartmentCapacityGPSViewModel> DepartmentCapacityGPSList { get; set; }
    }
}