﻿using System.Collections.Generic;
using Point.Database.Models.ViewModels;

namespace Point.MVC.Shared.ViewModels.Capacity
{
    public class CapacitySearchViewModel
    {
        public int? AfterCareTypeID { get; set; }
        public int? AfterCareGroupID { get; set; }
        public bool IsHospital { get; set; }
        public bool WithCapacityOnly { get; set; }
        public List<Option> OrganizationTypes { get; set; } = new List<Option>();
        public CapacityAfterCareGroupsAndTypesViewModel AfterCareGroupsAndTypes { get; set; } = new CapacityAfterCareGroupsAndTypesViewModel();
        public string AfterCareGroupIdAndTypeId => $"{AfterCareGroupID ?? 0};{AfterCareTypeID ?? 0}";

        public List<Option> Regions { get; set; }
        public RegionIdsSelection RegionIdsSelection { get; set; } = new RegionIdsSelection(null, null);

        public bool ShowByRegion { get; set; }

        // For ListView
        public string Name { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        public int? PostalCodeRadius { get; set; }
        public List<ValueAndNameOption> PostalCodeRadiusList { get; set; }

        public string GetResultsUrl { get; set; }
        public string GetAfterCareGroupsAndTypesUrl { get; set; }

        public string CurrentAction { get; set; }       // TabMenuItems.Edit/ListView/TileView/MapView


        // For EditView
        public int? OrganizationID { get; set; }
        public int? OrganizationTypeID { get; set; }
        public int? AfterCareCategoryID { get; set; }
        
        public List<Option> Organizations { get; set; } = new List<Option>();
        public List<Option> AfterCareCategories { get; set; } = new List<Option>();
        public bool AllowAfterCareCategorySelection => OrganizationTypeID.HasValue && OrganizationTypeID == (int)Point.Models.Enums.OrganizationTypeID.HealthCareProvider;

        public bool IsCapacityPublicVersion { get; set; }
    }
}