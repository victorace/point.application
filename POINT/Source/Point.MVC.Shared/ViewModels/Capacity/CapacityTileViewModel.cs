﻿using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using Point.Database.Models.ViewModels;
using Point.MVC.Shared.Constants;
using Point.MVC.Shared.Models.Capacity;

namespace Point.MVC.Shared.ViewModels.Capacity
{
    public class CapacityTileViewModel
    {
        public CapacitySearchViewModel Search { get; set; } = new CapacitySearchViewModel();      

        public CapacityTileRegionResultsViewModel Results { get; set; } = new CapacityTileRegionResultsViewModel();

    }


    public class CapacityTileRegionResultsViewModel
    {
        public CapacityTileRegionResultsViewModel(CapacityParameters parameters = null)
        {
            if (parameters != null)
            {
                IsSpecificSearch = parameters.IsSpecificSearch;
                WithCapacityOnly = parameters.WithCapacityOnly;
            }
        }

        public List<CapacityRegionViewModel> AfterCareRegions { get; set; } = new List<CapacityRegionViewModel>();
        

        // Needed to create query string parameter
        // Usage in view as array parameter for Url.Action:  Html.Raw("&" + Model.RegionIDsToQuery);
        //public NameValueCollection RegionIDsToQuery { get; set; }

        public bool IsSpecificSearch { get; set; }

        public bool HasResults => AfterCareRegions.Any() && !AfterCareRegions.First().IsEmpty;

        public NameValueCollection RegionIDsToQuery { get; internal set; }

        public bool WithCapacityOnly { get; set; }
    }

    public class CapacityRegionViewModel
    {
        public List<AfterCareFinancingViewModel> AfterCareTileGroups { get; set; } = new List<AfterCareFinancingViewModel>();
        public string Name { get; set; }
        public int RegionID { get; set; }
        public bool IsEmpty => RegionID == CapacityManagementConstants.RegionIdForNoneSelected;

    }

    public class RegionIdsSelection
    {
        private const string RegionIdsIdentifier = "regionIDs";
        private bool _hasAllRegionsSelected = false;
        public RegionIdsSelection(int[] selectedRegionIDs, int? currentUserRegionID)
        {
            DetermineSelection(selectedRegionIDs, currentUserRegionID);
            SetRegionIDsToQuery();
        }

        private void DetermineSelection(int[] selectedRegionIDs, int? currentUserRegionID)
        {
            var regionIds = new int[0];

            if (selectedRegionIDs != null)
            {
                if (selectedRegionIDs.Length == 1 && selectedRegionIDs[0] == CapacityManagementConstants.RegionIdForAllSelected)
                {
                    _hasAllRegionsSelected = true;
                }
                else
                {
                    regionIds = selectedRegionIDs;
                }
            }
            else if (currentUserRegionID.HasValue)
            {
                // Start with current region as default
                regionIds = new[]{currentUserRegionID.Value};
            }

            SelectedRegionIds = regionIds;
        }

        public int[] SelectedRegionIds { get; set; } = new int[0];

        public bool HasRegionsSelected => _hasAllRegionsSelected || (SelectedRegionIds != null && SelectedRegionIds.Any());


        // Determined and set in CapacityViewModelHelper in order to simplify sql query
        public bool HasAllRegionsSelected => _hasAllRegionsSelected;

        public bool NeedsRegionCheckOnQuery => !HasAllRegionsSelected && HasRegionsSelected;

        // Needed to create query string parameter
        // Usage in view as array parameter for Url.Action:  Html.Raw("&" + Model.RegionIDsToQuery);
        public NameValueCollection RegionIDsToQuery { get; set; }

        public void SetRegionIDsToQuery()
        {
            var regionIdsToQuery = HttpUtility.ParseQueryString("");

            if (HasAllRegionsSelected)
            {
                regionIdsToQuery.Add(RegionIdsIdentifier, CapacityManagementConstants.RegionIdForAllSelected.ToString());
            }
            else if (SelectedRegionIds.Any())
            {
                foreach (var regionId in SelectedRegionIds)
                {
                    regionIdsToQuery.Add(RegionIdsIdentifier, regionId.ToString());
                }
            }

            //return regionIdsToQuery;
            RegionIDsToQuery = regionIdsToQuery;
        }

    }


}
