﻿using System.Threading.Tasks;
using System.Web.Mvc;
using DevTrends.MvcDonutCaching;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Models.Enums;
using Point.MVC.Shared.Constants;
using Point.MVC.Shared.Helpers;
using Point.MVC.Shared.Logic;

namespace Point.MVC.Shared.Controllers
{
    [DonutOutputCache(Duration = 0)]
    public class CapacityController : Controller
    {
        private IUnitOfWork _uow;
        private PointUserInfo _pointUserInfo;
        private string _tileViewResultsUrl;
        private string _listViewResultsUrl;
        private string _mapViewResultsUrl;
        private string _afterCareGroupsAndTypesUrl;
        private Controller _callingController;
        private bool _showMutadedByUserFields;
        private bool _isPublicCapacityVersion;
        private string _pathViews = CapacityViewModelHelper.PathViews;

        public CapacityController(Controller callingController, IUnitOfWork uow, PointUserInfo pointUserInfo, bool showMutadedByUserFields, bool isPublicCapacityVersion)
        {
            _uow = uow;
            _pointUserInfo = pointUserInfo;
            _callingController = callingController;
            _tileViewResultsUrl = _callingController.Url.Action("GetTileResults");
            _listViewResultsUrl = _callingController.Url.Action("GetListResults");
            _mapViewResultsUrl = _callingController.Url.Action("GetMapResults");
            _afterCareGroupsAndTypesUrl = _callingController.Url.Action("GetAfterCareGroupsAndTypes");
            _showMutadedByUserFields = showMutadedByUserFields;
            _isPublicCapacityVersion = isPublicCapacityVersion;
        }

        public ActionResult Index()
        {
            return RedirectToAction("Tile");
        }

        public ActionResult Tile(bool showByRegion = true, bool withCapacityOnly = true, int[] regionIDs = null)
        {
            var searched = !withCapacityOnly || regionIDs != null;

            var parameters = CapacityViewModelHelper.GetParameters(_callingController, _uow, _pointUserInfo, showByRegion, withCapacityOnly, regionIDs, 
                null, null, null, null, null, null, null, null, searched, getResultsUrl: _tileViewResultsUrl, isPublicVersion: _isPublicCapacityVersion, getAfterCareGroupsAndTypesUrl: _afterCareGroupsAndTypesUrl);

            return View(_pathViews + "_CapacityManagement.cshtml", CapacityViewModelHelper.GetTileViewModel(parameters));
        }

        [HttpPost]
        public JsonResult GetTileResults(bool showByRegion = true, bool withCapacityOnly = true, int[] regionIDs = null)
        {
            var searched = regionIDs != null;
            var parameters = CapacityViewModelHelper.GetParameters(_callingController, _uow, _pointUserInfo, showByRegion, withCapacityOnly, regionIDs, 
                null, null, null, null, null, null, null, null, isPublicVersion: _isPublicCapacityVersion, isSpecificSearch: searched);
            return Json(new
            {
                IsResultsJson = false,
                htmlReplaces = CapacityViewModelHelper.GetAfterCareTileRegionsPartial(parameters)
            });
        }

        public ActionResult List(bool showByRegion = true, string name = null, string city = null,
            int? afterCareGroupID = null, int? afterCareTypeID = null, bool withCapacityOnly = true, int[] regionIDs = null,
            bool searched = false, int? afterCareTileGroupID = null, int? organizationTypeID = null, int pageNo = 1, string postalCode = null, int? postalCodeRadius = null)
        {
            if (!searched)
            {
                // !showByRegion || 
                searched = !withCapacityOnly || name != null || city != null || afterCareGroupID != null || afterCareTypeID != null || regionIDs != null || organizationTypeID != null;
            }

            var parameters = CapacityViewModelHelper.GetParameters(_callingController, _uow, _pointUserInfo, showByRegion, withCapacityOnly, regionIDs, organizationTypeID,
                afterCareTileGroupID, afterCareGroupID, afterCareTypeID, name, city, postalCode, postalCodeRadius, searched, pageNo, getResultsUrl: _listViewResultsUrl, 
                showMutadedByUserFields: _showMutadedByUserFields, isPublicVersion: _isPublicCapacityVersion, getAfterCareGroupsAndTypesUrl: _afterCareGroupsAndTypesUrl);

            return View(_pathViews + "_CapacityManagement.cshtml", CapacityViewModelHelper.GetListViewModel(parameters));           // TODO: IMPLEMENT Pager + Postalcode search
        }

        [HttpPost]
        public JsonResult GetListResults(bool showByRegion = true, string name = null, string city = null,
            int? afterCareGroupID = null, int? afterCareTypeID = null, bool withCapacityOnly = true, int[] regionIDs = null, int pageNo = 1, string postalCode = null, int? postalCodeRadius = null)
        {
            var parameters = CapacityViewModelHelper.GetParameters(_callingController, _uow, _pointUserInfo, showByRegion, withCapacityOnly, regionIDs, null, null, 
                afterCareGroupID, afterCareTypeID, name, city, postalCode, postalCodeRadius, pageNo: pageNo, showMutadedByUserFields: _showMutadedByUserFields, 
                isPublicVersion: _isPublicCapacityVersion, getAfterCareGroupsAndTypesUrl: _afterCareGroupsAndTypesUrl);
            return Json(new
            {
                IsResultsJson = false,
                htmlReplaces = CapacityViewModelHelper.GetAfterCareListPartial(parameters)          // TODO: IMPLEMENT Pager + Postalcode search
            });
        }

        [HttpPost]
        public JsonResult GetAfterCareGroupsAndTypes(bool withCapacityOnly = true, int[] regionIDs = null, int? afterCareGroupID = null, int? afterCareTypeID = null, int? organizationTypeID = null, string currentTabView = TabMenuItems.Other)
        {
            var parameters = CapacityViewModelHelper.GetParameters(_callingController, _uow, _pointUserInfo, false, withCapacityOnly, regionIDs, null, organizationTypeID, afterCareGroupID, afterCareTypeID, null, null, isPublicVersion: _isPublicCapacityVersion, currentTabView: currentTabView);
            return Json(new
            {
                IsResultsJson = false,
                htmlReplaces = CapacityViewModelHelper.GetAfterCareGroupAndTypeListPartial(parameters)          // TODO: IMPLEMENT Pager + Postalcode search
            });
        }

        public ActionResult AfterCareTypeDescriptionsModal()
        {
            return PartialView(_pathViews + "AfterCareTypeDescriptionsModal.cshtml");
        }

        public async Task<ActionResult> AfterCareTypeDescriptions(int organizationTypeID = (int)OrganizationTypeID.HealthCareProvider, int afterCareTileGroupID = -1)
        {
            var model = await AfterCareViewBL.GetAfterCareTypeDescriptions(_uow, organizationTypeID, afterCareTileGroupID);
            return PartialView(_pathViews + "AfterCareTypeDescriptionsBody.cshtml", model);
        }

        public async Task<ActionResult> AfterCareTypeDescriptionsByCategory(int[] ids)
        {
            var model = await AfterCareViewBL.GetAfterCareTypeDescriptionsByCategoryIDs(_uow, ids);
            return PartialView(_pathViews + "AfterCareTypeDescriptionsBody.cshtml", model);
        }

        public ActionResult Map(int[] regionIDs = null, int? afterCareGroupID = null, int? afterCareTypeID = null, bool withCapacityOnly = true, int? organizationTypeID = null)
        {
            var searched = afterCareGroupID != null || afterCareTypeID != null || regionIDs != null || organizationTypeID != null || !withCapacityOnly;
            var parameters = CapacityViewModelHelper.GetParameters(_callingController, _uow, _pointUserInfo, true, withCapacityOnly, regionIDs, organizationTypeID, 
                null, afterCareGroupID, afterCareTypeID, null, null, null, null, searched, fillLocationCoordinates: true, getResultsUrl: _mapViewResultsUrl, isPublicVersion: _isPublicCapacityVersion, getAfterCareGroupsAndTypesUrl: _afterCareGroupsAndTypesUrl);
            return View(_pathViews + "_CapacityManagement.cshtml", CapacityViewModelHelper.GetCapacityMapViewModel(parameters));
        }

        [HttpPost]
        public JsonResult GetMapResults(int[] regionIDs = null, int? afterCareGroupID = null, int? afterCareTypeID = null, bool withCapacityOnly = true, int? organizationTypeID = null)
        {
            var searched = afterCareGroupID != null || afterCareTypeID != null || regionIDs != null || organizationTypeID != null || !withCapacityOnly;
            var parameters = CapacityViewModelHelper.GetParameters(_callingController, _uow, _pointUserInfo, true, withCapacityOnly, regionIDs, organizationTypeID,
                null, afterCareGroupID, afterCareTypeID, null, null, null, null, searched, fillLocationCoordinates: true, isPublicVersion: _isPublicCapacityVersion,
                getAfterCareGroupsAndTypesUrl: _afterCareGroupsAndTypesUrl);
            return Json(new
            {
                IsResultsJson = false,
                htmlReplaces = CapacityViewModelHelper.GetCapacityMapPartial(parameters)          // TODO: IMPLEMENT Pager + Postalcode search
            });
        }

        [HttpPost]
        public JsonResult GetMapJsonResults(int[] regionIDs = null, int? afterCareGroupID = null, int? afterCareTypeID = null, bool withCapacityOnly = true, int? organizationTypeID = null)
        {
            var searched = afterCareGroupID != null || afterCareTypeID != null || regionIDs != null || organizationTypeID != null || !withCapacityOnly;
            var parameters = CapacityViewModelHelper.GetParameters(_callingController, _uow, _pointUserInfo, true, withCapacityOnly, regionIDs, organizationTypeID,
                null, afterCareGroupID, afterCareTypeID, null, null, null, null, searched, fillLocationCoordinates: true, isPublicVersion: _isPublicCapacityVersion, 
                getAfterCareGroupsAndTypesUrl: _afterCareGroupsAndTypesUrl);

            var viewModel = CapacityViewModelHelper.GetCapacityMapViewModel(parameters);

            return Json(new
            {
                IsResultsJson = true,
                Results = viewModel.MapViewModel.DepartmentCapacityGPSList
            });
        }

    }
}