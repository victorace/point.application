﻿using Point.Business.Helper;
using Point.Business.Logic;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Attributes;
using Point.Models.Enums;
using Point.MVC.Shared.Models.Capacity;
using Point.MVC.Shared.ViewModels.Capacity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Point.MVC.Shared.Constants;
using Point.MVC.Shared.Logic;
using Point.MVC.Shared.ViewModels;

namespace Point.MVC.Shared.Helpers
{
    public static class CapacityViewModelHelper
    {
        private const int _noPagerNo = -1;
        public const string PathViews = "~/Views/Capacity/_Linked/";
        
        // TODO: This should be implemented on the Capacity public controller as well
        public static CapacityParameters GetParameters(Controller controller, IUnitOfWork uow, PointUserInfo pointUserInfo, bool showByRegion, bool withCapacityOnly, int[] regionIDs, int? organizationTypeID, 
            int? afterCareTileGroupID, int? afterCareGroupID, int? afterCareTypeID, string nonSpecificOrgLocDepName, string nonSpecificCity, string nonSpecificPostalCode = null, int? nonSpecificPostalCodeRadius = null, 
            bool isSpecificSearch = false, int pageNo = _noPagerNo, bool fillLocationCoordinates = false, bool isPublicVersion = false, string getResultsUrl = null, bool showMutadedByUserFields = false, 
            int? organizationID = null, int? afterCareCategoryID = null, DateTime? adjustmentCapacityDate = null, string getAfterCareGroupsAndTypesUrl = null, string currentTabView = null)
        {

            var selectZH = false;
            if (organizationTypeID.HasValue || afterCareTileGroupID.HasValue)
            {
                // Got here from TileView
                selectZH = (organizationTypeID == (int)OrganizationTypeID.Hospital || afterCareTileGroupID == (int)AfterCareTileGroupID.Ziekenhuis);
            }

            if (!selectZH)
            {
                // VVT
                if (!afterCareGroupID.HasValue && afterCareTypeID.HasValue)
                {
                    afterCareGroupID = AfterCareTypeBL.GetByCurrentValue(uow, afterCareTypeID, null).FirstOrDefault()?.AfterCareGroupID;
                }
            }

            var parameters = new CapacityParameters
            {
                IsHospitalSearch = selectZH,
                Controller = controller,
                Uow = uow,
                PointUserInfo = pointUserInfo,
                ShowByRegion = showByRegion,
                WithCapacityOnly = withCapacityOnly,
                //RegionIDs = regionIDs,
                RegionIdsSelection = new RegionIdsSelection(regionIDs, pointUserInfo?.Region?.RegionID),
                OrganizationTypeID = organizationTypeID,
                AfterCareTileGroupID = afterCareTileGroupID,
                AfterCareGroupID = afterCareGroupID,
                AfterCareTypeID = afterCareTypeID,
                IsSpecificSearch = isSpecificSearch,
                PageNo = pageNo,
                FillLocationCoordinates = fillLocationCoordinates,
                SearchNonSpecificParameters = new CapacityParametersNonSpecificSearch { OrgLocDepName = nonSpecificOrgLocDepName, City = nonSpecificCity, PostalCode = nonSpecificPostalCode, PostalCodeRadius = nonSpecificPostalCodeRadius },
                IsCapacityPublicVersion = isPublicVersion,
                GetResultsUrl = getResultsUrl,
                ShowMutadedByUserFields = showMutadedByUserFields,
                OrganizationID = organizationID,
                AfterCareCategoryID = afterCareCategoryID,
                GetAfterCareGroupsAndTypesUrl = getAfterCareGroupsAndTypesUrl,
                CurrentAction = currentTabView
            };

            return parameters;
        }

        public static CapacityManagementViewModel GetCapacityViewModel(CapacityParameters parameters)
        {
            CapacityViewType viewType = CapacityViewType.Tiles;
            string screenTitle = string.Empty;
            switch (parameters.CurrentAction)
            {
                case TabMenuItems.EditView:
                    screenTitle = "bewerken";
                    viewType = CapacityViewType.Edit;
                    break;
                case TabMenuItems.ListView:
                    screenTitle = "lijst";
                    viewType = CapacityViewType.List;
                    break;
                case TabMenuItems.TileView:
                    screenTitle = "tegel";
                    viewType = CapacityViewType.Tiles;
                    break;
                case TabMenuItems.MapView:
                    screenTitle = "kaart";
                    viewType = CapacityViewType.Map;
                    break;
            }
            
            screenTitle = $"Capaciteitsbeheer ({screenTitle})";

            return new CapacityManagementViewModel {
                ViewType = viewType,
                TabMenu = GetTabMenuItems(parameters),
                CurrentTabView = parameters.CurrentAction,
                ScreenTitle = screenTitle,
                IsEditView = parameters.CurrentAction == TabMenuItems.EditView,
                IsCapacityPublicVersion = parameters.IsCapacityPublicVersion,
                AfterCareTypeDescriptionsUrl = parameters.Controller.Url.Action("AfterCareTypeDescriptions", "Capacity")

            };
        }
        
        public static CapacityManagementViewModel GetTileViewModel(CapacityParameters parameters)
        {
            parameters.CurrentAction = TabMenuItems.TileView;
            var viewModelMain = GetCapacityViewModel(parameters);
            if (!viewModelMain.TabMenu.UserHasViewRights)
            {
                return viewModelMain;
            }

            var viewModel = new CapacityTileViewModel
            {
                Results = GetAfterCareTileResults(parameters),
                Search = GetCapacitySearchViewModel(parameters)
            };

            viewModelMain.TileViewModel = viewModel;
            
            return viewModelMain;
        }

        private static void SetSelectedRegionIds(CapacityParameters parameters, RegionIdsSelection regionIdsSelection)
        {
            var regions = RegionViewBL.GetSelectedCapacityRegions(parameters).Select(x => x.RegionID).ToArray();
            
            if (regionIdsSelection.HasAllRegionsSelected && regions.Any())
            {
                regionIdsSelection.SelectedRegionIds = regions;
            }
        }
        
        private static List<Option> GetRegionOptions(CapacityParameters parameters)
        {
            var regionOptions = new List<Option>();
            RegionViewBL.GetCapacityRegionsAll(parameters).ToList().ForEach(reg => regionOptions.Add(new Option(reg.Name, reg.RegionID.ToString(), (parameters.RegionIdsSelection.HasAllRegionsSelected || parameters.RegionIdsSelection.SelectedRegionIds.Contains(reg.RegionID)), AntiFiddleInjection.CreateDigest(reg.RegionID))));
            return regionOptions;
        }

        public static Dictionary<string, string> GetAfterCareTileRegionsPartial(CapacityParameters parameters)
        {
            var htmlReplaces = new Dictionary<string, string>();
            var viewModel = GetAfterCareTileResults(parameters);

            var organizationsHtml = parameters.Controller.RenderPartialViewToString(PathViews + "TileResults.cshtml", viewModel);
            htmlReplaces.Add("#CapacityTileViewResults", organizationsHtml);
                        
            return htmlReplaces;
        }

        public static CapacityTileRegionResultsViewModel GetAfterCareTileResults(CapacityParameters parameters)
        {
            var viewModel = new CapacityTileRegionResultsViewModel(parameters);

            if (parameters.ShowByRegion)
            {
                viewModel.AfterCareRegions = GetTilesGroupedByRegion(parameters);
            }
            else
            {
                viewModel.AfterCareRegions = GetTilesNationWide(parameters);
                if (!parameters.RegionIdsSelection.HasAllRegionsSelected)
                {
                    // This is when Region TOTALS are bundled. We still need the selected regions for the urls to the ListView page
                    viewModel.RegionIDsToQuery = parameters.RegionIdsSelection.RegionIDsToQuery;
                }
            }
            
            return viewModel;
        }
        
        public static CapacityManagementViewModel GetListViewModel(CapacityParameters parameters)
        {
            parameters.CurrentAction = TabMenuItems.ListView;
            var pager = GetPager(parameters.PageNo);
            var viewModelMain = GetCapacityViewModel(parameters); //TabMenuItems.ListView);
            if (!viewModelMain.TabMenu.UserHasViewRights)
            {
                return viewModelMain;
            }

            var viewModel = new CapacityListViewModel
            {
                Results = {IsSpecificSearch = parameters.IsSpecificSearch, ShowMutadedByUserFields = parameters.ShowMutadedByUserFields },
                Search = GetCapacitySearchViewModel(parameters, fillPostalCodeOptions: true)
            };
           
            if (parameters.IsSpecificSearch)
            {
                viewModel.Results.Items = GetDepartmentCapacityItems(parameters, pager);
            }

            viewModel.Results.Paging = new PagingViewModel(pager);          
            viewModelMain.ListViewModel = viewModel;

            return viewModelMain;
        }

        private static List<ValueAndNameOption> GetPostalCodeRadiusList()
        {
            return new[] { 1, 3, 5, 10, 15, 20 }
                .Select(radius => new ValueAndNameOption(radius.ToString(), $"< {radius} km")).ToList();
        }

        private static List<DepartmentCapacityItemViewModel> GetDepartmentCapacityItems(CapacityParameters parameters, Pager pager)
        {
            if (!parameters.SearchNonSpecificParameters.HasSearchParameters && !parameters.AfterCareGroupID.HasValue && !parameters.AfterCareTypeID.HasValue)
            {
                return new List<DepartmentCapacityItemViewModel>();
            }

            var capacityRegions = RegionViewBL.GetSelectedCapacityRegions(parameters);
            var regionIDsX = capacityRegions.Select(x => x.RegionID);


            var results = GetListViewModelByRegion(parameters, regionIDsX, pager);
            if (pager == null)
            {
                return results != null ? results.ToList() : new List<DepartmentCapacityItemViewModel>();
            }

            return results.ToList();
        }

        public static Dictionary<string, string> GetAfterCareListPartial(CapacityParameters parameters)
        {
            var pager = GetPager(parameters.PageNo);
            var htmlReplaces = new Dictionary<string, string>();

            var viewModel = new DepartmentCapacityResultsViewModel
            {
                IsSpecificSearch = true,
                Items = GetDepartmentCapacityItems(parameters, pager),
                ShowMutadedByUserFields = parameters.ShowMutadedByUserFields,
                Paging = new PagingViewModel(pager)
            };

            var organizationsHtml = parameters.Controller.RenderPartialViewToString(PathViews + "ListResults.cshtml", viewModel);
            htmlReplaces.Add("#CapacityListViewResults", organizationsHtml);

            return htmlReplaces;
        }

        public static CapacityAfterCareGroupsAndTypesViewModel GetCapacityAfterCareGroupsAndTypesViewModel(CapacityParameters parameters, string selectedValue)
        {
            const string midConnector = "├─";
            const string endConnector = "└─";
            var viewModel = new CapacityAfterCareGroupsAndTypesViewModel();
            var idNone = CapacityManagementConstants.AfterCareGroupOrTypeIdNone;
            string value;
            var afterCareGroups = AfterCareViewBL.GetAfterCareGroups(parameters.Uow).Result;
            var afterCareTypes = AfterCareViewBL.GetAfterCareTypes(parameters).Result;

            foreach (var group in afterCareGroups)
            {
                var types = afterCareTypes.Where(x => x.AfterCareGroupID == group.AfterCareGroupID).ToList();
                var connector = string.Empty;
                var count = 0;
                var selected = false;
                
                if (types.Count > 1)
                {
                    value = $"{group.AfterCareGroupID};{idNone}";
                    selected = value == selectedValue;
                    viewModel.AfterCareGroupsAndTypesVVT.Add(new Option(group.Name, value, selected));
                    connector = midConnector + " ";
                }

                foreach (var type in types)
                {
                    count++;
                    if (count > 1 && count == types.Count)
                    {
                        connector = endConnector + " ";
                    }

                    value = $"{group.AfterCareGroupID};{type.AfterCareTypeID}";

                    if (!selected && types.Count == 1)
                    {
                        selected = (selectedValue.StartsWith($"{group.AfterCareGroupID};"));
                    }
                    else
                    {
                        selected = (value == selectedValue);
                    }
                    
                    viewModel.AfterCareGroupsAndTypesVVT.Add(new Option($"{connector}{type.Name}", value, selected));
                }
            }

            // Hospital
            foreach (var type in afterCareTypes.Where(x => x.OrganizationTypeID == (int)OrganizationTypeID.Hospital))
            {
                value = $"{idNone};{type.AfterCareTypeID}";
                viewModel.AfterCareGroupsAndTypesHospital.Add(new Option($"{type.Name}", value, value == selectedValue));
            }

            return viewModel;
        }

        public static Dictionary<string, string> GetAfterCareGroupAndTypeListPartial(CapacityParameters parameters)
        {
            var htmlReplaces = new Dictionary<string, string>();
            var afterCareGroupIdAndTypeId = $"{parameters.AfterCareGroupID ?? 0};{parameters.AfterCareTypeID ?? 0}";        // TODO: Use a helper function
            var viewModel = GetCapacityAfterCareGroupsAndTypesViewModel(parameters, afterCareGroupIdAndTypeId);

            var html = parameters.Controller.RenderPartialViewToString(PathViews + "CapacitySearchAfterCareGroupsAndTypes.cshtml", viewModel);
            htmlReplaces.Add("#CapacitySearchAfterCareGroupsAndTypes", html);

            return htmlReplaces;
        }


        public static CapacityManagementViewModel GetEditViewModel(CapacityParameters parameters, CapacityEditViewModel viewModel)
        {
            parameters.CurrentAction = TabMenuItems.EditView;
            var viewModelMain = GetCapacityViewModel(parameters);

            var accessLevel = FunctionRoleHelper.GetAccessLevel(parameters.Uow, FunctionRoleTypeID.ManageCapacity, parameters.PointUserInfo, new List<int> { (int)OrganizationTypeID.HealthCareProvider, (int)OrganizationTypeID.Hospital });

            if (viewModel.Locations == null)
            {
                viewModel.Locations = new List<CapacityLocationViewModel>();
            }

            viewModelMain.TabMenu.IsAllowed = accessLevel.MaxLevel > FunctionRoleLevelID.None;

            var regions = RegionBL.GetByRegionIDs(parameters.Uow, accessLevel.RegionIDs).OrderBy(it => it.Name).ToList();

            if (regions.Any())
            {
                regions.ForEach(reg => viewModel.Regions.Add(new Option(reg.Name, reg.RegionID.ToString(), viewModel.RegionID.HasValue && reg.RegionID == viewModel.RegionID)));
                viewModel.Regions.Insert(0, LookUpBL.OptionEmpty);

                if (viewModel.RegionID == null && regions.Count == 1)
                {
                    viewModel.RegionID = regions.FirstOrDefault()?.RegionID;
                }
            }

            // Organizations
            if (viewModel.RegionID.HasValue || accessLevel.RegionIDs.Length == 1)
            {
                var organizations = OrganizationBL.GetOrganizationsWithCapacityEnabledByRegionID(parameters.Uow, viewModel.RegionID ?? accessLevel.RegionIDs.FirstOrDefault()).ToList();
                organizations = organizations.Where(org => accessLevel.MaxLevel > FunctionRoleLevelID.Organization || accessLevel.OrganizationIDs.Contains(org.OrganizationID)).OrderBy(it => it.Name).ToList();

                if (organizations.Any())
                {
                    organizations.ForEach(org => viewModel.Organizations.Add(new Option(org.Name, org.OrganizationID.ToString(), viewModel.OrganizationID.HasValue && org.OrganizationID == viewModel.OrganizationID)));
                    viewModel.Organizations.Insert(0, LookUpBL.OptionEmpty);

                    if (viewModel.OrganizationID == null && organizations.Count == 1)
                    {
                        viewModel.OrganizationID = organizations.FirstOrDefault()?.OrganizationID;
                    }
                }
            }

            if (!viewModel.OrganizationID.HasValue)
            {
                viewModelMain.EditViewModel = viewModel;
                return viewModelMain;
            }

            var organization = OrganizationBL.GetByOrganizationID(parameters.Uow, viewModel.OrganizationID.Value);
            viewModel.OrganizationTypeID = (OrganizationTypeID?)organization?.OrganizationTypeID;

            if (viewModel.AfterCareCategoryID == null && viewModel.AllowAfterCareCategorySelection)
            {
                viewModel.AfterCareCategoryID = (int)AfterCareCategoryID.Zorginstelling;
            }

            //Calculate filter for department
            var departmentIDs = DepartmentBL.GetAllDepartmentsByOrganizationID(parameters.Uow, organization.OrganizationID).Where(it => it.CapacityFunctionality).Select(it => it.DepartmentID).ToArray();

            switch (accessLevel.MaxLevel)
            {
                case FunctionRoleLevelID.Department:
                    departmentIDs = departmentIDs.Where(depID => parameters.PointUserInfo.EmployeeDepartmentIDs.Contains(depID)).ToArray();
                    break;
                case FunctionRoleLevelID.None:
                    departmentIDs = new int[0];
                    break;
            }

            var isHospital = organization.OrganizationID == (int)OrganizationTypeID.Hospital;

            //Calculate filter for location
            var locations = organization.Location.Where(l =>
                !l.Inactive &&
                l.OrganizationID == organization.OrganizationID &&
                l.Department.Any(d => (isHospital || d.AfterCareType1?.AfterCareCategoryID == viewModel.AfterCareCategoryID) && departmentIDs.Contains(d.DepartmentID))
            ).AsQueryable();

            if (locations == null || !locations.Any())
            {
                viewModelMain.EditViewModel = viewModel;
                return viewModelMain;
            }

            if (accessLevel.MaxLevel <= FunctionRoleLevelID.Location)
            {
                locations = locations.Where(loc => accessLevel.LocationIDs.Contains(loc.LocationID));
            }

            if (locations.Any())
            {
                viewModel.Locations = locations.Select(l => new CapacityLocationViewModel
                {
                    LocationID = l.LocationID,
                    Name = l.Name,
                    Departments = l.Department.Where(d =>
                            departmentIDs.Contains(d.DepartmentID) &&
                            ((isHospital || d.AfterCareType1ID != null && d.AfterCareType1.AfterCareCategoryID == viewModel.AfterCareCategoryID) && !d.AfterCareType1.Inactive)
                            ).Select(d => new DepartmentCapacityViewModel
                            {
                                Name = d.Name,
                                DepartmentID = d.DepartmentID,
                                Capacity1 = d.CapacityDepartment,
                                Capacity2 = d.CapacityDepartmentNext,
                                Capacity3 = d.CapacityDepartment3,
                                Capacity4 = d.CapacityDepartment4,
                                Information = d.Information,
                                LastUpdated = d.AdjustmentCapacityDate,
                                DateLastUpdated = d.AdjustmentCapacityDate != null ? $"{d.AdjustmentCapacityDate:dd-MM-yyyy H:mm}" : ""
                            })
                            .OrderBy(d => d.Name)
                            .ToList()
                })
                    .OrderBy(l => l.Name)
                    .ToList();
            }

            viewModelMain.EditViewModel = viewModel;
            return viewModelMain;
        }


        public static bool SaveEditViewModel(CapacityParameters parameters, CapacityEditViewModel viewModel)
        {
            if (viewModel.Locations == null)
            {
                viewModel.Locations = new List<CapacityLocationViewModel>();
            }

            if (!viewModel.Locations.Any()) return false;

            var accessLevel = FunctionRoleHelper.GetAccessLevel(parameters.Uow, FunctionRoleTypeID.ManageCapacity, parameters.PointUserInfo, new List<int> { (int)OrganizationTypeID.HealthCareProvider, (int)OrganizationTypeID.Hospital });
            var allDepartments = DepartmentBL.GetByDepartmentIDs(parameters.Uow, viewModel.Locations.SelectMany(it => it.Departments.Select(dep => dep.DepartmentID)).ToArray());
            var isDepartmentMaxLevel = accessLevel.MaxLevel == FunctionRoleLevelID.Department;
            var needSave = false;

            foreach (var location in viewModel.Locations)
            {
                foreach (var departmentViewModel in location.Departments)
                {
                    //OWASP safety
                    if (isDepartmentMaxLevel && !parameters.PointUserInfo.EmployeeDepartmentIDs.Contains(departmentViewModel.DepartmentID))
                    {
                        continue;
                    }

                    var department = allDepartments.FirstOrDefault(it => it.DepartmentID == departmentViewModel.DepartmentID);

                    DateTime date;
                    DateTime? adjustmentCapacityDate = null;
                    if (DateTime.TryParse(departmentViewModel.DateLastUpdated, out date))
                    {
                        adjustmentCapacityDate = date;
                    }

                    // TODO: Disable this when DepartmentCapacity no longer needed!
                    var departmentUpdated = DepartmentBL.UpdateDepartmentCapacity(parameters.Uow, department, departmentViewModel.Capacity1, departmentViewModel.Capacity2, departmentViewModel.Capacity3, departmentViewModel.Capacity4, departmentViewModel.Information, parameters.PointUserInfo.Employee.UserName, parameters.PointUserInfo?.Employee?.EmployeeID, adjustmentCapacityDate);

                    if (departmentUpdated)
                    {
                        needSave = true;
                        DepartmentCapacityPublicBL.UpsertByDepartment(parameters.Uow, department, parameters.PointUserInfo.Employee.UserName);
                    }
                }
            }

            if (needSave)
            {
                parameters.Uow.Save();
            }
            
            return true;
        }

        private static void AddLocalMenuItem(Controller controller, List<LocalMenuItem> items, string currentAction, string action, string label)
        {
            var item = new LocalMenuItem
            {
                Action = action,
                Controller = "Capacity",
                Description = $"Capaciteit {label}",
                IsCurrent = currentAction == action, 
                Label = label,
                Url = controller.Url.Action(action)
            };
            items.Add(item);
        }

        private static bool IsUserAllowedToViewTab(CapacityParameters parameters)
        {
            return parameters.PointUserInfo.IsRegioAdmin || (parameters.IsCapacityPublicVersion ? parameters.PointUserInfo.Region.CapacityBedsPublic : parameters.PointUserInfo.Region.CapacityBeds) ||
                   parameters.PointUserInfo.Organization.OrganizationTypeID == (int)OrganizationTypeID.HealthCareProvider ||
                   parameters.PointUserInfo.Organization.OrganizationTypeID == (int)OrganizationTypeID.GeneralPracticioner;
        }

        private static TabMenu GetTabMenuItems(CapacityParameters parameters)
        {
            var tabMenu = new TabMenu
            {
                UserHasViewRights = IsUserAllowedToViewTab(parameters),
                UserHasEditRights = parameters.PointUserInfo.HasFunctionRole(FunctionRoleTypeID.ManageAll, FunctionRoleTypeID.ManageCapacity)
            };

            if (tabMenu.UserHasViewRights)
            {
                AddLocalMenuItem(parameters.Controller, tabMenu.MenuItems, parameters.CurrentAction, TabMenuItems.TileView, "Tegelweergave");
                AddLocalMenuItem(parameters.Controller, tabMenu.MenuItems, parameters.CurrentAction, TabMenuItems.ListView, "Lijstweergave");
            }

            if (tabMenu.UserHasEditRights)
            {
                AddLocalMenuItem(parameters.Controller, tabMenu.MenuItems, parameters.CurrentAction, TabMenuItems.EditView, "Bewerken");
            }

            tabMenu.IsAllowed = parameters.CurrentAction == TabMenuItems.EditView ? tabMenu.UserHasEditRights : tabMenu.UserHasViewRights;

            return tabMenu;
        }

        public static List<CapacityRegionViewModel> GetTilesNationWide(CapacityParameters parameters)
        {
            var viewModel = new List<CapacityRegionViewModel>
            {
                new CapacityRegionViewModel()
            };

            var capacityRegions = RegionViewBL.GetSelectedCapacityRegions(parameters);
            var regionIDs = capacityRegions.Select(x => x.RegionID);

            return DepartmentCapacityPublicViewBL.GetDepartmentCapacityPublicTiles(viewModel, parameters, regionIDs);
        }

        public static List<CapacityRegionViewModel> GetTilesGroupedByRegion(CapacityParameters parameters)
        {
            var capacityRegions = RegionViewBL.GetSelectedCapacityRegions(parameters);
            var regionIDs = capacityRegions.Select(x => x.RegionID);

            return DepartmentCapacityPublicViewBL.GetDepartmentCapacityPublicTiles(capacityRegions, parameters, regionIDs);
        }

        public static IEnumerable<DepartmentCapacityItemViewModel> GetListViewModelByRegion(CapacityParameters parameters, IEnumerable<int> regionIDs, Pager pager, string orderBy = "OrganizationName,LocationName,DepartmentName")
        {
            if (parameters.RegionIdsSelection.SelectedRegionIds == null)
            {
                return null;
            }

            if (parameters.AfterCareTypeID.HasValue)
            {
                parameters.AfterCareGroupID = null;
            }

            return DepartmentCapacityPublicViewBL.GetDepartmentCapacityPublicList(parameters, regionIDs, pager, orderBy);
        }

        private static Pager GetPager(int pageNo)
        {
            return pageNo > _noPagerNo ? new Pager { PageSize = 30, PageNo = pageNo } : null;
        }

        public static CapacityManagementViewModel GetCapacityMapViewModel(CapacityParameters parameters)
        {
            parameters.CurrentAction = TabMenuItems.MapView;
            var viewModelMain = GetCapacityViewModel(parameters); 
            if (!viewModelMain.TabMenu.UserHasViewRights)
            {
                return viewModelMain;
            }

            var viewModel = new CapacityMapViewModel
            {
                Results = { IsSpecificSearch = parameters.IsSpecificSearch },
                Search = GetCapacitySearchViewModel(parameters)
            };

            if (parameters.IsSpecificSearch)
            {
                viewModel.Results.Items = GetDepartmentCapacityItems(parameters, null);
            }

            viewModelMain.MapViewModel = viewModel;           

            if (viewModel.Results != null)
            {
                viewModel.DepartmentCapacityGPSList = DepartmentCapacityPublicViewBL.GetLocationMapMarkers(viewModel.Results.Items, parameters);
            }

            return viewModelMain;
        }

        private static CapacitySearchViewModel GetCapacitySearchViewModel(CapacityParameters parameters, bool fillPostalCodeOptions = false)
        {
            var viewModel = new CapacitySearchViewModel
            {
                IsHospital = parameters.IsHospitalSearch,
                AfterCareGroupID = parameters.AfterCareGroupID,
                AfterCareTypeID = parameters.AfterCareTypeID,
                WithCapacityOnly = parameters.WithCapacityOnly,
                ShowByRegion = parameters.ShowByRegion,
                Regions = GetRegionOptions(parameters),
                RegionIdsSelection = parameters.RegionIdsSelection,
                // For listView
                Name = parameters.SearchNonSpecificParameters.OrgLocDepName,
                City = parameters.SearchNonSpecificParameters.City,
                GetResultsUrl = parameters.GetResultsUrl,
                GetAfterCareGroupsAndTypesUrl = parameters.GetAfterCareGroupsAndTypesUrl,
                CurrentAction = parameters.CurrentAction,
                IsCapacityPublicVersion = parameters.IsCapacityPublicVersion
            };

            viewModel.AfterCareGroupsAndTypes = GetCapacityAfterCareGroupsAndTypesViewModel(parameters, viewModel.AfterCareGroupIdAndTypeId);
            viewModel.OrganizationTypes.Add(new Option("VVT", (int)OrganizationTypeID.HealthCareProvider, !parameters.IsHospitalSearch));
            viewModel.OrganizationTypes.Add(new Option("ZH", (int)OrganizationTypeID.Hospital, parameters.IsHospitalSearch));

            // Restore RegionIdsSelection
            SetSelectedRegionIds(parameters, viewModel.RegionIdsSelection);

            if (fillPostalCodeOptions)
            {
                viewModel.PostalCode = parameters.SearchNonSpecificParameters.PostalCode;
                viewModel.PostalCodeRadiusList = GetPostalCodeRadiusList();
                viewModel.PostalCodeRadius = parameters.SearchNonSpecificParameters.PostalCodeRadius;
            }

            return viewModel;
        }

        public static Dictionary<string, string> GetCapacityMapPartial(CapacityParameters parameters)
        {
            var htmlReplaces = new Dictionary<string, string>();
            var viewModel = GetCapacityMapViewModel(parameters);

            var mapViewResultsHtml = parameters.Controller.RenderPartialViewToString(PathViews + "MapResults.cshtml", viewModel.MapViewModel.DepartmentCapacityGPSList);
            htmlReplaces.Add("#MapViewResults", mapViewResultsHtml);

            return htmlReplaces;
        }

    }
}