﻿using System;
using System.IO;
using System.Web.Mvc;
using Point.Database.Models.ViewModels;
using Point.Infrastructure.Extensions;
using Point.Log4Net;

namespace Point.MVC.Shared.Helpers
{
    public static class LogHelper
    {
        public static LogParameters GetLogParameters(Controller controller, PointUserInfo pointUserInfo, string message, Exception exception = null)
        {
            if (string.IsNullOrEmpty(message))
            {
                // TODO: Determine what to set here when empty
                message = controller.GetType().Name;
            }

            //var pointUserInfo = PointUserInfo();
            int? employeeID = pointUserInfo?.Employee?.EmployeeID;
            var employeeUserName = pointUserInfo?.Employee?.UserName;

            var postData = string.Empty;
            var cookiesString = string.Empty;
            var isAjaxRequest = false;
            var userAgent = string.Empty;
            var queryString = string.Empty;
            var url = string.Empty;


            var httpContext = controller.HttpContext;
            if (httpContext != null)
            {
                var request = httpContext.Request;
                {
                    if (request.Url != null)
                    {
                        var uri = request.Url;
                        url = uri.AbsolutePath;
                        queryString = uri.Query;
                    }

                    userAgent = request.UserAgent ?? "";
                    isAjaxRequest = request.IsAjaxRequest();

                    var cookies = request.Cookies;
                    if (cookies != null)
                    {
                        foreach (var cookieKey in cookies.AllKeys)
                        {
                            if (cookieKey == ".POINTAUTH" || cookieKey == ".POINTROLES")
                            {
                                continue;
                            }

                            var httpCookie = cookies[cookieKey];
                            if (httpCookie != null)
                            {
                                cookiesString += $"{cookieKey}={httpCookie.Value}&";
                            }
                        }
                    }

                    if (httpContext.Request?.InputStream != null)
                    {
                        var streamReader = new StreamReader(httpContext.Request.InputStream);
                        postData = RemovePlainPassword(streamReader.ReadToEnd());
                    }
                    else if (request.Params?.Count > 0)
                    {
                        const string passwordSign = "password";

                        foreach (var parameter in request.Params)
                        {
                            if (parameter.ToString().ToLowerInvariant() == passwordSign)
                            {
                                continue;
                            }

                            postData += $"{parameter},";
                        }
                    }
                    postData = postData.TrimEnd(',');
                }
            }

            return new LogParameters
            {
                Message = message,
                TransferID = controller.Request.GetRequestVariable<int?>("transferid", null),
                MachineName = Environment.MachineName,
                WindowsIdentity = Environment.UserName,
                EmployeeID = employeeID,
                Exception = exception,
                IsAjaxRequest = isAjaxRequest,
                PostData = postData,
                Cookies = cookiesString,
                UserAgent = userAgent,
                QueryString = queryString,
                Url = url,
                EmployeeUserName = employeeUserName
            };
        }

        private static string RemovePlainPassword(string postData = null)
        {
            if (string.IsNullOrEmpty(postData)) return postData;

            const string passwordSign = "Password=";
            var postParams = postData.Split('&');
            foreach (var param in postParams)
            {
                // if postdata contains (exactly) 'Password' param : 
                if (param.IndexOf(passwordSign, StringComparison.InvariantCultureIgnoreCase) != 0)
                {
                    continue;
                }
                postData = postData.Replace("&" + param, "");
                break;
            }
            return postData;
        }

    }
}