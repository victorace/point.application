﻿using System.IO;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace Point.MVC.Shared.Helpers
{
    public static class ViewRenderingHelper
    {
        public static string RenderPartialViewToString(this Controller controller, string viewName, object model)
        {
            if (controller == null || controller.ControllerContext == null)
            {
                return string.Empty;
            }

            controller.ViewData.Model = model;

            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName);

                var viewContext = new ViewContext(
                    controller.ControllerContext,
                    viewResult.View,
                    controller.ViewData,
                    controller.TempData,
                    sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }

        public static void RenderPartialIfNotNull(this HtmlHelper htmlHelper, string partialViewName, object model)
        {
            if (model != null)
            {
                htmlHelper.RenderPartial(partialViewName, model);
            }
        }
    }
}