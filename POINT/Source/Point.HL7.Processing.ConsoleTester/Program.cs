﻿using Point.Database.Context;
using Point.HL7.Processing.Context;
using Point.HL7.Processing.Helpers;
using System;
using System.Data.Entity.Infrastructure.Interception;

namespace Point.HL7.Processing.ConsoleTester
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Data.Entity.Database.SetInitializer<HL7Context>(null);
            var useReadUncommited = ConfigHelper.GetAppSettingByName<bool>("UseReadUncommited");

            DbInterception.Add(new IsolationLevelInterceptor(useReadUncommited));
            var hl7servicehost = new HL7ServiceHost();
            hl7servicehost.Start();

            Console.ReadKey();

        }
    }
}
