﻿using DevTrends.MvcDonutCaching;
using Point.Business.Logic;
using Point.Database.Models.ViewModels;
using System.Linq;
using System.Web.Mvc;
using Point.Infrastructure.Constants;

namespace Point.MVC.WebApplication.Controllers
{
    [DonutOutputCache(CacheProfile = "Short")]
    public class SystemMessageController : PointBaseController
    {
        // GET: SystemMessage

        public PartialViewResult Status()
        {
            var userinfo = PointUserInfo();
            if (userinfo?.Employee == null) return PartialView();

            var latestmessage = SystemMessageBL.GetLatest(uow, userinfo.Employee.EmployeeID).FirstOrDefault();

            var viewmodel = new SystemMessageOverviewViewModel();
            viewmodel.LastSystemMessageID = latestmessage?.SystemMessageID;
            viewmodel.LastSystemMessage = latestmessage?.Title;
            viewmodel.ShowPopup = Session[SessionIdentifiers.SystemMessage.PopupSeen] == null;

            return PartialView(viewmodel);
        }

        public JsonResult Recent()
        {
            var userinfo = PointUserInfo();
            var messages = SystemMessageBL.GetLatest(uow, userinfo.Employee.EmployeeID);
            var data = SystemMessageViewBL.FromModel(messages);

            Session[SessionIdentifiers.SystemMessage.PopupSeen] = true;

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult MarkMessageRead()
        {
            var userinfo = PointUserInfo();
            SystemMessageBL.InsertSystemMessageRead(uow, userinfo.Employee.EmployeeID);

            return Json(new { Result = "OK" }, JsonRequestBehavior.AllowGet);
        }
    }
}