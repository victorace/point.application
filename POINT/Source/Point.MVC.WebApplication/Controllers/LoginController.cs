﻿using Point.Business.BusinessObjects;
using Point.Business.Helper;
using Point.Business.Logic;
using Point.Communication;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Infrastructure.Attributes;
using Point.Infrastructure.Extensions;
using Point.MVC.WebApplication.Extensions;
using Point.RADIUS.Client;
using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using Point.Infrastructure;


namespace Point.MVC.WebApplication.Controllers
{

    [Authorize]
    public partial class LoginController : PointController
    {

        private const string previousUrl = "previousUrl";
        private const string ismfaredirect = "isMFARedirect";
        private const string SSOMessage = "SSOMessage";

        [AllowAnonymous]
        public ActionResult Login()
        {
            return Redirect(FormsAuthentication.LoginUrl);
        }

        public ActionResult LogOff()
        {
            ExternalPatientHelper.ClearPatExternalReference(false);
            ExternalPatientHelper.ClearPatExternalReferenceBSN(false);
            ExternalPatientHelper.ClearIsAuth();
            SearchControlBL.DeleteState();
            PointUserInfoHelper.ClearCache();
            LoginBL.ClearIsSSO();
            SetTempDataMFA(false);
            Session.Abandon();
            FormsAuthentication.SignOut();
            return Redirect(FormsAuthentication.LoginUrl);
        }

        [AllowAnonymous]
        public ActionResult PrivacyInstruction()
        {
            return PartialView();
        }

        [AllowAnonymous]
        public ActionResult MoreInfo()
        {
            return PartialView();
        }

        [AllowAnonymous]
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult SSO()
        {
            return Index();
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult Index()
        {
            LoginBL.CheckAndSetIsSSO(Request);

            var viewmodel = new LoginViewModel();
            if (LoginBL.GetIsSSO())
            {
                viewmodel = ValidateSSOAndGetViewModel();
                if(viewmodel.SSOSucceeded == true)
                {
                    return DetermineRedirect(viewmodel);
                }
            }

            return View("Index", viewmodel);
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Index(LoginViewModel viewmodel)
        {
            if (ModelState.IsValid)
            {
                var membershipuser = Membership.GetUser(viewmodel.Username);

                PointUserInfo pointUserInfo = null;
                bool validUser = false;
                string loginerror = "";

                if (membershipuser != null)
                {
                    if (LoginBL.IsRadiusRequest(viewmodel))
                    {
                        validUser = isValidRadius(viewmodel, membershipuser);
                    }
                    else
                    {
                        validUser = Membership.ValidateUser(viewmodel.Username, viewmodel.Password);
                    }

                    if (validUser)
                    {
                        pointUserInfo = PointUserInfoHelper.GetPointUserInfo(uow, new Guid(membershipuser.ProviderUserKey.ToString()));

                        if (pointUserInfo == null)
                        {
                            loginerror = "Aanmelden was niet succesvol. Probeert u het a.u.b. nogmaals.";
                        }
                        if (String.IsNullOrEmpty(loginerror))
                        {
                            loginerror = LoginBL.CheckOrganization(pointUserInfo);
                        }
                        if (String.IsNullOrEmpty(loginerror))
                        {
                            loginerror = LoginBL.CheckPatExternalReference(Request);
                        }
                        if (String.IsNullOrEmpty(loginerror))
                        {
                            string clientip = LoginBL.GetClientIP(Request);
                            loginerror = LoginBL.CheckIP(pointUserInfo, clientip);
                        }
                        if (String.IsNullOrEmpty(loginerror))
                        {
                            loginerror = LoginBL.CheckLockedOut(membershipuser);
                        }
                    }
                    else
                    {
                        if (membershipuser.IsLockedOut)
                        {
                            EmployeeBL.LogTooManyBadLogins(uow, membershipuser);

                            loginerror = "Uw account is geblokkeerd. Mogelijk heeft u te vaak geprobeerd aan te melden met een fout wachtwoord.";

                            var emp = EmployeeBL.GetByUserName(uow, membershipuser.UserName);

                            if (emp != null && ( !emp.AutoLockAccountDate.HasValue || emp.AutoLockAccountDate > DateTime.Now))
                            {
                                var org = OrganizationBL.GetByEmployeeID(uow, emp.EmployeeID);
                                if ((org.DeBlockEmailsToOrganization == true && !string.IsNullOrEmpty(org.DeBlockEmails)) || (EmployeeBL.GetAdminsEmails(uow, emp.EmployeeID) != null))
                                {
                                    viewmodel.SendDeblockEmail = true;
                                }
                            }
                        }
                        else
                        {
                            loginerror = "Aanmelden was niet succesvol. Probeert u het a.u.b. nogmaals.";
                        }
                    }
                }
                else
                {
                    loginerror = "U kan niet worden aangemeld. Neem s.v.p contact op met uw regiobeheerder.";
                }

                if (!String.IsNullOrEmpty(loginerror))
                {
                    validUser = false;
                    ModelState.AddModelError("", loginerror);
                }
                
                logLogin(viewmodel, validUser, membershipuser, string.IsNullOrEmpty(loginerror), pointUserInfo);
                
                finishLogin(validUser, membershipuser);

                if (ModelState.IsValid)
                {
                    LoginBL.GetAndSetPatExternalReference(Request);
                    LoginBL.GetAndSetPatExternalReferenceBSN(Request);
                    LoginBL.SetDepartmentReference(uow, Request, pointUserInfo);

                    string returnurl = Request.Params.GetValueOrDefault<string>("ReturnUrl", "");
                    if (!String.IsNullOrEmpty(returnurl) && Url.IsLocalUrl(returnurl) && returnurl != "/")
                    {
                        return new RedirectResult(returnurl);
                    }

                    return DetermineRedirect(viewmodel);
                }
            }

            return View(viewmodel);
        }

        private void Membership_ValidatingPassword(object sender, ValidatePasswordEventArgs e)
        {
            var test = e.FailureInformation;
        }

        private void logLogin(LoginViewModel viewmodel, bool validUser, MembershipUser membershipUser, bool noLoginError , PointUserInfo pointUserInfo = null)
        {
            LoginHistoryBL.InsertLoginHistory(uow, viewmodel, HttpContext.Request, validUser, pointUserInfo);

            if (validUser && noLoginError)
            {
                membershipUser.LastActivityDate = DateTime.Now;
                membershipUser.LastLoginDate = DateTime.Now;
                Membership.UpdateUser(membershipUser);
            }
        }

        private void finishLogin(bool validUser, MembershipUser membershipUser)
        {
            if (validUser)
            {
                FormsAuthentication.SetAuthCookie(membershipUser.UserName, false);
                PointUserInfoHelper.SaveProviderUserKey((Guid)membershipUser.ProviderUserKey);
            }
        }

        private ActionResult DetermineRedirect(LoginViewModel loginViewModel)
        {
            if(!string.IsNullOrEmpty(loginViewModel.ReturnUrl))
            {
                return Redirect(loginViewModel.ReturnUrl);
            }

            var routevalues = new RouteValueDictionary();
            string actionName = "Search";
            string controllerName = "Transfer";

            if (IsMobileDevice())
            {
                actionName = "Overview";
            }

            return RedirectToAction(actionName, controllerName, routevalues);
        }

        private bool isValidRadius(LoginViewModel viewmodel, MembershipUser user)
        {
            bool validradius = false;
            var userinfo = PointUserInfoHelper.GetPointUserInfo(uow, new Guid(user.ProviderUserKey.ToString()));
            if (userinfo != null)
            {
                var organizationID = userinfo.Organization?.OrganizationID;
                if (organizationID.HasValue)
                {
                    var rrp = new RADIUSForPoint(viewmodel.Username, viewmodel.Password, organizationID.Value);

                    validradius = rrp.AuthenticatePointUser();
                }
            }
            return validradius;
        }

        private LoginViewModel ValidateSSOAndGetViewModel()
        {
            var loginviewmodel = new LoginViewModel();
            loginviewmodel.SSOSucceeded = false;

            var ssologinbo = new SSOLoginBO().FromRequest(Request);

            if (SecurityBL.ValidateUsingHash(uow, ssologinbo))
            {
                PointUserInfo pointuserinfo = SecurityBL.GetUserUsingSSO(uow, ssologinbo.ExternalReference, ssologinbo.OrganizationID());

                if (pointuserinfo == null && !String.IsNullOrEmpty(ssologinbo.EmplReference))
                {
                    pointuserinfo = SecurityBL.CreateNewAccountSSO(uow, ssologinbo);
                }

                SecurityBL.InvalidateHashCode(uow, ssologinbo.Sign, ssologinbo.IsAuth, pointuserinfo);

                if (pointuserinfo != null)
                {
                    MembershipUser membershipuser = Membership.GetUser(pointuserinfo.Employee.UserID);
                    bool validUser = (pointuserinfo != null && membershipuser != null && !membershipuser.IsLockedOut);
                    if (validUser)
                    {
                        LoginBL.GetAndSetPatExternalReference(Request);
                        LoginBL.GetAndSetPatExternalReferenceBSN(Request);
                        LoginBL.SetDepartmentReference(uow, Request, pointuserinfo);

                        loginviewmodel.SSOSucceeded = true;
                        loginviewmodel.Username = pointuserinfo.Employee.UserName;
                        loginviewmodel.ReturnUrl = ssologinbo.ReturnUrl;

                        logLogin(loginviewmodel, validUser, membershipuser, true, pointuserinfo);
                    }
                    finishLogin(validUser, membershipuser);
                } 
            }

            return loginviewmodel;
        }
        
        public ActionResult LoginMFASMS()
        {
            PageHeader = "Extra beveiliging bij inloggen in POINT";

            var mfaviewmodel = UsedMFACodeBL.GetLoginMFAByPointUserInfo(uow, PointUserInfo());

            mfaviewmodel.MFACode = UsedMFACodeBL.GetRandomMFACode();
            UsedMFACodeBL.CreateMFACode(uow, mfaviewmodel, MFACodeType.Login);

            new MessageBird().SendMessage(mfaviewmodel.MFANumber, String.Format("Uw beveiligingscode is: {0}", mfaviewmodel.MFACode));

            ViewBag.MFADigest = AntiFiddleInjection.CreateDigest(mfaviewmodel.MFACode);
            ViewData.SetIsMFAScreen(true);

            mfaviewmodel.MFACode = ""; // remove it from the viewmodel as it as sent to the client

            return View(mfaviewmodel);
        }

        public ActionResult LoginMFAGoogle()
        {
            PageHeader = "Extra beveiliging bij inloggen in POINT";

            var mfaviewmodel = UsedMFACodeBL.GetLoginMFAByPointUserInfo(uow, PointUserInfo());

            mfaviewmodel.MFACode = ""; 
            UsedMFACodeBL.CreateMFACode(uow, mfaviewmodel, MFACodeType.Login);

            ViewBag.MFADigest = AntiFiddleInjection.CreateDigest(mfaviewmodel.MFACode);
            ViewData.SetIsMFAScreen(true);

            return View(mfaviewmodel);
        }

        [HttpPost]
        public JsonResult CheckMFACode(MFAViewModel mfaViewModel, string mfaDigest)
        {
            if (mfaViewModel.MFACode != AntiFiddleInjection.GetPlainDigest(mfaDigest))
            {
                return Json(new { result = false });
            }

            bool checkmfacode = UsedMFACodeBL.CheckLoginMFACode(uow, mfaViewModel);

            return Json(new { result = checkmfacode });
        }

        [HttpPost]
        public JsonResult CheckMFAGoogle(MFAViewModel mfaViewModel)
        {
            var usedmfacode = UsedMFACodeBL.GetUsedMFACodeByEmployeeID(uow, mfaViewModel.EmployeeID, MFACodeType.Login);
            mfaViewModel.MFAKey = usedmfacode.MFAKey;
            bool checkmfacode = new GoogleAuth().CheckMFAViewModel(mfaViewModel);

            if (!checkmfacode)
            {
                return Json(new { result = false });
            }

            UsedMFACodeBL.CheckLoginMFAKey(uow, usedmfacode);

            return Json(new { result = checkmfacode });

        }

        public ActionResult DispatchFromMFA()
        {
            string url = "";
            if (Session[previousUrl] != null)
            {
                url = Session[previousUrl].ToString();
                Session.Remove(previousUrl);
            }
            if (String.IsNullOrEmpty(url))
            {
                url = "~/";
            }
            return Redirect(url);
        }

        public void SetPreviousUrl(HttpContextBase httpContextBase)
        {
            if (httpContextBase?.Session != null)
            {
                if (httpContextBase.Session[previousUrl] != null)
                {
                    httpContextBase.Session.Remove(previousUrl);
                }
            }
            if (httpContextBase != null && httpContextBase.Request != null)
            {
                httpContextBase.Session[previousUrl] = httpContextBase.Request.Url;
            }
        }

        public void SetTempDataMFA(bool value)
        {
            TempData[ismfaredirect] = value;
        }

        public bool GetTempDataMFA()
        {
            return TempData[ismfaredirect].ConvertTo<bool>();
        }

        
        [AllowAnonymous]
        public ActionResult SendEmailToAdmin(string username)
        {
            var viewmodel = new LoginViewModel();
            try
            {
                var emp = EmployeeBL.GetByUserName(uow, username);
                if (emp != null && !string.IsNullOrEmpty(emp.aspnet_Users.UserName))
                {

                    var org = OrganizationBL.GetByEmployeeID(uow, emp.EmployeeID);
                    var emails = EmployeeBL.GetAdminsEmails(uow, emp.EmployeeID);
                    if (org.DeBlockEmailsToOrganization == true && !string.IsNullOrEmpty(org.DeBlockEmails))
                    {
                        var orgEmails = org.DeBlockEmails.Split(';').ToList();
                        viewmodel.DeblockMessage = Communication.Infrastructure.Helpers.UnblockUser.SendUnblockEmailToOrganizationEmails(orgEmails, emp);
                    }
                    else if (emails.Any())
                    {
                        var empAdmins = emails.Take(2)?.ToList();
                        viewmodel.DeblockMessage = Communication.Infrastructure.Helpers.UnblockUser.SendUnblockEmailToAdmins(empAdmins, emp);
                    }
                    else
                    {
                        viewmodel.DeblockMessage = "E-mailadres van beheerder is niet beschikbaar!";
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHelper.SendMessageException(GetLogParameters("Neem contact op met uw beheerder. De e-mail kan niet naar de beheerder verzonden worden.", ex));
                ModelState.AddModelError("", "Neem contact op met uw beheerder. De e-mail kan niet naar de beheerder verzonden worden.");
            }
            return View("Index", viewmodel);
        }

        [AllowAnonymous]
        public ActionResult LoginHistory()
        {
            var systemMessageLoginHistoryViewModel = LoginHistoryViewBL.GetLoginHistoryViewModel(uow, PointUserInfo());

            if (systemMessageLoginHistoryViewModel == null || !systemMessageLoginHistoryViewModel.Items.Any()) return null;

            return PartialView(systemMessageLoginHistoryViewModel);
        }

    }
}