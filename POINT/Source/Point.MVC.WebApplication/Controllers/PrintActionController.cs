﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Point.Business.Logic;
using Point.Database.Models.ViewModels;
using Point.Infrastructure.Extensions;
using Point.MVC.WebApplication.Extensions;
using Point.MVC.WebApplication.Helpers.Pdf;
using Point.Database.Models;
using System.Web;
using System.IO;
namespace Point.MVC.WebApplication.Controllers
{
    public class PrintActionController : PointFormController
    {
        public PrintActionController()
        {
            PageTitle = "Printen";
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult PrintHtml(string html, string orientation = "Portrait", bool urlDecoded = true)
        {
            if (!Enum.TryParse(orientation, true, out Business.Pdf.GeneratePDF.Orientation printorientation))
            {
                printorientation = Business.Pdf.GeneratePDF.Orientation.Portrait;
            }

            if (urlDecoded)
            {
                html = HttpUtility.UrlDecode(html);
            }

            var pdf = new ViewAsPdf(html as object)
            {
                FormName = "Zoekresultaat",
                Orientation = printorientation
            };
            return pdf;
        }

        public ActionResult Print()
        {
            var errorMessage = Request.GetParam("ErrorMessage");

            var formtypes = PrintListBL.GetByTransferID(uow, TransferID, PointUserInfo());

            formtypes.ErrorMessage = errorMessage;

            return new SecureActionResult(formtypes);
        }

        public ActionResult PrintSingle(int formsetVersionID, bool viewashtml = false)
        {
            var pointuserinfo = PointUserInfo();

            var model = new PrintMultipleViewModel
            {
                FormSetVersions = new List<FormSetVersionContainer>(),
                TransferID = TransferID
            };

            var formsetversion = FormSetVersionBL.GetByID(uow, Convert.ToInt32(formsetVersionID));
            if (formsetversion == null)
            {
                return null;
            }

            var headermodel = ClientHeaderViewBL.GetViewModel(uow, TransferID, pointuserinfo.Organization?.OrganizationID);
            var clientpartial = this.RenderViewToString("Header", headermodel);
            var footerpartial = this.RenderViewToString("Footer", null, true);

            var formtype = FormTypeBL.GetByFormTypeID(uow, formsetversion.FormTypeID);

            var cont = new FormSetVersionContainer
            {
                PrintName = formsetversion.GetPrintName(),
                FormsetVersionID = formsetversion.FormSetVersionID,
                PhaseDefinitionID = formsetversion.PhaseInstance?.PhaseDefinitionID ?? -1,
                PhaseInstanceID = formsetversion.PhaseInstanceID ?? -1,
                FormTypeID = formsetversion.FormTypeID,
                ActionName = formtype.GetActionName(),
                ControllerName = formtype.GetControllerName(),
                EmployeeID = pointuserinfo.Employee.EmployeeID
            };

            model.FormSetVersions.Add(cont);

            if (viewashtml)
            {
                return new SecureActionResult("PrintMultiple", model);
            }

            return new ViewAsPdf("PrintMultiple", model, header: clientpartial, footer: footerpartial)
            {
                TransferID = TransferID,
                FormName = formtype.Name
            };
        }

        public ActionResult PrintMultiple(string checkedFormsetVersions, string checkedFormTypes, string combinedFormsetIDs, string checkedAttachments, bool viewashtml = false)
        {
            DisableClientCache = true;

            var model = new PrintMultipleViewModel
            {
                FormSetVersions = new List<FormSetVersionContainer>(),
                Attachments = new List<TransferAttachment>(),
                TransferID = TransferID
            };

            var headermodel = ClientHeaderViewBL.GetViewModel(uow, TransferID, PointUserInfo().Organization?.OrganizationID);
            var clientpartial = this.RenderViewToString("Header", headermodel);
            var footerpartial = this.RenderViewToString("Footer", null, true);

            if (!string.IsNullOrEmpty(checkedFormsetVersions))
            {
                model.FormSetVersions.AddRange(CreateCheckedFormSetVersionContainers(checkedFormsetVersions, combinedFormsetIDs));
            }

            if (!string.IsNullOrEmpty(checkedFormTypes))
            {
                model.FormSetVersions.AddRange(CreateCheckedFormTypeContainers(checkedFormTypes));
            }

            if (!string.IsNullOrEmpty(checkedAttachments))
            {
                model.Attachments.AddRange(CreateCheckedAttachments(checkedAttachments));
            }

            if (viewashtml)
            {
                return new SecureActionResult(model);
            }
            
            return new ViewAsPdf("PrintMultiple", model, header: clientpartial, footer: footerpartial)
            {
                TransferID = TransferID,
                FormName = "Meerdere"
            };
        }

        private IEnumerable<FormSetVersionContainer> CreateCheckedFormSetVersionContainers(string checkedFormsetVersions, string combinedFormsetIDs)
        {
            var combinedFormsetIDList = combinedFormsetIDs.ToIntList().ToList();
            var formSetVersionIDList = checkedFormsetVersions.ToIntList();

            var list = new List<FormSetVersionContainer>();

            if (!formSetVersionIDList.Any()) return list;

            var pointUserInfo = PointUserInfo();

            foreach (var formsetversionid in formSetVersionIDList)
            {
                var formsetversion = FormSetVersionBL.GetByTransferIDAndFormsetVersionID(uow, TransferID, formsetversionid);

                if (formsetversion == null)
                {
                    continue;
                }

                var formtype = FormTypeBL.GetByFormTypeID(uow, formsetversion.FormTypeID);

                if (formtype == null) continue;

                list.Add(new FormSetVersionContainer
                {
                    PrintName = formsetversion.GetPrintName(),
                    IsCombined = combinedFormsetIDList.Contains(formsetversion.FormSetVersionID),
                    FormsetVersionID = formsetversion.FormSetVersionID,
                    PhaseDefinitionID = formsetversion.PhaseInstanceID.HasValue ? formsetversion.PhaseInstance.PhaseDefinitionID : -1,
                    PhaseInstanceID = formsetversion.PhaseInstanceID ?? -1,
                    FormTypeID = formsetversion.FormTypeID,
                    ActionName = formtype.GetActionName(),
                    ControllerName = formtype.GetControllerName(),
                    EmployeeID = pointUserInfo.Employee.EmployeeID,
                    PrintOnNewPage = formtype.PrintOnNewPage.ConvertTo<bool>()
                });
            }

            return list;
        }

        private IEnumerable<FormSetVersionContainer> CreateCheckedFormTypeContainers(string checkedFormTypes)
        {
            var checkedFormTypesList = checkedFormTypes.Split(',').Where(it => !string.IsNullOrEmpty(it)).ToArray();

            var list = new List<FormSetVersionContainer>();
            var pointUserInfo = PointUserInfo();

            foreach (var formtypeid in checkedFormTypesList)
            {
                var formtype = FormTypeBL.GetByFormTypeID(uow, Convert.ToInt32(formtypeid));

                list.Add(new FormSetVersionContainer
                {
                    PrintName = formtype.GetPrintName(),
                    FormTypeID = formtype.FormTypeID,
                    ActionName = formtype.GetActionName(),
                    ControllerName = formtype.GetControllerName(),
                    EmployeeID = pointUserInfo.Employee.EmployeeID,
                    PrintOnNewPage = formtype.PrintOnNewPage.ConvertTo<bool>()
                });
            }

            return list;
        }

        private IEnumerable<TransferAttachment> CreateCheckedAttachments(string checkedAttachments)
        {
            var attachments = checkedAttachments.ToIntList().ToList();

            var list = new List<TransferAttachment>();
            foreach (var attachmentID in attachments)
            {
                var attachment = TransferAttachmentBL.GetByID(uow, attachmentID);
                if (attachment == null || attachment.TransferID != TransferID) continue;
                list.Add(attachment);
            }

            return list;
        }

        public FileResult ExportToXML()
        {
            var xmldoc= TransferToXMLBL.TransferToXML(uow, TransferID, ViewData);
            string filename = string.Format("Dossier{0}_{1}.xml", TransferID, DateTime.Now.ToString("yyyyMMddHHmmss"));
            MemoryStream ms = new MemoryStream();
            xmldoc.Save(ms);
            byte[] bytes = ms.ToArray();
            return File(bytes, "text/xml", filename);
        }
    }
}