﻿using System.Web.Mvc;
using Point.Business.Logic;
using Point.Infrastructure.Attributes;

namespace Point.MVC.WebApplication.Controllers
{
    public class ContactInformationController : PointController
    {
        [ValidateAntiFiddleInjection("id")]
        public ActionResult TransferPointInformation(int id)
        {
            var department = DepartmentBL.GetTransferPointByLocation(uow, id);
            var model = ContactInformationViewBL.FromModel(uow, department);
            return PartialView("_ContactInformation", model);
        }

        [ValidateAntiFiddleInjection("id")]
        public ActionResult LocationInformation(int id)
        {
            var location = LocationBL.GetByID(uow, id);
            var model = ContactInformationViewBL.FromModel(uow, location);
            return PartialView("_ContactInformation", model);
        }

        [ValidateAntiFiddleInjection("id")]
        public ActionResult DepartmentInformation(int id)
        {
            var department = DepartmentBL.GetByDepartmentID(uow, id);
            var model = ContactInformationViewBL.FromModel(uow, department);
            return PartialView("_ContactInformation", model);
        }
    }
}