﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using DevTrends.MvcDonutCaching;
using Point.Business.Logic;
using Point.Database.Extensions;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Infrastructure.Extensions;
using Point.Infrastructure.Attributes;
using System.Net;

namespace Point.MVC.WebApplication.Controllers
{
    public class TransferMemoController : PointFormController
    {
        public ActionResult SelectTemplate(TransferMemoTypeID transferMemoTypeID)
        {
            var flowinstance = FlowInstanceBL.GetByTransferID(uow, TransferID);
            var templates = TransferMemoBL.GetTemplatesByOrganizationIDAndMemoTypeID(uow, flowinstance.StartedByOrganizationID.Value, transferMemoTypeID).ToList();

            return View(templates);
        }

        public ActionResult AddTransferMemo(int? frequencyid = null, TransferMemoTypeID target = TransferMemoTypeID.Zorgprofessional, string templateText = "", bool enableSignalering = true)
        {
            var model = new TransferMemoEditViewModel
            {
                Target = target,
                TransferID = TransferID,
                FrequencyID = frequencyid.GetValueOrDefault(0),
                FormTypeID = FormTypeID,
                EnableSignalering = enableSignalering,
                SignaleringChecked = SignaleringTrigger != null,
                MemoContent = templateText
            };


            return PartialView("EditTransferMemo", model);
        }

        [DonutOutputCache(Duration = 0)]
        [ValidateAntiFiddleInjection("transfermemoid")]
        public ActionResult EditTransferMemo(int transfermemoid, bool enableSignalering = true)
        {
            var model = TransferMemoViewBL.GetByTransferMemoID(uow, transfermemoid);
            model.EnableSignalering = enableSignalering;
            model.SignaleringChecked = SignaleringTrigger != null;
            model.FormTypeID = FormTypeID;

            return PartialView("EditTransferMemo", model);
        }

        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiFiddleInjection("TransferMemoID")]
        public ActionResult SaveTransferMemo(TransferMemoEditViewModel model)
        {
            var target = model.Target.HasValue ? model.Target.Value :
                        (model.FrequencyID.HasValue ? TransferMemoTypeID.FrequencyGeneral : TransferMemoTypeID.Zorgprofessional);

            if (model.TransferMemoID == 0)
            {
                var transfermemo = TransferMemoViewBL.Insert(uow, model.TransferID, model.MemoContent, target);
                if (model.FrequencyID.HasValue)
                {
                    var frequency = FrequencyBL.GetByID(uow, model.FrequencyID.Value);
                    if (frequency != null)
                    {
                        FrequencyTransferMemoBL.Insert(uow, frequency, transfermemo);
                    }
                }

                if (model.SignaleringChecked && FormType != null)
                {
                    var pointuser = PointUserInfo();
                    var message = "Er is een memo toegevoegd door '" + pointuser.Employee.FullName() + "' in '" +
                                  FormType.Name + " - " + model.Target?.GetDescription() + "'";
                    SignaleringBL.HandleSignalByFlowDefinitionFormTypeAction(uow, FlowInstance, FormType.FormTypeID,
                        "SaveTransferMemo", pointuser, message);
                }
            }
            else
            {
                TransferMemoViewBL.Update(uow, model.TransferMemoID, model.MemoContent);

                if (model.SignaleringChecked && FormType != null)
                {
                    var pointuser = PointUserInfo();
                    var message = "Er is een memo aangepast door '" + pointuser.Employee.FullName() + "' in '" + FormType.Name + "'";
                    SignaleringBL.HandleSignalByFlowDefinitionFormTypeAction(uow, FlowInstance, FormType.FormTypeID, "SaveTransferMemo", pointuser, message);
                }
            }

            FlowInstanceSearchValuesBL.CalculateReadValuesByTransferIDAsync(uow, TransferID);

            uow.Save();
            return Json("ok", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ListFromFrequency(int transferid, TransferMemoTypeID target, 
            FrequencyType frequencytype = FrequencyType.None, bool readOnly = false)
        {
            var pointuserinfo = PointUserInfo();

            var viewmodel = new TransferMemoListViewModel
            {
                TransferID = TransferID,
                PhaseInstanceID = PhaseInstanceID,
                PhaseDefinitionID = PhaseDefinitionID,
                ReadOnly = readOnly,
                Action = ActionName,
                Target = target,
                Items = Enumerable.Empty<TransferMemoViewModel>()
            };

            var flowinstance = FlowInstanceBL.GetByTransferID(uow, TransferID);
            if (flowinstance.StartedByOrganizationID.HasValue)
            {
                viewmodel.Templates = TransferMemoBL.GetTemplatesByOrganizationIDAndMemoTypeID(uow, flowinstance.StartedByOrganizationID.Value, target).ToList();
            }

            if (frequencytype != FrequencyType.None)
            {
                var frequency = FrequencyBL.GetByType(uow, TransferID, frequencytype);
                if (frequency == null)
                {
                    return new SecureActionResult("List", viewmodel, true);
                }

                readOnly = frequency.Status == FrequencyStatus.Done;
                viewmodel.FrequencyID = frequency.FrequencyID;
                viewmodel.Items = TransferMemoViewBL.GetByFrequencyID(uow, frequency.FrequencyID, pointuserinfo, readOnly);
            }
            else
            {
                var targets = new List<TransferMemoTypeID> { target };
                viewmodel.Items = TransferMemoViewBL.GetByTransferIDAndTransferMemoTargetType(uow, TransferID, targets, pointuserinfo, readOnly, false);
            }

            return new SecureActionResult("List", viewmodel, true);
        }

        public ActionResult CommunicationJournal(bool readOnly = false, bool forcePartial = false)
        {
            return List(readOnly: readOnly, isCommunicationJournal: true, forcePartial: forcePartial);
        }

        public ActionResult List(TransferMemoTypeID? target = null, bool readOnly = false, 
            bool forcePartial = false, bool isCommunicationJournal = false, bool createtask = false, bool enableSignalering = true)
        {
            PageTitle = "Comm. journaal";
            PageHeader = "Comm. journaal";

            var pointuserinfo = PointUserInfo();
            var model = new TransferMemoListViewModel
            {
                TransferID = TransferID,
                FormSetVersionID = FormSetVersionID,
                PhaseInstanceID = PhaseInstanceID,
                PhaseDefinitionID = PhaseDefinitionID,
                IsCommunicationJournal = isCommunicationJournal,
                ReadOnly = readOnly,
                Action = ActionName,
                Target = target,
                ForcePartial = forcePartial,
                WithoutHeader = forcePartial, 
                EnableSignalering = enableSignalering
            };

            var flowinstance = FlowInstanceBL.GetByTransferID(uow, TransferID);
            var isclosed = FlowInstanceBL.IsFlowClosed(uow, flowinstance.FlowInstanceID);
            if (isclosed)
            {
                readOnly = true;
                createtask = false;
            }

            model.ShowTakeOverMessage = TransferMemoBL.TakeOverTransferMemos(uow, flowinstance , flowinstance.Transfer, pointuserinfo);           

            if (flowinstance.StartedByOrganizationID.HasValue && target.HasValue)
            {
                model.Templates = TransferMemoBL.GetTemplatesByOrganizationIDAndMemoTypeID(uow, flowinstance.StartedByOrganizationID.Value, target.Value).ToList();
            }

            if (target.HasValue && !isCommunicationJournal)
            {
                model.Items = TransferMemoViewBL.GetByTransferIDAndTransferMemoTargetType(
                    uow, TransferID, target.Value, pointuserinfo, readOnly, createtask);
            }
            else if (isCommunicationJournal)
            {
                model.Items = TransferMemoViewBL.GetCommunicationJournalByTransferID(
                    uow, TransferID, pointuserinfo, readOnly);
            }

            return new SecureActionResult(model.Action, model, forcePartial);
        }

        public ActionResult TransferMemos(int? frequencyid = null, TransferMemoTypeID? target = null, bool isCommunicationJournal = false, bool enableSignalering = true)
        {
            var model = new TransferMemoListViewModel
            {
                FrequencyID = frequencyid,
                IsCommunicationJournal = isCommunicationJournal,
                Target = target,
                EnableSignalering = enableSignalering
            };

            var pointuserinfo = PointUserInfo();

            if (frequencyid.HasValue)
            {
                model.Items = TransferMemoViewBL.GetByFrequencyID(uow, frequencyid.Value, pointuserinfo, false);
            }
            else if (target.HasValue && !isCommunicationJournal)
            {
                var usetasks = pointuserinfo?.Organization?.UseTasks == true;
                model.Items = TransferMemoViewBL.GetByTransferIDAndTransferMemoTargetType(uow, TransferID, target.Value, pointuserinfo, false, usetasks);
            }
            else if (isCommunicationJournal)
            {
                model.Items = TransferMemoViewBL.GetCommunicationJournalByTransferID(uow, TransferID, pointuserinfo, false);
            }

            return new SecureActionResult(model);
        }

        [ValidateAntiFiddleInjection("TransferMemoID")]
        public ActionResult DeleteRelatedMemo(int transferMemoID)
        {
            var errorMessage = "";
            var transferMemo = TransferMemoBL.GetByID(uow, transferMemoID);
            var employeeID = transferMemo.EmployeeID ?? default(int);
            var transferMemoOrg = OrganizationBL.GetByEmployeeID(uow, employeeID)?.OrganizationID;          

            var pointuserinfo = PointUserInfo();
            
            if (transferMemoOrg.HasValue && pointuserinfo.EmployeeOrganizationIDs.Contains(transferMemoOrg.Value))
            {              
                TransferMemoBL.DeleteRelatedMemo(uow, transferMemo.TransferMemoID);
                FlowInstanceSearchValuesBL.CalculateReadValuesByTransferIDAsync(uow, TransferID);
            }
            else
            {
                errorMessage = "U heeft geen rechten om deze journaal te verwijderen.";
            }
            Response.StatusCode = (int)(errorMessage == "" ? HttpStatusCode.OK : HttpStatusCode.BadRequest);
            if (errorMessage == "")
            {
                return Json(new { Url = Url.Action("List", ControllerName, new { TransferID }, Request.Url?.Scheme) }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { ErrorMessage = errorMessage }, JsonRequestBehavior.AllowGet);
        }

    }
}