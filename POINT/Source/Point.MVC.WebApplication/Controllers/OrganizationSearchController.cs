﻿using DevTrends.MvcDonutCaching;
using Point.Business.Logic;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Point.MVC.WebApplication.Controllers
{
    [DonutOutputCache(Duration = 0)]
    public class OrganizationSearchController : PointBaseController
    {
        private const int maxpagelen = 100;

        private bool useCapacityPlanning(PointUserInfo userinfo, OrganizationTypeID? organizationtype)
        {
            //Only VVT's have capacity (for now)
            if(organizationtype == OrganizationTypeID.HealthCareProvider)
            {
                if (userinfo?.Organization?.OrganizationTypeID == (int)OrganizationTypeID.Hospital)
                {
                    return (userinfo?.Region?.CapacityBeds).GetValueOrDefault(false);
                }

                return true;
            }

            return false;
        }

        public ActionResult DepartmentSearch(OrganizationSearchViewModel viewmodel)
        {
            if (viewmodel.OrganizationTypeID.HasValue)
            {
                var aftercarecategorylist = AfterCareCategoryBL.GetAll(uow).ToList();

                var aftercarecategoryselectlist = aftercarecategorylist
                    .Select(it => 
                        new SelectListItem() {
                            Text = string.Format("{0}", it.Name),
                            Value = it.Abbreviation
                        })
                    .ToList();
                aftercarecategoryselectlist.Insert(0, new SelectListItem() { Text = " - Alle - ", Value = ""  });
                if (String.IsNullOrWhiteSpace(viewmodel.AfterCareCategory))
                {
                    aftercarecategoryselectlist.ForEach(it => it.Selected = it.Value == viewmodel.AfterCareCategory);
                }

                ViewBag.AfterCareCategoryList = aftercarecategoryselectlist;
            }

            var userinfo = PointUserInfo();
            ViewBag.UseCapacity = useCapacityPlanning(userinfo, viewmodel.OrganizationTypeID);

            if (viewmodel.TransferID.HasValue && String.IsNullOrEmpty(viewmodel.PostalCode))
            {
                var client = ClientBL.GetByTransferID(uow, viewmodel.TransferID.Value);

                // tijdelijk postcode heeft proioriteit op postcode.
                if (!string.IsNullOrEmpty(client.TemporaryPostalCode))
                {
                    viewmodel.PostalCode = client.TemporaryPostalCode;
                    viewmodel.PostalCodeRange = client.TemporaryPostalCode;                 
                }
                else
                {
                    viewmodel.PostalCode = client.PostalCode;
                    viewmodel.PostalCodeRange = client.PostalCode;
                }
                viewmodel.TemporaryPostalCode = client.TemporaryPostalCode;
                viewmodel.PermanentPostalCode = client.PostalCode;

            }
            
            return View(viewmodel);
        }

        public PartialViewResult DepartmentSearchData(OrganizationSearchViewModel viewmodel)
        {
            var userinfo = PointUserInfo();

            int[] favoritelocationids = PreferredLocationBL.GetByLocationID(uow, userinfo.Department.LocationID).Select(it => it.PreferredLocationID).ToArray();
            var data = OrganizationSearchBL.GetByViewModel(uow, viewmodel, favoritelocationids, userinfo.Region.RegionID);

            var excludeInvisibleDepartments = data.Where(d => !d.OrganizationSearchParticipation.Any
            ( p => p.FlowDefinitionID == viewmodel.FlowDefinitionID && 
            ( p.Participation == Participation.ActiveButInvisibleInSearch ||  
              p.Participation == Participation.None)));

            var viewmodellist = OrganizationSearchDepartmentViewModel.FromModelList(excludeInvisibleDepartments, viewmodel.FlowDefinitionID, favoritelocationids);

            ViewBag.UseCapacity = useCapacityPlanning(userinfo, viewmodel.OrganizationTypeID);
            ViewBag.Count = viewmodellist.Count;
            ViewBag.ShowAll = viewmodel.ShowAll;

            viewmodellist = string.IsNullOrEmpty(viewmodel.SortBy)
                ? viewmodellist.OrderByDescending(hsp => hsp.IsFavorite).ThenBy(hsp => hsp.DepartmentName).ToList()
                : viewmodellist.OrderBy(viewmodel.SortBy).ToList();

            viewmodellist = viewmodel.ShowAll
                ? viewmodellist.ToList()
                : viewmodellist.Take(maxpagelen).ToList();

            return PartialView(viewmodellist);
        }

        public ActionResult LocationSearch(OrganizationSearchViewModel viewmodel)
        {
            if (viewmodel.TransferID.HasValue && String.IsNullOrEmpty(viewmodel.PostalCode))
            {
                var client = ClientBL.GetByTransferID(uow, viewmodel.TransferID.Value);
                viewmodel.PostalCode = client.PostalCode;
                viewmodel.PostalCodeRange = client.PostalCode;                
            }

            var userinfo = PointUserInfo();
            ViewBag.UseCapacity = useCapacityPlanning(userinfo, viewmodel.OrganizationTypeID);

            return View(viewmodel);
        }

        public PartialViewResult LocationSearchData(OrganizationSearchViewModel viewmodel)
        {
            var userinfo = PointUserInfo();

            int[] favoritelocationids = PreferredLocationBL.GetByLocationID(uow, userinfo.Department.LocationID).Select(it => it.PreferredLocationID).ToArray();
            var data = OrganizationSearchBL.GetByViewModel(uow, viewmodel, favoritelocationids, userinfo.Region.RegionID);
            var viewmodellist = OrganizationSearchLocationViewModel.FromModelList(data, viewmodel.FlowDefinitionID, favoritelocationids);

            ViewBag.Count = viewmodellist.Count;
            ViewBag.ShowAll = viewmodel.ShowAll;
            ViewBag.UseCapacity = useCapacityPlanning(userinfo, viewmodel.OrganizationTypeID);

            viewmodellist = string.IsNullOrEmpty(viewmodel.SortBy)
                ? viewmodellist.OrderByDescending(hsp => hsp.IsFavorite).ThenBy(hsp => hsp.LocationName).ToList()
                : viewmodellist.OrderBy(viewmodel.SortBy).ToList();

            viewmodellist = viewmodel.ShowAll
                ? viewmodellist.ToList()
                : viewmodellist.Take(maxpagelen).ToList();

            return PartialView(viewmodellist);
        }

        public ActionResult OrganizationSearch(OrganizationSearchViewModel viewmodel)
        {
            if (viewmodel.TransferID.HasValue && String.IsNullOrEmpty(viewmodel.PostalCode))
            {
                var client = ClientBL.GetByTransferID(uow, viewmodel.TransferID.Value);
                viewmodel.PostalCode = client.PostalCode;
                viewmodel.PostalCodeRange = client.PostalCode;                
            }

            if (viewmodel.OrganizationTypeID.HasValue)
            {
                var aftercarecategorylist = AfterCareCategoryBL.GetAll(uow).ToList();

                var aftercarecategoryselectlist = aftercarecategorylist
                    .Select(it =>
                        new SelectListItem()
                        {
                            Text = string.Format("{0}", it.Name),
                            Value = it.Abbreviation
                        })
                    .ToList();
                aftercarecategoryselectlist.Insert(0, new SelectListItem() { Text = " - Alle - ", Value = "" });
                if (String.IsNullOrWhiteSpace(viewmodel.AfterCareCategory))
                {
                    aftercarecategoryselectlist.ForEach(it => it.Selected = it.Value == viewmodel.AfterCareCategory);
                }

                ViewBag.AfterCareCategoryList = aftercarecategoryselectlist;
            }

            var userinfo = PointUserInfo();
            ViewBag.UseCapacity = this.useCapacityPlanning(userinfo, viewmodel.OrganizationTypeID);

            return View(viewmodel);
        }

        public PartialViewResult OrganizationSearchData(OrganizationSearchViewModel viewmodel)
        {
            var userinfo = PointUserInfo();

            int[] favoritelocationids = PreferredLocationBL.GetByLocationID(uow, userinfo.Department.LocationID).Select(it => it.PreferredLocationID).ToArray();
            var data = OrganizationSearchBL.GetByViewModel(uow, viewmodel, favoritelocationids, userinfo.Region.RegionID);
            var viewmodellist = OrganizationSearchOrganizationViewModel.FromModelList(data, viewmodel.FlowDefinitionID, favoritelocationids);

            ViewBag.Count = viewmodellist.Count;
            ViewBag.ShowAll = viewmodel.ShowAll;
            ViewBag.UseCapacity = this.useCapacityPlanning(userinfo, viewmodel.OrganizationTypeID);

            viewmodellist = string.IsNullOrEmpty(viewmodel.SortBy)
                ? viewmodellist.OrderByDescending(hsp => hsp.IsFavorite).ThenBy(hsp => hsp.OrganizationName).ToList()
                : viewmodellist.OrderBy(viewmodel.SortBy).ToList();

            viewmodellist = viewmodel.ShowAll
                ? viewmodellist.ToList()
                : viewmodellist.Take(maxpagelen).ToList();

            return PartialView(viewmodellist);
        }
    }
}