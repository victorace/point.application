﻿using DevTrends.MvcDonutCaching;
using Point.Business.Helper;
using Point.Business.Logic;
using Point.Database.Extensions;
using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Infrastructure.Attributes;
using Point.Infrastructure.Extensions;
using Point.Log;
using Point.Log.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using Point.Database.Repository;

namespace Point.MVC.WebApplication.Controllers
{

    [DonutOutputCache(CacheProfile = "Short")]
    public class HistoryController : PointFormController
    {
        public ActionResult Index()
        {
            PageTitle = "Transfer historie";
            PageHeader = "Transfer historie";

            var historyFlowFieldsUrl = Url.Action("FlowFields", "History");
            var historyFlowFieldValuesUrl = Url.Action("FlowFieldValues", "History", new { TransferID, flowFieldIDs = "__flowFieldIDs__", customfields = "__CustomFields__", formsetversionid = "__FormSetVersionID__" });

            var model = new HistoryViewModel ();

            FormHistoryForm item;

            var clientData = MutClientBL.GetLatestByTransferId(uow, TransferID);
            if (clientData != null)
            {
                item = new FormHistoryForm
                {
                    Description = "Patientgegevens",
                    DataType = FormHistoryDataTypes.Client,
                    FormTypeID = clientData.ClientID,
                    ModificationDate = clientData.ClientModifiedDate,
                    ModifiedBy = clientData.ClientModifiedByEmployee.FullName(),
                    FieldsUrl = Url.Action("ClientFields", "History"),
                    HistoryUrl = Url.Action("Client", "History", new { clientId = ViewBag.TransferID })
                };
                model.FormHistoryForms.Add(item);
            }
            
            foreach (var f in FormHistoryBL.GetForms(uow, TransferID).Where(fhf => fhf.FormTypeID != (int)FlowFormType.VerpleegkundigeOverdracht))
            {
                f.DataType = FormHistoryDataTypes.FlowForm;
                f.FieldsUrl = historyFlowFieldsUrl;
                f.HistoryUrl = historyFlowFieldValuesUrl;
                model.FormHistoryForms.Add(f);
            }
            
            return new SecureActionResult(model);
        }

        public ActionResult Forms()
        {
            var formHistoryForms = FormHistoryBL.GetForms(uow, TransferID).ToList();
            return PartialView(formHistoryForms.Where(fhf => fhf.FormTypeID != (int)FlowFormType.VerpleegkundigeOverdracht).OrderBy(x => x.Name));
        }

        public ActionResult FlowFields(int formtypeid, int formsetVersionID = -1)
        {
            var formHistoryFields = FormHistoryBL.GetFields(uow, formsetVersionID).ToList();

            if (formtypeid == (int)FlowFormType.MSVTUitvoeringsverzoek || formtypeid == (int)FlowFormType.MSVTIndicatiestelling)
            {
                formHistoryFields.Add(new FormHistoryField { CustomField = WellKnownFieldNames.MSVTActions, Label = "Handelingen", Name = WellKnownFieldNames.MSVTActions });
            }

            if (formtypeid == (int)FlowFormType.MSVTUitvoeringsverzoek)
            { 
                formHistoryFields.Add(new FormHistoryField { CustomField = WellKnownFieldNames.MSVTMedication, Label = "Medicatie", Name = WellKnownFieldNames.MSVTMedication });
            }

            return Json(new { Success = true, data = formHistoryFields.OrderBy(x => x.Label), formsetversionid = formsetVersionID }, JsonRequestBehavior.AllowGet);
        }

        public SecureContentResult FlowFieldValues(string flowFieldIDs, string customFields, int formsetversionid = -1)
        {
            var content = "";
            if (string.IsNullOrWhiteSpace(flowFieldIDs) && string.IsNullOrEmpty(customFields))
            {
                return new SecureContentResult(content);
            }

            var FlowFieldIDs = new int[] { };
            var CustomFields = new string[] { };
            try
            {
                if (!string.IsNullOrWhiteSpace(flowFieldIDs))
                {
                    FlowFieldIDs = flowFieldIDs.Split(',').Select(int.Parse).ToArray();
                }

                if (!string.IsNullOrWhiteSpace(customFields))
                {
                    CustomFields = customFields.Split(',').ToArray();
                }
            }
            catch { /* slient */ }

            if (FlowFieldIDs.Any())
            {
                var formhistoryfieldvalues = FormHistoryBL.GetValues(uow, FormSetVersionID, FlowFieldIDs);
                var datatable = convertToDataTable(uow, formhistoryfieldvalues);

                content = "<h3>Standaard velden</h3>" + DatatableHtmlHelper.GenerateHistoryHTMLTable(uow, datatable);
            }

            if (!CustomFields.Any())
            {
                return new SecureContentResult(content);
            }

            foreach (var customfield in CustomFields)
            {
                switch (customfield)
                {
                    case WellKnownFieldNames.MSVTActions:
                        var historyactions = MutActionHealthInsurerBL.GetByFormSetVersionID(uow, FormSetVersionID).ToList();
                        var dataTableactions = convertToDataTable(historyactions);

                        content += "<h3>Handelingen</h3>" + DatatableHtmlHelper.GenerateHistoryHTMLTable(uow, dataTableactions);
                        break;

                    case WellKnownFieldNames.MSVTMedication:

                        var historymedicine = MutMedicineCardBL.GetByFormSetVersionID(uow, FormSetVersionID).ToList();
                        var dataTablemedicine = convertToDataTable(historymedicine);

                        content += "<h3>Medicatie</h3>" +
                                   DatatableHtmlHelper.GenerateHistoryHTMLTable(uow, dataTablemedicine);
                        break;
                }
            }

            return new SecureContentResult(content);
        }

        [HttpPost]
        public ActionResult ClientFields()
        {
            var properties = typeof(Client).GetProperties();

            var fields = properties.Where(x => x.GetDisplayName() != "" && !x.Name.Contains("ContactPerson", StringComparison.InvariantCultureIgnoreCase) && !x.Name.Contains("Temporary", StringComparison.InvariantCultureIgnoreCase)).Select(x => new { Key = x.Name, Value = x.GetDisplayName() }).OrderBy(x => x.Value);

            return Json(fields);
        }

        [ValidateAntiFiddleInjection("primarykey")]
        public PartialViewResult GenericLogDetails(string tablename, string primarykey)
        {
            var details = LogBL.GetLogByTableNameAndPrimaryKey(tablename, primarykey);
            var viewmodel = GenericLogDetailsViewModel.FromModelList(uow, details);
            return PartialView(viewmodel);
        }

        [HttpPost]
        public SecureContentResult Client(int clientId, string[] fields)
        {
            var transfer = TransferBL.GetByClientID(uow, clientId);
            if (transfer == null || transfer.TransferID != this.TransferID)
            {
                return new SecureContentResult("");
            }

            var client = transfer.Client;
            if (client.ClientIsAnonymous)
            {
                var pointuserinfo = PointUserInfo();
                var flowinstance = transfer.FlowInstances.FirstOrDefault();

                bool isanonymous = ClientBL.ShowAnonymous(flowinstance, pointuserinfo.Organization?.OrganizationID);
                if (isanonymous)
                {
                    return new SecureContentResult("Historie is niet beschikbaar omdat bepaalde clientgegevens momenteel anoniem zijn.");
                }
            }

            List<string> allFields = new List<string> { "ClientModifiedByEmployeeName", "ClientModifiedDate" };
            if (fields != null)
            {
                allFields.AddRange(fields.AsEnumerable());
            }

            var data = MutClientBL.GetByClientId(uow, clientId).ToList().ConvertToDataTable();
            for (int i = data.Columns.Count - 1; i >= 0; i--)
            {
                if (!allFields.Contains(data.Columns[i].ColumnName))
                {
                    data.Columns.RemoveAt(i);
                }
            }

            foreach (var prop in typeof(MutClient).GetProperties())
            {
                if (data.Columns[prop.Name] != null)
                {
                    var displayName = prop.GetDisplayName();
                    if (!string.IsNullOrEmpty(displayName) && data.Columns[displayName] == null)
                    {
                        data.Columns[prop.Name].ColumnName = displayName;
                    }
                }
            }

            data.Columns["ClientModifiedByEmployeeName"].ColumnName = "Gewijzigd door";
            data.Columns["ClientModifiedDate"].ColumnName = "Datum wijziging";

            var htmlString = DatatableHtmlHelper.GenerateHistoryHTMLTable(uow, data);

            return new SecureContentResult() { Content = htmlString };
        }

        private DataTable convertToGenericDataTable(IEnumerable<EntityLog> logdata)
        {
            var outputTable = new DataTable();
            outputTable.Columns.Add("ModificationDate");
            outputTable.Columns.Add("ModifiedBy");

            foreach (var item in logdata)
            {
                var columnname = item.FieldName;

                if (!string.IsNullOrEmpty(columnname) && !outputTable.Columns.Contains(columnname))
                {
                    outputTable.Columns.Add(columnname);
                }
            }

            var groups = logdata.GroupBy(it => it.EntityLogSetID).OrderByDescending(it => it.Key).ToList();
            var rowCount = groups.Count;

            for (var y = 0; y <= rowCount - 1; y++)
            {
                var newRow = outputTable.NewRow();
                outputTable.Rows.Add(newRow);

                var group = groups.ToArray()[y];

                newRow["ModificationDate"] = group.Key;

                var employee = group.FirstOrDefault()?.EntityLogSet.EmployeeID;
                if (employee != null)
                    newRow["ModifiedBy"] = employee;

                foreach (var item in group)
                {
                    if (!string.IsNullOrEmpty(item.FieldName))
                    {
                        string value = item.NewValue;
                        if(item.NewValue == null && item.OldValue != null)
                        {
                            value = String.Empty; //Need to be explicit ""
                        }

                        newRow[item.FieldName] = value;
                    }
                }
            }

            return outputTable;
        }

        private DataTable convertToDataTable(IEnumerable<MutMedicineCard> mutMedicineCard)
        {
            var outputTable = new DataTable();
            outputTable.Columns.Add("ModificationDate");
            outputTable.Columns.Add("ModifiedBy");

            foreach (var item in mutMedicineCard)
            {
                var columnname = item.NotRegisteredMedicine;

                if (!string.IsNullOrEmpty(columnname) && !outputTable.Columns.Contains(columnname) && (!string.IsNullOrEmpty(item.Dosage) || !string.IsNullOrEmpty(item.Frequence) || !string.IsNullOrEmpty(item.Prescription)))
                    outputTable.Columns.Add(columnname);
            }

            var groups = mutMedicineCard.GroupBy(it => it.TimeStamp.ToSortablePointDateHourDisplay()).OrderBy(g => g.Key).ToList();
            var rowCount = groups.Count;
            for (var y = 0; y <= rowCount - 1; y++)
            {
                var newRow = outputTable.NewRow();
                outputTable.Rows.InsertAt(newRow, 0);

                var group = groups.ToArray()[y];

                newRow["ModificationDate"] = group.FirstOrDefault().TimeStamp.ToPointDateHourDisplay();

                var employee = group.FirstOrDefault()?.Employee;
                if (employee != null)
                    newRow["ModifiedBy"] = employee.FullName();

                foreach (var item in group)
                {
                    var columnname = item.NotRegisteredMedicine;

                    if (!string.IsNullOrEmpty(columnname) && (!string.IsNullOrEmpty(item.Dosage) || !string.IsNullOrEmpty(item.Frequence) || !string.IsNullOrEmpty(item.Prescription)))
                        newRow[columnname] = $"{item.Frequence} {item.Dosage} {item.Prescription}";
                }
            }

            return outputTable;
        }
        
        private DataTable convertToDataTable(IEnumerable<MutActionHealthInsurer> mutActionHealthInsurer)
        {
            var outputTable = new DataTable();
            outputTable.Columns.Add("ModificationDate");
            outputTable.Columns.Add("ModifiedBy");
            foreach (var item in mutActionHealthInsurer)
            {
                var columnname = !string.IsNullOrEmpty(item.ActionCodeHealthInsurer.ActionCodeName) ? item.ActionCodeHealthInsurer.ActionCodeName : item.Definition;

                if (!string.IsNullOrEmpty(columnname) && !outputTable.Columns.Contains(columnname) && (item.Amount.HasValue || !string.IsNullOrEmpty(item.Unit) || item.ExpectedTime.HasValue))
                    outputTable.Columns.Add(columnname);
            }

            var groups = mutActionHealthInsurer.GroupBy(it => it.TimeStamp.ToSortablePointDateHourDisplay()).OrderBy(g => g.Key).ToList();
            var rowCount = groups.Count;
            for (var y = 0; y <= rowCount - 1; y++)
            {
                var newRow = outputTable.NewRow();
                outputTable.Rows.InsertAt(newRow, 0);

                var group = groups.ToArray()[y];

                newRow["ModificationDate"] = group.FirstOrDefault().TimeStamp.ToPointDateHourDisplay();

                var employee = group.FirstOrDefault()?.Employee;
                if (employee != null)
                    newRow["ModifiedBy"] = employee.FullName();

                foreach (var actionitem in group)
                {
                    var columnname = !string.IsNullOrEmpty(actionitem.ActionCodeHealthInsurer.ActionCodeName) ? actionitem.ActionCodeHealthInsurer.ActionCodeName : actionitem.Definition;

                    if (string.IsNullOrEmpty(columnname) ||
                        (!actionitem.Amount.HasValue && string.IsNullOrEmpty(actionitem.Unit) &&
                         !actionitem.ExpectedTime.HasValue)) continue;

                    var timepiece = "";

                    if (actionitem.ExpectedTime.HasValue)
                        timepiece = $" {actionitem.ExpectedTime} mins";
                    else if (actionitem.ActionCodeHealthInsurer.TimeLimitMax > actionitem.ActionCodeHealthInsurer.TimeLimitMin)
                        timepiece =
                            $" ({actionitem.ActionCodeHealthInsurer.TimeLimitMin}-{actionitem.ActionCodeHealthInsurer.TimeLimitMax}) mins";
                    else if (actionitem.ActionCodeHealthInsurer.TimeLimitMin.HasValue)
                        timepiece = $" {actionitem.ActionCodeHealthInsurer.TimeLimitMin} mins";

                    if (actionitem.FormSetVersion.FormTypeID == (int)FlowFormType.MSVTIndicatiestelling)
                        newRow[columnname] = $"{actionitem.Amount} x {actionitem.Unit}{timepiece}";

                    if (actionitem.FormSetVersion.FormTypeID == (int)FlowFormType.MSVTUitvoeringsverzoek)
                        newRow[columnname] = $"{actionitem.Amount} x {actionitem.Unit}";
                }
            }

            return outputTable;
        }

        private DataTable convertToDataTable(IUnitOfWork uow, IEnumerable<FormHistoryFieldValue> formhistoryfieldvalues)
        {
            var outputTable = new DataTable();
            if (formhistoryfieldvalues.Count() <= 0)
            {
                return outputTable;
            }

            outputTable.Columns.Add("Wijzigingsdatum");
            outputTable.Columns.Add("Gewijzigd door");

            foreach (var item in formhistoryfieldvalues)
            {
                if (outputTable.Columns.Contains(item.Name))
                {
                    continue;
                }

                var newcolumn = new DataColumn(item.Name) { Caption = item.Label };
                outputTable.Columns.Add(newcolumn);
            }

            var columns = outputTable.Columns.Cast<DataColumn>()
                .Where(it => new[] { "ModificationDate", "ModifiedBy", "Wijzigingsdatum", "Gewijzigd door" }
                .Contains(it.ColumnName) == false).ToList();

            using (var lookupBL = new LookUpBL())
            {
                var groups = formhistoryfieldvalues.GroupBy(it => it.Timestamp.ToString("yyyyMMddHHmmss")).OrderBy(g => g.Key).ToArray();
                foreach (var group in groups)
                {
                    var previous = outputTable.Rows.Cast<DataRow>().FirstOrDefault();

                    var newRow = outputTable.NewRow();
                    newRow["Wijzigingsdatum"] = group.FirstOrDefault()?.Timestamp.ToPointDateHourDisplay();
                    newRow["Gewijzigd door"] = group.FirstOrDefault()?.EmployeeFullName;

                    foreach (var column in columns)
                    {
                        var displayvalue = "";
                        if (previous != null)
                        {
                            displayvalue = previous[column] as string;
                        }

                        var item = group.FirstOrDefault(it => it.Name == column.ColumnName);
                        if (item != null)
                        {
                            var flowWebField = FlowWebFieldBL.FromModel(uow, item.FlowField, item.Value, item.Source, ViewData);
                            displayvalue = flowWebField.DisplayValue ?? item.Value;

                            //Klikbare link voor weergeven vo attachment genereren
                            if (item.FlowFieldID == FlowFieldConstants.ID_VOFormulierTransferFileName)
                            {
                                var attachmentid = group.FirstOrDefault(it => it.FlowFieldID == FlowFieldConstants.ID_VOFormulierTransferAttachmentID);
                                if (!string.IsNullOrEmpty(attachmentid?.Value))
                                {
                                    displayvalue = "<a href=\"" + Url.Action("Download", "TransferAttachment", new { AttachmentID = attachmentid.Value, Digest = AntiFiddleInjection.CreateDigest(attachmentid.Value) }) + "\">" + item.Value + "</a>";
                                }
                            }
                        }

                        newRow[column.ColumnName] = displayvalue;
                    }

                    outputTable.Rows.InsertAt(newRow, 0);
                }
            }

            return outputTable;
        }

        private DataTable convertToDataTable(IEnumerable<TransferMemoChanges> transfermemochanges)
        {
            DataTable outputTable = new DataTable();
            outputTable.Columns.Add("Datum wijziging");
            outputTable.Columns.Add("Gewijzigd door");
            outputTable.Columns.Add("Wijzigingen");

            foreach (var item in transfermemochanges)
            {
                var row = outputTable.NewRow();
                row["Datum wijziging"] = item.DateTimeChanged?.ToString();
                row["Gewijzigd door"] = item.EmployeeName;
                row["Wijzigingen"] = item.OriginalContent;

                outputTable.Rows.Add(row);
            }

            return outputTable;
        }

    }
}