﻿using DevTrends.MvcDonutCaching;
using Point.Business.Logic;
using Point.Database.Extensions;
using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Infrastructure.Extensions;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Point.Infrastructure.Attributes;
using Point.MVC.WebApplication.Attributes;

namespace Point.MVC.WebApplication.Controllers
{
    [DonutOutputCache(CacheProfile = "Long")]
    [Authorize]
    [SessionState(System.Web.SessionState.SessionStateBehavior.ReadOnly)]
    [FormSecurity(SecurityAttribute.TransferID)]
    public class LookUpController : PointBaseController
    {
        protected override void Dispose(bool disposing)
        {
            uow.SafeDispose();
            base.Dispose(disposing);
        }

        private List<TransferMemoSimpleViewModel> getTransferMemoSimpleViewModel(int transferID, List<TransferMemoTypeID> transferMemoTargetTypes)
        {
            var userinfo = PointUserInfo();
            var transfermemos = TransferMemoViewBL.GetByTransferIDAndTransferMemoTargetType(uow, 
                transferID, transferMemoTargetTypes, userinfo, false, false).OrderByDescending(it => it.TransferMemo.MemoDateTime).ToList();

            
            var data = new List<TransferMemoSimpleViewModel>();
            if (!transfermemos.Any())
            {
                return data;
            }

            foreach (var transfermemo in transfermemos)
            {
                var DateTimeString = transfermemo.TransferMemo.MemoDateTime?.ToString("dd-MM-yyyy HH:mm") ?? "";
                var MemoContent = transfermemo.TransferMemo.MemoContent;
                var EmployeeName = transfermemo.TransferMemo.EmployeeName;

                data.Add(new TransferMemoSimpleViewModel { TransferMemoItem =
                    $"<b>{DateTimeString} {EmployeeName}</b>\r\n{MemoContent}"
                });
            }
            return data;
        }
        
        public PartialViewResult GetLastGrzForm(int transferid)
        {
            var grzform = FormSetVersionBL.GetByTransferIDAndFormTypeID(uow, transferid, (int)FlowFormType.GRZFormulier).OrderByDescending(it=>it.FormSetVersionID).FirstOrDefault();
            if (grzform == null)
                return null;

            var viewmodel = new LastGrzFormViewModel
            {
                DateTimeString = grzform.CreateDate.ToString("dd-MM-yyyy HH:mm") ?? "",
                FormTypeName = grzform.FormType.Name,
                Description = grzform.Description
            };

            return PartialView(viewmodel);
        }

        [ValidateAntiFiddleInjection("transferid")]
        public PartialViewResult GetLastTransferMemos(int transferid)
        {
            var transfermemotargettypes = new List<TransferMemoTypeID> { TransferMemoTypeID.Zorgprofessional, TransferMemoTypeID.Transferpunt, TransferMemoTypeID.Patient };
            var viewmodel = getTransferMemoSimpleViewModel(transferid, transfermemotargettypes);
            return PartialView(viewmodel);
        }

        [ValidateAntiFiddleInjection("transferid")]
        public PartialViewResult GetLastAdditionalTPMemo(int transferid)
        {
            var transfermemotargettypes = new List<TransferMemoTypeID> { TransferMemoTypeID.AanvullendeGegevensBevindingenMedewerkerTransferpunt };
            var viewmodel = getTransferMemoSimpleViewModel(transferid, transfermemotargettypes);
            return PartialView(viewmodel);
        }

        [ValidateAntiFiddleInjection("transferid")]
        public PartialViewResult GetLastTransferAttachment(int transferid)
        {
            var transferattachment = TransferAttachmentBL.GetByTransferID(uow, transferid).OrderByDescending(it => it.AttachmentID).FirstOrDefault();
            if (transferattachment == null) return null;
            var viewmodel = new LastTransferAttachmentViewModel
            {
                DateTimeString = transferattachment.UploadDate.ToString("dd-MM-yyyy HH:mm"),
                Name = transferattachment.Name,
                Description = transferattachment.Description,
                TransferID = transferid
            };

            return PartialView(viewmodel);
        }

        [ValidateAntiFiddleInjection("transferid")]
        public PartialViewResult GetLastFeedbackMemo(int transferid, int frequencyid)
        {
            var pointuser = PointUserInfo();
            var transfermemo = TransferMemoViewBL.GetByFrequencyID(uow, frequencyid, pointuser, false)
                .OrderByDescending(it => it.TransferMemo.TransferMemoID).FirstOrDefault();

            if (transfermemo == null) return null;

            var viewmodel = new LastFeedbackMemoViewModel
            {
                DateTimeString = transfermemo.TransferMemo.MemoDateTime?.ToString("dd-MM-yyyy HH:mm") ?? "",
                MemoContent = transfermemo.TransferMemo.MemoContent,
                EmployeeName = transfermemo.TransferMemo.EmployeeName
            };

            return PartialView(viewmodel);
        }

        [ValidateAntiFiddleInjection("transferid")]
        public PartialViewResult GetLastFeedbackTransferAttachment(int transferid, int frequencyid)
        {
            var transferattachment = TransferAttachmentBL.GetByFrequencyID(uow, frequencyid).OrderByDescending(it => it.AttachmentID).FirstOrDefault();
            if (transferattachment == null)
            {
                return null;
            }

            var viewmodel = new LastTransferAttachmentViewModel
            {
                DateTimeString = transferattachment.UploadDate.ToString("dd-MM-yyyy HH:mm"),
                Name = transferattachment.Name,
                Description = transferattachment.Description,
                TransferID = transferid
            };

            return PartialView(viewmodel);
        }

        public JsonResult GetTemplateDefaultText(int templatetypeid)
        {
            var templatedefault = TemplateDefaultBL.GetByTemplateTypeID(uow, templatetypeid) ?? new TemplateDefault();
            return new JsonResult
            {
                Data = new { templatedefault.TemplateDefaultText },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [ValidateAntiFiddleInjection("transferid")]
        public PartialViewResult GetTransferTaskList(int transferid)
        {
            var transfertasks = TransferTaskBL.GetByTransferID(uow, transferid, false)
                .Where(tt => tt.Status == Status.Active)
                .ToList();
            if (!transfertasks.Any())
            {
                return null;
            }

            List<TransferTaskViewModel> data = new List<TransferTaskViewModel>();
            foreach (var transfertask in transfertasks)
            {
                data.Add(new TransferTaskViewModel()
                {
                    DueDate = transfertask.DueDate,
                    Description = transfertask.Description,
                    CreatedByEmployee = transfertask.CreatedByEmployee.FullName()
                });
            }
            return PartialView(data);
        }

        [ValidateAntiFiddleInjection("transferid")]
        public PartialViewResult GetCopyOfTransfer(int transferid)
        {
            var pointuser = PointUserInfo();
            var transfer = TransferBL.GetByTransferID(uow, transferid);

            var copy = TransferBL.GetByTransferID(uow, transfer.OriginalTransferID.GetValueOrDefault());

            var viewmodel = new CopyOfTransferViewModel
            {
                TransferID = copy.TransferID,
                DateTimeCreated = copy.FlowInstances.FirstOrDefault().CreatedDate.ToPointDateHourDisplay()
            };

            return PartialView(viewmodel);
        }

        public JsonResult GetVerwijzingTypeByFormTypeID(int? formtypeid = null)
        {
            using (var lookupbl = new LookUpBL())
            {
                var data = lookupbl.GetVerwijzingTypeByFormTypeID(formtypeid);
                return Json(data);
            }
        }

        public JsonResult GetOndervoedingsScoreMUST()
        {
            var options = new List<Option>();
            var data = FlowListValues.OndervoedingsScoreMUST();
            data.ForEach(rt => options.Add(new Option { Text = rt.Text, Value = rt.Value }));
            return Json(options);
        }

        public JsonResult GetOndervoedingsScoreSNAQ()
        {
            var options = new List<Option>();
            var data = FlowListValues.OndervoedingsScoreSNAQ();
            data.ForEach(rt => options.Add(new Option { Text = rt.Text, Value = rt.Value }));
            return Json(options);
        }

        [ValidateAntiFiddleInjection("employeeID")]
        public JsonResult GetEmployeeEmailAddressByEmployeeID(int employeeID)
        {
            var options = new List<Option>();
            var employee = EmployeeBL.GetByID(uow, employeeID);
            if (employee != null)
                options.Add(new Option(employee.EmailAddress, employee.EmployeeID.ToString()));
            return new JsonResult
            {
                Data = options
            };
        }

        public JsonResult GetCentraalMeldpuntByOrganizationID(int organizationid)
        {
            var centraalmeldpunt = DepartmentBL.GetByOrganizationIDAndDepartmentTypeID(uow, new int[] { organizationid }, DepartmentTypeID.CentraalMeldpunt).FirstOrDefault();
            if (centraalmeldpunt == null)
            {
                return new JsonResult { Data = new { ErrorMessage = "Geen centraal meldpunt bekend voor deze organisatie " + organizationid } };
            }

            var jsonobject = new { DepartmentID = centraalmeldpunt.DepartmentID, DepartmentName = centraalmeldpunt.Name };
            return Json(jsonobject);
        }

        public JsonResult GetDoctorByID(int? doctorID)
        {
            var doctor = DoctorBL.GetByID(uow, doctorID.GetValueOrDefault(0));
            if (doctor == null) return new JsonResult { Data = new { ErrorMessage = "No doctor with ID " + doctorID.GetValueOrDefault(0) } };

            return new JsonResult
            {
                Data = new {doctor.Name, doctor.AGB, doctor.BIG, doctor.PhoneNumber, doctor.SpecialismID }
            };
        }

        public JsonResult GetLocationsByRegionID(int regionid)
        {
            var options = new List<Option>();
            var locations = LocationBL.GetActiveByRegionID(uow, regionid).OrderBy(loc => loc.Organization.OrganizationTypeID).ThenBy(loc => loc.Organization.Name).ThenBy(loc => loc.Name);

            options.Add(LookUpBL.OptionEmpty);
            locations.ToList().ForEach(loc => options.Add(new Option { Value = loc.LocationID.ToString(), Text = loc.Organization.OrganizationType.Name + " - " + loc.Organization.Name + " - " + loc.Name }));

            return Json(options);
        }

        private void EmployeesToOptions(List<Option> options, IEnumerable<Employee> employees)
        {
            options.AddRange((from e in employees
                              where options.All(x => x.Value != e.EmployeeID.ToString())
                              select MapEmployeeToOption(e)).ToList());
        }

        private static Option MapEmployeeToOption(Employee e)
        {
            return new Option(e.FullName(), e.EmployeeID.ToString());
        }

        public JsonResult GetDepartmentPhoneNumberByEmployeeID(int? employeeID)
        {
            var options = new List<Option>();
            if (!employeeID.HasValue)
                return new JsonResult
                {
                    Data = options
                };
            var employee = EmployeeBL.GetByID(uow, employeeID.Value);
            if (employee?.DefaultDepartment != null)
            {
                options.Add(new Option(employee.DefaultDepartment.PhoneNumber, employee.EmployeeID.ToString()));
            }
            return new JsonResult
            {
                Data = options
            };
        }

        [ValidateAntiFiddleInjection("employeeID", true)] 
        public JsonResult GetEmployeePhoneNumberByEmployeeID(int? employeeID)
        {
            var options = new List<Option>();
            if (!employeeID.HasValue)
                return new JsonResult
                {
                    Data = options
                };
            var employee = EmployeeBL.GetByID(uow, employeeID.Value);
            if (employee != null)
                options.Add(new Option(employee.PhoneNumber, employee.EmployeeID.ToString()));
            return new JsonResult
            {
                Data = options
            };
        }

        [ValidateAntiFiddleInjection("employeeID", true)]
        public JsonResult GetEmployeeDetailsByEmployeeID(int? employeeID)
        {
            var options = new List<Option>();
            if (!employeeID.HasValue)
            {
                
            }
            var employee = EmployeeBL.GetByID(uow, employeeID.GetValueOrDefault());
            if (employee != null)
            {
                return Json(data: new { PhoneNumber = employee.PhoneNumber, Position = employee.Position });
            } else
            {
                return Json(data: new { PhoneNumber = "", Position = "" });
            }
        }

        public JsonResult GetLocationsByOrganizationAndFlowDefinitionID(int organizationID, FlowDefinitionID flowdefinitionID)
        {
            var locations = LocationBL.GetActiveByOrganisationAndFlowDefinitionID(uow, organizationID, flowdefinitionID);
            var options = (from location in locations
                                    orderby location.Name ascending
                                    select new Option(location.Name, location.LocationID.ToString())).ToList();
            return new JsonResult
            {
                Data = options
            };
        }

        public JsonResult GetDepartmentsByLocation(int locationID)
        {
            var departments = DepartmentBL.GetByLocationIDAndDepartmentTypeID(uow, locationID, DepartmentTypeID.Afdeling);
            var optionList = (from department in departments
                orderby department.Name
                select new Option(department.Name, department.DepartmentID.ToString())).ToList();

            return new JsonResult
            {
                Data = optionList
            };
        }

        public JsonResult GetDepartmentsByOrganization(int organizationID)
        {
            var departments = DepartmentBL.GetByOrganizationIDSimple(uow, organizationID);
            var optionList = (from department in departments.ToList()
                orderby department.Location.Name, department.Name
                select new Option(department.Location.Name + " - " + department.Name, department.DepartmentID.ToString())).ToList();

            return new JsonResult
            {
                Data = optionList
            };
        }

        [DonutOutputCache(CacheProfile = "Short")]
        public JsonResult GetDossierTagsByTransferID(int transferID)
        {
            var tags = FlowInstanceTagBL.GetAllByTransferID(transferID);
            return Json(tags, JsonRequestBehavior.AllowGet);
        }

        [DonutOutputCache(CacheProfile = "Short")]
        public JsonResult GetAllTagsByTransferID(int? transferID)
        {
            var flowinstance = FlowInstanceBL.GetByTransferID(uow, transferID ?? 0);
            if(flowinstance != null)
            {
                var tags = OrganizationProjectBL.GetActiveByOrganizationID(uow, flowinstance.StartedByOrganizationID ?? 0);
                var tagsjsondata = tags.Select(t => t.Name).ToArray();

                return Json(tagsjsondata, JsonRequestBehavior.AllowGet);
            }

            return null;
        }

        public JsonResult GetDefaultFlowDefinitionParticipation(OrganizationTypeID organizationtypeid)
        {
            var defaults = new List<FlowParticipationViewModel>();

            switch (organizationtypeid)
            {
                case OrganizationTypeID.Hospital:
                    defaults.Add(new FlowParticipationViewModel() { FlowDefinitionID = FlowDefinitionID.ZH_VVT, FlowDirection = FlowDirection.Sending, Participation = Participation.Active });
                    break;

                case OrganizationTypeID.CIZ:
                case OrganizationTypeID.HealthCareProvider:
                    defaults.Add(new FlowParticipationViewModel() { FlowDefinitionID = FlowDefinitionID.RzTP, FlowDirection = FlowDirection.Receiving, Participation = Participation.FaxMail });
                    defaults.Add(new FlowParticipationViewModel() { FlowDefinitionID = FlowDefinitionID.CVA, FlowDirection = FlowDirection.Receiving, Participation = Participation.FaxMail });
                    defaults.Add(new FlowParticipationViewModel() { FlowDefinitionID = FlowDefinitionID.ZH_VVT, FlowDirection = FlowDirection.Receiving, Participation = Participation.FaxMail });
                    defaults.Add(new FlowParticipationViewModel() { FlowDefinitionID = FlowDefinitionID.VVT_VVT, FlowDirection = FlowDirection.Any, Participation = Participation.FaxMail });
                    defaults.Add(new FlowParticipationViewModel() { FlowDefinitionID = FlowDefinitionID.HA_VVT, FlowDirection = FlowDirection.Receiving, Participation = Participation.FaxMail });
                    defaults.Add(new FlowParticipationViewModel() { FlowDefinitionID = FlowDefinitionID.VVT_ZH, FlowDirection = FlowDirection.Sending, Participation = Participation.Active });
                    break;

                case OrganizationTypeID.RegionaalCoordinatiepunt:
                    defaults.Add(new FlowParticipationViewModel() { FlowDefinitionID = FlowDefinitionID.HA_VVT, FlowDirection = FlowDirection.Any, Participation = Participation.Active });
                    break;

                case OrganizationTypeID.GeneralPracticioner:
                    defaults.Add(new FlowParticipationViewModel() { FlowDefinitionID = FlowDefinitionID.HA_VVT, FlowDirection = FlowDirection.Sending, Participation = Participation.Active });
                    break;

                default:
                    break;
            }

            return Json(defaults, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetTransferPointsByLocation(int locationID)
        {
            var departments = DepartmentBL.GetByLocationIDAndDepartmentTypeID(uow, locationID, DepartmentTypeID.Transferpunt);
            var optionList = (from department in departments
                orderby department.Name
                select new Option(department.Name, department.DepartmentID.ToString())).ToList();

            return new JsonResult
            {
                Data = optionList
            };
        }

        [DonutOutputCache(CacheProfile = "Short")]
        public JsonResult GetAllowedLocationsByOrganization(int organizationid, FunctionRoleTypeID functionroletypeid)
        {
            var pointuserinfo = PointUserInfo();
            var maxlevel = pointuserinfo.GetFunctionRoleLevelID(functionroletypeid);

            var locationidfilter = maxlevel > FunctionRoleLevelID.Location ? null : pointuserinfo.EmployeeLocationIDs.ToArray();

            var options = LocationBL.GetActiveByOrganisationID(uow, organizationid, locationIDFilter: locationidfilter)
                   .OrderBy(l => l.Name)
                   .Select(l => new Option { Text = l.Name, Value = l.LocationID.ToString() })
                   .ToList();

            return new JsonResult
            {
                Data = options,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public JsonResult GetAllowedEmployeesByOrganization(int organizationid)
        {
            var pointuserinfo = PointUserInfo();         

           var options = EmployeeBL.GetByOrganizationID(uow, organizationid)
                   .OrderBy(l => l.FullName())
                   .Select(l => new Option { Text = l.FullName(), Value = l.EmployeeID.ToString() })
                   .ToList();

            return new JsonResult
            {
                Data = options,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }


        public JsonResult GetLocationsByOrganization(int organizationid)
        {
            var options = LocationBL.GetActiveByOrganisationID(uow, organizationid)
                   .OrderBy(l => l.Name)
                   .Select(l => new Option { Text = l.Name, Value = l.LocationID.ToString() })
                   .ToList();

            return new JsonResult
            {
                Data = options, JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public string GetLocationNameStringByLocationID(int locationID)
        {
            var name = "";
            var location = LocationBL.GetByID(uow, locationID);
            if (location != null)
            {
                name = location.Name;
            }

            return name;
        }

        public string GetOrganizationNameStringByOrganizationID(int organizationID)
        {
            var name = "";
            var organization = OrganizationBL.GetByOrganizationID(uow, organizationID);
            if (organization != null)
            {
                name = organization.Name;
            }

            return name;
        }

        [DonutOutputCache(CacheProfile = "Short")]
        public JsonResult GetOrganizationDetailsByOrganizationID(int organizationID)
        {
            var organization = OrganizationBL.GetByOrganizationID(uow, organizationID);
            if (organization != null)
            {
                return Json(new { organization.OrganizationTypeID, RegionName = organization.Region?.Name }, JsonRequestBehavior.AllowGet);
            }

            return null;
        }

        public string GetDepartmentNameStringByDepartmentID(int departmentID)
        {
            var name = "";
            var department = DepartmentBL.GetByDepartmentID(uow, departmentID);

            if (department != null)
            {
                name = department.Name;
            }

            return name;
        }

        public string GetSpecialismNameStringBySpecialismID(int specialismID)
        {
            var name = "";
            var specialism = SpecialismBL.GetBySpecialismID(uow, specialismID);
            if (specialism != null)
            {
                name = specialism.Name;
            }
            return name;
        }

        [ValidateAntiFiddleInjection("departmentid")]
        public PartialViewResult DepartmentInfo(int departmentid)
        {
            var department = DepartmentBL.GetByDepartmentID(uow, departmentid);
            return PartialView(department);
        }

        [ValidateAntiFiddleInjection("employeeid")]
        public PartialViewResult EmployeeInfo(int employeeid)
        {
            var employee = EmployeeBL.GetByID(uow, employeeid);
            return PartialView(employee);
        }

        [ValidateAntiFiddleInjection("formtypeid")]
        public PartialViewResult FormTypeInfo(int formtypeid)
        {
            var formtype = FormTypeBL.GetByFormTypeID(uow, formtypeid);
            return PartialView(formtype);
        }

        [ValidateAntiFiddleInjection("locationid")]
        public PartialViewResult LocationInfo(int locationid)
        {
            var location = LocationBL.GetByID(uow, locationid);
            return PartialView(location);
        }

        [ValidateAntiFiddleInjection("organizationid")]
        public PartialViewResult OrganizationInfo(int organizationid)
        {
            var organization = OrganizationBL.GetByOrganizationID(uow, organizationid);
            return PartialView(organization);
        }

        [ValidateAntiFiddleInjection("postalcodeid")]
        public PartialViewResult PostalCodeInfo(int postalcodeid)
        {
            var postalcode = PostalCodeBL.GetByID(uow, postalcodeid);
            return PartialView(postalcode);
        }

        [ValidateAntiFiddleInjection("regionid")]
        public PartialViewResult RegionInfo(int regionid)
        {
            var region = RegionBL.GetByRegionID(uow, regionid);
            return PartialView(region);
        }

        [ValidateAntiFiddleInjection("serviceareaid")]
        public PartialViewResult ServiceAreaInfo(int serviceareaid)
        {
            var servicearea = ServiceAreaBL.GetByServiceAreaID(uow, serviceareaid);
            return PartialView(servicearea);
        }

        public JsonResult GetAfterCareFinancingJson(int? aftercaretypeid)
        {
            using (var lookupbl = new LookUpBL())
            {
                return Json(lookupbl.GetAfterCareFinancing(null, aftercaretypeid: aftercaretypeid), JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetAfterCareTypes(int formTypeID, int afterCareCategoryID)
        {
            var aftercaretypes = AfterCareTypeBL.GetByFormTypeAndCategory(uow, formTypeID, afterCareCategoryID).ToList();
            return Json(aftercaretypes.Select(x => new Option(x.Name, x.AfterCareTypeID)).ToList(), JsonRequestBehavior.AllowGet);
        }

        public IList<Option> GetAfterCareTypesByValue(int? currentvalue, int? formtypeid = null)
        {
            var aftercaretypes = AfterCareTypeBL.GetByCurrentValue(uow, currentvalue, formtypeid).ToList();
            return aftercaretypes.Select(x => new Option(x.Name, x.AfterCareTypeID)).ToList();
        }

        [DonutOutputCache(CacheProfile = "Short")]
        public JsonResult GetDepartmentTypeByOrganizationID(int? organizationid)
        {
            var options = new List<Option>();
            var organization = OrganizationBL.GetByOrganizationID(uow, organizationid.GetValueOrDefault());
            if (organization != null)
            {
                options = DepartmentTypeBL.GetByOrganizationTypeID(uow, organization.OrganizationTypeID)
                   .OrderBy(dt => dt.Name)
                   .Select(dt => new Option { Text = dt.Name, Value = dt.DepartmentTypeID.ToString() })
                   .ToList();
            }

            return new JsonResult
            {
                Data = options,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public JsonResult GetAfterCareTypesByOrganizationID(int? organizationid)
        {
            var options = new List<Option>();
            var organization = OrganizationBL.GetByOrganizationID(uow, organizationid.GetValueOrDefault());
            if (organization != null)
            {
                options = AfterCareTypeBL.GetByOrganizationTypeID(organization.OrganizationTypeID)
                   .OrderBy(dt => dt.Name)
                   .Select(dt => new Option { Text = dt.Name, Value = dt.AfterCareTypeID.ToString() })
                   .ToList();
            }

            return new JsonResult
            {
                Data = options,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public JsonResult GetSpecialismByOrganizationID(int? organizationid)
        {
            var options = new List<Option>();
            var organization = OrganizationBL.GetByOrganizationID(uow, organizationid.GetValueOrDefault());
            if (organization != null)
            {
                options = SpecialismBL.GetByOrganizationTypeID(uow, organization.OrganizationTypeID)
                   .OrderBy(dt => dt.Name)
                   .Select(dt => new Option { Text = dt.Name, Value = dt.SpecialismID.ToString() })
                   .ToList();
            }

            return new JsonResult
            {
                Data = options,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public JsonResult GetAllDepartmentsByLocationID(int? locationID)
        {
            var options = DepartmentBL.GetByLocationID(uow, locationID.GetValueOrDefault())
                .OrderBy(d => d.Name)
                .Select(d => new Option(d.Name, d.DepartmentID))
                .ToList();
            return new JsonResult
            {
                Data = options,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public JsonResult GetAllOrganizationsByRegions(List<int> ids, int? organizationtypeid)
        {
            List<Option> options = new List<Option>();
            if (ids != null)
            {
                var organizations = new List<Organization>();
                if (organizationtypeid.HasValue)
                {
                    organizations = OrganizationBL.GetActiveByRegionIDsAndOrganizationTypeID(uow, ids, (int)organizationtypeid.Value).ToList();
                }
                else
                {
                    organizations = OrganizationBL.GetActiveByRegionIDs(uow, ids).ToList();
                }

                options = organizations.OrderBy(l => l.Name)
                       .Select(l => new Option(l.Name, l.OrganizationID, false, AntiFiddleInjection.CreateDigest(l.OrganizationID), (bool)l.Inactive))
                       .ToList();
            }

            return new JsonResult
            {
                Data = new { success = true, options = options },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public JsonResult GetAllLocationsByOrganizations(List<int> ids)
        {
            List<Option> options = new List<Option>();
            if (ids != null)
            {
                options = LocationBL.GetByOrganisationIDs(uow, ids)
                       .OrderBy(l => l.Name)
                       .Select(l => new Option(l.Name, l.LocationID, false, AntiFiddleInjection.CreateDigest(l.LocationID), (bool)l.Inactive))
                       .ToList();
            }

            return new JsonResult
            {
                Data = new { success = true, options = options },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public JsonResult GetAllDepartmentsByLocations(List<int> ids)
        {
            List<Option> options = new List<Option>();
            if (ids != null)
            {
                options.AddRange(DepartmentBL.GetByLocationIDs(uow, ids, true)
                    .OrderBy(d => d.Name)
                    .Select(d => new Option(d.Name, d.DepartmentID, false, AntiFiddleInjection.CreateDigest(d.DepartmentID), (bool)d.Inactive))
                    .ToList());
            }

            return new JsonResult
            {
                Data = new { success = true, options = options },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }
    }
}
