﻿using Point.Business.Logic;
using Point.Infrastructure.Attributes;
using System;
using System.Web;
using System.Web.Mvc;

namespace Point.MVC.WebApplication.Controllers
{
    public class OrganizationLogoController : PointBaseController
    {
        [ValidateAntiFiddleInjection("OrganizationID")]
        public FileResult GetByOrganizationID(int organizationid)
        {
            var organization = OrganizationBL.GetByOrganizationID(uow, organizationid);
            if (organization.OrganizationLogoID.HasValue)
            {
                var organizationlogo = OrganizationLogoBL.GetByID(uow, organization.OrganizationLogoID.Value);
                return File(organizationlogo.FileData, organizationlogo.ContentType, organizationlogo.FileName);
            }
            return File("~/Images/nosignature.png", "image/png");
        }

        public FileResult GetByID(int organizationlogoid)
        {

             var organizationlogo = OrganizationLogoBL.GetByID(uow, organizationlogoid);
             return File(organizationlogo.FileData, organizationlogo.ContentType, organizationlogo.FileName);             
        }

        public FileContentResult Logo(int organizationlogoid)
        {
            var organizationlogo = OrganizationLogoBL.GetByID(uow, organizationlogoid);
            if (organizationlogo != null)
            {
                Response.Cache.SetCacheability(HttpCacheability.Public);
                Response.Cache.SetExpires(DateTime.Now.AddMinutes(60));
                Response.Cache.SetMaxAge(TimeSpan.FromMinutes(60));

                return File(organizationlogo.FileData, organizationlogo.ContentType, organizationlogo.FileName);
            }
            return null;
        }
    }
}