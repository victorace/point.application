﻿using Point.Business.Logic;
using Point.Database.Context;
using Point.Database.Repository;
using Point.ServiceBus.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Point.MVC.WebApplication.Controllers
{
    //For now just controller with a separate uow
    //Note this is not a "test"
    [SessionState(System.Web.SessionState.SessionStateBehavior.ReadOnly)]
    public class ServiceBusCheckController : Controller
    {
        public readonly UnitOfWork<PointContext> uow = new UnitOfWork<PointContext>();

        public ActionResult ServiceBusCheck()
        {
            ServiceBusBL.SendMessage(uow, "Check", "ServiceBusCheck", new ServiceBusCheck() { MachineName = Environment.MachineName, TimeStamp = DateTime.Now, UniqueID = DateTime.Now.Ticks });

            return Json(new { Succeeded = true }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ServiceBusExpiredCount()
        {
            int expiredservicebusitems = ServiceBusLogBL.GetExpiredItems(uow).Count();

            return Json(new { ExpiredItems = expiredservicebusitems }, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> ServiceBusQueueCount(string queuename = "")
        {
            Dictionary<string, long> counters = new Dictionary<string, long>();

            List<string> queues = new List<string>();
            if (String.IsNullOrEmpty(queuename))
            {
                queues.Add(ServiceBus.Helpers.QueueExceptions);
                queues.Add(ServiceBus.Helpers.QueueOrganizationSearch);
                queues.Add(ServiceBus.Helpers.QueuePhaseQueue);
                queues.Add(ServiceBus.Helpers.QueueReadmodel);
            }
            else
            {
                queues.Add(queuename);
            }
            foreach (var queue in queues)
            {
                long count = await ServiceBus.Helpers.GetMessageCount(queue);
                counters.Add(queue, count);
            }
            return Json(counters, JsonRequestBehavior.AllowGet);

        }
    }
}