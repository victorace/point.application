﻿using Point.Business.Logic;
using Point.Database.Extensions;
using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Infrastructure.Attributes;
using Point.Infrastructure.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Point.MVC.WebApplication.Controllers
{
    public class TransferTaskController : PointFormController
    {
        public SecureActionResult TransferTasks(int formtypeid, bool readOnly = false)
        {
            var vm = TransferTaskViewBL.GetByTransferID(uow, TransferID, formtypeid, readOnly, true);
            return new SecureActionResult(ActionName, vm, true);
        }

        public ActionResult List(int formtypeid, bool readOnly = false, bool ispartial = false)
        {
            var pointuserinfo = PointUserInfo();
            var flowinstance = FlowInstanceBL.GetByTransferID(uow, TransferID);
            var phasedefinition = PhaseDefinitionCacheBL.GetByID(uow, PhaseDefinitionID);
            if (flowinstance != null && phasedefinition != null)
            {
                // just incase readOnly is tampered with, we will double check
                // the logged in user against the PhaseDefinitionRights.
                // setting readOnly accordingly.

                var isclosed = FlowInstanceBL.IsFlowClosed(uow, flowinstance.FlowInstanceID);

                var rights = PhaseDefinitionRightsBL.GetFormRightsPhaseDefinitionID(uow,
                    pointuserinfo, phasedefinition, flowinstance);
                if (readOnly == false && isclosed || rights < Rights.W)
                {
                    readOnly = true;
                }
            }

            var vm = TransferTaskViewBL.GetByTransferID(uow, TransferID, formtypeid, readOnly, ispartial);
            return new SecureActionResult(ActionName, vm, vm.IsPartial);
        }

        [ValidateAntiFiddleInjection("transferid")]
        public ActionResult Add(int formtypeid)
        {
            var vm = new TransferTaskViewModel()
            {
                TransferID = this.TransferID,
                FormTypeID = formtypeid
            };
            return View("Edit", vm);
        }

        [ValidateAntiFiddleInjection("transfermemoid")]
        public ActionResult AddFromTransferMemo(int transfermemoid, int formtypeid)
        {
            var comments = string.Empty;

            var transfermemo = TransferMemoBL.GetByID(uow, transfermemoid);
            if (transfermemo != null)
            {
                comments = transfermemo.MemoContent;
            }

            var vm = new TransferTaskViewModel()
            {
                TransferID = this.TransferID,
                Comments = comments,
                FormTypeID = formtypeid
            };
            return View("Edit", vm);
        }

        [ValidateAntiFiddleInjection("transfertaskid")]
        public ActionResult Edit(int transfertaskid, int formtypeid)
        {
            var vm = TransferTaskViewBL.GetByTransferTaskID(uow,
                transfertaskid, formtypeid, false);
            return View("Edit", vm);
        }

        [ValidateAntiFiddleInjection("transferid")]
        public ActionResult SetStatus(int transfertaskid, int formtypeid, Status status)
        {
            var pointuserinfo = PointUserInfo();
            var formtype = FormTypeBL.GetByFormTypeID(uow, formtypeid);
            if (formtype == null) throw new Exception("SetStatus failed, invalid FormType");
            var logentry = LogEntryBL.Create(formtype, pointuserinfo);
            var transfertask = TransferTaskBL.SetStatus(uow, transfertaskid, status, logentry);
            FlowInstanceSearchValuesBL.CalculateReadValuesByTransferIDAsync(uow, TransferID);

            return Json(new {
                TransferTaskID = transfertask.TransferTaskID,
                CompletedByEmployeeID = transfertask.CompletedByEmployeeID,
                CompletedByEmployee = transfertask.CompletedByEmployee.FullName(),
                StatusDescription = transfertask.Status.GetDescription(),
                Description = transfertask.Description
            }, JsonRequestBehavior.AllowGet);
        }

        [ValidateAntiFiddleInjection("transfertaskid")]
        public ActionResult Save(TransferTaskViewModel viewmodel)
        {
            if (ModelState.IsValid)
            {
                var pointuserinfo = PointUserInfo();
                if (viewmodel.FormTypeID == 0) throw new Exception("SetStatus failed, empty FormType");

                var logentry = LogEntryBL.Create((FlowFormType)viewmodel.FormTypeID, pointuserinfo);
                TransferTaskBL.Save(uow, viewmodel, logentry);

                FlowInstanceSearchValuesBL.CalculateReadValuesByTransferIDAsync(uow, TransferID);
            }

            return Json("ok", JsonRequestBehavior.AllowGet);
        }

        [ValidateAntiFiddleInjection("transfertaskid")]
        public ActionResult Delete(int transfertaskid, int formtypeid)
        {
            var pointuserinfo = PointUserInfo();
            var formtype = FormTypeBL.GetByFormTypeID(uow, formtypeid);
            if (formtype == null) throw new Exception("SetStatus failed, invalid FormType");
            var logentry = LogEntryBL.Create(formtype, pointuserinfo);
            var deleted = TransferTaskBL.Delete(uow, transfertaskid, logentry);
            if (deleted)
            {
                FlowInstanceSearchValuesBL.CalculateReadValuesByTransferIDAsync(uow, TransferID);
            }
            return Json(new { deleted, transfertaskid }, JsonRequestBehavior.AllowGet);
        }
    }
}