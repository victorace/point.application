﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DevTrends.MvcDonutCaching;
using Point.Business.Logic;
using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Infrastructure.Attributes;
using Point.Infrastructure.Extensions;
using Point.MVC.WebApplication.Attributes;

namespace Point.MVC.WebApplication.Controllers
{
    public class TempletController : PointFormController
    {
        public ActionResult List()
        {
            PageTitle = "Regio-documenten";
            PageHeader = "Regio-documenten";

            var templets = TempletBL.GetTempletsByRegionID(uow, PointUserInfo().Region.RegionID);
            var templetViews = TempletViewBL.FromModelList(uow, templets);
            return View(templetViews);
        }

        [ValidateAntiFiddleInjection("templetID")]
        public FileResult Download(int templetID)
        {
            var templet = TempletBL.GetByID(uow, templetID);
            return File(templet.FileData, templet.ContentType, Path.GetFileName(templet.FileName));
        }


    }
}