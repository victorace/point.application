﻿using System.Web.Mvc;
using Point.Business.Logic;
using Point.Infrastructure;
using Point.Models.Enums;
using Point.MVC.WebApplication.Helpers.Pdf;

namespace Point.MVC.WebApplication.Controllers
{
    public class FrequencyController : PointFormController
    {
        public ActionResult Print(int? frequencyID)
        {
            if (frequencyID.HasValue && frequencyID.Value <= 0)
            {
                frequencyID = null;
            }

            var frequencyPrint = FrequencyPrintViewBL.Get(uow, TransferID, frequencyID, PointUserInfo());
            return new ViewAsPdf("Print", frequencyPrint);
        }

        public JsonResult Reopen(int frequencyid, int transferid, int phaseinstanceid, int formsetversionid)
        {
            bool success = false;

            var formsetversion = FormSetVersionBL.GetByID(uow, formsetversionid);
            var frequency = FrequencyBL.GetByID(uow, frequencyid);
            if (formsetversion != null || frequency != null)
            {
                var logentry = LogEntryBL.Create(formsetversion.FormType, PointUserInfo());

                var phaseinstance = formsetversion.PhaseInstance;
                if (phaseinstance != null)
                {
                    PhaseInstanceBL.SetStatusActive(uow, phaseinstance, logentry);
                    FrequencyBL.SetStatus(uow, frequency, FrequencyStatus.Active, logentry);

                    FlowInstanceSearchValuesBL.CalculateReadValuesByTransferIDAsync(uow, frequency.TransferID);

                    success = true;
                }

            }

            return Json(new { success }, JsonRequestBehavior.AllowGet);
        }
    }
}