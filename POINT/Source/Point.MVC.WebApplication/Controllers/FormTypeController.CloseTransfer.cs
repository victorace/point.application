﻿using DevTrends.MvcDonutCaching;
using Point.Business.Helper;
using Point.Business.Logic;
using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Infrastructure.Extensions;
using Point.MVC.WebApplication.Helpers;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using System;

namespace Point.MVC.WebApplication.Controllers
{
    public partial class FormTypeController
    {
        public ActionResult CloseTransferHA()
        {
            var viewModel = FormTypeCloseTransferViewModelHelper.GetCloseTransferHAViewModel(uow, GetGlobalPropertiesFormType(getFlowWebFields: true));
            if (viewModel.HasErrors)
            {
                return Content(viewModel.ErrorMessage);
            }

            // this.FormType is empty if we get here via the menu
            FormType = FormTypeBL.GetByFormTypeID(uow, (int)FlowFormType.AfsluitenDossierHA);
            PageTitle = FormType.Name;
            return View(viewModel);
        }


        [HttpPost]
        public ActionResult CloseTransferHA([ModelBinder(typeof(FlowWebFieldBinder))] List<FlowWebField> model, FormCollection formCollection)
        {
            var flowinstance = FlowInstanceBL.GetByTransferID(uow, TransferID);
            if (flowinstance == null)
            {
                return Content(FormTypeCloseTransferViewModelHelper.NoFlowInstanceMessage);
            }

            // this.FormType is empty if we get here via the menu
            FormType = FormTypeBL.GetByFormTypeID(uow, (int)FlowFormType.AfsluitenDossierHA);
            PageTitle = FormType.Name;

            var flowinstanceID = flowinstance.FlowInstanceID;

            if (FlowInstanceBL.IsFlowClosed(uow, flowinstanceID))
            {
                return GetCloseTransferJsonResult(TransferID);
            }

            var pointuserinfo = PointUserInfo();
            
            if (!PhaseInstanceBL.HasEndFlowRights(uow, flowinstance.FlowDefinitionID, ControllerName, ActionName, pointuserinfo))
            {
                return GetCloseTransferJsonResult(TransferID);
            }

            var logentry = LogEntryBL.Create(FormType, pointuserinfo);
            var flowWebFields = new List<FlowWebField>(model);

            FlowWebFieldBL.SetValuesOriginCopyToAll(uow, flowWebFields, TransferID, FormType.FormTypeID, logentry, pointuserinfo, ViewData, closeDossier: true);
            FlowInstanceBL.CloseFlowInstance(uow, flowinstanceID, logentry.EmployeeID, logentry);
            ResetOutputCacheByTransferID(TransferID);

            FlowInstanceSearchValuesBL.CalculateReadValuesByTransferIDAsync(uow, TransferID);

            return GetCloseTransferJsonResult(TransferID);
        }


        public ActionResult CloseTransferVVT(int transferID, bool? confirmed)
        {
            var globalProps = GetGlobalProperties(getFlowInstance: true);

            var flowInstance = globalProps.FlowInstance;
            if (flowInstance == null)
            {
                return Content(FormTypeCloseTransferViewModelHelper.NoFlowInstanceMessage);
            }

            // this.FormType is empty if we get here via the menu
            FormType = FormTypeBL.GetByFormTypeID(uow, (int)FlowFormType.AfsluitenDossier);
            PageTitle = FormType.Name;

            var pointUserInfo = globalProps.PointUserInfo;

            if (!confirmed.HasValue)
            {
                var viewModel = new CloseTransferVVTViewModel
                {
                    IsAllowed = PhaseInstanceBL.HasEndFlowRights(uow, flowInstance.FlowDefinitionID, globalProps.ControllerName, globalProps.ActionName, pointUserInfo)
                };
                viewModel.SetGlobalProperties(globalProps);

                return View(viewModel);
            }
            if (confirmed == false)
            {
                return RedirectToAction("Dashboard", "FlowMain", new { globalProps.TransferID });
            }

            var logEntry = LogEntryBL.Create(FormType, pointUserInfo);
            FlowInstanceBL.CloseFlowInstance(uow, flowInstance.FlowInstanceID, logEntry.EmployeeID, logEntry);
            ResetOutputCacheByTransferID(globalProps.TransferID);

            FlowInstanceSearchValuesBL.CalculateReadValuesByTransferIDAsync(uow, TransferID);

            if (!Request.IsAjaxRequest())
            {
                return RedirectToAction("Dashboard", "FlowMain", new { globalProps.TransferID });
            }

            Response.StatusCode = (int)(ErrorMessage == "" ? HttpStatusCode.OK : HttpStatusCode.BadRequest);

            return Json(new { ErrorMessage }, JsonRequestBehavior.DenyGet);
        }

        [DonutOutputCache(Duration = 0)]
        public ActionResult CloseTransferZHVVT()
        {
            var viewModel = FormTypeCloseTransferViewModelHelper.GetCloseTransferZHVVTViewModel(uow, GetGlobalPropertiesFormType(getFlowWebFields: true), this);
            if (viewModel.HasErrors)
            {
                return Content(viewModel.ErrorMessage);
            }

            // this.FormType is empty if we get here via the menu
            FormType = FormTypeBL.GetByFormTypeID(uow, (int)FlowFormType.AfsluitenDossierZHVVT);
            PageTitle = FormType.Name;
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult CloseTransferZHVVT([ModelBinder(typeof(FlowWebFieldBinder))] List<FlowWebField> model, FormCollection formCollection)
        {
            // TODO: Check if resetOutputCacheByTransferID is really NOT needed or was just forgotten
            return GetCloseTransferResult(model, FlowFormType.AfsluitenDossierZHVVT, resetOutputCacheByTransferID: false);
        }

        public ActionResult CloseTransferRzTP()
        {
            var viewModel = FormTypeCloseTransferViewModelHelper.GetCloseTransferRzTPViewModel(uow, GetGlobalPropertiesFormType(getFlowWebFields: true));
            if (viewModel.HasErrors)
            {
                return Content(viewModel.ErrorMessage);
            }

            // this.FormType is empty if we get here via the menu
            FormType = FormTypeBL.GetByFormTypeID(uow, (int)FlowFormType.AfsluitenDossierRzTP);
            PageTitle = FormType.Name;
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult CloseTransferRzTP([ModelBinder(typeof(FlowWebFieldBinder))] List<FlowWebField> model, FormCollection formCollection)
        {
            var formTypeID = (int)FlowFormType.AfsluitenDossierRzTP;
            var flowInstance = FlowInstanceBL.GetByTransferID(uow, TransferID);
            if(flowInstance == null)
            {
                return Content(FormTypeCloseTransferViewModelHelper.NoFlowInstanceMessage);
            }

            var phaseDefinition = PhaseDefinitionCacheBL.GetByFlowDefinitionAndFormType(uow, flowInstance.FlowDefinitionID, formTypeID);
            if(phaseDefinition == null)
            {
                throw new Exception("Phasedefinition is niet bekend voor deze flow");
            }

            var flowWebFields = new List<FlowWebField>(model);
            var datumEindeBehandelingMedischSpecialist = model.FirstOrDefault(fwf => fwf.Name == FlowFieldConstants.Name_DatumEindeBehandelingMedischSpecialist);
            if (!string.IsNullOrWhiteSpace(datumEindeBehandelingMedischSpecialist?.Value))
            {
                var realizedDischargeDate = FlowWebFieldBL.GetFieldsFromFormTypeID(uow, formTypeID, phaseDefinition.PhaseDefinitionID, TransferID, ViewData).FirstOrDefault(fwf => fwf.Name == FlowFieldConstants.Name_RealizedDischargeDate);
                if (realizedDischargeDate != null)
                {
                    realizedDischargeDate.Value = datumEindeBehandelingMedischSpecialist.Value;
                    flowWebFields.Add(realizedDischargeDate);
                }
            }

            return GetCloseTransferResult(flowWebFields, FlowFormType.AfsluitenDossierRzTP);
        }

        public ActionResult CloseTransferCVA()
        {
            var viewModel = FormTypeCloseTransferViewModelHelper.GetCloseTransferCvaViewModel(uow, GetGlobalPropertiesFormType(getFlowWebFields: false));
            if (viewModel.HasErrors)
            {
                return Content(viewModel.ErrorMessage);
            }

            // this.FormType is empty if we get here via the menu
            FormType = FormTypeBL.GetByFormTypeID(uow, (int)FlowFormType.AfsluitenDossierCVA);
            PageTitle = FormType.Name;

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult CloseTransferCVA([ModelBinder(typeof(FlowWebFieldBinder))] List<FlowWebField> model, FormCollection formCollection)
        {
            return GetCloseTransferResult(model, FlowFormType.AfsluitenDossierCVA);
        }

        private ActionResult GetCloseTransferJsonResult(int transferID)
        {
            return Json(new
            {
                Url = Url.Action("Dashboard", "FlowMain", new { transferID }),
                ValidationErrors = new string[] { },
                ShowErrors = false
            });
        }

        private ActionResult GetCloseTransferResult(List<FlowWebField> flowWebFields, FlowFormType flowFormType, bool resetOutputCacheByTransferID = true)
        {
            var flowInstance = FlowInstanceBL.GetByTransferID(uow, TransferID);
            if (flowInstance == null)
            {
                return Content(FormTypeCloseTransferViewModelHelper.NoFlowInstanceMessage);
            }

            // this.FormType is empty if we get here via the menu
            FormType = FormTypeBL.GetByFormTypeID(uow, (int)flowFormType);
            PageTitle = FormType.Name;

            var isFlowClosed = FlowInstanceBL.IsFlowClosed(uow, flowInstance.FlowInstanceID);
            if (isFlowClosed)
            {
                return GetCloseTransferJsonResult(TransferID);
            }

            var pointUserInfo = PointUserInfo();

            if (!PhaseInstanceBL.HasEndFlowRights(uow, flowInstance.FlowDefinitionID, ControllerName, ActionName, pointUserInfo))
            {
                return GetCloseTransferJsonResult(TransferID);
            }

            var logEntry = LogEntryBL.Create(FormType, pointUserInfo);

            FlowWebFieldBL.SetValuesOriginCopyToAll(uow, flowWebFields, TransferID, FormType.FormTypeID, logEntry, pointUserInfo, ViewData, closeDossier: true);
            FlowInstanceBL.CloseFlowInstance(uow, flowInstance.FlowInstanceID, logEntry.EmployeeID, logEntry);
            if (resetOutputCacheByTransferID)
            {
                ResetOutputCacheByTransferID(TransferID);
            }

            // HA bericht: wordt op verkeerd moment verzonden https://linkassist.visualstudio.com/POINT.Application/_workitems/edit/14276
            if (OrganizationHelper.MessageToGPByClose(uow, flowInstance))
            {
                MedVRIBL.SendMedvri(uow, flowInstance, flowFormType, pointUserInfo);
            }

            FlowInstanceSearchValuesBL.CalculateReadValuesByTransferIDAsync(uow, TransferID);

            return GetCloseTransferJsonResult(TransferID);
        }
    }
}
