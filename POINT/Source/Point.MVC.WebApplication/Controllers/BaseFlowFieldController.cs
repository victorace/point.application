﻿using Point.Business.Logic;
using Point.Database.Extensions;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Infrastructure.Exceptions;
using Point.Infrastructure.Extensions;
using Point.MVC.WebApplication.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using System.Web.Routing;

namespace Point.MVC.WebApplication.Controllers
{


    public class BaseFlowFieldController : PointFormController
    {
        [HttpPost]
        public List<FlowWebField> Get(int formSetVersionID = -1)
        {
            FormType formtype = null;

            if (formSetVersionID <= 0)
            {
                formtype = FormTypeBL.GetByURL(uow, ControllerName, ActionName);
            }
            else
            {
                var formsetversion = FormSetVersionBL.GetByID(uow, formSetVersionID);
                if (formsetversion != null)
                {
                    formtype = formsetversion.FormType;
                }
            }

            if (formSetVersionID <= 0)
            {
                if (formtype == null)
                {
                    throw new Exception("FormType not found");
                }
                ViewBag.FormTypeID = formtype.FormTypeID;
                return FlowWebFieldBL.GetFieldsFromFormTypeID(uow, formtype.FormTypeID, PhaseDefinitionID, TransferID, ViewData).ToList();
            }

            var flowwebfields = FlowWebFieldBL.GetFieldsFromFormSetVersionID(uow, formSetVersionID, ViewData).ToList();
            if (!ShowHistory)
            {
                return flowwebfields;
            }

            var historyfields = FormHistoryBL.GetMutFlowFieldValues(uow, formSetVersionID, flowwebfields.Select(it => it.FlowFieldID)).ToList();

            var historyflowfieldids =
                historyfields.GroupBy(it => it.FlowFieldID)
                    .Where(grp => grp.Count() > 1)
                    .SelectMany(grp => grp.Where(it1 => HistoryFromDate == null || it1.Timestamp >= HistoryFromDate).Select(it => it.FlowFieldID)).ToArray();

            flowwebfields.Where(fwf => historyflowfieldids.Contains(fwf.FlowFieldID))
                .ToList()
                .ForEach(it =>
                {
                    it.HasHistory = true;
                    it.FormSetVersionID = formSetVersionID;
                });

            return flowwebfields;
        }

        public FormSetVersion SaveFormsetAndValidFields(List<FlowWebField> model, string description = null)
        {
            return SaveFormsetAndValidFields(model, -1, PhaseInstanceID, description: description);
        }

        public FormSetVersion SaveFormsetAndValidFields(List<FlowWebField> model, int formTypeID, int phaseInstanceID, bool validate = true, string description = null)
        {
            var htmlRegEx = new Regex(@"<[^>]+>");
            foreach (var field in model)
            {
                if (field.Value == null)
                {
                    continue;
                }
                if (htmlRegEx.IsMatch(field.Value))
                {
                    field.Value = htmlRegEx.Replace(field.Value, "");
                }
            }

            var pointUserInfo = PointUserInfo();
            var flowinstance = FlowInstanceBL.GetByTransferID(uow, TransferID);
            var formSetVersionID = FormSetVersionID;
            FormSetVersion formSetVersion;
            LogEntry logentry;

            if (phaseInstanceID <= 0 && PhaseDefinitionID > 0 && TransferID > 0)
            {
                var existingPhaseInstance = PhaseInstanceBL.GetAllByTransferAndPhaseDefinition(uow, TransferID, PhaseDefinitionID)
                    .Where(it => it.FormSetVersion.Any(fsv => fsv.Description == description))
                    .OrderByDescending(it => it.PhaseInstanceID).FirstOrDefault();

                if (existingPhaseInstance != null)
                {
                    phaseInstanceID = existingPhaseInstance.PhaseInstanceID;
                }
            }

            if (phaseInstanceID > 0 && formSetVersionID <= 0)
            {
                var existingFormSetVersions = FormSetVersionBL.GetByPhaseInstanceID(uow, phaseInstanceID).ToList();
                if (existingFormSetVersions.Any())
                {
                    SendException(new PointJustLogException("Double FormSetVersion prevented"), HttpContext);
                    var firstOrDefault = existingFormSetVersions.OrderByDescending(it => it.FormSetVersionID).FirstOrDefault();
                    if (firstOrDefault != null)
                    {
                        formSetVersionID = firstOrDefault.FormSetVersionID;
                    }
                }
            }

            if (formSetVersionID <= 0)
            {
                FormType formtype;

                if (formTypeID <= 0)
                {
                    formtype = FormTypeBL.GetByURL(uow, ControllerName, ActionName);
                    if (formtype == null)
                    {
                        throw new Exception("No formtype for this URL");
                    }

                    formTypeID = formtype.FormTypeID;
                }
                else
                {
                    formtype = FormTypeBL.GetByFormTypeID(uow, formTypeID);
                }

                logentry = LogEntryBL.Create(formtype, pointUserInfo);

                if (phaseInstanceID <= 0)
                {
                    var phasedefinition = PhaseDefinitionCacheBL.GetByID(uow, PhaseDefinitionID);
                    var phaseinstance = PhaseInstanceBL.Create(uow, flowinstance.FlowInstanceID, phasedefinition.PhaseDefinitionID, logentry);
                    if (phaseinstance == null)
                    {
                        return null;
                    }

                    phaseInstanceID = phaseinstance.PhaseInstanceID;
                }
                formSetVersion = FormSetVersionBL.CreateFormSetVersion(uow, TransferID, formTypeID, phaseInstanceID, pointUserInfo, logentry, description: description);
                uow.Save();

                //Strange behaviour: Force reload with phaseinstance property otherwise SaveResult will not work
                var oldversion = formSetVersion;
                formSetVersion = uow.FormSetVersionRepository.Get(fs => fs.FormSetVersionID == oldversion.FormSetVersionID, includeProperties: "PhaseInstance").FirstOrDefault();
            }
            else
            {
                formSetVersion = FormSetVersionBL.GetByID(uow, formSetVersionID);
                logentry = LogEntryBL.Create(formSetVersion.FormType, pointUserInfo);
                FormSetVersionBL.FillUpdateData(uow, formSetVersion, logentry, pointUserInfo.Employee.FullName(), description: description);
                uow.Save();
            }

            if (formSetVersion == null)
            {
                return null;
            }

            formSetVersionID = formSetVersion.FormSetVersionID;
            model = FlowWebFieldBL.HandleOriginFormTypeFields(uow, model, formSetVersion.FormTypeID);

            var validfields = model.Where(fwf => !fwf.ReadOnly).ToList();

            FlowWebFieldBL.SaveValues(uow, flowinstance.FlowInstanceID, validfields, formSetVersionID, logentry);
            FlowWebFieldBL.SetValuesOriginCopyToAll(uow, validfields, TransferID, formSetVersion.FormTypeID, logentry, pointUserInfo, ViewData);

            return formSetVersion;
        }

        public ActionResult SaveResult(FlowWebFieldModelState modelState, int phaseDefinitionID, int formSetVersionID, int phaseInstanceID)
        {
            var saveresult = new SaveResult
            {
                ActionName = ActionName,
                ControllerName = ControllerName,
                RouteValues = new RouteValueDictionary(new { TransferID, phaseDefinitionID, formSetVersionID, phaseInstanceID }),

                TransferID = TransferID,
                ForceReload = ForceReload,
                PhaseDefinitionID = phaseDefinitionID,
                FormSetVersionID = formSetVersionID,
                ValidationErrors = modelState.Errors,
                ShowErrors = !modelState.IsValid && ShowValidationErrors
            };

            return saveresult;
        }

        public ActionResult SaveResult(FlowWebFieldModelState modelState, FormSetVersion formSetVersion)
        {
            return GetSaveResult(modelState, formSetVersion, null);
        }

        public ActionResult SaveResult(FlowWebFieldModelState modelState, FormSetVersion formSetVersion, int? redirectToPhaseDefinitionID)
        {
            return GetSaveResult(modelState, formSetVersion, redirectToPhaseDefinitionID);
        }

        public ActionResult SaveResult(FlowWebFieldModelState modelState, FormSetVersion formSetVersion, RouteValueDictionary extraRouteValues)
        {
            var saveresult = GetSaveResult(modelState, formSetVersion, null);
            saveresult.RouteValues.AddRange(extraRouteValues);

            return saveresult;
        }

        private SaveResult GetSaveResult(FlowWebFieldModelState modelState, FormSetVersion formSetVersion, int? redirectToPhaseDefinitionID)
        {
            string actionname = ActionName;
            string controllername = ControllerName;
            int formsetversionid = formSetVersion.FormSetVersionID;
            int phasedefinitionid = formSetVersion.PhaseInstance.PhaseDefinitionID;
            int phaseinstanceid = formSetVersion.PhaseInstanceID ?? -1;
            int formtypeid = formSetVersion.FormTypeID;

            bool toDashboard = true;
            if (NextPhaseHandled || DefinitiveHandled)
            {
                var phasedefinition = PhaseDefinitionBL.GetByID(uow, phasedefinitionid);

                if (!redirectToPhaseDefinitionID.HasValue && phasedefinition?.RedirectToPhaseDefinitionIDOnNext != null)
                {
                    redirectToPhaseDefinitionID = phasedefinition.RedirectToPhaseDefinitionIDOnNext.Value;
                }

                if (FlowInstance != null && redirectToPhaseDefinitionID.HasValue)
                {
                    var formtype = FormTypeBL.GetByPhaseDefinitionID(uow, redirectToPhaseDefinitionID.Value);
                    if (formtype != null)
                    {
                        actionname = formtype.GetActionName();
                        controllername = formtype.GetControllerName();
                        phaseinstanceid = -1;
                        phasedefinitionid = redirectToPhaseDefinitionID.Value;
                        formsetversionid = -1;
                        formtypeid = formtype.FormTypeID;

                        var lastformsetversion = FormSetVersionBL.GetLastByTransferIDAndPhaseDefinitionID(uow, TransferID, redirectToPhaseDefinitionID.Value);
                        if (lastformsetversion != null)
                        {
                            formsetversionid = lastformsetversion.FormSetVersionID;
                            phaseinstanceid = lastformsetversion.PhaseInstanceID ?? -1;
                        }

                        toDashboard = false;
                    }
                }
            }

            HandleThirdPartyAsync(formsetversionid, PointUserInfo());

            var saveresult = new SaveResult
            {
                ActionName = actionname,
                ControllerName = controllername,
                RouteValues = new RouteValueDictionary(new { TransferID, phasedefinitionid, formsetversionid, phaseinstanceid }),

                TransferID = TransferID,
                ForceReload = ForceReload,
                PhaseDefinitionID = phasedefinitionid,
                FormTypeID = formtypeid,
                FormSetVersionID = formsetversionid,
                ValidationErrors = modelState.Errors,
                ShowErrors = !modelState.IsValid && ShowValidationErrors
            };

            if (toDashboard && (NextPhaseHandled || DefinitiveHandled) && string.IsNullOrEmpty(ErrorMessage) && modelState.IsValid)
            {
                saveresult.ActionName = "Dashboard";
                saveresult.ControllerName = "FlowMain";
                saveresult.RouteValues = new RouteValueDictionary(new { TransferID });
            }

            return saveresult;

        }

        public GlobalProperties GetGlobalPropertiesFormType(bool asPartial = false, bool getTransfer = false, bool getPhaseInstance = false, bool getNewFormRequestUrl = false, bool getFlowInstance = false, bool getFlowWebFields = true)
         {
            var globalProps = base.GetGlobalProperties(
                asPartial: asPartial, 
                getTransfer: getTransfer, 
                getPhaseInstance: getPhaseInstance, 
                getNewFormRequestUrl: getNewFormRequestUrl, 
                getFlowInstance: getFlowInstance);

            //Todo: Delete FlowInstance from PointController (pbi: 16654)
            //This overwrites the value gotten via getFlowInstance: true
            globalProps.FlowInstance = FlowInstance; 
            globalProps.HistoryFromDate = HistoryFromDate;

            if (getFlowWebFields)
            {
                globalProps.FlowWebFields = Get(FormSetVersionID);
            }

            return globalProps;
         }


    }
}