﻿using System.Web.Mvc;
using Point.MVC.WebApplication.Helpers;

namespace Point.MVC.WebApplication.Controllers
{
    public class PhaseMainController : PointController
    {
        public ActionResult Index()
        {
            return RedirectToAction("PhaseButtons");
        }

        public ActionResult PhaseButtons()
        {
            var viewModel = PhaseMainViewModelHelper.GetPhaseButtonsViewModel(this, GetGlobalProperties(getPhaseInstance: true));

            if (viewModel.HasErrors)
            {
                return Content(viewModel.ErrorMessage);
            }

            return PartialView("_PhaseButtons", viewModel);
        }
         

    }
}