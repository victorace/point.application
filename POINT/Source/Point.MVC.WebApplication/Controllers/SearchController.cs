﻿using System.Linq;
using System.Web.Mvc;
using Point.Business.Logic;
using Point.Database.Extensions;

namespace Point.MVC.WebApplication.Controllers
{
    public class SearchController : PointController
    {
        public JsonResult SearchOrganisation(string search, int organisationtype = 1)
        {
            var userinfo = PointUserInfo();
            var result = OrganizationSearchBL.GetOrganizationsByOrganizationTypeForSearch(uow, userinfo, organisationtype, search)
                .OrderBy(it => it.OrganizationName)
                .Select(it => new { label = it.OrganizationName, value = it.OrganizationName, id = it.OrganizationID });
            return new JsonResult() { Data = result };
        }

        public JsonResult SearchLocation(string search, int organisationid)
        {
            var result = LocationBL.GetLocationsForSearch(uow, search, organisationid)
                .Select(it => new { label = it.Name, value = it.Name, id = it.LocationID });
            return new JsonResult() { Data = result };
        }

        public JsonResult SearchDepartment(string search, int locationid)
        {
            var result = DepartmentBL.GetDepartmentsByLocationIDForSearch(uow, locationid, search)
                    .Select(it => new { label = it.Name, value = it.Name, id = it.DepartmentID });
            return new JsonResult() { Data = result };
        }

        public JsonResult SearchEmployeeByDepartmentID(int departmentid)
        {
            var result = EmployeeBL.GetByDepartmentID(uow, departmentid).ToList()
                .Select(it => new { label = it.FullName(), value = it.FullName(), id = it.EmployeeID });

            return new JsonResult() { Data = result };
        }

        public JsonResult SearchEmployeeByLocationID(int locationid)
        {
            var result = EmployeeBL.GetByLocationID(uow, locationid).ToList()
                .Select(it => new { label = it.FullName(), value = it.FullName(), id = it.EmployeeID });

            return new JsonResult() { Data = result };
        }

        public JsonResult SearchEmployeeByOrganizationID(int organizationid)
        {
            var result = EmployeeBL.GetByOrganizationID(uow, organizationid).ToList()
                .Select(it => new { label = it.FullName(), value = it.FullName(), id = it.EmployeeID });

            return new JsonResult() { Data = result };
        }

    }
}
