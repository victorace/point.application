﻿using Point.Business.Logic;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Infrastructure.Extensions;
using Point.MVC.WebApplication.Helpers;
using System.Web.Mvc;

namespace Point.MVC.WebApplication.Controllers
{
    public class ClientSearchController : PointBaseController
    {
        public ActionResult ClientSearch(string search = null)
        {
            var viewModel = new ClientSearchViewModel();
            if (!string.IsNullOrEmpty(search))
            {
                viewModel.Search = search;
            }
            return View(viewModel);                    
        }

        public PartialViewResult ClientSearchData(string search)
        {
            var pointuserinfo = PointUserInfo();

            var clientList = FlowInstanceSearchValuesBL.GetBySearchString(uow, pointuserinfo, search);

            if (clientList != null)
            {
                ViewBag.Count = clientList.Count;
            }
            else
            {
                ViewBag.Count = 0;
            }

            ViewBag.ShowAll = true;


            return PartialView(clientList);

        }
        public JsonResult GetClientByFlowInstance(int transferID)
        {
            var client = new Client();
            var contactPerson = new ContactPerson();

            client = ClientBL.GetByTransferID(uow, transferID);

            var contacts = ContactPersonBL.GetByClientID(uow, client.ClientID);
            contactPerson = contacts.FirstOrNew();

            return new JsonNetResult(new
            {
                HasClient = true,
                Client = client,
                HasContactPerson = contactPerson.ContactPersonID > 0,
                ContactPerson = contactPerson.PersonData,
                contactPerson.CIZRelationID
            });
        }
    }
}