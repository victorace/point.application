﻿using Point.Business.Logic;
using Point.Infrastructure.Attributes;
using System;
using System.Web.Mvc;

namespace Point.MVC.WebApplication.Controllers
{
    public class DigitalSignatureController : PointFormController
    {
        [ValidateAntiFiddleInjection("employeeid")]
        public FileResult GetByEmployeeID(int employeeid)
        {
            var employee = EmployeeBL.GetByID(uow, employeeid);
            if (employee.DigitalSignatureID.HasValue)
            {
                var digitalsignature = DigitalSignatureBL.GetByID(uow, employee.DigitalSignatureID.Value);
                return File(digitalsignature.FileData, digitalsignature.ContentType, digitalsignature.FileName);
            }
            return File("~/Images/nosignature.png", "image/png");
        }

        public ActionResult GetSignedPhaseInstanceImageAsBase64(int phaseinstanceid)
        {
            var signedphaseinstance = SignedPhaseInstanceBL.GetByPhaseInstanceID(uow, phaseinstanceid);
            if (signedphaseinstance != null)
            {
                var digitalsignature = signedphaseinstance.DigitalSignature;
                var base64 = Convert.ToBase64String(digitalsignature.FileData);
                return Content($"data:{digitalsignature.ContentType};base64,{base64}");
            }
            return Content("");
        }
    }
}