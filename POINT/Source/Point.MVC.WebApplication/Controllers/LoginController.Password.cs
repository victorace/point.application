﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Security;
using Point.Business.Helper;
using Point.Business.Logic;
using Point.Database.Models.ViewModels;
using System.Web;

namespace Point.MVC.WebApplication.Controllers
{

    [Authorize]
    public partial class LoginController : PointController
    {


       

        [AllowAnonymous]
        [HttpGet]
        public ActionResult ForgotPassword()
        {
            return View(new ForgotPasswordViewModel());
        }

        [AllowAnonymous]
        [HttpPost]
        public JsonResult RetrieveQuestion(string username = "", string emailaddress = "")
        {
            string question = "";
            bool isHospital = false;
            bool validUsername = false;
            
            if (!String.IsNullOrEmpty(username))
            {
                var employee = EmployeeBL.GetByUserName(uow, username);
                if (employee != null && employee.EmailAddress != null && employee.EmailAddress.ToLower() == emailaddress.ToLower())
                {
                    validUsername = true;
                    question = Membership.GetUser(username)?.PasswordQuestion ?? "";
                    isHospital = employee.DefaultDepartment?.Location?.Organization?.SSO == true ; // employee.DefaultDepartment?.Location?.Organization?.OrganizationTypeID == (int)OrganizationTypeID.Hospital;
                }                
            }
            return Json(new { Question = question, IsHospital = isHospital, ValidUserName = validUsername });
        }

        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ResetPassword(ForgotPasswordViewModel viewmodel)
        {
            var clientIP = Request.UserHostAddress;
            var success = false;
            var error = "";
            try
            {
                if (IsLockedTemporarily(clientIP))
                {
                    error = "U heeft te vaak een verkeerd antwoord gegeven. U moet tenminste 5 minuten wachten voordat u het nogmaals kunt proberen.";
                }
                else {
                    var user = Membership.GetUser(viewmodel.Username);
                    var employee = EmployeeBL.GetByUserName(uow, viewmodel.Username);
                    if (user == null || employee == null)
                    {
                        error = "Wachtwoord kon niet hersteld worden. Neem s.v.p contact op met een beheerder.";
                        AddFailedPasswordResetAttempt(clientIP);
                    }
                    //here must be for all none SSO organizations?
                    else if(employee.DefaultDepartment?.Location?.Organization?.SSO == true )
                    {
                        error = "Wachtwoord kon niet hersteld worden. Neem contact op met een beheerder om een nieuw wachtwoord aan te vragen.";
                        AddFailedPasswordResetAttempt(clientIP);
                    }
                    else
                    {
                        var provider = Membership.Providers["SqlMembershipProviderWithSecurityQuestion"];                        
                        var password = provider.ResetPassword(viewmodel.Username, viewmodel.SecretAnswer);
                        Point.Communication.Infrastructure.Helpers.ResetPassword.SendPasswordResetMail(viewmodel.EmailAddress, password, employee.LastName);
                        employee.ChangePasswordByEmployee = true;
                        EmployeeBL.Upsert(uow, employee);
                        uow.Save();
                        PointUserInfoHelper.ClearCache(employee.UserId.GetValueOrDefault());
                        success = true;
                    }
                }
            }
            catch(MembershipPasswordException)
            {
                error = "Het antwoord was niet correct of uw account is vergrendeld.";
                AddFailedPasswordResetAttempt(clientIP);
            }

            return Json(new { success = success, error = error });
        }


        private bool IsLockedTemporarily(string clientip)
        {
            Cleanup();
            var attempts = GetFailedPasswordResetAttemptsList();
            return attempts.IsLocked(clientip);
        }

        private void Cleanup()
        {
            var attempts = GetFailedPasswordResetAttemptsList();
            if (attempts.Count > 0)
            {
                foreach (var attempt in attempts)
                {
                    attempt.RemoveAll(a => a.LogDate < DateTime.Now.AddDays(-1));
                }

                attempts.RemoveAll(c => c.Count == 0);
            }
        }

        private void AddFailedPasswordResetAttempt(string clientip)
        {
            var attemptsList = GetFailedPasswordResetAttemptsList();
            attemptsList.Add(clientip);
        }

        private PasswordResetAttemptList GetFailedPasswordResetAttemptsList()
        {
            var app_state = System.Web.HttpContext.Current.Application as HttpApplicationState;
            var attemptslist = app_state.Get("ClientAttemptList") as PasswordResetAttemptList;
            if (attemptslist == null)
            {
                attemptslist = new PasswordResetAttemptList();
                app_state.Add("ClientAttemptList", attemptslist);
            }

            return attemptslist;
        }

        class PasswordResetAttemptList : List<SecurityQuestionAttemptList>
        {
            const int timeSpan = 1;
            const int max_attempts = 5;
            const int timeout = 5;

            public bool IsLocked(string clientip)
            {
                var clientAttempt = this.Find(a => a.ClientIP == clientip);
                if (clientAttempt == null)
                {
                    return false;
                }
                else if (clientAttempt.IsLocked && clientAttempt.LockDateTime < DateTime.Now.AddMinutes(0 - timeout))
                {
                    clientAttempt.IsLocked = false;
                    clientAttempt.Clear();
                }

                return clientAttempt.IsLocked;
            }

            public void Add(string clientip)
            {
                var clientattempt = this.Find(a => a.ClientIP == clientip);
                if (clientattempt == null)
                {
                    clientattempt = new SecurityQuestionAttemptList();
                    clientattempt.ClientIP = clientip;
                    this.Add(clientattempt);
                }

                clientattempt.Add();

                if (clientattempt.Count > max_attempts)
                {
                    clientattempt.IsLocked = true;
                    clientattempt.LockDateTime = DateTime.Now;
                }
            }
        }

        class SecurityQuestionAttemptList : List<SecurityQuestionAttempt>
        {
            public string ClientIP { get; set; }
            public bool IsLocked { get; set; }
            public DateTime LockDateTime { get; set; }

            public void Add()
            {
                this.Add(new SecurityQuestionAttempt());
            }
        }

        class SecurityQuestionAttempt
        {
            public SecurityQuestionAttempt()
            {
                this.LogDate = DateTime.Now;
            }

            public DateTime LogDate { get; set; }
        }
    }
}