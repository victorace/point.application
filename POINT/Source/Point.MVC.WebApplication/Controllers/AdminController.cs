using Point.Business.Logic;
using Point.Communication;
using Point.Database.Models.ViewModels;
using Point.Infrastructure.Caching;
using Point.Infrastructure.Extensions;
using Point.Infrastructure.Helpers;
using Point.MVC.WebApplication.Helpers;
using Point.ServiceBus.Models;
using System;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Runtime.Remoting.Channels;
using System.Web.Routing;
using Point.Infrastructure.Constants;
using Point.Processing.Handlers;
using Point.MVC.WebApplication.Attributes;
using Point.Models.Enums;

namespace Point.MVC.WebApplication.Controllers
{
    [FunctionRoles(FunctionRoleLevelID.Global, FunctionRoleTypeID.ManageAll)]
    public class AdminController : PointFormController
    {
        public ActionResult AutoSignaleringEmail()
        {
            ServiceBusBL.SendMessage(uow, "AutoSignalering", "AutoEmailSignalering", new AutoEmailSignalering());

            return Content("AutoEmailSignalering");
        }

        public string MoveLogoToDatabase()
        {
            var organizations = uow.OrganizationRepository.Get(org => !org.Inactive && org.Image && !string.IsNullOrEmpty(org.ImageName) && org.OrganizationLogoID == null)?.ToList();           
            var result = new List<string>();
            foreach (var org in organizations)
            {
                var fileToCopy = new Helpers.HttpFileCollectionBaseHelper();
                fileToCopy.MaxContentLength = 1 * 1024 * 1024;

                var extentions = new List<string>(new string[] { ".gif", ".jpe", ".jpeg", ".jpg", ".png", ".bmp" });
                var copied = fileToCopy.CopyImageToDatabase(this, org.ImageName, extentions);
                if (copied)
                {
                    var pointuserinfo = PointUserInfo();
                    var logentry = LogEntryBL.Create(Point.Models.Enums.FlowFormType.AdminRegion, pointuserinfo);
                    OrganizationLogoBL.Save(uow, org, fileToCopy.DatabaseImage, logentry);
                    result.Add(string.Format("Logo of {0} ({1}) moved to database successfully", org.Name, org.ImageName));
                }
                else
                {
                    result.Add(string.Format("Error in copying logo of {0} ({1}). Error:{2}", org.Name, org.ImageName, fileToCopy.Error));
                }
            }

            return (string.Join("<br />", result.ToArray()));
        }
        public string InvalidateCache()
        {
            CacheService.Instance.RemoveAll();

            return string.Concat("Done invalidating (", DateTime.Now.ToString(CultureInfo.InvariantCulture), ")");
        }

        public ActionResult ResendExpiredServiceBusItems()
        {
            var expireditems = ServiceBusLogBL.GetExpiredItems(uow).ToList();
            foreach (var item in expireditems)
            {
                var type = GetTypeByNameHelper.GetType(typeof(IPointServiceBusModel).Namespace + "." + item.ObjectType);
                var serializableobject = System.Web.Helpers.Json.Decode(item.ObjectData, type);
                ServiceBusBL.SendMessage(uow, item.Queue, item.Label, serializableobject, false);
            }
            return Json(new { FixedItems = expireditems.Count }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Recalculate()
        {
            return View();
        }

        public ActionResult Protect()
        {
            return View();
        }

        public string RecalculateAll()
        {
            var flowinstanceids = uow.FlowInstanceRepository.Get().Select(it => it.FlowInstanceID).ToArray();
            int i = 0;
            foreach(var batch in flowinstanceids.Batch(100))
            {
                FlowInstanceSearchValuesBL.CalculateReadValuesBatchAsync(uow, batch.ToArray());
                i++;
            }

            return "Updating " + i + " batches of (max) 100";
        }

        public string RecalculateAllFrequency()
        {
            var flowinstanceids = uow.FrequencyRepository.Get().SelectMany(f => f.Transfer.FlowInstances.Select(fli => fli.FlowInstanceID)).Distinct();
            int i = 0;
            foreach (var batch in flowinstanceids.Batch(100))
            {
                FlowInstanceSearchValuesBL.CalculateReadValuesBatchAsync(uow, batch.ToArray());
                i++;
            }

            return "Updating " + i + " batches of (max) 100";
        }

        [HttpPost]
        public ActionResult Recalculate(string transferids)
        {
            var transferidList = transferids.Replace("\r\n", ",").Replace(" ", ",").Split(',').Where(it => !string.IsNullOrEmpty(it)).ToArray();
            foreach (var item in transferidList)
            {
                int transferid = 0;
                if (int.TryParse(item, out transferid))
                {
                    FlowInstanceSearchValuesBL.CalculateReadValuesByTransferIDAsync(uow, transferid);
                }
            }

            return View();
        }

        [HttpPost]
        public ActionResult Protect(string value)
        {
            return Content(MachineKey.Protect(Encoding.UTF8.GetBytes(Convert.ToString(value)), "Encryption").ByteArrayToHexString());
        }

        public ActionResult OrganizationMigrate()
        {
            return View(new OrganizationMigrateViewModel{SourceOrTarget = OrganizationSourceOrTarget.Source});
        }

        [HttpPost]
        public JsonResult GetOrganizationsForNameOrID(string organizationNameOrID, string sourceOrTarget)
        {
            return Json(new
            {
                htmlReplaces = AdminHelper.GetOrganizationsForOrganizationNameOrID(this, uow, organizationNameOrID, sourceOrTarget)
            });
        }

        [HttpPost]
        public JsonResult GetLocationsForOrganizationID(int organizationID)
        {           
            return Json(new
            {
                htmlReplaces = AdminHelper.GetLocationsForOrganizationID(this, uow, organizationID)
            });
        }

        [AllowAnonymous]
        public ActionResult OrganizationMigrateResults(int[] locationIDs, int sourceOrganizationID, int targetOrganizationID)
        {
            var result = AdminHelper.OrganizationMigrateLocations(this, uow, locationIDs, sourceOrganizationID, targetOrganizationID);

            return PartialView(result);
        }

        [AllowAnonymous]
        public string UpdateOrganizationSearch()
        {
            OrganizationUpdateBL.CalculateAllAsync();
            return "Updating...";
        }

        public ActionResult UpdateCapManDaily(bool direct = false)
        {
            if (direct)
            {
                return Content(DepartmentBL.UpdateCapacityPerDay());
            }
            
            ServiceBusBL.SendMessage(ScheduledActions.QueueName, "UpdateCapmanDaily", new ScheduledActions.Schedule() { MethodName = "UpdateCapmanDaily", Name = "UpdateCapmanDaily" });

            return Content("Running ScheduledActions.UpdateCapmanDaily through servicebus.");
        }

        //public ActionResult UpdateCapManAll()
        //{
        //    var result = DepartmentBL.UpdateCapManAll(uow);
        //    return Content(result);
        //}


        [HttpGet]
        public ActionResult UpdateCapacity(UpdateCapacityViewModel model)
        {
            return View(model ?? new UpdateCapacityViewModel());
        }
        
        [HttpPost]
        public ActionResult UpdateCapacity(string startDateTime, bool isPost = false)
        {
            var responseMessage = string.Empty;
            DateTime? dateTime = null;
            var success = false;

            if (!string.IsNullOrEmpty(startDateTime))
            {
                if (DateTime.TryParseExact(startDateTime, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out var dt))
                {
                    dateTime = dt;
                    responseMessage = $"{TaskReturnValues.Failed}: Invalid StartDateTime";
                }
            }

            if (string.IsNullOrEmpty(responseMessage))
            {
                var result = DepartmentCapacityPublicBL.UpsertByDepartmentAll(uow, PointUserInfo().Employee.UserName, dateTime);
                success = result?.StartsWith(TaskReturnValues.Success) ?? false;
                responseMessage = result ?? "No results!";
            }
            
            var model = new UpdateCapacityViewModel { IsResponse = true, ResponseMessage = responseMessage, IsSuccessful = success };

            return RedirectToAction("UpdateCapacity", model);
        }


        public string CleanUp(int? transferid = null)
        {
            var total = PhaseInstanceBL.CleanupDoublePhases(uow, transferid);
            return string.Concat("Done cleaning up ", total, " double phaseinstances for all transfers", " (", DateTime.Now.ToString(CultureInfo.InvariantCulture), ")");
        }

        public string HL7UpdateDirectories()
        {
            var hl7servicedata = new HL7ServiceData();
            hl7servicedata.UpdateDirectories();

            return string.Concat("Done updating directories for HL7 (", DateTime.Now.ToString(CultureInfo.InvariantCulture), ")");
        }
    }

    public class UpdateCapacityViewModel
    {
        public bool IsSuccessful { get; set; }
        public bool IsResponse { get; set; }
        public string ResponseMessage { get; set; }
    }
}