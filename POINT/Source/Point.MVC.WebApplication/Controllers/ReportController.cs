﻿using System.Web.Mvc;
using Point.Models.Enums;
using Point.MVC.WebApplication.Attributes;

namespace Point.MVC.WebApplication.Controllers
{
    public class ReportController : PointController   
    {
        [FunctionRoles(FunctionRoleTypeID.Reporting)]
        public ActionResult Index()
        {
            return View();
        }
    }
}