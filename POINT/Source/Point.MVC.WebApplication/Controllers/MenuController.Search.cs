﻿using Point.Business.Helper;
using Point.Business.Logic;
using Point.Business.Navigation;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Infrastructure.Extensions;
using Point.Infrastructure.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Routing;
using Point.Infrastructure.Constants;
using Point.Database.Extensions;
using Point.Database.Models.Constants;

namespace Point.MVC.WebApplication.Controllers
{
    public partial class MenuController
    {
        private List<LocalMenuItem> buildParticipantsMenu()
        {
            var items = new List<LocalMenuItem>();

            var action = ControllerContext.ParentActionViewContext.RouteData.Values["action"].ToString();

            items.Add(new LocalMenuItem
            {
                Action = "Search",
                Controller = "Transfer",
                Url = Url.Action("Search", "Transfer"),
                Label = "Zoeken dossier",
                MenuType = MenuTypes.Main,
                Identifier = "Search",
                Description = "Zoeken dossier",
                OrderNumber = null,
                Subcategory = "",
                Disabled = false,
                Hidden = false,
                Glyphicon = "",
                CssClass = "pt-color pt-blue",
                OpenNewWindow = false
            });

            items.Add(participantsMenuItem(action.Equals("Participants",StringComparison.InvariantCultureIgnoreCase), MenuTypes.Main));

            addCapacity(MenuTypes.Main);

            return items;
        }

        private IList<LocalMenuItem> buildSearchMenu()
        {
            var pointUserInfo = PointUserInfo();

            menuItems = new List<LocalMenuItem>();

            if (!Enum.TryParse(Request.GetParam("status"), out DossierStatus status))
            {
                status = DossierStatus.Active;
            }

            if (!Enum.TryParse(Request.GetParam("type"), out DossierType type))
            {
                type = DossierType.All;
            }

            if (!Enum.TryParse(Request.GetParam("handling"), out DossierHandling handling))
            {
                handling = DossierHandling.All;
            }

            if (!Enum.TryParse(Request.GetParam("frequencyStatus"), out FrequencyDossierStatus frequencyStatus))
            {
                frequencyStatus = FrequencyDossierStatus.All;
            }

            var searchType = SearchType.Transfer;

            var action = ControllerContext.ParentActionViewContext.RouteData.Values["action"].ToString();

            var addNewDossierIsCurrent = false;
            if (action.Equals("NewFlowInstance", StringComparison.InvariantCultureIgnoreCase))
            {
                addNewDossierIsCurrent = true;
            }
            else if (action.Equals("Search", StringComparison.InvariantCultureIgnoreCase))
            {
                if (!Enum.TryParse(Request.GetParam("searchType"), out searchType))
                {
                    searchType = SearchType.Transfer;
                }
            }

            var scopeType = ScopeTypeHelper.From(status, frequencyStatus, handling, type, searchType);

            var flowdefinitionsSender = FlowDefinitionBL.GetSendingFlowDefinitionsByLocationIDDepartmentID(uow, pointUserInfo.Location.LocationID, pointUserInfo.Department.DepartmentID);
            if (flowdefinitionsSender != null && flowdefinitionsSender.Any())
            {
                menuItems.Add(newDossierMenuItem(addNewDossierIsCurrent));
            }

            if (pointUserInfo.Organization.HL7ConnectionCallFromPoint == true && !ExternalPatientHelper.HavePatExternalReference())
            {
                menuItems.Add(newDossierZISMenuItem());
            }

            menuItems.Add(new LocalMenuItem
            {
                Glyphicon = "glyphicon glyphicon-print",
                Controller = "PrintAction",
                Description = "Afdrukken",
                Disabled = false,
                Identifier = "PrintSearch",
                IsCurrent = action == "PrintSearch",
                Label = "Afdrukken",
                MenuType = MenuTypes.Actie,
                Url = "#",
                OnClick = "PrintSearchResults();return false;"
            });

            menuItems.AddRange(GetManagementMenuItems());

            addReports(action, pointUserInfo);

            if (!ExternalPatientHelper.GetIsAuth()) 
            {
                menuItems.Add(searchAllMenuItem(scopeType, searchType, addNewDossierIsCurrent));
            }

            if (searchType == SearchType.Transfer)
            {
                var organizationtypeid = (OrganizationTypeID)pointUserInfo.Organization.OrganizationTypeID;

                if (pointUserInfo.DepartmentTypeIDS.Contains((int)DepartmentTypeID.Transferpunt) || (organizationtypeid != OrganizationTypeID.Hospital))
                {
                    menuItems.Add(new LocalMenuItem
                    {
                        Glyphicon = "glyphicon glyphicon-inbox",
                        Controller = "Transfer",
                        Description = "Dossiers die nog niet in behandeling zijn genomen",
                        Disabled = false,
                        Identifier = "Dossier-1",
                        IsCurrent = scopeType == ScopeType.NotProcessing && !addNewDossierIsCurrent,
                        Label = "Niet in behandeling",
                        MenuType = MenuTypes.Filter,
                        Url = Url.Action("Search", "Transfer", new { handling = DossierHandling.NotHandling })
                    });

                    menuItems.Add(new LocalMenuItem
                    {
                        Glyphicon = "glyphicon glyphicon-transfer",
                        Controller = "Transfer",
                        Description = "Dossiers door mij in behandeling genomen",
                        Disabled = false,
                        Identifier = "Dossier-1",
                        IsCurrent = scopeType == ScopeType.HandledByMe && !addNewDossierIsCurrent,
                        Label = "In beh. door mij",
                        MenuType = MenuTypes.Filter,
                        Url = Url.Action("Search", "Transfer", new { handling = DossierHandling.HandledByMe })
                    });

                    menuItems.Add(new LocalMenuItem
                    {
                        Glyphicon = "glyphicon glyphicon-transfer",
                        Controller = "Transfer",
                        Description = "Dossiers die in behandeling zijn genomen",
                        Disabled = false,
                        Identifier = "Dossier-1",
                        IsCurrent = scopeType == ScopeType.Handled && !addNewDossierIsCurrent,
                        Label = "In behandeling",
                        MenuType = MenuTypes.Filter,
                        Url = Url.Action("Search", "Transfer", new { handling = DossierHandling.Handling })
                    });
                }

                if(!pointUserInfo.DepartmentTypeIDS.Contains((int)DepartmentTypeID.Transferpunt) || (organizationtypeid != OrganizationTypeID.Hospital))
                {
                    menuItems.Add(new LocalMenuItem
                    {
                        Glyphicon = "glyphicon glyphicon-user",
                        Controller = "Transfer",
                        Description = "Mijn dossiers",
                        Disabled = false,
                        Identifier = "Dossier-1",
                        IsCurrent = scopeType == ScopeType.OwnedByMe && !addNewDossierIsCurrent,
                        Label = "Mijn dossiers",
                        MenuType = MenuTypes.Filter,
                        Url = Url.Action("Search", "Transfer", new { handling = DossierHandling.OwnedByMe })
                    });

                    menuItems.Add(new LocalMenuItem
                    {
                        Glyphicon = "glyphicon glyphicon-transfer",
                        Controller = "Transfer",
                        Description = "Dossiers van mijn afdeling",
                        Disabled = false,
                        Identifier = "Dossier-1",
                        IsCurrent = scopeType == ScopeType.OwnedByMyDepartment && !addNewDossierIsCurrent,
                        Label = "Afdeling dossiers",
                        MenuType = MenuTypes.Filter,
                        Url = Url.Action("Search", "Transfer", new { handling = DossierHandling.OwnedByMyDepartment })
                    });
                }

                menuItems.Add(new LocalMenuItem
                {
                    Action = "Search",
                    Controller = "Transfer",
                    Description = "Onderbroken",
                    Disabled = false,
                    Identifier = "Dossier-1",
                    IsCurrent = scopeType == ScopeType.Interrupted,
                    Label = "Onderbroken",
                    MenuType = MenuTypes.Filter,
                    CssClass = "pt-color pt-gray",
                    Url = Url.Action("Search", "Transfer", new {status = DossierStatus.Interrupted})
                });

                menuItems.Add(new LocalMenuItem
                {
                    Action = "Search",
                    Controller = "Transfer",
                    Description = "VVT akkoord",
                    Disabled = false,
                    Identifier = "Dossier-1",
                    IsCurrent = scopeType == ScopeType.Accepted,
                    Label = "VVT akkoord",
                    MenuType = MenuTypes.Filter,
                    CssClass = "pt-color pt-green",
                    Url = Url.Action("Search", "Transfer", new {status = DossierStatus.CanBeTransferred})
                });

                menuItems.Add(new LocalMenuItem
                {
                    Action = "Search",
                    Controller = "Transfer",
                    Description = "VVT akkoord",
                    Disabled = false,
                    Identifier = "Dossier-1",
                    IsCurrent = scopeType == ScopeType.PartiallyAccepted,
                    Label = "VVT semi-akkoord",
                    MenuType = MenuTypes.Filter,
                    CssClass = "pt-color pt-orange",
                    Url = Url.Action("Search", "Transfer", new { status = DossierStatus.CanPerhapsBeTransferred })
                });

                menuItems.Add(new LocalMenuItem
                {
                    Action = "Search",
                    Controller = "Transfer",
                    Description = "VVT niet akkoord",
                    Disabled = false,
                    Identifier = "Dossier-1",
                    IsCurrent = scopeType == ScopeType.NotAccepted,
                    Label = "VVT niet akkoord",
                    MenuType = MenuTypes.Filter,
                    CssClass = "pt-color pt-red",
                    Url = Url.Action("Search", "Transfer", new {status = DossierStatus.CannotBeTransferred})
                });

                menuItems.Add(new LocalMenuItem
                {
                    Action = "Search",
                    Controller = "Transfer",
                    Description = "Definitief / af te sluiten",
                    Disabled = false,
                    Identifier = "Dossier-1",
                    IsCurrent = scopeType == ScopeType.Definitive,
                    Label = "Af te sluiten",
                    MenuType = MenuTypes.Filter,
                    CssClass = "pt-color pt-yellow",
                    Url = Url.Action("Search", "Transfer", new {status = DossierStatus.CanBeClosed})
                });
            }

            menuItems.Add(oudDossierMenuItem());

            menuItems.Add(doorlopenDossierMenuItem(searchType));

            menuItems.Add(participantsMenuItem(false));

            addCapacity(MenuTypes.Zoeken);

            return menuItems;
        }

        private LocalMenuItem searchAllMenuItem(ScopeType scopeType, SearchType searchType, bool addNewDossierIsCurrent)
        {
            return new LocalMenuItem
            {
                Glyphicon = "glyphicon glyphicon-duplicate",
                Controller = "Transfer",
                Description = "Alle dossiers",
                Disabled = false,
                Identifier = "Dossier-1",
                IsCurrent = scopeType == ScopeType.AllTransfers && !addNewDossierIsCurrent && searchType == SearchType.Transfer,
                Label = "Alle dossiers",
                MenuType = MenuTypes.Filter,
                Url = Url.Action("Search", "Transfer", new { status = DossierStatus.Active, Reset = searchType == SearchType.Frequency })
            };
        }

        private LocalMenuItem backToSearchMenuItem(ScopeType scopeType, SearchType searchType, bool addNewDossierIsCurrent)
        {
            var menuitem = searchAllMenuItem(scopeType, searchType, addNewDossierIsCurrent);
            menuitem.Label = "Transfer doss.";
            menuitem.Description = "Transfer dossiers";
            menuitem.CssClass = "pt-color pt-blue";
            menuitem.MenuType = MenuTypes.Zoeken;
            menuitem.Glyphicon = null;

            return menuitem;
        }

        private LocalMenuItem doorlopenDossierMenuItem(SearchType searchType)
        {
            return new LocalMenuItem
            {
                CssClass = "pt-color pt-blue",
                Controller = "Transfer",
                Description = "Zoek naar doorlopende dossiers",
                Disabled = false,
                Identifier = "Dossier-1",
                IsCurrent = searchType == SearchType.Frequency,
                Label = "Doorlopend doss.",
                MenuType = MenuTypes.Zoeken,
                Url = Url.Action("Search", "Transfer", new { SearchType = SearchType.Frequency })
            };
        }

        private IList<LocalMenuItem> mainMenuItems()
        {
            var items = new List<LocalMenuItem>();
            var action = ControllerContext.ParentActionViewContext.RouteData.Values["action"].ToString();

            var url = Url.Action("Search", "Transfer");
            var searchcontrol = SearchControlBL.GetState(this.HttpContext);
            if(searchcontrol != null)
            {
                url = Url.Action("Search", "Transfer", new {searchcontrol.handling, searchcontrol.status, searchcontrol.SearchType});
            }

            items.Add(new LocalMenuItem()
            {
                Action = "Search",
                Controller = "Transfer",
                Url = url,
                Label = "Zoeken dossier",
                MenuType = MenuTypes.Main,
                Identifier = "Search",
                Description = "Zoeken dossier",
                OrderNumber = null,
                Subcategory = "",
                Disabled = false,
                Hidden = false,
                IsCurrent = action == "Search",
                Glyphicon = "",
                CssClass = "pt-color pt-blue",
                OpenNewWindow = false
            });

            if (TransferID > 0)
            {
                items.Add(new LocalMenuItem()
                {
                    Action = "Dashboard",
                    Controller = "FlowMain",
                    Url = Url.Action("Dashboard", "FlowMain", new {TransferID}),
                    Label = "Dashboard",
                    MenuType = MenuTypes.Main,
                    Identifier = "Dashboard",
                    Description = "Dashboard",
                    OrderNumber = null,
                    Subcategory = "",
                    Disabled = false,
                    Hidden = false,
                    IsCurrent = action == "Dashboard",
                    Glyphicon = "",
                    CssClass = "pt-color pt-blue",
                    OpenNewWindow = false
                });
            }

            return items;
        }

        private IList<LocalMenuItem> buildExtReferenceMenu()
        {
            menuItems = new List<LocalMenuItem>();

            var action = ControllerContext.ParentActionViewContext.RouteData.Values["action"].ToString();

            if (!Enum.TryParse(Request.GetParam("status"), out DossierStatus status))
            {
                status = DossierStatus.Active;
            }

            if (!Enum.TryParse(Request.GetParam("type"), out DossierType type))
            {
                type = DossierType.All;
            }

            if (!Enum.TryParse(Request.GetParam("handling"), out DossierHandling handling))
            {
                handling = DossierHandling.All;
            }

            if (!Enum.TryParse(Request.GetParam("frequencyStatus"), out FrequencyDossierStatus frequencyStatus))
            {
                frequencyStatus = FrequencyDossierStatus.All;
            }

            var searchType = SearchType.Transfer;
            if (action.Equals("Search", StringComparison.InvariantCultureIgnoreCase))
            {
                if (!Enum.TryParse(Request.GetParam("searchType"), out searchType))
                {
                    searchType = SearchType.Transfer;
                }
            }

            var pointuserinfo = PointUserInfo();

            bool addNewIsCurrent = action.Equals("NewFlowInstance", StringComparison.InvariantCultureIgnoreCase);
            var flowdefinitionsSender = FlowDefinitionBL.GetSendingFlowDefinitionsByLocationIDDepartmentID(uow, pointuserinfo.Location.LocationID, pointuserinfo.Department.DepartmentID);
            if (flowdefinitionsSender != null && flowdefinitionsSender.Any())
            {
                menuItems.Add(newDossierMenuItem(addNewIsCurrent));
            }

            var hidelosefocus = pointuserinfo.Organization.HL7ConnectionDifferentMenu;
            if (!hidelosefocus)
            {
                menuItems.Add(new LocalMenuItem
                {
                    Action = "LoseFocus",
                    Controller = "Transfer",
                    Description = "Verlaat 'patientfocus' en bekijk het volledige zoekscherm",
                    Url = Url.Action("LoseFocus", "Transfer"),
                    MenuType = MenuTypes.ExtReference,
                    Label = "Alle dossiers",
                    Glyphicon = "glyphicon glyphicon-duplicate"
                });
            }
            
            var scopeType = ScopeTypeHelper.From(status, frequencyStatus, handling, type, searchType);
            menuItems.Add(backToSearchMenuItem(scopeType, searchType, addNewIsCurrent));
            menuItems.Add(doorlopenDossierMenuItem(searchType));

            if (OrganizationSettingBL.GetValue<bool>(uow, pointuserinfo.Organization.OrganizationID, OrganizationSettingBL.PatientOverview))
            {
                menuItems.Add(new LocalMenuItem
                {
                    Description = "Bekijk de voortgang van de dossiers van andere patiënten op de afdeling van de huidige patiënt",
                    Url = "#",
                    OnClick = $"ShowPatientOverview('{Url.Action("SearchResultPatientOverview", "Transfer")}');",
                    MenuType = MenuTypes.ExtReference,
                    Label = "Voortgang patiënten",
                    Glyphicon = "glyphicon glyphicon-zoom-in"
                });
            }

            menuItems.Add(participantsMenuItem(false));

            addCapacity(MenuTypes.Zoeken);

            addReports(action, pointuserinfo);

            var patExternalReferenceBSN = ExternalPatientHelper.GetPatExternalReferenceBSN();
            if (!String.IsNullOrEmpty(patExternalReferenceBSN))
            {
                return menuItems; //Geen oud dossier zoeken op basis BSN (bestaan geen filters in)
            }
            else
            {
                var patExternalReference = ExternalPatientHelper.GetPatExternalReference();
                var existingdossiercheckinfo = ExistingDossierCheckInfo.CreateForUser(uow, pointuserinfo, patExternalReference, patExternalReferenceBSN);
                if (existingdossiercheckinfo.DossiersExistAtOtherDepartments.Any() || existingdossiercheckinfo.UsersDepartmentExistingDossierCount > 0)
                {
                    menuItems.Add(oudDossierMenuItem(patExternalReference));
                }

                return menuItems;
            }

        }

        private LocalMenuItem oudDossierMenuItem(string patExternalReference = "")
        {
            var domainPointOud = ConfigHelper.GetAppSettingByName<string>("DomainPointOud");
            var routevalues = new RouteValueDictionary { { SessionIdentifiers.Point.ForceOld, 1 }, { "SessID", Session.SessionID } };

            if (!string.IsNullOrWhiteSpace(patExternalReference))
            {
                routevalues.Add("patexternalreference1", patExternalReference);
            }

            return new LocalMenuItem
            {
                CssClass = "pt-color pt-blue",
                Controller = "Transfer",
                Action = "Transfers.aspx",
                Description = "Zoek naar dossier binnen de voorgaande versie van POINT",
                Disabled = false,
                Identifier = "Dossier-1",
                IsCurrent = false,
                Label = "OUD dossier",
                MenuType = MenuTypes.Zoeken,
                Url = Url.Action("Transfers.aspx", "Transfer", routevalues, "https", domainPointOud)
            };
        }
        private LocalMenuItem newDossierMenuItem(bool isCurrent)
        {
            return new LocalMenuItem
            {
                Glyphicon = "glyphicon glyphicon-plus",
                CssClass = "",
                Action = "NewFlowInstance",
                Controller = "FlowMain",
                Description = "Nieuw dossier",
                Disabled = false,
                Identifier = "Dossier-1",
                IsCurrent = isCurrent,
                Label = "Nieuw dossier",
                MenuType = MenuTypes.Actie,
                Url = "#",
                OnClick = $"return NewFlowInstancePreCheck('{Url.Action("NewFlowInstance", "FlowMain")}', '{Url.Action("CheckForExistingDossiers", "FlowMain")}');"
            };
        }

        private LocalMenuItem newDossierZISMenuItem()
        {
            return new LocalMenuItem
            {
                Glyphicon = "glyphicon glyphicon-plus",
                CssClass = "",
                Action = "ImportZIS",
                Controller = "Client",
                Description = "Maak nieuw transferdossier via automatische koppeling met ZIS",
                Disabled = false,
                Identifier = "Dossier-1",
                IsCurrent = false,
                Label = "Nieuw dossier vanuit ZIS",
                MenuType = MenuTypes.Actie,
                Url = "#",
                OnClick = $"ImportZIS(this, '{Url.Action("ImportZIS", "Client")}', null);"
            };

        }

        private LocalMenuItem participantsMenuItem(bool isCurrent, string menuType = MenuTypes.Zoeken)
        {
            return new LocalMenuItem
            {
                CssClass = "pt-color pt-blue",
                Controller = "Organization",
                Action = "Participants",
                Description = "Alle organisaties die bekend zijn in POINT.",
                Disabled = false,
                Identifier = "Dossier-1",
                IsCurrent = isCurrent,
                Label = "Deelnemende inst.",
                MenuType = menuType,
                Url = Url.Action("Participants", "Organization")
            };
        }

        private void addReports(string currentAction, PointUserInfo pointUserInfo)
        {
            if (pointUserInfo.GetReportFunctionRoleLevelID() > FunctionRoleLevelID.None)
            {
                var url = Url.Action("Index", "Report");

                menuItems.Add(new LocalMenuItem
                {
                    Action = "Index",
                    Controller = "Report",
                    Url = url,
                    Label = "Rapportage",
                    MenuType = MenuTypes.Actie,
                    Identifier = "Rapportage",
                    Description = "Rapportage",
                    OrderNumber = null,
                    Subcategory = "",
                    Disabled = false,
                    Hidden = false,
                    IsCurrent = currentAction.Equals("Rapportage", StringComparison.InvariantCultureIgnoreCase),
                    Glyphicon = "glyphicon glyphicon-list-alt",
                    CssClass = "",
                    OpenNewWindow = false
                });
            }
        }

        private void addCapacity(string menuType)
        {
            if (PointUserInfo().IsRegioAdmin || PointUserInfo().Region.CapacityBeds ||
                PointUserInfo().Organization.IsHealthCareProvider() || PointUserInfo().Organization.IsGeneralPracticioner() )
            {
                menuItems.Add(new LocalMenuItem()
                {
                    CssClass = "pt-color pt-blue",
                    Controller = "Organization",
                    Action = "~/Management/Capacity/Tile",
                    Description = "Capaciteit",
                    Disabled = false,
                    Identifier = "Dossier-1",
                    IsCurrent = false,
                    Label = "Capaciteit",
                    MenuType = menuType,
                    Url = Url.Content("~/Management/Capacity/Tile")
                });

            }
        }
    }
}