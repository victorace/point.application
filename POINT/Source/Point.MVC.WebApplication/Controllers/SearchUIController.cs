﻿using Point.Business.Helper;
using Point.Business.Logic;
using Point.Database.Context;
using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Caching;
using Point.Infrastructure.Extensions;
using Point.MVC.WebApplication.Attributes;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;

namespace Point.MVC.WebApplication.Controllers
{
    [FunctionRoles("Deze functionaliteit is enkel voor (Organisatie)beheerders", FunctionRoleLevelID.Organization, FunctionRoleTypeID.ManageSearch, FunctionRoleTypeID.ManageAll)]
    public class SearchUIController : PointFormController
    {
        // GET: SearchUI
        public ActionResult Index()
        {
            var pointuserinfo = PointUserInfo();
            var accesslevel = FunctionRoleHelper.GetAccessLevel(uow, FunctionRoleTypeID.ManageSearch, pointuserinfo);
            var organizations = OrganizationBL.GetByOrganizationIDs(uow, accesslevel.OrganizationIDs).ToList();

            ViewBag.ThisOrganization = pointuserinfo.Organization;
            ViewBag.AllowEditDefault = pointuserinfo.IsGlobalAdmin;
            ViewBag.FlowDefinitions = FlowDefinitionBL.GetActiveFlowDefinitions(uow).OrderBy(it => it.Name).ToList();

            // default to SearchType.Transfer
            ViewBag.SearchTypes = Enum.GetValues(typeof(SearchType)).Cast<SearchType>()
                .Where(e => Convert.ToInt32(e) > 0)
                .Select(e => new Option
                {
                    Value = Convert.ToInt32(e).ToString(),
                    Text = e.GetDescription(),
                    Selected = (SearchType) e == SearchType.Transfer
                }).ToList();

            return View(organizations);
        }

        public void CopyConfig(UnitOfWork<PointContext> uowp, SearchUIConfiguration sourceconfig, int organizationid,
            SearchType searchtype)
        {
            var targetconfig = SearchUIConfigurationBL.GetConfiguration(uowp, sourceconfig.FlowDefinitionID,
                organizationid, searchtype);
            var generatenew = targetconfig == null;

            if (generatenew)
            {
                targetconfig = new SearchUIConfiguration();
                targetconfig.Merge(sourceconfig);
                targetconfig.SearchUIConfigurationID = 0;
                targetconfig.Organization = null;
                targetconfig.OrganizationID = organizationid;
                targetconfig.DepartmentID = null;
                targetconfig.Fields = new HashSet<SearchUIFieldConfiguration>();

                uowp.DbContext.SearchUIConfigurations.Add(targetconfig);
            }
            else
            {
                uowp.DbContext.Entry(targetconfig).State = EntityState.Modified;

                //Delete old fields
                foreach (var field in targetconfig.Fields.ToList())
                {
                    targetconfig.Fields.Remove(field);
                    uowp.DbContext.Entry(field).State = EntityState.Deleted;
                }
            }

            foreach (var item in sourceconfig.Fields)
            {
                var newfield = new SearchUIFieldConfiguration();
                newfield.Merge(item);
                newfield.SearchUIFieldConfigurationID = 0;
                newfield.SearchUIConfiguration = targetconfig;
                newfield.SearchUIConfigurationID = targetconfig.SearchUIConfigurationID;

                targetconfig.Fields.Add(newfield);

                uowp.DbContext.Entry(newfield).State = EntityState.Added;
            }
        }

        public SearchUIConfiguration SaveSearchUIConfig(UnitOfWork<PointContext> uowp, FlowDefinitionID flowdefinitionid,
            SearchType searchtype,
            int? organizationid, int? departmentid, FormCollection formcollection)
        {
            var currentconfig = SearchUIConfigurationBL.GetConfigurationOrDefault(uowp, flowdefinitionid, searchtype, organizationid, departmentid, false);
            var generatenew = currentconfig.OrganizationID != organizationid ||
                              currentconfig.DepartmentID != departmentid;

            // we may have missing fields. get hard coded default and add missing fields to our custom configuration
            var defaultconfiguration = SearchUIConfigurationBL.GetCodeDefault(flowdefinitionid, searchtype);
            var missingconfigurationitems = defaultconfiguration.Fields
                .Where(dcf => !currentconfig.Fields.Any(ccf => ccf.Field.Name == dcf.Field.Name)).ToList();
            currentconfig.Fields.AddRange(missingconfigurationitems);

            var newconfig = currentconfig;
            if (generatenew)
            {
                newconfig = new SearchUIConfiguration();
                newconfig.Merge(currentconfig);
                newconfig.SearchUIConfigurationID = 0;
                newconfig.FlowDefinitionID = flowdefinitionid;
                newconfig.OrganizationID = organizationid;
                newconfig.DepartmentID = departmentid;
                newconfig.Fields = new HashSet<SearchUIFieldConfiguration>();

                uowp.DbContext.SearchUIConfigurations.Add(newconfig);
                uowp.DbContext.Entry(newconfig).State = EntityState.Added;
            }
            else
            {
                uowp.DbContext.Entry(newconfig).State = EntityState.Modified;
            }

            newconfig.SortOrder = new SortOrderInfo();

            foreach (var field in currentconfig.Fields.ToList())
            {
                var newindex = formcollection.Get(field.SearchUIFieldConfigurationID.ToString());
                var newisvisible = formcollection.Get(field.SearchUIFieldConfigurationID + "_isvisible");
                var newsortorder = formcollection.Get(field.SearchUIFieldConfigurationID + "_sortorder");
                var newcssclass = formcollection.Get(field.SearchUIFieldConfigurationID + "_cssclass");

                var newfield = field;
                if (generatenew)
                {
                    newfield = new SearchUIFieldConfiguration();
                    newfield.Merge(field);
                    newfield.SearchUIFieldConfigurationID = 0;
                    newfield.SearchUIConfiguration = newconfig;
                    newfield.SearchUIConfigurationID = 0;
                    uowp.DbContext.Entry(newfield).State = EntityState.Added;

                    newconfig.Fields.Add(newfield);
                }
                else if (field.SearchUIFieldConfigurationID == 0)
                {
                    newfield = new SearchUIFieldConfiguration();
                    newfield.Merge(field);
                    newfield.SearchUIFieldConfigurationID = 0;
                    newfield.SearchUIConfiguration = currentconfig;
                    newfield.SearchUIConfigurationID = currentconfig.SearchUIConfigurationID;
                    uowp.DbContext.Entry(newfield).State = EntityState.Added;

                    newconfig.Fields.Add(newfield);
                }
                else
                {
                    uowp.DbContext.Entry(newfield).State = EntityState.Modified;
                }

                //Sortering index
                int newindexInt;
                if (int.TryParse(newindex, out newindexInt))
                    newfield.Column.Index = newindexInt;

                //Visible
                bool newisvisibleBool;
                if (bool.TryParse(newisvisible, out newisvisibleBool))
                    newfield.Column.IsVisible = newisvisibleBool;
                else if (newisvisible == null)
                    newfield.Column.IsVisible = false;

                //Sortering column
                if (!string.IsNullOrEmpty(newsortorder))
                {
                    newconfig.SortOrder.Field = newfield.Field;
                    newconfig.SortOrder.OrderByDescending = newsortorder == "desc";
                }

                //Breedte
                newfield.Column.CssClass = newcssclass;
            }

            return newconfig;
        }

        public ActionResult Edit(FlowDefinitionID flowdefinitionid, SearchType searchtype, int? organizationid, int? departmentid)
        {
            var pointuserinfo = PointUserInfo();

            if(organizationid == null && !pointuserinfo.IsGlobalAdmin)
            {
                return RedirectToAction("Index");
            }

            var department = DepartmentBL.GetByDepartmentID(uow, departmentid.GetValueOrDefault(0));

            ViewBag.Organization = OrganizationBL.GetByOrganizationID(uow, organizationid.GetValueOrDefault(0));
            ViewBag.DepartmentID = departmentid;
            ViewBag.DepartmentName = department != null ? department.Name : "Alle";
            ViewBag.AllowEditRegio = pointuserinfo.IsRegioAdmin;
            ViewBag.SearchType = searchtype;

            var currentconfig = SearchUIConfigurationBL.GetConfigurationOrDefault(uow, flowdefinitionid, searchtype, organizationid, departmentid, false);

            var isCustom = currentconfig.OrganizationID == organizationid && currentconfig.DepartmentID == departmentid &&
                           currentconfig.OrganizationID.HasValue;

            // we may have missing fields. get hard coded default and add missing fields to our custom configuration
            var defaultconfiguration = SearchUIConfigurationBL.GetCodeDefault(flowdefinitionid, searchtype);
            var missingconfigurationitems = defaultconfiguration.Fields
                .Where(dcf => !currentconfig.Fields.Any(ccf => ccf.Field.Name == dcf.Field.Name)).ToList();
            currentconfig.Fields.AddRange(missingconfigurationitems);

            ViewBag.IsCustom = isCustom;
            ViewBag.SortOrder = currentconfig.SortOrder;

            return View(currentconfig);
        }

        [HttpPost]
        public ActionResult Edit(FormCollection formcollection, FlowDefinitionID flowdefinitionid, SearchType searchtype,
            int? organizationid, int? departmentid)
        {
            uow.DbContext.Configuration.AutoDetectChangesEnabled = false;
            if (formcollection.Get("actionname") == "Terug")
            {
                return Redirect(Url.Action("Index"));
            }

            if (formcollection.Get("actionname") == "Reset default")
            {
                SearchUIConfigurationBL.DeleteDefaultByFlowDefinitionID(uow, flowdefinitionid);
                CacheService.Instance.RemoveAll("SearchUIConfiguration");
                return Redirect(Url.Action("Index"));
            }

            if (formcollection.Get("actionname") == "Verwijder aanpassing")
            {
                var currentconfig = SearchUIConfigurationBL.GetConfigurationOrDefault(uow, flowdefinitionid, searchtype, organizationid, departmentid, false);

                foreach (var field in currentconfig.Fields.ToList())
                    uow.DbContext.Entry(field).State = EntityState.Deleted;

                uow.DbContext.Entry(currentconfig).State = EntityState.Deleted;
                uow.DbContext.SaveChanges();

                CacheService.Instance.RemoveAll("SearchUIConfiguration");

                return Redirect(Url.Action("Index"));
            }

            var newconfig = SaveSearchUIConfig(uow, flowdefinitionid, searchtype, organizationid, departmentid,
                formcollection);
            uow.DbContext.SaveChanges();

            var saveto = formcollection["saveto"];
            if (saveto != null && saveto == "region")
            {
                if (organizationid != null)
                {
                    var organization = OrganizationBL.GetByOrganizationID(uow, organizationid.Value);
                    var organizations =
                        uow.OrganizationRepository.Get(
                                org =>
                                org.OrganizationTypeID == organization.OrganizationTypeID &&
                                org.RegionID == organization.RegionID
                            );

                    foreach (var orgid in organizations.Select(org => org.OrganizationID).Where(it => organizationid != it))
                    {
                        CopyConfig(uow, newconfig, orgid, searchtype);
                    }
                }

                uow.DbContext.SaveChanges();
            }

            CacheService.Instance.RemoveAll("SearchUIConfiguration");

            return Redirect(Url.Action("Index"));
        }
    }
}