﻿using Point.Business.BusinessObjects;
using Point.Business.Helper;
using Point.Business.Logic;
using Point.Communication;
using Point.Database.Extensions;
using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Infrastructure.Attributes;
using Point.Infrastructure.Extensions;
using System;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Point.MVC.WebApplication.Helpers;

namespace Point.MVC.WebApplication.Controllers
{
    public class ClientController : PointFormController
    {
        public ActionResult Header()
        {
            var viewModel = ClientViewModelHelper.GetHeaderViewModel(this, GetGlobalProperties());
            return PartialView(IsMobileDevice() ? "_Client_Header_Mobile" : "_Client_Header", viewModel);
        }

        [HttpPost]
        public JsonResult BSNIsRequired(bool clientHasNoBSN)
        {
            var isBSNRequired = !clientHasNoBSN;
            ViewBag.BSNRequired = isBSNRequired;             // TODO: Is this still needed?
            return new JsonNetResult(new
            {
                BSNRequired = isBSNRequired
            });
        }

        public JsonResult HasClientOtherOpenDossier(int transferID, string bsn, FlowDefinitionID dossierFlowDefinition)
        {
            var noresults = new JsonNetResult(new { HasOtherOpenDossier = "false", URL="" }, JsonRequestBehavior.AllowGet);

            if (transferID > 0 || string.IsNullOrEmpty(bsn))
            {
                return noresults;
            }
                
            if (ClientBL.HasClientOtherOpenTrnsfers(uow, bsn, transferID,PointUserInfo()))
            {               
                var url = "/Client/ClientOpenTransfers?BSN=" + bsn + "&TransferID=" + transferID;
                return new JsonNetResult(new { HasOtherOpenDossier = "true", URL= url }, JsonRequestBehavior.AllowGet);
            }

            return noresults;

        }

        public ActionResult ClientOpenTransfers(string bsn, int? transferid)
        {
            //This function loads previouse related transfer in a view.
            //Get all previouse transfers of this pateint. no matter which flow.
            var noresults = new JsonResult { Data = new { no_results = true } };

            if (String.IsNullOrEmpty(bsn) || !transferid.HasValue)
                
            {
                return noresults;
            }

            var pointuserinfo = PointUserInfo();
            var opentransfers = ClientBL.GetClientOtherOpenTrnsfers(uow, bsn, transferid.Value, pointuserinfo);            
            if (!opentransfers.Any())
            {
                return noresults;
            }

            var model = opentransfers.Select(s => ClientOpenTransferViewModel.ToViewModel(s));

            return PartialView(model);
        }


        public SecureActionResult EditSimple(int id = -1, int? departmentID = null, int? phaseDefinitionID = null) // id = ClientID
        {
            var viewModel = ClientViewModelHelper.GetEditSimpleViewModel(uow, id, departmentID, phaseDefinitionID, GetGlobalProperties(getFlowInstance: true));
            return new SecureActionResult("EditSimple", viewModel);
        }

        [HttpPost]
        [ValidateAntiFiddleInjection("ClientID")]
        public ActionResult EditSimple(ClientSimpleViewModel viewmodel, int? ClientID, FlowDefinitionID? flowDefinitionID, int? departmentID = null)
        {
            var client = ClientBL.FromModel(uow, viewmodel);
            return SaveClient(client, "EditSimple", flowDefinitionID, departmentID);
        }


        [HttpPost]
        [ValidateAntiFiddleInjection("ClientID")]
        public ActionResult Edit(ClientViewModel clientViewModel, int? ClientID, FlowDefinitionID? flowDefinitionID, int? departmentID = null)
        {
            var client = ClientBL.FromModel(uow, clientViewModel);
            return SaveClient(client, clientViewModel.ContactPerson, "EditFull", flowDefinitionID, departmentID);
        }

        [HttpPost]
        [ValidateAntiFiddleInjection("ClientID")]
        public ActionResult EditFull(ClientFullViewModel clientFullViewModel, int? ClientID, FlowDefinitionID? flowDefinitionID, int? departmentID = null)
        {
            var client = ClientBL.FromModel(uow, clientFullViewModel);
            return SaveClient(client, clientFullViewModel.ContactPerson, "EditFull", flowDefinitionID, departmentID );
        }

        public SecureActionResult Edit(int id = -1, int? phaseDefinitionID = null, string activeTab = null, int? departmentID = null) // id = ClientID
        {
            var viewModel = ClientViewModelHelper.GetEditViewModel(this, id, phaseDefinitionID, activeTab, departmentID, GetGlobalProperties(getFlowInstance: true));
            return new SecureActionResult("Edit", viewModel);
        }

        public SecureActionResult EditFull(int id = -1, int? phaseDefinitionID = null, string activeTab = null, int? departmentID = null) // id = ClientID
        {
            var viewModel = ClientViewModelHelper.GetEditFullViewModel(this, id, phaseDefinitionID, activeTab, departmentID, GetGlobalProperties(getFlowInstance:true));
            return new SecureActionResult("EditFull", viewModel);
        }

        public PartialViewResult ViewVO(int clientID)
        {
            var viewModel = ClientViewModelHelper.GetViewVOViewModel(this, clientID, GetGlobalProperties());
            return PartialView("ViewVO", viewModel);
        }

        public PartialViewResult ViewAddress(int clientID)
        {
            var viewModel = ClientViewModelHelper.GetViewAddressViewModel(this, clientID, GetGlobalProperties());
            return PartialView("ViewAddress", viewModel);
        }

        public PartialViewResult ViewBasic(int clientid)
        {
            var viewModel = ClientViewModelHelper.GetViewBasicViewModel(this, clientid, GetGlobalProperties());
            return PartialView("ViewBasic", viewModel);
        }

        [HttpPost]
        public JsonResult GetClientFromCivilServiceNumber(string civilServiceNumber)
        {
            var client = new Client();
            var contactPerson = new ContactPerson();
            var hasClient = false;

            if (!string.IsNullOrWhiteSpace(civilServiceNumber))
            {
                client = SearchBL.GetSecurityTrimmedClientByCivilServiceNumber(uow, PointUserInfo(), civilServiceNumber);
                if (client != null)
                {
                    contactPerson = ContactPersonBL.GetByClientID(uow, client.ClientID).FirstOrNew();
                    hasClient = true;
                }
            }
            
            return new JsonNetResult(new
            {
                HasClient = hasClient,
                Client = client,
                HasContactPerson = contactPerson.ContactPersonID > 0,
                ContactPerson = contactPerson.PersonData,
                contactPerson.CIZRelationID
            });
        }

        public ActionResult ImportZIS()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ImportZIS(string patExternalReference, string patExternalReferenceBSN = null)
        {
            ExternalPatientHelper.SetPatExternalReference(patExternalReference);
            if(!String.IsNullOrEmpty(patExternalReferenceBSN))
            {
                ExternalPatientHelper.SetPatExternalReferenceBSN(patExternalReferenceBSN);
            }

            return Redirect("~/Transfer/Search");
        }

        // ClienQRY and PatientRequest use similar logic but with data
        // from different database tables. Until the older ClientQRY clients 
        // are migrated to the new PatientRequest

        #region ClientQRY
        [HttpPost]
        public JsonResult CreateClientQRY(string patientNumber)
        {
            var messageid = ClientReceivedTempHL7BL.SaveClientQRY(uow, PointUserInfo().Organization, patientNumber);

            Response.StatusCode = (int)(!string.IsNullOrEmpty(messageid) ? HttpStatusCode.OK : HttpStatusCode.BadRequest);
            return new JsonNetResult(new { MessageID = messageid });
        }


        public ActionResult GetClientHL7Service(string patientNumber)
        {
            var hl7servicedata = new HL7ServiceData();
            var pointUserInfo = PointUserInfo();
            var hl7client = hl7servicedata.GetPatient(pointUserInfo.Organization.OrganizationID, pointUserInfo.Location.LocationID, patientNumber);

            var personData = new PersonData
            {
                LastName = hl7client.ContactPersonName,
                PhoneNumberGeneral = hl7client.ContactPersonPhoneNumberGeneral,
                PhoneNumberWork = hl7client.ContactPersonPhoneNumberWork,
                PhoneNumberMobile = hl7client.ContactPersonPhoneNumberMobile
            };

            return new JsonNetResult(new { ExternalClient = hl7client, ExternalContactPerson = personData });
        }

        public ActionResult GetClientReceivedTempHL7(string patientNumber, string messageID)
        {
            var clientReceivedHL7 = ClientReceivedTempHL7BL.GetClientReceivedTempHL7(uow, PointUserInfo().Organization, patientNumber, messageID);

            if (clientReceivedHL7 == null)
            {
                return new JsonNetResult(new { ExternalClient = (ClientReceivedTempHL7)null, ExternalContactPerson = (PersonData)null });
            }

            if (!string.IsNullOrWhiteSpace(clientReceivedHL7.HealthInsuranceCompanyUZOVICode))
            {
                var healthinsurer = HealthInsurerBL.GetByUzovi(uow, clientReceivedHL7.HealthInsuranceCompanyUZOVICode);
                if (healthinsurer != null)
                {
                    clientReceivedHL7.HealthInsuranceCompanyID = healthinsurer.HealthInsurerID;
                }
            }

            var personData = new PersonData
            {
                //BirthDate: Provided by clientReceivedHL7-table but not used in the contactperson-screen
                //RelationType: Provided by clientReceivedHL7-table but never filled by our customers
                //Gender, Initials, FirstName, Email : Not provided by clientReceivedHL7-table
                LastName = clientReceivedHL7.ContactPersonName,
                PhoneNumberGeneral = clientReceivedHL7.ContactPersonPhoneNumberGeneral,
                PhoneNumberWork = clientReceivedHL7.ContactPersonPhoneNumberWork,
                PhoneNumberMobile = clientReceivedHL7.ContactPersonPhoneNumberMobile
            };

            return new JsonNetResult(new { ExternalClient = clientReceivedHL7, ExternalContactPerson = personData });
        }
        #endregion

        #region PatientRequest
        [HttpPost]
        public JsonResult CreatePatientRequestTemp(string patientnumber)
        {
            var organization = PointUserInfo().Organization;
            var patientRequestTemp = PatientRequestTempBL.Create(uow, organization.OrganizationID, patientnumber);
            string messageID = patientRequestTemp?.GUID.ToString();

            Response.StatusCode = (int)(!string.IsNullOrEmpty(messageID) ? HttpStatusCode.OK : HttpStatusCode.BadRequest);
            return new JsonNetResult(new { MessageID = messageID });
        }

        public JsonResult GetPatientReceivedTemp(string patientnumber, string messageid)
        {
            var organization = PointUserInfo().Organization;

            var patientReceivedTemp = PatientReceivedTempBL.Get(uow, organization.OrganizationID, patientnumber, messageid);
            if (patientReceivedTemp == null)
            {
                return new JsonNetResult(new { ExternalClient = (PatientReceivedTemp)null, ExternalContactPerson = (PersonData)null });
            }

            var healthInsurer = HealthInsurerBL.GetByUzovi(uow, patientReceivedTemp.HealthInsuranceCompanyUZOVICode);
            if (healthInsurer != null)
            {
                patientReceivedTemp.HealthInsuranceCompanyID = healthInsurer.HealthInsurerID;
            }

            var personData = new PersonData
            {
                //BirthDate: Provided by clientReceivedHL7-table but not used in the contactperson-screen
                //RelationType: Provided by clientReceivedHL7-table but never filled by our customers
                //Gender, Initials, FirstName, Email : Not provided by clientReceivedHL7-table
                LastName = patientReceivedTemp.ContactPersonName,
                PhoneNumberGeneral = patientReceivedTemp.ContactPersonPhoneNumberGeneral,
                PhoneNumberWork = patientReceivedTemp.ContactPersonPhoneNumberWork,
                PhoneNumberMobile = patientReceivedTemp.ContactPersonPhoneNumberMobile
            };

            return new JsonNetResult(new { ExternalClient = patientReceivedTemp, ExternalContactPerson = personData });
        }
        #endregion

        public JsonResult GetClientFromNedapService(string civilServiceNumber, int departmentID)
        {
            Client client = null;
            var department = DepartmentBL.GetByDepartmentID(uow, departmentID);
            var organization = department.Location.Organization;

            var nedap = new Nedap { ReceivingOrganization = organization };
            var nedapclientdata = nedap.GetNedapClientFromBSN(uow, civilServiceNumber);

            if (nedapclientdata.ID > 0)
            {
                client = nedap.GetClientFromNedapClientData(nedapclientdata, civilServiceNumber);
            }

            return new JsonNetResult(new { HasClient = (client != null), Client = client });
        }


        private ActionResult SaveClient(Client client, string viewName, FlowDefinitionID? flowDefinitionID, int? departmentID = null)
        {
            return SaveClient(client, null, viewName, flowDefinitionID, departmentID);
        }

        private ActionResult SaveClient(Client client, ContactPersonViewModel contactPerson, string viewName, FlowDefinitionID? flowDefinitionID, int? departmentID = null)       
        {
            var globalProps = GetGlobalProperties();
            int transferID = globalProps.TransferID;
            HttpStatusCode statusCode = HttpStatusCode.OK;

            if (ModelState.IsValid)
            {
                ClientViewModelHelper.ClientValidation(this, client, transferID);
            }

            if (ModelState.IsValid)
            {
                var pointUserInfo = globalProps.PointUserInfo;

                var phaseDefinition = PhaseDefinitionCacheBL.GetByID(uow, PhaseDefinitionID);
                var rights = PhaseDefinitionRightsBL.GetFormRightsPhaseDefinitionID(uow, pointUserInfo, phaseDefinition, null);
                ViewBag.Rights = rights;

                var formType = FormTypeBL.GetByURL(uow, "Client", viewName);

                FlowInstance flowInstance = null;
                int organizationID = pointUserInfo.Organization.OrganizationID;

                ClientBL.SetModifiedFields(uow, client, pointUserInfo);
                bool IsNewClient = client.ClientID <= 0;
                var logEntry = LogEntryBL.Create(formType, pointUserInfo);

                ClientBL.Save(uow, client);

                if (IsNewClient && flowDefinitionID.HasValue)
                {
                    if (departmentID == null)
                    {
                        departmentID = pointUserInfo.Department.DepartmentID;
                    }

                    var newTransfer = TransferBL.Generate(uow, client.ClientID, logEntry);
                    transferID = newTransfer.TransferID;

                    flowInstance = FlowInstanceBL.NewFlowInstance(uow, flowDefinitionID.Value, transferID, logEntry, organizationID, departmentID.Value);                   

                    ClientBL.SetAnonymous(uow, flowInstance.FlowInstanceID);

                    FlowInstanceSearchValuesBL.Create(uow, flowInstance.FlowInstanceID, transferID, client.FullName(), departmentID);

                    var phaseDefinitionBeginFlow = PhaseDefinitionCacheBL.GetPhaseDefinitionBeginFlow(uow, flowDefinitionID.Value);
                    var firstPhaseInstance = PhaseInstanceBL.Create(uow, flowInstance.FlowInstanceID, phaseDefinitionBeginFlow.PhaseDefinitionID, logEntry, Status.Done);

                    if (phaseDefinitionBeginFlow != null)
                    {
                        int? nextPhaseDefinitionID = System.Web.HttpContext.Current.Request.GetRequestVariable<int?>("nextphasedefinitionid", null);
                        if (nextPhaseDefinitionID.HasValue)
                        {
                            PhaseInstanceBL.HandleNextPhase(uow, flowInstance.FlowInstanceID, firstPhaseInstance.PhaseInstanceID, nextPhaseDefinitionID.Value, pointUserInfo.Employee.EmployeeID, logEntry);
                        }
                        else
                        {
                            //depreciated
                            var nextPhaseDefintions = PhaseDefinitionBL.GetNextPhaseDefinitions(uow, phaseDefinitionBeginFlow.PhaseDefinitionID).ToList();
                            foreach (var nextPhaseDefintion in nextPhaseDefintions)
                            {
                                PhaseInstanceBL.HandleNextPhase(uow, flowInstance.FlowInstanceID, firstPhaseInstance.PhaseInstanceID, nextPhaseDefintion.PhaseDefinitionID,
                                    pointUserInfo.Employee.EmployeeID, logEntry);
                            }
                        }
                    }
                }

                if (contactPerson != null && !ContactPersonBL.GetByClientID(uow, client.ClientID).Any())
                {
                    contactPerson.ClientID = client.ClientID;
                    ContactPersonBL.Insert(uow, contactPerson);
                }

                ExternalPatientHelper.ClearPatExternalReference(pointUserInfo.Organization.HL7ConnectionDifferentMenu.ConvertTo<bool>());
                ExternalPatientHelper.ClearPatExternalReferenceBSN(pointUserInfo.Organization.HL7ConnectionDifferentMenu.ConvertTo<bool>());

                if (flowInstance != null)
                {
                    // use a FlowFieldAttributeException to determine if a region/organization take part in the VO as attachment!
                    var voFormulierType = FlowFieldAttributeBL.IsVisible(uow, pointUserInfo.Region.RegionID, pointUserInfo.Organization.OrganizationID,
                        (int)FlowFormType.VerpleegkundigeOverdrachtFlow, FlowFieldConstants.ID_VOFormulierType);

                    var latestAttachments = AttachmentReceivedTempBL.GetLatestByClientDetails(uow, organizationID, client.PatientNumber, client.VisitNumber, client.BirthDate);
                    
                    if (voFormulierType)
                    {
                        var voAttachment = latestAttachments.FirstOrDefault(art => art.AttachmentTypeID == AttachmentTypeID.ExternVODocument);
                        if (voAttachment != null)
                        {
                            var verpleegkundigeOverdrachtBO = new VerpleegkundigeOverdrachtBO(uow, flowInstance, pointUserInfo);
                            verpleegkundigeOverdrachtBO.HandleNextPhase(voAttachment, logEntry);
                        }
                    }

                    latestAttachments = latestAttachments.Where(art => art.AttachmentTypeID != AttachmentTypeID.ExternVODocument);
                    if (latestAttachments.Any())
                    {
                        // systememployee used here as the AttachmentReceivedTemp objects are automagically handled and have nothing to do with the currently logged in user
                        var systemEmployee = PointUserInfoHelper.GetSystemEmployee(uow);
                        var systemLogEntry = LogEntryBL.Create(FlowFormType.FormServiceAttachment, systemEmployee);

                        TransferAttachmentBL.Upsert(uow, transferID, latestAttachments, systemLogEntry);
                    }
                }

                FlowInstanceSearchValuesBL.CalculateReadValuesByTransferIDAsync(uow, transferID);
            }
            else
            {
                ErrorMessage = string.Join("; ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                statusCode = HttpStatusCode.BadRequest;
            }

            if (!Request.IsAjaxRequest())
            {
                return View(viewName, client);
            }

            Response.StatusCode = (int)statusCode;

            return statusCode == HttpStatusCode.OK 
                ? new JsonNetResult(new { TransferID = transferID, client.ClientID, Url = Url.Action(viewName, "Client", new { TransferID = transferID, id = client.ClientID, globalProps.PhaseDefinitionID, globalProps.PhaseInstanceID }) }) 
                : new JsonNetResult(new { ErrorMessage }, JsonRequestBehavior.DenyGet);
        }

    }
}
