﻿using Point.Infrastructure.Exceptions;
using System.Web.Mvc;

namespace Point.MVC.WebApplication.Controllers
{
    public class ErrorController : Controller
    {
        public ActionResult ErrorPage(ErrorViewModel model)
        {
            return View(model);
        }

        public ActionResult NotFound()
        {          
            return View();
        }
    }

}