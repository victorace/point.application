﻿using DevTrends.MvcDonutCaching;
using Point.Business.Logic;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Infrastructure.Attributes;
using Point.Infrastructure.Extensions;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Point.MVC.WebApplication.Controllers
{
    [DonutOutputCache(CacheProfile = "Short")]
    public class SignaleringController : PointController
    {
        public ActionResult PromptFlowFieldsChanged(SignaleringItemsViewModel model)
        {
            return View(model);
        }

        public JsonResult CloseMultiple(int[] signaleringids = null)
        {
            int employeeid = PointUserInfo().Employee.EmployeeID;
            SignaleringBL.CloseMultiple(uow, signaleringids, employeeid);

            return Json(new { SignaleringIDs = signaleringids, Success = true }, JsonRequestBehavior.AllowGet);
        }

        [ValidateAntiFiddleInjection("signaleringid")]
        public ActionResult EditComment(int signaleringid)
        {
            var model = SignaleringBL.GetById(uow, signaleringid);
            return View(model);
        }

        [ValidateAntiFiddleInjection("signaleringid")]
        public JsonResult CloseSignaleringDirect(int signaleringid)
        {
            var model = SignaleringBL.GetById(uow, signaleringid);
            return SaveSignalering(signaleringid, model.Comment, SignaleringStatus.Closed);
        }

        [ValidateAntiFiddleInjection("signaleringid")]
        public JsonResult SaveSignalering(int signaleringid, string comment, SignaleringStatus status)
        {
            var pointuserinfo = PointUserInfo();
            SignaleringBL.SaveSignalering(uow, signaleringid, comment, status, pointuserinfo);
            return Json(new { SignaleringID = signaleringid, Status = (int)status });
        }

        public ActionResult DossierSignalList(int flowinstanceid)
        {
            var pointuserinfo = PointUserInfo();

            var viewmodel = new SignalListViewModel
            {
                SignaleringItems = SignaleringBL.GetCurrentByFlowinstanceIDAndPointUserInfo(uow, flowinstanceid, pointuserinfo),
                SignalListType = SignalListViewModel.ListType.Dossier,
                FlowInstanceID = flowinstanceid
            };

            viewmodel.SignaleringEmployees = SignaleringBL.GetEmployees(uow, viewmodel.SignaleringItems.Select(si => si.CreatedByEmployeeID).Distinct().ToArray<int>()).ToList();
            viewmodel.SignaleringClients = SignaleringBL.GetClients(uow, viewmodel.SignaleringItems.Select(si => si.FlowInstanceID).Distinct().ToArray<int>()).ToList();
            viewmodel.OrganizationID = pointuserinfo.Organization.OrganizationID;

            return View("SignalList", viewmodel);
        }

        public ActionResult AllSignalList(SignaleringStatus status = SignaleringStatus.Open)
        {
            var pointuserinfo = PointUserInfo();
            var departmentidfocus = LoginBL.GetDepartmentIDReference();

            var viewmodel = new SignalListViewModel();

            string departmentfilter = "";
            if(departmentidfocus.GetValueOrDefault(0) > 0)
            {
                var department = DepartmentBL.GetByDepartmentID(uow, departmentidfocus.Value);
                if(department != null)
                {
                    departmentfilter = department.Name;
                }
            }

            var deparmentIdFilter = new List<int>();
            if (departmentidfocus != null)
            {
                deparmentIdFilter.Add(departmentidfocus.Value);
            }
            var searchStatus = SearchControlBL.GetState(this.HttpContext);
            if (searchStatus?.DepartmentIDs != null && searchStatus.DepartmentIDs.Any())
            {
                deparmentIdFilter.AddRange(searchStatus.DepartmentIDs);
            }

            viewmodel.DepartmentFilter = departmentfilter;
            viewmodel.SignaleringItems = SignaleringBL.GetCurrentByPointUserInfo(uow, pointuserinfo, status: status, departmentIdFilter: deparmentIdFilter);
            viewmodel.SignalListType = SignalListViewModel.ListType.Global;
            viewmodel.SignaleringEmployees = SignaleringBL.GetEmployees(uow, viewmodel.SignaleringItems.Select(si => si.CreatedByEmployeeID).Distinct().ToArray<int>()).ToList();
            viewmodel.SignaleringClients = SignaleringBL.GetClients(uow, viewmodel.SignaleringItems.Select(si => si.FlowInstanceID).Distinct().ToArray<int>()).ToList();
            viewmodel.OrganizationID = pointuserinfo.Organization.OrganizationID;

            return View("SignalList", viewmodel);
        }

        public ActionResult DispatchToForm(int signaleringID, SignaleringStatus status = SignaleringStatus.Open)
        {
            var item = SignaleringBL.GetById(uow, signaleringID);

            if (item == null)
            {
                return RedirectToAction("Search", "Transfer");
            }

            if (item.SignaleringTarget.SignaleringTrigger.FormTypeID.HasValue)
            {
                var formtypeid = item.SignaleringTarget.SignaleringTrigger.FormTypeID.Value;
                var formtypesignals = SignaleringBL.GetCurrentByPointUserInfo(uow, PointUserInfo(), status: status).Where(it => it.SignaleringTarget.SignaleringTrigger.FormTypeID == formtypeid).ToList();

                return RedirectToAction("StartFlowFormByFormTypeID", "FlowMain", new { item.FlowInstance.TransferID, formtypeid, ShowHistory = true, HistoryFromDate = formtypesignals.Any() ? formtypesignals.Min(it => it.Created).ToString("yyyy-MM-dd HH:mm") : null });
            }
            else if (item.SignaleringTarget.SignaleringTrigger.GeneralActionID.HasValue)
            {
                var generalaction = item.SignaleringTarget.SignaleringTrigger.GeneralAction;
                if (generalaction != null)
                {
                    return RedirectToAction(generalaction.MethodName.GetURLComponent(2), generalaction.MethodName.GetURLComponent(1), new { item.FlowInstance.TransferID });
                }
            }

            return RedirectToAction("DashBoard", "FlowMain", new { item.FlowInstance.TransferID });
        }

        public ActionResult CurrentSignaleringStatus(int? flowinstanceid = null)
        {
            var pointuserinfo = PointUserInfo();

            var deparmentIdFilter = new List<int>();
            var departmentidfocus = LoginBL.GetDepartmentIDReference();
            if(departmentidfocus != null)
            {
                deparmentIdFilter.Add(departmentidfocus.Value);
            }
            var searchStatus = SearchControlBL.GetState(this.HttpContext);
            if(searchStatus?.DepartmentIDs != null && searchStatus.DepartmentIDs.Any())
            {
                deparmentIdFilter.AddRange(searchStatus.DepartmentIDs);
            }

            var count = flowinstanceid.HasValue && flowinstanceid.Value > 0
                ? SignaleringBL.GetCurrentByFlowinstanceIDAndPointUserInfo(uow, flowinstanceid.Value, pointuserinfo).Count()
                : SignaleringBL.GetCurrentByPointUserInfo(uow, pointuserinfo, SignaleringStatus.Open, deparmentIdFilter).Count();

            var viewmodel = new SignaleringStatusViewModel()
            {
                FlowInstanceID = flowinstanceid.HasValue && flowinstanceid.Value > 0 ? flowinstanceid.Value : (int?)null,
                SignaleringCount = count
            };

            return PartialView("_CurrentSignaleringStatus", viewmodel);
        }
    }
}