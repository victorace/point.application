﻿using Point.Business.Logic;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Infrastructure.Attributes;
using Point.Infrastructure.Extensions;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Point.MVC.WebApplication.Controllers
{
    public class ContactPersonController : PointFormController
    {
        private string ListViewName = "_List"; 

        public ActionResult List(int clientID = -1)
        {
            var contactPerson = Enumerable.Empty<ContactPerson>();
            if (clientID > 0)
            {
                contactPerson = ContactPersonBL.GetByClientID(uow, clientID);
            }
            var contactPersonViewModel = ContactPersonViewBL.FromModelList(uow, contactPerson);

            ViewBag.ClientID = clientID;
            return new SecureActionResult(ListViewName, contactPersonViewModel, true);
        }

        public ActionResult ListReadOnly(int clientID = -1)
        {
            ListViewName = "_ListReadOnly";
            return List(clientID);
        }

        public ActionResult Insert(int clientID)
        {
            ViewBag.CizRelations = uow.CIZRelationRepository.GetAll().OrderBy(cr => cr.Description);

            var contactPerson = new ContactPersonViewModel {ClientID = clientID};

            return PartialView("_Insert", contactPerson);
        }

        [HttpPost]
        public ActionResult Insert(ContactPersonViewModel contactPersonViewModel)
        {
            var errorMessage = "";

            if (ModelState.IsValid)
            {
                ContactPersonBL.Insert(uow, contactPersonViewModel);
            }
            else
            {
                errorMessage = "Gegevens zijn niet juist: " +
                               string.Join("; ", ModelState.Values.SelectMany(m => m.Errors).Select(m => m.ErrorMessage));
            }

            return result(contactPersonViewModel.ClientID, errorMessage);
        }

        [ValidateAntiFiddleInjection("contactPersonID")]
        public ActionResult Edit(int clientID, int contactPersonID)
        {
            ViewBag.CizRelations = uow.CIZRelationRepository.GetAll().OrderBy(cr => cr.Description);

            var contactPerson = ContactPersonBL.GetByID(uow, contactPersonID);
            var contactPersonViewModel = ContactPersonViewBL.FromModel(uow, contactPerson);

            return PartialView("_Edit", contactPersonViewModel);
        }

        [HttpPost]
        [ValidateAntiFiddleInjection("ContactPersonID")]
        public ActionResult Edit(ContactPersonViewModel contactPersonViewModel)
        {
            var errorMessage = "";

            if (ModelState.IsValid)
            {
                ContactPersonBL.Update(uow, contactPersonViewModel);
            }
            else
            {
                errorMessage = "Gegevens zijn niet juist: " +
                               string.Join("; ", ModelState.Values.SelectMany(m => m.Errors).Select(m => m.ErrorMessage));
            }

            return result(contactPersonViewModel.ClientID, errorMessage);
        }

        [ValidateAntiFiddleInjection("contactPersonID")]
        public ActionResult Delete(int clientID, int contactPersonID)
        {
            var errorMessage = "";

            var contactPerson =  uow.ContactPersonRepository.Get(cp => cp.ContactPersonID == contactPersonID).FirstOrDefault();

            if (contactPerson != null)
            {
                if (!ContactPersonBL.Delete(uow, contactPerson))
                {
                    errorMessage = "Verwijderen is mislukt";
                }
            }
            else
            {
                errorMessage = "Gegevens zijn niet gevonden";
            }

            return result(clientID, errorMessage);
        }

        private ActionResult result(int clientID, string errorMessage)
        {
            Response.StatusCode = (int)(errorMessage == "" ? HttpStatusCode.OK : HttpStatusCode.BadRequest);
            return errorMessage == "" ? Json(new {success = true, Url = Url.Action("List", new {clientID})}, JsonRequestBehavior.AllowGet) 
                : Json(new {success = false, ErrorMessage = errorMessage}, JsonRequestBehavior.AllowGet);
        }
    }
}