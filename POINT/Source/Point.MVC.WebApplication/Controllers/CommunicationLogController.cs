﻿using Point.Business.Logic;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Infrastructure.Attributes;
using Point.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Mvc;

namespace Point.MVC.WebApplication.Controllers
{
    public class CommunicationLogController : PointFormController
    {
        private IEnumerable<CommunicationLogViewModel> getEditViewModelAndSetViewBag(int transferID, DateTime? dateFrom, DateTime? dateTo)
        {
            IEnumerable<CommunicationLog> communicationlogs = Enumerable.Empty<CommunicationLog>();
            if (dateFrom.HasValue && dateTo.HasValue)
            {
                communicationlogs = CommunicationLogBL.GetByTransferIDAndDate(uow, transferID, dateFrom, dateTo);
            }
            else
            {
                communicationlogs = CommunicationLogBL.GetByTransferID(uow, transferID);
            }
            var communicationlogviewmodels = CommunicationLogViewBL.FromModelList(uow, communicationlogs);
            return communicationlogviewmodels;
        }

        public ActionResult Index()
        {
            PageTitle = "Verstuurde e-mails";
            PageHeader = "Verstuurde e-mails";

            var model = getEditViewModelAndSetViewBag(TransferID, null, null);

            return new SecureActionResult(model);
        }

        [ValidateAntiFiddleInjection("communicationLogID")]
        public ActionResult ViewContent(int communicationLogID)
        {
            var communicationlog = CommunicationLogBL.GetByID(uow, communicationLogID);
            var userinfo = PointUserInfo();
            var showanonymous = false;

            if (userinfo.Organization?.OrganizationID != communicationlog.OrganizationID)
            { 
                var flowinstance = FlowInstanceBL.GetByTransferID(uow, communicationlog.TransferID.GetValueOrDefault(0));
                showanonymous = ClientBL.ShowAnonymous(flowinstance, userinfo.Organization?.OrganizationID);
            }

            var content = "";
            var beginNoteTag = "<notitie>";
            var endNoteTag = "</notitie>";
            if (communicationlog.MailType == CommunicationLogType.ZorgmailMedvri)
            {
                var beginNoteIdx = communicationlog.Content.IndexOf(beginNoteTag);
                var endNoteIdx = communicationlog.Content.IndexOf(endNoteTag);
                if (beginNoteIdx >= 0)
                {
                    content = communicationlog.Content.Substring(beginNoteIdx, endNoteIdx - beginNoteIdx);
                    content = content.Replace(beginNoteTag, string.Empty);                    
                }
            }
            else
            {
                content = communicationlog.Content;
            }

            ViewBag.ShowAnonymous = showanonymous;
            ViewBag.HasBR = Regex.IsMatch(content, @"<br\s*/>", RegexOptions.Compiled);
            ViewBag.Content = content;

            return new SecureActionResult(communicationlog, true);
        }
    }
}