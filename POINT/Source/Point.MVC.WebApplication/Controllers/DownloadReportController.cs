﻿using Point.Business.Logic;
using Point.Models.Enums;
using Point.Infrastructure.Attributes;
using Point.MVC.WebApplication.Attributes;
using System;
using System.Web.Mvc;
using Point.Infrastructure.Extensions;

namespace Point.MVC.WebApplication.Controllers
{
    public class DownloadReportController : PointController
    {
        public ActionResult CSVDumpOverview()
        {
            var pointuserinfo = PointUserInfo();
            var viewmodel = TransferAttachmentViewBL.GetByEmployeeID(uow, pointuserinfo.Employee.EmployeeID, AttachmentTypeID.DumpReport);

            return View(viewmodel);
        }

        [FunctionRoles(FunctionRoleTypeID.Reporting)]
        [ValidateAntiFiddleInjection("AttachmentID")]
        public FileResult Download(int AttachmentID )
        {
            var pointuserinfo = PointUserInfo();
            var genericfile = ReportCSVDumpBL.GetDumpReport(uow, pointuserinfo.Employee.EmployeeID, AttachmentID);
            if (genericfile == null)
            { 
                throw new Exception("Geen gegevens");
            }

            Response.DisableClientCache();

            return File(genericfile.FileData, genericfile.ContentType, genericfile.FileName);
        }
    }
}