﻿using Point.Business.Logic;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Infrastructure.Attributes;
using Point.Infrastructure.Caching;
using Point.Infrastructure.Exceptions;
using Point.Infrastructure.Extensions;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Point.MVC.WebApplication.Controllers
{
    public partial class FlowMainController
    {
        private static string Content_GeenFlowInstance = "Geen FlowInstance";

        public ActionResult RelocateDepartment()
        {
            PageTitle = "Verplaats naar andere afdeling";
            PageHeader = "Verplaats naar andere afdeling";

            var flowinstance = FlowInstanceBL.GetByTransferID(uow, TransferID);
            if (flowinstance == null || flowinstance == default(FlowInstance))
            {
                throw new InvalidOperationException(Content_GeenFlowInstance);
            }

            var viewmodel = RelocateDossierViewModelBL.GetViewModelByFlowInstanceID(uow, flowinstance.FlowInstanceID);

            ViewBag.Departments = DepartmentBL.GetByLocationID(uow, viewmodel.SourceLocationID)
                .OrderBy(it => it.Name).ToList()
                .ToSelectListItems(it => it.Name, it => it.DepartmentID.ToString(), firstItem: " - Maak uw keuze - ", firstItemValue: "");

            return new SecureActionResult(viewmodel);
        }

        [HttpPost]
        [ValidateAntiFiddleInjection("TransferID")]
        public JsonResult RelocateDepartment(RelocateDossierViewModel viewmodel)
        {
            var pointuserinfo = PointUserInfo();
            var flowinstance = SearchBL.GetSecurityTrimmedFlowInstance(uow, pointuserinfo, TransferID);
            if (flowinstance == null)
            {
                throw new PointSecurityException("U heeft geen toegang tot dit dossier", ExceptionType.AccessDenied);
            }

            RelocateDossierViewModelBL.RelocateToNewDepartment(uow, flowinstance.FlowInstanceID, viewmodel, pointuserinfo);
            FlowInstanceSearchValuesBL.CalculateSearchValues(flowinstance.FlowInstanceID);

            CacheService.Instance.RemoveAll(TransferID.ToString());

            flowinstance = SearchBL.GetSecurityTrimmedFlowInstance(uow, pointuserinfo, TransferID);

            var newdepartment = DepartmentBL.GetByDepartmentID(uow, viewmodel.DestinationDepartmentID);
            var message = $"Het dossier is verplaatst naar '{newdepartment.FullName()}'.";
            var returnurl = Url.Action("Dashboard", "FlowMain", new { TransferID });
            if (flowinstance == null)
            {
                message = $"Het dossier is verplaatst naar '{newdepartment.FullName()}'. U heeft geen toegang tot het dossier meer.";
                returnurl = Url.Action("Search", "Transfer");
            }

            return new JsonResult() { Data = new { message, returnurl } };
        }

        public ActionResult RelocateOrganization()
        {
            PageTitle = "Verplaats naar andere organisatie";
            PageHeader = "Verplaats naar andere organisatie";

            var flowinstance = FlowInstanceBL.GetByTransferID(uow, TransferID);
            if (flowinstance == null || flowinstance == default(FlowInstance))
            {
                throw new InvalidOperationException(Content_GeenFlowInstance);
            }

            var viewmodel = RelocateDossierViewModelBL.GetViewModelByFlowInstanceID(uow, flowinstance.FlowInstanceID);

            return new SecureActionResult(viewmodel);
        }

        [HttpPost]
        [ValidateAntiFiddleInjection("TransferID")]
        public ActionResult RelocateOrganization(RelocateDossierViewModel viewmodel)
        {
            var pointuserinfo = PointUserInfo();
            var flowinstance = SearchBL.GetSecurityTrimmedFlowInstance(uow, pointuserinfo, TransferID);
            if (flowinstance == null)
            {
                throw new PointSecurityException("U heeft geen toegang tot dit dossier", ExceptionType.AccessDenied);
            }

            RelocateDossierViewModelBL.RelocateToNewOrganization(uow, flowinstance.FlowInstanceID, viewmodel, pointuserinfo);
            FlowInstanceSearchValuesBL.CalculateSearchValues(flowinstance.FlowInstanceID);

            CacheService.Instance.RemoveAll(TransferID.ToString());

            flowinstance = SearchBL.GetSecurityTrimmedFlowInstance(uow, pointuserinfo, TransferID);

            var newdepartment = DepartmentBL.GetByDepartmentID(uow, viewmodel.DestinationDepartmentID);
            var message = $"Het dossier is verplaatst naar '{newdepartment.FullName()}'.";
            var returnurl = Url.Action("Dashboard", "FlowMain", new { TransferID });
            if (flowinstance == null)
            {
                message = $"Het dossier is verplaatst naar '{newdepartment.FullName()}'. U heeft geen toegang tot het dossier meer.";
                returnurl = Url.Action("Search", "Transfer");
            }

            return new JsonResult() { Data = new { message, returnurl } };
        }
    }
}