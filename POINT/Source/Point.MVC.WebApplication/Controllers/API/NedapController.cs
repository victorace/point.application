﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Point.Business.Logic;
using Point.Infrastructure.Constants;
using Point.Models.Enums;
using Point.Infrastructure.Extensions;
using Point.MVC.WebApplication.Attributes;
using Point.ServiceBus.Models;

namespace Point.MVC.WebApplication.Controllers.API.Scheduler
{

    [AllowAnonymous]
    public class NedapController : PointController
    {
        protected override void Dispose(bool disposing)
        {
            uow.SafeDispose();
            base.Dispose(disposing);
        }
        
        [FunctionRoles(FunctionRoleLevelID.Global, FunctionRoleTypeID.ManageAll)]
        public ActionResult NedapResults(int id = -1) // transferid
        {
            var nedaplogtypes = new List<CommunicationLogType>() { CommunicationLogType.NedapVO, CommunicationLogType.NedapAttachment, CommunicationLogType.NedapForm };

            var stringbuilder = new StringBuilder();
            stringbuilder.AppendLine("Resultaten voor TransferID: " + id.ToString());
            stringbuilder.AppendLine("----------------------------------");
            stringbuilder.AppendLine("CommunicationLog:");
            stringbuilder.AppendLine("----------------------------------");
            foreach (var communicationlog in CommunicationLogBL.GetByTransferID(uow, id).Where(cl => cl.MailType != null).OrderByDescending(cl => cl.Timestamp))
            {
                if (communicationlog.MailType != null && nedaplogtypes.Contains(communicationlog.MailType.Value))
                {
                    stringbuilder.AppendLine(communicationlog.Timestamp.ToSortablePointDateHourDisplay());
                    stringbuilder.AppendLine("   " + communicationlog.MailType.GetDescription() + ": " + communicationlog.Subject);
                    stringbuilder.AppendLine("   " + communicationlog.Content);
                }
            }
            stringbuilder.AppendLine("----------------------------------");
            stringbuilder.AppendLine("CommunicationQueue:");
            stringbuilder.AppendLine("----------------------------------");

            foreach (var communicationqueue in CommunicationQueueBL.GetByTransferID(uow, id).Where(cl => cl.MailType != null).OrderByDescending(cl => cl.Timestamp))
            {
                if (communicationqueue.MailType != null && nedaplogtypes.Contains(communicationqueue.MailType.Value))
                {
                    stringbuilder.AppendLine(communicationqueue.Timestamp.ToSortablePointDateHourDisplay());
                    stringbuilder.AppendLine("   " + communicationqueue.MailType.GetDescription() + $". FormSetVersionID: [{communicationqueue.FormSetVersionID}]. TransferAttachmentID: [{communicationqueue.TransferAttachmentID}]");
                    stringbuilder.AppendLine("   " + communicationqueue.ReasonNotSend);
                }
            }

            return Content("<html><body><pre>" + stringbuilder.ToString().Replace(Environment.NewLine, "<br />") + "</pre></body></html>");
        }

        public ActionResult NedapError(int organizationID) // organizationid
        {
            var nedaplogtypes = new List<CommunicationLogType>() { CommunicationLogType.NedapVO, CommunicationLogType.NedapAttachment, CommunicationLogType.NedapForm };

            var stringbuilder = new StringBuilder();


            foreach (var communicationlog in CommunicationLogBL.GetByOrganizationIDAndMailTypeAndDate(uow, organizationID, nedaplogtypes.ToArray(), DateTime.Now.AddMonths(-1)).Where(cl =>!cl.Content.StartsWith("{")).OrderByDescending(cl => cl.Timestamp))
            {
                stringbuilder.AppendLine(communicationlog.Timestamp.ToSortablePointDateHourDisplay());
                stringbuilder.AppendLine("TransferID: " + communicationlog.TransferID + ", " + communicationlog.MailType.GetDescription() + ": " + communicationlog.Subject);
                stringbuilder.AppendLine("----------------------------------");
                stringbuilder.AppendLine(System.Web.HttpUtility.HtmlEncode(communicationlog.Content));
                stringbuilder.AppendLine("----------------------------------");
            }

            return Content("<html><body><pre>" + stringbuilder.ToString().Replace(Environment.NewLine, "<br />") + "</pre></body></html>");

        }
    }
}