﻿using Point.Business.Logic;
using Point.ServiceBus.Models;
using System.Web.Mvc;

namespace Point.MVC.WebApplication.Controllers.API.Scheduler
{
    public class IguanaController : PointBaseController
    {
        [AllowAnonymous]
        public ActionResult GetIguanaChannelStatus()
        {
            var status = IguanaChannelBL.GetIguanaChannelStatus(uow);
            return Content(status);
        }
    }
}