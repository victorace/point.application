﻿using Newtonsoft.Json.Converters;
using Point.Business.Logic;
using Point.Communication;
using Point.Communication.HL7Service;
using Point.Database.Extensions;
using Point.Database.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Point.MVC.WebApplication.Controllers.API
{
    public class ExternalDataController : PointFormController
    {
        public async Task<ActionResult> GetFromEPD(int transferid, int formtypeid)
        {
            var client = ClientBL.GetByTransferID(uow, transferid);

            int organizationid = PointUserInfo().Organization.OrganizationID;

            Dictionary<string, string> data = new Dictionary<string, string>();

            var endpointconfiguration = EndpointConfigurationBL.Get(uow, organizationid, formtypeid);
            if (string.IsNullOrEmpty(endpointconfiguration?.Address) || string.IsNullOrEmpty(endpointconfiguration.Method))
            {
                Logger.Error($"No EndpointConfiguration for the Organization({organizationid}), FormType({formtypeid}) combination.");

                data.Add("Error", "Geen geldig address of method voor deze organisatie.");
                return new SecureJsonResult(data);
            }

            data = await get_endpoint_data_async(endpointconfiguration, client);

            return new SecureJsonResult(data) { JsonConverters = new[] { new IsoDateTimeConverter() { DateTimeFormat = "dd-MM-yyyy" } } };
        }

        private async Task<Dictionary<string, string>> get_endpoint_data_async(EndpointConfiguration endpointconfiguration, Client client)
        {
            var hl7epdreceiver = new HL7EPDReceiver();
            var epdrequest = new EPDRequest()
            {
                BirthDate = client.BirthDate,
                CivilServiceNumber = client.CivilServiceNumber,
                Gender = client.Gender.EPDRequestGender(),
                PatientNumber = client.PatientNumber,
                VisitNumber = client.VisitNumber
            };
            return await hl7epdreceiver.GetAsync(endpointconfiguration, epdrequest);
        }
    }
}