﻿using DevTrends.MvcDonutCaching;
using Point.Business.BusinessObjects;
using Point.Business.Helper;
using Point.Business.Logic;
using Point.Database.CacheModels;
using Point.Database.Extensions;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Infrastructure;
using Point.Infrastructure.Attributes;
using Point.Infrastructure.Exceptions;
using Point.Infrastructure.Extensions;
using Point.Models.Enums;
using Point.MVC.WebApplication.Extensions;
using Point.MVC.WebApplication.Helpers;
using Point.ServiceBus.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Point.Infrastructure.Constants;
using Point.Processing.Handlers;
using Point.ServiceBus.EventTypes;

namespace Point.MVC.WebApplication.Controllers
{
    [DonutOutputCache(CacheProfile = "Short")]
    public partial class FormTypeController : BaseFlowFieldController
    {
        private const string enter = "\r\n";

        private string dashboardUrl(UrlHelper Url)
        {
            return Url.Action("DashBoard", "FlowMain", new { TransferID }, Utils.UriScheme());
        }

        public ActionResult Index(int? id) //FormTypeID
        {
            FormType formType = null;

            if (id.HasValue)
            {
                formType = FormTypeBL.GetByFormTypeID(uow, id.Value);
            }

            if (formType == null)
            {
                return new HttpNotFoundResult();
            }

            var urlParts = formType.URL.Split('/');
            if (urlParts.Length != 2)
            {
                return new HttpNotFoundResult();

            }

            return RedirectToAction(urlParts[1], urlParts[0], RouteData.Values);
        }

        public ActionResult ClientCareForm(bool regenerate = false)
        {
            return new SecureActionResult(FormTypeViewModelHelper.GetClientCareFormViewModel(uow, GetGlobalPropertiesFormType(), regenerate));
        }

        [HttpPost]
        public ActionResult ClientCareForm(string patientBrief)
        {
            var pointUserInfo = PointUserInfo();
            var formsetversion = SaveFormsetAndValidFields(new List<FlowWebField>());
            var logEntry = LogEntryBL.Create(uow, (int)FlowFormType.PatientenBrief, pointUserInfo);
            FlowFieldValueBL.UpdateOrInsert(uow, FlowInstance.FlowInstanceID, formsetversion.FormSetVersionID,
                FlowFieldConstants.ID_Patientbrief, patientBrief, logEntry);
            return SaveResult(CurrentModelState, formsetversion);
        }

        public ActionResult ELVForm()
        {
            return new SecureActionResult(FormTypeViewModelHelper.GetElvFormViewModel(uow, GetGlobalPropertiesFormType()));
        }

        public ActionResult ELVFormHA()
        {
            return new SecureActionResult(FormTypeViewModelHelper.GetElvFormHAViewModel(uow, GetGlobalPropertiesFormType()));
        }

        [HttpPost]
        public ActionResult ELVForm([ModelBinder(typeof(FlowWebFieldBinder))] List<FlowWebField> model)
        {
            var formsetversion = SaveFormsetAndValidFields(model);
            var modelState = CurrentModelState;

            FlowInstanceSearchValuesBL.CalculateReadValuesByTransferIDAsync(uow, formsetversion.TransferID);

            return SaveResult(modelState, formsetversion);
        }

        [HttpPost]
        public ActionResult ELVFormHA([ModelBinder(typeof(FlowWebFieldBinder))] List<FlowWebField> model)
        {
            var formsetversion = SaveFormsetAndValidFields(model);
            var modelState = CurrentModelState;

            FlowInstanceSearchValuesBL.CalculateReadValuesByTransferIDAsync(uow, formsetversion.TransferID);

            return SaveResult(modelState, formsetversion);            
        }

        public ActionResult DispatchToForm(int? formTypeID, int? flowFieldID)
        {
            //Geen securitycheck hier, gebeurt pas in form. Meldingen werken anders niet.
            var flowinstance = FlowInstanceBL.GetByTransferID(uow, TransferID);
            if (flowinstance == null)
            {
                return new HttpNotFoundResult();
            }

            PhaseDefinitionCache phaseDefinition = null;
            if (formTypeID.HasValue)
            {
                phaseDefinition = PhaseDefinitionCacheBL.GetByFlowDefinitionAndFormType(uow, flowinstance.FlowDefinitionID, formTypeID.Value);
            }
            else
            {
                if (flowFieldID != null)
                {
                    var attrs = FlowFieldAttributeBL.GetByFlowFieldID(uow, flowFieldID.Value);
                    foreach (var attr in attrs)
                    {
                        phaseDefinition = PhaseDefinitionCacheBL.GetByFlowDefinitionAndFormType(uow, flowinstance.FlowDefinitionID, attr.FormTypeID);
                        if (phaseDefinition != null)
                        {
                            break;
                        }
                    }
                }
            }

            if (phaseDefinition == null)
            {
                return RedirectToAction("DashBoard", "FlowMain", new { TransferID });
            }

            RouteData.Values.Add("TransferID", TransferID);
            RouteData.Values.Add("PhaseDefinitionID", phaseDefinition.PhaseDefinitionID);

            var phaseInstance = PhaseInstanceBL.GetByFlowInstanceIDAndPhaseDefinition(uow, flowinstance.FlowInstanceID, phaseDefinition.PhaseDefinitionID);
            if (phaseInstance == null)
            {
                return Index(phaseDefinition.FormTypeID);
            }

            RouteData.Values.Add("PhaseInstanceID", phaseInstance.PhaseInstanceID);
            var versions = FormSetVersionBL.GetByPhaseInstanceAndFormType(uow, phaseInstance.PhaseInstanceID, phaseDefinition.FormTypeID).ToList();
            if (!versions.Any())
            {
                return Index(phaseDefinition.FormTypeID);
            }

            var firstOrDefault = versions.OrderByDescending(fsv => fsv.FormSetVersionID).FirstOrDefault();
            if (firstOrDefault != null)
            {
                RouteData.Values.Add("FormSetVersionID", firstOrDefault.FormSetVersionID);
            }

            return Index(phaseDefinition.FormTypeID);
        }

        public ActionResult DeleteFormSetVersion(bool? confirmed)
        {
            PageTitle = "Formulier verwijderen";
            PageHeader = "Formulier verwijderen";
            ViewBag.FormSetVersionID = FormSetVersionID;

            if (!confirmed.HasValue)
            {
                return View();
            }

            if (confirmed == false)
            {
                return RedirectToAction("Dashboard", "FlowMain", new { TransferID });
            }

            var logEntry = LogEntryBL.Create(FlowFormType.NoScreen, PointUserInfo());
            var formsetversion = FormSetVersionBL.DeleteByIDAndTransferID(uow, FormSetVersionID, TransferID, logEntry);
            if (formsetversion.PhaseInstance != null && !formsetversion.PhaseInstance.FormSetVersion.Any(fsv => !fsv.Deleted))
            {
                PhaseInstanceBL.Cancel(uow, formsetversion.PhaseInstance, logEntry);
            }

            uow.Save();
            ResetOutputCacheByTransferID(TransferID);

            FlowInstanceSearchValuesBL.CalculateReadValuesByTransferIDAsync(uow, TransferID);

            if (!Request.IsAjaxRequest())
            {
                return RedirectToAction("Dashboard", "FlowMain", new { TransferID });
            }

            Response.StatusCode = (int)(ErrorMessage == "" ? HttpStatusCode.OK : HttpStatusCode.BadRequest);
            return Json(new {ErrorMessage}, JsonRequestBehavior.DenyGet);
        }

        public ActionResult CloseDoorlopenddossier()
        {
            var formsetversion = FormSetVersionBL.GetByID(uow, this.FormSetVersionID);
            if (formsetversion == null) return null;

            var frequency = FrequencyBL.GetByTransferID(uow, this.TransferID);
            var logentry = LogEntryBL.Create(FormType, PointUserInfo());

            FrequencyBL.SetStatus(uow, frequency, FrequencyStatus.Done, logentry);
            PhaseInstanceBL.SetStatusDone(uow, formsetversion.PhaseInstance, logentry);

            FlowInstanceSearchValuesBL.CalculateReadValuesByTransferIDAsync(uow, formsetversion.TransferID);

            return RedirectToAction("DispatchToForm", new { TransferID, formsetversion.FormTypeID });
        }

        public ActionResult ReopenDoorlopendDossier()
        {
            bool success = false;

            var formsetversion = FormSetVersionBL.GetByID(uow, this.FormSetVersionID);
            var frequency = FrequencyBL.GetByTransferID(uow, this.TransferID);
            if (formsetversion != null || frequency != null)
            {
                var logentry = LogEntryBL.Create(formsetversion.FormType, PointUserInfo());

                var phaseinstance = formsetversion.PhaseInstance;
                if (phaseinstance != null)
                {
                    PhaseInstanceBL.SetStatusActive(uow, phaseinstance, logentry);
                    FrequencyBL.SetStatus(uow, frequency, FrequencyStatus.Active, logentry);

                    FlowInstanceSearchValuesBL.CalculateReadValuesByTransferIDAsync(uow, frequency.TransferID);

                    return RedirectToAction("DispatchToForm", new { TransferID, formsetversion.FormTypeID });
                }
            }

            return Json(new { success }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteDoorlopendDossier(bool? confirmed)
        {
            PageTitle = "Formulier verwijderen";
            PageHeader = "Formulier verwijderen";
            ViewBag.FormSetVersionID = FormSetVersionID;

            if (!confirmed.HasValue)
            {
                return View();
            }

            if (confirmed == false)
            {
                return RedirectToAction("Dashboard", "FlowMain", new { TransferID });
            }

            var phaseinstance = PhaseInstanceBL.GetByFormsetVersionID(uow, this.FormSetVersionID);
            if (phaseinstance != null)
            {
                var logentry = LogEntryBL.Create((FlowFormType)this.FormTypeID, PointUserInfo());

                PhaseInstanceBL.Cancel(uow, phaseinstance, logentry); //also deletes formsetversion
                FrequencyBL.CancelByTransferID(uow, TransferID, logentry);

                ResetOutputCacheByTransferID(TransferID);
                FlowInstanceSearchValuesBL.CalculateReadValuesByTransferIDAsync(uow, TransferID);

                return RedirectToAction("Dashboard", "FlowMain", new { TransferID });
            }

            return Json(new { ErrorMessage }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ZorgAdvies(bool asPartial = false, int formSetVersionID = -1)
        {
            return new SecureActionResult(FormTypeViewModelHelper.GetZorgAdviesViewModel(uow, GetGlobalPropertiesFormType(asPartial, getTransfer: true, getPhaseInstance:true)), asPartial);
        }

        [HttpPost]
        public ActionResult ZorgAdvies([ModelBinder(typeof(ZorgAdviesBinder))] ZorgAdviesViewModel model)
        {
            var formSetVersion = SaveFormsetAndValidFields(model.Global.FlowWebFields);
            ZorgAdviesItemValueBL.SaveByViewModelAndFlowInstanceID(uow, model.ZorgAdviesItems, formSetVersion.PhaseInstance.FlowInstanceID);

            var modelState = CurrentModelState;

            FlowInstanceSearchValuesBL.CalculateReadValuesByTransferIDAsync(uow, formSetVersion.TransferID);

            return SaveResult(modelState, formSetVersion);
        }

        public ActionResult MsvtIND()
        {
            return new SecureActionResult(FormTypeViewModelHelper.GetMsvtINDViewModel(uow,GetGlobalPropertiesFormType(getTransfer:true, getNewFormRequestUrl:true), this));
        }

        [HttpPost]
        public ActionResult MsvtIND([ModelBinder(typeof(MsvtFormBinder))] MsvtViewModel model, string indname, bool? renew, bool? digitallysigning)
        {
            var phasedefinition = PhaseDefinitionCacheBL.GetByID(uow, PhaseDefinitionID);
            if (phasedefinition == null)
            {
                throw new Exception("Geen phasedefinition");
            }

            var phaseinstanceid = PhaseInstanceID;
            if (phaseinstanceid > 0)
            {
                var phaseinstance = PhaseInstanceBL.GetById(uow, PhaseInstanceID);
                if (FormSetVersionID <= 0 && phaseinstance.FormSetVersion != null && phaseinstance.FormSetVersion.Any(fsv => !fsv.Deleted))
                {
                    phaseinstanceid = -1; //Genereer ook nieuwe phaseinstance (Zie: Product Backlog Item 9791:Setting a indicatiestelling to definitive sets all indicatiestelling to definitive) 
                }
            }

            var formsetversion = SaveFormsetAndValidFields(model.Global.FlowWebFields, phasedefinition.FormTypeID, phaseinstanceid, description: indname);

            var pointuserinfo = PointUserInfo();
            ActionHealthInsurerBL.SaveByViewModelAndFormSetVersionID(uow, model.TreatmentItems, formsetversion.FormSetVersionID, TransferID, pointuserinfo.Employee.EmployeeID);
            MedicineCardBL.SaveByViewModelAndFormSetVersionID(uow, model.MedicationItems, formsetversion.FormSetVersionID, TransferID, pointuserinfo.Employee.EmployeeID);

            var logentry = LogEntryBL.Create((FlowFormType)phasedefinition.FormTypeID, pointuserinfo);
            if (renew.GetValueOrDefault(false))
            {
                var oldphaseinstance = PhaseInstanceBL.GetById(uow, PhaseInstanceID);
                var phaseinstance = PhaseInstanceBL.Create(uow, oldphaseinstance.FlowInstanceID, PhaseDefinitionID, logentry);
                var destinationFormSetVersion = FormSetVersionBL.CreateFormSetVersion(uow, TransferID, phasedefinition.FormTypeID, phaseinstance.PhaseInstanceID, pointuserinfo, logentry, formsetversion.Description + " (verlenging)");

                FlowFieldValueBL.CopyTo(uow, formsetversion, destinationFormSetVersion);
                ActionHealthInsurerBL.CopyTo(uow, formsetversion, destinationFormSetVersion);
                MedicineCardBL.CopyTo(uow, formsetversion, destinationFormSetVersion);

                FlowInstanceSearchValuesBL.CalculateReadValuesByTransferIDAsync(uow, destinationFormSetVersion.TransferID);

                if (SignaleringTrigger != null)
                {
                    SignaleringBL.HandleSignalByFlowDefinitionFormTypeAction(uow, FlowInstance, (int)FlowFormType.MSVTIndicatiestelling, "Renew", pointuserinfo, "MSVT Indicatie: '" + formsetversion.Description + "' is verlengd");
                }

                return SaveResult(CurrentModelState, destinationFormSetVersion);
            }

            if (DefinitiveHandled && digitallysigning.GetValueOrDefault(false))
            {
                SignedPhaseInstanceBL.SignPhaseInstance(uow, PhaseInstanceID, pointuserinfo.Employee.EmployeeID, logentry);
                TransferAttachmentBL.InsertFormAsPDFAndSave(formsetversion.TransferID, formsetversion.FormSetVersionID, "Digitaal ondertekend MSVT", pointuserinfo, logentry);
            }

            FlowInstanceSearchValuesBL.CalculateReadValuesByTransferIDAsync(uow, formsetversion.TransferID);

            var modelState = CurrentModelState;

            return SaveResult(modelState, formsetversion);
        }

        public ActionResult MsvtUVV()
        {
            return new SecureActionResult(FormTypeViewModelHelper.GetMsvtViewModel(uow, GetGlobalPropertiesFormType(getTransfer:true, getNewFormRequestUrl:true)));
        }

        [HttpPost]
        public ActionResult MsvtUVV([ModelBinder(typeof(MsvtFormBinder))] MsvtViewModel model, string uvvname, bool? renew)
        {
            var phasedefinition = PhaseDefinitionCacheBL.GetByID(uow, PhaseDefinitionID);
            if (phasedefinition == null)
            {
                throw new Exception("Geen phasedefinition");
            }

            var phaseinstanceid = PhaseInstanceID;
            if (phaseinstanceid > 0)
            {
                var phaseinstance = PhaseInstanceBL.GetById(uow, PhaseInstanceID);
                if (FormSetVersionID <= 0 && phaseinstance.FormSetVersion != null && phaseinstance.FormSetVersion.Any(fsv => !fsv.Deleted))
                {
                    phaseinstanceid = -1; //Genereer ook nieuwe phaseinstance (Zie: Product Backlog Item 9791:Setting a indicatiestelling to definitive sets all indicatiestelling to definitive) 
                }
            }

            var formsetversion = SaveFormsetAndValidFields(model.Global.FlowWebFields, phasedefinition.FormTypeID, phaseinstanceid, description: uvvname);

            var pointuserinfo = PointUserInfo();
            ActionHealthInsurerBL.SaveByViewModelAndFormSetVersionID(uow, model.TreatmentItems, formsetversion.FormSetVersionID, TransferID, pointuserinfo.Employee.EmployeeID);
            MedicineCardBL.SaveByViewModelAndFormSetVersionID(uow, model.MedicationItems, formsetversion.FormSetVersionID, TransferID, pointuserinfo.Employee.EmployeeID);

            if (renew.GetValueOrDefault(false))
            {
                var userinfo = PointUserInfo();
                var logentry = LogEntryBL.Create((FlowFormType)phasedefinition.FormTypeID, userinfo);

                var oldphaseinstance = PhaseInstanceBL.GetById(uow, PhaseInstanceID);
                var phaseinstance = PhaseInstanceBL.Create(uow, oldphaseinstance.FlowInstanceID, PhaseDefinitionID, logentry);
                var destinationFormSetVersion = FormSetVersionBL.CreateFormSetVersion(uow, TransferID, phasedefinition.FormTypeID, phaseinstance.PhaseInstanceID, userinfo, logentry, formsetversion.Description + " (verlenging)");

                FlowFieldValueBL.CopyTo(uow, formsetversion, destinationFormSetVersion);
                ActionHealthInsurerBL.CopyTo(uow, formsetversion, destinationFormSetVersion);
                MedicineCardBL.CopyTo(uow, formsetversion, destinationFormSetVersion);

                FlowInstanceSearchValuesBL.CalculateReadValuesByTransferIDAsync(uow, formsetversion.TransferID);

                return SaveResult(CurrentModelState, destinationFormSetVersion);
            }

            FlowInstanceSearchValuesBL.CalculateReadValuesByTransferIDAsync(uow, formsetversion.TransferID);

            return SaveResult(CurrentModelState, formsetversion);
        }

        public ActionResult GVP()
        {
            return new SecureActionResult(FormTypeViewModelHelper.GetGVPViewModel(uow, GetGlobalPropertiesFormType(getTransfer:true, getFlowInstance:true, getNewFormRequestUrl:true)));
        }

        [HttpPost]
        public ActionResult GVP([ModelBinder(typeof(GVPFormBinder))] GVPViewModel model, string gvpname, bool? renew, bool? digitallysigning)
        {
            var phasedefinition = PhaseDefinitionCacheBL.GetByID(uow, PhaseDefinitionID);
            if (phasedefinition == null)
            {
                throw new Exception("Geen phasedefinition");
            }

            var phaseinstanceid = PhaseInstanceID;
            if (phaseinstanceid > 0)
            {
                var phaseinstance = PhaseInstanceBL.GetById(uow, PhaseInstanceID);
                if (FormSetVersionID <= 0 && phaseinstance.FormSetVersion != null && phaseinstance.FormSetVersion.Any(fsv => !fsv.Deleted))
                {
                    phaseinstanceid = -1; //Genereer ook nieuwe phaseinstance (Zie: Product Backlog Item 9791:Setting a indicatiestelling to definitive sets all indicatiestelling to definitive) 
                }
            }

            var formsetversion = SaveFormsetAndValidFields(model.Global.FlowWebFields, phasedefinition.FormTypeID, phaseinstanceid, description: gvpname);

            var pointuserinfo = PointUserInfo();
            var logentry = LogEntryBL.Create((FlowFormType)phasedefinition.FormTypeID, pointuserinfo);

            ActionHealthInsurerBL.SaveByViewModelAndFormSetVersionID(uow, model.GVPHandelingItems, formsetversion.FormSetVersionID, TransferID, pointuserinfo.Employee.EmployeeID);
            MedicineCardBL.SaveByViewModelAndFormSetVersionID(uow, model.MedicationItems, formsetversion.FormSetVersionID, TransferID, pointuserinfo.Employee.EmployeeID);

            if (renew.GetValueOrDefault(false))
            {
                var oldphaseinstance = PhaseInstanceBL.GetById(uow, PhaseInstanceID);
                var phaseinstance = PhaseInstanceBL.Create(uow, oldphaseinstance.FlowInstanceID, PhaseDefinitionID, logentry);

                var newdescription = "";
                var numberstring = formsetversion.Description.Split(' ').LastOrDefault();
                if (!String.IsNullOrEmpty(numberstring))
                {
                    var length = numberstring.Length;
                    var oldname = formsetversion.Description.Substring(0, formsetversion.Description.Length - length);
                    var oldformsetsets = FormSetVersionBL.GetByTransferIDAndFormTypeID(uow, TransferID, (int)FlowFormType.GVp);

                    newdescription = oldname + (oldformsetsets.Count() + 1);
                }

                var destinationFormSetVersion = FormSetVersionBL.CreateFormSetVersion(uow, TransferID, phasedefinition.FormTypeID, phaseinstance.PhaseInstanceID, pointuserinfo, logentry, newdescription);

                FlowFieldValueBL.CopyTo(uow, formsetversion, destinationFormSetVersion);
                ActionHealthInsurerBL.CopyTo(uow, formsetversion, destinationFormSetVersion);
                MedicineCardBL.CopyTo(uow, formsetversion, destinationFormSetVersion);

                FlowInstanceSearchValuesBL.CalculateReadValuesByTransferIDAsync(uow, formsetversion.TransferID);

                return SaveResult(CurrentModelState, destinationFormSetVersion);
            }

            if (DefinitiveHandled && digitallysigning.GetValueOrDefault(false))
            {
                SignedPhaseInstanceBL.SignPhaseInstance(uow, PhaseInstanceID, pointuserinfo.Employee.EmployeeID, logentry);
                TransferAttachmentBL.InsertFormAsPDFAndSave(formsetversion.TransferID, formsetversion.FormSetVersionID, "Digitaal ondertekend GVp", pointuserinfo, logentry);
            }

            FlowInstanceSearchValuesBL.CalculateReadValuesByTransferIDAsync(uow, formsetversion.TransferID);

            return SaveResult(CurrentModelState, formsetversion);
        }

        public ActionResult RequestFormZHZH()
        {
            return new SecureActionResult(FormTypeViewModelHelper.GetRequestFormZHZHViewModel(uow, GetGlobalPropertiesFormType()));
        }

        [HttpPost]
        public ActionResult RequestFormZHZH([ModelBinder(typeof(FlowWebFieldBinder))] List<FlowWebField> model)
        {
            var flowwebfields = new List<FlowWebField>(model);
            var formSetVersion = SaveFormsetAndValidFields(flowwebfields);

            var modelState = CurrentModelState;

            FlowInstanceSearchValuesBL.CalculateReadValuesByTransferIDAsync(uow, formSetVersion.TransferID);

            int? nextphasedefinitionid = null;
            if (NextPhaseHandled)
            {
                nextphasedefinitionid = System.Web.HttpContext.Current.Request.GetRequestVariable<int>("nextPhaseDefinitionID", -1);
            }

            return SaveResult(modelState, formSetVersion, nextphasedefinitionid);
        }

        public ActionResult RequestFormHAVVT()
        {
            return new SecureActionResult(FormTypeViewModelHelper.GetRequestFormHAVVTViewModel(uow, GetGlobalPropertiesFormType()));
        }

        [HttpPost]
        public ActionResult RequestFormHAVVT([ModelBinder(typeof(FlowWebFieldBinder))] List<FlowWebField> model)
        {
            var flowwebfields = new List<FlowWebField>(model);

            var formSetVersion = SaveFormsetAndValidFields(flowwebfields);
            var modelState = CurrentModelState;
            var pointuserinfo = PointUserInfo();
            var logentry = LogEntryBL.Create(FlowFormType.AanvraagFormulierHAVVT, pointuserinfo);

            if (ModelState.IsValid)
            {
                var transferroutephasedefinition = PhaseDefinitionCacheBL.GetByFlowDefinitionAndFormType(uow, FlowInstance.FlowDefinitionID, (int)FlowFormType.TransferRouteHAVVT);
                var transferrouteinvite = OrganizationInviteBL.GetAcceptedOrActiveInvitesByFlowInstanceIDAndPhaseDefinitionID(uow, FlowInstance.FlowInstanceID, transferroutephasedefinition?.PhaseDefinitionID ?? 0).FirstOrDefault();

                int? previousdepartmentid = transferrouteinvite?.DepartmentID;
                int? recodepartmentid = int.TryParse(flowwebfields.GetValueOrDefault(FlowFieldConstants.Name_AanvraagVersturenDoorTussenOrganisatie, ""), out int tempdepid) ? tempdepid : (int?)null;
                if (recodepartmentid.HasValue && recodepartmentid != previousdepartmentid)
                {
                    var recievingphasedefinition = PhaseDefinitionCacheBL.GetByFlowDefinitionAndFormType(uow, FlowInstance.FlowDefinitionID, (int)FlowFormType.TransferRouteHAVVT);
                    if (recievingphasedefinition != null)
                    {
                        var transferrouteurl = Url.Action("DispatchToForm", "FormType", new { TransferID, FormTypeID = (int)FlowFormType.TransferRouteHAVVT }, Utils.UriScheme());

                        OrganizationHelper.CancelInvite(uow, transferrouteinvite, pointuserinfo);

                        OrganizationHelper.Invite(uow, 
                            FlowInstance.FlowInstanceID, 
                            PhaseDefinitionID, recievingphasedefinition.PhaseDefinitionID,
                            recodepartmentid.Value, false, InviteType.Mediating, logentry, transferrouteurl);

                        OrganizationHelper.SetDestinationField(uow, FlowInstance.FlowInstanceID, recodepartmentid.Value);
                    }
                }
                else if (recodepartmentid == null && previousdepartmentid.HasValue && transferrouteinvite != null)
                {
                    OrganizationInviteBL.SetStatus(uow, transferrouteinvite, InviteStatus.Cancelled);
                }

            }

            FlowInstanceSearchValuesBL.CalculateReadValuesByTransferIDAsync(uow, formSetVersion.TransferID);

            int? nextphasedefinitionid = null;
            if (!NextPhaseHandled)
            {
                nextphasedefinitionid = System.Web.HttpContext.Current.Request.GetRequestVariable<int?>("nextPhaseDefinitionID", null);
                if (nextphasedefinitionid != null)
                {
                    NextPhaseHandled = true;
                }
            }

            return SaveResult(modelState, formSetVersion, nextphasedefinitionid);
        }

        public ActionResult RequestFormZHVVT()
        {
            return new SecureActionResult(FormTypeViewModelHelper.GetRequestFormZHVVTViewModel(uow, GetGlobalPropertiesFormType(getPhaseInstance:true)));
        }

        [HttpPost]
        public ActionResult RequestFormZHVVT([ModelBinder(typeof(FlowWebFieldBinder))] List<FlowWebField> model)
        {
            var flowwebfields = new List<FlowWebField>(model);

            var flowinstance = FlowInstanceBL.GetByTransferID(uow, TransferID);
            if (flowinstance.FlowDefinitionID == FlowDefinitionID.ZH_VVT)
            {
                var formZhVvtType = flowwebfields.FirstOrDefault(m => m.Name == FlowFieldConstants.Name_RequestFormZHVVTType);
                if (formZhVvtType != null && string.IsNullOrEmpty(formZhVvtType.Value))
                {
                    return Json(new
                    {
                        ValidationErrors = new List<string> {"Geen aanvraagtype geselecteerd"},
                        ShowErrors = true
                    });
                }
            }

            var datumEindeBehandelingMedischSpecialist = model.FirstOrDefault(fwf => fwf.FlowFieldID == FlowFieldConstants.ID_DatumEindeBehandelingMedischSpecialist);
            var datumEindeBehandelingMedischSpecialistIsFilled = !string.IsNullOrWhiteSpace(datumEindeBehandelingMedischSpecialist?.Value);

            if (datumEindeBehandelingMedischSpecialistIsFilled)
            {
                var oorspronkelijkeDatum = flowwebfields.FirstOrDefault(x => x.FlowFieldID == FlowFieldConstants.ID_OorspronkelijkeDatumEindeBehandelingMedischSpecialist);
                if (oorspronkelijkeDatum != null && string.IsNullOrEmpty(oorspronkelijkeDatum.Value))
                {
                    FlowWebFieldBL.SetValue(flowwebfields, FlowFieldConstants.ID_OorspronkelijkeDatumEindeBehandelingMedischSpecialist, datumEindeBehandelingMedischSpecialist.Value);
                }
            }
            
            if (FlowInstance.FlowDefinitionID == FlowDefinitionID.RzTP)
            {
                if (datumEindeBehandelingMedischSpecialistIsFilled)
                {
                    var realizedDischargeDate = FlowWebFieldBL.GetFieldsFromFormTypeID(uow, (int)FlowFormType.AanvraagFormulierZHVVT, PhaseDefinitionID, TransferID, ViewData).FirstOrDefault(fwf => fwf.Name == FlowFieldConstants.Name_RealizedDischargeDate);
                    if (realizedDischargeDate != null)
                    {
                        realizedDischargeDate.Value = datumEindeBehandelingMedischSpecialist.Value;
                        flowwebfields.Add(realizedDischargeDate);
                    }
                }
            }

            var formSetVersion = SaveFormsetAndValidFields(flowwebfields);

            if (flowinstance.FlowDefinitionID == FlowDefinitionID.CVA)
            {
                var pointuserinfo = PointUserInfo();
                var logentry = LogEntryBL.Create(FormType, pointuserinfo);
                if (FormSetVersionID == -1)
                {
                    FlowFieldValueBL.UpdateOrInsert(uow, FlowInstance.FlowInstanceID, formSetVersion.FormSetVersionID,
                        FlowFieldConstants.ID_RequestFormZHVVTType, FlowFormType.RequestFormZHVVT_FormulierStandaard.ToString("D")
                        , logentry);
                }

                FlowWebFieldBL.SyncValue(uow, pointuserinfo, TransferID, formSetVersion.FormSetVersionID,
                    FlowFormType.AanvraagFormulierZHVVT, FlowFieldConstants.Name_MedischeSituatieDatumOpname,
                    FlowFormType.InvullenCVARegistratieZH, FlowFieldConstants.Name_PatientWordtOpgenomenDatum,
                    logentry, ViewData);
            }

            if(model.HasValue(FlowFieldConstants.Name_DossierOwner))
            {
                var startedbyidstring = model.GetFlowFieldValue(FlowFieldConstants.ID_DossierOwner);
                flowinstance.StartedByEmployeeID = int.TryParse(startedbyidstring, out int intvalue) ? intvalue : flowinstance.StartedByEmployeeID;
                uow.Save();
            }

            var modelState = CurrentModelState;

            FlowInstanceSearchValuesBL.CalculateReadValuesByTransferIDAsync(uow, formSetVersion.TransferID);

            int? nextphasedefinitionid = null;
            if (NextPhaseHandled)
            {
                nextphasedefinitionid = System.Web.HttpContext.Current.Request.GetRequestVariable<int>("nextPhaseDefinitionID", -1);
            }

            return SaveResult(modelState, formSetVersion, nextphasedefinitionid);
        }

        public ActionResult SendToHospital()
        {
            var viewModel = FormTypeViewModelHelper.GetSendToHospitalViewModel(uow, GetGlobalPropertiesFormType(getPhaseInstance:true));
            if (viewModel.HasErrors)
            {
                return Content($"<!-- {viewModel.ErrorMessage} -->");
            }

            return new SecureActionResult(viewModel);
        }

        [HttpPost]
        public ActionResult SendToHospital([ModelBinder(typeof(FlowWebFieldBinder))] List<FlowWebField> model)
        {
            var formSetVersion = SaveFormsetAndValidFields(model);
            var modelState = CurrentModelState;

            if (modelState.IsValid && NextPhaseHandled)
            {
                var currentPhaseInstance = PhaseInstanceBL.GetById(uow, PhaseInstanceID);
                var nextphasedefinitions = PhaseDefinitionBL.GetNextPhaseDefinitions(uow, currentPhaseInstance.PhaseDefinitionID);
                var nextphasedefinitionid = nextphasedefinitions.FirstOrDefault().PhaseDefinitionID;
                var nextPhaseInstance = PhaseInstanceBL.GetByFlowInstanceIDAndPhaseDefinition(uow, currentPhaseInstance.FlowInstanceID, nextphasedefinitionid);

                var destinationhospitallocation = model.FirstOrDefault(fwf => fwf.Name == FlowFieldConstants.Name_DestinationHospitalLocation);
                var destinationHospitalDepartment = model.FirstOrDefault(fwf => fwf.Name == FlowFieldConstants.Name_DestinationHospitalDepartment);

                if (!string.IsNullOrEmpty(destinationhospitallocation?.Value) || !string.IsNullOrEmpty(destinationHospitalDepartment?.Value))
                {
                    bool hasspecificdepartment = !string.IsNullOrEmpty(destinationHospitalDepartment?.Value);

                    Department department = null;
                    if (!string.IsNullOrEmpty(destinationhospitallocation?.Value))
                    {
                        var locationid = int.Parse(destinationhospitallocation.Value);
                        department = DepartmentBL.GetTransferPointByLocation(uow, locationid);
                        if (department == null)
                        {
                            throw new Exception("Er is geen transferpunt gekoppeld aan locatie met ID(" + locationid + ")");
                        }
                    }

                    if (hasspecificdepartment)
                    {
                        department = DepartmentBL.GetByDepartmentID(uow, Convert.ToInt32(destinationHospitalDepartment.Value));
                        if (department == null)
                        {
                            throw new Exception("Er is geen afdeling met ID(" + destinationHospitalDepartment.Value + ")");
                        }
                    }

                    var userinfo = PointUserInfo();
                    var logentry = LogEntryBL.Create(FlowFormType.VersturenNaarZH, userinfo);

                    var previousinvites = OrganizationInviteBL.GetAcceptedOrActiveInvitesByFlowInstanceIDAndPhaseDefinitionID(uow, FlowInstance.FlowInstanceID, nextphasedefinitionid);
                    foreach (var previousinvite in previousinvites)
                    {
                        OrganizationHelper.CancelInvite(uow, previousinvite, userinfo);
                        SignaleringBL.ExpireSignaleringByFlowInstanceIDAndDepartmentID(uow, FlowInstance.FlowInstanceID, previousinvite.DepartmentID);
                    }

                    OrganizationHelper.Invite(uow, FlowInstance.FlowInstanceID, currentPhaseInstance.PhaseDefinitionID, nextphasedefinitionid, department.DepartmentID, false, InviteType.Regular, logentry, dashboardUrl(Url));
                    OrganizationHelper.SetDestinationField(uow, FlowInstance.FlowInstanceID, department.DepartmentID);
                    uow.Save();

                    OrganizationHelper.CheckAndSendVerpleegkundigeOverdracht(uow, FlowInstance, userinfo, logentry, false);
                    OrganizationHelper.CheckAndSendSurvey(uow, FlowInstance, userinfo);

                    if (hasspecificdepartment == false)
                    {
                        SignaleringBL.HandleSignalByFlowDefinitionFormTypeAction(uow, FlowInstance, FormTypeID, "NewZH", userinfo, "Locatie: '" + department.Location.Name + "'");
                    }
                    else
                    {
                        SignaleringBL.HandleSignalByFlowDefinitionFormTypeAction(uow, FlowInstance, FormTypeID, "NewZHDepartment", userinfo, "Afdeling: '" + department.Name + "'");
                    }

                    if (FlowInstance.FlowDefinition.FlowTypeID == (int)FlowTypeID.VVT_ZH || FlowInstance.FlowDefinition.FlowTypeID == (int)FlowTypeID.ZH_ZH)
                    {
                        var voformsetversion = FormSetVersionBL.GetByTransferIDAndFormTypeIDs(uow, TransferID, new int[] { (int)FlowFormType.VerpleegkundigeOverdrachtFlow, (int)FlowFormType.EOverdracht30 }).FirstOrDefault();
                        if (voformsetversion == null && FlowInstance.FlowDefinition.FlowTypeID == (int)FlowTypeID.VVT_ZH)
                        {
                            throw new PointJustLogException("Er is geen EOverdracht bekend in dit dossier");
                        }

                        if (voformsetversion != null)
                        {
                            ServiceBusBL.SendMessage("PhaseQueue", "FormSetVersionNotification",
                                new FormSetVersionNotification
                                {
                                    TimeStamp = DateTime.Now,
                                    UniqueID = DateTime.Now.Ticks,
                                    TransferID = TransferID,
                                    FormSetVersionID = voformsetversion.FormSetVersionID,
                                    RecievingDepartmentID = department.DepartmentID,
                                    RecievingOrganizationID = department.Location.OrganizationID,
                                    SendingEmployeeID = userinfo.Employee.EmployeeID,
                                    Url = Url.Action("DashBoard", "FlowMain", new { TransferID }, Utils.UriScheme()),
                                    IsVO = true
                                }
                            );
                        }
                    }
                }
            }

            FlowInstanceSearchValuesBL.CalculateReadValuesByTransferIDAsync(uow, formSetVersion.TransferID);

            return SaveResult(modelState, formSetVersion);
        }

        [HttpPost]
        public ActionResult SendToOtherHospital([ModelBinder(typeof(FlowWebFieldBinder))] List<FlowWebField> model)
        {
            var errormessage = "";

            var destinationhospitallocation = model.FirstOrDefault(fwf => fwf.Name == FlowFieldConstants.Name_DestinationHospitalLocation);
            var destinationHospitalDepartment = model.FirstOrDefault(fwf => fwf.Name == FlowFieldConstants.Name_DestinationHospitalDepartment);

            var pointUserInfo = PointUserInfo();
            var phaseInstance = PhaseInstanceBL.GetById(uow, PhaseInstanceID);

            var locationid = destinationhospitallocation != null ? int.Parse(destinationhospitallocation.Value) : 0;
            var departmentid = destinationHospitalDepartment != null ? int.Parse(destinationHospitalDepartment.Value) : 0;

            bool hasspecificdepartment = departmentid > 0;

            var department = hasspecificdepartment ? DepartmentBL.GetByDepartmentID(uow, departmentid) : DepartmentBL.GetTransferPointByLocation(uow, locationid);

            if (department == null)
            {
                throw new PointJustLogException("Er is geen afdeling gekoppeld aan locatie met ID(" + locationid + ")");
            }

            var formType = FormTypeBL.GetByFormTypeID(uow, (int)FlowFormType.VersturenNaarZH);
            var logentry = LogEntryBL.Create(formType, pointUserInfo);

            var organizationID = department.Location.OrganizationID;

            var previousdepartment = OrganizationHelper.GetRecievingDepartment(uow, phaseInstance.FlowInstanceID);
            if (previousdepartment != null)
            {
                SignaleringBL.ExpireSignaleringByFlowInstanceIDAndDepartmentID(uow, phaseInstance.FlowInstanceID, previousdepartment.DepartmentID);
            }

            PhaseInstanceBL.CancelByCodeGroup(uow, TransferID, "VVTTP", logentry);
            PhaseInstanceBL.CancelByCodeGroup(uow, TransferID, "SENDTODEP", logentry);
            PhaseInstanceBL.CancelByCodeGroup(uow, TransferID, "CLOSE", logentry);

            var sendToHospitalTimestamp = model.FirstOrDefault(fwf => fwf.Name == FlowFieldConstants.Name_SendToHospitalTimestamp);
            if (sendToHospitalTimestamp != null)
            {
                FlowWebFieldBL.SetValue(sendToHospitalTimestamp, DateTime.Now);
            }

            SaveFormsetAndValidFields(model, formType.FormTypeID, phaseInstance.PhaseInstanceID);

            var nextPhaseDefinition = PhaseDefinitionBL.GetNextPhaseDefinitions(uow, phaseInstance.PhaseDefinitionID).FirstOrDefault();
            if (nextPhaseDefinition != null)
            {
                OrganizationHelper.CancelInvite(uow, FlowInstance.FlowInstanceID, nextPhaseDefinition.PhaseDefinitionID, pointUserInfo);
                OrganizationHelper.Invite(uow, FlowInstance.FlowInstanceID, phaseInstance.PhaseDefinitionID, nextPhaseDefinition.PhaseDefinitionID, department.DepartmentID, false, InviteType.Regular, logentry, dashboardUrl(Url));
                PhaseInstanceBL.Create(uow, FlowInstance.FlowInstanceID, nextPhaseDefinition.PhaseDefinitionID, logentry);
                OrganizationHelper.SetDestinationField(uow, FlowInstance.FlowInstanceID, department.DepartmentID);
            }

            if (hasspecificdepartment == false)
            {
                SignaleringBL.HandleSignalByFlowDefinitionFormTypeAction(uow, FlowInstance, formType.FormTypeID, "NewZH", pointUserInfo, "Locatie: '" + department.Location.Name + "'");
            }
            else
            {
                SignaleringBL.HandleSignalByFlowDefinitionFormTypeAction(uow, FlowInstance, formType.FormTypeID, "NewZHDepartment", pointUserInfo, "Afdeling: '" + department.Name + "'");
            }

            //Pickup the formsetversion for the Eoverdracht
            if (FlowInstance.FlowDefinition.FlowTypeID == (int)FlowTypeID.VVT_ZH || FlowInstance.FlowDefinition.FlowTypeID == (int)FlowTypeID.ZH_ZH)
            {
                var voformsetversion = FormSetVersionBL.GetByTransferIDAndFormTypeID(uow, TransferID, (int)FlowFormType.VerpleegkundigeOverdrachtFlow).FirstOrDefault();
                //Alleen exception voor VVTZH VO zit daar in de flow ipv los formulier
                if (voformsetversion == null && FlowInstance.FlowDefinition.FlowTypeID == (int)FlowTypeID.VVT_ZH)
                {
                    throw new PointJustLogException("Er is geen EOverdracht bekend in dit dossier");
                }

                if (voformsetversion != null)
                {
                    ServiceBusBL.SendMessage(uow, MessageQueues.PhaseQueue, "FormSetVersionNotification",
                        new FormSetVersionNotification
                        {
                            TimeStamp = DateTime.Now,
                            UniqueID = DateTime.Now.Ticks,
                            TransferID = TransferID,
                            FormSetVersionID = voformsetversion.FormSetVersionID,
                            RecievingDepartmentID = department.DepartmentID,
                            RecievingOrganizationID = department.Location.OrganizationID,
                            SendingEmployeeID = pointUserInfo.Employee.EmployeeID,
                            Url = Url.Action("DashBoard", "FlowMain", new { TransferID }, Utils.UriScheme()),
                            IsVO = true
                        }
                    );
                }
            }

            ResetOutputCacheByTransferID(TransferID);
            FlowInstanceSearchValuesBL.CalculateReadValuesByTransferIDAsync(uow, TransferID);

            if (string.IsNullOrWhiteSpace(errormessage))
            {
                return Json(new { Success = true, Url = Url.Action("Dashboard", "FlowMain", new { TransferID }) }, JsonRequestBehavior.AllowGet);
            }
            return Json(new {Success = false, ErrorMessage = errormessage}, JsonRequestBehavior.AllowGet);
        }

        public ActionResult MoveToDepartment()
        {
            return new SecureActionResult(FormTypeViewModelHelper.GetMoveToDepartmentViewModel(uow, GetGlobalPropertiesFormType()));
        }

        [HttpPost]
        public ActionResult MoveToDepartment([ModelBinder(typeof(FlowWebFieldBinder))] List<FlowWebField> model)
        {
            var formtype = FormTypeBL.GetByURL(uow, ControllerName, ActionName);
            var formtypeid = formtype.FormTypeID;

            var pointuserinfo = PointUserInfo();
            var logentry = LogEntryBL.Create(formtype, pointuserinfo);

            var flowfieldattributeexceptions = FlowFieldAttributeExceptionBL.GetByFormTypeIDPhaseDefinitionIDAndRegioIDAndOrganizationID(
                uow, formtypeid, PhaseDefinitionID, pointuserinfo.Region.RegionID, pointuserinfo.Organization.OrganizationID).ToList();

            var formSetVersion = SaveFormsetAndValidFields(model);
            var modelState = CurrentModelState;

            var phaseinstance = formSetVersion.PhaseInstance;
            var newdepartment = model.FirstOrDefault(it => it.Name == "DestinationHospitalDepartment" && !string.IsNullOrEmpty(it.Value));
            if (newdepartment != null)
            {
                //Om er voor te zorgen dat deze fase niet gecancelled word.
                var currentinvites = OrganizationInviteBL.GetByFlowInstanceIDAndPhaseDefinitionID(uow, FlowInstance.FlowInstanceID, phaseinstance.PhaseDefinitionID).ToList();
                foreach (var invite in currentinvites)
                {
                    OrganizationHelper.CancelInvite(uow, invite, pointuserinfo);
                    SignaleringBL.ExpireSignaleringByFlowInstanceIDAndDepartmentID(uow, invite.FlowInstanceID, invite.DepartmentID);
                }

                var department = DepartmentBL.GetByDepartmentID(uow, Convert.ToInt32(newdepartment.Value));
                OrganizationHelper.Invite(
                    uow, FlowInstance.FlowInstanceID, phaseinstance.PhaseDefinitionID, phaseinstance.PhaseDefinitionID,
                    department.DepartmentID, false, InviteType.Regular, logentry, dashboardUrl(Url));

                PhaseInstanceBL.Create(uow, FlowInstance.FlowInstanceID, phaseinstance.PhaseDefinitionID, logentry);

                uow.Save();

                SignaleringBL.HandleSignalByFlowDefinitionFormTypeAction(uow, FlowInstance, (int)FlowFormType.DoorsturenNaarAfd, "NewZHDepartment", pointuserinfo, "Afdeling: '" + department.Name + "'");
            }

            var verpleegkundigeoverdrachtbo = new VerpleegkundigeOverdrachtBO(uow, FlowInstance, pointuserinfo);
            verpleegkundigeoverdrachtbo.Send(false, CommunicationLogType.RequestToHospitalDepartment);

            FlowInstanceSearchValuesBL.CalculateReadValuesByTransferIDAsync(uow, formSetVersion.TransferID);

            return SaveResult(modelState, formSetVersion);
        }

        public SecureActionResult AcceptAtHospital()
        {
            return new SecureActionResult(FormTypeViewModelHelper.GetAcceptAtHospitalViewModel(uow, GetGlobalPropertiesFormType()));
        }

        [HttpPost]
        public ActionResult AcceptAtHospital([ModelBinder(typeof(FlowWebFieldBinder))] List<FlowWebField> model)
        {
            if (NextPhaseHandled)
            {
                var acceptedDateTP = model.FirstOrDefault(fwf => fwf.Name == FlowFieldConstants.Name_AcceptedDateTP);
                if (acceptedDateTP != null)
                {
                    FlowWebFieldBL.SetValue(acceptedDateTP, DateTime.Now);
                }
            }

            var formSetVersion = SaveFormsetAndValidFields(model);

            if (formSetVersion == null) return null;

            FlowInstanceSearchValuesBL.CalculateReadValuesByTransferIDAsync(uow, formSetVersion.TransferID);

            return SaveResult(CurrentModelState, formSetVersion);
        }

        public ActionResult AdditionalInfoRequestTP()
        {
            return new SecureActionResult(FormTypeViewModelHelper.GetAdditionalInfoRequestTpViewModel(uow, GetGlobalPropertiesFormType()));
        }

        [HttpPost]
        public ActionResult AdditionalInfoRequestTP([ModelBinder(typeof(FlowWebFieldBinder))] List<FlowWebField> model)
        {
            var formSetVersion = SaveFormsetAndValidFields(model);
            var modelState = CurrentModelState;

            FlowInstanceSearchValuesBL.CalculateReadValuesByTransferIDAsync(uow, formSetVersion.TransferID);

            return SaveResult(modelState, formSetVersion);
        }

        public ActionResult TransferRouteHAVVT()
        {
            return new SecureActionResult(FormTypeViewModelHelper.GetTransferRouteHAVVTViewModel(uow, GetGlobalPropertiesFormType()));
        }

        public ActionResult TransferRoute()
        {
            var viewModel = FormTypeViewModelHelper.GetTransferRouteViewModel(uow, Url, GetGlobalPropertiesFormType());
            if (viewModel.HasErrors)
            {
                return Content($"<!-- {viewModel.ErrorMessage} -->");
            }

            return new SecureActionResult(viewModel);
        }

        [HttpPost]
        public JsonResult TransferRouteInvites(int transferID, int flowInstanceID, FlowDefinitionID flowDefinitionID)
        {
            var viewModel = FormTypeViewModelHelper.GetTransferRouteInvitesViewModel(uow, Url, transferID, flowInstanceID, flowDefinitionID);
            var transferRouteInvitesHtml = this.RenderPartialViewToString("TransferRouteInvites", viewModel);
            return Json(new
            {
                htmlReplaces = new Dictionary<string, string>
                {
                    {"#transferRouteInvitesPanel", transferRouteInvitesHtml }
                }
            });
        }

        [HttpPost]
        public ActionResult TransferRoute([ModelBinder(typeof(FlowWebFieldBinder))] List<FlowWebField> model)
        {
            var formSetVersion = SaveFormsetAndValidFields(model);
            var modelState = CurrentModelState;

            FlowInstanceSearchValuesBL.CalculateReadValuesByTransferIDAsync(uow, formSetVersion.TransferID);

            return SaveResult(modelState, formSetVersion);
        }

        public ActionResult TransferRouteRzTP()
        {
            var viewModel = FormTypeViewModelHelper.GetTransferRouteRzTPViewModel(uow, GetGlobalPropertiesFormType(getPhaseInstance:true));
            if (viewModel.HasErrors)
            {
                return Content($"<!-- {viewModel.ErrorMessage} -->");
            }

            return new SecureActionResult(viewModel);
        }

        [HttpPost]
        public ActionResult TransferRouteRzTP([ModelBinder(typeof(FlowWebFieldBinder))] List<FlowWebField> model)
        {
            var formSetVersion = SaveFormsetAndValidFields(model);
            var modelState = CurrentModelState;

            FlowInstanceSearchValuesBL.CalculateReadValuesByTransferIDAsync(uow, formSetVersion.TransferID);

            return SaveResult(modelState, formSetVersion);
        }

        [HttpPost]
        public ActionResult TransferRouteSendToOrganization([ModelBinder(typeof(FlowWebFieldBinder))] List<FlowWebField> model, int flowInstanceID, int nextPhaseDefinitionID,
            int[] departmentIDs = null, int formtypeid = (int)FlowFormType.NoScreen, InviteType inviteType = InviteType.Regular, bool onlyplan = false)
        {
            var pointuserinfo = PointUserInfo();
            var flowinstance = FlowInstanceBL.GetByID(uow, flowInstanceID);
            var logentry = LogEntryBL.Create(FormType, pointuserinfo);

            var transferrouteinstance = PhaseInstanceBL.GetAllByFlowInstanceIDAndFormTypeID(uow, flowInstanceID, formtypeid)
                .OrderByDescending(it => it.PhaseInstanceID)
                .FirstOrDefault();

            if (transferrouteinstance == null)
            {
                return null;
            }

            var formSetVersion = SaveFormsetAndValidFields(model, transferrouteinstance.PhaseDefinition.FormTypeID, transferrouteinstance.PhaseInstanceID);

            var hasdestination = departmentIDs != null && departmentIDs.Any();

            transferrouteinstance.RealizedDate = hasdestination ? DateTime.Now : (DateTime?)null;
            transferrouteinstance.RealizedByEmployeeID = hasdestination ? pointuserinfo.Employee.EmployeeID : (int?)null;
            transferrouteinstance.Status = hasdestination ? (int)Status.Done : (int)Status.Active;

            PhaseInstanceBL.Save(uow, transferrouteinstance);

            var modelState = CurrentModelState;
            if (!modelState.IsValid)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(new {ErrorMessage = "Vul eerst de verplichte velden in."});
            }

            if (hasdestination)
            {
                var nextphasedefinition = PhaseDefinitionCacheBL.GetByID(uow, nextPhaseDefinitionID);
                bool needsacknowledgement = nextphasedefinition.FormTypeID == (int)FlowFormType.InBehandelingVVTZHVVT;

                var destinationurl = (nextphasedefinition.FlowDefinitionID == FlowDefinitionID.RzTP) ?
                        Url.Action("DashBoard", "FlowMain", new { TransferID }, Utils.UriScheme()) :
                        Url.Action("DispatchToForm", "FormType", new { TransferID, nextphasedefinition.FormTypeID }, Utils.UriScheme());

                if (onlyplan)
                {
                    foreach (var departmentID in departmentIDs)
                    {
                        var departmenttosendto = DepartmentBL.GetByDepartmentID(uow, departmentID);
                        OrganizationHelper.Queue(uow, flowInstanceID, transferrouteinstance.PhaseDefinitionID, nextPhaseDefinitionID, departmenttosendto.DepartmentID, needsacknowledgement, inviteType, logentry);
                    }
                }
                else
                {
                    //Cancel vorige invites
                    var currentinvites = OrganizationInviteBL.GetAcceptedOrActiveInvitesByFlowInstanceIDAndInviteType(uow, flowInstanceID, inviteType);
                    foreach (var currentinvite in currentinvites)
                    {
                        OrganizationHelper.CancelInvite(uow, currentinvite, pointuserinfo, true);
                    }

                    //Cancel vorige fases
                    OrganizationHelper.CancelReceivingPhases(uow, flowInstanceID, nextPhaseDefinitionID, logentry);

                    //Stuur invite
                    int sendcount = 0;
                    foreach (var departmentID in departmentIDs)
                    {
                        var departmenttosendto = DepartmentBL.GetByDepartmentID(uow, departmentID);
                        if (sendcount == 0)
                        {
                            OrganizationHelper.Invite(uow, flowInstanceID, transferrouteinstance.PhaseDefinitionID, nextPhaseDefinitionID, departmenttosendto.DepartmentID, needsacknowledgement, inviteType, logentry, destinationurl);
                            PhaseInstanceBL.Create(uow, flowInstanceID, nextPhaseDefinitionID, logentry);
                        } else
                        {
                            //Queue the rest
                            OrganizationHelper.Queue(uow, flowInstanceID, transferrouteinstance.PhaseDefinitionID, nextPhaseDefinitionID, departmenttosendto.DepartmentID, needsacknowledgement, inviteType, logentry);
                        }
                        sendcount += 1;
                    }

                    OrganizationHelper.CheckAndSendVerpleegkundigeOverdracht(uow, flowinstance, pointuserinfo, logentry, false);
                    OrganizationHelper.CheckAndSendMedVRI(uow, flowinstance, pointuserinfo);
                }
            }
            else
            {
                var currentinvites = OrganizationInviteBL.GetAcceptedOrActiveInvitesByFlowInstanceIDAndPhaseDefinitionID(uow, flowInstanceID, nextPhaseDefinitionID);
                foreach (var currentinvite in currentinvites)
                {
                    OrganizationHelper.CancelInvite(uow, currentinvite, pointuserinfo, true);
                }

                OrganizationHelper.CancelReceivingPhases(uow, flowInstanceID, nextPhaseDefinitionID, logentry);
                OrganizationHelper.ClearDestinationField(uow, flowInstanceID);

                uow.Save();
            }

            ResetOutputCacheByTransferID(TransferID);
            FlowInstanceSearchValuesBL.CalculateReadValuesByTransferIDAsync(uow, formSetVersion.TransferID);

            return Json(
                new
                {
                    Success = true,
                    Url = Url.Action(FormType.GetActionName(), new {TransferID, transferrouteinstance.PhaseDefinitionID, transferrouteinstance.PhaseInstanceID, formSetVersion.FormSetVersionID})
                },
                JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SendToOrganizationsByViewModel([ModelBinder(typeof(FlowWebFieldBinder))] List<FlowWebField> model, SendToOrganizationViewModel viewmodel)
        {
            var pointuserinfo = PointUserInfo();
            var flowinstance = FlowInstanceBL.GetByID(uow, viewmodel.FlowInstanceID);
            var logentry = LogEntryBL.Create(FormType, pointuserinfo);

            var transferrouteinstance = PhaseInstanceBL.GetAllByFlowInstanceIDAndFormTypeID(uow, viewmodel.FlowInstanceID, viewmodel.FormTypeID)
                .OrderByDescending(it => it.PhaseInstanceID)
                .FirstOrDefault();

            if (transferrouteinstance == null)
            {
                return null;
            }

            var formSetVersion = SaveFormsetAndValidFields(model, transferrouteinstance.PhaseDefinition.FormTypeID, transferrouteinstance.PhaseInstanceID);

            var hasdestination = viewmodel.Departments != null && viewmodel.Departments.Any();

            transferrouteinstance.RealizedDate = hasdestination ? DateTime.Now : (DateTime?)null;
            transferrouteinstance.RealizedByEmployeeID = hasdestination ? pointuserinfo.Employee.EmployeeID : (int?)null;
            transferrouteinstance.Status = hasdestination ? (int)Status.Done : (int)Status.Active;

            PhaseInstanceBL.Save(uow, transferrouteinstance);

            var modelState = CurrentModelState;
            if (!modelState.IsValid)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(new {ErrorMessage = "Vul eerst de verplichte velden in."});
            }

            if (hasdestination)
            {
                var nextphasedefinition = PhaseDefinitionCacheBL.GetByID(uow, viewmodel.NextPhaseDefinitionID);
                bool needsacknowledgement = nextphasedefinition.FormTypeID != (int)FlowFormType.InBehandelingIIZHVVT;

                var destinationurl = (nextphasedefinition.FlowDefinitionID == FlowDefinitionID.RzTP) ?
                        Url.Action("DashBoard", "FlowMain", new { TransferID }, Utils.UriScheme()) :
                        Url.Action("DispatchToForm", "FormType", new { TransferID, nextphasedefinition.FormTypeID }, Utils.UriScheme());

                var activestatuses = new List<InviteStatus> { InviteStatus.Active, InviteStatus.Acknowledged, InviteStatus.Realized };

                var deletinginvites = viewmodel.Departments.Where(d => d.Deleted && d.OrganizationInviteID.HasValue);
                var currentinvites = viewmodel.Departments.Where(d => !d.Deleted && d.OrganizationInviteID.HasValue);
                var newinvites = viewmodel.Departments.Where(d => !d.Deleted && d.OrganizationInviteID == null);

                if (viewmodel.DeleteExisting)
                {
                    var activeinvites = OrganizationInviteBL.GetAcceptedOrActiveInvitesByFlowInstanceIDAndPhaseDefinitionID(uow, viewmodel.FlowInstanceID, viewmodel.NextPhaseDefinitionID);
                    foreach (var invite in activeinvites)
                    {
                        if (activestatuses.Contains(invite.InviteStatus))
                        {
                            OrganizationHelper.CancelReceivingPhases(uow, invite.FlowInstanceID, invite.RecievingPhaseDefinitionID, logentry);
                        }
                        if (invite.InviteSend.HasValue)
                        {
                            OrganizationHelper.CancelInvite(uow, invite, pointuserinfo);
                        }
                        else
                        {
                            OrganizationInviteBL.Delete(uow, invite.OrganizationInviteID);
                        }
                    }
                }
                else
                {
                    //Delete marked for deletion
                    foreach (var department in deletinginvites)
                    {
                        var invite = OrganizationInviteBL.GetByID(uow, department.OrganizationInviteID.Value);
                        if (invite != null)
                        {
                            if (activestatuses.Contains(invite.InviteStatus))
                            {
                                OrganizationHelper.CancelReceivingPhases(uow, invite.FlowInstanceID, invite.RecievingPhaseDefinitionID, logentry);
                            }

                            if (invite.InviteSend.HasValue)
                            {
                                OrganizationHelper.CancelInvite(uow, invite, pointuserinfo);
                            }
                            else
                            {
                                OrganizationInviteBL.Delete(uow, department.OrganizationInviteID.Value);
                            }
                        }
                    }

                    //Send any current queued marked as active
                    foreach (var currentinvite in currentinvites)
                    {
                        var invite = OrganizationInviteBL.GetByID(uow, currentinvite.OrganizationInviteID.Value);
                        if (invite != null)
                        {
                            if (currentinvite.InviteStatus == InviteStatus.Active && invite.InviteStatus == InviteStatus.Queued) 
                            {
                                //Activate queued invite
                                OrganizationHelper.InviteQueuedInvite(uow, invite, destinationurl);
                                PhaseInstanceBL.Create(uow, FlowInstance.FlowInstanceID, viewmodel.NextPhaseDefinitionID, logentry);
                            }
                        }
                    }
                }

                //Set new invites
                foreach (var newinvite in newinvites)
                {
                    if(newinvite.InviteStatus == InviteStatus.Queued)
                    {
                        OrganizationHelper.Queue(uow, viewmodel.FlowInstanceID, transferrouteinstance.PhaseDefinitionID, viewmodel.NextPhaseDefinitionID,
                            newinvite.DepartmentID, needsacknowledgement, viewmodel.InviteType, logentry);
                    } else
                    {
                        OrganizationHelper.Invite(uow, viewmodel.FlowInstanceID, transferrouteinstance.PhaseDefinitionID, viewmodel.NextPhaseDefinitionID,
                            newinvite.DepartmentID, needsacknowledgement, viewmodel.InviteType, logentry, destinationurl);
                        PhaseInstanceBL.Create(uow, FlowInstance.FlowInstanceID, viewmodel.NextPhaseDefinitionID, logentry);
                    }
                }

                OrganizationHelper.CheckAndSendVerpleegkundigeOverdracht(uow, flowinstance, pointuserinfo, logentry, false);
                OrganizationHelper.CheckAndSendMedVRI(uow, flowinstance, pointuserinfo);
            }
            else
            {
                var currentinvites = OrganizationInviteBL.GetAcceptedOrActiveInvitesByFlowInstanceIDAndPhaseDefinitionID(uow, viewmodel.FlowInstanceID, viewmodel.NextPhaseDefinitionID);
                foreach (var currentinvite in currentinvites)
                {
                    OrganizationHelper.CancelInvite(uow, currentinvite, pointuserinfo, true);
                }

                OrganizationHelper.CancelReceivingPhases(uow, viewmodel.FlowInstanceID, viewmodel.NextPhaseDefinitionID, logentry);
                OrganizationHelper.ClearDestinationField(uow, viewmodel.FlowInstanceID);

                uow.Save();
            }

            ResetOutputCacheByTransferID(TransferID);
            FlowInstanceSearchValuesBL.CalculateReadValuesByTransferIDAsync(uow, formSetVersion.TransferID);

            return Json(
                new
                {
                    Success = true,
                    Url = Url.Action(FormType.GetActionName(), new {TransferID, transferrouteinstance.PhaseDefinitionID, transferrouteinstance.PhaseInstanceID, formSetVersion.FormSetVersionID})
                },
                JsonRequestBehavior.AllowGet);
        }

        public ActionResult EmployeeII()
        {
            return new SecureActionResult(FormTypeViewModelHelper.GetEmployeeIIViewModel(uow, GetGlobalPropertiesFormType(getPhaseInstance:true)));
        }

        [HttpPost]
        public ActionResult EmployeeII([ModelBinder(typeof(FlowWebFieldBinder))] List<FlowWebField> model)
        {
            if (NextPhaseHandled)
            {
                var acceptedDateII = model.FirstOrDefault(fwf => fwf.Name == FlowFieldConstants.Name_AcceptedDateII);
                if (acceptedDateII != null)
                {
                    FlowWebFieldBL.SetValue(acceptedDateII, DateTime.Now);
                }
            }

            var formSetVersion = SaveFormsetAndValidFields(model);
            var modelState = CurrentModelState;

            FlowInstanceSearchValuesBL.CalculateReadValuesByTransferIDAsync(uow, formSetVersion.TransferID);

            return SaveResult(modelState, formSetVersion);
        }

        public ActionResult EmployeeGRZ()
        {
            return new SecureActionResult(FormTypeViewModelHelper.GetEmployeeIIViewModel(uow, GetGlobalPropertiesFormType(getPhaseInstance: true)));
        }

        [HttpPost]
        public ActionResult EmployeeGRZ([ModelBinder(typeof(FlowWebFieldBinder))] List<FlowWebField> model)
        {
            if (NextPhaseHandled)
            {
                var acceptedDateII = model.FirstOrDefault(fwf => fwf.Name == FlowFieldConstants.Name_AcceptedDateII);
                if (acceptedDateII != null)
                {
                    FlowWebFieldBL.SetValue(acceptedDateII, DateTime.Now);
                }
            }

            var formSetVersion = SaveFormsetAndValidFields(model);
            var modelState = CurrentModelState;

            FlowInstanceSearchValuesBL.CalculateReadValuesByTransferIDAsync(uow, formSetVersion.TransferID);

            return SaveResult(modelState, formSetVersion);
        }

        public ActionResult CIZResult()
        {
            return new SecureActionResult(FormTypeViewModelHelper.GetCIZResultViewModel(uow, GetGlobalPropertiesFormType()));
        }

        [HttpPost]
        public ActionResult CIZResult([ModelBinder(typeof(FlowWebFieldBinder))] List<FlowWebField> model)
        {
            var formSetVersion = SaveFormsetAndValidFields(model);
            var modelState = CurrentModelState;

            FlowInstanceSearchValuesBL.CalculateReadValuesByTransferIDAsync(uow, formSetVersion.TransferID);

            return SaveResult(modelState, formSetVersion);
        }

        public ActionResult CIZOverview()
        {
            return new SecureActionResult(FormTypeViewModelHelper.GetCizOverviewViewModel(uow, GetGlobalPropertiesFormType()));
        }

        [HttpPost]
        public ActionResult CIZOverview([ModelBinder(typeof(FlowWebFieldBinder))] List<FlowWebField> model)
        {
            var formSetVersion = SaveFormsetAndValidFields(model);
            var modelState = CurrentModelState;

            FlowInstanceSearchValuesBL.CalculateReadValuesByTransferIDAsync(uow, formSetVersion.TransferID);

            return SaveResult(modelState, formSetVersion);
        }

        public ActionResult Signature()
        {
            return new SecureActionResult(FormTypeViewModelHelper.GetSignatureViewModel(uow, GetGlobalPropertiesFormType(getTransfer:true)));
        }

        public ActionResult SignatureInfo()
        {
            return View();
        }

        public ActionResult RealizedCare()
        {
            return new SecureActionResult(FormTypeViewModelHelper.GetRealizedCareViewModel(uow, GetGlobalPropertiesFormType()));
        }

        public ActionResult RealizedCareHA()
        {
            return new SecureActionResult(FormTypeViewModelHelper.GetRealizedCareViewModel(uow, GetGlobalPropertiesFormType()));
        }

        [HttpPost]
        public ActionResult RealizedCareHA([ModelBinder(typeof(FlowWebFieldBinder))] List<FlowWebField> model)
        {
            var flowinstance = FlowInstanceBL.GetByTransferID(uow, TransferID);
            if (flowinstance == null)
            {
                return Content("<!-- Geen flowinstance -->");
            }

            var formsetversion = SaveFormsetAndValidFields(model);
            var modelState = CurrentModelState;

            handleRealizedCare(model, flowinstance);

            FlowInstanceSearchValuesBL.CalculateReadValuesByTransferIDAsync(uow, formsetversion.TransferID);

            return SaveResult(modelState, formsetversion);

        }

        private void handleRealizedCare(List<FlowWebField> model, FlowInstance flowinstance)
        {
            var registrationendedwithouttransfer = model.GetValueOrDefault(FlowFieldConstants.Name_RegistrationEndedWithoutTransfer, "false");
            if (registrationendedwithouttransfer.ConvertTo<bool>() == true)
            {
                var sendingphase = PhaseInstanceBL.GetByFlowInstanceIDAndFlowHandling(uow, flowinstance.FlowInstanceID, FlowHandling.Send);
                var cancelreason = model.FirstOrDefault(fwf => fwf.FlowFieldID == FlowFieldConstants.ID_RegistrationEndedWithoutTransfer)?.Label;
                if (sendingphase != null)
                {
                    OrganizationInviteBL.CancelAllInvitesInQueue(uow, flowinstance, InviteType.Regular);
                    OrganizationInviteBL.CancelInviteHandleNextInQueue(uow, flowinstance, sendingphase, PointUserInfo(), dashboardUrl(Url), cancelreason);
                }
            }

        }


        [HttpPost]
        public ActionResult RealizedCare([ModelBinder(typeof(FlowWebFieldBinder))] List<FlowWebField> model)
        {
            var flowinstance = FlowInstanceBL.GetByTransferID(uow, TransferID);
            if (flowinstance == null)
            {
                return Content("<!-- Geen flowinstance -->");
            }

            var formsetversion = SaveFormsetAndValidFields(model);
            var modelState = CurrentModelState;

            handleRealizedCare(model, flowinstance);

            FlowInstanceSearchValuesBL.CalculateReadValuesByTransferIDAsync(uow, formsetversion.TransferID);

            return SaveResult(modelState, formsetversion);
        }

        public ActionResult AcceptAtVVT()
        {
            return new SecureActionResult(FormTypeViewModelHelper.GetAcceptAtVVTViewModel(uow, GetGlobalPropertiesFormType(getPhaseInstance:true)));
        }

        [HttpPost]
        public ActionResult AcceptAtVVT([ModelBinder(typeof(FlowWebFieldBinder))] List<FlowWebField> model)
        {
            if (NextPhaseHandled)
            {
                var acceptedDateVVT = model.FirstOrDefault(fwf => fwf.Name == "AcceptedDateVVT");
                if (acceptedDateVVT != null)
                {
                    FlowWebFieldBL.SetValue(acceptedDateVVT, DateTime.Now);
                }
            }

            var formSetVersion = SaveFormsetAndValidFields(model);
            FlowInstanceSearchValuesBL.CalculateReadValuesByTransferIDAsync(uow, formSetVersion.TransferID);

            return SaveResult(CurrentModelState, formSetVersion);
        }

        public ActionResult GRZResult()
        {
            return new SecureActionResult(FormTypeViewModelHelper.GetGRZResultViewModel(uow, GetGlobalPropertiesFormType(getTransfer:true)));
        }

        [HttpPost]
        public ActionResult GRZResult([ModelBinder(typeof(FlowWebFieldBinder))] List<FlowWebField> model)
        {
            var formsetversion = SaveFormsetAndValidFields(model);
            var modelState = CurrentModelState;

            FlowInstanceSearchValuesBL.CalculateReadValuesByTransferIDAsync(uow, formsetversion.TransferID);

            return SaveResult(modelState, formsetversion);
        }

        public ActionResult GRZForm()
        {
            return new SecureActionResult(FormTypeViewModelHelper.GetGRZFormViewModel(uow, GetGlobalPropertiesFormType(getTransfer:true, getPhaseInstance: true, getFlowInstance: true)));
        }

        [HttpPost]
        public ActionResult GRZForm([ModelBinder(typeof(FlowWebFieldBinder))] List<FlowWebField> model)
        {
            var formsetversion = SaveFormsetAndValidFields(model);
            var modelState = CurrentModelState;

            FlowInstanceSearchValuesBL.CalculateReadValuesByTransferIDAsync(uow, formsetversion.TransferID);

            return SaveResult(modelState, formsetversion);
        }

        public ActionResult TransferPatientZHVVT()
        {
            return new SecureActionResult(FormTypeViewModelHelper.GetTransferPatientZHVVTViewModel(uow, GetGlobalPropertiesFormType(getTransfer:true, getPhaseInstance:true)));
        }

        [HttpPost]
        public ActionResult TransferPatientZHVVT([ModelBinder(typeof(FlowWebFieldBinder))] List<FlowWebField> model, int? nextPhaseDefinitionID = null)
        {
            var formSetVersion = SaveFormsetAndValidFields(model);
            var modelState = CurrentModelState;

            if (!SaveHandled && NextPhaseHandled && modelState.IsValid)
            {
                if (nextPhaseDefinitionID == null)
                {
                    throw new Exception("Er is geen volgende PhaseDefinitionID bekend voor dit dossier.");
                }

                var formtypeid = (int)FlowFormType.VastleggenBesluitVVTZHVVT;
                var pointuserinfo = PointUserInfo();
                var logentry = LogEntryBL.Create(uow, formtypeid, pointuserinfo);
                var flowinstance = FlowInstanceBL.GetByTransferID(uow, TransferID);
                var currentphaseinstance = PhaseInstanceBL.GetById(uow, PhaseInstanceID);

                var inviteToAccept = OrganizationInviteBL.GetMyInvite(uow, flowinstance.FlowInstanceID, pointuserinfo);
                var vvthasaccepted = model.GetValueOrDefault(FlowFieldConstants.Name_DischargePatientAcceptedYNByVVT, "").ToLogical();
                if (vvthasaccepted)
                {
                    var client = ClientBL.GetByTransferID(uow, TransferID);
                    client.ClientIsAnonymous = false;
                    uow.Save();

                    var nextPhaseDefinition = PhaseDefinitionCacheBL.GetByID(uow, nextPhaseDefinitionID.Value);
                    bool nextPhaseIsEvaluateDecision = nextPhaseDefinition?.FormTypeID == (int)FlowFormType.EvaluateDecision;
                    var remark = model.GetValueOrDefault(FlowFieldConstants.Name_DischargeRemarks, "");
                    if(nextPhaseIsEvaluateDecision)
                    {
                        OrganizationHelper.AcknowledgePartially(uow, inviteToAccept, logentry, pointuserinfo, dashboardUrl(Url), remark: remark);
                    }
                    else
                    {
                        OrganizationHelper.Acknowledge(uow, inviteToAccept, pointuserinfo, remark: remark);
                        OrganizationHelper.CreateCVARegistratie(uow, flowinstance.FlowInstanceID, logentry);
                        OrganizationHelper.CheckAndSendVerpleegkundigeOverdracht(uow, inviteToAccept.FlowInstance, pointuserinfo, logentry, false);
                        OrganizationHelper.CheckAndSendMedVRI(uow, inviteToAccept.FlowInstance, pointuserinfo);
                        OrganizationHelper.CheckAndSendSurvey(uow, inviteToAccept.FlowInstance, pointuserinfo);
                    }

                    OrganizationHelper.SetDestinationField(uow, flowinstance.FlowInstanceID, inviteToAccept?.DepartmentID);

                    ResetOutputCacheByTransferID(TransferID);

                    //return to dashboard
                    nextPhaseDefinitionID = null;
                }
                else
                {
                    //VVT heeft Nee gezegd
                    //Uitnodinging cancellen
                    var reason = model.GetValueOrDefault(FlowFieldConstants.Name_DischargeReason, "");
                    OrganizationHelper.Refuse(uow, inviteToAccept, pointuserinfo, reason);

                    if (inviteToAccept!= null)
                    {
                        OrganizationHelper.CancelReceivingPhases(uow, flowinstance.FlowInstanceID, inviteToAccept.RecievingPhaseDefinitionID, logentry);
                    }

                    
                    //Eventuele invite die in de wacht staan activeren.
                    var quedinvite = OrganizationHelper.InviteQueued(uow, flowinstance, pointuserinfo, dashboardUrl(Url));
                    if (quedinvite != null)
                    {
                        var phaseinstance = PhaseInstanceBL.Create(uow, flowinstance.FlowInstanceID, quedinvite.RecievingPhaseDefinitionID, logentry);
                        OrganizationHelper.SetDestinationField(uow, quedinvite.FlowInstanceID, quedinvite.DepartmentID);

                        if (flowinstance.FlowDefinition.FlowTypeID == (int)FlowTypeID.HA_VVT)
                        {
                            MedVRIBL.SendMedvriHAVVT(uow, flowinstance, FlowFormType.VastleggenBesluitVVTZHVVT, pointuserinfo, TemplateTypeID.ZorgMailHAVVTNeeEnNieuweVVT, inviteToAccept.Department);
                        }
                    } else
                    {
                        OrganizationHelper.ClearDestinationField(uow, flowinstance.FlowInstanceID);
                        if (flowinstance.FlowDefinition.FlowTypeID == (int)FlowTypeID.HA_VVT)
                        {
                            MedVRIBL.SendMedvriHAVVT(uow, flowinstance, FlowFormType.VastleggenBesluitVVTZHVVT, pointuserinfo, TemplateTypeID.ZorgMailHAVVTNee, inviteToAccept.Department);
                        }
                    }

                    var activerouterenphaseinstances = PhaseInstanceBL.GetAllByTransferAndPhaseDefinition(uow, TransferID, inviteToAccept.SendingPhaseDefinitionID.GetValueOrDefault());
                    if (activerouterenphaseinstances != null && activerouterenphaseinstances.Any())
                    {
                        //Deze is door HandleNextPhase verwerkt maar niet op cancelled gezet.
                        var phaseinstancetocancel = activerouterenphaseinstances.Where(it => it.Status == (int)Status.Done).LastOrDefault();
                        if (phaseinstancetocancel != null)
                        {
                            var newrouterendossierphaseinstance = activerouterenphaseinstances.Where(it => it.Status == (int)Status.Active).FirstOrDefault();
                            if (newrouterendossierphaseinstance != null)
                            {
                                var newrouterenformsetversion = FormSetVersionBL.CreateFormSetVersion(uow, TransferID,
                                    phaseinstancetocancel.PhaseDefinition.FormTypeID, newrouterendossierphaseinstance.PhaseInstanceID,
                                    pointuserinfo, logentry, createdbyemployeeid: newrouterendossierphaseinstance.StartedByEmployeeID);

                                FlowFieldValueBL.CopyTo(uow, phaseinstancetocancel.FormSetVersion.FirstOrDefault(), newrouterenformsetversion);

                                if(quedinvite != null)
                                {
                                    //Stap is al uitgevoerd ivm queued invite
                                    newrouterendossierphaseinstance.Status = (int)Status.Done;
                                }
                            }

                            PhaseInstanceBL.Cancel(uow, phaseinstancetocancel, logentry);

                            uow.Save();
                        }
                    }

                    ResetOutputCacheByTransferID(TransferID);

                    //return to dashboard
                    nextPhaseDefinitionID = null;
                }
                OrganizationHelper.HandleTransferMemo(uow, flowinstance, model, pointuserinfo);

                // so SaveResult can redirect as required
                NextPhaseHandled = true;
            }

                var transfer = TransferBL.GetByTransferID(uow, TransferID);
            ViewBag.ClientID = transfer.ClientID;

            FlowInstanceSearchValuesBL.CalculateReadValuesByTransferIDAsync(uow, formSetVersion.TransferID);

            return SaveResult(modelState, formSetVersion, nextPhaseDefinitionID);
        }

        public ActionResult EvaluateDecision()
        {
            return new SecureActionResult(FormTypeViewModelHelper.GetEvaluateDecisionViewModel(uow, GetGlobalPropertiesFormType(getTransfer: true, getPhaseInstance: true)));
        }

        [HttpPost]
        public ActionResult EvaluateDecision([ModelBinder(typeof(FlowWebFieldBinder))] List<FlowWebField> model, int? nextPhaseDefinitionID = null)
        {
            var modelState = CurrentModelState;
            var formSetVersion = SaveFormsetAndValidFields(model);

            if(nextPhaseDefinitionID.HasValue)
            {
                var flowinstance = FlowInstanceBL.GetByTransferID(uow, TransferID);
                var pointuserinfo = PointUserInfo();
                var formtypeid = (int)FlowFormType.EvaluateDecision;
                var logentry = LogEntryBL.Create(uow, formtypeid, pointuserinfo);
                var currentphaseinstance = PhaseInstanceBL.GetById(uow, PhaseInstanceID);
                var inviteToAccept = OrganizationInviteBL.GetPartiallyAcknowledgedByFlowInstanceID(uow, flowinstance.FlowInstanceID);

                var phasedefinitioncancel = PhaseDefinitionCacheBL.GetByFlowDefinitionIDAndFlowHandling(uow, this.FlowInstance.FlowDefinitionID, FlowHandling.Send);
                var cancelled = nextPhaseDefinitionID == phasedefinitioncancel?.PhaseDefinitionID;

                if (cancelled)
                {
                    //Uitnodinging cancellen
                    OrganizationHelper.CancelInvite(uow, inviteToAccept, pointuserinfo, true);

                    if (inviteToAccept != null)
                    {
                        OrganizationHelper.CancelReceivingPhases(uow, flowinstance.FlowInstanceID, inviteToAccept.RecievingPhaseDefinitionID, logentry);
                    }

                    //Eventuele invite die in de wacht staan activeren.
                    var quedinvite = OrganizationHelper.InviteQueued(uow, flowinstance, pointuserinfo, dashboardUrl(Url));
                    if (quedinvite != null)
                    {
                        var phaseinstance = PhaseInstanceBL.Create(uow, flowinstance.FlowInstanceID, quedinvite.RecievingPhaseDefinitionID, logentry);
                        OrganizationHelper.SetDestinationField(uow, quedinvite.FlowInstanceID, quedinvite.DepartmentID);

                        if (flowinstance.FlowDefinition.FlowTypeID == (int)FlowTypeID.HA_VVT)
                        {
                            MedVRIBL.SendMedvriHAVVT(uow, flowinstance, FlowFormType.EvaluateDecision, pointuserinfo, TemplateTypeID.ZorgMailHAVVTNeeEnNieuweVVT, inviteToAccept.Department);
                        }
                    } else
                    {
                        OrganizationHelper.ClearDestinationField(uow, flowinstance.FlowInstanceID);
                        if (flowinstance.FlowDefinition.FlowTypeID == (int)FlowTypeID.HA_VVT)
                        {
                            MedVRIBL.SendMedvriHAVVT(uow, flowinstance, FlowFormType.EvaluateDecision, pointuserinfo, TemplateTypeID.ZorgMailHAVVTNee, inviteToAccept.Department);
                        }
                    }

                    var activerouterenphaseinstances = PhaseInstanceBL.GetAllByTransferAndPhaseDefinition(uow, TransferID, inviteToAccept.SendingPhaseDefinitionID.GetValueOrDefault());
                    if (activerouterenphaseinstances != null && activerouterenphaseinstances.Any())
                    {
                        //Deze is door HandleNextPhase verwerkt maar niet op cancelled gezet.
                        var phaseinstancetocancel = activerouterenphaseinstances.Where(it => it.Status == (int)Status.Done).LastOrDefault();
                        if (phaseinstancetocancel != null)
                        {
                            var newrouterendossierphaseinstance = activerouterenphaseinstances.Where(it => it.Status == (int)Status.Active).FirstOrDefault();
                            if (newrouterendossierphaseinstance != null)
                            {
                                var newrouterenformsetversion = FormSetVersionBL.CreateFormSetVersion(uow, TransferID,
                                    phaseinstancetocancel.PhaseDefinition.FormTypeID, newrouterendossierphaseinstance.PhaseInstanceID,
                                    pointuserinfo, logentry, createdbyemployeeid: newrouterendossierphaseinstance.StartedByEmployeeID);

                                FlowFieldValueBL.CopyTo(uow, phaseinstancetocancel.FormSetVersion.FirstOrDefault(), newrouterenformsetversion);

                                if (quedinvite != null)
                                {
                                    //Stap is al uitgevoerd ivm queued invite
                                    newrouterendossierphaseinstance.Status = (int)Status.Done;
                                }
                            }

                            PhaseInstanceBL.Cancel(uow, phaseinstancetocancel, logentry);

                            uow.Save();
                        }
                    }
                }
                else
                {
                    var client = ClientBL.GetByTransferID(uow, TransferID);
                    client.ClientIsAnonymous = false;
                    uow.Save();

                    OrganizationHelper.Acknowledge(uow, inviteToAccept, pointuserinfo);

                    //VVT (A) krijgt CVA 3MND
                    OrganizationHelper.CreateCVARegistratie(uow, flowinstance.FlowInstanceID, logentry);
                    OrganizationHelper.CheckAndSendVerpleegkundigeOverdracht(uow, inviteToAccept.FlowInstance, pointuserinfo, logentry, false);
                    OrganizationHelper.CheckAndSendMedVRI(uow, inviteToAccept.FlowInstance, pointuserinfo);
                    OrganizationHelper.CheckAndSendSurvey(uow, inviteToAccept.FlowInstance, pointuserinfo);
                }

                var message = cancelled 
                    ? "Door ontvanger voorgestelde ingangsdatum zorg is afgewezen door verzender" 
                    : "Door ontvanger voorgestelde ingangsdatum zorg is geaccepteerd door verzender";

                TransferMemoViewBL.Insert(uow, TransferID, message, TransferMemoTypeID.Transferpunt);
                SignaleringBL.HandleSignalByFlowDefinitionFormTypeAction(uow, flowinstance, (int)FlowFormType.CommunicationJournal, "SaveTransferMemo", pointuserinfo, message);

                ServiceBusBL.SendMessage(uow, MessageQueues.PhaseQueue, "EvaluateDecisionNotification", new EvaluateDecisionNotification
                {
                    TimeStamp = DateTime.Now,
                    UniqueID = DateTime.Now.Ticks + inviteToAccept.OrganizationInviteID,
                    TransferID = inviteToAccept.FlowInstance.TransferID,
                    OrganizationInviteID = inviteToAccept.OrganizationInviteID,
                    EmployeeID = inviteToAccept.CreatedByID,
                    InviteUrl = dashboardUrl(Url)
                });
            }

            var result = SaveResult(modelState, formSetVersion, nextPhaseDefinitionID);

            ResetOutputCacheByTransferID(TransferID);
            FlowInstanceSearchValuesBL.CalculateReadValuesByTransferIDAsync(uow, formSetVersion.TransferID);

            return result;
        }

        public ActionResult Hulpmiddelen()
        {
            return new SecureActionResult(FormTypeViewModelHelper.GetHulpmiddelenViewModel(this, GetGlobalPropertiesFormType(getFlowInstance:true)));
        }

        [HttpPost]
        public ActionResult Hulpmiddelen([ModelBinder(typeof(HulpmiddelenBinder))] HulpmiddelenViewModel model, string formsetVersionName, string flowFieldAfleverTijd)
        {
            if (!ModelState.IsValid)
            {
                return Json(new
                {
                    ShowErrors = true,
                    ValidationErrors = ModelState.Values.SelectMany(m => m.Errors)
                        .Select(e => e.ErrorMessage)
                        .ToList()
                });

            }
            
            var globalProps = GetGlobalPropertiesFormType();

            var phaseDefinition = PhaseDefinitionCacheBL.GetByID(uow, globalProps.PhaseDefinitionID);
            if (phaseDefinition == null)
            {
                throw new Exception("Geen phasedefinition");
            }

            var phaseInstanceID = PhaseInstanceID;
            if (phaseInstanceID > 0)
            {
                var phaseInstance = PhaseInstanceBL.GetById(uow, PhaseInstanceID);
                if (FormSetVersionID <= 0 && phaseInstance.FormSetVersion != null && phaseInstance.FormSetVersion.Any(fsv => !fsv.Deleted))
                {
                    phaseInstanceID = -1;
                }
            }

            // MediPoint uses standard FlowField dropdown
            if (model.SupplierID == (int)SupplierID.MediPoint)
            {
                flowFieldAfleverTijd = "";
            }
            // Custom DropdownMultiSelect
            var afleverTijd = globalProps.FlowWebFields.FirstOrDefault(fwf => fwf.FlowFieldID == FlowFieldConstants.ID_AfleverTijd);
            if (afleverTijd != null)
            {
                FlowWebFieldBL.SetValue(afleverTijd, flowFieldAfleverTijd);
                model.Global.FlowWebFields.Add(afleverTijd);
            }

            var formSetVersion = SaveFormsetAndValidFields(model.Global.FlowWebFields, phaseDefinition.FormTypeID, phaseInstanceID, description: formsetVersionName);

            if (formSetVersion == null)
            {
                throw new Exception("Deze formulier bestaat niet meer");
            }

            AidProductBL.SaveOrderItems(uow, model.OrderItems, formSetVersion.FormSetVersionID);
            AidProductBL.UpdateAnswersSetFormSetVersionID(uow, model.UniqueFormKey, formSetVersion.FormSetVersionID);

            var orderStatusField = model.Global.FlowWebFields.FirstOrDefault(fwf => fwf.Name == FlowFieldConstants.Name_OrderStatus);

            if (model.IsExternalSupplier && DefinitiveHandled || (orderStatusField != null && !string.IsNullOrEmpty(orderStatusField.Value)))
            {
                var logEntry = LogEntryBL.Create(FlowFormType.Hulpmiddelen, globalProps.PointUserInfo);
                var timestamp = DateTime.Now;

                switch (model.SupplierID)
                {
                    case (int)SupplierID.MediPoint:
                        ServiceBusBL.SendMessage(uow, MessageQueues.PhaseQueue, "SendMediPointOrder",
                            new SendMediPointOrder
                            {
                                FormSetVersionID = formSetVersion.FormSetVersionID,
                                ScreenID = logEntry.ScreenID,
                                SendingEmployeeID = logEntry.EmployeeID,
                                SendingOrganizationID = globalProps.PointUserInfo.Organization.OrganizationID,
                                TransferID = formSetVersion.TransferID,
                                TimeStamp = timestamp,
                                UniqueID = timestamp.Ticks,
                            });
                        break;
                    case (int)SupplierID.Vegro:
                        ServiceBusBL.SendMessage(uow, MessageQueues.PhaseQueue, "SendVegroOrder",
                            new SendVegroOrder
                            {
                                FormSetVersionID = formSetVersion.FormSetVersionID,
                                ScreenID = logEntry.ScreenID,
                                SendingEmployeeID = logEntry.EmployeeID,
                                SendingOrganizationID = globalProps.PointUserInfo.Organization.OrganizationID,
                                TransferID = formSetVersion.TransferID,
                                TimeStamp = timestamp,
                                UniqueID = timestamp.Ticks,
                            });
                        break;
                }
            }

            FlowInstanceSearchValuesBL.CalculateReadValuesByTransferIDAsync(uow, formSetVersion.TransferID);

            var modelState = CurrentModelState;

            return SaveResult(modelState, formSetVersion);
        }


        public ActionResult InvullenCVARegistratieZH()
        {
            return new SecureActionResult(FormTypeViewModelHelper.GetInvullenCVARegistratieZHViewModel(uow, GetGlobalPropertiesFormType()));
        }

        [HttpPost]
        public ActionResult InvullenCVARegistratieZH([ModelBinder(typeof(FlowWebFieldBinder))] List<FlowWebField> model)
        {
            var formSetVersion = SaveFormsetAndValidFields(model);
            var modelState = CurrentModelState;

            var pointuserinfo = PointUserInfo();
            var logentry = LogEntryBL.Create(FormType, pointuserinfo);

            FlowWebFieldBL.SyncValue(uow, pointuserinfo, TransferID, formSetVersion.FormSetVersionID,
                FlowFormType.InvullenCVARegistratieZH, FlowFieldConstants.Name_PatientWordtOpgenomenDatum,
                FlowFormType.AanvraagFormulierZHVVT, FlowFieldConstants.Name_MedischeSituatieDatumOpname,
                logentry, ViewData);

            FlowInstanceSearchValuesBL.CalculateReadValuesByTransferIDAsync(uow, formSetVersion.TransferID);

            return SaveResult(modelState, formSetVersion);
        }

        public ActionResult CVARouteSelection()
        {

            var viewModel = FormTypeViewModelHelper.GetCVARouteSelectionViewModel(uow, GetGlobalPropertiesFormType(getPhaseInstance: true));
            if (viewModel.HasErrors)
            {
                return Content($"<!-- {viewModel.ErrorMessage} -->");
            }

            return new SecureActionResult(viewModel);
        }

        [HttpPost]
        public ActionResult CVARouteSelection([ModelBinder(typeof(FlowWebFieldBinder))] List<FlowWebField> model, int? nextphasedefinitionid = null)
        {
            var pointuserinfo = PointUserInfo();

            var previousontslagbestemming = FlowWebFieldBL.FromTransferIDAndName(uow, TransferID, FlowFieldConstants.Name_Ontslagbestemming, pointuserinfo, ViewData);
            var previousontslagbestemmingthuis  = FlowWebFieldBL.FromTransferIDAndName(uow, TransferID, FlowFieldConstants.Name_OntslagbestemmingThuis, pointuserinfo, ViewData);

            var bestemmingchanged = previousontslagbestemming?.Value != model.GetValueOrDefault(FlowFieldConstants.Name_Ontslagbestemming, null) ||
                                    previousontslagbestemmingthuis?.Value != model.GetValueOrDefault(FlowFieldConstants.Name_OntslagbestemmingThuis, null);

            var formsetversion = SaveFormsetAndValidFields(model);
            var flowinstance = FlowInstanceBL.GetByTransferID(uow, TransferID);
            var formtypeid = (int)FlowFormType.CVARouteSelection;
            var logentry = LogEntryBL.Create(uow, formtypeid, pointuserinfo);

            var currentphaseinstance = PhaseInstanceBL.GetByFlowInstanceIDAndPhaseDefinition(uow, flowinstance.FlowInstanceID, PhaseDefinitionID);
            if (nextphasedefinitionid.HasValue)
            {
                if (bestemmingchanged)
                {
                    var startphasetoclear = 3;
                    PhaseInstanceBL.CancelExceptNewPhase(uow, TransferID, startphasetoclear, nextphasedefinitionid.Value, logentry);
                }

                var nextphasedefinition = PhaseDefinitionCacheBL.GetByID(uow, nextphasedefinitionid.Value);
                if (nextphasedefinition.FormTypeID == (int)FlowFormType.AfsluitenDossierCVA)
                {
                    //Afsluiten met CVA
                    var lastphastinstance = PhaseInstanceBL.GetLastPhaseInstance(uow, flowinstance.FlowInstanceID);
                    PhaseInstanceBL.SetStatusDone(uow, lastphastinstance, logentry);

                    //ZH krijgt CVA 3MND
                    OrganizationHelper.CreateCVARegistratie(uow, flowinstance.FlowInstanceID, logentry);
                }
                else if (nextphasedefinition.FormTypeID == (int)FlowFormType.VersturenVO)
                {
                    //Verkorte VO(direct) flow
                    var selectedhealthcareprovider = model.GetValueOrDefault(FlowFieldConstants.Name_SelectedHealthCareProvider, null);
                    if (string.IsNullOrWhiteSpace(selectedhealthcareprovider))
                    {
                        Response.StatusCode = (int)HttpStatusCode.BadRequest;
                        return Json(new { ErrorMessage = "Geen (gewenste) VVT geselecteerd." });
                    }

                    var departmentvvtid = int.Parse(selectedhealthcareprovider);
                    var departmentvvt = DepartmentBL.GetByDepartmentID(uow, departmentvvtid);
                    if (departmentvvt == null)
                    {
                        Response.StatusCode = (int)HttpStatusCode.BadRequest;
                        return Json(new { ErrorMessage = "Afdeling: " + departmentvvtid + " niet gevonden." });
                    }

                    FlowFieldValueBL.UpdateOrInsert(uow, flowinstance.FlowInstanceID, formsetversion.FormSetVersionID, FlowFieldConstants.ID_SelectedHealthCareProvider, "", logentry);
                    FlowFieldValueBL.UpdateOrInsert(uow, flowinstance.FlowInstanceID, formsetversion.FormSetVersionID, FlowFieldConstants.ID_DestinationHealthCareProvider, departmentvvtid.ToString(), logentry);

                    var currentinvites = OrganizationInviteBL.GetAcceptedOrActiveInvitesByFlowInstanceIDAndPhaseDefinitionID(uow, FlowInstance.FlowInstanceID, nextphasedefinition.PhaseDefinitionID);
                    foreach (var currentinvite in currentinvites)
                    {
                        OrganizationHelper.CancelInvite(uow, currentinvite, pointuserinfo, true);
                        SignaleringBL.ExpireSignaleringByFlowInstanceIDAndDepartmentID(uow, FlowInstance.FlowInstanceID, currentinvite.DepartmentID);
                    }

                    OrganizationHelper.Invite(uow, flowinstance.FlowInstanceID, PhaseDefinitionID, nextphasedefinition.PhaseDefinitionID, departmentvvt.DepartmentID, false, InviteType.Regular, logentry, dashboardUrl(Url));
                    OrganizationHelper.SetDestinationField(uow, flowinstance.FlowInstanceID, departmentvvt.DepartmentID);

                    //VVT (A) krijgt CVA 3MND
                    OrganizationHelper.CreateCVARegistratie(uow, flowinstance.FlowInstanceID, logentry);

                    var verpleegkundigeoverdrachtbo = new VerpleegkundigeOverdrachtBO(uow, FlowInstance, pointuserinfo);
                    var alreadysent = alreadySentToCodeGroup(currentphaseinstance, "VVTGROUP");
                    verpleegkundigeoverdrachtbo.Send(alreadysent);
                    verpleegkundigeoverdrachtbo.HandleNextPhase(logentry);
                    HandleThirdParty(alreadysent, pointuserinfo);
                }
                else if (nextphasedefinition.FormTypeID == (int)FlowFormType.AanvraagFormulierZHVVT)
                {
                    //Niets te doen meer (cancelde vroeger CVA 3mnd)
                }
            }

            ResetOutputCacheByTransferID(TransferID);
            FlowInstanceSearchValuesBL.CalculateReadValuesByTransferIDAsync(uow, formsetversion.TransferID);

            return SaveResult(CurrentModelState, formsetversion, nextphasedefinitionid);
        }

        public ActionResult InvullenCVARegistratieVVT()
        {
            return new SecureActionResult(FormTypeViewModelHelper.GetInvullenCVARegistratieVVTViewModel(uow, GetGlobalPropertiesFormType()));
        }

        [HttpPost]
        public ActionResult InvullenCVARegistratieVVT([ModelBinder(typeof(FlowWebFieldBinder))] List<FlowWebField> model)
        {
            var formSetVersion = SaveFormsetAndValidFields(model);
            var modelState = CurrentModelState;

            if (NextPhaseHandled)
            {
                // PatientOntslagenVVTNaar
                // { "Text": "Ander ziekenhuis", "Value": "0"}
                // { "Text": "Naar huis met zorg", "Value": "2"}
                // { "Text": "Revalidatiecentrum", "Value": "3"}
                // { "Text": "Revalidatieafdeling CVA unit veprleeghuis", "Value": "4"}
                // { "Text": "Permanente plek verpleeghuis", "Value": "5"}
                // { "Text": "Naar huis met behandeling", "Value": "6"}
                // { "Text": "Naar huis met zorg én behandeling", "Value": "8"}
                string[] vvtvalues = { "0", "2", "3", "4", "5", "6", "8" };
                var patientontslagenvvtnaar = model.GetValueOrDefault(FlowFieldConstants.Name_PatientOntslagenVVTNaar, null);
                if (!string.IsNullOrWhiteSpace(patientontslagenvvtnaar) && vvtvalues.Contains(patientontslagenvvtnaar))
                {
                    Organization organization = null;
                    Department department = null;
                    if (patientontslagenvvtnaar == "0")
                    {
                        var patientontslagenvvtnaarnaamzh = model.GetValueOrDefault(FlowFieldConstants.Name_PatientOntslagenVVTNaarNaamZH, null);
                        if (int.TryParse(patientontslagenvvtnaarnaamzh, out int organizationoutsidepointid))
                        {
                            var organizationoutsidepoint = OrganizationOutsidePointBL.GetByID(uow, organizationoutsidepointid);
                            if (organizationoutsidepoint?.OrganizationID != null)
                            {
                                organization = OrganizationBL.GetByOrganizationID(uow, organizationoutsidepoint.OrganizationID.Value);
                                if (organization != null)
                                {
                                    department = organization.Location.Where(loc => !loc.Inactive).SelectMany(loc => loc.Department).FirstOrDefault(dep => !dep.Inactive);
                                    if (department == null)
                                    {
                                        Response.StatusCode = (int)HttpStatusCode.BadRequest;
                                        return Json(new { ErrorMessage = "Organisatie: " + organization.Name + " heeft geen actieve afdeling" });
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        var patientontslagenvvtnaarvvt = model.GetValueOrDefault(FlowFieldConstants.Name_PatientOntslagenVVTNaarVVT, "");
                        if (int.TryParse(patientontslagenvvtnaarvvt, out int departmentid))
                        {
                            department = DepartmentBL.GetByDepartmentID(uow, departmentid);
                        }
                    }

                    var pointuserinfo = PointUserInfo();
                    var logentry = LogEntryBL.Create(uow, FormTypeID, pointuserinfo);

                    if (department?.Location?.OrganizationID != null)
                    {
                        var inviteformtypeid = (int)FlowFormType.InvullenCVARegistratie3mnd;
                        var phasedefinitiontoinvite = PhaseDefinitionCacheBL.GetByFlowDefinitionAndFormType(uow, FlowInstance.FlowDefinitionID, inviteformtypeid);

                        var destinationurl = Url.Action("DispatchToForm", "FormType", new { TransferID, FormTypeID = inviteformtypeid }, Utils.UriScheme());
                        var currentinvites = OrganizationInviteBL.GetAcceptedOrActiveInvitesByFlowInstanceIDAndPhaseDefinitionID(uow, FlowInstance.FlowInstanceID, phasedefinitiontoinvite.PhaseDefinitionID);
                        foreach (var currentinvite in currentinvites)
                        {
                            OrganizationHelper.CancelInvite(uow, currentinvite, pointuserinfo, true);
                            SignaleringBL.ExpireSignaleringByFlowInstanceIDAndDepartmentID(uow, FlowInstance.FlowInstanceID, currentinvite.DepartmentID);
                        }

                        OrganizationHelper.Invite(uow, FlowInstance.FlowInstanceID, PhaseDefinitionID, phasedefinitiontoinvite.PhaseDefinitionID, department.DepartmentID, false, InviteType.DoorlDossier, logentry, destinationurl);
                        OrganizationHelper.SetDestinationField(uow, FlowInstance.FlowInstanceID, department.DepartmentID);
                    }
                }
            }

            FlowInstanceSearchValuesBL.CalculateReadValuesByTransferIDAsync(uow, formSetVersion.TransferID);

            return SaveResult(modelState, formSetVersion);
        }

        public ActionResult InvullenCVARegistratie3Mnd()
        {
            return new SecureActionResult(FormTypeViewModelHelper.GetInvullenCVARegistratie3MndViewModel(uow, GetGlobalPropertiesFormType(getTransfer:true)));
        }

        [HttpPost]
        public ActionResult InvullenCVARegistratie3Mnd([ModelBinder(typeof(FlowWebFieldBinder))] List<FlowWebField> model)
        {
            var formsetversion = SaveFormsetAndValidFields(model);
            var modelstate = CurrentModelState;

            if (DefinitiveHandled && modelstate.IsValid)
            {
                var pointuserinfo = PointUserInfo();
                var logentry = LogEntryBL.Create(FormType, pointuserinfo);

                var frequency = FrequencyBL.GetByType(uow, TransferID, FrequencyType.CVA);
                FrequencyBL.SetStatus(uow, frequency, FrequencyStatus.Done, logentry);
            }

            if (formsetversion == null)
            {
                return null;
            }
            FlowInstanceSearchValuesBL.CalculateReadValuesByTransferIDAsync(uow, formsetversion.TransferID);

            return SaveResult(modelstate, formsetversion);
        }

        public ActionResult MsvtINDFrequency(int? parentformsetversionid = null)
        {
            return new SecureActionResult(FormTypeViewModelHelper.GetMsvtINDFrequencyViewModel(uow, GetGlobalPropertiesFormType(getTransfer:true),parentformsetversionid, this));
        }

        [HttpPost]
        public ActionResult MsvtINDFrequency(FrequencyViewModel model)
        {
            var formsetversion = SaveFormsetAndValidFields(new List<FlowWebField>(), model.Name);

            var modelstate = new FlowWebFieldModelState(ModelState);
            if (modelstate.IsValid)
            {
                var pointuserinfo = PointUserInfo();
                var logentry = LogEntryBL.Create(FormType, pointuserinfo);

                var frequency = FrequencyViewBL.ToEntity(uow, model);
                if (DefinitiveHandled)
                {
                    FrequencyBL.SetStatus(uow, frequency, FrequencyStatus.Done, logentry);
                }

                uow.Save();
            }

            if (formsetversion == null) return null;
            FlowInstanceSearchValuesBL.CalculateReadValuesByTransferIDAsync(uow, formsetversion.TransferID);

            return SaveResult(modelstate, formsetversion);
        }

        public ActionResult Doorlopenddossier()
        {
            return new SecureActionResult(FormTypeViewModelHelper.GetDoorlopendDossierViewModel(uow, GetGlobalPropertiesFormType(getTransfer:true)));
        }

        [HttpPost]
        public ActionResult Doorlopenddossier(FrequencyViewModel viewmodel)
        {
            var sendingOrganizationID = OrganizationHelper.GetSendingOrganizationId(uow, FlowInstance.FlowInstanceID);
            var client = ClientBL.GetByTransferID(uow, TransferID);
            var createNewDoorlopendDossier = FormSetVersionID <= 0;
            var modelstate = new DoorlopendDossierModelState(ModelState, createNewDoorlopendDossier, sendingOrganizationID, client.CivilServiceNumber);
            if (modelstate.IsValid)
            {
                var formsetversion = SaveFormsetAndValidFields(new List<FlowWebField>(), viewmodel.Name);

                var pointuserinfo = PointUserInfo();
                var logentry = LogEntryBL.Create(FormType, pointuserinfo);

                var frequency = FrequencyViewBL.ToEntity(uow, viewmodel);
                LoggingBL.FillLogging(uow, frequency, logentry);

                if (DefinitiveHandled)
                {
                    FrequencyBL.SetStatus(uow, frequency, FrequencyStatus.Done, logentry);
                }

                uow.Save();

                FlowInstanceSearchValuesBL.CalculateReadValuesByTransferIDAsync(uow, formsetversion.TransferID);

                return SaveResult(modelstate, formsetversion);
            }

            return SaveResult(modelstate, PhaseDefinitionID, FormSetVersionID, PhaseInstanceID);
        }

        [ValidateAntiFiddleInjection("transferid")]
        public JsonResult CancelVVT(string cancelreason = null)
        {
            var flowinstance = FlowInstanceBL.GetByTransferID(uow, TransferID);

            var sendingphase = PhaseInstanceBL.GetByFlowInstanceIDAndFlowHandling(uow, flowinstance.FlowInstanceID, FlowHandling.Send);
            if (sendingphase != null)
            {
                OrganizationInviteBL.CancelInviteHandleNextInQueue(uow, flowinstance, sendingphase, PointUserInfo(), dashboardUrl(Url), cancelreason);

                FlowInstanceSearchValuesBL.CalculateReadValuesByTransferIDAsync(uow, TransferID);

                return Json(new { Success = true }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { Success = false, ErrorMessage = "VVT stap bestaat niet" }, JsonRequestBehavior.AllowGet);
        }

        [ValidateAntiFiddleInjection("transferid")]
        public JsonResult CancelReceivingOrganization()
        {
            var flowinstance = FlowInstanceBL.GetByTransferID(uow, TransferID);

            var sendingphase = PhaseInstanceBL.GetByFlowInstanceIDAndFlowHandling(uow, flowinstance.FlowInstanceID, FlowHandling.Send);
            if (sendingphase != null)
            {
                if (sendingphase.Status == (int)Status.Done)
                {
                    sendingphase.Status = (int)Status.Active;
                }

                var pointuserinfo = PointUserInfo();

                var logentry = LogEntryBL.Create(FlowFormType.NoScreen, pointuserinfo);
                var currentinvite = OrganizationInviteBL.GetAcceptedOrActiveInvitesByFlowInstanceIDAndInviteType(uow, flowinstance.FlowInstanceID, InviteType.Regular).FirstOrDefault();
                OrganizationHelper.CancelInvite(uow, currentinvite, pointuserinfo, true);
                OrganizationHelper.CancelReceivingPhases(uow, flowinstance.FlowInstanceID, currentinvite?.RecievingPhaseDefinitionID ?? 0, logentry);
                OrganizationHelper.ClearDestinationField(uow, flowinstance.FlowInstanceID);
                SignaleringBL.ExpireSignaleringByFlowInstanceIDAndDepartmentID(uow, flowinstance.FlowInstanceID, currentinvite.DepartmentID);

                uow.Save();

                FlowInstanceSearchValuesBL.CalculateReadValuesByTransferIDAsync(uow, TransferID);

                return Json(new { Success = true }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { Success = false, ErrorMessage = "Ontvangende stap bestaat niet" }, JsonRequestBehavior.AllowGet);
        }

        private bool alreadySentToCodeGroup(PhaseInstance phaseinstance, string codegroup)
        {
            var nextphasedefinitions = PhaseDefinitionBL.GetNextPhaseDefinitions(uow, phaseinstance.PhaseDefinitionID);
            var nextphasedefinition = nextphasedefinitions.Where(it => it.CodeGroup == codegroup).FirstOrDefault();
            if (nextphasedefinition != null)
            {
                return PhaseInstanceBL.GetByFlowInstanceAndPhase(uow, phaseinstance.FlowInstanceID, nextphasedefinition.Phase, true).Any();
            }
            return false;
        }

    }
}