﻿using System.Linq;
using System.Web.Mvc;
using Point.Business.Logic;
using Point.Database.Models.ViewModels;

namespace Point.MVC.WebApplication.Controllers
{
    public class DossierTagController : PointFormController
    {
        // GET: DossierTag
        public SecureJsonResult AddDossierTag(int transferID, string tag)
        {
            FlowInstanceTagBL.AddDossierTag(transferID, tag);

            return new SecureJsonResult(true, JsonRequestBehavior.AllowGet);
        }

        public SecureJsonResult DeleteDossierTag(int transferID, string tag)
        {
            FlowInstanceTagBL.DeleteDossierTag(transferID, tag);

            return new SecureJsonResult(true, JsonRequestBehavior.AllowGet);
        }

        public SecureActionResult DossierTags(int transferID)
        {
            var pointUserInfo = PointUserInfo();
            var flowInstance = FlowInstanceBL.GetByTransferID(uow, transferID);

            bool isSendingOrganization = flowInstance.StartedByOrganizationID == pointUserInfo.Organization.OrganizationID;
            bool isRegioAdmin = pointUserInfo.IsRegioAdmin;
            bool isClosed = FlowInstanceBL.IsFlowClosed(uow, flowInstance.FlowInstanceID);

            var viewmodel = new DossierTagsViewModel()
            {
                TransferID = transferID,
                Editable = !isClosed && (isSendingOrganization || isRegioAdmin)
            };

            return new SecureActionResult(viewmodel, asPartial: true);
        }

        public SecureActionResult DossierTagSelection(int transferid)
        {
            var flowinstance = FlowInstanceBL.GetByTransferID(uow, transferid);
            var alltags = OrganizationProjectBL.GetActiveByOrganizationID(uow, flowinstance.StartedByOrganizationID ?? 0).Select(it => it.Name).ToArray();
            var existing = FlowInstanceTagBL.GetAllByTransferID(transferid);

            var selection = alltags.Where(t => existing.Contains(t) == false).ToArray();

            return new SecureActionResult(selection, asPartial: true);
        }
    }
}