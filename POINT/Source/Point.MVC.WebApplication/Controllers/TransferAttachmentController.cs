﻿using DevTrends.MvcDonutCaching;
using Point.Business.Logic;
using Point.Database.Extensions;
using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Infrastructure.Attributes;
using Point.Infrastructure.Extensions;
using Point.Infrastructure.Helpers;
using Point.MVC.WebApplication.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Mvc;

namespace Point.MVC.WebApplication.Controllers
{
    public class TransferAttachmentController : PointFormController
    {
        public ActionResult List(AttachmentSource attachmentsource = AttachmentSource.None, FrequencyType frequencyType = FrequencyType.None, bool readOnly = false, bool skipPdf = false, bool asPartial = false)
        {
            var pointuserinfo = PointUserInfo();

            var flowinstance = FlowInstanceBL.GetByTransferID(uow, TransferID);
            var showTakenOverMessage = TransferAttachmentBL.TakeOverTransferAttachments(uow, flowinstance, TransferID, pointuserinfo);

            // SecureActionResult.SetFormRights is called too late to correctly handle SetReadMode for Attachments, so we handle the action ourselves here.
            if (!readOnly && frequencyType == FrequencyType.None)
            {                
                if (flowinstance != null)
                {
                    readOnly = FlowInstanceBL.IsFlowClosed(uow, flowinstance);
                }
                if (readOnly)
                {
                    SetReadMode();
                }
            }

            PageHeader = "Bijlagen";

            var viewmodel = new TransferAttachmentListViewModel
            {
                TransferID = TransferID,
                PhaseInstanceID = PhaseInstanceID,
                PhaseDefinitionID = PhaseDefinitionID,
                ReadOnly = readOnly,
                AttachmentSource = attachmentsource,
                Items = Enumerable.Empty<TransferAttachmentViewModel>(),
                ShowTakeOverMessage = showTakenOverMessage
            };

            if (frequencyType != FrequencyType.None)
            {
                var frequency = FrequencyBL.GetByType(uow, TransferID, frequencyType);
                if (frequency == null)
                {
                    return new SecureActionResult("List", viewmodel, true);
                }
                readOnly = frequency.Status == FrequencyStatus.Done;
                viewmodel.FrequencyID = frequency.FrequencyID;
                viewmodel.Items = TransferAttachmentViewBL.GetByFrequencyID(uow, TransferID, frequency.FrequencyID, pointuserinfo, readOnly);
            }
            else
            {
                viewmodel.Items = TransferAttachmentViewBL.GetByTransferIDAndSource(uow, TransferID, attachmentsource, pointuserinfo, readOnly, skipPdf: skipPdf);
            }

            viewmodel.Items = viewmodel.Items.Where(ta => ta.ShowInAttachmentList);

            return new SecureActionResult("List", viewmodel, asPartial);
        }

        public ActionResult FlowAttachments(AttachmentSource attachmentsource = AttachmentSource.None, int? frequencyid = null)
        {
            var pointuserinfo = PointUserInfo();

            var model = frequencyid.HasValue
                ? TransferAttachmentViewBL.GetByFrequencyID(uow, TransferID, frequencyid.Value, pointuserinfo)
                : TransferAttachmentViewBL.GetByTransferIDAndSource(uow, TransferID, attachmentsource, pointuserinfo);

            return new SecureActionResult(model.Where(m => m.ShowInAttachmentList));
        }

        public ActionResult Add(AttachmentSource attachmentsource, AttachmentTypeID attachmentTypeID = AttachmentTypeID.None, int? frequencyid = null, string partialname = "Edit", string name = "", bool onlyPDF = false)
        {
            var model = new TransferAttachmentViewModel
            {
                TransferID = TransferID,
                FrequencyID = frequencyid,
                AttachmentSource = attachmentsource,
                AttachmentTypeID = attachmentTypeID,
                Name = name,
                ShowInAttachmentList = (partialname == "UploadInForm" ? false : true),
                SignalOnSave = SignaleringTrigger != null,
                OnlyPDF = onlyPDF 
            };

            ViewBag.AttachmentTypeID = getAttachmentTypes(model.AttachmentTypeID);

            return PartialView(partialname, model);
        }

        [DonutOutputCache(Duration = 0), ValidateAntiFiddleInjection("AttachmentID")]
        public ActionResult Edit(int attachmentid)
        {
            var model = TransferAttachmentViewBL.GetByAttachmentID(uow, attachmentid);
            model.SignalOnSave = SignaleringTrigger != null;

            ViewBag.AttachmentTypeID = getAttachmentTypes(model.AttachmentTypeID); 

            return PartialView(model);
        }
        
        private IEnumerable<SelectListItem> getAttachmentTypes(AttachmentTypeID selectedAttachmentTypeID)
        {
            var attachmenttypesexclude = new[] {
                AttachmentTypeID.ExternVODocument,
                AttachmentTypeID.DumpReport,
                AttachmentTypeID.AuthorizationForm,
                AttachmentTypeID.LogRead
            };

            var attachmenttypes = Enum.GetValues(typeof(AttachmentTypeID)).Cast<AttachmentTypeID>()
                .Where(at => at > 0 && !attachmenttypesexclude.Contains(at))
                .Select(at => new SelectListItem {
                    Text = at.GetDescription(),
                    Value = ((int)at).ToString(),
                    Selected = (at == selectedAttachmentTypeID)
                }
            );

            attachmenttypes = (new[] { new SelectListItem { Text = " - Maak uw keuze - ", Value = "", Selected = selectedAttachmentTypeID == AttachmentTypeID.None } }).Concat(attachmenttypes);

            return attachmenttypes;
        }

        public ActionResult UploadInForm(AttachmentTypeID attachmentTypeID, string name, int transferAttachementID = -1, bool onlyPDF = false)
        {
            if (transferAttachementID == -1)
            {
                return Add(AttachmentSource.None, attachmentTypeID, null, "UploadInForm", name, onlyPDF);
            }
            return Edit(transferAttachementID);
        }

        [ValidateAntiFiddleInjection("AttachmentID")]
        public FileResult Download(int attachmentID)
        {
            DisableClientCache = true;

            var file = TransferAttachmentBL.GetFileByID(uow, attachmentID);
            if (file == null)
            {
                return File(Encoding.ASCII.GetBytes("Bijlage niet gevonden!"), "text/plain");
            }

            string filename = FileHelper.GetValidFileName(file.FileName);
            return File(file.FileData, file.ContentType, filename);
        }

        [ValidateAntiFiddleInjection("AttachmentID")]
        public ActionResult Delete(int attachmentID)
        {
            var errorMessage = "";
            var transferAttachment = TransferAttachmentBL.GetByID(uow, attachmentID);
            var employeeID = transferAttachment.EmployeeID ?? default(int);
            var pointuserinfo = PointUserInfo();

            var rightsDeleteItem = TransferAttachmentBL.HasRightsUpdateOrDeleteTransferAttachment(uow, TransferID, employeeID, pointuserinfo);
            if (rightsDeleteItem)
            {
                var logentry = LogEntryBL.Create(FlowFormType.Attachments, pointuserinfo);
                TransferAttachmentBL.DeleteAndSave(uow, transferAttachment, logentry);
            }
            else
            {
                errorMessage = "U heeft geen rechten om deze bijlage te verwijderen.";
            }

            FlowInstanceSearchValuesBL.CalculateReadValuesByTransferIDAsync(uow, TransferID);

            Response.StatusCode = (int)(errorMessage == "" ? HttpStatusCode.OK : HttpStatusCode.BadRequest);
            if (errorMessage == "")
            {
                return Json(new { Url = Url.Action("List", ControllerName, new { TransferID }, Request.Url?.Scheme) }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { ErrorMessage = errorMessage }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Save(TransferAttachmentViewModel viewmodel)
        {
            var pointuserinfo = PointUserInfo();
            var logentry = LogEntryBL.Create(FlowFormType.NoScreen, pointuserinfo);
            var errorMessage = "";
            TransferAttachment transferattachment = null;

            if (viewmodel.AttachmentID == 0)
            {
                if (ModelState.IsValid)
                {
                    var uploadhelper = new UploadHelper(TransferID, Request.Files, viewmodel);
                    if (uploadhelper.CreateAttachment())
                    {
                        transferattachment = uploadhelper.TransferAttachment;

                        TransferAttachmentBL.FromModel(transferattachment, viewmodel, pointuserinfo.Employee.EmployeeID, TransferID);
                        TransferAttachmentBL.InsertAndSave(uow, transferattachment, logentry);

                        if (viewmodel.FrequencyID.HasValue)
                        {
                            var frequency = FrequencyBL.GetByID(uow, viewmodel.FrequencyID.Value);
                            FrequencyTransferAttachmentBL.InsertAndSave(uow, frequency, transferattachment);
                        }

                        SignaleringBL.HandleSignalByTransferAttachmentVM(uow, FlowInstance, viewmodel, pointuserinfo);
                        TransferAttachmentBL.HandleThirdParty(uow, transferattachment, FlowInstance, pointuserinfo, TransferID);
                    }

                    errorMessage = uploadhelper.Error;
                }
                else
                {
                    errorMessage = "Gegevens zijn niet juist: " + string.Join("; ", ModelState.Values.SelectMany(m => m.Errors).Select(m => m.ErrorMessage));
                }
            }
            else
            {
                transferattachment = TransferAttachmentBL.GetByID(uow, viewmodel.AttachmentID);

                transferattachment.Name = viewmodel.Name;
                transferattachment.Description = viewmodel.Description;
                transferattachment.AttachmentTypeID = viewmodel.AttachmentTypeID;

                LoggingBL.FillLoggingWithID(uow, transferattachment, logentry);

                uow.Save();

                SignaleringBL.HandleSignalByTransferAttachmentVM(uow, FlowInstance, viewmodel, pointuserinfo);
            }

            FlowInstanceSearchValuesBL.CalculateReadValuesByTransferIDAsync(uow, TransferID);
            
            Response.StatusCode = (int)(errorMessage == "" ? HttpStatusCode.OK : HttpStatusCode.BadRequest);
            if (errorMessage == "" && transferattachment != null)
            {
                return Json(new
                {
                    TransferAttachmentID = transferattachment.AttachmentID,
                    DownloadUrl = Url.Action("Download", new { TransferID, transferattachment.AttachmentID, Digest = AntiFiddleInjection.CreateDigest(transferattachment.AttachmentID) }),
                    transferattachment.Name,
                    transferattachment.GenericFile?.FileName,
                    UploadDate = DateTime.Now.ToPointDateDisplay(),
                    EmployeeName = pointuserinfo.Employee.FullName(),
                    TransferID,
                    Digest = AntiFiddleInjection.CreateDigest(transferattachment.AttachmentID)
                }, JsonRequestBehavior.AllowGet);
            }

            return Json(new {ErrorMessage = errorMessage}, JsonRequestBehavior.AllowGet);
        }
    }
}