﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DevTrends.MvcDonutCaching;
using Point.Business.Helper;
using Point.Business.Logic;
using Point.Database.Context;
using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure;
using Point.Infrastructure.Caching;
using Point.Infrastructure.Exceptions;
using Point.Infrastructure.Extensions;
using Point.MVC.WebApplication.Extensions;
using System.Text;
using Newtonsoft.Json;
using Point.Infrastructure.Helpers;
using Point.MVC.WebApplication.Helpers;
using FiftyOne.Foundation.Mobile.Detection;
using System.Web.Routing;
using System.Threading.Tasks;
using Point.Infrastructure.Constants;
using Point.Log4Net;

namespace Point.MVC.WebApplication.Controllers
{
    [SessionState(System.Web.SessionState.SessionStateBehavior.ReadOnly)]
    public class PointBaseController : Controller
    {
        public readonly UnitOfWork<PointContext> uow = new UnitOfWork<PointContext>();
        protected string ErrorMessage = "";
        
        public static ILogger Logger = LogManager.GetLogger(typeof(PointBaseController));

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            if (ModelState.IsValid && !filterContext.IsChildAction && filterContext.HttpContext.Request.IsAuthenticated && ConfigurationManager.AppSettings["LogReadActions"] == "true")
            {
                var transferid = Request.GetRequestVariable<int?>("transferid", null);
                LogReadBL.Save(uow, filterContext.HttpContext.Request, this.PointUserInfo(), transferid);
            }

            base.OnActionExecuted(filterContext);
        }

        protected override void Dispose(bool disposing)
        {
            uow.SafeDispose();
            base.Dispose(disposing);
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            if (filterContext.ExceptionHandled)
            {
                return;
            }

            var errorNumber = SendException(filterContext.Exception, filterContext.HttpContext);
            var forceErrorHandling = ConfigHelper.GetAppSettingByName<bool>("ForceErrorHandling");

            if (HttpContext.IsDebuggingEnabled && !forceErrorHandling)
            {
                return;
            }

            filterContext.HttpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;
            filterContext.ExceptionHandled = true;

            if (filterContext.HttpContext.Request.IsAjaxRequest())
            {
                filterContext.Result = Json(new { ErrorMessage = ErrorMessage == "" ? filterContext.Exception.Message : ErrorMessage, ErrorNumber = errorNumber }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var exceptionType = ExceptionType.Default;
                var exception = filterContext.Exception;

                if (exception != null)
                {
                    switch (exception)
                    {
                        case PointSecurityException pointSecurityException:
                            exceptionType = pointSecurityException.ExceptionType;
                            break;
                        case PointDossierDeletedException _:
                            exceptionType = ExceptionType.DossierDeleted;
                            break;
                        default:
                        {
                            if (exception.InnerException is PointSecurityException pointSecurityException)
                            {
                                exceptionType = pointSecurityException.ExceptionType;
                            }
                            break;
                        }
                    }
                }

                filterContext.Result = new RedirectResult("~/error/errorpage?exceptiontype=" + (int)exceptionType + "&errornumber=" + errorNumber);
            }
        }


        private PointUserInfo SavedPointUserInfo { get; set; }
        public virtual PointUserInfo PointUserInfo()
        {
            if(SavedPointUserInfo != null)
            {
                return SavedPointUserInfo;
            }

            var savedguid = PointUserInfoHelper.GetSavedProviderUserKey();
            if (savedguid == Guid.Empty)
            {
                SavedPointUserInfo = PointUserInfoHelper.GetPointUserInfoNoCache(uow);
                PointUserInfoHelper.SaveProviderUserKey(SavedPointUserInfo?.Employee?.UserID);
            } else
            {
                SavedPointUserInfo = PointUserInfoHelper.GetPointUserInfo(uow, savedguid);
            }

            return SavedPointUserInfo;
        }

        private static string RemovePlainPassword(string postData = null)
        {
            if (string.IsNullOrEmpty(postData)) return postData;

            const string passwordSign = "Password=";
            var postParams = postData.Split('&');
            foreach (var param in postParams)
            {
                // if postdata contains (exactly) 'Password' param : 
                if (param.IndexOf(passwordSign, StringComparison.InvariantCultureIgnoreCase) != 0)
                {
                    continue;
                }
                postData = postData.Replace("&" + param, "");
                break;
            }
            return postData;
        }

        public int SendException(Exception exception, HttpContextBase httpcontext)
        {
            return ExceptionHelper.SendMessageException(GetLogParameters(exception.Message, exception));
        }

        public LogParameters GetLogParameters(string message, Exception exception = null)
        {
            if (string.IsNullOrEmpty(message))
            {
                // TODO: Determine what to set here when empty
                message = GetType().Name;
            }

            var pointUserInfo = PointUserInfo();
            int? employeeID = pointUserInfo?.Employee?.EmployeeID;
            var employeeUserName = pointUserInfo?.Employee?.UserName;

            var postData = string.Empty;
            var cookiesString = string.Empty;
            var isAjaxRequest = false;
            var userAgent = string.Empty;
            var queryString = string.Empty;
            var url = string.Empty;


            var httpContext = HttpContext;
            if (httpContext != null)
            {
                var request = httpContext.Request;
                {
                    if (request.Url != null)
                    {
                        var uri = request.Url;
                        url = uri.AbsolutePath;
                        queryString = uri.Query;
                    }

                    userAgent = request.UserAgent ?? "";
                    isAjaxRequest = request.IsAjaxRequest();

                    var cookies = request.Cookies;
                    if (cookies != null)
                    {
                        foreach (var cookieKey in cookies.AllKeys)
                        {
                            if (cookieKey == ".POINTAUTH" || cookieKey == ".POINTROLES")
                            {
                                continue;
                            }

                            var httpCookie = cookies[cookieKey];
                            if (httpCookie != null)
                            {
                                cookiesString += $"{cookieKey}={httpCookie.Value}&";
                            }
                        }
                    }

                    if (httpContext.Request?.InputStream != null)
                    {
                        var streamReader = new StreamReader(httpContext.Request.InputStream);
                        postData = RemovePlainPassword(streamReader.ReadToEnd());
                    }
                    else if (request.Params?.Count > 0)
                    {
                        const string passwordSign = "password";

                        foreach (var parameter in request.Params)
                        {
                            if (parameter.ToString().ToLowerInvariant() == passwordSign)
                            {
                                continue;
                            }
                            
                            postData += $"{parameter},";
                        }
                    }
                    postData = postData.TrimEnd(',');
                }
            }

            return new LogParameters
            {
                Message = message,
                TransferID = Request.GetRequestVariable<int?>("transferid", null),
                MachineName = Environment.MachineName,
                WindowsIdentity = Environment.UserName,
                EmployeeID = employeeID,
                Exception = exception,
                IsAjaxRequest = isAjaxRequest,
                PostData = postData,
                Cookies = cookiesString,
                UserAgent = userAgent,
                QueryString = queryString,
                Url = url,
                EmployeeUserName = employeeUserName
            };
        }

    }

    public class PointController : PointBaseController
    {

        protected string PageHeader = "";
        protected string PageTitle = "";
        protected string ActionName;
        protected string ControllerName;
        protected string ScreenName;
        protected bool ForceReload;
        protected bool DisableClientCache;
        protected DeviceHelper DeviceHelper;

        private const string LastLoginDateToPoint = "LastLoginDateToPoint";


        protected virtual GlobalProperties GetGlobalProperties(bool asPartial = false, bool getTransfer = false, bool getPhaseInstance = false, bool getNewFormRequestUrl = false, bool getFlowInstance = false)
        {
            var globalProperties = new GlobalProperties
            {
                ViewData = ViewData,
                ActionName = ActionName,
                ControllerName = ControllerName,
                AsPartial = asPartial,
                //FlowInstance = FlowInstance,          // FormType
                //FlowWebFields = XXXX,                 // FormType
                FormSetVersionID = FormSetVersionID,
                FormTypeID = FormTypeID,
                GeneralActionID = GeneralActionID,
                //HistoryFromDate = HistoryFromDate,    // FormType
                IsClosed = ViewData.IsClosed(),
                IsPageLock = ViewData.IsPageLock(),
                IsPrintRequest = Request.IsPrintRequest(),
                IsReadMode = ViewData.IsReadMode(),
                PhaseDefinitionID = PhaseDefinitionID,
                PhaseInstanceID = PhaseInstanceID,
                PointUserInfo = PointUserInfo(),
                TransferID = TransferID,
                FormRequestUrl = Url.Action(ActionName, ControllerName, new { TransferID, FormSetVersionID, PhaseDefinitionID, PhaseInstanceID })
            };

            if (getTransfer)
            {
                globalProperties.Transfer = TransferBL.GetByTransferID(uow, TransferID);
            }

            if (getPhaseInstance)
            {
                PhaseInstance phaseInstance = null;
                if (PhaseInstanceID > 0)
                {
                    phaseInstance = PhaseInstanceBL.GetById(uow, PhaseInstanceID);
                }
                if (phaseInstance == null)
                {
                    phaseInstance = PhaseInstanceBL.GetByTransferIDAndControllerAndAction(uow, TransferID, ControllerName, ActionName);
                }
                globalProperties.PhaseInstance = phaseInstance;
            }

            if (getNewFormRequestUrl)
            {
                globalProperties.NewFormRequestUrl = Url.Action(ActionName, ControllerName, new { TransferID, PhaseDefinitionID });
            }

            if (getFlowInstance)
            {
                globalProperties.FlowInstance = FlowInstanceBL.GetByTransferID(uow, TransferID);
            }

            return globalProperties;
        }

        internal void GlobalPropertiesUpdateFromViewData(GlobalProperties globalProps)
        {
            globalProps.IsClosed = ViewData.IsClosed();
            globalProps.IsPageLock = ViewData.IsPageLock();
            globalProps.IsReadMode = ViewData.IsReadMode();
        }

        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);

            if (WebProvider.ActiveProvider != null)
            {
                var match = WebProvider.ActiveProvider.Match(requestContext.HttpContext.Request.Headers);
                this.DeviceHelper = new DeviceHelper(match);
            }

            ViewBag.IsMobile = IsMobileDevice();
        }

        public bool IsMobileDevice()
        {
            if (ForcedDesktop())
            {
                return false;
            }

            var forcemobile = ConfigHelper.GetAppSettingByName<bool>("ForceMobileDevice", false);

            return forcemobile || this.DeviceHelper?.IsMobile == true;
        }

        public bool ForcedDesktop()
        {
            return (bool?)Session[SessionIdentifiers.Point.ForceDesktop] == true;
        }
         
        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            ValidateGlobalViewModel();

            SetViewBagHeader();
            SetViewBagTitle();
            SetClientCacheHeaders();

            base.OnActionExecuted(filterContext);
        }

        public GlobalViewModel GetAsGlobalViewModel(object model)
        {
            if (model == null || !model.GetType().IsSubclassOf(typeof(GlobalViewModel)))
            {
                return null;
            }
            return (GlobalViewModel)model;
        }

        private void ValidateGlobalViewModel()
        {
            var viewModel = GetAsGlobalViewModel(ViewData.Model);
            if (viewModel == null)
            {
                return;
            }

            if (!viewModel.IsGlobalPropertiesSet)
            {
                throw new ArgumentException($"{ControllerName}.{ActionName} : ViewModel needs to have GlobalProperties set (viewModel.SetGlobalProperties in controller/helper)!");
            }
        }

        protected void SetViewBagHeader()
        {
            ViewBag.Header = PageHeader;
        }

        protected void SetClientCacheHeaders()
        {
            if(this.DisableClientCache)
            {
                Response.DisableClientCache();
            }
        }

        protected virtual void SetViewBagTitle()
        {
            ViewBag.Title = PageTitleBL.GetTitle(PageTitle, PageHeader);
        }

        public int TransferID { get; set; }
        public int FormSetVersionID { get; set; }
        public int PhaseDefinitionID { get; set; }
        public int PhaseInstanceID { get; set; }
        public int FormTypeID { get; set; }
        public int GeneralActionID { get; set; }

        public SignaleringTrigger SignaleringTrigger { get; set; }

        public static string AppPath
        {
            get
            {
                var appPath = System.Web.HttpContext.Current.Request.ApplicationPath == "/" ?
                    System.Web.HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) :
                    $"{System.Web.HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority)}{System.Web.HttpContext.Current.Request.ApplicationPath}";

                if (ConfigurationManager.AppSettings["ForceHTTPS"] == "true")
                {
                    appPath = appPath.Replace("http://", "https://");
                }

                return appPath;
            }
        }

        private bool AreYouSure
        {
            get
            {
                if (String.IsNullOrEmpty(ControllerName))
                {
                    return false;
                }

                var userinfo = PointUserInfo();
                if (userinfo?.Organization == null)
                {
                    return false;
                }

                var includeAreYouSureMessage = (ControllerName.Equals("Client", StringComparison.InvariantCultureIgnoreCase)
                                                || ControllerName.Equals("FormType", StringComparison.InvariantCultureIgnoreCase))
                                               && PointUserInfo().Organization.ShowUnloadMessage == true;

                return includeAreYouSureMessage;
            }
        }

        private OutputCacheManager _outputCacheManager;
        public void ResetOutputCacheByTransferID(int transferid)
        {
            if (_outputCacheManager == null)
            {
                _outputCacheManager = new OutputCacheManager();
            }

            _outputCacheManager.RemoveItems(null, null, new { TransferID = transferid });
        }


        private RedirectToRouteResult redirectMFA(ActionExecutingContext filterContext)
        {
            RedirectToRouteResult redirectmfa = null;
            LoginController logincontroller = new LoginController();

            if (filterContext.HttpContext.Request.IsAuthenticated
                && !filterContext.HttpContext.Request.IsAjaxRequest()
                && !filterContext.IsChildAction
                && !(filterContext.Controller is LoginController)
                && !(filterContext.Controller is AccountController)
                && logincontroller.GetTempDataMFA() != true)
            {

                logincontroller.SetPreviousUrl(filterContext.HttpContext);
           
                var pointuserinfo = PointUserInfo();

                string clientIp = LoginBL.GetClientIP(Request);
                if (PointUserInfoHelper.IsMFARequired(pointuserinfo, clientIp))
                {
                    string action = PointUserInfoHelper.GetMFAAction(pointuserinfo, "ChangeMFA");
                    if (!String.IsNullOrEmpty(action))
                    {
                        redirectmfa = RedirectToAction(action, "Account", new { Area = "" });
                    }
                }
                else if (!PointUserInfoHelper.HasValidMFA(uow, pointuserinfo, clientIp))
                {
                    string action = PointUserInfoHelper.GetMFAAction(pointuserinfo, "LoginMFA");
                    if (!String.IsNullOrEmpty(action))
                    {
                        redirectmfa = RedirectToAction(action, "Login", new { Area = "" });
                    }
                }
            }

            if (redirectmfa != null)
            {
                logincontroller.SetTempDataMFA(true);
            }

            logincontroller.Dispose();

            return redirectmfa;
        }

        private RedirectToRouteResult redirectChangePassword(ActionExecutingContext filterContext)
        {
            RedirectToRouteResult redirectchangepassword = null;

            if (filterContext.HttpContext.Request.IsAuthenticated
                && !filterContext.HttpContext.Request.IsAjaxRequest()
                && !filterContext.IsChildAction
                && !(filterContext.Controller is LoginController)
                && !(filterContext.Controller is AccountController))
            {
                if (PointUserInfoHelper.GetPasswordExpired(PointUserInfo(), LoginBL.IsSSORequest(filterContext.HttpContext.Request)))
                {
                    redirectchangepassword = RedirectToAction("ChangePassword", "Account", new { expired = true, Area = "" } );
                }
            }

            return redirectchangepassword;

        }

        private RedirectToRouteResult redirectFillSecretQuestion(ActionExecutingContext filterContext)
        {
            RedirectToRouteResult redirectsecretquestion = null;

            if (filterContext.HttpContext.Request.IsAuthenticated
                && !filterContext.HttpContext.Request.IsAjaxRequest()
                && !filterContext.IsChildAction
                && !(filterContext.Controller is LoginController)
                && !(filterContext.Controller is AccountController))
            {
                var pointuserinfo = PointUserInfo();
                if (!pointuserinfo.Organization.SSO && !PointUserInfoHelper.GetCancelSecurityQuestion(pointuserinfo))
                {
                    var employee = EmployeeBL.GetByID(uow, pointuserinfo.Employee.EmployeeID);

                    if (string.IsNullOrEmpty(employee?.aspnet_Users?.aspnet_Membership?.PasswordQuestion) || string.IsNullOrEmpty(employee?.EmailAddress))
                    {
                        PointUserInfoHelper.SetCancelSecurityQuestion(pointuserinfo);
                        redirectsecretquestion = RedirectToAction("SecretQuestion", "Account", new { expired = true, Area = "" });
                    }
                }             
            }

            return redirectsecretquestion;
        }

        private void setLastLoginToPoint(ActionExecutingContext filterContext)
        {
            var user = PointUserInfo();
            if (user?.Employee != null &&
                filterContext.HttpContext.Request.IsAuthenticated
                && !filterContext.HttpContext.Request.IsAjaxRequest()
                && !filterContext.IsChildAction
                && !(filterContext.Controller is LoginController)
                && !(filterContext.Controller is AccountController)&&
                Session[LastLoginDateToPoint] == null)
            {
                var logdate = DateTime.Now;
                var employee = EmployeeBL.GetByID(uow, user.Employee.EmployeeID);
                employee.LastRealLoginDate = logdate;
                EmployeeBL.Save(uow, employee);
                Session[LastLoginDateToPoint] = logdate;
            }
        }

        private void resetLastLoginToPoint()
        {
            Session.Remove(LastLoginDateToPoint);     
        }



        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //Keep for T1 too many issues to be safe
            //ReferrerHelper.CheckReferrer(filterContext?.HttpContext?.Request);

            ActionName = filterContext.RouteData.Values["action"].ToString();
            ControllerName = filterContext.RouteData.Values["controller"].ToString();
            ScreenName = $"{ControllerName}.{ActionName}";

            if(ConfigHelper.GetAppSettingByName("CheckAccountRedirects", true))
            {
                var redirectmfa = redirectMFA(filterContext);
                if (redirectmfa != null)
                {
                    filterContext.Result = redirectmfa;
                    resetLastLoginToPoint();
                    return;
                }

                var redirectchangepassword = redirectChangePassword(filterContext);
                if (redirectchangepassword != null)
                {
                    filterContext.Result = redirectchangepassword;
                    resetLastLoginToPoint();
                    return;
                }

                var redirectSetSecretQuestion = redirectFillSecretQuestion(filterContext);
                if (redirectSetSecretQuestion != null)
                {
                    filterContext.Result = redirectSetSecretQuestion;
                    resetLastLoginToPoint();
                    return;
                }
            }

            
            
            var forcedesktop = System.Web.HttpContext.Current.Request.GetRequestVariable<bool?>("forcedesktop", null); 
            if(forcedesktop.HasValue)
            {
                Session[SessionIdentifiers.Point.ForceDesktop] = forcedesktop.Value;
                ViewBag.IsMobile = !forcedesktop;
            }

            base.OnActionExecuting(filterContext);

            if (filterContext.RouteData.GetValue<bool>("printmode", false))
            {
                TransferID = filterContext.RouteData.GetValue<int>("transferid", -1);
                FormSetVersionID = filterContext.RouteData.GetValue<int>("formsetversionid", -1);
                FormTypeID = filterContext.RouteData.GetValue<int>("formtypeid", -1);
                PhaseDefinitionID = filterContext.RouteData.GetValue<int>("phasedefinitionid", -1);
                PhaseInstanceID = filterContext.RouteData.GetValue<int>("phaseinstanceid", -1);
                GeneralActionID = filterContext.RouteData.GetValue<int>("generalactionid", -1);
            }
            else
            {
                TransferID = System.Web.HttpContext.Current.Request.GetRequestVariable<int>("transferid", -1);
                FormSetVersionID = System.Web.HttpContext.Current.Request.GetRequestVariable<int>("formsetversionid", -1);
                FormTypeID = System.Web.HttpContext.Current.Request.GetRequestVariable<int>("formtypeid", -1);
                PhaseDefinitionID = System.Web.HttpContext.Current.Request.GetRequestVariable<int>("phasedefinitionid", -1);
                PhaseInstanceID = System.Web.HttpContext.Current.Request.GetRequestVariable<int>("phaseinstanceid", -1);
                GeneralActionID = System.Web.HttpContext.Current.Request.GetRequestVariable<int>("generalactionid", -1);
            }

            if(Request.IsAuthenticated && !filterContext.IsChildAction && !Request.IsAjaxRequest())
            {
                if(ExternalPatientHelper.HavePatExternalReferenceFromRequest(Request) || ExternalPatientHelper.HavePatExternalReferenceBSNFromRequest(Request))
                {
                    LoginBL.GetAndSetPatExternalReference(Request);
                    LoginBL.GetAndSetPatExternalReferenceBSN(Request);
                }

                var paramDepartmentReference1 = LoginBL.paramDepartmentReference1;
                if (Request.GetRequestVariable<string>(paramDepartmentReference1, null) != null)
                {
                    var pointuserinfo = PointUserInfo();
                    LoginBL.SetDepartmentReference(uow, Request, pointuserinfo);
                }
            }

            ViewBag.AreYouSure = AreYouSure.ToString().ToLower();
            ViewBag.AppPath = AppPath;
            ViewBag.TransferID = TransferID;
            ViewBag.FormSetVersionID = FormSetVersionID;
            ViewBag.FormTypeID = FormTypeID;
            ViewBag.PhaseInstanceID = PhaseInstanceID;
            ViewBag.PhaseDefinitionID = PhaseDefinitionID;
            ViewBag.GeneralActionID = GeneralActionID;

            if (string.Compare("LogOff", ActionName, StringComparison.OrdinalIgnoreCase) == 0 && (filterContext.Controller is LoginController))
            {
                resetLastLoginToPoint();
            }

            setLastLoginToPoint(filterContext);
        }
    }


    public class PointFormController : PointController
    {
        public FormType FormType;
        public FlowInstance FlowInstance;
        public Transfer Transfer;
        public FlowWebFieldModelState CurrentModelState { get; set; }

        protected bool SaveHandled;
        protected bool NextPhaseHandled;
        protected bool DefinitiveHandled;
        protected bool SendSignal;
        protected string SendSignalMessage;
        protected bool ShowHistory;
        protected DateTime? HistoryFromDate;
        protected bool ShowValidationErrors { get; set; }

        public void SetModelState(PointUserInfo pointuserinfo)
        {
            CurrentModelState = new FlowWebFieldModelState();

            if (FormType == null)
            {
                return;
            }

            List<FlowWebField> flowwebfields = null;
            if (ViewData.Model is List<FlowWebField> list)
            {
                flowwebfields = list;
            }
            else
            {
                var viewModel = GetAsGlobalViewModel(ViewData.Model);
                if (viewModel != null)
                {
                    if (viewModel.Global?.FlowWebFields?.Any() ?? false)
                    {
                        flowwebfields = viewModel.Global.FlowWebFields;
                    }
                }
            }

            if (flowwebfields == null)
            {
                return;
            }

            var flowfieldattributeexceptions = FlowFieldAttributeExceptionBL
                .GetByFormTypeIDPhaseDefinitionIDAndRegioIDAndOrganizationID(uow, 
                FormType.FormTypeID, PhaseDefinitionID, pointuserinfo.Region.RegionID, 
                pointuserinfo.Organization.OrganizationID).ToList();

            CurrentModelState = FlowWebFieldBL.Validate(uow, flowwebfields, flowfieldattributeexceptions, ViewData);
        }


        protected override void SetViewBagTitle()
        {
            ViewBag.Title = PageTitleBL.GetTitle(PageTitle, PageHeader, FormType);
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var masterpagevars = new MasterPageVars { PageTitle = PageHeader };
            ViewBag.MasterPageVars = masterpagevars;

            base.OnActionExecuted(filterContext);
        }
        
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            if (FormType == null && FormTypeID > 0)
            {
                FormType = FormTypeBL.GetByFormTypeID(uow, FormTypeID);
            }

            if (PhaseDefinitionID > 0 && FormType == null)
            {
                FormType = FormTypeBL.GetByPhaseDefinitionID(uow, PhaseDefinitionID);
                FormTypeID = FormType.FormTypeID;
                ViewBag.FormTypeID = FormType.FormTypeID;
            }

            if (TransferID <= 0)
            {
                int clientid = System.Web.HttpContext.Current.Request.GetRequestVariable<int>("clientid", -1);
                if (clientid > 0)
                {
                    var transferfromclient = TransferBL.GetByClientID(uow, clientid);
                    if (transferfromclient != null)
                    {
                        TransferID = transferfromclient.TransferID;
                    }
                }
            }

            if (TransferID <= 0)
            {
                //Nog steeds 0 daarom geen dossier
                return;
            }

            ShowHistory = System.Web.HttpContext.Current.Request.GetRequestVariable<bool>("ShowHistory", false);
            if (ShowHistory)
            {
                HistoryFromDate = System.Web.HttpContext.Current.Request.GetRequestVariable<DateTime?>("HistoryFromDate", null);
            }

            var pointuserinfo = PointUserInfo();
            if (Request.RequestType == "POST")
            {
                SetModelState(pointuserinfo);
            }
            
            var cachestring = string.Concat("currentFlowInstance_", pointuserinfo.Employee.UserID, "_", TransferID);
            FlowInstance = CacheService.Instance.Get<FlowInstance>(cachestring);
            if (FlowInstance == null)
            {
                FlowInstance = SearchBL.GetSecurityTrimmedFlowInstance(uow, pointuserinfo, TransferID);
                if (FlowInstance != null)
                {
                    CacheService.Instance.Insert(cachestring, FlowInstance, "CacheShort");
                }
            }
            else
            {
                //Refresh allways (But skip security because we allready have rights)
                FlowInstance = FlowInstanceBL.GetByID(uow, FlowInstance.FlowInstanceID);
            }

            if (FlowInstance == null)
            {
                var uncheckedflowinstance = FlowInstanceBL.GetByTransferIDWithDeleted(uow, TransferID);
                if(uncheckedflowinstance == null)
                {
                    throw new PointSecurityException("Dit dossier bestaat niet", ExceptionType.PageNotFound);
                }

                if (uncheckedflowinstance.Deleted == true)
                {
                    throw new PointDossierDeletedException($"Dossier {TransferID} is verwijderd en is niet meer toegankelijk.");
                }
                else if(uncheckedflowinstance.OrganizationInvite.Any(inv => pointuserinfo.EmployeeOrganizationIDs.Contains(inv.OrganizationID)))
                {
                    throw new PointSecurityException($"Dossier {TransferID} is ingetrokken en is daardoor niet meer toegankelijk.", ExceptionType.AccessDeniedDifferentVVT);
                }
                else if(uncheckedflowinstance.Deleted == false)
                {
                    throw new PointSecurityException("U heeft geen toegang tot dit dossier", ExceptionType.AccessDenied);
                }
            }

            if (FormType != null)
            {
                SignaleringTrigger = SignaleringTriggerBL.GetByFlowDefinitionIDAndFormTypeID(uow, FlowInstance, FormType.FormTypeID);
            }
            else if (GeneralActionID > 0)
            {
                SignaleringTrigger = SignaleringTriggerBL.GetByFlowDefinitionIDAndGeneralActionID(uow, FlowInstance, GeneralActionID);
            }

            if (System.Web.HttpContext.Current.Request.GetRequestVariable<bool>("SendSignal", false))
            {
                SendSignal = true;
                SendSignalMessage = System.Web.HttpContext.Current.Request.GetRequestVariable<string>("SignaleringMessage", "");
            }

            if (System.Web.HttpContext.Current.Request.GetRequestVariable<bool>("ShowValidationErrors", false))
            {
                ShowValidationErrors = true;
            }

            var phasedefinition = PhaseDefinitionCacheBL.GetByID(uow, PhaseDefinitionID);
            if (System.Web.HttpContext.Current.Request.GetRequestVariable<bool>("handleSave", false))
            {
                SaveHandled = true;
                if (SendSignal)
                {
                    SignaleringBL.HandleSignalByFlowDefinitionFormTypeAction(uow, FlowInstance, phasedefinition.FormTypeID, "Save", pointuserinfo, SendSignalMessage);
                }
            }
            else if (System.Web.HttpContext.Current.Request.GetRequestVariable<bool>("handleNextPhase", false))
            {
                var currentPhaseInstance = PhaseInstanceBL.GetById(uow, PhaseInstanceID);
                // TODO: #17148 Technical debt: Form als bijlage
                // HACK: #17150 RzTP VO als bijlage fails validation (externalform)
                var externalform = System.Web.HttpContext.Current.Request.GetRequestVariable<bool>("externalform", false);
                var nextphasedefinitionid = System.Web.HttpContext.Current.Request.GetRequestVariable<int>("nextPhaseDefinitionID", -1);
                if (nextphasedefinitionid > 0 && (externalform || CurrentModelState.Errors.Count == 0))
                {
                    var logentry = LogEntryBL.Create(FormType, pointuserinfo);

                    if (SendSignal)
                    {
                        SignaleringBL.HandleSignalByFlowDefinitionFormTypeAction(uow, FlowInstance, currentPhaseInstance.PhaseDefinition.FormTypeID, "NextPhase", pointuserinfo, SendSignalMessage);
                    }

                    if (System.Web.HttpContext.Current.Request.GetRequestVariable<bool>("issamephase", false))
                    {
                        PhaseInstanceBL.SetStatusDone(uow, currentPhaseInstance, logentry);
                    }
                    else
                    {
                        PhaseInstanceBL.HandleNextPhase(uow, currentPhaseInstance.FlowInstanceID, PhaseInstanceID, nextphasedefinitionid, pointuserinfo.Employee.EmployeeID, logentry);
                    }

                    NextPhaseHandled = true;
                    ResetOutputCacheByTransferID(TransferID);
                }
            }
            else if (System.Web.HttpContext.Current.Request.GetRequestVariable<bool>("handledefinitive", false))
            {
                PhaseInstance currentPhaseInstance = null;
                if (PhaseInstanceID > 0)
                {
                    currentPhaseInstance = PhaseInstanceBL.GetById(uow, PhaseInstanceID);
                    if (CurrentModelState.Errors.Count == 0 )
                    {
                        var logEntry = LogEntryBL.Create(FormType, pointuserinfo);
                        PhaseInstanceBL.SetStatusDone(uow, currentPhaseInstance, logEntry);
                        DefinitiveHandled = true;
                    }
                }
                else
                {
                    if (CurrentModelState.Errors.Count == 0)
                    {
                        var logentry = LogEntryBL.Create(FormType, pointuserinfo);
                        if (FlowInstance != null)
                        {
                            currentPhaseInstance = PhaseInstanceBL.Create(uow, FlowInstance.FlowInstanceID, PhaseDefinitionID, logentry, Status.Done);
                        }
                        if (currentPhaseInstance != null)
                        {
                            PhaseInstanceID = currentPhaseInstance.PhaseInstanceID;
                        }
                        DefinitiveHandled = true;
                    }
                }

                if (SendSignal)
                {
                    SignaleringBL.HandleSignalByFlowDefinitionFormTypeAction(uow, FlowInstance, phasedefinition.FormTypeID, "Definitive", pointuserinfo, SendSignalMessage);
                }

                ResetOutputCacheByTransferID(TransferID);
            }
            if (TransferID > 0)
            {
                ViewTransferHistoryBL.Save(pointuserinfo.Employee.EmployeeID, TransferID);
            }
        }

        public void SetReadMode(bool readMode = true)
        {
            ViewData.SetReadMode(readMode);
        }

        public void SetIsClosed(bool isClosed = true)
        {
            ViewData.SetIsClosed(isClosed);
        }

        public void SetPageLock(int pagelocktimer, bool pageLock = true, int? pageLockID = null)
        {
            ViewData.SetPageLock(pagelocktimer, pageLock, pageLockID);
        }

        public void HandleThirdPartyAsync(int formSetVersionID, PointUserInfo pointUserInfo)
        {
            Task.Run(async () =>
            {
                await CommunicationQueueBL.AddToQueueAsync(formSetVersionID, pointUserInfo.Employee.EmployeeID);
            });
        }
    }


    public class SecureContentResult : SecureActionResult
    {
        public SecureContentResult() { }
        public SecureContentResult(string content)
        {
            Content = content;
        }
        public string Content { get; set; }
        public Encoding ContentEncoding { get; set; }
        public string ContentType { get; set; }

        public override void ExecuteResult(ControllerContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            var contentresult = new ContentResult() { Content = Content, ContentEncoding = ContentEncoding, ContentType = ContentType };
            contentresult.ExecuteResult(context);
        }
    }

    public class SecureJsonResult : SecureActionResult
    {
        public Encoding ContentEncoding { get; set; }
        public string ContentType { get; set; }
        public object Data { get; set; }
        public JsonRequestBehavior JsonRequestBehavior { get; set; }
        public int? MaxJsonLength { get; set; }
        public int? RecursionLimit { get; set; }

        public JsonConverter[] JsonConverters { get; set; }


        public SecureJsonResult() { }

        public SecureJsonResult(object data)
        {
            Data = data;
        }

        public SecureJsonResult(object data, JsonRequestBehavior jsonRequestBehavior)
        {
            Data = data;
            JsonRequestBehavior = jsonRequestBehavior;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            if (JsonConverters == null || JsonConverters.Length == 0)
            {
                // Handle it the normal MVC-way
                var jsonresult = new JsonResult()
                {
                    ContentEncoding = ContentEncoding,
                    ContentType = ContentType,
                    Data = Data,
                    JsonRequestBehavior = JsonRequestBehavior,
                    MaxJsonLength = MaxJsonLength,
                    RecursionLimit = RecursionLimit
                };
                jsonresult.ExecuteResult(context);
            }
            else
            {
                // Fancier with Newtonsoft
                HttpResponseBase response = context.HttpContext.Response;

                if (!String.IsNullOrEmpty(ContentType))
                {
                    response.ContentType = ContentType;
                }
                else
                {
                    response.ContentType = "application/json";
                }
                if (ContentEncoding != null)
                {
                    response.ContentEncoding = ContentEncoding;
                }
                if (Data != null)
                {
                    response.Write(JsonConvert.SerializeObject(Data, JsonConverters));
                }
            }

        }

    }


    public class SecureActionResult : ActionResult
    {
        public string MasterName { get; set; }
        private string viewName { get; set; }
        public IView View { get; set; }
        public object Model { get; set; }
        public bool AsPartial { get; set; }

        public SecureActionResult(bool asPartial = false) { AsPartial = asPartial; }
        public SecureActionResult(string viewName, bool asPartial = false) { this.viewName = viewName; AsPartial = asPartial; }
        public SecureActionResult(object model, bool asPartial = false) { Model = model; AsPartial = asPartial; }
        public SecureActionResult(string viewName, object model, bool asPartial = false) { this.viewName = viewName; Model = model; AsPartial = asPartial; }

        public void SetReadMode(PointFormController controller, bool readMode = true)
        {
            controller.SetReadMode(readMode);
        }

        public void SetIsClosed(PointFormController controller, bool isClosed = true)
        {
            controller.SetIsClosed(isClosed);
        }

        public void SetPageLock(PointFormController controller, int pagelocktimer, bool pageLock = true, int? pageLockID = null)
        {
            controller.SetPageLock(pagelocktimer, pageLock, pageLockID);
        }

        public override void ExecuteResult(ControllerContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            var controller = context.Controller as PointFormController;
            if (controller != null)
            {
                var pointuserinfo = controller.PointUserInfo();
                SetFormRights(controller, pointuserinfo);
                SetPageLocking(controller, pointuserinfo);
            }

            if (string.IsNullOrEmpty(viewName))
            {
                viewName = context.RouteData.GetRequiredString("action");
            }

            ViewEngineResult result = null;

            if (View == null)
            {
                result = FindView(context);
                View = result.View;
            }

            if (controller is PointController pointController && Model is GlobalViewModel globalViewModel)
            {
                pointController.GlobalPropertiesUpdateFromViewData(globalViewModel.Global);
            }

            context.Controller.ViewData.Model = Model;

            using (var stringwriter = new StringWriter())
            {
                var viewContext = new ViewContext(context, View, context.Controller.ViewData, context.Controller.TempData, stringwriter);
                View.Render(viewContext, context.HttpContext.Response.Output);

                result?.ViewEngine.ReleaseView(context, View);
            }
        }

        protected ViewEngineResult FindView(ControllerContext context)
        {
            var result = AsPartial ? ViewEngines.Engines.FindPartialView(context, viewName) : ViewEngines.Engines.FindView(context, viewName, MasterName ?? "");

            if (result?.View != null)
            {
                return result;
            }

            return null;
        }

        protected void SetPageLocking(PointFormController controller, PointUserInfo pointuserinfo)
        {
            if (controller.FormType == null || controller.FlowInstance == null || controller.ViewData.IsReadMode())
            {
                return;
            }

            if (controller.RouteData.GetValue<bool>("printmode", false))
            {
                return;
            }

            if (pointuserinfo.Employee.EmployeeID == PointUserInfoHelper.GetSystemEmployeeID())
            {
                return;
            }

            var pageLockTimeoutTimer = TransferBL.GetPageLockTimeout(controller.uow, controller.TransferID);
            if (pageLockTimeoutTimer <= 0 || !controller.FormType.HasPageLocking) 
            {
                return;
            }

            var isPageLocked = false;
            var pageLock = PageLockBL.GetByTransferIDAndFormType(controller.uow, controller.TransferID, controller.FormType);
            if (pageLock == null)
            {
                pageLock = PageLockBL.Insert(controller.uow, controller.TransferID, controller.FormType, HttpContext.Current.Session.SessionID);
            }
            else if (pageLock.EmployeeID != pointuserinfo.Employee.EmployeeID)
            {
                isPageLocked = true;
                PageLockBL.DeleteByEmployeeID(controller.uow, pointuserinfo.Employee.EmployeeID);
            }

            SetPageLock(controller, pageLockTimeoutTimer, isPageLocked, pageLock.PageLockID);
        }

        protected void SetFormRights(PointFormController controller, PointUserInfo pointuserinfo)
        {
            if (controller.FormType == null && controller.FlowInstance == null) return;

            if (controller.FlowInstance != null && controller.FormType != null)
            {
                var isclosed = FlowInstanceBL.IsFlowClosed(controller.uow, controller.FlowInstance.FlowInstanceID);
                if (isclosed && controller.FormType.TypeID != (int)TypeID.FlowDoorlopendForm)
                {
                    SetIsClosed(controller);
                    SetReadMode(controller);
                    return;
                }
            }

            var phaseinstance = PhaseInstanceBL.GetById(controller.uow, controller.PhaseInstanceID);
            if (phaseinstance != null && phaseinstance.Status == (int)Status.Done && phaseinstance.PhaseDefinition.ReadOnlyOnDone.GetValueOrDefault())
            {
                SetReadMode(controller);
                return;
            }

            if (pointuserinfo.IsGlobalAdmin || pointuserinfo.IsRegioAdmin)
            {
                return;
            }

            var phasedefinition = PhaseDefinitionCacheBL.GetByID(controller.uow, controller.PhaseDefinitionID);
            if (phasedefinition != null)
            {
                var rights = PhaseDefinitionRightsBL.GetFormRightsPhaseDefinitionID(controller.uow, pointuserinfo, phasedefinition, controller.FlowInstance);
                if (rights <= Rights.R)
                {
                    SetReadMode(controller);
                }
            } else if(controller.FlowInstance != null)
            {
                var rights = PhaseDefinitionRightsBL.GetBasicFlowRightsByFlowInstance(controller.uow, pointuserinfo, controller.FlowInstance);
                if (rights <= Rights.R)
                {
                    SetReadMode(controller);
                }
            }
        }
    }
}