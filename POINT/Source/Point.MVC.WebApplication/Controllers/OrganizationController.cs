﻿using System.Web.Mvc;
using Point.Business.Logic;
using Point.Database.Models.ViewModels;
using Point.Infrastructure.Attributes;
using Point.MVC.WebApplication.Helpers;

namespace Point.MVC.WebApplication.Controllers
{
    public class OrganizationController : PointController
    {
        [ValidateAntiFiddleInjection("departmentid")]
        public ActionResult DepartmentDetails(int departmentid)
        {
            var department = DepartmentBL.GetByDepartmentID(uow, departmentid);
            return new SecureActionResult(department, Request.IsAjaxRequest());
        }

        public JsonResult DeleteInvite(int organizationinviteid)
        {
            var success = OrganizationInviteBL.Delete(uow, organizationinviteid);

            return Json(new {Success = success}, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Participants(OrganizationParticipantsViewModel viewmodel)
        {
            viewmodel = OrganizationViewModelHelper.GetParticipantsViewModel(viewmodel, PointUserInfo());

            return new SecureActionResult("Participants", viewmodel, Request.IsAjaxRequest());
        }
    }
}