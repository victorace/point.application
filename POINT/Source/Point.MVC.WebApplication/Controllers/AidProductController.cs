﻿using Point.Business.Helper;
using Point.Business.Logic;
using Point.Database.Context;
using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Extensions;
using Point.MVC.WebApplication.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Point.Communication.Infrastructure.Helpers;
using Point.Database.Helpers;
using Point.Infrastructure.Helpers;
using Point.MVC.WebApplication.Helpers;

namespace Point.MVC.WebApplication.Controllers
{
    public class AidProductController : Controller
    {
        private readonly UnitOfWork<PointContext> uow = new UnitOfWork<PointContext>();
        private PointUserInfo pointUserInfo(bool usecache = true)
        {
            return usecache ? PointUserInfoHelper.GetPointUserInfo(uow) : PointUserInfoHelper.GetPointUserInfoNoCache(uow);
        }

        private int[] getProductIDs(string productids)
        {
            if (String.IsNullOrEmpty(productids))
            {
                return new int[0];
            }
            return productids.Split(',').Where(it => !String.IsNullOrEmpty(it)).Select(it => Convert.ToInt32(it)).ToArray();
        }

        protected override void Dispose(bool disposing)
        {
            uow.SafeDispose();
            base.Dispose(disposing);
        }

        public PartialViewResult AddItem(SupplierID supplierID, int uzovi)
        {
            var viewmodel = AidProductViewModelHelper.GetAidProductSearchViewModel(uow, (int)supplierID, uzovi);
            viewmodel.AidProducts = supplierID == SupplierID.MediPoint ? AidProductViewModelHelper.GetProductsBySupplierIDAndUzovi(uow, (int)supplierID, uzovi)  : AidProductViewModelHelper.GetProductsBySupplierID(uow, (int)supplierID);
            return PartialView(viewmodel);
        }


        public PartialViewResult NewOrderedItem(int supplierID, int transferid, int aidproductid, int uniqueFormKey, string supplierinsurercode, int? formSetVersionID)
        {
            var aidproduct = AidProductBL.GetProductByID(uow, aidproductid);

            var viewmodel = AidProductOrderItemViewModel.FromModel(aidproduct);

            //Update ProductGroupCode and ExtraItems in answered questions
            if (!string.IsNullOrEmpty(supplierinsurercode))
            {
                var possiblenewquestions = AidProductQuestionHelper.GetQuestions(uow, supplierID, transferid, new int[] { aidproductid }, supplierinsurercode, uniqueFormKey, formSetVersionID);
                var currentanswers = AidProductBL.GetAnswers(uow, uniqueFormKey, formSetVersionID);
                foreach (var currentanswer in currentanswers)
                {
                    var matchingnewquestion = possiblenewquestions.FirstOrDefault(it => it.QuestionID == currentanswer.QuestionID);
                    if (matchingnewquestion != null)
                    {
                        currentanswer.ProductGroupCode += "," + matchingnewquestion.ProductGroupCode;
                        if (matchingnewquestion.PossibleAnswers.Any(it => it.ExtraItems && it.Value == currentanswer.Value))
                        {
                            currentanswer.ExtraItems = true;
                            currentanswer.ExtraItemsGroupCode = String.IsNullOrEmpty(currentanswer.ExtraItemsGroupCode) ? matchingnewquestion.ProductGroupCode : (currentanswer.ExtraItemsGroupCode + "," + matchingnewquestion.ProductGroupCode);
                        }
                    }
                }
                uow.Save();
            }

            return PartialView("_OrderedItem", viewmodel);
        }

        [OutputCache(Duration = 3600, VaryByParam = "aidproductid")]
        public ActionResult Picture(int aidproductid)
        {
            var aidproductimage = AidProductBL.GetProductImageByID(uow, aidproductid);
            var mimetype = FileHelper.GetImageMimeType(aidproductimage);
            return File(aidproductimage, mimetype);
        }

        public JsonResult SaveAnswer(AidProductAnswerViewModel viewmodel, int uniqueFormKey, int? formSetVersionID)
        {
            AidProductBL.SaveAnswer(uow, viewmodel, uniqueFormKey, formSetVersionID);

            return Json(new { Success = true }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteAllAnswers(int uniqueFormKey)
        {
            AidProductBL.DeleteAllAnswers(uow, uniqueFormKey);

            return Json(new { Success = true }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetUnansweredQuestions(int supplierID, int transferID, string productIDs, string supplierInsurerCode, int uniqueFormKey, int? formSetVersionID)
        {
            var questions = GetQuestions(supplierID, transferID, productIDs, supplierInsurerCode, uniqueFormKey, formSetVersionID);
            var unansweredQuestions = AidProductQuestionViewModelHelper.GetUnansweredQuestions(supplierID, questions);

            var jsonObj = new { UnansweredQuestions = unansweredQuestions, HasQuestions = questions.Any()};

            return Json(jsonObj, JsonRequestBehavior.AllowGet);
        }
        public PartialViewResult Questions(int supplierID, int transferID, string productIDs, string supplierInsurerCode, int uniqueFormKey, bool readMode = false, int? formSetVersion = null)
        {
            var questions = GetQuestions(supplierID, transferID, productIDs, supplierInsurerCode, uniqueFormKey, formSetVersion);
            ViewData.SetReadMode(readMode);

            return PartialView(questions);
        }

        private List<AidProductQuestionViewModel> GetQuestions(int supplierID, int transferID, string productIDs, string supplierInsurerCode, int uniqueFormKey, int? formSetVersionID)
        {
            var questions = new List<AidProductQuestionViewModel>();

            if (string.IsNullOrEmpty(productIDs))
            {
                return questions;
            }

            int[] productIDsArray = productIDs.Split(',').Where(it => !string.IsNullOrEmpty(it)).Select(it => Convert.ToInt32(it)).ToArray();
            if (productIDsArray.Any())
            {
                questions = AidProductQuestionHelper.GetQuestions(uow, supplierID, transferID, productIDsArray, supplierInsurerCode, uniqueFormKey, formSetVersionID);
            }

            return questions;
        }

        public JsonResult GetTransportStatus(string productids)
        {
            decimal transportcosts = 0;
            bool hastransportcosts = false;
            int[] productidsarray = getProductIDs(productids);

            if (productidsarray.Length > 0)
            {
                var products = AidProductBL.GetProductsByIDs(uow, productidsarray);
                //(PBI 14530) Zodra er 1 of meerdere in zitten zonder transportkosten 
                //dan hoeft er over de hele bestelling geen transportkosten te worden betaald 
                hastransportcosts = products.Any() && products.All(it => it.TransportCosts > 0);
                transportcosts = hastransportcosts ? products.Max(it => it.TransportCosts) : 0;
            }

            return Json(new { TransportCosts = transportcosts, HasTransportCosts = hastransportcosts }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAuthorizationFormStatus(string productids)
        {
            int authorizationformid = -1;
            bool hasauthorizationform = false;
            int[] productidsarray = getProductIDs(productids);

            if (productidsarray.Length > 0)
            {
                var products = AidProductBL.GetProductsByIDs(uow, productidsarray);
                hasauthorizationform = products.Any(it => it.AuthorizationForm);
            }
            return Json(new { AuthorizationFormID = authorizationformid, HasAuthorizationForm = hasauthorizationform }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetOrderStatus(int formSetVersionID)
        {
            var orderStatusResult = AidProductBL.GetOrderStatusWithMessageIndication(uow, formSetVersionID, updateStatus: true);
            if (orderStatusResult.NeedOrderUpdate)
            {
                new Communication.SupplierSendOrder().UpdateOrderStatus(uow, formSetVersionID, FlowFieldConstants.Value_OrderMislukt);
            }

            return Json(new { OrderStatus = orderStatusResult.OrderStatus }, JsonRequestBehavior.AllowGet);
        }



    }
}