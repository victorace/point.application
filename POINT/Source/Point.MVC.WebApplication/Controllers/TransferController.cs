﻿using DevTrends.MvcDonutCaching;
using Point.Business.Helper;
using Point.Business.Logic;
using Point.Database.Extensions;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Infrastructure.Extensions;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using System.Threading.Tasks;
using Point.MVC.WebApplication.Helpers;
using System.Net;
using Point.Infrastructure.Exceptions;

namespace Point.MVC.WebApplication.Controllers
{
    [DonutOutputCache(CacheProfile = "Short")]
    public class TransferController : PointController
    {
        public TransferController()
        {
            ViewBag.Title = PageTitleBL.GetTitleFromTitle("Zoeken");
        }
        
        public JsonResult GetValues(string name, string flowdefinitionids = "")
        {
            var pointUserInfo = PointUserInfo();

            JsonResult data = null;

            switch (name.ToLower())
            {
                case "destinationhospital":
                    var hospitals = OrganizationBL.GetActiveByOrganizationTypeID(uow, (int)OrganizationTypeID.Hospital).OrderBy(it => it.Name).ToList();
                    data = new JsonResult
                    {
                        Data = (from hospital in hospitals select new Option(hospital.Name, hospital.OrganizationID.ToString())).ToList()
                    };
                    break;
                case "acceptedby":
                    var employees = EmployeeBL.GetTransferPointEmployeesByOrganizationID(uow, pointUserInfo.Organization.OrganizationID).ToList();
                    data = new JsonResult
                    {
                        Data = (from employee in employees select new Option(employee.FullName(), employee.EmployeeID.ToString())).ToList()
                    };
                    break;
                case "requestformzhvvttype":
                    
                    if (!string.IsNullOrEmpty(flowdefinitionids))
                    {
                        var flowdefinitionIDList = new List<FlowDefinitionID>();
                        var parts = flowdefinitionids.Split(',').Where(s => !s.IsNullOrEmpty());
                        foreach (var part in parts)
                        {
                            if (Enum.TryParse(part, out FlowDefinitionID flowdefinitionid))
                            {
                                flowdefinitionIDList.Add(flowdefinitionid);
                            }
                        }
                        if (flowdefinitionIDList.Any())
                        {
                            var formTypes = FormTypeBL.GetRequestFormsByOrganizationIDAndFlowDefinitionIDs(uow, pointUserInfo.EmployeeOrganizationIDs.FirstOrDefault(), flowdefinitionIDList.ToArray());
                            data = new JsonResult
                            {
                                Data = (from formType in formTypes select new Option(formType.Name, formType.FormTypeID.ToString())).ToList()
                            };
                        }
                    }
                    break;

                default:
                    var flowFieldAttribute = FlowFieldAttributeBL.GetByName(uow, name);
                    var flowfieldattributeexception = flowFieldAttribute.FlowFieldAttributeException.FilterSingleByPointUser(pointUserInfo);
                    var flowWebField = FlowWebFieldBL.FromModel(uow, flowFieldAttribute, flowfieldattributeexception);

                    data = new JsonResult
                    {
                        Data = flowWebField.FlowFieldDataSource?.Options
                    };
                        
                    break;
            }

            return data;
        }
        
        public ActionResult Index()
        {
            return RedirectToAction("Search");
        }

        public ActionResult Null()
        {
            return RedirectToAction("Search");
        }

        public JsonResult GetPhases(SearchControl searchcontrol)
        {
            var options = TransferViewModelHelper.GetActivePhaseList(this, searchcontrol.FlowDefinitionIDs, searchcontrol.phases);
            return Json(new { Options = options }, JsonRequestBehavior.AllowGet);
        }

        public ViewResult Overview(SearchUrlViewModel viewmodel)
        {
            return View(TransferViewModelHelper.GetOverviewViewModel(this, viewmodel));
        }
        
        public ViewResult Search(SearchUrlViewModel viewmodel)
        {
            PageTitle = "Zoeken";
            return View(TransferViewModelHelper.GetSearchViewModel(this, viewmodel));
        }

        public async Task<PartialViewResult> SearchResultPatientPrevious(SearchStatement searchStatement)
        {
            var pointUserInfo = PointUserInfo();
            var flowdefinitionids = FlowDefinitionBL.GetSendingFlowDefinitionsByLocationIDDepartmentID(uow, pointUserInfo.Location.LocationID, pointUserInfo.Department.DepartmentID).Select(fd => fd.FlowDefinitionID); 
            var searchuiconfig = SearchUIConfigurationBL.GetConfigurationOrDefault(uow, flowdefinitionids.ToArray(), SearchType.PatientOverview, pointUserInfo);
            bool havepatexternalreference = ExternalPatientHelper.HavePatExternalReference(); // Note: we're losing httpcontext after calling an async static, so get the value while we still can.

            var viewtransferhistory = ViewTransferHistoryBL.GetLastViewTransferHistory(uow, pointUserInfo.Employee.EmployeeID);

            SearchQueryBL searchquerybl = SearchQueryBL.GetSearchQuerySimple(uow);
            searchquerybl.ApplyTransfersFilter(viewtransferhistory);
            searchquerybl.ApplyFilter(pointUserInfo, DossierStatus.All, FlowDirection.Any);

            int resultCount = await searchquerybl.Queryable.CountAsync();
            var searchresults = SearchBL.ToSearchResultEntriesTable(searchquerybl.Queryable, searchStatement, searchuiconfig.PageSize).ToList()
                                        .OrderBy(sr => viewtransferhistory.ToList().IndexOf(sr.FlowInstanceSearchValues.TransferID)).ToList();

            TransferViewModelHelper.SetAnonymous(searchresults, pointUserInfo.Organization.OrganizationID);

            var searchViewModel = TransferViewModelHelper.GetSearchViewModel(searchresults, resultCount, searchStatement, searchuiconfig, usePagination: true);
            searchViewModel.HavePatExternalReference = havepatexternalreference;

            return PartialView("SearchResult", searchViewModel);
        }

        public async Task<PartialViewResult> SearchResultPatientOverview(SearchStatement searchStatement, int? transferID = null) 
        {
            var pointUserInfo = PointUserInfo();
            var departmentIDs = new List<int>();
            bool havePatExternalReference = ExternalPatientHelper.HavePatExternalReference(); // Note: we're losing httpcontext after calling an async static, so get the value while we still can.
            
            if (departmentIDs.Count == 0 && transferID != null)
            {
                var department = DepartmentBL.GetSendingDepartmentByTransferID(uow, transferID.Value);
                if (department != null)
                {
                    departmentIDs.Add(department.DepartmentID);
                }
            }

            if (departmentIDs.Count == 0 && ExternalPatientHelper.HavePatExternalReference())
            {
                SearchQueryBL searchqueryblexternalreference = SearchQueryBL.GetSearchQuerySimple(uow);
                searchqueryblexternalreference.ApplyFilter(pointUserInfo, DossierStatus.Active, FlowDirection.Sending);
                searchqueryblexternalreference.ApplyCivilServiceNumberOrPatientNumberFilter(ExternalPatientHelper.GetPatExternalReferenceBSN(), ExternalPatientHelper.GetPatExternalReference());
                departmentIDs.AddRange(searchqueryblexternalreference.Queryable.SelectMany(fisvm => fisvm.FlowInstanceSearchValues.FlowInstanceOrganization)
                                       .Where(fio => fio.FlowDirection == FlowDirection.Sending || fio.FlowDirection == FlowDirection.Any).Select(fio => fio.DepartmentID).ToList());
            }

            var tmpDepartmentID = LoginBL.GetDepartmentIDReference();
            if (tmpDepartmentID.HasValue)
            {
                departmentIDs.Add(tmpDepartmentID.Value);
            }

            var flowDefinitionIDs = FlowDefinitionBL.GetSendingFlowDefinitionsByLocationIDDepartmentID(uow, pointUserInfo.Location.LocationID, pointUserInfo.Department.DepartmentID).Select(fd => fd.FlowDefinitionID);
            var searchUiConfig = SearchUIConfigurationBL.GetConfigurationOrDefault(uow, flowDefinitionIDs.ToArray(), SearchType.PatientOverview, pointUserInfo);

            SearchQueryBL searchQueryBl = SearchQueryBL.GetSearchQuerySimple(uow);
            searchQueryBl.ApplyFilter(pointUserInfo, DossierStatus.Active, FlowDirection.Sending);
            searchQueryBl.ApplyDepartmentFilter(departmentIDs.ToArray());

            SearchQueryBL searchQueryBlOwnedByMe = SearchQueryBL.GetSearchQuerySimple(uow);
            searchQueryBlOwnedByMe.ApplyFilter(pointUserInfo, DossierStatus.Active, FlowDirection.Sending);
            searchQueryBlOwnedByMe.ApplyHandlingFilter(DossierHandling.OwnedByMe, pointUserInfo);

            
            int resultCount = await searchQueryBl.Queryable.CountAsync();
            var searchResults = SearchBL.ToSearchResultEntriesTable(searchQueryBl.Queryable, searchStatement, searchUiConfig.PageSize).ToList();
            var searchResultsOwnedByMe = SearchBL.ToSearchResultEntriesTable(searchQueryBlOwnedByMe.Queryable, searchStatement, searchUiConfig.PageSize).ToList();
            searchResultsOwnedByMe.RemoveAll(fisvm => searchResults.Select(result => result.FlowInstanceSearchValues.FlowInstanceID).Contains(fisvm.FlowInstanceSearchValues.FlowInstanceID));
            resultCount += searchResultsOwnedByMe.Count;
            searchResults.AddRange(searchResultsOwnedByMe);
            TransferViewModelHelper.SetAnonymous(searchResults, pointUserInfo.Organization.OrganizationID);

            var searchViewModel = TransferViewModelHelper.GetSearchViewModel(searchResults, resultCount, searchStatement, searchUiConfig, usePagination: true);
            searchViewModel.HavePatExternalReference = havePatExternalReference;

            return PartialView("SearchResult", searchViewModel);
        }

        public async Task<PartialViewResult> SearchResult(SearchControl searchControl)
        {
            var pointUserInfo = PointUserInfo();

            if (ExternalPatientHelper.HavePatExternalReference())
            {
                searchControl.status = DossierStatus.All;
            }
            else if (!DossierStatusFilterHelper.IsScopeStatusFilter(searchControl.status))
            {
                searchControl.status = searchControl.statusFilter;
            }

            SearchQueryBL searchQueryBl;

            if (searchControl.SearchType == SearchType.Frequency)
            {
                searchQueryBl = SearchQueryBL.GetSearchQueryDoorlopendDossier(uow);
                searchQueryBl.ApplyFilter(pointUserInfo, searchControl.frequencyStatusFilter, searchControl.FlowDirection, searchControl.FlowDefinitionIDs);
            }
            else
            {
                searchQueryBl = SearchQueryBL.GetSearchQuerySimple(uow);
                searchQueryBl.ApplyFilter(pointUserInfo, searchControl.status, searchControl.FlowDirection, searchControl.FlowDefinitionIDs);
            }

            searchQueryBl.ApplyHandlingFilter(searchControl.handling, pointUserInfo);

            if (searchControl.DepartmentIDs != null)
            {
                searchQueryBl.ApplyDepartmentFilter(searchControl.DepartmentIDs);
            }

            var patExternalReferenceBSN = ExternalPatientHelper.GetPatExternalReferenceBSN();
            var isEmptyPatExternalReferenceBSN = string.IsNullOrEmpty(patExternalReferenceBSN);
            var patExternalReference = ExternalPatientHelper.GetPatExternalReference();
            var isEmptyPatExternalReference  = string.IsNullOrEmpty(patExternalReference);

            if (!isEmptyPatExternalReferenceBSN && !isEmptyPatExternalReference)
            {
                searchQueryBl.ApplyCivilServiceNumberOrPatientNumberFilter(ExternalPatientHelper.GetPatExternalReferenceBSN(), ExternalPatientHelper.GetPatExternalReference());
            }
            else if (!isEmptyPatExternalReferenceBSN)
            {
                searchQueryBl.ApplyCivilServiceNumberFilter(patExternalReferenceBSN);
            }
            else if (!isEmptyPatExternalReference)
            {
                searchQueryBl.ApplyPatientNumberFilter(patExternalReference);
            }

            TransferViewModelHelper.ApplyFilters(this, searchControl, searchQueryBl, pointUserInfo);

            int resultCount = await searchQueryBl.Queryable.CountAsync();
            var searchUiConfig = SearchUIConfigurationBL.GetConfigurationOrDefault(uow, searchControl.FlowDefinitionIDs, searchControl.SearchType, pointUserInfo);
            var searchResults = SearchBL.ToSearchResultEntriesTable(searchQueryBl.Queryable, searchControl.SearchStatement, searchUiConfig.PageSize).ToList();

            TransferViewModelHelper.SetAnonymous(searchResults, pointUserInfo.Organization.OrganizationID);

            var searchViewModel = TransferViewModelHelper.GetSearchViewModel(searchResults, resultCount, searchControl.SearchStatement, searchUiConfig, usePagination: true);

            SearchControlBL.SaveState(searchControl, this.HttpContext);

            return PartialView(searchViewModel);
        }
        
        public ActionResult RelatedTransfersWithVVT(int transferid, int maxnum = 7)
        {
            var noresults = new JsonResult { Data = new { no_results = true } };

            var client = ClientBL.GetByTransferID(uow, transferid);
            if (client == null || String.IsNullOrEmpty(client.CivilServiceNumber))
            {
                return noresults;
            }

            var flowinstance = FlowInstanceBL.GetByTransferID(uow, transferid);
            if (flowinstance == null)
            {
                return noresults;
            }

            var sendingorganization = OrganizationHelper.GetSendingOrganization(uow, flowinstance.FlowInstanceID);
            if (sendingorganization == null)
            {
                return noresults;
            }

            var relatedTransfers = FlowInstanceBL.GetRelatedTransfersWithVVTByBSN(uow, client.CivilServiceNumber, sendingorganization.OrganizationID, transferid);

            var output = relatedTransfers.OrderByDescending(fi => fi.TransferID).Take(maxnum).ToList();
            if (!output.Any())
            {
                return noresults;
            }

            var model = output.Select(s => RelatedTransferViewBL.FromModel(uow, s, sendingorganization.OrganizationID)).ToList();

            return PartialView(model);
        }

        public ActionResult LoseFocus()
        {
            ExternalPatientHelper.ClearPatExternalReference(false);
            ExternalPatientHelper.ClearPatExternalReferenceBSN(false);
            return RedirectToAction("Search");
        }
  
        public JsonResult RelatedTransferVVTVVTTransfer(int? transferid)
        {
            //Looking for previous transfers of the patient.
            //show them in a modal view if any exists.      
            var noresults = Json(new { URL = "" }, JsonRequestBehavior.AllowGet);
            if (!transferid.HasValue)
            {
                return noresults;
            }
           
            var currentTransfer = TransferBL.GetByTransferID(uow, transferid.Value);
            var bsn = currentTransfer?.Client?.CivilServiceNumber;
            var flowInstance = currentTransfer.FlowInstances.FirstOrDefault();

            if (FlowInstanceBL.IsFlowClosed(uow, flowInstance.FlowInstanceID))
            {
                return noresults;
            }
            
            if (string.IsNullOrEmpty(bsn) || flowInstance?.FlowDefinitionID != FlowDefinitionID.VVT_VVT || flowInstance?.RelatedTransferID != null)
            {
                return noresults;
            }

            var pointuserinfo = PointUserInfo();
            var sendingorganization = pointuserinfo.Organization;
            if (sendingorganization == null)
            {
                return noresults;
            }

            var relatedTransfers = FlowInstanceBL.GetRelatedTransfersVVT_VVTByBSN(uow, bsn, transferid, pointuserinfo);
            if (!relatedTransfers.Any())
            {
                return noresults;
            }

            var url = "/Transfer/RelatedTransferVVTVVT?BSN=" + bsn + "&TransferID=" + transferid;             
            return Json(new { URL = url }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult RelatedTransferVVTVVT(string bsn, int? transferid, int maxnum = 7)
        {
            //This function loads previouse related transfer in a view.
            //Get all previouse transfers of this pateint. no matter which flow.
            var noresults = new JsonResult { Data = new { no_results = true } };

            if (String.IsNullOrEmpty(bsn))
            {
                return noresults;
            }

            var pointuserinfo = PointUserInfo();
            var sendingorganization = pointuserinfo.Organization;
            if (sendingorganization == null)
            {
                return noresults;
            }

            var relatedTransfers = FlowInstanceBL.GetRelatedTransfersVVT_VVTByBSN(uow, bsn, transferid, pointuserinfo);
            if (!relatedTransfers.Any())
            {
                return noresults;
            }

            var model = relatedTransfers.Select(s => RelatedTransferViewBL.FromFlowInstanceSearchModel(uow, s.FlowInstanceSearchValues))?.Where(fi => fi.TransferID > 0)?.Take(maxnum)?.ToList();

            return PartialView(model);
        }

        public JsonResult SaveRelatedTransfer(int transferID, int relatedTransferID)
        {
            var noresults = Json(new { RelatedTransfer = "" }, JsonRequestBehavior.AllowGet);
            var flowInstance = FlowInstanceBL.SaveRelatedTransfer(uow, transferID, relatedTransferID);
            return Json(new { RelatedTransfer = flowInstance?.RelatedTransferID }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SourceAndDestination(int transferID)
        {
            var transfer = TransferBL.GetByTransferID(uow, transferID);
            var viewmodel = TransferInformationViewBL.FromModel(uow, transfer);
            return PartialView("SourceAndDestination", viewmodel);
        }
    }
}