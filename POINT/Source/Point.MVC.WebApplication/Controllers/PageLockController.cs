﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DevTrends.MvcDonutCaching;
using Point.Business.Logic;
using Point.Infrastructure.Extensions;

namespace Point.MVC.WebApplication.Controllers
{
    public class PageLockController : PointController
    {
        public ActionResult Timeout(Uri UrlReferrer)
        {
            if (string.IsNullOrWhiteSpace(UrlReferrer?.Query))
            {
                return PartialView();
            }

            var formType = FormTypeBL.GetByURL(uow, UrlReferrer);
            if (formType == null || !formType.HasPageLocking)
            {
                return PartialView();
            }

            ViewBag.FormName = formType.Name;

            var actionParams = HttpUtility.ParseQueryString(UrlReferrer.Query);
            var queryparameters = actionParams.AllKeys
                .ToDictionary(k => k.ToLowerInvariant(), k => actionParams[k])
                .Where(kv => !string.IsNullOrWhiteSpace(kv.Value))
                .Select(kv => kv).ToDictionary(kv => kv.Key, kv => kv.Value);

            if (!queryparameters.ContainsKey("formtypeid"))
            {
                queryparameters.Add("formtypeid", formType.FormTypeID.ToString());
            }

            if (!queryparameters.ContainsKey("formsetversionid"))
            {
                var formsetversion =
                    FormSetVersionBL.GetByTransferIDAndFormTypeID(uow, TransferID, formType.FormTypeID)
                        .OrderByDescending(fsv => fsv.CreateDate)
                        .FirstOrDefault();
                if (formsetversion != null)
                {
                    queryparameters.Add("formsetversionid", formsetversion.FormSetVersionID.ToString());
                }
            }

            if (queryparameters.ContainsKey("phasedefinitionid") && !queryparameters.ContainsKey("phaseinstanceid"))
            {
                if (int.TryParse(queryparameters["phasedefinitionid"], out int phasedefinitionid))
                {
                    var phaseinstance = PhaseInstanceBL.GetLastByTransferAndPhaseDefinition(uow, TransferID, phasedefinitionid);
                    if (phaseinstance != null)
                    {
                        queryparameters.Add("phaseinstanceid", phaseinstance.PhaseInstanceID.ToString());
                    }
                }
            }

            ViewBag.Url = Url.Action("StartFlowForm", "FlowMain", queryparameters.ToRouteValueDictionary());
            return PartialView();
        }

        public ActionResult Locked(int pageLockID)
        {
            var pageLock = PageLockBL.GetByID(uow, pageLockID);
            if (pageLock == null)
            {
                return PartialView();
            }

            var userinfo = PointUserInfo();

            return PartialView(PageLockViewBL.FromModel(uow, pageLock, userinfo));
        }

        [DonutOutputCache(Duration = 0)]
        public ActionResult RemovePageLockFromSession()
        {
            PageLockBL.DeleteBySessionID(uow, System.Web.HttpContext.Current.Session.SessionID);
            return Json(new {Status = "OK"}, JsonRequestBehavior.AllowGet);
        }

        [DonutOutputCache(Duration = 0)]
        public ActionResult RemovePageLockByID(int pagelockid)
        {
            PageLockBL.DeleteByPageLockID(uow, pagelockid);
            return Json(new { Status = "OK" }, JsonRequestBehavior.AllowGet);
        }
    }
}