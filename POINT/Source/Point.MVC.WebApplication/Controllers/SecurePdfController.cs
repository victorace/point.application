﻿using Point.Business.Helper;
using Point.Business.Logic;
using Point.Database.Models.ViewModels;
using Point.Infrastructure.Attributes;
using Point.MVC.WebApplication.Extensions;
using Point.MVC.WebApplication.Helpers.Pdf;
using System;
using System.Collections.Generic;
using System.Net;
using System.Security;
using System.Web.Mvc;
using System.Web.Security;
using Point.Infrastructure.Constants;

namespace Point.MVC.WebApplication.Controllers
{
    public class SecurePdfController : PointController
    {
        [ValidateAntiFiddleInjection("formsetversionid")]
        public ActionResult Connect()
        {
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [ValidateAntiFiddleInjection("formsetversionid")]
        public ActionResult Login(int formsetversionid, int employeeid)
        {
            PerformChecks(employeeid);

            var employee = EmployeeBL.GetByID(uow, employeeid);
            if (employee != null)
            {
                //All went well so far. Handle the PDF's by the systememployee from here
                var systememployee = PointUserInfoHelper.GetSystemEmployee(uow);
                if (systememployee != null)
                {
                    FormsAuthentication.SetAuthCookie(systememployee.Employee.UserName, false);
                    PointUserInfoHelper.SaveProviderUserKey(systememployee.Employee.UserID);
                }
            }

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [ValidateAntiFiddleInjection("formsetversionid")]
        public ActionResult GetFormSetVersion(int formSetVersionID, int employeeID)
        {
            DisableClientCache = true;

            PerformChecks(employeeID);

            var formsetversion = FormSetVersionBL.GetByID(uow, formSetVersionID);
            if (formsetversion == null)
            {
                return null;
            }

            var model = new PrintMultipleViewModel
            {
                FormSetVersions = new List<FormSetVersionContainer>(),
                TransferID = formsetversion.TransferID
            };

            var organization = OrganizationBL.GetByEmployeeID(uow, employeeID);
            var headermodel = ClientHeaderViewBL.GetViewModel(uow, formsetversion.TransferID, organization?.OrganizationID);
            var clientpartial = this.RenderViewToString("Header", headermodel);
            var footerpartial = this.RenderViewToString("Footer", null, true);

            var container = new FormSetVersionContainer
            {
                PrintName = formsetversion.GetPrintName(),
                FormsetVersionID = formsetversion.FormSetVersionID,
                PhaseDefinitionID = formsetversion.PhaseInstanceID.HasValue ? formsetversion.PhaseInstance.PhaseDefinitionID : -1,
                PhaseInstanceID = formsetversion.PhaseInstanceID ?? -1,
                FormTypeID = formsetversion.FormTypeID,
                ActionName = formsetversion.FormType.GetActionName(),
                ControllerName = formsetversion.FormType.GetControllerName(),
                EmployeeID = employeeID
            };

            model.FormSetVersions.Add(container);
            RouteData.Values.Add("PrintMode", true);

            return new ViewAsPdf(model, header: clientpartial, footer: footerpartial);
        }

        public ActionResult Disconnect()
        {
            Session.Abandon();
            FormsAuthentication.SignOut();
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [HttpPost]
        public ActionResult GetExceptionPDF(string errorcode)
        {
            string contents = $"<html><body><h2>Er is een fout opgetreden bij het samenstellen of verzenden van de pdf vanuit POINT</h2><br />Foutcode: {errorcode}<br />Tijdstip: {DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss")}</body></html>";

            return new ViewAsPdf((object)contents, null, "", "");
        }

        private void PerformChecks(int employeeid)
        {
            if (HttpContext.Request.Headers["DigestEmployee"] == null || !AntiFiddleInjection.CheckDigest(employeeid, HttpContext.Request.Headers["DigestEmployee"].ToString()))
            {
                throw new SecurityException("DigestEmployee check failed");
            }

            if (!AntiFiddleInjection.CheckDigest(HttpContext.Session.SessionID, HttpContext.Request.Headers["DigestSession"])
                || HttpContext.Session.SessionID != HttpContext.Request.Cookies[Infrastructure.Helpers.FormsetVersionPdfHelper.GetSessionCookieName()].Value)
            {
                throw new SecurityException("DigestSession check failed or SessionID mismatch");
            }

        }


    }
}