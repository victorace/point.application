﻿using Point.Business.BusinessObjects;
using Point.Business.Helper;
using Point.Business.Logic;
using Point.Database.Extensions;
using Point.Database.Models;
using Point.Database.Models.Constants;
using Point.Database.Models.ViewModels;
using Point.Infrastructure.Extensions;
using Point.Models.Enums;
using Point.MVC.WebApplication.Helpers;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;

namespace Point.MVC.WebApplication.Controllers
{
    public partial class FormTypeController
    {
        public ActionResult EOverdracht30()
        {
            return new SecureActionResult(FormTypeEOverdrachtViewModelHelper.GetEOverdracht30ViewModel(this, uow, GetGlobalPropertiesFormType(getFlowWebFields: true)));
        }

        [HttpPost]
        public ActionResult EOverdracht30([ModelBinder(typeof(FlowWebFieldBinder))] List<FlowWebField> model)
        {
            var formSetVersion = SaveFormsetAndValidFields(model);
            var flowinstance = FlowInstanceBL.GetByTransferID(uow, TransferID);
            var modelState = CurrentModelState;
            var pointuserinfo = PointUserInfo();

            var extraurlparameters = new RouteValueDictionary();
            var logentry = LogEntryBL.Create(FormType, pointuserinfo);

            if (modelState.IsValid && DefinitiveHandled)
            {
                OrganizationHelper.CheckAndSendVerpleegkundigeOverdracht(uow, flowinstance, pointuserinfo, logentry, false);
                OrganizationHelper.CheckAndSendMedVRI(uow, flowinstance, pointuserinfo);
                OrganizationHelper.CheckAndSendSurvey(uow, flowinstance, pointuserinfo);
                HandleThirdParty(false, pointuserinfo);
            }
            else if (modelState.IsValid)
            {
                var verpleegkundigeoverdrachtbo = new VerpleegkundigeOverdrachtBO(uow, flowinstance, pointuserinfo);
                if (FlowInstanceBL.CanSendVO(uow, FlowInstance) && verpleegkundigeoverdrachtbo.IsDefinitive())
                {
                    extraurlparameters.Add("ShowResendQuestion", true);
                    ForceReload = true;
                }
            }
            FlowInstanceSearchValuesBL.CalculateReadValuesByTransferIDAsync(uow, formSetVersion.TransferID);

            return SaveResult(modelState, formSetVersion, extraurlparameters);
        }
        
        public ActionResult EOverdrachtFlow(bool showResendQuestion = false)
        {
            return new SecureActionResult(FormTypeEOverdrachtViewModelHelper.GetEOverdrachtFlowViewModel(this, uow, GetGlobalPropertiesFormType(getFlowWebFields: true), showResendQuestion));
        }

        [HttpPost]
        public ActionResult EOverdrachtFlow([ModelBinder(typeof(FlowWebFieldBinder))] List<FlowWebField> model)
        {
            var modelState = CurrentModelState;

            var pointuserinfo = PointUserInfo();
            var logentry = LogEntryBL.Create(FormType, pointuserinfo);

            // TODO: #17148 Technical debt: Form als bijlage
            var voformuliertype = model.GetValueOrDefault(FlowFieldConstants.Name_VOFormulierType, "");
            int attachmentid = 0;
            bool voasupload = voformuliertype == FlowFieldDataSourceConstants.VOFormulierType.Pdf &&
                                int.TryParse(model.GetValueOrDefault(FlowFieldConstants.Name_VOFormulierTransferAttachmentID, ""), out attachmentid);
            bool vomeegegevn = voformuliertype == FlowFieldDataSourceConstants.VOFormulierType.ToPatient;

            if (voasupload && FormSetVersionID > 0)
            {
                TransferAttachmentBL.DeleteByFormSetVersionID(uow, FormSetVersionID, logentry);
                FormSetVersionBL.DeleteByIDAndTransferID(uow, FormSetVersionID, TransferID, logentry);

                // reset the FormSetVersionID so that SaveFormsetAndValidFields will create a new one
                FormSetVersionID = -1;
            }

            var formSetVersion = SaveFormsetAndValidFields(model);

            if (voasupload)
            {
                var transferattachment = TransferAttachmentBL.GetByID(uow, attachmentid);
                if (transferattachment != null)
                {
                    transferattachment.FormSetVersionID = formSetVersion.FormSetVersionID;
                    transferattachment.Deleted = false;

                    LoggingBL.FillLoggingWithID(uow, transferattachment, logentry);
                    uow.Save();
                }
            }
            else
            {
                TransferAttachmentBL.DeleteExistingVOAttachments(uow, TransferID, logentry);
            }

            // TODO: #17148 Technical debt: Form als bijlage
            // we can't use DefinitiveHandled here as it has could been marked as false
            // in the PointController due to validation errors
            if ((voasupload || vomeegegevn) && System.Web.HttpContext.Current.Request.GetRequestVariable<bool>("handledefinitive", false))
            {
                PhaseInstanceBL.SetStatusDone(uow, formSetVersion.PhaseInstance, logentry);
                DefinitiveHandled = true;
                modelState = new FlowWebFieldModelState();
            }

            var extraurlparameters = new RouteValueDictionary();
            if (modelState.IsValid && DefinitiveHandled)
            {
                OrganizationHelper.CheckAndSendVerpleegkundigeOverdracht(uow, FlowInstance, pointuserinfo, logentry, false);
                OrganizationHelper.CheckAndSendMedVRI(uow, FlowInstance, pointuserinfo);
                OrganizationHelper.CheckAndSendSurvey(uow, FlowInstance, pointuserinfo);
                HandleThirdParty(false, pointuserinfo);
            }
            else if (modelState.IsValid)
            {
                var verpleegkundigeoverdrachtbo = new VerpleegkundigeOverdrachtBO(uow, FlowInstance, pointuserinfo);
                if (FlowInstanceBL.CanSendVO(uow, FlowInstance) && verpleegkundigeoverdrachtbo.IsDefinitive())
                {
                    extraurlparameters.Add("ShowResendQuestion", true);
                    ForceReload = true;
                }
            }
            FlowInstanceSearchValuesBL.CalculateReadValuesByTransferIDAsync(uow, formSetVersion.TransferID);

            return SaveResult(modelState, formSetVersion, extraurlparameters);
        }

        [HttpPost]
        public ActionResult EOverdrachtFlowResend()
        {
            var pointuserinfo = PointUserInfo();
            var logentry = LogEntryBL.Create(FlowFormType.VerpleegkundigeOverdrachtFlow, pointuserinfo);

            OrganizationHelper.CheckAndSendVerpleegkundigeOverdracht(uow, FlowInstance, pointuserinfo, logentry, true);
            HandleThirdParty(true, pointuserinfo);

            return Json(new { EOverdrachtFlowResend = true });
        }

        public ActionResult SendOverdracht()
        {
            var viewModel = FormTypeEOverdrachtViewModelHelper.GetSendOverdrachtViewModel(uow, GetGlobalProperties(getFlowInstance:true));
            return new SecureActionResult(viewModel);
        }

        private void HandleThirdParty(bool isResend, PointUserInfo pointuserinfo)
        {
            if (!isResend) // Only sending the existing attachments and forms on the "first" sending of the VO
            {
                // Nedap
                var receivingorganization = OrganizationHelper.GetReceivingOrganization(uow, FlowInstance.FlowInstanceID);
                if (receivingorganization == null)
                {
                    return;
                }

                if (OrganizationHelper.IsValidNedapOrganization(receivingorganization) && FlowInstanceBL.IsValidNedapDossierStatus(uow, FlowInstance, pointuserinfo))
                {
                    var allowedattachments = SendAttachmentCacheBL.GetByOrganizationIDAllowed(uow, receivingorganization.OrganizationID, SendDestinationType.Nedap).Select(sa => sa.AttachmentTypeID.ConvertTo<int>());
                    foreach (var transferattachment in TransferAttachmentBL.GetByTransferID(uow, FlowInstance.TransferID).Where(ta => allowedattachments.Contains((int)ta.AttachmentTypeID)))
                    {
                        TransferAttachmentBL.HandleThirdParty(uow, transferattachment, FlowInstance, pointuserinfo, FlowInstance.TransferID);
                    }

                    var allowedformtypes = SendFormTypeCacheBL.GetByOrganizationIDAllowed(uow, receivingorganization.OrganizationID, SendDestinationType.Nedap).Select(sft => (int)sft.FormTypeID);
                    foreach (var formsetversion in FormSetVersionBL.GetByTransferIDAndFormTypeIDs(uow, FlowInstance.TransferID, allowedformtypes))
                    {
                        HandleThirdPartyAsync(formsetversion.FormSetVersionID, pointuserinfo);
                    }
                }
            }
        }

    }
}