﻿using DevTrends.MvcDonutCaching;
using Point.Business.Helper;
using Point.Business.Logic;
using Point.Business.Navigation;
using Point.Database.CacheModels;
using Point.Database.Extensions;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Infrastructure.Attributes;
using Point.Infrastructure.Exceptions;
using Point.Infrastructure.Extensions;
using Point.Models.Enums;
using Point.MVC.WebApplication.Attributes;
using Point.MVC.WebApplication.Helpers;
using Point.ServiceBus.Models;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using System.Web.Routing;

namespace Point.MVC.WebApplication.Controllers
{
    [DonutOutputCache(CacheProfile = "Short")]
    [ValidateOrigin]
    public partial class FlowMainController
    {
        private void buildSelectableForms(FlowInstance flowinstance)
        {
            var pointuserinfo = PointUserInfo();
            var dashboarditems = DashboardMenuItemViewBL.Get(uow, flowinstance, pointuserinfo);

            //Weghalen zorgadvies als zorgadvies niet "aan" staat
            if (dashboarditems.Any(it => it.FormTypeID == (int)FlowFormType.ZorgAdvies))
            {
                var zorgAdviesToevoegen = FlowFieldValueBL.GetByTransferIDAndFieldID(uow, TransferID, FlowFieldConstants.ID_ZorgAdviesToevoegen).FirstOrDefault();
                if (zorgAdviesToevoegen?.Value != "Ja")
                {
                    dashboarditems = dashboarditems.Where(it => it.FormTypeID != (int)FlowFormType.ZorgAdvies);
                }
            }

            ViewData.Add("extraForms", dashboarditems.Where(d => !d.HasPhaseInstance));
            ViewData.Add("statusForms", dashboarditems.Where(d => d.HasPhaseInstance));
        }

        public ActionResult Index()
        {
            return new SecureActionResult();
        }

        public ActionResult OverviewInterruptions()
        {
            var flowinstance = FlowInstanceBL.GetByID(uow, FlowInstance.FlowInstanceID);
            var allstatusses = flowinstance.FlowInstanceStatus.ToList().OrderByDescending(it => it.FlowInstanceStatusID);

            return PartialView(allstatusses);
        }

        public ActionResult Dashboard(bool Timeout = false, bool isCopy = false)
        {
            var flowinstance = FlowInstanceBL.GetByTransferID(uow, TransferID);
            PageTitle = "Dashboard";

            if (flowinstance == null)
            {
                return RedirectToAction("NewFlowInstance");
            }

            var hasFormTab = flowinstance.FlowDefinition?.HasFormTab == true;
            if (hasFormTab)
            {
                buildSelectableForms(flowinstance);
            }

            var pointuserinfo = PointUserInfo();

            ViewBag.Timeout = Timeout;
            ViewBag.HasTransferMemo = TransferMemoViewBL.GetCommunicationJournalByTransferID(uow, TransferID, pointuserinfo, false).Any();
            ViewBag.HasAttachment = TransferAttachmentBL.HasAttachment(uow, TransferID);
            ViewBag.HasFormTab = hasFormTab;

            var flowFieldsForDashboard = FlowFieldDashboardBL.GetForTransfer(uow, flowinstance.FlowInstanceID, ViewData);

            var lastinterruption = flowinstance.FlowInstanceStatus.GetInterruptionStatus();

            ViewData.Add("DashboardFlowFields", flowFieldsForDashboard);

            ViewBag.IsClosed = FlowInstanceBL.IsFlowClosed(uow, flowinstance.FlowInstanceID);
            ViewBag.LastInterruption = lastinterruption;

            ViewBag.HasFlowInstanceHistory = flowinstance.FlowInstanceStatus.Any();

            if (lastinterruption != null)
            {
                ViewBag.InterruptedEmployeeName = EmployeeBL.GetByID(uow, lastinterruption.EmployeeID.ConvertTo<int>()).FullName();
            }

            if (isCopy && !flowinstance.Transfer.OriginalTransferID.HasValue)
            {
                isCopy = false;
            }

            ViewBag.IsCopy = isCopy;

            if (IsMobileDevice())
            {
                return new SecureActionResult("DashboardMobile", flowinstance);
            }

            return new SecureActionResult(flowinstance);
        }

        public ActionResult NewFlowInstance()
        {
            PageTitle = PageHeader = "Maak nieuw transferdossier";

            var flowInstance = FlowInstanceBL.GetByTransferID(uow, TransferID);
            if (flowInstance != null)
            {
                return RedirectToAction("Dashboard");
            }

            var pointuserinfo = PointUserInfo();
            var locationid = pointuserinfo.Location.LocationID;
            var departmentid = pointuserinfo.Department.DepartmentID;

            var sendingflowdefinitions = FlowDefinitionBL.GetSendingFlowDefinitionsByLocationIDDepartmentID(uow, locationid, departmentid).ToList();
            if (!sendingflowdefinitions.Any())
            {
                return RedirectToAction("Search", "Transfer");
            }

            var pointUserInfo = PointUserInfoHelper.GetPointUserInfo(uow);

            var departments = DepartmentBL.GetLinkedDepartmentsByRole(uow, pointUserInfo).ToList();

            var viewModel = new NewFlowInstanceViewModel()
            {
                MultipleDepartments = departments
                    .OrderBy(d => d.SortOrder == null)
                    .ThenBy(d => d.SortOrder)
                    .ThenBy(d => d.Location.Name)
                    .ThenBy(d => d.Name)
                    .Select(d => new Option { Text = d.Location.Name + " / " + d.Name, Value = d.DepartmentID.ToString() })
                    .ToList(),
                FlowDefinitions = sendingflowdefinitions
                    .OrderBy(f => f.SortOrder)
                    .Select(l => new Option { Text = l.Description, Value = l.FlowDefinitionID.ToString() })
                    .ToList(),
                HasMultipleDepartments = departments.Count > 1,
                DepartmentIDReference = LoginBL.GetDepartmentIDReference()?.ToString() ?? "",
                HasMultipleFlowDefinitions = sendingflowdefinitions.Count() > 1,
              
            };
            return View(viewModel);
        }

        public JsonResult GetAllowedFlowDefinitionsByDepartment(int departmentID)
        {
            var pointuserinfo = PointUserInfo();
            var department = DepartmentBL.GetByDepartmentID(uow, departmentID);
            var sendingflowdefinitions = FlowDefinitionBL.GetSendingFlowDefinitionsByLocationIDDepartmentID(uow, department.LocationID, departmentID).ToArray();
            if (!sendingflowdefinitions.Any())
            {
                return null;
            }

            var options = sendingflowdefinitions
                  .OrderBy(f => f.SortOrder)
                  .Select(l => new Option { Text = l.Description, Value = l.FlowDefinitionID.ToString() })
                  .ToList();
            return new JsonResult
            {
                Data = options,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }
        public ActionResult ReopenTransfer()
        {
            PageTitle = "Dossier heropenen";
            PageHeader = "Dossier heropenen";

            var flowinstance = FlowInstanceBL.GetByTransferID(uow, TransferID);
            if (flowinstance == null)
            {
                return Content("<!-- Geen flowInstance -->");
            }

            var flowdefinitionID = flowinstance.FlowDefinitionID;
            var pointuserinfo = PointUserInfo();

            ViewBag.IsAllowed = PhaseInstanceBL.HasEndFlowRights(uow, flowdefinitionID, ControllerName, ActionName, pointuserinfo);

            return new SecureActionResult();
        }

        [HttpPost]
        public ActionResult ReopenTransfer(int transferID)
        {
            PageTitle = "Dossier heropenen";
            PageHeader = "Dossier heropenen";

            var flowinstance = FlowInstanceBL.GetByTransferID(uow, TransferID);
            if (flowinstance == null)
            {
                return Content("<!-- Geen flowInstance -->");
            }

            var flowdefinitionID = flowinstance.FlowDefinitionID;
            var flowinstanceID = flowinstance.FlowInstanceID;
            var pointuserinfo = PointUserInfo();

            var hasRights = PhaseInstanceBL.HasEndFlowRights(uow, flowdefinitionID, ControllerName, ActionName, pointuserinfo);

            if (!hasRights)
            {
                throw new PointJustLogException("Geen rechten om dit dossier te heropenen");
            }

            ViewBag.IsAllowed = true;

            var logentry = LogEntryBL.Create(FlowFormType.ReopenTransfer, PointUserInfo());
            FlowInstanceBL.ReOpenFlowInstance(uow, flowinstanceID, logentry);
            ResetOutputCacheByTransferID(TransferID);
            FlowInstanceSearchValuesBL.CalculateReadValuesByTransferIDAsync(uow, TransferID);
            ServiceBusBL.SendMessage("FlowInstance", "ReopenFlowInstance", new ReopenFlowInstance { FlowInstanceID = flowinstanceID });

            if (!Request.IsAjaxRequest())
            {
                return RedirectToAction("Dashboard", new { TransferID });
            }

            Response.StatusCode = (ErrorMessage == "" ? HttpStatusCode.OK : HttpStatusCode.BadRequest).ConvertTo<int>();
            return Json(new { ErrorMessage }, JsonRequestBehavior.DenyGet);
        }

        public ActionResult ResumeTransfer()
        {
            PageTitle = "Dossier hervatten";
            PageHeader = "Dossier hervatten";

            var flowinstance = FlowInstanceBL.GetByTransferID(uow, TransferID);
            if (flowinstance == null)
            {
                return Content("<!-- Geen flowInstance -->");
            }

            var isallowed = PhaseInstanceBL.HasPhaseRights(uow, flowinstance.FlowDefinitionID, 0, ControllerName, ActionName);
            if (!isallowed)
            {
                throw new PointSecurityException(ExceptionType.AccessDenied);
            }

            var activeinvite = OrganizationInviteBL.GetAcceptedOrActiveInvitesByFlowInstanceIDAndInviteType(uow, flowinstance.FlowInstanceID, InviteType.Regular).FirstOrDefault();
            var viewmodel = new FlowResumeViewModel
            {
                TransferID = TransferID,
                RedirectFormTypeID = activeinvite?.SendingPhaseDefinition?.FormTypeID,
                ResumeDate = DateTime.Now,
                SignalOnSave = SignaleringTrigger != null,
                ResumeReason = "",
                HasActiveReciever = activeinvite != null,
                ActiveRecieverName = activeinvite?.Department == null ? "" : activeinvite.Department.FullName()
            };

            return new SecureActionResult(viewmodel);
        }

        [HttpPost]
        [ValidateAntiFiddleInjection("TransferID")]
        public ActionResult ResumeTransfer(FlowResumeViewModel viewmodel)
        {
            var userinfo = PointUserInfo();
            var logentry = LogEntryBL.Create(FlowFormType.ResumeTransfer, userinfo);

            var flowinstance = SearchBL.GetSecurityTrimmedFlowInstance(uow, userinfo, TransferID);
            if (flowinstance == null)
            {
                throw new PointSecurityException("U heeft geen toegang tot dit dossier", ExceptionType.AccessDenied);
            }

            FlowInstanceStatusBL.CreateResume(uow, FlowInstance.FlowInstanceID, viewmodel, logentry);

            var generalaction = GeneralActionBL.GetByURL(uow, ControllerName, ActionName);
            if (viewmodel.SignalOnSave && FlowInstance != null && generalaction != null)
            {
                SignaleringBL.HandleSignalByFlowDefinitionGeneralAction(uow, FlowInstance,
                    generalaction.GeneralActionID, userinfo,
                    "Vanaf: " + viewmodel.ResumeDate + "\r\nReden: " +
                    viewmodel.ResumeReason);
            }

            FlowInstanceSearchValuesBL.CalculateReadValuesByTransferIDAsync(uow, TransferID);

            if (viewmodel.RedirectFormTypeID.HasValue)
            {
                return RedirectToAction("StartFlowFormByFormTypeID", "FlowMain", new { TransferID, FormTypeID = viewmodel.RedirectFormTypeID });
            }

            return RedirectToAction("Dashboard", "FlowMain", new { TransferID });
        }

        public ActionResult InterruptTransfer()
        {
            PageTitle = "Dossier onderbreken";
            PageHeader = "Dossier onderbreken";

            var flowinstance = FlowInstanceBL.GetByTransferID(uow, TransferID);
            if (flowinstance == null)
            {
                return Content("<!-- Geen flowInstance -->");
            }

            var isallowed = PhaseInstanceBL.HasPhaseRights(uow, flowinstance.FlowDefinitionID, 0, ControllerName, ActionName);
            if (!isallowed)
            {
                throw new PointSecurityException(ExceptionType.AccessDenied);
            }

            var viewmodel = new FlowInterruptionViewModel
            {
                TransferID = TransferID,
                InterruptionDate = DateTime.Now,
                SignalOnSave = SignaleringTrigger != null,
                InterruptionReason = ""
            };

            return new SecureActionResult(viewmodel);
        }

        [HttpPost]
        [ValidateAntiFiddleInjection("TransferID")]
        public ActionResult InterruptTransfer(FlowInterruptionViewModel viewmodel)
        {
            var userinfo = PointUserInfo();
            var logentry = LogEntryBL.Create(FlowFormType.InterruptTransfer, userinfo);

            var flowinstance = SearchBL.GetSecurityTrimmedFlowInstance(uow, userinfo, TransferID);
            if (flowinstance == null)
            {
                throw new PointSecurityException("U heeft geen toegang tot dit dossier", ExceptionType.AccessDenied);
            }

            FlowInstanceStatusBL.CreateInterruption(uow, flowinstance.FlowInstanceID, viewmodel, logentry);

            var generalaction = GeneralActionBL.GetByURL(uow, ControllerName, ActionName);
            if (viewmodel.SignalOnSave && flowinstance != null && generalaction != null)
            {
                SignaleringBL.HandleSignalByFlowDefinitionGeneralAction(uow, flowinstance,
                    generalaction.GeneralActionID, userinfo,
                    "Vanaf: " + viewmodel.InterruptionDate + "\r\nReden: " +
                    viewmodel.InterruptionReason);
            }

            FlowInstanceSearchValuesBL.CalculateReadValuesByTransferIDAsync(uow, TransferID);

            return RedirectToAction("Dashboard", "FlowMain", new { TransferID });
        }

        public ActionResult DeleteTransfer()
        {
            PageTitle = "Dossier verwijderen";
            PageHeader = "Dossier verwijderen";

            var pointuserinfo = PointUserInfo();
            var flowinstance = SearchBL.GetSecurityTrimmedFlowInstance(uow, pointuserinfo, TransferID);
            if (flowinstance == null)
            {
                throw new PointSecurityException("U heeft geen toegang tot dit dossier", ExceptionType.AccessDenied);
            }

            var isAllowed = PhaseInstanceBL.HasEndFlowRights(uow, flowinstance.FlowDefinitionID, ControllerName, ActionName, pointuserinfo);

            ViewBag.IsAllowed = isAllowed;

            return new SecureActionResult();
        }

        [HttpPost]
        [ValidateAntiFiddleInjection("TransferID")]
        public ActionResult DeleteTransfer(int TransferID)
        {
            PageTitle = "Dossier verwijderen";
            PageHeader = "Dossier verwijderen";

            var pointuserinfo = PointUserInfo();
            var flowinstance = SearchBL.GetSecurityTrimmedFlowInstance(uow, pointuserinfo, TransferID);
            if (flowinstance == null)
            {
                throw new PointSecurityException("U heeft geen toegang tot dit dossier", ExceptionType.AccessDenied);
            }

            var hasRights = PhaseInstanceBL.HasEndFlowRights(uow, flowinstance.FlowDefinitionID, ControllerName, ActionName, pointuserinfo);

            ViewBag.IsAllowed = hasRights;

            FlowInstanceBL.DeleteFlowInstance(uow, TransferID);
            SignaleringBL.ExpireSignaleringByFlowInstanceID(uow, flowinstance.FlowInstanceID);

            FlowInstanceSearchValuesBL.CalculateReadValuesByTransferIDAsync(uow, TransferID);

            if (!Request.IsAjaxRequest())
            {
                return RedirectToAction("Search", "Transfer");
            }

            Response.StatusCode = (int)(ErrorMessage == "" ? HttpStatusCode.OK : HttpStatusCode.BadRequest);
            return Json(new { ErrorMessage }, JsonRequestBehavior.DenyGet);
        }

        public ActionResult OverviewPhaseDefinition(FlowDefinitionID flowDefinitionID)
        {
            var phaseDefinitions = PhaseDefinitionCacheBL.GetByFlowDefinitionID(uow, flowDefinitionID).ToList();
            var phaseInstances = new List<PhaseInstance>();

            var modellist = FlowPhaseViewBL.FromModelList(phaseDefinitions, phaseInstances).OrderBy(x => x.PhaseNum).ToList();
            return View("OverviewFlow", modellist);
        }

        public ActionResult OverviewFlow(int flowinstanceID)
        {
            var flowinstance = FlowInstanceBL.GetByID(uow, flowinstanceID);

            var phasedefinitions = PhaseDefinitionCacheBL.GetByFlowDefinitionID(uow, flowinstance.FlowDefinitionID).ToList();
            var outputInstances = new List<PhaseInstance>();
            var pointuserinfo = PointUserInfo();

            if (IsMobileDevice())
            {
                foreach (var item in phasedefinitions.ToList())
                {
                    var rights = PhaseDefinitionRightsBL.GetFormRightsPhaseDefinitionID(uow, pointuserinfo, item, FlowInstance);
                    if (rights <= Rights.W)
                    {
                        phasedefinitions.Remove(item);
                    }
                }
            }

            var phaseInstances = PhaseInstanceBL.GetByFlowInstanceId(uow, flowinstanceID)
                .Where(it => it.PhaseDefinition.Phase >= 0)
                .OrderBy(it => it.PhaseDefinition.Phase)
                .ThenByDescending(it => it.StartProcessDate).ToList();

            var phasenums = phaseInstances.Select(it => it.PhaseDefinition.Phase).Distinct().ToList();
            foreach (var phasenum in phasenums)
            {
                outputInstances.Add(phaseInstances.LastOrDefault(it => it.PhaseDefinition.Phase == phasenum));
            }

            var modellist = FlowPhaseViewBL.FromModelList(phasedefinitions, outputInstances).OrderBy(it => it.PhaseNum).ToList();
            return PartialView(modellist);
        }

        public ActionResult BeginFlowForm(FlowDefinitionID flowDefinitionID, int? departmentID = null)
        {
            var phaseDefinitionBeginFlow = PhaseDefinitionCacheBL.GetBeginFlowPhaseDefinition(uow, flowDefinitionID);

            if (phaseDefinitionBeginFlow == null)
            {
                return Content("<!-- FlowMain: PhaseDefinitionBeginFlow niet gevonden -->");
            }

            return StartFlowFormByModel(phaseDefinitionBeginFlow, departmentID: departmentID);
        }

        public ActionResult StartFlowFormByFormTypeID(int formtypeid, bool showhistory = false)
        {
            var phaseinstance = PhaseInstanceBL.GetByTransferIDAndFormTypeID(uow, TransferID, formtypeid);

            var phaseDefinition = phaseinstance != null
                ? PhaseDefinitionCacheBL.GetByID(uow, phaseinstance.PhaseDefinitionID)
                : PhaseDefinitionCacheBL.GetByFlowDefinitionAndFormType(uow, FlowInstance.FlowDefinitionID, formtypeid);

            return phaseDefinition == null
                ? Content("<!-- FlowMain: PhaseDefinition niet gevonden -->")
                : StartFlowFormByModel(phaseDefinition, phaseinstance?.PhaseInstanceID, showhistory: showhistory);
        }

        public ActionResult StartFlowForm(int phaseDefinitionID, int? phaseInstanceID = null)
        {
            var phaseDefinition = PhaseDefinitionCacheBL.GetByID(uow, phaseDefinitionID);
            if (phaseDefinition == null)
            {
                return Content("<!-- FlowMain: PhaseDefinition niet gevonden -->");
            }

            return StartFlowFormByModel(phaseDefinition, phaseInstanceID);
        }

        public ActionResult StartTabForm(string tabgroup)
        {
            var flowinstance = FlowInstanceBL.GetByTransferID(uow, TransferID);
            var phasedefinitions = PhaseDefinitionCacheBL.GetPhaseDefinitionsBySubcategory(uow, tabgroup, flowinstance.FlowDefinitionID).ToList();
            var phasedefinitionids = phasedefinitions.Select(it => it.PhaseDefinitionID).ToArray();
            var phaseinstances = PhaseInstanceBL.GetByFlowinstanceIDAndPhaseDefinitionIDs(uow, flowinstance.FlowInstanceID, phasedefinitionids).ToList();

            if (flowinstance.FlowDefinitionID == FlowDefinitionID.ZH_VVT || flowinstance.FlowDefinitionID == FlowDefinitionID.CVA)
            {
                return getZHVVTActiveTab(tabgroup, phasedefinitions, phaseinstances);
            }

            return getActiveTab(phasedefinitions, phaseinstances);
        }

        public ActionResult StartFlowFormByModel(PhaseDefinitionCache phaseDefinition, int? phaseInstanceID = null, int? departmentID = null, bool showhistory = false)
        {
            var queryparameters = getExtraQueryParameters(Request.QueryString);

            if (phaseInstanceID == null)
            {
                var latestphaseinstance = PhaseInstanceBL.GetLastByTransferAndPhaseDefinition(uow, TransferID, phaseDefinition.PhaseDefinitionID);
                phaseInstanceID = latestphaseinstance?.PhaseInstanceID;
            }

            if (Request.IsAjaxRequest())
            {
                var routeValues = new RouteValueDictionary
                {
                    {"TransferID", TransferID},
                    {"FlowDefinitionID", phaseDefinition.FlowDefinitionID},
                    {"PhaseDefinitionID", phaseDefinition.PhaseDefinitionID}
                };

                if (showhistory)
                {
                    routeValues.Add("ShowHistory", true);
                }

                if (phaseInstanceID.HasValue)
                {
                    routeValues.Add("PhaseInstanceID", phaseInstanceID);
                }

                if (departmentID.HasValue)
                {
                    routeValues.Add("DepartmentID", departmentID.Value);
                }

                if (queryparameters.Any())
                {
                    foreach (var key in queryparameters.Keys)
                    {
                        if (!routeValues.Keys.Contains(key))
                        {
                            routeValues.Add(key, queryparameters[key]);
                        }
                    }
                }

                Response.StatusCode = (int)HttpStatusCode.OK;

                return Json(new
                {
                    TransferID,
                    Url = Url.Action(phaseDefinition.GetActionName(), phaseDefinition.GetControllerName(), routeValues)
                }, JsonRequestBehavior.AllowGet);
            }

            int? formsetversionid = null;
            if (phaseInstanceID != null)
            {
                formsetversionid = FormSetVersionBL.GetByPhaseInstanceID(uow, phaseInstanceID.Value).FirstOrDefault()?.FormSetVersionID;
                if (formsetversionid.HasValue)
                {
                    RouteData.Values.Add("FormSetVersionID", formsetversionid.Value);
                }
            }

            RouteData.Values.Add("TransferID", TransferID);
            RouteData.Values.Add("FlowDefinitionID", (int)phaseDefinition.FlowDefinitionID);
            RouteData.Values.Add("PhaseDefinitionID", phaseDefinition.PhaseDefinitionID);

            if (showhistory)
            {
                RouteData.Values.Add("ShowHistory", true);
            }
            if (phaseInstanceID.HasValue)
            {
                RouteData.Values.Add("PhaseInstanceID", phaseInstanceID);
            }
            if (departmentID.HasValue)
            {
                RouteData.Values.Add("DepartmentID", departmentID.Value);
            }

            if (queryparameters.Any())
            {
                foreach (var key in queryparameters.Keys)
                {
                    if (!RouteData.Values.Keys.Contains(key))
                    {
                        RouteData.Values.Add(key, queryparameters[key]);
                    }
                }
            }

            var formTypeController = ViewRenderer.CreateController<FormTypeController>(RouteData);
            return formTypeController.Index(phaseDefinition.FormTypeID);
        }

        public JsonResult CheckForExistingDossiers()
        {
            ExistingDossierCheckInfo existingdossiercheckinfo;

            if (ExternalPatientHelper.HavePatExternalReference())
            {
                existingdossiercheckinfo = ExistingDossierCheckInfo.CreateForUser(uow, PointUserInfo(), ExternalPatientHelper.GetPatExternalReference(), ExternalPatientHelper.GetPatExternalReferenceBSN());
            }
            else
            {
                existingdossiercheckinfo = ExistingDossierCheckInfo.CreateForNoNeed();
            }

            var res = new JsonResult
            {
                Data = existingdossiercheckinfo,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };

            return res;
        }

        #region -- Helpers --

        public bool IsFlowClosed(int transferID)
        {
            var flowInstance = FlowInstanceBL.GetByTransferID(uow, transferID);

            if (flowInstance == default(FlowInstance))
            {
                return false;
            }

            return FlowInstanceBL.IsFlowClosed(uow, flowInstance.FlowInstanceID);
        }

        private static Dictionary<string, string> getExtraQueryParameters(NameValueCollection querystring)
        {
            var keystoremove = new List<string>
            {
                "nextphasedefinitionid",
                "transferid",
                "formsetversionid",
                "formtypeid",
                "phasedefinitionid",
                "phaseinstanceid",
                "handlesave",
                "handlenextphase",
                "handledefinitive",
                "flowdefinitionid",
                "tabgroup"
            };

            var queryparameters = new Dictionary<string, string>();
            var paramsLowered = querystring.AllKeys.ToDictionary(k => k.ToLowerInvariant(), k => querystring[k]);
            foreach (var key in paramsLowered.Keys)
            {
                if (!keystoremove.Contains(key))
                {
                    queryparameters.Add(key, paramsLowered[key]);
                }
            }

            return queryparameters;
        }

        private ActionResult getActiveTab(List<PhaseDefinitionCache> phasedefinitions, List<PhaseInstance> phaseinstances)
        {
            PhaseInstance phaseinstance = null;

            if (phaseinstances.Any())
            {
                phaseinstance = (phaseinstances.LastOrDefault(it => it.Status == 1 && it.PhaseDefinition.Phase >= 0) ??
                                 phaseinstances.LastOrDefault(it => it.Status == 2)) ?? phaseinstances.LastOrDefault();
            }

            if (phaseinstance != null)
            {
                var phasedefinitioncache = PhaseDefinitionCacheBL.GetByID(uow, phaseinstance.PhaseDefinitionID);
                return StartFlowFormByModel(phasedefinitioncache, phaseinstance.PhaseInstanceID);
            }

            var phasedefinition =
                phasedefinitions.Where(it => it.Phase == -1).OrderBy(it => it.OrderNumber).FirstOrDefault() ??
                phasedefinitions.OrderBy(it => it.OrderNumber).FirstOrDefault();

            return StartFlowFormByModel(phasedefinition);
        }

        private ActionResult getZHVVTActiveTab(string subcatgegory, List<PhaseDefinitionCache> phasedefinitions, List<PhaseInstance> phaseinstances)
        {
            PhaseDefinitionCache phasedefinition = null;

            if (subcatgegory != PhaseDefinitionConstants.ZHVVT_SUBCATEGORY_TRANSFERPUNT)
            {
                return getActiveTab(phasedefinitions, phaseinstances);
            }

            var phaseinstance = phaseinstances.FirstOrDefault(pi => pi.PhaseDefinition.FormTypeID == (int)FlowFormType.InBehandelingTP);

            if (phaseinstance != null && phaseinstance.Status == (int)Status.Done)
            {
                phaseinstance = phaseinstances.FirstOrDefault(pi => pi.PhaseDefinition.FormTypeID == (int)FlowFormType.AanvullendeGegevensTPZHVVT);
                phasedefinition = phasedefinitions.FirstOrDefault(pd => pd.FormTypeID == (int)FlowFormType.AanvullendeGegevensTPZHVVT);
            }

            if (phasedefinition != null)
            {
                return StartFlowFormByModel(phasedefinition, phaseinstance?.PhaseInstanceID);
            }

            return getActiveTab(phasedefinitions, phaseinstances);
        }

        #endregion
    }
}