﻿using Point.Business.Helper;
using Point.Business.Logic;
using Point.Communication;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Infrastructure.Attributes;
using Point.Infrastructure.ProxiedMembership;
using Point.MVC.WebApplication.Extensions;
using System;
using System.Web.Mvc;
using System.Web.Security;

namespace Point.MVC.WebApplication.Controllers
{
    [Authorize]
    public class AccountController : PointController
    {
        [HttpGet]
        public ActionResult Details(bool accountischanged = false)
        {
            var userinfo = PointUserInfo();
            var employee = EmployeeBL.GetByID(uow, userinfo.Employee.EmployeeID);
            var viewmodel = AccountDetailsViewModelBL.FromModel(employee);
            ViewBag.AccountIsChanged = accountischanged;
            using (var lookupbl = new LookUpBL())
            {
                ViewBag.SecretQuestions = lookupbl.GetSecretQuestions(viewmodel.SecretQuestion);
            }
                
            return View(viewmodel);
        }

        [HttpGet]
        public ActionResult SecretQuestion()
        {
            var userinfo = PointUserInfo();
            var employee = EmployeeBL.GetByID(uow, userinfo.Employee.EmployeeID);
            var viewmodel = SecretQuestionViewModelBL.FromModel(employee);
            //ViewBag.AccountIsChanged = accountischanged;
            using (var lookupbl = new LookUpBL())
            {
                ViewBag.SecretQuestions = lookupbl.GetSecretQuestions(viewmodel.SecretQuestion);
            }

            return View(viewmodel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateAntiFiddleInjection("EmployeeID")]
        public JsonResult SecretQuestion(SecretQuestionViewModel viewmodel)
        {
            SecretQuestionViewModelBL.Save(uow, viewmodel);
            return Json(new { Message = "Opgeslagen", Url = Url.Action("Search", "Transfer") });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateAntiFiddleInjection("EmployeeID")]
        public JsonResult Details(AccountDetailsViewModel viewmodel)
        {
            var userInfo = PointUserInfo();
            //Users with sso login authentication shouldn't be asked for password. 
            AccountDetailsViewModelBL.Save(uow, viewmodel, !userInfo.Organization.SSO);

            var employee = EmployeeBL.GetByID(uow, viewmodel.EmployeeID);
            PointUserInfoHelper.ClearCache(employee.UserId.GetValueOrDefault());

            return Json(new { Message = "Opgeslagen", Url = Url.Action("Search", "Transfer")});
        }

        [HttpGet]
        public ActionResult ChangePassword(bool expired = false)
        {
            ViewBag.Expired = expired;
            return View(new ChangePasswordViewModel() { EmployeeID = PointUserInfo().Employee.EmployeeID });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateAntiFiddleInjection("EmployeeID")]
        public ActionResult ChangePassword(ChangePasswordViewModel model)
        {
            var newPassword = model.NewPassword ?? "";

            if (!Membership.ValidateUser(PointUserInfo().Employee.UserName, model.OldPassword))
            {
                ModelState.AddModelError("", "Huidig wachtwoord is onjuist");
            }

            foreach(string error in SecurityBL.TestValidPassword(newPassword))
            {
                ModelState.AddModelError("", error);
            }

            if (ModelState.IsValid)
            {
                var pointuserinfo = PointUserInfo();
                if (Membership.Provider.ChangePassword(pointuserinfo.Employee.UserName, model.OldPassword, model.NewPassword))
                {
                    EmployeeBL.UpdateChangePassword(uow, pointuserinfo, false);
                    ViewBag.ShowSuccessMessage = true;
                }
            }

            return View(new ChangePasswordViewModel() { EmployeeID = PointUserInfo().Employee.EmployeeID });
        }




        public ActionResult ChangeMFASMS()
        {
            PageHeader = "Extra beveiliging bij inloggen in POINT";

            var model = UsedMFACodeBL.GetChangeMFAByPointUserInfo(uow, PointUserInfo());

            ViewData.SetIsMFAScreen(true);

            return View(model);
        }

        public ActionResult ChangeMFAGoogle()
        {
            PageHeader = "Extra beveiliging bij inloggen in POINT";

            var mfaViewModel = UsedMFACodeBL.GetChangeMFAByPointUserInfo(uow, PointUserInfo());

            if (String.IsNullOrEmpty(mfaViewModel.MFAKey)) //howto reset (2nd time)
            {

                mfaViewModel.MFAKey = UsedMFACodeBL.GetRandomGoogleCode(20);
                UsedMFACodeBL.SaveMFAKey(uow, mfaViewModel.EmployeeID, mfaViewModel.MFAKey);
            }

            var url = Request.Url.Host.Split('.');
            new GoogleAuth().FillMFAViewModel(mfaViewModel,PointUserInfo().Employee.UserName, url[0]);

            mfaViewModel.MFACode = "";
            UsedMFACodeBL.CreateMFACode(uow, mfaViewModel, MFACodeType.Register);

            ViewData.SetIsMFAScreen(true);

            return View(mfaViewModel);
        }



        [HttpPost]
        [ValidateAntiFiddleInjection("employeeID")]
        public JsonResult SendMFACode(MFAViewModel mfaViewModel)
        {
            UsedMFACodeBL.SaveMFANumber(uow, mfaViewModel.EmployeeID, mfaViewModel.MFANumber);

            mfaViewModel.MFACode = UsedMFACodeBL.GetRandomMFACode();
            UsedMFACodeBL.CreateMFACode(uow, mfaViewModel, MFACodeType.Register);

            new MessageBird().SendMessage(mfaViewModel.MFANumber, String.Format("Uw bevestigingscode is: {0}", mfaViewModel.MFACode));

            return Json(new { MFADigest = AntiFiddleInjection.CreateDigest(mfaViewModel.MFACode) });
        }





        [HttpPost]
        public JsonResult CheckMFACode(MFAViewModel mfaViewModel, string mfaDigest)
        {
            if (mfaViewModel.MFACode != AntiFiddleInjection.GetPlainDigest(mfaDigest))
            {
                return Json(new { result = false });
            }

            bool checkmfacode = UsedMFACodeBL.CheckConfirmMFACode(uow, mfaViewModel);


            var user = MembershipProxy.Instance.GetUser();
            if (user != null)
            {
                PointUserInfoHelper.ClearCache(user.UserId);
            }

            return Json(new { result = checkmfacode });

        }

        [HttpPost]
        [ValidateAntiFiddleInjection("employeeID")]
        public JsonResult CheckGoogleCode(MFAViewModel mfaViewModel)
        {
            var usedmfacode = UsedMFACodeBL.GetUsedMFACodeByEmployeeID(uow, mfaViewModel.EmployeeID, MFACodeType.Register);
            mfaViewModel.MFAKey = usedmfacode.MFAKey;
            bool checkmfacode = new GoogleAuth().CheckMFAViewModel(mfaViewModel); 

            if (!checkmfacode)
            {
                return Json(new { result = false });
            }

            UsedMFACodeBL.CheckConfirmMFAKey(uow, usedmfacode);

            var user = MembershipProxy.Instance.GetUser();
            if (user != null)
            {
                PointUserInfoHelper.ClearCache(user.UserId);
            }

            return Json(new { result = checkmfacode });
        }

    }
}