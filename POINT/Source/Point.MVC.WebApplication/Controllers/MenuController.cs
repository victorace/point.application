﻿using DevTrends.MvcDonutCaching;
using Point.Business.Helper;
using Point.Business.Logic;
using Point.Database.CacheModels;
using Point.Database.Extensions;
using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Infrastructure.Extensions;
using Point.MVC.WebApplication.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Point.Database.Models.Constants;
using Point.Infrastructure.ProxiedMembership;
using Point.Models.LDM.EOverdracht30.Enums;

namespace Point.MVC.WebApplication.Controllers
{
    [DonutOutputCache(CacheProfile = "Short")]
    public partial class MenuController : PointController
    {
        public ActionResult Index(int id = -1)
        {
            var model = new MenuViewModel();

            setParents();

            var user = MembershipProxy.Instance.GetUser();
            if (user == null || string.IsNullOrEmpty(user.UserName))
            {
                return View(model); //return empty menu if user not logged in.
            }

            IList<LocalMenuItem> items = new List<LocalMenuItem>();

            model.IsError = parentController.Equals("Error", StringComparison.InvariantCultureIgnoreCase);
            model.IsSearch = HttpContext.Request.Path.ToLower().Contains("search");

            if (HttpContext.Request.IsManagementArea())
            {
                items = buildManagementMenu();
            }
            else
            {
                if (TransferID <= 0)
                {
                    if (!string.IsNullOrWhiteSpace(Request.GetParam("TransferID")))
                    {
                        return View(model);
                    }

                    if (parentAction.Equals("Participants", StringComparison.InvariantCultureIgnoreCase))
                    {
                        items = buildParticipantsMenu();
                    }
                    else if (parentAction.Equals("Search", StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (ExternalPatientHelper.HavePatExternalReference())
                        {
                            items = buildExtReferenceMenu();
                        }
                        else
                        {
                            items = buildSearchMenu();
                        }
                    }
                    else if (model.IsError)
                    {
                        items = buildErrorMenu();
                    }
                    else if (parentController.Equals("Report", StringComparison.InvariantCultureIgnoreCase) || parentController.Equals("DownloadReport", StringComparison.InvariantCultureIgnoreCase))
                    {
                        items = buildReportMenu();
                    }
                }
                else
                {
                    items = buildFlowMenu(TransferID);
                    if (items == null)
                    {
                        return Content("<!-- Menu: Geen flow bekend -->");
                    }
                }
            }

            model.MenuItems = items;

            return View(model);
        }

        public ActionResult MobileHeaderMenu()
        {
            setParents();
            var flowinstance = FlowInstanceBL.GetByTransferID(uow, TransferID);

            var items = new List<LocalMenuItem>
            {
                new LocalMenuItem
                {
                    Glyphicon = "glyphicon glyphicon-home",
                    Description = "Home",
                    Disabled = false,
                    Label = "",
                    MenuType = MenuTypes.Actie,
                    Url = Url.Action("Overview", "Transfer")
                }
            };


            if (flowinstance != null)
            {
                var startphasedefinition = PhaseDefinitionCacheBL.GetBeginFlowPhaseDefinition(uow, flowinstance.FlowDefinitionID);

                items.Add(new LocalMenuItem
                {
                    Glyphicon = "glyphicon glyphicon-th-list",
                    Description = "Dashboard",
                    Disabled = false,
                    Label = "",
                    MenuType = MenuTypes.Actie,
                    Url = Url.Action("Dashboard", "FlowMain", new { TransferID })
                });

                items.Add(new LocalMenuItem
                {
                    Glyphicon = "glyphicon glyphicon-user gender-color-" + flowinstance?.Transfer?.Client?.Gender.GetDescription().ToLower(),
                    Description = "Patiëntinformatie",
                    Disabled = false,
                    Label = flowinstance.Transfer.Client.FullName(),
                    MenuType = MenuTypes.Actie,
                    Url = Url.Action("StartFlowForm", "FlowMain", new { TransferID, startphasedefinition.PhaseDefinitionID })
                });
            }

            return PartialView(items);
        }

        public ActionResult FormTabs()
        {
            setParents();
            var flowinstance = FlowInstanceBL.GetByTransferID(uow, TransferID);
            return buildFormTabs(flowinstance);
        }

        private IEnumerable<LocalMenuItem> GetManagementMenuItems()
        {
            var items = new List<LocalMenuItem>();

            var pointUserInfo = PointUserInfo();
            if (!pointUserInfo.HasManagementFunctions)
            {
                return items;
            }

            items.Add(new LocalMenuItem
            {
                Glyphicon = "glyphicon glyphicon-edit",
                Description = "Beheer functies",
                Disabled = false,
                Identifier = "Beheer",
                IsCurrent = parentAction == "Beheer",
                Label = "Beheer",
                MenuType = MenuTypes.Actie,
                Url = Url.Content("~/Management")
            });

            if (parentController == "Organization")
            {
                var onlyorganizations = this.HttpContext.Request.GetRequestVariable<bool>("onlyorganizations", false);

                items.Add(new LocalMenuItem
                {
                    Glyphicon = "glyphicon glyphicon-th-list",
                    Description = "Organisaties beheer",
                    Disabled = false,
                    Identifier = "Organisaties beheer",
                    IsCurrent = parentAction == "Index" && onlyorganizations == false,
                    Label = "Organisaties beheer",
                    MenuType = MenuTypes.Actie,
                    Url = Url.Content("~/Management/Organization")
                });

                items.Add(new LocalMenuItem
                {
                    Glyphicon = "glyphicon glyphicon-th-list",
                    Description = "Organisaties overzicht",
                    Disabled = false,
                    Identifier = "Organisaties overzicht",
                    IsCurrent = parentAction == "Index" && onlyorganizations,
                    Label = "Organisaties overzicht",
                    MenuType = MenuTypes.Actie,
                    Url = Url.Content("~/Management/Organization?onlyorganizations=true")
                });

                items.Add(new LocalMenuItem
                {
                    Glyphicon = "glyphicon glyphicon-th-list",
                    Description = "Afdeling rapport",
                    Disabled = false,
                    Identifier = "Afdeling rapport",
                    IsCurrent = parentAction == "DepartmentReport",
                    Label = "Afdeling rapport",
                    MenuType = MenuTypes.Actie,
                    Url = Url.Content("~/Management/Organization/DepartmentReport")
                });
            }

            return items;
        }

        #region Fields

        private string parentAction = "";
        private string parentController = "";
        private string parentSubcategory = "";
        private IList<LocalMenuItem> menuItems = new List<LocalMenuItem>();

        #endregion

        #region Private impl.

        private void setParents()
        {
            if (ControllerContext.ParentActionViewContext == null)
            {
                return;
            }

            parentAction = ControllerContext.ParentActionViewContext.RouteData.Values["action"].ToString();
            parentController = ControllerContext.ParentActionViewContext.RouteData.Values["controller"].ToString();
        }

        private void addMenuItem(PhaseDefinitionCache phasedefinition, List<PhaseInstance> phaseinstances, MenuItemDestination menuitemdestination, PointUserInfo userinfo)
        {
            var buildingtabs = menuitemdestination == MenuItemDestination.FormTab;

            var menutype = MenuHelper.GetMenuTypeFromMaincategory(phasedefinition.Maincategory);
            if (string.IsNullOrWhiteSpace(menutype))
            {
                return;
            }

            //No doubles on PhaseDefinitionID or Subcategory
            string identifier = String.Concat(menutype, phasedefinition.PhaseDefinitionID);
            if (menuItems.Any(mi => mi.Identifier == identifier || (!buildingtabs && mi.Label == phasedefinition.Subcategory)))
            {
                return;
            }

            //If zorgadvies check for "Ja"
            if (phasedefinition.FormTypeID == (int)FlowFormType.ZorgAdvies)
            {
                var zorgAdviesToevoegen = FlowFieldValueBL.GetByTransferIDAndFieldID(uow, TransferID, FlowFieldConstants.ID_ZorgAdviesToevoegen).FirstOrDefault();
                if (zorgAdviesToevoegen == null || (zorgAdviesToevoegen.Value != "Ja"))
                {
                    return;
                }
            }

            //Check standard rights
            var menuvisablility = MenuBL.GetMenuItemStatus(uow, phasedefinition, phaseinstances, userinfo, menuitemdestination);
            bool isCurrent = base.PhaseDefinitionID == phasedefinition.PhaseDefinitionID;

            if (menuvisablility == MenuItemStatus.Invisible && !isCurrent)
            {
                return;
            }

            var label = phasedefinition.PhaseName;
            var url = ""; 

            if (!buildingtabs && !String.IsNullOrEmpty(phasedefinition.Subcategory))
            {
                label = phasedefinition.Subcategory;
                url = MenuHelper.GetUrl(Url, TransferID, phasedefinition.PhaseDefinitionID, null, phasedefinition.Subcategory);
                var loadedPhasedefinition = PhaseDefinitionCacheBL.GetByID(uow, base.PhaseDefinitionID);
                if (loadedPhasedefinition != null)
                {
                    isCurrent = loadedPhasedefinition.Subcategory == phasedefinition.Subcategory;
                }
            }
            else
            {
                int? phaseinstancesperPhaseDefinitionID = phaseinstances.Where(phi => phi.PhaseDefinitionID == phasedefinition.PhaseDefinitionID)
                    .OrderByDescending(it => it.PhaseInstanceID)
                    .FirstOrDefault()?.PhaseInstanceID;

                url = MenuHelper.GetUrl(Url, TransferID, phasedefinition.PhaseDefinitionID, phaseinstancesperPhaseDefinitionID, null);
            }

            menuItems.Add(new LocalMenuItem
            {
                MenuType = menutype,
                Label = label,
                Url = url,
                Identifier = identifier,
                Description = phasedefinition.Description,
                Disabled = menuvisablility == MenuItemStatus.Disabled,
                Action = phasedefinition.GetActionName(),
                Controller = phasedefinition.GetControllerName(),
                CssClass = FlowPhaseAttributeBL.GetClassNameMenuItem(uow, userinfo, phasedefinition),
                Subcategory = phasedefinition.Subcategory,
                IsCurrent = isCurrent,
                OrderNumber = phasedefinition.OrderNumber
            });
        }

        private void addMenuItemActie(GeneralActionPhase generalactionphase, Rights phaseRights, bool flowIsClosed, bool flowIsInterrupted)
        {
            var generalaction = generalactionphase.GeneralAction;

            if (generalaction.GeneralActionTypeID != (int)GeneralActionTypeName.Menu &&
                generalaction.GeneralActionTypeID != (int)GeneralActionTypeName.MenuProcess &&
                generalaction.GeneralActionTypeID != (int)GeneralActionTypeName.MenuTransfer) return;

            var menutype = MenuHelper.GetMenuTypeFromGeneralAction(generalaction.GeneralActionTypeID);
            var identifier = menutype + generalaction.GeneralActionID;

            var url = Url.Action(generalaction.MethodName.GetURLComponent(2), generalaction.MethodName.GetURLComponent(1), new { TransferID, generalaction.GeneralActionID });

            if (menuItems.Count(mi => mi.Identifier == identifier) != 0)
            {
                return;
            }

            Enum.TryParse(generalaction.Rights, out Rights generalactionRights);

            var menuitem = new LocalMenuItem
            {
                MenuType = menutype,
                Label = generalaction.Name,
                Url = url,
                Identifier = identifier,
                Action = generalaction.MethodName.GetURLComponent(2),
                Description = generalaction.Description,
                Controller = generalaction.MethodName.GetURLComponent(1),
                Disabled = generalactionRights > phaseRights,
                Hidden = generalactionRights > phaseRights,
                CssClass = generalaction.ClassNameMenuItem,
                Glyphicon = generalaction.ClassNameGlyphicon,
                OrderNumber = generalaction.OrderNumber,
                Subcategory = generalaction.Subcategory
            };

            if (menuitem.Controller.Equals("FlowMain", StringComparison.InvariantCultureIgnoreCase) &&  menuitem.Action.StartsWith("Relocate", StringComparison.InvariantCultureIgnoreCase))
            {
                menuitem.Hidden = menuitem.Hidden || flowIsClosed;
            }

            if (menuitem.Action.StartsWith("CloseTransfer", StringComparison.InvariantCultureIgnoreCase))
            {
                menuitem.Hidden = menuitem.Hidden || flowIsClosed;
            }

            if (menuitem.Action.Equals("ReopenTransfer", StringComparison.InvariantCultureIgnoreCase))
            {
                menuitem.Hidden = menuitem.Hidden || !flowIsClosed;
            }

            if (menuitem.Action.Equals("InterruptTransfer", StringComparison.InvariantCultureIgnoreCase))
            {
                menuitem.Hidden = menuitem.Hidden || flowIsInterrupted;
                menuitem.Disabled = flowIsClosed;
            }

            if (menuitem.Action.Equals("ResumeTransfer", StringComparison.InvariantCultureIgnoreCase))
            {
                menuitem.Hidden = menuitem.Hidden || !flowIsInterrupted;
                menuitem.Disabled = flowIsClosed;
            }

            menuItems.Add(menuitem);
        }


        private ActionResult buildFormTabs(FlowInstance flowinstance)
        {
            if (flowinstance == null)
            {
                return Content("<!-- buildFormTabs: flow unknown -->");
            }

            if (PhaseDefinitionID <= 0)
            {
                return Content("<!-- buildFormTabs: PhaseDefinitionID unknown -->");
            }

            var phasedefinitionsub = PhaseDefinitionCacheBL.GetByID(uow, PhaseDefinitionID);
            if (phasedefinitionsub == null)
            {
                return Content("<!-- buildFormTabs: PhaseDefinition unknown -->");
            }

            if (string.IsNullOrWhiteSpace(phasedefinitionsub.Subcategory))
            {
                return Content("<!-- buildFormTabs: Subcategory unknown -->");
            }

            var phasedefinitions = PhaseDefinitionCacheBL.GetPhaseDefinitionsBySubcategory(uow, phasedefinitionsub.Subcategory, flowinstance.FlowDefinitionID).ToList();
            if (phasedefinitions.Count <= 1)
            {
                return Content("<!-- buildFormTabs: No or only one item for this subcategory -->");
            }

            var phaseinstances = flowinstance.PhaseInstances.Where(it => !(bool)it.Cancelled).ToList();

            var pointuserinfo = PointUserInfo();

            foreach (var phasedefinition in phasedefinitions.OrderBy(pd => pd.OrderNumber))
            {
                addMenuItem(phasedefinition, phaseinstances, MenuItemDestination.FormTab, pointuserinfo);
            }

            return View(menuItems.Where(mi => !mi.Hidden).OrderBy(mi => mi.OrderNumber));
        }

        private IList<LocalMenuItem> buildFlowMenu(int transferid)
        {
            var flowinstance = FlowInstanceBL.GetByTransferID(uow, transferid);
            if (flowinstance == null)
            {
                return null;
            }
            
            menuItems = mainMenuItems();

            var phaseinstances = flowinstance.PhaseInstances.Where(it => !(bool)it.Cancelled).ToList();

            var flowIsClosed = FlowInstanceBL.IsFlowClosed(uow, flowinstance);
            var flowIsInterrupted = flowinstance.Interrupted;
            var phasedefinitions = PhaseDefinitionCacheBL.GetByFlowDefinitionID(uow, flowinstance.FlowDefinitionID).ToList();
            var pointuserinfo = PointUserInfo();

            foreach (var phasedefinition in phasedefinitions)
            {
                addMenuItem(phasedefinition, phaseinstances, MenuItemDestination.Menu, pointuserinfo);
                var phaseRights = PhaseDefinitionRightsBL.GetFormRightsPhaseDefinitionID(uow, pointuserinfo, phasedefinition, null);

                foreach (
                    var generalactionphase in
                        GeneralActionPhaseBL.GetByPhaseDefinitionID(uow, phasedefinition.PhaseDefinitionID,
                            pointuserinfo.Organization.OrganizationTypeID)
                            .Where(pd => pd.GeneralAction.OrderNumber > 0)
                            .OrderBy(ga => ga.PhaseDefinition.Phase)
                            .ThenBy(ga => ga.GeneralAction.OrderNumber))
                {
                    addMenuItemActie(generalactionphase, phaseRights, flowIsClosed, flowIsInterrupted);
                }
            }

            var visibleItems = menuItems.OrderBy(mi => mi.OrderNumber).ToList();
            return visibleItems;
        }

        private IList<LocalMenuItem> buildErrorMenu()
        {
            menuItems = mainMenuItems();

            var visibleItems = menuItems.OrderBy(mi => mi.OrderNumber).ToList();

            return visibleItems;
        }

        private IList<LocalMenuItem> buildReportMenu()
        {
            var pointUserInfo = PointUserInfo();

            menuItems = mainMenuItems();

            if (pointUserInfo.GetReportFunctionRoleLevelID() > FunctionRoleLevelID.None)
            {
                menuItems.Add(new LocalMenuItem
                {
                    Action = "Index",
                    Controller = "Report",
                    Url = Url.Action("Index", "Report"),
                    Label = "Rapportage",
                    MenuType = MenuTypes.Actie,
                    Identifier = "Rapportage",
                    Description = "Rapportage",
                    OrderNumber = null,
                    Subcategory = "",
                    Disabled = false,
                    Hidden = false,
                    IsCurrent = false,
                    CssClass = "pt-color pt-blue",
                    OpenNewWindow = false
                });

                var dumps = TransferAttachmentViewBL.GetByEmployeeID(uow, pointUserInfo.Employee.EmployeeID, AttachmentTypeID.DumpReport);
                if(dumps.Any())
                {
                    menuItems.Add(new LocalMenuItem
                    {
                        Action = "CSVDumpOverview",
                        Controller = "DownloadReport",
                        Url = Url.Action("CSVDumpOverview", "DownloadReport"),
                        Label = "Download dump",
                        MenuType = MenuTypes.Actie,
                        Identifier = "Download dump",
                        Description = "Download gegenereerde dump bestanden",
                        OrderNumber = null,
                        Subcategory = "",
                        Disabled = false,
                        Hidden = false,
                        IsCurrent = false,
                        CssClass = "pt-color pt-blue",
                        OpenNewWindow = false
                    });
                }
            }

            var itemsToBeSetCurrent = menuItems.Where(mi => mi.Controller == parentController
                                                            ||
                                                            (mi.Subcategory == parentSubcategory &&
                                                             !string.IsNullOrWhiteSpace(parentSubcategory))).ToList();

            itemsToBeSetCurrent.ForEach(mi => mi.IsCurrent = true);

            var visibleItems = menuItems.OrderBy(mi => mi.OrderNumber).ToList();

            return visibleItems;
        }

        private List<LocalMenuItem> buildManagementMenu()
        {
            var menuItems = new List<LocalMenuItem>
            {
                new LocalMenuItem()
                {
                    Action = "Search",
                    Controller = "Transfer",
                    Url = Url.Action("Search", "Transfer"),
                    Label = "Zoeken dossier",
                    MenuType = MenuTypes.Main,
                    Identifier = "Zoeken",
                    Description = "Zoeken",
                    OrderNumber = null,
                    Subcategory = "",
                    Disabled = false,
                    Hidden = false,
                    IsCurrent = false,
                    Glyphicon = null,
                    CssClass = "pt-color pt-gray",
                    OpenNewWindow = false
                }
            };
            
            menuItems.AddRange(GetManagementMenuItems());

            return menuItems;
        }

        #endregion
    }
}