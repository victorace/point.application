﻿using Point.Business.Logic;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Infrastructure.Attributes;
using Point.Infrastructure.Exceptions;
using Point.Infrastructure.Extensions;
using Point.Models.Enums;
using Point.MVC.WebApplication.Helpers;
using System.Net;
using System.Web.Mvc;

namespace Point.MVC.WebApplication.Controllers
{
    public partial class FlowMainController : PointFormController
    {
        public ActionResult CopyTransfer()
        {
            var pointuserinfo = PointUserInfo();

            PageTitle = "Dossier kopieren";
            PageHeader = "Dossier kopieren";

            var flowinstance = FlowInstanceBL.GetByTransferID(uow, TransferID);
            if (flowinstance == null)
            {
                return Content("<!-- Geen flowInstance -->");
            }

            ViewBag.HasEndflowRights = PhaseInstanceBL.HasEndFlowRights(uow, flowinstance.FlowDefinitionID, ControllerName, ActionName, pointuserinfo);
            return new SecureActionResult(CopyTransferViewBL.Get(uow, flowinstance));
        }

        [HttpPost]
        [ValidateAntiFiddleInjection("transferid")]
        public ActionResult CopyTransfer([ModelBinder(typeof(CopyTransferBinder))] CopyTransferViewModel viewmodel)
        {
            var pointuserinfo = PointUserInfo();

            var flowinstance = SearchBL.GetSecurityTrimmedFlowInstance(uow, pointuserinfo, TransferID);
            if (flowinstance == null)
            {
                throw new PointSecurityException("U heeft geen toegang tot dit dossier", ExceptionType.AccessDenied);
            }

            bool hasendflowrights = PhaseInstanceBL.HasEndFlowRights(uow, flowinstance.FlowDefinitionID, ControllerName, ActionName, pointuserinfo);
            if (!hasendflowrights)
            {
                throw new PointSecurityException("U kunt dit dossier niet kopieren in deze rol", ExceptionType.AccessDenied);
            }

            Transfer transfer = null;
            try
            {
                var logentry = LogEntryBL.Create(FlowFormType.CopyTransfer, pointuserinfo);
                transfer = CopyTransferBL.Copy(uow, viewmodel, pointuserinfo, logentry);
                if (transfer != null)
                {
                    FlowInstanceSearchValuesBL.CalculateReadValuesByTransferIDAsync(uow, transfer.TransferID);
                }
            }
            catch
            {
                transfer = null;
            }

            bool iscopy = transfer != null;
            var ErrorMessage = iscopy ? "" : "Er is iets fout gegaan. Er is geen kopie gemaakt.";
            Response.StatusCode = (int)(iscopy ? HttpStatusCode.OK : HttpStatusCode.BadRequest);

            var dashboardUrl = Url.Action("Dashboard", "FlowMain", new { transferid = (transfer == null ? -1 : transfer.TransferID), iscopy });
            return Json(new { ErrorMessage, autoRedirectUrl = dashboardUrl }, JsonRequestBehavior.DenyGet);
        }
    }
}