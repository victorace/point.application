String.prototype.startsWith = function (pattern) {
    return this.lastIndexOf(pattern, 0) === 0;
}

String.prototype.endsWith = function endsWith (pattern) {
    var d = this.length - pattern.length;
    return d >= 0 && this.indexOf(pattern, d) === d;
}

var divScrollPanel;

function getScrollPanel(divID) {

    if ( divScrollPanel == null )
    {

        if (!divID || divID == null || divID.length == 0) {
            var divs = document.getElementsByTagName("div");
            for (var i = 0; i < divs.length; i++) {
                if (divs[i].id.endsWith("_ScrollPanel")) {
                    divID = divs[i].id;
                    break;
                }
            }
        }

        divScrollPanel = document.getElementById(divID);
    }

    return divScrollPanel;
}

function resizeScrollPanel(divID) {

    if (divScrollPanel == null) getScrollPanel(divID);

    if ( divScrollPanel != null ) {

        var topPos = 0;
        var heightMargin = 0;

        var parentTag = divScrollPanel;
        do {
            topPos += parentTag.offsetTop;
            parentTag = parentTag.offsetParent;
        } while (parentTag && parentTag != null && parentTag.tagName != "BODY");

        // Set 'resizeCorrection' on a specific page if it contains unmanageable height of some controls around the scrolldiv
        if (typeof(resizeCorrection) != 'undefined') heightMargin += resizeCorrection; 

        var nextSibl = divScrollPanel.nextSibling;
        if (nextSibl) {
            do {
                if (nextSibl.tagName == "TABLE" || nextSibl.tagName == "DIV") heightMargin += nextSibl.offsetHeight;
                nextSibl = nextSibl.nextSibling;
            } while (nextSibl && nextSibl != null && nextSibl.tagName != "BODY");
        }

        divScrollPanel.style.height = (getViewportHeight() - topPos - heightMargin) + 'px';
    }
}

var txbPosLeft;
var txbPosTop;

function saveScrollPosition(divID) {

    if (divScrollPanel == null) getScrollPanel(divID);

    if (txbPosLeft == null) txbPosLeft = document.getElementById('__SCROLLPOS_LEFT');
    if (txbPosTop == null) txbPosTop = document.getElementById('__SCROLLPOS_TOP');

    if (divScrollPanel != null && txbPosLeft != null && txbPosTop != null) {
        txbPosLeft.value = divScrollPanel.scrollLeft;
        txbPosTop.value = divScrollPanel.scrollTop;
    }

}

function setScrollPosition(divID) {

    if (divScrollPanel == null) getScrollPanel(divID);

    if (txbPosLeft == null) txbPosLeft = document.getElementById('__SCROLLPOS_LEFT');
    if (txbPosTop == null) txbPosTop = document.getElementById('__SCROLLPOS_TOP');

    if (divScrollPanel != null && txbPosLeft != null && txbPosTop != null) {
        if(typeof scrollAndSizeAdjustmentTop !== 'undefined') {
            txbPosTop.value = parseInt(txbPosTop.value, 10) + scrollAndSizeAdjustmentTop;
        }

        setTimeout(function () { divScrollPanel.scrollLeft = parseInt(txbPosLeft.value,10); }, 5);
        setTimeout(function () { divScrollPanel.scrollTop = parseInt(txbPosTop.value,10); }, 10);
    }
    
}

function retainScrollPosition(divID) {

    if (divScrollPanel == null) getScrollPanel(divID);

    if (divScrollPanel != null)
        divScrollPanel.onscroll = function () { saveScrollPosition(divID); };

}



function setTextBoxHeight(textboxID, keypressed) {

    var txb = document.getElementById(textboxID);

    if (txb != null) {

        var minSizeEntering = 150;
        var minSizeDisplay = 40;
        var h = minSizeDisplay;

        if (keypressed) {
            if (txb.offsetHeight < minSizeEntering) {
                txb.style.height = minSizeEntering + 'px';
            }
        }
        else {
            if (txb.scrollHeight > minSizeDisplay) {
                txb.style.height = (txb.scrollHeight + 2) + 'px';
            }
            else {
                txb.style.height = minSizeDisplay + 'px';
            }
        }
    }

}

function retainTextBoxHeight(textboxID) {

    var txb = document.getElementById(textboxID);
    if (txb != null) {
        txb.onkeypress = function () { setTextBoxHeight(textboxID, true); };
        txb.onfocus = function () { setTextBoxHeight(textboxID, true); };
        txb.onblur = function () { setTextBoxHeight((typeof textboxID != 'undefined' ? textboxID : ""), true); };
    }

}



function focusFirst(eid) {

    if (eid != null && eid != "") {
        var o = document.getElementById(eid);
        if (o != null && o.disabled != true && o.style.visibility != "hidden" && o.readOnly == false) {
            o.focus();
            return;
        }
    }

    if (divScrollPanel != null && txbPosTop != null && txbPosTop.value > 0) {
        return; // don't focus, keep the scrollposition
    }

    for (f = 0; f < document.forms.length; f++) {
        for (i = 0; i < document.forms[f].length; i++) {
            if (document.forms[f][i].type == "text" || document.forms[f][i].type == "select-one" || document.forms[f][i].type == "textarea") {
                if (document.forms[f][i].disabled != true && document.forms[f][i].style.visibility != "hidden" && (document.forms[f][i].readOnly == undefined || document.forms[f][i].readOnly == false)) {
                    try { document.forms[f][i].focus(); } catch (ex) { /* ignore */ }
                    break;
                }
            }
        }
    }

}




var _viewportheight;
function getViewportHeight() {

    if (_viewportheight === undefined) {

        if (typeof window.innerHeight != 'undefined') { //Standards compliant browsers
            _viewportheight = window.innerHeight;
        }
        else if (typeof document.documentElement != 'undefined' && typeof document.documentElement.clientHeight != 'undefined' && document.documentElement.clientHeight != 0) { // IE7-
            _viewportheight = document.documentElement.clientHeight;
        }
        else { //Older IE
            _viewportheight = document.getElementsByTagName('body')[0].clientHeight;
        }
    }
    return _viewportheight;
}

var _viewportwidth;
function getViewportWidth() {

    if (_viewportwidth === undefined) {
        if (typeof window.innerWidth != 'undefined') { //Standards compliant browsers
            _viewportwidth = window.innerWidth;
        }
        else if (typeof document.documentElement != 'undefined' && typeof document.documentElement.clientWidth != 'undefined' && document.documentElement.clientWidth != 0) { // IE7-
            _viewportwidth = document.documentElement.clientWidth;
        }
        else { //Older IE
            _viewportwidth = document.getElementsByTagName('body')[0].clientWidth;
        }
    }
    return _viewportwidth;
}


var _scrollbarWidth;
function scrollbarWidth() {
    if (_scrollbarWidth === undefined) {
        var parent, child;
        parent = $('<div style="width:50px;height:50px;overflow:auto"><div/></div>').appendTo('body');
        child = parent.children();
        _scrollbarWidth = child.innerWidth() - child.height(99).innerWidth();
        parent.remove();
    }
    return _scrollbarWidth;
}

function resizeBodypaneContainer() { // <= IE7
    var bodypanecontainer = $(".theme_bodypanecontainer");
    if (bodypanecontainer) {
        bodypanecontainer.height(2000);
        bodypanecontainer.height((getViewportHeight() - bodypanecontainer.offset().top));
    }
}
