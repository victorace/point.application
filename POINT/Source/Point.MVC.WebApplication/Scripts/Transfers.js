



handleFilterStatus = function () {
    var prevSelected = "";

    domRef.filterStatus.click(function () {
        if ($(this).val() == "1") {
            domRef.predefinedFilters.prop('disabled', true);
            prevSelected = domRef.predefinedFilters.val();
            domRef.predefinedFilters.val("-1");
        }
        else if ($(this).val() == "2") {
            domRef.predefinedFilters.prop('disabled', true);
            prevSelected = domRef.predefinedFilters.val();
            domRef.predefinedFilters.val("-1");
        }
        else {
            domRef.predefinedFilters.removeProp('disabled');
            domRef.predefinedFilters.val(prevSelected);
        }
    });

    domRef.predefinedFilters.change(function () {
        if ($(this).find("option:selected").val() == '-1') {
            domRef.filterStatus.removeProp('disabled');
        }
        else {
            domRef.filterStatus.prop('disabled', true);
        }
    }).trigger('change');
}

handleAvailableSize = function () {

    if (typeof resizeBodypaneContainer == 'function') { resizeBodypaneContainer(); }

    var sbWidth = scrollbarWidth();
    // set the widths and height to what's available
    var gridDivWidth = domRef.textSearchDiv.width() - fixedColumnWidth + sbWidth - 2;
    var gridDivHeight = getViewportHeight() - domRef.headerDiv.offset().top - domRef.headerDiv.height() - 12;

    domRef.gridDiv.width(gridDivWidth);
    domRef.gridDiv.height(gridDivHeight);
    domRef.headerDiv.width(gridDivWidth-sbWidth); 
    domRef.headerDiv.closest("td").addClass("theme_headerstyle");

    domRef.fixedResultDiv.height(gridDivHeight); 
    domRef.fixedResultDiv.width(fixedColumnWidth);
    domRef.fixedResultScroll.height(gridDivHeight - sbWidth);
    domRef.fixedResultScroll.width(domRef.fixedResultDiv.width() - sbWidth);

    domRef.mainTable.find("tr td:first").width(fixedColumnWidth - sbWidth);

}


var scrollTimer = 0;
handleTransferGridScrolling = function () {
    var isinscroll = "isinscroll" // variable to prevent them calling eachother
    domRef.gridDiv.scroll(function (ev) {
        domRef.headerTable.offset({ left: domRef.gridTable.offset().left });

        if (domRef.mainDiv.data(isinscroll)) {
            domRef.mainDiv.data(isinscroll, false);
            return;
        }
        domRef.mainDiv.data(isinscroll, true);

        domRef.fixedResultDiv.scrollTop(domRef.gridDiv[0].scrollTop);

        if (scrollTimer) clearTimeout(scrollTimer);
        scrollTimer = setTimeout(function () { domRef.mainDiv.data(isinscroll, false); domRef.fixedResultDiv.scrollTop(domRef.gridDiv[0].scrollTop); }, 100);
    });

    // and vice versa
    domRef.fixedResultDiv.scroll(function (ev, o) {
        if (domRef.mainDiv.data(isinscroll)) {
            domRef.mainDiv.data(isinscroll, false);
            return;
        }
        domRef.mainDiv.data(isinscroll, true);

        domRef.gridDiv.scrollTop(domRef.fixedResultDiv[0].scrollTop);

        if (scrollTimer) clearTimeout(scrollTimer);
        scrollTimer = setTimeout(function () { domRef.mainDiv.data(isinscroll, false); domRef.gridDiv.scrollTop(domRef.fixedResultDiv[0].scrollTop); }, 100);
    });
}

handleHideFilter = function () {
    // Put the ID's in an array and loop them, in stead of combining it in a multiple selector. Performance x100
    $.each(["#showFilterCriteriaTop", "#hideFilterCriteriaTop"], function (i, v) {
        $(v).click(function () {
            hideFilterBlock();
        }).css("cursor", "pointer");
    });
}

hideFilterBlock = function () {
    domRef.searchDiv.toggle();
    $.each(["#showFilterCriteriaTop", "#hideFilterCriteriaTop"], function (i, v) {
        $(v).toggle();
    });
    handleAvailableSize();
}

hideOnSubmit = function () {
    $("body").css("cursor", "wait");
    domRef.gridDiv.height(1);
    domRef.fixedResultScroll.hide();

    domRef.headerDiv.closest("td").removeClass("theme_headerstyle");

    domRef.fixedHeaderTable.hide();
    domRef.headerTable.hide();
    domRef.mainTable.removeClass("greyline");

    domRef.lblTextSearch.hide();

    domRef.belowGridPager.hide();

    domRef.loadingDiv.show();
}


rearrangeTransferGrid = function () {

    if (domRef.gridDiv.find("tr").length == 0) {
        domRef.mainDiv.hide();
        return;
    }

    // move the first cell from the header to the seperate div/table
    var firstth = domRef.gridTable.find("thead th:first");
    domRef.fixedHeaderTable.find("thead tr").append(firstth.height(firstth.height()));

    // move the rest of the header (completely) from the gridview to the seperate div/table
    domRef.headerTable.width(domRef.gridTable.width()); //"copy" the width, so the header will keep their width
    domRef.gridTable.find("thead").appendTo(domRef.headerTable);

    // move the first column from the gridview to the seperate div
    domRef.gridDiv.find("tr").each(function (key, row) {
        var clas = $(row).attr("class");
        var styl = $(row).attr("style");
        var parentrow = $("<tr>").appendTo(domRef.fixedResultTable).attr('class', clas).attr('style', styl);//Copy the css-class(es) and style(s) of the tablerow
        $(this).find("td:first").each(function () {
            $(this).appendTo(parentrow);
        });
    });

}






// --- Print the result of the grid, without the rest of the page ---

var printGridDiv;
var printGrid = function () {
    // Create a random name for the print frame.
    var frameName = ("printer-" + (new Date()).getTime());

    // Create an iFrame with the new name.
    var jFrame = $("<iframe name='" + frameName + "'>");

    // Hide the frame (sort of) and attach to the body.
    jFrame.css("width", "1px").css("height", "1px").css("position", "absolute").css("left", "1px").appendTo($("body:first"));


    // Get a reference to the DOM in the new frame, and add the html to it
    var objFrame = window.frames[frameName];
    var objDoc = objFrame.document;
    objDoc.open();
    objDoc.write('<html><head>');
    objDoc.write('<title>POINT - Zoeken</title>');
    for (var i = 0; i < document.styleSheets.length; i++) {
        if (document.styleSheets[i].href != "") {
            objDoc.write('<link href="' + document.styleSheets[i].href + '" type="text/css" rel="stylesheet" />');
        }
    }
    objDoc.write('<style type="text/css"> A { color:black !important; }</style>');
    objDoc.write('</head>');
    objDoc.write('<body>');
    objDoc.write(printGridDiv.html());
    objDoc.write('</body></html>');
    objDoc.close();

    setTimeout(function () { objFrame.focus(); objFrame.print(); jFrame.remove(); }, 2000);

}





$(document).ready(function () {

    //$.fillDomReferences();
    domRef = GetDomRef();

    handleFilterStatus();

    // fix that ugly asp-rules-border-thing
    domRef.gridTable.css("border", "none").removeAttr("border").removeAttr("rules");
    // and add a nice line on the bottom of all rows
    domRef.mainTable.addClass("greyline");

    // Save the untouched table to our variable
    printGridDiv = domRef.gridDiv.clone();

    handleAvailableSize();

    rearrangeTransferGrid();

    handleTransferGridScrolling();

    handleHideFilter(); // The filter-block is hidden by default on page load.

    // set a wait cursor when clicking the buttons or one of the grid-actionlinks, and hide the several blocks
    $.each([domRef.btnSearch, domRef.btnReset], function (i, v) {
        $(v).click(function () { hideOnSubmit(); });
    });

    $("#divScopeButtons input").click(function () { $(this).closest("div").find("input").removeClass("TransferRow_ButtonBorder"); $(this).blur().addClass("TransferRow_ButtonBorder"); hideOnSubmit(); });
    domRef.belowGridPager.find("a").click(function () { hideOnSubmit(); });
    domRef.headerTable.find("thead th a").click(function () { hideOnSubmit(); });
    domRef.fixedHeaderTable.find("thead th a").click(function () { hideOnSubmit(); });

    // Done... hide the loader and show the results...                
    domRef.loadingDiv.hide();

    domRef.belowGridPager.show();

    $(window).resize(function () {
        handleAvailableSize();
    });

});
