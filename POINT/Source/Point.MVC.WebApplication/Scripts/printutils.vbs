Option Explicit

Const pc_strIEPrintSettingsRegistryPath = "HKCU\Software\Microsoft\Internet Explorer\PageSetup\"
Const pc_strIEPrintSettingsHeader       = "header"
Const pc_strIEPrintSettingsFooter       = "footer"
Const pc_strIEPrintSettingsMarginTop    = "margin_top"
Const pc_strIEPrintSettingsMarginBottom = "margin_bottom"
Const pc_strIEPrintSettingsMarginLeft   = "margin_left"
Const pc_strIEPrintSettingsMarginRight  = "margin_right"

Dim strHeader       'As String
Dim strFooter       'As String
Dim strMarginTop    'As String
Dim strMarginBottom 'As String
Dim strMarginLeft   'As String
Dim strMarginRight  'As String
Dim blnMarginsAdjusted 'As Boolean

Function PrintOverzichtForm(ByVal strHeaderText) 'As Boolean
    
    Dim oShell          'As Object
    Dim strErrorMessage 'As String
   
    PrintOverzichtForm = False
    
    strErrorMessage = ""
    strHeader = ""
    strFooter = "" 
    strMarginTop = "" 
    strMarginBottom = "" 
    strMarginLeft = "" 
    strMarginRight = "" 
    blnMarginsAdjusted = False
    
    Set oShell = Nothing
    On Error Resume Next
        Set oShell = CreateObject("WScript.Shell") 
    On Error Goto 0
    
    If oShell Is Nothing Then
        strErrorMessage = "Kan de print marges niet aanpassen doordat de beveiligingsinstellingen " & vbcrlf & _
                          "van de browser het niet toelaat om ActiveX objecten aan te maken." & vbcrlf & vbcrlf & _
                          "De pagina zal worden afgedrukt met de standaard ingestelde marges."
    End If
    
    If strErrorMessage = "" Then
        '
        'First store original settings
        '
        strHeader = oShell.RegRead(pc_strIEPrintSettingsRegistryPath & pc_strIEPrintSettingsHeader)
        strFooter = oShell.RegRead(pc_strIEPrintSettingsRegistryPath & pc_strIEPrintSettingsFooter)
        strMarginTop = oShell.RegRead(pc_strIEPrintSettingsRegistryPath & pc_strIEPrintSettingsMarginTop)
        strMarginBottom = oShell.RegRead(pc_strIEPrintSettingsRegistryPath & pc_strIEPrintSettingsMarginBottom)
        strMarginLeft = oShell.RegRead(pc_strIEPrintSettingsRegistryPath & pc_strIEPrintSettingsMarginLeft)
        strMarginRight = oShell.RegRead(pc_strIEPrintSettingsRegistryPath & pc_strIEPrintSettingsMarginRight)

        If strMarginTop = "" And strMarginBottom = "" And strMarginLeft = "" And strMarginRight = "" Then
            strErrorMessage = "Kan de print marges niet aanpassen doordat de huidige " & vbcrlf & _
                              "beveiligingsinstellingen het niet toelaat om het register uit te lezen." & vbcrlf & vbcrlf & _
                              "De pagina zal worden afgedrukt met de standaard ingestelde marges."
        Else
            '
            'Set the margins, header and footer
            '
            On Error Resume Next
            oShell.RegWrite pc_strIEPrintSettingsRegistryPath & pc_strIEPrintSettingsHeader, strHeaderText
            oShell.RegWrite pc_strIEPrintSettingsRegistryPath & pc_strIEPrintSettingsFooter, "Pagina &p van &P"
            If Err.Number <> 0 Then
                strErrorMessage = "Kan de print marges niet aanpassen doordat de huidige " & vbcrlf & _
                                  "beveiligingsinstellingen het niet toelaat om het register te bewerken." & vbcrlf & vbcrlf & _
                                  "De pagina zal worden afgedrukt met de standaard ingestelde marges."
            Else
                blnMarginsAdjusted = True
            End If
            On Error Goto 0
        End If
        
    End If
    
    If strErrorMessage <> "" Then
        MsgBox strErrorMessage, 0, "Formulier printen"
    End If
    
    'Only print the main frame
    parent.print()
    Set oShell = Nothing

    PrintOverzichtForm = True

End Function


Function ResetMarginsToDefault()

    If blnMarginsAdjusted And strMarginTop <> "" And strMarginBottom <> "" And strMarginLeft <> "" And strMarginRight <> "" Then
        Dim oShell          'As Object
        Set oShell = CreateObject("WScript.Shell") 

        strHeader = "&u&b&d"
        strFooter = "Pagina &p van &P"

        'Set back to original values
        oShell.RegWrite pc_strIEPrintSettingsRegistryPath & pc_strIEPrintSettingsHeader, strHeader
        oShell.RegWrite pc_strIEPrintSettingsRegistryPath & pc_strIEPrintSettingsFooter, strFooter
        
        Set oShell = Nothing
       
    End If

    ResetMarginsToDefault = True
End Function


Function ResetMarginsToStandart()

        Dim oShell          'As Object
        Set oShell = CreateObject("WScript.Shell") 

        strHeader = "&u&b&d"
        strFooter = "Pagina &p van &P"
              
        'Set back to standart values
        oShell.RegWrite pc_strIEPrintSettingsRegistryPath & pc_strIEPrintSettingsHeader, strHeader
        oShell.RegWrite pc_strIEPrintSettingsRegistryPath & pc_strIEPrintSettingsFooter, strFooter
        
        Set oShell = Nothing
       
    ResetMarginsToDefault = True
End Function