﻿/*The user cannot type characters into the input field, only numbers*/
function OnlyNumbers(e) {
    if (e.shiftKey) {
        return false;
    }
    if (e.keyCode == 46 || e.keyCode == 8 || e.keyCode == 9) {  // Backspace, delete or TAB are OK.                    
        return true;
    }
    else {
        if (e.keyCode < 95) {
            if (e.keyCode < 48 || e.keyCode > 57) {

                return false;
            }
        }
        else {
            if (e.keyCode < 96 || e.keyCode > 105) {
                return false;
            }
        }

        return true;
    }
}
/*The user cannot type characters into the input field, only numbers and dot*/
function OnlyNumbersAndDot(e) {
    if (e.shiftKey) {
        return false;
    }
    if (e.keyCode == 46 || e.keyCode == 8 || e.keyCode == 9 || e.keyCode == 190) {  // Backspace, delete, TAB or '.' are OK.                    
        return true;
    }
    else {
        if (e.keyCode < 95) {
            if (e.keyCode < 48 || e.keyCode > 57) {

                return false;
            }
        }
        else {
            if (e.keyCode < 96 || e.keyCode > 105) {
                return false;
            }
        }

        return true;
    }
}