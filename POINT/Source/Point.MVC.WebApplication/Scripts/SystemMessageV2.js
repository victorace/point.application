﻿var systemMessageIds = [];

function ReadLater() {
    $("#systemMessageModal").modal('hide');
}

function MessagesRead() {
    $("#systemMessageModal").modal('hide');
    $("#liSystemMessages").hide();
    $.ajax({
        type: 'POST',
        dataType: 'text',
        cache: false,
        url: messageReadUrl
    });
}

function GetMessages() {
    var max_messages = 3;
    var url = messageSourceUrl;

    $.getJSON(url,
        function(data) {

            if (data.length <= 0) {
                return;
            }

            var items = [];
            var itemnr = 1;

            $.each(data,
                function(key, val) {
                    if (itemnr <= max_messages || max_messages === 0) {

                        systemMessageIds.push(val.SystemMessageID);

                        var dateFormatted = "";

                        var date = new Date(+val.StartDate.replace(/\/Date\((\d+)\)\//, '$1'));
                        if (isNaN(date.getMonth()) === false) {
                            var month = [
                                'Januari', 'Februari', 'Maart', 'April', 'Mei', 'Juni',
                                'Juli', 'Augustus', 'September', 'Oktober', 'November', 'December'
                            ];
                            dateFormatted = date.getDate() + " " + month[date.getMonth()] + " " + date.getFullYear();
                        }

                        items.push("<div class=\"panel panel-default\">");

                        var permanent = val.IsPermanent
                            ? "<span class=\"label label-warning\">Permanent bericht</span>"
                            : "";

                        var link = val.LinkReference.length > 0 && val.LinkText.length > 0
                            ? "<div><br><b>Zie:</b> <a class='text-info' href='" +
                            val.LinkReference +
                            "' target='_blank'>" +
                            val.LinkText +
                            "</a></div>"
                            : "";

                        items.push("<div class=\"panel-heading\">");
                        items.push("<div class=\"systemmessage-title\">" + val.Title + "</div>");
                        items.push("<span class=\"systemmessage-date\">" + dateFormatted + "</span>");
                        items.push("</div>");
                        items.push("<div class=\"panel-body\">"
                            + val.Message + link + permanent + "</div>");
                        items.push("</div>");

                        itemnr += 1;
                    }
                });

            $("#systemMessageModalBody").html(items.join(""));

            $("#systemMessageModal").modal();
        });
}