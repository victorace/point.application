﻿if (typeof initInterceptors == 'function') { initInterceptors(); }; if (typeof focusFirst == 'function') { focusFirst(''); }; var ctl00_portalMenu_Data = new Object();
ctl00_portalMenu_Data.disappearAfter = 500;
ctl00_portalMenu_Data.horizontalOffset = 0;
ctl00_portalMenu_Data.verticalOffset = 0;
ctl00_portalMenu_Data.hoverClass = 'ctl00_portalMenu_15 theme_TopNav_Hover';
ctl00_portalMenu_Data.hoverHyperLinkClass = 'ctl00_portalMenu_14 theme_TopNav_Hover ctl00_portalMenu_FixWhiteHover';
ctl00_portalMenu_Data.staticHoverClass = 'ctl00_portalMenu_13 theme_Topnav_Static';
ctl00_portalMenu_Data.staticHoverHyperLinkClass = 'ctl00_portalMenu_12 theme_Topnav_Static';

var Page_ValidationActive = false;
if (typeof (ValidatorOnLoad) == "function") {
    ValidatorOnLoad();
}

function ValidatorOnSubmit() {
    if (Page_ValidationActive) {
        return ValidatorCommonOnSubmit();
    }
    else {
        return true;
    }
}