/**
 *
 * This script adds some extra functionality on the Date-object
 * as an extension of the 'date.js'
 *
**/

var reISO = /^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2}(?:\.\d*))(?:Z|(\+|-)([\d|:]*))?$/;
var reMsAjax = /^\/Date\((d|-|.*)\)[\/|\\]$/;

Date.prototype.jsonParseString = function (val) {
    if (typeof val === 'string') {
        var matchISO = reISO.exec(val);
        if (matchISO) {
            return new Date(val);
        }
        var matchMsAjax = reMsAjax.exec(val);
        if (matchMsAjax) {
            var splitMsAjax = matchMsAjax[1].split(/[-+,.]/);
            return new Date(splitMsAjax[0] ? + splitMsAjax[0] : 0 - +splitMsAjax[1]);
        }
    }
    return val;
};
