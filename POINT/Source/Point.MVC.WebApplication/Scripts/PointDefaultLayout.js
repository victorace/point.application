﻿$(function () {
    var timeout = null;
    var loaderLoaded = false;
    $(document)
        .ajaxStart(function () {
            timeout = setTimeout(function () {
                $("#loader").dialog({
                    modal: true,
                    dialogClass: 'noTitle'
                });
                loaderLoaded = true;
            }, 200);
            $("#error").hide();
        })
        .ajaxComplete(function () {
            clearTimeout(timeout);
            if (loaderLoaded) {
                $("#loader").dialog("close");
            }
        })
        .ajaxError(function (event, jqxhr, settings, thrownError) {
            $("#loader").dialog("close");
        });

    $("#loader").click(function () { $(this).dialog("close"); })
    $("#error").click(function () { $(this).hide(); })
    $("#success").click(function () { $(this).hide(); })
});

var showErrorMessage = function (message, splitter) {

    if (splitter) message = message.split(splitter).join("**");

    $("#error #errormessage").html(message);
    $("#error").show().delay(6000).fadeOut();
};

var errorMessageIsShowing = function () {
    return $('#error').is(':visible');
};

var showSuccessMessage = function (message) {
    $("#success #successmessage").html(message);
    $("#success").show().delay(4000).fadeOut();
};
var showLoader = function () {
    $("#loader").show();
    $("#error").hide();
    $("#success").hide();
};

var getQueryStringParameter = function (name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
};