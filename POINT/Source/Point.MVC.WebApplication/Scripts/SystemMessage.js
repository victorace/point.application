﻿$(document).ready(function () {
    GetMessages();
});

var max_messages = 3;

function GetMessages() {
    var apppath = $("#hflAppPath").val();
    var url = apppath + '/SystemMessages/Handlers/GetMessages.ashx';
    $.getJSON(url, function (data) {
        if(data.length > 0)
        {
            $(".HeaderSystemMessage").html(data[0].Title + " <i>(<u>Lees verder</u>)</i>")
            $(".HeaderSystemMessage").show();
            var show_on_load = $("#hflShowOnLoad").val();
            if (show_on_load == 'True') {
                FillMessages(data);
            }
        } else {
            $(".HeaderSystemMessage").hide();
        }
    });
}

function FillMessages(data)
{
    $("#SystemMessages").empty();
    if(data != "")
    {
        var items = [];
        var itemnr = 1;

        if (data.length > 0)
        {
            $.each(data, function (key, val) {

                if (itemnr == 1)
                {
                    $("#hflLastMessageId").val(val.SystemMessageID);
                }

                var date_formatted = "";

                var date = new Date(+val.StartDate.replace(/\/Date\((\d+)\)\//, '$1'));
                if (isNaN(date.getMonth()) == false)
                {
                    var month = ['Januari', 'Februari', 'Maart', 'April', 'Mei', 'Juni', 'Juli', 'Augustus', 'September', 'Oktober', 'November', 'December'];
                    date_formatted = date.getDate() + " " + month[date.getMonth()] + " " + date.getFullYear();
                }
                //apply a class to the max_visible messages so we can select them later more easily
                if (itemnr <= max_messages)
                    items.push("<div class=\"SystemMessage SystemMessageVisible\">");
                else
                    items.push("<div class=\"SystemMessage\">");

                var permanent = "";
                if (val.IsPermanent) {
                    permanent = "<div class=\"SystemMessageMessagePermanent\">Permanent bericht</div>";
                }

                //Create the actual messages
                items.push("<div class=\"SystemMessageDate\">" + date_formatted + "</div>");
                items.push("<div class=\"SystemMessageTitle\">" + val.Title + "</div>");
                items.push("<div class=\"SystemMessageMessage\">" + val.Message + permanent + "</div>");
                items.push("</div>");

                itemnr += 1;
            });

            //put the messages in #UserMessages
            $("#SystemMessages").append(items.join(""));

            //Make the control scrollable if needed
            if (data.length > max_messages) {
                $("#SystemMessages").addClass('SystemMessagesScrollable');
            } else {
                $("#SystemMessages").removeClass('SystemMessagesScrollable');
            }

            //First show in order to measure the actual height
            $(".SystemMessageContainer").show();

            var visibleitems = $(".SystemMessageVisible");
            var totalheight = 0;

            //Measure each items height
            $.each(visibleitems, function (key, val) {

                var h = $(val).outerHeight(true);
                totalheight += h;
            });

            //Set the exact height of the box to the height of the total visible messages
            $("#SystemMessages").css('height', totalheight - 2);

            //Center the "window" vertically
            var total_height = $(window).height();
            var overlay_height = $(".SystemMessageOverlay").height();
            $(".SystemMessageOverlay").css("top", Math.round(total_height / 2 - overlay_height / 2) + "px");
        }
    }
}

function CheckMessageOverlay(force) {
    var show_on_load = $("#hflShowOnLoad").val();
    if (show_on_load == 'True' || force) {
        
        var apppath = $("#hflAppPath").val();
        var url = apppath + '/SystemMessages/Handlers/GetMessages.ashx';
        $.getJSON(url, function (data) {
            FillMessages(data);
        });
    }
}

function CloseMessageOverlay(markread) {
    var last_id = ""

    if(markread)
     last_id = $("#hflLastMessageId").val();

    $("#hflShowOnLoad").val('False');
    $(".SystemMessageContainer").hide();

    var appPath = $("#hflAppPath").val();
    $.ajax({ async: false, type: 'POST', dataType: 'text', data: 'LastMessageId=' + last_id, cache: false, url: appPath + '/SystemMessages/Handlers/MarkMessageRead.ashx' });
    
    if(markread)
        GetMessages();
    
}