﻿function fix(skip_validate) {
    var valid = true;
    if(!skip_validate) valid = Page_ClientValidate("");

    if (valid)
    {
        fixEncoding("txtTitle");
        fixEncoding("txtMessage");
    }
}

function fixEncoding(txbId) {
    var txbObj = document.getElementById(txbId);
    if (txbObj != null && txbObj != 'undefined') {
        var txbVal = txbObj.value;
        while (txbVal.indexOf("<") != -1) {
            txbVal = txbVal.replace("<", "&lt;");
        }
        while (txbVal.indexOf(">") != -1) {
            txbVal = txbVal.replace(">", "&gt;");
        }
        txbObj.value = txbVal;
    }
}

function resetFix() {
    resetFixEncoding("txtTitle");
    resetFixEncoding("txtMessage");
}

function resetFixEncoding(txbId) {
    var txbObj = document.getElementById(txbId);
    if (txbObj != null && txbObj != 'undefined') {
        var txbVal = txbObj.value;
        while (txbVal.indexOf("&lt;") != -1) {
            txbVal = txbVal.replace("&lt;", "<");
        }
        while (txbVal.indexOf("&gt;") != -1) {
            txbVal = txbVal.replace("&gt;", ">");
        }
        txbObj.value = txbVal;
    }

}