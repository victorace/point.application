﻿swallowBackspace = function() {
    /*
     * this swallows backspace keys on any non-input element.
     * stops backspace -> back
     */
    var rx = /INPUT|TEXTAREA/i;
    $(document).bind("keydown keypress", function (e) {
        if (e.which == 8) { // 8 == backspace
            var is_checkbox = $(e.target).is(':checkbox');
            var is_radiobox = $(e.target).is(':radio');

            if (!rx.test(e.target.tagName) || e.target.disabled || e.target.readOnly || is_checkbox || is_radiobox) {
                e.preventDefault();
            }
        }
    });
}

$(function () {

    swallowBackspace();

});