﻿// Reference website: http://blogs.msdn.com/howard_dierking/archive/2007/04/23/polymorphic-javascript-well-kind-of.aspx?CommentPosted=true#commentmessage -->  
var myVar; 
var myTimeoutID; 
var myNode, myData; 
var ref_Menu_HoverStatic; 
var ref_Menu_Unhover; 
var ref_overrideMenu_HoverStatic; 
 
// This function is called in <body onload="..."> 
function initInterceptors() 
{           
        // *** Interceptors *** 
        // @:: Menu_Hover
        if (typeof Menu_HoverStatic != 'function') return;

        ref_Menu_HoverStatic = Menu_HoverStatic; 
        Menu_HoverStatic = My_Menu_HoverStatic; 
         
        // @:: Menu_Unhover 
        ref_Menu_Unhover = Menu_Unhover; 
        Menu_Unhover = My_Menu_Unhover; 
         
        // @:: overrideMenu_HoverStatic 
        ref_overrideMenu_HoverStatic = Menu_HoverStatic; 
        overrideMenu_HoverStatic = My_overrideMenu_HoverStatic; 
} 
 
function My_Menu_HoverStatic(item) 
{        
        My_overrideMenu_HoverStatic(item); 
} 
 
function My_overrideMenu_HoverStatic(item) 
{ 
        var node = Menu_HoverRoot(item); 
    var data = Menu_GetData(item); 
        myNode=node; 
        myData=data; 
    if (!data) return;   
         
        myVar = item;                    
        myTimeoutID=setTimeout("My_DelayExpandMenu(myNode,myData)",5); 
} 

function My_DelayExpandMenu(node, data) 
{        
    __disappearAfter = 100; //data.disappearAfter; 
    Menu_Expand(node, data.horizontalOffset, data.verticalOffset);  
} 

function My_Menu_Unhover(item) 
{                
        clearTimeout(myTimeoutID); 
        ref_Menu_Unhover(item); 
} 
               
