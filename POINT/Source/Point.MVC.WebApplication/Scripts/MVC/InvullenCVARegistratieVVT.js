﻿$(function () {

    //Ignore validation on all elements inside a container with ignore as classname
    $.validator.setDefaults({ ignore: '.ignore *' });

    SetupDependencies()
});

function SetupDependencies() {
    SelectToggleChildren("#PatientOntslagenVVTNaar", "#OntslagenNaarAnderZiekenhuis", ["0"]);
    SelectToggleChildren("#PatientOntslagenVVTNaar", "#OntslagenNaarAnders", ["7"]);
    SelectToggleChildren("#PatientOntslagenVVTNaar", "#OntslagenNaarVVT", ["2", "3", "4", "5", "6", "8"]);
}

function SearchOrganization(sender, url, callback) {
    ShowModal(url, callback, inittable)
}

var PutHealthcareProvider = function (department) {
    $('input[data-linked="PatientOntslagenVVTNaarVVT"]:not([type=hidden])').val(department.DepartmentName);
    $('input[data-linked="PatientOntslagenVVTNaarVVT"][type=hidden]').val(department.DepartmentID);
    $("#modal").modal('hide');
};