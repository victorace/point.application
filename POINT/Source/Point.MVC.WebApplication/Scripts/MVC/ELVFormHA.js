﻿$(function () {

    $('[data-toggle="clickover"]:not(.client-header)').clickover({
        html: true,
        placement: $(this).attr("data-placement") ? $(this).data("placement") : "top",
        global_close: true,
        esc_close: true,
        template: '<div class="popover zorgvraagpopover"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>',
        content: $(this).data("content")
    });

    $("#divelvzorgtype input.elvzorgtype").change(function () { OnELVZorgtypeChanged($(this)); });
    OnELVZorgtypeChanged(); 

    $("#divwlzzorgprofiel input.wlzzorgprofiel").change(function () { OnWLZZorgprofielChanged($(this)); });
    OnWLZZorgprofielChanged();
    
    $(window).on("resize", function () {
        var height = GetScrollHeight();
        $("#Scroller").css("max-height", height);
    });
    $(window).trigger("resize");
});

function OnELVZorgtypeChanged(sender)
{
    if (sender === undefined || sender.length === 0) {
        sender = $("#divelvzorgtype input.elvzorgtype:checked");
    }
    
    $("#divelvzorgtype input.elvzorgtype").prop('checked', false);
    if (sender.length > 0) {
        sender.prop('checked', true);

        $("#ELVTypeSection").toggle(sender.val().toLowerCase() == "elv");
        $("#WLZPGSection").toggle(sender.val().toLowerCase() == "wlzpg");
    }
}

function OnWLZZorgprofielChanged(sender) {
    if (sender === undefined || sender.length === 0) {
        sender = $("#divwlzzorgprofiel input.wlzzorgprofiel:checked");
    }

    $("#divwlzzorgprofiel input.wlzzorgprofiel").prop('checked', false);
    if (sender.length > 0) {
        sender.prop('checked', true);

        $("#WLZJaSection").toggle(sender.val().toLowerCase() == "ja");
        $("#WLZNeeOnbekendSection").toggle(sender.val().toLowerCase() == "nee" || sender.val().toLowerCase() == "onbekend");
    }
}


