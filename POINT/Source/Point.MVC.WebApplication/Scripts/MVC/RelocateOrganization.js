﻿function SearchOrganization(sender, url, callback) {
    ShowModal(url, callback, inittable)
}

var PutOrganisation = function (organization) {
    $("#DestinationOrganizationID").val(organization.OrganizationID);
    $("#DestinationOrganizationName").val(organization.OrganizationName);
    $("#modal").modal("hide");

    $("#DestinationLocationID").removeAttr("change");
    $("#DestinationLocationID").children().remove();
    $("#DestinationLocationID").val(undefined);

    $("#DestinationDepartmentID").children().remove();
    $("#DestinationDepartmentID").val(undefined);
    
    $("#DestinationTransferPoint").children().remove();
    $("#DestinationTransferPoint").val(undefined);

    $.ajax({
        type: "POST",
        url: jsGetLocationsByOrganizationAndFlowDefinitionID,
        data: { organizationID: organization.OrganizationID, flowdefinitionID: jsFlowDefinitionID },
        success: function (data) {
            $("#DestinationLocationID").removeAttr("change");

            fillSelectWithOptions($("#DestinationLocationID"), data);

            $("#DestinationLocationID").change(function () {
                var locationid = $(this).val();
                if (locationid === "") {
                    $("#DestinationDepartmentID").children().remove();
                }

                $("#DestinationDepartmentID").val(locationid);

                $.ajax({
                    type: "POST",
                    url: jsGetDepartmentsByLocation,
                    data: { locationID: locationid },
                    success: function (data) {
                        var selecteddepartment = $("#DestinationDepartmentID");
                        if (selecteddepartment.length > 0) {
                            fillSelectWithOptions(selecteddepartment, data);

                            var selectedDepartmentOptions = selecteddepartment[0].options;
                            if (selectedDepartmentOptions.length === 2) {
                                var singleton = selectedDepartmentOptions[1];
                                $("#DestinationDepartmentID").val(singleton.value);
                            }
                        }
                    }
                });

                $.ajax({
                    type: "POST",
                    url: jsGetTransferPointsByLocation,
                    data: { locationID: locationid },
                    success: function (data) {
                        var selectedtp = $("#DestinationTransferPoint");
                        if (selectedtp.length > 0) {
                            fillSelectWithOptions(selectedtp, data);

                            var selectedDepartmentOptions = selectedtp[0].options;
                            if (selectedDepartmentOptions.length === 2) {
                                var singleton = selectedDepartmentOptions[1];
                                $("#DestinationTransferPoint").val(singleton.value);
                            }
                        }
                    }
                });

            });

            var selectedLocationOptions = document.getElementById("DestinationLocationID").options;
            if (selectedLocationOptions.length === 2) {
                var singleton = selectedLocationOptions[1];
                $("#DestinationLocationID").val(singleton.value);
                $("#DestinationLocationID").trigger("change");
            }
        }
    });
};

$(function () {
    var thisForm = $(formID);
    thisForm.submit(function (e) {
        e.preventDefault();
        AjaxSave(formID, thisForm.action, false, function (data) {
            $("#movedmodal").on("shown.bs.modal", function () {
                $("#movedmodal .modal-body").html(data.message);
                $("#movedmodal #btnmovedok").on("click", function () {
                    $("#movedmodal").modal("hide");
                    location.replace(data.returnurl);
                });
            });
            $("#movedmodal").modal({ show: "true", backdrop: "static", keyboard: false });
        }, true);
    });
});