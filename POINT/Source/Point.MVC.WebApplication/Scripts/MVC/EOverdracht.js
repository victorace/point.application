﻿function handleTelephoneContactpersoon(data, targetselector) {
    if (data !== undefined && data.length === 1) {
        $(targetselector).changeval(data[0].Text);
    }
}

function handleEmailContactpersoon(data, targetselector) {
    if (data !== undefined && data.length === 1) {
        $(targetselector).changeval(data[0].Text);
    }
}

function SearchField() {
    var search = $("#searchfield").val();
    var searchedelements = $("#Scroller .collapse:contains('" + search + "')");
    searchedelements.collapse("show");
    if (searchedelements.length > 0) {
        $('#Scroller').animate({
            scrollTop: searchedelements.first().offset().top + $('#Scroller').scrollTop()
        }, 500);
    }
}

var unfolded = false;
function FoldAll() {
    if (unfolded === false) {
        $("#Scroller .collapse").collapse("show");
        unfolded = true;
    }
    else {
        $("#Scroller .collapse").collapse("hide");
        unfolded = false;
    }
}

function getLabelByID(elementID) {
    var element = $("#" + elementID);
    var label = element.parents(".form-group").find(".control-label");
    var header = element.parents(".form-group").siblings("h4");
    if (!header.length) {
        header = element.parents(".form-group").parent().siblings("h4");
    }
    var retval = label.html();
    if (header.length) {
        retval = header.html() + " - " + retval;
    }
    return retval;
}

function ShowResendQuestion()
{
    $("#resendModal").modal('show');
}

function SendOverdracht()
{
    $("#resendModal").modal('hide');
    AjaxSave(formID, resendVOUrl);
}

//Ignore validation on all elements inside a container with ignore as classname
$.validator.setDefaults({ ignore: '.ignore *' });

function SetupContainers()
{
    $("#Scroller input[type=text]:filled,input[type=radio]:checked,textarea:filled,select:filled").parents("li").addClass("active");
    $("#Scroller input[type=text],input[type=radio],textarea,select").on("change keyup click", "", function (e) {

        e.stopPropagation();

        var isfilled = this.checked || $(this).is("select:filled") || $(this).is("textarea:filled") || $(this).is("[type=text]:filled");

        if (isfilled)
            $(this).parents("li").addClass("active");
        else {
            if ($(this).parents(".uitklapcontainer").find("input[type=radio]:checked, input[type=text]:filled, select:filled, textarea:filled").length === 0)
                $(this).parents("li").removeClass("active");
        }
    });
}

function SetupOther()
{
    $('#OrganisatieNaamContactpersoon').on("change", function () {
        var employeeID = $(this).val();
        var digest = $(this).find(":selected").data("digest");
        $.ajax({
            type: "POST",
            url: getEmployeePhoneNumberByEmployeeID,
            data: { employeeID: employeeID, digest: digest },
            success: function (data) { handleTelephoneContactpersoon(data, '#OrganisatieTelefoonnummer'); }
        });
        $.ajax({
            type: "POST",
            url: GetEmployeeEmailAddressByEmployeeID,
            data: { employeeID: employeeID, digest: digest },
            success: function (data) { handleEmailContactpersoon(data, '#OrganisatieEmail'); }
        });
    });
    var voedingOndervoedingAanwezigMeetmethodeName = GetNameByID("#VoedingOndervoedingAanwezigMeetmethode");
    $("[name='" + voedingOndervoedingAanwezigMeetmethodeName + "']input:radio").on("click", function () {
        FillVoedingOndervoedingAanwezigScores($(this).val(), null);
    });
}

function FillVoedingOndervoedingAanwezigScores(methode, callback, source) {
    $.ajax({
        type: "POST",
        url: methode === "SNAQ" ? GetOndervoedingsScoreSNAQ : GetOndervoedingsScoreMUST,
        success: function (data) {
            fillSelectWithOptions($('#VoedingOndervoedingAanwezigScore'), data, source);
            if (callback) {
                callback();
            }
        }
    });
}

function SetupAttachment()
{
    $('.insertAttachment').on('click', function (e) {
        $('#dialog-insert').modal('show');
        var url = $(this).data("url");
        $.ajax({
            type: "GET",
            url: url,
            success: function (data) {
                $("#InsertAttachment").html(data);
            }
        });
    });

    var formInsertOptions = {
        url: transferAttachmentInsertUrl,
        type: 'post',
        dataType: 'json',
        clearForm: true,
        success: function (data, textStatus, jqxhr, form) {
            if (data.ErrorMessage) {
                showErrorMessage($.parseJSON(jqxhr.responseText).ErrorMessage);
            }
            else {
                $("#VOFormulierTransferAttachmentID").changeval(data.TransferAttachmentID);
                $("#VOFormulierTransferFileName").changeval(data.FileName);
                $("#VOFormulierUploadDate").changeval(data.UploadDate);
                $("#VOFormulierEmployeeName").changeval(data.EmployeeName);

                $('#dialog-insert').modal('hide');

                $("#attachmentHref").attr("href", data.DownloadUrl);
                $("#attachmentHref").text(data.FileName);
                $("#attachmentName").toggle(true);

                $("#VOFormulierUploadDateText").text(data.UploadDate);
                $("#VOFormulierEmployeeNameText").text(data.EmployeeName);

                FormIsDirty();

                showSuccessMessage("Bijlage is succesvol bijgewerkt/toegevoegd.");
            }
        },
        error: function (xhr, status, error) {
            var msg;
            try {
                var response = JSON.parse(xhr.responseText);
                msg = response.ErrorMessage;
            }
            catch (ex) {
                msg = xhr.statusText;
            }
            showErrorMessage(msg);
        }
    };

    $("#dialog-insert #btnUpload").click(function (ev) {
        $("#dialog-insert #transferAttachmentForm").ajaxSubmit(formInsertOptions);
    });
}

function setVOFormulierTypeValidation(value) {
    var validate = validateondefinitive;
    var externalform = false;
    if (value === "Standaard") {
        // default
    }
    else if (value === "Pdf") {
        validate = false;
        externalform = true; 
    }
    else if (value === "Aan patient") {
        validate = false;
        externalform = true;
    }
    var btndefinitive = $("#phaseButtonsHolder").find(".phaseButtonDefinitive, .phaseButtonNext");
    if (btndefinitive.length === 1) {
        $(btndefinitive).data("showvalidationerrors", validate);
        $(btndefinitive).data("externalform", externalform);
    }

    return validate;
}

function HandleJa(sourcename, targetselector) {
    //Simplified for performance reasons (doesnt check for children to reset "GotSetChildren")
    $("[data-field='" + sourcename + "']input:radio").on("change", function () {
        $(targetselector).toggle(this.value === 'Ja' && this.checked);
        updateIgnore();
    });
    
    updateIgnore();

    function updateIgnore() {
        if ($("input[data-field='" + sourcename + "']:checked").val() === "Ja") {
            $(targetselector).removeClass("ignore");
        } else {
            $(targetselector).addClass("ignore");
        }
    }
}

function SetupRadios()
{
    var radioname = GetNameByID("#VOFormulierType");
    GetRadioByName(radioname).on("change", function () {

        var value = this.value;

        $("#pdfSection").toggle(value === "Pdf");
        $("#standardSection").toggle(value === "Standaard");
        $("#handedToSection").toggle(value === "Aan patient");
        $("#showall").toggle(value === "Standaard");

        if (value === "Standaard") {
            ClearContainer('#pdfSection');
            ClearContainer('#handedToSection');

            $("#attachmentName").toggle(false);
            $("#VOFormulierTransferAttachmentID").changeval("");

            $("#pdfSection").addClass("ignore");
            $("#handedToSection").addClass("ignore");
            $("#standardSection").removeClass("ignore");
            $("#epdButtonContainer").toggle(true);
        }
        else if (value === "Pdf") {
            ClearContainer('#handedToSection');

            $("#standardSection").addClass("ignore");
            $("#handedToSection").addClass("ignore");
            $("#pdfSection").removeClass("ignore");
            $("#epdButtonContainer").toggle(false);
        }
        else if (value === "Aan patient") {
            ClearContainer('#pdfSection');

            $("#attachmentName").toggle(false);
            $("#VOFormulierTransferAttachmentID").changeval("");

            $("#standardSection").addClass("ignore");
            $("#pdfSection").addClass("ignore");
            $("#handedToSection").removeClass("ignore");
            $("#epdButtonContainer").toggle(false);
        }

        setVOFormulierTypeValidation(value);
    });

    RadioToggleChildren("#HuidDecubitusAanwezig", "#HuidDecubitusAanwezigChildren", ["Ja"]);
    RadioToggleChildren("#HuidHuidletselAanwezig", "#HuidHuidletselAanwezigChildren", ["Ja"]);
    RadioToggleChildren("#HuidWondAanwezig", "#HuidWondAanwezigChildren", ["Ja"]);
    RadioToggleChildren("#HuidOverigeAanwezig", "#HuidOverigeAanwezigChildren", ["Ja"]);
    RadioToggleChildren("#Zuurstofgebruik", "#ZuurstofgebruikChildren", ["Ja"]);
    RadioToggleChildren("#MedicatieGebruik", "#MedicatieGebruikChildren", ["Ja"]);
    RadioToggleChildren("#MobiliteitValrisicoAanwezig", "#MobiliteitValrisicoAanwezigChildren", ["Ja"]);
    RadioToggleChildren("#InfectierisicoAanwezig", "#InfectierisicoAanwezigChildren", ["Ja"]);
    RadioToggleChildren("#SeksualiteitZwangerschapZwanger", "#SeksualiteitZwangerschapZwangerChildren", ["Ja"]);
    RadioToggleChildren("#VoedingOndervoedingAanwezig", "#VoedingOndervoedingAanwezigChildren", ["Ja"]);
    RadioToggleChildren("#VoedingSprake", "#VoedingSprakeChildren", ["Ja"]);
    RadioToggleChildren("#VoedingDieetAanwezig", "#VoedingDieetAanwezigChildren", ["Ja"]);
    RadioToggleChildren("#ZijnMedeBehandelaars", "#ZijnMedeBehandelaarsChildren", ["Ja"]);
    SelectToggleChildren("#PsychischFunctionerenDwangEnDrangInterventie", "#PsychischFunctionerenDwangEnDrangInterventieChildren", []);
    RadioToggleChildren("#PsychischFunctionerenWilsbekwaamSprakeVan", "#PsychischFunctionerenWilsbekwaamSprakeVanChildren", ["Nee"]);
    RadioToggleChildren("#PsychischFunctionerenVrijheidsbeperkendeInterventiesAanwezig", "#PsychischFunctionerenVrijheidsbeperkendeInterventiesAanwezigChildren", ["Ja"]);
    RadioToggleChildren("#KinderenAanwezig", "#KinderenAanwezigChildren", ["Ja"]);
    RadioToggleChildren("#LevensOvertuigingWilsverklaringAanwezig", "#LevensOvertuigingWilsverklaringAanwezigChildren", ["Ja"]);

    RadioToggleChildren("#SprakeVanPalliatieveZorg", "#divPalliatievezorgJa", ["Ja"]);
    RadioToggleChildren("#LevensVerwachting", "#divKorteLevensverwachting", ["Maanden", "Weken", "Dagen"]);
    RadioToggleChildren("#OpZiekteGerichteBehandelingenMogelijk", "#divOpZiekteGerichteBehandelingenAfspraken", ["Ja"]);
    RadioToggleChildren("#WensenPatientNaastenBehandelingEnZiekte", "#divWensenPatientNaastenBehandelingEnZiekteAfspraken", ["Ja"]);

    HandleJa("BesprokenReanimatiebeleid", "#divBesprokenReanimatiebeleidToelichting", ["Ja"]);
    HandleJa("BesprokenPalliatieveSedatie", "#divBesprokenPalliatieveSedatieToelichting", ["Ja"]);
    HandleJa("BesprokenEuthenasie", "#divBesprokenEuthenasieToelichting", ["Ja"]);
    HandleJa("BesprokenStoppenSondeParenteraleVoeding", "#divBesprokenStoppenSondeParenteraleVoedingToelichting", ["Ja"]);
    HandleJa("BesprokenStoppenParenteraleVochttoediening", "#divBesprokenStoppenParenteraleVochttoedieningToelichting", ["Ja"]);
    HandleJa("BesprokenToedieningBloedtransfusie", "#divBesprokenToedieningBloedtransfusieToelichting", ["Ja"]);
    HandleJa("BesprokenToedieningAntibiotica", "#divBesprokenToedieningAntibioticaToelichting", ["Ja"]);
    HandleJa("BesprokenWensZHopnameVsNietOpnemenZHOfSEH", "#divBesprokenWensZHopnameVsNietOpnemenZHOfSEHToelichting", ["Ja"]);
    HandleJa("BesprokenUitzettenICDOfPacemaker", "#divBesprokenUitzettenICDOfPacemakerToelichting", ["Ja"]);
    HandleJa("BesprokenUitvoerenDiagnostischOnderzoek", "#divBesprokenUitvoerenDiagnostischOnderzoekToelichting", ["Ja"]);

    HandleJa("VerwachtingObstipatie", "#divVerwachtingObstipatieToelichting", ["Ja"]);
    HandleJa("VerwachtingMisselijkheid", "#divVerwachtingMisselijkheidToelichting", ["Ja"]);
    HandleJa("VerwachtingBenauwdheid", "#divVerwachtingBenauwdheidToelichting", ["Ja"]);
    HandleJa("VerwachtingKoorts", "#divVerwachtingKoortsToelichting", ["Ja"]);
    HandleJa("VerwachtingBloeding", "#divVerwachtingBloedingToelichting", ["Ja"]);
    HandleJa("VerwachtingInsulten", "#divVerwachtingInsultenToelichting", ["Ja"]);
    HandleJa("VerwachtingAscites", "#divVerwachtingAscitesToelichting", ["Ja"]);
    HandleJa("VerwachtingPleuravocht", "#divVerwachtingPleuravochtToelichting", ["Ja"]);
    HandleJa("VerwachtingAnders", "#divVerwachtingAndersToelichting", ["Ja"]);

    MakeRadiosDeselectableByID("#MedicatieGebruik");
}

function epd_callback(first, payload, source) {
    // Modify fields not modified by the user.
    // Source.USER = 1
    for (var item in payload) {
        if (payload.hasOwnProperty(item)) {
            if (get_flowfieldvalue_source(item) !== 1) {
                setValue(item, payload[item], source);
            }
        }
    }
}

$(function () {

    if (voformuliertypevalue === "Standaard") {
        showPrefillModal(prefill, hasendpoint);
    }

    //Scrollable auto-resize
    $(window).on("resize", function () {
        var height = GetScrollHeight();
        $("#Scroller").css("height", height);
        $("#Scroller").css("max-height", height);
    });
    $(window).trigger("resize");

    if (showResendQuestion === true) {
        ShowResendQuestion();
    }

    setTimeout(SetupOther, 0);
    setTimeout(SetupContainers, 0);
    setTimeout(SetupRadios, 0);
    setTimeout(SetupAttachment, 0);

    $('[data-toggle="clickover"]:not(.client-header)').clickover({
        html: true,
        placement: "top",
        global_close: true,
        esc_close: true
    });

    setVOFormulierTypeValidation(voformuliertypevalue);
});