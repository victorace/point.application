﻿function SearchOrganization(sender, url, callback) {
    if (!$(formID).valid()) {
        return;
    }

    var senderobj = $(sender);
    if (senderobj) {
        if (senderobj.data("canadd") === false) {
            $("#dialog-cantadd").modal("show");
            return;
        }
    }

    ShowModal(url, callback, inittable);
}

var PutHealthcareProvider = function (department) {
    var ispoint = department.Participation === 2;

    $("#modal").modal('hide');
    
    $.ajax({
        type: "POST",
        url: centraalmeldpunturl,
        data: { OrganizationID: department.OrganizationID },
        async: true,
        success: function (data) {

            if (typeof data.ErrorMessage === "string")
            {
                showErrorMessage(data.ErrorMessage);
            } else {
                $('input[data-linked="DestinationHealthCareProvider"]:not([type=hidden])').val(data.DepartmentName);
                $('input[data-linked="DestinationHealthCareProvider"][type=hidden]').val(data.DepartmentID);

                var extradata = $(this).data();
                extradata.departmentIDs = data.DepartmentID.toString().split(';');
                extradata.onlyplan = addQued;

                AjaxSave(formID, pointZANextPhaseUrl, true, null, false, extradata);
            }
        }
    });
};

function AskCancelVVT()
{
    $('#dialog-confirm').modal('show').one('click', '#delete', function () {
        CancelVVT();
    });
}

function CancelVVT()
{
    $.ajax({
        type: "POST",
        url: resetVVTUrl,
        async: true,
        success: function (data) {
            FormIsNotDirty();
            window.location.reload();
        }
    });

    $("#btncancelvvt").hide();
}

function DeleteInvite(sender)
{
    $.ajax({
        type: "POST",
        url: deleteinviteurl,
        data: $(sender).data(),
        success: function (data) {
            if (data.Success === true)
            {
                $(sender).closest("tr").remove();
            } else {
                showErrorMessage('Deze kon niet worden verwijderd. Hij was al reeds verstuurd.')
            }
        }
    });
}

$(document).ready(function () {

    $("#cancelvvt").on("click", CancelVVT);

    $("#btnPlanZa").on("click", function () {       
        if ($(formID).valid()) {
            var destinationID = $('input[data-linked="QueuedHealthCareProvider"][type=hidden]').val();
            var destinationName = $('input[data-linked="QueuedHealthCareProvider"]:not([type=hidden])').val();

            
        }
    });

    function getLinkedValue(linked) {
        if (linked === undefined) return "";
        if ($(linked).is("select")) {
            return $(linked).find("option:selected").val();
        } else {
            return $(linked).val();
        }
    };

    function getLinkedDigest(linked) {
        if (linked === undefined) return "";
        if ($(linked).is("select")) {
            return $(linked).find("option:selected").data("digest");
        } else {
            return $(linked).data("digest");
        }
    }

    function getLinkedText(linked) {
        if (linked === undefined) return "";
        if ($(linked).is("select")) {
            var selected = $(linked).find("option:selected");
            if (selected && selected.val()) {
                return selected.text();
            }
        } else {
            return $(linked).text();
        }
        return "";
    }

    function getClickoverTitle() {
        var linked = $(this).data("linked");
        if (linked !== undefined) {
            return getLinkedText($("#" + linked));
        } else {
            return $(this).data("title");
        }
    }

    function getClickoverContent() {
        var noContent = "Nog geen gegevens beschikbaar";

        var requestUrl = $(this).data("url");
        if (requestUrl !== undefined) {

            var linked = $(this).data("linked");
            if ($.getUrlVar(requestUrl, "id") === undefined && linked !== undefined) {
                var value = getLinkedValue($("#" + linked));
                var id = parseInt(value);
                if (isNaN(id)) {
                    return noContent;
                }
                var digest = getLinkedDigest($("#" + linked));
                requestUrl = update_query_string(requestUrl, "id", id);
                requestUrl = update_query_string(requestUrl, "digest", digest);
            };

            var content = $.ajax({
                url: requestUrl,
                type: "GET",
                dataType: "html",
                async: false,
                success: function () { /*just get the response*/ },
                error: function () { content = noContent }
            }).responseText;

            return content;
        }
        return $(this).attr("data-content");
    }

    $('[data-toggle="clickover"]:not(.client-header)').clickover({
        html: true,
        placement: "top",
        global_close: true,
        esc_close: true,
        title: getClickoverTitle,
        content: getClickoverContent
    });

    if (showtussenorgwarning === true)
    {
        $("#tussenorgwarning").modal("show");
    }
});