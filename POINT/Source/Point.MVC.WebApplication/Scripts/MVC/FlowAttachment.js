﻿function getFlowAttachmentContainer(button) {
    return $(button).closest(".flowAttachmentContainer");
}

function getFlowAttachmentModal(button) {
    return getFlowAttachmentContainer(button).find(".flowattachmentmodal");
}

function showFlowAttachmentModal(requestUrl, modal) {
    $.ajax({
        type: "GET",
        url: requestUrl,
        data: $(this).data(),
        async: false,
        beforeSend: function () {
            $(modal).modal({ backdrop: "static" });
        },
        success: function (data) {
            $(modal).find(".modal-body").html(data);
        },
        error: function (data) {
            $(modal).find(".modal-body").html(data.responseText);
        }
    });
}

function AddFlowAttachment(sender) {
    showFlowAttachmentModal($(sender).data("addurl"), getFlowAttachmentModal(sender));
}

function EditFlowAttachment(sender) {
    showFlowAttachmentModal($(sender).data("editurl"), getFlowAttachmentModal(sender));
}

function DeleteFlowAttachment(sender) {
    var url = $(sender).data("deleteurl");
    $(".flowattachmentdeletemodal").modal("show")
        .one("click", "#delete", function () {
            $.ajax({
                type: "GET",
                url: url,
                success: function (data, textStatus, jqxhr) {
                    reloadFlowAttachments(sender);
                },
                error: function (xhr, status, error) {
                    var msg;
                    try {
                        var response = JSON.parse(xhr.responseText);
                        msg = response.ErrorMessage;
                    }
                    catch (ex) {
                        msg = xhr.statusText;
                    }
                    showErrorMessage(msg);
                    form.valid();
                }
            });
        });
}

function SaveFlowAttachment(sender) {
    var form = $(sender).closest("form");
    var options = {
        url: $(sender).data("saveurl"),
        type: "POST",
        dataType: "json",
        clearForm: true,
        success: function (data, textStatus, jqxhr, form) {
            reloadFlowAttachments(sender);
        },
        error: function (xhr, status, error) {
            var msg;
            try {
                var response = JSON.parse(xhr.responseText);
                msg = response.ErrorMessage;
            }
            catch (ex) {
                msg = xhr.statusText;
            }
            showErrorMessage(msg);
            form.valid();
        }
    };
    if (form.length) {
        var cancelSubmit = false;
        if (form.data("validator") != undefined) {
            cancelSubmit = form.data("validator").cancelSubmit;
        }
        if (cancelSubmit || form.valid()) {
            form.ajaxSubmit(options);
        }
    }
}

function reloadFlowAttachments(sender) {

    var form = $(sender).closest("form");
    if (form.length) {
        form.data("validator", null);
        form.unbind("validate");
    }

    var container = getFlowAttachmentContainer(sender);
    var listurl = container.data("listurl");
    getFlowAttachmentModal(sender).modal("hide");
    AjaxLoad("", listurl, container.find(".flowAttachmentEntries"));
}

function ShowRelatedTransferWarning()
{
    var showmessage = $('#ShowTakeOverMessage').val();
    if (showmessage === 'true' || showmessage === 'True')
    {
        $(".prefilledRelatedMemo").modal("show");
    }
}

$(function () {
    $('[data-toggle="clickover"]:not(.client-header)').clickover({
        html: true,
        placement: "top",
        global_close: true,
        esc_close: true,
        title: $(this).data("title"),
        content: $(this).data("content")
    });

    ShowRelatedTransferWarning();
});