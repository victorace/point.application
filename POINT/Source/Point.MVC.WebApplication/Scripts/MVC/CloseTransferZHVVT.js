﻿function SearchOrganization(sender, url, callback) {
    ShowModal(url, callback, inittable)
}

var PutDestinationHealthCareProvider = function (department) {
    $('input[data-linked="SelectedHealthCareProvider"]:not([type=hidden])').val(department.DepartmentName);
    $('input[data-linked="SelectedHealthCareProvider"][type=hidden]').val(department.DepartmentID);
    $("#modal").modal('hide');
    // because validator.settings.unhighlight doesn't appear to get called automatically on a readonly element
    $("#SelectedHealthCareProvider").trigger("blur");
};

var attachButtenHandlers = function () {
    $("#closeVO").on("click", function () {
        location.replace($(this).data('url'));
    });

    $(".btnTakeOver").click(function () {
        var source = $(this).data("source");
        var destination = $(this).data("destination");
        $('input[data-linked="' + destination + '"]:not([type=hidden])').val($('input[data-linked="' + source + '"]:not([type=hidden])').val());
        $('input[data-linked="' + destination + '"][type=hidden]').val($('input[data-linked="' + source + '"][type=hidden]').val());
    });

    $('.btnCloseTransfer').on('click', function (e) {
        validateDateWarningRules();
        removeDateWarningRules();
        var children = fixDataLinked();
        var validform = $(formID).valid();
        if (validform) {
            $(this).attr("disabled", true);
            AjaxSave(formID, formRequestUrl, true);
            return;
        }
        fixDataLinked(children);
        attachDateWarningRules();
    });

    $('.btnCancel').one('click', function (e) {
        FormIsNotDirty();
        location.reload();
    });

    $('#btn-close-ind').one('click', function (e) {
        var url = $(this).data("url");
        if (url)
            window.location.replace(url);
    });

    $('#btnTakeDischargeProposedStartDate').on('click', function (e) {
        $("#RealizedDischargeDate").val($("#DischargeProposedStartDate").val());
        $("#RealizedDischargeDate").trigger("blur");
    });
}

$(function () {
    $(window).on("resize", function () {
        var height = GetScrollHeight();
        $("#Scroller").css("max-height", height);
        $("#Scroller").css("height", height);
    });
    $(window).trigger("resize");

    attachButtenHandlers();

    $("#DischargeProposedStartDate").on("blur", function () {
        $("#btnTakeDischargeProposedStartDate").toggle(($(this).val().length !== 0))
    });
    $("#DischargeProposedStartDate").trigger("blur");
});
