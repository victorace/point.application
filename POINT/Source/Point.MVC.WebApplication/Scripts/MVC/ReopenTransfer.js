﻿// Page Setup
$(function () {
    var thisForm = $("#reopenTransferForm");

    thisForm.submit(function (e) {
        e.preventDefault();
        AjaxSave(formID, thisForm.action, false, function (data) {
            location.replace(autoRedirectUrl);
        });
    });
});