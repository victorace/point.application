﻿$(document).ready(function () {
    $("#failed").hide();

    $("#getQuestionBtn").on("click", function (event) {
        var username = $("#Username").val();
        var email = $("#EmailAddress").val();
        $.ajax({
            type: "POST",
            url: getSecretQuestionUrl,
            data: { username: username, emailaddress: email },
            success: function (data) {
                if (data.ValidUserName) {
                    $("#failed").hide();
                    $("#findQuestionContainer").hide();
                    if (data.IsHospital) {
                        $("#SendPassword").click();
                    } else {
                        $("#SecretQuestionContainer").html(data.Question);
                        $("#secretQuestionAndAnswerContainer").show();
                        $("#secretAnswerTextBox").focus();
                    }
                } else {
                    $("#failed").show();
                    $("#FailedResult").html("Wachtwoord kon niet hersteld worden. Neem s.v.p contact op met een beheerder.");
                }
            }
        });
    });

    $("#SendPassword").on("click", function (event) {
        var formData = $("#resetpasswordform").serialize();
        $.ajax({
            type: "POST",
            url: resetPasswordUrl,
            data: formData,
            success: function (data) {
                $("#failed").hide();
                if (data) {
                    if (data.error) {
                        $("#FailedResult").html(data.error);
                        $("#failed").show();
                    }
                    else {
                        $("#ResetPasswordResultContainer").show();
                        $("#secretQuestionAndAnswerContainer").hide();
                        $("#ResetPasswordResult").html("U kunt binnen enkele ogenblikken een e-mail ontvangen met uw nieuwe wachtwoord.<br />Na inloggen dient u direct een ander wachtwoord op te geven.<br /><br /><i>Als u geen e-mail ontvangt, neemt u dan contact op met uw organisatie- of regiobeheerder.</i><br /><br />");
                    }
                } else {
                    $("#FailedResult").html("Wachtwoord kon niet verstuurd worden.");
                    $("#failed").show();
                }
            }
        })
    });
});