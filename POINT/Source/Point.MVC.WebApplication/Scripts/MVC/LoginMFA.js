﻿$(function () {

    $("#CheckMFACode").click(function (e) {
        $(this).prop('disabled', true);
        $(formID).submit();
        $(this).prop('disabled', false);
    });

    $(formID).submit(function (e) {
        e.preventDefault();
        var data = $(formID).serializeArray();
        $.ajax({
            type: 'POST',
            cache: false,
            url: checkmfacodeurl,
            data: $.param(data),
            success: function (data) {
                var result = (data.result == true);
                $("#ResultSuccess").toggle(result);
                $("#ResultWarning").toggle(!result);
                if (result) {
                    location.replace(dispatchurl);
                }
            }
        });
    });

});