﻿var hasprojectselect = false;

var BLANKS_text = "[Lege waarden]";
var BLANK_val = "BLANKS";
var BLANK_number = "-999";

//var source_region = 0;
//var source_organization = 1;
//var source_location = 2;
//var source_department = 3;

function getorganizationprojects(element) {
    if (hasprojectselect && typeof urlgetprojectsbyorganizations !== "undefined" && element !== undefined) {
        var id = $(element).attr("id");
        if (id.toLowerCase() === "organizationid") {
            var values = getselectvalues(element, true);
            $.ajax({
                type: "POST",
                url: urlgetprojectsbyorganizations,
                data: { organizationids: values }
            }).done(function (data) {
                fillprojects(data);
            });
        }
    }
}

function fillprojects(data) {
    var selectprojectid = $("#ProjectID");
    if (selectprojectid.length) {
        fillMultiSelectWithOptions(selectprojectid, data, true);
    }
}

// not yet used
//function filltrendfilter(data) {
//    if (typeof data !== "undefined" && data.success && typeof data.destinationfilter !== "undefined") {
//        $.each(data.destinationfilter, function (key, value) {
//            var element = $("#" + key);
//            if (element.length) {
//                fillMultiSelectWithOptions(element, value, true);
//            }
//        });
//    }
//}

//function gettrendfilter(object, source) {
//    if (typeof urlgettrendreportfilter !== "undefined") {
//        var values = getselectvalues(object, $(object).val() !== "-1");
//        var reportid = parseInt($("#ReportID").val(), 10);
//        $.ajax({
//            type: "POST",
//            url: urlgettrendreportfilter,
//            data: {
//                reportid: reportid,
//                trendreportfiltersource: source,
//                ids: values
//            },
//            success: function (data) {
//                filltrendfilter(data);
//            }
//        });
//    }
//}

function hideFilterBlock() {
    $("#reportfilterdiv").toggle();
    $.each(["#showFilterCriteriaTop", "#hideFilterCriteriaTop"], function (item, value) {
        $(value).toggle();
    });
}

function handleHideFilter() {
    $.each(["#showFilterCriteriaTop", "#hideFilterCriteriaTop"], function (item, value) {
        $(value).click(function () {
            hideFilterBlock();
        }).css("cursor", "pointer");
    });
}

function multiselect_blanks(element) {
    var blanks = $(element)
        .find("option")
        .filter(function () {
            return (!this.value || $.trim(this.value).length === 0) && (!this.Text || $.trim(this.Text).length === 0);
        });

    if ($(element).data("blanks") === true) { //09-08-17: needs to be fixed, since it's sent as a string
        if (blanks.length === 0) {
            $(element).prepend($("<option />").text(BLANKS_text).val(BLANK_val));
        }
        else {
            $(blanks).not(":first").remove();
            $(blanks).first().text(BLANKS_text).val(BLANK_val);
        }
    }
    else if ($(element).data("blanksnumber") === true || $(element).data("blanksnumber") === "True") {
        if (blanks.length === 0) {
            $(element).prepend($("<option />").text(BLANKS_text).val(BLANK_number));
        }
        else {
            $(blanks).not(":first").remove();
            $(blanks).first().text(BLANKS_text).val(BLANK_number);
        }
    }
    else {
        $(blanks).remove();
    }
}

function multiselect_count(element) {
    var hascount = $(element).data("count");
    if (hascount === undefined || hascount === true) {
        var combinedname = $(element).attr("id") + "Count";
        var combinedelement = $("#" + combinedname);
        var count = $(element).find("option").length;
        if (combinedelement.length === 0) {
            $("<input/>").attr("type", "hidden").attr("name", combinedname).attr("id", combinedname)
                .val(count)
                .insertAfter($(element));
        } else {
            combinedelement.val(count);
        }
    }
}

function multiselect_disable_link(element) {
    $(element).multiselect("selectAll", false);
    $(element).multiselect("updateButtonText");
    $(element).multiselect("disable");

    var linkelment = $("#" + element.data("linkid"));
    if (linkelment.length === 1) {
        multiselect_disable_link(linkelment);
    }
}

function multiselect_onchange(element) {
    if (element !== undefined) {
        var linkurl = element.data("linkurl");
        if (linkurl !== undefined) {
            var linkelement = $("#" + element.data("linkid"));
            if (linkelement.length === 1) {
                var selectedoptions = element.find("option:selected");
                var blanks = element.find("option:selected[value='" + BLANK_val + "']");
                if (selectedoptions.length === 1 && blanks.length === 0) {
                    var postdata = { ids: [selectedoptions[0].value] };
                    var organizationtypeid = element.data("organizationtype");
                    if (organizationtypeid !== undefined) {
                        postdata.organizationtypeid = organizationtypeid;
                    }
                    $.ajax({
                        type: "POST",
                        url: linkurl,
                        async: true,
                        data: postdata
                    }).done(function (data) {
                        if (data.options !== undefined) {
                            var hasblanks = $(element).data("blanks");
                            fillMultiSelectWithOptions(linkelement, data.options, true, hasblanks ? { Text: BLANKS_text, Value: BLANK_val } : undefined);
                            multiselect_count(linkelement);
                            getorganizationprojects(element);
                            if (data.options.length === 1) {
                                multiselect_onchange(linkelement);
                            }
                        }
                    });
                } else {
                    multiselect_disable_link(linkelement);
                }
            }
        }
    }
}

function multiselect_initialise() {
    $.each($(".linked"), function () {
        $.each($(this).find("select").splice(1), function () {
            $(this).prop("disabled", "disabled");
        });
    });

    // Hack #14615
    var pagename = document.location.pathname.split("/").slice(-1)[0];
    if (pagename === "ZHTrendGrafieken") {
        $.each(["#GewensteRegionID", "#GerealiseerdeRegionID"], function (key, value) {
            $(value).prop("disabled", "disabled");
        });

        $.each(["#GewensteOrganizationID", "#GerealiseerdeOrganizationID"], function (key, value) {
            $(value).removeAttr("disabled");
            $(value).prop("disabled", false);
        });
    }

    if (typeof $.fn.multiselect !== "undefined") {
        hasprojectselect = $("#ProjectID").length === 1;

        $(formID + " [multiple]").each(function () {
            var multiple = $(this);

            multiselect_blanks(multiple);
            multiselect_count(multiple);

            $(multiple).multiselect({
                includeSelectAllOption: $(multiple).attr('data-selectall') !== "false",
                selectAllText: "Alles/Niets Selecteren",
                allSelectedText: "Alles geselecteerd",
                nonSelectedText: "Selecteer een waarde",
                disableIfEmpty: true,
                maxHeight: 200,
                onChange: function (option, checked, select) {
                    multiselect_onchange(this.$select);
                },
                numberDisplayed: 1,
                nSelectedText: 'geselecteerd'
            });

            $(multiple).multiselect("selectAll", false);
            $(multiple).multiselect("updateButtonText");
        });

        $.each($(".linked"), function () {
            $.each($(this).find("select"), function () {
                var id = $(this).data("id");
                if (id !== undefined) {
                    $(this).multiselect("select", id, true)
                }
            });
        });
    }
}

$(function () {
    $(window).on("resize", function () {
        var height = GetScrollHeight();
        $("#Scroller").css("max-height", height);
        $("#Scroller").css("height", height);
    });
    $(window).trigger("resize");

    $('[data-toggle="clickover"]:not(.client-header)').clickover({
        html: true,
        placement: "top",
        global_close: true,
        esc_close: true,
        title: $(this).data("title"),
        content: $(this).data("content")
    });

    multiselect_initialise();

    $("#btngeneratereport").on("click", function () {
        if (typeof urlgeneratereport !== "undefined") {
            $("#loader").show();
            var form = $(formID);
            if (form.valid()) {
                form.attr("action", urlgeneratereport);
                form.attr("target", "PointReport");
                window.open("", "PointReport");
                form.submit();
                window.focus();
            }
            $("#loader").hide();
        }
    });

    $("#btncancel").one("click", function () {
        location.replace(reportlisturl);
    });

    $("#btnlegend").on("click", function () {
        $("#legend").toggle();
    });

    $.each(["#showFilterCriteriaTop", "#hideFilterCriteriaTop"], function (index, element) {
        $(element).click(function () { hideFilterBlock(); }).css("cursor", "pointer");
    });

    if (typeof formID !== "undefined") {
        var validator = $.data($(formID)[0], "validator");
        if (validator) {
            validator.settings.ignore = ':hidden:not(".multiselect")';
        }
    }
});