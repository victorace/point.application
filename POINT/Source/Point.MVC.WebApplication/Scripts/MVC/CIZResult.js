﻿function checkedDisableContainer(control, container) {
    function updateContainer() {
        if ($(this).is(":checked") == true) {
            EnableContainer(container);
        } else {
            DisableContainer(container);
            ClearContainer(container);
        }
    }
    $(control).on("change", updateContainer);
    $(control).each(updateContainer);
}

$(function () {
    checkedDisableContainer("#WlzGrondslagSom", "#GrondslagSom");
    checkedDisableContainer("#WlzGrondslagPG", "#GrondslagPG");
    checkedDisableContainer("#WlzProfiel", "#Profiel");
    checkedDisableContainer("#WlzAnders", "#Anders");
    checkedDisableContainer("#WMOBegeleiding", "#Begeleiding");
    checkedDisableContainer("#WMOHuishoudelijkeondersteuning", "#Huishoudelijkeondersteuning");
    checkedDisableContainer("#WMOJeugdzorg", "#Jeugdzorg");
});