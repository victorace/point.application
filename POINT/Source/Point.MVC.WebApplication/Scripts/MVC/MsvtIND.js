﻿$.validator.setDefaults({ ignore: '.ignore *' });

function CheckFormSetName(sender) {
    var formsetname = $(sender).val();
    var dropdownnames = $("#IndicatieVersion option").map(function () { return this.innerText; }).get();

    var hasformsetversion = formSetVersionID > 0;
    var valid = $.inArray(formsetname, dropdownnames) === -1 || hasformsetversion;

    if (valid) {
        $(sender).css("background-color", "white");
        $("#ValidFormSetName").val("true");
    } else {
        $(sender).css("background-color", "#ffd0d0");
        $("#ValidFormSetName").removeAttr("value");
    }
}

function ClearMsvtItemRow(sender)
{
    ClearContainerByObject($(sender).closest(".msvt-item-row"));
    UnSetRequired($(sender).closest(".msvt-item-row"));
}

function SearchOrganization(sender, url, callback) {
    ShowModal(url, callback, inittable)
}

var PutOrganisation = function (organization) {
    $('input[data-linked="MSVTLeverendeOrganisatie"]:not([type=hidden])').val(organization.DepartmentName);
    $('input[data-linked="MSVTLeverendeOrganisatie"][type=hidden]').val(organization.DepartmentID);
    $("#modal").modal('hide');
    
    $("#leverendemedewerkerdetails").toggle(vvtDepartmentID == organization.DepartmentID);
};

function Round2Decimal(number) {
    return Math.round(number);
}

function SetTotalHours() {
    var totaltimeperday = 0;
    $("#indicatieactions .msvt-item-row").each(function () {
        var frequenty_input = $(this).find(".frequency");
        var frequencyunit_input = $(this).find(".freqencyunit:checked");
        var expectedtime_input = $(this).find(".expectedtime");

        var frequency = parseInt(frequenty_input.val(), 10);
        var frequencyunit = parseInt(frequencyunit_input.val(), 10);
        var expectedtime = parseInt(expectedtime_input.val(), 10);

        if (isNaN(expectedtime))
            expectedtime = parseInt(expectedtime_input.attr("data-averagenormtime"), 10);

        if (frequency > 0 && frequencyunit > 0 && expectedtime > 0) {
            var totalitem = frequency * (1 / frequencyunit) * expectedtime;
            totaltimeperday += totalitem;
        }
    });

    var valstartdate = $("#GeldigheidsDuurStartdatum").val();
    var valenddate = $("#GeldigheidsDuurEinddatum").val();

    if (valstartdate != '' && valenddate != '') {

        var startdate = moment(valstartdate, "DD-MM-YYYY");
        var enddate = moment(valenddate, "DD-MM-YYYY");

        var days = moment.duration(enddate.diff(startdate)).asDays();
        var hours = totaltimeperday * days / 60;
        $("#IndicatieBenodigdeTijd").val(Round2Decimal(hours) + " uur");
    }
}

function SetRequired(row)
{
    row.find(".partly-required input").each(
        function () {
            $(this).rules(
                "add",
                {
                    required: true,
                    messages: { required: $(this).attr("data-label") + " is verplicht" }
                }
            )
        }
    );
}

function UnSetRequired(row)
{
    row.removeClass("has-error");
    row.find(".partly-required input").each(
        function () {
            
            $(this).rules("remove");
        }
    );
}

function ResizeIndicaties()
{
    var scrollerwidth = $("#Scroller").width();
    $("#indicatieactions .resizetomax").each(
        function () {
            $(this).css("max-width", scrollerwidth - 520 + "px");
        }
    );
}

function AddMSVTActionToSignalering() {
    var form = $(formID);
    var itemmsg = "Indicatie handelingen zijn aangepast";

    var isinarray = $.inArray(itemmsg, GetSignaleringItems()["SignaleringItems"]) >= 0;
    if (!isinarray) {
        GetSignaleringItems()["SignaleringItems"].push(itemmsg);
    }

    if (ShowSignaleringPrompt() === false) {
        form.data("SendSignal", true);
    } else {
        form.data("SendSignalQuestion", true);
    }
}

$(function () {
    //Scrollable auto-resize
    $(window).on("resize", function () {
        var height = GetScrollHeight();
        $("#Scroller").css("max-height", height);
        ResizeIndicaties();
    });
    $(window).trigger("resize");

    $("#btnRenew").on("click", function () {
        var data = $(this).data();
        var url = formRequestUrl;
        AjaxSave(formID, url, true, null, false, data);
    });

    $("#BehandelendArtsID").on("change", function () {
        var artsID = $(this).val();
        var digest = $(this).find(":selected").data("digest");
        $.ajax({
            type: "POST",
            url: getDoctorByID,
            data: { doctorID: artsID, digest: digest },
            success: function (data) {
                setDoctor(data);
            },
            error: function (data) {
                showErrorMessage(data);
            }
        });
    });

    $("#nieuwexemplaar").on("click", function () {
        var url = $(this).attr("data-url");
        location.replace(url);
    });

    $("#btnFrequency").on("click", function () {
        var url = $(this).attr("data-url");
        location.replace(url);
    });

    $("#IndicatieVersion").on("change", "", "", function (item) {
        var url = $(this).find("option:selected").attr("data-url");
        FormIsNotDirty();
        location.replace(url);
    });

    $("#indicatieactions .msvt-item-row").each(function ()
    {
        var firstradio = $(this).find("input[name$='freqencyunit']");
        var name = firstradio.first().attr("name");
        MakeRadiosDeselectableByName(name);

        if($(this).find("input[type=radio]:checked, input[type=text]:filled").length > 0)
            SetRequired($(this));
    });

    $("#indicatieactions .msvt-item-row[data-hashistory='true']").addClass("bg-orange color-pt-white").on("contextmenu", function (e) {
        ShowModal(appPath + "/History/FlowFieldValues?CustomFields=MSVTActions&FormSetVersionID=" + $(this).data("formsetversionid"));
        return false;
    });

    $("#indicatieactions .collapse input, #Scroller .collapse span").on("change keyup click", "", function ()
    {
        var isfilled = this.checked || ($(this).is("[type=text]:filled"));
        var item = $(this);

        AddMSVTActionToSignalering();

        if (isfilled) {
            $(this).closest("li").addClass("active");
            SetRequired($(this).closest(".msvt-item-row"));
        }
        else {
            if ($(this).closest(".uitklapcontainer").find("input[type=radio]:checked, input[type=text]:filled").length == 0)
                $(this).closest("li").removeClass("active");

            if($(this).closest(".msvt-item-row").find("input[type=radio]:checked, input[type=text]:filled").length == 0)
                UnSetRequired($(this).closest(".msvt-item-row"));
        }

        SetTotalHours();
    });

    $(".date").on('dp.change', '', function () {
        SetTotalHours();
    });

    $("#indicatieactions .collapse input[type=radio]:checked, #indicatieactions .collapse input[type=text]:filled").closest(".collapse").collapse("show").parents("li").addClass("active");

    if (formSetVersionID === -1) {
        $("#IndicatieVersion option:selected").removeAttr("selected");
        $('#IndicatieVersion')
            .append($("<option></option>")
                .attr("value", "-1")
                .attr("selected", "")
                .attr("data-url", newrequesturl)
                .text(" -- Specificeer hieronder -- "));
    }
});