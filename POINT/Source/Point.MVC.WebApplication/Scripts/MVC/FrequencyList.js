﻿

// Page Setup
$(function () {
    var saveFormID = formID;

    $(".phaseButtonCancel").unbind("click");
    $(".phaseButtonCancel").on("click", function () {

        var formSetVersionID = $("#selectedFormSetVersionID").val();

        var url = msvtFormURL + "&formSetVersionID=" + formSetVersionID;

        location.replace(url);
    });

    $(".phaseButtonSave").unbind("click");
    $(".phaseButtonSave").on("click", function () {

        var thisForm = $("#frequencyForm")[0];

        if ($.isArray(thisForm)) {
            thisForm = thisForm[0]; // NOTE pick the native object i.e. the form
        }

        AjaxSave(formID, thisForm.action, false, function (data) {

            if (data.IsNew) {

                location.replace(data.Url);
            } else {

                updateView();
            }
        });

    });

    var closeOrReopenUrl;
    $("#closeOrReopen").click(function () {

        AjaxSave(formID, closeOrReopenUrl, false, function (data) {

            frequencyShower(data.Item);
        });
    });

    $("#print").click(function (e) {

        e.preventDefault();

        var freqID = currentFrequencyId();

        if (freqID !== undefined && freqID > 0) {

            $('#printCurrent').click(function () {

                var url = printUrl + "&frequencyID=" + freqID;

                historyUpdater(true);

                location.replace(url);
            });

            $('#printAll').click(function () {

                historyUpdater(true);

                location.replace(printUrl);
            });

            $('#dialog-print').modal('show');
        }
    });

    var memoShower = function () {

        $("#transferMemosSection").hide();

        var freqID = currentFrequencyId();

        if (freqID !== undefined && freqID > 0) {

            var formSetVersionID = $("#selectedFormSetVersionID").val();

            var url = listFromFrequencyURL + "&frequencyID=" + freqID + "&formSetVersionID=" + formSetVersionID;

            $("#transferMemos").load(url, function () {
                formID = saveFormID;
            });
            $("#transferMemosSection").show();
        }
    };

    var historyUpdater = function (push) {

        var formSetVersionID = $("#selectedFormSetVersionID").val();

        var url = selfUrl + "&formSetVersionID=" + formSetVersionID;

        if (push) {

            window.history.pushState(null, null, url);
        } else {

            window.history.replaceState(null, null, url);
        }
    }

    $("#selectedFormSetVersionID").change(function () {

        historyUpdater();

        updateView();
    });

    var setButtons = function () {

        var status = $("#status").val();

        if (status == Enum.FrequencyStatus.Active) {

            closeOrReopenUrl = closeUrl;

            $("#closeOrReopenText").text("Afsluiten");

            $("#closeOrReopen").show();

            $("#isClosed").hide();

        } else if (status == Enum.FrequencyStatus.Done) {

            closeOrReopenUrl = reopenUrl;

            $("#closeOrReopenText").text("Heropenen");

            $("#closeOrReopen").show();

            $("#isClosed").show();
        } else {

            $("#closeOrReopen").hide();

            $("#isClosed").hide();
        }
    };

    var frequencyShower = function (data) {

        $("#startDate").val(data.StartDateText);
        $("#endDate").val(data.EndDateText);

        if (data.Quantity !== undefined && data.Quantity > 0) {
            $("#quantity").val(data.Quantity);
        } else {
            $("#quantity").val(undefined);
        }

        $("#status").val(data.Status);

        setButtons();
    }

    var currentFrequencyId = function () {

        var selectedOption = $("#selectedFormSetVersionID").find(':selected');
        if ($.isArray(selectedOption)) {
            selectedOption = selectedOption[0];
        }

        var freqID = parseInt(selectedOption.data('frequencyid'), 10);

        if (freqID !== undefined && freqID > 0) {

            return freqID;
        }

        return 0;
    }

    var frequencyFetcher = function (freqID) {

        $("#frequencyID").val(freqID);

        $.ajax({
            type: "GET",
            url: getUrl + "&frequencyID=" + freqID,
            async: true,
            success: function (data) {

                frequencyShower(data);
            }
        });
    }

    var newFetcher = function (formSetVersionID) {

        $.ajax({
            type: "GET",
            url: newUrl + "&formSetVersionID=" + formSetVersionID,
            async: true,
            success: function (data) {

                frequencyShower(data);
            }
        });
    }

    var updateView = function () {

        var freqID = currentFrequencyId();

        $('#frequencyID').val(freqID);

        if (freqID > 0) {

            $("#print").show();

            frequencyFetcher(freqID);
        } else {

            $("#print").hide();

            var formSetVersionID = $("#selectedFormSetVersionID").val();

            newFetcher(formSetVersionID);
        }

        memoShower();

        attachmentShower();
    }

    var attachmentShower = function () {

        var formSetVersionID = $("#selectedFormSetVersionID").val();

        var returnUrl = encodeURIComponent(selfUrl + "&formSetVersionID=" + formSetVersionID);

        var transferAttachmentUrl = transferAttachmentListURL + "&formSetVersionID=" + formSetVersionID + "&returnUrl=" + returnUrl;

        $('#transferAttachments').load(transferAttachmentUrl, function () {

            formID = saveFormID;

            $('#transferAttachmentsSection').show();
        });
    }

    updateView();
});