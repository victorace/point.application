﻿function bindFormDialog(dialog) {
    $("form", dialog).submit(function () {
        var digest = $(this).find("#DigestContactPersonID").val();

        $.ajax({
            url: this.action,
            type: this.method,
            data: $(this).serialize() + "&Digest=" + digest,
            success: function (data) {
                $("#contactPersonModal").removeClass("fade").modal("hide");
                LoadPersonContainer(data.Url);
            },
            error: function (jqxhr, textStatus, errorThrown) {
                showErrorMessage($.parseJSON(jqxhr.responseText).ErrorMessage, true);
            }
        });
        return false;
    });
}

function LoadPersonContainer(url) {
    $.ajax({
        url: url,
        type: "POST"
    }).done(function (data) {
        $("#contactPersonContainer").html(data);
    });
}

function DeleteContactPerson(sender, e) {
    e.preventDefault();
    var url = $(sender).data("url");

    $("#deleteContactPersonModal").modal("show")
        .one("click", "#delete", function () {
            $.ajax({
                type: "GET",
                url: url,
                success: function (data) {
                    $("#deleteContactPersonModal").removeClass("fade").modal("hide");
                    LoadPersonContainer(data.Url);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $("#deleteContactPersonModal").modal("hide");
                    showErrorMessage($.parseJSON(jqXHR.responseText).ErrorMessage, true);
                }
            });
        });
}

$(function () {
    $("#btnInsertContactPerson, .editContactPerson").on("click", function (e) {
        var url = $(this).data("url");
        $("#contactPersonModal .modal-content").load(url, function () {
            $("#contactPersonModal").modal("show");
            bindFormDialog(this);
        });
        return false;
    });
});
