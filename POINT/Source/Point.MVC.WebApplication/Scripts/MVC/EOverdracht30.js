﻿var formID = "#EOverdracht30";

function ClickBox(sender) {
    $(sender).find("input").trigger("click");
}

function OpenGroup(sender, groupid, subgroupid) {
    var box = $(sender).closest(".group-box");
    box.removeClass("filled");

    var enabled = $(sender).is(":checked");
    if (enabled) {
        box.addClass("filled");
    }

    if (enabled) {
        $("#subsectioncontainer_" + groupid + "_" + subgroupid).removeClass("invisible").addClass("visible");
    } else {
        $("#subsectioncontainer_" + groupid + "_" + subgroupid).addClass("invisible").removeClass("visible");
    }

    var groupenabled = $("#mainsectioncontainer_" + groupid).find(".subsection.visible").length > 0;
    if (groupenabled) {
        $("#mainsectioncontainer_" + groupid).removeClass("invisible").addClass("visible");
    } else {
        $("#mainsectioncontainer_" + groupid).addClass("invisible").removeClass("visible");
    }
}