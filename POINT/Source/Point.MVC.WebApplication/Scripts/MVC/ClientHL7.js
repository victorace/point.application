﻿function createPatientRequest(patientNumber) {
    if (hasHL7 === true && patientrequesturl !== undefined && patientrequesturl.length > 0) {

        $("#loaderImportHL7").show();

        $.ajax({
            url: patientrequesturl,
            type: "POST",
            data: { patientNumber: patientNumber },
            success: function (data) {
                if (data.MessageID != null) {
                    pollPatientReceived(data.MessageID, patientNumber);
                }
            },
            error: function (xhr, status, error) {
                showErrorMessage(error);
            },
            global: false,
            timeout: 10000
        });
    }
}

function createPatientFromHL7Service(patientNumber) {

    if (hasHL7 === true && patientreceivedurl !== undefined && patientreceivedurl.length > 0) {

        $("#loaderImportHL7").show();

        $.ajax({
            url: patientreceivedurl,
            type: "POST",
            data: { patientNumber: patientNumber },
            success: function (data) {
                if (data.ExternalClient !== null && data.ExternalClient.PatientNumber !== null && data.ExternalClient.PatientNumber === patientNumber) {
                    fillFieldsFromData(data.ExternalClient);
                    fillFieldsFromData(data.ExternalContactPerson, "ContactPerson");
                    $("#loaderImportHL7").hide();
                }
                else {
                    showErrorMessage("Het is niet gelukt om de gegevens op te halen. Probeer het opnieuw.");
                    $("#loaderImportHL7").hide();
                }
            },
            error: function (xhr, status, error) {
                showErrorMessage("Het is niet gelukt om de gegevens op te halen: " + error + ". Probeer het opnieuw.");
                $("#loaderImportHL7").hide();
            },
            global: false,
            timeout: 20000
        });


    }
}

var maxHL7Time = 30000;
var doneHL7Time = 0;
var pollHL7Time = 2000;

function pollPatientReceived(messageID, patientNumber) {

    if (hasHL7 === true && patientreceivedurl !== undefined && patientreceivedurl.length > 0) {

        if (doneHL7Time > maxHL7Time) {
            showErrorMessage("Het is niet gelukt om de gegevens op te halen. Probeer het opnieuw.");
            $("#loaderImportHL7").hide();
            return;
        }

        doneHL7Time += pollHL7Time;

        $.ajax({
            url: patientreceivedurl,
            type: "POST",
            data: { patientNumber: patientNumber, messageID: messageID },
            success: function (data) {
                if (data.ExternalClient !== null && data.ExternalClient.PatientNumber !== null && data.ExternalClient.PatientNumber === patientNumber) {
                    fillFieldsFromData(data.ExternalClient);
                    fillFieldsFromData(data.ExternalContactPerson, "ContactPerson");
                    $("#loaderImportHL7").hide();
                }
                else {
                    setTimeout(function () { pollPatientReceived(messageID, patientNumber); }, pollHL7Time);
                }
            },
            error: function (xhr, status, error) {
                $("#loaderImportHL7").hide();
                showErrorMessage(error);
            },
            dataType: "json",
            global: false,
            timeout: 30000
        });
    }
}