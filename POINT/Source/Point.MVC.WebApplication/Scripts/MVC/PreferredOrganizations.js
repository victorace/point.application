﻿var selectedLocationRef = "#SelectedLocationID";

$(document).ready(function () {
	
    $('#orgSearchBox').bind('typeahead:select', function (ev, suggestion) {
        $("#addButton").removeClass("disabled");
        $("#favoritelocationId").val(suggestion.LocationId);
    });

    $("#addButton").click(function () {
        addLocation($("#locationID").val(), $("#favoritelocationId").val());
    });

    $('[data-toggle="clickover"]:not(.client-header)').clickover({
        html: true,
        placement: "top",
        global_close: true,
        esc_close: true,
        title: $(this).data("title"),
        content: $(this).data("content")
    });
});

loadLocations = function (element) {
    var id = $(element).val();
    var digest = $(element).find(":selected").data("digest");
    $.ajax({
        type: "POST",
        async: true,
        url: getLocationsByRegionIDUrl,
        data: { "regionid": id, "digest": digest },
        success: function (data) {
            $(selectedLocationRef + " option").remove();
            $("#favorites tbody tr").remove();

            $.each(data, function (k, item) {
                $("<option value='" + item.Value + "'>" + item.Text + "</option>").appendTo(selectedLocationRef);
            });
        }
    });
}

changeLocation = function (element) {
    var regionid = $("#SelectedRegionID").val();
    var id = $(element).val();
    document.location.href = 'Favorites?regionid=' + regionid + "&locationid=" + id;
}

function deleteLocation(element) {
    var preferredlocationid = $(element).data("preferredlocationid");
    var locationid = $(element).data("locationid");

    $.ajax({
        type: "DELETE",
        cache: false,
        url: deleteFavoriteUrl,
        data: { "locationid": locationid, "preferredlocationid": preferredlocationid },
        success: function (success) {
            if (success) {
                $("#row" + preferredlocationid).remove();
            }
        }
    });
}

function SearchLocation() {
    if (isNullOrEmpty($(selectedLocationRef).val())) {
		$("#locationrequiredmessage").show();
		$(selectedLocationRef).focus();
		return;
	}
	ShowModal(searchLocationUrl, addLocation);
}

var addLocation = function (searchedlocation) {
    var row = $("#favorites tr[id='row" + searchedlocation.LocationID + "']");
    if (row.length === 0) {
        var locationid = $(selectedLocationRef).val();
        $.ajax({
            type: "POST",
            cache: false,
            url: addFavoriteUrl,
            data: { "locationid": locationid, "preferredlocationid": searchedlocation.LocationID },
            success: function (partial) {
                if (partial) {
                    $("#favorites tbody").append(partial);
                }
            }
        });
    }
    $("#modal").modal('hide');
}