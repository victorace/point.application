﻿function SearchOrganization(sender, url, callback) {
    ShowModal(url, callback, inittable)
}

var PutHealthcareProvider = function (department) {
    var ispoint = department.Participation == 2;
    $('#destinationComment').toggle(!ispoint);

    $('input[data-linked="SelectedHealthCareProvider"]:not([type=hidden])').val(department.DepartmentName);
    $('input[data-linked="SelectedHealthCareProvider"][type=hidden]').val(department.DepartmentID);
    $("#modal").modal('hide');
    $('#SelectedHealthCareProvider').trigger("change");
};

function getLinkedValue(linked) {
    if (linked === undefined) return "";
    if ($(linked).is("select")) {
        return $(linked).find("option:selected").val();
    } else {
        return $(linked).val();
    }
};

function getLinkedDigest(linked) {
    if (linked === undefined) return "";
    if ($(linked).is("select")) {
        return $(linked).find("option:selected").data("digest");
    } else {
        return $(linked).data("digest");
    }
}

function getLinkedText(linked) {
    if (linked === undefined) return "";
    if ($(linked).is("select")) {
        var selected = $(linked).find("option:selected");
        if (selected && selected.val()) {
            return selected.text();
        }
    } else {
        return $(linked).text();
    }
    return "";
}

function getClickoverTitle() {
    var linked = $(this).data("linked");
    if (linked !== undefined) {
        return getLinkedText($("#" + linked));
    } else {
        return $(this).data("title");
    }
}

function getClickoverContent() {
    var noContent = "Nog geen gegevens beschikbaar";

    var requestUrl = $(this).data("url");
    if (requestUrl !== undefined) {

        var linked = $(this).data("linked");
        if ($.getUrlVar(requestUrl, "id") === undefined && linked !== undefined) {
            var value = getLinkedValue($("#" + linked));
            var id = parseInt(value);
            if (isNaN(id)) {
                return noContent;
            }
            var digest = getLinkedDigest($("#" + linked));
            requestUrl = update_query_string(requestUrl, "id", id);
            requestUrl = update_query_string(requestUrl, "digest", digest);
        };

        var content = $.ajax({
            url: requestUrl,
            type: "GET",
            dataType: "html",
            async: false,
            success: function () { /*just get the response*/ },
            error: function () { content = noContent }
        }).responseText;

        return content;
    }
    return $(this).attr("data-content");
}

$(function () {

    $('#SelectedHealthCareProvider').on("change", function () {
        var organizationid = $(this).val();
        var hasselectedorganizationid = organizationid !== '';
        $("#btnSendZa").toggle(hasselectedorganizationid);
        $("#sendtodestination").toggle(hasselectedorganizationid);
    });

    $("#btnSendZa").on("click", function () {       
        if ($(formID).valid()) {
            var destinationID = $('input[data-linked="SelectedHealthCareProvider"][type=hidden]').val();
            var destinationName = $('input[data-linked="SelectedHealthCareProvider"]:not([type=hidden])').val();

            var extradata = $(this).data();
            extradata.departmentIDs = destinationID.split(';');
            extradata.formsetversionid = formSetVersionID;
            extradata.alreadysent = ($(this).data("alreadysent") == "True");
            extradata.formtypeid = formtypeid;

            $('input[data-linked="DestinationHealthCareProvider"]:not([type=hidden])').val(destinationName);
            $('input[data-linked="DestinationHealthCareProvider"][type=hidden]').val(destinationID);
            $('input[data-linked="SelectedHealthCareProvider"][type=hidden]').val('');
            $('input[data-linked="SelectedHealthCareProvider"]:not([type=hidden])').val('');

            var m = moment().format("DD-MM-YYYY HH:mm");
            $('#TransferDateVVT').val(m);

            AjaxSave(formID, pointZANextPhaseUrl, true, null, false, extradata);
        }
    });

    $('[data-toggle="clickover"]:not(.client-header)').clickover({
        html: true,
        placement: "top",
        global_close: true,
        esc_close: true,
        title: getClickoverTitle,
        content: getClickoverContent
    });
});