﻿function CalculateBMI() {
    var objWeight = document.getElementById("ObesitasGewicht");
    var objHeight = document.getElementById("ObesitasLengte");
    var objBMI = document.getElementById("ObesitasBMI");
    if (!objHeight || !objWeight || !objBMI) return;

    var weight = objWeight.value == "" ? "0" : objWeight.value;
    var height = objHeight.value == "" ? "0" : objHeight.value;

    var res = "";
    if (height != "0" && weight != "0") {
        var res = parseFloat(weight) / Math.pow(parseInt(height) / 100, 2);
        res = res.toFixed(1);
    }

    objBMI.value = res;
}

function SearchOrganization(sender, url, callback) {
    ShowModal(url, callback, inittable)
}

var PutOrganisation = function (department) {
    $('input[data-linked="HealthCareProvider"]:not([type=hidden])').val(department.DepartmentName);
    $('input[data-linked="HealthCareProvider"][type=hidden]').val(department.DepartmentID);
    $("#modal").modal('hide');
};

function infectieIsolatieAanwezig() {
    if ($(this).val() == "Ja") {
        $("#InfectieIsolatieAanwezig").show();
    }
    else {
        $("#InfectieIsolatieAanwezig").hide();
        ClearContainer("#InfectieIsolatieAanwezig");
    }
}

function setVerslaving()
{
    $("#VerslavingAanwezig").toggle($(this).is(":checked"));
    if ($(this).is(":checked") == false)
        ClearContainer("#VerslavingAanwezig");
}

function clearHealthCareProvider() {
    $("input[name*='_HealthCareProvider_']").val("");
}


function CheckContraIndicatieGRZ() {
    var dwaalzwerggedragchecked = $("#RelevanteAspectenVoorZorgverleningDwaalZwerfgedrag").is(":checked");
    var bewustzijnunchecked = !$("#Bewustzijn").is(":checked");
    var motivatieunchecked = !$("#MotivatieVoorRevalidatie").is(":checked");

    var desorientatie = $("input[name*='_Desorientatie_']:checked").val();
    var desorientatiechecked = desorientatie === "Matig" || desorientatie === "Ernstig";

    $("#indicaties .indic-dwaal").toggle(dwaalzwerggedragchecked);
    $("#indicaties .indic-bewustzijn").toggle(bewustzijnunchecked);
    $("#indicaties .indic-motivatie_revalidatie").toggle(motivatieunchecked);
    $("#indicaties .indic-desorientatie").toggle(desorientatiechecked);

    var hascontraindicatie = dwaalzwerggedragchecked || bewustzijnunchecked || motivatieunchecked || desorientatiechecked;
    $("#contraindicatiegrz").toggle(hascontraindicatie);

    if (hascontraindicatie) {
        $("#GRZFormContraIndicationOverride").removeProp("checked");
        $("#contraindicatiegrz").removeClass("indicatie-off").addClass("indicatie-on");
    } else {
        $("#contraindicatiegrz").removeClass("indicatie-on").add("indicatie-off");
    }

    $("#divGRZFormContraIndicationOverride").removeProp("style");

    if ($("#divGRZFormContraIndicationOverride").find(".indicatie-on").length > 0) {
        $("#divGRZFormContraIndicationOverride").removeClass("ignore");
    } else {
        $("#divGRZFormContraIndicationOverride").addClass("ignore");
    }

    $(window).trigger("resize");
}

function CheckOverlegSO() {
    var bewustzijnunchecked = !$("#Bewustzijn").is(":checked");
    var cognitieunchecked = !$("#Cognitie").is(":checked");
    var verwaarlozingchecked = $("#RelevanteAspectenVoorZorgverleningVerwaarlozing").is(":checked");
    var zwakmantelzorgchecked = $("#RelevanteAspectenVoorZorgverleningZwakOntbrekendMantelzorgnetwerk").is(":checked");
    var motivatieunchecked = !$("#MotivatieVoorRevalidatie").is(":checked");

    var delier = $("input[name*='_Delier_']:checked").val();
    var delierchecked = delier === "Aanwezig";

    var terugkeernaarhuis = $("input[name*='_InschattingTerugkeerNaarHuis_']:checked").val();
    var terugkeernaarhuischecked = terugkeernaarhuis === "Misschien" || terugkeernaarhuis === "Nee";

    var desorientatie = $("input[name*='_Desorientatie_']:checked").val();
    var desorientatiechecked = desorientatie === "Matig" || desorientatie === "Ernstig";

    $("#indicaties .indic-bewustzijn").toggle(bewustzijnunchecked);
    $("#indicaties .indic-verwaarlozing").toggle(verwaarlozingchecked);
    $("#indicaties .indic-mantelzorg").toggle(zwakmantelzorgchecked);
    $("#indicaties .indic-cognitie").toggle(cognitieunchecked);
    $("#indicaties .indic-delier").toggle(delierchecked);
    $("#indicaties .indic-motivatie_revalidatie").toggle(motivatieunchecked);
    $("#indicaties .indic-terugkeer_thuis").toggle(terugkeernaarhuischecked);
    $("#indicaties .indic-desorientatie").toggle(desorientatiechecked);

    var hasoverlegso =
        cognitieunchecked ||
        bewustzijnunchecked ||
        verwaarlozingchecked ||
        zwakmantelzorgchecked ||
        delierchecked ||
        motivatieunchecked ||
        terugkeernaarhuischecked ||
        desorientatiechecked;

    if (hasoverlegso) {
        $("#overlegso").removeClass("indicatie-off").addClass("indicatie-on");
    } else {
        $("#overlegso").removeClass("indicatie-on").addClass("indicatie-off");
    }

    $("#divGRZFormContraIndicationOverride").removeProp("style");

    if ($("#divGRZFormContraIndicationOverride").find(".indicatie-on").length > 0) {
        $("#divGRZFormContraIndicationOverride").removeClass("ignore");
    } else {
        $("#divGRZFormContraIndicationOverride").addClass("ignore");
    }

    $(window).trigger("resize");
}

$(function () {
    $(window).on("resize", function () {
        var height = GetScrollHeight();
        $("#Scroller").css("max-height", height);
    });
    $(window).trigger("resize");

    var infectieIsolatieJaNee = GetNameByID("#InfectieIsolatie");
    $("[name='" + infectieIsolatieJaNee + "']input:radio").on("click", infectieIsolatieAanwezig);
    GetSelectedRadioByName(infectieIsolatieJaNee).each(infectieIsolatieAanwezig); 

    $("#Verslaving").on("change", setVerslaving);
    $("#Verslaving").each(setVerslaving);

    $("#ObesitasGewicht, #ObesitasLengte").on("change keyup", function () {
        CalculateBMI();
    });

    $("#RelevanteAspectenVoorZorgverleningDwaalZwerfgedrag").on("click", function (e) {
        CheckContraIndicatieGRZ();
    });

    $("#RelevanteAspectenVoorZorgverleningVerwaarlozing,#RelevanteAspectenVoorZorgverleningZwakOntbrekendMantelzorgnetwerk").on("click", function (e) {
        CheckOverlegSO();
    });

    $("#Bewustzijn").on("click", function (e) {
        CheckContraIndicatieGRZ();
        CheckOverlegSO();
    });

    $("#Cognitie").on("click", function (e) {
        CheckOverlegSO();
    });

    $("input[name*='_Delier_']").on("click", function (e) {
        CheckOverlegSO();
    });

    $("#MotivatieVoorRevalidatie").on("click", function (e) {
        CheckContraIndicatieGRZ();
        CheckOverlegSO();
    });

    $("input[name*='_InschattingTerugkeerNaarHuis_']").on("click", function (e) {
        CheckOverlegSO();
    });

    $("input[name*='_Desorientatie_']").on("click", function (e) {
        CheckOverlegSO();
        CheckContraIndicatieGRZ();
    });
    
    $("input[name*='_MantelzorgerAanwezig_']").on("click", function (e) {
        var value = e.target.defaultValue;
        $("#MantelzorgerAanwezigJa").toggle(value === "Ja");
    });
});