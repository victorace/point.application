﻿var transfertask = true;

function transfertaskadd(addbutton) {
    ShowModal($(addbutton).data("url"));
}

function transfertaskedit(editbutton) {
    ShowModal($(editbutton).data("url"));
}

function transfertaskconfirmdelete(deletebutton) {
    $(".transfertaskmodaldelete").data("url", $(deletebutton).data("url"));
    $(".transfertaskmodaldelete").modal("show");
}

function transfertaskdelete(confirmbutton) {
    $.ajax({
        type: "GET",
        url: $(confirmbutton).closest(".transfertaskmodaldelete").data("url"),
        success: function (data) {
            if (data.deleted) {
                $(".transfertask[data-id='" + data.transfertaskid + "']").remove();
                $("#modal").modal("hide");
                $(".transfertaskmodaldelete").modal("hide");
            }
        }
    });
}

function transfertaskreopen(button) {
    setstatus(button, 1); //1 active
}

function transfertasksetdone(button) {
    setstatus(button, 2); //2 done
}

function setstatus(button, status) {
    $.ajax({
        type: "GET",
        url: $(button).data("url"),
        data: { status: status },
        success: function (data) {
            reloadtasklist();
        }
    });
}

var reloadtasklist = function (data) {
    $.ajax({
        cache: false,
        type: "GET",
        url: $(".transfertasklistcontainer").data("url"),
        success: function (data) {
            $(".transfertasklistcontainer").html(data);
            $("#modal").modal("hide");
        },
        error: function (data) {
            showErrorMessage(data.responseText);
        }
    });
};

function transfertasksave(savebutton) {
    $(savebutton).attr("disabled", true);
    AjaxSave($(savebutton).closest("form"), $(savebutton).data("url"), null, reloadtasklist, true);
}

function transfertaskfrommemo(sender) {
    ShowModal($(sender).data("url"));
}