﻿var locationCheckboxSelector = "#organizationMigrateLocationList input.locationcheckbox";
var selectAllLocationsSelector = "#locationsAll";
var startMigrationSelector = "#migrationStart";
var organizationGridSelector = "#organizationMigrateGrid";
var searchOrgButtSelector = "#searchOrganizations";
var orgCheckboxSelector = "input.organizationcheckbox";
var orgNameOrIDSelector = "#organizationNameOrID";
var isChecked = ":checked";

$(function() {

    $(document).ready(function() {
        $("[name='SourceOrTarget']").on("click",
            function() {
                setFocusToOrganization();
                $(orgNameOrIDSelector).focus();
            });

        $(orgNameOrIDSelector).keyup(function (event) {
            if (event.keyCode === 13) {
                $(searchOrgButtSelector).click();
            }
        });
        
        // Initialization
        setFocusToOrganization();
        enableStartMigration();
    });

});


function ajaxRequest(inputData, url) {
    if (!inputData) {
        return;
    };

    $.ajax({
        type: "POST",
        url: url,
        data: inputData,
        dataType: "json",
        success: function(data) { replaceData(data); },
        error: function (xhr, status, error) { showErrorMessage(error); }
    });
}

function getSourceOrTarget() {
    return $("#SourceOrTarget" + isChecked).val();
}

function setFocusToOrganization() {
    var sourceOrTarget = getSourceOrTarget();

    $(".org-header").removeClass("header-active");
    $("#orgarnizationHeader" + sourceOrTarget).addClass("header-active");

    $(".org-title").removeClass("title-active");
    $("#organizationTitle" + sourceOrTarget).addClass("title-active");

    if (sourceOrTarget === sourceID) {
        $("#locationsTitle").addClass("title-active");
    }
}

function replaceData(data) {

    $.each(data.htmlReplaces || [],
        function(id, html) {
            if ($(id).length) {
                $(id).replaceWith($(html));
            };
        });

    enableStartMigration();
    setFocusToOrganization();
}

function enableStartMigration() {
    $(startMigrationSelector).removeClass("disabled").addClass("btn").addClass("active");

    var sourceOrganizationRef = $(organizationGridSelector + sourceID + " " + orgCheckboxSelector + ":checked");
    var targetOrganizationRef = $(organizationGridSelector + targetID + " " + orgCheckboxSelector + ":checked");
    var locationsRef = $(locationCheckboxSelector + ":checked");

    if (sourceOrganizationRef.length !== 1 || targetOrganizationRef.length !== 1 || sourceOrganizationRef.val() === targetOrganizationRef.val() || locationsRef.length==0) {
        $(startMigrationSelector).removeClass("btn").removeClass("active").addClass("disabled");
    }
}

function getLocations(organizationID) {
    var organizationRef = organizationID + isChecked;
    var inputData = "organizationID=" + ($(organizationRef).length ? $(organizationRef).val() : "0");
    ajaxRequest(inputData, getLocationsUrl);
}

function getOrganizations() {
    $(searchOrgButtSelector).addClass("header-active");
    $(searchOrgButtSelector).addClass("title-active");
    
    var inputData = "organizationNameOrID=" + $(orgNameOrIDSelector).val() + "&sourceOrTarget=" + getSourceOrTarget();
    ajaxRequest(inputData, getOrganizationsUrl);

    $(searchOrgButtSelector).removeClass("header-active");
    $(searchOrgButtSelector).removeClass("title-active");
}


function migrateLocations() {
    if ($(startMigrationSelector).hasClass("disabled")) {
        return;
    };

    var sourceOrganizationID = $(organizationGridSelector + sourceID + " " + orgCheckboxSelector + ":checked").val();
    var targetOrganizationID = $(organizationGridSelector + targetID + " " + orgCheckboxSelector + ":checked").val();

    var inputData = $("[name='LocationIDs']" + isChecked).serialize() + "&sourceOrganizationID=" + sourceOrganizationID + "&targetOrganizationID=" + targetOrganizationID;
    
    ShowModal(appPath + migrateResultsUrl + "?" + inputData);
}

function refreshLocations() {
    var sourceOrganizationIDRef = organizationGridSelector + sourceID + " " + orgCheckboxSelector;
    getLocations(sourceOrganizationIDRef);
}

function selectAllLocations() {
    $(locationCheckboxSelector).prop('checked', $(selectAllLocationsSelector).is(isChecked));
    enableStartMigration();
}

function selectLocation(locationID) {
    var id = "#loc" + locationID;
    if ($(id)) {
        if ($(locationCheckboxSelector + ':checked').length === 0 && $(selectAllLocationsSelector).is(isChecked)) {
            $(locationCheckboxSelector).prop('checked', false);
        };
        enableStartMigration();
    };
}

function selectOrganization(organizationID, sourceOrTarget) {
    var id = "#" + sourceOrTarget + organizationID;
    if ($(id)) {
        $(organizationGridSelector + sourceOrTarget + " " + orgCheckboxSelector + ":not(" + id + ")").prop("checked", false);

        if (sourceOrTarget !== sourceID) {
            enableStartMigration();
            return;
        };
        getLocations(id);
    };
}

function filterLines() {
    var filterCheckboxRef = "#filterLines input[data-filtertype='";
    var linesSelector = "#migrateOrgResultLines #gridTable tr";

    var checkboxAll = filterCheckboxRef + displayAll + "']";
    var checkboxErrors = filterCheckboxRef + displayErrors + "']";
    var checkboxHeaders = filterCheckboxRef + displayHeaders + "']";
    var checkboxWarnings = filterCheckboxRef + displayWarnings + "']";

    if ($(checkboxAll).is(isChecked)) {
        $(linesSelector).removeClass("hidden");
    } else {
        $(linesSelector).addClass("hidden");
        if ($(checkboxErrors).is(isChecked)) {
            $(linesSelector + ".line-error").removeClass("hidden");
        }
        if ($(checkboxHeaders).is(isChecked)) {
            $(linesSelector + ".line-header").removeClass("hidden");
        }
        if ($(checkboxWarnings).is(isChecked)) {
            $(linesSelector + ".line-warning").removeClass("hidden");
        }
    }

}
