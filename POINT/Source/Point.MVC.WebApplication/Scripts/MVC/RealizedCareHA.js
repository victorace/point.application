﻿//Note: also called from CloseTransferHA.cshtml

$(function () {

    var registrationEndedWithoutTransfer = $("#RegistrationEndedWithoutTransfer:checked").val();
    $("#ReasonEndRegistrationContainer").toggle(registrationEndedWithoutTransfer == "true");

    $("#RegistrationEndedWithoutTransfer").on("change", function () {
        if (!$(this).checked) {
            var reasonendregistrationha = GetNameByID("#ReasonEndRegistrationHA");
            $("[name='" + reasonendregistrationha + "']input:checkbox").removeAttr("checked");
        }

        $("#ReasonEndRegistrationContainer").toggle($(this).checked);
    });



});
