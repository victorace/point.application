﻿function GetDepartments()
{
    $("#DepartmentID").html("");

    var organizationID = $(this).val();
    if (organizationID != "") {
        $("#editbutton").prop('disabled', false);
        $.ajax({
            type: "POST",
            url: getDepartmentsByOrganizationUrl,
            data: {
                organizationID: organizationID
            },
            success: function (data) {
                $("#DepartmentID").append($('<option></option>').val("").html("Alle afdelingen"));
                for (var opt in data) {
                    $("#DepartmentID").append($('<option></option>').val(data[opt].Value).html(data[opt].Text));
                }
            }
        });
    }
}


$(function () {
    $(".edittype-select").on("change", function () {
        $(".edittype-row").find("input, select").prop('disabled', true);
        $(this).closest(".row").find("input, select").prop('disabled', false);
    });

    $(".sortorder-select").on("change", function () {
        $(this).closest("form").find(".sortorder-select").not(this).val('');
    });

    $(".edittype-select").first().click();

    $("#OrganizationID").on("change", GetDepartments);
    $("#OrganizationID").first("option").each(GetDepartments);
});