﻿

$(function () {

    //Ignore validation on all elements inside a container with ignore as classname
    $.validator.setDefaults({ ignore: '.ignore *' });

    $(window).on("resize", function () {
        var height = GetScrollHeight();
        $("#Scroller").css("max-height", height);
        $("#Scroller").css("height", height);
    });
    $(window).trigger("resize");

    SetupDependencies()
});

function SetupDependencies() {
    SelectToggleChildren("#VerblijfplaatsNa3mnd", "#OntslagenNaarAnderZiekenhuis", ["0"]);
    SelectToggleChildren("#VerblijfplaatsNa3mnd", "#OntslagenNaarAnders", ["7"]);
}

function reopendoorlopenddossier(sender) {
    $.ajax({
        type: "GET",
        url: $(sender).attr("data-url"),
        success: function (data) { location.reload(); }
    });
}