﻿function handleEmployeeTelephoneNumber(data) {
    if (data !== undefined && data.length === 1) {
        $('#AcceptedByTelephoneII').val(data[0].Text);
    }
}

$(function () {

    $('#AcceptedByII').on("change", function () {
        var employeeID = $(this).val();
        var digest = $(this).find(":selected").data("digest");
        $.ajax({
            type: "POST",
            url: getEmployeePhoneNumberByEmployeeID,
            data: { employeeID: employeeID, digest: digest },
            success: function (data) { handleEmployeeTelephoneNumber(data); }
        });
    });

    //Alleen bij nieuw doc
    if (formSetVersionID === -1) {
        $("#AcceptedByII").trigger("change");
    }
});