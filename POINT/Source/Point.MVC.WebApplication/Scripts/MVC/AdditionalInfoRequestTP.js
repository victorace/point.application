﻿function GetScrollHeight() {
    return ($(window).innerHeight() - $("#Scroller").offset().top - 20);
}

$(function () {
    $(window).on("resize", function () {
        var height = GetScrollHeight();
        $("#Scroller").css("max-height", height);
    });
    $(window).trigger("resize");

    MakeRadiosDeselectableByID("#AfterCarePreference");
});