﻿function ShowModal(requestUrl, callback) {
    $.ajax({
        cache: false,
        type: "POST",
        url: requestUrl,
        data: $(this).data(),
        async: true,
        success: function (data) {
            if (data.no_results == undefined) {
                $("#modal").modal('show');
                $("#modal .modal-content").data("callback", callback);
                $("#modal .modal-body").html(data);
            }
        },
        error: function (data) {
            $("#modal").modal('show');
            $("#modal .modal-body").html(data.responseText);
        }
    });
}

function DoCallback(item) {
    var callback = $("#modal .modal-content").data('callback');
    if (callback) {
        callback($.parseJSON(item));
    }
}

function showRelatedTransfersWithVVT() {
    var transferid = getRequestParam("transferid");
    var url = appPath + "/Transfer/RelatedTransfersWithVVT?TransferID=" + transferid;
    ShowModal(url);
}


function getRequestParam(name) {
    if (name = (new RegExp('[?&]' + encodeURIComponent(name.toLowerCase()) + '=([^&]*)')).exec(location.search.toLowerCase()))
        return decodeURIComponent(name[1]);
}

function UseVVTFromTransfer(data)
{
    $("#modal").modal('hide');
    var jsondata = $.parseJSON(data);

    var vvtid = jsondata.RealizedVVTID;
    var vvttxt = jsondata.RealizedVVT;
    var pointparticipator = jsondata.PointParticipator;

    if (vvtid == null) {
        vvtid = jsondata.PreferredVVTID;
        vvttxt = jsondata.PreferredVVT;
    }
    if (vvtid == null) {
        vvtid = jsondata.SendingVVTID;
        vvttxt = jsondata.SendingVVT;
    }

    if ($('input[data-linked="ZorgInZorginstellingVoorkeurPatient1"][type=hidden]').length > 0) {
        $('input[data-linked="ZorgInZorginstellingVoorkeurPatient1"]:not([type=hidden])').val(vvttxt);
        $('input[data-linked="ZorgInZorginstellingVoorkeurPatient1"][type=hidden]').val(vvtid);
    }

    //even niet verwijderen er komt nog wijziging
    //if ($('input[data-linked="ZorginzetVoorDezeOpnameTypeZorginstellingNaam"][type=hidden]').length > 0) {
    //    $('input[data-linked="ZorginzetVoorDezeOpnameTypeZorginstellingNaam"]:not([type=hidden])').val(vvttxt);
    //    $('input[data-linked="ZorginzetVoorDezeOpnameTypeZorginstellingNaam"][type=hidden]').val(vvtid);
    //}

    if ($("#thuiszorgshortrequest").length > 0) {
        var shortRequestHomeCare = $("#BestaandeAanbieder");
        shortRequestHomeCare.val(vvttxt);
    }

    if ($("#zorgsoortshortrequest").length > 0) {
        var shortRequestHidden = $("#zorgsoortshortrequest .OrganizationSelectorHiddenId input");
        var shortRequestHiddenHasPoint = $("#zorgsoortshortrequest .OrganizationSelectorHiddenHasPoint input");
        var shortRequestHiddenText = $("#zorgsoortshortrequest .OrganizationSelectorHiddenText input");

        var shortRequestSelectedText = $("#zorgsoortshortrequest .SelectedText");
        var shortRequestSearchSelectedText = $("#zorgsoortshortrequest .SearchSelectedText");

        shortRequestHidden.val(vvtid);
        shortRequestHiddenHasPoint.val(pointparticipator);
        shortRequestHiddenText.val(vvttxt);

        shortRequestSelectedText.val(vvttxt);
        shortRequestSearchSelectedText.val(vvttxt);
    }

    if($("#hdfPreviousZA").length > 0)
    {
        if (vvtid != null) {
            var fullRequestHidden = $("#zorgsoortfullrequestvoorkeur1 .OrganizationSelectorHiddenId input");
            var fullRequestHiddenHasPoint = $("#zorgsoortfullrequestvoorkeur1 .OrganizationSelectorHiddenHasPoint input");
            var fullRequestHiddenText = $("#zorgsoortfullrequestvoorkeur1 .OrganizationSelectorHiddenText input");

            var fullRequestSelectedText = $("#zorgsoortfullrequestvoorkeur1 .SelectedText");
            var fullRequestSearchSelectedText = $("#zorgsoortfullrequestvoorkeur1 .SearchSelectedText");

            fullRequestHidden.val(vvtid);
            fullRequestHiddenHasPoint.val(pointparticipator);
            fullRequestHiddenText.val(vvttxt);

            fullRequestSelectedText.val(vvttxt);
            fullRequestSearchSelectedText.val(vvttxt);
        }

        if (jsondata.SendingVVTID != null)
        {
            $("#hdfPreviousZA").val(jsondata.SendingVVTID);
            $("#hdfPreviousZA").closest(".NewFormFieldFieldCell").find("input:radio").first().click();
        }
    }

}