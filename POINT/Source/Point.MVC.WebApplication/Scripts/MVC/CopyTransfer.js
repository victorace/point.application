﻿function cancel (sender) {
    var url = $(sender).data("url");
    if (url !== undefined && url.length > 0) {
        $("#loader").show();
        location.replace(url);
    }
}

$(function () {
    var form = $(formID);
    form.submit(function (e) {
        e.preventDefault();
        $("#loader").show();
        AjaxSave(formID, copytransferurl, false, function (data) {
            if (data.errormessage) {
                $("#loader").hide();
                showErrorMessage(data.errormessage)
            } else {
                window.setTimeout(function () { $("#loader").show(); location.replace(data.autoRedirectUrl); }, 2000);
            }
        }, true);
    });
});