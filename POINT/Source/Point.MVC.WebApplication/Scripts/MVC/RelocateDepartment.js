﻿$(function () {
    var thisForm = $(formID);
    thisForm.submit(function (e) {
        e.preventDefault();
        AjaxSave(formID, thisForm.action, false, function (data) {
            $("#movedmodal").on("shown.bs.modal", function () {
                $("#movedmodal .modal-body").html(data.message);
                $("#movedmodal #btnmovedok").on("click", function () {
                    $("#movedmodal").modal("hide");
                    location.replace(data.returnurl);
                });
            });
            $("#movedmodal").modal({ show: "true", backdrop: "static", keyboard: false });
        }, true);
    });
});