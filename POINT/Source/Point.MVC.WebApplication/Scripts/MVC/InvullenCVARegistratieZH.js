﻿$(function () {

    $(window).on("resize", function () {
        var height = GetScrollHeight();
        $("#Scroller").css("max-height", height);
    });
    $(window).trigger("resize");


    //Ignore validation on all elements inside a container with ignore as classname
    $.validator.setDefaults({ ignore: '.ignore *' });

    SetupDependencies();
});

function SetupDependencies() {
    RadioToggleChildren("#Diagnose", "#VerrichtingBijDiagnoseOnbloedig", ["2"]);
    RadioToggleChildren("#Diagnose", "#VerrichtingBijDiagnoseTIA", ["3"]);

    RadioToggleChildren("#InZHIntraveneusGetrombolyseerd", "#IntraveneusGetrombolyseerdNee", ["0"]);
    RadioToggleChildren("#InZHIntraveneusGetrombolyseerd", "#IntraveneusGetrombolyseerdJa", ["1"]);

    RadioToggleChildren("#InZHIntraArterieelBehandeld", "#ArterieelBehandeldJa", ["1"]);

    RadioToggleChildren("#PatientWordtOpgenomenOpStrokeUnit", "#PatientWordtOpgenomenOpStrokeUnitChildren", ["1"]);
    CheckboxToggleChildren("#RedenTrombolyseNietUitgevoerdAnders", "#RedenTrombolyseNietUitgevoerdAndersChildren");

    RadioToggleChildren("#DiagnoseEersteKlachtDatumBekend", "#DiagnoseEersteKlachtDatumBekendJa", ["1"]);
}
