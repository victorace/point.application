﻿function PhaseButtonStart()
{
    if ($(formID).valid()) {
        $(".phaseButtonStart").attr("disabled", true);
        AjaxSave(formID, formRequestUrl, true, null, true);
        FormIsNotDirty();
    }
}

function PhaseButtonCreate(sender) {
    if ($(formID).valid()) { 
        $(sender).data('FlowDefinitionID', getQueryStringParameter("FlowDefinitionID"));
        CheckOpenDossier($(sender).data(), DoNevermindOpenDossierCallback);  
    }
}

function DoNevermindOpenDossierCallback(data)
{
    $(".phaseButtonCreate").attr("disabled", true);
    AjaxSave(formID, formRequestUrl, false, doSaveCallback, null, data);    
    FormIsNotDirty();
}

function doSaveCallback(data) {
    location.replace(dashboardUrl + "?TransferID=" + data.TransferID);
}

function PhaseButtonSave(sender, formsetversionid, disabledoubleclick, forcereload)
{
    if (SignaleringModalNeeded()) {
        ShowSignaleringModal(appPath + "/Signalering/PromptFlowFieldsChanged", JSON.stringify(GetSignaleringItems()), sender);
    } else if (digitalSignatureModalNeeded(sender)) {
        $("#signphaseinstancemodal").modal("show");
    } else {
        if ($(sender).hasClass("cancel") && $(formID).data("validator") != undefined) {
            $(formID).data("validator").cancelSubmit = true;
        }

        // disable buttons when formsetversionid = -1
        if (formsetversionid == '-1' && disabledoubleclick != 'True') {
            $("#phaseButtonsHolder .btn").attr("disabled", true);
        }

        // do reload if current formsetversion == -1
        AjaxSave(formID, formRequestUrl, formsetversionid == '-1' || forcereload, null, false, $(sender).data());
        FormIsNotDirty();
    }
}

function phaseButtonNextConfirmed(sender, formsetversionid)
{
    $("#confirmphasenextmodal").modal("hide");

    $("#phaseButtonsHolder .btn").attr("disabled", true);

    if (formsetversionid != '-1') {
        $(sender).data("formsetversionid", formsetversionid);
    }

    AjaxSave(formID, formRequestUrl, true, null, false, $(sender).data());
    FormIsNotDirty();
}

function PhaseButtonNext(sender, formsetversionid)
{
    if ($(formID).valid()) {
        if (SignaleringModalNeeded()) {
            ShowSignaleringModal(appPath + "/Signalering/PromptFlowFieldsChanged", JSON.stringify(GetSignaleringItems()), sender);
        } else {
            if (typeof $(sender).data("confirm") !== "undefined") {
                var confirmcontent = $(sender).data("confirm");
                var content = $("#" + confirmcontent);
                if (content.length) {
                    $("#confirmphasenextmodalyes").data("nextphasedefinitionid", $(sender).data("nextphasedefinitionid"));
                    $("#confirmphasenextmodalyes").data("handlenextphase", $(sender).data("handlenextphase"));
                    $("#confirmphasenextmodal .modal-body").html(content.clone().contents());
                    $("#confirmphasenextmodal").modal("show");
                }
                else {
                    phaseButtonNextConfirmed(sender, formsetversionid);
                }
            }
            else {
                phaseButtonNextConfirmed(sender, formsetversionid);
            }
        }
   }
}

function PhaseButtonCancel()
{
    FormIsNotDirty();
    location.assign($(this).data("url"));
}

function PhaseButtonExtra()
{
    $(this).click(function () {
        location.assign($(this).data("url"));
    });
}

function digitalSignatureModalNeeded(sender) {
    return ($(sender).data("digitalsignature") === "True" && $(sender).data("digitallysigning") === undefined);
}

function digitalSignatureModalAction(signing) {
    $("#signphaseinstancemodal").modal("hide");
    $(".phaseButtonDefinitive").data("digitallysigning", signing);
    $(".phaseButtonDefinitive").trigger("click");
}
