﻿$(function () {
    var elvwlzindicatiename = GetNameByID("#ELVWlzIndicatieAanwezig");
    var elvwlzindicatie = GetRadioByName(elvwlzindicatiename);

    $(elvwlzindicatie).on("change", function () {
        CheckAndSetConclusion();
    });

    var elvmeervoudigeproblematiekname = GetNameByID("#ELVBeinvloedendeMeervoudigeProblematiek");
    var elvmeervoudigeproblematiek = GetRadioByName(elvmeervoudigeproblematiekname);

    $(elvmeervoudigeproblematiek).on("change", function () {
        CheckAndSetConclusion();
    });

    var elvlevensverwachtingminderdan3mndname = GetNameByID("#ELVLevensVerwachtingMinderDan3Mnd");
    var elvlevensverwachtingminderdan3mnd = GetRadioByName(elvlevensverwachtingminderdan3mndname);
    $(elvlevensverwachtingminderdan3mnd).on("change", function () {
        CheckAndSetConclusion();
    });

    var elvIndicatieVerzilverdName = GetNameByID("#ELVIndicatieVerzilverd");
    var elvIndicatieVerzilverd = GetRadioByName(elvIndicatieVerzilverdName);
    $(elvIndicatieVerzilverd).on("change", function () {
        CheckAndSetConclusion();
    });

    var elvIndicatieZZPProfielName = GetNameByID("#ELVIndicatieZZPProfiel");
    var elvIndicatieZZPProfiel = GetRadioByName(elvIndicatieZZPProfielName);
    $(elvIndicatieZZPProfiel).on("change", function () {
        CheckAndSetConclusion();
    });

	$('[data-toggle="clickover"]:not(.client-header)').clickover({
        html: true,
        placement: $(this).attr("data-placement") ? $(this).data("placement") : "top",
        global_close: true,
        esc_close: true,
        template: '<div class="popover zorgvraagpopover"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>',
        content: $(this).data("content")
    });

    RadioToggleChildren("#ELVWlzIndicatieAanwezig", "#divELVWlzIndicatieAanwezig", ["Ja"]);

    var lvwlzindicatieVal = getRadioValueByID("#ELVWlzIndicatieAanwezig");
    if(lvwlzindicatieVal === "Ja")
    {
        $("#divELVWlzIndicatieAanwezig").removeClass("ignore");
    }

    var zorgprofiel = getRadioValueByID("#PatientHeeftWLZZorgprofiel");
    if (zorgprofiel === "Nee" || zorgprofiel === "Onbekend") {
        $("#PatientHeeftBezwaar").show();
    }
    else
    {
        $("#PatientHeeftBezwaar").hide();
    }

    var patientHeeftBezwaarTegenZorgprofielVerificatie = getRadioValueByID("#PatientHeeftBezwaarTegenZorgprofielVerificatie");
    if (patientHeeftBezwaarTegenZorgprofielVerificatie === "Ja") {
        $("#BezwaarTekst").Show();
    }
    else {
        $("#BezwaarTekst").hide();
    }

    $(window).on("resize", function () {
        var height = GetScrollHeight();
        $("#Scroller").css("max-height", height);
    });
    $(window).trigger("resize");
});

function OnWLZchange(sender)
{
    if ($(sender).val().toLowerCase() != 'ja') {        
        $('#PatientHeeftBezwaar').show();
        $("#PatientHeeftBezwaarTegenZorgprofielVerificatie").on("change", function () {
            OnPatientHeeftBezwaar("#PatientHeeftBezwaarTegenZorgprofielVerificatie");
        });        
    }
    else
    {
        $('#PatientHeeftBezwaar').hide();
    }
}

function OnPatientHeeftBezwaar(sender) {
    if ($(sender).val().toLowerCase() === 'ja') {
        $('#BezwaarTekst').show();
    }
    else {
        $('#BezwaarTekst').hide();
    }
}


function CheckAndSetConclusion()
{
    var lvwlzindicatieVal = getRadioValueByID("#ELVWlzIndicatieAanwezig");
    var elvmeervoudigeproblematiekVal = getRadioValueByID("#ELVBeinvloedendeMeervoudigeProblematiek");
    var elvlevensverwachtingminderdan3mndVal = getRadioValueByID("#ELVLevensVerwachtingMinderDan3Mnd");
    var elvIndicatieVerzilverdVal = getRadioValueByID("#ELVIndicatieVerzilverd");
    var elvIndicatieZZPProfielVal = getRadioValueByID("#ELVIndicatieZZPProfiel");

    if (lvwlzindicatieVal === "Ja" && !(elvIndicatieVerzilverdVal === "Niet verzilverd" && elvIndicatieZZPProfielVal === "Laag"))
    {
        if (elvIndicatieVerzilverdVal > "" && elvIndicatieZZPProfielVal > "") {
            setRadioButtonValue("#ELVConclusieAfweging", "Opname Logeeropvang/Crisisbed");
        }
    } else {
        if (elvlevensverwachtingminderdan3mndVal === "Ja") {
            setRadioButtonValue("#ELVConclusieAfweging", "Opname ELV Palliatief");
        } else if (elvmeervoudigeproblematiekVal === "Meervoudig") {
            setRadioButtonValue("#ELVConclusieAfweging", "Opname ELV Hoog-Complex");
        } else if (elvmeervoudigeproblematiekVal === "Enkelvoudig") {
            setRadioButtonValue("#ELVConclusieAfweging", "Opname ELV Laag-Complex");
        } else {
            var elementName = GetNameByID("#ELVConclusieAfweging");
            var elements = GetRadioByName(elementName);
            $(elements).removeAttr("checked");
        }
    }
}

function getRadioValueByID(elementID)
{
    var elementName = GetNameByID(elementID);
    var radioButtonControlElement = $("input:radio[name='" + elementName + "']:checked");
    return radioButtonControlElement.val();
}