﻿$(function () {
    var form = $("#EditFlowAttachmentForm");
    if (form.length) {
        var dropdown = form.find("#AttachmentTypeID");
        var textbox = form.find("#Name");
        dropdown.change(function (ev) {
            if (textbox.val() === "")
                textbox.val($("#AttachmentTypeID").find("option:selected").text());
        });
    }
});