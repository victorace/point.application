﻿function ExternalPatient(options) {

    var instance = this;
    var _timer = 0;
    var _count = 0;
    var _elapsedtime = 0;
    var _messageid = null;

    options = $.extend({}, { loader: '#loaderImportHL7', maxtime: 30000, polltime: 2000, timeout: 10000, patientnumber: null, requesturl: null, receiveurl: null, messages: { notfound: 'Het is niet gelukt om de gegevens op te halen. Probeer het opnieuw.' } }, options);

    getmessageid = function () {
        if (!isNullOrEmpty(options.requesturl)) {
            $.ajax({
                url: options.requesturl,
                type: "POST",
                data: { patientNumber: options.patientnumber },
                success: function (data) {
                    if (data.MessageID !== null) {
                        _messageid = data.MessageID;
                        instance.collect();
                    }
                },
                error: function (xhr, status, error) {
                    $(loader).hide();
                    clearmessageid();
                    showErrorMessage(error);
                },
                global: false,
                timeout: options.timeout
            });
        }
    };

    clearmessageid = function () {
        _messageid = null;
    };

    stop = function () {
        $(options.loader).hide();
        if (_timer) {
            clearTimeout(_timer);
            _timer = 0;
        }
    };

    this.collect = function () {
        if (!isNullOrEmpty(options.patientnumber) && !isNullOrEmpty(options.receiveurl)) {
            $(options.loader).show();
            if (!isNullOrEmpty(options.requesturl) && isNullOrEmpty(_messageid)) {
                getmessageid();
            } else {
                if (_elapsedtime > options.maxtime) {
                    showErrorMessage(options.messages.notfound);
                    stop();
                    return false;
                }
                _elapsedtime += options.polltime;
                $.ajax({
                    url: options.receiveurl,
                    type: "POST",
                    data: { patientNumber: options.patientnumber, messageID: _messageid },
                    success: function (data) {
                        if (data.ExternalClient !== null && ((data.ExternalClient.PatientNumber !== null && data.ExternalClient.PatientNumber === options.patientnumber)
                            || (data.ExternalClient.CivilServiceNumber !== null && data.ExternalClient.CivilServiceNumber === options.patientnumber))) {
                            fillFieldsFromData(data.ExternalClient);
                            fillFieldsFromData(data.ExternalContactPerson, "ContactPerson");
                            stop();
                            clearmessageid();
                            return true;
                        }
                        else {
                            _timer = setTimeout(function () { instance.collect(); }, options.polltime);
                        }
                    },
                    error: function (xhr, status, error) {
                        stop();
                        showErrorMessage(error);
                    },
                    dataType: "json",
                    global: false,
                    timeout: options.timeout
                });
            }
        }
        return false;
    };
}