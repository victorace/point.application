﻿var returnDirtyForm = function () {
    return 'U heeft op deze POINT pagina wijziging aangebracht die u nog niet heeft opgeslagen.\r\nWilt u deze pagina verlaten zonder de wijzigingen op te slaan of wilt u in dit formulier blijven (om de wijzigingen alsnog op te slaan)?';
};

function FormIsDirty(event) {
    if (typeof areyousure !== "undefined" && areyousure && typeof actOnKeyStroke === "function") {
        if (actOnKeyStroke(event)) {
            window.onbeforeunload = returnDirtyForm;
        }
    }
}

function FormIsNotDirty() {
    window.onbeforeunload = null;
}

var returnDirtyMemo = function () {
    return 'U heeft wijzigingen in het opmerkingenveld,\nmaar u heeft deze nog niet opgeslagen.\n\nWilt u verder gaan zonder op te slaan?';
};

function MemoIsDirty() {
    if (typeof areyousure !== "undefined" && areyousure) {
        if (!$.isFunction(window.onbeforeunload)) {
            window.onbeforeunload = returnDirtyMemo;
        }
    }
}

function MemoIsNotDirty() {
    if ($.isFunction(window.onbeforeunload) && window.onbeforeunload === returnDirtyMemo) {
        window.onbeforeunload = null;
    }
}

$(function () {

    if (typeof areyousure !== "undefined" && areyousure) {
        FormIsNotDirty();
    }

});