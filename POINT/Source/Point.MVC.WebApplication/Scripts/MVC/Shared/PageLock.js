﻿var idleTime = 0;
var idleTimeout = pageLockTimeoutTimer * 60000; // miniutes to be idle, before a redirect
var idleTimerInterval = 15 * 1000; // 15 sec. interval in which the idleTime will tick, to check if it should refresh

idleTimerIncrement = function () {
    idleTime = idleTime + idleTimerInterval;
    if (idleTime >= idleTimeout) {
        idleTime = 0;
        FormIsNotDirty();
        if ($(formID).data("validator") !== undefined) {
            $(formID).data("validator").cancelSubmit = true;
        }

        if (formRequestUrl) {
            AjaxSave(formID, formRequestUrl, false, null, true, null);
        }
        window.location.href = dashboardUrl;
    }
};

// Give the timer some extra time if the user is working on the screen
idleTimerReset = function () {
    //if ((idleTimeout - idleTime) <= idleTimerInterval) {
    //    idleTimeout = idleTimeout + idleTimerInterval;
    //}

    idleTime = 0;
};

$(function () {
    if (hasPageLockTimer) {
        $(window).on('beforeunload', function () {
            $.ajax({ async: false, type: 'GET', url: removePageLockFromSessionUrl });
        });
    }

    if (hasPageLockTimer && !isPageLocked) {
        if (idleTimeout > 0) {
        // set the timer for checking the idle time
        var idleInterval = setInterval("idleTimerIncrement()", idleTimerInterval);
        // reset the idle time, when there's some action of the user, only reset on the last interval.
            $(this).mousemove(function (e) { idleTimerReset(); });
            $(this).keyup(function (e) { idleTimerReset(); });
        }
    }
});