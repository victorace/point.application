﻿function ShowModal(requestUrl, callback, afterload, header) {
    $.ajax({
        cache: false,
        type: "GET",
        url: requestUrl,
        data: $(this).data(),
        async: false,
        beforeSend: function() {
            $("#modal").modal('show');
            $("#modal").on("hidden.bs.modal", function (e) {
                if (e.target.id === "modal") {
                    $("#modal .modal-body").html("");
                }
            });
        },
        success: function (data) {
            $("#modal .modal-content").data("callback", callback);
            $("#modal .modal-body").html(data);
            if (header) {
                $("#modal .modal-header").html(header);
            }
            if (afterload) {
                afterload();
            }
        },
        error: function (data) {
            $("#modal .modal-body").html(data.responseText);
        }
    });
}

function ShowModal2(requestUrl, callback, afterload, size) {
    var modal = $("#modal2").find(".modal-dialog");
    if (modal.length === 1) {
        if (size === undefined) size = 2;
        $(modal).removeClass("modal-sm").removeClass("modal-lg");
        if (size === 1) $(modal).addClass("modal-sm");
        else if (size === 2) $(modal).addClass("modal-lg");
        // else none - modal-md
    }

    $.ajax({
        type: "GET",
        url: requestUrl,
        data: $(this).data(),
        async: false,
        beforeSend: function () {
            $("#modal2").modal("show");
        },
        success: function (data) {
            $("#modal2 .modal-content").data("callback", callback);
            $("#modal2 .modal-body").html(data);
            if (afterload) {
                afterload();
            }
        },
        error: function (data) {
            $("#modal2 .modal-body").html(data.responseText);
        }
    });
}

function ShowModalDirect(message, header) {
    $("#modal .modal-body").html(message);
    if (header) {
        $("#modal .modal-header").html(header);
    }
    $("#modal").modal('show');
}

function DoCallback(item) {
    var callback = $("#modal .modal-content").data('callback');
    if (callback) {
        callback($.parseJSON(item));
    }
}

$(function() {

    $("input:text, select").filter(":visible:first").focus();

    $("#modal").on("shown.bs.modal",
        function() {
            setTimeout(function() {
                    $('input:visible:first').focus();
                },
                1);
        });
});
