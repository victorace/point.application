﻿var isie8 = $("html.ie8").length > 0;

function getHiddenHeight(item) {
    
    var parent = $(item).closest(".form-group");
    if (parent.length === 0) {
        parent = $(item).closest(".input-group");
    }
    if (parent.length === 0) {
        parent = $(item).closest("tr");
    }
    if (parent.length === 0) {
        parent = $(item).closest("div");
    }
    if (parent.length === 0) {
        return 0;
    }

    var container = $("#Scroller");
    if (container.length === 0) {
        container = $("body");
    }

    var scrollheight = 0;
    var tmpobject = $(parent[0].outerHTML).appendTo(container);
    tmpobject.removeClass("display-none");
    var originalobject = $(tmpobject).find("#" + item.id);
    if (originalobject.length > 0) {
        scrollheight = originalobject[0].scrollHeight;
    }
    tmpobject.remove();
    return scrollheight;

}

function initializeContains() {
    jQuery.expr[":"].containsid = function (a, i, m) {
        if (isie8) {
            return false;
        }
        return jQuery(a).find("#" + m[3]).size() > 0;
    };

    jQuery.expr[":"].containsname = function (a, i, m) {
        if (isie8) {
            return false;
        }
        return jQuery(a).find('*[name = "' + m[3] + '"]').size() > 0;
    };

    jQuery.expr[":"].contains = function (a, i, m) {
        return jQuery(a).text().toUpperCase().indexOf(m[3].toUpperCase()) > -1;
    };
}

function initializeHasHistory() {
    $(":input[data-hashistory='true']").on("contextmenu", function (e) {
        ShowModal(appPath + "/History/FlowFieldValues?FormSetVersionID=" + $(this).data("formsetversionid") + "&flowfieldids=" + $(this).data("flowfieldid"));
        return false;
    });

    $("label:has(> :input[data-hashistory='true'])").addClass("bg-orange color-pt-white").on("contextmenu", function (e) {
        var field = $(this).find(":input[data-hashistory='true']");
        ShowModal(appPath + "/History/FlowFieldValues?FormSetVersionID=" + field.data("formsetversionid") + "&flowfieldids=" + field.data("flowfieldid"));
        return false;
    });
}

function fixYear(date) {
    var fixeddate;
    if (window.moment && moment.isMoment(date)) {
        if (date.year() === 0) {
            fixeddate = date.year(moment().year());
        } else if (date.year() < 100 && date.year() > moment().year() - 2000) {
            fixeddate = date.year(1900 + date.year());
        } else if (date.year() < 100) {
            fixeddate = date.year(2000 + date.year());
        } else if (date.year() < 1900) {
            fixeddate = date.year(1900 + date.year() - Math.floor(date.year() / 100) * 100);
        }
    }
    return fixeddate;
}

// TODO: This is a quickfix for the date time controls. A more generic solution 
// on how we update / trigger elements is required #16218 - PP
function updateDateTimeSource(element, source) {
    if (element === undefined || $(element).length === 0) {
        return;
    }
    var name = $(element).attr("name");
    if (name) {
        update_flowfieldvalue_source(name, source);
    }
}

function initializeDateTime() {
    $(".input-group.date").each(function() {
        var minDateX = $(this).find('input').data('mindate');
        if (typeof minDateX === 'undefined') {
            minDateX = false;
        }
        $(this).datetimepicker({ locale: "nl", format: "DD-MM-YYYY", useCurrent: false, debug: isie8, minDate: minDateX })
            .on("dp.change", function(ev) {
                var newDate = fixYear(ev.date);
                if (newDate) {
                    $(ev.target).find("input.form-control").changeval(newDate.format("DD-MM-YYYY"));
                }
                $(ev.target).find("input.form-control").change();
                updateDateTimeSource($(ev.target).find("input.form-control"), 1);
            });
    });		
		
    $(".form-control.date").datetimepicker({ locale: "nl", format: "DD-MM-YYYY", useCurrent: false, debug: isie8 })
        .on("dp.change", function (ev) {
            var newDate = fixYear(ev.date);
            if (newDate) {
                $(ev.target).changeval(newDate.format("DD-MM-YYYY"));
            }
            $(ev.target).change();
            updateDateTimeSource($(ev.target), 1);
        });

    $(".input-group.datetime").datetimepicker({ locale: "nl", format: "DD-MM-YYYY HH:mm", debug: isie8 })
        .on("dp.change", function (ev) {
            var newDate = fixYear(ev.date);
            if (newDate) {
                $(ev.target).find("input.form-control").changeval(newDate.format("DD-MM-YYYY HH:mm"));
            }
            $(ev.target).find("input.form-control").change();
            updateDateTimeSource($(ev.target).find("input.form-control"), 1);
        });

    $(".form-control.datetime").datetimepicker({ locale: "nl", format: "DD-MM-YYYY HH:mm", debug: isie8 })
        .on("dp.change", function (ev) {
            var newDate = fixYear(ev.date);
            if (newDate) {
                $(ev.target).changeval(newDate.format("DD-MM-YYYY HH:mm"));
            }
            $(ev.target).change();
            updateDateTimeSource($(ev.target), 1);
        });

    $(".input-group.time").datetimepicker({ locale: "nl", format: "HH:mm", useCurrent: false, debug: isie8 })
        .on("dp.change", function (ev) {
            $(ev.target).find("input.form-control").change();
            updateDateTimeSource($(ev.target).find("input.form-control"), 1);
        });

    $(".form-control.time").datetimepicker({ locale: "nl", format: "HH:mm", useCurrent: false, debug: isie8 })
        .on("dp.change", function (ev) {
            $(ev.target).change();
            updateDateTimeSource($(ev.target), 1);
        });

    $(document).click(function () {
        $("html.ie8 .timepicker").closest(".bootstrap-datetimepicker-widget").hide();
    });
}

function initializeTextarea() {
    $('textarea').each(function () {              
        if ($(this).val() === "") {
            return;
        }
        var scrollheight = this.scrollHeight;

        if (scrollheight === 0) {
            scrollheight = getHiddenHeight(this);
            if (scrollheight === 0) {
                // Parent is flat but we have a value. Display textarea!
                var parent = $("#div" + $(this).context.id + ".ignore");
                if (parent) {
                    $(parent).removeClass("ignore");
                }
                return;
            }
        }

        this.setAttribute('style', 'height:' + scrollheight + 'px;overflow-y:hidden;');
    }).on('input', function () {
        this.style.height = 'auto';

        var scrollheight = Math.max(44, this.scrollHeight+4);
        this.style.height = scrollheight + 'px';
    });
}

function initializeNumberKeyDown() {
    $(":input[data-val-number]").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl+...
            e.ctrlKey === true ||
            // Allow: Ctrl+A, Command+A
            e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true) ||
            // Allow: home, end, left, right, down, up
            e.keyCode >= 35 && e.keyCode <= 40) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
}

function UncollapseInvalidByName(elementname) {
    if (elementname === "") {
        return;
    }
    $("form .collapse:containsname('" + elementname + "')").collapse("show");
}

function GotoElementByName(elementname) {
    if (elementname === "") {
        return;
    }
    $('form *[name="' + elementname + '"').focus();
}

function GotoElementByID(elementid) {
    if (elementid === "") {
        return;
    }
    $("#" + elementid).focus();
}

function AjaxLoad(formID, requestUrl, target, callback) {
    var form = $(formID);
    var ajaxcall = $.ajax({
        url: requestUrl,
        data: form.serialize(),
        type: "POST"
    }).done(function (data) {
        $(target).html(data);
    });

    if (callback) {
        ajaxcall.complete(callback);
    }
}

function AjaxSave(formID, requestUrl, replaceLocation, callback, nosuccesmsg, extradata, cleardirty) {

    var form = $(formID);
    var cancelSubmit = false;
    if (form.data("validator") !== undefined) {
        cancelSubmit = form.data("validator").cancelSubmit;
    }
    if (cancelSubmit || form.valid()) {



        var submitdata = form.serialize();

        var flowwebvaluessources = getflowwebvaluesource(formID);
        if (flowwebvaluessources.length !== 0) {
            var obj = {};
            obj["flowwebvaluesource"] = JSON.stringify(flowwebvaluessources);
            if (!submitdata || !submitdata.length) {
                submitdata = $.param(obj);
            } else {
                submitdata = submitdata + "&" + $.param(obj);
            }
        }

        if (extradata && jQuery.isEmptyObject(extradata) === false) {
            if (!submitdata || !submitdata.length) {
                submitdata = $.param(extradata);
            } else {
                submitdata = submitdata + "&" + $.param(extradata);
            }
        }
        
        if (SendSignal()) {
            submitdata = submitdata + "&" + $.param(GetSignaleringObject());
        }

        $.ajax({
            type: "POST",
            cache: false,
            url: requestUrl,
            data: submitdata,
            async: true,
            success: function (data) {
                if (data.ShowErrors) {
                    showErrorMessage(" - " + data.ValidationErrors.join("<br/> - "));
                }
                else if (data.Url && (replaceLocation === true || data.ForceReload === true)) {
                    if (nosuccesmsg === undefined || nosuccesmsg === false) {
                        showSuccessMessage("Succesvol opgeslagen.");
                    }

                    if (cleardirty === undefined || cleardirty === true) {
                        FormIsNotDirty();
                    }

                    location.replace(data.Url);
                }
                else {
                    if (nosuccesmsg === undefined || nosuccesmsg === false) {
                        showSuccessMessage("Succesvol opgeslagen.");
                    }

                    if (cleardirty === undefined || cleardirty === true) {
                        FormIsNotDirty();
                    }

                    if (callback) callback(data);
                }
            },
            error: function (xhr) {
                var msg;
                try {
                    var response = JSON.parse(xhr.responseText);
                    msg = response.ErrorMessage;
                }
                catch (ex) {
                    msg = xhr.statusText;
                }
                showErrorMessage(msg);
                form.valid();
                $("#phaseButtonsHolder .btn").attr("disabled", false);
            }
        });
    } else {
        $("#phaseButtonsHolder .btn").attr("disabled", false);
    }
}

function fillSelectWithOptions(source, options, firstoption) {
    $(source).find("option").remove();
    if (typeof firstoption !== "undefined" && typeof firstoption === "object" && firstoption !== null) {
        $(source).append("<option value='" + firstoption.Value + "' data-digest='" + firstoption.Digest + "'>" + firstoption.Text + "</option>");
    }
    else {
        var addselecteer = true;
        if (typeof firstoption !== "undefined" && typeof firstoption === "boolean") {
            addselecteer = firstoption;
        }
        if (addselecteer) {
            $(source).append("<option value=''>&lt;Selecteer&gt;</option>");
        }
    }

    if (typeof options !== "undefined" && options !== null && options.constructor === Array)
    {
        $.each(options, function (index, item) {
            $(source).append("<option value='" + item.Value + "' data-digest='" + item.Digest + "'>" + item.Text + "</option>");
        });
        if ($(source).find(":selected").length === 0) {
            $(source).find("option:eq(0)").prop("selected", true);
        }
    }
}

function fillMultiSelectWithOptions(source, options, selectall, firstoption) {

    $(source).empty();

    var hasoptions = typeof options !== "undefined" && options !== null && options.constructor === Array && options.length;
    if (hasoptions) {
        $.each(options, function (index, item) {
            if (item.Selected) {
                $(source).append("<option value='" + item.Value + "' selected>" + item.Text + "</option>");
            } else {
                $(source).append("<option value='" + item.Value + "'>" + item.Text + "</option>");
            }
        });
    }

    if (typeof firstoption !== "undefined" && typeof firstoption === "object" && firstoption !== null) {
        $(source).prepend($("<option />").text(firstoption.Text).changeval(firstoption.Value));
    }

    if (typeof $.fn.multiselect !== "undefined") {
        $(source).multiselect(hasoptions ? "enable" : "disable");
        $(source).multiselect("rebuild");
        if (selectall) {
            $(source).multiselect("selectAll", false);
            $(source).multiselect("updateButtonText");
        }
    }
}

function showMessage(message) {
    $("#mainDiv").prepend($("<div>").html(message).addClass("errorMessage"));
}

function GetScrollHeight() {
    var offs = $("#Scroller").offset();
    if (offs !== null) {
        return $(window).innerHeight() - $("#Scroller").offset().top - 20;
    }
    else
    {        
        return $(window).innerHeight() - 20;
    }
    
}

function GetNameByID(id) {
    return $(id).prop("name");
}

function IsChecked(control) {
    return control.is(":checked");
}

function GetRadioValueByName(name) {
    return GetSelectedRadioByName(name).val();
}

function GetRadioByName(name) {
    return $("input[name='" + name + "'][type=radio]");
}

function GetSelectedRadioByName(name) {
    return $("input[name='" + name + "'][type=radio]:checked");
}

function GetSelectedCheckboxByName(name) {
    return $("input[name='" + name + "'][type=checkbox]:checked");
}

function DisableContainer(id) {
    $(id).find("input,textarea,select,label").attr("disabled", "");
}

function EnableContainer(id) {
    $(id).find("input,textarea,select,label").removeAttr("disabled");
}

function ClearContainer(id, dirty) {

    //$(id).find("input,textarea,select,label")
    //    .removeAttr("checked")
    //    .not(":radio,:checkbox").changeval("");

    //$(id).find("input,textarea,select")
    //    .removeClassPrefix("sourcefield")
    //    .addClass("sourcefield-1");

    //$(id).find(".sourcecontainer")
    //    .removeClassPrefix("sourcefield")
    //    .addClass("sourcefield-1");

    ClearContainerByObject($(id), dirty);
}

function ClearContainerByObject(object, dirty) {

    $(object).find("input,textarea,select,label")
        .removeAttr("checked")
        .removeClassPrefix("sourcefield")
        .not(":radio,:checkbox").changeval("");

    $(object).find("input,textarea,select")
        .addClass("sourcefield-1");

    $(object).find(".sourcecontainer")
        .removeClassPrefix("sourcefield-")
        .addClass("sourcefield-1");

    if (dirty !== undefined && dirty) {
        FormIsDirty();
    }
}

function MakeRadiosDeselectableByName(name) {
    $('input[name="' + name + '"]').each(function () {
        $(this).prop("previousValue", $(this).is(":checked"));
    });

    $('input[name="' + name + '"]').click(function () {
        if ($(this).prop("previousValue") === true) {
            $(this).prop("checked", false);
            $(this).parent().removeClass("checked");
        } else {
            $('input[name="' + name + '"]').prop("previousValue", false);
        }

        $(this).prop("previousValue", $(this).is(":checked"));
    });
}

function MakeRadiosDeselectableByID(id) {
    var name = GetNameByID(id);
    MakeRadiosDeselectableByName(name);
}

function getselectvalues(object, selected) {
    var options = $(object).find("option");
    if (selected === true) {
        options = $(object).find(":selected");
    }
    var values = $.map(options, function (option) {
        if (option.value === "-1") return;
        return option.value;
    });
    return values;
}

function update_query_string(uri, key, value) {

    // Use window URL if no query string is provided
    if (!uri) { uri = window.location.href; }

    // Create a dummy element to parse the URI with
    var a = document.createElement("a"),

        // match the key, optional square bracktes, an equals sign or end of string, the optional value
        reg_ex = new RegExp(key + "((?:\\[[^\\]]*\\])?)(=|$)(.*)")

        // Setup some additional variables
        ,
        qs_len,
        key_found = false;

    // Use the JS API to parse the URI 
    a.href = uri;

    // If the URI doesn't have a query string, add it and return
    if (!a.search) {

        a.search = "?" + key + "=" + value;

        return a.href;
    }

    // Split the query string by ampersands
    var qs = a.search.replace(/^\?/, "").split(/&(?:amp;)?/);
    qs_len = qs.length;

    // Loop through each query string part
    while (qs_len > 0) {

        qs_len--;

        // Check if the current part matches our key
        if (reg_ex.test(qs[qs_len])) {

            // Replace the current value
            qs[qs_len] = qs[qs_len].replace(reg_ex, key + "$1") + "=" + value;

            key_found = true;
        }
    }

    // If we haven't replaced any occurences above, add the new parameter and value
    if (!key_found) { qs.push(key + "=" + value); }

    // Set the new query string
    a.search = "?" + qs.join("&");

    return a.href;
}

function setDoctor(data) {
    if (data !== undefined && data.Name !== undefined) {
        $("#BehandelaarNaam").changeval(data.Name);
        $("#BehandelaarAGB").changeval(data.AGB);
        $("#BehandelaarBIG").changeval(data.BIG);
        $("#BehandelaarSpecialisme").changeval(data.SpecialismID);
        $("#BehandelaarTelefoonPieper").changeval(data.PhoneNumber);
    }
}

function clearSelect(element) {
    $(element).changeval("");
    $(element).find("option").remove();
}

function isNullOrEmpty(value) {
    return value === null || value === "";
}

function getflowwebvaluesource(formid) {
    var flowwebvaluesources = $(formid).find("[class*='sourcefield']").map(function () {
        if (typeof $(this).data("flowfieldid") === "number" && typeof $(this).data("source") === "number") {
            return {
                flowfieldid: parseInt($(this).data("flowfieldid")),
                source: parseInt($(this).data("source"))
            };
        }
    }).get();
    return flowwebvaluesources;
}

function update_flowfieldvalue_source(name, source) {
    $("[class*='sourcefield']").filter("[name='" + name + "']").each(function () {
        if (typeof $(this).data("source") === "number") {
            var oldsource = parseInt($(this).data("source"), 10);
            if (oldsource !== source) {
                $(this).addClass("sourcefield-" + source).removeClass("sourcefield-" + oldsource).data("source", source);
                $(this).closest(".sourcecontainer").addClass("sourcefield-" + source).removeClass("sourcefield-" + oldsource);
            }
        }
    });
}

function get_flowfieldvalue_source(elementid) {
    var target = $("#" + elementid);
    var source = $(target).data("source");
    if (typeof source !== "number") {
        return -1;
    }
    return parseInt(source, 10);
}

function getElementIDRef(elementID) {
    if (typeof elementID === "undefined" || elementID === null || elementID.length === 0) {
        return "";
    }
    var idPrefix = "#";
    if (elementID.indexOf(idPrefix) === -1) {
        elementID = idPrefix + elementID;
    }
    return elementID;
}

function setDropdownValue(elementID, optionvalue, source) {
    if (optionvalue) {
        elementID = getElementIDRef(elementID);
        if (!elementID.length) {
            return false;
        }
        var target = $(elementID);
        if (target.length === 1) {
            var elementName = GetNameByID(elementID);
            var optionexists = $(elementID + " option").filter(function () { return $(this).val() === optionvalue.toString(); }).length === 1;
            if (optionexists) {
                target.changeval(optionvalue.toString());
                update_flowfieldvalue_source(elementName, source);
                return true;
            }
        }
    }

    return false;
}

function setRadioButtonValue(elementID, optionValue, source) {
    if (optionValue) {
        elementID = getElementIDRef(elementID);
        if (!elementID.length) {
            return false;
        }
        var target = $(elementID);
        if (target.length === 1) {
            var elementName = GetNameByID(elementID);
            var radioButtonControlElement = $("input:radio[name='" + elementName + "']").filter("[value='" + optionValue + "']");
            if (radioButtonControlElement.length) {
                radioButtonControlElement.prop('checked', true).trigger("change");
                target.changeval(optionValue);
                update_flowfieldvalue_source(elementName, source);
                return true;
            }
        }
    }

    return false;
}

function setValue(elementID, value, source) {
    elementID = getElementIDRef(elementID);
    if (!elementID.length) {
        return false;
    }
    var target = $(elementID);
    if (target.length === 1) {
        var elementName = GetNameByID(elementID);
        if (target.is(":radio") || $("input:radio[name='" + elementName + "']").length > 0) {
            return setRadioButtonValue(elementID, value, source);
        }
        else if (target.is("select")) {
            return setDropdownValue(elementID, value, source);
        }
        else {
            target.changeval(value);
            update_flowfieldvalue_source(elementName, source);
            return true;
        }
    }

    return false;
}

function showPrefillModal(prefill, hasEPDEndpoint) {

    if (typeof readonly !== "undefined" && readonly) {
        return;
    }

    if (hasEPDEndpoint) {
        var prefilled = $("[class*='sourcefield']").filter(function () {
            return $(this).attr("data-source") >= 0;
        });
        if (prefilled.length > 0) {
            // TODO: uitklapcontainer only makes sense in the VO. For the moment this is OK. - PP
            prefilled.parents(".uitklapcontainer").collapse("show");
            $("#prefillmodal").on("show.bs.modal", function () {
                $("#prefillmodal .modal-body div").hide();
                if (prefill) {
                    $("#prefillmodal .modal-body #epdprefilltext").show();
                }
                else {
                    $("#prefillmodal .modal-body #savedepdprefilltext").show();
                }
            }).modal("show");
        }
    }
    else {
        if (prefill) {
            $("#prefillmodal").on("show.bs.modal", function () {
                $("#prefillmodal .modal-body div").hide();
                $("#prefillmodal .modal-body #defaultprefilltext").show();
            }).modal("show");
        }
    }
}

$.extend({
    getUrlVars: function (url) {
        var vars = [], hash, key;
        var hashes = url.slice(url.indexOf("?") + 1).split("&");
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split("=");
            key = hash[0].toLowerCase();
            vars.push(key);
            vars[key] = hash[1];
        }
        return vars;
    },
    getUrlVar: function (url, name) {
        return $.getUrlVars(url)[name.toLowerCase()];
    }
});

$.fn.toggleDisabled = function () {
    return this.each(function () {
        var $this = $(this);
        if ($this.attr("disabled")) $this.removeAttr("disabled");
        else $this.attr("disabled", "disabled");
    });
};

$.fn.changeval = function (v) {
    return this.each(function () {
        var $this = $(this);
        if ($this.val() !== v) {
            $this.val(v);
            //.trigger("change");
        }
    });
};

$.fn.removeClassPrefix = function (prefix) {
    return this.each(function (index, element) {
        var classes = element.className.split(" ").filter(function (c) {
            return c.lastIndexOf(prefix, 0) !== 0;
        });
        element.className = $.trim(classes.join(" "));
    });
};

$(function () {

    initializeContains();
    initializeHasHistory();
    initializeDateTime();
    initializeTextarea();
    initializeNumberKeyDown();

    var formSpecificPhaseButtonsSource = $("#formSpecificPhaseButtonsSource");
    var formSpecificPhaseButtonsTarget = $("#formSpecificPhaseButtonsTarget");
    if (formSpecificPhaseButtonsSource.length && formSpecificPhaseButtonsTarget.length) {
        formSpecificPhaseButtonsTarget.html(formSpecificPhaseButtonsTarget.html() + formSpecificPhaseButtonsSource.html());
        formSpecificPhaseButtonsSource.html("");
    }

    $('.client-header[data-toggle="clickover"]').clickover({
        html: true,
        placement: "bottom",
        global_close: true,
        esc_close: true
    });

});

