﻿function SelectToggleChildren(sourceselector, targetselector, showvalues) {
    function setChildren() {
        var selectedValue = $(this).find(":selected").val();
        if ($.isArray(showvalues) === false) showvalues = [];
        var show = (showvalues.length == 0 && selectedValue != '') || $.inArray(selectedValue, showvalues) !== -1;
        if (show) {
            $(targetselector).removeClass("ignore");
        }
        else {
            $(targetselector).addClass("ignore");
            ClearContainer(targetselector);
            $(targetselector).find(".GotSetChildren").change();
        }
    }

    $(sourceselector).on("change", setChildren).addClass("GotSetChildren");
    $(sourceselector).each(setChildren);
}

function RadioToggleChildren(sourceselector, targetselector, showvalue) {

    var loaded = false;

    // Add array to keep track of elements which have already been processed
    function setChildren() {
        var isinarray = ($.inArray($(this).val(), showvalue) >= 0 && this.checked);
        var isnotempty = (showvalue == '' && $(this).val() != '');
        var show = isinarray || isnotempty;

        if (show) {
            $(targetselector).removeClass("ignore");
            if (!loaded) return;
            var children = $(targetselector + " input[checked]");
            if (children.length) {
                children.each(function () {
                    $(this).change();
                });
            }
        }
        else {
            $(targetselector).addClass("ignore");
            ClearContainer(targetselector);
            $(targetselector).find(".GotSetChildren").change();
        }
    }

    var sourceName = GetNameByID(sourceselector);
    $("[name='" + sourceName + "']input:radio").on("change", setChildren).addClass("GotSetChildren");
    $("[name='" + sourceName + "']:checked").each(setChildren);
    loaded = true;
}

function CheckboxToggleChildren(sourceselector, targetselector) {

    function setChildren() {
        $(targetselector).toggle($(this).is(":checked"));
        if ($(this).is(":checked") == false) {
            ClearContainer(targetselector);
            $(targetselector).find(".GotSetChildren").change();
        }
    }

    $(sourceselector).on("change", setChildren).addClass("GotSetChildren");
    $(sourceselector).each(setChildren);
}
