﻿$(function () {
    if (typeof formID !== "undefined") {
        var form = $(formID);

        if (form.length > 0)
        {
            form.data("signalering-items", {});
            form.data("signalering-items")["SignaleringItems"] = [];
            form.data("SendSignalQuestion", false);
            form.data("SendSignal", false);
        }

        $("#mainContainer :input[data-signalonchange='true']").on("change keyup paste", function () {
            AddToSignalering(this);
        });

        $("#modal").on("shown.bs.modal", function () {
            setTimeout(function () {
                $("#modal #btnSignalSendYes:visible:first").focus();
            }, 100);
        });
    }
});

function SendSignal()
{
    if (typeof formID !== "undefined") {
        return $(formID).data("SendSignal");
    }

    return false;
}

function HandleSignalering(sourcebutton)
{
    var url = $(sourcebutton).data("url");
    ShowHandleModal(url, $(this).data());
}

function RefreshSignaleringStatus() {
    var currentstatus = $("#signaleringstatus");
    var data = { flowinstanceid: currentstatus.data("flowinstanceid") };
    $.ajax({
        type: "POST",
        url: appPath + "/Signalering/CurrentSignaleringStatus",
        data: data,
        async: true,
        success: function (htmlresult) {
            currentstatus.html(htmlresult);
        }
    });
}

function AttachRefreshOnModal(modal) {
    $(modal).off("hide.bs.modal");
    $(modal).on("hide.bs.modal", function (e) {
        RefreshSignaleringStatus();
    });
}

function OpenDossierSignalList(flowinstanceid)
{
    ShowModal(appPath + "/Signalering/DossierSignalList?FlowInstanceID=" + flowinstanceid);
    AttachRefreshOnModal("#modal");
}

function OpenAllSignalList() {
    ShowModal(appPath + "/Signalering/AllSignalList");
    AttachRefreshOnModal("#modal");
}

function GetSignaleringObject()
{
    var obj = {};
    obj["SendSignal"] = SendSignal();
    obj["SignaleringMessage"] = GetSignaleringMessage();

    return obj;
}

function SetSendSignal() {
    var form = $(formID);
    form.data("SendSignal", true);
    form.data("SendSignalQuestion", false);

    $("#modal").modal("hide");
    $(SendingButton).trigger("click");
}

function SetSendNoSignal() {
    var form = $(formID);
    form.data("SendSignal", false);
    form.data("SendSignalQuestion", false);
    $("#modal").modal("hide");
    $(SendingButton).trigger("click");
}

function ShowHandleModal(requestUrl, data) {
    $.ajax({
        type: "POST",
        url: requestUrl,
        data: data,
        async: true,
        beforeSend: function () {
            $("#signalmodal").modal("show");
        },
        success: function (data) {
            $("#signalmodal .modal-body").html(data);
        },
        error: function (data) {
            $("#signalmodal .modal-body").html(data.responseText);
        }
    });
}

function CloseSignalering(button)
{
    $.ajax({
        type: "POST",
        url: $(button).data("url"),
        data: $(button).data(),
        async: true,
        success: function (data) {
            $(button).closest("tr").remove();
            $("#signalmodal").modal("hide");
        }
    });
}

function SaveSignalering(button)
{
    var handleform = $(button).closest("form");
    var buttondata = $(button).data();
    var submitdata = handleform.serialize();
    submitdata += "&" + $.param(buttondata);

    $.ajax({
        type: "POST",
        url: $(button).data("url"),
        data: submitdata,
        async: true,
        beforeSend: function () {
        },
        success: function (data) {
            var signaleringid = data.SignaleringID;
            var status = data.Status;
            $("#signallist tr[data-id='" + signaleringid + "']").attr("class", "status-" + status);
            $("#signalmodal").modal("hide");
        }
    });
}

var SendingButton = null;
function ShowSignaleringModal(requestUrl, data, sendingbutton) {
    $.ajax({
        type: "POST",
        url: requestUrl,
        contentType: "application/json; charset=utf-8",
        data: data,
        async: true,
        beforeSend: function () {
            $("#modal").modal("show");
        },
        success: function (data) {
            $("#modal .modal-body").html(data);
            
            SendingButton = sendingbutton;
        },
        error: function (data) {
            $("#modal .modal-body").html(data.responseText);
        }
    });
}

function AddToSignalering(source)
{
    var form = $(formID);
    var label = source.getAttribute("data-label");
    var itemmsg = "'" + label + "' is aangepast";

    var isinarray = $.inArray(itemmsg, GetSignaleringItems()["SignaleringItems"]) >= 0;
    if (!isinarray) {
        GetSignaleringItems()["SignaleringItems"].push(itemmsg);
    }

    if (ShowSignaleringPrompt() === false)
    {
        form.data("SendSignal", true);
    } else {
        form.data("SendSignalQuestion", true);
    }
}

function GetSignaleringMessage()
{
    var changedfields = GetSignaleringItems()["SignaleringItems"];
    var changedfieldscount = changedfields.length;
    if(changedfieldscount > 0)
    {
        var message = "";
        $.each(changedfields, function () {
            message += this + "\r\n";
        });

        return message;
    }

    return "Geen signalering";
}

function GetSignaleringItems()
{
    return $(formID).data("signalering-items");
}

function ShowSignaleringPrompt()
{
    return $(formID).data("signalering-prompt") === true;
}

function SignaleringModalNeeded()
{
    return $(formID).data("SendSignalQuestion") === true;
}