﻿initializevalidation = function (form) {

    /* !!! set "var formID = "#id-of-the-form";" before calling this script !!! */

    $.validator.methods.date = function (value, element) {
        return this.optional(element) || Date.isValid(value, "dd-MM-yyyy HH:mm") || Date.isValid(value, "dd-MM-yyyy");
    };

    $(form + " textarea[maxlength]").each(function () {
        $(this).bind("keyup input paste", function () {
            var limit = parseInt($(this).attr("maxlength"));
            if ($(this).val().length > limit) {
                $(this).val($(this).val().substr(0, limit));
            }
        });
    });
    $(form + " input[data-val-length-max]").each(function () {
        var length = parseInt($(this).attr("data-val-length-max"));
        $(this).prop("maxlength", length);
    });

    $.each($(form + " .form-group *[readonly]:not([data-linked])"), function () {
        $(this).closest(".form-group").removeAttr("data-val-required").find(".control-label,.form-control").removeAttr("data-val-required");
    });

    $.each($(form + " .form-group *[data-val-required]"), function () {
        var item = $(this);
        $(this).closest(".form-group").each(
            function () {
                var name = (item.prop("name") + "").replace(".", "_");
                item.addClass("required");
                $(".control-label[for='" + name + "']").addClass("required");
            }
        );
    });

    var validator = $.data($(form)[0], 'validator');
    if (validator) {
        var settngs = validator.settings;
        settngs.unhighlight = function (element) {
            $(element).filter("*[aria-required='true']").closest(".form-group").removeClass("has-error");
        };
    }

    $(form).bind("invalid-form.validate",
        function (event, validator) {
            var firstelement = null;
            var errorMessage = $.map(validator.errorList,
                function (item) {
                    //Moved highlighting here for IE
                    $(item.element).closest(".form-group").addClass("has-error");

                    if (firstelement === null) firstelement = item.element;
                    UncollapseInvalidByName($(item.element).attr('name'));
                    return "- " + item.message + "<br />";
                });

            if (firstelement !== null && $(firstelement).is(':visible')) firstelement.focus();

            if (!errorMessageIsShowing()) {
                showErrorMessage(errorMessage);
            }

            setFirstErrorTabActiveAndSetFocusIfAny(validator.errorList);
        });

    function setFirstErrorTabActiveAndSetFocusIfAny(errorList) {
        if (errorList.length === 0) {
            return;
        }

        $("#setFocusOnErrorModalClose").val(errorList[0].element.id);
        var containingTabPane = $(errorList[0].element).closest("div.tab-pane");
        if (containingTabPane !== null && containingTabPane !== 'undefined') {
            $('[data-target="#' + containingTabPane.attr("id") + '"]').tab("show");
        }
    }
};

$(document).ready(function () {
    if (typeof formID !== 'undefined') {
        var form = $(formID);
        if (form.length > 0)
        {
            initializevalidation(formID);
        }
    }
});