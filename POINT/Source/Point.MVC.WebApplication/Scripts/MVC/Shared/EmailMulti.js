﻿

var _EmailMulti = {
    _SeparatorDefault: ";",     // DEFAULT SEPARATOR
    _EmailInputTypeIsFreeText: false,
    _ErrorMessages: null,
    _EmailAddressesList: [],
    _EmailsFreeTextOriginalValue: "",
    _IsInstantiated: false,
    _TargetEmailElement: null,  // Actual Input element field

    SetupEmailAddressPopover: function(sender) {
        this.ResetDefaults();
        this.HandleErrorDisplay();
        
        if (!sender) {
            return;
        }

        var emailElementObj = $(sender).prev(this.CssSelectors.BoundField);
        if (!emailElementObj.length) {
            return;
        }

        this._TargetEmailElement = emailElementObj;

        var emailValue = this.ReplaceSeparatorInValue(emailElementObj.val()); // Set the correct separator

        $(this.GetID(this.IDs.MessageDisplay)).text('');
        this.FreeTextInputObj().val(emailValue);
        
        this.InitiateEmailAddressesList(emailValue);
        var hasInitialErrors = this.HasErrors();
        var element = "";
        if (hasInitialErrors) {
            this.UpdateEmailFreeTextDisplay(); // Default on list display
            element = "#emailTypeSelector div[data-emailtype='freetext']";
        } else {
            this.UpdateEmailListDisplay(); // Default on list display
            element = "#emailTypeSelector div[data-emailtype='list']";
        }

        this._EmailInputTypeIsFreeText = !hasInitialErrors;

        this.ChangeEmailInputType($(element), true);
        this.DefaultEnable();
        $(this.GetID(this.IDs.Modal)).show();
    },

    ResetDefaults: function () {
        this._EmailAddressesList = [];
        this._ErrorMessages = "";
    },

    GetEmailListString: function() {
        var emailsCleanNewVal = "";
        for (var i = 0; i < this._EmailAddressesList.length; i++) {
            var sep = i < this._EmailAddressesList.length - 1 ? this._SeparatorDefault : "";
            emailsCleanNewVal += this._EmailAddressesList[i] + sep;
        }
        return emailsCleanNewVal;
    },

    // Send new value to calling db-field
    SendToField: function() {
        if (!this.SetEmailAddressesNewVal()) {
            return;
        }

        if (this._TargetEmailElement !== null) {
            var newEmailValue = this.GetEmailListString();
            this._TargetEmailElement.val(newEmailValue);
        }

        this.HidePopover();
    },

    HidePopover: function() {
        $('body').click();
    },

    IsEmail: function(email) {
        var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    },

    AddNewEmail: function() {
        $("#emailList").append(this.Html.ListEditItem());
        this.EnableElement("#emailBtnSaveNew, #emailBtnCancelNew");
        this.DisableElement("#emailBtnAdd,#emailBtnRemove,#emailTypeSelector .tab, #emailList li.value-static input,#btnSendToField");
        this.InputNewEmailObj().focus();
    },
    CancelNewEmail: function () {
        this._ErrorMessages = "";

        if (this._EmailInputTypeIsFreeText) {
            this.FreeTextInputObj().val(this._EmailsFreeTextOriginalValue);
        } else {
            $("#emailList li.value-edit").remove();
            this.EnableElement("#emailList li.value-static input");
        }

        this.EnableElement("#btnSendToField");
        this.HandleErrorDisplay();
        this.DefaultEnable();
    },
    SaveNewEmail: function() {
        if (!this._EmailInputTypeIsFreeText) {
            var emailNew = this.InputNewEmailObj().val();
            if (emailNew.trim() === "") {
                this._ErrorMessages = "Vul e-mailadres";
                this.HandleErrorDisplay();
                return;
            }
        }

        if (!this.SetEmailAddressesNewVal()) {
            return;
        }

        if (this._EmailInputTypeIsFreeText) {
            this.UpdateEmailFreeTextDisplay();
        } else {
            this.UpdateEmailListDisplay();
        }

        this.CancelNewEmail();
    },
    RemoveExistingEmail: function () {
        var selected = $("#emailList li.value-static input:checked");
        if (selected.length > 0) {
            $(selected).closest("li").remove();
        }
    },

    DisableElement: function(element) {
        $(element).attr("disabled", "disabled");
    },
    EnableElement: function (element) {
        $(element).removeAttr("disabled");
    },
    ShowElement: function (element) {
        $(element).show();
    },
    HideElement: function (element) {
        $(element).hide();
    },
    EnableIfTrue: function (elements, condition) {
        if (condition) {
            this.EnableElement(elements);
        } else {
            this.DisableElement(elements);
        }
    },
    DefaultEnable: function () {
        this.EnableElement("#emailBtnAdd, #emailTypeSelector div");
        this.DisableElement("#emailBtnSaveNew, #emailBtnRemove, #emailBtnCancelNew");
    },

    CheckEnableAddRemove: function () {
        if (this._EmailInputTypeIsFreeText) {
            var condition = this._EmailsFreeTextOriginalValue !== this.FreeTextInputObj().val();
            this.EnableIfTrue("#emailBtnSaveNew, #emailBtnCancelNew", condition);
            this.EnableIfTrue("#emailTypeSelector .tab", !condition);
            this.EnableIfTrue("#btnSendToField", !condition);
        } else {
            var selected = $("#emailList li.value-static input:checked").length > 0;
            this.EnableIfTrue("#emailBtnRemove", selected);
            this.EnableIfTrue("#emailTypeSelector .tab", !selected);
        }
    },

    UpdateEmailFreeTextDisplay: function() {
        var freeText = "";

        if (this._EmailAddressesList.length > 0) {
            for (var i = 0; i < this._EmailAddressesList.length; i++) {
                var newLine = i < this._EmailAddressesList.length - 1 ? "\n" : "";
                freeText += this._EmailAddressesList[i] + newLine;
            }
        }

        this._EmailsFreeTextOriginalValue = freeText;
        this.FreeTextInputObj().val(freeText);
        this.FreeTextInputObj().focus();
    },

    UpdateEmailListDisplay: function() {
        var emailListObj = $("#emailList");
        emailListObj.children('li').remove();

        if (this._EmailAddressesList.length > 0) {
            for (var i = 0; i < this._EmailAddressesList.length; i++) {
                var email = this._EmailAddressesList[i].trim();
                if (email === "") {
                    continue;
                }
                emailListObj.append(this.Html.ListStaticItem(email));
            }
        }
    },

    InitiateEmailAddressesList: function (emailListString) {
        var emailListOriginal = this.ReplaceSeparatorInValue(emailListString).split(this._SeparatorDefault); // Set the correct separator
        this.ResetDefaults();
        var emailAddressesList = [];

        for (var i = 0; i < emailListOriginal.length; i++) {
            var email = this.GetValidEmail(emailAddressesList, emailListOriginal[i]);
            if (email === null) {
                // Invalid email
                email = emailListOriginal[i];
            }

            email = email.toLowerCase();
            if (emailAddressesList.indexOf(email) === -1) {
                emailAddressesList.push(email);
            }
        }

        this._EmailAddressesList = emailAddressesList;
        this.HandleErrorDisplay();
    },

    SetEmailAddressesNewVal: function() {
        this.ResetDefaults();
        var emailAddressesList = [];

        if (this._EmailInputTypeIsFreeText) {
            var emailsFreeText = this.FreeTextInputObj().val().trim();
            if (emailsFreeText !== "") {
                var emailList = this.ReplaceSeparatorInValue(emailsFreeText).split(this._SeparatorDefault); // Set the correct separator

                for (var i = 0; i < emailList.length; i++) {
                    var email = this.GetValidEmail(emailAddressesList, emailList[i]);
                    if (email !== null) {
                        emailAddressesList.push(email);
                    }
                }
            }
        } else {
            var mainObj = this;
            $("#emailList li.value-static, #emailList li.value-edit").each(function(ix, el) {
                var email = $(this).hasClass("value-edit") ? $(this).find("input.value-holder").val() : $(this).find("span.value-holder").text();
                email = mainObj.GetValidEmail(emailAddressesList, email);
                if (email !== null) {
                    emailAddressesList.push(email);
                }
            });
        }

        var success = !this.HasErrors();
        if (success) {
            this._EmailAddressesList = emailAddressesList;
        }

        this.HandleErrorDisplay();

        return (success);
    },

    FreeTextInputObj: function() {
        return $(this.GetID(this.IDs.FreeTextInput));
    },

    InputNewEmailObj: function() {
        return $(this.GetID(this.IDs.InputNewEmail));
    },
    
    HandleErrorDisplay: function() {
        var controlObj = this._EmailInputTypeIsFreeText ? this.FreeTextInputObj() : this.InputNewEmailObj();
        var errorContainerRef = this.GetID(this.IDs.EmailErrorContainer);
        var errorClass = "email-multi-error";
        var errorMessageRef = this.GetID(this.IDs.MessageDisplay);
        if (!this.HasErrors()) {
            this.HideElement(errorContainerRef);
            $(errorMessageRef).text();
            controlObj.removeClass(errorClass);
        } else {
            this.ShowElement(errorContainerRef);
            $(errorMessageRef).text(this._ErrorMessages);
            controlObj.addClass(errorClass);
            controlObj.focus();
        }
    },

    GetValidEmail: function(emailList, email) {
        if (email !== null && email !== "") {
            email = email.trim().toLowerCase();
            if (emailList.indexOf(email) === -1) {
                if (this.IsEmail(email)) {
                    return email;
                } else {
                    this._ErrorMessages += email + " is ongeldig; ";
                }
            } else {
                this._ErrorMessages += email + " bestaat al;";
            }
        }
        return null;
    },

    ReplaceSeparatorInValue: function(emailString) {
        return emailString.replace(/,/g, this._SeparatorDefault).replace(/:/g, this._SeparatorDefault)
            .replace(/\n/g, this._SeparatorDefault).replace(/ /g, this._SeparatorDefault).replace(/;;/g, this._SeparatorDefault);
    },

    ChangeEmailInputType: function(sender, isInit) {
        if ($(sender).is('[disabled]')) return;
        
        var currentType = this._EmailInputTypeIsFreeText ? "freetext" : "list";
        var emailType = $(sender).data('emailtype');
        var isTypeChange = currentType !== emailType;

        if (!isInit && (!isTypeChange || !this.SetEmailAddressesNewVal())) {
            return;
        }

        this._EmailInputTypeIsFreeText = emailType === 'freetext';

        var freeTextElements = this.GetID(this.IDs.EmailTextContainer);
        var listElements = this.GetID(this.IDs.EmailListContainer) + "," + this.GetClass(this.Classes.EmailListButtons);

        if (this._EmailInputTypeIsFreeText) {
            this.UpdateEmailFreeTextDisplay();
            this.ShowElement(freeTextElements);
            this.HideElement(listElements);
        } else {
            this.UpdateEmailListDisplay();
            this.ShowElement(listElements);
            this.HideElement(freeTextElements);
        }

        $("#emailTypeSelector div.tab-selected").removeClass("tab-selected").addClass("tab-not-selected");
        $(sender).removeClass("tab-not-selected").addClass("tab-selected");
    },

    HasErrors: function() {
        return (this._ErrorMessages !== "");
    },

    CssSelectors: {
        BoundField: "input.email-multi:not([readonly]):not([disabled])",
        BoundFieldBtnInvokerClass: ".email-multi-btn",
        BoundFieldBtnInvoker: function () { return _EmailMulti.CssSelectors.BoundField + "+.email-multi-btn"; }
    },

    Classes: {
        EmailListButtons: "email-list-buttons"
    },

    GetID: function (element) {
        return "#" + element;
    },

    GetClass: function (element) {
        return "." + element;
    },

    IDs: {
        EmailErrorContainer: "emailErrorContainer",
        EmailListContainer: "emailListContainer",
        EmailTextContainer: "emailTextContainer",
        FreeTextInput: "freeTextInput",
        InputNewEmail: "inputNewEmail",
        MessageDisplay: "emailMessage",
        Modal: "emailMultiModal"
    },

    Html: {
        Modal: function () { return "<div id='" + _EmailMulti.IDs.Modal + "' style='display:none;'><div class='email-multi-header'><button type='button' class='close' onclick='_EmailMulti.HidePopover();'>&times;</button><h4 class='email-multi-title'>Emailadres(en)</h4></div><div class='email-multi-body'><div id='" + _EmailMulti.IDs.EmailErrorContainer + "' class='hidden-multi'><div class='col-xs-12'><label id='" + _EmailMulti.IDs.MessageDisplay + "'> </label></div></div><div id='emailTopBar'><div id='emailTypeSelector' class='col-xs-7'><div class='tab tab-selected col-xs-6' id='tab1' data-emailtype='list' onclick='_EmailMulti.ChangeEmailInputType(this, false);' title='Lijst invoer'><span class='glyphicon glyphicon-list'></span>Lijst</div><div class='tab tab-not-selected col-xs-6' id='tab2' data-emailtype='freetext' onclick='_EmailMulti.ChangeEmailInputType(this, false);' title='Vrije tekst invoer'><span class='glyphicon glyphicon-paste'></span>Vrije tekst</div></div><div id='emailTopBarActionButtons' class='col-xs-5'><div class='col-xs-5'><button id='emailBtnRemove' class='" + _EmailMulti.Classes.EmailListButtons + "' type='button' onclick='_EmailMulti.RemoveExistingEmail()' title='Selectie verwijderen'><span class='glyphicon glyphicon-minus'></span></button><button id='emailBtnAdd' class='email-list-buttons' type='button' onclick='_EmailMulti.AddNewEmail()' title='E-mail toevoegen'><span class='glyphicon glyphicon-plus'></span></button></div><div class='col-xs-7'><button id='emailBtnSaveNew' class='email-btn pull-right' type='button' onclick='_EmailMulti.SaveNewEmail()' title='Opslaan'><span class='glyphicon glyphicon-floppy-disk'></span></button><button id='emailBtnCancelNew' class='email-btn pull-right' type='button' onclick='_EmailMulti.CancelNewEmail()' title='annuleren'><span class='glyphicon glyphicon-remove'></span></button></div></div></div><div id='emailContainer'><div id='" + _EmailMulti.IDs.EmailListContainer + "'><div class='col-xs-12'><ul id='emailList'></ul></div></div><div id='" + _EmailMulti.IDs.EmailTextContainer + "' class='hidden-multi' ><div class='col-xs-12'><textarea id='" + _EmailMulti.IDs.FreeTextInput + "' class='form-control' value='' maxlength='4000' onkeyup='_EmailMulti.CheckEnableAddRemove()'> </textarea></div></div></div></div><div class='email-multi-footer'><button id='btnSendToField' type='button' class='btn btn-primary pull-right' onclick='_EmailMulti.SendToField();' title='Wijzigingen overnemen'>Ok<span class='glyphicon glyphicon-ok pull-left'></span></button><button type='button' class='btn btn-default pull-right' onclick='_EmailMulti.HidePopover();' title='Wijzigingen negeren'>Annuleren</button></div></div>"; },
        ModalInvoker: function(isInInputGroup) {
            var classNotInputGroup = !isInInputGroup ? "not-input-group" : "";
            return "<span class='" + classNotInputGroup + " input-group-addon email-multi-btn'><span class='glyphicon glyphicon-envelope'></span></span>";
        },
        ListStaticItem: function(email) { return "<li class='value-static'><label><input type='checkbox' value='false' onchange='_EmailMulti.CheckEnableAddRemove()' ><span class='value-holder'>" + email + "</span></label></li>";},
        ListEditItem: function() {return "<li class='value-edit'><label><input type='checkbox' value='false' disabled style='display:none;'><input id='" + _EmailMulti.IDs.InputNewEmail  +"' type='text' class='value-holder' placeholder='Nieuwe e-mail adres' style='width:90%' ></input></label></li>";}
    },

    Setup: function () {
        if (this._IsInstantiated) {
            return;
        }
        this._IsInstantiated = true;
    }
};


$(document).ready(function () {
    if (typeof _EmailMulti !== 'undefined') {
        var mainContainer = $("#mainContainer");        // Point's Main content container

        if (mainContainer.length > 0) {
            _EmailMulti.Setup();
            $(mainContainer).append($(_EmailMulti.Html.Modal()));

            $(_EmailMulti.CssSelectors.BoundField).each(function(e, el) {
                var isInInputGroup = $(el).closest(".input-group").length > 0;
                $(el).after($(_EmailMulti.Html.ModalInvoker(isInInputGroup)));
                $(el).attr("autocomplete", "off");

                $(el).on('click keyup',
                    function (e) {
                        e.stopPropagation();
                        $(this).next(_EmailMulti.CssSelectors.BoundFieldBtnInvoker()).click();
                        return false;
                    });
            });

            $(_EmailMulti.CssSelectors.BoundFieldBtnInvoker()).clickover({
                html: true,
                placement: "left",
                global_close: true,
                esc_close: true,
                width: 380,
                height: 250,
                template: '<div class="popover"><div class="popover-content"></div></div>',
                content: _EmailMulti.Html.Modal()
            });

            $(_EmailMulti.CssSelectors.BoundFieldBtnInvoker()).on('shown.bs.popover',
                function() {
                    _EmailMulti.SetupEmailAddressPopover(this);
                });
        }
    }
});
