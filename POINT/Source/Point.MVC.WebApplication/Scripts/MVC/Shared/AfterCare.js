﻿$(function() {
    var loadAfterCareTypes = function (url, target) {
        $.getJSON(url, function (data) {
            var options = "";
            if (data.length > 0) {
                $(target).removeAttr("readonly");
                for (var x = 0; x < data.length; x++) {
                    options += "<option value='" + data[x]["Value"] + "'>" + data[x]["Text"] + "</option>";
                }
            } else {
                options += "<option value=''></option>";
                $(target).attr("readonly", "readonly");
            }
            $(target).html(options);
            $(target).trigger("change");
        });
    };

    var afterCareCategory = $("input.AfterCareCategory[type='radio']");
    if (!afterCareCategory.is(":disabled")) { // bind the click if it hasn't any disabled items
        afterCareCategory.on("click", function () {
            var selectedcategory = parseInt($(this).val());
            var url = $(this).data("aftercaretypeurl") + "&afterCareCategoryID=" + selectedcategory;
            var target = $(this).data("target");
            loadAfterCareTypes(url, target);
        });
    }
});