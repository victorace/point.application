﻿var target = '#OrganizationSearchData';

var inittable = initfix;

$(function () {
    $("#OrganizationSearchData").closest(".modal").on('shown.bs.modal', function () {
        //for some odd reason it does work with setTimeout even at 0ms..
        setTimeout(function () {
            $("#OrganizationSearchData").closest(".modal").find(':input[type=text]:enabled:visible:not([readonly]):first').focus();
        }, 0);
    });
});

function initfix() {
    if (!String.prototype.startsWith) {
        String.prototype.startsWith = function(searchString, position) {
            if (typeof searchString === "undefined") {
                return false;
            }

            position = position || 0;
            return this.substr(position, searchString.length) === searchString;
        };
    }

    if (!String.prototype.endsWith) {
        String.prototype.endsWith = function(searchString, position) {
            if (typeof searchString === "undefined") {
                return false;
            }

            var subjectString = this.toString();
            if (typeof position !== 'number' ||
                !isFinite(position) ||
                Math.floor(position) !== position ||
                position > subjectString.length) {
                position = subjectString.length;
            }
            position -= searchString.length;
            var lastIndex = subjectString.indexOf(searchString, position);
            return lastIndex !== -1 && lastIndex === position;
        };
    }

    $("#OrganizationSearchData").scrollTop(0);

    var currentsort = $("#SortBy").val();

    $(".gridTableHeader th").removeClass("sorting sorting_asc sorting_desc").addClass("sorting");
    $(".gridTableHeader th").each(function() {
        var th = $(this);
        var sortcolumn = $(this).data("sortby");
        
        if (typeof currentsort !== "undefined" && currentsort) {
            if (currentsort.startsWith(sortcolumn) && currentsort.endsWith("asc")) {
                th.removeClass("sorting").addClass("sorting_asc");
            } else if (currentsort.startsWith(sortcolumn) && currentsort.endsWith("desc")) {
                th.removeClass("sorting").addClass("sorting_desc");
            }
        }
    });

    $(".gridTableHeader th")
        .off() // unbind all events first
        .on("click",
            function() {
                var sortcolumn = $(this).data("sortby");
                var currentsort = $("#SortBy").val();
                var direction = "asc";

                if (typeof currentsort !== "undefined" && currentsort) {
                    if (currentsort.startsWith(sortcolumn) && currentsort.endsWith(direction)) {
                        direction = "desc";
                    }
                }

                $("#SortBy").val(sortcolumn + " " + direction);
                SearchForOrganization();
        });

    $(".searchtable-data").off().on("scroll", function () {
        var scrollleft = $(".searchtable-data").scrollLeft();
        $(".searchtable-header").scrollLeft(scrollleft);
    });
}

var lastid = null;
function searchlive()
{
    if (lastid !== null)
        clearTimeout(lastid);

    $("#filterPostalCode").toggle($("#AfterCareCategory").val() === '' || $("#AfterCareCategory").val() === 'TZ');
    $("#filterPostalCodeRange").toggle($("#AfterCareCategory").val() === '' || $("#AfterCareCategory").val() === 'IM' || $("#AfterCareCategory").val() === 'HO');

    lastid = setTimeout(SearchForOrganization, 300);
}

function switchpostalcode()
{
    var pc = $("input[name='PostalCodeType']:checked");    
    if (pc !== null)
    {
        $("#Postcode").val(pc.val());
        $("#PostalCodeRange").val(pc.val());

        searchlive();
    }        
}

function showAll()
{
    $("#ShowAll").val(true);
    SearchForOrganization();
}

function SearchForOrganization() {

    $("#showallbutton").hide();

    var form = $('#OrganizationSearchForm');

    if (form.valid()) {
        $.ajaxSetup({
            global: false
        });

        AjaxLoad("#OrganizationSearchForm", form.data("url"), target, inittable);

        $.ajaxSetup({
            global: true
        });
    } else {
        $(target).find("tbody").empty();
    }
}

function visiblepermanenttemporarypostalcode() {
    $("#filterpermanenttemporarypostalcode").show();
}

function invisiblepermanenttemporarypostalcode() {
    $("#filterpermanenttemporarypostalcode").hide();
}


function disablepostcode() {    
    $("#Postcode").attr("disabled", true);   
}

function enablepostcode() {
    $("#Postcode").closest("form").validate();
    $("#Postcode").rules(
       "add",
       {
           minlength: 4,
           required: true,
           messages: { required: "Formaat: 1234AB", minlength: "Formaat: 1234AB" }
       }
   );
    $("#Postcode").removeAttr("disabled");
}

function disablerange() {
    $("#PostalCodeRange, #MaxRange").attr("disabled", true);
}

function enablerange() {
    $("#PostalCodeRange").closest("form").validate();
    $("#PostalCodeRange").rules(
        "add",
        {
            minlength: 4,
            required: true,
            messages: { required: "Formaat: 1234AB", minlength: "Formaat: 1234AB" }
        }
    );
    $("#MaxRange").rules("add", { required: true });
    $("#PostalCodeRange, #MaxRange").removeAttr("disabled");
}

function enablemin3()
{
    $("#NaamPlaats").closest("form").validate();
    $("#NaamPlaats").rules(
        "add",
        {
            minlength: $("#NaamPlaats").data("minlength"),
            required: $("#NaamPlaats").data("minlength") === "0" ? false : true,
            messages: { required: "Voor landelijk zoeken minimaal " + $("#NaamPlaats").data("minlength") + " tekens", minlength: "Voor landelijk zoeken minimaal " + $("#NaamPlaats").data("minlength") + " tekens" }
        }
    );
}

function disablemin3() {
    $("#NaamPlaats").closest("form").validate();
    $("#NaamPlaats").rules("remove");
}