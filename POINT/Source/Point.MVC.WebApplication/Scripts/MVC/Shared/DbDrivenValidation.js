﻿
// Find element by the attribute: field attribute id
function findByFieldAttributeId(fieldAttribute) {
    return $("[data-dbval-field-attribute-id=" + fieldAttribute + "]");
}

// Input: "14-09-2017"
// Output: date object
function stringToDate(value) {
    var ymd = value.split("-");
    return new Date(ymd[2], ymd[1] - 1, ymd[0]);
}

// DB DRIVEN VALIDATION FUNCTIONS:
// RETURN: 0: valid, 1+: invalid.
var dbDrivenValidationFunctions = {

    // --------------------------------------
    // VALIDATION FUNCTION:
    DateDiffMax60days: function(fieldAttributes) {
        var array = fieldAttributes.split("|");
        if (array.length !== 2) {
            return 0; // abort validation
        }

        var dateArray = jQuery.map(array,
            function(fieldAttribute) {
                var value = findByFieldAttributeId(fieldAttribute).val();
                return stringToDate(value);
            });

        var diffInDays = Math.floor((dateArray[1] - dateArray[0]) / 1000 / 60 / 60 / 24);
        if (diffInDays > 60) {
            return 1; // error result
        }
        if (diffInDays < 0) {
            return 2; // error result
        }
        return 0; // valid
    }
};

// --------------------------------------------------------------

function makeNewMessageElement(messageClass, message) {
    return $(
            '<div class="form-group color-pt-red">' +
            '  <label class="col-xs-4"></label>' +
            '  <div class="col-xs-8">' +
            message +
            '  </div>' +
            '</div>')
        .addClass(messageClass); // eg: dbval-message-149-73
}

// --------------------------------------------------------------
$(document).ready(function () {

    // FOR EACH ELEMENT WITH A data-dbval-method:
    $("[data-dbval-method]").each(function() {
        var $element = $(this);

        // data-dbval-.. ATTRIBUTES:
        var method = $element.data("dbval-method"); // eg: DateDiffMax60days
        var fieldAttributes = $element.data("dbval-field-attributes"); // eg: 149|73
        //var fieldAttributeId = $element.data("dbval-field-attribute-id"); // eg: 149
        var fieldAttributeIndexWithinValidation = $element.data("dbval-field-attribute-index-within-validation"); // eg: 0
        var warningTriggers = $element.data("dbval-warning-triggers"); // eg: Load|Blur
        var messages = $element.data("dbval-messages"); // eg: message1|message2

        // --------------------------------------
        function callDbDrivenValidationFunction() {

            // DELETE EXISTING MESSAGES, IF ANY:
            var messageClass = "dbval-message-" + fieldAttributes.replace("|", "-"); // eg: dbval-message-149-73
            $("." + messageClass).remove();

            // CALL THE METHOD:
            var errorResult = dbDrivenValidationFunctions[method](fieldAttributes);

            // IF ERROR, INSERT ERROR MESSAGE:
            if (errorResult > 0) {

                var messageArray = messages.split("|");
                if (errorResult <= messageArray.length) {

                    var message = "LET OP: " + messageArray[errorResult - 1];
                    jQuery.each(fieldAttributes.split("|"),
                        function (index, value) {
                            var newMessageElement = makeNewMessageElement(messageClass, message);
                            $("[data-dbval-field-attribute-id=" + value + "]")
                                .parent().parent().parent(".form-group")
                                .after(newMessageElement);
                        });
                }
            }
        }

        // --------------------------------------
        // ON LOAD: register only for the FIRST of the field attributes (ie. when page loaded)
        if (fieldAttributeIndexWithinValidation === 0) {
            jQuery.each(warningTriggers.split("|"),
                function(index, value) {
                    var trigger = value.trim();
                    if (trigger === "Load") {
                        callDbDrivenValidationFunction(); // call directly here on document ready
                    }
                });
        }

        // ON BLUR: register for ALL field attributes (ie. if any changes)
        jQuery.each(warningTriggers.split("|"),
            function(index, value) {
                var trigger = value.trim();
                if (trigger === "Blur") {
                    $element.blur(function() {
                        callDbDrivenValidationFunction(); // call on element blur
                    });
                }
            });
    });
});
