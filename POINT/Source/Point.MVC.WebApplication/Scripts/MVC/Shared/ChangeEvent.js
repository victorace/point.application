﻿var ctrlDown = false;
var ctrlKey = 17, tabKey = 9, cKey = 67;

$(document).keydown(function (e) {
    if (e.keyCode === ctrlKey) {
        ctrlDown = true;
    }
}).keyup(function (e) {
    if (e.keyCode === ctrlKey) {
        ctrlDown = false;
    }
});

function actOnKeyStroke(event) {
    if (event) {
        if (event.keyCode === ctrlKey || ctrlDown && event.keyCode === cKey || event.keyCode === tabKey) {
            return false;
        }
    }
    return true;
}

function isAreYouSureLoaded() {
    return typeof areyousure !== "undefined" && areyousure;
}

$(function () {

    $("#mainContainer :input").on("change keyup paste", function (event) {

        if ($(this).hasClass("busy") === false) {
            var name = $(this).attr("name");
            update_flowfieldvalue_source(name, 1);
        }

        if (isAreYouSureLoaded()) {
            FormIsDirty(event);
        }

    });

    if (isAreYouSureLoaded()) {
        FormIsNotDirty();
    }

});