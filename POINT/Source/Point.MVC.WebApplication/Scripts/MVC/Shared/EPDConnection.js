﻿function epd_connection(formid, url, transferid, formtypeid, callback) {

    var first = $("#ValuesRetrievedFromWebservice").val() === "";

    $.ajax({
        type: "POST",
        url: url,
        data: {
            TransferID: transferid,
            FormTypeID: formtypeid
        },
        success: function (data) {
            if (!data.Error) {
                $(formid).find("[class*='sourcefield']").addClass("busy");

                var elements = [];
                for (var dataitem in data) {
                    elements.push([dataitem, $(formid).find("*").index($("#" + dataitem))]);
                }
                elements.sort(function (a, b) { return a[1] > b[1] ? 1 : -1; });

                var payload = {};
                for (var itemindex in elements) {
                    var item = elements[itemindex];
                    if (data.hasOwnProperty(item[0])) {
                        payload[item[0]] = data[item[0]];
                    }
                }

                if (typeof callback === "function") {
                    callback(first, payload, parseInt(data["source"]));
                }

                $("#ValuesRetrievedFromWebservice").val("1");

                initializeTextarea();

                $(formid).find("[class*='sourcefield']").removeClass("busy");

                showSuccessMessage("Update met gegevens uit EPD/EVD gereed.");
            }
        }
    });
}

function check_epd_mapping(payload) {
    var errors = [];
    for (var item in payload) {
        var value = payload[item];

        var erroritem = {};
        erroritem[item] = value;

        if (payload.hasOwnProperty(item)) {
            var target = $("#" + item);
            var elementName = GetNameByID("#" + item);
            if (target.length === 0) {
                errors.push(erroritem);
            }
            else if (target.is(":radio") || $("input:radio[name='" + elementName + "']").length > 0) {
                if ($("input:radio[name='" + elementName + "']").filter("[value='" + value + "']").length === 0) {
                    errors.push(erroritem);
                }
            }
            else if (target.is("select")) {
                if ($("#" + elementID + " option").filter(function () { return $(this).val() === value.toString(); }).length === 0) {
                    errors.push(erroritem);
                }
            }
        }
    }
    return errors;
}