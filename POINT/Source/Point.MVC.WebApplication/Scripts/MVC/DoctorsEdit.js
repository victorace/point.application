﻿
var formID = '#DoctorEdit';
var modalRef = "#modal";


function closeModal() {
    $(modalRef).modal("hide");
    changeOrganization();
}

// *** MANUAL INSERT ***

function SaveForm(sender) {
    if ($(formID).valid()) {
        var url = $(sender).data("saveurl");
        AjaxSave($(sender).closest("form"),
            url,
            false,
            function () {
                closeModal();
            },
            true);
    }
}

// *** IMPORT FROM CSV ***
var fileUploadRef = "#FileUpload1";
var panelDoctorsImportRef = "#panelDoctorsImport";
var panelDoctorsEditRef = "#panelDoctorsEdit";
var doctorsImportTableRef = "#doctorsImportTable";
var organizationIDRef = "#OrganizationID";
var btnErrorsImportRef = "#btnErrorsImport";
var importButtonGroupRef = "#importButtonGroup";
var errorsImportRef = "#errorsImport";
var actionMessageRef = "#ActionMessage";
var startDoctorsImportRef = "#startDoctorsImport";
var doctorsImportWrapperRef = "#doctorsImportWrapper";

var messageClassError = "error";
var messageClassWarning = "warning";
var messageClassSuccess = "success";

function displayError(message) {
    displayMessage(message, messageClassError);
}

function displaySuccess(message) {
    displayMessage(message, messageClassSuccess);
}

function displayWarning(message) {
    displayMessage(message, messageClassWarning);
}

function displayMessage(message, messageClass) {
    var objRef = $(actionMessageRef);
    objRef.text(message);
    objRef.removeClass();

    if (message.length) {
        objRef.addClass("actionmessage-" + messageClass);
    } else {
        objRef.addClass("hidden");
    }
}

function checkEnableStartImport() {
    var objRef = $(startDoctorsImportRef);

    if ($(doctorsImportTableRef + " tr>.selected>.checkbox-inline:checked").length > 0) {
        objRef.removeClass("disabled");
    } else {
        objRef.addClass("disabled");
    }
}

function clearImportRows() {
    $(doctorsImportTableRef).find("tr:gt(0)").remove();
    $(fileUploadRef).val('');
    showImportDoctorsPanel(true);
}

function switchPanel(panelIdToShow, setHeight, setImportHeight) {
    if (!setImportHeight) {
        displayError('');
    }
    
    var heightEdit = -1;
    if (setHeight) {
        $(doctorsImportWrapperRef).css('max-height', $(window).height() * 0.7);

        var heightImport = $(panelDoctorsImportRef).height();
        heightEdit = $(panelDoctorsEditRef).height();

        if (setImportHeight && heightImport > heightEdit) {
            setHeight = false;
        }
    } else {
        clearImportRows();
    }
    
    if (setImportHeight) {
        $(btnErrorsImportRef).addClass("hidden");
        $(errorsImportRef).html();
    }

    $(".panelDoctors").hide();

    if (setHeight) {
        $(panelDoctorsImportRef).height(heightEdit);
        $(importButtonGroupRef).css('position', 'absolute');;
    } else {
        $(importButtonGroupRef).css('position', 'static');;
    }

    $(panelIdToShow).show();
}

function showImportDoctorsPanel(setImportHeight) {

    switchPanel(panelDoctorsImportRef, true, setImportHeight);
}

function showEditDoctorsPanel() {

    switchPanel(panelDoctorsEditRef, false, false);
}

function bindEvents() {
    $(fileUploadRef).on('change', uploadCsv);
    $(doctorsImportTableRef + " .checkbox-inline").on('click', checkEnableStartImport);
    $(doctorsImportTableRef + " td," + doctorsImportTableRef + " td li").on('click', function (e, obj) {
        displayRowErrors(this);
        e.stopPropagation();
    });
}

// UPLOAD CSV FOR DOCTOR LIST EXTRACTION

function displayCsvInfo() {
    showWarningMessage($("#CsvInfoExample").html(), "Csv voorbeeld");
}

function selectCsv() {
    $(fileUploadRef).click();
}

function displayErrorsFromCsv() {
    var errorsImport = $(errorsImportRef).html();
    if (errorsImport.trim() !== '') {
        $(btnErrorsImportRef).removeClass("hidden");
        showErrorMessage(errorsImport);
    } else {
        $(btnErrorsImportRef).addClass("hidden");
    }
}

function replaceDataDoctors(data) {

    displayError(data.errorMessage);

    if (data.errorMessage.length) {
        clearImportRows();
        return;
    }

    $.each(data.htmlReplaces || [],
        function (id, html) {
            if ($(id).length) {
                $(id).replaceWith($(html));
            };
        });

    checkEnableStartImport();
    showImportDoctorsPanel(true);
    bindEvents();
    displayErrorsFromCsv();
}

function ajaxRequestGetDoctorsFromCsv(inputData, url) {

    if (!inputData) {
        return;
    };

    $.ajax({
        type: "POST",
        url: url,
        data: inputData,
        contentType: false,
        processData: false,
        cache: false,
        success: function (data) { replaceDataDoctors(data); },
        error: function(xhr, status, error) {
            displayError('ajax ERROR!');
            showErrorMessage(error);
        }
    });
}

function uploadCsv() {

    if (window.FormData !== undefined) {
        var fileUpload = $(fileUploadRef).get(0);
        var files = fileUpload.files;
        var fileData = new FormData();
        var fileName = "";

        if (files.length > 0) {
            var file = files[0];
            fileData.append(file.name, file);
            fileName = file.name;
        }

        if (fileName === "") {
            // no selection
            return;
        }

        var ext = fileName.split('.').pop().toLowerCase();

        if (ext !== 'csv') {
            displayError('invalid extension "' + ext + '"');
            return;
        }

        fileData.append('organizationID', $(organizationIDRef).val());

        ajaxRequestGetDoctorsFromCsv(fileData, readDoctorsFromCsvUrl);

    } else {
        displayError("FormData is not supported.");
    }
};

function displayRowErrors(obj) {
    if (obj == null) {
        return;
    }

    if ($(obj).hasClass('error')) {
        displayError($(obj).attr('title'));
    } else {
        displayError('');
    }
}



// IMPORT SELECTED ITEMS IN DB

function ajaxRequestInsertDoctors(inputData, url) {

    if (!inputData) {
        return;
    };

    $.ajax({
        type: "POST",
        url: url,
        data: inputData,
        contentType: "application/json",
        success: function (data) { responseInsert(data); },
        error: function (xhr, status, error) { displayError('ajax ERROR!'); showErrorMessage(error); }
    });
}

function responseInsert(data) {
    displayError(data.errorMessage);

    if (data.errorMessage.length) {
        return;
    }

    if (data.successMessage.length) {
        displaySuccess(data.successMessage);

        setTimeout(function () {
            closeModal();;
        }, 3000);
    }
}

function getDoctorsJsonData() {

    var doctors = [];

    $(doctorsImportTableRef + " tr").each(function (i, obj) {
        if ($(obj).find(".selected .checkbox-inline").is(":checked")) {
            var locationIDs = $(obj).find(".LocationIDS").text();

            var doctor = {
                AGB: $(obj).find(".AGB").text(),
                Name: $(obj).find(".Name").text(),
                SearchName: $(obj).find(".SearchName").text(),
                PhoneNumber: $(obj).find(".PhoneNumber").text(),
                LocationIDS: (locationIDs.length ? locationIDs.split(",") : null),
                BIG: $(obj).find(".BIG").text(),
                OrganizationID: $(organizationIDRef).val(),
                SpecialismID: $(obj).find(".SpecialismID").text()
            };

            doctors.push(doctor);
        }
    });

    if (doctors.length === 0) {
        return null;
    }

    return JSON.stringify({
        Doctors: doctors
    });
}

function importDoctors(obj) {
    if ($(obj).hasClass('disabled')) {
        return;
    }
    var doctors = getDoctorsJsonData();

    if (doctors == null) {
        displayWarning("Geen artsen geselecteerd");
        return;
    }

    ajaxRequestInsertDoctors(doctors, importDoctorsUrl);
}

// *** INIT ***
$(document).ready(function () {

    initializevalidation(formID);

    $.validator.setDefaults({ ignore: '.ignore *' });
    $.validator.unobtrusive.parse($(formID));

    doctorsImportViewInit();
    bindEvents();

    function doctorsImportViewInit() {
        $(doctorsImportTableRef).DataTable({
            "paging": false,
            "searching": false
        });
    }

});