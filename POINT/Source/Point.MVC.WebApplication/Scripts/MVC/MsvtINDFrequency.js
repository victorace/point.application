﻿$(function () {
    //Ignore validation on all elements inside a container with ignore as classname
    $.validator.setDefaults({ ignore: '.ignore *' });

    $('[data-toggle="clickover"]').clickover({
        html: true,
        placement: $(this).attr("data-placement") ? $(this).data("placement") : "top",
        global_close: true,
        esc_close: true,
        template: '<div class="popover frequencytoelichting"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>',
        content: $(this).data("content")
    });

    $("#btnIndicatiestelling").on("click", function () {
        var url = $(this).attr("data-url");
        location.replace(url);
    });

    $("#IndicatieVersion").on("change", "", "", function (item) {
        var url = $(this).find("option:selected").attr("data-url");
        FormIsNotDirty();
        location.replace(url);
    });

    $.each($("#MsvtINDFrequency[data-disabled='disabled'] :input:not([data-ignoredisabled='true'])"), function () {
        $(this).attr("disabled", "");
    });
});

function reopendoorlopenddossier(sender) {
    $.ajax({
        type: "GET",
        url: $(sender).attr("data-url"),
        success: function (data) { location.reload(); }
    });
}