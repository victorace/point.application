﻿function loadPhaseDefinitionsTabData() {
    var phaseDefinitions = $("#phaseDefinitions");

    if (phaseDefinitions.children().length > 0) return;

    $.ajax({
        type: "GET",
        url: overviewFlowInstanceUrl,
        success: function(data) {
            phaseDefinitions.html(data);
        },
        error: function(jqxhr, textStatus, errorThrown) {
            showErrorMessage($.parseJSON(jqxhr.responseText).ErrorMessage);
        }
    });
}

function loaddossierTagsTabData() {
    var dossierTags = $("#dossierTagsContainer");
    if (dossierTags.children().length > 0) return;

    $.ajax({
        type: "GET",
        url: dosserTagsUrl,
        success: function (data) {
            dossierTags.html(data);
        },
        error: function (jqxhr, textStatus, errorThrown) {
            showErrorMessage($.parseJSON(jqxhr.responseText).ErrorMessage);
        }
    });
}


function GetClientPreviousTransfers() {
    var transferid = getQueryStringParameter("TransferID");
    var url = appPath + "/Transfer/RelatedTransferVVTVVTTransfer?TransferID=" + transferid;
    $.ajax({
        type: "GET",
        url: url,
        success: function (data) {
            if (data.URL !== '') {
                ShowModal(appPath + data.URL);
            }
        },
        error: function (jqxhr, textStatus, errorThrown) {
            showErrorMessage($.parseJSON(jqxhr.responseText).ErrorMessage);
        }
    });
}

function DoNotTakeOverTransfer()
{
    //if user doesn't choose to take over data, set RelatedTransferID to -1. 
    //Then related transfers will not be shown next time that dashboard is opened.
    SaveRelatedTransferID(-1);    
}

function SaveRelatedTransferID(relatedTransferId)
{
    var transferid = getQueryStringParameter("TransferID");
    var url = appPath + "/Transfer/SaveRelatedTransfer?TransferID=" + transferid + "&RelatedTransferID=" + relatedTransferId;
    $.ajax({
        type: "GET",
        url: url,
        success: function (data) {
            $("#modal").modal('hide');
        },
        error: function (jqxhr, textStatus, errorThrown) {
            showErrorMessage($.parseJSON(jqxhr.responseText).ErrorMessage);
        }
    });
}

function GetTransferForms(data) {
    var jsondata = $.parseJSON(data);
    var selectedtransfer = jsondata.TransferID;
    SaveRelatedTransferID(selectedtransfer);
}

$(function () {
	if ($("#forms").length === 0) {
        loadPhaseDefinitionsTabData();
    }
    GetClientPreviousTransfers();
});