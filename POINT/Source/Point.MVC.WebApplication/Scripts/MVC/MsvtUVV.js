﻿$.validator.setDefaults({ ignore: '.ignore *' });

function CheckFormSetName(sender) {
    var formsetname = $(sender).val();
    var dropdownnames = $("#PhaseInstanceID option").map(function () { return this.innerText; }).get();

    var hasformsetversion = formSetVersionID > 0;
    var valid = $.inArray(formsetname, dropdownnames) === -1 || hasformsetversion;

    if (valid) {
        $(sender).css("background-color", "white");
        $("#ValidFormSetName").val("true");
    } else {
        $(sender).css("background-color", "#ffd0d0");
        $("#ValidFormSetName").removeAttr("value");
    }
}

function ClearMsvtItemRow(sender)
{
    ClearContainerByObject($(sender).closest(".msvt-item-row"));
}

function SearchOrganization(sender, url, callback) {
    ShowModal(url, callback, inittable)
}

var PutOrganisation = function (organization) {
    $('input[data-linked="MSVTLeverendeOrganisatie"]:not([type=hidden])').val(organization.DepartmentName);
    $('input[data-linked="MSVTLeverendeOrganisatie"][type=hidden]').val(organization.DepartmentID);
    $("#modal").modal('hide');

    $("#leverendemedewerkerdetails").toggle(vvtDepartmentID == organization.DepartmentID);
};

function ResizeIndicaties() {
    var scrollerwidth = $("#Scroller").width();
    $("#uitvoeringsverzoekactions .resizetomax").each(
        function () {
            $(this).css("max-width", scrollerwidth - 400 + "px");
        }
    );
}

function SetRequired(row) {
    row.find(".partly-required input").each(
        function () {
            $(this).rules(
                "add",
                {
                    required: true,
                    messages: { required: $(this).attr("data-label") + " is verplicht" }
                }
            )
        }
    );
}

function UnSetRequired(row) {
    row.removeClass("has-error");
    row.find(".partly-required input").each(
        function () {

            $(this).rules("remove");
        }
    );
}

$(function () {
    //Scrollable auto-resize
    $(window).on("resize", function () {
        var height = GetScrollHeight();
        $("#Scroller").css("max-height", height);
        ResizeIndicaties();
    });
    $(window).trigger("resize");

    $("#btnRenew").on("click", function () {
        var data = $(this).data();
        var url = formRequestUrl;
        AjaxSave(formID, url, true, null, false, data);
    });

    $("#BehandelendArtsID").on("change", function () {
        var artsID = $(this).val();
        var digest = $(this).find(":selected").data("digest");
        $.ajax({
            type: "POST",
            url: getDoctorByID,
            data: { doctorID: artsID, digest: digest },
            success: function (data) {
                setDoctor(data);
            },
            error: function (data) {
                showErrorMessage(data);
            }
        });
    });

    $("#nieuwexemplaar").on("click", function () {
        var url = $(this).attr("data-url");
        location.replace(url);
    });

    $("#PhaseInstanceID").on("change", "", "", function (item) {
        var url = $(this).find("option:selected").attr("data-url");
        FormIsNotDirty();
        location.replace(url);
    });

    $("#uitvoeringsverzoekactions .msvt-item-row").each(function ()
    {
        var firstradio = $(this).find("input[name$='freqencyunit']");
        var name = firstradio.first().attr("name");
        MakeRadiosDeselectableByName(name);

        if ($(this).find("input[type=radio]:checked, input[type=text]:filled").length > 0)
            SetRequired($(this));
    });

    $("#uitvoeringsverzoekactions .collapse input, #Scroller .collapse span").on("change keyup click", "", function ()
    {
        var isfilled = this.checked || ($(this).is("[type=text]:filled"));

        if (isfilled) {
            $(this).closest("li").addClass("active");
            SetRequired($(this).closest(".msvt-item-row"));
        }
        else {
            if ($(this).closest(".uitklapcontainer").find("input[type=radio]:checked, input[type=text]:filled").length == 0)
                $(this).closest("li").removeClass("active");

            if ($(this).closest(".msvt-item-row").find("input[type=radio]:checked, input[type=text]:filled").length == 0)
                UnSetRequired($(this).closest(".msvt-item-row"));
        }
    });

    $("#uitvoeringsverzoekactions .collapse input[type=radio]:checked, #uitvoeringsverzoekactions .collapse input[type=text]:filled").closest(".collapse").collapse("show").parents("li").addClass("active");

    if (formSetVersionID === -1) {
        $("#PhaseInstanceID option:selected").removeAttr("selected");
        $('#PhaseInstanceID')
            .append($("<option></option>")
                .attr("value", "-1")
                .attr("selected", "")
                .attr("data-url", newrequesturl)
                .text(" -- Specificeer hieronder -- "));
    }
});

$(document).ready(function () {
    var printConditions = $(".printconditions").is(":checked");
    if (!printConditions) {
        $("#uvvconditionstext").addClass("hidden");
    } else {
        $("#uvvconditionstext").addClass("ignore-printfilter");
    }
});