﻿$(function () {
    function log(message) {
        $("<div>").text(message).prependTo("#output");
        $("#output").scrollTop(0);
    }

    $(".EmployeeSearch .inputselect input").autocomplete({
        source: function (request, response) {
            var inputbox = $(this.element);
            var hiddenfield = inputbox.parent().find("#" + inputbox.data("hiddenfield"));
            var data = {};
            data.search = request.term;

            if (inputbox.data("extraparameter") != undefined)
            {
                var parameterdata = inputbox.data("extraparameter").split(":");
                var fieldid = parameterdata[0];
                var parametername = parameterdata[1];
                var value = inputbox.parents(".EmployeeSearch").find("#" + fieldid).val();

                if (value == "") return;

                data[parametername] = value;
            }
            
            $.ajax({
                url: inputbox.data("controlleraction"),
                type: "POST",
                data: data,
                dataType: "json",
                success: function (data) {
                    hiddenfield.val("");
                    response(data);
                }
            });
        },
        minLength: 0,
        delay: 300,

        select: function (event, ui) {
            var inputbox = $(event.target);
            var hiddenfield = inputbox.parent().find("#" + inputbox.data("hiddenfield"));
            hiddenfield.val(ui.item.id);
        },

        open: function () {
            $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
        },

        close: function () {
           $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
        }
    });

});


