﻿function handleEmployeeTelephoneNumber(data) {
    if (data !== undefined && data.length === 1) {
        $('#DossierOwnerTelephoneNumber').val(data[0].Text);
    }
}

function SetUpScreen() {
    //Prefill DossierOwner Telephone
    $('#DossierOwner').on("change", function () {
        var employeeID = $(this).val();
        var digest = $(this).find(":selected").data("digest");
        $.ajax({
            type: "POST",
            url: getEmployeePhoneNumberByEmployeeID,
            data: { employeeID: employeeID, digest: digest },
            success: function (data) { handleEmployeeTelephoneNumber(data); }
        });
    });

    $("#BehandelendArtsID").on("change", function () {
        var artsID = $(this).val();
        var digest = $(this).find(":selected").data("digest");
        $.ajax({
            type: "POST",
            url: getDoctorByID,
            data: { doctorID: artsID, digest: digest },
            success: function (data) {
                setDoctor(data);
            },
            error: function (data) {
                showErrorMessage(data);
            }
        });
    });
}

function SetResize() {
    //Scrollable auto-resize
    $(window).on("resize", function () {
        var height = GetScrollHeight();
        $("#Scroller").css("max-height", height);
    });
    $(window).trigger("resize");
}

$(function () {
    setTimeout(SetResize, 0);
    setTimeout(SetUpScreen, 0);
});