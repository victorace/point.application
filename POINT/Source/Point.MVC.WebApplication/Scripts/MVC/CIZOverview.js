﻿function checkedDisableContainer(control, container) {
    function updateContainer() {
        if ($(this).is(":checked") == true) {
            EnableContainer(container);
        }else {
            DisableContainer(container);
            ClearContainer(container);
        }
    }
    $(control).on("change", updateContainer);
    $(control).each(updateContainer);
}

$(function () {

    checkedDisableContainer("#IndicationsForHomecareNew", "#HomecareNew");
    checkedDisableContainer("#IndicationsForHomecareExpansion", "#HomecareExpansion");
    checkedDisableContainer("#PrimaryStayBasis", "#StayBasis");
    checkedDisableContainer("#PrimaryStaySom", "#StaySom");
    checkedDisableContainer("#PrimaryStayPG", "#StayPG");
    checkedDisableContainer("#PrimaryStayPalliative", "#StayPalliative");
    checkedDisableContainer("#GRZCVA", "#CVA");
    checkedDisableContainer("#WlzGrondslagSom", "#GrondslagSom");
    checkedDisableContainer("#WlzGrondslagPG", "#GrondslagPG");
    checkedDisableContainer("#WlzProfiel", "#Profiel");
    checkedDisableContainer("#WlzAnders", "#Anders");
    checkedDisableContainer("#WMOBegeleiding", "#Begeleiding");
    checkedDisableContainer("#WMOHuishoudelijkeondersteuning", "#Huishoudelijkeondersteuning");
    checkedDisableContainer("#WMOJeugdzorg", "#Jeugdzorg");
});