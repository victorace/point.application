﻿var otherHealthAidDeviceCheckBox;
var otherDeliveryAddressCheckBox;
var otherHealthAidDeviceTextContainer;
var otherDeliveryAddressTextContainer;

$(document).ready(function () {
    otherHealthAidDeviceCheckBox = $("label:contains('Anders') input[type=checkbox]");
    otherDeliveryAddressCheckBox = $("label:contains('Ander adres') input[type=checkbox]");
    otherHealthAidDeviceTextContainer = $("#healthAidDevicesTextContainer");
    otherDeliveryAddressTextContainer = $("#deliveryAddressTextContainer");

    if (otherHealthAidDeviceCheckBox) {
        otherHealthAidDeviceCheckBox.on("change", function (e) {
            ShowHideOtherTextContainer(e.target, otherHealthAidDeviceTextContainer);
        });

        ShowHideOtherTextContainer(otherHealthAidDeviceCheckBox.first()[0], otherHealthAidDeviceTextContainer);
    }

    if (otherDeliveryAddressCheckBox) {
        otherDeliveryAddressCheckBox.on("change", function (e) {
            ShowHideOtherTextContainer(e.target, otherDeliveryAddressTextContainer);
        });

        ShowHideOtherTextContainer(otherDeliveryAddressCheckBox.first()[0], otherDeliveryAddressTextContainer);
    }

    $("#BehandelendArtsID").on("change", function () {
        var artsID = $(this).val();
        var digest = $(this).find(":selected").data("digest");
        $.ajax({
            type: "POST",
            url: getDoctorByID,
            data: { doctorID: artsID, digest: digest },
            success: function (data) {
                setDoctor(data);
            },
            error: function (data) {
                showErrorMessage(data);
            }
        });
    });
});

function ShowHideOtherTextContainer(obj, target) {

    if (obj.checked)
        target.show();
    else
        target.hide();
}