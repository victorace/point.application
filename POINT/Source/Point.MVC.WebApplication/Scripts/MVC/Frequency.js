﻿function selectTab(formSetVersionID) {
    var tabs = getTabs();

    var index = $.inArray(formSetVersionID, tabs);
    if (index < 0) index = 0;
    $("#tabsFrequency").tabs("option", "active", index);

    var currentTab = $('#tabsFrequency .ui-tabs-nav li a[data-formsetversionid="' + formSetVersionID + '"]');
    if (currentTab.length === 0) selectAllTab();
    else currentTab.trigger('click');
}

function selectAllTab() {
    var tabs = getTabs();

    var index = tabs.length;
    $("#tabsFrequency").tabs("option", "active", index);

    var currentTab = $('#tabsFrequency .ui-tabs-nav li a:last');
    currentTab.trigger('click');
}

function getTabs() {
    var tabs = $('#tabsFrequency .ui-tabs-nav li a[data-formsetversionid]').map(function (index) {
        return $(this).data('formsetversionid');
    });
    return tabs;
}

function EditFrequency(sender, target, title, status) {

    var hasSave = true;

    var buttons = [];
    if (typeof status !== "undefined") {

        var editFrequencyUrl = $(sender).data("closeurl");
        var buttonLabel = "Afsluiten";
        if (status === 2) {
            hasSave = false;
            editFrequencyUrl = $(sender).data("openurl");
            buttonLabel = "Heropenen";
        }

        buttons.push({
            click: function () {
                if ($(formID).valid()) {
                    $.ajax({
                        type: "POST",
                        cache: false,
                        url: editFrequencyUrl,
                        data: $(formID).serialize(),
                        async: true,
                        success: function (data) {
                            location.replace(data.Url);
                        },
                        error: function (xhr, status, error) {
                            showErrorMessage("Er is een fout opgetreden: " + xhr.responseText);
                        }
                    });
                }
            }, text: buttonLabel
        });
    }

    if (hasSave) {
        buttons.push({
            click: function () {
                if ($(formID).valid()) {
                    $.ajax({
                        type: "POST",
                        cache: false,
                        url: $(sender).data("saveurl"),
                        data: $(formID).serialize(),
                        async: true,
                        success: function (data) {
                            location.replace(data.Url);
                        },
                        error: function (xhr, status, error) {
                            showErrorMessage("Er is een fout opgetreden: " + xhr.responseText);
                        }
                    });
                }
            }, text: "Opslaan"
        });
    }

    buttons.push({ click: function () { $(this).dialog("close") }, text: "Annuleren" });

    $(target).dialog({
        open: function (event, ui) {
            $('.ui-widget-overlay').bind('click', function (event, ui) {
                $(target).dialog('close');
            });
        },
        title: title,
        autoOpen: false,
        modal: true,
        resizable: false,
        height: 240,
        width: 480,
        buttons: buttons,
        position: { my: "left top", at: "left bottom", of: $(sender) }
    });

    $(target).load($(sender).data("loadurl"))
    $(target).dialog("open");
}

// Page Setup
$(function () {
    $("#frequencyEditForm input").attr('disabled', (isClosed === 1));

    $.validator.addMethod('date',
        function (value, element) {
            if (this.optional(element)) return true;
            try { $.datepicker.parseDate('dd-mm-yy', value); } catch (error) { return false; }
            return true;
        }
    );

    $("[data-datetime='date']:enabled").each(function (index) {
        $(this).datepicker({
            dateFormat: "dd-mm-yy",
            showOn: "button",
            buttonImage: appPath + "Images/PopupCalendar/calendar.gif",
            buttonImageOnly: true,
            buttonText: ""
        },
        $.datepicker.regional["nl"]
        );
    });

    tooltip.init();
});