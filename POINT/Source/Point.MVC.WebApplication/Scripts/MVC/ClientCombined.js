﻿var fillFieldsFromData = function (data, prefix) {
    var hasPrefix = false;
    if (typeof prefix !== 'undefined') {
        hasPrefix = true;
    }

    var huisartsskipped = false;

    if (data["GeneralPractitionerZorgmail"] !== undefined && data["AddressGPForZorgmail"] === undefined) {
        data["AddressGPForZorgmail"] = data["GeneralPractitionerZorgmail"];
    }

    if (data["NumberAddition"] !== undefined && data["HuisnummerToevoeging"] === undefined) {
        data["HuisnummerToevoeging"] = data["NumberAddition"]
    }

    for (var fieldname in data) {
        var field = $(":input[name='" + (hasPrefix ? prefix + "." + fieldname : fieldname) + "']");

        if (field.prop("type") === "hidden" || field.length === 0) {
            continue;
        }

        if (fieldname === "GeneralPractitionerName" || fieldname === "GeneralPractitionerPhoneNumber" || fieldname === "AddressGPForZorgmail") {
            //"Huisarts" niet vullen als het zorgmailadres al reeds is ingevuld
            if ($("#AddressGPForZorgmail").val() !== "") {
                //Alleen als er daadwerkelijk zou worden geimporteerd daarvan een melding geven
                if (fieldname === "" && data[fieldname] !== "") {
                    huisartsskipped = true;
                }

                continue;
            }
        }

        if (fieldname.match(/Date/)) {
            if (data[fieldname] === null) {
                field.val('');
            }
            else {
                var dateformat = field.data('dateformat');
                field.val(new Date().jsonParseString(data[fieldname]).format(dateformat));
            }
        }
        else if (field.is(':radio')) {
            field.removeProp("checked");
            field.filter("input[value='" + data[fieldname] + "']:first").prop("checked", true).trigger("change");
        }
        else {
            field.val(data[fieldname]);
        }
    }

    if (huisartsskipped === true) {
        showWarningMessage("Huisarts veld is overgeslagen omdat deze al was ingevuld.");
    }
};

$.validator.setDefaults({ ignore: '.ignore *' });

function PutClientSearchData(data) {
    fillFieldsFromData(data.Client);
    if (data.HasContactPerson) {
        fillFieldsFromData(data.ContactPerson, "ContactPerson");
    }
};


$(function () {
    $(window).on("resize", function () {
        var height = GetScrollHeight();
        $("#Scroller").css("max-height", height);
        $("#Scroller").css("height", height);
    });
    $(window).trigger("resize");

    $("#btnImportClient").click(function () {
        var searchData = { CivilServiceNumber: $("#CivilServiceNumber").val() };
        GetClientDataByBSN(importClientUrl, searchData);
    });


    function ShowModal(requestUrl) {
        $.ajax({
            type: "POST",
            url: requestUrl,
            data: $(this).data(),
            async: true,
            success: function (data) {
                if (data.no_results == undefined) {
                    $("#modal").modal('show');
                    $("#modal .modal-content").data();
                    $("#modal .modal-body").html(data);
                }
            },
            error: function (data) {
                $("#modal").modal('show');
                $("#modal .modal-body").html(data.responseText);
            }
        });
    }


    $("#btnSearchByPatientInfo").click(function (sender) {
        ShowModal(searchByPatientInfoUrl);
    });

    $(formID + " .label").each(function () {
        $(this).html($(this).html() + ":");
    });

    $(".showContact").each(function () {
        $(this).click(function () { $(this).nextAll("div").eq(0).toggle(); });
    });

    $('#btnValidate').on('click', function () {
        $(formID).valid();
    });

    $.each($("#clientForm[data-disabled='disabled'] :input"), function () {
        $(this).attr("disabled", "");
    });

    $('[data-toggle="clickover"]:not(.client-header)').clickover({
        html: true,
        placement: "top",
        global_close: true,
        esc_close: true,
        title: $(this).data("title"),
        content: $(this).data("content")
    });

    $("#btnNedap").on("click", function () {
        var searchData = { CivilServiceNumber: $("#CivilServiceNumber").val(), DepartmentID: departmentID };
        GetClientDataByBSN(nedapClientUrl, searchData);
    });

});


function GetClientDataByBSN(url, searchData) {

    var error = ControleBsn($("#CivilServiceNumber").val());

    if (error > '') {
        showErrorMessage(error, true);
    } else {
        $.ajax({
            type: "Post",
            url: url,
            datatype: 'json',
            data: searchData,
            async: true,
            success: function (data) {
                if (data.HasClient) {
                    fillFieldsFromData(data.Client);
                    if (data.HasContactPerson) {
                        fillFieldsFromData(data.ContactPerson, "ContactPerson");
                        //$('#ContactPerson_CIZRelationID').val(data.CIZRelationID);
                    }
                }
                else {
                    showErrorMessage("Geen patient gevonden met dit BSN");
                }
            },
            error: function (jqxhr, textStatus, errorThrown) {
                showErrorMessage($.parseJSON(jqxhr.responseText).ErrorMessage, true);
            }
        });
    }

}
