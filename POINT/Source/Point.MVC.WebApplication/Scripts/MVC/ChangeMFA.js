﻿var mfadigest = '';

$(function () {

    $("input[data-val-maxlength-max]").each(function (index, element) {
        var length = parseInt($(this).attr("data-val-maxlength-max"), 10);
        $(this).prop("maxlength", length);
    });

    $(mfanumber).keyup(function () {
        $("#GetConfirmation").prop('disabled', $(this).val().length < 10 || $(this).val().indexOf("06") != 0);
    }).keyup();

    $("#GetConfirmation").click(function () {
        $(this).prop('disabled', true);

        $.ajax({
            type: 'POST',
            cache: false,
            url: sendmfacodeurl,
            data: $(formID).serialize(),
            success: function (data) {
                mfadigest = data.MFADigest;
            }
        });

        $(mfanumber).prop('disabled', true);

        $("#divMFACode").show();
        $(mfacode).focus();

    });

    $("#CheckMFACode").click(function () {
        $(this).prop('disabled', true);
        $(mfanumber).prop('disabled', false);

        var data = $(formID).serializeArray();
        data.push({ name: "MFADigest", value: mfadigest });

        $(mfanumber).prop('disabled', true);

        $.ajax({
            type: 'POST',
            cache: false,
            url: checkmfacodeurl,
            data: $.param(data),
            success: function (data) {
                var result = (data.result == true);
                $("#ResultSuccess").toggle(result);
                $("#ResultWarning").toggle(!result);
                if (result) {
                    $("#SetupCode").hide();
                }
            }
        });

        $(this).prop('disabled', false);

    });
});