﻿function ToggleHolder()
{
    var showFlowDefinition = true;
    var showPhaseDefinition = false;
    var department = $("#DepartmentID");
    var flowDefinition = $("#FlowDefinitionID");
    if (hasMultipleDepartments) {
        if (department.val() > -1) {
            if (flowDefinition.val() !== null && flowDefinition.val() !== "-1") {
                showPhaseDefinition = true;
            }
        } else {
            showFlowDefinition = false;
        }
    }
    else
    {
        if (flowDefinition.val() !== null && flowDefinition.val() !== "-1") {
            showPhaseDefinition = true;
        }
    }

    $("#flowDefinitionPanel").toggle(showFlowDefinition);
    $("#phaseDefinitionsPanel").toggle(showPhaseDefinition); 
    $("#phaseButtonsHolder .phaseButtonStart").prop("disabled", !showPhaseDefinition);
    $("#phaseButtonsHolder .phaseButtonStart").toggleClass("disabled", !showPhaseDefinition);

}

function DepartmentChanged()
{
    var departmentVal = $("#DepartmentID").val();
    if (departmentVal > -1) {
        $.get(urlGetAllowedFlowDefinitionsByDepartment, { DepartmentID: departmentVal },
            function (data) {
                fillSelectWithOptions($("#FlowDefinitionID"), data, { Value: "-1", Text: "Selecteer een transfertype" });
                ToggleHolder();
            });
    }
    else
    {
        ToggleHolder();
    }   
}

function FlowDefinitionChanged()
{
    var flowDefinitionVal = $("#FlowDefinitionID").val();   
    if (flowDefinitionVal !== null && flowDefinitionVal !== "-1") {
        AjaxLoad("#newFlowInstanceForm", overviewPhaseDefinitionUrl, "#phaseDefinitions", ToggleHolder);
    }
    else {
        ToggleHolder();
    }
}

$(function () {
    if (hasMultipleDepartments) {
        $("#selectDepartmentPanel").show();
    } else {
        $("#selectDepartmentPanel").hide();
    }

    $("#DepartmentID").change(function () {
        DepartmentChanged();
    }).change();

    $("#FlowDefinitionID").change(function () {
        FlowDefinitionChanged();
    }).change();
});