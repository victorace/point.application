﻿function SearchOrganization(sender, url, callback) {
    ShowModal(url, callback, inittable);
}

var PutDepartment = function (department) {
    $("input[data-linked='DestinationHospitalDepartment']:not([type=hidden])").val(department.DepartmentName);
    $("input[data-linked='DestinationHospitalDepartment'][type=hidden]").val(department.DepartmentID);
    departmentChanged();

    $("#modal").modal("hide");
};

function departmentChanged() {
    var currentselection = $("input[data-linked='DestinationHospitalDepartment'][type=hidden]").val();
    if (currentselection !== "" && prefill === false) {
        $("#SendToOther").show();
    } else {
        $("#SendToOther").hide();
    }
}

$(function () {
    $('#dialogConfirmSendToOther #btnSendToOther').one('click', function (e) {
        AjaxSave(formID, sendToOtherUrl, true, null, false);
        $("#SendToOther").hide();
    });
    $("#SendToOther").click(function () {
        if ($(formID).valid()) {
            $('#dialogConfirmSendToOther').modal('show');
        }
    });
});