﻿var formID = "#searchForm";
var ButtonKeys = { "EnterKey": 13 };
var patientOverview = "patientoverview";

// Global function to be called from Menu i.e. MenuController
function PrintSearchResults() {
    var htmldata = $("<table cellspacing='0' />");
    var htmldatahead = $("#TransferForSearchTableHead").find(".table.table-other > thead").clone();
    var htmldatabody = $("#TransferForSearchTable").find(".table.table-other > tbody").clone();

    htmldatabody.find("a").removeAttr("href");
    htmldatahead.appendTo(htmldata);
    htmldatabody.appendTo(htmldata);

    $("#FormPrint").find("#html").val(encodeURI(htmldata[0].outerHTML));
    $("#FormPrint").submit();
}

function getAvailableSearchHeight(resultsDiv) {
    var margin = 85;
    if ($(resultsDiv).data("popup") === true) {
        margin = 118;
    }
    return window.innerHeight - $(resultsDiv).offset().top - margin;
}

function resizeSearchResultTable(resultsDiv) {
    var availableSearchHeight = getAvailableSearchHeight(resultsDiv);
    $(resultsDiv).find("#TransferForSearchTable, .body-container-first, .body-container-scroller").height(availableSearchHeight);
    var firstcolumn = $(resultsDiv).find(".head-container-first");
    var gridWidth = $(resultsDiv).find("#TransferForSearchTableHead").width()-firstcolumn.width();
    $(resultsDiv).find(".head-container-scroller, .body-container-scroller").width(gridWidth);
}

function addsearch() {
    $(".extrasearchproperties").slideDown("slow");
}

function removesearch() {
    $(".extrasearchproperties").slideUp("slow");
    resetProperties(1);
}

displaySearchResults = function (data, source, resultsDiv) {
    $(resultsDiv).html(data);

    resizeSearchResultTable(resultsDiv);

    $(resultsDiv).find(".body-container-scroller").on("scroll", function () {
        var scrollleft = $(".body-container-scroller").scrollLeft();
        var scrolltop = $(".body-container-scroller").scrollTop();
        $(resultsDiv).find(".head-container-scroller").scrollLeft(scrollleft);
        $(resultsDiv).find(".body-container-first").scrollTop(scrolltop);
    });

    $(resultsDiv).find("th").not(".unsortable").on("click", function () {
        if ($.inArray($(this).attr("id"), noneSortableColumns) >= 0) {
            return;
        }

        var columns = $(resultsDiv).find("th").not(".unsortable");
        columns.each(function (index, element) {
            if ($.inArray($(element).attr("id"), noneSortableColumns) === -1) {
                $(element).removeClass("sorting_asc sorting_desc");
            }
        });

        var order = $(this).attr("data-order");
        if (order === undefined || order === "desc") {
            $(this).attr("data-order", "asc").removeClass("sorting").addClass("sorting_asc");
        }
        else {
            $(this).attr("data-order", "desc").removeClass("sorting").addClass("sorting_desc");
        }

        columns.not(".sorting_asc, .sorting_desc").each(function (index, element) {
            if ($.inArray($(element).attr("id"), noneSortableColumns) === -1) {
                $(element).removeAttr("data-order").addClass("sorting");
            }
        });

        if ($(resultsDiv).data("searchtype") === patientOverview) { 
            doSearchPatientOverview(source);
        }
        else {
            doSearch(source);
        }
    });

    attachPopovers(resultsDiv);

    $("div#pagination a").each(function (index, element) {
        if ($(element).data("page") === undefined) {
            $(element).prop("disabled", true);
        }
        else {
            $(element).click(function () {
                if ($(resultsDiv).data("searchtype") === patientOverview) { 
                    doSearchPatientOverview($(this));
                }
                else {
                    doSearch($(this));
                }
            }).css("cursor", "pointer");
        }
    });

    if ($('#renewsignalering').val() === 'true') {
        $('#renewsignalering').val('false');
        RefreshSignaleringStatus();
    }

};

function getClickoverInfoContent(el, resultsDiv) {

    var url = $(el).data("infourl");
    if (url === "") {
        return "";
    }

    var returnedcontent = null;

    $.ajax({
        url: url, 
        async: false
    }).done(
        function (data) {
            returnedcontent = data;
        }
     ).error(
        function (a, b) {
            showErrorMessage('Kan info niet ophalen');
        }
     );

    var containerdiv = $("<div/>").append(returnedcontent);

    if ($(resultsDiv).data("searchtype") === patientOverview) {
        $(containerdiv).click(function (ev) { 
            ev.preventDefault; return false;
        }); 
    }

    return containerdiv;
}

attachPopovers = function (resultsDiv) {
    $('[data-toggle="clickover"]:not(.client-header)').clickover({
        html: true,
        placement: "top",
        global_close: true,
        esc_close: true,
        content: function () { return getClickoverInfoContent($(this), resultsDiv); }
    });	
};

var getselectedphasevalues = function () {
    var phase = $("#phase");
    return $(phase).find("option:selected")
        .map(function (index, item) { return "&" + encodeURIComponent("phases[" + index + "]") + "=" + encodeURIComponent(item.value); })
        .get()
        .join("");
};

var getphases = function (values, phaseurl) {
    var phasevalues = getselectedphasevalues(); 
    var phaseurlvalues = values + phasevalues;
    return $.ajax({
        type: "POST",
        url: phaseurl,
        data: phaseurlvalues,
        async: true
    });
};

var getsearchresults = function (values, searchresulturl) {
    var phasevalues = getselectedphasevalues();
    var searchresultvalues = values + phasevalues;
    return $.ajax({
        type: "POST",
        url: searchresulturl,
        data: searchresultvalues,
        async: true
    });
};



getPageAndSorting = function (source, resultsDiv) {

    var page = $(source).data("page");
    if (page === undefined) {
        page = 1;
    }

    var values = "Page=" + encodeURIComponent(page);

    var column = $(resultsDiv).find("th[data-order]");
    if ($(column).length > 0) {
        sortOrder = $(column).attr("data-order");
        sortColumn = $(column).attr("id");
        searchfieldowningtype = $(column).attr("data-owningtype");
    }

    if (sortOrder !== null && sortColumn !== null) {
        values += "&SortOrder=" + encodeURIComponent(sortOrder) + "&SortColumn=" + encodeURIComponent(sortColumn) + "&SearchFieldOwningType=" + encodeURIComponent(searchfieldowningtype);
    }

    return values;

};

doSearch = function (source) {

    var values = $(formID).serialize();
    values += "&SearchType=" + encodeURIComponent(searchType);

    if (patExternalReference) {
        values += "&patExternalReference=" + patExternalReference;
    }
    
    values += "&" + getPageAndSorting(source, $("#searchResultsDiv"));

    var phase = $("#phase");
    getphases(values, $(source).data("phaseurl"))
        .done(function (data) {
            if (typeof data.Options !== "undefined") {
                fillMultiSelectWithOptions(phase, data.Options, false);
            }
        });

    getsearchresults(values, $(source).data("url"))
        .done(function (data) {
            displaySearchResults(data, source, "#searchResultsDiv");
        })
        .fail(function (xhr, status, error) {
            showErrorMessage("Er is een fout opgetreden");
        });
};

doSearchPatientOverview = function (source) {
    var resultsdivid = "searchResultsDivPopup";
    var values = getPageAndSorting(source, $("#" + resultsdivid));
    var title = function () {
        if (/SearchResultPatientOverview$/i.test($(source).data("url"))) {
            return "Voortgang patiënten";
        }
        else if (/SearchResultPatientPrevious$/i.test($(source).data("url"))) {
            return "Recent bekeken patiënten";
        }
    };

    $.ajax({
        type: "POST",
        url: $(source).data("url"),
        data: values,
        async: true,
        beforeSend: function () {
            $("#modal").modal('show');
        }
    })
        .done(function (data) {
            var resultsdiv = $("<div />").attr("id", resultsdivid).data("popup", true).data("searchtype", patientOverview);
            var titlediv = $("<div />").css("padding", "0 0 10px 0").addClass("modal-header")
                .append($("<button />").addClass("btn btn-primary pull-right").attr("data-dismiss", "modal").attr("type", "button").text("Sluiten"))
                .append($("<label />").css("margin", "0").addClass("pull-left h3").text(title));
            $("#modal .modal-body").html("").append(titlediv).append(resultsdiv);
            displaySearchResults(data, source, resultsdiv);
        })
        .fail(function (xhr, status, error) {
            showErrorMessage("Er is een fout opgetreden");
        });
};

function initSearchScreen() {

    hideFilterBlock = function () {
        $("#searchfilters").toggle();
        $(".filtertoggle").each(function (item, value) {
            $(value).toggle();
        });

        resizeSearchResultTable("#searchResultsDiv");
    };

    if (patExternalReference) {
        $("#searchControlsContainer").hide();
    }

    clearSearchInputs = function (source, searchPropertyType) {
        source.find(":input").hide();
        source.find("select").not(".static-data").find("option").remove();      
        source.find(":input[data-searchpropertytype='" + searchPropertyType + "']").show();
    };

    fillSearchSelect = function (source, data) {
        var select = $(source).find("select").not(".static-data");
        $.each(data, function (index, element) {
            $(select).append("<option value='" + element.Value + "'>" + element.Text + "</option>");
        });
    };

    resetSelect = function (source) {
        $(source).each(function (ix, el) {
            $(el).find("option").prop("selected", "");
            var defaultselected = $(el).find("option[data-defaultselected='selected']");
            if (defaultselected === null || defaultselected.length === 0) {
                defaultselected = $(el).find("option:first");
            }
            defaultselected.prop("selected", "selected");
            $(el).change();
        });
    };

    resetFilter = function () {
        $("input[name=statusFilter]").prop("disabled", false);
        $("input[name=statusFilter][value='1']").prop("checked", true); // NOTE '1' ~ DossierStatus.Active

        $("input[name=frequencyStatusFilter]").prop("disabled", false);
        $("input[name=frequencyStatusFilter][value='1']").prop("checked", true); // NOTE '1' ~ FrequencyDossierStatus.Active

        var phase = $("#phase");

        phase.multiselect({
            numberDisplayed: 1,
            nonSelectedText: "Alle fasen",
            nSelectedText: " geselecteerd"
        });

        if (phase.length === 1) {
            phase.multiselect('deselectAll', false);
            phase.multiselect('updateButtonText');
        }

        resetProperties(0);
        resetProperties(1);
    };

    resetProperties = function (index) {
        resetSelect($("#propertyList" + index));
        $("#inputs" + index + " input").val("");
        resetSelect($("#inputs" + index + " select"));
    };

    $("#searchForm").keypress(function (e) {
        if (e.which === ButtonKeys.EnterKey) {
            $("#search").click();
            return false;
        }
    });

    if (autoRefreshMilliseconds > 0) {
        setInterval(function () {
            $("#search").click();
            RefreshSignaleringStatus();
        }, autoRefreshMilliseconds);
    }

    $(window).on("resize", function () {
        resizeSearchResultTable("#searchResultsDiv");
    });

    if (noFilterOptions) {
        hideFilterBlock();
        $("#filtervisibility").hide();
    }
    else {
        $(".filtertoggle").each(function (index, element) {
            $(element).click(function () { hideFilterBlock(); }).css("cursor", "pointer");
        });
    }

    $(".propertyList").each(function (index, element) {
        $(element).on("change", function () {            
            var parent = $(this);
            var source = $("#" + $(parent).data("source") + "");
            var selected = $(parent).find(":selected");
            var searchPropertyType = $(selected).data("searchpropertytype");
            var selectedflowdefinitionids = $("#filterFlowDefinitionID li.active input").map(function () { return this.value; }).get().join();

            clearSearchInputs(source, searchPropertyType);

            if (searchPropertyType === Enum.SearchPropertyType.Lookup) {
                $.ajax({
                    type: "POST",
                    url: $(parent).data("url"),
                    data: {
                        name: $(selected).val(),
                        flowdefinitionids: selectedflowdefinitionids
                    },
                    success: function (data) {
                        fillSearchSelect(source, data);
                    }
                });
            }
            if (searchPropertyType === Enum.SearchPropertyType.Date) {
                //reserved                
            }
        });
    });

    $("#reset").on("click", function () {
        resetFilter();
        location.href = $(this).data("url");
    });

    $("#search").on("click", function () {
        doSearch($(this));
    });

    $("#addsearch").on("click", function () {
        addsearch();
    });

     $("#removesearch").on("click", function () {
        removesearch();
    });

    resetSelect(".propertyList");

    doSearch($("#search"));
}