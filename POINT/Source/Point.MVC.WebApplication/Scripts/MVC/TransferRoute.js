﻿
var currentInvitesRef = "#selectedza #vvtTableBody tr";

function GetFlowFieldName(flowFieldName) {
    return $('input[data-linked="' + flowFieldName + '"]:not([type=hidden])').val();
}

function GetFlowFieldID(flowFieldName) {
    return $('input[data-linked="' + flowFieldName + '"][type=hidden]').val();
}

function SetFlowFieldIdAndName(flowFieldName, id, name) {
    $('input[data-linked="' + flowFieldName + '"][type=hidden]').val(id);
    $('input[data-linked="' + flowFieldName + '"]:not([type=hidden])').val(name);
}

function SearchOrganization(sender, url, callback) {
    if (!$(formID).valid()) {
        return;
    }

    var senderobj = $(sender);
    if (senderobj) {
        if (senderobj.data("canadd") === false) {
            $("#dialog-cantadd").modal("show");
            return;
        }

        if (callback.name === "PutOrganisationGRZ" && grzCanBeSent !== true) {
            $("#dialog-grzwarning").modal("show");
            return;
        }
    }
    ShowModal(url, callback, inittable);
}


function addParameter(url, parameterName, parameterValue, atStart/*Add param before others*/) {
    replaceDuplicates = true;
    if (url.indexOf('#') > 0) {
        var cl = url.indexOf('#');
        urlhash = url.substring(url.indexOf('#'), url.length);
    } else {
        urlhash = '';
        cl = url.length;
    }
    sourceUrl = url.substring(0, cl);

    var urlParts = sourceUrl.split("?");
    var newQueryString = "";

    if (urlParts.length > 1) {
        var parameters = urlParts[1].split("&");
        for (var i = 0; i < parameters.length; i++) {
            var parameterParts = parameters[i].split("=");
            if (!(replaceDuplicates && parameterParts[0] === parameterName)) {
                if (newQueryString === "")
                    newQueryString = "?";
                else
                    newQueryString += "&";
                newQueryString += parameterParts[0] + "=" + (parameterParts[1] ? parameterParts[1] : '');
            }
        }
    }
    if (newQueryString === "")
        newQueryString = "?";

    if (atStart) {
        newQueryString = '?' + parameterName + "=" + parameterValue + (newQueryString.length > 1 ? '&' + newQueryString.substring(1) : '');
    } else {
        if (newQueryString !== "" && newQueryString !== '?')
            newQueryString += "&";
        newQueryString += parameterName + "=" + (parameterValue ? parameterValue : '');
    }
    return urlParts[0] + newQueryString + urlhash;
}

function SetZorginstellingVoorkeurPatient1(id, name) {
    SetFlowFieldIdAndName("ZorgInZorginstellingVoorkeurPatient1", id, name);
}

function SetSelectedHealthcareProvider(id, name) {
    SetFlowFieldIdAndName("SelectedHealthCareProvider", id, name);
}

function SetDestinationHealthcareProvider(id, name) {
    SetFlowFieldIdAndName("DestinationHealthCareProvider", id, name);
}

function Remove(sender) {
    var row = $(sender).closest("tr");
    if (row.data("organizationinviteid") > 0) {
        row.addClass("deleted").attr("data-deleted", true).hide();
    } else {
        row.remove();
    }

    var invites = $(currentInvitesRef);

    var first = invites.not(".deleted").first();
    if (first.length === 1) {
        first.find("td.invitestatus").text(newinviteactivestatusname).closest("tr").attr("class", 'invitestatus_' + inviteactivestatus);
        first.attr("data-invitestatus", inviteactivestatus);
    }

    var newids = invites.not(".deleted").map(function () { return $(this).data("departmentid"); }).get();
    $('#multiinfo').toggle(newids.length === 1);

	Send(true);
}

function AddSendRow(departmentid, departmentname, organizationinviteid, invitestatusname, invitestatus) {
    var deletebuttontext = 'Verwijderen';
    if (organizationinviteid && invitestatus === inviteactivestatus) {
        deletebuttontext = 'Intrekken';
    }

    var deletebutton = '<button class="btn btn-sm btn-danger" onclick="Remove(this)">' + deletebuttontext + '</button>';

    $('#selectedza tbody').append('<tr class="invitestatus_' + invitestatus + '" data-organizationinviteid="' + organizationinviteid + '" data-departmentid="' + departmentid + '" data-departmentname="' + departmentname + '" data-invitestatus="' + invitestatus + '" data-invitestatusname="' + invitestatusname + '"><td class="departmentname">' + departmentname + '</td><td class="invitestatus">' + invitestatusname + '</td><td></td><td class="deletebutton text-right">' + deletebutton + '</td></tr>');
}


function AskPutMultiHealthcareProvider(department) {
    var isPoint = department.Participation === 2;

    if (!isPoint) {
        $('#dialogConfirmPutMultiHealthcareProvider').modal('show').one('click', '#btnPutMultiHealthcareProvider',
            function(event) {
                PutMultiHealthcareProvider(department, isPoint);
                event.stopImmediatePropagation();
                return false;
            });
    } else {
        PutMultiHealthcareProvider(department, true);
    }
}

var PutMultiHealthcareProvider = function (department, isPoint) {
    
    var currentids = $(currentInvitesRef).map(function () { return $(this).data("departmentid"); }).get();

    var invitestatus = currentids.length === 0 ? inviteactivestatus : invitequeuedstatus;
    var invitestatusname = currentids.length === 0 ? newinviteactivestatusname : newinvitequeuedstatusname;

    var isChanged = true;
    var index = currentids.indexOf(department.DepartmentID);
    if (index < 0) {
        AddSendRow(department.DepartmentID, department.DepartmentName, '', invitestatusname, invitestatus);
        currentids.push(department.DepartmentID);
    } else {
        var obj = $(currentInvitesRef + "[data-departmentid='" + department.DepartmentID + "']");
        isChanged = obj.data("organizationinviteid") === "";
        if (obj.hasClass("deleted")) {
            obj.removeClass("deleted").attr("data-deleted", false).show();
        }
    }

    $('#destinationComment').toggle(!isPoint);

    $('#multiinfo').toggle(currentids.length === 1);

    if (isChanged) {
		Send();
    }
    
    $('#modal').modal('hide');
};

function transferRouteInvitesAjax() {
    var inputData = "transferID=" + $("#TransferID").val() + "&flowInstanceID=" + $("#FlowInstanceID").val() + "&flowDefinitionID=" + $("#FlowDefinitionID").val();
    
    $.ajax({
        type: "POST",
        url: transferRouteInvitesUrl,
        data: inputData,
        dataType: "json",
        success: function (data) { transferRouteInvitesUpdatePage(data); },
        error: function (xhr, status, error) { showErrorMessage(error); }
    });
}

function transferRouteInvitesUpdatePage(data) {

    $.each(data.htmlReplaces || [],
        function (id, html) {
            if ($(id).length) {
                $(id).replaceWith($(html));
            }
        });

    CheckVVTButtons();

    $('#transferRouteInvitesPanel [data-toggle="clickover"]').clickover({
        html: true,
        placement: "top",
        global_close: true,
        esc_close: true,
        title: getClickoverTitle,
        content: getClickoverContent
    });
}

var PutHealthcareProviderChoice = function (department) {
    SetFlowFieldIdAndName("ZorgInZorginstellingVoorkeurPatient1", department.DepartmentID, department.DepartmentName);
    $('#modal').modal('hide');
};


function SendVVTPreference() {

    if (!$(formID).valid()) {
        return;
    }

    var department = {
        DepartmentID: parseInt(GetFlowFieldID("ZorgInZorginstellingVoorkeurPatient1")),
        DepartmentName: GetFlowFieldName("ZorgInZorginstellingVoorkeurPatient1"),
        Participation: parseInt($("#VVTPreferenceParticipation").val())
    };
    AskPutMultiHealthcareProvider(department);
}

function Cancel() {
    transferRouteInvitesAjax();
}

function Send(deleteExisting) {

    if (deleteExisting === undefined) {
        deleteExisting = false;
    }

    if ($(formID).valid()) {
        var extradata = $(this).data();
        extradata.DeleteExisting = deleteExisting;

        var departments = $(currentInvitesRef).map(
            function (i, element) {
                return $(element).data();
            }
        ).get();

        var firstactivedepartment = $(currentInvitesRef).not(".deleted").map(
            function (i, element) {
                return $(element).data();
            }
        ).first();

        if (departments.length > 0) {
            extradata.Departments = departments;
        }

        var url = pointZANextPhaseUrl;
        if (firstactivedepartment.length > 0) {
            SetDestinationHealthcareProvider(firstactivedepartment[0].departmentid, firstactivedepartment[0].departmentname);
        } else {
            SetDestinationHealthcareProvider('', '');
        }

        SetSelectedHealthcareProvider('', '');

        AjaxSave(formID, url, true, null, false, extradata);

        showResetVVT = true;

    }
    
}

var PutOrganisationII = function (department) {
    SetFlowFieldIdAndName("SelectedII", department.DepartmentID, department.DepartmentName);
    $("#modal").modal('hide');
    $('#SelectedII').trigger("change");
};

var PutOrganisationGRZ = function (department) {
    SetFlowFieldIdAndName("SelectedGRZ", department.DepartmentID, department.DepartmentName);
    $("#modal").modal('hide');
    $('#SelectedGRZ').trigger("change");
};

var getDefaultAftercareClassificationSelectionTransferPoint = function (source, aftercareClassificationTypeID) {
    $.ajax({
        type: "POST",
        url: getDefaultAftercareClassificationSelectionUrl,
        data: { aftercareClassificationTypeID: aftercareClassificationTypeID, aftercareClassificationShowID: Enum.AftercareClassificationShowID.TransferPoint },
        success: function (data) {
            selectDefaultAftercareClassificationSelection(source, data);
        },
        error: function (data) {
        }
    });
};

var selectDefaultAftercareClassificationSelection = function (source, data) {
    $(source).val(data.AftercareClassificationID);
};

function CheckVVTButtons()
{
    var zorgInZorginstellingVoorkeurPatient1ID = GetFlowFieldID("ZorgInZorginstellingVoorkeurPatient1");
    var destinationHealthCareProviderID = GetFlowFieldID("DestinationHealthCareProvider");
    var selectedHealthCareProviderID = GetFlowFieldID("SelectedHealthCareProvider");

    var takeoverZorgInZorginstellingVoorkeurPatient1 = zorgInZorginstellingVoorkeurPatient1ID !== '' && selectedHealthCareProviderID === '';
    var takeoverDestinationHealthCareProvider = destinationHealthCareProviderID !== '' && zorgInZorginstellingVoorkeurPatient1ID === '';

    $("#btnFromZorgInZorginstellingVoorkeurPatient1").toggle(takeoverZorgInZorginstellingVoorkeurPatient1);
    $("#btnFromDestinationHealthCareProvider").toggle(takeoverDestinationHealthCareProvider);
}

function CheckAndShowToestemming() {
    if (showResetVVT) {
        $("#resetvvtmodal").modal('show');
    }
}

function AskCancelVVT()
{
    $('#dialog-confirm').modal('show').one('click', '#delete', function () {
        CancelVVT();
    });
}

function CancelVVT()
{
    if ($("#cancelReasonTxt") !== null)
    {
        var cancelres = $("#cancelReasonTxt").val();
    }

    $.ajax({
        type: "POST",
        url: resetVVTUrl,
        async: true,
        data: {'cancelreason': cancelres},
        success: function (data) {
            FormIsNotDirty();
            window.location.reload();
        }
    });

    $("#btncancelvvt").hide();
}

function TakeOverHCP()
{
    SetSelectedHealthcareProvider(GetFlowFieldID("ZorgInZorginstellingVoorkeurPatient1"), GetFlowFieldName("ZorgInZorginstellingVoorkeurPatient1"));
    $('#SelectedHealthCareProvider').trigger("change");

    CheckVVTButtons();
}

function ActueleVVTToVoorkeurPatient1() {
    var name = GetFlowFieldName("DestinationHealthCareProvider");
    if (name) {
        SetZorginstellingVoorkeurPatient1(GetFlowFieldID("DestinationHealthCareProvider"), name);
    }

    CheckVVTButtons();
}

function fillSelectWithOptions(source, options, lastvalue) {
    $(source).find("option").remove();

    if (options.length > 1)
    {
        $(source).append("<option value=''>&lt;Selecteer&gt;</option>");
    }

    if (options.constructor === Array) {
        $.each(options, function (index, item) {
            var selected = "";
            if (item.Selected === true || item.Value === lastvalue)
                selected = "selected";

            $(source).append("<option value='" + item.Value + "' " + selected + ">" + item.Text + "</option>");
        });
    }
}

function DeleteInvite(sender)
{
    $.ajax({
        type: "POST",
        url: deleteinviteurl,
        data: $(sender).data(),
        success: function (data) {
            if (data.Success === true)
            {
                $(sender).closest("tr").remove();
            } else {
                showErrorMessage('Deze kon niet worden verwijderd. Hij was al reeds verstuurd.');
            }
        }
    });
}

//Toestemming
var loadedToestemmingPatient = false;

function setToestemmingPatient() {
    $("#ToestemmingPatientJa").toggle($(this).val() === "ja" && (this.checked || loadedToestemmingPatient === false));
    $("#ToestemmingPatientNee").toggle($(this).val() === "nee" && (this.checked || loadedToestemmingPatient === false));
    $("#ToestemmingPatientNietMogelijk").toggle($(this).val() === "niet mogelijk" && (this.checked || loadedToestemmingPatient === false));

    if (loadedToestemmingPatient) {
        if ($(this).val() === "ja" && this.checked) {
            ClearContainer("#ToestemmingPatientNee");
            ClearContainer("#ToestemmingPatientNietMogelijk");
        } else if ($(this).val() === "nee" && this.checked) {
            ClearContainer("#ToestemmingPatientJa");
            ClearContainer("#ToestemmingPatientNietMogelijk");
            CheckAndShowToestemming();
        } else if ($(this).val() === "niet mogelijk" && this.checked) {
            ClearContainer("#ToestemmingPatientNee");
            ClearContainer("#ToestemmingPatientJa");
        }
    }
}

function toestemmingVan() {
    var toestemmingPatientVanVertegenwoordiger = $(this).val() === "Vertegenwoordiger";
    $("#ToestemmingPatientVanVertegenwoordiger").toggle(toestemmingPatientVanVertegenwoordiger && this.checked);

    if ((toestemmingPatientVanVertegenwoordiger && this.checked) === false)
        ClearContainer("#ToestemmingPatientVanVertegenwoordiger");
}

function getClickoverTitle() {
    var linked = $(this).data("linked");
    if (linked !== undefined) {
        return getLinkedText($("#" + linked));
    } else {
        return $(this).data("title");
    }
}

function getClickoverContent() {
    var noContent = "Nog geen gegevens beschikbaar";

    var requestUrl = $(this).data("url");
    if (requestUrl !== undefined) {

        var linked = $(this).data("linked");
        if ($.getUrlVar(requestUrl, "id") === undefined && linked !== undefined) {
            var value = getLinkedValue($("#" + linked));
            var id = parseInt(value);
            if (isNaN(id)) {
                return noContent;
            }
            var digest = getLinkedDigest($("#" + linked));
            requestUrl = update_query_string(requestUrl, "id", id);
            requestUrl = update_query_string(requestUrl, "digest", digest);
        }

        var content = $.ajax({
            url: requestUrl,
            type: "GET",
            dataType: "html",
            async: false,
            success: function () { /*just get the response*/ },
            error: function () { content = noContent; }
        }).responseText;

        return content;
    }
    return $(this).attr("data-content");
}

function getLinkedValue(linked) {
    if (linked === undefined) return "";
    if ($(linked).is("select")) {
        return $(linked).find("option:selected").val();
    } else {
        return $(linked).val();
    }
}

function getLinkedDigest(linked) {
    if (linked === undefined) return "";
    if ($(linked).is("select")) {
        return $(linked).find("option:selected").data("digest");
    } else {
        return $(linked).data("digest");
    }
}

function getLinkedText(linked) {
    if (linked === undefined) return "";
    if ($(linked).is("select")) {
        var selected = $(linked).find("option:selected");
        if (selected && selected.val()) {
            return selected.text();
        }
    } else {
        return $(linked).text();
    }
    return "";
}

function getFinancingItems(lastvalue) {
    $.ajax({
        type: "POST",
        url: getFinancingUrl,
        data: { aftercaretypeid: $("#AfterCareDefinitive").val() },
        success: function (data) {
            fillSelectWithOptions("#AfterCareFinancing", data, lastvalue);
        }
    });
}

$(document).ready(function () {
    //Extend for IE, chrome allready has this property per default.
    PutOrganisationGRZ.name = "PutOrganisationGRZ";

    $("#cancelvvt").on("click", CancelVVT);

    if (hasToestemmingSection) {

        $.validator.addMethod("validateToestemming", function (value, element) { return value === 'ja' || value === "niet mogelijk"; }, "Patient moet toestemming hebben gegeven.");
        var toestemmingPatientName = GetNameByID("#ToestemmingPatient");
        var toestemmingPatient = $("input[name='" + toestemmingPatientName + "']");
        toestemmingPatient.rules("add", "validateToestemming");

        MakeRadiosDeselectableByID("#ToestemmingPatient");
        toestemmingPatientName = GetNameByID("#ToestemmingPatient");
        $("[name='" + toestemmingPatientName + "']input:radio").on("click", setToestemmingPatient);
        GetSelectedRadioByName(toestemmingPatientName).each(setToestemmingPatient);
        loadedToestemmingPatient = true;

        //Toestemming Van
        var toestemmingPatientVanName = GetNameByID("#ToestemmingPatientVan");
        $("[name='" + toestemmingPatientVanName + "']input:radio").on("click", toestemmingVan);
        GetSelectedRadioByName(toestemmingPatientVanName).each(toestemmingVan);
    }

    $('#SelectedHealthCareProvider').on("change", function () {
        var orgID = $(this).val();
        var showSendButton = orgID !== '';
        $("#btnSendZa").toggle(showSendButton);
    });

    $('#QueuedHealthCareProvider').on("change", function () {
        var orgID = $(this).val();
        var showSendButton = orgID !== '';
        $("#btnPlanZa").toggle(showSendButton);
    });

    $('#SelectedII').on("change", function () {
        var orgID = $(this).val();
        var showSendButton = orgID !== '';
        $("#btnSendII").toggle(showSendButton);
    });

    $('#SelectedGRZ').on("change", function () {
        var orgID = $(this).val();
        var showSendButton = orgID !== '';
        $("#btnSendGRZ").toggle(showSendButton);
    });

    $("#btnSendII").on("click", function () {
        if ($(formID).valid()) {
            var destinationID =  GetFlowFieldID("SelectedII");
            var destinationName = GetFlowFieldName("SelectedII");

            var extradata = $(this).data();
            extradata.departmentIDs = destinationID.split(';');
            extradata.formsetversionid = formSetVersionID;
            extradata.alreadysent = $(this).data("alreadysent") === "True";

            SetFlowFieldIdAndName("DestinationII", destinationID, destinationName);
            SetFlowFieldIdAndName("SelectedII", '', '');

            AjaxSave(formID, pointIINextPhaseUrl, true, null, false, extradata);
        }
    });

    $("#btnSendGRZ").on("click", function () {
        if ($(formID).valid()) {
            var destinationID = GetFlowFieldID("SelectedGRZ");
            var destinationName = GetFlowFieldName("SelectedGRZ");

            var extradata = $(this).data();
            extradata.departmentIDs = destinationID.split(';');
            extradata.formsetversionid = formSetVersionID;
            extradata.alreadysent = $(this).data("alreadysent") === "True";

            SetFlowFieldIdAndName("DestinationGRZ", destinationID, destinationName);
            SetFlowFieldIdAndName("SelectedGRZ", '', '');

            AjaxSave(formID, pointGRZNextPhaseUrl, true, null, false, extradata);
        }
    });

    if (hideThuiszorg) {
        var aftercarecategorydefinitive = $("[name='" + GetNameByID("#AfterCareCategoryDefinitive") + "'][value='1']");
        if (!aftercarecategorydefinitive.is(":checked")) {
            aftercarecategorydefinitive.parent("label").hide();
        }
    }

    $("#AfterCareDefinitive").on("change", getFinancingItems);

    if ($("#AfterCareFinancing").val() !== null) {
        var lastvalue = $("#AfterCareFinancing").val();
        getFinancingItems(lastvalue);
    }

    if ($("#AfterCareFinancing").val() === null && $("#AfterCareDefinitive").val() !== null) {
        $("#AfterCareDefinitive").trigger("change");
    }

    $('[data-toggle="clickover"]:not(.client-header)').clickover({
        html: true,
        placement: "top",
        global_close: true,
        esc_close: true,
        title: getClickoverTitle,
        content: getClickoverContent
    });
});
