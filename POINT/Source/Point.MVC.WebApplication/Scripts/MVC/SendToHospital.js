﻿function SearchOrganization(sender, url, callback) {
    ShowModal(url, callback, inittable);
}

var PutLocation = function (location) {
    $("input[data-linked='DestinationHospitalLocation']:not([type=hidden])").val(location.LocationName);
    $("input[data-linked='DestinationHospitalLocation'][type=hidden]").val(location.LocationID);
    destinationChanged();

    $("#modal").modal("hide");
};

var PutDepartment = function (department) {
    $("input[data-linked='DestinationHospitalDepartment']:not([type=hidden])").val(department.DepartmentName);
    $("input[data-linked='DestinationHospitalDepartment'][type=hidden]").val(department.DepartmentID);
    departmentChanged();

    $("#modal").modal("hide");
};

function CheckAndShowToestemming() {
    if (showResetZH) {
        $("#resetzhmodal").modal('show');
    }
}

function destinationChanged() {
    var selectedvalue = $("input[data-linked='DestinationHospitalLocation'][type=hidden]").val();

    if (phaseInstanceStatus === 2) {
        if (selectedvalue !== "" && selectedvalue != undefined && selectedvalue != parseInt(lastSendToHospitalLocation)) {
            $("#SendToOther").show();
        } else {
            $("#SendToOther").hide();
        }
    } else {
        $(".phaseButtonNext").toggle(selectedvalue !== "");
    }
}

function departmentChanged() {
    var selectedvalue = $("input[data-linked='DestinationHospitalDepartment'][type=hidden]").val();
    if (phaseInstanceStatus === 2) {
        if (selectedvalue !== "" && selectedvalue != undefined && selectedvalue != parseInt(lastSendToHospitalDepartment)) {
            $("#SendToOtherDepartment").show();
        } else {
            $("#SendToOtherDepartment").hide();
        }
    } else {
        $(".phaseButtonNext").toggle(selectedvalue !== "");
    }
}

function AskCancelOrganization() {
    $('#dialog-confirm').modal('show').one('click', '#delete', function () {
        CancelOrganization();
    });
}

function CancelOrganization() {
    $.ajax({
        type: "POST",
        url: resetUrl,
        async: true,
        success: function (data) {
            FormIsNotDirty();
            window.location.reload();
        }
    });

    $("#btncancellocation").hide();
    $("#btncanceldepartment").hide();
}

function DeleteInvite(sender) {
    $.ajax({
        type: "POST",
        url: deleteinviteurl,
        data: $(sender).data(),
        success: function (data) {
            if (data.Success === true) {
                $(sender).closest("tr").remove();
            } else {
                showErrorMessage('Deze kon niet worden verwijderd. Hij was al reeds verstuurd.')
            }
        }
    });
}

function getLinkedValue(linked) {
    if (linked === undefined) return "";
    if ($(linked).is("select")) {
        return $(linked).find("option:selected").val();
    } else {
        return $(linked).val();
    }
    return "";
};

function getLinkedDigest(linked) {
    if (linked === undefined) return "";
    if ($(linked).is("select")) {
        return $(linked).find("option:selected").data("digest");
    } else {
        return $(linked).data("digest");
    }
    return "";
}

function getLinkedText(linked) {
    if (linked === undefined) return "";
    if ($(linked).is("select")) {
        var selected = $(linked).find("option:selected");
        if (selected && selected.val()) {
            return selected.text();
        }
    } else {
        return $(linked).text();
    }
    return "";
}

function getClickoverTitle() {
    var linked = $(this).data("linked");
    if (linked !== undefined) {
        return getLinkedText($("#" + linked));
    } else {
        return $(this).data("title");
    }
}

function getClickoverContent() {
    var noContent = "Nog geen gegevens beschikbaar";

    var requestUrl = $(this).data("url");
    if (requestUrl !== undefined) {

        var linked = $(this).data("linked");
        if ($.getUrlVar(requestUrl, "id") === undefined && linked !== undefined) {
            var value = getLinkedValue($("#" + linked));
            var id = parseInt(value);
            if (isNaN(id)) {
                return noContent;
            }
            var digest = getLinkedDigest($("#" + linked));
            requestUrl = update_query_string(requestUrl, "id", id);
            requestUrl = update_query_string(requestUrl, "digest", digest);
        };

        var content = $.ajax({
            url: requestUrl,
            type: "GET",
            dataType: "html",
            async: false,
            success: function () { /*just get the response*/ },
            error: function () { content = noContent }
        }).responseText;

        return content;
    }
    return $(this).attr("data-content");
}

$(document).ready(function () {

    $(".phaseButtonNext").hide();

    $.validator.addMethod("validateToestemming", function (value, element) { return value === 'ja' || value === "niet mogelijk"; }, "Patient moet toestemming hebben gegeven.");
    var toestemmingPatientName = GetNameByID("#ToestemmingPatient");
    var toestemmingPatient = $("input[name='" + toestemmingPatientName + "']");
    toestemmingPatient.rules("add", "validateToestemming");

    //Toestemming
    var loadedToestemmingPatient = false;
    function setToestemmingPatient() {
        $("#ToestemmingPatientJa").toggle($(this).val() === "ja" && (this.checked || loadedToestemmingPatient === false));
        $("#ToestemmingPatientNee").toggle($(this).val() === "nee" && (this.checked || loadedToestemmingPatient === false));
        $("#ToestemmingPatientNietMogelijk").toggle($(this).val() === "niet mogelijk" && (this.checked || loadedToestemmingPatient === false));

        if (loadedToestemmingPatient) {
            if ($(this).val() === "ja" && this.checked) {
                ClearContainer("#ToestemmingPatientNee");
                ClearContainer("#ToestemmingPatientNietMogelijk");
            } else if ($(this).val() === "nee" && this.checked) {
                ClearContainer("#ToestemmingPatientJa");
                ClearContainer("#ToestemmingPatientNietMogelijk");
                CheckAndShowToestemming();
            } else if ($(this).val() === "niet mogelijk" && this.checked) {
                ClearContainer("#ToestemmingPatientNee");
                ClearContainer("#ToestemmingPatientJa");
            }
        }
    }

    MakeRadiosDeselectableByID("#ToestemmingPatient");
    toestemmingPatientName = GetNameByID("#ToestemmingPatient");
    $("[name='" + toestemmingPatientName + "']input:radio").on("click", setToestemmingPatient);
    GetSelectedRadioByName(toestemmingPatientName).each(setToestemmingPatient);
    loadedToestemmingPatient = true;

    function toestemmingVan() {
        var toestemmingPatientVanVertegenwoordiger = $(this).val() === "Vertegenwoordiger";
        $("#ToestemmingPatientVanVertegenwoordiger").toggle(toestemmingPatientVanVertegenwoordiger && this.checked);

        if ((toestemmingPatientVanVertegenwoordiger && this.checked) === false)
            ClearContainer("#ToestemmingPatientVanVertegenwoordiger");
    }

    //Toestemming Van
    var toestemmingPatientVanName = GetNameByID("#ToestemmingPatientVan");
    $("[name='" + toestemmingPatientVanName + "']input:radio").on("click", toestemmingVan);
    GetSelectedRadioByName(toestemmingPatientVanName).each(toestemmingVan);

    $('[data-toggle="clickover"]').clickover({
        html: true,
        placement: "top",
        global_close: true,
        esc_close: true,
        template: '<div class="popover contactinformationpopover"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>',
        title: getClickoverTitle,
        content: getClickoverContent
    });

    $("#SendToOther, #SendToOtherDepartment").hide();

    $("#DestinationHospitalLocation").on("change", destinationChanged);
    $("#DestinationHospitalDepartment").on("change", departmentChanged);

    $('#dialogConfirmSendToOther #btnSendToOther').one('click', function (e) {
        AjaxSave(formID, sendToOtherUrl, true, null, false);
        $("#SendToOther").hide();
    });

    $("#SendToOther, #SendToOtherDepartment").click(function () {
        if ($(formID).valid()) {
            $('#dialogConfirmSendToOther').modal('show');
        }
    });

    // handle save but not sent
    if (selectdepartment) {
        departmentChanged();
    } else {
        destinationChanged();
    }
});