﻿function ClickCheck() {
    var checkedfs = [];
    var checkedft = [];
    var combinedfs = [];
    var attachments = [];
    $(".formsetversion-group input:checked").each(function () {
        if ($(this).data("formsetversionid") !== "") {
            checkedfs.push($(this).data("formsetversionid"));
        }

        if ($(this).data("formtypeid") !== "") {
            checkedft.push($(this).data("formtypeid"));
        }

        if ($(this).data("combined") !== ""){
            combinedfs.push($(this).data("combined"));
        }

        if ($(this).data("attachment") !== "") {
            attachments.push($(this).data("attachment"));
        }
    });
    $("#CheckedFormsetVersions").val(checkedfs.join());
    $("#CheckedFormTypes").val(checkedft.join());
    $("#CombinedFormsetIDs").val(combinedfs.join());
    $("#CheckedAttachments").val(attachments.join());
}

function CheckAll() {
    var checked = $("#printPanel :checkbox").first().is(":checked");
    $("#printPanel :checkbox").each(function () { if ($(this).is(":checked") === checked) { $(this).click(); } });
}

$(function () {
    $("#print").attr("disabled", "disabled");
    $("input[type=checkbox]").change(function () {
        if ($("input[type=checkbox]:checked").length === 0) {
            $("#print").attr("disabled", "disabled");
        } else {
            $("#print").removeAttr("disabled");
        }
    });

    $(".formsetversion-group .formname").click(function () {
        $(this).parent().find("input[type=checkbox]").click();
    });

    AjaxLoad(null, overviewAttachmentUrl, '#attachmentPanel .panel-body');

    var formSetVersionID = $.getUrlVar(document.referrer, "FormSetVersionID");
    if (formSetVersionID !== undefined) {
        $(".formsetversion-group input:checkbox").each(function () {
            var formSetVersionIDs = [], combinedIDs = [];
            var data = $(this).data("formsetversionid");
            if (data !== undefined) {
                formSetVersionIDs = data.toString().split(',');
            }
            data = $(this).data("combined");
            if (data !== undefined) {
                combinedIDs = data.toString().split(',');
            }
            formSetVersionIDs = formSetVersionIDs.concat(combinedIDs.filter(function (item) { return formSetVersionIDs.indexOf(item) < 0; }));
            if ($.inArray(formSetVersionID, formSetVersionIDs) !== -1) {
                $(this).click();
            }
        });
    }
});