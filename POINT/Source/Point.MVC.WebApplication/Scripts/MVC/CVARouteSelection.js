﻿function SearchOrganization(sender, url, callback) {
    ShowModal(url, callback, inittable)
}

function ChangeHuisbestemming(sender) {
    var isaanvraag = ['2', '4'].indexOf($(sender).val()) > -1;
    var isafsluiten = ['1', '3'].indexOf($(sender).val()) > -1;

    var phasedefinitionidtoshow  = '';
    if (isafsluiten) { phasedefinitionidtoshow = afsluitenPhaseDefinitionID }
    if (isaanvraag) { phasedefinitionidtoshow = afdAanvraagPhaseDefinitionID }

    ShowNextButton(phasedefinitionidtoshow );
}

function ChangeBestemming(sender) {
    var isafsluiten = ['5', '6', '7', '8'].indexOf($(sender).val()) > -1;
    var isaanvraag = ['3', '4'].indexOf($(sender).val()) > -1;
    var isvo = $(sender).val() === '2';

    var phasedefinitionidtoshow = '';
    if (isafsluiten) { phasedefinitionidtoshow = afsluitenPhaseDefinitionID }
    if (isaanvraag) { phasedefinitionidtoshow = afdAanvraagPhaseDefinitionID }
    if (isvo) { phasedefinitionidtoshow = afdVOPhaseDefinitionID }

    ShowNextButton(phasedefinitionidtoshow);
}

function ShowNextButton(phasedefinitionid) {
    $(".phaseButtonNext").hide();
    $(".phaseButtonNext[data-nextphasedefinitionid='" + phasedefinitionid + "']").css('display', '');
}

var putRevalidatiecentrum = function (department) {
    var ispoint = department.Participation == 2;
    $("#destinationComment").toggle(!ispoint);

    $("input[data-linked='SelectedHealthCareProvider']:not([type=hidden])").val(department.DepartmentName);
    $("input[data-linked='SelectedHealthCareProvider'][type=hidden]").val(department.DepartmentID);
    $("#modal").modal("hide");
    $("#SelectedHealthCareProvider").trigger("change");
    // because validator.settings.unhighlight doesn't appear to get called automatically on a readonly element
    $("#SelectedHealthCareProvider").trigger("blur");

    ShowNextButton(afdVOPhaseDefinitionID);
};

var putGRZCVAverpleeghuis = function (department) {
    $("input[data-linked='ZorgInZorginstellingVoorkeurPatient1']:not([type=hidden])").val(department.DepartmentName);
    $("input[data-linked='ZorgInZorginstellingVoorkeurPatient1'][type=hidden]").val(department.DepartmentID);
    $("#modal").modal("hide");
    $("#ZorgInZorginstellingVoorkeurPatient1").trigger("change");

    ShowNextButton(afdAanvraagPhaseDefinitionID);
}

$(function () {
    $(".phaseButtonNext").hide();
    $(".phaseButtonNext").html(resendavailable ? "Wijzigen ontslagbestemming" : "Ontslagbestemming vastleggen");
    if (resendavailable) {
        $(".phaseButtonNext").attr("data-confirm", "confirmcvachangerouteselection");
    } else {
        $(".phaseButtonNext").removeData("data-confirm");
    }

    //Ignore validation on all elements inside a container with ignore as classname
    $.validator.setDefaults({ ignore: '.ignore *' });

    $("input[data-linked='SelectedHealthCareProvider']:not([type=hidden])").addClass("required");
    $("input[data-linked='SelectedHealthCareProvider']:not([type=hidden])").attr('data-msg-required', 'Te versturen naar VVT is verplicht.');
    $("input[data-linked='SelectedHealthCareProvider']:not([type=hidden])").attr('data-val', 'true');

    RadioToggleChildren("#Ontslagbestemming", "#huis", ["1"]);
    RadioToggleChildren("#Ontslagbestemming", "#revalidatiecentrum", ["2"]);
    RadioToggleChildren("#Ontslagbestemming", "#grzcvaverpleeghuis", ["3"]);
    RadioToggleChildren("#Ontslagbestemming", "#anderziekenhuis", ["5"]);
    RadioToggleChildren("#Ontslagbestemming", "#buitenland", ["6"]);
    RadioToggleChildren("#Ontslagbestemming", "#overig", ["7"]);
});