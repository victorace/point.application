﻿// Global function to be called from Menu i.e. MenuController
function ImportZIS(sender, url, callback) {
    ShowModalZIS(url, callback);
}

// Global function to be called from Menu i.e. MenuController
function NewFlowInstancePreCheck(originalUrl, checkUrl) {
    $.ajax({
        url: checkUrl,
        success: function (existingDossierCheckInfo) {
            if (existingDossierCheckInfo.NeedToShowWarning) {
                $("#confirmExistingDossierBody").html(existingDossierCheckInfo.WarningTextHtml);

                $("#confirmExistingDossierOK").click(function () {
                    location.href = originalUrl;
                });

                $("#confirmExistingDossierModal").modal({ show: true });
                $("#confirmExistingDossierModal").css("visibility", "visible");
            }
            else {
                location.href = originalUrl;
            }
        },
        error: function (xhr, status, error) {
            showErrorMessage(error);
        }
    });
}

function ShowModalZIS(requestUrl, callback, afterload) {
    $.ajax({
        type: "GET",
        url: requestUrl,
        data: $(this).data(),
        async: false,
        beforeSend: function () {
            $("#modal").modal('show');
        },
        success: function (data) {
            $("#modal .modal-content").data("callback", callback);
            $("#modal .modal-body").html(data);
            if (afterload) {
                afterload();
            }
        },
        error: function (data) {
            $("#modal .modal-body").html(data.responseText);
        }
    });
}

function ShowPatientOverview(searchResultUrl) {

    doSearchPatientOverview($("<a />").data("url", searchResultUrl));
}

$(function () {
    $(".menuItem[data-disabled='True']").each(function () {
        $(this).addClass("disabled").attr("disabled", 'disabled');
    });
    $(".menuItem").not(".menuItem[data-disabled='True']").each(function () {
        $(this).find("a").attr("href", $(this).data("url")).attr("target", $(this).data("target"));
    });
    $(".menuItem[data-hidden='True']").each(function () { $(this).hide(); });
});