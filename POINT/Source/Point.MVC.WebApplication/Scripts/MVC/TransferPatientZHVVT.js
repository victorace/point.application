﻿$(function () {

    $(window).on("resize", function () {
        var height = GetScrollHeight();
        $("#Scroller").css("max-height", height);
        $("#Scroller").css("height", height);
    });
    $(window).trigger("resize");

    $(".phaseButtonNext").hide();

    var handleDischargePatientAcceptedYNByVVTName = function () {

        $(".phaseButtonNext").hide();

        var canoffercare = $(this);
        var canoffercareval = canoffercare.val().toLowerCase();
        var canoffercarebool = canoffercareval === "ja";


        $("#divDischargePatientAcceptedYes").toggle(canoffercarebool);
        $("#divDischargePatientAcceptedNo").toggle(!canoffercarebool);

        if (canoffercarebool) {
            ClearContainer("#divDischargePatientAcceptedNo");
            if (prefill) {
                $("#DischargeProposedStartDate").val($("#CareBeginDate").val());
            }
        }
        if (!canoffercarebool) {
            ClearContainer("#divDischargePatientAcceptedYes");
            if (canoffercare.is(":enabled")) {
                $(".phaseButtonNext[data-nextphasedefinitionid='" + nextPhaseNee + "']").css('display', '');
            }
        }
    };
    var handlepatientAcceptedAtDateTimeYNName = function () {
        $(".phaseButtonNext").hide();

        var canoffercareatdate = $(this);
        var canoffercareatdateval = canoffercareatdate.val().toLowerCase();
        var canoffercareatdateja = canoffercareatdateval === "ja";
        var canoffercareatdatenee = canoffercareatdateval === "nee";
        var canoffercareatdatehasvalue = canoffercareatdateval !== '';

        $("#divPatientAcceptedAtDateTime").toggle(canoffercareatdatehasvalue);
        $("#lblDischargeProposedStartDateTrue").toggle(canoffercareatdateja);
        $("#lblDischargeProposedStartDateFalse").toggle(canoffercareatdatenee);

        if (canoffercareatdateja) {
            ClearContainer("#divPatientAcceptedAtDateTimeNo");
            if (canoffercareatdate.is(":enabled")) {
                $(".phaseButtonNext[data-nextphasedefinitionid='" + nextPhaseJa + "']").css('display', '');
            }
        }

        if (canoffercareatdatenee) {
            if (canoffercareatdate.is(":enabled")) {
                $(".phaseButtonNext[data-nextphasedefinitionid='" + nextPhaseJaMaar + "']").css('display', '');
            }
        }
    };

    var dischargePatientAcceptedYNByVVTName = GetNameByID("#DischargePatientAcceptedYNByVVT");
    var radioDischargePatientAcceptedYNByVVTName = $("[name='" + dischargePatientAcceptedYNByVVTName + "']input:radio");
    radioDischargePatientAcceptedYNByVVTName.on("click", handleDischargePatientAcceptedYNByVVTName);
    radioDischargePatientAcceptedYNByVVTName.filter(":checked").each(handleDischargePatientAcceptedYNByVVTName);
    
    var patientAcceptedAtDateTimeYNName = GetNameByID("#PatientAcceptedAtDateTimeYN");
    var radiopatientAcceptedAtDateTimeYNName = $("[name='" + patientAcceptedAtDateTimeYNName + "']input:radio");
    radiopatientAcceptedAtDateTimeYNName.on("click", handlepatientAcceptedAtDateTimeYNName);
    radiopatientAcceptedAtDateTimeYNName.filter(":checked").each(handlepatientAcceptedAtDateTimeYNName);

    $('[data-toggle="clickover"]').clickover({
        html: true,
        placement: $(this).attr("data-placement") ? $(this).data("placement") : "top",
        global_close: true,
        esc_close: true,
        template: '<div class="popover zorgvraagpopover"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>',
        content: $(this).data("content")
    });
});