﻿// Multiselect Dropdown

$(function () {

    if (typeof (_dropdownMultiSel) === 'undefined') {
        // Multiselect Dropdown
        function DropdownMultiSelect() {
            "use strict";
            var maxSelectItems = 3;
            var objFunc = this;

            this.displaySelections = function (dropdownRef, fieldNameForPost) {
                var html = '<div class="multisel-short">';
                var selectedValues = '';
                var checkboxRef = dropdownRef + ' .multisel-items input[type="checkbox"]';
                var selectedCount = $(checkboxRef + ':checked').length;
                var isFirst = true;

                $(checkboxRef).each(function () {
                    var isChecked = $(this).is(':checked');
                    $(this).prop("disabled", selectedCount === maxSelectItems && !isChecked);

                    if (isChecked) {
                        var separator = isFirst ? '' : ',';
                        html += separator + '<span title="' + $(this).data('displaytextfull') + '">' + $(this).data('displaytextshort') + '</span > ';
                        selectedValues += separator + $(this).val();
                        isFirst = false;
                    }
                });
                html += '</div >';

                if (fieldNameForPost !== '' && $('#' + fieldNameForPost).length) {
                    $('#' + fieldNameForPost).val(selectedValues);
                }

                $(dropdownRef + ' .multisel-short').replaceWith(html);

                if (selectedCount > 0) {
                    $(dropdownRef + " .multisel-none").hide();
                } else {
                    $(dropdownRef + " .multisel-none").show();
                }
            }

            this.init = function (fieldNameForPost) {

                if (typeof (fieldNameForPost) === 'undefined') {
                    fieldNameForPost = '';
                }

                var parentRef = "#DropdownMulti" + fieldNameForPost;
                var dropdownRef = parentRef + '.dropdown-multisel';

                if (!$(parentRef).hasClass('disabled')) {

                    $(dropdownRef + ' dt a').on('click',
                        function () {
                            $(dropdownRef + ' dd ul').slideToggle('fast');
                        });

                    $(dropdownRef + ' dd ul li a').on('click',
                        function () {
                            $(dropdownRef + ' dd ul').hide();
                        });

                    $(document).bind('click',
                        function (e) {
                            var $clicked = $(e.target);
                            if (!$clicked.parents().hasClass('dropdown-multisel')) {
                                $(dropdownRef + ' dd ul').hide();
                            }
                        });

                    $(dropdownRef + ' .multisel-items input[type="checkbox"]').on('click',
                        function () {
                            objFunc.displaySelections(dropdownRef, fieldNameForPost);
                        });
                }

                objFunc.displaySelections(dropdownRef, fieldNameForPost);
            };
        }

        _dropdownMultiSel = new DropdownMultiSelect();
    }

});