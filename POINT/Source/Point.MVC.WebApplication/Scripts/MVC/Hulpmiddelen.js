﻿
var btnPlaceOrderRef = "#btnPlaceOrder";
var emailChangedNotifierRef = "#emailChangedNotifier";
var ontvangerBevestigingsEmailSelectedIdentifierRef = "#BevestigingsEmailTarget input:checked";
var orderItemsContainerRef = _Hulpmiddelen.OrderItemsContainerRef;
var orderItemsRef = _Hulpmiddelen.OrderItemsContainerRef + " .orderitem";
var questionListButtonRequiredRef = "#questionlistbuttonRequired";
var rentalPatientConsentRef = "#RentalPatientConsent";
var rentalPatientConsentLabelRef = rentalPatientConsentRef + " .control-label";
var toestemmingPatientHuurartikelenRef = "#ToestemmingPatientHuurartikelen";
var ontvangerBevestigingsEmailAdresRef = "#" + _Hulpmiddelen.OntvangerBevestigingsEmailAdresIdentifier;
var ontvangerBevestigingsEmailAdresTransferpuntRef = "#" + _Hulpmiddelen.OntvangerBevestigingsEmailAdresTransferpuntIdentifier;
var ontvangerBevestigingsEmailAdresPatientRef = "#" + _Hulpmiddelen.OntvangerBevestigingsEmailAdresPatientIdentifier;
var orderStatusRef = "#OrderStatus";
var dialogInsertAttachmentRef = '#dialog-insert';
var aidProductIDsRef = "#AidProductIDs";
var afleverDatumGroupRef = "#afleverdatumgroup";
var supplierMediPointRef = ".supplier-medipoint";
var supplierVegroRef = ".supplier-vegro";
var supplierPointRef = ".supplier-point";

function checkTransportStatus() {
    var postData = { productids: getOrderItems() };
    _Hulpmiddelen.AjaxPost(_Hulpmiddelen.Urls.GetTransportStatus, postData, transportStatusUpdate);
}

function transportStatusUpdate(responseData) {
    var transportCostsRef = "#transportcosts";
    var transportCostsFieldRef = "#TransportKosten";

    if (responseData.length === 0 || responseData.HasTransportCosts === false) {
        $(transportCostsRef).hide().find(transportCostsFieldRef).val('');
    }
    else if (responseData.HasTransportCosts === true) {
        $(transportCostsRef).show().find(transportCostsFieldRef).val('\u20AC ' + responseData.TransportCosts);
    }
}


function setQuestionListButton() {
    if (_Hulpmiddelen.IsCurrentOrderDone) {
        displayQuestionListButtonRequired(false);
        return;
    }

    var hasItems = _Hulpmiddelen.IsExternalSupplier && orderHasItems();

    $("#questionlistbutton").toggle(hasItems);

    var questionsAnswered = $("#QuestionsAnsweredAll").val();
    if (hasItems && (questionsAnswered !== _Hulpmiddelen.Enums.QuestionsAnsweredAllYes && questionsAnswered !== _Hulpmiddelen.Enums.QuestionsAnsweredAllNotNecessary)) {
        displayQuestionListButtonRequired(true);
    } else {
        displayQuestionListButtonRequired(false);
    }
}

function checkQuestionStatus() {
    var productIDs = getOrderItems();
    var supplierInsurerCode = _Hulpmiddelen.GetSupplierCode();
    var postData = { supplierID: _Hulpmiddelen.SupplierID, transferid: _Hulpmiddelen.TransferID, productids: productIDs, uniqueFormKey: _Hulpmiddelen.UniqueFormKey, supplierInsurerCode: supplierInsurerCode, formSetVersionID: _Hulpmiddelen.FormSetVersionID };

    _Hulpmiddelen.AjaxPost(_Hulpmiddelen.Urls.GetUnansweredQuestions, postData, questionStatusUpdate);
}

function questionStatusUpdate(responseData) {
    var areQuestionsStillRequired = false;
    var questionsAnsweredValue = _Hulpmiddelen.Enums.QuestionsAnsweredAllNotNecessary; // 1=Yes, 2=Not necessary

    if (responseData.HasQuestions) { // If Questions only used then it is an External Supplier
        if (responseData.UnansweredQuestions.length === 0) {
            questionsAnsweredValue = _Hulpmiddelen.Enums.QuestionsAnsweredAllYes;
        } else {
            $("#QuestionsAnsweredGroup input").prop('checked', false);
            questionsAnsweredValue = "";
            areQuestionsStillRequired = true;
        }
    }

    _Hulpmiddelen.ShowQuestionWarning = areQuestionsStillRequired;

    if (!areQuestionsStillRequired) {
        setRadioButtonValue("#QuestionsAnswered", questionsAnsweredValue);
    }
    
    displayQuestionListButtonRequired(areQuestionsStillRequired);
}

function orderHasItems() {
    return $(orderItemsRef).length > 0;
}

function showQuestionListWarning() {
    $("#questionWarningModal").modal('show');
}

function showExternalSupplierWarning() {
    showWarningMessage($("#insurerExternalSupplierWarningMessage").html());
}

function showBSNWarning() {
    showWarningMessage($("#bsnWarningMessage").html());
}

function disableHulpmiddelenPage() {
    $(_Hulpmiddelen.FormID + " input:not('#FormsetVersion'), .phaseButtonSave," + _Hulpmiddelen.FormID + " .dropdown-multisel input").attr('disabled', true);
    $(_Hulpmiddelen.FormID + " .btn").hide();
    $(btnPlaceOrderRef).hide();
}

function isVariableUndefined(val) {
    return typeof val == 'undefined';
}

function isEmailDefault(filledVal) {
    var ontvangerBevestigingsEmailObj = $(ontvangerBevestigingsEmailAdresRef);
    if (ontvangerBevestigingsEmailObj.length === 0) {
        return;
    }

    if (isVariableUndefined(filledVal)) {
        filledVal = ontvangerBevestigingsEmailObj.val().trim().toLowerCase();   
    }
    return (filledVal === "" || filledVal === _Hulpmiddelen.PatientEmail.toLowerCase() || filledVal === _Hulpmiddelen.TransferPuntEmail.toLowerCase());
}

function ontvangerEmailChanged() {
    var ontvangerBevestigingsEmailObj = $(ontvangerBevestigingsEmailAdresRef);
    if (ontvangerBevestigingsEmailObj.length === 0) {
        return;
    }
    var filledVal = ontvangerBevestigingsEmailObj.val().trim().toLowerCase();
    var isEmailNotDefault = !isEmailDefault(filledVal);
    $(emailChangedNotifierRef).tooltip(isEmailNotDefault ? "enable" : "disable");
    $(emailChangedNotifierRef).prop("disabled", isEmailNotDefault);

    if (filledVal === "") {
        _Hulpmiddelen.ChangeOntvangerEmail();
    }

    var classValueChanged = "value-changed";

    if (isEmailNotDefault) {
        ontvangerBevestigingsEmailObj.addClass(classValueChanged);
    } else {
        ontvangerBevestigingsEmailObj.removeClass(classValueChanged);
    }
}

// ----- MEDIPOINT
function ontvangerEmailMediPointChanged(sender) {

    var classValueChanged = "value-changed";
    if ($(sender).val() !== '') {
        $(sender).removeClass(classValueChanged);
        return;
    }
    var target = $(sender).data("target");

    if (target === "TRANSFERPUNT") {
        $(sender).val(_Hulpmiddelen.TransferPuntEmail);
    }
    if (target === "PATIENT") {
        $(sender).val(_Hulpmiddelen.PatientEmail);
    }

    $(sender).addClass(classValueChanged);
}

function SetOntvangerEmail() {
    $("#ConfirmationEmail").val($(_Hulpmiddelen.OntvangerBevestigingsEmailAdresIdentifier).val());
}

function getSupplierData(responseIfPoint, responseIfVegro, responseIfMediPoint, defaultValue) {
    if (isVariableUndefined(defaultValue)) {
        defaultValue = null;
    }

    switch (_Hulpmiddelen.SupplierID) {
        case _Hulpmiddelen.Enums.PointID:
            return responseIfPoint;
        case _Hulpmiddelen.Enums.VegroID:
            return responseIfVegro;
        case _Hulpmiddelen.Enums.MediPointID:
            return responseIfMediPoint;
    }
    return defaultValue;
}

function getSupplierName() {
    return getSupplierData(_Hulpmiddelen.SupplierNamePoint, _Hulpmiddelen.SupplierNameVegro, _Hulpmiddelen.SupplierNameMediPoint);
}

function hasSupplierAccount() {
    return getSupplierData(true, _Hulpmiddelen.HasVegroAccount, _Hulpmiddelen.HasMediPointAccount, false);
}

function addOrderItem(sender) {
    if (!hasSupplierAccount()) {
        showExternalSupplierWarning();
        return;
    }

    if (_Hulpmiddelen.IsExternalSupplier && !isFieldValueFilled($("#PatientBSN").val(), 8, 9)) {
        showBSNWarning();
        return;
    }

    ShowModal(_Hulpmiddelen.Urls.AddItem + "?supplierid=" + _Hulpmiddelen.SupplierID + "&uzovi=" + _Hulpmiddelen.HealthInsurerUzovi);
}

function setPatientAddress() {
    setAddress(_Hulpmiddelen.PatientAddress.Street, _Hulpmiddelen.PatientAddress.HouseNumber, _Hulpmiddelen.PatientAddress.HouseNumberExtra, _Hulpmiddelen.PatientAddress.PostalCode, _Hulpmiddelen.PatientAddress.City);
}

function setVVTAddress() {
    setAddress(_Hulpmiddelen.HealthInsurerAddress.Street, _Hulpmiddelen.HealthInsurerAddress.HouseNumber, _Hulpmiddelen.HealthInsurerAddress.HouseNumberExtra, _Hulpmiddelen.HealthInsurerAddress.PostalCode, _Hulpmiddelen.HealthInsurerAddress.City);
}

function setHospitalAddress() {
    setAddress(_Hulpmiddelen.HospitalAddress.Street, _Hulpmiddelen.HospitalAddress.HouseNumber, _Hulpmiddelen.HospitalAddress.HouseNumberExtra, _Hulpmiddelen.HospitalAddress.PostalCode, _Hulpmiddelen.HospitalAddress.City);
}

function setAddress(street, houseNumber, houseNumberExtra, postalCode, city) {
    $("#OntvangerStraat").val(street);
    $("#OntvangerHuisnummer").val(houseNumber);
    $("#OntvangerHuisnummerExtra").val(houseNumberExtra);
    $("#OntvangerPostcode").val(postalCode);
    $("#OntvangerPlaats").val(city);
}

function clearOntvangerAddress() {
    setAddress('', '', '', '', '');
}

function displayQuestionListButtonRequired(show) {
    if (orderHasItems() && show) {
        $(questionListButtonRequiredRef).addClass("required").show();
    } else {
        $(questionListButtonRequiredRef).removeClass("required").hide();
    }
}

function checkSupplierDependentItemsDisplay() {
    if (_Hulpmiddelen.SupplierID == _Hulpmiddelen.Enums.VegroID) {
        $(rentalPatientConsentRef).show();
    } else {
        $(rentalPatientConsentRef).hide();
    }

    $(supplierMediPointRef + "," + supplierVegroRef + "," + supplierPointRef).addClass("ignore").hide();

    var selectedSupplierRef = "";
    switch (_Hulpmiddelen.SupplierID) {
        case _Hulpmiddelen.Enums.MediPointID:
            selectedSupplierRef = supplierMediPointRef;
            break;
        case _Hulpmiddelen.Enums.VegroID:
            selectedSupplierRef = supplierVegroRef;
            break;
        case _Hulpmiddelen.Enums.PointID:
            selectedSupplierRef = supplierPointRef;
            break;
    }

    $(selectedSupplierRef).removeClass("ignore").show();
    
    setQuestionListButton();
}

function isFieldValueFilled(fieldValue, minLength, maxLength) {
    return !(fieldValue === "" || isNaN(fieldValue) || fieldValue.length < minLength || fieldValue.length > maxLength);
}

function isOrderDone() {
    var orderStatusVal = $(orderStatusRef).val();
    return orderStatusVal === _Hulpmiddelen.Enums.OrderStatusProcessing || orderStatusVal === _Hulpmiddelen.Enums.OrderStatusSent || orderStatusVal === _Hulpmiddelen.Enums.OrderStatusConfirmed;
}

function checkEnableCurrentOrder() {
    if (isOrderDone()) {
        disableHulpmiddelenPage();
    }
}

function checkDisplayMessage() {
    if (_Hulpmiddelen.ShowOrderStatusMessage) {
        $("#processingOrderMessage .processingOrder-messages").hide();
        
        if (_Hulpmiddelen.OrderStatus === _Hulpmiddelen.Enums.OrderStatusFailed) {
            showErrorMessage($("#processingOrderFailed").html());
        }
        if (_Hulpmiddelen.OrderStatus === _Hulpmiddelen.Enums.OrderStatusProcessing || _Hulpmiddelen.OrderStatus === _Hulpmiddelen.OrderStatusSent) {
            showWarningMessage($("#processingOrderInfo").html(), "Bestelling hulpmiddelen");
        }
        if (_Hulpmiddelen.OrderStatus === _Hulpmiddelen.Enums.OrderStatusConfirmed) {
            showWarningMessage($("#processingOrderSuccess").html(), "Bestelling hulpmiddelen");
        }
    }
}

function ajaxGet(getUrl, onSuccessFunction, getDataType) {
    if (isVariableUndefined(getDataType)) {
        getDataType = 'html';
    };

    $.ajax({
        cache: false,
        type: "GET",
        url: getUrl,
        dataType: getDataType,
        success: function (responseData) {
            onSuccessFunction(responseData);
        }
    });
}


function getOrderItems() {
    return $(aidProductIDsRef).val();
}

function setOrderItems(productIds) {
    $(aidProductIDsRef).val(productIds);
}

function refreshOrderStatus() {
    checkTransportStatus();
    checkQuestionStatus();
    setQuestionListButton();
    _Hulpmiddelen.CheckOrderStatus();
}


/*
*************************************************
     _Hulpmiddelen GLOBAL Functions
*************************************************
*/

_Hulpmiddelen.AjaxPost = function(postUrl, postData, onSuccessFunction) {
    $.ajax({
        cache: false,
        type: "POST",
        url: postUrl,
        data: postData,
        success: function (responseData) {
            if (!isVariableUndefined(onSuccessFunction)) {
                onSuccessFunction(responseData);
            }
        }
    });
}


_Hulpmiddelen.ShowQuestions = function(sender) {
    if (!hasSupplierAccount()) {
        showExternalSupplierWarning();
        return;
    }

    if (_Hulpmiddelen.IsExternalSupplier && !isFieldValueFilled($("#PatientBSN").val(), 8, 9)) {
        showBSNWarning();
        return;
    }

    $(sender).data("productids", getOrderItems());
    $(sender).data("supplierinsurercode", _Hulpmiddelen.GetSupplierCode());
    $(sender).data("supplierid", _Hulpmiddelen.SupplierID);

    var params = $.param($(sender).data());
    var checkquestionStatus = checkQuestionStatus();
    var modal = $('#modal');

    modal.off('hidden.bs.modal');
    modal.on('hidden.bs.modal', function () {
        checkQuestionStatus();
    });
    ShowModal(_Hulpmiddelen.Urls.Questions + "?" + params, checkquestionStatus);
}

_Hulpmiddelen.CheckFormSetName = function(sender) {
    var formsetName = $(sender).val();
    var dropdownNames = $("#FormsetVersion option").map(function () { return this.innerText; }).get();
    var isValid = _Hulpmiddelen.FormSetVersionID !== "-1" || ($.inArray(formsetName, dropdownNames) === -1 && formsetName !== "");

    var validFormSetNameRef = "#ValidFormSetName";
    var phaseButtonSaveRef = ".phaseButtonSave";

    if (isValid) {
        $(sender).css("background-color", "white");
        $(validFormSetNameRef).val("true");
        $(phaseButtonSaveRef).css("display", '');
    } else {
        $(sender).css("background-color", "#ffd0d0");
        if ($())
        $(validFormSetNameRef).val();
        $(phaseButtonSaveRef).hide();
    }
}

_Hulpmiddelen.CheckOrderStatus = function() {
    $(btnPlaceOrderRef).hide();

    var show = orderHasItems() && !isOrderDone();
    $(rentalPatientConsentLabelRef).removeClass("required");
    if (show && _Hulpmiddelen.IsExternalSupplier) {
        var hasRentalItems = $(orderItemsRef + "[data-aidproductgroupid='" + _Hulpmiddelen.Enums.AidProductGroupRental + "']").length > 0;
        if (hasRentalItems) {
            $(rentalPatientConsentLabelRef).addClass("required");

            if (!$(toestemmingPatientHuurartikelenRef).is(':checked')) {
                show = false;
            }
        }
    }

    if (show) {
        $(btnPlaceOrderRef).show();
    }
}


_Hulpmiddelen.ChangeOntvangerEmail = function () {
    var ontvangerBevestigingsEmailObj = $(ontvangerBevestigingsEmailSelectedIdentifierRef);
    if (ontvangerBevestigingsEmailObj.length === 0) {
        return;
    }
    var targetEmail = ontvangerBevestigingsEmailObj.val() === '1' ? "#PatientEmail" : "#TransferPuntEmail";
    if (isEmailDefault()) {
        // Override currently filled email address only if it's not a default
        $(ontvangerBevestigingsEmailAdresRef).val($(targetEmail).val());
    }
}

_Hulpmiddelen.ChangeOntvangerType = function(sender) {
    switch ($(sender).val()) {
    case '0':
        setPatientAddress();
        break;
    case '1':
        setVVTAddress();
        break;
    case '2':
        setHospitalAddress();
        break;
    case '3':
        clearOntvangerAddress();
        break;
    }
}

_Hulpmiddelen.ChangeSupplier = function(sender) {
    var hulpmiddelenToelichtingRef = "#hulpmiddelentoelichting";
    var transporterInfoRef = "#transporterinfo";
    
    $(_Hulpmiddelen.FormID).validate(); //sets up the validator

    _Hulpmiddelen.SupplierID = parseInt($(sender).val());
    _Hulpmiddelen.IsExternalSupplier = (_Hulpmiddelen.SupplierID !== _Hulpmiddelen.Enums.PointID);

    if (_Hulpmiddelen.IsExternalSupplier) {
        $(hulpmiddelenToelichtingRef).hide().find("input").val('');
        $(transporterInfoRef).show();
    } else {
        $(hulpmiddelenToelichtingRef).show();
        $(transporterInfoRef).hide().find("input").val('').removeProp("checked");
    }

    var supplierName = getSupplierName();
    var supplierPhoneNumber = getSupplierData(_Hulpmiddelen.SupplierPhoneNumberPoint, _Hulpmiddelen.SupplierPhoneNumberVegro, _Hulpmiddelen.SupplierPhoneNumberMediPoint, "");

    $(".supplier-name").text(supplierName);
    $(".supplier-phonenumber").text(supplierPhoneNumber);

    $(btnPlaceOrderRef).hide();

    var addOrderItem = $("#additem");
    addOrderItem.data("supplierinsurercode", _Hulpmiddelen.GetSupplierCode());
    addOrderItem.data("supplierid", _Hulpmiddelen.SupplierID);
    $("#ModelSupplierID").val(_Hulpmiddelen.SupplierID);
    $(orderItemsContainerRef + ' *').remove();

    $(toestemmingPatientHuurartikelenRef).removeAttr('checked');

    _Hulpmiddelen.DeleteAllAnswers();
    _Hulpmiddelen.CheckOrderStatus();
    checkSupplierDependentItemsDisplay();
}

_Hulpmiddelen.GetSupplierCode = function() {
    return getSupplierData(null, _Hulpmiddelen.ExternalSupplierVegroCode, _Hulpmiddelen.ExternalSupplierMediPointCode);
}

_Hulpmiddelen.AidProductOrderItemPopover = function (rootItemRef) {
    if (isVariableUndefined(rootItemRef)) {
        rootItemRef = "";
    }

    var obj = $(rootItemRef + " a[rel=popover]");

    if (obj.length > 0) {
        obj.popover({
            html: true,
            trigger: 'hover',
            content: function () {
                return '<img src="' + $(this).data('img') + '" style="max-width:220px" />';
            }
        });
    }
}

_Hulpmiddelen.DeleteAllAnswers = function () {
    var postData = { uniqueFormKey: _Hulpmiddelen.UniqueFormKey };

    _Hulpmiddelen.AjaxPost(_Hulpmiddelen.Urls.DeleteAllAnswers, postData, checkQuestionStatus);
}

_Hulpmiddelen.RefreshSelectedOrderItems = function() {
    var allProductIDs = [];
    $.map($(_Hulpmiddelen.OrderItemsContainerRef + " .aidproductid"), function (item) {
        allProductIDs.push($(item).val());
    });

    setOrderItems(allProductIDs.join());
    refreshOrderStatus();
}

_Hulpmiddelen.ChangeFormsetVersion = function(sender) {
    location.replace($(sender).find("option:selected").attr("data-url"));
}

/*
*************************************************
     SETUP
*************************************************
*/
$(document).ready(function () {
    $('[data-toggle="clickover"]').clickover({
        html: true,
        placement: "top",
        global_close: true,
        esc_close: true
    });

    _Hulpmiddelen.AidProductOrderItemPopover(orderItemsContainerRef);

    $(".phaseButtonDefinitive").hide();

    var dateTimeRequirementsMetGroupInputRef = "#DateTimeRequirementsMetGroup input";

    //Dit is om de woorden "is verplicht weg te halen".
    $(dateTimeRequirementsMetGroupInputRef).attr("data-val-required", "Voor zondagen of spoed: Bel " + getSupplierName() + " of kies een andere datum.");

    $(afleverDatumGroupRef + ' .date').on('dp.change', function (ev) {
        var currentDate = new Date();
        var weekday = ev.date.weekday();
        if (weekday === 6 || ev.date <= currentDate) {
            $(dateTimeRequirementsMetGroupInputRef).prop('checked', false);
        } else {
            setRadioButtonValue("#DateTimeRequirementsMet", "1");
        }
    });

    $(btnPlaceOrderRef).toggle(!_Hulpmiddelen.IsExternalSupplier);

    $(window).on("resize", function () {
        var height = GetScrollHeight();
        var scroller = $("#Scroller");
        scroller.css("max-height", height);
        scroller.css("height", height);
    });
    $(window).trigger("resize");

    if (_Hulpmiddelen.ShowQuestionWarning) {
        showQuestionListWarning();
    }
    if (_Hulpmiddelen.ShowExternalSupplierWarning) {
        showExternalSupplierWarning();
    }
    if (_Hulpmiddelen.ShowBsnWarning) {
        showBSNWarning();
    }

    $('#BevestigingsEmailTarget [data-toggle="tooltip"]').tooltip({
        placement: 'top'
    });

    $(ontvangerBevestigingsEmailAdresRef).keyup(function () {
        ontvangerEmailChanged();
    });

    $(ontvangerBevestigingsEmailAdresTransferpuntRef).keyup(function () {
        ontvangerEmailMediPointChanged(this);
    });
    $(ontvangerBevestigingsEmailAdresPatientRef).keyup(function () {
        ontvangerEmailMediPointChanged(this);
    });

    areyousure = false;
    if ($(ontvangerBevestigingsEmailAdresTransferpuntRef).length > 0) {
        $(ontvangerBevestigingsEmailAdresTransferpuntRef).keyup();
    }
    if ($(ontvangerBevestigingsEmailAdresPatientRef).length > 0) {
        $(ontvangerBevestigingsEmailAdresPatientRef).keyup();
    }

    setRadioButtonValue("#QuestionsAnswered", $("#QuestionsAnsweredAll").val());  // Calculated value from Model

    _Hulpmiddelen.ChangeOntvangerEmail();
    ontvangerEmailChanged();
    _Hulpmiddelen.CheckOrderStatus();
    checkSupplierDependentItemsDisplay();
    checkEnableCurrentOrder();
    checkDisplayMessage();
    areyousure = true;
});