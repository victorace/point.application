﻿$(function () {

    var registrationEndedWithoutTransfer = $("#RegistrationEndedWithoutTransfer:checked").val();
    $("#ReasonEndRegistrationContainer").toggle(registrationEndedWithoutTransfer == "true");

    $("#RegistrationEndedWithoutTransfer").on("change", function () {
        if (!$(this).checked)
            $("#ReasonEndRegistration").val('');

        $("#ReasonEndRegistrationContainer").toggle($(this).checked);
    });
});
