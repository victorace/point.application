﻿function handleTelephoneContactpersoon(data, targetselector) {
    if (data !== undefined && data.length === 1) {
        $(targetselector).val(data[0].Text);
    }
}

function handleEmailContactpersoon(data, targetselector) {
    if (data !== undefined && data.length === 1) {
        $(targetselector).val(data[0].Text);
    }
}

function Round2Decimal(number) {
    return Math.round(number * 100) / 100;
}

function ClearCheckActive(sender) {
    if ($(sender).closest(".uitklapcontainer").find("input:checked, input[type=text]:filled, textarea:filled").length == 0) {
        $(sender).closest("li").removeClass("active");
        var activeparent = $(sender).closest("li.active");
        if (activeparent.length > 0)
            if (activeparent.find("li.active").length == 0)
                activeparent.removeClass("active");
    }
}

function RecalcHours()
{
    var incomplete = false;
    var totalminspermonth = 0;

    $("#verwachteinspanning").text("");
    $(".timetrigger-container").each(function () {
        var isprof = $(this).find(".timetrigger-actor-pro:checked").length > 0;
        var amount = $(this).find(".timetrigger-amount").val();
        var timespan = $(this).find(".timetrigger-timespan:checked").val();
        var mins = $(this).find(".timetrigger-mins").val();

        if (isprof) {
            var rowisincomplete = amount == '' || timespan == undefined || mins == '';

            if (!rowisincomplete)
                totalminspermonth += amount * (28 / timespan) * mins;
                //$("#verwachteinspanning").text($("#verwachteinspanning").text() + "\r\n" + amount + "-" + timespan + "-" + mins);
            else
                incomplete = true;
        }
    });

    $("#verwachteinspanning").html(Round2Decimal(totalminspermonth / 60) + ' uur/maand');
    if (incomplete)
        $("#verwachteinspanning").html($("#verwachteinspanning").html() + " <i>(sommige akties zijn niet compleet ingevuld)</i>");
}



$(function () {
    $(window).on("resize", function () {
        var height = GetScrollHeight();
        $("#Scroller").css("max-height", height);
    });
    $(window).trigger("resize");

    $('#OrganisatieNaamContactpersoon').on("change", function () {
        var employeeID = $(this).val();
        var digest = $(this).find(":selected").data("digest");
        $.ajax({
            type: "POST",
            url: getEmployeePhoneNumberByEmployeeID,
            data: { employeeID: employeeID, digest: digest },
            success: function (data) { handleTelephoneContactpersoon(data, '#OrganisatieTelefoonnummer'); }
        });
        $.ajax({
            type: "POST",
            url: GetEmployeeEmailAddressByEmployeeID,
            data: { employeeID: employeeID },
            success: function (data) { handleEmailContactpersoon(data, '#OrganisatieEmail'); }
        });
        GetEmployeeEmailAddressByEmployeeID
    });

    //Noodzakelijk te regelen hulpmiddelen
    $("#NoodzakelijkTeRegelenHulpmiddelen").on("change", function () {
        $("#divNoodzakelijkTeRegelenHulpmiddelen").toggle($(this).is(":checked"));
        if ($(this).is(":checked") == false) {
            ClearContainer("#divNoodzakelijkTeRegelenHulpmiddelen");

            $("#divHulpmiddelenAnders").toggle($(this).is(":checked"));
            ClearContainer("#divHulpmiddelenAnders");

            $("#divAfleveringAnderAdres").toggle($(this).is(":checked"));
            ClearContainer("#divAfleveringAnderAdres");
        }
    });
    $("#NoodzakelijkTeRegelenHulpmiddelen").each(function () {
        $("#divNoodzakelijkTeRegelenHulpmiddelen").toggle($(this).is(":checked"));
        if ($(this).is(":checked") == false) {
            ClearContainer("#divNoodzakelijkTeRegelenHulpmiddelen");

            $("#divHulpmiddelenAnders").toggle($(this).is(":checked"));
            ClearContainer("#divHulpmiddelenAnders");

            $("#divAfleveringAnderAdres").toggle($(this).is(":checked"));
            ClearContainer("#divAfleveringAnderAdres");
        }
    }); //trigger("change");

    CheckboxToggleChildren("#NoodzakelijkTeRegelenHulpmiddelenAnders", "#divHulpmiddelenAnders");

    var afleveringName = GetNameByID("#NoodzakelijkTeRegelenHulpmiddelenAflevering");
    $("[name='" + afleveringName + "']input:radio").on("click", function () {
        var afleveringDestination = $(this).val();
        $("#divAfleveringAnderAdres").toggle(afleveringDestination == "anders");
        if (afleveringDestination == "Thuis" || afleveringDestination == "Ziekenhuis")
            ClearContainer("#divAfleveringAnderAdres");
    });
    GetSelectedRadioByName(afleveringName).click();

    SelectToggleChildren("#PsychischFunctionerenDwangEnDrangInterventie", "#PsychischFunctionerenDwangEnDrangInterventieChildren", []);
    RadioToggleChildren("#PsychischFunctionerenWilsbekwaamSprakeVan", "#PsychischFunctionerenWilsbekwaamSprakeVanChildren", ["Nee"]);
    RadioToggleChildren("#PsychischFunctionerenVrijheidsbeperkendeInterventiesAanwezig", "#PsychischFunctionerenVrijheidsbeperkendeInterventiesAanwezigChildren", ["Ja"]);
    RadioToggleChildren("#KinderenAanwezig", "#KinderenAanwezigChildren", ["Ja"]);
    RadioToggleChildren("#LevensOvertuigingWilsverklaringAanwezig", "#LevensOvertuigingWilsverklaringAanwezigChildren", ["Ja"]);

    $("#zorgadviesdata").find("input[type=text],input[type=radio],input[type=checkbox],textarea,select").on("change keyup", "", CheckFilled);
    $("#zorgadviesdata .collapse").find("input:checked, input[type=text]:filled, textarea:filled").closest(".collapse").parents("li").addClass("active");

    $(".timetrigger").on("change keyup", "", function () {
        RecalcHours();
    });
    RecalcHours();
});

function CheckFilled()
{
    var isfilled = this.checked || $(this).is("select:filled") || $(this).is("textarea:filled") || $(this).is("[type=text]:filled");
    var item = $(this);

    if (isfilled) {
        $(this).parents("li").addClass("active");
    }
    else {
        if ($(this).closest(".uitklapcontainer").find("input:checked, input[type=text]:filled, textarea:filled").length == 0) {
            $(this).closest("li").removeClass("active");
            var activeparent = $(this).closest("li.active");
            if (activeparent.length > 0)
                if (activeparent.find("li.active").length == 0)
                    activeparent.removeClass("active");
        }

    }
}

