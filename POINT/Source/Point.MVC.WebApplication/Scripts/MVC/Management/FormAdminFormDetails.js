﻿var reload = function () {
    location.reload();
};

function SaveFormDetails(sender) {
    var requestUrl = "/Management/FormAdmin/SaveFormDetails";
    var formID = $(sender).closest("form");

    AjaxSave(formID, requestUrl, null, reload, true);
}

function DeleteFormDetails(url) {
    $.ajax({
        type: "POST",
        cache: false,
        url: url,
        async: true,
        success: reload
    });
}