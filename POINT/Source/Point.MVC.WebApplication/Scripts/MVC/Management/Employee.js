﻿function changeRegion(element) {
    document.location.href = "?regionid=" + $(element).val();
}

function changeOrganization(element) {
    var regionid = $("#RegionID").val();
    document.location.href = "?regionid=" + regionid + "&organizationid=" + $(element).val();
}

function ShowAll()
{
    $("#ShowAll").prop("checked", true);
    $("#searchButton").trigger("click");
}


function deletesignature(sender) {
    $.ajax({
        type: "GET",
        url: deletesignatureurl,
        async: false,
        success: function (data) {
            $("#digitalsignature").attr("src", getimageurl);
            $("#removesignature").hide();
            $("#deletesignaturemodal").modal("hide");
        },
        error: function (data) {
            $("#deletesignaturemodal .modal-body").html(data.responseText);
        }
    });
}

function uploadsignature(sender) {
    var options = {
        url: uploadsignatureurl,
        type: "POST",
        dataType: "JSON",
        clearForm: true,
        success: function (data, textStatus, jqxhr, form) {
            if (!data.Success)
                showErrorMessage($.parseJSON(jqxhr.responseText).ErrorMessage);
            else {
                $("#digitalsignature").attr("src", getimageurl);
                $("#removesignature").show();
                $("#modal2").modal("hide");
            }
        }
    };
    $("#signatureform").ajaxSubmit(options);
}

function previewsignature(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $("#imgpreviewsignature").attr("src", e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}