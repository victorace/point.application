﻿function LocationChanged(sender) {
    CalcFullName();
}

function CalcFullName() {
    if ($("#OrganizationID").val() === '') {
        $(".concatname").text('');
        return;
    }

    var organizationname = $("#OrganizationID option:selected").text();
    var locationname = $("#Name").val();

    var concatname = organizationname;
    if (locationname != "Locatie 1" && locationname != "") {
        concatname += ' - ' + locationname;
    }

    $(".concatname").text(concatname);
}

$(function () {
    $('[data-toggle="clickover"]:not(.client-header)').clickover({
        html: true,
        placement: "left",
        global_close: true,
        esc_close: true,
        title: $(this).data("title"),
        content: $(this).data("content")
    });
})