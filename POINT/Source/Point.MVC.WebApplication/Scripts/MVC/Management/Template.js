﻿$(document).ready(function () {

    var form = $("#TemplateZorgMail");

    $("#ddlTemplateTypeID").on("change", function () {
        var templateTypeID = $(this).val();
        document.location.href = "?templateTypeID=" + templateTypeID;
    });

    $("#ddlOrganizations").on("change", function () {
        var organizationid = $(this).val();
        var templatetypeid = $("input:hidden#TemplateTypeID").val();
        document.location.href = "?templateTypeID=" + templatetypeid + "&organizationid=" + organizationid;
    });

    $("#saveTemplate").click(function () {
        var organizationid = $("#ddlOrganizations").val();
        var templatetypeid = $("input:hidden#TemplateTypeID").val();
        if (organizationid !== "" && templatetypeid !== "") {
            AjaxSave(form, form.attr("action"), true);
        }
    });

    $("#loadDefault").click(function (e) {
        var url = $(this).data("url");
        $("#areyousuremodal").modal("show")
            .one("click", "#btniamsure", function () {
                $.ajax({
                    type: "GET",
                    cache: false,
                    url: url
                }).done(function (data) {
                    if (!isNullOrEmpty(data)) {
                        var templateid = $("input:hidden#TemplateID").val();
                        var templatetext = $("#TemplateText").text();
                        $("#TemplateText").val(data.TemplateDefaultText);
                        if (templateid !== "0" && templatetext !== data.TemplateDefaultText) {
                            $("#saveTemplate").trigger("click");
                        } else {
                            $("#areyousuremodal").modal("hide");
                            location.reload();
                        }
                    }
                });
            });
    });

    $("#deleteTemplate").click(function () {
        var url = $(this).data("url");
        if (!isNullOrEmpty(url)) {
            $("#areyousuremodal").modal("show")
                .one("click", "#btniamsure", function () {
                    $.ajax({
                        type: "POST",
                        cache: false,
                        data: form.serialize(),
                        url: url
                    }).done(function (data) {
                        if (data !== undefined) {
                            location.replace(data.Url);
                        }
                    });
                });
        }
    });

    $("#showExample").click(function (e) {
        var url = $(this).data("url");
        if (!isNullOrEmpty(url)) {
            var transferid = $("#TransferID").val();
            if (!isNullOrEmpty(transferid)) {
                var ajaxcall = $.ajax({
                    type: "POST",
                    cache: false,
                    url: url,
                    data: form.serialize()
                }).done(function (data) {
                    $("#modal .modal-body").empty();
                    $("<div>").addClass("plaintext").text(data).appendTo("#modal .modal-body");
                    $("#modal").modal("show");
                });
            } else {
                showErrorMessage("U heeft geen TransferID ingevuld");
            }
        }
    });

    if (canbeorganizationspecific) {
        $("#TemplateText").on("change keypress", function () {
            $("#TemplateText").toggleClass("defaulttemplate", false);
            $("#loadDefault").show();
        });
        $("#TemplateText").toggleClass("defaulttemplate", isdefault);
    }
});