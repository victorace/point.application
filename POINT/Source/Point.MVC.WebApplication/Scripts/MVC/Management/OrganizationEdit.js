﻿
var organizationProjectModalRef = "#organizationprojectmodal";
var tableOrganizationProjectRef = "#tbodyorganizationproject";

function reorderOrganizationProjectIndex() {
    $(tableOrganizationProjectRef + " tr").each(function (rowindex, row) {
        $(row).find("input:hidden").each(function (inputindex, input) {
            var name = $(input).attr("name").replace(/\[.*?\]/g, '[' + rowindex + ']');
            $(input).attr("name", name);
        });
    });
}

function updateOrganizationProjectCount() {
    var rowCount = $(tableOrganizationProjectRef).find("tr").length;
    $("#organizationProjectCount").text(rowCount);
}


/* **********************************************************************
 *  _OrganizationEdit Global Object (Defined in Organization\Edit.cshtml)
***********************************************************************/
 
_OrganizationEdit.Functions = {
    AddOrganizationProject: function () {
        var name = $("#projectname");
        name.val("");
        $(organizationProjectModalRef).modal("show")
            .one("click",
                "#ok",
                function () {
                    var id = -1;
                    var value = name.val();
                    if ($.trim(value).length !== 0) {
                        var template = "<tr><td style='display:none;'> \
                <input data-property='organizationprojectid' type='hidden' name='OrganizationProjects[{0}].OrganizationProjectID' value='{1}'> \
                <input data-property='name' type='hidden' name='OrganizationProjects[{0}].Name' value='{2}'></td> \
                <td class='clickable' data-property='name' onclick='_OrganizationEdit.Functions.EditOrganizationProject(this)'>{2}</td> \
                <td><span onclick='_OrganizationEdit.Functions.RemoveOrganizationProject(this)' class='btn glyphicon glyphicon-remove color-pt-red'></span></td></tr>";
                        var html = $.validator.format(template, $(tableOrganizationProjectRef + " tr").length, id, value);
                        $(tableOrganizationProjectRef).append(html);
                    }
                    $(organizationProjectModalRef).modal("hide");
                    updateOrganizationProjectCount();
                });
    },
    DeleteLogo: function (sender) {
        $.ajax({
            type: "GET",
            url: _OrganizationEdit.Urls.DeleteLogo,
            async: false,
            success: function (data) {              
                $("#imgorganizationlogo").removeAttr("src");
                $("#removelogo").hide();
                $("#deletelogomodal").modal("hide");
            },
            error: function (data) {
                $("#deletelogomodal .modal-body").html(data.responseText);
            }
        });
    },
    EditOrganizationProject: function (sender) {
        var row = $(sender).closest("tr");
        var name = $("#projectname");
        name.val(row.find("td[data-property='name']").html());
        $(organizationProjectModalRef).modal("show")
            .one("click", "#ok", function () {
                var value = name.val();
                if ($.trim(value).length !== 0) {
                    row.find("[data-property='name']").each(function (index, element) {
                        if ($(element).is("td")) { $(element).html(value); }
                        else { $(element).val(value); }
                    });
                }
                $(organizationProjectModalRef).modal("hide");
            });
    },
    OrganizationTypeChanged: function (sender) {
        var type = $(sender).val();
        var isVVT = type === _OrganizationEdit.OrganizationTypeVVT;

        $("li.serviceareas").toggle(isVVT);
        $("div.serviceareas").toggle(isVVT);
    },
    PreviewLogo: function (input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $("#imgpreviewlogo").attr("src", e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    },
    RemoveOrganizationProject: function (sender) {
        $("#organizationprojectdeletemodal").modal("show")
            .one("click",
                "#delete",
                function () {
                    $(sender).closest("tr").remove();
                    reorderOrganizationProjectIndex();
                    updateOrganizationProjectCount();
                });
    },

    UploadLogo: function (sender) {
        var options = {
            url: _OrganizationEdit.Urls.UploadLogo,
            type: "POST",
            dataType: "JSON",
            clearForm: true,
            success: function (data, textStatus, jqxhr, form) {
                if (!data.Success)
                    showErrorMessage($.parseJSON(jqxhr.responseText).ErrorMessage);
                else {
                    $("#modal2").modal("hide");
                    $("#imgorganizationlogo").attr("src", _OrganizationEdit.Urls.GetLogo);
                    $("#removelogo").show();                   
                }
            }
        };
        $("#organizaitonlogoform").ajaxSubmit(options);
    }
};