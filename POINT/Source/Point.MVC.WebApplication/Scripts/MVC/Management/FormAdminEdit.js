﻿var reload = function () {
    location.reload();
};

function SaveAttributes(sender) {
    var requestUrl = "/Management/FormAdmin/UpdateAttributes";
    var formID = $(sender).closest("form");

    AjaxSave(formID, requestUrl, null, reload, true);
}

function DeleteAttribute(url) {
    $.ajax({
        type: "POST",
        cache: false,
        url: url,
        async: true,
        success: reload
    });
}