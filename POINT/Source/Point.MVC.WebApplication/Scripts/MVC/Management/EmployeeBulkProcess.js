﻿function visibledate() {
    $("#lastlogindateholder").show();
    $("#employeebulklist").empty();
}

function invisibledate() {
    $("#lastlogindateholder").hide();
    $("#LastLoginDate").val('');
    $("#employeebulklist").empty();
}

$(function () {

    var height = GetScrollHeight();

    if ($.fn.dataTable.isDataTable('#employeeBatchtable')) {
        table = $('#employeeBatchtable').DataTable();
    }
    else {
        table = $('#employeeBatchtable').DataTable({
            "paging": false,
            "order": [],
            "dom": 'flrt'
        });
    }

    $('[data-toggle="clickover"]').clickover({
        html: true,
        global_close: true
    });
    $(window).on("resize", function () {        
		$("#Scroller").css("max-height", height);
        $("#Scroller").css("height", height);      
    });

	$(window).trigger("resize");

	$('#chkAll').change(function () {
		var checkboxes = $(".employeeCheck");
		if ($(this).is(':checked')) {
			checkboxes.prop('checked', true);
		} else {
			checkboxes.prop('checked', false);
		}
    });
});