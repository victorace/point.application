﻿
var selectedRegionRef = "#RegionID";
var selectedOrganizationRef = "#OrganizationID";

function showDoctorsMessage() {
    hideAlerts();

    if (isNullOrEmpty($(selectedRegionRef).val())) {
        $("#regionrequiredmessage").show();
        $(selectedRegionRef).focus();
        return;
    }

    if (isNullOrEmpty($(selectedOrganizationRef).val())) {
        $("#organizationrequiredmessage").show();
        $(selectedOrganizationRef).focus();
        return;
    }

    ShowModal(createDoctorUrl);
};

function hideAlerts() {
    $("#doctorIndexPlaceHolder .alert.alert-danger").hide();
}

function changeRegion() {
    changeRegionOrganization(false);
}

function changeOrganization() {
    changeRegionOrganization(true);
}

function changeRegionOrganization(getOrganization) {
    hideAlerts();

    var selectedRegion = $("#RegionID option:selected").val();

    if (selectedRegion === '') {
        selectedRegion = $("#PointUserRegionID").val();
    }

    var inputData = "regionID=" + selectedRegion;

    if (getOrganization) {
        var selectedOrganization = $("#OrganizationID option:selected").val();
        if (selectedOrganization !== "") {
            inputData += "&organizationID=" + selectedOrganization;
        }
    }

    ajaxRequest(inputData, doctorPageUpdateUrl);
}

function ajaxRequest(inputData, url) {
    if (!inputData) {
        return;
    };

    $.ajax({
        type: "POST",
        url: url,
        data: inputData,
        dataType: "json",
        success: function (data) { replaceData(data); },
        error: function (xhr, status, error) { showErrorMessage(error); }
    });
}

function replaceData(data) {
    $.each(data.htmlReplaces || [],
        function (id, html) {
            if ($(id).length) {
                $(id).replaceWith($(html));
            };
        });

}