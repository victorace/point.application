﻿function LocationChanged(sender) {
    CalcFullName();
}

function DepartmentChanged(sender) {
    CalcFullName();
}

function DepartmentTypeIDChanged(sender) {
    $.get(urlGetOrganizationDetailsByOrganizationID, { OrganizationID: $("#OrganizationID").val() }, function (data) {
        $("#capacityfields").toggle(data.OrganizationTypeID === '2' || $(sender).val() === '2');
    });
}

function CalcFullName()
{
    if ($("#OrganizationID").val() === '' || $("#LocationID").val() === '') {
        $("#concatname").text('');
        return;
    }

    var organizationname = $("#OrganizationID option:selected").text();
    var locationname = $("#LocationID option:selected").text();
    var department = $("#Name").val();

    var concatname = organizationname;
    if(locationname !== "Locatie 1" && locationname !== "")
    {
        concatname += ' - ' + locationname;
    }

    if (department !== "Afdeling 1" && department !== "") {
        concatname += ' - ' + department;
    }

    $("#concatname").text(concatname);
}

function OrganizationChanged(sender) {
    var organizationid = $(sender).val();

    $.get(urlgetlocationsbyorganization, { OrganizationID: organizationid }, function (data) { fillSelectWithOptions($("#LocationID"), data, { Value: "", Text: " - Maak uw keuze - " }); }),
        $.get(urlgetdepartmenttypebyorganizationid, { OrganizationID: organizationid }, function (data) { fillSelectWithOptions($("#DepartmentTypeID"), data, { Value: "", Text: " - Maak uw keuze - " }); });
    $.get(urlgetaftercaretypesbyorganizationid, { OrganizationID: organizationid }, function (data) { fillSelectWithOptions($("#AfterCareType1ID"), data, { Value: "", Text: " - Maak uw keuze - " }); });
    $.get(urlGetOrganizationDetailsByOrganizationID, { OrganizationID: organizationid }, function (data) {
        $("#vvtfields").toggle(data.OrganizationTypeID === '2');
        $("#capacityfields").toggle(data.OrganizationTypeID === '2' || $("#DepartmentTypeID").val() === '2');
        $("li.serviceareas").toggle(data.OrganizationTypeID !== '1');
        $("div.serviceareas").toggle(data.OrganizationTypeID !== '1');
    });
        
    clearSelect($("#LocationID"));
    clearSelect($("#DepartmentTypeID"));
    clearSelect($("#AfterCareType1ID"));

    CalcFullName();
}

$(function () {
	$('[data-toggle="clickover"]:not(.client-header)').clickover({
        html: true,
        placement: "left",
        global_close: true,
        esc_close: true,
        title: $(this).data("title"),
        content: $(this).data("content")
    });
});

