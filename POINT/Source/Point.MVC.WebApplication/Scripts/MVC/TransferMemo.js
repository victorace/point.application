﻿function partial(func /*, 0..n args */) {
    var args = Array.prototype.slice.call(arguments, 1);
    return function () {
        var allArguments = args.concat(Array.prototype.slice.call(arguments));
        return func.apply(this, allArguments);
    };
}

function attachclickover() {
    $('[data-toggle="clickover"]:not(.client-header)').clickover({
        html: true,
        placement: "top",
        global_close: true,
        esc_close: true,
        content: $(this).data("content")
    });
}

function GetContainer(button) {
    return $(button).closest(".transferMemoContainer");
}

function GetModal(button) {
    return GetContainer(button).find(".transfermemomodal");
}

function ShowMemoModal(sender, requestUrl) {

    var modal = GetModal(sender);
    var templatetext = $(sender).data("templatetext");

    $.ajax({
        type: "GET",
        url: requestUrl,
        data: { "TemplateText": templatetext },
        async: false,
        beforeSend: function () {
            $(modal).on('shown.bs.modal', function (e) {
                $(modal).find('textarea').focus();
            });
            $(modal).on("hidden.bs.modal", function () {
                $(sender).removeData("templatetext");
            });
            $(modal).modal({ backdrop: 'static' });
        },
        success: function (data) {
            $(modal).find(".modal-body").html(data);
        },
        error: function (data) {
            $(modal).find(".modal-body").html(data.responseText);
        }
    });
}

function TransferMemoNotification(sender) {
    var url = $(sender).data("notifyurl");
    $.ajax({
        type: "POST",
        url: url,
        success: function (data) {
            $("#transferMemoNotification").modal("show");
            setTimeout(function () { $('#transferMemoNotification').modal('hide'); }, 4000);
        }
    });
}

function EditTransferMemo(sender) {
    ShowMemoModal($(sender), $(sender).data("editurl"));
}

function SelectTemplate(sender) {
    ShowMemoModal($(sender), $(sender).data("editurl"));
}

function UseTemplate(sender) {
    var templatetext = $(sender).find(".templatetext").text();

    var addButton = GetContainer(sender).find(".addButton");
    addButton.data("templatetext", templatetext);

    EditTransferMemo(addButton);
}

function SaveTransferMemo(sender) {
    $(sender).attr("disabled", "disabled");
    AjaxSave($(sender).closest("form"), $(sender).data("saveurl"), false, partial(ReloadTransferMemos, sender), true, null, false);
}

function ReloadTransferMemos(sender) {
    var container = GetContainer(sender);

    var listurl = container.data("listurl");
    GetModal(sender).modal('hide');
    AjaxLoad('', listurl, container.find(".transferMemoEntries"), attachclickover);
}

function highlightTransferMemo() {
    var transfermemoid = $.getUrlVar(window.location.href, "transfermemoid");
    if (transfermemoid !== undefined) {
        var element = $("#tme" + transfermemoid);
        if (element.length > 0) {
            var position = element.offset().top;
            $(window).scrollTop(position);
        }
    }
}

function showhistory(sender) {
    $('.memohistorydiv').hide();
    var activehistory = "memohistoryid" + $(sender).data("openhistoryid");

    if ($(sender).hasClass('plus')) {
        $('.hashistory').removeClass('minus').addClass('plus');
        $(sender).removeClass('plus').addClass('minus');        
        document.getElementById(activehistory).style.display = "block"; 
    }
    else
    {
        $(sender).removeClass('minus').addClass('plus');        
    }

}

function ShowRelatedMemoWarning() {
    var showmessage = $('#ShowTakeOverMessage').val();
    if (showmessage === 'true' || showmessage === 'True') {
        $(".prefilledRelatedMemo").modal("show");
    }
}

function DeleteRelatedMemo(sender) {
    var url = $(sender).data("deleteurl");
    $(".flowmemodeletemodal").modal("show")
        .one("click", "#delete", function () {
            $.ajax({
                type: "GET",
                url: url,
                success: function (data, textStatus, jqxhr) {
                    ReloadTransferMemos(sender);
                },
                error: function (xhr, status, error) {
                    var msg;
                    try {
                        var response = JSON.parse(xhr.responseText);
                        msg = response.ErrorMessage;
                    }
                    catch (ex) {
                        msg = xhr.statusText;
                    }
                    showErrorMessage(msg);
                    form.valid();
                }
            });
        });
}
$(function () {

    attachclickover();

    if (typeof window.transfertask === "undefined") {
        $(".btntransfertaskfrommemo").each(function (i, item) {
            $(this).addClass("disabled");
        });
    }
    highlightTransferMemo();
    ShowRelatedMemoWarning();
});