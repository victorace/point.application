﻿$(function () {

    initDataTable("#attachmentsTable");

    $('.deleteAttachment').on('click', function (e) {
        e.preventDefault();
        var url = $(this).data("url");
        $('#dialog-confirm').modal('show')
            .one('click', '#delete', function () {
                $.ajax({
                    type: "GET",
                    url: url,
                    success: function (data) {
                        location.replace(transferAttachmentListUrl);
                    }
                });
            });
    });

    $('.insertAttachment').on('click', function (e) {
        //e.preventDefault();
        $('#dialog-insert').modal('show');
        var url = $(this).data("url");
        $.ajax({
            type: "GET",
            url: url,
            success: function (data) {
                $("#InsertAttachment").html(data);
            }
        });
    });

    $('.editAttachment').on('click', function (e) {
        //e.preventDefault();
        $('#dialog-edit').modal('show');
        var url = $(this).data("url");
        $.ajax({
            type: "GET",
            url: url,
            success: function (data) {
                $("#EditAttachment").html(data);
            }
        });
    });

    var formInsertOptions = {
        url: transferAttachmentInsertUrl,
        type: 'post',
        dataType: 'json',
        clearForm: true,
        success: function (data, textStatus, jqxhr, form) {
            if (data.ErrorMessage)
                showErrorMessage($.parseJSON(jqxhr.responseText).ErrorMessage);
            else if (typeof (transferAttachmentListUrl) !== "undefined")
                location.replace(transferAttachmentListUrl);
            else
                $('#dialog-insert').modal('hide');
        }
    };

    var formEditOptions = {
        url: transferAttachmentUpdateUrl,
        type: 'post',
        dataType: 'json',
        clearForm: false,
        success: function (data, textStatus, jqxhr, form) {
            if (data.ErrorMessage)
                showErrorMessage($.parseJSON(jqxhr.responseText).ErrorMessage);
            else if (typeof (transferAttachmentListUrl) !== "undefined")
                location.replace(transferAttachmentListUrl);
            else
                $('#dialog-edit').modal('hide');
        }
    };

    $(function () {
        $("#dialog-insert #btnUpload").click(function (ev) {
            $("#dialog-insert #transferAttachmentForm").ajaxSubmit(formInsertOptions);
        })
        $("#dialog-edit #btnSave").click(function (ev) {
            $("#dialog-edit #transferAttachmentForm").ajaxSubmit(formEditOptions);
        })
    });

})
