﻿var fillFieldsFromData = function (data, prefix) {
    var hasPrefix = false;
    if (typeof prefix !== 'undefined') {
        hasPrefix = true;
    }

    var huisartsskipped = false;

    if (data["GeneralPractitionerZorgmail"] !== undefined && data["AddressGPForZorgmail"] === undefined) {
        data["AddressGPForZorgmail"] = data["GeneralPractitionerZorgmail"]; 
    }

    if (data["NumberAddition"] !== undefined && data["HuisnummerToevoeging"] === undefined) {
        data["HuisnummerToevoeging"] = data["NumberAddition"]
    }

    for (var fieldname in data) {
        var field = $(":input[name='" + (hasPrefix ? prefix + "." + fieldname : fieldname) + "']");

        if (field.prop("type") === "hidden" || field.length === 0) {
            continue;
        }

        if (fieldname === "GeneralPractitionerName" || fieldname === "GeneralPractitionerPhoneNumber" || fieldname === "AddressGPForZorgmail") {
            //"Huisarts" niet vullen als het zorgmailadres al reeds is ingevuld
            if ($("#AddressGPForZorgmail").val() !== "") {
                //Alleen als er daadwerkelijk zou worden geimporteerd daarvan een melding geven
                if (fieldname === "" && data[fieldname] !== "") {
                    huisartsskipped = true;
                }

                continue;
            }
        }

        if (fieldname.match(/Date/)) {
            if (data[fieldname] === null) {
                field.val('');
            }
            else {
                var dateformat = field.data('dateformat');
                field.val(new Date(data[fieldname]).format(dateformat));
            }
        }
        else if (field.is(':radio')) {
            field.removeProp("checked");
            field.filter("input[value='" + data[fieldname] + "']:first").prop("checked", true).trigger("change");
        }
        else {
            field.val(data[fieldname]);
        }
    }

    if (huisartsskipped === true) {
        showWarningMessage("Huisarts veld is overgeslagen omdat deze al was ingevuld.");
    }

    setPatientNumberAccessibility();
    prefillInsuranceNumber();
};

$.validator.setDefaults({ ignore: '.ignore *' });

function PutClientSearchData(data) {
    fillFieldsFromData(data.Client);
    if (data.HasContactPerson) {
        fillFieldsFromData(data.ContactPerson, "ContactPerson");
    }
};


function setPatientNumberAccessibility() {
    $.each($("#PatientNumber"), function () {
      if (disablePN)
      {
          $("#PatientNumber").prop('readonly', true);
      }
    });
}

$(function () {

    $("label[for='CivilServiceNumber']").addClass("required");
    $("#CivilServiceNumber").attr("data-val-required", "Burger Service Nummer is verplicht.");

    setBSNRequirement();
    $("#HasNoCivilServiceNumber").click(function () {
        setBSNRequirement();
    });

    if (pharmacyRequired) {
        $("label[for='PharmacyName'").addClass("required");
        $("#PharmacyName").rules('add', { required: true, messages: { required: "Apotheek is verplicht." }});
    }

    $(window).on("resize", function () {
        var height = GetScrollHeight();
        $("#Scroller").css("max-height", height);
        $("#Scroller").css("height", height);
    });
    $(window).trigger("resize");

    $("#btnImportClient").click(function () {
        var searchData = { CivilServiceNumber: $("#CivilServiceNumber").val() };
        GetClientDataByBSN(importClientUrl, searchData);
    });

    function ShowModal(requestUrl) {
        $.ajax({
            type: "POST",
            url: requestUrl,
            data: $(this).data(),
            async: true,
            success: function (data) {
                if (data.no_results == undefined) {
                    $("#modal").modal('show');
                    $("#modal .modal-content").data();
                    $("#modal .modal-body").html(data);
                }
            },
            error: function (data) {
                $("#modal").modal('show');
                $("#modal .modal-body").html(data.responseText);
            }
        });
    }


    $("#btnSearchByPatientInfo").click(function (sender) {
        var bsn = $("#CivilServiceNumber").val();
        var url = searchByPatientInfoUrl + "?search=" + bsn;
        ShowModal(url);
    });

    $(formID + " .label").each(function () {
        $(this).html($(this).html() + ":");
    });

    $(".showContact").each(function () {
        $(this).click(function () { $(this).nextAll("div").eq(0).toggle(); });
    });

    $('#btnValidate').on('click', function () {
        $(formID).valid();
    });

    $.each($("#clientForm[data-disabled='disabled'] :input"), function () {
        $(this).attr("disabled", "");
    });

    $('[data-toggle="clickover"]:not(.client-header)').clickover({
        html: true,
        placement: "top",
        global_close: true,
        esc_close: true,
        title: $(this).data("title"),
        content: $(this).data("content")
    });

    $("#btnNedap").on("click", function () {
        var searchData = { CivilServiceNumber: $("#CivilServiceNumber").val(), DepartmentID: departmentID };
        GetClientDataByBSN(nedapClientUrl, searchData);
    });

    $("#HealthInsuranceCompanyID").on("change", function () {
        prefillInsuranceNumber();
    });

    setPatientNumberAccessibility();

    $("#btnContactPersonUnknown").click(function () {
        prefillContactPersonUnknown();
    }); 
});

function GetClientDataByBSN(url, searchData) {

    var error = ControleBsn($("#CivilServiceNumber").val());

    if (error > '') {
        showErrorMessage(error, true);
    } else {
        $.ajax({
            type: "Post",
            url: url,
            datatype: 'json',
            data: searchData,
            async: true,
            success: function (data) {
                if (data.HasClient) {
                    fillFieldsFromData(data.Client);
                    if (data.HasContactPerson) {
                        fillFieldsFromData(data.ContactPerson, "ContactPerson");
                    }                
                }
                else {
                    showErrorMessage("Geen patient gevonden met dit BSN");
                }
            },
            error: function (jqxhr, textStatus, errorThrown) {
                showErrorMessage($.parseJSON(jqxhr.responseText).ErrorMessage, true);
            }
        });
    }

}

function setBSNRequirement() {
    var hasNoBsn = $("#HasNoCivilServiceNumber").prop('checked');
    var orgbsnrequiredstr = $("#BSNRequired").val();
    var orgbsnrequired = orgbsnrequiredstr === 'true' || orgbsnrequiredstr === 'True';

    //bsnRequired is a combination of bsn flowfield reqired (for organization) and patient has bsn or not.     
    bsnRequired = orgbsnrequired && !hasNoBsn;

    if (!hasNoBsn) {
        $("#CivilServiceNumber").prop('disabled', false);
    }
    else if (hasNoBsn) {
        $("#CivilServiceNumber").val('');
        $("#CivilServiceNumber").prop('disabled', true);
    }

}

function prefillContactPersonUnknown() {
    $("#ContactPerson_LastName").val("nog niet bekend");
    $("#ContactPerson_PhoneNumberGeneral").val("nog niet bekend");
}

function prefillInsuranceNumber() {
    if (healthinsurerIDsNotInsured.indexOf(parseInt($("#HealthInsuranceCompanyID").val(), 10)) >= 0 && $("#InsuranceNumber").val() === "") {
        $("#InsuranceNumber").val("n.v.t.");
    }
}

function ShowModalopen(requestUrl, callback, afterload, size) {
    var modal = $("#modal2").find(".modal-dialog");
    if (modal.length === 1) {
        if (size === undefined) size = 2;
        $(modal).removeClass("modal-sm").removeClass("modal-lg");
        if (size === 1) $(modal).addClass("modal-sm");
        else if (size === 2) $(modal).addClass("modal-lg");
        // else none - modal-md
    }
    $.ajax({
        type: "GET",
        url: requestUrl,
        data: $(this).data(),
        async: false,
        beforeSend: function () {
            $("#modal2").modal("show");
        },
        success: function (data) {
            $("#modal2 .modal-content").data("callback", callback);
            $("#modal2 .modal-body").html(data);
            if (afterload) {
                afterload();
            }
        },
        error: function (data) {
            $("#modal2 .modal-body").html(data.responseText);
        }
    });

}

function CheckOpenDossier(senderdata, callback) {   
    var transferId = getQueryStringParameter("TransferID");  
    var flowDefinition = getQueryStringParameter("FlowDefinitionID");
    
    var searchData = { TransferID: transferId, BSN: $("#CivilServiceNumber").val(), DossierFlowDefinition: flowDefinition };
    $.ajax({
        type: "GET",
        url: hasClientOpenDossierUrl,
        data: searchData,
        datatype: 'json',
        async: false,
        success: function (data) {
            if (data.HasOtherOpenDossier === 'true' && data.URL !== '') {
                var afterload = function (data) {
                    $("#opendossiernee").on("click", function () {
                        $("#modal2").modal("hide");
                    });
                    $("#opendossierja").on("click", function () {
                        callback(senderdata);
                    });
                }
                ShowModalopen(data.URL,null, afterload,3);            
            } else {
                callback(senderdata);
            }
        },
        error: function (jqxhr, textStatus, errorThrown) {
            showErrorMessage($.parseJSON(jqxhr.responseText).ErrorMessage);
        }
    });
}
