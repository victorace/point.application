﻿$.validator.setDefaults({ ignore: '.ignore *' });

function SearchOrganization(sender, url, callback) {
    ShowModal(url, callback, inittable);
}

var PutTussenOrganisatie = function (department) {
    PutOrganisation(department, "AanvraagVersturenDoorTussenOrganisatie");
};

function PutOrganisation(department, targetid) {
    $('input[data-linked="' + targetid + '"]:not([type=hidden])').val(department.DepartmentName);
    $('input[data-linked="' + targetid + '"][type=hidden]').val(department.DepartmentID);
    $("#modal").modal('hide');
};

function handleEmployeeDetails(data) {
    if (data !== undefined) {
        $('#DossierOwnerTelephoneNumber').val(data.PhoneNumber);
        $('#DossierOwnerPosition').val(data.Position);
    } else {
        $('#DossierOwnerTelephoneNumber').val('');
        $('#DossierOwnerPosition').val('');
    }
}

function setEmployeeDetails() {
    var employeeID = $(this).val();
    var digest = $(this).find(":selected").data("digest");

    $.ajax({
        type: "POST",
        url: getEmployeeDetailsByEmployeeIDUrl,
        data: { employeeID: employeeID, digest: digest },
        success: function (data) { handleEmployeeDetails(data); }
    });
}

var loaded;

function SetRequestForm() {
    var opdracht = '244';
    var aanvraagopname = '245';
    var consultatie = '246';
    var kortformulier = '247';
    var tijdelijkezorg = '253';

    var processrequestformtype = function () {
        var requestformtype = $('#RequestFormZHVVTType').val();

        if (loaded === true) {
            $.ajax({
                type: "POST",
                url: getVerwijzingTypeByFormTypeID,
                data: { formtypeid: requestformtype },
                async: true,
                success: function (data) {
                    fillSelectWithOptions("#VerwijzingType", data);
                }
            });
        }

        //TODO: Make this smarter, f.e. true/false as parameter for removeClass/addClass. Or define relevant formtypeid on the div itself with a new data-attribute
        var showoverigeinputs = function () { $("#overigeinputs, #overigeinputs div").removeClass("ignore"); };
        var hideoverigeinputs = function () { $("#overigeinputs").addClass("ignore"); };
        var showkortformulierinputs = function () { $("#kortformulierinputs, #kortformulierinputs div").removeClass("ignore"); };
        var hidekortformulierinputs = function () { $("#kortformulierinputs").addClass("ignore"); };
        var showpatientmoetzorgontvangen = function () { $("#CareBeginDateContainer").removeClass("ignore"); };
        var hidepatientmoetzorgontvangen = function () { $("#CareBeginDateContainer").addClass("ignore"); };
        var showbeleid = function () { $("#OpdrachtThuiszorgContainer").removeClass("ignore"); };
        var hidebeleid = function () { $("#OpdrachtThuiszorgContainer").addClass("ignore"); };
        var showingesteldebehandeling = function () { $("#IngesteldeBehandelingContainer").removeClass("ignore"); };
        var hideingesteldebehandeling = function () { $("#IngesteldeBehandelingContainer").addClass("ignore"); };
        var showepisodelijst = function () { $("#HISEpisodeLijstContainer").removeClass("ignore"); }; 
        var hideepisodelijst = function () { $("#HISEpisodeLijstContainer").addClass("ignore"); }; 
        var showovernemenhis = function () { $("#GegevensOvernemenHIS, #GegevensOvernemenHIS div").removeClass("ignore"); };
        var hideovernemenhis = function () { $("#GegevensOvernemenHIS").addClass("ignore"); };
        var showAanvullendOnderzoek = function () { $("#divHISOvernemenAanvullendOnderzoek").removeClass("ignore"); };
        var hideAanvullendOnderzoek = function () { $("#divHISOvernemenAanvullendOnderzoek").addClass("ignore"); };
        var showVerwijzingType = function () { $("#VerwijzingTypeContainer").removeClass("ignore"); };
        var hideVerwijzingType = function () { $("#VerwijzingTypeContainer").addClass("ignore"); };
        var showAfhandeling = function () { $("#Afhandeling").removeClass("ignore"); };
        var hideAfhandeling = function () { $("#Afhandeling").addClass("ignore"); };
        var showTijdelijkezorg = function () { $("#IngangsdatumZorg, #PsychischFunctioneren, #Toestemming, #ZorgVraagBasis").removeClass("ignore"); };
        var hideTijdelijkezorg = function () { $("#IngangsdatumZorg, #PsychischFunctioneren, #Toestemming, #ZorgVraagBasis").addClass("ignore"); };
        var showSourceOrganization = function () { $("#divSourceOrganization").removeClass("ignore"); };
        var hideSourceOrganization = function () { $("#divSourceOrganization").addClass("ignore"); };



        if (requestformtype === opdracht)
        {
            hideSourceOrganization();
            showVerwijzingType();
            showoverigeinputs();
            showovernemenhis();
            showpatientmoetzorgontvangen();
            showbeleid();
            hideingesteldebehandeling();
            showepisodelijst();
            showAanvullendOnderzoek();
            showAfhandeling();
            hideTijdelijkezorg();
        }
        else if (requestformtype === aanvraagopname)
        {
            hideSourceOrganization();
            showVerwijzingType();
            showoverigeinputs();
            showovernemenhis();
            showpatientmoetzorgontvangen();
            hidebeleid();
            showingesteldebehandeling();
            showepisodelijst();
            showAanvullendOnderzoek();
            showAfhandeling();
            hideTijdelijkezorg();
        }
        else if (requestformtype === consultatie)
        {
            hideSourceOrganization();
            showVerwijzingType();
            showoverigeinputs();
            showovernemenhis();
            hidepatientmoetzorgontvangen();
            hidebeleid();
            showingesteldebehandeling();
            showepisodelijst();
            showAanvullendOnderzoek();
            showAfhandeling();
            hideTijdelijkezorg();
        }
        else if (requestformtype === kortformulier)
        {
            hideSourceOrganization();
            hideoverigeinputs();
            hideovernemenhis();
            showkortformulierinputs();
            hideVerwijzingType();
            showAfhandeling();
            hideTijdelijkezorg();
        }
        else if (requestformtype === tijdelijkezorg)
        {
            showSourceOrganization();
            hideoverigeinputs();
            hideovernemenhis();
            hideVerwijzingType();
            hideAfhandeling();
            hidepatientmoetzorgontvangen();
            showTijdelijkezorg();
        }
        else if (requestformtype === '')
        {
            hideVerwijzingType();
            hideoverigeinputs();
            hideovernemenhis();
        }
    }

    $('#RequestFormZHVVTType').on("change", processrequestformtype);
    $('#RequestFormZHVVTType option:selected').each(processrequestformtype);

    if ($('#RequestFormZHVVTType option').filter(function () { return $(this).attr('value') > 0; }).length === 1) {
        $('#RequestFormZHVVTType option').filter(function () { return $(this).attr('value') > 0 }).first().prop('selected', 'selected');
        processrequestformtype()
    }
    
    //Incontinentie
    function setIncontinentieAanwezig() {
        $("#IncontinentieAanwezig").toggle($(this).is(":checked"));
        if ($(this).is(":checked") == false)
            ClearContainer("#IncontinentieAanwezig");
    }
    $("#RelevanteAspectenVoorZorgverleningIncontinentie").on("change", setIncontinentieAanwezig);
    $("#RelevanteAspectenVoorZorgverleningIncontinentie").each(setIncontinentieAanwezig);

    MakeRadiosDeselectableByID("#RelevanteAspectenVoorZorgverleningIncontinentieKeus");

    //Verslaving
    function setVerslavingAanwezig() {
        $("#VerslavingAanwezig").toggle($(this).is(":checked"));
        if ($(this).is(":checked") == false)
            ClearContainer("#VerslavingAanwezig");
    }
    $("#Verslaving").on("change", setVerslavingAanwezig);
    $("#Verslaving").each(setVerslavingAanwezig);


    //PsychischFunctionerenJaNee
    MakeRadiosDeselectableByID("#PsychischFunctionerenJaNee");
    MakeRadiosDeselectableByID("#PsychischFunctionerenGeheugenverlies");
    MakeRadiosDeselectableByID("#PsychischFunctionerenDesorientatie");
    MakeRadiosDeselectableByID("#PsychischFunctionerenGedragsstoornissen");
    MakeRadiosDeselectableByID("#PsychischFunctionerenStemmingsstoornissen");
    MakeRadiosDeselectableByID("#PsychischFunctionerenDenkWaarnemingsstroornissen");
    MakeRadiosDeselectableByID("#PsychischFunctionerenItemIngevuldToelichting");
    var psychischFunctionerenJaNee = GetNameByID("#PsychischFunctionerenJaNee");

    function psychischfunctioneren() {
        if ($(this).val() == "nee" && $(this).is(":checked")) {
            $("#divPsychischFunctionerenJaNee").show();
        }
        else {
            $("#divPsychischFunctionerenJaNee").hide();
            ClearContainer("#divPsychischFunctionerenJaNee");
        }
    }
    $("[name='" + psychischFunctionerenJaNee + "']input:radio").on("click", psychischfunctioneren);
    GetSelectedRadioByName(psychischFunctionerenJaNee).each(psychischfunctioneren);

    //Toestemming
    var loadedToestemmingPatient = false;
    function setToestemmingPatient() {
        $("#ToestemmingPatientJa").toggle($(this).val() == "ja" && (this.checked || loadedToestemmingPatient == false));
        $("#ToestemmingPatientNee").toggle($(this).val() == "nee" && (this.checked || loadedToestemmingPatient == false));
        $("#ToestemmingPatientNietMogelijk").toggle($(this).val() == "niet mogelijk" && (this.checked || loadedToestemmingPatient == false));

        if (loadedToestemmingPatient) {
            if ($(this).val() == "ja" && this.checked) {
                ClearContainer("#ToestemmingPatientNee");
                ClearContainer("#ToestemmingPatientNietMogelijk");
            } else if ($(this).val() == "nee" && this.checked) {
                ClearContainer("#ToestemmingPatientJa");
                ClearContainer("#ToestemmingPatientNietMogelijk");
                CheckAndShowToestemming();
            } else if ($(this).val() == "niet mogelijk" && this.checked) {
                ClearContainer("#ToestemmingPatientNee");
                ClearContainer("#ToestemmingPatientJa");
            }
        }
    }

    MakeRadiosDeselectableByID("#ToestemmingPatient");
    var toestemmingPatientName = GetNameByID("#ToestemmingPatient");
    $("[name='" + toestemmingPatientName + "']input:radio").on("click", setToestemmingPatient);
    GetSelectedRadioByName(toestemmingPatientName).each(setToestemmingPatient);
    loadedToestemmingPatient = true;

    function toestemmingVan() {
        var toestemmingPatientVanVertegenwoordiger = $(this).val() == "Vertegenwoordiger";
        $("#ToestemmingPatientVanVertegenwoordiger").toggle(toestemmingPatientVanVertegenwoordiger && this.checked);

        if ((toestemmingPatientVanVertegenwoordiger && this.checked) == false)
            ClearContainer("#ToestemmingPatientVanVertegenwoordiger");
    }

    //Toestemming Van
    var toestemmingPatientVanName = GetNameByID("#ToestemmingPatientVan");
    $("[name='" + toestemmingPatientVanName + "']input:radio").on("click", toestemmingVan);
    GetSelectedRadioByName(toestemmingPatientVanName).each(toestemmingVan);

    //ZorgVraag
    $('#ZorgVraagBasis :input').each(function () {
        $(this).on('keyup paste', function () {
            var trgroup = $(this).closest('tr');
            var checkbox = trgroup.find('div.checkbox :checkbox');
            if (checkbox.length === 1) {
                var noneEmptyElements = trgroup.find(':input:not(:checkbox):not(:hidden)').filter(function () { return $.trim($(this).val()).length > 0; });
                checkbox.prop('checked', (noneEmptyElements.length > 0));
                checkbox.trigger("change");
            }
        });
    });

}

function SetResize() {
    //Scrollable auto-resize
    $(window).on("resize", function () {
        var height = GetScrollHeight();
        $("#Scroller").css("max-height", height);
        $("#Scroller").css("height", height);
    });
    $(window).trigger("resize");
}

function CalculateBMI() {
    var objWeight = document.getElementById("ObesitasGewicht");
    var objHeight = document.getElementById("ObesitasLengte");
    var objBMI = document.getElementById("ObesitasBMI");
    if (!objHeight || !objWeight || !objBMI) return;

    var weight = objWeight.value == "" ? "0" : objWeight.value;
    var height = objHeight.value == "" ? "0" : objHeight.value;

    var res = "";
    if (height != "0" && weight != "0") {
        var res = parseFloat(weight) / Math.pow(parseInt(height) / 100, 2);
        res = res.toFixed(1);
    }

    objBMI.value = res;
}

$(function () {
    //Prefill DossierOwner Telephone
    $('#DossierOwner').on("change", setEmployeeDetails);

    //Alleen bij nieuw doc
    if (formSetVersionID === -1) {
        $("#DossierOwner").each(setEmployeeDetails);
    }

    $('[data-toggle="clickover"]').clickover({
        html: true,
        placement: $(this).attr("data-placement") ? $(this).data("placement") : "top",
        global_close: true,
        esc_close: true,
        template: '<div class="popover"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>',
        content: $(this).data("content")
    });

    var kwetsbareOudereJaNee = GetNameByID("#KwetsbareOudereJaNee");
    function kwetsbareoudere() {
        if ($(this).val() == "ja") {
            $("#divKwetsbareOudereJaNee").show();
        }
        else {
            $("#divKwetsbareOudereJaNee").hide();
            ClearContainer("#divKwetsbareOudereJaNee");
        }
    }
    $("[name='" + kwetsbareOudereJaNee + "']input:radio").on("click", kwetsbareoudere);
    GetSelectedRadioByName(kwetsbareOudereJaNee).each(kwetsbareoudere);

    //BMI
    $("#ObesitasGewicht, #ObesitasLengte").on("change", function () {
        CalculateBMI();
    });

    var terugkoppelingGewenstJaNee = GetNameByID("#TerugkoppelingGewenst");
    function terugkoppelingGewenst() {
        if ($(this).val() == "ja") {
            $("#divTerugkoppelingGewenstToelichting").show();
        }
        else {
            $("#divTerugkoppelingGewenstToelichting").hide();
            ClearContainer("#divTerugkoppelingGewenstToelichting");
        }
    }
    $("[name='" + terugkoppelingGewenstJaNee + "']input:radio").on("click", terugkoppelingGewenst);
    GetSelectedRadioByName(terugkoppelingGewenstJaNee).each(terugkoppelingGewenst);

    var hISOvernemenAanvullendOnderzoekName = GetNameByID("#HISOvernemenAanvullendOnderzoek");
    function hISOvernemenAanvullendOnderzoek() {
        if ($(this).is(":checked")) {
            $("#divHISAanvullendOnderzoek").show();
        }
        else {
            $("#divHISAanvullendOnderzoek").hide();
            ClearContainer("#divHISAanvullendOnderzoek");
        }
    }
    $("[name='" + hISOvernemenAanvullendOnderzoekName + "']input:checkbox").on("click", hISOvernemenAanvullendOnderzoek);
    GetSelectedCheckboxByName(hISOvernemenAanvullendOnderzoekName).each(hISOvernemenAanvullendOnderzoek);

    var hISOvernemenMedicatieName = GetNameByID("#HISOvernemenMedicatie");
    function hISOvernemenMedicatie() {
        if ($(this).is(":checked")) {
            $("#divHISOvernemenMedicatie").show();
        }
        else {
            $("#divHISOvernemenMedicatie").hide();
            ClearContainer("#divHISOvernemenMedicatie");
        }
    }
    $("[name='" + hISOvernemenMedicatieName + "']input:checkbox").on("click", hISOvernemenMedicatie);
    GetSelectedCheckboxByName(hISOvernemenMedicatieName).each(hISOvernemenMedicatie);

    var aanvraagVersturenTypeName = GetNameByID("#AanvraagVersturenType");
    function aanvraagVersturenType() {
        if ($(this).val() === "coordinatiepunt") {
            $("#divAanvraagVersturenDoorTussenOrganisatie").removeClass("ignore").show();
        }
        else {
            $("#divAanvraagVersturenDoorTussenOrganisatie").addClass("ignore").hide();
            ClearContainer("#divAanvraagVersturenDoorTussenOrganisatie");
        }
    }
    $("[name='" + aanvraagVersturenTypeName + "']input:radio").on("click", aanvraagVersturenType);
    GetSelectedRadioByName(aanvraagVersturenTypeName).each(aanvraagVersturenType);
    
    SetRequestForm();
    SetResize();
    loaded = true;
});