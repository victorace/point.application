﻿
$(function () {

    $("#FieldChoices .btn").prop("disabled", true);

    $(document).bind("change", ":checkbox", function () {
        var hascheckeditems = $(":checkbox:checked").length !== 0;
        $("#displayFields").prop("disabled", function () { return !hascheckeditems; });
    });

    $("#allFields").bind("click", function () {
        $(":checkbox").prop("checked", true).change();
    });

    $("#noFields").bind("click", function () {
        $(":checkbox.field").prop("checked", false).change();
    });

    $("#displayFields").bind("click", function () {
        if ($(":checkbox:checked").length === 0) {
            return;
        }
        var selectedform = $(".list-group-item.active");
        if (selectedform.length > 0) {
            var type = selectedform.data("type");
            var url = selectedform.data("historyurl");
            var id = selectedform.data("formtypeid");

            var formTypeID = selectedform.val();
            var formSetVersionID = selectedform.data("formsetversionid");

            var flowFieldIDs = $(":checkbox:checked").map(function () { return $(this).data("flowfieldid"); }).get();
            var customFields = $(":checkbox:checked").map(function () { return $(this).data("customfield"); }).get();

            if (type === "clientdata") {
                $("#DataTableContent").load(url, { clientId: id, fields: flowFieldIDs }, function (obj) {
                    $("#DataTableContent table").dataTable({ "ordering": false, "sDom": "tip", "scrollX": true });
                });
            } else {
                url = url.replace("__FormTypeID__", id);
                url = url.replace("__FormSetVersionID__", formSetVersionID);
                url = url.replace("__flowFieldIDs__", flowFieldIDs.join());
                url = url.replace("__CustomFields__", customFields.join());
                $("#DataTableContent").load(url, function (obj) {
                    $("#DataTableContent table").dataTable({
                        "ordering": false, "sDom": "tip", "scrollX": true
                    });
                });
            }
        }
    });

    $("input[type='radiobutton'].formType").prop("checked", false);

    $(".list-group-item").click(function () {

        var formTypeID = $(this).data("formtypeid");
        if (formTypeID.length === 0) {
            return;
        }

        var type = $(this).data("type");

        $("#FieldSelector").css("display", "block");
   
        $("#FieldChoices .btn").prop("disabled", true);
        $(".list-group-item").removeClass("active");

        var container = $("#FormFieldContainer").empty();

        $(this).addClass("active");

        var urlval = $(this).data("fieldsurl");
        

         var formsetVersionID = $(this).data("formsetversionid");

        var ajaxdata;
        if (type === "clientdata") {

            ajaxdata = { ClientId: this.value };
        }
        else if (type === "flowform") {
            ajaxdata = { FormTypeID: formTypeID, FormSetVersionID: formsetVersionID };
        }
        
        $.ajax({
            type: "POST",
            url: urlval,
            data: ajaxdata, 
            async: false,
            success: function (data) {
                var hasfields = type === "clientdata" || type ==="commjournal" ? data.length !== 0 : data.data.length !== 0;
                if (hasfields) {
                    if (type === "clientdata") {
                        $.each($(data), function (index, item) {
                            var flowFieldDiv = $("<div class='field' />").appendTo(container);
                            $("<input />", { type: "checkbox", id: "cb" + index, "data-flowfieldid": item.Key }).addClass("field").appendTo(flowFieldDiv);
                            $("<label />", { "for": "cb" + index, text: item.Value }).css("margin-left", "10px").appendTo(flowFieldDiv);
                        });
                    }
                    else if (type === "flowform") {
                        $.each($(data.data), function (index, item) {
                            var flowFieldDiv = $("<div class='field' />").appendTo(container);
                            $("<input />", { type: "checkbox", id: "cb" + index, "data-formsetversionid": data.formsetversionid, "data-flowfieldid": item.FlowFieldID, "data-customfield": item.CustomField }).addClass("field").appendTo(flowFieldDiv);
                            $("<label />", { "for": "cb" + index, text: item.Label }).css("margin-left", "10px").appendTo(flowFieldDiv);
                        });
                    }                 
                }
                $("#FieldChoices .btn-default").prop("disabled", !hasfields);
            },
            error: function (xhr, status, error) {
                $("#FieldChoices .btn").prop("disabled", true);
                response = JSON.parse(xhr.responseText);
                showMessage("Er is een fout opgetreden: " + response.ErrorMessage);
            }
        });
    });
});