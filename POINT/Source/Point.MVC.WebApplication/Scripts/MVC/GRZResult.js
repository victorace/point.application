﻿$(function () {
    var conclusieWelGeenGRZName = GetNameByID("#ConclusieWelGeenGRZ");
    $("[name='" + conclusieWelGeenGRZName + "']input:radio").on("click", function () {
        $("#welgrzcontainer").toggle($(this).val() === "Wel GRZ" && this.checked);

        if ($(this).val() !== "Wel GRZ" && this.checked) {
            ClearContainer("#welgrzcontainer");
        }
    });
    
    var selectedconclusie = GetSelectedRadioByName(conclusieWelGeenGRZName);
    if (selectedconclusie.length === 0)
    {
        $("#welgrzcontainer").hide();
    }
    else
        selectedconclusie.click();

    var conclusieTypeGRZName = GetNameByID("#ConclusieTypeGRZ");
    $("[name='" + conclusieTypeGRZName + "']input:radio").on("click", function () {
        var value = $(this).val();
        var disabled = value !== "Overig" && this.checked;

        $("#ConclusieOverigBeschrijving").prop("disabled", disabled);
        if (disabled) {
            ClearContainer("#ConclusieOverigBeschrijving");
        }

        $("#divGRZVoorlopigeDiagnoseCVA").toggle(value === 'CVA');
        $("#divGRZVoorlopigeDiagnoseEO").toggle(value === 'Electieve orthopedie');
        $("#divGRZVoorlopigeDiagnoseTrauma").toggle(value === 'Trauma');
        $("#divGRZVoorlopigeDiagnoseAmputatie").toggle(value === 'Amputatie');
        $("#divGRZVoorlopigeDiagnoseOverig").toggle(value === 'Overig');

    });
    GetSelectedRadioByName(conclusieTypeGRZName).click();
});