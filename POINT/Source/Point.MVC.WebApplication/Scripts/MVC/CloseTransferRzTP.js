﻿$(function () {
    $(window).on("resize", function () {
        var height = GetScrollHeight();
        $("#Scroller").css("max-height", height);
        $("#Scroller").css("height", height);
    });
    $(window).trigger("resize");

    $('.btnCloseTransfer').on('click', function (e) {
        validateDateWarningRules();
        removeDateWarningRules();
        var validform = $(formID).valid();
        if (validform) {
            $(this).attr("disabled", true);
            AjaxSave(formID, formRequestUrl, true);
            return;
        }
        attachDateWarningRules();
    });

    $(".btnCancel").one("click", function (e) {
        if ($(formID).find(":input:not([type=hidden])").length > 0) {
            FormIsNotDirty();
            location.reload();
        } else if (dashboardUrl) {
            location.replace(dashboardUrl);
        }
    });
});