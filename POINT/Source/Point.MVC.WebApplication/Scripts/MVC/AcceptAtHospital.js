﻿function handleEmployeeTelephoneNumber(data) {
    if (data !== undefined && data.length === 1) {
        $('#AcceptedByTelephoneNumber').val(data[0].Text);
    }
}

function setTelePhone()
{
    var employeeID = $(this).val();
    var digest = $(this).find(":selected").data("digest");
    $.ajax({
        type: "POST",
        url: getEmployeePhoneNumberByEmployeeID,
        data: { employeeID: employeeID, digest: digest },
        success: function (data) { handleEmployeeTelephoneNumber(data); }
    });
}

$(function () {
    $('#AcceptedBy').on("change", setTelePhone);

    if (prefill) {
        $("#AcceptedBy").each(setTelePhone);
    }
});