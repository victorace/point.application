﻿var target = '#ClientSearchData';

var inittable = initfix;

$(function () {
    $("#ClientSearchData").closest(".modal").on('shown.bs.modal', function (e) {
        setSearchLive();
        //for some odd reason it does work with setTimeout even at 0ms..
        setTimeout(function () {
            $("#ClientSearchData").closest(".modal").find(':input[type=text]:enabled:visible:not([readonly]):first').focus();
        }, 0);        
    });

});

function initfix() {

    if (!String.prototype.startsWith) {
        String.prototype.startsWith = function (searchString, position) {
            if (typeof searchString === "undefined") {
                return false;
            }

            position = position || 0;
            return this.substr(position, searchString.length) === searchString;
        };
    }

    if (!String.prototype.endsWith) {
        String.prototype.endsWith = function (searchString, position) {
            if (typeof searchString === "undefined") {
                return false;
            }

            var subjectString = this.toString();
            if (typeof position !== 'number' || !isFinite(position) || Math.floor(position) !== position || position > subjectString.length) {
                position = subjectString.length;
            }
            position -= searchString.length;
            var lastIndex = subjectString.indexOf(searchString, position);
            return lastIndex !== -1 && lastIndex === position;
        };
    }

    $("#ClientSearchData").scrollTop(0);

    $("#gridTable th.sorting").each(function () {
        var th = $(this);
        var sortcolumn = $(this).data("sortby");

        var currentsort = $("#SortBy").val();
        if (typeof currentsort !== "undefined") {
            if (currentsort.startsWith(sortcolumn) && currentsort.endsWith("asc")) {
                th.removeClass("sorting").addClass("sorting_asc");
            } else if (currentsort.startsWith(sortcolumn) && currentsort.endsWith("desc")) {
                th.removeClass("sorting").addClass("sorting_desc");
            }
        }
    });

    $("#gridTable th.sorting, #gridTable th.sorting_asc, #gridTable th.sorting_desc").on("click", function () {
        var sortcolumn = $(this).data("sortby");
        var currentsort = $("#SortBy").val();
        var direction = "asc";

        if (typeof currentsort !== "undefined") {
            if (currentsort.startsWith(sortcolumn) && currentsort.endsWith(direction)) {
                direction = "desc";
            }
        }

        $("#SortBy").val(sortcolumn + " " + direction);
        SearchForClient();
    });

}

var lastid = null;

function searchlive(sender) {
    setSearchLive();
}

function setSearchLive()
{  if (lastid !== null)
        clearTimeout(lastid);
    enablemin3();
    lastid = setTimeout(SearchForClient, 300);
}

function showAll() {
    $("#ShowAll").val(true);
    SearchForClient();
}

function SearchForClient() { 
    $("#showallbutton").hide();

    var form = $('#ClientSearchForm');
    url = form.data("url");

    var sorting = form.data("sorting");

    if (form.valid()) {
        $.ajaxSetup({
            global: false
        });
        AjaxLoad('#ClientSearchForm', url, target, inittable);

        $.ajaxSetup({
            global: true
        });
    } else {
        $(target).find("tbody").empty();
    }
}

function getClientDataByTransferID(url, searchData) {
    $.ajax({
        type: "Post",
        url: url,
        datatype: 'json',
        data: searchData,
        async: true,
        success: function (data) { 
            PutClientSearchData(data);
            $("#modal").modal('hide');         
        },
        error: function (jqxhr, textStatus, errorThrown) {
            showErrorMessage($.parseJSON(jqxhr.responseText).ErrorMessage, true);
        }
    });
}

function enablemin3() {   
    $('#ClientSearchForm').validate();
    $("#Search").rules(
        "add",
        {
            minlength: $("#Search").data("minlength"),
            required: $("#Search").data("minlength") === "0" ? false : true,
            messages: { required: "Voor deze search minstens " + $("#Search").data("minlength") + " tekens invoeren.", minlength: "Voor deze search minstens " + $("#Search").data("minlength") + " tekens invoeren." }
        }
    );
}

$(document).ready(function () {
    setSearchLive();
});

