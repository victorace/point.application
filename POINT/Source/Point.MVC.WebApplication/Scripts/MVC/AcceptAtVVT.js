﻿function handleEmployeeTelephoneNumber(data) {
    if (data !== undefined && data.length === 1) {
        $('#AcceptedByTelephoneVVT').val(data[0].Text);
    }
}

function setTelephone()
{
    var employeeID = $(this).val();
    var digest = $(this).find(":selected").data("digest");
    $.ajax({
        type: "POST",
        url: getEmployeePhoneNumberByEmployeeID,
        data: { employeeID: employeeID, digest: digest },
        success: function (data) { handleEmployeeTelephoneNumber(data); }
    });
}

$(function () {

    $('#AcceptedByVVT').on("change", setTelephone);

    //Alleen bij nieuw doc
    if (formSetVersionID === -1) {
        $("#AcceptedByVVT").each(setTelephone);
    }
});