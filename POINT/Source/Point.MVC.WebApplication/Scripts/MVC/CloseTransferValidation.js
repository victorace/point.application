﻿var validationMethod_IsDateBefore = "isDateBefore";
var validationMethod_IsDateAfter = "isDateAfter";
var validationMethod_GreaterThanDays = "greaterThanDays";
var validationMethod_BetweenYears = "betweenYears";

var momentDateFormat = "DD-MM-YYYY";

var validationMessage_Empty = "";
var validationMessage_IsDateBefore = "Let op! {0} is voor {1}.";
var validationMessage_IsDateAfter = "Let op! {0} is na {1}.";
var validationMessage_GreaterThanDays = "Let op! Tussen {0} en {1} ligt meer dan {2} dagen.";
var validationMessage_BetweenYears = "Let op! {0} is meer dan {1} jaar geleden of is meer dan {1} jaar in de toekomst.";

var getLabels = function (elements) {
    return $.map(elements, function (e) {
        return $(e).closest(".form-group").find(".control-label").text();
    });
}

var displayIsDateBefore = function (element, elementsWithError) {
    var labels = getLabels(elementsWithError);
    return $.validator.format(validationMessage_IsDateBefore,
        $(element).closest(".form-group").find(".control-label").text(),
        labels.join(","));
}

var displayIsDateAfter = function (element, elementsWithError) {
    var labels = getLabels(elementsWithError);
    return $.validator.format(validationMessage_IsDateAfter,
        $(element).closest(".form-group").find(".control-label").text(),
        labels.join(","));
}

var displayGreaterThanDays = function (element, elementsWithError, params) {
    var labels = getLabels(elementsWithError);
    return $.validator.format(validationMessage_GreaterThanDays,
        $(element).closest(".form-group").find(".control-label").text(),
        labels.join(","), params.Days);
}

var displayBetweenYears = function (element, elementsWithError, params) {
    return $.validator.format(validationMessage_BetweenYears,
        $(element).closest(".form-group").find(".control-label").text(), params.Years);
}

var toggleRule = function (method, element, elementsWithError, params) {
    var id = $.validator.format("{0}.{1}", $(element).attr("id"), method);
    var rule = $(".form-group[data-rule='" + id + "']");
    if (elementsWithError.length > 0) {
        if (typeof params.Callback !== 'undefined' && params.Callback)
        {
            if (rule.length === 0) {
                rule = $("#rulecontainer div:first")
                    .clone()
                    .attr("data-rule", id)
                    .insertAfter($(element).closest(".form-group"));
            }
            var display = params.Callback(element, elementsWithError, params);
            rule.find(".rule-message")
                .text(display);
        }
    }
    else {
        rule.remove();
    }
}

var attachDateWarningRules = function () {
    var medischesituatiedatumopname = $("#MedischeSituatieDatumOpname");
    if (medischesituatiedatumopname.length) {
        $("#MedischeSituatieDatumOpname").rules("add", {
            betweenYears: {
                Years: 1,
                Callback: displayBetweenYears
            },
            greaterThanDays: {
                Elements: ["#RealizedDischargeDate"],
                Days: 200,
                Callback: displayGreaterThanDays
            },
            isDateAfter: {
                Elements: ["#DatumEindeBehandelingMedischSpecialist", "#CareBeginDate", "#RealizedDischargeDate"],
                Callback: displayIsDateAfter
            },
            messages: {
                betweenYears: validationMessage_Empty,
                greaterThanDays: validationMessage_Empty,
                isDateAfter: validationMessage_Empty
            }
        });
    }

    var datumeindebehandelingmedischspecialist = $("#DatumEindeBehandelingMedischSpecialist");
    if (datumeindebehandelingmedischspecialist.length) {
        $("#DatumEindeBehandelingMedischSpecialist").rules("add", {
            betweenYears: {
                Years: 1,
                Callback: displayBetweenYears
            },
            greaterThanDays: {
                Elements: ["#RealizedDischargeDate"],
                Days: 10,
                Callback: displayGreaterThanDays
            },
            isDateBefore: {
                Elements: ["#MedischeSituatieDatumOpname"],
                Callback: displayIsDateBefore
            },
            messages: {
                betweenYears: validationMessage_Empty,
                greaterThanDays: validationMessage_Empty,
                isDateBefore: validationMessage_Empty
            }
        });
    }

    var carebegindate = $("#CareBeginDate");
    if (carebegindate.length) {
        $("#CareBeginDate").rules("add", {
            betweenYears: {
                Years: 1,
                Callback: displayBetweenYears
            },
            isDateBefore: {
                Elements: ["#MedischeSituatieDatumOpname"],
                Callback: displayIsDateBefore
            },
            messages: {
                betweenYears: validationMessage_Empty,
                isDateBefore: validationMessage_Empty
            }
        });
    }

    var dischargeproposedstartdate = $("#DischargeProposedStartDate");
    if (dischargeproposedstartdate.length) {
        $("#DischargeProposedStartDate").rules("add", {
            betweenYears: {
                Years: 1,
                Callback: displayBetweenYears
            },
            isDateBefore: {
                Elements: ["#MedischeSituatieDatumOpname"],
                Callback: displayIsDateBefore
            },
            messages: {
                betweenYears: validationMessage_Empty,
                isDateBefore: validationMessage_Empty
            }
        });
    }

    var realizeddischargedate = $("#RealizedDischargeDate");
    if (realizeddischargedate.length) {
        $("#RealizedDischargeDate").rules("add", {
            betweenYears: {
                Years: 1,
                Callback: displayBetweenYears
            },
            greaterThanDays: {
                Elements: ["#MedischeSituatieDatumOpname"],
                Days: 30,
                Callback: displayGreaterThanDays
            },
            isDateBefore: {
                Elements: ["#MedischeSituatieDatumOpname"],
                Callback: displayIsDateBefore
            },
            messages: {
                isDateBefore: validationMessage_Empty,
                greaterThanDays: validationMessage_Empty,
                betweenYears: validationMessage_Empty
            }
        });
    }
}

var removeDateWarningRules = function () {
    var validationMethods = [validationMethod_IsDateBefore, validationMethod_IsDateAfter,
        validationMethod_GreaterThanDays, validationMethod_BetweenYears];
    $(".panel-dates .form-control").each(function (index, element) {
        var rules = $(element).rules();
        $.each(rules,
            function(name) {
                if ($.inArray(name, validationMethods) !== -1) {
                    $(element).rules("remove", name);
                }
            });
    });
}

var addDateValidationMethods = function () {
    $.validator.addMethod(validationMethod_IsDateBefore, function (value, element, params) {
        var elementsWithError = [];
        var date1 = moment(value, momentDateFormat);
        if (date1.isValid()) {
            elementsWithError = $.map(params.Elements, function (e) {
                var date2 = moment($(e).val(), momentDateFormat);
                return (date2.isValid() && date1.isBefore(date2)) ? e : null;
            });
        }
        toggleRule(validationMethod_IsDateBefore, element, elementsWithError, params);
        return (elementsWithError.length === 0);
    });

    $.validator.addMethod(validationMethod_IsDateAfter, function (value, element, params) {
        var elementsWithError = [];
        var date1 = moment(value, momentDateFormat);
        if (date1.isValid()) {
            elementsWithError = $.map(params.Elements, function (e) {
                var date2 = moment($(e).val(), momentDateFormat);
                return (date2.isValid() && date1.isAfter(date2)) ? e : null;
            });
        }
        toggleRule(validationMethod_IsDateAfter, element, elementsWithError, params);
        return (elementsWithError.length === 0);
    });

    $.validator.addMethod(validationMethod_GreaterThanDays, function (value, element, params) {
        var elementsWithError = [];
        var date1 = moment(value, momentDateFormat);
        var date2 = moment($(params.Elements[0]).val(), momentDateFormat);
        if (date1.isValid() && date2.isValid()) {
            if (Math.abs(date1.diff(date2, "days")) > params.Days) {
                elementsWithError.push(params.Elements[0]);
            }
        }
        toggleRule(validationMethod_GreaterThanDays, element, elementsWithError, params);
        return elementsWithError.length === 0;
    });

    $.validator.addMethod(validationMethod_BetweenYears, function (value, element, params) {
        var elementsWithError = [];
        var date = moment(value, momentDateFormat);
        if (date.isValid()) {
            if (!date.isBetween(moment().subtract(1, "years"), moment().add(1, "years"))) {
                elementsWithError.push(element);
            }
        }
        toggleRule(validationMethod_BetweenYears, element, elementsWithError, params);
        return elementsWithError.length === 0;
    });
}

var validateDateWarningRules = function () {
    var count = 0;
    if (typeof (formID) !== "undefined") {
        var validator = $(formID).validate();
        $(".panel-dates .form-control").each(function (index, element) {
            if (!($(element).hasClass("required") && $(element).val().length === 0)) {
                if (!validator.element(element)) {
                    count++;
                }
            }
        });
    }
    return (count === 0);
}

var fixDataLinked = function (children) {
    // See jquery-validate-1.14 changeLog: Revert "Ignore readonly as well as disabled fields." 
    if (typeof children === "undefined" || children === null)
    {
        if (typeof (formID) !== "undefined") {
            children = $(formID).find('input[readonly][data-linked].required');
            children.removeAttr("readonly");
        }
    }
    else
    {
        children.attr("readonly", "readonly");
    }
    return children;
}

$(function () {
    if (typeof (formID) !== "undefined") {
        var validator = $.data($(formID)[0], "validator");
        if (validator) {
            addDateValidationMethods();
            attachDateWarningRules();
            validateDateWarningRules();
        }
    }
});