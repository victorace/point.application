﻿$(function () {

    $('.btnCloseTransfer').on('click', function (e) {
        var validform = $(formID).valid();
        if (validform) {
            $(this).attr("disabled", true);
            AjaxSave(formID, formRequestUrl, true);
            return;
        }
    });

    $(".btnCancel").one("click", function (e) {
        if ($(formID).find(":input:not([type=hidden])").length > 0) {
            FormIsNotDirty();
            location.reload();
        } else if (dashboardUrl) {
            location.replace(dashboardUrl);
        }
    });
});