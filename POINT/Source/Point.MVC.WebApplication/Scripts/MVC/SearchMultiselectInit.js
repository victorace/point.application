﻿function setActiveFlowDefinition(toDefault, toActive) {
    toActive.next("div").find("button.multiselect").removeClass("btn-default active").addClass("active");
    $("#FlowDirection").val(toActive.data("flowdirection"));
}

$(function () {

    // NOTE needs to run before Global.js ---

    var departmentids = $("#DepartmentIDs").prop("name", "DepartmentIDs");
    departmentids.multiselect({
        templates: {
            //multiselect-grouped-container allows separate styling (see search.css)
            ul: '<ul class="multiselect-container multiselect-grouped-container dropdown-menu"></ul>',
        },
        selectAllText: "Selecteer alle afdelingen",
        nonSelectedText: "Filter afdelingen",
        allSelectedText: "Alle afdelingen",
        numberDisplayed: 1,
        nSelectedText: " geselecteerd",
        includeSelectAllOption: true,
        enableClickableOptGroups: true,
        onDropdownHidden: function (option, checked, select) {
            $('#renewsignalering').val(true);
            Search();
        } 
    });

    if (typeof flowDefinitionIDs !== 'undefined') {

        var phasechanged = false;
        var phase = $("#phase");
        phase.multiselect({
            onChange: function () { phasechanged = true; },
            onDropdownShown: function (event) { phasechanged = false; },
            onDropdownHidden: function (event) { if (phasechanged) { Search(); } },
            numberDisplayed: 1,
            nonSelectedText: "Alle fasen",
            nSelectedText: " geselecteerd"
        });
        if (flowDefinitionIDs === 0 || flowDefinitionIDs === "") {
            phase.multiselect("disable");
        }

        var flowDefinitionsReceiver = $("#FlowDefinitionsReceiver");
        var flowDefinitionsSender = $("#FlowDefinitionsSender");
        var flowdefinitionsreceiverchanged = false;
        flowDefinitionsReceiver.multiselect({
            onChange: function () { setActiveFlowDefinition(flowDefinitionsSender, flowDefinitionsReceiver); },
            onDropdownShow: function (event) {
                var buttongroup = $(event.currentTarget);
                var checkedcount = buttongroup.find("input:checked").length;
                if (checkedcount === 0)
                {
                    flowDefinitionsReceiver.multiselect('selectAll', false).multiselect('refresh');
                    setActiveFlowDefinition(flowDefinitionsSender, flowDefinitionsReceiver);
                }

                flowDefinitionsSender.multiselect('deselectAll', false).multiselect('refresh');
                flowDefinitionsSender.next("div").find("button.multiselect").removeClass("active").addClass("btn-default");
            },
            onDropdownHidden: function (event) { Search(); },
            selectAllText: 'Selecteer alle',
            allSelectedText: "Alle ontvangen transfers",
            nonSelectedText: "Selecteer",
            nSelectedText: " geselecteerd",
            buttonWidth: "185px"
        });

        var flowdefinitionssenderchanged = false;
        flowDefinitionsSender.multiselect({
            onChange: function () { setActiveFlowDefinition(flowDefinitionsReceiver, flowDefinitionsSender); },
            onDropdownShow: function (event) {
                var buttongroup = $(event.currentTarget);
                var checkedcount = buttongroup.find("input:checked").length;
                if (checkedcount === 0) {
                    flowDefinitionsSender.multiselect('selectAll', false).multiselect('refresh');
                    setActiveFlowDefinition(flowDefinitionsReceiver, flowDefinitionsSender);
                }

                flowDefinitionsReceiver.multiselect('deselectAll', false).multiselect('refresh');
                flowDefinitionsReceiver.next("div").find("button.multiselect").removeClass("active").addClass("btn-default");
            },
            onDropdownHidden: function (event) { Search(); },
            selectAllText: 'Selecteer alle',
            allSelectedText: "Alle verzonden transfers",
            nonSelectedText: "Selecteer",
            nSelectedText: " geselecteerd",
            buttonWidth: "185px"
        });

        if (flowDefinitionsReceiver.find("option:selected").index() >= 0)
            setActiveFlowDefinition(flowDefinitionsSender, flowDefinitionsReceiver);
        else if (flowDefinitionsSender.find("option:selected").index() >= 0)
            setActiveFlowDefinition(flowDefinitionsReceiver, flowDefinitionsSender);
    }
});
