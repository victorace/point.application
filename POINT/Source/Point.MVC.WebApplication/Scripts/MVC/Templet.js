﻿$(function () {

    $('#dialog-edit').on('hidden.bs.modal', function () {
        $("#templetForm #Name").removeAttr("readonly").removeClass("disabled");
        $("#templetForm #Description").removeAttr("readonly").removeClass("disabled");
    });

    $('#formSelectRegion').submit(function () {
        $(this).append('<input type="hidden" name="Digest" value="' + $("#regionID").find(":selected").data("digest") + '" /> ');
        return true;
    });

   //initDataTable("#templetsTable");    
    var height = $("#templetsTable").closest('div').innerHeight();
    $("#templetsTable").DataTable({
        aaSorting: [[5, 'asc'],[0, 'asc']],
        bFilter: false,
        sDom: "ftip",
        scrollY: height
    });

    $('.deleteTemplet').on('click', function (e) {
        e.preventDefault();
        var url = $(this).data("url");
        $('#dialog-confirm').modal('show')
            .one('click', '#delete', function () {
                $.ajax({
                    type: "GET",
                    url: url,
                    success: function (data) {
                        location.replace(templetListUrl);
                    }
                });
            });
    });

    $('#btnInsertTemplet').on('click', function (e) {
        //e.preventDefault();
        $('#dialog-insert').modal('show');
        var url = $(this).data("url");
        $.ajax({
            type: "GET",
            url: url,
            success: function (data) {
                $("#InsertTemplet").html(data);
            }
        });
    });

    $('.editTemplet').on('click', function (e) {
        //e.preventDefault();
        $('#dialog-edit').modal('show');
        var url = $(this).data("url");
        $.ajax({
            type: "GET",
            url: url,
            success: function (data) {
                $("#EditTemplet").html(data);
            }
        });
    });

    var formInsertOptions = {
        url: templetInsertUrl,
        type: 'post',
        dataType: 'json',
        clearForm: true,
        success: function (data, textStatus, jqxhr, form) {
            if (data.ErrorMessage)
                showErrorMessage($.parseJSON(jqxhr.responseText).ErrorMessage);
            else
                location.replace(templetListUrl);
        }
    };

    var formEditOptions = {
        url: templetUpdateUrl,
        type: 'post',
        dataType: 'json',
        clearForm: true,
        success: function (data, textStatus, jqxhr, form) {
            if (data.ErrorMessage)
                showErrorMessage($.parseJSON(jqxhr.responseText).ErrorMessage);
            else
                location.replace(templetListUrl);
        }
    };

    $(function () {
        $("#dialog-insert #btnUpload").click(function (ev) {
            $("#dialog-insert #templetForm").ajaxSubmit(formInsertOptions);
        })
        $("#dialog-edit #btnSave").click(function (ev) {
            $("#dialog-edit #templetForm").ajaxSubmit(formEditOptions);
        })
    });
})
