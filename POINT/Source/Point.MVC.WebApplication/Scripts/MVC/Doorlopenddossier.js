﻿$(function () {
    //Ignore validation on all elements inside a container with ignore as classname
    $.validator.setDefaults({ ignore: '.ignore *' });

    $(window).on("resize", function () {
        var height = GetScrollHeight();
        $("#Scroller").css("max-height", height);
        $("#Scroller").css("height", height);
    });
    $(window).trigger("resize");

    $('[data-toggle="clickover"]:not(.client-header)').clickover({
        html: true,
        placement: $(this).attr("data-placement") ? $(this).data("placement") : "top",
        global_close: true,
        esc_close: true,
        template: '<div class="popover frequencytoelichting"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>',
        content: $(this).data("content")
    });

    $.each($("#doorlopenddossier[data-disabled='disabled'] :input:not([data-ignoredisabled='true'])"), function () {
        $(this).attr("disabled", "");
    });
});

function reopendoorlopenddossier(sender) {
    $.ajax({
        type: "GET",
        url: $(sender).attr("data-url"),
        success: function (data) { location.reload(); }
    });
}