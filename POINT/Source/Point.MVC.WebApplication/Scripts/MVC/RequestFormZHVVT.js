﻿var containers = ["Basisgegevens", "SituatieClient", "Toestemming", "Inventarisatie", "ZorgVraag", "HervattenZorg", "ZorgSoort", "Hulpmiddelen", "DatumOpname", "Toelichting"];

function handleEmployeeTelephoneNumber(data) {
    if (data !== undefined && data.length === 1) {
        $('#DossierOwnerTelephoneNumber').changeval(data[0].Text);
    }
}

function setAanvraagFormType(formtypeid) {
    var currentvalue = $("#RequestFormZHVVTType").attr("currentvalue");
    var newSections = sectiondata[formtypeid];
    $("section").each(function () {
        if ($(this).attr("data-section") !== undefined) {
            var section = $(this).attr("data-section");
            var index = $.inArray(section, newSections);
            $(this).toggle(index >= 0);

            if (formtypeid !== currentvalue)
            {
                if (index < 0 && $(this).is(":visible") === false) {
                    ClearContainerByObject(this);
                }
            }
        }
    });

    if (newSections !== undefined) {
        //Shuffle the sections within SituatieClient according to the sequence they are defined in RequestFormBL.cs
        var situatieClientDiv = $("div[id='SituatieClient']");
        $.each(newSections, function (ix, el) {
            var currentSection = $(situatieClientDiv).find("[data-section='" + el + "']");
            $(currentSection).detach().appendTo(situatieClientDiv);
        });
    }

    SetUpContainers();

    if (formtypeid === 213) { //Formulier kind
        $("#zorgverleningAspecten").hide();
        $("#divKwetsbareOudere").hide();
    } else {
        $("#zorgverleningAspecten").show();
        $("#divKwetsbareOudere").show();
    }

    ShowPhaseButtonSave(formtypeid);
    ShowPhaseButtonNext(formtypeid);
}

function ShowPhaseButtonSave(formtypeid) {
    if (formtypeid !== "") {
        $(".phaseButtonSave").show();
    }
    else {
        $(".phaseButtonSave").hide();
    }
}

function ShowPhaseButtonNext(formtypeid) {
    if (hasActiveNextPhase) {
        return;
    }
    if (formtypeid !== "") {
        $(".phaseButtonNext").show();
    }
    else {
        $(".phaseButtonNext").hide();
    }
}

function SearchOrganization(sender, url, callback) {
    ShowModal(url, callback, inittable);
}
function addParameter(url, parameterName, parameterValue, atStart/*Add param before others*/) {
    replaceDuplicates = true;
    if (url.indexOf('#') > 0) {
        var cl = url.indexOf('#');
        urlhash = url.substring(url.indexOf('#'), url.length);
    } else {
        urlhash = '';
        cl = url.length;
    }
    sourceUrl = url.substring(0, cl);

    var urlParts = sourceUrl.split("?");
    var newQueryString = "";

    if (urlParts.length > 1) {
        var parameters = urlParts[1].split("&");
        for (var i = 0; (i < parameters.length); i++) {
            var parameterParts = parameters[i].split("=");
            if (!(replaceDuplicates && parameterParts[0] == parameterName)) {
                if (newQueryString == "")
                    newQueryString = "?";
                else
                    newQueryString += "&";
                newQueryString += parameterParts[0] + "=" + (parameterParts[1] ? parameterParts[1] : '');
            }
        }
    }
    if (newQueryString == "")
        newQueryString = "?";

    if (atStart) {
        newQueryString = '?' + parameterName + "=" + parameterValue + (newQueryString.length > 1 ? '&' + newQueryString.substring(1) : '');
    } else {
        if (newQueryString !== "" && newQueryString != '?')
            newQueryString += "&";
        newQueryString += parameterName + "=" + (parameterValue ? parameterValue : '');
    }
    return urlParts[0] + newQueryString + urlhash;
};

var putZorginzetVoorDezeOpnameTypeZorginstellingNaam = function (department) {
    PutOrganisation(department, "ZorginzetVoorDezeOpnameTypeZorginstellingNaam");
}

var PutHealthcareProvider1 = function (department) {
    PutOrganisation(department, "ZorgInZorginstellingVoorkeurPatient1");
};

var PutHealthcareProvider2 = function (department) {
    PutOrganisation(department, "ZorgInZorginstellingVoorkeurPatient2");
};

var PutHealthcareProvider3 = function (department) {
    PutOrganisation(department, "ZorgInZorginstellingVoorkeurPatient3");
};

function PutOrganisation(department, targetid) {
    $('input[data-linked="' + targetid + '"]:not([type=hidden])').changeval(department.DepartmentName);
    $('input[data-linked="' + targetid + '"][type=hidden]').changeval(department.DepartmentID);
    $("#modal").modal('hide');
};

var getDefaultAftercareClassificationSelectionRequest = function (source, aftercareClassificationTypeID) {
    $.ajax({
        type: "POST",
        url: getDefaultAftercareClassificationSelectionUrl,
        data: { aftercareClassificationTypeID: aftercareClassificationTypeID, aftercareClassificationShowID: Enum.AftercareClassificationShowID.Request },
        success: function (data) {
            selectDefaultAftercareClassificationSelection(source, data);
        },
        error: function (data) {
        }
    });
}

var selectDefaultAftercareClassificationSelection = function (source, data) {
    $(source).changeval(data.AftercareClassificationID);
}

function hasData(object) {
    if (object) {
        return true;
    }
    else {
        return false;
    }
}


function SetUpContainers() {

    $(".sectionTitle").hide();
    $(".sectionContainer").each(function () {
        $(this).find("section:visible div.sectionTitle").first().show();
    });

    $("#sectionselector").find("a[data-location]").hide();
    $.each(containers, function () {
        if ($("#" + this).find("section[data-section]:visible .sectionTitle:visible").length > 0) {
            $('#sectionselector a[data-location="' + this + '"]').attr("href", "#" + this).show();
        }
    });

}

function DatumEindBehandelingChanged()
{
    var originalval = $("#hiddenoorspronkelijkeontslagdatum");
    if (originalval != null && ( originalval.val() == null || originalval.val() == 0 ))
    {
        var dateval = $("#DatumEindeBehandelingMedischSpecialist").val();
        $("#OorspronkelijkeDatumEindeBehandelingMedischSpecialist").changeval(dateval);        
    }
    
}

function setTelephone() {
    var employeeID = $(this).val();
    var digest = $(this).find(":selected").data("digest");

    var useemployee = $("#DossierOwnerTelephoneNumber").length > 0;
    if (useemployee) {
        $.ajax({
            type: "POST",
            url: getEmployeePhoneNumberByEmployeeID,
            data: { employeeID: employeeID, digest: digest },
            success: function (data) { handleEmployeeTelephoneNumber(data); }
        });
    }
}

function doformchange(formtype) {
    if (typeof formtype === "undefined") {
        formtype = "";
    }

    setAanvraagFormType(formtype);

    $('#RequestFormZHVVTType').val(formtype);
    $('#RequestFormZHVVTType').attr("currentvalue", formtype);

    if (formtype > 0) {
        $("#btnRelatedTransfers").show();
    }
    else {
        $("#btnRelatedTransfers").hide();
    }
}

function requestformchange(sender) {
    var currentvalue = $(sender).attr("currentvalue");
    var newvalue = $(sender).val();
    
    if (currentvalue !== "") {
        $("#FormTypeWarningGo").attr("onclick", "doformchange(" + newvalue + ");");
        $(sender).val(currentvalue);
        $("#changeFormTypeWarning").modal('show');
    } else {
        doformchange(newvalue);
    }
}

function SetUpScreen() {
    //Custom validatie toestemming patient
    $.validator.addMethod("validateToestemming", function (value, element) { return value == 'ja' || value == "niet mogelijk"; }, "Patient moet toestemming hebben gegeven.");
    //Custom validatie toestemming patient (sta ook leeg toe als waarde niet required is)
    $.validator.addMethod("validateToestemmingnotrequired", function (value, element) { return value === undefined || value == 'ja' || value == "niet mogelijk"; }, "Patient moet toestemming hebben gegeven.");

    var toestemmingPatientName = GetNameByID("#ToestemmingPatient");
    var toestemmingPatient = $("input[name='" + toestemmingPatientName + "']");

    if (toestemmingPatient.data("val-required") > '') {
        toestemmingPatient.rules("add", "validateToestemming");
    } else {
        toestemmingPatient.rules("add", "validateToestemmingnotrequired");
    }

    $("#cancelvvt").on("click", CancelVVT);

    $('#RequestFormZHVVTType').on("change", function (e) {
        requestformchange(this);
    });

    if (autoselectformtypeid > 0) {
        $('#RequestFormZHVVTType').changeval(autoselectformtypeid);
        doformchange(autoselectformtypeid);
    }
    else if ($('#RequestFormZHVVTType').val() > 0) {
        doformchange($('#RequestFormZHVVTType').val());
    }

    $('[data-toggle="clickover"]').clickover({
        html: true,
        placement: $(this).attr("data-placement") ? $(this).data("placement") : "top",
        global_close: true,
        esc_close: true,
        template: '<div class="popover zorgvraagpopover"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>',
        content: $(this).data("content")
    });

    $("#btnRelatedTransfers").on("click", function () {
        if (typeof showRelatedTransfersWithVVT == 'function') {
            showRelatedTransfersWithVVT();
        }
    });

    //Prefill DossierOwner Telephone
    $('#DossierOwner').on("change", setTelephone);

    //Alleen bij nieuw doc
    if (formSetVersionID === -1) {
        $("#DossierOwner").each(setTelephone);
    }

    //PreFill arts data
    $("#BehandelendArtsID").on("change", function () {
        var artsID = $(this).val();
        var digest = $(this).find(":selected").data("digest");
        $.ajax({
            type: "POST",
            url: getDoctorByID,
            data: { doctorID: artsID, digest: digest },
            success: function (data) {
                setDoctor(data);
            },
            error: function (data) {
                showErrorMessage(data);
            }
        });
    });

    var kwetsbareOudereJaNee = GetNameByID("#KwetsbareOudereJaNee");
    function kwetsbareoudere() {
        if ($(this).val() == "ja") {
            $("#divKwetsbareOudereJaNee").show();
        }
        else {
            $("#divKwetsbareOudereJaNee").hide();
            ClearContainer("#divKwetsbareOudereJaNee");
        }
    }
    $("[name='" + kwetsbareOudereJaNee + "']input:radio").on("click", kwetsbareoudere);
    GetSelectedRadioByName(kwetsbareOudereJaNee).each(kwetsbareoudere);


    //BMI
    $("#ObesitasGewicht, #ObesitasLengte").on("change", function () {
        CalculateBMI();
    });

    //PsychischFunctionerenJaNee
    MakeRadiosDeselectableByID("#PsychischFunctionerenJaNee");
    MakeRadiosDeselectableByID("#PsychischFunctionerenGeheugenverlies");
    MakeRadiosDeselectableByID("#PsychischFunctionerenDesorientatie");
    MakeRadiosDeselectableByID("#PsychischFunctionerenGedragsstoornissen");
    MakeRadiosDeselectableByID("#PsychischFunctionerenStemmingsstoornissen");
    MakeRadiosDeselectableByID("#PsychischFunctionerenDenkWaarnemingsstroornissen");
    MakeRadiosDeselectableByID("#PsychischFunctionerenItemIngevuldToelichting");
    var psychischFunctionerenJaNee = GetNameByID("#PsychischFunctionerenJaNee");

    function psychischfunctioneren() {
        if ($(this).val() == "nee" && $(this).is(":checked")) {
            $("#divPsychischFunctionerenJaNee").show();
        }
        else {
            $("#divPsychischFunctionerenJaNee").hide();
            ClearContainer("#divPsychischFunctionerenJaNee");
        }
    }
    $("[name='" + psychischFunctionerenJaNee + "']input:radio").on("click", psychischfunctioneren);
    GetSelectedRadioByName(psychischFunctionerenJaNee).each(psychischfunctioneren);

    //Tooltip handling
    $('[data-toggle="tooltip"]').tooltip({
        placement: 'top'
    });

    //Thought this would be handy but only used once...
    $(".source").find("input:checked").each(function () {
        $(this).closest(".form-group").find(".target").show();
    });

    $(".source").find("input").on("change", function () {
        var source = $(this);
        var target = $(this).closest(".form-group").find(".target");

        var ischecked = IsChecked(source);
        target.toggle(ischecked);

        if (!ischecked) {
            target.find("input").each(function () {
                $(this).prop('checked', false);
                $(this).prop('previousValue', false);
            });
        }
    });

    //Uncheck Radio
    MakeRadiosDeselectableByID("#RelevanteAspectenVoorZorgverleningIncontinentieKeus");

    //Infectie/Isolatie
    var infectieIsolatieJaNee = GetNameByID("#InfectieIsolatie");
    function infectieIsolatieAanwezig() {
        if ($(this).val() == "Ja") {
            $("#InfectieIsolatieAanwezig").show();
        }
        else {
            $("#InfectieIsolatieAanwezig").hide();
            ClearContainer("#InfectieIsolatieAanwezig");
        }
    }
    $("[name='" + infectieIsolatieJaNee + "']input:radio").on("click", infectieIsolatieAanwezig);
    GetSelectedRadioByName(infectieIsolatieJaNee).each(infectieIsolatieAanwezig); 

    // VrijheidsbeperkendeMaatregelen
    var VrijheidsbeperkendeMaatregelenJaNee = GetNameByID("#VrijheidsbeperkendeMaatregelen");
    function VrijheidsbeperkendeMaatregelenAanwezig() {
        if ($(this).val() === "Ja") {
            $("#VrijheidsbeperkendeMaatregelenAanwezig").show();
        }
        else {
            $("#VrijheidsbeperkendeMaatregelenAanwezig").hide();
            ClearContainer("#VrijheidsbeperkendeMaatregelenAanwezig");
        }
    }
    $("[name='" + VrijheidsbeperkendeMaatregelenJaNee + "']input:radio").on("click", VrijheidsbeperkendeMaatregelenAanwezig);
    GetSelectedRadioByName(VrijheidsbeperkendeMaatregelenJaNee).each(VrijheidsbeperkendeMaatregelenAanwezig); 

    //Verslaving
    function setVerslavingAanwezig() {
        $("#VerslavingAanwezig").toggle($(this).is(":checked"));
        if ($(this).is(":checked") == false)
            ClearContainer("#VerslavingAanwezig");
    }
    $("#Verslaving").on("change", setVerslavingAanwezig);
    $("#Verslaving").each(setVerslavingAanwezig);

    function CheckAndShowToestemming() {
        if (showResetVVT) {
            $("#resetvvtmodal").modal('show');
        }
    }

    function CancelVVT() {
        $.ajax({
            type: "POST",
            url: resetVVTUrl,
            async: true,
            success: function (data) {
                $("#resetvvtdone").modal('show');
            }
        });
    }

    //Toestemming
    var loadedToestemmingPatient = false;
    function setToestemmingPatient() {
        $("#ToestemmingPatientJa").toggle($(this).val() == "ja" && (this.checked || loadedToestemmingPatient == false));
        $("#ToestemmingPatientNee").toggle($(this).val() == "nee" && (this.checked || loadedToestemmingPatient == false));
        $("#ToestemmingPatientNietMogelijk").toggle($(this).val() == "niet mogelijk" && (this.checked || loadedToestemmingPatient == false));

        if (loadedToestemmingPatient) {
            if ($(this).val() == "ja" && this.checked) {
                ClearContainer("#ToestemmingPatientNee");
                ClearContainer("#ToestemmingPatientNietMogelijk");
            } else if ($(this).val() == "nee" && this.checked) {
                ClearContainer("#ToestemmingPatientJa");
                ClearContainer("#ToestemmingPatientNietMogelijk");
                CheckAndShowToestemming();
            } else if ($(this).val() == "niet mogelijk" && this.checked) {
                ClearContainer("#ToestemmingPatientNee");
                ClearContainer("#ToestemmingPatientJa");
            }
        }
    }

    MakeRadiosDeselectableByID("#ToestemmingPatient");
    toestemmingPatientName = GetNameByID("#ToestemmingPatient");
    $("[name='" + toestemmingPatientName + "']input:radio").on("click", setToestemmingPatient);
    GetSelectedRadioByName(toestemmingPatientName).each(setToestemmingPatient);
    loadedToestemmingPatient = true;

    function toestemmingVan() {
        var toestemmingPatientVanVertegenwoordiger = $(this).val() == "Vertegenwoordiger";
        $("#ToestemmingPatientVanVertegenwoordiger").toggle(toestemmingPatientVanVertegenwoordiger && this.checked);

        if ((toestemmingPatientVanVertegenwoordiger && this.checked) == false)
            ClearContainer("#ToestemmingPatientVanVertegenwoordiger");
    }

    //Toestemming Van
    var toestemmingPatientVanName = GetNameByID("#ToestemmingPatientVan");
    $("[name='" + toestemmingPatientVanName + "']input:radio").on("click", toestemmingVan);
    GetSelectedRadioByName(toestemmingPatientVanName).each(toestemmingVan);

    function zorgVoorOpname() {
        var ontvingZorgVoorOpname = $(this).val() == "ja";
        $("#ZorginzetVoorDezeOpname").toggle(ontvingZorgVoorOpname && this.checked);
        if (ontvingZorgVoorOpname == false)
            ClearContainer("#ZorginzetVoorDezeOpname");
    }
    //ZorgVoorOpname
    var ontvingZorgVoorOpnameName = GetNameByID("#PatientOntvingZorgVoorOpnameJaNee");
    $("[name='" + ontvingZorgVoorOpnameName + "']input:radio").on("click", zorgVoorOpname);
    GetSelectedRadioByName(ontvingZorgVoorOpnameName).each(zorgVoorOpname);

    //ZorgVraag
    $('#ZorgVraag :input').each(function () {
        $(this).on('keyup paste', function () {
            var trgroup = $(this).closest('tr');
            var checkbox = trgroup.find('div.checkbox :checkbox');
            if (checkbox.length === 1) {
                var noneEmptyElements = trgroup.find(':input:not(:checkbox):not(:hidden)').filter(function () { return $.trim($(this).val()).length > 0; });
                checkbox.prop('checked', (noneEmptyElements.length > 0));
                checkbox.trigger("change");
            }
        });
    });

    CheckboxToggleChildren("#MedicatieToediening", "#divMedicatieToediening");
    CheckboxToggleChildren("#CatheterVerzorging", "#divCatheterVerzorging");
    CheckboxToggleChildren("#Infusietherapie", "#divInfusietherapie");
    CheckboxToggleChildren("#NoodzakelijkTeRegelenHulpmiddelenAnders", "#divHulpmiddelenAnders");

    //Noodzakelijk te regelen hulpmiddelen
    function setHulpmiddelen() {
        $("#divNoodzakelijkTeRegelenHulpmiddelen").toggle($(this).is(":checked"));
        if ($(this).is(":checked") == false) {
            ClearContainer("#divNoodzakelijkTeRegelenHulpmiddelen");

            $("#divHulpmiddelenAnders").toggle($(this).is(":checked"));
            ClearContainer("#divHulpmiddelenAnders");

            $("#divAfleveringAnderAdres").toggle($(this).is(":checked"));
            ClearContainer("#divAfleveringAnderAdres");
        }
    }
    $("#NoodzakelijkTeRegelenHulpmiddelen").on("change", setHulpmiddelen);
    $("#NoodzakelijkTeRegelenHulpmiddelen").each(setHulpmiddelen);

    function afleveringname() {
        var afleveringDestination = $(this).val();
        $("#divAfleveringAnderAdres").toggle(afleveringDestination == "anders");
        if ((afleveringDestination == "Thuis" || afleveringDestination == "Ziekenhuis"))
            ClearContainer("#divAfleveringAnderAdres");
    }
    var afleveringName = GetNameByID("#NoodzakelijkTeRegelenHulpmiddelenAflevering");
    $("[name='" + afleveringName + "']input:radio").on("click", afleveringname);
    GetSelectedRadioByName(afleveringName).each(afleveringname);
}

function SetResize() {
    //Scrollable auto-resize
    $(window).on("resize", function () {
        var height = GetScrollHeight();
        $("#Scroller").css("max-height", height);
    });
    $(window).trigger("resize");
}

$(function () {
    ShowPhaseButtonNext($("#RequestFormZHVVTType option:selected").val());
    ShowPhaseButtonSave($("#RequestFormZHVVTType option:selected").val());

    setTimeout(SetUpContainers, 0);
    setTimeout(SetResize, 0);
    setTimeout(SetUpScreen, 0);

    if ($("#TakeRelatedTransfer").val() === "true" || $("#TakeRelatedTransfer").val() === "True") {
        $(".prefilledRelatedMemo").modal("show");
    }
});

function CalculateBMI() {
    var objWeight = document.getElementById("ObesitasGewicht");
    var objHeight = document.getElementById("ObesitasLengte");
    var objBMI = document.getElementById("ObesitasBMI");
    if (!objHeight || !objWeight || !objBMI) return;

    var weight = objWeight.value == "" ? "0" : objWeight.value;
    var height = objHeight.value == "" ? "0" : objHeight.value;

    var res = "";
    if (height != "0" && weight != "0") {
        var res = parseFloat(weight) / Math.pow(parseInt(height) / 100, 2);
        res = res.toFixed(1);
    }

    objBMI.value = res;
}