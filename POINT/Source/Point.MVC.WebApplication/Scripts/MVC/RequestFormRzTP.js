﻿function handleEmployeeTelephoneNumber(data) {
    if (data !== undefined && data.length === 1) {
        $('#DossierOwnerTelephoneNumber').val(data[0].Text);
    }
}

function SearchOrganization(sender, url, callback) {
    ShowModal(url, callback, inittable);
}

function PutOrganisation(organization, targetid) {
    $('input[data-linked="' + targetid + '"]:not([type=hidden])').val(organization.OrganizationName);
    $('input[data-linked="' + targetid + '"][type=hidden]').val(organization.OrganizationID);
    $("#modal").modal('hide');
};

var getDefaultAftercareClassificationSelectionRequest = function (source, aftercareClassificationTypeID) {
    $.ajax({
        type: "POST",
        url: getDefaultAftercareClassificationSelectionUrl,
        data: { aftercareClassificationTypeID: aftercareClassificationTypeID, aftercareClassificationShowID: Enum.AftercareClassificationShowID.Request },
        success: function (data) {
            selectDefaultAftercareClassificationSelection(source, data);
        },
        error: function (data) {
        }
    });
}

var selectDefaultAftercareClassificationSelection = function (source, data) {
    $(source).val(data.AftercareClassificationID);
}

function SetUpScreen() {

    $('[data-toggle="clickover"]').clickover({
        html: true,
        placement: $(this).attr("data-placement") ? $(this).data("placement") : "top",
        global_close: true,
        esc_close: true,
        template: '<div class="popover zorgvraagpopover"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>',
        content: $(this).data("content")
    });

    //Prefill DossierOwner Telephone
    $('#DossierOwner').on("change", function () {
        var employeeID = $(this).val();
        var digest = $(this).find(":selected").data("digest");
        $.ajax({
            type: "POST",
            url: getEmployeePhoneNumberByEmployeeID,
            data: { employeeID: employeeID, digest: digest },
            success: function (data) { handleEmployeeTelephoneNumber(data); }
        });
    });

    //Alleen bij nieuw doc
    if (formSetVersionID === -1) {
        $("#DossierOwner").trigger("change");
    }

    //PreFill arts data
    $("#BehandelendArtsID").on("change", function () {
        var artsID = $(this).val();
        var digest = $(this).find(":selected").data("digest");
        $.ajax({
            type: "POST",
            url: getDoctorByID,
            data: { doctorID: artsID, digest: digest },
            success: function (data) {
                setDoctor(data);
            },
            error: function (data) {
                showErrorMessage(data);
            }
        });
    });

    //BMI
    $("#ObesitasGewicht, #ObesitasLengte").on("change", function () {
        CalculateBMI();
    });

    //Tooltip handling
    $('[data-toggle="tooltip"]').tooltip({
        placement: 'top'
    });

    //Thought this would be handy but only used once...
    $(".source").find("input:checked").each(function () {
        $(this).closest(".form-group").find(".target").show();
    });

    $(".source").find("input").on("change", function () {
        var source = $(this);
        var target = $(this).closest(".form-group").find(".target");

        var ischecked = IsChecked(source);
        target.toggle(ischecked);

        if (!ischecked) {
            target.find("input").each(function () {
                $(this).prop('checked', false);
                $(this).prop('previousValue', false);
            });
        }
    });

    //Uncheck Radio
    MakeRadiosDeselectableByID("#RelevanteAspectenVoorZorgverleningIncontinentieKeus");
}

function SetResize() {
    //Scrollable auto-resize
    $(window).on("resize", function () {
        var height = GetScrollHeight();
        $("#Scroller").css("max-height", height);
    });
    $(window).trigger("resize");
}

$(function () {
    setTimeout(SetResize, 0);
    setTimeout(SetUpScreen, 0);
});

function CalculateBMI() {
    var objWeight = document.getElementById("ObesitasGewicht");
    var objHeight = document.getElementById("ObesitasLengte");
    var objBMI = document.getElementById("ObesitasBMI");
    if (!objHeight || !objWeight || !objBMI) return;

    var weight = objWeight.value == "" ? "0" : objWeight.value;
    var height = objHeight.value == "" ? "0" : objHeight.value;

    var res = "";
    if (height != "0" && weight != "0") {
        var res = parseFloat(weight) / Math.pow(parseInt(height) / 100, 2);
        res = res.toFixed(1);
    }

    objBMI.value = res;
}