﻿String.prototype.startsWith = function (pattern) {
    return this.lastIndexOf(pattern, 0) === 0;
}

String.prototype.endsWith = function (pattern) {
    var d = this.length - pattern.length;
    return d >= 0 && this.indexOf(pattern, d) === d;
}


function getSiblingByID(el, tagname, elementid) {
    elp = el.parentNode.parentNode;
    els = elp.getElementsByTagName(tagname);
    for (var i = 0; i < els.length; i++) {
        if (els[i].id.endsWith(elementid))
            return els[i];
    }
    return null;
}

function getSiblingByName(el, tagname, elementid) {
    elp = el.parentNode.parentNode;
    els = elp.getElementsByTagName(tagname);
    for (var i = 0; i < els.length; i++) {
        if (els[i].name.endsWith(elementid)) {
            if (els[i].type != "radio")
                return els[i];
            else
                if (els[i].checked)
                    return els[i];
        }
    }
    return null;
}

function setExpectedTime(el, tagname, elementid) {
    txb = getSiblingByID(el, tagname, elementid);
    if (txb == null) return;
    if (txb.value == '') {
        txb.value = txb.getAttribute("ExpectedTime");
    }
}

function calculateTotal() {
    var t = document.getElementById("tblActionHealth");
    var dStart = document.getElementById(startDatumID);
    var dEnd = document.getElementById(eindDatumID);
    var trows = t.getElementsByTagName("tr"); // Note: This also return the tr's of the radiobuttonlist and other included tables
    var totAmount = 0;

    for (var i = 0; i < trows.length; i++) { //

        var tels = trows[i].getElementsByTagName("input"); // Get all the input's for this row.

        for (var j = 0; j < tels.length; j++) {

            var txbExpectedTime = null;
            var txbAmount = null;
            var rblUnit = null;

            
            if (tels[j].id.endsWith("txbExpectedTime")) { 
                txbExpectedTime = tels[j]; 
            }
            else if (tels[j].id.endsWith("hdfExpectedTime")) {
                txbExpectedTime = tels[j];
            }
                
            if (txbExpectedTime != null ) {
                
                txbAmount = getSiblingByID(tels[j], "input", "txbAmount");
                rblUnit = getSiblingByName(tels[j], "input", "rblUnit");

                if (txbExpectedTime != null && txbExpectedTime.attributes["IsIfNeeded"] == "True") {
                    if (txbAmount == null) {
                        txbAmount = document.createElement("input");
                        txbAmount.type = "text";
                        txbAmount.value = "1";
                    }

                    if (rblUnit == null) {
                        rblUnit = document.createElement("input")
                        rblUnit.type = "radio";
                        rblUnit.value = "duur";
                    }
                }

                
                //}
            }

            
        }

        totAmount = totAmount + calculateLineTime(txbAmount, rblUnit, txbExpectedTime, dStart, dEnd);


    }


    if (isNaN(totAmount)) {
        // Skip
    }
    else if (totAmount > 0) {
        var tot = document.getElementById(totaleTijdID);
        tot.value = Math.round(totAmount / 60);
        var lbltot = document.getElementById(totaleTijdLabelID);
        lbltot.innerHTML = Math.round(totAmount / 60) + "&nbsp;uur";

    }


}

function parseDate(dIn, useUTC) {
    dSplit = dIn.split("-");
    if (useUTC === true)
        return new Date(Date.UTC(dSplit[2], dSplit[1] - 1, dSplit[0], 0, 0, 0, 0));
    else
        return new Date(dSplit[2], dSplit[1] - 1, dSplit[0]);
}

function calculateLineTime(txbAmount, rblUnit, txbExpectedTime, dStart, dEnd) {
    if (txbAmount == null || rblUnit == null || txbExpectedTime == null) return 0;

    var amount = 0;
    if (!isNaN(txbAmount.value)) {
        amount = (txbAmount.value == "" ? 0 : parseInt(txbAmount.value));
    }

    var expectedTime = 0;
    if (txbExpectedTime.value == null || txbExpectedTime.value == "" || isNaN(txbExpectedTime.value))
        expectedTime = parseInt(txbExpectedTime.getAttribute("ExpectedTime"));
    else
        expectedTime = parseInt(txbExpectedTime.value);

    if (expectedTime == "" || isNaN(expectedTime)) expectedTime = 0;


    var dateStart = parseDate(dStart.value, true);
    var dateEnd = parseDate(dEnd.value, true);

    var days = (dateEnd - dateStart) / (1000 * 60 * 60 * 24); //datediff works with milliseconds
    days = days + 1; // Add one day to include "dateEnd"-day

    var factor = 1;
    switch (rblUnit.value) {
        case "dag":
            factor = 1;
            break;
        case "week":
            factor = 1 / 7;
            break;
        case "maand":
            factor = 1 / 30;
            break;
        case "duur":
            factor = 1 / days;
            break;
        default:
            factor = 0;
    }

    var t = (factor * days * amount * expectedTime);
    if (t == "" || isNaN(t)) t = 0;

    return t;

}

