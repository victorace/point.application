
$(document).ready(function () {

    $(window).resize(function () {
        $("#divNavigation").height(getViewportHeight() - $("#divNavigation").offset().top - 14);
    });
    $(window).resize();

    //Handle the colors of the section
    handleColors();

    //AddHandlers
    addHandlers();

    //Add a click-event to all the headers. !!! Add this in the doc.ready, not in the addHandlers()
    $(scrollPanelID + " .aanvraag-sectie-header").click(function () {
        handleTitleBar($(this));
    });

    // Save the scroll-position while scrolling
    $(scrollPanelID).scroll(function () {
        $(pageMainID + "_hdfScrollPosition").val($(this).scrollTop());
    });

    //If a scrollposition is known, go back to it
    var lastScrollPosition = $(pageMainID + "_hdfScrollPosition").val();
    if (lastScrollPosition.length > 0 && lastScrollPosition > 0)
        $(scrollPanelID).scrollTop(lastScrollPosition);

    //Loop all the tr's with a childFormField, preventing to loop (radio)controls we don't need
    var formFields = [];

    $(scrollPanelID + " tr[childFormField]").each(function () {
        //Add to the array
        formFields.push($(this).attr("childFormField"));
        //and hide the row by default
        $(this).toggle(false);
    });

    //Disable RadioButtonList's Radio inputs with "disabledControlTag" attribute
    $(scrollPanelID + " table[disabledControlTag]").each(function () {
        var val = $(this).attr("disabledControlTag");
        $(scrollPanelID + " table[disabledControlTag='" + val + "'] input[type='radio']").each(function () {
            $(this).attr('disabled', true);
        });
    });

    //Disable DropDownLists with "disableDropDown" attribute
    $(scrollPanelID + " select[disableDropDown]").each(function () {
        var val = $(this).attr("disableDropDown");
        $(scrollPanelID + " select[disableDropDown='" + val + "'] option").each(function () {
            if (!$(this).attr('selected')) {
                $(this).attr('disabled', true);
            }
        });
    });

    //Voeding meetmethode handling 
    $(pageMainID + "_VoedingOndervoedingAanwezigMeetmethode input[value='SNAQ']").next('label').text('SNAQ')
    $(pageMainID + "_VoedingOndervoedingAanwezigMeetmethode input[value='MUST']").next('label').text('MUST')

    $(pageMainID + "_VoedingOndervoedingAanwezigMeetmethode input").click(function () {
        handleVoeding(this);
    });
    $(pageMainID + "_VoedingOndervoedingAanwezigScore").change(function () {
        $(pageMainID + "_VoedingOndervoedingAanwezigScoreTxt").val($(this).val());
    });

    parseValidationRules();



    // Limit the textareas
    $("textarea[maxlength]").bind("keyup input paste", function () {
        var limit = parseInt($(this).attr('maxlength'));
        var text = $(this).val();
        var chars = text.length;
        if (chars > limit) {
            var new_text = text.substr(0, limit);
            $(this).val(new_text);
        }
    });

    $("textarea.SmallText").each(function () {
        resizeTextarea(this);
    });

    $(".HulpMiddelenCheckbox input").each(function () {
        checkhulpmiddelen(this);
    });

    setTotals();

    $(".BlueBoxRight input").bind("keyup click", function () {
        setTotals();
    });

    $(".validation").click(function () {
        var hasErrors = formHasErrors();
        if (hasErrors) { return false; }
    });

    $('.testvalidation').click(function () {
        var allstart = (new Date).getTime();
        var hasErrors = formHasErrors();
        var allend = (new Date).getTime();

        $(this).val( (hasErrors?'NOT ':'') + ' valid, duration:' + (allend - allstart)); return false;
    });




});

function checkhulpmiddelen(checkbox) {
    var checked = checkbox.checked;
    if (checked) {
        $("#tblHulpmiddelen").show();
    }
    else {
        $("#tblHulpmiddelen").hide();
        clearSubsection($("#tblHulpmiddelen"));
    }
}

function resizeTextarea(textarea) {
    var str = $(textarea).val();
    var cols = textarea.cols;
    var linecount = 1;
    $(str.split("\n")).each(function (i,l) {
        linecount += 1 + Math.floor(l.length / cols);
    })
    textarea.rows = linecount;
};

function setTotals()
{
    //Calculate total
    var totalminutes = calculateMinutes();

    var totalhours = Math.floor(totalminutes / 60);
    var partminutes = totalminutes;
    if (totalhours > 0) {
        partminutes = totalminutes - (totalhours * 60);
    }

    partminutes = pad(partminutes, 2);

    $("#TotalsAmount").html(totalhours + ':' + partminutes + ' uur/maand');
}

function pad(str, max) {
    str = str.toString();
    return str.length < max ? pad("0" + str, max) : str;
}

function calculateMinutes()
{
    //Calculate total
    var total_minutes = 0;
    $(".CarePlanControlItem .BlueBoxRight").each(function () {
        var tdByVal = $(this).find("#chkByProf input:checked").val();
        var tdFreqVal = $(this).find("#spanFreq input").val();
        var tdTimespanVal = $(this).find("#spanTimespan input:checked").val();
        var tdMinutesVal = $(this).find("#spanMinutes input").val();

        var amount_per_maand = 0;
        var minutes = 0;
        if (tdTimespanVal == "per dag" && tdByVal != undefined && tdFreqVal != "" && tdMinutesVal != "") {
            amount_per_maand = tdFreqVal * 28;
            minutes = amount_per_maand * tdMinutesVal;

        }
        else if (tdTimespanVal == "per week" && tdByVal != undefined && tdFreqVal != "" && tdMinutesVal != "") {
            amount_per_maand = tdFreqVal * 4;
            minutes = amount_per_maand * tdMinutesVal;
        }
        else if (tdTimespanVal == "per maand" && tdByVal != undefined && tdFreqVal != "" && tdMinutesVal != "") {
            amount_per_maand = tdFreqVal;
            minutes = amount_per_maand * tdMinutesVal;
        }

        total_minutes += minutes;
    });

    return total_minutes;
}

var errorsList = $([]);

$.fn.extend({

    format: function (source, params) {
	    if ( arguments.length === 1 ) {
	        return function() {
	            var args = $.makeArray(arguments);
	            args.unshift(source);
	            return $.validator.format.apply( this, args );
	        };
	    }
    if ( arguments.length > 2 && params.constructor !== Array  ) {
        params = $.makeArray(arguments).slice(1);
    }
    if ( params.constructor !== Array ) {
        params = [ params ];
    }
    $.each(params, function(i, n) {
        source = source.replace(new RegExp("\\{" + i + "\\}", "g"), n);
    });
    return source;
    },

    elementVal: function (element) {
        var type = $(element).attr('type'),
				val = $(element).val();

        if (type === 'radio' || type === 'checkbox') {
            return $('input[name="' + $(element).attr('name') + '"]:checked').val() || "";
        }

        if (typeof val === 'string') {
            return val.replace(/\r/g, "");
        }
        return val;
    },

    errorsFor: function (element) {
        var nm = $.fn.idOrName(element);

        var errorforelement; // leave it undefined
        for (var i = 0, len = errorsList.length; i < len; i++) {
            if (errorsList[i].name == nm) {
                errorforelement = errorsList[i].errorelement;
                break;
            }
        };

        return errorforelement;
    },

    idOrName: function (element) {
        //return element.id || element.name; 
        return ($.fn.checkable(element) ? element.name : element.id || element.name);
    },
    checkable: function (element) {
        return (/radio|checkbox/i).test(element.type);
    },


    showErrorLabel: function (message) {
        var element = this[0];

        if (this.is("select")) { 
            if (!this.parent().is("div")) {
                var errordiv = $("<div/>")
                    .css("display", "inline")
                    .css("padding-top", "2px")
                    .css("padding-bottom","4px")
                    .addClass("error");
                this.wrap(errordiv);
            }
            else {
                this.parent().addClass("error");
            }
        }
        else if (this.is("input:radio")) {
            this.closest("table").addClass("error");
        }
        else if (this.is("input:checkbox")) {
            this.closest("table").addClass("error");
        }
            //always add the 'error'-class to the element for findability
        else {
            this.addClass("error");
        }


        //var label = $.fn.errorsFor(element);
        //if (label != undefined) {
        //    label.removeClass("valid").addClass("error");
        //    label.html(message);
        //}
        //else {
        //    label = $("<label/>")
        //        .addClass("error")
        //        .html(message);
        //    $.fn.errorPlacement(label, $(element));
        //    errorsList.push({ name: $.fn.idOrName(element), errorelement: label });

        //}
    },


    hideErrorLabel: function () {
        var element = this[0];
        var error_element = element;

        if (this.is("select")) {
            if (this.parent().is("div")) {
                this.parent().removeClass("error");
                error_element = this.parent();
            }
        }
        else if (this.closest("table").attr("FormField") != undefined) {
            this.closest("table").removeClass("valid error");
            error_element = this.closest("table")[0];
        }
        else if (this.is("table")) {
            this.removeClass("valid error");
        }
        else if (this.is("input:radio")) {
            this.closest("table").removeClass("valid error");
            error_element = this.closest("table")[0];
        }
        else if (this.is("input:checkbox")) {
            this.closest("table").removeClass("valid error");
            error_element = this.closest("table")[0];
        }
            //always remove the 'error'-class to the element for findability
        else {
            this.removeClass("valid error");
        }

        var label = $.fn.errorsFor(error_element);
        if (label != undefined) {
            label.removeClass("valid error");
            label.html("");
        }
    },


    errorPlacement: function (label, element) {
        if (element.is("input:radio")) {
            label.css("float", "left").css("padding-top","10px");
            label.insertAfter(element.closest("table").css("float","left")); //Add the css-tweak since we have the table-element already
        }
        else if (element.is("input:checkbox")) {
            label.insertAfter(element.closest("tr").find("td:last *:last"));
        }
        else if (element.attr("id").match(/txt_Date$/)) {
            label.insertAfter(element.closest("table").find("img"));
        }
        else if (element.is("select")) { 
            label.insertAfter(element.closest("div"));
        }
        else {
            label.insertAfter(element);
        }
    },

    requiredPlacement: function (label) {
        var element = this;
        if (element.is("input:radio") || element.is("input:checkbox") || element.attr("id").match(/txt_Date$/)) {
            element.closest("table").closest("tr").find("td:nth-child(4)").append(label); // a little too hardcoded
        }
        else {
            element.closest("tr").find("td:nth-child(4)").append(label);  // a little too hardcoded
        }
    },

    validDependent: function(recursiveCall) {

        if (recursiveCall) return;

        var currentFormField = $(this).attr("FormField");
        if (validationRules[currentFormField] && validationRules[currentFormField].Depends) {
            var fieldset = this.closest("fieldset"); //limit it's searchscope
            var dependingon = fieldset.find("[FormField='" + validationRules[currentFormField].Depends + "']").first();
            if (dependingon) {
                dependingon.validElement(true);
            }
        }
        else {
            for (var validationRule in validationRules) {
                if (validationRules[validationRule].Depends && currentFormField && validationRules[validationRule].Depends == currentFormField) {
                    var fieldset = this.closest("fieldset"); //limit it's searchscope
                    var dependingon = fieldset.find("[FormField='" + validationRule + "']").first();
                    if (dependingon) {
                        dependingon.validElement(true);
                    }
                }
            }
        }
    },
    
    validElement: function(recursiveCall) { 
        var element = this;
        var elementChecks;
        
        var value = PointVal(element);

        if (this.is('input:radio')) {
            elementChecks = this.closest("table").data('elementChecks')
            var value = PointVal(this.closest("table"));
        }
        else if (this.is('input:checkbox')) {
            elementChecks = this.closest("table").data('elementChecks')
            var value = PointVal(this.closest("table"));
        }
        else {
            elementChecks = this.data('elementChecks');
        }

        if (!elementChecks) 
        { 
            if (!recursiveCall) this.validDependent(recursiveCall);
            return true; 
        }

        //using the .each for this one, since it only loops on a few (<5) elements
        var errorMessage = "";
        $.each(elementChecks, function () {
            var elementCheck = this;
            if (elementCheck.regx) {
                if (!elementCheck.regx.test(value)) {
                    errorMessage = elementCheck.mess;
                }
            }
            if (errorMessage != "") { return false; } //Leave the for-loop, not checking other validationRules

            if (elementCheck.maxl) {
                if (element.val().length > elementCheck.maxl) {
                    errorMessage = elementCheck.mess;
                }
            }
            if (errorMessage != "") { return false; } //Leave the for-loop, not checking other validationRules

            if (elementCheck.reqr) {
                var val = PointVal(element);
                //var isradio = element.is('input:radio');
                //var ischeckbox = element.is('input:checkbox');
                //if (isradio || ischeckbox)
                //    val = $('[name="' + element.attr('name') + '"]:checked').val();
                //else
                //    val = value;
                
                if (elementCheck.reqr === 1 || elementCheck.reqr === 4)
                {
                    if (val === undefined || val == "") {
                        var parentelement = (isradio ? element.closest("table").closest("tr") : element.closest("tr"));
                        if ((parentelement.css("display") == "table-row" || parentelement.css("display") == "block") && !checkAllEmpty(element.closest("fieldset")))
                            errorMessage = elementCheck.mess;
                    }
                }
                else if (elementCheck.reqr === 2)
                {
                    var fieldset = element.closest("fieldset"); //limit it's searchscope
                    var currentFormField = element.attr("FormField"); //get the FormField-name of the current element
                    
                    for (var validationRule in validationRules) {
                        if (validationRules[validationRule].Depends == currentFormField) {
                            var dependingon = fieldset.find("[FormField='" + validationRule + "']").first();
                            
                            if ( (val === undefined || val == "") && PointVal(dependingon) != "") {
                                errorMessage = elementCheck.mess;
                            }
                        }
                    }

                }
            }
            if (errorMessage != "") { return false; } //Leave the for-loop, not checking other validationRules

            if (elementCheck.depn) {
                if (value == "") {
                    var fieldset = element.closest("fieldset"); //limit it's searchscope
                    var dependson = fieldset.find("[FormField='" + elementCheck.depn + "']").first();
                    if (isYes(dependson) || (PointVal(dependson) != "" && PointVal(dependson) != "0" && PointVal(dependson) != "functie.0")) {
                        errorMessage = elementCheck.mess;
                    }
                }
            }
            if (errorMessage != "") { return false; } //Leave the for-loop, not checking other validationRules

            if (elementCheck.depg) {
                    var fieldset = element.closest("fieldset"); //Limit it's searchscope
                    if (!checkAllEmpty(element.closest("fieldset"))) { //Only continue if the section isn't empty
                        var dependents = fieldset.find("[depg='" + elementCheck.depg + "']");
                        var oneisfilled = false;
                        $.each(dependents, function () {
                            if ($(this).val() != "") {
                                oneisfilled = true;
                                return false; //exit this .each
                            }
                        });
                        if (!oneisfilled) {
                            errorMessage = elementCheck.mess;
                            $.each(dependents, function () {
                                $(this).showErrorLabel(errorMessage);
                            });
                        }
                        else {
                            errorMessage = "";
                            $.each(dependents, function () {
                                $(this).hideErrorLabel();
                            });
                        }
                    }
            }
            if (errorMessage != "") { return false; } //Leave the for-loop, not checking other validationRules

        });

        if (errorMessage != "") {
            element.showErrorLabel(errorMessage);
            return false;
        }
        else  {
            element.hideErrorLabel();
            if (!recursiveCall) {
                if (this.is('input:radio'))
                {
                    this.closest("table").validDependent(recursiveCall);
                }
                else if (this.is('input:checkbox'))
                {
                    this.closest("table").validDependent(recursiveCall);
                }
                else
                {
                    this.validDependent(recursiveCall);
                }
            }
            return true;
        }
    }



});

function formHasErrors() {

    
    var errorInForm = false;
    var firstElementWithError = null;

    $(scrollPanelID + " fieldset").each(function () {

        var section = $(this).attr("id").substring(3);

        var errorInSection = false;

        if (!checkAllEmpty(this)) {
            
            $(this).find(":input:enabled").each(function (index, item) { //Check/validate all the fields which aren't disabled
                if ($(item).closest("table").attr("FormField") != undefined)
                {
                    var elementIsValid = $(item).closest("table").validElement(true);
                    if (!elementIsValid && firstElementWithError == null) firstElementWithError = $(item).closest("table");
                    errorInSection = !elementIsValid || errorInSection;
                } else {
                    var elementIsValid = $(this).validElement(true);
                    if (!elementIsValid && firstElementWithError == null) firstElementWithError = $(this);
                    errorInSection = !elementIsValid || errorInSection;
                }
            });

            if (errorInSection) { //($(this).find(".error").length > 0) {
                var chbItem = $(leftPaneID + "_chb" + section);
                chbItem.prop('checked', true); //Open the section with an error
                handleCheck(chbItem);
                errorInForm = errorInForm || errorInSection;
            }

        }
    });

    if (errorInForm) {

        var firsterror = $("#aspnetForm").find("text.error, textarea.error, div.error, table.error, label.error").first(); //Get the first invalid field
        if (firsterror == undefined || firsterror == null || firsterror.length <= 0) { firsterror = firstElementWithError; }
        if (firsterror != undefined && firsterror != null && firsterror.length > 0) {
            var section = firsterror.closest("fieldset").attr("id").substring(3);
            var chbItem = $(leftPaneID + "_chb" + section);
            handleScroll(chbItem); //Move the first section with an error to top
            firsterror.focus(); //Set the focus to the error field
        }
    }

    return errorInForm;
}

function parseValidationRules() {

    for (var ctrlName in validationRules) {

        var validationRule = validationRules[ctrlName];
        var element = $("[FormField='" + ctrlName + "']");

        var elementChecks = $([]);

        var currentFormField = element.attr("FormField"); //get the FormField-name of the current element
        if (currentFormField == "VoedingOndervoedingAanwezigMeetmethode") {
            var test = true;
        }

        switch (validationRule["Type"]) {
            case 1: //Number
                elementChecks.push({ regx: /^(\d*)$/, mess: "Alleen cijfers" });
                break;
            case 2: //Numeric
                elementChecks.push({ regx: /^\d*([.,]\d*)?$/, mess: "Alleen cijfers met &eacute;&eacute;n punt of komma" });
                break;
            case 3: //Character
                break;
            case 4: //Dropdown
                break;
            case 5: //Radio
                break;
            case 6: //RegExp
                break;
            case 7: //DateTime
                element = $(pageMainID + '_' + ctrlName + "_txt_Date");
                element.attr("FormField", ctrlName);
                elementChecks.push({ regx: /^((0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-](19|20|21)\d{2}?)?$/, mess: "Vul een geldige datum in (dd-mm-jjjj)" }); 
                break;
            case 8: //Email
                //elementChecks.push({ regx: /^((([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5}){1,25})+([;.](([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5}){1,25})+)*)?$/, mess: "Vul een geldig e-mailadres in" });
                break;
            case 9: //Phone
                //elementChecks.push({ regx: /^(([\d\+\(\)\-]{10,14}))?$/, mess: "Vul een geldig telefoonnummer in" });
                break;
            default:
                break;
        }
        
        if (validationRule["MaxLength"]) {
            var ml = validationRule["MaxLength"];
            if (element.is("textarea")) { ml = 1000; }  // Overruling listvalues.vb (400 according to mail Ismil 23-09-13, 800 30-10-13, 1000 21-11-13)
            element.prop('maxLength', ml); // Use HTML's default property for this, to prevent users entering/pasting too much 
            elementChecks.push({ maxl: ml, mess: $.fn.format("U kunt tekst invoeren tot een maximum van {0} tekens", ml) });
        }
        
        if (validationRule["Required"]) {
            var validationMessage = "Verplicht"; //Default-message for RequiredType=1 (Field is required)
            var titleMessage = validationMessage;
            var labelColor = "#000000";
            var labelText = "*";
            if (validationRule["Required"] == 2) {
                validationMessage = "Verplicht"; //, na invullen van volgende(n)"; //More info when required is DependingOn child-fields
                titleMessage = "Verplicht";
                labelText = "&#176;"
            }
            
            if (validationRule["Required"] == 1 || validationRule["Required"] == 2 || validationRule["Required"] == 4) {
                elementChecks.push({ reqr: validationRule["Required"], mess: validationMessage });
                //var label = $("<label/>").html(labelText).css("float", "right").css("color", labelColor).attr("title", titleMessage);
                //$(element).requiredPlacement(label);
            }
        }

        if (validationRule["DependsOnGroup"]) {
            var validationMessage = "Vul minimaal &eacute;&eacute;n van deze velden";
            var labelColor = "#C0C0C0";

            elementChecks.push({ depg: validationRule["DependsOnGroup"], mess: validationMessage });
            element.attr("depg", validationRule["DependsOnGroup"]); //add it as an attribute to find it a little easier
            
            //var label = $("<label/>").html("1").css("float", "right").css("color", labelColor).attr("title", validationMessage);
            //$(element).requiredPlacement(label);
        }

        if (validationRule["Depends"]) {
            if (validationRule["Required"] == 3) {
                var validationMessage = "Verplicht"; //, na invullen van voorgaande(n)"; //Default-message for RequiredType=1 (Field is required)
                elementChecks.push({ depn: validationRule["Depends"], mess: validationMessage});
            }
        }

        if (validationRule["HandleRadioValue"]) {
            $("input", element).attr("radioval", validationRule["HandleRadioValue"]);
        }

        element.data('elementChecks', elementChecks);

        element.keyup(function (event) {
            $(this).validElement();
        });

        if (element.is(":input")) {
            element.blur(function (event) {
                $(this).validElement();
            });
        }


        if (validationRule["Type"] == 5)
        {
            element.find("input:radio").change(function (event) {
                $(this).validElement();
            });
            element.find("input:checkbox").change(function (event) {
                $(this).validElement();
            });
        }

    }


}


function handleRadio(item, doFocus) {
    var section = item.closest("table").attr("FormField"); //Since we have this attribute already... use it...

    var selectedYes = false;

    if (item.attr("radioval"))
        selectedYes = (item.val() == item.attr("radioval"))
    else
        selectedYes = isYes(item);

    var sect = $("tr[childFormField='" + section + "']");
    if (!selectedYes) {
        //Clear the fields
        sect.find('input:text, input:password, input:file, select, textarea').filter(":enabled").val('');
        sect.find('input:radio, input:checkbox').filter(":enabled").removeProp('checked').removeProp('selected');
    }

    //Show/hide the tablerows according to what's selected                    
    sect.toggle(selectedYes);

    if (doFocus && selectedYes) {
        sect.find(":input:visible").first().focus();
    }

}

function PointVal(item) {
    var val = item.val();
    if (item.find("input:checked").length > 0)
        val = item.find("input:checked").val();

    return val;
}

function isYes(item) {
    var val = PointVal(item);
    if (val === "") return false;
    return ($.inArray(val.toLowerCase(), ["ja", "yes"]) > -1);
}

function isNo(item) {
    var val = PointVal(item);
    if (val === "") return false;
    return ($.inArray(val.toLowerCase(), ["nee", "no"]) > -1);
}

function isUnknown(item) {
    var val = PointVal(item);
    if (val === "") return false;
    return ($.inArray(val.toLowerCase(), ["onbekend", "unknown"]) > -1);
}

function checkAllEmpty(item) {

        var allEmpty = true;
        var section = $(item).attr("id").substring(3);

        //Loop all the textboxes and textareas
        $(item).find("input[type='text'], textarea").each(function () {
            if ($(this).val() !== "") {
                allEmpty = false;
                return false;
            }
        });
        if (!allEmpty) return false;

        //Do a count of all checkboxes and radiobuttons which are checked, i.e. having a selected value
        if ($(item).find("input:checked").length > 0) {
            allEmpty = false;
        }
        if (!allEmpty) return false;

        $(item).find("select").each(function () {
            if ($(this).prop("selectedIndex") > 0) {
                allEmpty = false;
                return false;
            }
        });
        if (!allEmpty) return false;

        return allEmpty;

}

function handleColors() {

    $(scrollPanelID + " fieldset").each(function () {

        var checkstart = (new Date).getTime();


        var allEmpty = checkAllEmpty(this);
        var section = $(this).attr("id").substring(3);

        if (allEmpty == true) {
            //All the fields are empty. Overrule the blue colors, by their grey-equivalents
            $(this).find("td.blueBar").toggleClass('greyBar');
            $(this).find("td.blueHeader").toggleClass('greyStreepSituatie');
            $(this).find("td.CaptionCell").toggleClass('greyCaptionCell');
        }
        else {
            //A field is filled (and saved to the database)
            var chbItem = $(leftPaneID + "_chb" + section);
            //Toggle the "blueExpanded" css-class, based on the state of the checkbox (passed as item), select the 2nd tablecell
            $(chbItem).closest("tr").find("td:nth-child(2)").toggleClass('blueExpanded');
        }

    });

}

function handleCheck(item) {
    //Get the section from the closest tablerow the clicked checkbox is in
    var section = $(item).closest("tr").attr("section");

    //Toggle the panel, based on the state of the checkbox (passed as item)
    $(pageMainID + "_pnl" + section).toggle($(item).is(":checked"));

    //Fix the small line beneath the first td in the fieldset
    $("#fls" + section).find("td:first").toggleClass("blueBarFixBottom", !$(item).is(":checked"));
}

function handleTitleBar(item) {
    //Item will be a 'td' (or perhaps an 'a' in that 'td'). Get the section from its closest parent (being a 'fieldset')
    var section = $(item).closest("fieldset").attr("id").substring(3);
    //Find and (un)check the according checkbox and call the method which handles the (un)checking
    var chbItem = $(leftPaneID + "_chb" + section);
    chbItem.prop('checked', !$(chbItem).is(":checked"));
    handleCheck(chbItem);
}

function handleScroll(item) {
    //Get the section of the tablerow that's clicked
    var section = $(item).attr("section");

    if (section == undefined)
        section = $(item).closest("tr").attr("section");

    //Reset the dummy-div (which adds extra space on the bottom)
    $("#dummyTopAlign").height(0);

    //Scroll back to the top
    $(scrollPanelID).scrollTop(0);

    //And scroll the according fieldset to the top, with a correction of the offset of the scrollpanel it's in
    $(scrollPanelID).scrollTop($("#fls" + section).offset().top - scrollPanelOffset.top - 18);

    var topDiff = $("#fls" + section).offset().top - scrollPanelOffset.top;

    if (topDiff > 0) {
        //Adjust the height of the dummy-div accordingly and scroll these extra pixels
        $("#dummyTopAlign").height(topDiff + 6); //6 for the margin between the blocks
        $(scrollPanelID).scrollTop($(scrollPanelID).scrollTop() + topDiff);
    }

    $("#fls" + section + " input, #fls" + section + " select").first().focus();
}

function handleVoeding(item) {

    var voedingScore = $(pageMainID + "_VoedingOndervoedingAanwezigScore");

    voedingScore.empty();
    switch ($(item).val()) {
        case "MUST":
        case "414648004":
            for (var opt in scoreMUST) {
                voedingScore.append($('<option></option>').val(scoreMUST[opt].Value).html(scoreMUST[opt].Text));
            }
            break;
        case "SNAQ":
            for (var opt in scoreSNAQ) {
                voedingScore.append($('<option></option>').val(scoreSNAQ[opt].Value).html(scoreSNAQ[opt].Text));
            }
            break;
    }

    voedingScore.prepend("<option value=''>&lt;Selecteer een waarde&gt;</option>");

    voedingScore.change();


}

function checkAll(item) {
    //Save the value of the '(un)check all' checkbox
    var ischecked = $(item).is(":checked");

    //Loop all the checkboxes in the navigationpane, (un)check and handle it
    $(formNavigationID).find("input").each(function () {
        $(this).prop('checked', ischecked);
        handleCheck($(this));
    });
}

function addHandlers() {
    //Add a click-event to all checkboxes in the navigation-part
    $(formNavigationID + " input[type=checkbox]").click(function (event) {
        handleCheck(event.target);
        //event.stopPropagation(); //Propagate to the "tr"-click()
    });

    //Add a click-event to all tablerows in the navigation-part
    $(formNavigationID + " tr").click(function (event) {
        handleScroll(event.target);
        event.stopPropagation();
    });

    //Handle the state of all checkboxes in the navigation-part
    $(formNavigationID).find("input").each(function () {
        handleCheck($(this));
    });

    //Initialize the offset of the scrollpanel for further use
    scrollPanelOffset = $(scrollPanelID).offset();
}

function CheckProfessional(checkbox)
{
    if (checkbox.checked == false) {
        var sect = $(checkbox).closest("#tdFreq");
        clearSubsection(sect);
    }
}

function clearSubsection(sect) {
    sect.find('input:text, input:password, input:file, select, textarea').filter(":enabled").val('');
    sect.find('input:radio').click(); //perform a click on the radio to "reset" the panel below it (if present)
    sect.find('input:radio, input:checkbox').filter(":enabled").removeAttr('checked').removeAttr('selected');
    var firstElementWithError;
    var errorInSection = false;
    sect.find(":input:enabled").each(function (index, item) { //Check/validate all the fields which aren't disabled
        if ($(item).closest("table").attr("FormField") != undefined) {
            var elementIsValid = $(item).closest("table").validElement(true);
            if (!elementIsValid && firstElementWithError == null) firstElementWithError = $(item).closest("table");
            errorInSection = !elementIsValid || errorInSection;
        } else {
            var elementIsValid = $(this).validElement(true);
            if (!elementIsValid && firstElementWithError == null) firstElementWithError = $(this);
            errorInSection = !elementIsValid || errorInSection;
        }
    });
}

function clearSection(item) {
    var sect = $(item).closest("fieldset");
    clearSubsection(sect);
}
