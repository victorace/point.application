﻿function FormActionClick(url, newwindow, winname) {

    if (confirmLeave()) {

        try {

            if (!newwindow) {
                window.location = url;
            }
            else {
                if (winname != undefined)
                    window.open(url, winname);
                else
                    window.open(url);
            }

        }

        catch (err) { /* ignore */ }
    }

    return false;

}

function FormActionClickConfirm(url, newwindow, confirmation) {

    if (confirmLeave()) {

        try {

            if (window.confirm(confirmation)) {
                if (!newwindow) {
                    window.location = url;
                }
                else {
                    window.open(url);
                }
            }
        }
        catch (err) { /* ignore */ }
    }

    return false;

}