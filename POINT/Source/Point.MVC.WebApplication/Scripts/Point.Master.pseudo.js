﻿// Implement a "psuedo" console
window.console = window.console || (function () {
    var c = {}; c.log = c.warn = c.debug = c.info = c.error = c.time = c.dir = c.profile = c.clear = c.exception = c.trace = c.assert = function () { };
    return c;
})();

// Implement a "dummy" function, since it's needed by the FormActionScript.js
confirmLeave = function () { return true; }