$.extend($.fn.dataTable.defaults, {
    language: {
        "sProcessing": "Even geduld a.u.b.",
        "sLengthMenu": "_MENU_ resultaten",
        "sZeroRecords": "Geen resultaten om weer te geven",
        "sInfo": "_TOTAL_ resultaten",
        "sInfoEmpty": "",
        "sInfoFiltered": " (gefilterd uit _MAX_ resultaten)",
        "sInfoPostFix": "",
        "sSearch": "Filter: ",
        "sEmptyTable": "",
        "sInfoThousands": ".",
        "thousands": ".",
        "sLoadingRecords": "Even geduld a.u.b. - bezig met laden...",
        "oPaginate": {
            "sFirst": "|&lt;",
            "sLast": "$gt;|",
            "sNext": "&gt;",
            "sPrevious": "&lt;"
        }
    }
    ,

    sDom: "tip",
    bAutoWidth: false,
    "aoColumnDefs": [],
    iDisplayLength: 50
});


function initDataTable(tableToDataTableID, filter) {

    var tableToDataTable = $(tableToDataTableID);

    var dataTableOptions = {
        sScrollY: tableToDataTable.closest('div').innerHeight(),
        aaSorting: [[0, 'asc']],
        bFilter: filter || false,
        sDom: "ftip"
    };

    //Set the columns widths
    dataTableOptions.aoColumnDefs = [];
    $("tr th", tableToDataTable).each(function (ix, el) {
        var visibleWidth = $(this).innerWidth();
        if (visibleWidth < 0) visibleWidth = 0;

        dataTableOptions.aoColumnDefs.push({ sWidth: visibleWidth + "px", aTargets: [ix] });

        if (visibleWidth <= 0) {
            dataTableOptions.aoColumnDefs.push({ bvisible: false, aTargets: [ix] });
        }

        if ($.trim($(this)[0].innerText).length <= 0 || $(this).hasClass("sorting_disabled")) {
            dataTableOptions.aoColumnDefs.push({ bSortalbe: false, aTarget: [ix] });
        }
    });

    tableToDataTable.dataTable(dataTableOptions);
}

$(document).ready(function () {
    $.fn.dataTable.moment("DD-MM-YYYY HH:mm");
    $.fn.dataTable.moment("DD-MM-YYYY");
});