﻿$(function () {

	jQuery.validator.addMethod('regex', function (value, element, param) {
	    if (!value) return false
	    var regex = new RegExp(param.pattern, 'g');
	    return regex.test(value);
	});

	jQuery.validator.unobtrusive.adapters.add('regex', ['pattern'], function (options) {
		var params = {
			pattern: options.params.pattern
		};
		options.rules['regex'] = params;
		if (options.message) {
			options.messages['regex'] = options.message;
		}
	});


}(jQuery));