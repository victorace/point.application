
function ControleBsn($bsnr) {
    $len = $bsnr.length;
    if (bsnRequired) {
        if (isNaN($bsnr) || $len != 9) {
            return 'BSN dient uit 9 cijfers te bestaan.';
        }
    }
    else {
        if (isNaN($bsnr) || ($len > 0 && $len != 9)) {
            return 'BSN niet verplicht, maar dient uit 9 cijfers te bestaan.';
        }
    }

    if (!_bsn_elfproef($bsnr)) {
        return 'BSN is niet correct.';
    }
    return '';
}

function _bsn_elfproef($bsnr) {
    $res = 0;
    $verm = $bsnr.length;

    for ($i = 0; $i < $bsnr.length; $i++, $verm--) {
        if ($verm == 1) {
            $verm = -1;
        }
        $res += $bsnr.substr($i, 1) * $verm;
    }
    return !($res % 11);
}


$(function () {
    $.validator.addMethod("bsn", function (value, element) {
        if (!$(element).is(":focus")) {
            bsnmessage = ControleBsn(value);
            //Showing messages in between causes double validation messages
            return bsnmessage == "";
        }
    }, "BSN is niet correct!");
    if ($.validator.unobtrusive) {
        $.validator.unobtrusive.adapters.addBool("bsn");
    }
});
