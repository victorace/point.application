﻿$(function () {
    var timeout = null;
    var loaderLoaded = false;
    $(document)
        .ajaxStart(function () {
            timeout = setTimeout(function () {
                $('#loader').modal();
                loaderLoaded = true;
            }, 500);
            $("#error").hide();
        })
        .ajaxComplete(function () {
            clearTimeout(timeout);
            if (loaderLoaded) {
                $("#loader").modal("hide");
            }
        })
        .ajaxError(function () {
            $("#loader").modal("hide");
        });

    $("#error").click(function () { $(this).hide(); });
    $("#success").click(function () { $(this).hide(); });
});

var showErrorMessage = function(message, splitter) {

    if (splitter) message = message.split(splitter).join("**");
    if (!message) {
        message = "Fout in de verwerking";
    }
    $("#errormodal").removeClass('warning');
    $("#errormodal").modal('show');
    $("#errormodal .modal-header").html("Er is een fout opgetreden");
    $("#errormodal .modal-body").html(message);
    $("#errormodal").on("hidden.bs.modal",
        function() {
            var id = $("#setFocusOnErrorModalClose").val();
            $("#" + id).focus();
        });
};

var hideErrorMessage = function () {
    $("#error").hide();
};

var errorMessageIsShowing = function () {
    return $('#error').is(':visible');
};

var showSuccessMessage = function (message) {
    
    $("#success #successmessage").html(message);

    $("#success").show();
    window.setTimeout(function () { $("#success").fadeOut() }, 4000); 
};

var showWarningMessage = function (message, title, splitter) {

    if (splitter) {
        message = message.split(splitter).join("**");
    }
    if (!title) {
        title = "LET OP!";
    };

    $("#errormodal").addClass('warning');
    $("#errormodal").modal('show');
    $("#errormodal .modal-header").html(title);
    $("#errormodal .modal-body").html(message);

    $("#errormodal").on("hidden.bs.modal",
        function () {
            var id = $("#setFocusOnErrorModalClose").val();
            $("#" + id).focus();
        });
};

var showLoader = function () {
    $("#loader").modal({ show: true, backdrop: false });
    $("#error").hide();
    $("#success").hide();
    $("#warning").hide();
};

var getQueryStringParameter = function (name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
};