﻿using Point.Business.Logic;
using Point.Models.Enums;
using Point.Infrastructure.Extensions;
using Point.MVC.WebApplication.Attributes;
using Point.MVC.WebApplication.Controllers;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Point.MVC.WebApplication.Areas.Management.Controllers
{
    [FunctionRoles(FunctionRoleLevelID.Global, FunctionRoleTypeID.ManageAll)]
    [ValidateOrigin]
    public class CryptoCertificateController : PointController
    {
        public ActionResult Index()
        {
            var model = CryptoCertificateBL.GetAll(uow);

            return View(model.OrderBy(cc => cc.FriendlyName));
        }

        public ActionResult Upload(string privateKey, string description, HttpPostedFileBase file)
        {
            var errormessage = "";
            var validextensions = ".pfx";

            if (Request.Files.Count > 0)
            {
                var httpPostedFileBase = file;
                var extension = Path.GetExtension(httpPostedFileBase.FileName);
                if (validextensions.Contains(extension, StringComparison.InvariantCultureIgnoreCase))
                {
                    byte[] bytes = new byte[file.ContentLength];
                    file.InputStream.Read(bytes, 0, file.ContentLength);
                    try
                    {
                        var cryptocertificate = CryptoCertificateBL.Save(uow, Path.GetFileName(file.FileName), privateKey, bytes, description);
                    }
                    catch (Exception ex)
                    {
                        errormessage = $"Upload mislukt: {ex.Message.Replace("netwerkwachtwoord", "privatekey/netwerkwachtwoord")}";
                    }
                }
                else
                {
                    errormessage = $"Alleen bestandstypen '{validextensions}' worden ondersteund.";
                }
            }
            else
            {
                errormessage = "Selecteer eerst een bestand.";
            }

            // IE9 JSON Data 'do you want to open or save this file' - http://stackoverflow.com/a/15911008
            var contenttype = Request.AcceptTypes != null && (Request == null || !Request.AcceptTypes.Contains("application/json")) ? "application/json" : "text/plain";

            // NOTE System.Net.HttpStatusCode.OK is always returned in order to keep IE9 from removing our json object due to a Bad Request
            Response.StatusCode = (int)HttpStatusCode.OK;

            if (errormessage != "")
            {
                return Json(new { ErrorMessage = errormessage }, contenttype, JsonRequestBehavior.DenyGet);
            }

            return Json(new { Url = Url.Action("List", ControllerName, new { TransferID }) }, contenttype);


        }
    }
}