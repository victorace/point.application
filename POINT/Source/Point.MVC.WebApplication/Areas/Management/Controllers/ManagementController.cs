﻿using System.Web.Mvc;
using Point.MVC.WebApplication.Attributes;
using Point.MVC.WebApplication.Controllers;

namespace Point.MVC.WebApplication.Areas.Management.Controllers
{
    [ValidateOrigin]
    public class ManagementController : PointController
    {
        // GET: Management
        public ActionResult Index()
        {
            ViewBag.PointUserInfo = PointUserInfo();

            return View();
        }
    }
}