﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Point.Business.Logic;
using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Infrastructure.Extensions;
using Point.MVC.WebApplication.Controllers;
using Point.MVC.WebApplication.Attributes;
using Point.Infrastructure.Attributes;
using System.Net;
using System.IO;

namespace Point.MVC.WebApplication.Areas.Management.Controllers
{
    [FunctionRoles(FunctionRoleLevelID.Region, FunctionRoleTypeID.ManageAll, FunctionRoleTypeID.ManageDocuments)]
    [ValidateOrigin]
    public class TempletController : PointController
    {
        private IEnumerable<TempletViewModel> getViewModelAndSetViewBag(int? regionid)
        {
            var pointuserinfo = PointUserInfo();
            var maxlevel = pointuserinfo.GetFunctionRoleLevelID(FunctionRoleTypeID.ManageDocuments);

            if (regionid == null)
            {
                regionid = pointuserinfo.Region.RegionID;
            }
            
            var regions = maxlevel == FunctionRoleLevelID.Global ? RegionBL.GetAll(uow) : new[] { pointuserinfo.Region };
            var regionoptions = new List<Option>();
            regions.ToList().ForEach(reg => regionoptions.Add(new Option(reg.Name, reg.RegionID.ToString(), regionid == reg.RegionID, AntiFiddleInjection.CreateDigest(reg.RegionID))));
            ViewBag.Regions = regionoptions;
            ViewBag.RegionID = regionid;

            var templets = TempletBL.GetTempletsByRegionID(uow, regionid.ConvertTo<int>());
            var templetViews = TempletViewBL.FromModelList(uow, templets);
            return templetViews;
        }


        public ActionResult Index(int? regionid)
        {
            var viewmodel = getViewModelAndSetViewBag(regionid);
            return View(viewmodel);
        }

        [ValidateAntiFiddleInjection("TempletID")]
        public ActionResult Edit(int templetID = -1, int? regionID = null)
        {
            var templet = TempletBL.GetByID(uow, templetID) ?? new Templet { RegionID = regionID };

            var values = Enum.GetValues(typeof(TempletTypeID)).Cast<TempletTypeID>().Select(at => new SelectListItem { Text = at.GetDescription(), Value = ((int)at).ToString(), Selected = (int)at == templet.TempletTypeID });
            ViewBag.TempletTypeID = values;

            return PartialView(TempletViewBL.FromModel(uow, templet));
        }

        [HttpPost]
        public ActionResult Insert(TempletViewModel viewmodel, HttpPostedFileBase file)
        {
            var errorMessage = "";

            if (ModelState.IsValid)
            {
                if (Request.Files.Count > 0)
                {
                    var httpPostedFileBase = file;
                    var extension = Path.GetExtension(httpPostedFileBase.FileName);
                    if (".pdf .doc .docx .xps .jpg .jpeg .png .xls .xlsx .msg .eml".Contains(extension, StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (httpPostedFileBase.ContentLength > 0 && httpPostedFileBase.ContentLength <= (10 * 1024 * 1024))
                        {
                            try
                            {
                                var employeeinfo = PointUserInfo();

                                viewmodel.EmployeeID = employeeinfo.Employee.EmployeeID;
                                if (viewmodel.RegionID == null)
                                    viewmodel.RegionID = employeeinfo.Region.RegionID;

                                var model = TempletViewBL.ToModel(viewmodel, uow);

                                TempletBL.FillFileData(model, httpPostedFileBase);
                                TempletBL.Insert(uow, model);
                                uow.Save();
                            }
                            catch { errorMessage = "Upload mislukt."; }
                        }
                        else
                            errorMessage = "Het gekozen bestand is te groot, maximaal 10 MB.";
                    }
                    else
                        errorMessage = "Het gekozen bestandstype wordt niet ondersteund.";
                }
                else
                    errorMessage = "Selecteer eerst een bestand.";
            }
            else
                errorMessage = "Gegevens zijn niet juist: " + string.Join("; ", ModelState.Values.SelectMany(m => m.Errors).Select(m => m.ErrorMessage));


            // IE9 JSON Data 'do you want to open or save this file' - http://stackoverflow.com/a/15911008
            var contenttype = Request.AcceptTypes != null && (Request == null || !Request.AcceptTypes.Contains("application/json")) ? "application/json" : "text/plain";

            // NOTE System.Net.HttpStatusCode.OK is always returned in order to keep IE9 from removing our json object due to a Bad Request
            Response.StatusCode = (int)HttpStatusCode.OK;

            if (errorMessage == "")
                return Json(new { Url = Url.Action("List", ControllerName, new { TransferID }) }, contenttype);
            return Json(new { ErrorMessage = errorMessage }, contenttype, JsonRequestBehavior.DenyGet);
        }

        [ValidateAntiFiddleInjection("TempletID")]
        [HttpPost]
        public ActionResult Update(TempletViewModel viewmodel)
        {
            var templet = TempletBL.GetByID(uow, viewmodel.TempletID);
            // Not a lot to update/save, copying the values manual. Merge could be used otherwise.            
            templet.TempletTypeID = (int)viewmodel.TempletTypeID;
            templet.SortOrder = viewmodel.SortOrder.HasValue ? viewmodel.SortOrder : TempletBL.GetSortOrder(uow, viewmodel.RegionID ?? 0);
            uow.Save();

            // IE9 JSON Data 'do you want to open or save this file' - http://stackoverflow.com/a/15911008
            var contenttype = Request.AcceptTypes != null && Request.AcceptTypes.Contains("application/json") ? "application/json" : "text/plain";

            // NOTE System.Net.HttpStatusCode.OK is always returned in order to keep IE9 from removing our json object due to a Bad Request
            Response.StatusCode = (int)HttpStatusCode.OK;

            return ErrorMessage == ""
                ? Json(new { Url = Url.Action("Index", ControllerName, new { TransferID }, Request.Url?.Scheme) }, contenttype)
                : Json(new { ErrorMessage }, contenttype, JsonRequestBehavior.DenyGet);
        }

        [ValidateAntiFiddleInjection("TempletID")]
        public ActionResult Delete(int templetID)
        {
            var errorMessage = "";
            TempletBL.DeleteByID(uow, templetID);

            Response.StatusCode = (int)(errorMessage == "" ? HttpStatusCode.OK : HttpStatusCode.BadRequest);
            if (errorMessage == "")
            {
                return RedirectToAction("Index");
            }
            return Json(new { ErrorMessage = errorMessage }, JsonRequestBehavior.AllowGet);
        }
    }
}