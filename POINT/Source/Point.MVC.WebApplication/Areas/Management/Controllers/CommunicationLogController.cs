﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Point.Business.Logic;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Infrastructure.Extensions;
using Point.MVC.WebApplication.Controllers;
using Point.MVC.WebApplication.Attributes;
using Point.Business.Helper;

namespace Point.MVC.WebApplication.Areas.Management.Controllers
{

    [FunctionRoles(FunctionRoleLevelID.Location, FunctionRoleTypeID.ManageAll, FunctionRoleTypeID.ManageLogs)]
    [ValidateOrigin]
    public class CommunicationLogController : PointController
    {
        private void setDropdownViewBag(CommunicationLogType? logType = null)
        {
            List<SelectListItem> logtypes = new List<SelectListItem>
            {
                new SelectListItem() { Value = "", Text = "Alle", Selected = true },
                new SelectListItem() { Value = CommunicationLogType.ZorgmailSendPDF.ToString(), Text = CommunicationLogType.ZorgmailSendPDF.GetDescription() },
                new SelectListItem() { Value = CommunicationLogType.ZorgmailMedvri.ToString(), Text = CommunicationLogType.ZorgmailMedvri.GetDescription() },
                new SelectListItem() { Value = CommunicationLogType.ToRequestDesiredOrganization.ToString(), Text = CommunicationLogType.ToRequestDesiredOrganization.GetDescription() },
                new SelectListItem() { Value = CommunicationLogType.ToCIZ.ToString(), Text = CommunicationLogType.ToCIZ.GetDescription() },
                new SelectListItem() { Value = CommunicationLogType.NotifyVO.ToString(), Text = CommunicationLogType.NotifyVO.GetDescription() },
                new SelectListItem() { Value = CommunicationLogType.Canceled.ToString(), Text = CommunicationLogType.Canceled.GetDescription() },
                new SelectListItem() { Value = CommunicationLogType.NedapVO.ToString(), Text = CommunicationLogType.NedapVO.GetDescription() }
            };

            if(logType.HasValue)
            {
                logtypes.ForEach(lt => lt.Selected = (lt.Value.ToString() == logType.Value.ToString()));
            }

            ViewBag.LogTypes = logtypes;
        }

        private IEnumerable<CommunicationLogViewModel> getEditViewModelAndSetViewBag(int? transferID, DateTime? dateFrom = null, DateTime? dateTo = null, CommunicationLogType? communicationLogType = null)
        {
            var accesslevel = FunctionRoleHelper.GetAccessLevel(uow, FunctionRoleTypeID.ManageLogs, PointUserInfo());
            var communicationlog = CommunicationLogBL.GetForManagementSearch(uow, accesslevel, dateFrom, dateTo, transferID, communicationLogType).Take(500);
            var communicationlogviewmodels = CommunicationLogViewBL.FromModelList(uow, communicationlog);

            return communicationlogviewmodels;
        }
        

        public ActionResult Search()
        {
            var model = Enumerable.Empty<CommunicationLogViewModel>();
            setDropdownViewBag();

            return View(model);
        }

        [HttpPost]
        public ActionResult Search(int? transferID, DateTime? dateFrom, DateTime? dateTo, CommunicationLogType? communicationLogType = null)
        {
            var model = getEditViewModelAndSetViewBag(transferID, dateFrom, dateTo, communicationLogType);
            setDropdownViewBag(communicationLogType);

            return View(model);
        }
        
    }
}