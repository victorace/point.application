﻿using DevTrends.MvcDonutCaching;
using Point.Business.Logic;
using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Infrastructure.Exceptions;
using Point.MVC.WebApplication.Controllers;
using System.Web.Mvc;
using Point.MVC.WebApplication.Attributes;

namespace Point.MVC.WebApplication.Areas.Management.Controllers
{
    [DonutOutputCache(Duration = 0)]
    [ValidateOrigin]
    public class ServiceAreaController : PointController
    {
        public PartialViewResult ServiceAreas(int organizationid = 0)
        {
            if (organizationid == 0)
                throw new PointJustLogException("U kunt geen gebieden instellen. Sla eerst de organisatie (tussentijds) op.");

            var model = ServiceAreaBL.GetByOrganizationID(uow, organizationid);

            return PartialView(model);
        }

        public JsonResult UpdateDepartmentServiceArea(int departmentid, int serviceareaid = 0, bool selected = false)
        {
            var pointuserinfo = PointUserInfo();
            var logentry = LogEntryBL.Create(FlowFormType.AdminDepartment, pointuserinfo);

            var department = DepartmentBL.GetByDepartmentID(uow, departmentid);
            LoggingBL.FillLoggable(uow, department, logentry, forceupdate: true);

            ServiceAreaBL.UpdateDepartmentServiceArea(uow, departmentid, serviceareaid, selected, logentry);

            uow.Save(logentry);

            OrganizationUpdateBL.CalculateForDepartmentAsync(departmentid);

            return Json(new { Updated = "true" }, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult DepartmentServiceAreas(int departmentid = 0)
        {
            if (departmentid == 0)
                throw new PointJustLogException("U kunt geen gebieden instellen. Sla eerst de afdeling (tussentijds) op.");

            var department = DepartmentBL.GetByDepartmentID(uow, departmentid);
            var organization = department.Location?.Organization;
            if (organization == null)
                throw new PointJustLogException("Deze afdeling heeft geen gekoppelde locatie en/of organisatie.");

            var viewmodel = DepartmentServiceAreaViewModel.FromModel(department);

            return PartialView(viewmodel);
        }

        public JsonResult AddServiceArea(ServiceAreaViewModel viewmodel)
        {
            var pointuserinfo = PointUserInfo();
            var logentry = LogEntryBL.Create(FlowFormType.AdminOrganization, pointuserinfo);

            var model = ServiceAreaBL.Add(uow, viewmodel, logentry);
            var organization = OrganizationBL.GetByOrganizationID(uow, viewmodel.OrganizationID);
            LoggingBL.FillLoggable(uow, organization, logentry, forceupdate: true);

            uow.Save(logentry);

            viewmodel = ServiceAreaViewModel.FromModel(model);

            OrganizationUpdateBL.CalculateForOrganizationAsync(viewmodel.OrganizationID);

            return Json(viewmodel, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteServiceAreaPostalCode(int serviceAreaPostalCodeID)
        {
            var serviceareapostalcode = ServiceAreaBL.GetServiceAreaPostalCodeByID(uow, serviceAreaPostalCodeID);
            if(serviceareapostalcode!= null)
            {
                var pointuserinfo = PointUserInfo();
                LogEntry logentry = null;
                
                if(serviceareapostalcode.DepartmentID.HasValue)
                {
                    logentry = LogEntryBL.Create(FlowFormType.AdminDepartment, pointuserinfo);
                    var department = DepartmentBL.GetByDepartmentID(uow, serviceareapostalcode.DepartmentID.Value);
                    LoggingBL.FillLoggable(uow, department, logentry, forceupdate: true);
                }
                else if((serviceareapostalcode.ServiceArea?.OrganizationID).HasValue)
                {
                    logentry = LogEntryBL.Create(FlowFormType.AdminOrganization, pointuserinfo);
                    var organization = OrganizationBL.GetByOrganizationID(uow, serviceareapostalcode.ServiceArea.OrganizationID.Value);
                    LoggingBL.FillLoggable(uow, organization, logentry, forceupdate: true);
                }

                int? departmentid = serviceareapostalcode.DepartmentID;
                int? organizationid = serviceareapostalcode.ServiceArea?.OrganizationID;

                if (logentry == null)
                {
                    logentry = LogEntryBL.Create(FlowFormType.NoScreen, pointuserinfo);
                }

                ServiceAreaBL.DeletePostalCode(uow, serviceAreaPostalCodeID, logentry);

                uow.Save(logentry);

                if (departmentid.HasValue)
                {
                    OrganizationUpdateBL.CalculateForDepartmentAsync(departmentid.Value);
                }
                if(organizationid.HasValue)
                {
                    OrganizationUpdateBL.CalculateForOrganizationAsync(organizationid.Value);
                }
            }

            return Json(new { Deleted = "true" }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteServiceArea(int serviceareaid)
        {
            var servicearea = ServiceAreaBL.GetByServiceAreaID(uow, serviceareaid);
            int? organizationid = servicearea?.OrganizationID;

            if (servicearea != null)
            {
                var logentry = LogEntryBL.Create(FlowFormType.AdminOrganization, PointUserInfo());
                ServiceAreaBL.DeleteServiceArea(uow, serviceareaid, logentry);

                if (organizationid.HasValue)
                {
                    var organization = OrganizationBL.GetByOrganizationID(uow, organizationid.Value);
                    LoggingBL.FillLoggable(uow, organization, logentry, forceupdate: true);
                }

                uow.Save(logentry);

                if(organizationid.HasValue)
                {
                    OrganizationUpdateBL.CalculateForOrganizationAsync(organizationid.Value);
                }
            }

            return Json(new { Deleted = "true" }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult EditServiceAreaPostalCode(int serviceAreaPostalCodeID)
        {
            var serviceareapostalcode = ServiceAreaBL.GetServiceAreaPostalCodeByID(uow, serviceAreaPostalCodeID);

            return Json(
                new
                {
                    serviceareapostalcode.DepartmentID,
                    serviceareapostalcode.ServiceArea?.OrganizationID,
                    serviceareapostalcode.ServiceAreaPostalCodeID,
                    serviceareapostalcode.PostalCode.StartPostalCode,
                    serviceareapostalcode.PostalCode.EndPostalCode,
                    serviceareapostalcode.PostalCode.Description
                }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult UpdateServiceAreaPostalCode(ServiceAreaPostalCodeViewModel viewmodel)
        {
            if (string.IsNullOrEmpty(viewmodel.StartPostalCode) || string.IsNullOrEmpty(viewmodel.EndPostalCode))
                throw new PointJustLogException("Vul het postcodeberiek in");

            var pointuserinfo = PointUserInfo();

            if(viewmodel.DepartmentID.HasValue)
            {
                var logentry = LogEntryBL.Create(FlowFormType.AdminDepartment, pointuserinfo);
                ServiceAreaBL.UpdatePostalCode(uow, viewmodel, logentry);

                var department = DepartmentBL.GetByDepartmentID(uow, viewmodel.DepartmentID.Value);
                LoggingBL.FillLoggable(uow, department, logentry, forceupdate: true);

                uow.Save(logentry);

                OrganizationUpdateBL.CalculateForDepartmentAsync(viewmodel.DepartmentID.Value);
            }

            if (viewmodel.OrganizationID.HasValue)
            {
                var logentry = LogEntryBL.Create(FlowFormType.AdminOrganization, pointuserinfo);
                ServiceAreaBL.UpdatePostalCode(uow, viewmodel, logentry);

                var organization = OrganizationBL.GetByOrganizationID(uow, viewmodel.OrganizationID.Value);
                LoggingBL.FillLoggable(uow, organization, logentry, forceupdate: true);

                uow.Save(logentry);

                OrganizationUpdateBL.CalculateForOrganizationAsync(viewmodel.OrganizationID.Value);
            }

            return Json(viewmodel, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AddServiceAreaPostalCode(ServiceAreaPostalCodeViewModel viewmodel)
        {
            if (string.IsNullOrEmpty(viewmodel.StartPostalCode) || string.IsNullOrEmpty(viewmodel.EndPostalCode))
            {
                throw new PointJustLogException("Vul het postcodeberiek in");
            }

            var pointuserinfo = PointUserInfo();
            var logentry = LogEntryBL.Create(FlowFormType.AdminOrganization, pointuserinfo);

            var model = ServiceAreaBL.AddPostalCode(uow, viewmodel, logentry);

            if(viewmodel.DepartmentID.HasValue)
            {
                var department = DepartmentBL.GetByDepartmentID(uow, viewmodel.DepartmentID.Value);
                LoggingBL.FillLoggable(uow, department, logentry, forceupdate: true);

                OrganizationUpdateBL.CalculateForDepartmentAsync(viewmodel.DepartmentID.Value);
            }
            else if(viewmodel.ServiceAreaID.HasValue)
            {
                var servicearea = ServiceAreaBL.GetByServiceAreaID(uow, viewmodel.ServiceAreaID.Value);
                if(servicearea?.OrganizationID != null)
                {
                    var organization = OrganizationBL.GetByOrganizationID(uow, servicearea.OrganizationID.Value);
                    LoggingBL.FillLoggable(uow, organization, logentry, forceupdate: true);

                    OrganizationUpdateBL.CalculateForOrganizationAsync(servicearea.OrganizationID.Value);
                }
            }

            uow.Save(logentry);

            viewmodel = ServiceAreaPostalCodeViewModel.FromModel(model);

            return Json(viewmodel, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult Details(int serviceareaid)
        {
            var servicearea = ServiceAreaBL.GetByServiceAreaID(uow, serviceareaid);
            var viewmodel = ServiceAreaViewModel.FromModel(servicearea);

            return PartialView(viewmodel);
        }
    }
}