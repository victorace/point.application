﻿using DevTrends.MvcDonutCaching;
using Point.Business.Logic;
using Point.Database.Models.ViewModels;
using Point.Models.Enums;
using Point.MVC.WebApplication.Attributes;
using Point.MVC.WebApplication.Controllers;
using System.Web.Mvc;

namespace Point.MVC.WebApplication.Areas.Management.Controllers
{
    [DonutOutputCache(Duration = 0)]
    [FunctionRoles(FunctionRoleLevelID.Global, FunctionRoleTypeID.ManageAll)]
    [ValidateOrigin]
    public class FormAdminController : PointController
    {
        public ActionResult Index()
        {
            var viewmodel = FormAdminViewModelBL.GetMain();
            return View(viewmodel);
        }

        
        public ActionResult FormDetails(int formTypeID)
        {
            var viewmodel = FormAdminViewModelBL.GetFormDetails(formTypeID);
            return View(viewmodel);
        }

        public ActionResult Edit(int formTypeID, int? phaseDefinitionID, int? regionID, int? organizationID)
        {
            var viewmodel = FormAdminViewModelBL.GetFieldAdminEdit(formTypeID, phaseDefinitionID, regionID, organizationID);
            return View(viewmodel);
        }

        public PartialViewResult EditAttributes(int flowFieldAttributeExceptionID)
        {
            var viewmodel = FormAdminViewModelBL.GetAttributeEdit(flowFieldAttributeExceptionID);
            return PartialView(viewmodel);
        }

        [HttpPost]
        public JsonResult UpdateAttributes(FormAdminEditViewModel.Item viewmodel)
        {
            FlowFieldAttributeExceptionBL.UpdateAttribute(viewmodel.FlowFieldAttributeExceptionID, viewmodel.ReadOnly, viewmodel.Visible, viewmodel.Required, viewmodel.SignalOnChange, viewmodel.RequiredByFlowFieldID, viewmodel.RequiredByFlowFieldValue);
            return Json(new { success = true });
        }

        public ActionResult AddFormDetails(int formTypeID)
        {
            var viewmodel = FormAdminViewModelBL.GetAddFormDetails(formTypeID);
            return View(viewmodel);
        }

        [HttpPost]
        public ActionResult SaveFormDetails(AddFormDetailsViewModel viewModel)
        {
            FlowFieldAttributeExceptionBL.AddAttributes(viewModel.FlowFieldAttributeID, viewModel.PhaseDefinitionID, viewModel.RegionID, viewModel.OrganizationID);
            return Json(new { success = true });
        }

        [HttpPost]
        public ActionResult DeleteFormDetails(int formTypeID, int? phaseDefinitionID, int? regionID, int? organizationID)
        {
            FlowFieldAttributeExceptionBL.DeleteAttributes(formTypeID, phaseDefinitionID, regionID, organizationID);
            return Json(new { success = true });
        }

        [HttpPost]
        public ActionResult DeleteAttribute(int flowFieldAttributeExceptionID)
        {
            FlowFieldAttributeExceptionBL.DeleteAttribute(flowFieldAttributeExceptionID);
            return Json(new { success = true });
        }

        public PartialViewResult AddAttribute(int formTypeID, int? phaseDefinitionID, int? regionID, int? organizationID)
        {
            var viewmodel = FormAdminViewModelBL.GetAttributeAdd(formTypeID, phaseDefinitionID, regionID, organizationID);
            return PartialView(viewmodel);
        }

        [HttpPost]
        public ActionResult AddAttribute(AttributeAddViewModel viewModel)
        {
            FlowFieldAttributeExceptionBL.AddAttribute(
                viewModel.FlowFieldAttributeID,
                viewModel.PhaseDefinitionID,
                viewModel.RegionID,
                viewModel.OrganizationID,
                viewModel.ReadOnly,
                viewModel.Required,
                viewModel.Visible,
                viewModel.SignalOnChange,
                viewModel.RequiredByFlowFieldID,
                viewModel.RequiredByFlowFieldValue);

            return Json(new { success = true });
        }
    }
}