﻿using Point.Business.Logic;
using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Infrastructure.Attributes;
using Point.MVC.WebApplication.Attributes;
using Point.MVC.WebApplication.Controllers;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Point.Infrastructure.Helpers;

namespace Point.MVC.WebApplication.Areas.Management.Controllers
{
    [ValidateOrigin]
    public class ServicePageController : PointController
    {
        private HtmlText getViewModelAndSetViewBag(int? regionID = null)
        {
            var maxlevel = PointUserInfo().GetFunctionRoleLevelID(FunctionRoleTypeID.ManageServicePage);

            var regions = maxlevel == FunctionRoleLevelID.Global
                            ? RegionBL.GetAll(uow).OrderBy(reg => reg.Name).ToList()
                            : new List<Region>() { PointUserInfo().Region };

            int selectedRegionID = regionID ?? PointUserInfo().Region.RegionID;

            var regionoptions = new List<Option>();
            regions.ForEach(reg => regionoptions.Add(new Option(reg.Name, reg.RegionID.ToString(), selectedRegionID == reg.RegionID, AntiFiddleInjection.CreateDigest(reg.RegionID))));
            ViewBag.Regions = regionoptions;

            var model = HtmlTextBL.GetHtmlTextByRegion(uow, selectedRegionID);
            if (model.RegionID <= 0)
            {
                model.RegionID = selectedRegionID;
            }

            return model;
        }

        public ActionResult Index()
        {
            int regionID = PointUserInfo().Region.RegionID;

            var model = HtmlTextBL.GetHtmlTextByRegion(uow, regionID, true);

            return View(model);
        }

        [FunctionRoles(FunctionRoleLevelID.Region, FunctionRoleTypeID.ManageAll, FunctionRoleTypeID.ManageServicePage)]
        public ActionResult Edit(int? regionID = null)
        {
            var model = getViewModelAndSetViewBag(regionID);
            return View(model);
        }

        [ValidateAntiFiddleInjection("regionID")]
        [HttpPost]
        [FunctionRoles(FunctionRoleLevelID.Region, FunctionRoleTypeID.ManageAll, FunctionRoleTypeID.ManageServicePage)]
        public ActionResult Save(int regionID, string htmlText1)
        {
            HtmlTextBL.Upsert(uow, regionID, htmlText1, PointUserInfo());
            var model = getViewModelAndSetViewBag(regionID);
            return View("Edit", model);
        }

        public ActionResult Questions()
        {
            

            var userInfo = PointUserInfo();
            //because its a one way data transfer I used viewbag.
            //var admins = EmployeeBL.GetDepartmentAdmins(uow, userInfo);
            ViewBag.DepartmentAdmins = GetFilteredAdmins(EmployeeBL.GetDepartmentAdmins(uow, userInfo));
            ViewBag.LocationAdmins = GetFilteredAdmins(EmployeeBL.GetLocationAdmins(uow, userInfo));
            ViewBag.OrganizationAdmins = GetFilteredAdmins(EmployeeBL.GetOrganizationAdmins(uow, userInfo));
            ViewBag.RegionalAdmins = GetFilteredAdmins(EmployeeBL.GetRegionalAdmins(uow, userInfo));
            return View();
        }

        private List<AdminInfoViewModel> GetFilteredAdmins(IQueryable<Employee> admin)
        {
            string[] exclude = new[] { "@linkassist.", "@techxxc.", "@techxx." };
            return AdminInfoViewModel.FromModelList(EmployeeBL.ExcludeAdminsWithEmailsContaining(admin, exclude));
        }

        public FileResult Handleiding()
        {            
            var filePath = ConfigHelper.GetAppSettingByName<string>("UserManualFilePath", "");
            return File(filePath, "application/pdf");
        }

        public ActionResult Privacyverklaring()
        {
            return View();
        }
    }
}