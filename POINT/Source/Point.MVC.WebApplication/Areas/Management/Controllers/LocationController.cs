﻿using DevTrends.MvcDonutCaching;
using Point.Business.Logic;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Infrastructure.Attributes;
using Point.MVC.WebApplication.Attributes;
using Point.MVC.WebApplication.Controllers;
using System.Web.Mvc;
using System.Web.Routing;
using Point.MVC.WebApplication.Helpers;

namespace Point.MVC.WebApplication.Areas.Management.Controllers
{
    [DonutOutputCache(Duration = 0)]
    [ValidateOrigin]
    public class LocationController : PointController
    {
        [HttpGet]
        [FunctionRoles(FunctionRoleLevelID.Location, FunctionRoleTypeID.ManageAll, FunctionRoleTypeID.ManageOrganization)]
        public ActionResult Create(int? organizationID = null)
        {
            return PartialView("Edit", LocationViewModelHelper.GetCreateViewModel(uow, GetGlobalProperties(), organizationID));
        }

        [HttpGet]
        [FunctionRoles(FunctionRoleLevelID.Location, FunctionRoleTypeID.ManageAll, FunctionRoleTypeID.ManageOrganization)]
        [ValidateAntiFiddleInjection("LocationID")]
        public ActionResult Edit(int locationid)
        {           
            return PartialView(LocationViewModelHelper.GetEditViewModel(uow, GetGlobalProperties(), locationid));
        }

        [HttpPost]
        [FunctionRoles(FunctionRoleLevelID.Location, FunctionRoleTypeID.ManageAll, FunctionRoleTypeID.ManageOrganization)]
        [ValidateAntiForgeryToken]
        [ValidateAntiFiddleInjection("LocationID")]
        public ActionResult Save(LocationViewModel viewmodel)
        {
            var pointUserInfo = PointUserInfo();

            LocationViewBL.ValidateViewModel(uow, pointUserInfo, viewmodel);

            var success = false;
            var locationId = 0;
            if (ModelState.IsValid)
            {
                var logEntry = LogEntryBL.Create(FlowFormType.AdminOrganization, pointUserInfo);
                var location = LocationBL.Save(uow, viewmodel, logEntry, pointUserInfo);
                locationId = location.LocationID;

                OrganizationUpdateBL.CalculateForLocationAsync(location.LocationID);
                //OrganizationUpdateBL.CapManForLocationAsync(location.LocationID);

                success = true;
            }

            return Json(new { Success = success, LocationID = locationId, Digest = AntiFiddleInjection.CreateDigest(locationId), Url = Url.Action("Index", "Organization", new RouteValueDictionary() { { "Area", "Management" } }) });
        }

        [FunctionRoles(FunctionRoleLevelID.Location, FunctionRoleTypeID.ManageAll, FunctionRoleTypeID.ManageOrganization)]
        public PartialViewResult NewAutoCreateSet(int locationId)
        {
            return PartialView("_AutoCreateSet", LocationViewModelHelper.GetNewAutoCreateSetViewModel(uow, GetGlobalProperties(), locationId));
        }

        [FunctionRoles(FunctionRoleLevelID.Location, FunctionRoleTypeID.ManageAll, FunctionRoleTypeID.ManageOrganization)]
        public PartialViewResult EditAutoCreateSet(int autocreatesetid)
        {
            return PartialView("_AutoCreateSetEdit", LocationViewModelHelper.GetEditAutoCreateSetViewModel(uow, GetGlobalProperties(), autocreatesetid));
        }

        [HttpPost]
        [FunctionRoles(FunctionRoleLevelID.Location, FunctionRoleTypeID.ManageAll, FunctionRoleTypeID.ManageOrganization)]
        public JsonResult EditAutoCreateSet(AutoCreateSetViewModel viewmodel)
        {
            var pointUserInfo = PointUserInfo();
            var logEntry = LogEntryBL.Create(FlowFormType.AdminLocation, pointUserInfo);

            AutoCreateSetBL.Save(uow, viewmodel, logEntry, pointUserInfo);

            return Json(new { Success = true }, JsonRequestBehavior.AllowGet);
        }

        [FunctionRoles(FunctionRoleLevelID.Location, FunctionRoleTypeID.ManageAll, FunctionRoleTypeID.ManageOrganization)]
        public JsonResult DeleteAutoCreateSet(int autocreatesetid)
        {
            var logEntry = LogEntryBL.Create(FlowFormType.AdminLocation, PointUserInfo());
            var model = AutoCreateSetBL.GetByID(uow, autocreatesetid);

            LoggingBL.FillLoggable(uow, model.Location, logEntry, forceupdate: true);

            uow.AutoCreateSetDepartmentRepository.Delete(it => it.AutoCreateSetID == autocreatesetid);
            uow.AutoCreateSetRepository.Delete(model);
            
            uow.Save(logEntry);

            return Json(new { Success = true }, JsonRequestBehavior.AllowGet);
        }
    }
}