﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Mvc;
using DevTrends.MvcDonutCaching;
using Point.Business.Logic;
using Point.Database.Models.ViewModels;
using Point.Infrastructure.Attributes;
using Point.Models.Enums;
using Point.MVC.WebApplication.Attributes;
using Point.MVC.WebApplication.Controllers;
using Point.MVC.WebApplication.Helpers;

namespace Point.MVC.WebApplication.Areas.Management.Controllers
{
    [DonutOutputCache(Duration = 0)]
    [ValidateOrigin]
    public class DoctorsController : PointController
    {
        [FunctionRoles(FunctionRoleTypeID.ManageAll, FunctionRoleTypeID.ManageDoctors)]
        public ActionResult Index(int? regionID = null, int? organizationID = null)
        {
            return View(DoctorsViewModelHelper.GetIndexViewModel(this, uow, regionID, organizationID));
        }

        [ValidateAntiFiddleInjection("DoctorID")]
        [FunctionRoles(FunctionRoleTypeID.ManageAll, FunctionRoleTypeID.ManageDoctors)]
        public ActionResult Delete(int doctorID, bool confirm = false)
        {
            var model = DoctorBL.GetByID(uow, doctorID);
            if (confirm == false)
            {
                return PartialView(model);
            }

            DoctorBL.DeleteByID(uow, model.DoctorID);
            return RedirectToAction("Index", "Doctors", new { model.Organization.RegionID, model.OrganizationID });
        }

        [ValidateAntiFiddleInjection("OrganizationID")]
        [FunctionRoles(FunctionRoleTypeID.ManageAll, FunctionRoleTypeID.ManageDoctors)]
        public ActionResult Create(int organizationID)
        {
            return PartialView("Edit", DoctorsViewModelHelper.GetCreateViewModel(this, organizationID));
        }

        [ValidateAntiFiddleInjection("DoctorID")]
        [FunctionRoles(FunctionRoleTypeID.ManageAll, FunctionRoleTypeID.ManageDoctors)]
        public ActionResult Edit(int doctorID)
        {
            return PartialView(DoctorsViewModelHelper.GetEditViewModel(this, doctorID));
        }

        [ValidateAntiFiddleInjection("DoctorID")]
        [FunctionRoles(FunctionRoleTypeID.ManageAll, FunctionRoleTypeID.ManageDoctors)]
        public ActionResult Save(DoctorViewModel viewmodel)
        {
            DoctorBL.Save(uow, viewmodel);
            var organization = OrganizationBL.GetByOrganizationID(uow, viewmodel.OrganizationID);

            return RedirectToAction("Index", "Doctors", new { organization.RegionID, viewmodel.OrganizationID });
        }


        [System.Web.Mvc.HttpPost]
        [FunctionRoles(FunctionRoleTypeID.ManageAll, FunctionRoleTypeID.ManageDoctors)]
        public JsonResult ImportDoctors([FromBody] DoctorImportListViewModel doctorList)
        {
            var count = 0;
            var errorMsg = "";

            if (doctorList != null && doctorList.Doctors.Any())
            {
                foreach (var doctor in doctorList.Doctors)
                {
                    doctor.DoctorID = uow.DoctorRepository.Get(doc => doc.OrganizationID == doctor.OrganizationID && doc.AGB == doctor.AGB).Select(d => d.DoctorID).FirstOrDefault();

                    try
                    {
                        DoctorBL.Save(uow, doctor);
                        count++;
                    }
                    catch (Exception ex)
                    {
                        errorMsg = ex.Message;
                    }
                }
            }

            return Json(new
            {
                success = string.IsNullOrEmpty(errorMsg),
                successMessage = $"{count} arts(en) succesvol geïmporteerd",
                errorMessage = errorMsg
            });
        }

        [System.Web.Mvc.HttpPost]
        [FunctionRoles(FunctionRoleTypeID.ManageAll, FunctionRoleTypeID.ManageDoctors)]
        public JsonResult ReadDoctorsFromCsv(int organizationID)
        {
            var doctorListViewModel = DoctorsViewModelHelper.GetDoctorImportListViewModel(this, organizationID);
            var importListHtml = this.RenderPartialViewToString("ImportList", doctorListViewModel);

            return Json(new
            {
                htmlReplaces = new Dictionary<string, string>
                {
                    {"#panelDoctorsImport", importListHtml }
                },
                errorMessage = doctorListViewModel.ErrorMessage
            });
        }

        [System.Web.Mvc.HttpPost]
        [FunctionRoles(FunctionRoleTypeID.ManageAll, FunctionRoleTypeID.ManageDoctors)]
        public JsonResult PageUpdate(int? regionID = null, int? organizationID = null)
        {
            var doctorsViewModel = DoctorsViewModelHelper.GetIndexViewModel(this, uow, regionID, organizationID);
            string htmlID;
            string view;

            if (!organizationID.HasValue)
            {
                htmlID = "#doctorIndexPlaceHolder";
                view = "Index";
            }
            else
            {
                htmlID = "#doctorsListPlaceholder";
                view = "List";
            }
            return Json(new
            {
                htmlReplaces = new Dictionary<string, string>
                {
                    {htmlID, this.RenderPartialViewToString(view, doctorsViewModel) }
                }
            });
        }
    }
}