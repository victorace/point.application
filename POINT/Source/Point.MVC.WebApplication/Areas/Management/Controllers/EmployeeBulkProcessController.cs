﻿using Point.Business.Helper;
using Point.Business.Logic;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.MVC.WebApplication.Attributes;
using Point.MVC.WebApplication.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Point.MVC.WebApplication.Areas.Management.Controllers
{
    [ValidateOrigin]
    [FunctionRoles(FunctionRoleTypeID.ManageAll, FunctionRoleTypeID.ManageEmployees)]
    public class EmployeeBulkProcessController : PointController
    {
        public ActionResult Index(string howtosearch, DateTime? lastlogindate)
        {
            return View(GetViewModel(howtosearch, lastlogindate));
        }
  
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditUsers(BulkProcessViewModel viewModel)
        {
            var pointuserinfo = PointUserInfo();
            var logentry = LogEntryBL.Create(FlowFormType.AdminBulkEmployee, pointuserinfo);
            EmployeeBulkProcessViewBL.ProcessBulkRequest(uow, viewModel, logentry);
            return RedirectToAction("Index", "EmployeeBulkProcess", new { howtosearch = viewModel.ProcessType, lastlogindate = viewModel.LastLoginDate });
        }

        private BulkProcessViewModel GetViewModel(string howToSearch, DateTime? lastLoginDate)
        {
            var pointuserinfo = PointUserInfo();

            var accesslevel = FunctionRoleHelper.GetAccessLevel(uow, FunctionRoleTypeID.ManageEmployees, pointuserinfo);

            int? regionid = null;
            int[] organizationids = null;
            int[] locationids = null;
            int[] departmentids = null;

            switch (accesslevel.MaxLevel)
            {
                case FunctionRoleLevelID.Department:
                    departmentids = pointuserinfo.EmployeeDepartmentIDs.ToArray();
                    break;
                case FunctionRoleLevelID.Location:
                    locationids = accesslevel.LocationIDs.ToArray();
                    break;
                case FunctionRoleLevelID.Organization:
                    organizationids = accesslevel.OrganizationIDs.ToArray();
                    break;
                case FunctionRoleLevelID.Region:
                    regionid = pointuserinfo.Region.RegionID;
                    break;
            }

            //get blocked users if you want to deblock them:
            ViewBag.HowToSearch = howToSearch;
            ViewBag.LastLoginDate = lastLoginDate;          

            if (string.IsNullOrEmpty(howToSearch))
            {
                return new BulkProcessViewModel() {
                    ProcessType = howToSearch,
                    LastLoginDate = lastLoginDate,
                    Employees = new List<EmployeeBulkProcessViewModel>()
                };
            }

            var blockEmployee = (howToSearch != "DeBlockUsers" ? true : false);
            var employees = EmployeeBL.SearchBulkEmployee(uow, regionid, organizationids, locationids, departmentids, blockEmployee, lastLoginDate);
            var result = new BulkProcessViewModel
            {
                ProcessType = howToSearch,
                LastLoginDate = lastLoginDate,
                Employees = EmployeeBulkProcessViewBL.FromModelList(employees.ToList())
            };
            return result;                
        }
    }
}