﻿using Point.Business.Helper;
using Point.Business.Logic;
using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Infrastructure.Exceptions;
using Point.MVC.WebApplication.Attributes;
using Point.MVC.WebApplication.Controllers;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Point.Infrastructure.Extensions;

namespace Point.MVC.WebApplication.Areas.Management.Controllers
{
    [FormSecurity(FunctionRoleTypeID.ManageFavorites, SecurityAttribute.RegionID, SecurityAttribute.LocationID)]
    public class FavoritesController : PointController
    {
        [HttpGet]
        [FunctionRoles(FunctionRoleTypeID.ManageAll, FunctionRoleTypeID.ManageFavorites)]
        public ActionResult Index(int? locationid, int? regionid)
        {
            var currentUser = PointUserInfo();
            var selectedLocationId = locationid ?? currentUser.Location.LocationID;
            var selectedRegioId = regionid ?? currentUser.Region.RegionID;

            var accesslevel = FunctionRoleHelper.GetAccessLevel(uow, FunctionRoleTypeID.ManageFavorites, currentUser);

            var regions = RegionBL.GetByRegionIDs(uow, accesslevel.RegionIDs).OrderBy(r => r.Name).ToList();
            var regionoptions = new List<Option>();
            regions.ForEach(reg => regionoptions.Add(new Option(reg.Name, reg.RegionID.ToString(), selectedRegioId == reg.RegionID)));
            
            List<Location> locations = LocationBL.GetActiveByRegionID(uow, selectedRegioId).OrderBy(org => org.Organization.OrganizationTypeID).ThenBy(loc => loc.Organization.Name).ThenBy(loc => loc.Name).ToList();
            if (accesslevel.MaxLevel == FunctionRoleLevelID.Organization)
            {
                locations = locations.Where(loc => accesslevel.OrganizationIDs.Contains(loc.OrganizationID)).ToList();
            }
            else if (accesslevel.MaxLevel <= FunctionRoleLevelID.Location)
            {
                locations = locations.Where(loc => accesslevel.LocationIDs.Contains(loc.LocationID)).ToList();
            }
            var locationoptions = new List<Option>();
            locations.ForEach(loc => locationoptions.Add(new Option(loc.FullName(), loc.LocationID.ToString(), loc.LocationID == selectedLocationId)));
            
            var preferredLocations = PreferredLocationBL.GetByLocationID(uow, selectedLocationId).OrderBy(loc => loc.Location?.Organization?.OrganizationTypeID).ThenBy(org => org.Location?.Organization?.Name).ToList();
            var preferredlocationsviewmodel = PreferredLocationViewBL.FromModel(preferredLocations);

            var viewmodel = new PreferredLocationsViewModel
            {
                SelectedRegionID = selectedRegioId,
                SelectedLocationID = selectedLocationId,
                Regions = regionoptions,
                Locations = locationoptions,
                PreferredLocations = preferredlocationsviewmodel
            };

            return View(viewmodel);
        }

        [HttpDelete]
        [FunctionRoles(FunctionRoleTypeID.ManageAll, FunctionRoleTypeID.ManageFavorites)]
        public JsonResult DeleteFavorite(int locationid, int preferredlocationid)
        {
            var currentUser = PointUserInfo();
            var maxlevel = currentUser.GetFunctionRoleLevelID(FunctionRoleTypeID.ManageServicePage);

            if (maxlevel < FunctionRoleLevelID.Organization)
            {
                if (!currentUser.EmployeeLocationIDs.Contains(locationid))
                    throw new PointSecurityException(ExceptionType.AccessDenied);
            }

            PreferredLocationBL.Delete(uow, locationid, preferredlocationid);

            return Json(true);
        }

        [HttpPost]
        [FunctionRoles(FunctionRoleTypeID.ManageAll, FunctionRoleTypeID.ManageFavorites)]
        public PartialViewResult AddFavorite(int locationid, int preferredlocationid)
        {
            var currentUser = PointUserInfo();
            var maxlevel = currentUser.GetFunctionRoleLevelID(FunctionRoleTypeID.ManageServicePage);

            if (maxlevel < FunctionRoleLevelID.Region)
            {
                if (!currentUser.EmployeeLocationIDs.Contains(locationid))
                    throw new PointSecurityException(ExceptionType.AccessDenied);
            }

            var preferredlocation = PreferredLocationBL.Insert(uow, locationid, preferredlocationid);
            preferredlocation.Location = LocationBL.GetByID(uow, preferredlocationid);

            return PartialView("_FavoriteLocation", PreferredLocationViewBL.FromModel(preferredlocation));
        }
    }
}