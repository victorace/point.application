﻿using System.Threading.Tasks;
using DevTrends.MvcDonutCaching;
using Point.Models.Enums;
using Point.MVC.WebApplication.Attributes;
using Point.MVC.WebApplication.Controllers;
using System.Web.Mvc;
using System.Web.Routing;
using Point.Database.Models.ViewModels;
using Point.MVC.Shared.Constants;
using Point.MVC.Shared.Helpers;
using Point.MVC.Shared.ViewModels.Capacity;

namespace Point.MVC.WebApplication.Areas.Management.Controllers
{
    [DonutOutputCache(Duration = 0)]
    [ValidateOrigin]
    public sealed class CapacityController : PointFormController
    {
        private PointUserInfo _pointUserInfo;
        private Point.MVC.Shared.Controllers.CapacityController _sharedCapacityController;
        
        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            _pointUserInfo = PointUserInfo();
            _sharedCapacityController = new Shared.Controllers.CapacityController(this, uow, _pointUserInfo, showMutadedByUserFields: true, isPublicCapacityVersion: false);
        }

        public ActionResult Index()
        {
            return _sharedCapacityController.Index();
        }
        
        public ActionResult Tile(bool showByRegion = true, bool withCapacityOnly = true, int[] regionIDs = null)
        {
            return _sharedCapacityController.Tile(showByRegion, withCapacityOnly, regionIDs);
        }

        [HttpPost]
        public JsonResult GetTileResults(bool showByRegion = true, bool withCapacityOnly = true, int[] regionIDs = null)
        {
            return _sharedCapacityController.GetTileResults(showByRegion, withCapacityOnly, regionIDs);
        }

        public ActionResult List(bool showByRegion = true, string name = null, string city = null,
            int? afterCareGroupID = null, int? afterCareTypeID = null, bool withCapacityOnly= true, int[] regionIDs = null, 
            bool searched=false, int? afterCareTileGroupID = null, int? organizationTypeID = null, int pageNo = 1, string postalCode = null, int? postalCodeRadius = null)
        {
            return _sharedCapacityController.List(showByRegion, name, city, afterCareGroupID, afterCareTypeID, withCapacityOnly, regionIDs, searched, afterCareTileGroupID, organizationTypeID, pageNo, postalCode, postalCodeRadius);
        }

        [HttpPost]
        public JsonResult GetListResults(bool showByRegion = true, string name = null, string city = null, 
            int? afterCareGroupID = null, int? afterCareTypeID = null, bool withCapacityOnly = true, int[] regionIDs = null, int pageNo = 1, string postalCode = null, int? postalCodeRadius = null)
        {
            return _sharedCapacityController.GetListResults(showByRegion, name, city, afterCareGroupID, afterCareTypeID, withCapacityOnly, regionIDs, pageNo, postalCode, postalCodeRadius);
        }

        [FunctionRoles(FunctionRoleTypeID.ManageAll, FunctionRoleTypeID.ManageCapacity)]
        public ActionResult Edit(CapacityEditViewModel viewModel)
        {
            var parameters = CapacityViewModelHelper.GetParameters(this, uow, _pointUserInfo, false, false, null, null, null,
                null, null, null, null, null, null, isPublicVersion: false);
            return View(CapacityViewModelHelper.PathViews + "_CapacityManagement.cshtml", CapacityViewModelHelper.GetEditViewModel(parameters, viewModel));
        }

        [HttpPost]
        [FunctionRoles(FunctionRoleTypeID.ManageAll, FunctionRoleTypeID.ManageCapacity)]
        public ActionResult CapacitySave(CapacityEditViewModel viewModel)
        {
            var parameters = CapacityViewModelHelper.GetParameters(this, uow, _pointUserInfo, false, false, null, null, null,
                null, null, null, null, null, null, isPublicVersion: false);
            CapacityViewModelHelper.SaveEditViewModel(parameters, viewModel);
            return RedirectToAction("Edit", viewModel);
        }

        [HttpPost]
        public JsonResult GetAfterCareGroupsAndTypes(bool withCapacityOnly = true, int[] regionIDs = null, int? afterCareGroupID = null, int? afterCareTypeID = null, int? organizationTypeID = null, string currentTabView = TabMenuItems.Other)
        {
            return _sharedCapacityController.GetAfterCareGroupsAndTypes(withCapacityOnly, regionIDs, afterCareGroupID, afterCareTypeID, organizationTypeID, currentTabView);
        }

        public ActionResult AfterCareTypeDescriptionsModal()
        {
            return _sharedCapacityController.AfterCareTypeDescriptionsModal();
        }

        public async Task<ActionResult> AfterCareTypeDescriptions(int organizationTypeID = (int)OrganizationTypeID.HealthCareProvider, int afterCareTileGroupID = -1)
        {
            return await _sharedCapacityController.AfterCareTypeDescriptions(organizationTypeID, afterCareTileGroupID);
        }

        public async Task<ActionResult> AfterCareTypeDescriptionsByCategory(int[] ids)
        {
            return await _sharedCapacityController.AfterCareTypeDescriptionsByCategory(ids);
        }
    }
}