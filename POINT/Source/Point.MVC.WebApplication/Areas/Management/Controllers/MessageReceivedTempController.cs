﻿using Point.Business.Helper;
using Point.Business.Logic;
using Point.Database.Models;
using Point.Infrastructure.Attributes;
using Point.Models.Enums;
using Point.MVC.WebApplication.Attributes;
using Point.MVC.WebApplication.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Point.MVC.WebApplication.Areas.Management.Controllers
{
    [FunctionRoles(FunctionRoleLevelID.Organization, FunctionRoleTypeID.ManageAll, FunctionRoleTypeID.ManageForms)]
    [ValidateOrigin]
    public class MessageReceivedTempController : PointController
    {
        public ActionResult Index(DateTime? dateFrom, DateTime? dateTo)
        {
            var accesslevel = FunctionRoleHelper.GetAccessLevel(uow, FunctionRoleTypeID.ManageFavorites, PointUserInfo());
            IEnumerable<MessageReceivedTemp> model = Enumerable.Empty<MessageReceivedTemp>();
            if (dateFrom.HasValue && dateTo.HasValue)
            {
                model = MessageReceivedTempBL.Get(uow, accesslevel.OrganizationIDs, dateFrom.Value, dateTo.Value);
            }

            return View(model);
        }

        [ValidateAntiFiddleInjection("messageReceivedTempID")]
        public ActionResult ViewContent(int messageReceivedTempID)
        {
            var model = FieldReceivedTempBL.GetByMessageReceivedTempID(uow, messageReceivedTempID);
            return View(model);
        }

    }
}