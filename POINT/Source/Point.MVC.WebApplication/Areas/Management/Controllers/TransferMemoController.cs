﻿using System.Linq;
using System.Web.Mvc;
using Point.Business.Logic;
using Point.Database.Models;
using Point.Models.Enums;
using Point.MVC.WebApplication.Controllers;
using Point.MVC.WebApplication.Attributes;
using Point.Infrastructure.Attributes;
using DevTrends.MvcDonutCaching;
using System.Collections.Generic;
using Point.Infrastructure.Extensions;

namespace Point.MVC.WebApplication.Areas.Management.Controllers
{
    [DonutOutputCache(Duration = 0)]
    [FormSecurity(FunctionRoleTypeID.ManageTransferMemoTemplates, SecurityAttribute.RegionID, SecurityAttribute.OrganizationID)]
    [ValidateOrigin]
    public class TransferMemoController : PointController
    {
        [FunctionRoles(FunctionRoleTypeID.ManageTransferMemoTemplates, FunctionRoleTypeID.ManageAll)]
        public ActionResult Templates(int? regionid = null, int? organizationid = null)
        {
            var pointuserinfo = PointUserInfo();

            if (regionid == null)
                regionid = pointuserinfo.Region.RegionID;

            if (organizationid == null && regionid == pointuserinfo.Region.RegionID)
                organizationid = pointuserinfo.Organization.OrganizationID;

            var multipleregions = pointuserinfo.IsGlobalAdmin;
            var multipleorganizations = pointuserinfo.IsRegioAdmin;

            ViewBag.RegionID = regionid;
            ViewBag.OrganizationID = organizationid;

            var regions = multipleregions
                ? RegionBL.GetAll(uow)
                : new[] { pointuserinfo.Region };

            ViewBag.Regions = regions.ToSelectListItems(it => it.Name, it => it.RegionID.ToString(), it => it.RegionID == regionid,
                    multipleregions ? " - Maak uw keuze - " : null).OrderBy(it => it.Text);

            var organizations = multipleorganizations
                ? OrganizationBL.GetActiveByRegionIDAndOrganizationTypeID(uow, regionid.Value, (int)OrganizationTypeID.Hospital)
                : new List<Organization>() { pointuserinfo.Organization };

            ViewBag.Organizations = organizations.ToSelectListItems(it => it.Name, it => it.OrganizationID.ToString(), it => it.OrganizationID == organizationid,
                    multipleorganizations ? " - Maak uw keuze - " : null).OrderBy(it => it.Text);

            var models = TransferMemoBL.GetTemplatesByOrganizationID(uow, organizationid).ToList();


            // Why is database model used as view model? is there any reason?????
            return View(models);
        }

        [FunctionRoles(FunctionRoleTypeID.ManageTransferMemoTemplates, FunctionRoleTypeID.ManageAll)]
        public ActionResult EditTemplate(int transfermemotemplateid)
        {
            var model = TransferMemoBL.GetTemplateByID(uow, transfermemotemplateid);
            return View(model);
        }

        [FunctionRoles(FunctionRoleTypeID.ManageTransferMemoTemplates, FunctionRoleTypeID.ManageAll)]
        public ActionResult AddTemplate(int organizationid)
        {
            var model = new TransferMemoTemplate();
            model.OrganizationID = organizationid;

            return View("EditTemplate", model);
        }

        public ActionResult DeleteTemplate(int transferMemoTemplateID, bool confirm = false)
        {
            if(confirm == false)
            {
                var model = TransferMemoBL.GetTemplateByID(uow, transferMemoTemplateID);
                return View("DeleteTemplate", model);
            }

            var template = TransferMemoBL.GetTemplateByID(uow, transferMemoTemplateID);
            var organization = OrganizationBL.GetByOrganizationID(uow, template.OrganizationID);
            return RedirectToAction("Templates", new { RegionID = organization.RegionID, OrganizationID = organization.OrganizationID });
        }

        [HttpPost]
        [ValidateAntiFiddleInjection("TransferMemoTemplateID")]
        public ActionResult DeleteTemplate(int transferMemoTemplateID)
        {
            var template = TransferMemoBL.GetTemplateByID(uow, transferMemoTemplateID);
            var organization = OrganizationBL.GetByOrganizationID(uow, template.OrganizationID);
            TransferMemoBL.DeleteTemplateByID(uow, transferMemoTemplateID);

            return RedirectToAction("Templates", new { RegionID = organization.RegionID, OrganizationID = organization.OrganizationID });
        }

        [HttpPost]
        [ValidateAntiFiddleInjection("TransferMemoTemplateID")]
        public JsonResult SaveTemplate(TransferMemoTemplate model)
        {
            TransferMemoBL.SaveTemplate(uow, model);
            var organization = OrganizationBL.GetByOrganizationID(uow, model.OrganizationID);
            return Json(new { Success = true, Url = Url.Action("Templates", "TransferMemo", new { Area = "Management", RegionID = organization.RegionID, OrganizationID = organization.OrganizationID })});
        }
    }
}