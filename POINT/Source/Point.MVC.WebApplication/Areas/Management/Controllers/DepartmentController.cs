﻿using System.Linq;
using DevTrends.MvcDonutCaching;
using Point.Business.Logic;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Infrastructure.Attributes;
using Point.MVC.WebApplication.Attributes;
using Point.MVC.WebApplication.Controllers;
using System.Web.Mvc;
using System.Web.Routing;
using Point.MVC.WebApplication.Helpers;

namespace Point.MVC.WebApplication.Areas.Management.Controllers
{
    [DonutOutputCache(Duration = 0)]
    [ValidateOrigin]
    public class DepartmentController : PointController
    {
        public ActionResult DepartmentSearch(int? regionID = null, string organizationIDs = null, string locationIDs = null, string departmentIDs = null, string selectedDepartmentIDs = null)
        {
            return View(DepartmentViewModelHelper.GetDepartmentSearchViewModel(regionID, organizationIDs, locationIDs, departmentIDs, selectedDepartmentIDs));
        }

        public ActionResult DepartmentSearchData(int? regionID, string organizationIDs = null, string locationIDs = null, string departmentIDs = null, string selectedDepartmentIDs = null, string naamPlaats = "", string sortBy = "")
        {
            var viewModel = DepartmentViewModelHelper.GetDepartmentSearchDataViewModel(uow, regionID, organizationIDs, locationIDs, departmentIDs, selectedDepartmentIDs, naamPlaats, sortBy);
            return PartialView(viewModel);
        }
        
        [HttpGet]
        [FunctionRoles(FunctionRoleTypeID.ManageAll, FunctionRoleTypeID.ManageOrganization)]
        public ActionResult Create(int? locationID = null, int? organizationID = null)
        {
            var viewModel = DepartmentViewModelHelper.GetEditViewModel(this, null, locationID, organizationID);
            return PartialView("Edit", viewModel);
        }

        [HttpGet]
        [FunctionRoles(FunctionRoleTypeID.ManageAll, FunctionRoleTypeID.ManageOrganization)]
        [ValidateAntiFiddleInjection("DepartmentID")]
        public ActionResult Edit(int departmentID)
        {
            var viewModel = DepartmentViewModelHelper.GetEditViewModel(this, departmentID);
            return PartialView(viewModel);
        }

        [HttpPost]
        [FunctionRoles(FunctionRoleTypeID.ManageAll, FunctionRoleTypeID.ManageOrganization)]
        [ValidateAntiForgeryToken]
        [ValidateAntiFiddleInjection("DepartmentID")]
        public ActionResult Save(DepartmentViewModel viewmodel)
        {
            var pointUserInfo = PointUserInfo();
            DepartmentViewBL.ValidateViewModel(uow, pointUserInfo, viewmodel);

            bool success;
            int departmentID = viewmodel.DepartmentID;
            if (!ModelState.IsValid)
            {
                return Json(new
                {
                    ShowErrors = true,
                    ValidationErrors = ModelState.Values.SelectMany(m => m.Errors)
                        .Select(e => e.ErrorMessage)
                        .ToList()
                });
            }

            var logEntry = LogEntryBL.Create(FlowFormType.AdminOrganization, pointUserInfo);
            var department = DepartmentBL.Save(uow, viewmodel, logEntry, pointUserInfo);
            departmentID = department.DepartmentID;

            OrganizationUpdateBL.CalculateForDepartmentAsync(department.DepartmentID);      // TODO: IS THIS STILL NEEDED?
            // TODO: XXX    
            //OrganizationUpdateBL.CapManForDepartmentAsync(department.DepartmentID);
            DepartmentCapacityPublicBL.UpsertByDepartment(uow, department, pointUserInfo.Employee.UserName);


            success = true;

            return Json(new { success, DepartmentID = departmentID, Digest = AntiFiddleInjection.CreateDigest(departmentID), Url = Url.Action("Index", "Organization", new RouteValueDictionary { { "Area", "Management" } }) });
        }
    }
}