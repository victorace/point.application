﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Point.Business.Logic;
using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Infrastructure.Extensions;
using Point.MVC.WebApplication.Controllers;
using Point.MVC.WebApplication.Attributes;
using Point.Infrastructure.Exceptions;

namespace Point.MVC.WebApplication.Areas.Management.Controllers
{
    [FunctionRoles(FunctionRoleLevelID.Region, FunctionRoleTypeID.ManageAll, FunctionRoleTypeID.ManageLogs)]
    [ValidateOrigin]
    public class LogReadController : PointController
    {
        public ActionResult Index(int? transferID)
        {
            IEnumerable<LogRead> logread = Enumerable.Empty<LogRead>();

            if (transferID > 0)
            {
                var maxlevel = PointUserInfo().GetFunctionRoleLevelID(FunctionRoleTypeID.ManageLogs);
                var userRegionID = PointUserInfo().Region.RegionID;
                switch (maxlevel)
                {
                    case FunctionRoleLevelID.Global:                        
                        break;
                    case FunctionRoleLevelID.Region:
                        var flowinstance = FlowInstanceBL.GetByTransferID(uow, transferID.ConvertTo<int>());
                        if (flowinstance == null)
                        {
                            transferID = -1;
                        }
                        else
                        {
                            //as a region admin you may see those logreads that sender organization is in your region, so:                          
                            if (flowinstance.StartedByOrganization.RegionID != userRegionID)
                            {
                                throw new PointSecurityException("Geen rechten om dit dossier te bekijken", ExceptionType.AccessDenied);
                            }
                        }
                        break;
                    default:
                        transferID = -1; //reset the transferID, though it shouldn't have come here
                        break;
                }
                logread = LogReadBL.GetByTransferID(uow, transferID.ConvertTo<int>());
            }

            var employees = EmployeeBL.GetByIDs(uow, logread?.Where(lr => lr.EmployeeID != null)?.Select(lr => (int)lr.EmployeeID).Distinct());

            var screens = ScreenBL.GetScreens(uow);

            var viewmodel = LogReadViewModel.FromModelList(logread, employees, screens);           

            return View(viewmodel.OrderByDescending(lr => lr.LogRead.TimeStamp));
        }
        
    }
}