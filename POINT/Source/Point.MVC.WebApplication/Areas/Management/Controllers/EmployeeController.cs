﻿using DevTrends.MvcDonutCaching;
using Point.Business.Helper;
using Point.Business.Logic;
using Point.Database.Extensions;
using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Infrastructure.Attributes;
using Point.MVC.WebApplication.Attributes;
using Point.MVC.WebApplication.Controllers;
using Point.MVC.WebApplication.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Point.MVC.WebApplication.Areas.Management.Controllers
{
    [OutputCache(Duration = 0)]
    [ValidateOrigin]
    public class EmployeeController : PointController
    {
        private const int MAX_EMPLOYEES = 100;

        private EmployeeViewModel getEditViewModelandSetViewBag(int? employeeid)
        {
            var model = employeeid.HasValue
                ? EmployeeBL.GetByID(uow, employeeid.Value)
                : null;

            var pointuserinfo = PointUserInfo();
            var maxlevel = pointuserinfo.GetFunctionRoleLevelID(FunctionRoleTypeID.ManageEmployees);

            var accesslevel = FunctionRoleHelper.GetAccessLevel(uow, FunctionRoleTypeID.ManageEmployees, pointuserinfo);

            switch (maxlevel)
            {
                case FunctionRoleLevelID.Department:
                    ViewBag.DepartmentIDs = pointuserinfo.EmployeeDepartmentIDs.ToArray();
                    break;
                case FunctionRoleLevelID.Location:
                    ViewBag.LocationIDs = accesslevel.LocationIDs.ToArray();
                    break;
                case FunctionRoleLevelID.Organization:
                    ViewBag.OrganizationIDs = accesslevel.OrganizationIDs.ToArray();
                    break;
                case FunctionRoleLevelID.Region:
                    ViewBag.RegionID = pointuserinfo.Region.RegionID;
                    break;
            }

            var mfarequired = false;

            List<Organization> organizations;
            List<Location> locations = new List<Location>();
            List<Department> departments = new List<Department>();

            if(employeeid.HasValue && model != null)
            {
                organizations = new List<Organization> { model.DefaultDepartment.Location.Organization };
            }
            else if (maxlevel == FunctionRoleLevelID.Global)
            {
                organizations = OrganizationBL.GetActive(uow).ToList();
            }
            else if (maxlevel == FunctionRoleLevelID.Region)
            {
                organizations = OrganizationBL.GetActiveByRegionID(uow, pointuserinfo.Region.RegionID).ToList();
            }
            else if (maxlevel == FunctionRoleLevelID.Organization)
            {
                organizations = OrganizationBL.GetByOrganizationIDs(uow, accesslevel.OrganizationIDs).ToList();
            }
            else if (maxlevel == FunctionRoleLevelID.Location)
            {
                organizations = OrganizationBL.GetByLocationIDs(uow, accesslevel.LocationIDs).ToList();
            }
            else
            {
                organizations = new List<Organization>() { pointuserinfo.Organization };
            }

            if(organizations.Count == 1 && model == null)
            {
                // Organization level:
                // model is null, so we don't need to check if MFA is needed on employee level.
                mfarequired = organizations.FirstOrDefault().MFARequired ;

                locations = LocationBL.GetActiveByOrganisationID(uow, organizations.FirstOrDefault().OrganizationID)
                    .ToList()
                    .Where(it => accesslevel.MaxLevel >= FunctionRoleLevelID.Organization || accesslevel.LocationIDs.Contains(it.LocationID))
                    .ToList();
            }

            if(locations.Count == 1 && model == null)
            {
                departments = DepartmentBL.GetByLocationID(uow, locations.FirstOrDefault().LocationID)
                    .ToList()
                    .Where(it => accesslevel.MaxLevel >= FunctionRoleLevelID.Location || pointuserinfo.EmployeeDepartmentIDs.Contains(it.DepartmentID))
                    .ToList();
            }
            
            if (model != null)
            {
                if (model.DefaultDepartment?.Location?.Organization != null)
                {
                    ViewBag.OrganizationIsDeleted = model.DefaultDepartment.Location.Organization.Inactive == true;
                    ViewBag.LocationIsDeleted = model.DefaultDepartment.Location.Inactive == true;
                    ViewBag.PrimaryDepartmentIsDeleted = model.DefaultDepartment.Inactive == true;
                    //If editing existing employee only organizations of current type
                    organizations = organizations.Where(org => org.OrganizationTypeID == model.DefaultDepartment.Location.Organization.OrganizationTypeID).ToList();

                    //allways fill the organization with at least the one currently connected (even without rights)
                    if (organizations.All(org => org.OrganizationID != model.DefaultDepartment.Location.OrganizationID))
                    {
                        organizations.Add(model.DefaultDepartment?.Location?.Organization);
                    }


                    //The same for location
                    var currentlocations = LocationBL.GetActiveByOrganisationID(uow, model.DefaultDepartment.Location.OrganizationID)
                        .ToList()
                        .Where(it => it.LocationID == model.DefaultDepartment.LocationID || accesslevel.LocationIDs.Contains(it.LocationID))
                        .ToList();

                    foreach (var currentlocation in currentlocations.Where(it => locations.All(loc => loc.LocationID != it.LocationID)))
                    {
                        locations.Add(currentlocation);
                    }

                    var currentdepartments = DepartmentBL.GetByLocationID(uow, model.DefaultDepartment.LocationID)
                        .ToList()
                        .Where(it => 
                                it.DepartmentID == model.DefaultDepartment.DepartmentID 
                                || pointuserinfo.DepartmentTypeIDS.Contains(it.DepartmentID) 
                                || accesslevel.MaxLevel > FunctionRoleLevelID.Department
                              );

                    foreach (var currentdepartment in currentdepartments.Where(it => departments.All(dep => dep.DepartmentID != it.DepartmentID)))
                    {
                        departments.Add(currentdepartment);
                    }

                    //Fix for inactive organization
                    if (!organizations.Any(d => d.OrganizationID == model.DefaultDepartment.Location.OrganizationID))
                    {
                        organizations.Add(model.DefaultDepartment.Location.Organization);
                    }

                    //Fix for inactive location
                    if (!locations.Any(d => d.LocationID == model.DefaultDepartment.LocationID))
                    {
                        locations.Add(model.DefaultDepartment.Location);
                    }

                    //Fix for inactive primary department
                    if (!departments.Any(d => d.DepartmentID == model.DepartmentID))
                    {
                        departments.Add(model.DefaultDepartment);
                    }

                    // Organization level:
                    // model is not null, so we need to check if employee needs to be controlled for MFA. So:
                    mfarequired = model.DefaultDepartment.Location.Organization.MFARequired || model.MFARequired;
                }
            }

            ViewBag.Organizations = organizations.OrderBy(it =>it.Name).ToList();
            ViewBag.Locations = locations.OrderBy(it => it.Name).ToList();
            ViewBag.Departments = departments.OrderBy(it => it.Name).ToList();

            var myDossierLevel = pointuserinfo.Employee.PointRole.ToFunctionRoleLevelID();
            var myDossierAdminLevel = pointuserinfo.GetMaxDossierLevelAdminLevelID();
            var myFunctionLevel = pointuserinfo.GetFunctionRoleLevelID(FunctionRoleTypeID.ManageEmployees);
            var myReportingLevel = pointuserinfo.GetReportFunctionRoleLevelID();

            var viewmodel = EmployeeViewBL.FromModel(uow, model);

            viewmodel.ShowMFANumber = mfarequired && myFunctionLevel >= viewmodel.FunctionRoleLevelID;

            viewmodel.AllowedDossierAdminLevelsIDs = getAllowedFunctionRoleLevelIDs(myDossierAdminLevel, viewmodel.MaxDossierLevelAdminLevelID);
            viewmodel.AllowedDossierLevelIDs = getAllowedFunctionRoleLevelIDs(myDossierAdminLevel, viewmodel.DossierLevelID);
            viewmodel.AllowedFunctionRoleLevelIDs = getAllowedFunctionRoleLevelIDs(myFunctionLevel, viewmodel.FunctionRoleLevelID);
            viewmodel.AllowedReportingLevelIDs = getAllowedFunctionRoleLevelIDs(myReportingLevel, viewmodel.ReportingLevelID);

            var allowedFunctionRoleTypeIDs = pointuserinfo.HasFunctionRole(FunctionRoleTypeID.ManageAll)
                ? ((FunctionRoleTypeID[])Enum.GetValues(typeof(FunctionRoleTypeID))).ToList()
                : pointuserinfo.Employee.FunctionRoles.Select(it => it.FunctionRoleTypeID).ToList();

            var functionRoleFilter = hiddenFunctionRoleTypeIDSByOrganizationID((model?.DefaultDepartment?.Location?.OrganizationID).GetValueOrDefault());

            //!!!! Uitzonderingen !!!!
            if (viewmodel.EmployeeID == 0 && myDossierAdminLevel > FunctionRoleLevelID.None)
            {
                //!!!! Bij nieuwe medewerker !!!!
                //!!!! Alleen DossierRole op Department als je "Te beheren dossier toegangsniveau" niet op None staat !!!!!
                viewmodel.DossierLevelID = FunctionRoleLevelID.Department;
            }
            if (pointuserinfo.Employee.EmployeeID == viewmodel.EmployeeID && viewmodel.AllowedDossierLevelIDs != null)
            {
                //!!!! Bij jezelf bewerken !!!!
                //!!!! Je mag nooit je eigen dossierniveau hoger zetten ook al staat je "Te beheren dossier toegangsniveau" dat toe !!!!
                viewmodel.AllowedDossierLevelIDs = viewmodel.AllowedDossierLevelIDs.Where(r => r <= myDossierLevel).ToList();
            }
            //!!!! Bepaalde functies niet voor bepaalde type organisaties
            viewmodel.AllowedFunctionRoleTypeIDs = allowedFunctionRoleTypeIDs.Where(afr => !functionRoleFilter.Contains((int)afr)).ToList();
            viewmodel.HiddenFunctions = hiddenFunctionsByDepartmentID((model?.DefaultDepartment?.DepartmentID).GetValueOrDefault());

            return viewmodel;
        }

        private List<FunctionRoleLevelID> getAllowedFunctionRoleLevelIDs(FunctionRoleLevelID myFunctionRoleLevelID, FunctionRoleLevelID levelIDToEdit)
        {
            var allowedLevelsIDs = ((FunctionRoleLevelID[])Enum.GetValues(typeof(FunctionRoleLevelID))).Where(it => it <= myFunctionRoleLevelID).ToList();
            if(levelIDToEdit > allowedLevelsIDs.Max())
            {
                return null;
            }

            return allowedLevelsIDs;
        }

        private int[] hiddenFunctionRoleTypeIDSByOrganizationID(int organizationid)
        {
            var organization = OrganizationBL.GetByOrganizationID(uow, organizationid);
            var returndata = new int[] { }; ;
            if (organization?.OrganizationTypeID == (int)OrganizationTypeID.HealthCareProvider)
            {
                returndata = new int[] { (int)FunctionRoleTypeID.ManageDoctors, (int)FunctionRoleTypeID.ManageDocuments, (int)FunctionRoleTypeID.ManageForms, (int)FunctionRoleTypeID.ManageServicePage, (int)FunctionRoleTypeID.ManageAll };
            }

            return returndata;
        }

        public JsonResult HiddenFunctionRoleTypeIDSByOrganizationID(int organizationid)
        {
            var returndata = hiddenFunctionRoleTypeIDSByOrganizationID(organizationid);

            return Json(returndata, JsonRequestBehavior.AllowGet);
        }

        private string[] hiddenFunctionsByDepartmentID(int departmentid)
        {
            var department = DepartmentBL.GetByDepartmentID(uow, departmentid);
            var returndata = new string[] { }; ;
            if (department?.Location?.Organization?.OrganizationTypeID == (int)OrganizationTypeID.HealthCareProvider || department?.DepartmentTypeID == (int)DepartmentTypeID.Transferpunt)
            {
                returndata = new string[] { "EditAllForms" };
            }

            return returndata;
        }

        public JsonResult HiddenFunctionsByDepartmentID(int departmentid)
        {
            var returndata = hiddenFunctionsByDepartmentID(departmentid);

            return Json(returndata, JsonRequestBehavior.AllowGet);
        }

        private List<EmployeeViewModel> getViewModelAndSetViewBag(string name, string organizationname, bool searchDeleted, DateTime? lastLogin = null, bool showall = false)
        {
            var pointuserinfo = PointUserInfo();

            var accesslevel = FunctionRoleHelper.GetAccessLevel(uow, FunctionRoleTypeID.ManageEmployees, pointuserinfo);

            int? regionid = null;
            int[] organizationids = null;
            int[] locationids = null;
            int[] departmentids = null;

            switch (accesslevel.MaxLevel)
            {
                case FunctionRoleLevelID.Department:
                    departmentids = pointuserinfo.EmployeeDepartmentIDs.ToArray();
                    break;
                case FunctionRoleLevelID.Location:
                    locationids = accesslevel.LocationIDs.ToArray();
                    break;
                case FunctionRoleLevelID.Organization:
                    organizationids = accesslevel.OrganizationIDs.ToArray();
                    break;
                case FunctionRoleLevelID.Region:
                    regionid = pointuserinfo.Region.RegionID;
                    break;
            }

            ViewBag.Name = name;
            ViewBag.Organization = organizationname;
            ViewBag.SearchDeleted = searchDeleted;
            ViewBag.ShowAll = showall;

            if (accesslevel.MaxLevel > FunctionRoleLevelID.Organization && string.IsNullOrEmpty(name) && string.IsNullOrEmpty(organizationname) && !lastLogin.HasValue)
                return new List<EmployeeViewModel>();

            var employees = EmployeeBL.Search(uow, name, organizationname, regionid, organizationids, locationids, departmentids, searchDeleted, lastLogin);

            int count = employees.Count();
            bool toomanyresults = count > MAX_EMPLOYEES;
            ViewBag.Total = count;
            ViewBag.TooManyResults = toomanyresults;

            return EmployeeViewBL.FromModelList(uow, toomanyresults && !showall ? new List<Employee>() : employees.ToList());
        }

        [FunctionRoles(FunctionRoleTypeID.ManageAll, FunctionRoleTypeID.ManageEmployees)]
        public ActionResult Index(string name, string organization, bool searchDeleted = false)
        {
            var viewmodel = getViewModelAndSetViewBag(name, organization, searchDeleted);
            return View(viewmodel);
        }

        [ValidateAntiFiddleInjection("EmployeeID")]
        [FunctionRoles(FunctionRoleTypeID.ManageAll, FunctionRoleTypeID.ManageEmployees)]
        public ActionResult Delete(int employeeid, bool confirm = false)
        {
            var model = EmployeeBL.GetByID(uow, employeeid);
            if (confirm == false)
            {
                return PartialView(model);
            }

            EmployeeBL.DeleteByID(uow, model.EmployeeID);
            return Json(new { succeeded = true });
        }

        [FunctionRoles(FunctionRoleTypeID.ManageAll, FunctionRoleTypeID.ManageEmployees)]
        public ActionResult Create()
        {
            var viewmodel = getEditViewModelandSetViewBag(null);

            return PartialView("Edit", viewmodel);
        }

        
        [ValidateAntiFiddleInjection("EmployeeID")]
        [FunctionRoles(FunctionRoleTypeID.ManageAll, FunctionRoleTypeID.ManageEmployees)]
        public ActionResult Edit(int employeeid)
        {
            var viewmodel = getEditViewModelandSetViewBag(employeeid);

            return PartialView(viewmodel);
        }

        [ValidateAntiFiddleInjection("EmployeeID")]
        [FunctionRoles(FunctionRoleTypeID.ManageAll, FunctionRoleTypeID.ManageEmployees)]
        public ActionResult Save(EmployeeViewModel viewmodel)
        {
            var pointuserinfo = PointUserInfo();
            EmployeeViewBL.ValidateViewModel(uow, pointuserinfo, viewmodel);

            if(string.IsNullOrEmpty(viewmodel.Password) && string.IsNullOrEmpty(viewmodel.Password2))
            {
                ModelState["Password"].Errors.Clear();
                ModelState["Password2"].Errors.Clear();
            }

            if (ModelState.IsValid)
            {
                
                var logentry = LogEntryBL.Create(FlowFormType.AdminEmployee, pointuserinfo);
                var employee = EmployeeBL.Save(uow, viewmodel, logentry, pointuserinfo);
                PointUserInfoHelper.ClearCache(employee.UserId.GetValueOrDefault());
                
                return Json(new { employee.EmployeeID, Digest = AntiFiddleInjection.CreateDigest(employee.EmployeeID), succeeded = true });
            }
            else
            {
                return Json(new
                {
                    ShowErrors = true,
                    ValidationErrors = ModelState.Values.SelectMany(m => m.Errors)
                                                        .Select(e => e.ErrorMessage)
                                                        .ToList()
                });
            }
        }


        [ValidateAntiFiddleInjection("EmployeeID")]
        [FunctionRoles(FunctionRoleTypeID.ManageAll, FunctionRoleTypeID.ManageEmployees)]
        public ActionResult EditDigitalSignature(int employeeid)
        {
            return View();
        }

        [ValidateAntiFiddleInjection("EmployeeID")]
        [FunctionRoles(FunctionRoleTypeID.ManageAll, FunctionRoleTypeID.ManageEmployees)]
        public JsonResult DeleteDigitalSignature(int employeeid)
        {
            var employee = EmployeeBL.GetByID(uow, employeeid);

            var pointuserinfo = PointUserInfo();
            var logentry = LogEntryBL.Create(FlowFormType.AdminEmployee, pointuserinfo);

            var errormessage = "";
            DigitalSignatureBL.Delete(uow, employee, logentry);
            bool success = string.IsNullOrWhiteSpace(errormessage);
            
            return Json(new
            {
                Success = success,
                ErrorMessage = errormessage
            }, JsonRequestBehavior.AllowGet);
        }

        [ValidateAntiFiddleInjection("EmployeeID")]
        [HttpPost, FunctionRoles(FunctionRoleTypeID.ManageAll, FunctionRoleTypeID.ManageEmployees)]
        public JsonResult UploadDigitalSignature(int employeeid)
        {
            var httpfilecollectionhelper = new HttpFileCollectionBaseHelper();
            httpfilecollectionhelper.MaxContentLength = 1 * 1024 * 1024;
            if (httpfilecollectionhelper.CreateDatabaseImage(Request.Files,
                HttpFileCollectionBaseHelper.ImageMimeDictionary.Keys.ToList()))
            {
                var employee = EmployeeBL.GetByID(uow, employeeid);
                var pointuserinfo = PointUserInfo();
                var logentry = LogEntryBL.Create(FlowFormType.AdminEmployee, pointuserinfo);

                DigitalSignatureBL.Save(uow, employee, httpfilecollectionhelper.DatabaseImage, logentry);
            }

            bool success = string.IsNullOrWhiteSpace(httpfilecollectionhelper.Error);
            return Json(new
            {
                Success = success,
                ErrorMessage = httpfilecollectionhelper.Error
            });
        }

        private byte[] exportEmployeesToCSV(string name, string organizationname, bool searchDeleted)
        {
            var pointuserinfo = PointUserInfo();

            var accesslevel = FunctionRoleHelper.GetAccessLevel(uow, FunctionRoleTypeID.ManageEmployees, pointuserinfo);

            int? regionid = null;
            int[] organizationids = null;
            int[] locationids = null;
            int[] departmentids = null;

            switch (accesslevel.MaxLevel)
            {
                case FunctionRoleLevelID.Department:
                    departmentids = pointuserinfo.EmployeeDepartmentIDs.ToArray();
                    break;
                case FunctionRoleLevelID.Location:
                    locationids = accesslevel.LocationIDs.ToArray();
                    break;
                case FunctionRoleLevelID.Organization:
                    organizationids = accesslevel.OrganizationIDs.ToArray();
                    break;
                case FunctionRoleLevelID.Region:
                    regionid = pointuserinfo.Region.RegionID;
                    break;
            }

            if (accesslevel.MaxLevel > FunctionRoleLevelID.Organization && string.IsNullOrEmpty(name) && string.IsNullOrEmpty(organizationname))
                throw new Exception($"EmployeeCSV failed.");
         
            return EmployeeExportCSVModelBL.ExportEmployeesToCSV(uow, name, organizationname, regionid, organizationids, locationids, departmentids, searchDeleted, null, pointuserinfo);
        }

        [FunctionRoles(FunctionRoleTypeID.ManageAll, FunctionRoleTypeID.ManageEmployees)]
        public FileResult ExportToCSV(string searchName, string searchOrganization, bool searchDel =false)
        {
            string filename = string.Format("EmployeeCSV_{0}.csv", DateTime.Now.ToString("yyyyMMddHHmmss"));
            var bytes = exportEmployeesToCSV(searchName, searchOrganization, searchDel);

            
            return File(bytes, "text/csv", filename);
        }
    }
}