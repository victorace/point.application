﻿using Point.Business.Logic;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Infrastructure.Attributes;
using Point.MVC.WebApplication.Attributes;
using Point.MVC.WebApplication.Controllers;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Routing;

namespace Point.MVC.WebApplication.Areas.Management.Controllers
{
    [FunctionRoles(FunctionRoleLevelID.Region, FunctionRoleTypeID.ManageRegion, FunctionRoleTypeID.ManageAll)]
    [ValidateOrigin]
    public class RegionController : PointController
    {
        public ActionResult Index(string search)
        {
            var viewmodel = getViewModelAndSetViewBag(search);
            return View(viewmodel);
        }

        [HttpGet]
        [FunctionRoles(FunctionRoleLevelID.Region, FunctionRoleTypeID.ManageAll, FunctionRoleTypeID.ManageOrganization)]
        [ValidateAntiFiddleInjection("RegionID")]
        public ActionResult Edit(int regionid, FlowDefinitionID flowdefinitionid = FlowDefinitionID.ZH_VVT)
        {
            var viewModel = RegionViewModelBL.GetViewModelByID(uow, regionid);
            if (viewModel != null)
            {
                viewModel.FormTypesForRegion = FormTypesForRegionBL.GetByRegionIDAndFlowDefinitionID(uow, regionid, flowdefinitionid);
                viewModel.AllFormTypesOfRegion = FormTypesForRegionBL.GetByRegionID(uow, regionid);
            }

            return PartialView(viewModel);
        }

        [FunctionRoles(FunctionRoleLevelID.Region, FunctionRoleTypeID.ManageAll, FunctionRoleTypeID.ManageOrganization)]
        [ValidateAntiFiddleInjection("RegionID")]
        public ActionResult Save(RegionViewModel viewmodel)
        {
            var pointuserinfo = PointUserInfo();

            bool success;

            var regionExists = RegionViewModelBL.GetViewModelByName(uow, viewmodel.Name);
            if (regionExists != null && regionExists.RegionID != viewmodel.RegionID)
            {
                throw new System.Exception("Er is al een regio met dezelfde naam.");
            }
            
            var logentry = LogEntryBL.Create(FlowFormType.AdminRegion, pointuserinfo);

            RegionViewModelBL.Save(uow, viewmodel, pointuserinfo, logentry);

            //OrganizationUpdateBL.CalculateForRegionAsync(viewmodel.RegionID);

            success = true;

            //check why main list is not refreshed?
            return Json(new { success, RegionID = viewmodel.RegionID, Url = Url.Action("Index", "Region", new RouteValueDictionary() { { "Area", "Management" } }) });           
        }

        public PartialViewResult SelectedFormTypes(FlowDefinitionID flowdefinitionid, int regionID, RegionViewModel viewmodel )
        {            
            var formtypes = FormTypesForRegionBL.GetByRegionIDAndFlowDefinitionID(uow, regionID, flowdefinitionid);
            viewmodel.DefaultFlowDefinitionID = flowdefinitionid;
            viewmodel.FormTypesForRegion = formtypes;
            return PartialView("_SelectedFormTypes", viewmodel);          
        }

        private List<RegionViewModel> getViewModelAndSetViewBag(string search)
        {
            var pointuserinfo = PointUserInfo();
            var maxlevel = pointuserinfo.GetFunctionRoleLevelID(FunctionRoleTypeID.ManageAll);

            ViewBag.Search = search;

            //all communications with DAL goes through view model BL. So no direct call to RegionBL.
            var regions = maxlevel == FunctionRoleLevelID.Global ? RegionViewModelBL.FromModelList(uow, search) : new List<RegionViewModel>() { RegionViewModelBL.GetViewModelByID(uow, pointuserinfo.Region.RegionID) };

            return (regions);
        }
    }
}