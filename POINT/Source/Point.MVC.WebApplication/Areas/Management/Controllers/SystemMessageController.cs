﻿using DevTrends.MvcDonutCaching;
using Point.Business.Logic;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Infrastructure.Attributes;
using Point.MVC.WebApplication.Attributes;
using Point.MVC.WebApplication.Controllers;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;

namespace Point.MVC.WebApplication.Areas.Management.Controllers
{
    [DonutOutputCache(Duration = 0)]
    [ValidateOrigin]
    public class SystemMessageController : PointController
    {
        public ActionResult Overview()
        {
            var systemmessages = SystemMessageBL.GetAllPastMessages(uow).OrderByDescending(it => it.StartDate);
            var vm = SystemMessageViewBL.FromModel(systemmessages);

            return View(vm);
        }

        [FunctionRoles(FunctionRoleLevelID.Global, FunctionRoleTypeID.ManageAll)]
        public ActionResult Index(string search, bool searchdeleted = false, bool searchnationwide = false)
        {
            var model = getViewModelAndSetViewBag();
            return View(model);
        }

        [HttpGet]
        [FunctionRoles(FunctionRoleLevelID.Global, FunctionRoleTypeID.ManageAll)]
        public ActionResult Create()
        {
            var systemmessage = getEditViewModelandSetViewBag(null);
            return PartialView("Edit", systemmessage);
        }

        [HttpGet]
        [FunctionRoles(FunctionRoleLevelID.Global, FunctionRoleTypeID.ManageAll)]
        [ValidateAntiFiddleInjection("systemmessageid")]
        public ActionResult Edit(int systemmessageid)
        {
            var systemmessage = getEditViewModelandSetViewBag(systemmessageid);
            return PartialView(systemmessage);
        }

        [FunctionRoles(FunctionRoleLevelID.Global, FunctionRoleTypeID.ManageAll)]
        [ValidateAntiFiddleInjection("systemmessageid")]
        public ActionResult Delete(int systemmessageid, bool confirm = false)
        {
            var model = SystemMessageBL.GetByID(uow, systemmessageid);
            if (model == null) throw new System.Exception("No SystemMessage found for delete.");

            if (confirm == false)
            {
                var vm = SystemMessageViewBL.FromModel(model);
                return PartialView(vm);
            }

            SystemMessageBL.DeleteByID(uow, model.SystemMessageID);

            return RedirectToAction("Index", "SystemMessage");
        }

        [HttpPost]
        [FunctionRoles(FunctionRoleLevelID.Global, FunctionRoleTypeID.ManageAll)]
        [ValidateAntiForgeryToken]
        public ActionResult Save(SystemMessageViewModel model)
        {
            var success = ModelState.IsValid;

            if (success)
            {
                SystemMessageBL.Save(uow, model);
            }

            return Json(new
            {
                Success = success,
                Url = Url.Action("Index", "SystemMessage", new RouteValueDictionary {{"Area", "Management"}})
            });
        }

        private static List<Option> getFunctionCodes()
        {
            return new List<Option>()
            {
                new Option(" - Kies eventueel een functie - ", "", true),
                new Option("Controle vraag check", "ACCOUNT_CONTROL_QUESTION")
            };
        }

        private SystemMessageViewModel getEditViewModelandSetViewBag(int? systemmessageid)
        {
            var systemmessage = systemmessageid.HasValue
                ? SystemMessageBL.GetByID(uow, systemmessageid.Value)
                : null;

            ViewBag.FunctionCodes = getFunctionCodes();

            var vm = SystemMessageViewBL.FromModel(systemmessage);
            return vm;
        }

        private IEnumerable<SystemMessageViewModel> getViewModelAndSetViewBag()
        {
            ViewBag.FunctionCodes = getFunctionCodes();

            var systemmessages = SystemMessageBL.GetAll(uow).OrderByDescending(it => it.StartDate);
            var vm = SystemMessageViewBL.FromModel(systemmessages);
            return vm;
        }
    }
}