﻿using Point.Business.Logic;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Infrastructure.Attributes;
using Point.MVC.WebApplication.Attributes;
using Point.MVC.WebApplication.Controllers;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Point.MVC.WebApplication.Areas.Management.Controllers
{
    [FunctionRoles(FunctionRoleLevelID.Global, FunctionRoleTypeID.ManageAll)]
    public class ScreenController : PointController
    {
        private List<ScreenViewModel> getScreenViewModels(string search = "1")
        {
            var pointuserinfo = PointUserInfo();
            var maxlevel = pointuserinfo.GetFunctionRoleLevelID(FunctionRoleTypeID.ManageAll);
            
            ViewBag.SearchVals = getScreenTypes();
            
            ViewBag.Search = search;

            var screens = maxlevel == FunctionRoleLevelID.Global ? ScreenViewModel.GetViewModelList(ScreenBL.GetModelList(uow)) : new List<ScreenViewModel>();

            switch (search)
            {
                case "2":
                    screens = screens.Where(scr => scr.FormTypeID != null).ToList();
                    break;
                case "3":
                    screens = screens.Where(scr => scr.GeneralActionID != null).ToList();
                    break;
                case "4":
                    screens = screens.Where(scr => !string.IsNullOrEmpty(scr.URL) || !string.IsNullOrEmpty(scr.ScreenTitle)).ToList();
                    break;
            }
            return screens;         
        }

        private List<SimpleObject> getScreenTypes()
        {
            var searchVals = new List<SimpleObject>();

            searchVals.Add(new SimpleObject { ID = 1, Text = "Show all" });
            searchVals.Add(new SimpleObject { ID = 2, Text = "Show form types" });
            searchVals.Add(new SimpleObject { ID = 3, Text = "Show general actions" });
            searchVals.Add(new SimpleObject { ID = 4, Text = "Show screens" });

            return searchVals;
        }

        private ScreenViewModel getEditViewModelandSetViewBag(int? screenId)
        {
            var viewModel = ScreenViewModel.GetViewModel(ScreenBL.GetModelByID(uow, screenId));
            var pointuserinfo = PointUserInfo();

            ////Set static data:
            //var formTypeList = FormTypeBL.GetFormTypes(uow).Where(ft => ft.FormTypeID > 100).Select(ft => new SimpleObject { ID = ft.FormTypeID, Text = ft.Name + " : " + ft.URL }).ToList();
            //formTypeList.Add(new SimpleObject { ID = 0, Text = "- Maak uw keuze - "});
            //ViewBag.FormTypes = formTypeList;

            //var generalActionList = GeneralActionBL.getGeneralActions(uow).Select(ga => new SimpleObject { ID = ga.GeneralActionID, Text = ga.Description + " : " + ga.MethodName }).ToList();
            //generalActionList.Add(new SimpleObject { ID = 0, Text = "- Maak uw keuze - " });
            //ViewBag.GeneralAction = generalActionList;

            return viewModel;
        }

        // GET: Management/Screen
        public ActionResult Index(string search)
        {
            var viewmodel = getScreenViewModels(search);
            return View(viewmodel);
        }

        [HttpGet]
        [ValidateAntiFiddleInjection("ScreenID")]
        public ActionResult Edit(int? screenID)
        {
            return PartialView(getEditViewModelandSetViewBag(screenID));
        }

        public ActionResult Save(ScreenViewModel viewModel)
        {
         //   bool success = false;
            var model = ScreenBL.GetModelByID(uow, viewModel.ScreenID);
            model = ScreenBL.ToModel(viewModel,model);
            ScreenBL.Save(uow, model);

            return RedirectToAction("Index", "Screen");      
        }
    }
}