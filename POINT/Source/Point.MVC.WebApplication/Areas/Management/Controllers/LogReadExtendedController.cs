﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Point.Business.Logic;
using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Infrastructure.Extensions;
using Point.MVC.WebApplication.Controllers;
using Point.MVC.WebApplication.Attributes;
using Point.Database.Extensions;
using System.Threading.Tasks;
using Point.Infrastructure.Attributes;
using Point.Communication.Infrastructure.Helpers;
using Point.Infrastructure.Helpers;

namespace Point.MVC.WebApplication.Areas.Management.Controllers
{
    [FunctionRoles(FunctionRoleLevelID.Region, FunctionRoleTypeID.ManageAll, FunctionRoleTypeID.ManageLogs)]
    [ValidateOrigin]
    public class LogReadExtendedController : PointController
    {
        // GET: Management/LogReadExtended
        public ActionResult Index(LogReadExtendedViewModel model)
        {
            if(model == null)
            {
                model = new LogReadExtendedViewModel();
            }

            var pointuserinfo = PointUserInfo();
                        
            SetViewBags(pointuserinfo, model);
            SetPreviousLogReadReports(pointuserinfo.Employee?.EmployeeID, model);
            return View(model);
        }

        private void SetViewBags(PointUserInfo pointuserinfo, LogReadExtendedViewModel model)
        {
            List<Organization> organizations = new List<Organization>();

            var maxlevel = pointuserinfo.GetFunctionRoleLevelID(FunctionRoleTypeID.ManageLogs);
            if (maxlevel == FunctionRoleLevelID.Region)
            {
                organizations = OrganizationBL.GetActiveByRegionID(uow, pointuserinfo.Region.RegionID).ToList();
            } else if (maxlevel == FunctionRoleLevelID.Global)
            {
                organizations = OrganizationBL.GetActive(uow).ToList();
            }

            ViewBag.Organizations = organizations
                .ToSelectListItems(it => it.Name, it => it.OrganizationID.ToString(), it => it.OrganizationID == model.OrganizationID, " - Maak uw keuze - ")
                .OrderBy(it => it.Text);

            if(model.OrganizationID == null)
            {
                ViewBag.Employees = new List<SelectListItem>() { new SelectListItem() { Text = " - Maak uw keuze - ", Value = "" } };
            } else
            {
                var employees = EmployeeBL.GetByOrganizationID(uow, model.OrganizationID.Value).ToList();

                ViewBag.Employees = employees
                    .ToSelectListItems(it => it.FullName(), it => it.EmployeeID.ToString(), it => it.EmployeeID == 0, " - Maak uw keuze - ")
                    .OrderBy(it => it.Text);
            }
        }

        [HttpPost]
        [FunctionRoles(FunctionRoleLevelID.Region, FunctionRoleTypeID.ManageAll, FunctionRoleTypeID.ManageLogs)]
        public ActionResult GetLogs(LogReadExtendedViewModel model)
        {
            var msg = "";
            var pointuserinfo = PointUserInfo();

            try
            {

                EmailValidation(pointuserinfo);
                DateValidation(model.DateFrom, model.DateTm);
                OptionalFieldsValidation(model.TransferID, model.BSN, model.EmployeeID);
                
                Task.Run(async () => { await CreateReportAndSend(pointuserinfo, model); });
                                
                msg = "Uw verzoek is verstuurd.";
                msg += "\n" + "U krijgt binnen enkele minuten een e-mail met de status van uw verzoek.";
                model.Message = msg;
                model.ReportCommandSent = true;

                SetViewBags(pointuserinfo, model);
            }
            catch (MissingFieldException ex)
            {
                SetViewBags(pointuserinfo, model);
                model.Message = ex.Message;
                model.ReportCommandSent = false;
            }

            return View("Index", model);
        }
        
        private void DateValidation(DateTime dtFrom, DateTime dtTo)
        {
            if ((dtFrom == null || dtFrom == DateTime.MinValue) || (dtTo == null || dtTo == DateTime.MinValue))
            {
                throw new MissingFieldException("Datum van / tot en met moet gevuld zijn.");
            }

            if (dtTo < dtFrom)
            {
                throw new MissingFieldException("Datum tot en met moet groter of gelijk zijn aan Datum van.");
            }

            if ((dtTo - dtFrom).Days >14)
            {
                throw new MissingFieldException("Om het aantal logging regels werkbaar te houden, is het maximale aantal dagen op 14 ingesteld.");
            }
        }

        //minimum, one of the following parameters must be filled.
        private void OptionalFieldsValidation(int? transferID, string BSN, int? employeeID)
        {
            if (transferID == null && string.IsNullOrEmpty(BSN) && employeeID == null)
            {
                throw new MissingFieldException("Naast de van/tot en met datums, moet tenminste 1 van de volgende velden ook zijn ingevuld: TransferID, BSN OF Organisatie/Medewerker.");
            }
        }

        private void EmailValidation(PointUserInfo pointuser)
        {
            if (string.IsNullOrEmpty(pointuser?.Employee?.EmailAddress))
            {
                throw new MissingFieldException("Uw emailadres is niet bekend. Ga naar \"Accountgegevens aanpassen\"");
            }
        }

        private async Task CreateReportAndSend(PointUserInfo userInfo, LogReadExtendedViewModel model)
        {
            var errornumber = 0;
            var userId = userInfo.Employee.EmployeeID;
            try
            {
                await LogReadCSVFileBL.CreateExtendedReport(userInfo, model);
            }
            catch (Exception ex)
            {
                //errornumber = Infrastructure.ExceptionHelper.SendMessageException(ex, employeeid: userId);
                Logger.Error(GetLogParameters(ex.Message, ex));

            }
            finally
            {
                SendEmails(userId, errornumber);
            }
        }

        private void SendEmails(int userId, int errorNumber)
        {
            if (errorNumber > 0)
            {
                AsyncReportMail.SendErrorEmail(userId, "Uigebreide leesactie misluk. Error number: " + errorNumber.ToString(), TemplateTypeID.ErrorLogReadMail);
            }
            else
            {
                string downloadLink = ConfigHelper.GetAppSettingByName<string>("WebServer") + "/Management/LogReadExtended";
                AsyncReportMail.SendAsyncReportMail(userId, downloadLink, "Logactie aangevraagd", TemplateTypeID.LogReadMail);
            }
        }

        private void SetPreviousLogReadReports(int? employeeId, LogReadExtendedViewModel model)
        {
            if(employeeId == null)
            {
                return;
            }

            var logReadReports = TransferAttachmentViewBL.GetByEmployeeID(uow, employeeId.Value, AttachmentTypeID.LogRead).ToList();
            model.LogReadReportList = logReadReports;
        }

        [FunctionRoles(FunctionRoleTypeID.ManageAll,FunctionRoleTypeID.ManageRegion)]
        [ValidateAntiFiddleInjection("AttachmentID")]
        public FileResult DownloadLogRead(int AttachmentID)
        {
            var pointuserinfo = PointUserInfo();
            var path = LogReadCSVFileBL.DownloadLogRead(uow, pointuserinfo.Employee.EmployeeID, AttachmentID);
            if (string.IsNullOrEmpty(path))
            {
                throw new Exception("Rapportage mislukt.");
            }

            return File(path, "text/csv", System.IO.Path.GetFileName(path));
        }
    }
}