﻿using DevTrends.MvcDonutCaching;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Infrastructure.Attributes;
using Point.MVC.WebApplication.Attributes;
using Point.MVC.WebApplication.Controllers;
using System.Web.Mvc;
using Point.MVC.WebApplication.Helpers;

namespace Point.MVC.WebApplication.Areas.Management.Controllers
{
    [DonutOutputCache(Duration = 0)]
    public class OrganizationController : PointController
    {
        [FunctionRoles(FunctionRoleTypeID.ManageAll, FunctionRoleTypeID.ManageOrganization)]
        [ValidateOrigin]
        public ActionResult DepartmentReport()
        {
            return View();
        }

        [FunctionRoles(FunctionRoleTypeID.ManageAll, FunctionRoleTypeID.ManageOrganization)]
        public ActionResult Index(string search, bool searchDeleted = false, bool onlyOrganizations = false)
        {
            var viewmodel = OrganizationManagementViewModelHelper.GetIndexViewModel(this, search, searchDeleted, onlyOrganizations);
            return View(viewmodel);
        }

        [FunctionRoles(FunctionRoleTypeID.ManageAll, FunctionRoleTypeID.ManageOrganization)]
        public PartialViewResult List(string search, bool searchDeleted = false, bool onlyOrganizations = false)
        {
            var viewmodel = OrganizationManagementViewModelHelper.GetListViewModel(this, search, searchDeleted, onlyOrganizations);
            return PartialView(viewmodel);
        }

        [HttpGet]
        [FunctionRoles(FunctionRoleLevelID.Organization, FunctionRoleTypeID.ManageAll, FunctionRoleTypeID.ManageOrganization)]
        public ActionResult Create()
        {
            var viewmodel = OrganizationManagementViewModelHelper.GetOrganizationEditViewModel(this, null);
            return PartialView("Edit", viewmodel);
        }

        [HttpGet]
        [FunctionRoles(FunctionRoleLevelID.Organization, FunctionRoleTypeID.ManageAll, FunctionRoleTypeID.ManageOrganization)]
        [ValidateAntiFiddleInjection("OrganizationID")]
        public ActionResult Edit(int organizationID)
        {
            var viewmodel = OrganizationManagementViewModelHelper.GetOrganizationEditViewModel(this, organizationID);
            return PartialView(viewmodel);
        }

        [HttpPost]
        [FunctionRoles(FunctionRoleLevelID.Organization, FunctionRoleTypeID.ManageAll, FunctionRoleTypeID.ManageOrganization)]
        [ValidateAntiForgeryToken]
        [ValidateAntiFiddleInjection("OrganizationID")]
        public ActionResult Save(OrganizationViewModel viewModel)
        {
            var jsonObject = OrganizationManagementViewModelHelper.Save(this, viewModel);
            return Json(jsonObject);
        }
        
        [ValidateAntiFiddleInjection("OrganizationID")]
        [FunctionRoles(FunctionRoleTypeID.ManageAll, FunctionRoleTypeID.ManageOrganization, FunctionRoleTypeID.ManageOrganization)]
        public ActionResult UpdateOrganizationLogo(int organizationID)
        {
            var jsonObject = OrganizationManagementViewModelHelper.UpdateOrganizationLogo(this, organizationID);
            return Json(jsonObject);
        }

        [ValidateAntiFiddleInjection("OrganizationID")]
        [FunctionRoles(FunctionRoleTypeID.ManageAll, FunctionRoleTypeID.ManageOrganization, FunctionRoleTypeID.ManageOrganization)]
        public ActionResult EditOrganizationLogo(int organizationID)
        {
            return View();
        }

        [ValidateAntiFiddleInjection("OrganizationID")]
        [FunctionRoles(FunctionRoleTypeID.ManageAll, FunctionRoleTypeID.ManageOrganization)]
        public JsonResult DeleteOrganizationLogo(int organizationID)
        {
            var jsonObject = OrganizationManagementViewModelHelper.DeleteOrganizationLogo(this, organizationID);
            return Json(jsonObject, JsonRequestBehavior.AllowGet);
        }

    }
}