﻿using Point.Business.Helper;
using Point.Business.Logic;
using Point.Business.Logic.TemplateDataSets;
using Point.Communication;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Infrastructure.Attributes;
using Point.MVC.WebApplication.Attributes;
using Point.MVC.WebApplication.Controllers;
using System.Web.Mvc;

namespace Point.MVC.WebApplication.Areas.Management.Controllers
{
    [FunctionRoles(FunctionRoleLevelID.Global, FunctionRoleTypeID.ManageAll)]
    [ValidateOrigin]
    public class TemplateController : PointFormController
    {
        public ActionResult Index(TemplateTypeID templatetypeid = TemplateTypeID.ZorgMailZHVVTJa, int? organizationid = null)
        {
            return View(TemplateViewBL.Get(uow, templatetypeid, organizationid));
        }

        [HttpPost]
        [ValidateAntiFiddleInjection("TemplateID")]
        [ValidateInput(false)]
        public ActionResult Save(TemplateViewModel templateviewmodel)
        {
            if (templateviewmodel.OrganizationID == null)
            {
                var templatedefault = TemplateDefaultBL.FromViewModel(uow, templateviewmodel);
                TemplateDefaultBL.Save(uow, templatedefault);
            }
            else
            {
                var templatedefault = TemplateDefaultBL.GetByTemplateTypeID(uow, (int)templateviewmodel.TemplateTypeID);
                var template = TemplateBL.FromModel(uow, templateviewmodel);
                if (templateviewmodel.TemplateID > 0)
                {
                    if (string.IsNullOrWhiteSpace(template.TemplateText) || template.TemplateText == templatedefault.TemplateDefaultText)
                    {
                        TemplateBL.Delete(uow, template.TemplateID);
                    }
                    else
                    {
                        TemplateBL.Save(uow, template);
                    }
                }
                else
                {
                    if (template.TemplateText != templatedefault.TemplateDefaultText)
                    {
                        TemplateBL.Save(uow, template);
                    }
                }
            }

            return Json(new
            {
                Success = true,
                Url = Url.Action("Index", ControllerName, new
                {
                    TemplateTypeID = (int)templateviewmodel.TemplateTypeID,
                    OrganizationID = templateviewmodel.OrganizationID
                })
            });
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Example(TemplateExampleViewModel templatezorgmailexampleviewmodel)
        {
            string example = "Er zijn geen gegevens gevonden";

            if (templatezorgmailexampleviewmodel == null) return Content(example, "text/plain");

            var pointuserinfo = PointUserInfo();

            switch ((TemplateTypeID)templatezorgmailexampleviewmodel.TemplateTypeID)
            {
                case TemplateTypeID.ZorgMailZHVVTJa:
                case TemplateTypeID.ZorgMailHAVVTJa:
                case TemplateTypeID.ZorgMailHAVVTNee:
                case TemplateTypeID.ZorgMailHAVVTNeeEnNieuweVVT:
                    var noticedoctor = new NoticeDoctor(templatezorgmailexampleviewmodel.TransferID, pointuserinfo.Employee.EmployeeID);
                    example = noticedoctor.GetExampleNoticeDG(uow, pointuserinfo, example: templatezorgmailexampleviewmodel.TemplateText);
                    break;

                case TemplateTypeID.PatientenBrief :
                    var flowinstance = FlowInstanceBL.GetByTransferID(uow, templatezorgmailexampleviewmodel.TransferID);
                    if (flowinstance != null)
                    {
                        var patientenbrief = new PatientenBrief(uow, flowinstance, pointuserinfo);
                        var dataset = patientenbrief.Get();
                        var templateparser = new TemplateParser(templatezorgmailexampleviewmodel.TemplateText, dataset);
                        example = templateparser.Parse();
                    }
                    break;
            }

            return Content(example, "text/plain");
        }

        [HttpPost]
        [ValidateAntiFiddleInjection("TemplateID")]
        [ValidateInput(false)]
        public ActionResult Delete(TemplateViewModel templateviewmodel)
        {
            if (templateviewmodel.TemplateID > 0)
            {
                TemplateBL.Delete(uow, templateviewmodel.TemplateID);
            }

            return Json(new
            {
                Success = true,
                Url = Url.Action("Index", ControllerName, new
                {
                    TemplateTypeID = (int)templateviewmodel.TemplateTypeID,
                    OrganizationID = templateviewmodel.OrganizationID
                })
            });
        }
    }
}