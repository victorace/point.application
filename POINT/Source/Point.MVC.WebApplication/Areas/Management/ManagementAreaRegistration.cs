﻿using System.Web.Mvc;

namespace Point.MVC.WebApplication.Areas.Management
{
    public class ManagementAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Management";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            // http://haacked.com/archive/2010/01/12/ambiguous-controller-names.aspx/
            context.MapRoute(
                "Management_default",
                "Management/{controller}/{action}/{id}",
                new { controller = "ManagementHome", action = "Index", id = UrlParameter.Optional },
                new[] { "Point.MVC.WebApplication.Areas.Management.Controllers" }
            );
        }
    }
}