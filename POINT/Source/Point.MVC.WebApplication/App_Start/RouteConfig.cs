﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Point.MVC.WebApplication
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "DefaultLegacy",
                "Default.aspx",
                new { controller = "Transfer", action = "Index" }
                );

            routes.MapRoute(
                "TransferLegacy",
                "Transfer/Transfers.aspx",
                new { controller = "Transfer", action = "Index" }
                );

            routes.MapRoute(
                "TransferNull",
                "Transfer/Null",
                new { controller = "Transfer", action = "Index" }
                );

            routes.MapRoute(
                "LoginLegacy",
                "Security/Login.aspx",
                new { controller = "Login", action = "SSO" } 
                );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Transfer", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "Point.MVC.WebApplication.Controllers" }
            );
        }
    }
    
}