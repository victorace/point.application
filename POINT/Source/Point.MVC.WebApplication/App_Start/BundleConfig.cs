﻿using Point.MVC.Bundling;
using System.IO;
using System.Web;
using System.Web.Optimization;

namespace Point.MVC.WebApplication
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptWithSourceMapBundle("~/Bundles/Scripts/PointLogin").Include(
                "~/Scripts/Point.Master.pseudo.js",
                "~/Scripts/Point.MasterBootstrap.js",
                "~/Scripts/jQuery/jquery-1.11.2.js",
                "~/Scripts/Bootstrap/bootstrap.js",
                "~/Scripts/Bootstrap/IE8Compat.js",
                "~/Scripts/PointDefaultLayoutBootstrap.js",
                "~/Scripts/MVC/Shared/Modal.js"
            ));

            bundles.Add(new ScriptWithSourceMapBundle("~/Bundles/Scripts/PointDefault").Include(
                "~/Scripts/Point.Master.pseudo.js",
                "~/Scripts/Point.MasterBootstrap.js",
                "~/Scripts/jQuery/jquery-1.11.2.js",
                "~/Scripts/Bootstrap/bootstrap.js",
                "~/Scripts/Bootstrap/moment.js",
                "~/Scripts/Bootstrap/locales/nl.js",
                "~/Scripts/Bootstrap/IE8Compat.js",
                "~/Scripts/Bootstrap/bootstrap-datetimepicker.js",
                "~/Scripts/Bootstrap/bootstrap-clickover.js",
                "~/Scripts/PointDefaultLayoutBootstrap.js",
                "~/Scripts/jQuery/jquery.dataTables.js",
                "~/Scripts/Bootstrap/dataTables.bootstrap.js",
                "~/Scripts/jQuery/jquery.dataTables.defaults.js",
                "~/Scripts/jQuery/jquery.dataTables.datetime-moment.js",
                "~/Scripts/MVC/Shared/PointAreYouSure.js",
                "~/Scripts/MVC/Shared/PointSignalering.js",
                "~/Scripts/MVC/Shared/PageLock.js",
                "~/Scripts/MVC/Shared/Modal.js"
            ));

            bundles.Add(new ScriptWithSourceMapBundle("~/Bundles/Scripts/FormType").Include(
                "~/Scripts/date.js",
                "~/Scripts/date.extend.js",
                "~/Scripts/jquery/jquery.validate.js",
                "~/Scripts/jquery/jquery.validate.bsn.js",
                "~/Scripts/jquery/jquery.validate.unobtrusive.js",
                "~/Scripts/MVC/Shared/Global.js",                
                "~/Scripts/MVC/Shared/OrganizationSearch.js",
                "~/Scripts/MVC/Shared/AfterCare.js",
                "~/Scripts/MVC/Shared/ChangeEvent.js",
                "~/Scripts/MVC/Shared/ToggleChildren.js",
                "~/Scripts/MVC/Shared/Validation.js",
                "~/Scripts/MVC/Shared/DbDrivenValidation.js",
                "~/Scripts/MVC/Shared/EPDConnection.js"
            ));

            bundles.Add(new ScriptWithSourceMapBundle("~/Bundles/Scripts/BootstrapIE8").Include(
                "~/Scripts/Bootstrap/html5shiv.js",
                "~/Scripts/Bootstrap/respond.js"
            ));

            bundles.Add(new ScriptWithSourceMapBundle("~/Bundles/Scripts/SystemMessage").Include(
                "~/Scripts/js.cookie.js",
                "~/Scripts/SystemMessageV2.js"
            ));

            bundles.Add(new ScriptWithSourceMapBundle("~/Bundles/Scripts/JQueryForm").Include(
                "~/Scripts/jQuery/jquery.form.js"
            ));

            bundles.Add(new ScriptWithSourceMapBundle("~/Bundles/Scripts/JQueryValidation").Include(
            "~/Scripts/jQuery/jquery.validate.js",
            "~/Scripts/jQuery/jquery.validate.unobtrusive.js",
            "~/Scripts/MVC/Shared/Validation.js"
            ));

            bundles.Add(new ScriptWithSourceMapBundle("~/Bundles/Scripts/GlobalPrint").Include(
                "~/Scripts/MVC/Shared/Global.js",
                "~/Scripts/MVC/Print.js"
            ));

            bundles.Add(new ScriptWithSourceMapBundle("~/Bundles/Scripts/GlobalDashboard").Include(
                "~/Scripts/MVC/Shared/Global.js",
                "~/Scripts/MVC/Dashboard.js"
            ));

            bundles.Add(new ScriptWithSourceMapBundle("~/Bundles/Scripts/SearchMultiselect").Include(
                "~/Scripts/MVC/Shared/Global.js",
                "~/Scripts/Bootstrap/bootstrap-multiselect.js",
                "~/Scripts/MVC/SearchMultiselectInit.js",
                "~/Scripts/jQuery/dataTables.fixedColumns.js"
            ));



            bundles.Add(new StyleBundle("~/Bundles/Styles/capacity")
                .Include("~/Content/Admin/_Linked/capacity-tile-list.css", new CssRewriteUrlTransform())
                .Include("~/Content/bootstrap-multiselect-point.css", new CssRewriteUrlTransform()));

            bundles.Add(new ScriptBundle("~/Bundles/Scripts/capacity")
                .Include("~/Scripts/Bootstrap/bootstrap-multiselect.js")
                .Include("~/Scripts/MVC/Management/_Linked/CapMan.js")
            );


            foreach (var file in Directory.GetFiles(HttpContext.Current.Server.MapPath("~/Scripts/MVC"),"*.js"))
            {
                bundles.Add(new ScriptWithSourceMapBundle("~/Bundles/Scripts/" + Path.GetFileNameWithoutExtension(file))
                .Include(
                    string.Concat("~/Scripts/MVC/", Path.GetFileName(file))
                ));
            }

            foreach (var file in Directory.GetFiles(HttpContext.Current.Server.MapPath("~/Scripts/MVC/Management"), "*.js"))
            {
                bundles.Add(new ScriptWithSourceMapBundle("~/Bundles/Scripts/Management/" + Path.GetFileNameWithoutExtension(file)).Include(
                    string.Concat("~/Scripts/MVC/Management/", Path.GetFileName(file))
                ));
            }

            bundles.Add(new StyleBundle("~/Bundles/Styles/PointDefault")
                .Include("~/Content/Bootstrap-Buro26/bootstrap.css", new CssRewriteUrlTransform())
                .Include("~/Content/Print.css", new CssRewriteUrlTransform())
                .Include("~/Content/bootstrap-datetimepicker.css", new CssRewriteUrlTransform())
                .Include("~/Content/dataTables.bootstrap.css", new CssRewriteUrlTransform())
            );

            //BundleTable.EnableOptimizations = true; // << set to true / false to quickly enable/disable it and overrule default from 'compilation debug'
        }
    }
}