﻿using Point.Database.Context;
using Point.Infrastructure;
using Point.Infrastructure.Extensions;
using Point.Infrastructure.Helpers;
using Point.Log.Context;
using System;
using System.Data.Entity.Infrastructure.Interception;
using System.Web;
using System.Web.Helpers;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Net;
using Point.Infrastructure.Constants;
using Point.Log4Net;

namespace Point.MVC.WebApplication
{

    public class MvcApplication : HttpApplication
    {
        public static readonly ILogger Logger = LogManager.GetLogger(typeof(MvcApplication));
        protected void Application_Error(object sender, EventArgs e)
        {
            var logError = true;
            var exception = Server.GetLastError();
            if (exception is HttpRequestValidationException && Request.IsAjaxRequest())
            {
                var json = Json.Encode(new
                {
                    ShowErrors = true,
                    ValidationErrors = new[]
                    {
                        "Validatie is mislukt op 1 of meerdere velden. " +
                        "Controleer uw formulier op abnormale of onveilige tekens. " +
                        "Verwijder in ieder geval alle < en > tekens."
                    }
                });
                Response.Clear();
                Response.ContentType = "application/json; charset=utf-8";
                Response.Write(json);
                Response.End();

                Server.ClearError();
            }

            //handle invalid urls (invalid control or action name)
            HttpException httpException = exception as HttpException;
            if (httpException != null)
            {
                int httpCode = httpException.GetHttpCode();
                if (httpCode == (int)HttpStatusCode.NotFound)
                {
                    Response.Redirect($"~/Error/NotFound");
                    logError = false;
                    //Instead of redirecting to NotFound, we can use following code. but its less beautiful.
                    //Response.Clear();
                    //Response.Write("<HTML><BODY style='font-family:Verdana'><H1>Page not found.</H1><H6 style='color:LightGray'>De door u gezochte pagina wordt niet gevonden.</H6></BODY></HTML>");
                    //Response.End();
                }
            }

            if (!HttpContext.Current.IsDebuggingEnabled)
            {
                Response.Clear();
                if (Request.IsAjaxRequest())
                {
                    Response.ContentType = "application/json; charset=utf-8";
                    Response.Write("Er is een onvoorziene fout opgetreden in POINT." + Server.HtmlEncode(exception.Message));
                }
                else
                {
                    Response.Write("<HTML><BODY style='font-family:Verdana'><H1>Er is een onvoorziene fout opgetreden in POINT</H1><H6 style='color:LightGray'>" + Server.HtmlEncode(exception?.Message )+ "</H6></BODY></HTML>");
                }
                Response.End();                           
            }

            if (logError)
            {
                Logger.Error(new LogParameters { Exception = exception, Message = "Unexpected exception occurred in Point.MVC.WebApplication." });
            }

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
            if (Request.IsAuthenticated == false)
            {
                var authOrganizationID = Request.GetRequestVariable<int?>("AuthOrganizationID", null);
                if (authOrganizationID.HasValue)
                {
                    var authwebserver = ConfigHelper.GetAppSettingByName("WebServerAuth", "");
                    var uriBuilder = new UriBuilder(authwebserver);

                    var query = HttpUtility.ParseQueryString(uriBuilder.Query);
                    query.Add("OrganizationID", authOrganizationID.ToString());
                    query.Add("ReturnUrl", Request.Url.AbsoluteUri);
                    uriBuilder.Query = query.ToString();

                    Response.Redirect(uriBuilder.ToString());
                }
            }
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            Utils.ClearHeaders(Response);
        }

        protected void Session_Start(Object sender, EventArgs e)
        {
            //Dummy entry to fix session is always new see https://forums.asp.net/t/1520827.aspx
            Session[SessionIdentifiers.Point.SessionID] = Session.SessionID;
        }

        protected void Application_Start()
        {
            Business.Pdf.GeneratePDF.SetLicense();


            var useReadUncommited = ConfigHelper.GetAppSettingByName<bool>("UseReadUncommited");

            DbInterception.Add(new IsolationLevelInterceptor(useReadUncommited));

            System.Data.Entity.Database.SetInitializer<PointLogContext>(null);
            System.Data.Entity.Database.SetInitializer<PointContext>(null);
            System.Data.Entity.Database.SetInitializer<PointSearchContext>(null);

            DataAnnotationsModelValidatorProvider.AddImplicitRequiredAttributeForValueTypes = false;

            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }



    }
}