﻿using System.Linq;
using System.Web.Mvc;
using Point.Business.Helper;
using Point.Business.Logic;
using Point.Database.Models;
using Point.Infrastructure.Caching;
using Point.Infrastructure.Exceptions;
using Point.Infrastructure.Extensions;
using Point.Models.Enums;
using Point.MVC.WebApplication.Controllers;

namespace Point.MVC.WebApplication.Attributes
{
    public enum SecurityAttribute
    {
        TransferID,
        DepartmentID,
        LocationID,
        OrganizationID,
        RegionID
    }

    public class FormSecurity : ActionFilterAttribute
    {
        private FunctionRoleTypeID? FunctionRoleTypeID { get; set; }
        private SecurityAttribute[] SecurityAttributes { get; set; }

        public FormSecurity(FunctionRoleTypeID functionRoleTypeID, params SecurityAttribute[] securityAttributes)
        {
            this.FunctionRoleTypeID = functionRoleTypeID;
            this.SecurityAttributes = securityAttributes;
        }

        public FormSecurity(params SecurityAttribute[] securityAttributes)
        {
            this.SecurityAttributes = securityAttributes;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var controller = (PointBaseController)filterContext.Controller;
            if (controller == null)
            {
                throw new PointSecurityException("Deze functie word alleen ondersteund op PointBaseControllers", ExceptionType.Default);
            }

            var userinfo = controller.PointUserInfo();
            if (userinfo.IsGlobalAdmin)
            {
                return;
            }

            var request = filterContext.HttpContext.Request;
            var maxlevel = FunctionRoleTypeID.HasValue 
                ? userinfo.GetFunctionRoleLevelID(FunctionRoleTypeID.Value) 
                : FunctionRoleLevelID.None;

            foreach (var securityattribute in SecurityAttributes)
            {
                switch (securityattribute)
                {
                    case SecurityAttribute.DepartmentID:
                        var departmentid = request.GetRequestVariable<int?>("departmentid", null);
                        if (departmentid == null) continue;

                        if (PointUserInfoHelper.IsDepartmentIDsValid(controller.uow, userinfo, maxlevel, new int[] { departmentid.Value }) == false)
                        {
                            throw new PointSecurityException("U heeft geen toegang tot deze afdeling", ExceptionType.AccessDenied);
                        }

                        break;
                    case SecurityAttribute.LocationID:
                        var locationid = request.GetRequestVariable<int?>("locationid", null);
                        if (locationid == null) continue;

                        if (PointUserInfoHelper.IsLocationIDsValid(controller.uow, userinfo, maxlevel, new int[] { locationid.Value }) == false)
                        {
                            throw new PointSecurityException("U heeft geen toegang tot deze locatie", ExceptionType.AccessDenied);
                        }

                        break;
                    case SecurityAttribute.OrganizationID:
                        var organizationid = request.GetRequestVariable<int?>("organizationid", null);
                        if (organizationid == null) continue;

                        if (PointUserInfoHelper.IsOrganizationIDsValid(controller.uow, userinfo, maxlevel, new int[] { organizationid.Value }) == false)
                        {
                            throw new PointSecurityException("U heeft geen toegang tot deze organisatie", ExceptionType.AccessDenied);
                        }

                        break;
                    case SecurityAttribute.RegionID:
                        var regionid = request.GetRequestVariable<int?>("regionid", null);
                        if (regionid == null) continue;

                        if (PointUserInfoHelper.IsRegionIDValid(controller.uow, userinfo, maxlevel, regionid.Value) == false)
                        {
                            throw new PointSecurityException("U heeft geen toegang tot deze regio", ExceptionType.AccessDenied);
                        }

                        break;
                    case SecurityAttribute.TransferID:
                        var transferID = request.GetRequestVariable<int?>("transferid", null);
                        if (transferID.HasValue)
                        {
                            var cachestring = string.Concat("currentFlowInstance_", userinfo.Employee.UserID, "_", transferID);
                            var flowinstance = CacheService.Instance.Get<FlowInstance>(cachestring);
                            if (flowinstance == null)
                            {
                                flowinstance = SearchBL.GetSecurityTrimmedFlowInstance(controller.uow, userinfo, transferID.Value);
                                if (flowinstance != null)
                                {
                                    CacheService.Instance.Insert(cachestring, flowinstance, "CacheShort");
                                }
                                else
                                {
                                    throw new PointSecurityException("U heeft geen toegang tot dit dossier", ExceptionType.AccessDenied);
                                }
                            }
                        }
                        break;
                }

            }
        }
    }
}