﻿using System.Linq;
using System.Web.Mvc;
using Point.Business.Helper;
using Point.Business.Logic;
using Point.Database.Models;
using Point.Infrastructure.Caching;
using Point.Infrastructure.Exceptions;
using Point.Infrastructure.Extensions;
using Point.Infrastructure.Helpers;
using Point.Models.Enums;
using Point.MVC.WebApplication.Controllers;

namespace Point.MVC.WebApplication.Attributes
{
    public class ValidateOriginAttribute : ActionFilterAttribute
    {
        private bool PostOnly { get; set; }

        public ValidateOriginAttribute(bool postOnly = true)
        {
            PostOnly = postOnly;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (PostOnly == true && filterContext.HttpContext.Request.HttpMethod != "POST")
            {
                return;
            }

            ReferrerHelper.CheckReferrer(filterContext.HttpContext.Request);
        }
    }
}