﻿using System;
using System.Web.Mvc;

namespace Point.MVC.WebApplication.Extensions
{
    public static class ControllerExtensions
    {
        public static ViewResult MasterView(this Controller controller)
        {
            return MasterView(controller, null, null);
        }

        public static ViewResult MasterView(this Controller controller, object model)
        {
            return MasterView(controller, null, model);
        }

        public static ViewResult MasterView(this Controller controller, string viewName)
        {
            return MasterView(controller, viewName, null);
        }

        public static ViewResult MasterView(this Controller controller, string viewName, object model)
        {
            if (model != null)
                controller.ViewData.Model = model;

            controller.ViewBag._ViewName = GetViewName(controller, viewName);

            return new ViewResult
            {
                ViewName = "MasterView", 
                ViewData = controller.ViewData,
                TempData = controller.TempData
            };
        }

        static string GetViewName(Controller controller, string viewName)
        {
            return !String.IsNullOrEmpty(viewName)
                ? viewName
                : controller.RouteData.GetRequiredString("action");
        }

        public static string RenderViewToString(this Controller controller, string viewName, object model, bool onlyPartial = false, string mastername = "_PointDefaultLayoutPrint")
        {
            controller.ViewData.Model = model;
            using (var stringwriter = new System.IO.StringWriter())
            {

                ViewEngineResult viewResult = null;
                if (onlyPartial)
                    viewResult = ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName);
                else
                    viewResult = ViewEngines.Engines.FindView(controller.ControllerContext, viewName, mastername);

                var viewContext = new ViewContext(controller.ControllerContext, viewResult.View,
                                             controller.ViewData, controller.TempData, stringwriter);

                viewResult.View.Render(viewContext, stringwriter);
                viewResult.ViewEngine.ReleaseView(controller.ControllerContext, viewResult.View);
                return stringwriter.GetStringBuilder().ToString();
            }
        }

    }
}