﻿using System.Web.Mvc;
using Point.Infrastructure.Extensions;

namespace Point.MVC.WebApplication.Extensions
{
    public static class ViewDataExtensions
    {
        public static void SetReadMode(this ViewDataDictionary viewData, bool readMode = true)
        {
            if (viewData["ReadMode"] != null)
            {
                viewData.Remove("ReadMode");
            }

            viewData.Add("ReadMode", readMode);
        }

        public static bool IsReadMode(this ViewDataDictionary viewData)
        {
            if (viewData == null || viewData["ReadMode"] == null)
            {
                return false;
            }

            return viewData["ReadMode"].ConvertTo<bool>();
        }

        public static void SetPageLock(this ViewDataDictionary viewData, int pagelocktimer, bool pageLocked = true, int? pageLockID = null)
        {
            if (viewData["HasPageLocking"] != null)
            {
                viewData.Remove("HasPageLocking");
            }

            if (viewData["PageLockTimer"] != null)
            {
                viewData.Remove("PageLockTimer");
            }

            if (viewData["PageLock"] != null)
            {
                viewData.Remove("PageLock");
            }

            if (viewData["PageLockID"] != null)
            {
                viewData.Remove("PageLockID");
            }

            viewData.Add("HasPageLocking", true);
            viewData.Add("PageLockTimer", pagelocktimer);
            viewData.Add("PageLock", pageLocked);

            if(pageLocked == true)
            {
                SetReadMode(viewData);
            }

            if (pageLockID.HasValue)
            {
                viewData.Add("PageLockID", pageLockID);
            }
        }

        public static bool IsPageLock(this ViewDataDictionary viewData)
        {
            if (viewData == null || viewData["PageLock"] == null)
            {
                return false;
            }

            return viewData["PageLock"].ConvertTo<bool>();
        }

        public static bool HasPageLocking(this ViewDataDictionary viewData)
        {
            if (viewData == null || viewData["HasPageLocking"] == null)
            {
                return false;
            }

            return viewData["HasPageLocking"].ConvertTo<bool>();
        }

        public static int PageLockTimer(this ViewDataDictionary viewData)
        {
            if (viewData == null || viewData["PageLockTimer"] == null)
            {
                return 0;
            }

            return viewData["PageLockTimer"].ConvertTo<int>();
        }

        public static void SetIsClosed(this ViewDataDictionary viewData, bool isClosed = true)
        {
            if (viewData["IsClosed"] != null)
            {
                viewData.Remove("IsClosed");
            }

            viewData.Add("IsClosed", isClosed);
        }

        public static bool IsClosed(this ViewDataDictionary viewData)
        {
            if (viewData == null || viewData["IsClosed"] == null)
            {
                return false;
            }

            return viewData["IsClosed"].ConvertTo<bool>();
        }

        public static void SetIsMFAScreen(this ViewDataDictionary viewData, bool ismfascreen = true)
        {
            if (viewData["IsMFAScreen"] != null)
            {
                viewData.Remove("IsMFAScreen");
            }

            viewData.Add("IsMFAScreen", ismfascreen);
        }

        public static bool IsMFAScreen(this ViewDataDictionary viewData)
        {
            if (viewData == null || viewData["IsMFAScreen"] == null)
            {
                return false;
            }

            return viewData["IsMFAScreen"].ConvertTo<bool>();
        }
    }
}