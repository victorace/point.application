﻿using Point.Business.Helper;
using Point.Database.Extensions;
using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Infrastructure.Extensions;
using Point.Infrastructure.Helpers;
using Point.MVC.WebApplication.Controllers;
using Point.MVC.WebApplication.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;

namespace Point.MVC.WebApplication.Extensions
{
    public static class HtmlHelperExtensions
    {
        // how can this be used?
        // ModelMetadata metadata = ModelMetadata.FromLambdaExpression<FlowWebValueCollection, string>(
        //  x => x.Values[index].Name, new ViewDataDictionary<FlowWebValueCollection>(html.ViewData));

        // http://stackoverflow.com/questions/10654925/preserving-state-in-an-extension-method

        public static MvcHtmlString PrintFormSetVersionButton(this HtmlHelper htmlHelper, int transferID, int formsetVersionID, string title = "Print")
        {

            if (transferID <= 0 || formsetVersionID <= 0)
                return new MvcHtmlString("<!-- no transferid or formsetversionid -->");

            TagBuilder tagBuilder = new TagBuilder("a");

            PointController basecontroller = htmlHelper.ViewContext.Controller as PointController;
            string action = basecontroller.RouteData.Values["action"].ToString();
            string cont = basecontroller.RouteData.Values["controller"].ToString();

            var routevaluedict = new RouteValueDictionary();
            routevaluedict.Add("TransferID", transferID);
            routevaluedict.Add("formsetVersionID", formsetVersionID);
            routevaluedict.Add("PrintMode", true);

            tagBuilder.Attributes["name"] = "printbutton";
            tagBuilder.Attributes["href"] = System.Web.Mvc.UrlHelper.GenerateUrl("", "PrintSingle", "PrintAction", routevaluedict, htmlHelper.RouteCollection, htmlHelper.ViewContext.RequestContext, true);
            tagBuilder.AddCssClass("btn btn-default");
            tagBuilder.InnerHtml = "<span class=\"glyphicon glyphicon-print\"></span> " + title;

            return MvcHtmlString.Create(tagBuilder.ToString(TagRenderMode.Normal));
        }

        public static MvcHtmlString FlowLabel(this HtmlHelper htmlHelper, IEnumerable<FlowWebField> flowFields, string fieldName,
            object htmlAttributes = null, BootstrapSize labelSize = null, bool hasInformationIcon = false, string overrideText = null)
        {
            //var flowWebField = flowFields.Where(f => f.Name == fieldName && f.Type != FlowFieldType.Checkbox).FirstOrDefault();
            var flowWebField = flowFields.FirstOrDefault(f => f.Name == fieldName && f.Visible);
            if (flowWebField == null)
            {
                return MvcHtmlString.Empty;
            }

            RouteValueDictionary routeValueDictionary = HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes);

            TagBuilder tagBuilder = new TagBuilder("label");
            tagBuilder.Attributes["for"] = FlowControlCreator.GenerateProtectedName(flowWebField);
            //Via parameter breedte?

            tagBuilder.AddCssClass("control-label");
            if ((labelSize?.Col_xs).HasValue)
            {
                tagBuilder.AddCssClass("col-xs-" + labelSize.Col_xs.Value);
            }
            if ((labelSize?.Col_sm).HasValue)
            {
                tagBuilder.AddCssClass("col-sm-" + labelSize.Col_sm.Value);
            }
            if ((labelSize?.Col_md).HasValue)
            {
                tagBuilder.AddCssClass("col-md-" + labelSize.Col_md.Value);
            }
            if ((labelSize?.Col_lg).HasValue)
            {
                tagBuilder.AddCssClass("col-lg-" + labelSize.Col_lg.Value);
            }

            string text = (overrideText != null ? overrideText : flowWebField.Label);
            object seperator = routeValueDictionary.FirstOrDefault(rv => rv.Key.ToLower() == "seperator").Value;
            if (seperator != null)
                tagBuilder.SetInnerText(String.Format("{0}{1}", text, seperator));
            else
                tagBuilder.SetInnerText(text);

            tagBuilder.MergeAttributes(routeValueDictionary, replaceExisting: true);

            if (hasInformationIcon && !string.IsNullOrWhiteSpace(flowWebField.Description))
            {
                // <span style="cursor:pointer;" data-toggle="clickover" data-content="bla"><span class="glyphicon glyphicon-info-sign"></span></span>
                TagBuilder tagOuterSpan = new TagBuilder("span");
                tagOuterSpan.Attributes["style"] = "cursor:pointer;";
                tagOuterSpan.Attributes["data-toggle"] = "clickover";
                tagOuterSpan.Attributes["data-content"] = flowWebField.Description;
                TagBuilder tagInnerSpan = new TagBuilder("span");
                tagInnerSpan.AddCssClass("glyphicon glyphicon-info-sign");
                tagOuterSpan.InnerHtml = tagInnerSpan.ToString(TagRenderMode.Normal);
                tagBuilder.InnerHtml += String.Format("&nbsp;{0}", tagOuterSpan.ToString(TagRenderMode.Normal));
            }

            return MvcHtmlString.Create(tagBuilder.ToString(TagRenderMode.Normal));
        }

        public static string FlowCheckBoxValue(this HtmlHelper htmlHelper, IEnumerable<FlowWebField> flowFields, string fieldName)
        {
            var flowWebField = flowFields.Where(f => f.Name == fieldName && f.Visible).FirstOrDefault();
            if (flowWebField == null)
            {
                return "";
            }

            if (flowWebField.Value.ToLogical())
            {
                return flowWebField.Label;
            }

            return "";
        }

        public static string FlowValueString(this HtmlHelper htmlHelper, IEnumerable<FlowWebField> flowFields, string fieldName, object htmlAttributes = null, bool withlookup = false)
        {
            var flowWebField = flowFields.Where(f => f.Name == fieldName && f.Visible).FirstOrDefault();
            if (flowWebField == null)
            {
                return "";
            }

            return flowWebField.DisplayValue;
        }

        public static MvcHtmlString FlowLabelName(this HtmlHelper htmlHelper, IEnumerable<FlowWebField> flowFields, string fieldName)
        {
            var flowWebField = flowFields.FirstOrDefault(f => f.Name == fieldName && f.Visible);
            return flowWebField == null ? MvcHtmlString.Empty : MvcHtmlString.Create(flowWebField.Label);
        }

        public static MvcHtmlString FlowControl(this HtmlHelper htmlHelper,
            IEnumerable<FlowWebField> flowFields,
            string fieldName,
            object htmlAttributes = null,
            bool inputOnly = false,
            bool forceHidden = false,
            string onclick = "")
        {
            var flowWebField = flowFields.FirstOrDefault(f => f.Name == fieldName);
            if (flowWebField == null)
            {
                return MvcHtmlString.Empty;
            }

            if (forceHidden || flowWebField.Visible == false)
            {
                flowWebField.Type = FlowFieldType.Hidden;
            }

            var htmlattributes = HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes);
            if (!String.IsNullOrEmpty(onclick))
            {
                htmlattributes.Add("onclick", onclick);
            }

            FlowControlCreator flowControlCreator = new FlowControlCreator(flowWebField, htmlattributes, htmlHelper, inputOnly: inputOnly);

            return flowControlCreator.Create();
        }

        public static List<SelectListItem> ToSelectListItems(this IEnumerable<Option> options)
        {
            return ToSelectListItems(options, null);
        }

        public static List<SelectListItem> ToSelectListItems(this IEnumerable<Option> options, string selectedValue = null)
        {
            return options.Select(o => new SelectListItem
            {
                Text = o.Text,
                Value = o.Value,
                Selected = o.Selected || o.Value == selectedValue
            }).ToList();
        }

        public static List<SelectListItem> ToSelectListItems<TEnum>(this IEnumerable<TEnum> enums) where TEnum : Enum
        {
            return enums.Select(v => new SelectListItem
            {
                Text = v.GetDescription(),
                Value = v.ToString("d")
            }).ToList();
        }

        public static List<TEnum> GetEnumList<TEnum>(this HtmlHelper helper) where TEnum : Enum
        {
            return ((TEnum[])Enum.GetValues(typeof(TEnum))).ToList();
        }

        public static List<SelectListItem> GetEnumSelectListItem<TModel, TEnum>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TEnum?>> targetExpression) where TEnum : struct, Enum
        {
            var selectListItems = GetEnumList<TEnum>(helper).ToSelectListItems();
            var selectedValue = targetExpression.Compile().Invoke(helper.ViewData.Model);
            if (((Enum)selectedValue) != null)
            {
                selectListItems.ForEach(sli => sli.Selected = sli.Value == selectedValue.Value.ToString("d"));
            }

            return selectListItems;
        }

        public static List<SelectListItem> GetEnumSelectListItem<TModel, TEnum>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TEnum>> targetExpression) where TEnum : struct, Enum
        {
            var selectListItems = GetEnumList<TEnum>(helper).ToSelectListItems();
            var selectedValue = targetExpression.Compile().Invoke(helper.ViewData.Model);
            if(((Enum)selectedValue) != null)
            {
                selectListItems.ForEach(sli => sli.Selected = sli.Value == selectedValue.ToString("d"));
            }

            return selectListItems;
        }

        public static List<SelectListItem> GetEnumMultiSelectListItem<TModel, TEnum>(this HtmlHelper<TModel> helper, Expression<Func<TModel, IEnumerable<TEnum>>> targetExpression) where TEnum : Enum
        {
            var selectListItems = GetEnumList<TEnum>(helper).ToSelectListItems();
            var selectedValues = targetExpression.Compile().Invoke(helper.ViewData.Model);

            if(selectedValues != null && selectedValues.Any())
            {
                selectListItems.ForEach(sli => sli.Selected = selectedValues.Any(sv => sli.Value == sv.ToString("d")));
            }

            return selectListItems;
        }

        public static List<SelectListItem> GetEnumSelectListItem<TEnum>(this HtmlHelper helper) where TEnum : Enum
        {
            return GetEnumList<TEnum>(helper).ToSelectListItems();
        }

        public static MvcHtmlString SecureTextBoxFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, object htmlAttributes)
        {
            var pointUserInfo = htmlHelper.GetPointUserInfo();

            var model = htmlHelper.ViewData.Model;
            if (model.CanReadValue(expression, pointUserInfo) == false)
            {
                return new MvcHtmlString(" - Beveiligde gegevens - ");
            }

            if (model.CanWriteValue(expression, pointUserInfo))
            {
                return htmlHelper.TextBoxFor(expression, htmlAttributes);
            }
            else
            {
                var hidden = htmlHelper.HiddenFor(expression);
                var label = htmlHelper.ValueFor(expression);
                return new MvcHtmlString(hidden.ToHtmlString() + label.ToHtmlString());
            }
        }

        private static PointUserInfo GetPointUserInfo(this HtmlHelper htmlHelper)
        {
            var pointbasecontroller = htmlHelper.ViewContext.Controller as PointBaseController;
            var pointUserInfo = pointbasecontroller?.PointUserInfo();
            if (pointUserInfo == null)
            {
                throw new Exception("SecureDropDownListFor extension only works on PointBaseController");
            }

            return pointUserInfo;
        }

        public static MvcHtmlString SecureRadioButtonListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, List<SelectListItem> selectList, object htmlAttributes = null)
        {
            var pointUserInfo = htmlHelper.GetPointUserInfo();

            var model = htmlHelper.ViewData.Model;
            if (model.CanReadValue(expression, pointUserInfo) == false)
            {
                return new MvcHtmlString(" - Beveiligde gegevens - ");
            }

            if (model.CanWriteValue(expression, pointUserInfo))
            {
                return htmlHelper.RadioButtonListFor(expression, selectList, htmlAttributes: htmlAttributes);
            }
            else
            {
                var stringValue = selectList.FirstOrDefault(s => s.Selected)?.Text;
                var hidden = htmlHelper.HiddenFor(expression);

                return new MvcHtmlString(hidden.ToHtmlString() + stringValue);
            }
        }

        public static MvcHtmlString SecureDropDownListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, IEnumerable<SelectListItem> selectList, string optionLabel = null, object htmlAttributes = null)
        {
            var pointUserInfo = htmlHelper.GetPointUserInfo();

            var model = htmlHelper.ViewData.Model;
            if (model.CanReadValue(expression, pointUserInfo) == false)
            {
                return new MvcHtmlString(" - Beveiligde gegevens - ");
            }

            if (model.CanWriteValue(expression, pointUserInfo))
            {
                return htmlHelper.DropDownListFor(expression, selectList, optionLabel, htmlAttributes);
            }
            else
            {
                var stringValue = selectList.FirstOrDefault(s => s.Selected)?.Text;
                var hidden = htmlHelper.HiddenFor(expression);

                return new MvcHtmlString(hidden.ToHtmlString() + stringValue);
            }
        }

        public static MvcHtmlString SecureEnumDropDownListFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TEnum>> targetExpression, object htmlAttributes) where TEnum : struct, Enum
        {
            var selectlistitems = htmlHelper.GetEnumSelectListItem(targetExpression);

            return htmlHelper.SecureDropDownListFor(targetExpression, selectlistitems, optionLabel: null, htmlAttributes: htmlAttributes);
        }

        public static MvcHtmlString SecureNullableWithEmptyEnumDropDownListFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TEnum?>> targetExpression, object htmlAttributes, SelectListItem emptyListItem) where TEnum : struct, Enum
        {
            var selectlistitems = htmlHelper.GetEnumSelectListItem(targetExpression);
            selectlistitems.Insert(0, emptyListItem);
            return htmlHelper.SecureDropDownListFor(targetExpression, selectlistitems, optionLabel: null, htmlAttributes: htmlAttributes);
        }

        public static MvcHtmlString SecureNullableEnumDropDownListFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TEnum?>> targetExpression, object htmlAttributes) where TEnum : struct, Enum
        {
            var selectlistitems = htmlHelper.GetEnumSelectListItem(targetExpression);

            return htmlHelper.SecureDropDownListFor(targetExpression, selectlistitems, optionLabel: "<Selecteer een waarde>", htmlAttributes: htmlAttributes);
        }


        public static MvcHtmlString SecureNullableEnumRadioButtonListFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TEnum?>> targetExpression, object htmlAttributes = null) where TEnum : struct, Enum
        {
            var selectlistitems = htmlHelper.GetEnumSelectListItem(targetExpression);

            return htmlHelper.SecureRadioButtonListFor(targetExpression, selectlistitems, htmlAttributes: htmlAttributes);
        }

        public static MvcHtmlString SecureCheckBoxFor<TModel, P>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, P>> targetExpression, object htmlAttributes)
        {
            var pointUserInfo = htmlHelper.GetPointUserInfo();

            PropertyInfo propertyInfo = null;
            P value = default(P);

            if (targetExpression?.Body is MemberExpression targetMemberExp)
            {
                propertyInfo = (PropertyInfo)targetMemberExp.Member;
                value = (P)propertyInfo.GetValue(htmlHelper.ViewData.Model);
            } else
            {
                throw new Exception("No propertyinfo");
            }

            if (pointUserInfo == null)
            {
                throw new Exception("SecureCheckBoxFor extension only works on PointBaseController");
            }

            var model = htmlHelper.ViewData.Model;
            if (model.CanReadValue(targetExpression, pointUserInfo) == false)
            {
                return new MvcHtmlString(" - Beveiligde gegevens - ");
            }

            if (model.CanWriteValue(targetExpression, pointUserInfo))
            {
                //CheckBoxFor doesnt allow bool?
                return htmlHelper.CheckBox(propertyInfo.Name, Convert.ToBoolean(value) == true, htmlAttributes);
            }
            else
            {
                var hidden = htmlHelper.HiddenFor(targetExpression);
                var stringValue = (Convert.ToBoolean(value) == true).BoolToJaNee();

                return new MvcHtmlString(hidden.ToHtmlString() + stringValue);
            }
        }

        public static MvcHtmlString SecureEnumDropDownListLimitedFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TEnum>> expression, IEnumerable<TEnum> limitlist = null, object htmlAttributes = null) where TEnum : struct, Enum
        {
            var pointUserInfo = htmlHelper.GetPointUserInfo();

            var model = htmlHelper.ViewData.Model;
            if (model.CanReadValue(expression, pointUserInfo) == false)
            {
                return new MvcHtmlString(" - Beveiligde gegevens - ");
            }

            if (model.CanWriteValue(expression, pointUserInfo))
            {
                return htmlHelper.EnumDropDownListLimitedFor(expression, limitlist, htmlAttributes);
            }
            else
            {
                var stringValue = "";
                if (expression?.Body is MemberExpression targetMemberExp)
                {
                    var targetPropertyInfo = (PropertyInfo)targetMemberExp.Member;
                    var enumval = (TEnum?)(targetPropertyInfo.GetValue(htmlHelper.ViewData.Model));
                    stringValue = enumval.GetDescription();
                }

                var hidden = htmlHelper.HiddenFor(expression);
                return new MvcHtmlString(hidden.ToHtmlString() + stringValue);
            }
        }

        public static MvcHtmlString SecureNullableEnumDropDownListLimitedFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TEnum?>> expression, IEnumerable<TEnum> limitlist = null, object htmlAttributes = null) where TEnum : struct, Enum
        {
            var pointUserInfo = htmlHelper.GetPointUserInfo();

            var model = htmlHelper.ViewData.Model;
            if (model.CanReadValue(expression, pointUserInfo) == false)
            {
                return new MvcHtmlString(" - Beveiligde gegevens - ");
            }

            if (model.CanWriteValue(expression, pointUserInfo))
            {
                return htmlHelper.NullableEnumDropDownListLimitedFor(expression, limitlist, htmlAttributes);
            }
            else
            {
                var stringValue = "";
                if (expression?.Body is MemberExpression targetMemberExp)
                {
                    var targetPropertyInfo = (PropertyInfo)targetMemberExp.Member;
                    var enumval = (TEnum?)(targetPropertyInfo.GetValue(htmlHelper.ViewData.Model));
                    stringValue = enumval.GetDescription();
                }

                var hidden = htmlHelper.HiddenFor(expression);
                return new MvcHtmlString(hidden.ToHtmlString() + stringValue);
            }
        }

        public static MvcHtmlString SecureListBoxFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, IEnumerable<SelectListItem> selectList, object htmlAttributes)
        {
            var pointUserInfo = htmlHelper.GetPointUserInfo();

            var model = htmlHelper.ViewData.Model;
            if (model.CanReadValue(expression, pointUserInfo) == false)
            {
                return new MvcHtmlString(" - Beveiligde gegevens - ");
            }

            if (model.CanWriteValue(expression, pointUserInfo))
            {
                return htmlHelper.ListBoxFor(expression, selectList, htmlAttributes);
            }
            else
            {
                var stringValue = String.Join(",", selectList.Where(s => s.Selected).Select(s => s.Text));
                var hidden = htmlHelper.HiddenFor(expression);

                return new MvcHtmlString(hidden.ToHtmlString() + stringValue);
            }
        }

        public static MvcHtmlString SecureEnumRadioButtonListFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TEnum>> expression, IEnumerable<TEnum> limitlist = null, object htmlAttributes = null, bool isvertical = false) where TEnum : struct, Enum
        {
            var pointUserInfo = htmlHelper.GetPointUserInfo();

            var model = htmlHelper.ViewData.Model;
            if (model.CanReadValue(expression, pointUserInfo) == false)
            {
                return new MvcHtmlString(" - Beveiligde gegevens - ");
            }

            if (model.CanWriteValue(expression, pointUserInfo))
            {
                return htmlHelper.EnumRadioButtonListFor(expression, limitlist, htmlAttributes, isvertical);
            }
            else
            {
                var stringValue = "";
                if (expression?.Body is MemberExpression targetMemberExp)
                {
                    var targetPropertyInfo = (PropertyInfo)targetMemberExp.Member;
                    var enumval = (TEnum?)(targetPropertyInfo.GetValue(htmlHelper.ViewData.Model));
                    stringValue = enumval.GetDescription();
                }

                var hidden = htmlHelper.HiddenFor(expression);
                return new MvcHtmlString(hidden.ToHtmlString() + stringValue);
            }
        }

        public static MvcHtmlString EnumDropDownListLimitedFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TEnum>> expression, IEnumerable<TEnum> limitlist = null, object htmlAttributes = null, string propertyName = null) where TEnum : struct, Enum
        {
            var allOptions = htmlHelper.GetEnumSelectListItem(expression);

            if (limitlist != null)
            {
                allOptions = limitlist.ToSelectListItems();

                var selecteditem = expression.Compile().Invoke(htmlHelper.ViewData.Model);
                allOptions.ForEach(o => o.Selected = string.Equals(selecteditem.ToString("d"), o.Value, StringComparison.CurrentCultureIgnoreCase));
            }

            // Derive property name for dropdownlist name
            if (propertyName == null)
            {
                propertyName = htmlHelper.GetFullHtmlFieldName(expression);
            }

            return htmlHelper.DropDownList(propertyName, allOptions, htmlAttributes);
        }

        public static MvcHtmlString NullableEnumDropDownListLimitedFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TEnum?>> expression, IEnumerable<TEnum> limitlist = null, object htmlAttributes = null, string propertyName = null) where TEnum : struct, Enum
        {
            var allOptions = htmlHelper.GetEnumSelectListItem(expression);

            if (limitlist != null)
            {
                allOptions = limitlist.ToSelectListItems();

                var selecteditem = expression.Compile().Invoke(htmlHelper.ViewData.Model);
                if(selecteditem.HasValue)
                {
                    allOptions.ForEach(o => o.Selected = string.Equals(selecteditem.Value.ToString("d"), o.Value, StringComparison.CurrentCultureIgnoreCase));
                }
            }

            // Derive property name for dropdownlist name
            if (propertyName == null)
            {
                propertyName = htmlHelper.GetFullHtmlFieldName(expression);
            }

            return htmlHelper.DropDownList(propertyName, allOptions, htmlAttributes);
        }

        public static MvcHtmlString RadioButtonListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, List<SelectListItem> selectListItems, List<SelectListItem> limitlist = null, object htmlAttributes = null, bool isvertical = false)
        {
            var allOptions = selectListItems;

            if (limitlist != null)
            {
                allOptions = limitlist;
            }
            var propertyName = htmlHelper.GetFullHtmlFieldName(expression);

            // Get currently select values from the ViewData model
            var selectedvalue = expression.Compile().Invoke(htmlHelper.ViewData.Model).ToString();
            // Convert selected value list to a List<string> for easy manipulation

            string totalitems = "";

            // Add checkboxes
            foreach (var item in allOptions)
            {
                var radiobutton = string.Format(
                                                  "<label class=\"" + (isvertical ? "" : "radio-inline") + "\"><input type=\"radio\" name=\"{0}\" id=\"{1}_{2}\" " +
                                                  "value=\"{2}\" {3} />{4}</label>",
                                                  propertyName,
                                                  TagBuilder.CreateSanitizedId(propertyName),
                                                  item.Value,
                                                  (selectedvalue == item.Value || item.Selected) ? "checked=\"checked\"" : string.Empty,
                                                  item.Text);

                if (isvertical)
                {
                    var innertag = new TagBuilder("div");
                    innertag.AddCssClass("radio");
                    innertag.InnerHtml = radiobutton;
                    totalitems += innertag.ToString();
                }
                else
                {
                    totalitems += radiobutton;
                }
            }

            return MvcHtmlString.Create(totalitems);
        }

        public static string GetFullHtmlFieldName<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression)
        {
            return htmlHelper.NameFor(expression).ToString();
        }

        public static MvcHtmlString EnumRadioButtonListFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TEnum>> expression, IEnumerable<TEnum> limitlist = null, object htmlAttributes = null, bool isvertical = false) where TEnum : struct, Enum
        {
            var allOptions = htmlHelper.GetEnumSelectListItem(expression);

            if (limitlist != null)
            {
                allOptions = limitlist.ToSelectListItems();
            }

            // Derive property name for checkbox name
            var propertyName = htmlHelper.GetFullHtmlFieldName(expression);

            // Get currently select values from the ViewData model
            var selectedvalue = expression.Compile().Invoke(htmlHelper.ViewData.Model).ToString("d");
            // Convert selected value list to a List<string> for easy manipulation

            string totalitems = "";

            // Add checkboxes
            foreach (var item in allOptions)
            {
                var radiobutton = string.Format(
                                                  "<label class=\"" + (isvertical ? "" : "radio-inline") + "\"><input type=\"radio\" name=\"{0}\" id=\"{1}_{2}\" " +
                                                  "value=\"{2}\" {3} />{4}</label>",
                                                  propertyName,
                                                  TagBuilder.CreateSanitizedId(propertyName),
                                                  item.Value,
                                                  (selectedvalue == item.Value) ? "checked=\"checked\"" : string.Empty,
                                                  item.Text);

                if (isvertical)
                {
                    var innertag = new TagBuilder("div");
                    innertag.AddCssClass("radio");
                    innertag.InnerHtml = radiobutton;
                    totalitems += innertag.ToString();
                } else
                {
                    totalitems += radiobutton;
                }
            }

            return MvcHtmlString.Create(totalitems);
        }

        public static MvcHtmlString EnumCheckBoxListFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, IEnumerable<TEnum>>> expression, IEnumerable<TEnum> limitlist = null, object htmlAttributes = null, bool isvertical = true) where TEnum : struct, Enum
        {
            var allOptions = htmlHelper.GetEnumMultiSelectListItem(expression);

            if (limitlist != null)
            {
                allOptions = limitlist.ToSelectListItems();
            }

            var modelMetadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            // Derive property name for checkbox name
            var propertyName = htmlHelper.GetFullHtmlFieldName(expression);

            // Get currently select values from the ViewData model
            var selecteditems = expression.Compile().Invoke(htmlHelper.ViewData.Model);
            // Convert selected value list to a List<string> for easy manipulation
            var selectedValues = new List<TEnum>(selecteditems).ToSelectListItems();

            // Create div
            var divTag = new TagBuilder("div");

            // Add checkboxes
            foreach (var item in allOptions)
            {
                var innertag = new TagBuilder(isvertical ? "div" : "label");

                if (isvertical)
                {
                    innertag.AddCssClass("checkbox");
                }

                var input = new TagBuilder("input");
                input.Attributes.Add("type", "checkbox");
                input.Attributes.Add("name", propertyName);
                input.Attributes.Add("id", String.Format("{0}_{1}", propertyName, item.Value));
                input.Attributes.Add("value", item.Value);
                if (selectedValues.Any(it => it.Value == item.Value))
                {
                    input.Attributes.Add("checked", null);
                }

                input.MergeAttributes(new RouteValueDictionary(htmlAttributes), true);

                var label = new TagBuilder("label");
                label.InnerHtml = input.ToString(TagRenderMode.SelfClosing) + item.Text;
                if (isvertical == false)
                {
                    label.Attributes.Add("class", "checkbox-inline");
                }

                innertag.InnerHtml = label.ToString(TagRenderMode.Normal);

                divTag.InnerHtml += innertag.ToString();
            }

            return MvcHtmlString.Create(divTag.ToString());
        }

        public static MvcHtmlString CheckBoxListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, IEnumerable<TProperty>>> expression, IEnumerable<SelectListItem> allOptions, object htmlAttributes = null, bool isvertical = true)
        {
            var modelMetadata = ModelMetadata.FromLambdaExpression<TModel, IEnumerable<TProperty>>(expression, htmlHelper.ViewData);
            // Derive property name for checkbox name
            var propertyName = htmlHelper.GetFullHtmlFieldName(expression);

            // Get currently select values from the ViewData model
            var list = expression.Compile().Invoke(htmlHelper.ViewData.Model);

            // Convert selected value list to a List<string> for easy manipulation
            var selectedValues = new List<string>();

            if (list != null)
            {
                selectedValues = new List<TProperty>(list).ConvertAll<string>(i => i.ToString());
            }

            // Create div
            var divTag = new TagBuilder("div");
            divTag.MergeAttributes(new RouteValueDictionary(htmlAttributes), true);

            // Add checkboxes
            if (allOptions != null)
            {
                foreach (var item in allOptions)
                {
                    var innertag = new TagBuilder(isvertical ? "div" : "label");

                    if (isvertical)
                    {
                        innertag.AddCssClass("checkbox");
                    }

                    innertag.InnerHtml += string.Format(
                                                      "<label class=\"" + (isvertical ? "" : "checkbox-inline") + "\"><input type=\"checkbox\" name=\"{0}\" id=\"{1}_{2}\" " +
                                                      "value=\"{2}\" {3} />{4}</label>",
                                                      propertyName,
                                                      TagBuilder.CreateSanitizedId(propertyName),
                                                      item.Value,
                                                      selectedValues.Contains(item.Value) ? "checked=\"checked\"" : string.Empty,
                                                      item.Text);

                    divTag.InnerHtml += innertag.ToString();
                }
            }

            return MvcHtmlString.Create(divTag.ToString());
        }

        public static MvcHtmlString LabelWithInformationIconFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, 
            Expression<Func<TModel, TProperty>> expression, object htmlAttributes, string information, int labelsize = 3)
        {
            RouteValueDictionary routeValueDictionary = HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes);
            
            var propertyName = htmlHelper.GetFullHtmlFieldName(expression);

            var metadata = ModelMetadata.FromLambdaExpression<TModel, TProperty>(expression, htmlHelper.ViewData);
            string resolvedLabelText = metadata.DisplayName ?? metadata.PropertyName;

            TagBuilder tagBuilder = new TagBuilder("label");
            tagBuilder.Attributes["for"] = TagBuilder.CreateSanitizedId(propertyName);
            tagBuilder.AddCssClass("control-label col-xs-" + labelsize);

            tagBuilder.SetInnerText(resolvedLabelText);

            tagBuilder.MergeAttributes(routeValueDictionary, replaceExisting: true);

            if (!string.IsNullOrWhiteSpace(information))
            {
                TagBuilder tagOuterSpan = new TagBuilder("span");
                tagOuterSpan.Attributes["style"] = "cursor:pointer;";
                tagOuterSpan.Attributes["data-title"] = resolvedLabelText;
                tagOuterSpan.Attributes["data-toggle"] = "clickover";
                tagOuterSpan.Attributes["data-content"] = information;
                TagBuilder tagInnerSpan = new TagBuilder("span");
                tagInnerSpan.AddCssClass("glyphicon glyphicon-info-sign");
                tagOuterSpan.InnerHtml = tagInnerSpan.ToString(TagRenderMode.Normal);
                tagBuilder.InnerHtml += String.Format("&nbsp;{0}", tagOuterSpan.ToString(TagRenderMode.Normal));
            }

            return MvcHtmlString.Create(tagBuilder.ToString(TagRenderMode.Normal));
        }
        
        public static IDisposable BeginFlowGroup(this HtmlHelper html, IEnumerable<FlowWebField> flowFields, string fieldName)
        {
            var writer = html.ViewContext.Writer;
            var flowWebField = flowFields.Where(f => f.Name == fieldName && f.Visible).FirstOrDefault();
            if (flowWebField == null)
            {
                writer.Write(String.Format("<!-- Begin FlowFieldGroup '{0}' not found. -->", fieldName));
            }
            else
            {
                writer.Write(String.Format("<div><p class=\"flowFieldGroupHeader\"><b>{1}</b></p><div id=\"{0}\" class=\"flowFieldGroup\">", flowWebField.Name, flowWebField.Label));
            }

            return new FlowControlGroup(writer, fieldName, flowWebField);
        }

        public static HtmlString GetEnums<T>(this HtmlHelper helper) where T : struct
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.AppendLine("<script type=\"text/javascript\">");
            sb.AppendLine("if(!window.Enum) Enum = {};");
            var enumeration = Activator.CreateInstance(typeof(T));
            var enums = Enum.GetValues(typeof(T)).Cast<int>().ToDictionary(k => Enum.GetName(typeof(T), k), v => v);
            sb.AppendLine("Enum." + typeof(T).Name + " = " + System.Web.Helpers.Json.Encode(enums) + " ;");
            sb.AppendLine("</script>");
            return new HtmlString(sb.ToString());
        }

        /// <remarks>Convenient for Bootstrap</remarks>
        public static string ToActive(this HtmlHelper htmlHelper, bool active)
        {
            return active ? "active" : string.Empty;
        }

        public static MvcHtmlString DisplaySearchResultEntryTable(this HtmlHelper htmlHelper, FlowInstanceSearchViewModel sitem, SearchUIFieldConfiguration searchProperty, bool clickable, string digest)
        {
            if (sitem.FlowInstanceSearchValues == null)
            {
                return new MvcHtmlString("");
            }

            var urlHelper = new UrlHelper(htmlHelper.ViewContext.RequestContext);

            int transferID = sitem.FlowInstanceSearchValues.TransferID;

            var routeValues = new RouteValueDictionary(new { TransferID = transferID, sitem.FlowInstanceDoorlopendDossierSearchValues?.FrequencyID, Digest = digest });

            string url = "#";
            if (searchProperty.SearchDestination == SearchDestination.Dashboard)
            {
                url = urlHelper.Action("Dashboard", "FlowMain", routeValues);
            }

            if (searchProperty.SearchDestination == SearchDestination.ListFromFrequencySearch)
            {
                url = urlHelper.Action("ListFromFrequency", "TransferMemo", routeValues);
            }

            if (searchProperty.SearchDestination == SearchDestination.TransferAttachment)
            {
                url = urlHelper.Action("List", "TransferAttachment", routeValues);
            }

            if (searchProperty.SearchDestination == SearchDestination.CommunicationJournal)
            {
                routeValues.Add("FormTypeID", (int)FlowFormType.CommunicationJournal);
            }

            if (searchProperty.SearchDestination == SearchDestination.AdditionalInfoRequestTP)
            {
                routeValues.Add("FormTypeID", (int)FlowFormType.AanvullendeGegevensTPZHVVT);
            }

            if (searchProperty.SearchDestination == SearchDestination.TransferRoute)
            {
                routeValues.Add("FormTypeID", (int)FlowFormType.RouterenDossierZHVVT);
            }

            if (searchProperty.SearchDestination == SearchDestination.TransferRouteRzTP)
            {
                routeValues.Add("FormTypeID", (int)FlowFormType.RouterenDossierRzTP);
            }

            if (searchProperty.SearchDestination == SearchDestination.RequestFormZHVVT)
            {
                routeValues.Add("FormTypeID", (int)FlowFormType.AanvraagFormulierZHVVT);
            }

            if (searchProperty.SearchDestination == SearchDestination.RequestFormHAVVT)
            {
                routeValues.Add("FormTypeID", (int)FlowFormType.AanvraagFormulierHAVVT);
            }

            if (searchProperty.SearchDestination == SearchDestination.AcceptAtHospital)
            {
                routeValues.Add("FormTypeID", (int)FlowFormType.InBehandelingTP);
            }

            if (searchProperty.SearchDestination == SearchDestination.GRZForm)
            {
                routeValues.Add("FormTypeID", (int)FlowFormType.GRZFormulier);
            }

            if (sitem.FlowInstanceDoorlopendDossierSearchValues != null)
            {
                if (routeValues.ContainsKey("FormTypeID"))
                {
                    routeValues.Remove("FormTypeID");
                }

                switch (sitem.FlowInstanceDoorlopendDossierSearchValues.FrequencyType)
                {
                    case FrequencyType.CVA:
                        routeValues.Add("FormTypeID", (int)FlowFormType.InvullenCVARegistratie3mnd);
                        break;
                    case FrequencyType.MSVT:
                        routeValues.Add("FormTypeID", (int)FlowFormType.MSVTIndDoorldossier);
                        break;
                    case FrequencyType.General:
                        routeValues.Add("FormTypeID", (int)FlowFormType.DoorlopendDossier);
                        break;
                    default:
                        break;
                }
            }

            if (routeValues.ContainsKey("FormTypeID"))
            {
                url = urlHelper.Action("DispatchToForm", "FormType", routeValues);
            }

            string htmlText = null;
            string valueText = null;

            if (searchProperty.SearchFieldOwningType == SearchFieldOwningType.Transfer)
            {
                valueText = PropertyHelper.GetPropValue(sitem.FlowInstanceSearchValues, searchProperty.Field.Name);
            }
            else if (searchProperty.SearchFieldOwningType == SearchFieldOwningType.Frequency)
            {
                valueText = PropertyHelper.GetPropValue(sitem.FlowInstanceDoorlopendDossierSearchValues, searchProperty.Field.Name);
            }

            if (searchProperty.Icon.ShowAsIcon && searchProperty.Icon.TreatNullAsFalse && string.IsNullOrWhiteSpace(valueText))
            {
                valueText = "Nee";
            }

            string icon = "glyphicon glyphicon-info-sign";
            string infourl = "";
            string customtext = "";

            switch (searchProperty.Field.Name)
            {
                case WellKnownFieldNames.LastGrzFormDateTime:
                    customtext = sitem.FlowInstanceSearchValues.LastGrzFormDateTime.ToPointDayMonthTime();
                    infourl = urlHelper.Action("GetLastGrzForm", "Lookup", routeValues);
                    break;
                case WellKnownFieldNames.LastTransferMemoDateTime:
                    customtext = sitem.FlowInstanceSearchValues.LastTransferMemoDateTime.ToPointDayMonthTime();
                    infourl = urlHelper.Action("GetLastTransferMemos", "Lookup", routeValues);
                    break;
                case WellKnownFieldNames.LastTransferAttachmentUploadDate:
                    customtext = sitem.FlowInstanceSearchValues.LastTransferAttachmentUploadDate.ToPointDayMonthTime();
                    infourl = urlHelper.Action("GetLastTransferAttachment", "Lookup", routeValues);
                    break;
                case WellKnownFieldNames.LastAdditionalTPMemoDateTime:
                    customtext = sitem.FlowInstanceSearchValues.LastAdditionalTPMemoDateTime.ToPointDayMonthTime();
                    infourl = urlHelper.Action("GetLastAdditionalTPMemo", "Lookup", routeValues);
                    break;
                case WellKnownFieldNames.LastFrequencyTransferMemoDateTime:
                    customtext = sitem.FlowInstanceDoorlopendDossierSearchValues?.LastFrequencyTransferMemoDateTime.ToPointDayMonthTime();
                    infourl = urlHelper.Action("GetLastFeedbackMemo", "Lookup", routeValues);
                    break;
                case WellKnownFieldNames.LastFrequencyAttachmentUploadDate:
                    customtext = sitem.FlowInstanceDoorlopendDossierSearchValues?.LastFrequencyAttachmentUploadDate.ToPointDayMonthTime();
                    infourl = urlHelper.Action("GetLastFeedbackTransferAttachment", "Lookup", routeValues);
                    break;
                case WellKnownFieldNames.CopyOfTransferID:
                    infourl = urlHelper.Action("GetCopyOfTransfer", "Lookup", routeValues);
                    break;
                case WellKnownFieldNames.TransferTaskDueDate:
                    customtext = sitem.FlowInstanceSearchValues.TransferTaskDueDate.ToPointDayMonthTime();
                    infourl = urlHelper.Action("GetTransferTaskList", "Lookup", routeValues);
                    break;
            }

            if (searchProperty.Icon.ShowAsIcon && !string.IsNullOrEmpty(valueText))
            {
                icon = searchProperty.Icon.GlyphName;

                if (valueText.IsLogical() && valueText.ToLogical() == false)
                {
                    icon = searchProperty.Icon.GlyphNameFalse;
                }
                else if (valueText.IsLogical() == false && string.IsNullOrEmpty(valueText))
                {
                    icon = searchProperty.Icon.GlyphNameFalse;
                }

                //Only the icon
                customtext = "";
            }

            if (!string.IsNullOrEmpty(infourl) && !string.IsNullOrEmpty(valueText))
            {
                htmlText = $"<span class='clickover-info' style='cursor:pointer;' data-toggle='clickover'" +
                    $" data-title='{searchProperty.Column.FullName}' data-infourl='{infourl}'><span class='{icon}'></span></span>";
                htmlText += (clickable ? $" <a href='{url}'>{customtext}</a>" : $" {customtext}"); 
            }

            if (searchProperty.Field.Name == WellKnownFieldNames.LastFrequencyTransferMemoEmployeeName && sitem.FlowInstanceDoorlopendDossierSearchValues != null)
            {
                valueText = sitem.FlowInstanceDoorlopendDossierSearchValues.LastFrequencyTransferMemoEmployeeName;
            }
            if (searchProperty.Field.Name == WellKnownFieldNames.FrequencyStartDate && sitem.FlowInstanceDoorlopendDossierSearchValues != null)
            {
                valueText = sitem.FlowInstanceDoorlopendDossierSearchValues.FrequencyStartDate.ToPointDateDisplay();
            }
            if (searchProperty.Field.Name == WellKnownFieldNames.FrequencyEndDate && sitem.FlowInstanceDoorlopendDossierSearchValues != null)
            {
                valueText = sitem.FlowInstanceDoorlopendDossierSearchValues.FrequencyEndDate.ToPointDateDisplay();
            }
            if (searchProperty.Field.Name == WellKnownFieldNames.FrequencyType && sitem.FlowInstanceDoorlopendDossierSearchValues != null)
            {
                valueText = sitem.FlowInstanceDoorlopendDossierSearchValues.FrequencyType.GetDescription();
            }
            if (searchProperty.Field.Name == WellKnownFieldNames.FrequencyName && sitem.FlowInstanceDoorlopendDossierSearchValues != null)
            {
                valueText = sitem.FlowInstanceDoorlopendDossierSearchValues.FrequencyName;
            }
            if (sitem.Anonymous && (searchProperty.Field.Name == WellKnownFieldNames.ClientCivilServiceNumber))
            {
                valueText = "xxxxxxxxxxxx";
            }
            if (sitem.Anonymous && (searchProperty.Field.Name == WellKnownFieldNames.ClientFullname))
            {
                valueText = sitem.FlowInstanceSearchValues.TransferID.ToString();
            }

            if (htmlText == null)
            {
                htmlText = (clickable ? $"<a href='{url}'>{valueText}</a>" : valueText);
            }

            return new MvcHtmlString(htmlText);
        }

        public static string Conditional(this HtmlHelper htmlHelper, bool condition, string trueReply, string falseReply = "")
        {
            return condition ? trueReply : falseReply;
        }

        public static void RenderPartialIfNotNull(this HtmlHelper htmlHelper, string partialViewName, object model)
        {
            if (model != null)
            {
                htmlHelper.RenderPartial(partialViewName, model);
            }
        }
        
    }
}