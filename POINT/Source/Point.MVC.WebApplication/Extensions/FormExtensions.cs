﻿using Point.Database.Models.ViewModels;
using Point.Infrastructure.Extensions;
using Point.Models.Enums;
using Point.MVC.WebApplication.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Point.MVC.WebApplication.Extensions
{
    public class FlowGroup : IDisposable
    {
        public HtmlHelper HtmlHelper;

        private bool isDisposed;
        public BootstrapSize LabelSize { get; set; }
        public BootstrapSize ControlSize { get; set; }
        private string grouptag { get; set; }
        public bool Disabled { get; set; }

        public FlowGroup(HtmlHelper htmlhelper, BootstrapSize labelsize, BootstrapSize controlsize, string grouptag, bool disabled)
        {
            HtmlHelper = htmlhelper;
            LabelSize = labelsize;
            ControlSize = controlsize;
            this.grouptag = grouptag;
            Disabled = disabled;
        }

        public void Dispose()
        {
            Dispose(true);
        }

        public virtual void Dispose(bool disposing)
        {
            if (!isDisposed)
            {
                isDisposed = true;

                HtmlHelper.ViewContext.Writer.Write("</" + grouptag + ">");
                HtmlHelper.ViewContext.Writer.Flush();
            }
        }
    }

    public class FlowForm : FlowGroup
    {
        public FlowForm(HtmlHelper htmlhelper, BootstrapSize labelsize, BootstrapSize controlsize, string tag = "form", bool disabled = false)
            : base(htmlhelper, labelsize, controlsize, tag, disabled)
        {
        }
    }
    public class FlowFormSection : FlowGroup
    {
        public FlowFormSection(HtmlHelper htmlhelper, BootstrapSize labelsize, BootstrapSize controlsize, string tag = "section", bool disabled = false)
            : base(htmlhelper, labelsize, controlsize, tag, disabled)
        {
        }
    }

    public class InputGroupProperties
    {
        public enum InputGroupPosition
        {
            Left = 0,
            Right = 1
        }

        public string GlyphIcon { get; set; }
        public string AjaxFunction { get; set; }
        public bool BindAjaxAlsoOnControl { get; set; }
        public InputGroupPosition Position { get; set; }
        public string ClearButtonTargetElementId { get; set; } // eg: HealthCareProvider

        public InputGroupProperties(
            string glyphIcon,
            string ajaxFunction,
            InputGroupPosition position = InputGroupPosition.Right,
            bool bindajaxalsooncontrol = false,
            string clearButtonTargetElementId = null)
        {
            GlyphIcon = glyphIcon;
            AjaxFunction = ajaxFunction;
            BindAjaxAlsoOnControl = bindajaxalsooncontrol;
            Position = position;
            ClearButtonTargetElementId = clearButtonTargetElementId;
        }
    }

    public class BootstrapSize
    {
        public BootstrapSize(int? xssize, int? smsize = null, int? mdsize = null, int? lgsize = null)
        {
            Col_xs = xssize;
            Col_sm = smsize;
            Col_md = mdsize;
            Col_lg = lgsize;
        }

        public override string ToString()
        {
            return string.Format("{0} {1} {2} {3}", 
                (Col_xs.HasValue ? "col-xs-" + Col_xs : ""), 
                (Col_sm.HasValue ? "col-sm-" + Col_xs : ""),
                (Col_md.HasValue ? "col-md-" + Col_md : ""),
                (Col_lg.HasValue ? "col-lg-" + Col_lg : ""))
            .Replace("   ", " ").Replace("  ", " ");
        }

        public int? Col_lg { get; set; }
        public int? Col_md { get; set; }
        public int? Col_sm { get; set; }
        public int? Col_xs { get; set; }
    }

    public static class FormExtensions
    {
        public static FlowForm BeginFlowFormResponsive(this HtmlHelper htmlHelper, string formAction, FormMethod method, IDictionary<string, object> htmlAttributes, BootstrapSize labelSize, BootstrapSize controlSize, bool disableform = false)
        {
            string tag = disableform ? "disabledform" : "form";

            TagBuilder tagBuilder = new TagBuilder(tag);
            tagBuilder.MergeAttributes(htmlAttributes);
            tagBuilder.MergeAttribute("action", formAction);
            tagBuilder.MergeAttribute("method", HtmlHelper.GetFormMethodString(method), true);
            if (htmlHelper.ViewContext.Controller is PointFormController)
            {
                var pointformcontroller = (PointFormController) htmlHelper.ViewContext.Controller;
                if (pointformcontroller.SignaleringTrigger != null)
                {
                    tagBuilder.Attributes.Add("data-signalering-prompt", pointformcontroller.SignaleringTrigger.ShowPrompt.ToString().ToLower());
                }
            }

            htmlHelper.ViewContext.Writer.Write(tagBuilder.ToString(TagRenderMode.StartTag));

            return new FlowForm(htmlHelper, labelSize, controlSize, tag, disableform);
        }

        public static FlowForm BeginFlowForm(this HtmlHelper htmlHelper, string formAction, FormMethod method, IDictionary<string, object> htmlAttributes, int labelSize = 3, int controlSize = 5, bool disableform = false)
        {
            var objlabelSize = new BootstrapSize(labelSize);
            var objcontrolSize = new BootstrapSize(controlSize);

            return BeginFlowFormResponsive(htmlHelper, formAction, method, htmlAttributes, objlabelSize, objcontrolSize, disableform);
        }

        public static FlowFormSection BeginFlowFormSectionResponsive(this FlowForm flowform, IDictionary<string, object> htmlAttributes = null, BootstrapSize labelSize = null, BootstrapSize controlSize = null)
        {
            TagBuilder tagBuilder = new TagBuilder("section");
            tagBuilder.MergeAttributes(htmlAttributes);

            flowform.HtmlHelper.ViewContext.Writer.Write(tagBuilder.ToString(TagRenderMode.StartTag));

            var objlabelSize = labelSize ?? flowform.LabelSize;
            var objcontrolSize = controlSize ?? flowform.ControlSize;

            return new FlowFormSection(flowform.HtmlHelper, objlabelSize, objcontrolSize);
        }

        public static FlowFormSection BeginFlowFormSection(this FlowForm flowform, IDictionary<string, object> htmlAttributes = null, int? labelSize = null, int? controlSize = null, bool disableform = false)
        {
            TagBuilder tagBuilder = new TagBuilder("section");
            tagBuilder.MergeAttributes(htmlAttributes);

            flowform.HtmlHelper.ViewContext.Writer.Write(tagBuilder.ToString(TagRenderMode.StartTag));

            var objlabelSize = labelSize == null ? flowform.LabelSize : new BootstrapSize(labelSize);
            var objcontrolSize = controlSize == null ? flowform.ControlSize : new BootstrapSize(controlSize);

            return new FlowFormSection(flowform.HtmlHelper, objlabelSize, objcontrolSize, disabled: disableform);
        }

        public static MvcHtmlString FlowLabelResponsive(this FlowGroup flowform, IEnumerable<FlowWebField> flowwebfields, string fieldName,
            object htmlAttributes = null, BootstrapSize overrideLabelSize = null, bool hasInformationIcon = false, string overrideText = null)
        {
            return flowform.HtmlHelper.FlowLabel(flowwebfields, fieldName, htmlAttributes,
                labelSize: overrideLabelSize ?? flowform.LabelSize, hasInformationIcon: hasInformationIcon,
                overrideText: overrideText);
        }

        public static MvcHtmlString FlowLabel(this FlowGroup flowform, IEnumerable<FlowWebField> flowwebfields, string fieldName,
            object htmlAttributes = null, int? overrideLabelSize = null, bool hasInformationIcon = false, string overrideText = null)
        {
            var objoverrideLabelSize = overrideLabelSize == null ? null : new BootstrapSize(overrideLabelSize);

            return FlowLabelResponsive(flowform, flowwebfields, fieldName, htmlAttributes, objoverrideLabelSize, hasInformationIcon, overrideText);
        }

        public static MvcHtmlString FlowValue(this FlowGroup flowform, IEnumerable<FlowWebField> flowwebfields, string fieldName)
        {
            var flowWebField = flowwebfields.FirstOrDefault(fwf => fwf.Name == fieldName);
            return flowWebField != null ? MvcHtmlString.Create(flowWebField.DisplayValue) : MvcHtmlString.Empty;
        }

        public static MvcHtmlString FlowDisplayValue(this FlowGroup flowform, IEnumerable<FlowWebField> flowwebfields, string fieldName)
        {
            var flowWebField = flowwebfields.FirstOrDefault(fwf => fwf.Name == fieldName);
            if (flowWebField != null)
                return MvcHtmlString.Create(flowWebField.DisplayValue);
            return MvcHtmlString.Empty;
        }

        public static bool HasValue(this IEnumerable<FlowWebField> flowwebfields, string fieldName)
        {
            return flowwebfields.Any(it => it.Name == fieldName && ((it.Type == FlowFieldType.Checkbox && it.Value.ToLogical()) || (it.Type != FlowFieldType.Checkbox && !String.IsNullOrEmpty(it.Value))));
        }

        public static bool IsTrue(this IEnumerable<FlowWebField> flowwebfields, string fieldName)
        {
            return flowwebfields.Any(it => it.Name == fieldName && it.Value.ToLogical());
        }

        public static MvcHtmlString FlowInput(this FlowGroup flowform, IEnumerable<FlowWebField> flowwebfields, string fieldName, object htmlAttributes = null, InputGroupProperties inputgroupproperty = null)
        {
            MvcHtmlString flowcontrol = flowform.HtmlHelper.FlowControl(flowwebfields, fieldName, htmlAttributes, inputOnly: true);

            return MvcHtmlString.Create(flowcontrol.ToString());
        }

        public static MvcHtmlString FlowHiddenInput(this FlowGroup flowform, IEnumerable<FlowWebField> flowwebfields, string fieldName)
        {
            MvcHtmlString flowcontrol = flowform.HtmlHelper.FlowControl(flowwebfields, fieldName, inputOnly: true, forceHidden: true);

            return MvcHtmlString.Create(flowcontrol.ToString());
        }

        public static MvcHtmlString FlowControl(this FlowGroup flowform, IEnumerable<FlowWebField> flowwebfields, string fieldName,
            object htmlAttributes = null, InputGroupProperties inputGroupProperties = null)
        {
            var flowWebField = flowwebfields.FirstOrDefault(fwf => fwf.Name == fieldName);
            var isReadOnly = false;
            if (flowWebField != null)
            {
                isReadOnly = flowWebField.ReadOnly;
            }

            var readMode = flowform.HtmlHelper.ViewData.IsReadMode() || flowform.Disabled;
            var printmode = flowform.HtmlHelper.ViewContext.RequestContext.HttpContext.Request.IsPrintRequest();

            var onclick =
                inputGroupProperties != null
                && !isReadOnly
                && !readMode
                && !string.IsNullOrEmpty(inputGroupProperties.AjaxFunction)
                && inputGroupProperties.BindAjaxAlsoOnControl
                    ? inputGroupProperties.AjaxFunction
                    : "";

            var flowcontrol = flowform.HtmlHelper.FlowControl(flowwebfields, fieldName, htmlAttributes, onclick: onclick);

            if (!readMode && !printmode && inputGroupProperties != null)
            {
                var div = new TagBuilder("div");
                div.AddCssClass("input-group");

                TagBuilder spanRemove = null;
                if (!string.IsNullOrWhiteSpace(inputGroupProperties.ClearButtonTargetElementId))
                {
                    spanRemove = new TagBuilder("span");
                    spanRemove.AddCssClass("input-group-addon btn btn-secondary");
                    spanRemove.InnerHtml = "<span class='glyphicon glyphicon-remove'></span>";
                    spanRemove.Attributes.Add("style", "cursor: pointer;");
                    spanRemove.Attributes.Add("onclick", "clearHealthCareProvider();");
                }

                var span1 = new TagBuilder("span");
                span1.AddCssClass("input-group-addon");
                span1.InnerHtml = $"<span class='glyphicon {inputGroupProperties.GlyphIcon}'></span>";
                span1.Attributes.Add("style", $"cursor: {(isReadOnly ? "not-allowed" : "pointer")};");

                if (!isReadOnly && !string.IsNullOrEmpty(inputGroupProperties.AjaxFunction))
                {
                    span1.Attributes.Add("onclick", inputGroupProperties.AjaxFunction);
                }

                var inputgroupControlsLeft =
                    inputGroupProperties.Position == InputGroupProperties.InputGroupPosition.Left
                        ? span1.ToString()
                        : null;
                var inputgroupControlsRight =
                    inputGroupProperties.Position == InputGroupProperties.InputGroupPosition.Right
                        ? span1.ToString()
                        : null;

                div.InnerHtml = inputgroupControlsLeft + flowcontrol + spanRemove + inputgroupControlsRight;
                return MvcHtmlString.Create(div.ToString());
            }

            return MvcHtmlString.Create(flowcontrol.ToString());
        }

        public static MvcHtmlString FormGroup(
            this FlowGroup flowform,
            IEnumerable<FlowWebField> flowwebfields,
            string fieldName,
            object htmlAttributes = null,
            int? overrideLabelSize = null,
            int? overrideControlSize = null,
            InputGroupProperties inputGroupProperties = null,
            bool hasInformationIcon = false,
            string overrideText = null)
        {
            var objlabelSize = overrideLabelSize == null ? null : new BootstrapSize(overrideLabelSize);
            var objcontrolSize = overrideControlSize == null ? null : new BootstrapSize(overrideControlSize);

            return FormGroupResponsive(
                flowform, flowwebfields, fieldName, htmlAttributes, objlabelSize, objcontrolSize,
                inputGroupProperties, hasInformationIcon, overrideText);
        }

        public static MvcHtmlString FormGroupResponsive(
            this FlowGroup flowform,
            IEnumerable<FlowWebField> flowwebfields,
            string fieldName,
            object htmlAttributes = null,
            BootstrapSize overrideObjLabelSize = null,
            BootstrapSize overrideObjControlSize = null,
            InputGroupProperties inputGroupProperties = null,
            bool hasInformationIcon = false,
            string overrideText = null,
            bool showLabel = true)
        {
            overrideObjLabelSize = overrideObjLabelSize ?? flowform.LabelSize;
            overrideObjControlSize = overrideObjControlSize ?? flowform.ControlSize;

            TagBuilder formgroupbuilder = new TagBuilder("div");
            formgroupbuilder.AddCssClass("form-group");

            MvcHtmlString flowlabel = MvcHtmlString.Empty;
            if (showLabel)
            {
                flowlabel = flowform.HtmlHelper.FlowLabel(
                                flowwebfields, 
                                fieldName,
                                labelSize: (overrideObjLabelSize ?? flowform.LabelSize),
                                hasInformationIcon: hasInformationIcon, overrideText: overrideText);
            }

            var controlContainer = new TagBuilder("div");           
            var controlsize = overrideObjControlSize ?? flowform.ControlSize;

            if (controlsize != null)
            {
                if (controlsize.Col_xs.HasValue)
                {
                    controlContainer.AddCssClass("col-xs-" + controlsize.Col_xs.Value);
                }
                if (controlsize.Col_sm.HasValue)
                {
                    controlContainer.AddCssClass("col-sm-" + controlsize.Col_sm.Value);
                }
                if (controlsize.Col_md.HasValue)
                {
                    controlContainer.AddCssClass("col-md-" + controlsize.Col_md.Value);
                }
                if (controlsize.Col_lg.HasValue)
                {
                    controlContainer.AddCssClass("col-lg-" + controlsize.Col_lg.Value);
                }
            }

            MvcHtmlString flowControl = FlowControl(flowform, flowwebfields, fieldName, htmlAttributes, inputGroupProperties);
            controlContainer.InnerHtml = flowControl.ToString();

            formgroupbuilder.InnerHtml = flowlabel + controlContainer.ToString();

            return MvcHtmlString.Create(formgroupbuilder.ToString());
        }
    }
}