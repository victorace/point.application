﻿using System;
using System.Collections.Generic;
using System.Linq;
using Point.Business.Helper;
using Point.Business.Logic;
using Point.Database.Extensions;
using Point.Database.Models;
using Point.Database.Models.Constants;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Extensions;
using Point.Models.Enums;
using Point.MVC.WebApplication.Controllers;

namespace Point.MVC.WebApplication.Helpers
{
    public static class FormTypeEOverdrachtViewModelHelper
    {
        private const string _enter = "\r\n";

        private static string ContactPersonToString(IEnumerable<ContactPerson> contactPerson)
        {
            return contactPerson.Aggregate(string.Empty, (current, cp) => current + cp.FullName() + Environment.NewLine);
        }

        public static EOverdracht30ViewModel GetEOverdracht30ViewModel(PointController controller, IUnitOfWork uow, GlobalProperties globalProps)
        {
            var viewModel = new EOverdracht30ViewModel();
            var flowWebFields = globalProps.FlowWebFields;

            viewModel.ClientID = ClientBL.GetByTransferID(uow, globalProps.TransferID)?.ClientID;
            
            var vobl = new EOverdracht30BL(uow, globalProps.PointUserInfo, globalProps.FlowInstance.FlowInstanceID);
            controller.ViewBag.Groups = vobl.GetSections(EOverdrachtType.Volwassenen, ref flowWebFields);

            globalProps.FlowWebFields = flowWebFields;
            viewModel.SetGlobalProperties(globalProps);

            return viewModel;
        }

        public static EOverdrachtFlowViewModel GetEOverdrachtFlowViewModel(FormTypeController controller, IUnitOfWork uow, GlobalProperties globalProps, bool showResendQuestion)
        {
            var viewModel = new EOverdrachtFlowViewModel();
            viewModel.SetGlobalProperties(globalProps);

            var flowWebFields = globalProps.FlowWebFields;

            var vOFormulierType = flowWebFields.FirstOrNew(it => it.FlowFieldID == FlowFieldConstants.ID_VOFormulierType);
            var vOFormulierTypeValue = vOFormulierType.Value;

            if (globalProps.FormSetVersionID > 0 && !string.IsNullOrEmpty(vOFormulierTypeValue))
            {
                vOFormulierType.ReadOnly = true;
                if(vOFormulierType.FlowFieldDataSource.Options.FirstOrDefault(o => o.Value == vOFormulierTypeValue) == null)
                {
                    vOFormulierType.FlowFieldDataSource.Options.Insert(0, new Option() { Value = vOFormulierTypeValue, Text = vOFormulierTypeValue, Selected = true });
                }

            } else if (string.IsNullOrEmpty(vOFormulierTypeValue))
            {
                var defaultval = vOFormulierType.FlowFieldDataSource.Options.FirstOrDefault()?.Value;
                FlowWebFieldBL.SetValue(vOFormulierType, defaultval, Source.DEFAULT, viewData: globalProps.ViewData);
            }

            //viewModel.VOFormulierTypeIsReadOnly = vOFormulierType.ReadOnly;
            //viewModel.VOFormulierTypeValue = vOFormulierType.Value;
            var voFormulierTransferAttachmentID = flowWebFields.FirstOrDefault(it => it.FlowFieldID == FlowFieldConstants.ID_VOFormulierTransferAttachmentID && !string.IsNullOrEmpty(it.Value));
            if (voFormulierTransferAttachmentID != null)
            {
                viewModel.vOFormulierTransferHasAttachment = true;
                viewModel.vOFormulierTransferAttachmentID = Convert.ToInt32(voFormulierTransferAttachmentID.Value);
            }
            
            var pointUserInfo = globalProps.PointUserInfo;

            var client = ClientBL.GetByTransferID(uow, globalProps.TransferID);
            viewModel.ClientID = client.ClientID;
            viewModel.ShowResendQuestion = showResendQuestion;

            var endPointConfiguration = EndpointConfigurationBL.Get(uow, pointUserInfo.Organization.OrganizationID, globalProps.FormTypeID);
            viewModel.HasEndpoint = endPointConfiguration != null;

            // Quickfix for:
            // #14824 Definitief maken VO als bijlage: foutmelding indien palliatieve sectie aanwezig
            // A more generic solution for Form as bijlage is under discussion
            // #14994 VO als bijlage: optimalisatie van de wijze waarop 2 type formulieren/soorten voor de VO gebruikt worden
            viewModel.ValidateOnDefinitive = PhaseDefinitionBL.GetByID(uow, globalProps.PhaseDefinitionID)?.PhaseDefinitionNavigation?
                                                 .FirstOrDefault(pdn => pdn.ActionTypeID == ActionTypeID.Definitive)?.Validate ?? false;

            var flowInstance = globalProps.FlowInstance;    //FlowInstanceBL.GetByTransferID(uow, globalProps.TransferID);
            controller.ViewBag.FlowInstanceID = flowInstance.FlowInstanceID;

            var explanationHelper = new FlowFieldExplanationHelper(flowWebFields, null);

            //Pre-Fill form with data of a related transfer (if exists). 
            //Pre-Fill data from current transfer has priority on related transfer
            var relatedTransferTakeOver = flowInstance.RelatedTransferID.HasValue && flowInstance.RelatedTransferID.Value != (int)TakeOverRelatedTransferStatus.DoNotTakeOver &&
                                          !globalProps.HasFormSetVersion && !globalProps.IsPrintRequest && !globalProps.IsReadMode && flowInstance.FlowDefinitionID == FlowDefinitionID.VVT_VVT;
            if (relatedTransferTakeOver)
            {
                var relatedFlowInstance = FlowInstanceBL.GetByTransferID(uow, flowInstance.RelatedTransferID.Value);
                TakeOverFromRelatedTransfer(uow,flowWebFields, flowInstance, relatedFlowInstance, (int)FlowFormType.VerpleegkundigeOverdrachtFlow);
            }


            //Versturende
            var sendingDepartment = OrganizationHelper.GetSendingDepartment(uow, flowInstance.FlowInstanceID);
            var sendingContact = PhaseInstanceBL.GetByFormsetVersionID(uow, globalProps.FormSetVersionID)?.StartedByEmployee;

            if (sendingDepartment != null)
            {
                explanationHelper.FlowWebFieldFillValue(FlowFieldConstants.ID_NaamInstelling, sendingDepartment.Location.Organization.OrganizationID);
                explanationHelper.FlowWebFieldFillValue(FlowFieldConstants.ID_OrganisatieAfdeling, sendingDepartment.DepartmentID);
                explanationHelper.FlowWebFieldFillValue(FlowFieldConstants.ID_OrganisatieLocatie, sendingDepartment.Location.LocationID);
                if (sendingDepartment.Location.Organization.OrganizationTypeID == (int) OrganizationTypeID.Hospital)
                {
                    explanationHelper.FlowWebFieldFillValue(FlowFieldConstants.ID_SoortOrganisatie, FlowListValues.GetHealthCareOrganizationTypeByCode("V4").Value);
                }
            }

            if (sendingContact != null)
            {
                explanationHelper.FlowWebFieldFillValue(FlowFieldConstants.ID_OrganisatieNaamContactpersoon, sendingContact.EmployeeID);
                explanationHelper.FlowWebFieldFillValue(FlowFieldConstants.ID_OrganisatieTelefoonnummer, sendingContact.PhoneNumber);
                string emailaddress = sendingContact.EmailAddress;
                if (sendingDepartment != null && !string.IsNullOrEmpty(sendingDepartment.EmailAddress))
                {
                    emailaddress = sendingDepartment.EmailAddress;
                }

                explanationHelper.FlowWebFieldFillValue(FlowFieldConstants.ID_OrganisatieEmail, emailaddress);
            }

            //Ontvangende
            var receivingOrganization = OrganizationHelper.GetReceivingOrganization(uow, flowInstance.FlowInstanceID);
            var receivingLocation = OrganizationHelper.GetRecievingLocation(uow, flowInstance.FlowInstanceID);
            var receivingDepartment = OrganizationHelper.GetRecievingDepartment(uow, flowInstance.FlowInstanceID);
            var receivingEmployee = OrganizationHelper.GetRecievingEmployee(uow, flowInstance.FlowInstanceID);

            explanationHelper.FlowWebFieldFillValue(FlowFieldConstants.ID_OntvangendeNaam, receivingOrganization?.OrganizationID);
            explanationHelper.FlowWebFieldFillValue(FlowFieldConstants.ID_OntvangendeLocatie, receivingLocation?.LocationID);
            explanationHelper.FlowWebFieldFillValue(FlowFieldConstants.ID_OntvangendeContactpersoon, receivingEmployee?.EmployeeID);
            
            if (receivingDepartment?.AfterCareType1 != null)
            {
                var ontvangendeSoortOrganisatie = explanationHelper.GetFlowWebField(FlowFieldConstants.ID_OntvangendeSoortOrganisatie);
                if (ontvangendeSoortOrganisatie != null)
                {
                    var defaultOrganizationType = FlowListValues.GetDefaultHealthCareOrganizationType((AfterCareCategoryID?)receivingDepartment.AfterCareType1.AfterCareCategoryID);
                    if (defaultOrganizationType != null)
                    {
                        ontvangendeSoortOrganisatie.Value = defaultOrganizationType.Value;
                        ontvangendeSoortOrganisatie.DisplayValue = defaultOrganizationType.Text;
                    }
                }
            }
            
            var ontvangendeDatumOverplaatsing = explanationHelper.GetFlowWebField(FlowFieldConstants.ID_OntvangendeDatumOverplaatsing);
            if (ontvangendeDatumOverplaatsing != null && string.IsNullOrEmpty(ontvangendeDatumOverplaatsing.Value))
            {
                var flowFieldIDs = new[] { FlowFieldConstants.ID_RealizedDischargeDate, FlowFieldConstants.ID_DatumEindeBehandelingMedischSpecialist, FlowFieldConstants.ID_CareBeginDate };
                var ontvangendeDatumOverplaatsingValue = explanationHelper.GetFirstFlowFieldValueNotEmpty(uow, globalProps.TransferID, flowFieldIDs);
                
                explanationHelper.FlowWebFieldSetValue(ontvangendeDatumOverplaatsing, ontvangendeDatumOverplaatsingValue);    
            }

            if (viewModel.Prefill)
            {
                var aanvraagformulier = (int)FlowFormType.AanvraagFormulierZHVVT;

                explanationHelper.FlowWebFieldFillValue(FlowFieldConstants.ID_DossierOwner, pointUserInfo.Employee.EmployeeID);

                var transfer = flowInstance.Transfer;

                var behandelaarNaam = explanationHelper.GetFlowWebField(FlowFieldConstants.ID_BehandelaarNaam);
                if (behandelaarNaam != null)
                {
                    var behandelaarNaamPrevious = FlowFieldValueBL.GetByTransferIDAndFieldID(uow, globalProps.TransferID, FlowFieldConstants.ID_BehandelaarNaam).FirstOrDefault(it => it.FormSetVersion.FormTypeID == aanvraagformulier);
                    var behandelaarNaamPreviousValue = behandelaarNaamPrevious?.Value ?? transfer.Client.GeneralPractitionerName;
                    explanationHelper.FlowWebFieldSetValue(behandelaarNaam, behandelaarNaamPreviousValue);
                }

                var medischeSituatieRedenOpname = explanationHelper.GetFlowWebField(FlowFieldConstants.ID_MedischeSituatieRedenOpname);
                if (medischeSituatieRedenOpname != null)
                {
                    var medischeSituatieRedenOpnameAanvraag = FlowFieldValueBL.GetByTransferIDAndFieldID(uow, globalProps.TransferID, FlowFieldConstants.ID_MedischeSituatieRedenOpname).FirstOrDefault(it => it.FormSetVersion.FormTypeID == aanvraagformulier);
                    explanationHelper.FlowWebFieldFillValue(FlowFieldConstants.ID_MedischeSituatieRedenOpname, medischeSituatieRedenOpnameAanvraag?.Value);
                }

                var burgerlijkeStaatValue = (int?)transfer.Client.CivilClass;
                explanationHelper.FlowWebFieldFillValue(FlowFieldConstants.ID_BurgerlijkeStaat, burgerlijkeStaatValue);

                var kinderenAanwezigAantal = explanationHelper.GetFlowWebField(FlowFieldConstants.ID_KinderenAanwezigAantal);
                if (kinderenAanwezigAantal != null)
                {
                    var childrenInHouseKeeping = transfer?.Client?.ChildrenInHousekeeping.GetValueOrDefault(0);
                    if (childrenInHouseKeeping > 0)
                    {
                        explanationHelper.FlowWebFieldFillValue(FlowFieldConstants.ID_KinderenAanwezigAantal, childrenInHouseKeeping);
                    }
                }
                
                explanationHelper.FlowWebFieldFillValue(FlowFieldConstants.ID_KinderenAanwezig, string.IsNullOrEmpty(kinderenAanwezigAantal?.Value) ? "Onbekend" : "Ja");
                explanationHelper.FlowWebFieldFillValue(FlowFieldConstants.ID_HousingType, (int?)transfer.Client.HousingType);
                
                var aanvraagFormulierValues = FlowFieldValueBL.GetByTransferIDAndFormTypeID(uow, globalProps.TransferID, aanvraagformulier);

                explanationHelper.SetFlowFieldValues(aanvraagFormulierValues);  // Activate aanvraagFormulierValues to be used in the underlying methods!

                explanationHelper.SetDestinationFormulier(FlowFieldConstants.ID_BeginInZorgBehandeling, FlowFieldConstants.ID_MedischeSituatieDatumOpname);
                explanationHelper.SetDestinationFormulier(FlowFieldConstants.ID_Prognose, FlowFieldConstants.ID_PrognoseVerwachteOntwikkeling);
                explanationHelper.SetDestinationFormulier(FlowFieldConstants.ID_InfectierisicoAanwezig, FlowFieldConstants.ID_InfectieIsolatie);
                explanationHelper.SetDestinationFormulier(FlowFieldConstants.ID_Zuurstofgebruik, FlowFieldConstants.ID_ControleLichaamsfunctiesZuurstoftoediening);
                explanationHelper.SetDestinationFormulier(FlowFieldConstants.ID_RedenInZorg, FlowFieldConstants.ID_MedischeSituatieRedenOpname);
                explanationHelper.SetDestinationFormulier(FlowFieldConstants.ID_Zorgvragen, FlowFieldConstants.ID_MedischeSituatieBehandeling);
                explanationHelper.SetDestinationFormulier(FlowFieldConstants.ID_AndereRelevanteMedischeAandoening, FlowFieldConstants.ID_MedischeSituatieVoorgeschiedenis);

                explanationHelper.CreateDagWeekToelichting(FlowFieldConstants.ID_MobiliteitVerpleegkundigeInterventies, "",
                    FlowFieldConstants.ID_BasiszorgMobiliteitKerenPerDag,
                    FlowFieldConstants.ID_BasiszorgMobiliteitKerenPerWeek,
                    FlowFieldConstants.ID_BasiszorgMobiliteitToelichting);

                var flowFieldIDsAndCaptions = new Dictionary<int, string>
                {
                    {FlowFieldConstants.ID_InfectieIsolatieSoortInfectie, "Infectie: "},
                    {FlowFieldConstants.ID_InfectieIsolatieAndersTekst, "Anders: "},
                    {FlowFieldConstants.ID_InfectieIsolatieSoortIsolatie, "Isolatie: "},
                    {FlowFieldConstants.ID_InfectieIsolatieInfectieAndersTekst, "Anders: "}
                };

                explanationHelper.CreateAlgemeneToelichting(FlowFieldConstants.ID_InfectierisicoAanwezigToelichting, flowFieldIDsAndCaptions);

                explanationHelper.CreateDagWeekToelichting(FlowFieldConstants.ID_ZuurstofgebruikToelichting, "",
                    FlowFieldConstants.ID_ControleLichaamsfunctiesZuurstoftoedieningKerenPerDag,
                    FlowFieldConstants.ID_ControleLichaamsfunctiesZuurstoftoedieningKerenPerWeek,
                    FlowFieldConstants.ID_ControleLichaamsfunctiesZuurstoftoedieningToelichting);

                explanationHelper.FlowWebFieldSetValueAndTimestampFromSourceValue(FlowFieldConstants.ID_Gewicht, FlowFieldConstants.ID_ObesitasGewicht, FlowFieldConstants.ID_DatumGewicht);
                explanationHelper.FlowWebFieldSetValueAndTimestampFromSourceValue(FlowFieldConstants.ID_Lengte, FlowFieldConstants.ID_ObesitasLengte, FlowFieldConstants.ID_DatumLengte);
                explanationHelper.FlowWebFieldFillValue(FlowFieldConstants.ID_RedenOverdragenZorg, "Patient heeft zorg nodig");

                var ontvangendeDatumOverplaatsingVO = explanationHelper.GetFlowWebField(FlowFieldConstants.ID_OntvangendeDatumOverplaatsing);
                if (ontvangendeDatumOverplaatsingVO != null)
                {
                    var careBeginDateSource = FlowFieldValueBL.GetByTransferIDAndFieldID(uow, globalProps.TransferID, FlowFieldConstants.ID_CareBeginDate).FirstOrDefault();
                    explanationHelper.FlowWebFieldSetValue(ontvangendeDatumOverplaatsingVO, careBeginDateSource?.Value);
                }

                var medicatieGebruikVO = explanationHelper.GetFlowWebField(FlowFieldConstants.ID_MedicatieGebruik);
                if (medicatieGebruikVO != null)
                {
                    var toelichting = string.Empty;

                    if (explanationHelper.GetSourceFieldValueBoolean(FlowFieldConstants.ID_MedicatieToedieningPerOs))
                    {
                        toelichting += explanationHelper.CreateDagWeekToelichting("Per os: ",
                            FlowFieldConstants.ID_MedicatieToedieningPerOsKerenPerDag,
                            FlowFieldConstants.ID_MedicatieToedieningPerOsKerenPerWeek,
                            FlowFieldConstants.ID_MedicatieToedieningPerOsToelichting);
                    }

                    if (explanationHelper.GetSourceFieldValueBoolean(FlowFieldConstants.ID_MedicatieToedieningPerInjectie))
                    {
                        toelichting += explanationHelper.CreateDagWeekToelichting("Per injectie: ",
                            FlowFieldConstants.ID_MedicatieToedieningPerInjectieKerenPerDag,
                            FlowFieldConstants.ID_MedicatieToedieningPerInjectieKerenPerWeek,
                            FlowFieldConstants.ID_MedicatieToedieningPerInjectieToelichting);
                    }

                    if (explanationHelper.GetSourceFieldValueBoolean(FlowFieldConstants.ID_MedicatieToedieningInhalatieVerneveling))
                    {
                        toelichting += explanationHelper.CreateDagWeekToelichting("Inhalatie/verneveling: ",
                            FlowFieldConstants.ID_MedicatieToedieningInhalatieVernevelingKerenPerDag,
                            FlowFieldConstants.ID_MedicatieToedieningInhalatieVernevelingKerenPerWeek,
                            FlowFieldConstants.ID_MedicatieToedieningInhalatieVernevelingToelichting);
                    }

                    if (explanationHelper.GetSourceFieldValueBoolean(FlowFieldConstants.ID_MedicatieToedieningDruppelen))
                    {
                        toelichting += explanationHelper.CreateDagWeekToelichting("Druppelen: ",
                            FlowFieldConstants.ID_MedicatieToedieningDruppelenKerenPerDag,
                            FlowFieldConstants.ID_MedicatieToedieningDruppelenKerenPerWeek,
                            FlowFieldConstants.ID_MedicatieToedieningDruppelenToelichting);
                    }

                    if (explanationHelper.GetSourceFieldValueBoolean(FlowFieldConstants.ID_MedicatieToedieningZalven))
                    {
                        toelichting += explanationHelper.CreateDagWeekToelichting("Zalven: ",
                            FlowFieldConstants.ID_MedicatieToedieningZalvenKerenPerDag,
                            FlowFieldConstants.ID_MedicatieToedieningZalvenKerenPerWeek,
                            FlowFieldConstants.ID_MedicatieToedieningZalvenToelichting);
                    }

                    if (explanationHelper.GetSourceFieldValueBoolean(FlowFieldConstants.ID_MedicatieToedieningAnders))
                    {
                        toelichting += explanationHelper.CreateDagWeekToelichting("Anders: ",
                            FlowFieldConstants.ID_MedicatieToedieningAndersKerenPerDag,
                            FlowFieldConstants.ID_MedicatieToedieningAndersKerenPerWeek,
                            FlowFieldConstants.ID_MedicatieToedieningAndersToelichting);
                    }

                    var medicatieGebruik = !string.IsNullOrEmpty(toelichting);
                    if (explanationHelper.FlowWebFieldSetValue(medicatieGebruikVO, medicatieGebruik))
                    {
                        explanationHelper.FlowWebFieldFillValue(FlowFieldConstants.ID_MedicatieGebruikToediening, toelichting);
                    }
                }

                var aanspreekpersoonVoorClient = explanationHelper.GetFlowWebField(FlowFieldConstants.ID_AanspreekpersoonVoorClient);
                if (aanspreekpersoonVoorClient != null)
                {
                    var contactPerson = ContactPersonBL.GetByClientID(uow, viewModel.ClientID.Value);
                    var cpstr = ContactPersonToString(contactPerson);
                    FlowWebFieldBL.SetValue(aanspreekpersoonVoorClient, cpstr, Source.POINT_PREFILL);
                }
                
                var overigAanvraagHulpmiddelenVO = explanationHelper.GetFlowWebField(FlowFieldConstants.ID_OverigAanvraagHulpmiddelen);
                if (overigAanvraagHulpmiddelenVO != null)
                {
                    var hulpmiddelenFormulierValues = FlowFieldValueBL.GetByTransferIDAndFormTypeID(uow, globalProps.TransferID, (int)FlowFormType.Hulpmiddelen);

                    var toelichting = string.Empty;
                    var hulpmiddelName = AidProductBL.GetOrderItemsByTransferID(uow, globalProps.TransferID);
                    if (hulpmiddelName.Any())
                    {
                        toelichting += "Hulpmiddel(en): " + string.Join(",", hulpmiddelName.Select(hm => hm.AidProduct.Name).ToArray()) + _enter;
                    }

                    // TODO: IS THIS CORRECT? aanvraagFormulierValues? or should it be hulpmiddelenFormulierValues?
                    toelichting += explanationHelper.CreateAlgemeneToelichting("Toelichting:", FlowFieldConstants.ID_NoodzakelijkTeRegelenHulpmiddelenToelichting);

                    explanationHelper.SetFlowFieldValues(hulpmiddelenFormulierValues);  // Activate hulpmiddelenFormulierValues to be used in the underlying methods!

                    var ontvanger = explanationHelper.GetSourceFieldValue(FlowFieldConstants.ID_OntvangerType);
                    if (ontvanger != null)
                    {
                        var src = FlowWebFieldBL.FromTransferIDAndFlowFieldID(uow, globalProps.TransferID, FlowFieldConstants.ID_OntvangerType, pointUserInfo, controller.ViewData);
                        if (src != null)
                        {
                            var displayTxt = src.FlowFieldDataSource.Options.Where(x => x.Value == ontvanger.Value).Select(m => m.Text).FirstOrDefault();
                            toelichting += "Type ontvanger: " + displayTxt + _enter;
                        }
                    }

                    var deliveryAddress = string.Empty;
                    deliveryAddress += explanationHelper.CreateAlgemeneToelichting("Straat:", FlowFieldConstants.ID_OntvangerStraat);
                    deliveryAddress += explanationHelper.CreateAlgemeneToelichting("Huisnummer:", FlowFieldConstants.ID_OntvangerHuisnummer);
                    deliveryAddress += explanationHelper.CreateAlgemeneToelichting("toev.:", FlowFieldConstants.ID_OntvangerHuisnummerExtra);
                    deliveryAddress += explanationHelper.CreateAlgemeneToelichting("postcode:", FlowFieldConstants.ID_OntvangerPostcode);
                    deliveryAddress += explanationHelper.CreateAlgemeneToelichting("plaats:", FlowFieldConstants.ID_OntvangerPlaats);

                    if (!string.IsNullOrEmpty(deliveryAddress))
                    {
                        toelichting = toelichting + "Afleveradres:" + _enter + deliveryAddress;
                    }

                    var deliverDate = explanationHelper.GetSourceFieldValue(FlowFieldConstants.ID_AfleverDatum);
                    if (deliverDate != null)
                    {
                        var src = FlowWebFieldBL.FromTransferIDAndFlowFieldID(uow, globalProps.TransferID, FlowFieldConstants.ID_AfleverDatum, pointUserInfo, controller.ViewData);
                        if (!string.IsNullOrEmpty(src?.DisplayValue))
                        {
                            toelichting += "Gewenste aflever datum: " + src.DisplayValue + _enter;
                        }
                    }

                    var deliverTime = explanationHelper.GetSourceFieldValue(FlowFieldConstants.ID_AfleverTijd);
                    if (deliverTime != null)
                    {
                        var src = FlowWebFieldBL.FromTransferIDAndFlowFieldID(uow, globalProps.TransferID, FlowFieldConstants.ID_AfleverTijd, pointUserInfo, controller.ViewData);
                        if (!string.IsNullOrEmpty(deliverTime?.Value))
                        {
                            toelichting += "Gewenste aflever tijd(en): " + src.DisplayValue + _enter;
                        }
                    }

                    explanationHelper.FlowWebFieldSetValue(overigAanvraagHulpmiddelenVO, toelichting);
                }

                var mobiliteitVoortbewegenHulpmiddelen = explanationHelper.GetFlowWebField(FlowFieldConstants.ID_MobiliteitVoortbewegenHulpmiddelen);
                if (mobiliteitVoortbewegenHulpmiddelen != null)
                {
                    var hulpmiddelName = AidProductBL.GetOrderItemsByTransferID(uow, globalProps.TransferID);
                    if (hulpmiddelName.Any())
                    {
                        var hmList = hulpmiddelName.Where(hm => hm.AidProduct.Name.ToLower().Contains("rollator") || hm.AidProduct.Name.ToLower().Contains("krukken")).Select(hm => hm.AidProduct.Name);
                        var hms = string.Join(",", hmList.ToArray()) + _enter;
                        explanationHelper.FlowWebFieldSetValue(mobiliteitVoortbewegenHulpmiddelen, hms);
                    }
                }

                explanationHelper.SetFlowFieldValues(aanvraagFormulierValues);      // Activate aanvraagFormulierValues to be used in the underlying methods!

                explanationHelper.CreateDagWeekToelichting(FlowFieldConstants.ID_WassenVerpleegkundigeInterventies, "Wassen: ",
                    FlowFieldConstants.ID_BasiszorgPersoonlijkeVerzorgingKerenPerDag,
                    FlowFieldConstants.ID_BasiszorgPersoonlijkeVerzorgingKerenPerWeek,
                    FlowFieldConstants.ID_BasiszorgPersoonlijkeVerzorgingToelichting);

                explanationHelper.CreateDagWeekToelichting(FlowFieldConstants.ID_UitkledenVerpleegkundigeInterventies, "Uitkleden: ",
                    FlowFieldConstants.ID_BasiszorgPersoonlijkeVerzorgingKerenPerDag,
                    FlowFieldConstants.ID_BasiszorgPersoonlijkeVerzorgingKerenPerWeek,
                    FlowFieldConstants.ID_BasiszorgPersoonlijkeVerzorgingToelichting);

                explanationHelper.CreateDagWeekToelichting(FlowFieldConstants.ID_ToiletgangVerpleegkundigeInterventies, "Toiletgang: ",
                    FlowFieldConstants.ID_BasiszorgToiletgangKerenPerDag,
                    FlowFieldConstants.ID_BasiszorgToiletgangKerenPerWeek,
                    FlowFieldConstants.ID_BasiszorgToiletgangToelichting);

                explanationHelper.CreateDagWeekToelichting(FlowFieldConstants.ID_EtenDrinkenVerpleegkundigeInterventies, "Eten/ drinken: ",
                    FlowFieldConstants.ID_BasiszorgEtenEnDrinkenKerenPerDag,
                    FlowFieldConstants.ID_BasiszorgEtenEnDrinkenKerenPerWeek,
                    FlowFieldConstants.ID_BasiszorgEtenEnDrinkenToelichting);

                var voedingVerpleegkundigeInterventies = explanationHelper.GetFlowWebField(FlowFieldConstants.ID_VoedingVerpleegkundigeInterventies);
                if (voedingVerpleegkundigeInterventies != null)
                {
                    var toelichting = explanationHelper.CreateDagWeekToelichting("Parenterale voeding: ",
                        FlowFieldConstants.ID_InfusietherapieParenteraleVoedingKerenPerDag,
                        FlowFieldConstants.ID_InfusietherapieParenteraleVoedingKerenPerWeek,
                        FlowFieldConstants.ID_InfusietherapieParenteraleVoedingToelichting);

                    toelichting += explanationHelper.CreateDagWeekToelichting("Sondevoeding: ",
                        FlowFieldConstants.ID_SondevoedingKerenPerDag,
                        FlowFieldConstants.ID_SondevoedingKerenPerWeek,
                        FlowFieldConstants.ID_SondevoedingToelichting);

                    explanationHelper.FlowWebFieldSetValue(voedingVerpleegkundigeInterventies, toelichting);
                }

                var toedieningVerpleegkundigeInterventies = explanationHelper.GetFlowWebField(FlowFieldConstants.ID_ToedieningVerpleegkundigeInterventies);
                if (toedieningVerpleegkundigeInterventies != null)
                {
                    var toelichting = explanationHelper.CreateDagWeekToelichting("Parenterale toediening medicatie/pijnbestrijding: ",
                        FlowFieldConstants.ID_InfusietherapieParenteraleToedieningMedicatieKerenPerDag,
                        FlowFieldConstants.ID_InfusietherapieParenteraleToedieningMedicatieKerenPerWeek,
                        FlowFieldConstants.ID_InfusietherapieParenteraleToedieningMedicatieToelichting);

                    toelichting += explanationHelper.CreateDagWeekToelichting("PICC: ",
                        FlowFieldConstants.ID_InfusietherapiePICCKerenPerDag,
                        FlowFieldConstants.ID_InfusietherapiePICCKerenPerWeek,
                        FlowFieldConstants.ID_InfusietherapiePICCToelichting);

                    toelichting += explanationHelper.CreateDagWeekToelichting("Centraal Veneuze lijn: ",
                        FlowFieldConstants.ID_InfusietherapieCentraalVeneuzeLijnKerenPerDag,
                        FlowFieldConstants.ID_InfusietherapieCentraalVeneuzeLijnKerenPerWeek,
                        FlowFieldConstants.ID_InfusietherapieCentraalVeneuzeLijnToelichting);

                    explanationHelper.FlowWebFieldSetValue(toedieningVerpleegkundigeInterventies, toelichting);
                }

                explanationHelper.CreateDagWeekToelichting(FlowFieldConstants.ID_UitscheidingUrineKatheterToelichting, "Suprapubis katheter: ",
                    FlowFieldConstants.ID_CatheterVerzorgingSuprapubisCatheterKerenPerDag,
                    FlowFieldConstants.ID_CatheterVerzorgingSuprapubisCatheterKerenPerWeek,
                    FlowFieldConstants.ID_CatheterVerzorgingSuprapubisCatheterToelichting);

                var huidWondAanwezig = explanationHelper.GetFlowWebField(FlowFieldConstants.ID_HuidWondAanwezig);
                if (huidWondAanwezig != null)
                {
                    var wondverzorging = explanationHelper.GetSourceFieldValue(FlowFieldConstants.ID_Wondverzorging);
                    var valAf = wondverzorging?.Value.StringToBool();
                    if (valAf == true)
                    {
                        explanationHelper.FlowWebFieldSetValue(huidWondAanwezig, true);

                        var toelichting = explanationHelper.CreateDagWeekToelichting("",
                            FlowFieldConstants.ID_WondverzorgingKerenPerDag,
                            FlowFieldConstants.ID_WondverzorgingKerenPerWeek,
                            FlowFieldConstants.ID_WondverzorgingToelichting);

                        explanationHelper.FlowWebFieldFillValue(FlowFieldConstants.ID_HuidWondBehandeling, toelichting);
                        explanationHelper.FlowWebFieldFillValue(FlowFieldConstants.ID_HuidWondSoort, "OTH");
                    }
                }

                explanationHelper.CreateDagWeekToelichting(FlowFieldConstants.ID_UitscheidingStomaToelichting, "",
                    FlowFieldConstants.ID_StomaverzorgingKerenPerDag,
                    FlowFieldConstants.ID_StomaverzorgingKerenPerWeek,
                    FlowFieldConstants.ID_StomaverzorgingToelichting);

                explanationHelper.CreateDagWeekToelichting(FlowFieldConstants.ID_UitscheidingVerpleegkundigeInterventies, "",
                    FlowFieldConstants.ID_StomaverzorgingKerenPerDag,
                    FlowFieldConstants.ID_StomaverzorgingKerenPerWeek,
                    FlowFieldConstants.ID_StomaverzorgingToelichting);

                var hulpVanAnderenMantelzorgAard = explanationHelper.GetFlowWebField(FlowFieldConstants.ID_HulpVanAnderenMantelzorgAard);
                if (hulpVanAnderenMantelzorgAard != null)
                {
                    var toelichting = explanationHelper.CreateMantelZorgBijdrage(uow, globalProps.TransferID);
                    explanationHelper.FlowWebFieldSetValue(hulpVanAnderenMantelzorgAard, toelichting);
                }

                // #13459 VO - voorvullen extra velden
                var medebehandelaars = FlowFieldValueBL.GetByTransferIDAndFieldIDs(uow, globalProps.TransferID, new[]
                {
                    FlowFieldConstants.ID_IngeschakeldeOndersteunendeDienstenGeriater,
                    FlowFieldConstants.ID_IngeschakeldeOndersteunendeDienstenPsychiater,
                    FlowFieldConstants.ID_IngeschakeldeOndersteunendeDienstenMedischMaatschappelijkWerk,
                    FlowFieldConstants.ID_IngeschakeldeOndersteunendeDienstenGespecialiseerdVerpleegkundige,
                    FlowFieldConstants.ID_IngeschakeldeOndersteunendeDienstenDietist,
                    FlowFieldConstants.ID_IngeschakeldeOndersteunendeDienstenLogopedist,
                    FlowFieldConstants.ID_IngeschakeldeOndersteunendeDienstenFysiotherapeut,
                    FlowFieldConstants.ID_IngeschakeldeOndersteunendeDienstenErgotherapeut,
                    FlowFieldConstants.ID_IngeschakeldeOndersteunendeDienstenPedagogischMedewerker,
                    FlowFieldConstants.ID_IngeschakeldeOndersteunendeDienstenRevalidatiearts
                }).Where(it => it.FormSetVersion.FormTypeID == aanvraagformulier).ToList();

                var medebehandelaarvalues = medebehandelaars.Where(ffv => string.Equals(ffv.Value, FlowFieldConstants.Value_True, StringComparison.InvariantCultureIgnoreCase))
                    .Select(ffv => ffv.FlowField.Label).ToList();

                var ingeschakeldeondersteunendedienstenanders = FlowFieldValueBL.GetByTransferIDAndFieldID(uow, globalProps.TransferID, FlowFieldConstants.ID_IngeschakeldeOndersteunendeDienstenAnders)
                    .FirstOrDefault(it => it.FormSetVersion.FormTypeID == aanvraagformulier);

                if (ingeschakeldeondersteunendedienstenanders != null && string.Equals(ingeschakeldeondersteunendedienstenanders.Value, FlowFieldConstants.Value_True, StringComparison.InvariantCultureIgnoreCase))
                {
                    var ingeschakeldeondersteunendedienstentoelichting = FlowFieldValueBL.GetByTransferIDAndFieldID(uow, globalProps.TransferID, FlowFieldConstants.ID_IngeschakeldeOndersteunendeDienstenToelichting)
                        .FirstOrDefault(it => it.FormSetVersion.FormTypeID == aanvraagformulier);
                    if (!string.IsNullOrWhiteSpace(ingeschakeldeondersteunendedienstentoelichting?.Value))
                    {
                        medebehandelaarvalues.Add(ingeschakeldeondersteunendedienstentoelichting.Value);
                    }
                }

                if (medebehandelaarvalues.Any())
                {
                    explanationHelper.FlowWebFieldFillValue(FlowFieldConstants.ID_ZijnMedeBehandelaars, FlowFieldConstants.Value_Ja);
                    explanationHelper.FlowWebFieldFillValue(FlowFieldConstants.ID_DisciplineMedeBehandelaars, string.Join(";", medebehandelaarvalues));
                }
            }

            viewModel.ShowPalliatief = sendingDepartment?.Location?.Organization.UsesPalliativeCareInVO == true;

            if (viewModel.ShowPalliatief && globalProps.FormSetVersionID <= 0)
            {
                var prognoseVerwachteOntwikkeling = FlowFieldValueBL.GetByTransferIDAndFieldID(uow, globalProps.TransferID, FlowFieldConstants.ID_PrognoseVerwachteOntwikkeling).FirstOrDefault();
                bool palliatieveaftercare = AfterCareTypeBL.IsPalliatief(uow, globalProps.TransferID);
                bool kortelevensduur = prognoseVerwachteOntwikkeling?.Value == FlowFieldDataSourceConstants.PrognoseRHD.LifeExpectancyLessThan3Months ||
                                       prognoseVerwachteOntwikkeling?.Value == FlowFieldDataSourceConstants.PrognoseRHD.LifeExpectancyLessThan1Year;

                if (palliatieveaftercare || kortelevensduur)
                {
                    FlowWebFieldBL.SetValue(flowWebFields, FlowFieldConstants.ID_SprakeVanPalliatieveZorg, FlowFieldConstants.Value_Ja, Source.POINT_PREFILL);

                    if (palliatieveaftercare && kortelevensduur)
                    {
                        FlowWebFieldBL.SetValue(flowWebFields, FlowFieldConstants.ID_LevensVerwachting, "Maanden", Source.POINT_PREFILL);
                    }
                }
                else
                {
                    FlowWebFieldBL.SetValue(flowWebFields, FlowFieldConstants.ID_SprakeVanPalliatieveZorg, FlowFieldConstants.Value_Nee);
                }
            }

            flowWebFields = explanationHelper.GetUpdatedFlowWebFields();

            if (globalProps.FormSetVersionID <= 0 && endPointConfiguration != null)
            {
                var endpointhelper = new EndpointConfigurationHelper(endPointConfiguration, client);
                var copyflowwebfields = endpointhelper.Merge(flowWebFields, new List<Source> {Source.USER}).ToList();
                viewModel.Global.FlowWebFields = copyflowwebfields;
                return viewModel;
            }
            
            viewModel.Global.FlowWebFields = flowWebFields;

            return viewModel;
        }

        private static bool TakeOverFromRelatedTransfer(IUnitOfWork uow, List<FlowWebField> destinationWebFields, FlowInstance destinationFlowInstance, FlowInstance relatedFlowInstance, int destinationFormTypeID)
        {
            var result = false;
            if (relatedFlowInstance == null)
            {
                return false;
            }

            var source = FormSetVersionBL.GetByTransferID(uow, relatedFlowInstance.TransferID)?.Where(fsv => fsv.FormTypeID == (int)FlowFormType.VerpleegkundigeOverdrachtFlow).FirstOrDefault();
            if (source == null)
            {
                return false;
            }

            var sourceFlowFieldValues = uow.FlowFieldValueRepository.Get(ffv => ffv.FlowInstance.TransferID == relatedFlowInstance.TransferID && ffv.FormSetVersionID == source.FormSetVersionID, asNoTracking: true);

            var destinationFlowFields = uow.FlowDefinitionFormTypeFlowFieldRepository.Get(fff => fff.TakeOver && fff.FormTypeID == destinationFormTypeID && fff.FlowDefinitionID == (int)destinationFlowInstance.FlowDefinitionID);
            foreach (var flowFieldValue in sourceFlowFieldValues)
            {
                if (destinationFlowFields.Any(fff => fff.FlowFieldID == flowFieldValue.FlowFieldID) && !string.IsNullOrEmpty(flowFieldValue.Value))
                {
                    FlowWebFieldBL.SetValue(destinationWebFields, flowFieldValue.FlowFieldID, flowFieldValue.Value, Source.POINT_PREFILL);
                    result = true;
                }
            }
            return result;
        }

        public static SendOverdrachtViewModel GetSendOverdrachtViewModel(IUnitOfWork uow, GlobalProperties globalProps)
        {
            var viewModel = new SendOverdrachtViewModel();
            viewModel.SetGlobalProperties(globalProps);

            var flowInstance = globalProps.FlowInstance;
            if (flowInstance == null)
            {
                return viewModel;
            }

            var previousInvitesToBeAcknowledgedByFlowInstanceID = OrganizationInviteBL.GetPreviousInvitesToBeAcknowledgedByFlowInstanceID(uow, flowInstance.FlowInstanceID);

            viewModel.DisplayHasAccepted = previousInvitesToBeAcknowledgedByFlowInstanceID.Any();
            viewModel.HasAccepted = PatientHelper.IsAccepted(uow, flowInstance.FlowInstanceID) == AcceptanceState.Acknowledged;

            var voPhaseInstance = PhaseInstanceBL.GetVoPhaseInstance(uow, flowInstance);
            viewModel.VOIsDone = voPhaseInstance?.Status == (int)Status.Done;

            var versturenVoPhaseDefinition = PhaseDefinitionCacheBL.GetByFlowDefinitionAndFormType(uow, flowInstance.FlowDefinitionID, (int)FlowFormType.VersturenVO);
            if (versturenVoPhaseDefinition == null)
            {
                return viewModel;
            }

            var versturenVoPhaseInstance = PhaseInstanceBL.GetByFlowInstanceIDAndPhaseDefinition(uow, flowInstance.FlowInstanceID, versturenVoPhaseDefinition.PhaseDefinitionID);
            if (versturenVoPhaseInstance == null)
            {
                return viewModel;
            }

            viewModel.VersturenVoRealizedDate = versturenVoPhaseInstance.RealizedDate.ToPointDateHourDisplay();
            viewModel.VersturenVoRealizedEmployee = versturenVoPhaseInstance.RealizedByEmployee.FullName();

            var voPhaseDefinition = PhaseDefinitionCacheBL.GetByFlowDefinitionAndFormType(uow, flowInstance.FlowDefinitionID, (int)FlowFormType.VerpleegkundigeOverdrachtFlow);
            if (voPhaseDefinition != null)
            {
                viewModel.VOPhaseDefinitionID = voPhaseDefinition.PhaseDefinitionID;
            }

            return viewModel;
        }

    }

}