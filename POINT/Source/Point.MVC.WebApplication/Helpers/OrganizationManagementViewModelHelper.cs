﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Routing;
using Point.Business.Helper;
using Point.Business.Logic;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Infrastructure.Attributes;
using Point.Models.Enums;
using Point.MVC.WebApplication.Areas.Management.Controllers;

namespace Point.MVC.WebApplication.Helpers
{
    public static class OrganizationManagementViewModelHelper
    {
        public static OrganizationViewModel GetOrganizationEditViewModel(OrganizationController controller, int? organizationID)
        {
            var model = organizationID.HasValue ? OrganizationBL.GetByOrganizationID(controller.uow, organizationID.Value) : null;

            var pointUserInfo = controller.PointUserInfo();
            var maxLevel = pointUserInfo.GetFunctionRoleLevelID(FunctionRoleTypeID.ManageOrganization);
            var regions = new List<Region>();

            switch (maxLevel)
            {
                case FunctionRoleLevelID.Global:
                    regions = RegionBL.GetAll(controller.uow).OrderBy(r => r.Name).ToList();
                    break;
                case FunctionRoleLevelID.Department:
                case FunctionRoleLevelID.Location:
                case FunctionRoleLevelID.Organization:
                case FunctionRoleLevelID.Region:
                    regions.Add(pointUserInfo.Region);
                    break;
            }

            var cryptoCertificates = CryptoCertificateBL.GetAll(controller.uow).OrderBy(n => n.FriendlyName).ToList();

            List<OrganizationType> organizationTypes;
            if (model != null && maxLevel < FunctionRoleLevelID.Region)
            {
                organizationTypes = new List<OrganizationType> { model.OrganizationType };
            }
            else
            {
                organizationTypes = OrganizationTypeBL.GetOrganizationTypes(controller.uow).OrderBy(ot => ot.Name).ToList();
            }

            var viewModel = OrganizationViewBL.FromModel(controller.uow, model);          

            if (organizationTypes.Any())
            {
                organizationTypes.ForEach(orgType => viewModel.OrganizationTypeOptions.Add(new Option(orgType.Name, orgType.OrganizationTypeID.ToString())));
            }

            if (regions.Any())
            {
                regions.ForEach(reg => viewModel.RegionOptions.Add(new Option(reg.Name, reg.RegionID.ToString())));
            }

            viewModel.CryptoCertificateOptions.Add(LookUpBL.OptionEmpty);
            cryptoCertificates.ForEach(cryptCert => viewModel.CryptoCertificateOptions.Add(new Option(cryptCert.FriendlyName, cryptCert.CryptoCertificateID.ToString())));

            #region FormTypes
            //flowDefinition dropdown 
            //controller.ViewBag.FlowDefinitions = FlowDefinitionBL.GetFlowDefinitions(controller.uow, true).ToList();    // TODO: Determine if this is used anywhere!

            #endregion
            
            return viewModel;
        }

        public static OrganizationManagementViewModel GetIndexViewModel(OrganizationController controller, string search, bool searchDeleted, bool onlyOrganizations)
        {
            var viewModel = GetOrganizationManagementViewModel(controller, search, searchDeleted, onlyOrganizations);
            viewModel.OrganizationLocationDepartments = viewModel.OrganizationLocationDepartments.GroupBy(it => it.LocationID).Select(it => it.FirstOrDefault()).ToList();
            return viewModel;
        }

        public static OrganizationManagementViewModel GetListViewModel(OrganizationController controller, string search, bool searchDeleted, bool onlyOrganizations)
        {
            return GetOrganizationManagementViewModel(controller, search, searchDeleted, onlyOrganizations);
        }
        public static object Save(OrganizationController controller, OrganizationViewModel viewmodel)
        {
            var pointUserInfo = controller.PointUserInfo();

            OrganizationViewBL.ValidateViewModel(controller.uow, pointUserInfo, viewmodel);

            var success = false;
            var organizationID = 0;
            var organizationProjects = new List<object>();      // For Json

            if (controller.ModelState.IsValid)
            {
                var logEntry = LogEntryBL.Create(FlowFormType.AdminOrganization, pointUserInfo);
                var organization = OrganizationBL.Save(controller.uow, viewmodel, logEntry);
                organizationID = organization.OrganizationID;

                foreach (var op in organization.OrganizationProjects)
                {
                    if (op.OrganizationProjectID != -1)
                    {
                        organizationProjects.Add(new {op.OrganizationProjectID, op.Name });
                    }
                }

                OrganizationUpdateBL.CalculateForOrganizationAsync(organization.OrganizationID);
                //OrganizationUpdateBL.CapManForOrganizationAsync(organization.OrganizationID);
                success = true;
            }

            return new
            {
                success,
                OrganizationID = organizationID,
                OrganizationProjects = organizationProjects,
                Digest = AntiFiddleInjection.CreateDigest(organizationID),
                Url = controller.Url.Action("Index", "Organization", new RouteValueDictionary { { "Area", "Management" } })
            };
        }

        public static object UpdateOrganizationLogo(OrganizationController controller, int organizationID)
        {
            var httpFileCollectionHelper = new HttpFileCollectionBaseHelper {MaxContentLength = 1 * 1024 * 1024};
            var extensions = new List<string>(new[] { ".gif", ".jpe", ".jpeg", ".jpg", ".png", ".bmp" });
            if (httpFileCollectionHelper.CreateDatabaseImage(controller.Request.Files, extensions, true))
            {
                var org = OrganizationBL.GetByOrganizationID(controller.uow, organizationID);
                var logEntry = LogEntryBL.Create(FlowFormType.AdminOrganization, controller.PointUserInfo());

                OrganizationLogoBL.Save(controller.uow, org, httpFileCollectionHelper.DatabaseImage, logEntry);
            }

            bool success = string.IsNullOrWhiteSpace(httpFileCollectionHelper.Error);

            return new
            {
                Success = success,
                ErrorMessage = httpFileCollectionHelper.Error
            };
        }

        public static object DeleteOrganizationLogo(OrganizationController controller, int organizationid)
        {
            var errorMessage = string.Empty;
            var org = OrganizationBL.GetByOrganizationID(controller.uow, organizationid);
            var logEntry = LogEntryBL.Create(FlowFormType.AdminOrganization, controller.PointUserInfo());

            try
            {
                OrganizationLogoBL.Delete(controller.uow, org, logEntry);
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
            }
            
            return new
            {
                Success = string.IsNullOrWhiteSpace(errorMessage),
                ErrorMessage = errorMessage
            };
        }

        private static OrganizationManagementViewModel GetOrganizationManagementViewModel(OrganizationController controller, string search, bool searchDeleted, bool onlyOrganizations)
        {
            var viewModel = new OrganizationManagementViewModel();
            var pointUserInfo = controller.PointUserInfo();
            var accessLevel = FunctionRoleHelper.GetAccessLevel(controller.uow, FunctionRoleTypeID.ManageOrganization, pointUserInfo);

            int? regionID = null;
            int[] organizationIDs = null;
            int[] locationIDs = null;
            int[] departmentIDs = null;

            switch (accessLevel.MaxLevel)
            {
                case FunctionRoleLevelID.Department:
                    departmentIDs = pointUserInfo.EmployeeDepartmentIDs.ToArray();
                    break;
                case FunctionRoleLevelID.Location:
                    locationIDs = accessLevel.LocationIDs;
                    break;
                case FunctionRoleLevelID.Organization:
                    organizationIDs = accessLevel.OrganizationIDs;
                    break;
                case FunctionRoleLevelID.Region:
                    regionID = pointUserInfo.Region.RegionID;
                    break;
            }

            viewModel.FunctionRoleLevelID = accessLevel.MaxLevel;
            viewModel.Search = search;
            viewModel.SearchDeleted = searchDeleted;
            viewModel.OnlyOrganizations = onlyOrganizations;

            var level = onlyOrganizations ? FunctionRoleLevelID.Region : FunctionRoleLevelID.Organization;

            if (accessLevel.MaxLevel > level && string.IsNullOrEmpty(search))
            {
                return viewModel;
            }

            viewModel.OrganizationLocationDepartments = OrganizationBL
                .Search(controller.uow, search, regionID, organizationIDs, locationIDs, departmentIDs, searchDeleted)
                .ToList();

            var previousOrganizationID = 0;
            var previousLocationID = 0;

            foreach (var row in viewModel.OrganizationLocationDepartments)
            {
                row.ShowOrganization = previousOrganizationID != row.OrganizationID;
                row.ShowLocation = previousLocationID != row.LocationID && row.LocationID.HasValue;
                var showDepartment = row.DepartmentID.HasValue;

                previousOrganizationID = row.OrganizationID;
                previousLocationID = row.LocationID.GetValueOrDefault();

                row.IsLocationEditAllowed = viewModel.HasLocationEditRights && row.ShowLocation;
                row.IsDepartmentEditAllowed = viewModel.HasDepartmentEditRights && showDepartment;
                row.IsLocationInsertAllowed = viewModel.HasOrganizationEditRights && row.ShowOrganization;
                row.IsDepartmentInsertAllowed = row.IsLocationEditAllowed;      // TODO: VERIFY WHETHER THIS IS OK!
            }

            return viewModel;
        }
    }
}