﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Point.Business.Expressions;
using Point.Business.Helper;
using Point.Business.Logic;
using Point.Database.Extensions;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Constants;
using Point.Infrastructure.Extensions;
using Point.Models.Enums;
using Point.MVC.WebApplication.Controllers;

namespace Point.MVC.WebApplication.Helpers
{
    public static class TransferViewModelHelper
    {
        private const int _PAGESIZE = 50;
        public static TransferOverviewViewModel GetOverviewViewModel(TransferController controller, SearchUrlViewModel searchUrlViewModel)
        {
            var pointUserInfo = controller.PointUserInfo();
            var locationID = pointUserInfo.Location.LocationID;
            var departmentID = pointUserInfo.Department.DepartmentID;

            var searchQueryHandled = SearchQueryBL.GetSearchQuerySimple(controller.uow);

            var flowDefinitionsReceiver = FlowDefinitionBL.GetRecievingFlowDefinitionsByLocationIDDepartmentID(controller.uow, locationID, departmentID).ToList();
            var flowDefinitionsSender = FlowDefinitionBL.GetSendingFlowDefinitionsByLocationIDDepartmentID(controller.uow, locationID, departmentID).ToList();

            if (searchUrlViewModel.FlowDefinitionsReceiver == null && searchUrlViewModel.FlowDefinitionsSender == null)
            {
                searchUrlViewModel.FlowDirection = (pointUserInfo.Organization.IsHealthCareProvider() || pointUserInfo.Organization.IsCIZ() || pointUserInfo.Organization.IsGRZ()) ? FlowDirection.Receiving : FlowDirection.Sending;
                searchUrlViewModel.FlowDefinitionsReceiver = searchUrlViewModel.FlowDirection == FlowDirection.Receiving ? flowDefinitionsReceiver.Select(fd => fd.FlowDefinitionID).ToArray() : new FlowDefinitionID[] { };
                searchUrlViewModel.FlowDefinitionsSender = searchUrlViewModel.FlowDirection == FlowDirection.Receiving ? new FlowDefinitionID[] { } : flowDefinitionsSender.Select(fd => fd.FlowDefinitionID).ToArray();
            }

            searchQueryHandled.ApplyFilter(pointUserInfo, DossierStatus.Active, searchUrlViewModel.FlowDirection);
            searchQueryHandled.ApplyHandlingFilter(DossierHandling.Handling, pointUserInfo);

            var handledCount = searchQueryHandled.Queryable.Count();

            var searchQueryNotHandled = SearchQueryBL.GetSearchQuerySimple(controller.uow);
            searchQueryNotHandled.ApplyFilter(pointUserInfo, DossierStatus.Active, searchUrlViewModel.FlowDirection);
            searchQueryNotHandled.ApplyHandlingFilter(DossierHandling.NotHandling, pointUserInfo);

            var notHandledCount = searchQueryNotHandled.Queryable.Count();

            var signalering = SignaleringBL.GetCurrentByPointUserInfo(controller.uow, pointUserInfo);
            var signaleringCount = signalering.Count();

            var viewModel = new TransferOverviewViewModel
            {
                NotHandled = notHandledCount,
                Handled = handledCount,
                SignalCount = signaleringCount
            };

            return viewModel;
        }

        public static TransferSearchViewModel GetSearchViewModel(TransferController controller, SearchUrlViewModel searchUrlViewModel)
        {
            var pointUserInfo = controller.PointUserInfo();
            var locationID = pointUserInfo.Location.LocationID;
            var departmentID = pointUserInfo.Department.DepartmentID;

            var flowDefinitionsReceiver = FlowDefinitionBL.GetRecievingFlowDefinitionsByLocationIDDepartmentID(controller.uow, locationID, departmentID).ToList();
            var flowDefinitionsSender = FlowDefinitionBL.GetSendingFlowDefinitionsByLocationIDDepartmentID(controller.uow, locationID, departmentID).ToList();

            if (searchUrlViewModel.FlowDefinitionsReceiver == null && searchUrlViewModel.FlowDefinitionsSender == null)
            {
                searchUrlViewModel.FlowDirection = (pointUserInfo.Organization.IsHealthCareProvider() || pointUserInfo.Organization.IsCIZ() || pointUserInfo.Organization.IsGRZ()) ? FlowDirection.Receiving : FlowDirection.Sending;
                searchUrlViewModel.FlowDefinitionsReceiver = searchUrlViewModel.FlowDirection == FlowDirection.Receiving ? flowDefinitionsReceiver.Select(fd => fd.FlowDefinitionID).ToArray() : new FlowDefinitionID[] { };
                searchUrlViewModel.FlowDefinitionsSender = searchUrlViewModel.FlowDirection == FlowDirection.Receiving ? new FlowDefinitionID[] { } : flowDefinitionsSender.Select(fd => fd.FlowDefinitionID).ToArray();
            }

            if (controller.Session[SessionIdentifiers.Point.ForceOld] != null)
            {
                controller.Session.Remove(SessionIdentifiers.Point.ForceOld);
            }

            if (searchUrlViewModel.Reset)
            {
                SearchControlBL.DeleteState();
            }

            if (controller.IsMobileDevice())
            {
                searchUrlViewModel.SearchType = SearchType.Mobile;
            }

            var searchUIConfiguration = SearchUIConfigurationBL.GetConfigurationOrDefault(controller.uow, searchUrlViewModel.FlowDefinitionIDs, searchUrlViewModel.SearchType, pointUserInfo); //NOTE 17-12-15: flowDefinitionID's is not used yet.

            var searchControl = SearchControlBL.GetState(controller.HttpContext) ?? new SearchControl
            {
                SortColumn = searchUIConfiguration.SortOrder.Field.Name,
                SortOrder = searchUIConfiguration.SortOrder.OrderByDescending ? SortOrder.desc : SortOrder.asc,
                FlowDefinitionsReceiver = searchUrlViewModel.FlowDefinitionsReceiver,
                FlowDefinitionsSender = searchUrlViewModel.FlowDefinitionsSender,
                FlowDirection = searchUrlViewModel.FlowDirection,
                type = searchUrlViewModel.Type,
                statusFilter = searchUrlViewModel.Status,
                frequencyStatusFilter = searchUrlViewModel.FrequencyStatus,
                date = new string[2],
                input = new string[2],
                number = new string[2],
                matchtype = new int[2],
                DepartmentIDs = GetDefaultDepartmentSelection(controller, pointUserInfo)
            };

            searchControl.Departments = GetDepartments(controller, pointUserInfo);
            searchControl.frequencyStatus = searchUrlViewModel.FrequencyStatus;
            searchControl.handling = searchUrlViewModel.Handling;
            searchControl.status = searchUrlViewModel.Status;
            searchControl.SearchType = searchUrlViewModel.SearchType;

            var searchFieldOwningType = SearchFieldOwningType.Transfer;
            var searchUiFieldConfiguration = searchUIConfiguration.Fields.FirstOrDefault(f => f.Field.Name == searchControl.SortColumn);
            if (searchUiFieldConfiguration != null)
            {
                searchFieldOwningType = searchUiFieldConfiguration.SearchFieldOwningType;
            }
            searchControl.SearchFieldOwningType = searchFieldOwningType;

            var properties = searchUIConfiguration.Fields.Where(p => p.UseInSearchFilter).OrderBy(it => it.Column.Index).ToList();
            var defaultProp = properties.FirstOrDefault(it => it.SelectedInSearchFilter) ?? properties.FirstOrDefault();

            if (searchControl.propertyList == null)
            {
                searchControl.propertyList = new string[2];
            }
            if (searchControl.matchtype == null)
            {
                searchControl.matchtype = new int[2];
            }

            if (defaultProp != null)
            {
                for (var i = 0; i < searchControl.propertyList.Length; i++)
                {
                    if (string.IsNullOrEmpty(searchControl.propertyList[i]))
                    {
                        searchControl.propertyList[i] = defaultProp.Field.Name;
                    }
                }
            }

            var viewModel = new TransferSearchViewModel
            {
                AutoRefreshMilliseconds = searchUIConfiguration.AutoRefreshSeconds * 1000,
                ReceiverFlowDefinitions = flowDefinitionsReceiver,
                SenderFlowDefinitions = flowDefinitionsSender,
                ActivePhases = GetActivePhaseList(controller, searchControl.FlowDefinitionIDs, searchControl.phases),
                SearchProperties = properties,
                NoneSortableColumns = searchUIConfiguration.Fields.Where(sc => sc.Column.IsSortable == false).Select(sc => sc.Field.Name),
                NoFilterOptions = ExternalPatientHelper.HaveConnectionDifferentMenu(pointUserInfo.Organization.HL7ConnectionDifferentMenu.ConvertTo<bool>()),
                PatExternalReference = ExternalPatientHelper.GetPatExternalReference(),
                PatExternalReferenceBSN = ExternalPatientHelper.GetPatExternalReferenceBSN(),
                SearchControl = searchControl
            };

            //viewModel.SearchLogicOperators = EnumHelper.SelectListFor<SearchLogicOperatorType>();
            //viewModel.SearchLogicOperators = new List<SearchLogicOperatorType>();   // EnumHelper.SelectListFor<SearchLogicOperatorType>();

            return viewModel;
        }

        public static SearchViewModel GetSearchViewModel(IEnumerable<FlowInstanceSearchViewModel> results, int count, SearchStatement searchStatement, SearchUIConfiguration searchuiconfig, bool usePagination)
        {
            var searchVM = new SearchViewModel
            {
                ItemsTable = results,
                UsePaging = usePagination,
                ResultCount = count,
                PageCount = (count / _PAGESIZE) + 1,
                CurrentPage = searchStatement.Page,
                SortOrder = searchStatement.SortOrder.ToString(),
                SortColumn = searchStatement.SortColumn,
                CurrentSearchType = (SearchType) Enum.Parse(typeof(SearchType), searchuiconfig.SearchType)
            };

            searchVM.IsClickable = searchVM.CurrentSearchType != SearchType.PatientOverview || !searchVM.HavePatExternalReference;
            searchVM.Fields = searchuiconfig.Fields.Where(it => it.Column.IsVisible).OrderBy(it => it.Column.Index);
            SetPaging(searchVM);

            return searchVM;
        }

        private static void SetPaging(SearchViewModel searchVM)
        {
            if (!searchVM.UsePaging)
            {
                return;
            }

            const int numberOfPageButtons = 9;

            var startPage = 1;
            var endPage = searchVM.PageCount;

            if (searchVM.PageCount > numberOfPageButtons)
            {
                startPage = searchVM.CurrentPage - 4;
                endPage = searchVM.CurrentPage + 4;
                if (startPage < 1)
                {
                    startPage = 1;
                    endPage = startPage + numberOfPageButtons - 1;
                }
                if (endPage > searchVM.PageCount)
                {
                    endPage = searchVM.PageCount;
                    startPage = searchVM.PageCount - numberOfPageButtons - 1;
                }
            }

            searchVM.StartPage = startPage;
            searchVM.EndPage = endPage;
        }

        public static void SetAnonymous(List<FlowInstanceSearchViewModel> results, int searchingorganizationid)
        {
            var toMakeAnonymous = results.Where(it => it.FlowInstanceSearchValues.FlowInstanceOrganization.Any(dep => dep.OrganizationID == searchingorganizationid && dep.ClientIsAnonymous));
            foreach (var item in toMakeAnonymous)
            {
                item.Anonymous = true;
            }
        }

        public static void ApplyFilters(TransferController controller, SearchControl searchControl, SearchQueryBL searchQueryBL, PointUserInfo pointUserInfo)
        {
            if (searchControl.phases != null)
            {
                if (searchControl.FlowDefinitionIDs.Length > 0) // Point.Flow
                {
                    var phaseDefinitionIDs = new List<int>(searchControl.phases.Length);
                    foreach (var phaseDefinitionID in searchControl.phases)
                    {
                        phaseDefinitionIDs.Add(int.Parse(phaseDefinitionID));
                    }

                    searchQueryBL.ApplyPhaseDefinitionsFilter(phaseDefinitionIDs);
                }
            }

            List<Expression<Func<FlowInstanceSearchViewModel, bool>>> expressions = GetFlowInstancePropertyExpressions(controller, searchControl, pointUserInfo);
            if (!expressions.Any())
            {
                return;
            }
            if (expressions.Count > 1)
            {
                if (searchControl.connector == SearchLogicOperatorType.And)
                {
                    searchQueryBL.ApplyFilter(expressions[0].And(expressions[1]));
                }
                else if (searchControl.connector == SearchLogicOperatorType.Or)
                {
                    searchQueryBL.ApplyFilter(expressions[0].Or(expressions[1]));
                }
            }
            else
            {
                searchQueryBL.ApplyFilter(expressions[0]);
            }
        }

        private static List<Expression<Func<FlowInstanceSearchViewModel, bool>>> GetFlowInstancePropertyExpressions(TransferController controller, SearchControl searchControl, PointUserInfo pointUserInfo)
        {
            var res = new List<Expression<Func<FlowInstanceSearchViewModel, bool>>>();

            if (searchControl.propertyList == null)
            {
                return res;
            }

            for (var index = 0; index < searchControl.propertyList.Length; index++)
            {
                var searchCritera = GetSearchCritera(controller.uow, pointUserInfo, searchControl, index);
                if (searchCritera == null || string.IsNullOrEmpty(searchCritera.SearchFor))
                {
                    continue;
                }

                Expression<Func<FlowInstanceSearchViewModel, bool>> expression = null;
                if (WellKnownFieldNames.ClientFullname.Equals(searchCritera.SearchIn, StringComparison.OrdinalIgnoreCase))
                {
                    expression = FlowInstanceExpressions.ByClientFullNameOrTransferID(searchCritera.SearchFor);
                }
                else if (WellKnownFieldNames.PatientNumber.Equals(searchCritera.SearchIn, StringComparison.OrdinalIgnoreCase))
                {
                    expression = FlowInstanceExpressions.ByClientPatientNumber(searchCritera.SearchFor);
                }
                else if (WellKnownFieldNames.ClientCivilServiceNumber.Equals(searchCritera.SearchIn, StringComparison.OrdinalIgnoreCase))
                {
                    expression = FlowInstanceExpressions.BySearchValuesClientCivilServiceNumber(searchCritera.SearchFor);
                }
                else if (WellKnownFieldNames.ClientInsuranceNumber.Equals(searchCritera.SearchIn, StringComparison.OrdinalIgnoreCase))
                {
                    expression = FlowInstanceExpressions.ByClientInsuranceNumber(searchCritera.SearchFor);
                }
                else if (WellKnownFieldNames.PatientNumber.Equals(searchCritera.SearchIn, StringComparison.OrdinalIgnoreCase))
                {
                    expression = FlowInstanceExpressions.BySearchValuesClientPatientNumber(searchCritera.SearchFor);
                }
                else if (WellKnownFieldNames.ClientBirthDate.Equals(searchCritera.SearchIn, StringComparison.OrdinalIgnoreCase))
                {
                    expression = FlowInstanceExpressions.ByClientBirthDate(DateTime.Parse(searchCritera.SearchFor), searchCritera.SearchMatchType);
                }
                else if (WellKnownFieldNames.ClientGender.Equals(searchCritera.SearchIn, StringComparison.OrdinalIgnoreCase))
                {
                    expression = FlowInstanceExpressions.ByClientGender(searchCritera.SearchFor);
                }
                else if (WellKnownFieldNames.TransferCreatedDate.Equals(searchCritera.SearchIn, StringComparison.OrdinalIgnoreCase))
                {
                    expression = FlowInstanceExpressions.ByTransferCreatedDate(DateTime.Parse(searchCritera.SearchFor), searchCritera.SearchMatchType);
                }
                else if (WellKnownFieldNames.CopyOfTransferID.Equals(searchCritera.SearchIn, StringComparison.OrdinalIgnoreCase))
                {
                    expression = FlowInstanceExpressions.ByCopyOfTransfer(int.Parse(searchCritera.SearchFor));
                }
                else if (WellKnownFieldNames.DepartmentName.Equals(searchCritera.SearchIn, StringComparison.OrdinalIgnoreCase))
                {
                    expression = FlowInstanceExpressions.ByDepartmentName(searchCritera.SearchFor);
                }
                else if (WellKnownFieldNames.TyperingNazorgCombined.Equals(searchCritera.SearchIn, StringComparison.OrdinalIgnoreCase))
                {
                    expression = FlowInstanceExpressions.ByTyperingNazorgCombined(searchCritera.SearchFor);
                }
                else if (WellKnownFieldNames.RequestTransferPointIntakeDate.Equals(searchCritera.SearchIn, StringComparison.OrdinalIgnoreCase))
                {
                    expression = FlowInstanceExpressions.ByRequestTransferPointIntakeDate(DateTime.Parse(searchCritera.SearchFor), searchCritera.SearchMatchType);
                }
                else if (WellKnownFieldNames.RequestFormZHVVTType.Equals(searchCritera.SearchIn, StringComparison.OrdinalIgnoreCase))
                {
                    var formType = FormTypeBL.GetByFormTypeID(controller.uow, Convert.ToInt32(searchCritera.SearchFor));
                    if (formType != null)
                    {
                        expression = FlowInstanceExpressions.ByRequestFormZHVVTType(formType.Name);
                    }
                }
                else if (WellKnownFieldNames.DatumEindeBehandelingMedischSpecialist.Equals(searchCritera.SearchIn, StringComparison.OrdinalIgnoreCase))
                {
                    expression = FlowInstanceExpressions.ByDatumEindeBehandelingMedischSpecialist(DateTime.Parse(searchCritera.SearchFor), searchCritera.SearchMatchType);
                }
                else if (WellKnownFieldNames.AcceptedBy.Equals(searchCritera.SearchIn, StringComparison.OrdinalIgnoreCase))
                {
                    expression = FlowInstanceExpressions.ByAcceptedByTP(Convert.ToInt32(searchCritera.SearchFor));
                }
                else if (WellKnownFieldNames.AcceptedDateTP.Equals(searchCritera.SearchIn, StringComparison.OrdinalIgnoreCase))
                {
                    expression = FlowInstanceExpressions.ByAcceptedDateTP(DateTime.Parse(searchCritera.SearchFor), searchCritera.SearchMatchType);
                }
                else if (WellKnownFieldNames.CareBeginDate.Equals(searchCritera.SearchIn, StringComparison.OrdinalIgnoreCase))
                {
                    expression = FlowInstanceExpressions.ByCareBeginDate(DateTime.Parse(searchCritera.SearchFor), searchCritera.SearchMatchType);
                }
                else if (WellKnownFieldNames.BehandelaarSpecialisme.Equals(searchCritera.SearchIn, StringComparison.OrdinalIgnoreCase))
                {
                    var specialism = SpecialismBL.GetBySpecialismID(controller.uow, Convert.ToInt32(searchCritera.SearchFor));
                    if (specialism != null)
                    {
                        expression = FlowInstanceExpressions.ByBehandelaarSpecialisme(specialism.Name);
                    }
                }
                else if (WellKnownFieldNames.TransferDateVVT.Equals(searchCritera.SearchIn, StringComparison.OrdinalIgnoreCase))
                {
                    expression = FlowInstanceExpressions.ByTransferDateVVT(DateTime.Parse(searchCritera.SearchFor), searchCritera.SearchMatchType);
                }
                else if (WellKnownFieldNames.DischargeProposedStartDate.Equals(searchCritera.SearchIn, StringComparison.OrdinalIgnoreCase))
                {
                    expression = FlowInstanceExpressions.ByDischargeProposedStartDate(DateTime.Parse(searchCritera.SearchFor), searchCritera.SearchMatchType);
                }
                else if (WellKnownFieldNames.AcceptedByTelephoneNumber.Equals(searchCritera.SearchIn, StringComparison.OrdinalIgnoreCase))
                {
                    expression = FlowInstanceExpressions.ByAcceptedByTelephoneNumber(searchCritera.SearchFor);
                }
                else if (WellKnownFieldNames.OrganizationName.Equals(searchCritera.SearchIn, StringComparison.OrdinalIgnoreCase))
                {
                    expression = FlowInstanceExpressions.ByOrganizationName(searchCritera.SearchFor);
                }
                else if (WellKnownFieldNames.LocationName.Equals(searchCritera.SearchIn, StringComparison.OrdinalIgnoreCase))
                {
                    expression = FlowInstanceExpressions.ByLocationName(searchCritera.SearchFor);
                }

                if (expression != null)
                {
                    res.Add(expression);
                }
            }

            return res;
        }

        private static SearchCritera GetSearchCritera(IUnitOfWork uow, PointUserInfo pointUserInfo, SearchControl searchControl, int index)
        {
            if (searchControl == null)
            {
                return null;
            }

            var searchUiConfig = SearchUIConfigurationBL.GetConfigurationOrDefault(uow, searchControl.FlowDefinitionIDs, searchControl.SearchType, pointUserInfo);

            var propertyList = searchUiConfig.Fields.Where(p => p.UseInSearchFilter).ToList();
            var searchProperty = propertyList.Where(p => p.Field.Name.Equals(searchControl.propertyList[index], StringComparison.InvariantCultureIgnoreCase)).Select(p => p).FirstOrDefault();

            if (searchProperty == null)
            {
                return null;
            }

            var matchtype = SearchCritera.MatchType.equal;
            if (searchControl.matchtype.GetUpperBound(0) >= index)
            {
                matchtype = (SearchCritera.MatchType)searchControl.matchtype[index];
            }

            string searchString;
            if (searchProperty.SearchPropertyType == SearchPropertyType.String && searchControl.input != null && index < searchControl.input.Length)
            {
                searchString = searchControl.input[index];
            }
            else if (searchProperty.SearchPropertyType == SearchPropertyType.Numeric && searchControl.number != null && index < searchControl.number.Length)
            {
                searchString = searchControl.number[index];
            }
            else if (searchProperty.SearchPropertyType == SearchPropertyType.Date && searchControl.date != null && index < searchControl.date.Length)
            {
                searchString = searchControl.date[index];
            }
            else if (searchProperty.SearchPropertyType == SearchPropertyType.Lookup && searchControl.select != null && index < searchControl.select.Length)
            {
                searchString = searchControl.select[index];
            }
            else
            {
                return null;
            }

            if (string.IsNullOrWhiteSpace(searchString))
            {
                return null;
            }

            return new SearchCritera
            {
                SearchMatchType = matchtype,
                SearchFor = searchString,
                SearchPropertyType = searchProperty.SearchPropertyType,
                SearchIn = searchProperty.Field.Name
            };
        }

        public static List<Option> GetActivePhaseList(TransferController controller, FlowDefinitionID[] flowDefinitionIDs, string[] activephases)
        {
            var phaseList = new List<Option>();
            var phaseDefinitions = PhaseDefinitionCacheBL.GetByFlowDefinitionIDs(controller.uow, flowDefinitionIDs);
            foreach (var phaseDefinition in phaseDefinitions)
            {
                if (phaseDefinition.Phase >= 0)
                {
                    var phasenum = (phaseDefinition.Phase % 1 == 0) ? decimal.Round(phaseDefinition.Phase) : phaseDefinition.Phase;

                    phaseList.Add(new Option($"Fase {phasenum}: {phaseDefinition.PhaseName}", phaseDefinition.PhaseDefinitionID.ToString()));
                }
            }

            if (activephases != null && activephases.Length > 0)
            {
                phaseList.Where(it => activephases.Contains(it.Value)).ToList().ForEach(it => it.Selected = true);
            }

            return phaseList;
        }

        private static LocationDepartmentSelection[] GetDepartments(TransferController controller, PointUserInfo pointUserInfo)
        {
            var optionList = new List<LocationDepartmentSelection>();

            if (PointUserInfoHelper.HasMultipleDepartments(controller.uow, pointUserInfo) == false)
            {
                return optionList.ToArray();
            }

            var dict = new Dictionary<int, LocationDepartmentSelection>();
            var defaultDepartment = DepartmentBL.GetByDepartmentID(controller.uow, pointUserInfo.Department.DepartmentID);
            dict.Add(defaultDepartment.DepartmentID, new LocationDepartmentSelection(
                defaultDepartment.LocationID,
                defaultDepartment.DepartmentID,
                defaultDepartment.Location.Name,
                defaultDepartment.Name
            ));

            var departments = DepartmentBL.GetLinkedDepartmentsByRoleForSearch(controller.uow, pointUserInfo);
            if (departments != null)
            {
                foreach (var department in departments)
                {
                    if (!dict.ContainsKey(department.DepartmentID))
                    {
                        dict.Add(department.DepartmentID, new LocationDepartmentSelection(
                            department.LocationID,
                            department.DepartmentID,
                            department.Location.Name,
                            department.Name
                        ));
                    }
                }
            }

            var list = dict.Select(d => d.Value)
                .OrderBy(d => d.LocationName)
                .ThenBy(d => d.DepartmentName).ToList();

            return list.ToArray();
        }

        private static int[] GetDefaultDepartmentSelection(TransferController controller, PointUserInfo pointUserInfo)
        {
            var departmentidreference = LoginBL.GetDepartmentIDReference();
            if (departmentidreference.HasValue && PointUserInfoHelper.HasMultipleDepartments(controller.uow, pointUserInfo))
            {
                return new int[] { departmentidreference.Value };
            }

            return new int[] { };
        }
    }
}