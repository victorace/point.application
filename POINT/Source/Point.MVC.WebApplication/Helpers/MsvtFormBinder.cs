﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using Point.Database.Models.ViewModels;

namespace Point.MVC.WebApplication.Helpers
{
    public class MsvtFormBinder : FlowWebValueBinderBase
    {
        private static Regex _stripIDAndFieldnameTreatmentItemRegex = new Regex(MsvtTreatmentItemViewModel.NAME_FORMAT_REGEXP, RegexOptions.Compiled);
        private static Regex _stripIDAndFieldnameMedicationItemRegex = new Regex(MedicationItemViewModel.NAME_FORMAT_REGEXP, RegexOptions.Compiled);

        public List<MsvtTreatmentItemViewModel> GetTreatmentItems(ControllerContext controllerContext)
        {
            HttpRequestBase request = controllerContext.HttpContext.Request;
            List<MsvtTreatmentItemViewModel> treatmentitems = new List<MsvtTreatmentItemViewModel>();

            foreach (string key in request.Form.AllKeys)
            {
                if (key != null && key.StartsWith(MsvtTreatmentItemViewModel.NAME_PREFIX))
                {
                    string protectedName = key.Remove(0, MsvtTreatmentItemViewModel.NAME_PREFIX.Length);
                    if (!String.IsNullOrWhiteSpace(protectedName))
                    {
                        var match = _stripIDAndFieldnameTreatmentItemRegex.Match(protectedName);
                        if (match.Success)
                        {
                            string value = request.Form.Get(key);

                            int id = 0;
                            Int32.TryParse(match.Groups["id"].Value, out id);
                            string complex = match.Groups["complex"].Value;
                            string fieldName = match.Groups["fieldName"].Value;

                            var treatmentitem = treatmentitems.FirstOrDefault(it => it.Id == id);
                            if (treatmentitem == null)
                            {
                                treatmentitem = new MsvtTreatmentItemViewModel();
                                treatmentitem.Id = id;

                                MsvtTreatmentItemViewModel.ComplexType complextype = default(MsvtTreatmentItemViewModel.ComplexType);
                                if (Enum.TryParse<MsvtTreatmentItemViewModel.ComplexType>(complex, out complextype))
                                    treatmentitem.Complex = complextype;

                                treatmentitems.Add(treatmentitem);
                            }

                            switch (fieldName)
                            {
                                case "expectedtime":
                                    int expectedtime = 0;
                                    if (int.TryParse(value, out expectedtime))
                                        treatmentitem.ExpectedTime = expectedtime;
                                    break;
                                case "frequency":
                                    int freq = 0;
                                    if(int.TryParse(value, out freq))
                                        treatmentitem.Frequency = freq;
                                    break;
                                case "amountunitfrequency":
                                    int amountunitfreq = 0;
                                    if (int.TryParse(value, out amountunitfreq))
                                        treatmentitem.AmountUnitFrequency = amountunitfreq;
                                    break;
                                case "freqencyunit":
                                    MsvtTreatmentItemViewModel.DayWeekMonth frequnit = default(MsvtTreatmentItemViewModel.DayWeekMonth);
                                    if (Enum.TryParse<MsvtTreatmentItemViewModel.DayWeekMonth>(value, out frequnit))
                                        treatmentitem.FrequencyUnit = frequnit;
                                    break;
                                case "definition":
                                    treatmentitem.Definition = value;
                                    break;
                                default:
                                    break;
                            }

                            
                        }
                    }
                }
            }

            return treatmentitems;
        }

        public List<MedicationItemViewModel> GetMedicationItems(ControllerContext controllerContext)
        {
            HttpRequestBase request = controllerContext.HttpContext.Request;
            List<MedicationItemViewModel> medicationitems = new List<MedicationItemViewModel>();

            foreach (string key in request.Form.AllKeys)
            {
                if (key != null && key.StartsWith(MedicationItemViewModel.NAME_PREFIX))
                {
                    string protectedName = key.Remove(0, MedicationItemViewModel.NAME_PREFIX.Length);
                    if (!String.IsNullOrWhiteSpace(protectedName))
                    {
                        var match = _stripIDAndFieldnameMedicationItemRegex.Match(protectedName);
                        if (match.Success)
                        {
                            string value = request.Form.Get(key);

                            int nr = 0;
                            Int32.TryParse(match.Groups["nr"].Value, out nr);
                            string fieldName = match.Groups["fieldName"].Value;

                            MedicationItemViewModel medicationitem = null;
                            if (medicationitems.Count == nr)
                            {
                                medicationitem = new MedicationItemViewModel();
                                medicationitems.Add(medicationitem);
                            }
                            medicationitem = medicationitems[nr];

                            switch (fieldName)
                            {
                                case "frequency":
                                    medicationitem.Frequency = value;
                                    break;
                                case "medication":
                                    medicationitem.Medication = value;
                                    break;
                                case "dosage":
                                    medicationitem.Dosage = value;
                                    break;
                                case "deliverymethod":
                                    medicationitem.DeliveryMethod = value;
                                    break;
                                default:
                                    break;
                            }


                        }
                    }
                }
            }

            return medicationitems;
        }

        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            if (bindingContext.ModelType == typeof(MsvtViewModel))
            {
                var msvtuvvviewmodel = new MsvtViewModel
                {
                    TreatmentItems = GetTreatmentItems(controllerContext),
                    MedicationItems = GetMedicationItems(controllerContext)
                };

                msvtuvvviewmodel.SetGlobalProperties();
                msvtuvvviewmodel.Global.FlowWebFields = GetFlowWebFields(controllerContext);

                controllerContext.Controller.ViewData.Model = msvtuvvviewmodel;
                return msvtuvvviewmodel;
            }

            // call the default model binder this new binding context
            return base.BindModel(controllerContext, bindingContext);
        }
    }
}