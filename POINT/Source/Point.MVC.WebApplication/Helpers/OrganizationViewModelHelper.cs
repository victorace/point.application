﻿using System;
using System.Linq;
using Point.Business.Logic;
using Point.Database.Context;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Models.Enums;
using Point.MVC.WebApplication.Controllers;

namespace Point.MVC.WebApplication.Helpers
{
    public static class OrganizationViewModelHelper
    {
        private const int _maxPageLen = 100;

        public static OrganizationParticipantsViewModel GetParticipantsViewModel(OrganizationParticipantsViewModel viewmodel, PointUserInfo pointUserInfo)
        {
            using (var uow = new UnitOfWork<PointSearchContext>())
            {
                var regionSearch = viewmodel.DoRegionSearch ? RegionForSearch.OwnRegion : RegionForSearch.AllRegions;

                var participants = OrganizationSearchBL.GetOrganizationsByOrganizationTypeForSearch(
                        uow, pointUserInfo, (int)viewmodel.OrganizationTypeID, namecitySearch: viewmodel.Search,
                        regionSearch: regionSearch.ToString(), userRegionID: pointUserInfo.Region.RegionID)
                    .ToList();

                if (viewmodel.IsOrderByAnyFlowSending)
                {
                    if (viewmodel.IsOrderDescending)
                    {
                        participants = participants.OrderByDescending(it => it.IsSendingParty).ToList();
                    }
                    else
                    {
                        participants = participants.OrderBy(it => it.IsSendingParty).ToList();
                    }
                }
                else if (viewmodel.IsOrderByAnyFlowReceiving)
                {
                    if (viewmodel.IsOrderDescending)
                    {
                        participants = participants.OrderByDescending(it => it.IsReceivingParty).ToList();
                    }
                    else
                    {
                        participants = participants.OrderBy(it => it.IsReceivingParty).ToList();
                    }
                }
                else if (viewmodel.IsOrderByRegionName)
                {
                    if (viewmodel.IsOrderDescending)
                    {
                        participants = participants.OrderByDescending(it => it.OrganizationSearchRegion.Select(r => r.RegionName).FirstOrDefault()).ToList();
                    }
                    else
                    {
                        participants = participants.OrderBy(it => it.OrganizationSearchRegion.OrderBy(r => r.RegionName).Select(r => r.RegionName).FirstOrDefault()).ToList();
                    }
                }
                else
                {
                    participants = participants.OrderBy(viewmodel.OrderBy).ToList();
                }

                var totalCount = participants.Count;

                viewmodel.Pager.PageCount = (int)Math.Ceiling(totalCount / (decimal)_maxPageLen);
                viewmodel.Pager.TotalCount = totalCount;
                viewmodel.Participants = participants.Skip(_maxPageLen * (viewmodel.Pager.PageNo - 1)).Take(_maxPageLen);

                return viewmodel;
            }
        }
    }
}