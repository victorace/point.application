﻿using System;
using System.Collections.Generic;
using System.Linq;
using Point.Business.Helper;
using Point.Business.Logic;
using Point.Database.Extensions;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Extensions;
using Point.Models.Enums;
using Point.MVC.WebApplication.Controllers;

namespace Point.MVC.WebApplication.Helpers
{
    public static class FormTypeCloseTransferViewModelHelper
    {
        public const string NoFlowInstanceMessage = "<!-- Geen flowinstance -->";

        public static CloseTransferHAViewModel GetCloseTransferHAViewModel(IUnitOfWork uow, GlobalProperties globalProps)
        {
            var viewModel = new CloseTransferHAViewModel();
            var closeTransferViewModel = GetCloseTransferViewModel(uow, globalProps);
            viewModel.Merge(closeTransferViewModel);
            viewModel.SetGlobalProperties(globalProps);

            if (viewModel.HasErrors)
            {
                return viewModel;
            }

            var destinationHealthCareProviderDepartment = OrganizationHelper.GetRecievingDepartment(uow, globalProps.FlowInstance.FlowInstanceID);
            if (destinationHealthCareProviderDepartment != null)
            {
                viewModel.DestinationHealthCareProviderDepartmentID = destinationHealthCareProviderDepartment.DepartmentID;
                viewModel.DestinationHealthCareProviderDepartmentName = destinationHealthCareProviderDepartment.FullName();
                viewModel.HasDestinationHealthCareProvider = true;
            }

            return viewModel;
        }

        public static CloseTransferViewModel GetCloseTransferRzTPViewModel(IUnitOfWork uow, GlobalProperties globalProps)
        {
            return GetCloseTransferViewModel(uow, globalProps);
        }

        public static CloseTransferViewModel GetCloseTransferCvaViewModel(IUnitOfWork uow, GlobalProperties globalProps)
        {
            return GetCloseTransferViewModel(uow, globalProps);
        }


        public static CloseTransferZHVVTViewModel GetCloseTransferZHVVTViewModel(IUnitOfWork uow, GlobalProperties globalProps, PointController controller)
        {
            var flowInstance = globalProps.FlowInstance;

            if (flowInstance == null)
            {
                return new CloseTransferZHVVTViewModel { ErrorMessage = NoFlowInstanceMessage };
            }

            var flowInstanceID = flowInstance.FlowInstanceID;
            var flowDefinitionID = flowInstance.FlowDefinitionID;

            var viewModel = new CloseTransferZHVVTViewModel
            {
                FlowDefinitionID = flowInstance.FlowDefinitionID,
                IsAllowed = PhaseInstanceBL.HasEndFlowRights(uow, flowDefinitionID, globalProps.ControllerName, globalProps.ActionName, globalProps.PointUserInfo),
                IsFlowClosed = FlowInstanceBL.IsFlowClosed(uow, flowInstanceID),
                ActivePhases = FlowInstanceBL.GetActivePhaseTextByFlowInstance(uow, flowInstance)
            };
            viewModel.SetGlobalProperties(globalProps);

            var flowWebFields = globalProps.FlowWebFields;

            var requestFormZHVVTType = FlowFieldValueBL.GetByFlowInstanceIDAndFieldID(uow, globalProps.FlowInstance.FlowInstanceID, FlowFieldConstants.ID_RequestFormZHVVTType).FirstOrDefault();
            if (requestFormZHVVTType != null && requestFormZHVVTType.Value == ((int)FlowFormType.RequestFormZHVVT_FormulierPoli).ToString())
            {
                string[] poliHiddenFields = { FlowFieldConstants.Name_DatumEindeBehandelingMedischSpecialist, FlowFieldConstants.Name_MedischeSituatieDatumOpname, FlowFieldConstants.Name_RealizedDischargeDate };
                flowWebFields.Where(it => poliHiddenFields.Contains(it.Name)).ToList().ForEach(it =>
                { it.Visible = false; it.Required = false; it.RequiredBy = null; it.RequiredByValue = null; it.RequiredByLabel = null; });
            }
            
            var voPhaseDefinition = PhaseDefinitionCacheBL.GetByFlowDefinitionAndFormType(uow, flowDefinitionID, (int)FlowFormType.VerpleegkundigeOverdrachtFlow);
            PhaseInstance voPhaseInstance = null;
            if (voPhaseDefinition != null)
            {
                voPhaseInstance = PhaseInstanceBL.GetByFlowInstanceIDAndPhaseDefinition(uow, flowInstanceID, voPhaseDefinition.PhaseDefinitionID);
            }

            viewModel.VOAction = (voPhaseInstance == null ? CloseTransferViewModel.VOActions.Missing : (voPhaseInstance.Status == (int)Status.Active ? CloseTransferViewModel.VOActions.Active : CloseTransferViewModel.VOActions.None));
            if (viewModel.VOAction != CloseTransferViewModel.VOActions.None)
            {
                viewModel.VOUrl = controller.Url.Action("StartFlowForm", "FlowMain", new
                {
                    globalProps.TransferID,
                    voPhaseDefinition?.PhaseDefinitionID,
                    PhaseInstanceID = voPhaseInstance?.PhaseInstanceID ?? -1,
                    FormType = (int)FlowFormType.VerpleegkundigeOverdrachtFlow
                });
            }

            viewModel.HasOrganizationDefinedTags = OrganizationProjectBL.HasOrganizationTagsDefined(uow, globalProps.FlowInstance.StartedByOrganizationID ?? 0);

            // Transfer
            var registrationEndedWithoutTransfer = flowWebFields.FirstOrDefault(m => m.Name == FlowFieldConstants.Name_RegistrationEndedWithoutTransfer);
            var hasRegistrationEndedWithoutTransfer = !string.IsNullOrWhiteSpace(registrationEndedWithoutTransfer?.Value) && registrationEndedWithoutTransfer.Value.StringToBool();
            
            if (hasRegistrationEndedWithoutTransfer)
            {
                // https://techxx.visualstudio.com/POINT.Application/_workitems/edit/13939
                flowWebFields.ForEach(it => { it.Required = false; it.RequiredBy = null; it.RequiredByValue = null; it.RequiredByLabel = null; });

                viewModel.HasRegistrationEndedWithoutTransfer = true;
                var reasonEndRegistration = flowWebFields.Where(m => m.Name == FlowFieldConstants.Name_ReasonEndRegistration).FirstOrNew();
                viewModel.HasRegistrationEndedWithoutReason = string.IsNullOrWhiteSpace(reasonEndRegistration.Value);
            }
            
            var routerenPhaseDefinition = PhaseDefinitionCacheBL.GetByFlowDefinitionAndFormType(uow, flowDefinitionID, (int)FlowFormType.RouterenDossierZHVVT);
            var routerenPhaseInstance = PhaseInstanceBL.GetByFlowInstanceIDAndPhaseDefinition(uow, flowInstanceID, routerenPhaseDefinition.PhaseDefinitionID);
            
            if (routerenPhaseInstance != null)
            {
                viewModel.HasRouterenPhaseInstance = true;
                var destinationHealthCareProviderDepartment = OrganizationHelper.GetRecievingDepartment(uow, flowInstanceID);
                var hasDestinationHealthCareProvider = false;
                if (destinationHealthCareProviderDepartment != null)
                {
                    viewModel.DestinationHealthCareProviderDepartmentID = destinationHealthCareProviderDepartment.DepartmentID;
                    viewModel.DestinationHealthCareProviderDepartmentName = destinationHealthCareProviderDepartment.FullName();
                    hasDestinationHealthCareProvider = true;
                }
                else
                {
                    var destinationHealthCareProvider = flowWebFields.FirstOrDefault(m => m.Name == FlowFieldConstants.Name_DestinationHealthCareProvider);
                    if (!string.IsNullOrWhiteSpace(destinationHealthCareProvider?.Value))
                    {
                        if (int.TryParse(destinationHealthCareProvider.Value, out int destinationHealthCareProviderDepartmentID))
                        {
                            var department = DepartmentBL.GetByDepartmentID(uow, destinationHealthCareProviderDepartmentID);
                            if (department != null)
                            {
                                viewModel.DestinationHealthCareProviderDepartmentID = department.DepartmentID;
                                viewModel.DestinationHealthCareProviderDepartmentName = department.FullName();
                                hasDestinationHealthCareProvider = true;
                            }
                        }
                    }

                    // should we also look at 'ZorgInZorginstellingVoorkeurPatient1'?
                }
                viewModel.HasDestinationHealthCareProvider = hasDestinationHealthCareProvider;
                viewModel.ValidAftercareClassification = AftercareClassificationHelper.TransferPointValidateAftercareClassification(flowWebFields);
            }

            // Openstaande indicatiestelling(en)
            viewModel.ActiveIndicatieMSVTs = GetActiveIndicatieMSVTForms(uow, controller, globalProps.TransferID);

            // Flow Interruption
            IsInterrupted(viewModel, flowInstance);

            return viewModel;
        }

        private static CloseTransferViewModel GetCloseTransferViewModel(IUnitOfWork uow, GlobalProperties globalProps)
        {
            var viewModel = new CloseTransferViewModel();
            var flowInstance = globalProps.FlowInstance;

            if (flowInstance == null)
            {
                viewModel.ErrorMessage = NoFlowInstanceMessage;
                return viewModel;
            }

            viewModel.SetGlobalProperties(globalProps);
            viewModel.IsAllowed = PhaseInstanceBL.HasEndFlowRights(uow, flowInstance.FlowDefinitionID, globalProps.ControllerName, globalProps.ActionName, globalProps.PointUserInfo);
            viewModel.IsFlowClosed = FlowInstanceBL.IsFlowClosed(uow, flowInstance.FlowInstanceID);
            viewModel.FlowDefinitionID = flowInstance.FlowDefinitionID;
            viewModel.ActivePhases = FlowInstanceBL.GetActivePhaseTextByFlowInstance(uow, flowInstance);

            // Flow Interruption
            IsInterrupted(viewModel, flowInstance);

            return viewModel;
        }

        private static bool IsInterrupted(CloseTransferViewModel viewModel, FlowInstance flowInstance)
        {
            // Flow Interruption
            if (flowInstance == null || !flowInstance.Interrupted)
            {
                return false;
            }
            var flowInterruption = flowInstance.FlowInstanceStatus.GetInterruptionStatus();
            if (flowInterruption == null)
            {
                return false;
            }
            viewModel.IsFlowInterrupted = true;
            viewModel.InterruptionDate = flowInterruption.StatusDate;
            viewModel.InterruptionReason = flowInterruption.Comment;
            return true;

        }

        private static List<CloseTransferViewModel.ActiveIndicatieMSVT> GetActiveIndicatieMSVTForms(IUnitOfWork uow, PointController controller, int transferID)
        {
            var indicatieMSVTFormsetversions = FormSetVersionBL.GetByTransferIDAndFormTypeID(uow, transferID, (int)FlowFormType.MSVTIndicatiestelling);
            indicatieMSVTFormsetversions = indicatieMSVTFormsetversions.Where(fsv => fsv.PhaseInstance.Status == (int)Status.Active);
            var activeIndicatieMSVTs = new List<CloseTransferViewModel.ActiveIndicatieMSVT>();
            foreach (var formSetVersion in indicatieMSVTFormsetversions)
            {
                if (formSetVersion.PhaseInstanceID != null)
                {
                    activeIndicatieMSVTs.Add(new CloseTransferViewModel.ActiveIndicatieMSVT
                    {
                        Name = formSetVersion.Description,
                        Url = controller.Url.Action("MsvtIND", "FormType", new
                        {
                            transferID,
                            formSetVersion.FormSetVersionID,
                            PhaseInstanceID = formSetVersion.PhaseInstanceID.Value,
                            formSetVersion.PhaseInstance.PhaseDefinitionID
                        })
                    });
                }
            }
            return activeIndicatieMSVTs;
        }


    }
}