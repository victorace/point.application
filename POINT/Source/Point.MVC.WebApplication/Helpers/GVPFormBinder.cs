﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using Point.Database.Models.ViewModels;

namespace Point.MVC.WebApplication.Helpers
{
    public class GVPFormBinder : FlowWebValueBinderBase
    {
        private static Regex _stripIDAndFieldnameTreatmentItemRegex = new Regex(MsvtTreatmentItemViewModel.NAME_FORMAT_REGEXP, RegexOptions.Compiled);
        private static Regex _stripIDAndFieldnameMedicationItemRegex = new Regex(MedicationItemViewModel.NAME_FORMAT_REGEXP, RegexOptions.Compiled);

        public List<GVPHandelingViewModel> GetGVPHandelingItems(ControllerContext controllerContext)
        {
            HttpRequestBase request = controllerContext.HttpContext.Request;
            List<GVPHandelingViewModel> treatmentitems = new List<GVPHandelingViewModel>();

            foreach (string key in request.Form.AllKeys)
            {
                if (key != null && key.StartsWith(GVPHandelingViewModel.NAME_PREFIX))
                {
                    string protectedName = key.Remove(0, GVPHandelingViewModel.NAME_PREFIX.Length);
                    if (!String.IsNullOrWhiteSpace(protectedName))
                    {
                        var match = _stripIDAndFieldnameTreatmentItemRegex.Match(protectedName);
                        if (match.Success)
                        {
                            string value = request.Form.Get(key);

                            Int32.TryParse(match.Groups["id"].Value, out int id);
                            string complex = match.Groups["complex"].Value;
                            string fieldName = match.Groups["fieldName"].Value;

                            var treatmentitem = treatmentitems.FirstOrDefault(it => it.Id == id);
                            if (treatmentitem == null)
                            {
                                treatmentitem = new GVPHandelingViewModel
                                {
                                    Id = id
                                };

                                if (Enum.TryParse(complex, out GVPHandelingViewModel.ComplexType complextype))
                                {
                                    treatmentitem.Complex = complextype;
                                }

                                treatmentitems.Add(treatmentitem);
                            }

                            switch (fieldName)
                            {
                                case "frequency":
                                    treatmentitem.Frequency = value;
                                    break;
                                case "definition":
                                    treatmentitem.Definition = value;
                                    break;
                                default:
                                    break;
                            }

                            
                        }
                    }
                }
            }

            return treatmentitems;
        }

        public List<MedicationItemViewModel> GetMedicationItems(ControllerContext controllerContext)
        {
            HttpRequestBase request = controllerContext.HttpContext.Request;
            List<MedicationItemViewModel> medicationitems = new List<MedicationItemViewModel>();

            foreach (string key in request.Form.AllKeys)
            {
                if (key != null && key.StartsWith(MedicationItemViewModel.NAME_PREFIX))
                {
                    string protectedName = key.Remove(0, MedicationItemViewModel.NAME_PREFIX.Length);
                    if (!String.IsNullOrWhiteSpace(protectedName))
                    {
                        var match = _stripIDAndFieldnameMedicationItemRegex.Match(protectedName);
                        if (match.Success)
                        {
                            string value = request.Form.Get(key);

                            int nr = 0;
                            Int32.TryParse(match.Groups["nr"].Value, out nr);
                            string fieldName = match.Groups["fieldName"].Value;

                            MedicationItemViewModel medicationitem = null;
                            if (medicationitems.Count == nr)
                            {
                                medicationitem = new MedicationItemViewModel();
                                medicationitems.Add(medicationitem);
                            }
                            medicationitem = medicationitems[nr];

                            switch (fieldName)
                            {
                                case "frequency":
                                    medicationitem.Frequency = value;
                                    break;
                                case "medication":
                                    medicationitem.Medication = value;
                                    break;
                                case "dosage":
                                    medicationitem.Dosage = value;
                                    break;
                                case "deliverymethod":
                                    medicationitem.DeliveryMethod = value;
                                    break;
                                default:
                                    break;
                            }


                        }
                    }
                }
            }

            return medicationitems;
        }

        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            if (bindingContext.ModelType == typeof(GVPViewModel))
            {
                var viewmodel = new GVPViewModel
                {
                    GVPHandelingItems = GetGVPHandelingItems(controllerContext),
                    MedicationItems = GetMedicationItems(controllerContext)
                };

                viewmodel.SetGlobalProperties();
                viewmodel.Global.FlowWebFields = GetFlowWebFields(controllerContext);

                controllerContext.Controller.ViewData.Model = viewmodel;
                return viewmodel;
            }

            // call the default model binder this new binding context
            return base.BindModel(controllerContext, bindingContext);
        }
    }
}