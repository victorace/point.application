﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Point.Business.Logic;
using Point.Database.Repository;
using Point.Database.Models.ViewModels;
using Point.Infrastructure.Extensions;
using Point.Log4Net;

namespace Point.MVC.WebApplication.Helpers
{
    public static class AdminHelper
    {
        private static readonly ILogger Logger = LogManager.GetLogger(typeof(AdminHelper));
        const string MessageNoSuccess = "Migration FAILED";

        internal static class MigrationEntities
        {
            public const string Location = "Location";
            public const string OrganizationInvite = "OrganizationInvite";
            public const string Recalculate = "RECALC";
            public const string End = "END";
        }

        public enum DisplayLineTypes
        {
            All,
            Errors,
            Headers,
            Warnings
        }

        public static Dictionary<string, string> GetLocationsForOrganizationID(Controller controller, IUnitOfWork uow, int organizationID)
        {
            var locations = new List<OrganizationMigrateItemViewModel>();

            if (organizationID > 0)
            {
                // All locations selected by default
                locations = LocationBL.GetLocationsForSearch(uow, null, organizationID).Select(o => new OrganizationMigrateItemViewModel { ID = o.LocationID, Name = o.Name, IsSelected = true }).OrderBy(x => x.ID).ToList();
            }

            var locationsHtml = controller.RenderPartialViewToString("OrganizationMigrateLocationList", locations);
            
            return new Dictionary<string, string>
            {
                {"#organizationMigrateLocationList", locationsHtml }
            };
        }

        public static Dictionary<string, string> GetOrganizationsForOrganizationNameOrID(Controller controller, IUnitOfWork uow, string organizationNameOrID, string sourceOrTarget)
        {
            var htmlReplaces = new Dictionary<string, string>();

            if (string.IsNullOrEmpty(sourceOrTarget))
            {
                return htmlReplaces;
            }

            var organization = new OrganizationMigrateOrganizationViewModel(sourceOrTarget);

            if (!string.IsNullOrEmpty(sourceOrTarget) && !string.IsNullOrEmpty(organizationNameOrID))
            {
                organization.Organizations = OrganizationBL.SearchByPhraseOrID(uow, organizationNameOrID, false).Select(o => new OrganizationMigrateItemViewModel { ID = o.OrganizationID, Name = o.Name }).ToList();
                if (organization.Organizations.Count == 1)
                {
                    organization.Organizations[0].IsSelected = true;
                }
            }
            
            var organizationsHtml = controller.RenderPartialViewToString("OrganizationMigrateOrganizationList", organization);
            htmlReplaces.Add($"#organizationMigrateOrganizationList{sourceOrTarget}", organizationsHtml);

            if (sourceOrTarget != OrganizationSourceOrTarget.Source)
            {
                return htmlReplaces;
            }

            if (organization.Organizations.Count == 1)
            {
                htmlReplaces.AddRange(GetLocationsForOrganizationID(controller, uow, organization.Organizations[0].ID));
            }
            else
            {
                var locationsHtml = controller.RenderPartialViewToString("OrganizationMigrateLocationList", new List<OrganizationMigrateItemViewModel>());
                htmlReplaces.Add("#organizationMigrateLocationList", locationsHtml);
            }

            return htmlReplaces;
        }

        private static void AppendLog(string migrationTitle, OrganizationMigrateResultsViewModel result,  string tableName, string message, StringBuilder logs, bool isLineHeader, bool isWarning, bool isEnd=false)
        {
            if (logs == null)
            {
                return;
            }

            result.ResultLines.Add(new OrganizationMigrateResultLineViewModel {Step = tableName, Message=message, IsLineHeader = isLineHeader, IsWarning = isWarning, IsEnd=isEnd});
            logs.AppendFormat("{0}{1}", message, Environment.NewLine);
            Logger.Info($"{migrationTitle} - Step: {tableName} - {message}");
        }

        private static void LogError(string migrationTitle, OrganizationMigrateResultsViewModel result, string tableName, string errorMessage)
        {
            result.ResultLines.Add(new OrganizationMigrateResultLineViewModel {Step = tableName, Message=errorMessage, IsError = true});
            Logger.Error($"{migrationTitle} - Message: {errorMessage}");
        }

        public static OrganizationMigrateResultsViewModel OrganizationMigrateLocations(Controller controller, IUnitOfWork uow, int[] locationIDs, int sourceOrganizationID, int targetOrganizationID)
        {
            var result = new OrganizationMigrateResultsViewModel();

            var migrationTitle = $"MIGRATION ORGANIZATION ({sourceOrganizationID} > {targetOrganizationID})";

            if (sourceOrganizationID == 0 || targetOrganizationID == 0 || locationIDs.Length == 0)
            {
                LogError(migrationTitle, result, "", "Missing sourceOrganizationID or targetOrganizationID or locationIDs");
                return result;
            }
            
            var step = "";
            var hasError = false;
            var logs = new StringBuilder();

            var flowInstances = (from oi in uow.OrganizationInviteRepository.Get()
                join fi in uow.FlowInstanceRepository.Get() on oi.FlowInstanceID equals fi.FlowInstanceID
                where oi.OrganizationID == sourceOrganizationID
                select new { fi.TransferID, fi.FlowInstanceID});

            var flowInstanceIDs = flowInstances.Select(x => x.FlowInstanceID).ToArray();
            var transferIDs = flowInstances.Select(x => x.TransferID).ToList();     // Purely for informational purposes

            var sourceOrganizationName = uow.OrganizationRepository.Get().Where(o => o.OrganizationID == sourceOrganizationID).Select(o => o.Name).FirstOrDefault();
            var targetOrganizationName = uow.OrganizationRepository.Get().Where(o => o.OrganizationID == targetOrganizationID).Select(o => o.Name).FirstOrDefault();
            
            if (sourceOrganizationName == null)
            {
                LogError(migrationTitle, result, step, $"{MessageNoSuccess} - Could not find record for organizationID {sourceOrganizationID}");
                return result;
            }

            if (targetOrganizationName == null)
            {
                LogError(migrationTitle, result, step, $"{MessageNoSuccess} - Could not find record for organizationID {targetOrganizationID}");
                return result;
            }

            result.Title = $"Van {sourceOrganizationName} ({sourceOrganizationID}) naar {targetOrganizationName} ({targetOrganizationID})   {DateTime.Now:dd-MM-yyyy HH:mm}";

            using (var dbContextTransaction = uow.DatabaseContext.Database.BeginTransaction())
            {
                try
                {
                    var locations = uow.LocationRepository.Get(loc => loc.OrganizationID == sourceOrganizationID && locationIDs.Contains(loc.LocationID)).ToList();
                    step = MigrationEntities.Location;
                    if (locations.Any())
                    {
                        AppendLog(migrationTitle, result, step, $"* Migrating {locations.Count} location(s)", logs, true, false);
                        var count = 0;
                        foreach (var location in locations)
                        {
                            uow.DatabaseContext.Entry(location).State = EntityState.Modified;
                            location.OrganizationID = targetOrganizationID;
                            uow.Save();
                            count++;
                        }

                        AppendLog(migrationTitle, result, step, $"+ Done migrating {count} location(s):" + GetIDsBlock(locationIDs), logs, true, false);

                        var organizationInvites = uow.OrganizationInviteRepository.Get().Where(oi => oi.OrganizationID == sourceOrganizationID).ToList();
                        step = MigrationEntities.OrganizationInvite;
                        if (organizationInvites.Any())
                        {
                            AppendLog(migrationTitle, result, step, $"* Migrating {organizationInvites.Count} organizationInvite(s).", logs, true, false);
                            count = 0;
                            var organizationInviteIDs = new List<int>();
                            foreach (var organizationInvite in organizationInvites)
                            {
                                uow.DatabaseContext.Entry(organizationInvite).State = EntityState.Modified;
                                organizationInvite.OrganizationID = targetOrganizationID;
                                uow.Save();
                                organizationInviteIDs.Add(organizationInvite.OrganizationInviteID);
                                count++;
                            }

                            AppendLog(migrationTitle, result, step, $"+ Done migrating {count} organizationInvite(s):" + GetIDsBlock(organizationInviteIDs), logs, true, false);
                        }
                        else
                        {
                            AppendLog(migrationTitle, result, step, $"No organizationInvites connected to {sourceOrganizationName}", logs, false, true);
                        }

                        dbContextTransaction.Commit();
                    }
                    else
                    {
                        hasError = true;
                        LogError(migrationTitle, result, step, $"No locations connected to {sourceOrganizationName}");
                    }
                }
                catch (Exception ex)
                {
                    hasError = true;
                    LogError(migrationTitle, result, step, $"Details: {ex.Message}");
                    dbContextTransaction.Rollback();
                }
            }

            if (!hasError)
            {
                step = MigrationEntities.Recalculate;
                if (flowInstanceIDs.Any())
                {
                    AppendLog(migrationTitle, result, step, $"* Recalculating {flowInstanceIDs.Length} flowInstanceID(s):{GetIDsBlock(flowInstanceIDs)}", logs, true, false);

                    FlowInstanceSearchValuesBL.CalculateReadValuesBatchAsync(uow, flowInstanceIDs);

                    AppendLog(migrationTitle, result, step, $"+ Done recalculating {flowInstanceIDs.Length} flowInstanceID(s)", logs, true, false);
                }
                else
                {
                    AppendLog(migrationTitle, result, step, "No flowInstanceIDs to recalculate", logs, false, true);
                }

                // For informational purposes only
                if (transferIDs.Any())
                {
                    AppendLog(migrationTitle, result, step, $"* {transferIDs.Count} transferID(s):{GetIDsBlock(transferIDs)}", logs, true, false);
                }
                else
                {
                    AppendLog(migrationTitle, result, step, $"No transferIDs connected to {sourceOrganizationName}", logs, false, true);
                }


                step = MigrationEntities.End;
                AppendLog(migrationTitle, result, step, "*** Migration was successful ***", logs, true, false, true);
            }

            result.HtmlReplaces = GetLocationsForOrganizationID(controller, uow, sourceOrganizationID);

            return result;
        }

        private static string GetIDsBlock(IEnumerable<int> flowInstanceIDs)
        {
            var list = new StringBuilder();
            list.AppendLine(Environment.NewLine);

            foreach (var id in flowInstanceIDs)
            {
                list.AppendFormat("{0}{1}", id, Environment.NewLine);
            }

            return list.ToString();
        }

    }
}
