﻿using System;
using System.Collections.Generic;

namespace Point.MVC.WebApplication.Helpers
{
    public class PointFlowResponse
    {
        public PointFlowResponseMeta meta { get; set; }
        public PointFlowResponseResponse response { get; set; }
        public List<PointFlowResponseErrors> errors { get; set; }
    }

    public class PointFlowResponseMeta
    {
        public bool success { get; set; }
    }

    public class PointFlowResponseResponse
    {
        public Object data { get; set; }
    }

    public class PointFlowResponseErrors
    {
        public string domain { get; set; }
        public string message { get; set;}
    }
}