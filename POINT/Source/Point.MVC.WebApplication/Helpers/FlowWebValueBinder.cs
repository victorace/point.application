﻿using Point.Database.Models.ViewModels;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Point.MVC.WebApplication.Helpers
{
    public class FlowWebFieldBinder : FlowWebValueBinderBase
    {
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            if (bindingContext.ModelType == typeof(List<FlowWebField>))
            {
                var model = GetFlowWebFields(controllerContext);

                controllerContext.Controller.ViewData.Model = model;
                return model;
            }

            // call the default model binder this new binding context
            return base.BindModel(controllerContext, bindingContext);
        }
    }
}