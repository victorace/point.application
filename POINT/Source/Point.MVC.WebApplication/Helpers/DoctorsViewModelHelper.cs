﻿using System;
using System.Collections.Generic;
using System.Linq;
using Point.Business.Helper;
using Point.Business.Logic;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Extensions;
using Point.Models.Enums;
using Point.MVC.WebApplication.Controllers;

namespace Point.MVC.WebApplication.Helpers
{
    public static class DoctorsViewModelHelper
    {
        public static DoctorsViewModel GetIndexViewModel(PointController controller, IUnitOfWork uow, int? regionID, int? organizationID)
        {
            var viewModel = new DoctorsViewModel();
            var pointUserInfo = controller.PointUserInfo();

            if (regionID == null)
            {
                regionID = pointUserInfo.Region.RegionID;
            }

            viewModel.RegionID = regionID;
            viewModel.OrganizationID = organizationID;
            viewModel.PointUserRegionID = pointUserInfo.Region.RegionID;

            var multipleRegions = pointUserInfo.IsGlobalAdmin;
            var regions = (multipleRegions ? RegionBL.GetAll(uow) : new[] { pointUserInfo.Region }).ToList();
            regions.ForEach(reg => viewModel.Regions.Add(new Option(reg.Name, reg.RegionID.ToString(), reg.RegionID == regionID)));

            var multipleOrganizations = pointUserInfo.IsRegioAdmin;
            var organizations = (multipleOrganizations
                ? OrganizationBL.GetActiveByRegionIDAndOrganizationTypeID(uow, regionID.Value, (int)OrganizationTypeID.Hospital)
                : new List<Organization> { pointUserInfo.Organization }).ToList();
            organizations.ForEach(org => viewModel.Organizations.Add(new Option(org.Name, org.OrganizationID.ToString(), org.OrganizationID == organizationID)));

            viewModel.Doctors = DoctorBL.GetByOrganizationID(uow, organizationID.GetValueOrDefault()).ToList();

            viewModel.Regions = viewModel.Regions.OrderBy(r => r.Text).ToList();
            viewModel.Organizations = viewModel.Organizations.OrderBy(r => r.Text).ToList();
            viewModel.Organizations.Insert(0, LookUpBL.OptionEmpty);
            viewModel.Regions.Insert(0, LookUpBL.OptionEmpty);

            return viewModel;
        }

        public static DoctorViewModel GetCreateViewModel(PointController controller, int organizationID)
        {
            var specialisms = SpecialismBL.GetByOrganizationTypeID(controller.uow, (int)OrganizationTypeID.Hospital)
                .ToSelectListItems(s => s.Name, s => s.SpecialismID.ToString(),
                    null, " - Maak uw keuze - ");

            var locations = LocationBL.GetActiveByOrganisationID(controller.uow, organizationID)
                .ToSelectListItems(s => s.Name, s => s.LocationID.ToString());

            controller.ViewBag.Specialisms = specialisms;
            controller.ViewBag.Locations = locations;

            return DoctorViewModel.FromModel(new Doctor { OrganizationID = organizationID });
        }

        public static DoctorViewModel GetEditViewModel(PointController controller, int doctorID)
        {
            var model = DoctorBL.GetByID(controller.uow, doctorID);

            var specialisms = SpecialismBL.GetByOrganizationTypeID(controller.uow, (int)OrganizationTypeID.Hospital)
                .ToSelectListItems(s => s.Name, s => s.SpecialismID.ToString(),
                    s => s.SpecialismID == model.SpecialismID, " - Maak uw keuze - ");

            var locations = LocationBL.GetActiveByOrganisationID(controller.uow, model.OrganizationID.GetValueOrDefault())
                .ToSelectListItems(s => s.Name, s => s.LocationID.ToString(),
                    s => model.DoctorLocation.Select(it => it.LocationID).Contains(s.LocationID));

            controller.ViewBag.Specialisms = specialisms;
            controller.ViewBag.Locations = locations;

            return DoctorViewModel.FromModel(model);
        }

        public static DoctorImportListViewModel GetDoctorImportListViewModel(PointController controller, int organizationID)
        {
            var doctorListViewModel = new DoctorImportListViewModel();
            var errorMsg = "";

            if (organizationID < 1)
            {
                doctorListViewModel.ErrorMessage = "OrganizationID is leeg";
                return doctorListViewModel;
            }

            const string uploadFolder = "~/_Uploads/";
            var uploadFolderFull = controller.Server.MapPath(uploadFolder);
            var requestFileResult = HttpRequestHelper.GetUploadedFile(controller.Request, uploadFolderFull, controller.PointUserInfo());

            if (!requestFileResult.HasErrors)
            {
                // Define expected structure (columns/required) for the CSV
                doctorListViewModel.AddColumnIdentifier(DoctorImportColumnIdentifiers.AGB_LABEL, true);
                doctorListViewModel.AddColumnIdentifier(DoctorImportColumnIdentifiers.Name_LABEL, true);
                doctorListViewModel.AddColumnIdentifier(DoctorImportColumnIdentifiers.SearchName_LABEL, true);
                doctorListViewModel.AddColumnIdentifier(DoctorImportColumnIdentifiers.Specialism_LABEL, true);
                doctorListViewModel.AddColumnIdentifier(DoctorImportColumnIdentifiers.BIG_LABEL);
                doctorListViewModel.AddColumnIdentifier(DoctorImportColumnIdentifiers.Locations_LABEL);
                doctorListViewModel.AddColumnIdentifier(DoctorImportColumnIdentifiers.PhoneNumber_LABEL);

                var locations = controller.uow.LocationRepository.Get(loc => loc.OrganizationID == organizationID && !loc.Inactive)
                .Select(l => new DoctorImportDbItemViewModel { ID = l.LocationID, Name = l.Name.Trim() }).ToList();

                var specialisms = controller.uow.SpecialismRepository.Get(s => !s.Inactive)
                                .Select(l => new DoctorImportDbItemViewModel { ID = l.SpecialismID, Name = l.Name.Trim() }).ToList();

                var fieldValues = CSVHelper.ReadFromLocalStorage(doctorListViewModel.ColumnIdentifiers, requestFileResult.FileNameFullPath, deleteOnCompletion: true);

                if (fieldValues == null)
                {
                    doctorListViewModel.ErrorMessage = "Ongeldige CSV structuur (Zie 'Csv info') ";
                    return doctorListViewModel;
                }

                if (fieldValues.Any())
                {
                    DoctorImportViewModel doctorImport;
                    var count = 0;
                    var doctorsAGB = new List<string>();

                    foreach (var keyValues in fieldValues)
                    {
                        count++;
                        var fieldValueErrors = new Dictionary<string, string>();
                        doctorImport = new DoctorImportViewModel();

                        var value = GetFieldValue(keyValues, fieldValueErrors, DoctorImportColumnIdentifiers.AGB_ID, count, true, doctorImport.AGBItem);
                        if (value != null)
                        {
                            if (doctorsAGB.Contains(value))
                            {
                                var errorMessage = $"Duplicaat van AGB {value}";
                                AddError(fieldValueErrors, $"AGB_{count}_A", errorMessage);
                                doctorImport.IsDuplicate = true;
                                doctorImport.AGBItem.ErrorMessage += errorMessage;
                            }
                            else
                            {
                                doctorsAGB.Add(value);
                            }
                        }

                        GetFieldValue(keyValues, fieldValueErrors, DoctorImportColumnIdentifiers.Name_ID, count, true, doctorImport.NameItem);
                        GetFieldValue(keyValues, fieldValueErrors, DoctorImportColumnIdentifiers.SearchName_ID, count, true, doctorImport.SearchNameItem);

                        doctorImport.PhoneNumber = GetFieldValue(keyValues, fieldValueErrors, DoctorImportColumnIdentifiers.PhoneNumber_ID, count, false);
                        doctorImport.BIG = GetFieldValue(keyValues, fieldValueErrors, DoctorImportColumnIdentifiers.BIG_ID, count, false);

                        value = GetFieldValue(keyValues, fieldValueErrors, DoctorImportColumnIdentifiers.Locations_ID, count, false);
                        if (value != null)
                        {
                            var locationIds = new List<int>();
                            var locationsDoctor = value.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                            foreach (var locationName in locationsDoctor)
                            {
                                var item = GetDoctorImportItem(locations, fieldValueErrors, locationName, count, DoctorImportColumnIdentifiers.Locations_LABEL1);

                                if (item.ID > 0)
                                {
                                    locationIds.Add(item.ID.Value);
                                }
                                doctorImport.Locations.Add(item);
                            }

                            if (locationIds.Any())
                            {
                                doctorImport.LocationIDS = locationIds.ToArray();
                            }
                        }

                        value = GetFieldValue(keyValues, fieldValueErrors, DoctorImportColumnIdentifiers.Specialism_ID, count, true, doctorImport.SpecialismItem);

                        if (value != null && value != DoctorImportViewModel.FlagEmpty)
                        {
                            var item = GetDoctorImportItem(specialisms, fieldValueErrors, value, count, DoctorImportColumnIdentifiers.Specialism_LABEL);
                            doctorImport.SpecialismItem = item;
                            doctorImport.SpecialismID = item.ID;
                        }

                        doctorListViewModel.Doctors.Add(doctorImport);

                        if (fieldValueErrors.Any())
                        {
                            AddError(doctorListViewModel.Errors, $"#{count}", $"# Row {count}:");
                            AddErrors(doctorListViewModel.Errors, fieldValueErrors);
                        }
                    }
                }
                else
                {
                    errorMsg = "Ongeldige CSV structuur (kolommen)";
                }
            }
            else
            {
                errorMsg = requestFileResult.ErrorMessage;
            }

            doctorListViewModel.ErrorMessage = errorMsg;

            return doctorListViewModel;
        }

        private static DoctorImportItemViewModel GetDoctorImportItem(List<DoctorImportDbItemViewModel> dbList, Dictionary<string, string> errors, string findFieldName, int index, string fieldLabel)
        {
            var item = new DoctorImportItemViewModel();

            if (!dbList.Any())
            {
                item.Value = findFieldName.Trim();
                return item;
            }

            var location = dbList.FirstOrDefault(i => i.NameUpper == findFieldName.Trim().ToUpperInvariant());
            if (location != null)
            {
                item.ID = location.ID;
                item.Value = location.Name;
            }
            else
            {
                item.Value = findFieldName.Trim();
                var msg = $"{fieldLabel} '{item.Value}' is ongeldig";
                AddError(errors, $"{fieldLabel}_{index}", msg);
                item.ErrorMessage = msg;
            }

            return item;
        }

        private static string GetFieldValue(Dictionary<string, object> keyValues, Dictionary<string, string> errors, string fieldName, int rowIndex, bool isRequired, DoctorImportItemViewModel item = null)
        {
            if (!keyValues.TryGetValue(fieldName.ToUpperInvariant(), out var obj))
            {
                return null;
            }

            var setItem = item != null;
            var objVal = (string)obj;
            if (!string.IsNullOrEmpty(objVal))
            {
                if (setItem)
                {
                    item.Value = objVal;
                }
                return objVal;
            }

            if (!isRequired)
            {
                return objVal;
            }

            objVal = DoctorImportViewModel.FlagEmpty;
            var msg = $"{fieldName} is verplicht";
            AddError(errors, $"{fieldName}_{rowIndex}", msg);
            if (setItem)
            {
                item.Value = objVal;
                item.ErrorMessage = msg;
            }

            return objVal;
        }

        private static void AddError(Dictionary<string, string> errorsTarget,  string key, string value)
        {
            if (errorsTarget.ContainsKey(key))
            {
                return;
            }
            errorsTarget.Add(key, value);
        }

        public static void AddErrors(Dictionary<string, string> errorsTarget, Dictionary<string, string> errorsToAdd)
        {
            foreach (var error in errorsToAdd)
            {
                if (errorsTarget.ContainsKey(error.Key))
                {
                    continue;
                }
                errorsTarget.Add(error.Key, error.Value);
            }
        }

    }
}
