﻿using Point.Business.Logic;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Infrastructure.Extensions;
using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Web;

namespace Point.MVC.WebApplication.Helpers
{
    public class UploadHelper
    {
        private readonly HttpFileCollectionBase _httpfilecollectionbase;
        private readonly TransferAttachmentViewModel _model;
        private readonly int _transferid;

        //validate file signatures for upload, signatures are in Dec format.
        private static readonly Dictionary<string, byte[]> UploadFileSignature = new Dictionary<string, byte[]>
        {
            {".DOCX", new byte[] { 80, 75, 3, 4 } },
            {".PDF", new byte[] { 37, 80, 68, 70 } },
            {".DOC", new byte[] { 208,207,17,224 } },
            {".XPS", new byte[] { 80, 75, 3, 4 } },
            {".JPG", new byte[] { 255, 216, 255 } },
            {".PNG", new byte[] { 137, 80, 78, 71, 13, 10, 26, 10 }  },
            {".XLS", new byte[] { 208,207,17,224 } },
            {".XLSX", new byte[] { 80, 75, 3, 4 } },
            {".MSG", new byte[] { 208, 207, 17, 224, 161, 177, 26, 225 } },
            {".EML", new byte[] { 70, 114, 111, 109 } },
            {"PasswordProtected" , new byte [] { 208,207,17,224 } }
        };
        
        private static List<string> passwordProtectedExtentions = new List<string>(new string[] { ".DOC", ".DOCX", ".XLS", ".XLSX" });

        public TransferAttachment TransferAttachment { get; private set; }

        public string Error { get; private set; }

        public UploadHelper(int transferid, HttpFileCollectionBase httpfilecollectionbase, TransferAttachmentViewModel model)
        {
            Error = "";
            _transferid = transferid;
            _httpfilecollectionbase = httpfilecollectionbase;
            _model = model;
        }

        public static bool ValidateContent(HttpPostedFileBase file)
        {
            if (file != null && file.ContentLength > 0)
            { 
                byte[] buffer = new byte[256];

                MemoryStream ms = new MemoryStream();
                file.InputStream.CopyTo(ms);
                file.InputStream.Position = ms.Position = 0;

                ms.Read(buffer, 0, Math.Min(file.ContentLength, 256));

                var fileinfo = new FileInfo(file.FileName);
                var extension = fileinfo.Extension.ToUpper();

                if (UploadFileSignature.ContainsKey(extension) == false)
                {
                    if (!string.IsNullOrEmpty(extension))
                    {
                        throw new Exception(String.Format("Extensie {0} wordt niet ondersteund.", extension));
                    }
                    else
                    {
                        throw new Exception("Bestanden zonder extensie wordt niet ondersteund.");
                    }
                }

                //******* Content control is uitgezet vanwege onduidelijke file signatures ******//

                //var filesignature = UploadFileSignature[extension];
                //var buffersignature = buffer.Take(filesignature.Length);
                //var validsignature = filesignature.SequenceEqual(buffersignature);
                
                ////checkeing for password protected word and excel files.
                //validsignature = validsignature || 
                //    (passwordProtectedExtentions.Contains(extension) && UploadFileSignature["PasswordProtected"].SequenceEqual(buffersignature));
                //if(!validsignature)
                //{                    
                //    throw new Exception(String.Format("Geen valide '{1}' bestand.", file.FileName, extension));
                //}

                return true;
            }

            return false;
        }
        public bool CreateAttachment()
        {
            if (_httpfilecollectionbase.Count > 0)
            {
                var httppostedfilebase = _httpfilecollectionbase[0];
                var extension = Path.GetExtension(httppostedfilebase?.FileName);

                var allowedextensions = _model.OnlyPDF ? ".pdf" : ".pdf .doc .docx .xps .jpg .jpeg .png .xls .xlsx .msg .eml";
                if (allowedextensions.Contains(extension, StringComparison.InvariantCultureIgnoreCase) && httppostedfilebase != null && 
                    httppostedfilebase.ContentLength > 0 && ValidateContent(httppostedfilebase))
                {
                    if (httppostedfilebase.ContentLength <= (10 * 1024 * 1024))
                    {                         
                        try
                        {
                            TransferAttachment = new TransferAttachment
                            {
                                TransferID = _transferid,
                                Name = _model.Name,
                                Description = _model.Description,
                                AttachmentSource = _model.AttachmentSource,
                                AttachmentTypeID = _model.AttachmentTypeID
                            };

                            TransferAttachmentBL.FillFileData(TransferAttachment, httppostedfilebase);
                        }
                        catch
                        {
                            Error = "Upload mislukt.";
                        }
                    }
                    else
                    {
                        Error = "Het gekozen bestand is te groot, maximaal 10 MB.";
                    }
                }
                else
                {
                    Error = "Het gekozen bestandstype wordt niet ondersteund.";
                }
            }
            else
            {
                Error = "Selecteer eerst een bestand.";
            }

            return string.IsNullOrWhiteSpace(Error);
        }
    }
}