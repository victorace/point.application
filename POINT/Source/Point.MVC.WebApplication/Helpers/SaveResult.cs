﻿using Point.Infrastructure.Extensions;
using System;
using System.Collections.Generic;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Script.Serialization;

namespace Point.MVC.WebApplication.Helpers
{
    public class SaveResult : ActionResult
    {
        [ScriptIgnore]
        public string ActionName { get; set; }
        [ScriptIgnore]
        public string ControllerName { get; set; }
        [ScriptIgnore]
        public RouteValueDictionary RouteValues { get; set; }

        public int TransferID { get; set; }
        public bool ForceReload { get; set; }
        public int PhaseDefinitionID { get; set; }
        public int FormTypeID { get; set; }
        public int FormSetVersionID { get; set; }
        public string Url { get; private set; }
        public List<string> ValidationErrors { get; set; }
        public bool ShowErrors { get; set; }

        public override void ExecuteResult(ControllerContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException("JsonSaveResult context");
            }

            var urlhelper = new UrlHelper(context.RequestContext);
            Url = urlhelper.Action(ActionName, ControllerName, RouteValues);

            HttpResponseBase response = context.HttpContext.Response;
            response.StatusCode = (int)HttpStatusCode.OK;

            response.ContentType = "application/json";

            JavaScriptSerializer serializer = new JavaScriptSerializer();

            response.Write(serializer.Serialize(this));
        }
    }
}