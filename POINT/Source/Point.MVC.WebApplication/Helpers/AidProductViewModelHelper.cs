﻿using System.Collections.Generic;
using System.Linq;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Models.Enums;

namespace Point.MVC.WebApplication.Helpers
{
    public class AidProductViewModelHelper
    {
        public static AidProductSearchViewModel GetAidProductSearchViewModel(IUnitOfWork uow, int supplierID, int uzovi)
        {
            var viewmodel = new AidProductSearchViewModel
            {
                AidProductGroups = supplierID == (int)SupplierID.MediPoint ? GetGroupsBySupplierIDAndUzovi(uow, supplierID, uzovi) : GetGroupsBySupplierID(uow, supplierID).ToList(),
                SupplierID = supplierID
            };

            return viewmodel;
        }

        // Intended for Vegro & Point
        public static List<AidProductGroupViewModel> GetGroupsBySupplierID(IUnitOfWork uow, int supplierid)
        {
            return SetAidProductGroupOrder((from ag in uow.AidProductGroupRepository.Get()
                where ag.AidProducts.Any(prod => prod.SupplierID == supplierid)
                select new AidProductGroupViewModel { Name = ag.Name, AidProductGroupID = ag.AidProductGroupID }).ToList());
        }

        public static List<AidProductGroupViewModel> GetGroupsBySupplierIDAndUzovi(IUnitOfWork uow, int supplierid, int uzovi)
        {
            var uzoviString = GetUzoviString(uzovi);
            return SetAidProductGroupOrder((from ag in uow.AidProductGroupRepository.Get()
                    where ag.AidProducts.Any(prod => prod.SupplierID == supplierid && prod.InsurersUZOVIList.Contains(uzoviString))
                    select new AidProductGroupViewModel { Name = ag.Name, AidProductGroupID = ag.AidProductGroupID }).ToList());
        }

        public static List<AidProductViewModel> GetProductsBySupplierIDAndUzovi(IUnitOfWork uow, int supplierID, int uzovi)
        {
            var uzoviString = GetUzoviString(uzovi);
            return uow.AidProductRepository.Get(ap => ap.SupplierID == supplierID && ap.InsurersUZOVIList.Contains(uzoviString)).Select(ap => new AidProductViewModel { Name = ap.Name, AidProductID = ap.AidProductID, AidProductGroupID = ap.AidProductGroupID, Description = ap.Description, Price = ap.Price, PricePerDay = ap.PricePerDay }).ToList();
        }

        public static List<AidProductViewModel> GetProductsBySupplierID(IUnitOfWork uow, int supplierID)
        {
            return uow.AidProductRepository.Get(ap => ap.SupplierID == supplierID).Select(ap => new AidProductViewModel { Name = ap.Name, AidProductID = ap.AidProductID, AidProductGroupID = ap.AidProductGroupID, Description = ap.Description, Price = ap.Price, PricePerDay = ap.PricePerDay }).ToList();
        }
        
        private static List<AidProductGroupViewModel> SetAidProductGroupOrder(List<AidProductGroupViewModel> groups)
        {
            return groups.OrderByDescending(x => x.IsGroupType).ThenBy(x => x.Name).ToList();
        }
        
        // Intended for MediPoint
        private static string GetUzoviString(int uzovi)
        {
            return $"[{uzovi}]";
        }

    }
}