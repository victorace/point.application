﻿using Point.Business.Helper;
using Point.Database.Context;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Helpers;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Point.Database.Models.Constants;

namespace Point.MVC.WebApplication.Helpers
{
    public static class MenuHelper
    {
        public static List<MenuItemViewModel> BuildServiceMenu()
        {
            var menuitems = new List<MenuItemViewModel>();
            using (var uow = new UnitOfWork<PointContext>())
            {
                var pointuserinfo = PointUserInfoHelper.GetPointUserInfo(uow);
                
                var urlhelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
                var managementarea = new RouteValueDictionary() { { "Area", "Management" } };
                var basearea = new RouteValueDictionary() { { "Area", "" } };
                
                menuitems.Add(new MenuItemViewModel("Accountgegevens aanpassen", urlhelper.Action("Details", "Account", basearea), ""));
                
                if (pointuserinfo?.Organization?.MFARequired == true || (pointuserinfo.Employee != null && pointuserinfo.Employee.MFARequired))
                {
                    var action = PointUserInfoHelper.GetMFAAction(pointuserinfo, "ChangeMFA");
                    if (!string.IsNullOrEmpty(action))
                    {
                        menuitems.Add(new MenuItemViewModel("Nummer aanpassen (extra beveiliging)", urlhelper.Action(action, "Account", basearea), ""));
                    }
                }
                menuitems.Add(new MenuItemViewModel("Servicepagina bekijken", urlhelper.Action("Index", "ServicePage", managementarea), ""));
                menuitems.Add(new MenuItemViewModel("Systeemberichten overzicht", urlhelper.Action("Overview", "SystemMessage", managementarea), ""));
                menuitems.Add(new MenuItemViewModel("Wachtwoord wijzigen", urlhelper.Action("ChangePassword", "Account", basearea), ""));

                var domainPointOud = ConfigHelper.GetAppSettingByName<string>("DomainPointOud", "");
                if (!string.IsNullOrEmpty(domainPointOud))
                {
                    menuitems.Add(new MenuItemViewModel("Instructie", string.Concat("https://", domainPointOud, "/Documents/Instructies/copy_of_start_instructie_zh_vvt.aspx"), ""));
                }
                menuitems.Add(new MenuItemViewModel("Support", urlhelper.Action("Questions", "ServicePage", managementarea), ""));
                menuitems.Add(new MenuItemViewModel("Handleiding", urlhelper.Action("Handleiding", "ServicePage", managementarea), "_blank"));
                var releaseInfoUrl = ConfigHelper.GetAppSettingByName("ReleaseInfoUrl", "");
                if(!string.IsNullOrEmpty(releaseInfoUrl))
                {
                    menuitems.Add(new MenuItemViewModel("Releaseinfo", releaseInfoUrl, "_blank"));
                }
                menuitems.Add(new MenuItemViewModel("Privacyverklaring", urlhelper.Action("Privacyverklaring", "ServicePage", managementarea), ""));
            }
            return menuitems;
        }

        public static string GetMenuTypeFromGeneralAction(int generalActionTypeName)
        {
            string menutype;
            switch ((GeneralActionTypeName)generalActionTypeName)
            {
                case GeneralActionTypeName.MenuProcess:
                    menutype = MenuTypes.Proces; break;
                case GeneralActionTypeName.MenuTransfer:
                    menutype = MenuTypes.Aanvullend; break;
                case GeneralActionTypeName.Menu:
                    menutype = MenuTypes.Actie; break;
                default:
                    menutype = MenuTypes.Actie; break;
            }
            return menutype;
        }

        public static string GetMenuTypeFromMaincategory(string mainCategory)
        {
            string menutype;
            switch(mainCategory.ToLower())
            {
                case "proces":
                    menutype = MenuTypes.Proces; break;
                case "aanvullend":
                    menutype = MenuTypes.Aanvullend; break;
                case "actie":
                    menutype = MenuTypes.Actie; break;
                default:
                    menutype = ""; break;
            }
            return menutype;
        }

        public static string GetUrl(UrlHelper urlHelper, int transferID, int phaseDefinitionID, int? phaseInstanceID, string tabGroup)
        {
            if(!string.IsNullOrEmpty(tabGroup))
            {
                return urlHelper.Action("StartTabForm", "FlowMain", new
                {
                    TransferID = transferID,
                    TabGroup = tabGroup
                });
            }

            return urlHelper.Action("StartFlowForm", "FlowMain", new
            {
                TransferID = transferID,
                PhaseDefinitionID = phaseDefinitionID,
                PhaseInstanceID = phaseInstanceID
            });
        }
    }
}