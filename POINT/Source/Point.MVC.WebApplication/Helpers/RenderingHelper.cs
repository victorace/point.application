﻿using System.IO;
using System.Web.Mvc;

namespace Point.MVC.WebApplication.Helpers
{
    public static class RenderingHelper
    {
        public static string RenderPartialViewToString(this Controller controller, string viewName, object model)
        {
            if (controller == null || controller.ControllerContext == null)
            {
                return string.Empty;
            }

            controller.ViewData.Model = model;

            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName);

                var viewContext = new ViewContext(
                    controller.ControllerContext,
                    viewResult.View,
                    controller.ViewData,
                    controller.TempData,
                    sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }
    }
}