﻿using System.IO;
using System.Linq;
using System.Web;
using Point.Database.Models.ViewModels;

namespace Point.MVC.WebApplication.Helpers
{
    public static class HttpRequestHelper
    {
        public static string[] InternetExplorer = { "IE", "INTERNETEXPLORER" };
        public static string[] Firefox = { "FIREFOX" };
        public static string[] Safari = { "SAFARI", "APPLEMAC-SAFARI" };
        public static string[] Chrome = { "CHROME" };

        public static bool IsBrowserInternetExplorer(HttpRequestBase request)
        {
            return InternetExplorer.Contains(request.Browser.Browser.ToUpper());
        }

        public static bool IsBrowserFirefox(HttpRequestBase request)
        {
            return Firefox.Contains(request.Browser.Browser.ToUpper());
        }

        public static bool IsBrowserSafari(HttpRequestBase request)
        {
            return Safari.Contains(request.Browser.Browser.ToUpper());
        }

        public static bool IsBrowserChrome(HttpRequestBase request)
        {
            return Chrome.Contains(request.Browser.Browser.ToUpper());
        }

        public static bool IsBrowserSupported(HttpRequestBase request, int minimumVersionInternetExplorer = -1, int minimumVersionFirefox=-1, int minimumVersionSafari = -1, int minimumVersionChrome = -1)
        {
            if (minimumVersionInternetExplorer > -1 && IsBrowserInternetExplorer(request))
            {
                return request.Browser.MajorVersion >= minimumVersionInternetExplorer;
            }
            if (minimumVersionFirefox > -1 && IsBrowserFirefox(request))
            {
                return request.Browser.MajorVersion >= minimumVersionFirefox;
            }
            if (minimumVersionSafari > -1 && IsBrowserSafari(request))
            {
                return request.Browser.MajorVersion >= minimumVersionSafari;
            }
            if (minimumVersionChrome > -1 && IsBrowserChrome(request))
            {
                return request.Browser.MajorVersion >= minimumVersionChrome;
            }
            return true;
        }

        public static RequestFileResult GetUploadedFile(HttpRequestBase request, string localStorageUploadFolderFullPath, PointUserInfo pointUserInfo = null)
        {
            var result = new RequestFileResult();

            HttpFileCollectionBase files = request.Files;

            if (files.Count == 0)
            {
                // No error, simply no files uploaded
                return result;
            }

            if (!Directory.Exists(localStorageUploadFolderFullPath))
            {
                result.ErrorMessage = $"{localStorageUploadFolderFullPath} bestaat niet";
                return result;
            }

            try
            {
                HttpPostedFileBase file = files[0];
                string fileName;

                if (IsBrowserInternetExplorer(request))
                {
                    var filesX = file.FileName.Split('\\');
                    fileName = filesX[filesX.Length - 1];
                }
                else
                {
                    fileName = file.FileName;
                }

                result.FileName = fileName;

                if (pointUserInfo != null)
                {
                    // Get a unique file name
                    fileName = GetUniqueNameFromUserInfo(pointUserInfo);
                }

                var fileNameUploadFull = Path.Combine(localStorageUploadFolderFullPath, fileName);
                file.SaveAs(fileNameUploadFull);
               
                result.FileNameFullPath = fileNameUploadFull;
            }
            catch (System.Exception ex)
            {
                result.ErrorMessage = ex.Message;
            }

            return result;
        }

        private static string GetUniqueNameFromUserInfo(PointUserInfo pointUserInfo)
        {
            var id = $"{pointUserInfo.Organization?.OrganizationID}{pointUserInfo.Employee?.UserID}";
            // Scramble ids so they're not publicly visible to the eye
            var parts = id.Split('-');
            var fileName = "";
            for (var i = parts.Length-1; i >= 0; i--)
            {
                fileName += parts[i];
            }
            return fileName;
        }

        public class RequestResult
        {
            public string ErrorMessage { get; set; }
            public bool HasErrors => !string.IsNullOrEmpty(ErrorMessage);
        }

        public class RequestFileResult : RequestResult
        {
            public string FileName { get; set; }
            public string FileNameFullPath { get; set; }
        }

    }
}