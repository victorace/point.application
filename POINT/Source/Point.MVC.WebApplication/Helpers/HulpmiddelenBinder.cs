﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using Point.Database.Models.ViewModels;

namespace Point.MVC.WebApplication.Helpers
{
    public class HulpmiddelenBinder : FlowWebValueBinderBase
    {
        private static Regex _stripIDAndFieldnameOrderItemRegex = new Regex(AidProductOrderItemViewModel.NAME_FORMAT_REGEXP, RegexOptions.Compiled);

        public List<AidProductOrderItemViewModel> GetOrderItems(ControllerContext controllerContext)
        {
            HttpRequestBase request = controllerContext.HttpContext.Request;
            List<AidProductOrderItemViewModel> orderitems = new List<AidProductOrderItemViewModel>();

            foreach (string key in request.Form.AllKeys)
            {
                if (key != null && key.StartsWith(AidProductOrderItemViewModel.NAME_PREFIX))
                {
                    string protectedName = key.Remove(0, AidProductOrderItemViewModel.NAME_PREFIX.Length);
                    if (!String.IsNullOrWhiteSpace(protectedName))
                    {
                        var match = _stripIDAndFieldnameOrderItemRegex.Match(protectedName);
                        if (match.Success)
                        {
                            string value = request.Form.Get(key);

                            int productid = int.TryParse(match.Groups["productid"].Value, out productid) ? productid : 0;
                            string fieldname = match.Groups["fieldname"].Value;

                            var orderitem = orderitems.FirstOrDefault(it => it.AidProductID == productid);
                            if (orderitem == null)
                            {
                                orderitem = new AidProductOrderItemViewModel();
                                orderitem.AidProductID = productid;

                                orderitems.Add(orderitem);
                            }

                            switch (fieldname)
                            {
                                case "aidproductorderitemid":
                                    int aidproductorderitemid = int.TryParse(value, out aidproductorderitemid) ? aidproductorderitemid : 0;
                                    orderitem.AidProductOrderItemID = aidproductorderitemid;
                                    break;
                                case "quantity":
                                    int quantity = int.TryParse(value, out quantity) ? quantity : 0;
                                    orderitem.Quantity = quantity;
                                    break;
                                case "name":
                                    orderitem.Name = value;
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                }
            }

            return orderitems;
        }

        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            if (bindingContext.ModelType == typeof(HulpmiddelenViewModel))
            {
                HttpRequestBase request = controllerContext.HttpContext.Request;
                var viewmodel = base.BindModel(controllerContext, bindingContext) as HulpmiddelenViewModel;
                viewmodel.SetGlobalProperties();
                viewmodel.Global.FlowWebFields = GetFlowWebFields(controllerContext);
                viewmodel.OrderItems = GetOrderItems(controllerContext);

                controllerContext.Controller.ViewData.Model = viewmodel;
                return viewmodel;
            }

            // call the default model binder this new binding context
            return base.BindModel(controllerContext, bindingContext);
        }
    }
}