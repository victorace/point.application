﻿using Point.Database.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace Point.MVC.WebApplication.Helpers
{
    public class CopyTransferBinder : DefaultModelBinder
    {
        private static Regex _idregex = new Regex(CopyTransferViewModel.NAME_FORMAT_REGEXP, RegexOptions.Compiled);

        public List<CopyTransferFormViewModel> GetCopyTransferForm(ControllerContext controllerContext)
        {
            HttpRequestBase request = controllerContext.HttpContext.Request;
            List<CopyTransferFormViewModel> copytransferforms = new List<CopyTransferFormViewModel>();

            foreach (string key in request.Form.AllKeys)
            {
                if (key != null && key.StartsWith(CopyTransferViewModel.NAME_PREFIX))
                {
                    string protectedName = key.Remove(0, CopyTransferViewModel.NAME_PREFIX.Length);
                    if (!String.IsNullOrWhiteSpace(protectedName))
                    {
                        var match = _idregex.Match(protectedName);
                        if (match.Success)
                        {
                            int formsetversionid = int.TryParse(match.Groups["formsetversionid"].Value, out formsetversionid) ? formsetversionid : 0;
                            int formtypeid = int.TryParse(match.Groups["formtypeid"].Value, out formtypeid) ? formtypeid : 0;

                            copytransferforms.Add(new CopyTransferFormViewModel()
                            {
                                FormSetVersionID = formsetversionid,
                                FormTypeID = formtypeid
                            });
                        }
                    }
                }
            }

            return copytransferforms;
        }

        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            if (bindingContext.ModelType == typeof(CopyTransferViewModel))
            {
                HttpRequestBase request = controllerContext.HttpContext.Request;
                var viewmodel = base.BindModel(controllerContext, bindingContext) as CopyTransferViewModel;
                viewmodel.CopyTransferForms = GetCopyTransferForm(controllerContext);

                controllerContext.Controller.ViewData.Model = viewmodel;
                return viewmodel;
            }
            else
            {
                // call the default model binder this new binding context
                return base.BindModel(controllerContext, bindingContext);
            }
        }
    }
}