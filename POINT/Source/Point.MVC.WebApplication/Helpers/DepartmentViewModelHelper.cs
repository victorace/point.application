﻿using System;
using System.Collections.Generic;
using System.Linq;
using Point.Business.Logic;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Exceptions;
using Point.Models.Enums;
using Point.MVC.WebApplication.Areas.Management.Controllers;
using AfterCareType = Point.Database.Models.AfterCareType;

namespace Point.MVC.WebApplication.Helpers
{
    public static class DepartmentViewModelHelper
    {
        const char _separator = ',';

        public static DepartmentSearchViewModel GetDepartmentSearchViewModel(int? regionID, string organizationIDs, string locationIDs, string departmentIDs, string selectedDepartmentIDs)
        {
            return new DepartmentSearchViewModel
            {
                Search = "DepartmentSearch",
                RegionID = regionID,
                OrganizationIDs = GetIDsAsStringArray(organizationIDs),
                LocationIDs = GetIDsAsStringArray(locationIDs),
                DepartmentIDs = GetIDsAsStringArray(departmentIDs),
                SelectedDepartmentIDs = GetIDsAsStringArray(selectedDepartmentIDs)
            };
        }

        private static string[] GetIDsAsStringArray(string ids, bool nullWhenEmpty = true)
        {
            return string.IsNullOrEmpty(ids) ? (nullWhenEmpty ? null : new string[] { }) : ids.Split(_separator);
        }

        private static int[] GetIDsAsIntArray(string ids, bool nullWhenEmpty=true)
        {
            return string.IsNullOrEmpty(ids) ? (nullWhenEmpty ? null : new int[]{}) : ids.Split(_separator).Select(it => Convert.ToInt32(it)).ToArray();
        }
        public static DepartmentSearchDataViewModel GetDepartmentSearchDataViewModel(IUnitOfWork uow, int? regionID, string organizationIDs, string locationIDs, string departmentIDs, string selectedDepartmentIDs, string naamPlaats, string sortBy)
        {
            var organizatioIDArr = GetIDsAsIntArray(organizationIDs);
            var locationIDArr = GetIDsAsIntArray(locationIDs);
            var departmentIDArr = GetIDsAsIntArray(departmentIDs);
            var selectedDepartmentIDArr = GetIDsAsIntArray(selectedDepartmentIDs, false);

            var organizationSearches = OrganizationSearchBL.GetDepartments(uow, regionID, null, organizatioIDArr, locationIDArr, departmentIDArr, naamPlaats);
            var departments = DepartmentSearchViewModelAdmin.FromModelList(organizationSearches, selectedDepartmentIDArr).ToList();
            
            //controller.ViewBag.SortBy = sortBy;     // ViewBar needed by parent OrganizationSearch
            
            var viewModel = new DepartmentSearchDataViewModel
            {
                Departments = string.IsNullOrEmpty(sortBy)
                    ? departments.OrderBy(hsp => hsp.LocationName).ThenBy(hsp => hsp.DepartmentName).ToList()
                    : departments.OrderBy(sortBy).ToList()
            };
            
            return viewModel;
        }

        private static List<Location> GetLocations(IUnitOfWork uow, PointUserInfo pointUserInfo, FunctionRoleLevelID maxLevel, int? organizationID)
        {
            if (!organizationID.HasValue)
            {
                return new List<Location>();
            }

            int[] locationIDFilter = null;
            if (maxLevel <= FunctionRoleLevelID.Location)
            {
                locationIDFilter = pointUserInfo.EmployeeLocationIDs.ToArray();
            }
            return LocationBL.GetActiveByOrganisationID(uow, organizationID.Value, locationIDFilter).ToList();
        }

        public static DepartmentViewModel GetEditViewModel(DepartmentController controller, int? departmentID, int? selectedLocationID = null, int? selectedOrganizationID = null)
        {
            var model = departmentID.HasValue
                ? DepartmentBL.GetByDepartmentID(controller.uow, departmentID.Value)
                : null;

            var pointUserInfo = controller.PointUserInfo();
            var maxLevel = pointUserInfo.GetFunctionRoleLevelID(FunctionRoleTypeID.ManageOrganization);

            var organizations = new List<Organization>();
            var locations = new List<Location>();
            var departmentTypes = new List<DepartmentType>();
            var afterCareTypes = new List<AfterCareType>();

            Organization selectedOrganization = null;

            switch (maxLevel)
            {
                case FunctionRoleLevelID.Global:
                    organizations = OrganizationBL.GetActive(controller.uow).OrderBy(o => o.Name).ToList();
                    break;
                case FunctionRoleLevelID.Region:
                    organizations = OrganizationBL.GetActiveByRegionID(controller.uow, pointUserInfo.Region.RegionID).OrderBy(o => o.Name).ToList();
                    break;
                default:
                    organizations = new List<Organization> { pointUserInfo.Organization };
                    selectedOrganization = pointUserInfo.Organization;
                    break;
            }

            //check if the user has rights to the organizationID passed in the querystring
            if (selectedOrganizationID.HasValue && !organizations.Select(o => o.OrganizationID).Contains(selectedOrganizationID.Value))
            {
                throw new PointSecurityException("U heeft geen toegang tot deze organisatie", ExceptionType.AccessDenied);
            }

            if (model != null)
            {
                if (model.Location?.Organization != null)
                {
                    organizations = organizations.Where(o => o.OrganizationID == model.Location.OrganizationID).ToList();
                    selectedOrganization = organizations.FirstOrDefault();
                }
            }
            else if (selectedOrganizationID.HasValue)
            {
                selectedOrganization = OrganizationBL.GetByOrganizationID(controller.uow, selectedOrganizationID.Value);
            }
            else if (selectedLocationID.HasValue)
            {
                selectedOrganization = OrganizationBL.GetByLocationID(controller.uow, selectedLocationID.Value);
            }

            if (selectedOrganization != null)
            {
                locations = GetLocations(controller.uow, pointUserInfo, maxLevel, selectedOrganization.OrganizationID).OrderBy(it => it.Name).ToList();

                var organizationTypeID = selectedOrganization.OrganizationTypeID;
                departmentTypes = DepartmentTypeBL.GetByOrganizationTypeID(controller.uow, organizationTypeID).OrderBy(it => it.Name).ToList();
                afterCareTypes = AfterCareTypeBL.GetByOrganizationTypeID(organizationTypeID).OrderBy(o => o.Name).ToList();
            }

            var flowDefinitions = FlowDefinitionBL.GetActiveFlowDefinitions(controller.uow).ToList();

            var locationAutoCreateSets = new List<AutoCreateSet>();
            if (model?.Location?.AutoCreateSet != null)
            {
                locationAutoCreateSets = model.Location.AutoCreateSet.ToList();
            }

            var viewModel = DepartmentViewBL.FromModel(model, flowDefinitions, locationAutoCreateSets);

            organizations.ForEach(org => viewModel.Organizations.Add(new Option(org.Name, org.OrganizationID.ToString(), org.OrganizationID == model?.Location?.OrganizationID)));
            locations.ForEach(loc => viewModel.Locations.Add(new Option(loc.Name, loc.LocationID.ToString(), loc.LocationID == model?.LocationID)));
            departmentTypes.ForEach(dt => viewModel.DepartmentTypes.Add(new Option(dt.Name, dt.DepartmentTypeID.ToString(), dt.DepartmentTypeID == model?.DepartmentTypeID)));
            afterCareTypes.ForEach(act => viewModel.AfterCareTypes1.Add(new Option(act.Name, act.AfterCareTypeID.ToString(), act.AfterCareTypeID == model?.AfterCareType1ID)));

            viewModel.FunctionRoleLevelID = maxLevel;               //***
            controller.ViewBag.FunctionRoleLevelID = maxLevel;      // TODO: Remove after verifying it's obsolete

            if (model == null && selectedLocationID.HasValue && selectedOrganizationID.HasValue)
            {
                viewModel.LocationID = selectedLocationID.Value;
                viewModel.OrganizationID = selectedOrganizationID.Value;
                viewModel.Organization = selectedOrganization;
            }

            viewModel.ShowCapacityFunctionality = viewModel.Organization?.OrganizationTypeID == (int)OrganizationTypeID.HealthCareProvider ||
                                                  viewModel.Organization?.OrganizationTypeID == (int)OrganizationTypeID.Hospital ||
                                                  viewModel.DepartmentTypeID == DepartmentTypeID.Transferpunt;

            return viewModel;
        }
    }
}