﻿using Point.Business.Logic;
using Point.Communication;
using Point.Communication.HL7Service;
using Point.Database.Extensions;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Point.MVC.WebApplication.Helpers
{
    public class EndpointConfigurationHelper
    {
        private const string ERROR_KEY = "Error";

        private Dictionary<string, string> data;
        private EndpointConfiguration endpointconfiguration;
        private Client client;
        
        public EndpointConfigurationHelper(EndpointConfiguration endpointconfiguration, Client client)
        {
            this.endpointconfiguration = endpointconfiguration ?? throw new ArgumentNullException(nameof(endpointconfiguration));
            this.client = client ?? throw new ArgumentNullException(nameof(client));
        }

        public IEnumerable<FlowWebField> Merge(IEnumerable<FlowWebField> flowwebfields, List<Source> exclude)
        {
            if (flowwebfields == null)
            {
                throw new ArgumentNullException(nameof(flowwebfields));
            }

            if (exclude == null)
            {
                throw new ArgumentNullException(nameof(exclude));
            }

            data = get_endpoint_data();

            if (data.ContainsKey(ERROR_KEY) && !string.IsNullOrEmpty(data[ERROR_KEY])) {
                return flowwebfields;
            }

            return merge_endpoint_data_with_flowwebfields(flowwebfields, exclude);
        }

        private Dictionary<string, string> get_endpoint_data()
        {
            var epdrequest = new EPDRequest()
            {
                BirthDate = client.BirthDate,
                CivilServiceNumber = client.CivilServiceNumber,
                Gender = client.Gender.EPDRequestGender(),
                PatientNumber = client.PatientNumber,
                VisitNumber = client.VisitNumber
            };

            var hl7epdfetcher = new HL7EPDReceiver();
            var data = hl7epdfetcher.Get(endpointconfiguration, epdrequest);
            return data;
        }

        // TODO: this function should return a new list of FlowWebField and not modified the function parameters - PP
        private IEnumerable<FlowWebField> merge_endpoint_data_with_flowwebfields(IEnumerable<FlowWebField> flowwebfields, List<Source> exclude)
        {
            var copyflowwebfields = new List<FlowWebField>(flowwebfields).ToList();
            foreach (var flowwebfield in copyflowwebfields.Where(fwf => !exclude.Contains(fwf.Source)))
            {
                string key = flowwebfield.Name;
                if (data.ContainsKey(key))
                {
                    string value = data[key];
                    if (value != null)
                    {
                        FlowWebFieldBL.SetValue(copyflowwebfields, flowwebfield.FlowFieldID, value, Source.WEBSERVICE);
                    }
                }
                else
                {
                    // TODO: Log missing data from Endpoint - PP
                }
            }
            return copyflowwebfields;
        }
    }
}