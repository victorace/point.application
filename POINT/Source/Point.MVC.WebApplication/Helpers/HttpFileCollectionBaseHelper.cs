﻿using Point.Database.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Point.MVC.WebApplication.Controllers;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace Point.MVC.WebApplication.Helpers
{
    public class HttpFileCollectionBaseHelper
    {
        public static readonly IDictionary<string, string> ImageMimeDictionary = new Dictionary<string, string>
            {
                { ".bmp", "image/bmp" },
                { ".dib", "image/bmp" },
                { ".gif", "image/gif" },
                { ".svg", "image/svg+xml" },
                { ".jpe", "image/jpeg" },
                { ".jpeg", "image/jpeg" },
                { ".jpg", "image/jpeg" },
                { ".png", "image/png" },
                { ".pnz", "image/png" }
            };

        public static readonly IDictionary<string, string> TransferAttachmentMimeDictionary = new Dictionary<string, string>
            {
                { ".pdf", "application/pdf" },
                { ".doc", "application/msword" },
                { ".docx", "application/vnd.openxmlformats-officedocument.wordprocessingml.document" },
                { ".xps", "application/vnd.ms-xpsdocument" },
                { ".jpg", "image/jpeg" },
                { ".jpeg", "image/jpeg" },
                { ".png", "image/jpeg" },
                { ".xls", "application/vnd.ms-excel" },
                { ".xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" },
                { ".msg", "application/vnd.ms-outlook" },
                { ".eml", "message/rfc822" }
            };

        public int MaxContentLength { get; set; } = 10 * 1024 * 1024;

        public BaseDatabaseImage DatabaseImage { get; set; }
        public string UploadedFileName { get; set; }

        public string Error { get; set; } = "";

        public bool CreateDatabaseImage(HttpFileCollectionBase httpfilecollectionbase, List<string> acceptedfileextensions, bool resizeImage = false)
        {
            Error = "";

            if (httpfilecollectionbase.Count == 0)
            {
                Error = "Selecteer eerst een bestand.";
            }
            else
            {
                DatabaseImage = new BaseDatabaseImage();

                var httppostedfilebase = httpfilecollectionbase[0];
                var extension = Path.GetExtension(httppostedfilebase?.FileName);
                if (acceptedfileextensions.FindAll(s => s.IndexOf(extension, StringComparison.InvariantCultureIgnoreCase) >= 0).Any())
                {
                    if (httppostedfilebase != null && (httppostedfilebase.ContentLength > 0 && httppostedfilebase.ContentLength <= MaxContentLength))
                    {
                        try
                        {
                            if (resizeImage)
                            {
                                ResizeImage(httppostedfilebase);
                            }
                            else
                            {
                                byte[] bytes = new byte[httppostedfilebase.ContentLength];
                                httppostedfilebase.InputStream.Read(bytes, 0, httppostedfilebase.ContentLength);
                                DatabaseImage.FileData = bytes;
                                DatabaseImage.ContentLength = httppostedfilebase.ContentLength;
                                DatabaseImage.ContentType = httppostedfilebase.ContentType;
                                DatabaseImage.FileName = httppostedfilebase.FileName;
                            } 

                            
                            
                        }
                        catch (Exception ex)
                        {
                            Error = $"Upload mislukt: {ex.Message}";
                        }
                    }
                    else
                    {
                        Error = $"Het gekozen bestand is te groot, maximaal {MaxContentLength/1024/1024} MB.";
                    }
                }
                else
                {
                    Error = "Het gekozen bestandstype wordt niet ondersteund.";
                }
            }

            return string.IsNullOrEmpty(Error);
        }

        /// <summary>
        /// This function is used only once (per database) to move current logo's from image/logo folder to database.
        /// </summary>
        public bool CopyImageToDatabase(PointController pointserver, string fileName, List<string> acceptedfileextensions)
        {
           
            Error = "";

            if (string.IsNullOrEmpty(fileName))
            {
                Error = "Bestandsnaam is leeg";
            }
            else
            {
                DatabaseImage = new BaseDatabaseImage();
                
                var extension = Path.GetExtension(fileName);
                if (acceptedfileextensions.FindAll(s => s.IndexOf(extension, StringComparison.InvariantCultureIgnoreCase) >= 0).Any())
                {
                    var path = pointserver.Server.MapPath(Path.Combine("~/Images/Logo", fileName));
                    if (File.Exists(path))
                    {
                        byte[] fileContent = File.ReadAllBytes(path);

                        if (fileContent != null && (fileContent.Length > 0 && fileContent.Length <= MaxContentLength))
                        {
                            try
                            {

                                DatabaseImage.FileData = fileContent;
                                DatabaseImage.ContentLength = fileContent.Length;
                                DatabaseImage.ContentType = ImageMimeDictionary[extension.ToLower()];
                                DatabaseImage.FileName = fileName;
                            }
                            catch (Exception ex)
                            {
                                Error = $"Upload mislukt: {ex.Message}";
                            }
                        }
                        else
                        {
                            Error = $"Het gekozen bestand is te groot, maximaal {(MaxContentLength/1024/1024)} MB.";
                        }
                    }
                    else
                    {
                        Error = "Bestand bestaat niet op de server.";
                    }
                }
                else
                {
                    Error = "Het gekozen bestandstype wordt niet ondersteund.";
                }
            }

            return string.IsNullOrEmpty(Error);
        }

        private void ResizeImage(HttpPostedFileBase postedFile )
        {
            var imgOriginal = Image.FromStream(postedFile.InputStream, true, true);
            Image imgActual = Scale(imgOriginal);
            
            using (var ms = new MemoryStream())
            {
                var ff = new System.Drawing.Imaging.ImageFormat(imgOriginal.RawFormat.Guid);                   
                imgActual.Save(ms,ff);
                DatabaseImage.ContentLength = (int)ms.Length;
                DatabaseImage.FileData = ms.ToArray();                
                DatabaseImage.FileName = postedFile.FileName; 
                DatabaseImage.ContentType = postedFile.ContentType; 
            }
            imgOriginal.Dispose();
            imgActual.Dispose();
        }
        private Image Scale(Image imgPhoto)
        {
            int destWidth = imgPhoto.Width;
            int destHeight = imgPhoto.Height;
            float Wbase = 600;
            float Hbase = 600;
            float ratio = 1;

            if (destWidth > destHeight)
            {
                ratio = destWidth / Wbase;
                Hbase = destHeight / ratio;
            }
            else if (destHeight > destWidth)
            {
                ratio = destHeight / Hbase;
                Wbase = destWidth / ratio; 
            }
            
            Bitmap bmPhoto = new Bitmap((int)Wbase, (int)Hbase);
            bmPhoto.SetResolution(imgPhoto.HorizontalResolution, imgPhoto.VerticalResolution);

            Graphics grPhoto = Graphics.FromImage(bmPhoto);
            grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;

            grPhoto.DrawImage(imgPhoto, 0, 0, (int)Wbase, (int)Hbase);
            grPhoto.Dispose();

            return bmPhoto;
        }
    }
}