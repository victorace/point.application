﻿using System;
using System.Linq;
using Point.Business.Logic;
using Point.Database.CacheModels;
using Point.Database.Extensions;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Extensions;
using Point.Models.Enums;
using Point.MVC.WebApplication.Controllers;
using Point.MVC.WebApplication.Extensions;

namespace Point.MVC.WebApplication.Helpers
{
    public static class PhaseMainViewModelHelper
    {
        private static string parentAction = "";
        private static string parentController = "";

        public static PhaseButtonsViewModel GetPhaseButtonsViewModel(PointController controller, GlobalProperties globalProps)
        {
            var viewModel = new PhaseButtonsViewModel();
            viewModel.SetGlobalProperties(globalProps);

            var pointUserInfo = globalProps.PointUserInfo;

            if (controller.ControllerContext.ParentActionViewContext != null)
            {
                parentAction = controller.ControllerContext.ParentActionViewContext.RouteData.Values["action"].ToString();
                parentController = controller.ControllerContext.ParentActionViewContext.RouteData.Values["controller"].ToString();

                viewModel.IsReadMode = controller.ControllerContext.ParentActionViewContext.ViewData.IsReadMode();
                viewModel.IsPageLocked = controller.ControllerContext.ParentActionViewContext.ViewData.IsPageLock();
                viewModel.IsFlowClosed = controller.ControllerContext.ParentActionViewContext.ViewData.IsClosed();
            }

            if (viewModel.Global.GeneralActionID > 0)
            {
                var generalAction = GeneralActionBL.GetByID(controller.uow, viewModel.Global.GeneralActionID);
                if(generalAction.HidePhaseButtons)
                {
                    viewModel.ErrorMessage = "<!-- PhaseButtons: none -->";
                    return viewModel;
                }
            }

            viewModel.CurrentPhaseInstanceID = -1;
            viewModel.DisableDoubleClickProtection = parentController.Equals("Client", StringComparison.InvariantCultureIgnoreCase);

            PhaseDefinitionCache cachedPhaseDefinition = PhaseDefinitionCacheBL.GetByID(controller.uow, globalProps.PhaseDefinitionID);

            if (globalProps.TransferID == -1)
            {
                if (globalProps.PhaseDefinitionID <= 0 && !parentAction.Equals("NewFlowInstance", StringComparison.InvariantCultureIgnoreCase))
                {
                    viewModel.ErrorMessage = "<!-- PhaseButtons: No Transfer nor phaseDefinitionID specified -->";
                    return viewModel;
                }

                var phaseDefinitionNewFlow = PhaseDefinitionBL.GetByID(controller.uow, globalProps.PhaseDefinitionID);
                bool isNewFlowInstance = parentController.Equals("FlowMain", StringComparison.InvariantCultureIgnoreCase) && parentAction.Equals("NewFlowInstance", StringComparison.InvariantCultureIgnoreCase);
                var rights = PhaseDefinitionRightsBL.GetFormRightsPhaseDefinitionID(controller.uow, pointUserInfo, cachedPhaseDefinition, null);
                viewModel.Rights = rights;
                viewModel.IsCurrentPhaseActive = true;
                viewModel.IsNewFlowInstance = isNewFlowInstance;
                viewModel.IsDossierForm = (cachedPhaseDefinition != null && cachedPhaseDefinition.Phase == -1);
                viewModel.IsInterrupted = false;

                SetNavigation(viewModel, phaseDefinitionNewFlow);

                if ((viewModel.IsReadMode || rights <= Rights.R) && isNewFlowInstance == false)
                {
                    viewModel.ErrorMessage = "<!-- PhaseButtons: No rights -->";
                    return viewModel;
                }

                return viewModel;
            }
            if (globalProps.PhaseDefinitionID <= 0 && globalProps.PhaseInstanceID <= 0)
            {
                viewModel.ErrorMessage = "<!-- PhaseButtons: no phasedefinitionid or phaseinstanceid -->";
                return viewModel;
            }

            var phaseDefinition = PhaseDefinitionBL.GetByID(controller.uow, globalProps.PhaseDefinitionID);
            if (phaseDefinition == null || !phaseDefinition.PhaseDefinitionNavigation.Any(it => it.Visible))
            {
                viewModel.ErrorMessage = "<!-- PhaseButtons: PhaseDefinitionNavigation: 0 -->";
                return viewModel;
            }

            var flowInstance = FlowInstanceBL.GetByTransferID(controller.uow, globalProps.TransferID);

            PhaseInstance loadedPhaseInstance;
            if (globalProps.PhaseInstanceID > 0)
            {
                loadedPhaseInstance = PhaseInstanceBL.GetById(controller.uow, globalProps.PhaseInstanceID);
            }
            else
            {
                loadedPhaseInstance = PhaseInstanceBL.GetByTransferIDAndControllerAndAction(controller.uow, globalProps.TransferID, parentController, parentAction);
            }

            viewModel.IsCurrentPhaseActive = loadedPhaseInstance?.Status == (int)Status.Active;
            viewModel.IsCurrentPhaseDone = loadedPhaseInstance?.Status == (int)Status.Done;
            viewModel.CurrentPhaseInstanceID = loadedPhaseInstance?.PhaseInstanceID ?? -1;

            if (flowInstance == null || flowInstance == default(FlowInstance))
            {
                viewModel.ErrorMessage = "<!-- PhaseButtons: geen flowinstance -->";
                return viewModel;
            }

            Rights phaseRights = PhaseDefinitionRightsBL.GetFormRightsPhaseDefinitionID(controller.uow, pointUserInfo, cachedPhaseDefinition, flowInstance);

            if (globalProps.FormSetVersionID > 0)
            {
                var formSetVersion = loadedPhaseInstance?.FormSetVersion.FirstOrDefault();
                if (formSetVersion != null)
                {
                    viewModel.LastSaved = (formSetVersion.UpdateDate ?? formSetVersion.CreateDate).ToPointDateHourDisplay();
                    viewModel.LastSavedBy = formSetVersion.UpdatedBy ?? formSetVersion.CreatedBy;
                }
            }

            Status? currentStatus = null;
            if (loadedPhaseInstance != null)
            {
                currentStatus = (Status?)loadedPhaseInstance.Status;
            }

            if ((viewModel.IsReadMode || phaseRights <= Rights.R) && globalProps.FormSetVersionID <= 0)
            {
                viewModel.ErrorMessage = "<!-- PhaseButtons: No rights and nothing to show -->";
                return viewModel;
            }

            var generalActions = GeneralActionBL.GetByPhaseDefinitionAndTypeName(controller.uow, globalProps.PhaseDefinitionID, GeneralActionTypeName.Phase, currentStatus);
            viewModel.ExtraActions = generalActions.Where(ga =>
                            (ga.OnFormSetVersionOnly == false || globalProps.FormSetVersionID > 0) &&
                            HasRights(ga, phaseRights) &&
                            loadedPhaseInstance != null && HasStatus(controller.uow, ga, loadedPhaseInstance.Status)
                        ).ToList();

            viewModel.Rights = phaseRights;
            viewModel.IsDossierForm = phaseDefinition.Phase == -1;

            SetNavigation(viewModel, phaseDefinition);

            if (pointUserInfo.Organization.UseDigitalSignature && phaseDefinition.FormType.HasDigitalSignature)
            {
                viewModel.HasDigitalSignature = pointUserInfo.Employee.DigitalSignatureID.HasValue;
                if (loadedPhaseInstance != null)
                {
                    var signedPhaseInstance = SignedPhaseInstanceBL.GetByPhaseInstanceID(controller.uow, loadedPhaseInstance.PhaseInstanceID);

                    if (signedPhaseInstance != null)
                    {
                        viewModel.HasSignedPhaseInstance = true;
                        viewModel.LastSaved = signedPhaseInstance.Timestamp.ToPointDateHourDisplay();
                        viewModel.LastSavedBy = signedPhaseInstance.SignedBy.FullName();
                    }
                }
            }
            
            viewModel.ShowPrintButton = phaseRights >= Rights.R && phaseDefinition.HasPrintButton.GetValueOrDefault(false);
            viewModel.IsInterrupted = flowInstance.Interrupted;
            viewModel.HasFlowInstance = true;

            if (!viewModel.IsReadMode && (phaseRights > Rights.R || globalProps.FormSetVersionID <= 0))
            {
                return viewModel;
            }

            if (viewModel.ShowPrintButton == false && !viewModel.ExtraActions.Any())
            {
                viewModel.ErrorMessage = "<!-- PhaseButtons: No actions and no printbutton and readonly -->";
                return viewModel;
            }

            return viewModel;
        }

        private static void SetNavigation(PhaseButtonsViewModel viewModel, PhaseDefinition phaseDefinition)
        {
            if (phaseDefinition == null)
            {
                return;
            }

            viewModel.SavePhasebuttons = phaseDefinition.PhaseDefinitionNavigation.Where(it => it.ActionTypeID == ActionTypeID.Save && it.Visible).ToList();
            viewModel.NextPhaseButtons = phaseDefinition.PhaseDefinitionNavigation.Where(it => it.ActionTypeID == ActionTypeID.Next && it.Visible).ToList();
            viewModel.DefinitivePhaseButtons = phaseDefinition.PhaseDefinitionNavigation.Where(it => it.ActionTypeID == ActionTypeID.Definitive && it.Visible).ToList();

            var canWrite = viewModel.Rights >= Rights.X && !viewModel.IsFlowClosed && !viewModel.IsReadOnly;

            viewModel.ShowSavePhaseButtons = viewModel.SavePhasebuttons.Any() && canWrite;
            viewModel.ShowNextPhaseButtons = viewModel.NextPhaseButtons.Any() && viewModel.IsCurrentPhaseActive && canWrite;
            viewModel.ShowDefinitivePhaseButtons = viewModel.DefinitivePhaseButtons.Any() && !viewModel.IsCurrentPhaseDone && canWrite;
        }

        private static bool HasRights(GeneralAction generalAction, Rights phaseRights)
        {
            Rights generalAcionRights;
            Enum.TryParse(generalAction.Rights, out generalAcionRights);

            return generalAcionRights <= phaseRights;
        }

        private static bool HasStatus(IUnitOfWork uow, GeneralAction generalAction, int currentStatus)
        {
            foreach (var generalActionPhase in GeneralActionPhaseBL.GetByGeneralActionID(uow, generalAction.GeneralActionID))
            {
                if ((generalActionPhase.PhaseStatusBegin ?? (int)Status.NotActive) <= currentStatus && (generalActionPhase.PhaseStatusEnd ?? (int)Status.Done) >= currentStatus)
                {
                    return true;
                }
            }
            return false;
        }

    }
}