﻿using Point.Business.Helper;
using Point.Business.Logic;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Extensions;
using Point.Models.Enums;
using Point.MVC.WebApplication.Controllers;
using Point.MVC.WebApplication.Extensions;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace Point.MVC.WebApplication.Helpers
{
    public static class ClientViewModelHelper
    {
        public static ClientHeaderViewModel GetHeaderViewModel(ClientController controller, GlobalProperties globalProps)
        {
            var pointUserInfo = globalProps.PointUserInfo;

            var viewModel = ClientHeaderViewBL.GetViewModel(controller.uow, globalProps.TransferID, pointUserInfo.Organization?.OrganizationID)
                            ?? new ClientHeaderViewModel { IsInstanced = false };

            viewModel.SetGlobalProperties(globalProps);
            viewModel.DesktopForced = controller.ForcedDesktop();

            if (HttpContext.Current.User != null)
            {
                viewModel.UserName = HttpContext.Current.User.Identity.Name;
                viewModel.ExternalPatientIsAuth = ExternalPatientHelper.GetIsAuth();
            }

            viewModel.ExternalPatientReference = ExternalPatientHelper.GetPatExternalReference();
            viewModel.ExternalPatientReferenceBSN = ExternalPatientHelper.GetPatExternalReferenceBSN();

            if (globalProps.FormSetVersionID > 0)
            {
                viewModel.FormSetVersion = FormSetVersionBL.GetByID(controller.uow, globalProps.FormSetVersionID);
            }

            if (viewModel.IsInstanced && viewModel.IsCopy)
            {
                viewModel.OriginalTransferUrl = controller.Url.Action("Dashboard", "FlowMain", new { TransferID = viewModel.OriginalTransferID });
            }
            else if (viewModel.HasRelatedTransfer)
            {
                viewModel.RelatedTransferUrl = controller.Url.Action("Dashboard", "FlowMain", new { TransferID = viewModel.RelatedTransferID });
            }

            if (viewModel.DesktopForced)
            {
                const string parameterName = "forcedesktop";
                var mobileUrl = controller.Request.RawUrl.ToLower();
                if (controller.Request.Params.ContainsKey(parameterName))
                {
                    mobileUrl = mobileUrl.Replace(parameterName + "=true", parameterName + "=false");
                }
                else
                {
                    var sep = mobileUrl.Contains("?") ? "&" : "?";
                    mobileUrl += $"{sep}{parameterName}=false";
                }

                viewModel.MobileUrl = mobileUrl;
            }

            var skipSignaleringCheck = false;
            bool isMFFAScreen = controller.ControllerContext.ParentActionViewContext.ViewData.IsMFAScreen();
            viewModel.IsMFAScreen = isMFFAScreen;
            if (!isMFFAScreen)
            {
                viewModel.ServiceMenuItems = MenuHelper.BuildServiceMenu();
            }
            if (controller.ControllerContext?.ParentActionViewContext?.RouteData != null)
            {
                skipSignaleringCheck = controller.ControllerContext.ParentActionViewContext.RouteData.Values["controller"].ToString().ToLower() == "error" || isMFFAScreen;
            }

            if (skipSignaleringCheck)
            {
                return viewModel;
            }

            IEnumerable<Signalering> signals = null;

            if (viewModel.FlowInstance == null)
            {
                var deparmentIdFilter = new List<int>();
                var departmentidfocus = LoginBL.GetDepartmentIDReference();
                if (departmentidfocus != null)
                {
                    deparmentIdFilter.Add(departmentidfocus.Value);
                }
                var searchStatus = SearchControlBL.GetState(controller.HttpContext);
                if (searchStatus?.DepartmentIDs != null && searchStatus.DepartmentIDs.Any())
                {
                    deparmentIdFilter.AddRange(searchStatus.DepartmentIDs);
                }

                signals = SignaleringBL.GetCurrentByPointUserInfo(controller.uow, pointUserInfo, SignaleringStatus.Open, deparmentIdFilter);
            }
            else
            {
                int? flowInstanceID = viewModel.FlowInstance.FlowInstanceID;
                if (flowInstanceID > 0)
                {
                    signals = SignaleringBL.GetCurrentByFlowinstanceIDAndPointUserInfo(controller.uow, flowInstanceID.Value, pointUserInfo);
                    viewModel.FlowInstanceID = flowInstanceID;
                }
            }

            if (signals != null)
            {
                viewModel.OpenSignalItemsCount = signals.Where(it => it.Status == SignaleringStatus.Open).ToList().Count;

                if (!isMFFAScreen)
                {
                    viewModel.SignaleringStatusViewModel = new SignaleringStatusViewModel
                    {
                        FlowInstanceID = viewModel.FlowInstanceID,
                        SignaleringCount = viewModel.OpenSignalItemsCount
                    };
                }
            }

            return viewModel;
        }


        public static ClientSimpleViewModel GetEditSimpleViewModel(IUnitOfWork uow, int id, int? departmentID, int? phaseDefinitionID, GlobalProperties globalProps) // id = ClientID
        {
            var pointUserInfo = globalProps.PointUserInfo;
            if (departmentID == null)
            {
                departmentID = pointUserInfo.Department.DepartmentID;
            }

            var department = DepartmentBL.GetByDepartmentID(uow, (int)departmentID);
            var organization = department?.Location?.Organization;

            var phaseDefinition = PhaseDefinitionCacheBL.GetByID(uow, phaseDefinitionID.GetValueOrDefault(0));
            FlowDefinitionID? flowDefinitionID = phaseDefinition?.FlowDefinitionID ?? globalProps.FlowInstance?.FlowDefinitionID;

            var client = GetClient(uow, id, globalProps.TransferID);
            var flowWebField = FlowWebFieldBL.FromNameAndUserinfo(uow, FlowFieldConstants.Name_CivilServiceNumber, pointUserInfo);

            var viewModel = ClientSimpleViewBL.FromModel(uow, client, pointUserInfo, flowDefinitionID);

            viewModel.SetGlobalProperties(globalProps);
            viewModel.DepartmentID = departmentID;
            viewModel.ShowNedapRetrieve = OrganizationHelper.IsValidNedapOrganization(organization);
            viewModel.BSNRequired = flowWebField != null && flowWebField.Required && !client.HasNoCivilServiceNumber;

            if (!flowDefinitionID.HasValue || viewModel.ClientID != 0 || flowDefinitionID != FlowDefinitionID.HA_VVT)
            {
                //AutoFill huisarts adressGP alleen voor HA_VVT
                return viewModel;
            }

            viewModel.AddressGPForZorgmail = department?.AddressGPForZorgmail;
            viewModel.GeneralPractitionerName = department?.FullName();
            viewModel.GeneralPractitionerPhoneNumber = department?.PhoneNumber;

            return viewModel;
        }

        public static ClientFullViewModel GetViewVOViewModel(ClientController controller, int clientid, GlobalProperties globalProps)
        {
            var client = GetClient(controller.uow, clientid, globalProps.TransferID);
            var pointUserInfo = globalProps.PointUserInfo;
            var viewModel = ClientFullViewBL.FromModel(controller.uow, client, pointUserInfo, null);
            viewModel.SetGlobalProperties(globalProps);
            viewModel.ClientLookups = GetClientLookups(controller.uow);

            if (client?.HealthInsuranceCompanyID == null)
            {
                return viewModel;
            }

            var healthInsurer = HealthInsurerBL.GetByID(controller.uow, client.HealthInsuranceCompanyID.Value);
            if (healthInsurer == null)
            {
                return viewModel;
            }

            viewModel.HealthInsurerName = healthInsurer.Name;

            if (healthInsurer.UZOVI != null)
            {
                viewModel.HealthInsurerUZOVI = healthInsurer.UZOVI?.ToString();
            }

            return viewModel;
        }

        public static ClientFullViewModel GetViewAddressViewModel(PointFormController controller, int clientid, GlobalProperties globalProps)
        {
            var client = GetClient(controller.uow, clientid, globalProps.TransferID);
            var viewModel = ClientFullViewBL.FromModel(controller.uow, client, globalProps.PointUserInfo, null);
            viewModel.SetGlobalProperties(globalProps);
            return viewModel;
        }

        public static ClientFullViewModel GetViewBasicViewModel(ClientController controller, int clientid, GlobalProperties globalProps)
        {
            const string uzoviUnknown = "106";
            var client = GetClient(controller.uow, clientid, globalProps.TransferID);
            var viewModel = ClientFullViewBL.FromModel(controller.uow, client, globalProps.PointUserInfo, null);

            viewModel.SetGlobalProperties(globalProps);
            viewModel.ClientLookups = GetClientLookups(controller.uow);

            viewModel.HealthInsurerUZOVI = "";
            if (!client.HealthInsuranceCompanyID.HasValue)
            {
                return viewModel;
            }

            var healthInsurer = HealthInsurerBL.GetByID(controller.uow, client.HealthInsuranceCompanyID.Value);
            viewModel.HealthInsurerUZOVI = healthInsurer == null ? uzoviUnknown : (healthInsurer.UZOVI?.ToString() ?? "");

            if (healthInsurer == null)
            {
                return viewModel;
            }

            viewModel.HealthInsurerName = healthInsurer.Name;

            return viewModel;
        }

        public static ClientViewModel GetEditViewModel(ClientController controller, int id, int? phaseDefinitionID, string activeTab, int? departmentID, GlobalProperties globalProps) // id = ClientID
        {
            var pointUserInfo = globalProps.PointUserInfo;

            departmentID = departmentID ?? pointUserInfo.Department?.DepartmentID;
            var department = DepartmentBL.GetByDepartmentID(controller.uow, departmentID.Value);

            var client = GetClient(controller.uow, id, globalProps.TransferID);

            var phaseDefinition = PhaseDefinitionCacheBL.GetByID(controller.uow, phaseDefinitionID.GetValueOrDefault(0));
            FlowDefinitionID? flowDefinitionID = phaseDefinition?.FlowDefinitionID ?? globalProps.FlowInstance?.FlowDefinitionID;

            var viewModel = ClientViewBL.FromModel(controller.uow, client, pointUserInfo, flowDefinitionID);
            viewModel.SetGlobalProperties(globalProps);

            InitializeExternalPatientData(controller, viewModel, client, pointUserInfo);

            var flowWebField = FlowWebFieldBL.FromNameAndUserinfo(controller.uow, FlowFieldConstants.Name_PharmacyName, pointUserInfo);
            viewModel.PharmacyRequired = (flowWebField != null && flowWebField.Required);

            flowWebField = FlowWebFieldBL.FromNameAndUserinfo(controller.uow, FlowFieldConstants.Name_CivilServiceNumber, pointUserInfo);
            viewModel.BSNRequired = (flowWebField != null && flowWebField.Required);

            if (flowDefinitionID == FlowDefinitionID.HA_VVT)
            {
                if (viewModel.ClientID == 0)
                {
                    //AutoFill huisarts adressGP alleen voor HA_VVT
                    viewModel.AddressGPForZorgmail = department?.AddressGPForZorgmail;
                    viewModel.GeneralPractitionerName = department?.FullName();
                    viewModel.GeneralPractitionerPhoneNumber = department?.PhoneNumber;
                }
            }

            viewModel.ForceContactPersonCreation = (client.ClientID == 0 || !ContactPersonBL.GetByClientID(controller.uow, client.ClientID).Any());
            viewModel.ClientLookups = GetClientLookups(controller.uow);
            viewModel.HavePatExternalReference = ExternalPatientHelper.HavePatExternalReference();
            viewModel.ActiveTabName = activeTab;
            viewModel.DepartmentID = departmentID;

            return viewModel;
        }

        public static ClientFullViewModel GetEditFullViewModel(ClientController controller, int id, int? phaseDefinitionID, string activeTab, int? departmentID, GlobalProperties globalProps) // id = ClientID
        {
            var pointUserInfo = globalProps.PointUserInfo;

            departmentID = departmentID ?? pointUserInfo.Department?.DepartmentID;
            var department = DepartmentBL.GetByDepartmentID(controller.uow, departmentID.Value);

            var client = GetClient(controller.uow, id, globalProps.TransferID);
            var organization = department?.Location?.Organization;

            var phaseDefinition = PhaseDefinitionCacheBL.GetByID(controller.uow, phaseDefinitionID.GetValueOrDefault(0));
            FlowDefinitionID? flowDefinitionID = phaseDefinition?.FlowDefinitionID ?? globalProps.FlowInstance?.FlowDefinitionID;

            PrefillPatientData(controller.uow, pointUserInfo, client);

            var viewModel = ClientFullViewBL.FromModel(controller.uow, client, pointUserInfo, flowDefinitionID);
            viewModel.SetGlobalProperties(globalProps);

            InitializeExternalPatientData(controller, viewModel, client, pointUserInfo);

            var flowWebField = FlowWebFieldBL.FromNameAndUserinfo(controller.uow, FlowFieldConstants.Name_PharmacyName, pointUserInfo);
            viewModel.PharmacyRequired = flowDefinitionID != FlowDefinitionID.VVT_VVT && flowWebField != null && flowWebField.Required;

            flowWebField = FlowWebFieldBL.FromNameAndUserinfo(controller.uow, FlowFieldConstants.Name_CivilServiceNumber, pointUserInfo);
            viewModel.BSNRequired = (flowWebField != null && flowWebField.Required);

            viewModel.ShowNedapRetrieve = OrganizationHelper.IsValidNedapOrganization(organization);

            if (flowDefinitionID == FlowDefinitionID.HA_VVT)
            {
                if (viewModel.ClientID == 0 && organization.OrganizationTypeID == (int)OrganizationTypeID.GeneralPracticioner)
                {
                    viewModel.AddressGPForZorgmail = department?.AddressGPForZorgmail;
                    viewModel.GeneralPractitionerName = department?.FullName();
                    viewModel.GeneralPractitionerPhoneNumber = department?.PhoneNumber;
                }
                viewModel.HideHospitalFields = true;
            }

            viewModel.ShowContactPersonUnknown = (viewModel.ClientID == 0);
            viewModel.ForceContactPersonCreation = (client.ClientID == 0 || !ContactPersonBL.GetByClientID(controller.uow, client.ClientID).Any());
            viewModel.ClientLookups = GetClientLookups(controller.uow);
            viewModel.HavePatExternalReference = ExternalPatientHelper.HavePatExternalReference();
            viewModel.ActiveTabName = activeTab;
            viewModel.DepartmentID = departmentID;

            return viewModel;
        }

        public static void ClientValidation(ClientController controller, Client client, int transferID)
        {
            if (client?.ClientID > 0 && transferID > 0)
            {
                var transferClient = TransferBL.GetByTransferID(controller.uow, transferID);
                if (transferClient?.ClientID != client.ClientID)
                {
                    controller.ModelState.AddModelError("", "U heeft geen toegang tot dit dossier");
                }
            }
            //if BSN is filled but 'heeft nog geen bsn' checkbox is still checked, uncheck it!
            if (!string.IsNullOrEmpty(client?.CivilServiceNumber))
            {
                client.HasNoCivilServiceNumber = false;
            }
        }


        private static Client GetClient(IUnitOfWork uow, int clientID, int transferID)
        {
            var client = new Client();

            if (clientID > 0)
            {
                client = ClientBL.GetByClientID(uow, clientID);
            }
            else if (transferID > 0)
            {
                client = ClientBL.GetByTransferID(uow, transferID);
            }

            return client;
        }

        private static void InitializeExternalPatientData(ClientController controller, ClientViewModelBase viewModel, Client client, PointUserInfo pointUserInfo)
        {
            // TO DO: HL7Interface is not used anymore! HL7 settings are moved to OrganizationSettings. 
            // So HL7Interface and InterfaceNumber may be removed from Organization table (and also client queries)
            // Ther will be another PBI for that.
            bool hl7Interface = pointUserInfo.Organization.HL7Interface;
            bool wsInterface = pointUserInfo.Organization.WSInterface;
            string hl7ServiceInterfaceCode = OrganizationSettingBL.GetValue<string>(controller.uow, pointUserInfo.Organization.OrganizationID, OrganizationSettingBL.InterfaceCode);

            var patExternalReference = ExternalPatientHelper.GetPatExternalReference();

            if (!string.IsNullOrEmpty(hl7ServiceInterfaceCode))
            {
                viewModel.PatientReceiveURL = controller.Url.Action("GetClientHL7Service", "Client");
            }
            else if (hl7Interface)
            {
                // HL7/Client processing using ClientQRY & ClientReceivedTemp
                viewModel.PatientRequestURL = controller.Url.Action("CreateClientQRY", "Client");
                viewModel.PatientReceiveURL = controller.Url.Action("GetClientReceivedTempHL7", "Client");
            }
            else if (wsInterface)
            {
                if (!string.IsNullOrEmpty(patExternalReference) && client.ClientID <= 0)
                {
                    // Handle via private static void PrefillPatientData(IUnitOfWork uow, PointUserInfo pointUserInfo, Client client)
                }
                else
                {
                    // HL7/Client processing using PatientRequestTemp & PatientReceivedTemp. Not really used (yet)
                    viewModel.PatientRequestURL = controller.Url.Action("CreatePatientRequestTemp", "Client");
                    viewModel.PatientReceiveURL = controller.Url.Action("GetPatientReceivedTemp", "Client");
                }
            }

            viewModel.PatExternalReference = patExternalReference;
        }

        private static void PrefillPatientData(IUnitOfWork uow, PointUserInfo pointUserInfo, Client client)
        {
            // Only handle WSInterface (Spaarne, Haga, VUmc). All other combiniations (HL7Interface and InterfaceCode)
            // are handled via InitializeExternalPatientData

            var patexternalreference = ExternalPatientHelper.GetPatExternalReference();
            bool wsinterface = pointUserInfo.Organization.WSInterface;
            if (wsinterface)
            {
                if (!string.IsNullOrEmpty(patexternalreference) && client.ClientID <= 0)
                {
                    var clientProperties = client.GetType().GetProperties();

                    ClientReceivedTemp clientReceived = null;
                    PropertyInfo[] receivedProperties = { };

                    var patientReceived = PatientReceivedTempBL.Get(uow, pointUserInfo.Organization.OrganizationID, patexternalreference);
                    if (patientReceived != null)
                    {
                        receivedProperties = patientReceived.GetType().GetProperties();
                    }
                    else
                    {
                        clientReceived = ClientReceivedTempBL.Get(uow, pointUserInfo.Organization.OrganizationID, patexternalreference);
                        if (clientReceived != null)
                        {
                            receivedProperties = clientReceived.GetType().GetProperties();
                        }
                    }

                    foreach (var property in clientProperties)
                    {
                        var fieldName = property.Name;
                        if (fieldName == WellKnownFieldNames.HealthInsuranceCompanyID)
                        {
                            fieldName = WellKnownFieldNames.HealthInsuranceCompanyUZOVICode;
                        }

                        var receivedProperty = receivedProperties.FirstOrDefault(x => x.Name == fieldName);
                        if (receivedProperty == null)
                        {
                            continue;
                        }

                        object value = null;
                        if (patientReceived != null)
                        {
                            value = receivedProperty.GetValue(patientReceived);
                        }
                        else if (clientReceived != null)
                        {
                            value = receivedProperty.GetValue(clientReceived);
                        }

                        if (fieldName == WellKnownFieldNames.HealthInsuranceCompanyUZOVICode)
                        {
                            var healthInsurer = HealthInsurerBL.GetByUzovi(uow, value?.ToString());
                            if (healthInsurer != null)
                            {
                                property.SetValue(client, healthInsurer.HealthInsurerID);
                            }
                        }
                        else
                        {
                            var mappedValue = FieldReceivedValueMappingBL.MapValue(uow, property.Name, value?.ToString());
                            if (mappedValue != null)
                            {
                                value = mappedValue.TargetValue;
                            }
                            var typedvalue = value.ConvertTo<object>(property.PropertyType);
                            property.SetValue(client, typedvalue);
                        }
                    }
                }
            }
        }

        private static ClientLookupsViewModel GetClientLookups(IUnitOfWork uow)
        {
            using (var lookupbl = new LookUpBL())
            {
                return new ClientLookupsViewModel
                {
                    Nationalities = lookupbl.GetNationalities(),
                    HealthInsurers = lookupbl.GetHealthInsurers(),
                    HealthInsurerIDsNotInsured = lookupbl.GetHealthInsurerIDsNotInsured(),
                    HousingType = lookupbl.GetWoningType(),
                    CizRelations = uow.CIZRelationRepository.GetAll().OrderBy(cr => cr.Description)
                };
            }
        }
    }
}