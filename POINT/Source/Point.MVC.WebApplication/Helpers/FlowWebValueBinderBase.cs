﻿using Newtonsoft.Json;
using Point.Database.Models.ViewModels;
using Point.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace Point.MVC.WebApplication.Helpers
{
    public class FlowWebValueBinderBase : DefaultModelBinder
    {
        private class FlowWebValueSource
        {
            public int FlowFieldID { get; set; }
            public Source Source { get; set; }
        }

        private static Regex _stripIDAndFieldnameRegex = new Regex(FlowWebField.NAME_FORMAT_REGEXP, RegexOptions.Compiled);

        private List<FlowWebValueSource> flowwebvaluesources = new List<FlowWebValueSource>();

        private void initialiseFlowWebValueSources(HttpRequestBase httprequestbase)
        {
            if (httprequestbase == null)
            {
                return;
            }

            var flowwebvaluesource = httprequestbase.Form["flowwebvaluesource"];
            if (!string.IsNullOrEmpty(flowwebvaluesource))
            {
                flowwebvaluesources = JsonConvert.DeserializeObject<List<FlowWebValueSource>>(flowwebvaluesource);
            }
        }

        private Source getFlowWebValueSource(int flowfieldid)
        {
            var source = Source.DEFAULT;
            var flowwebvaluesource = flowwebvaluesources.FirstOrDefault(f => f.FlowFieldID == flowfieldid);
            if (flowwebvaluesource != null)
            {
                source = (Source)flowwebvaluesource.Source;
            }

            return source;
        }

        public List<FlowWebField> GetFlowWebFields(ControllerContext controllerContext)
        {
            HttpRequestBase request = controllerContext.HttpContext.Request;

            initialiseFlowWebValueSources(request);

            List<FlowWebField> flowWebValues = new List<FlowWebField>();
            foreach (string key in request.Form.AllKeys)
            {
                // remove f_ from f_<token>
                if (key != null && key.StartsWith(FlowWebField.NAME_PREFIX))
                {
                    string name = key.Remove(0, FlowWebField.NAME_PREFIX.Length);
                    if (!String.IsNullOrWhiteSpace(name))
                    {
                        var match = _stripIDAndFieldnameRegex.Match(name);
                        if (match.Success)
                        {
                            string value = request.Form.Get(key);

                            int flowfieldid = 0;
                            Int32.TryParse(match.Groups["id"].Value, out flowfieldid);

                            int flowfieldattributeid = 0;
                            Int32.TryParse(match.Groups["attributeid"].Value, out flowfieldattributeid);

                            string fieldName = match.Groups["fieldName"].Value;

                            int type = 0;
                            Int32.TryParse(match.Groups["type"].Value, out type);

                            bool signalonchange = false;
                            Boolean.TryParse(match.Groups["signalonchange"].Value, out signalonchange);

                            if ((FlowFieldType)type == FlowFieldType.Checkbox)
                            {
                                // handle hidden checkbox value contain 'true, false'
                                value = (value.Contains("true") ? "true" : "false");
                            }
                            else if ((FlowFieldType)type == FlowFieldType.CheckboxList)
                            {
                                if (value == "false")
                                    value = "";
                                else
                                    value = value.Replace(",false", "");
                            }
                            else if ((FlowFieldType)type == FlowFieldType.RadioButton)
                            {
                                // we have 2 elements with the same name. we
                                // are only interested in the first one (id)
                                value = value.Split(',').First();
                            }
                            else if ((FlowFieldType)type == FlowFieldType.IDText)
                            {
                                // we have elements with the same name. we
                                // are only interested in the last one (id)
                                value = value.Split(',').Last();
                            }

                            flowWebValues.Add(new FlowWebField()
                            {
                                FlowFieldID = flowfieldid,
                                FlowFieldAttributeID = flowfieldattributeid,
                                Name = fieldName,
                                Type = (FlowFieldType)type,
                                Value = value,
                                SignalOnChange = signalonchange,
                                Source = getFlowWebValueSource(flowfieldid)
                            });
                        }
                    }
                }
            }

            return flowWebValues;
        }
    }
}