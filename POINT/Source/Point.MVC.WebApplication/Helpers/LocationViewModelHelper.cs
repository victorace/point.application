﻿using Point.Business.Logic;
using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using System.Collections.Generic;
using System.Linq;
using Point.Business.Helper;
using Point.Database.Repository;

namespace Point.MVC.WebApplication.Helpers
{
    public static class LocationViewModelHelper
    {
        private static LocationViewModel GetLocationViewModel(IUnitOfWork uow, GlobalProperties globalProps, int? locationid)
        {
            var model = locationid.HasValue ? LocationBL.GetByID(uow, locationid.Value) : null;

            var pointUserInfo = globalProps.PointUserInfo;
            var maxLevel = pointUserInfo.GetFunctionRoleLevelID(FunctionRoleTypeID.ManageOrganization);
            var maxDossierLevel = Authorization.GetPointFlowRole(uow, pointUserInfo.Employee.UserID);

            var regions = RegionBL.GetAll(uow).OrderBy(it => it.Name).ToList();
            var flowDefinitions = FlowDefinitionBL.GetActiveFlowDefinitions(uow).ToList();

            var viewmodel = LocationViewBL.FromModel(model, flowDefinitions, regions, maxLevel, maxDossierLevel);

            List<Organization> organizations;

            if (viewmodel.Assisting.HasGlobalRights)
            {
                organizations = OrganizationBL.GetActive(uow).ToList();
            }
            else if (viewmodel.Assisting.HasRegionRights)
            {
                organizations = OrganizationBL.GetActiveByRegionID(uow, pointUserInfo.Region.RegionID).ToList();
            }
            else
            {
                organizations = new List<Organization> { pointUserInfo.Organization };
            }

            if (model?.Organization != null)
            {
                organizations = organizations.Where(o => o.OrganizationID == model.Organization.OrganizationID).ToList();
            }

            viewmodel.Assisting.OrganizationList = organizations.OrderBy(it => it.Name).ToList();
            viewmodel.Assisting.EnableAutoCreate = maxLevel >= FunctionRoleLevelID.Location;

            return viewmodel;
        }

        public static LocationViewModel GetEditViewModel(IUnitOfWork uow, GlobalProperties globalProps, int? locationId)
        {
            return GetLocationViewModel(uow, globalProps, locationId);
        }

        public static LocationViewModel GetCreateViewModel(IUnitOfWork uow, GlobalProperties globalProps, int? organizationID = null)
        {
            var viewmodel = GetLocationViewModel(uow, globalProps, null);

            if (!organizationID.HasValue)
            {
                return viewmodel;
            }

            var organization = OrganizationBL.GetByOrganizationID(uow, organizationID.Value);
            viewmodel.OrganizationID = organizationID.Value;
            viewmodel.OrganizationTypeID = organization.OrganizationTypeID;
            viewmodel.OrganizationName = organization.Name;
            viewmodel.Assisting.EnableOtherRegions = organization.OrganizationTypeID == (int)OrganizationTypeID.HealthCareProvider;
            viewmodel.OrganizationRegionName = organization.Region?.Name;

            return viewmodel;
        }

        public static AutoCreateSetViewModel GetNewAutoCreateSetViewModel(IUnitOfWork uow, GlobalProperties globalProps, int locationId)
        {
            var location = LocationBL.GetByID(uow, locationId);
            var organizationSets = AutoCreateSetBL.GetSettingsCreateUserByOrganizationID(uow, location.OrganizationID);
            var organizationSetCount = organizationSets.Count();

            var locationSets = location.AutoCreateSet.ToList();
            var locationSetCount = locationSets.Count;

            //Default if its the first of the organization
            var pointUserInfo = globalProps.PointUserInfo;
            var maxDossierLevel = Authorization.GetPointFlowRole(uow, pointUserInfo.Employee.UserID);

            var logEntry = LogEntryBL.Create(FlowFormType.AdminLocation, pointUserInfo);
            var model = AutoCreateSetBL.CreateNewSet(uow, locationId, organizationSetCount == 0 ? "Default Set" : "Set " + (locationSetCount + 1), isdefault: organizationSetCount == 0);

            //Force modified state to set modified timestamp (See LoggingBL.FillLoggable)
            uow.DatabaseContext.Entry(location).State = System.Data.Entity.EntityState.Modified;
            LoggingBL.FillLoggable(uow, location, logEntry);

            uow.Save(logEntry);

            return AutoCreateSetBL.FromModel(model, location, maxDossierLevel);
        }

        public static AutoCreateSetViewModel GetEditAutoCreateSetViewModel(IUnitOfWork uow, GlobalProperties globalProps, int autocreatesetid)
        {
            var maxDossierLevel = Authorization.GetPointFlowRole(uow, globalProps.PointUserInfo.Employee.UserID);

            var model = AutoCreateSetBL.GetByID(uow, autocreatesetid);
            return AutoCreateSetBL.FromModel(model, model.Location, maxDossierLevel);
        }


    }
}