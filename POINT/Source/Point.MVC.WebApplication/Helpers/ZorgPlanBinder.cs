﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using Point.Database.Models.ViewModels;

namespace Point.MVC.WebApplication.Helpers
{
    public class ZorgAdviesBinder : FlowWebValueBinderBase
    {
        private static Regex _stripIDAndFieldnameZorgAdviesItemRegex = new Regex(ZorgAdviesItemViewModel.NAME_FORMAT_REGEXP, RegexOptions.Compiled);

        public List<ZorgAdviesItemViewModel> GetZorgAdviesItems(ControllerContext controllerContext)
        {
            HttpRequestBase request = controllerContext.HttpContext.Request;
            List<ZorgAdviesItemViewModel> zorgadviesitems = new List<ZorgAdviesItemViewModel>();

            foreach (string key in request.Form.AllKeys)
            {
                if (key != null && key.StartsWith(ZorgAdviesItemViewModel.NAME_PREFIX))
                {
                    string protectedName = key.Remove(0, ZorgAdviesItemViewModel.NAME_PREFIX.Length);
                    if (!String.IsNullOrWhiteSpace(protectedName))
                    {
                        var match = _stripIDAndFieldnameZorgAdviesItemRegex.Match(protectedName);
                        if (match.Success)
                        {
                            string value = request.Form.Get(key);

                            int itemid = 0;
                            Int32.TryParse(match.Groups["itemid"].Value, out itemid);
                            int itemvalueid = 0;
                            Int32.TryParse(match.Groups["itemvalueid"].Value, out itemvalueid);
                            bool needsfreq = false;
                            bool.TryParse(match.Groups["needsfreq"].Value, out needsfreq);
                            string fieldName = match.Groups["fieldName"].Value;

                            var zorgadviesitem = zorgadviesitems.FirstOrDefault(it => it.ZorgAdviesItemID == itemid);
                            if (zorgadviesitem == null)
                            {
                                zorgadviesitem = new ZorgAdviesItemViewModel();
                                zorgadviesitem.ZorgAdviesItemID = itemid;
                                zorgadviesitem.ZorgAdviesItemValueID = itemvalueid;
                                zorgadviesitem.NeedsFrequencyAndTime = needsfreq;

                                zorgadviesitems.Add(zorgadviesitem);
                            }

                            switch (fieldName)
                            {
                                case "text":
                                    zorgadviesitem.Text = value;
                                    break;
                                case "by":
                                    if (needsfreq)
                                    {
                                        if (!String.IsNullOrEmpty(value))
                                            zorgadviesitem.ActionBy = Array.ConvertAll(value.Split(','), s => (ZorgAdviesItemViewModel.By)Enum.Parse(typeof(ZorgAdviesItemViewModel.By), s));
                                    }
                                    break;
                                case "frequency":
                                    int freq = 0;
                                    if(int.TryParse(value, out freq))
                                        zorgadviesitem.ActionFrequency = freq;
                                    break;
                                case "timespan":
                                    zorgadviesitem.ActionTimespan = (ZorgAdviesItemViewModel.Timespan?)Enum.Parse(typeof(ZorgAdviesItemViewModel.Timespan), value);
                                    break;
                                case "time":
                                int time = 0;
                                    if (int.TryParse(value, out time))
                                        zorgadviesitem.ActionTime = time;
                                    break;
                                default:
                                    break;
                            }

                            
                        }
                    }
                }
            }

            return zorgadviesitems;
        }

        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            if (bindingContext.ModelType == typeof(ZorgAdviesViewModel))
            {
                HttpRequestBase request = controllerContext.HttpContext.Request;
                var zorgAdviesViewModel = new ZorgAdviesViewModel();
                zorgAdviesViewModel.SetGlobalProperties();
                zorgAdviesViewModel.Global.FlowWebFields = GetFlowWebFields(controllerContext);
                zorgAdviesViewModel.ZorgAdviesItems = GetZorgAdviesItems(controllerContext);

                return zorgAdviesViewModel;
            }

            // call the default model binder this new binding context
            return base.BindModel(controllerContext, bindingContext);
        }
    }
}