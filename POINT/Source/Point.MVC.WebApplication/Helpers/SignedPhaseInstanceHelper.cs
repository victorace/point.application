﻿using Point.Business.Logic;
using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using signedPhaseInstanceBL = Point.Business.Logic.SignedPhaseInstanceBL;

namespace Point.MVC.WebApplication.Helpers
{
    public static class SignedPhaseInstanceHelper
    {
        public static SignedPhaseInstanceViewModel GetSignedPhaseInstanceViewModel(IUnitOfWork uow, int formSetVersionID, int phaseInstanceID)
        {
            SignedPhaseInstance signedphaseinstance = null;

            if (formSetVersionID > 0)
            {
                var phaseinstance = PhaseInstanceBL.GetById(uow, phaseInstanceID);
                if (phaseinstance?.Status == (int)Status.Done)
                {
                    signedphaseinstance = signedPhaseInstanceBL.GetByPhaseInstanceID(uow, phaseinstance.PhaseInstanceID);
                }
            }

            return new SignedPhaseInstanceViewModel { ObjectInstance = signedphaseinstance };
        }
    }
}