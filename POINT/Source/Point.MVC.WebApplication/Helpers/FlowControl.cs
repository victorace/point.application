﻿using Point.Business.Helper;
using Point.Database.Extensions;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Infrastructure.Attributes;
using Point.Infrastructure.Extensions;
using Point.Models.Enums;
using Point.MVC.WebApplication.Extensions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Point.MVC.WebApplication.Helpers
{
    public class FlowControlCreator
    {
        public const int MAXLENINPUT = 100;

        private readonly FlowWebField flowWebField;
        private readonly IDictionary<string, object> htmlAttributes;
        private readonly HtmlHelper htmlHelper;
        private readonly string protectedName;
        private readonly bool isReadMode;
        private readonly bool isPrintMode;
        private readonly bool isInputOnly;

        public FlowControlCreator(FlowWebField flowWebField, IDictionary<string, object> htmlAttributes, HtmlHelper htmlHelper, bool inputOnly = false)
        {
            this.flowWebField = flowWebField;
            this.htmlAttributes = htmlAttributes;
            this.htmlHelper = htmlHelper;
            isReadMode = htmlHelper.ViewData.IsReadMode();
            isPrintMode = htmlHelper.ViewContext.RequestContext.HttpContext.Request.IsPrintRequest();
            protectedName = GenerateProtectedName(flowWebField);
            isInputOnly = inputOnly;
        }

        public MvcHtmlString Create()
        {

            switch (flowWebField.Type)
            {
                case FlowFieldType.Hidden:
                    return createHidden();

                case FlowFieldType.String:
                case FlowFieldType.Number:
                    return createInput();

                case FlowFieldType.Date:
                case FlowFieldType.DateTime:
                case FlowFieldType.Time:
                    return createDateTime();

                case FlowFieldType.RadioButton:
                    return createRadioButton();

                case FlowFieldType.CheckboxList:
                    return createCheckBoxList();

                case FlowFieldType.Checkbox:
                    return createCheckBox();

                case FlowFieldType.DropDownList:
                    return isPrintMode ? createIDText() : createDropDownList();

                case FlowFieldType.IDText:
                    return createIDText();

                default:
                    return MvcHtmlString.Create(
                        $"<!-- FlowField '{flowWebField.Name}' has the type '{flowWebField.Type}' which is currently unsupported. -->");
            }
        }

        private TagBuilder createPrintControl(string value = null)
        {
            var multiline = (flowWebField.MaxLength.HasValue && flowWebField.MaxLength > MAXLENINPUT);

            var tagBuilder = new TagBuilder("span");
            if (value == null)
            {
                tagBuilder.SetInnerText(string.IsNullOrEmpty(flowWebField.DisplayValue) ? "" : flowWebField.DisplayValue.Replace("\t", " "));
            }
            else
            {
                tagBuilder.SetInnerText(value);
            }

            tagBuilder.AddCssClass("print-control");
            if (multiline)
            {
                tagBuilder.AddCssClass("print-multiline");
            }

            return tagBuilder;
        }

        private MvcHtmlString createHidden()
        {
            var tagBuilder = new TagBuilder("input");

            if(!String.IsNullOrEmpty(flowWebField.Value))
            {
                tagBuilder.Attributes["value"] = flowWebField.Value;
            }
            tagBuilder.Attributes["id"] = TagBuilder.CreateSanitizedId(flowWebField.Name);
            tagBuilder.Attributes["type"] = "hidden";
            tagBuilder.Attributes["name"] = protectedName;

            tagBuilder.Attributes["data-flowfieldid"] = flowWebField.FlowFieldID.ToString();
            tagBuilder.Attributes["data-source"] = $"{(int)flowWebField.Source}";
            tagBuilder.AddCssClass($"sourcefield-{(int)flowWebField.Source}");

            addReadOnlyAttribute();
            addjQueryUnobtrusiveValidationAttribute();
            addSpecialAttributes();

            tagBuilder.MergeAttributes(htmlAttributes, replaceExisting: true);

            return MvcHtmlString.Create(tagBuilder.ToString(TagRenderMode.Normal));
        }

        private MvcHtmlString createInput()
        {
            if (flowWebField.MaxLength.HasValue && flowWebField.MaxLength > MAXLENINPUT)
            {
                return createTextArea();
            }

            var tagBuilder = new TagBuilder("input");

            if (isPrintMode)
            {
                tagBuilder = createPrintControl();
            }
            else
            {
                tagBuilder.Attributes["value"] = string.IsNullOrEmpty(flowWebField.DisplayValue) ? "" : flowWebField.DisplayValue;
                tagBuilder.AddCssClass("flowfield form-control");
            }

            tagBuilder.Attributes["id"] = TagBuilder.CreateSanitizedId(flowWebField.Name);
            tagBuilder.Attributes["type"] = "text";
            tagBuilder.Attributes["name"] = protectedName;

            tagBuilder.Attributes["data-flowfieldid"] = flowWebField.FlowFieldID.ToString();
            tagBuilder.Attributes["data-source"] = $"{(int)flowWebField.Source}";
            tagBuilder.AddCssClass($"sourcefield-{(int)flowWebField.Source}");

            addReadOnlyAttribute();          
            addjQueryUnobtrusiveValidationAttribute();
            addSpecialAttributes();

            tagBuilder.MergeAttributes(htmlAttributes, replaceExisting: true);

            return MvcHtmlString.Create(tagBuilder.ToString(TagRenderMode.Normal));
        }

        private MvcHtmlString createTextArea()
        {
            var tagBuilder = new TagBuilder("textarea");

            if (isPrintMode)
            {
                tagBuilder = createPrintControl();
            }
            else
            {
                tagBuilder.SetInnerText(string.IsNullOrWhiteSpace(flowWebField.DisplayValue) ? "" : flowWebField.DisplayValue);
                tagBuilder.AddCssClass("flowfield form-control");
            }

            tagBuilder.Attributes["id"] = TagBuilder.CreateSanitizedId(flowWebField.Name);
            tagBuilder.Attributes["name"] = protectedName;
            if (flowWebField.MaxLength != null && flowWebField.MaxLength > 0)
            {
                tagBuilder.Attributes["maxlength"] = flowWebField.MaxLength.ConvertTo<string>();
            }

            tagBuilder.Attributes["data-flowfieldid"] = flowWebField.FlowFieldID.ToString();
            tagBuilder.Attributes["data-source"] = $"{(int)flowWebField.Source}";
            tagBuilder.AddCssClass($"sourcefield-{(int)flowWebField.Source}");

            addReadOnlyAttribute();
            addjQueryUnobtrusiveValidationAttribute();
            addSpecialAttributes();

            tagBuilder.MergeAttributes(htmlAttributes, replaceExisting: true);

            return MvcHtmlString.Create(tagBuilder.ToString(TagRenderMode.Normal));
        }

        private MvcHtmlString createDateTime()
        {
            var cssClass = "";
            switch (flowWebField.Type)
            {
                case FlowFieldType.DateTime:
                    cssClass = "datetime";
                    break;

                case FlowFieldType.Date:
                    cssClass = "date";
                    break;

                case FlowFieldType.Time:
                    cssClass = "time";
                    break;
            }

            var tagBuilder = new TagBuilder("input");

            var displayvalue = string.IsNullOrWhiteSpace(flowWebField.DisplayValue) ? "" : tagBuilder.Attributes["value"] = flowWebField.DisplayValue;

            if (isPrintMode)
            {
                tagBuilder = createPrintControl(displayvalue);
            }
            else
            {
                tagBuilder.Attributes["value"] = displayvalue;
                tagBuilder.AddCssClass("flowfield form-control");
            }

            tagBuilder.Attributes["id"] = TagBuilder.CreateSanitizedId(flowWebField.Name);
            tagBuilder.Attributes["type"] = "text";
            tagBuilder.Attributes["name"] = protectedName;

            tagBuilder.Attributes["data-flowfieldid"] = flowWebField.FlowFieldID.ToString();
            tagBuilder.Attributes["data-source"] = $"{(int)flowWebField.Source}";
            tagBuilder.AddCssClass($"sourcefield-{(int)flowWebField.Source}");

            addReadOnlyAttribute();
            addjQueryUnobtrusiveValidationAttribute();
            addDbDrivenValidationAttributes();
            addSpecialAttributes();

            tagBuilder.MergeAttributes(htmlAttributes, replaceExisting: true);

            if (isPrintMode)
            {
                return MvcHtmlString.Create(tagBuilder.ToString(TagRenderMode.Normal));
            }

            if (flowWebField.Type != FlowFieldType.Time && !isReadMode)
            {
                var inputgroup = new TagBuilder("div");
                inputgroup.AddCssClass($"input-group {cssClass}".Trim());

                inputgroup.InnerHtml = tagBuilder.ToString();
                inputgroup.InnerHtml += "<span class=\"input-group-addon\" style=\"cursor:pointer;\">";
                inputgroup.InnerHtml += "<span class=\"glyphicon glyphicon-calendar\"></span></span>";
                return MvcHtmlString.Create(inputgroup.ToString(TagRenderMode.Normal));
            }
            tagBuilder.AddCssClass(cssClass);            
            return MvcHtmlString.Create(tagBuilder.ToString(TagRenderMode.Normal));
        }

        private MvcHtmlString createRadioButton()
        {
            var name = protectedName;
            var radiobutton = new StringBuilder();

            if(flowWebField.FlowFieldDataSource?.Options== null)
            {
                throw new Exception("Geen datasource");
            }

            foreach (var item in flowWebField.FlowFieldDataSource.Options)
            {
                var label = new TagBuilder("label");
                var radio = new TagBuilder("input");
                radio.Attributes.Add("type", "radio");
                radio.Attributes.Add("name", name);
                radio.Attributes.Add("value", item.Value ?? item.Text);

                radio.AddCssClass("flowfield");

                radio.Attributes["data-flowfieldid"] = flowWebField.FlowFieldID.ToString();
                radio.Attributes["data-source"] = $"{(int)flowWebField.Source}";
                radio.AddCssClass($"sourcefield-{(int)flowWebField.Source}");

                addReadOnlyAttribute();
                addjQueryUnobtrusiveValidationAttribute();
                addSpecialAttributes();

                radio.MergeAttributes(htmlAttributes, replaceExisting: true);

                if (item.Selected)
                {
                    radio.Attributes.Add("checked", null);
                }

                var displayValue = new TagBuilder("span") {InnerHtml = HttpUtility.HtmlEncode(item.Text)};

                label.InnerHtml += radio + (isInputOnly ? "" : displayValue.ToString());

                if (flowWebField.FlowFieldDataSource?.Type == FlowFieldDataSourceType.VerticalList)
                {
                    var outerdiv = new TagBuilder("div");
                    outerdiv.AddCssClass("radio");
                    outerdiv.InnerHtml = label.ToString();
                    radiobutton.Append(outerdiv);
                }
                else
                {
                    label.AddCssClass("radio-inline");
                    radiobutton.Append(label);
                }
            }

            var hiddenInput = new TagBuilder("input");
            hiddenInput.Attributes["id"] = TagBuilder.CreateSanitizedId(flowWebField.Name);
            hiddenInput.Attributes["type"] = "hidden";
            hiddenInput.Attributes["name"] = protectedName;

            hiddenInput.Attributes["data-flowfieldid"] = flowWebField.FlowFieldID.ToString();
            hiddenInput.Attributes["data-source"] = $"{(int)flowWebField.Source}";
            hiddenInput.AddCssClass($"sourcefield-{(int)flowWebField.Source}");

            if (!flowWebField.ReadOnly && !isReadMode)
            {
                var sourcefieldcontainer = new TagBuilder("div");
                sourcefieldcontainer.AddCssClass($"sourcecontainer");
                sourcefieldcontainer.AddCssClass($"sourcefield-{(int)flowWebField.Source}");
                sourcefieldcontainer.InnerHtml = radiobutton + hiddenInput.ToString();
                return MvcHtmlString.Create(sourcefieldcontainer.ToString());
            }

            var firstselected = flowWebField.FlowFieldDataSource.Options.FirstOrDefault(op => op.Selected);
            if (firstselected != null)
            {
                hiddenInput.Attributes["value"] = firstselected.Value ?? firstselected.Text;
            }

            return MvcHtmlString.Create(radiobutton + hiddenInput.ToString());
        }

        private MvcHtmlString createCheckBoxList()
        {
            if (flowWebField.FlowFieldDataSource?.Options == null)
            {
                throw new Exception("Geen datasource");
            }

            var stringBuilder = new StringBuilder();

            var value = new string[1];
            if (flowWebField.Value != null)
            {
                value = flowWebField.Value.Split(',');
            }

            foreach (var t in flowWebField.FlowFieldDataSource.Options)
            {
                var checkboxId = string.Concat(TagBuilder.CreateSanitizedId(flowWebField.Name), t.Value);

                var tagBuilder = new TagBuilder("input");
                tagBuilder.Attributes["id"] = checkboxId;
                tagBuilder.Attributes["type"] = "checkbox";
                tagBuilder.Attributes["name"] = protectedName;
                tagBuilder.Attributes["value"] = t.Value;
                tagBuilder.AddCssClass("flowfield");

                tagBuilder.Attributes["data-flowfieldid"] = flowWebField.FlowFieldID.ToString();
                tagBuilder.Attributes["data-source"] = $"{(int)flowWebField.Source}";
                tagBuilder.AddCssClass($"sourcefield-{(int)flowWebField.Source}");

                addReadOnlyAttribute();
                addjQueryUnobtrusiveValidationAttribute();
                addSpecialAttributes();
                
                if (value.Contains(t.Value))
                {
                    tagBuilder.Attributes.Add("checked", null);
                }

                tagBuilder.MergeAttributes(htmlAttributes, replaceExisting: true);

                var displayValue = new TagBuilder("span") {InnerHtml = t.Text};
                var label = new TagBuilder("label");
                label.Attributes.Add("for", checkboxId);
                label.InnerHtml = tagBuilder + displayValue.ToString();

                var container = label;
                if (flowWebField.FlowFieldDataSource?.Type == FlowFieldDataSourceType.VerticalList)
                {
                    var outerdiv = new TagBuilder("div");
                    outerdiv.AddCssClass("checkbox");
                    outerdiv.InnerHtml = label.ToString();
                    container = outerdiv;
                }
                else
                {
                    label.AddCssClass("checkbox-inline");
                }

                stringBuilder.Append(container);
            }

            // http://stackoverflow.com/a/14731571/1176273

            // Render an additional <input type="hidden".../> for checkboxes. This
            // addresses scenarios where unchecked checkboxes are not sent in the request.
            // Sending a hidden input makes it possible to know that the checkbox was present
            // on the page when the request was submitted.
            // http://aspnetwebstack.codeplex.com/SourceControl/latest#src/System.Web.Mvc/Html/InputExtensions.cs
            var hiddenInput = new TagBuilder("input");
            hiddenInput.Attributes["id"] = TagBuilder.CreateSanitizedId(flowWebField.Name);
            hiddenInput.Attributes["type"] = "hidden";

            hiddenInput.Attributes["name"] = protectedName;

            hiddenInput.Attributes["data-flowfieldid"] = flowWebField.FlowFieldID.ToString();
            hiddenInput.Attributes["data-source"] = $"{(int)flowWebField.Source}";

            // if ReadOnly then we want to store the fields value to make sure we keep it during a postback
            hiddenInput.Attributes["value"] = "false";

            stringBuilder.Append(hiddenInput);

            return MvcHtmlString.Create(stringBuilder.ToString());
        }

        private MvcHtmlString createCheckBox()
        {

            var div = new TagBuilder("div");
            div.AddCssClass("checkbox");
            div.MergeAttributes(htmlAttributes, replaceExisting: true);

            var label = new TagBuilder("label");

            var tagBuilder = new TagBuilder("input");
            tagBuilder.Attributes["id"] = TagBuilder.CreateSanitizedId(flowWebField.Name);
            tagBuilder.Attributes["type"] = "checkbox";
            tagBuilder.Attributes["name"] = protectedName;
            tagBuilder.Attributes["value"] = "true";
            tagBuilder.AddCssClass("flowfield");

            tagBuilder.Attributes["data-flowfieldid"] = flowWebField.FlowFieldID.ToString();
            tagBuilder.Attributes["data-source"] = $"{(int)flowWebField.Source}";
            tagBuilder.AddCssClass($"sourcefield-{(int)flowWebField.Source}");
            
            if (flowWebField.Value == "true")
            {
                tagBuilder.Attributes.Add("checked", null);
            }

            addReadOnlyAttribute();
            addjQueryUnobtrusiveValidationAttribute();
            addSpecialAttributes();

            tagBuilder.MergeAttributes(htmlAttributes, replaceExisting: true);

            // http://stackoverflow.com/a/14731571/1176273

            // Render an additional <input type="hidden".../> for checkboxes. This
            // addresses scenarios where unchecked checkboxes are not sent in the request.
            // Sending a hidden input makes it possible to know that the checkbox was present
            // on the page when the request was submitted.
            // http://aspnetwebstack.codeplex.com/SourceControl/latest#src/System.Web.Mvc/Html/InputExtensions.cs
            var hiddenInput = new TagBuilder("input");
            hiddenInput.Attributes["id"] = TagBuilder.CreateSanitizedId(flowWebField.Name);
            hiddenInput.Attributes["type"] = "hidden";
            hiddenInput.Attributes["name"] = protectedName;

            hiddenInput.Attributes["data-flowfieldid"] = flowWebField.FlowFieldID.ToString();
            hiddenInput.Attributes["data-source"] = $"{(int)flowWebField.Source}";

            // if ReadOnly then we want to store the fields value to make sure we keep it during a postback
            hiddenInput.Attributes["value"] = (flowWebField.ReadOnly ? flowWebField.Value : "false");

            if (isInputOnly)
            {
                label.InnerHtml += tagBuilder + hiddenInput.ToString();
            }
            else
            {
                var displayValue = new TagBuilder("span") { InnerHtml = flowWebField.Label };
                label.InnerHtml += tagBuilder + hiddenInput.ToString() + displayValue;
            }

            div.InnerHtml = label.ToString();

            return MvcHtmlString.Create(div.ToString());
        }

        private MvcHtmlString createDropDownList()
        {
            TagBuilder hiddenField = null;

            if (flowWebField.FlowFieldDataSource?.Options == null)
            {
                throw new Exception("Geen datasource");
            }

            var tagBuilder = new TagBuilder("select");
            tagBuilder.Attributes["id"] = TagBuilder.CreateSanitizedId(flowWebField.Name);
            tagBuilder.Attributes["name"] = protectedName;
            tagBuilder.AddCssClass("flowfield form-control");

            tagBuilder.Attributes["data-flowfieldid"] = flowWebField.FlowFieldID.ToString();
            tagBuilder.Attributes["data-source"] = $"{(int)flowWebField.Source}";
            tagBuilder.AddCssClass($"sourcefield-{(int)flowWebField.Source}");

            addReadOnlyAttribute();
            addjQueryUnobtrusiveValidationAttribute();
            addSpecialAttributes();
            
            if (flowWebField.ReadOnly || isReadMode)
            {
                flowWebField.FlowFieldDataSource.Options = flowWebField.FlowFieldDataSource.Options.Where(op => op.Selected).ToList();

                hiddenField = new TagBuilder("input");
                hiddenField.Attributes["name"] = protectedName;
                hiddenField.Attributes["value"] = flowWebField.Value;
                hiddenField.Attributes["type"] = "hidden";

                hiddenField.Attributes["data-flowfieldid"] = flowWebField.FlowFieldID.ToString();
                hiddenField.Attributes["data-source"] = $"{(int)flowWebField.Source}";
            }

            var optionsBuilder = toOptionString(flowWebField.FlowFieldDataSource.Options);
            
            tagBuilder.InnerHtml = optionsBuilder.ToString();
            tagBuilder.MergeAttributes(htmlAttributes, replaceExisting: true);

            return MvcHtmlString.Create(String.Concat(tagBuilder.ToString(TagRenderMode.Normal), (hiddenField != null ? hiddenField.ToString(TagRenderMode.SelfClosing) : "")));
        }

        private MvcHtmlString createIDText()
        {
            var inputControl = new TagBuilder("input");

            if (isPrintMode)
            {
                inputControl = createPrintControl(flowWebField.DisplayValue);
            }
            else
            {
                inputControl.Attributes["value"] = inputControl.Attributes["title"] = flowWebField.DisplayValue;
                inputControl.Attributes["data-linked"] = flowWebField.Name;
                inputControl.AddCssClass("flowfield form-control cursor-default");
                inputControl.Attributes["readonly"] = "readonly";
            }

            inputControl.Attributes["id"] = TagBuilder.CreateSanitizedId(flowWebField.Name);
            inputControl.Attributes["type"] = "text";
            inputControl.Attributes["name"] = protectedName;

            inputControl.Attributes["data-flowfieldid"] = flowWebField.FlowFieldID.ToString();
            inputControl.Attributes["data-source"] = $"{(int)flowWebField.Source}";
            inputControl.AddCssClass($"sourcefield-{(int)flowWebField.Source}");

            addReadOnlyAttribute();
            addjQueryUnobtrusiveValidationAttribute();
            addSpecialAttributes();

            inputControl.MergeAttributes(htmlAttributes, replaceExisting: true);

            var hiddenInput = new TagBuilder("input");
            hiddenInput.Attributes["id"] = TagBuilder.CreateSanitizedId(flowWebField.Name);
            hiddenInput.Attributes["type"] = "hidden";
            hiddenInput.Attributes["name"] = protectedName;
            hiddenInput.Attributes["value"] = flowWebField.Value;
            hiddenInput.Attributes["data-linked"] = flowWebField.Name;

            hiddenInput.Attributes["data-flowfieldid"] = flowWebField.FlowFieldID.ToString();
            hiddenInput.Attributes["data-source"] = $"{(int)flowWebField.Source}";

            return MvcHtmlString.Create(inputControl + hiddenInput.ToString());
        }

        private string toOptionString(Option option)
        {
            var builder = new TagBuilder("option")
            {
                InnerHtml = HttpUtility.HtmlEncode(option.Text)
            };
            if (option.Value != null)
            {
                builder.Attributes["value"] = option.Value;
                builder.Attributes["data-digest"] = AntiFiddleInjection.CreateDigest(option.Value);
            }
            if (option.Selected)
            {
                builder.Attributes["selected"] = "selected";
            }

            return builder.ToString(TagRenderMode.Normal);
        }

        private StringBuilder toOptionString(IList<Option> options)
        {
            var optionsBuilder = new StringBuilder();
            foreach (var option in options)
            {
                optionsBuilder.Append(toOptionString(option));
            }
            return optionsBuilder;
        }

        private IList<Option> setSelectedOption(IList<Option> options)
        {
            foreach (var option in options.ToList())
            {
                if (string.IsNullOrEmpty(flowWebField.Value) ||
                    (!Equals(flowWebField.Value.Normalize(NormalizationForm.FormC), option.Value) &&
                     !Equals(flowWebField.Value.Normalize(NormalizationForm.FormC), option.Text)))
                {
                    continue;
                }

                options.ToList().ForEach(it => it.Selected = false);
                option.Selected = true;
                break;
            }

            return options;
        }

        private void addReadOnlyAttribute()
        {
            switch (flowWebField.Type)
            {
                case FlowFieldType.String:
                case FlowFieldType.Date:
                case FlowFieldType.DateTime:
                case FlowFieldType.Time:
                case FlowFieldType.Number:
                case FlowFieldType.IDText:
                    if (flowWebField.ReadOnly || isReadMode)
                    {
                        htmlAttributes["readonly"] = "readonly";
                    }
                    break;

                case FlowFieldType.RadioButton:
                case FlowFieldType.Checkbox:
                case FlowFieldType.CheckboxList:
                case FlowFieldType.DropDownList:
                    if (flowWebField.ReadOnly || isReadMode)
                    {
                        htmlAttributes["disabled"] = "disabled";
                    }
                    break;

                case FlowFieldType.None:
                case FlowFieldType.Hidden:
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void addSpecialAttributes()
        {
            if (flowWebField.ReadOnly || !flowWebField.Visible)
            {
                return;
            }

            if (flowWebField.SignalOnChange)
            {
                htmlAttributes["data-signalonchange"] = "true";
                htmlAttributes["data-label"] = flowWebField.Label;
            }
            if (flowWebField.HasHistory)
            {
                htmlAttributes["data-hashistory"] = "true";
                htmlAttributes["data-formsetversionid"] = flowWebField.FormSetVersionID;
                //htmlAttributes["data-flowfieldid"] = flowWebField.FlowFieldID;
            }
        }

        private void addjQueryUnobtrusiveValidationAttribute()
        {
            if (flowWebField.ReadOnly || !flowWebField.Visible)
            {
                return;
            }

            var beforeValidationAttributes = htmlAttributes.Count;

            if (flowWebField.Required && !flowWebField.ReadOnly)
            {
                htmlAttributes["data-val-required"] = BaseEntity.ErrorMessage(BaseEntity.errorRequired, new object[] {flowWebField.Label});
                htmlAttributes["data-msg-required"] = BaseEntity.ErrorMessage(BaseEntity.errorRequired, new object[] {flowWebField.Label});
            }

            htmlAttributes["data-field"] = flowWebField.Name;

            if (!string.IsNullOrEmpty(flowWebField.RequiredBy) && !flowWebField.ReadOnly)
            {
                htmlAttributes["data-val-requiredby"] = flowWebField.RequiredBy;
                htmlAttributes["data-val-requiredby-errormessage"] = BaseEntity.ErrorMessage(BaseEntity.errorRequiredBy, new object[] {flowWebField.Label, flowWebField.RequiredByLabel});
            }

            if (flowWebField.MaxLength.HasValue)
            {
                htmlAttributes["data-val-length"] = BaseEntity.ErrorMessage(BaseEntity.errorMaxLength, new object[] {flowWebField.Label, flowWebField.MaxLength});
                htmlAttributes["data-val-length-max"] = flowWebField.MaxLength;
            }

            if (!string.IsNullOrWhiteSpace(flowWebField.Regexp))
            {
                htmlAttributes["data-val-regex"] = BaseEntity.ErrorMessage(BaseEntity.errorRegexp, new object[] {flowWebField.Label});
                htmlAttributes["data-val-regex-pattern"] = flowWebField.Regexp;
            }
            else
            {
                if (flowWebField.Type == FlowFieldType.String)
                {
                }
                else if (flowWebField.Type == FlowFieldType.Number)
                {
                    htmlAttributes["data-val-number"] = BaseEntity.ErrorMessage(BaseEntity.errorNumber, new object[] {flowWebField.Label});
                }
                else if (flowWebField.Type == FlowFieldType.RadioButton)
                {
                }
                else if (flowWebField.Type == FlowFieldType.Checkbox)
                {
                }
                else if (flowWebField.Type == FlowFieldType.DropDownList)
                {
                }
            }

            if (htmlAttributes.Count > beforeValidationAttributes)
            {
                htmlAttributes["data-val"] = "true";
            }
        }

        private void addDbDrivenValidationAttributes()
        {
            if (flowWebField.ReadOnly || !flowWebField.Visible || flowWebField.DbDrivenValidation == null)
            {
                return;
            }

            var validation = flowWebField.DbDrivenValidation;

            htmlAttributes["data-dbval-method"] = validation.Method;
            htmlAttributes["data-dbval-field-attributes"] = string.Join("|", validation.FieldAttributes);
            htmlAttributes["data-dbval-warning-triggers"] = string.Join("|", validation.WarningTriggers);
            htmlAttributes["data-dbval-messages"] = string.Join("|", validation.Messages);
            htmlAttributes["data-dbval-field-attribute-index-within-validation"] = validation.FieldAttributeIndexWithinValidation;
            htmlAttributes["data-dbval-field-attribute-id"] = flowWebField.FlowFieldAttributeID;
        }

        public static string GenerateProtectedName(FlowWebField flowWebField)
        {
            var generatedName = flowWebField.GenerateWebName();
            return $"{FlowWebField.NAME_PREFIX}{generatedName}";
        }
    }

    public class FlowControlGroup : IDisposable
    {
        private readonly TextWriter writer;
        private readonly string fieldName;
        private readonly FlowWebField flowWebField;

        public FlowControlGroup(TextWriter writer, string fieldName, FlowWebField flowWebField)
        {
            this.writer = writer;
            this.fieldName = fieldName;
            this.flowWebField = flowWebField;
        }

        private bool isDisposed;

        public void Dispose()
        {
            Dispose(true);
        }

        public virtual void Dispose(bool disposing)
        {
            if (isDisposed) return;

            isDisposed = true;
            if (flowWebField == null)
            {
                writer.Write("<!-- End FlowFieldGroup '{0}' not found. -->", fieldName);
            }
            else
            {
                writer.Write("</div></div>");
            }
            writer.Flush();
        }
    }
}