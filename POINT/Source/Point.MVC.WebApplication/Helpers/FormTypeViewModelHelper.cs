﻿using Point.Business.Helper;
using Point.Business.Logic;
using Point.Database.CacheModels;
using Point.Database.Extensions;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure;
using Point.Infrastructure.Extensions;
using Point.Models.Enums;
using Point.MVC.WebApplication.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Point.Communication.Infrastructure.Helpers;
using Point.Database.Helpers;
using Point.MVC.WebApplication.Controllers;

namespace Point.MVC.WebApplication.Helpers
{
    public static class FormTypeViewModelHelper
    {
        public static ZorgAdviesViewModel GetZorgAdviesViewModel(IUnitOfWork uow, GlobalProperties globalProps)
        {
            var viewModel = new ZorgAdviesViewModel();
            viewModel.SetGlobalProperties(globalProps);

            var zorgAdviesToevoegen = FlowFieldValueBL.GetByTransferIDAndFieldID(uow, globalProps.TransferID, FlowFieldConstants.ID_ZorgAdviesToevoegen).FirstOrDefault();
            viewModel.HideZorgAdvies = (zorgAdviesToevoegen == null || zorgAdviesToevoegen.Value != "Ja");

            var transfer = globalProps.Transfer;

            if (transfer != null && viewModel.Prefill && !globalProps.IsReadMode)
            {
                var burgerlijkeStaat = viewModel.Global.FlowWebFields.FirstOrDefault(fwf => fwf.Name == FlowFieldConstants.Name_BurgerlijkeStaat);
                if (burgerlijkeStaat != null && transfer.Client.CivilClass.HasValue)
                {
                    FlowWebFieldBL.SetValue(burgerlijkeStaat, (int)transfer.Client.CivilClass, Source.POINT_PREFILL);
                }

                var kinderenAanwezigAantal = viewModel.Global.FlowWebFields.FirstOrDefault(fwf => fwf.Name == FlowFieldConstants.Name_KinderenAanwezigAantal);
                if (kinderenAanwezigAantal != null && transfer.Client.ChildrenInHousekeeping.GetValueOrDefault(0) > 0)
                {
                    FlowWebFieldBL.SetValue(kinderenAanwezigAantal, transfer.Client.ChildrenInHousekeeping.GetValueOrDefault(0), Source.POINT_PREFILL);
                }

                var kinderenAanwezig = viewModel.Global.FlowWebFields.FirstOrDefault(fwf => fwf.Name == FlowFieldConstants.Name_KinderenAanwezig);
                if (kinderenAanwezig != null)
                {
                    FlowWebFieldBL.SetValue(kinderenAanwezig, string.IsNullOrEmpty(kinderenAanwezigAantal?.Value) ? "Onbekend" : "Ja", Source.POINT_PREFILL);
                }

                var soortWoning = viewModel.Global.FlowWebFields.FirstOrDefault(fwf => fwf.Name == FlowFieldConstants.Name_HousingType);
                if (soortWoning != null && transfer.Client.HousingType.HasValue)
                {
                    FlowWebFieldBL.SetValue(soortWoning, (int)transfer.Client.HousingType, Source.POINT_PREFILL);
                }
            }

            viewModel.ZorgAdviesItems = ZorgAdviesItemViewBL.GetViewModel(uow, globalProps.FlowInstance.FlowInstanceID);

            return viewModel;
        }

        public static ELVFormViewModel GetElvFormViewModel(IUnitOfWork uow, GlobalProperties globalProps)
        {
            var sendingDep = globalProps.FlowInstance.StartedByDepartment;
            var sendingOrganizationName = "";
            if (sendingDep?.Location?.Organization != null)
            {
                string agb = sendingDep.Location.Organization.AGB;
                if (!string.IsNullOrEmpty(sendingDep.AGB)) { agb = sendingDep.AGB; }

                sendingOrganizationName = $"{sendingDep.Location.FullName()} {agb}";
            }

            if (!globalProps.HasFormSetVersion && !globalProps.IsReadMode)
            {
                var ingevuldDoor = globalProps.FlowWebFields.FirstOrDefault(it => it.Name == "ELVIngevuldDoor");
                FlowWebFieldBL.SetValue(ingevuldDoor, (int)globalProps.PointUserInfo?.Employee?.EmployeeID);

                var datumAfweging = globalProps.FlowWebFields.FirstOrDefault(it => it.Name == "ELVDatumAfweging");
                FlowWebFieldBL.SetValue(datumAfweging, DateTime.Now);
            }
            var viewModel =new ELVFormViewModel
            {
                ClientID = globalProps.FlowInstance?.Transfer?.ClientID,
                SendingOrganizationName = sendingOrganizationName
            };
            viewModel.SetGlobalProperties(globalProps);

            return viewModel;
        }

        public static ELVFormHAViewModel GetElvFormHAViewModel(IUnitOfWork uow, GlobalProperties globalProps)
        {
            var sendingDep = globalProps.FlowInstance.StartedByDepartment;
            var sendingOrganizationName = "";
            if (sendingDep?.Location?.Organization != null)
            {
                string agb = sendingDep.Location.Organization.AGB;
                if (!string.IsNullOrEmpty(sendingDep.AGB)) { agb = sendingDep.AGB; }

                sendingOrganizationName = $"{sendingDep.Location.FullName()} {agb}";
            }

            if (!globalProps.HasFormSetVersion && !globalProps.IsReadMode)
            {
                var ingevuldDoor = globalProps.FlowWebFields.FirstOrDefault(it => it.Name == "ELVIngevuldDoor");
                FlowWebFieldBL.SetValue(ingevuldDoor, (int)globalProps.PointUserInfo?.Employee?.EmployeeID);

                var datumAfweging = globalProps.FlowWebFields.FirstOrDefault(it => it.Name == "ELVDatumAfweging");
                FlowWebFieldBL.SetValue(datumAfweging, DateTime.Now);
            }

            var viewModel = new ELVFormHAViewModel
            {
                ClientID = globalProps.FlowInstance?.Transfer?.ClientID,
                SendingOrganizationName = sendingOrganizationName
            };
            viewModel.SetGlobalProperties(globalProps);

            return viewModel;
        }

        public static GVPViewModel GetGVPViewModel(IUnitOfWork uow, GlobalProperties globalProps)
        {
            var viewModel = new GVPViewModel( );
            viewModel.SetGlobalProperties(globalProps);

            int? vvtdepartmentid = null;
            int? vvtdepartmentidmsvt = null;

            var uvvactionsinfostring = new StringBuilder();
            var flowInstanceID = globalProps.FlowInstance.FlowInstanceID;

            GenerateGVpInfoLine(
                uow,
                flowInstanceID,
                uvvactionsinfostring,
                "Medicatietoediening per injectie",
                FlowFieldConstants.ID_MedicatieToedieningPerInjectie,
                FlowFieldConstants.ID_MedicatieToedieningPerInjectieKerenPerDag,
                FlowFieldConstants.ID_MedicatieToedieningPerInjectieKerenPerWeek,
                FlowFieldConstants.ID_MedicatieToedieningPerInjectieToelichting);
            GenerateGVpInfoLine(
                uow,
                flowInstanceID,
                uvvactionsinfostring,
                "Parenterale toediening medicatie / pijnbestrijding",
                FlowFieldConstants.ID_InfusietherapieParenteraleToedieningMedicatie,
                FlowFieldConstants.ID_InfusietherapieParenteraleToedieningMedicatieKerenPerDag,
                FlowFieldConstants.ID_InfusietherapieParenteraleToedieningMedicatieKerenPerWeek,
                FlowFieldConstants.ID_InfusietherapieParenteraleToedieningMedicatieToelichting);
            GenerateGVpInfoLine(
                uow,
                flowInstanceID,
                uvvactionsinfostring,
                "Parenterale voeding",
                FlowFieldConstants.ID_InfusietherapieParenteraleVoeding,
                FlowFieldConstants.ID_InfusietherapieParenteraleVoedingKerenPerDag,
                FlowFieldConstants.ID_InfusietherapieParenteraleVoedingKerenPerWeek,
                FlowFieldConstants.ID_InfusietherapieParenteraleVoedingToelichting);
            GenerateGVpInfoLine(
                uow,
                flowInstanceID,
                uvvactionsinfostring,
                "PICC",
                FlowFieldConstants.ID_InfusietherapiePICC,
                FlowFieldConstants.ID_InfusietherapiePICCKerenPerDag,
                FlowFieldConstants.ID_InfusietherapiePICCKerenPerWeek,
                FlowFieldConstants.ID_InfusietherapiePICCToelichting);
            GenerateGVpInfoLine(
                uow,
                flowInstanceID,
                uvvactionsinfostring,
                "Centraal veneuze lijn",
                FlowFieldConstants.ID_InfusietherapieCentraalVeneuzeLijn,
                FlowFieldConstants.ID_InfusietherapieCentraalVeneuzeLijnKerenPerDag,
                FlowFieldConstants.ID_InfusietherapieCentraalVeneuzeLijnKerenPerWeek,
                FlowFieldConstants.ID_InfusietherapieCentraalVeneuzeLijnToelichting);
            GenerateGVpInfoLine(
                uow,
                flowInstanceID,
                uvvactionsinfostring,
                "Nierfunctievervangende therapie",
                FlowFieldConstants.ID_NierfunctievervangendeTherapie,
                FlowFieldConstants.ID_NierfunctievervangendeTherapieKerenPerDag,
                FlowFieldConstants.ID_NierfunctievervangendeTherapieKerenPerWeek,
                FlowFieldConstants.ID_NierfunctievervangendeTherapieToelichting);
            GenerateGVpInfoLine(
                uow,
               flowInstanceID,
                uvvactionsinfostring,
                "Transfusies bloed(producten)",
                FlowFieldConstants.ID_TransfusiesBloed,
                FlowFieldConstants.ID_TransfusiesBloedKerenPerDag,
                FlowFieldConstants.ID_TransfusiesBloedKerenPerWeek,
                FlowFieldConstants.ID_TransfusiesBloedToelichting);
            GenerateGVpInfoLine(
                uow,
                flowInstanceID,
                uvvactionsinfostring,
                "Overige verrichtingen",
                FlowFieldConstants.ID_OverigeVerrichtingen,
                FlowFieldConstants.ID_OverigeVerrichtingenKerenPerDag,
                FlowFieldConstants.ID_OverigeVerrichtingenKerenPerWeek,
                FlowFieldConstants.ID_OverigeVerrichtingenToelichting);

            viewModel.UvvActionsInfo = uvvactionsinfostring.ToString();

            var highriskactionsinfostring = new StringBuilder();

            GenerateGVpInfoLine(
                uow,
                flowInstanceID,
                highriskactionsinfostring,
                "Medicatie toediening per os",
                FlowFieldConstants.ID_MedicatieToedieningPerOs,
                FlowFieldConstants.ID_MedicatieToedieningPerOsKerenPerDag,
                FlowFieldConstants.ID_MedicatieToedieningPerOsKerenPerWeek,
                FlowFieldConstants.ID_MedicatieToedieningPerOsToelichting);
            GenerateGVpInfoLine(
                uow,
                flowInstanceID,
                highriskactionsinfostring,
                "Zuurstoftoediening",
                FlowFieldConstants.ID_ControleLichaamsfunctiesZuurstoftoediening,
                FlowFieldConstants.ID_ControleLichaamsfunctiesZuurstoftoedieningKerenPerDag,
                FlowFieldConstants.ID_ControleLichaamsfunctiesZuurstoftoedieningKerenPerWeek,
                FlowFieldConstants.ID_ControleLichaamsfunctiesZuurstoftoedieningToelichting);
            GenerateGVpInfoLine(
                uow,
                flowInstanceID,
                highriskactionsinfostring,
                "Medicatie toediening inhalatie/verneveling",
                FlowFieldConstants.ID_MedicatieToedieningInhalatieVerneveling,
                FlowFieldConstants.ID_MedicatieToedieningInhalatieVernevelingKerenPerDag,
                FlowFieldConstants.ID_MedicatieToedieningInhalatieVernevelingKerenPerWeek,
                FlowFieldConstants.ID_MedicatieToedieningInhalatieVernevelingToelichting);
            GenerateGVpInfoLine(
                uow,
                flowInstanceID,
                highriskactionsinfostring,
                "Medicatie toediening druppelen",
                FlowFieldConstants.ID_MedicatieToedieningDruppelen,
                FlowFieldConstants.ID_MedicatieToedieningDruppelenKerenPerDag,
                FlowFieldConstants.ID_MedicatieToedieningDruppelenKerenPerWeek,
                FlowFieldConstants.ID_MedicatieToedieningDruppelenToelichting);
            GenerateGVpInfoLine(
                uow,
                flowInstanceID,
                highriskactionsinfostring,
                "Medicatie toediening zalven",
                FlowFieldConstants.ID_MedicatieToedieningZalven,
                FlowFieldConstants.ID_MedicatieToedieningZalvenKerenPerDag,
                FlowFieldConstants.ID_MedicatieToedieningZalvenKerenPerWeek,
                FlowFieldConstants.ID_MedicatieToedieningZalvenToelichting);
            GenerateGVpInfoLine(
                uow,
                flowInstanceID,
                highriskactionsinfostring,
                "Bloedsuiker controle",
                FlowFieldConstants.ID_ControleLichaamsfunctiesBloedsuikerControle,
                FlowFieldConstants.ID_ControleLichaamsfunctiesBloedsuikerControleKerenPerDag,
                FlowFieldConstants.ID_ControleLichaamsfunctiesBloedsuikerControleKerenPerWeek,
                FlowFieldConstants.ID_ControleLichaamsfunctiesBloedsuikerControleToelichting);
            GenerateGVpInfoLine(
                uow,
                flowInstanceID,
                highriskactionsinfostring,
                "Wondverzorging",
                FlowFieldConstants.ID_Wondverzorging,
                FlowFieldConstants.ID_WondverzorgingKerenPerDag,
                FlowFieldConstants.ID_WondverzorgingKerenPerWeek,
                FlowFieldConstants.ID_WondverzorgingToelichting);
            GenerateGVpInfoLine(
                uow,
                flowInstanceID,
                highriskactionsinfostring,
                "Stomaverzorging",
                FlowFieldConstants.ID_Stomaverzorging,
                FlowFieldConstants.ID_StomaverzorgingKerenPerDag,
                FlowFieldConstants.ID_StomaverzorgingKerenPerWeek,
                FlowFieldConstants.ID_StomaverzorgingToelichting);
            GenerateGVpInfoLine(
                uow,
                flowInstanceID,
                highriskactionsinfostring,
                "Drainverzorging",
                FlowFieldConstants.ID_Drainverzorging,
                FlowFieldConstants.ID_DrainverzorgingKerenPerDag,
                FlowFieldConstants.ID_DrainverzorgingKerenPerWeek,
                FlowFieldConstants.ID_DrainverzorgingToelichting);
            GenerateGVpInfoLine(
                uow,
                flowInstanceID,
                highriskactionsinfostring,
                "Suprapubis katheter verzorging",
                FlowFieldConstants.ID_CatheterVerzorgingSuprapubisCatheter,
                FlowFieldConstants.ID_CatheterVerzorgingSuprapubisCatheterKerenPerDag,
                FlowFieldConstants.ID_CatheterVerzorgingSuprapubisCatheterKerenPerWeek,
                FlowFieldConstants.ID_CatheterVerzorgingSuprapubisCatheterToelichting);
            GenerateGVpInfoLine(
                uow,
                flowInstanceID,
                highriskactionsinfostring,
                "Blaaskatheter verzorging",
                FlowFieldConstants.ID_CatheterVerzorgingBlaascatheter,
                FlowFieldConstants.ID_CatheterVerzorgingBlaascatheterKerenPerDag,
                FlowFieldConstants.ID_CatheterVerzorgingBlaascatheterKerenPerWeek,
                FlowFieldConstants.ID_CatheterVerzorgingBlaascatheterToelichting);
            GenerateGVpInfoLine(
                uow,
                flowInstanceID,
                highriskactionsinfostring,
                "Sondevoeding",
                FlowFieldConstants.ID_Sondevoeding,
                FlowFieldConstants.ID_SondevoedingKerenPerDag,
                FlowFieldConstants.ID_SondevoedingKerenPerWeek,
                FlowFieldConstants.ID_SondevoedingToelichting);
            GenerateGVpInfoLine(
                uow,
                flowInstanceID,
                highriskactionsinfostring,
                "Overige verrichtingen",
                FlowFieldConstants.ID_OverigeVerrichtingen,
                FlowFieldConstants.ID_OverigeVerrichtingenKerenPerDag,
                FlowFieldConstants.ID_OverigeVerrichtingenKerenPerWeek,
                FlowFieldConstants.ID_OverigeVerrichtingenToelichting);

            viewModel.HighRiskActionsInfo = highriskactionsinfostring.ToString();

            viewModel.ClientID = globalProps.Transfer?.ClientID;

            var formtype = FormTypeBL.GetByURL(uow, globalProps.ControllerName, globalProps.ActionName);

            if (viewModel.Prefill)
            {
                PrefillDoctor(uow, globalProps.FlowWebFields, globalProps.TransferID);
            }

            viewModel.SendingOrganization = "";

            var sendingdep = globalProps.FlowInstance.StartedByDepartment;
            if (sendingdep?.Location?.Organization != null)
            {
                string agb = sendingdep.Location.Organization.AGB;
                if (!String.IsNullOrEmpty(sendingdep.AGB))
                {
                    agb = sendingdep.AGB;
                }

                viewModel.SendingOrganization = $"{sendingdep.Location.FullName()} {agb}";
            }

            var MSVTLeverendeOrganisatie = globalProps.FlowWebFields.FirstOrDefault(fwv => fwv.Name == FlowFieldConstants.Name_MSVTLeverendeOrganisatie);
            if (!string.IsNullOrEmpty(MSVTLeverendeOrganisatie?.Value))
            {
                if (int.TryParse(MSVTLeverendeOrganisatie.Value, out int msvtleverendeorganisatie))
                {
                    vvtdepartmentidmsvt = msvtleverendeorganisatie;
                }
            }
            viewModel.ReceivingOrganization = "";
            var patientaccepted = PatientHelper.IsAccepted(uow, globalProps.FlowInstance.FlowInstanceID);
            if (patientaccepted ==  AcceptanceState.Acknowledged)
            {
                var receivingDepartment = OrganizationHelper.GetRecievingDepartment(uow, globalProps.FlowInstance.FlowInstanceID);
                var receivingEmployee = OrganizationHelper.GetRecievingEmployee(uow, globalProps.FlowInstance.FlowInstanceID);

                if (receivingDepartment != null)
                {
                    vvtdepartmentid = receivingDepartment.DepartmentID;
                    viewModel.ReceivingOrganization = receivingDepartment.FullName();
                }

                if (MSVTLeverendeOrganisatie != null && receivingDepartment != null && string.IsNullOrEmpty(MSVTLeverendeOrganisatie.Value))
                {
                    FlowWebFieldBL.SetValue(MSVTLeverendeOrganisatie, receivingDepartment.DepartmentID, Source.POINT_PREFILL);
                }

                if (receivingEmployee != null)
                {
                    var MSVTLeverendeMedewerker = globalProps.FlowWebFields.FirstOrDefault(fwv => fwv.Name == FlowFieldConstants.Name_MSVTLeverendeMedewerker);
                    if (MSVTLeverendeMedewerker != null)
                    {
                        MSVTLeverendeMedewerker.DisplayValue = receivingEmployee.FullName();
                        FlowWebFieldBL.SetSource(MSVTLeverendeMedewerker, Source.POINT_PREFILL);
                    }

                    var MSVTLeverendeMedewerkerBIG = globalProps.FlowWebFields.FirstOrDefault(fwv => fwv.Name == FlowFieldConstants.Name_MSVTLeverendeMedewerkerBIG);
                    if (MSVTLeverendeMedewerkerBIG != null)
                    {
                        MSVTLeverendeMedewerkerBIG.DisplayValue = receivingEmployee.BIG;
                        FlowWebFieldBL.SetSource(MSVTLeverendeMedewerkerBIG, Source.POINT_PREFILL);
                    }

                    var MSVTLeverendeMedewerkerTelefoon = globalProps.FlowWebFields.FirstOrDefault(fwv => fwv.Name == FlowFieldConstants.Name_MSVTLeverendeMedewerkerTelefoon);
                    if (MSVTLeverendeMedewerkerTelefoon != null)
                    {
                        MSVTLeverendeMedewerkerTelefoon.DisplayValue = receivingEmployee.PhoneNumber;
                        FlowWebFieldBL.SetSource(MSVTLeverendeMedewerkerTelefoon, Source.POINT_PREFILL);
                    }

                    var MSVTLeverendeMedewerkerEmail = globalProps.FlowWebFields.FirstOrDefault(fwv => fwv.Name == FlowFieldConstants.Name_MSVTLeverendeMedewerkerEmail);
                    if (MSVTLeverendeMedewerkerEmail != null)
                    {
                        MSVTLeverendeMedewerkerEmail.DisplayValue = receivingEmployee.EmailAddress;
                        FlowWebFieldBL.SetSource(MSVTLeverendeMedewerkerEmail, Source.POINT_PREFILL);
                    }
                }
            }

            var actions = ActionCodeHealthInsurerBL.GetByRegionIDAndFormTypeID(uow, viewModel.Global.PointUserInfo.Region.RegionID, (int)FlowFormType.GVp);
            List<ActionHealthInsurer> actionvalues = null;
            List<MedicineCard> medicinevalues = null;
            if (globalProps.HasFormSetVersion)
            {
                actionvalues = ActionHealthInsurerBL.GetByFormSetVersionID(uow, globalProps.FormSetVersionID).ToList();
                medicinevalues = MedicineCardBL.GetByFormSetVersionID(uow, globalProps.FormSetVersionID).ToList();
            }

            viewModel.GVPHandelingItems = GVPHandelingViewModel.GetByModelList(actions, actionvalues);
            var filledItems = viewModel.GVPHandelingItems.Where(it => !string.IsNullOrEmpty(it.Frequency)).ToList();
            viewModel.IsRisicoVolle = filledItems.All(it => GVPViewModel.RisicovolleGroupIds.Contains(it.GroupID));        //Alle moeten matchen met risicovolle groepen (All)
            viewModel.IsUitvoeringsVerzoek = filledItems.Any(it => GVPViewModel.UitvoeringsVerzoekGroupIds.Contains(it.GroupID));  //Tenminste 1 in de uitvoeringverzoek/voorbehouden groepen (Any)
            viewModel.HasRisicoVolle = filledItems.Any(it => GVPViewModel.RisicovolleGroupIds.Contains(it.GroupID));
            viewModel.HasUitvoeringsVerzoek = filledItems.Any(it => GVPViewModel.UitvoeringsVerzoekGroupIds.Contains(it.GroupID));
            viewModel.MedicationItems = MedicationItemViewModel.GetByModelList(medicinevalues);

            viewModel.FormSetVersions = FormSetVersionBL.GetByTransferIDAndFormTypeID(uow, globalProps.TransferID, formtype.FormTypeID);
            viewModel.VVTDepartmentID = vvtdepartmentid;
            viewModel.VVTDepartmentIDMSVT = vvtdepartmentidmsvt;

            var phasedefinition = PhaseDefinitionCacheBL.GetByID(uow, globalProps.PhaseDefinitionID);
            var rights = PhaseDefinitionRightsBL.GetFormRightsPhaseDefinitionID(uow, globalProps.PointUserInfo, phasedefinition, globalProps.FlowInstance);
            viewModel.HideNewButtons = (rights == Rights.R || rights == Rights.None) || globalProps.IsClosed || !globalProps.HasFormSetVersion;

            var requestFormZHVVTType = FlowFieldValueBL.GetByFlowInstanceIDAndFieldID(uow, globalProps.FlowInstance.FlowInstanceID, FlowFieldConstants.ID_RequestFormZHVVTType).FirstOrDefault();
            if (requestFormZHVVTType != null && requestFormZHVVTType.Value == ((int)FlowFormType.RequestFormZHVVT_FormulierPoli).ToString())
            {
                globalProps.FlowWebFields.Where(it => it.Name == FlowFieldConstants.Name_MedischeSituatieDatumOpname).ToList().ForEach(it => it.ReadOnly = true);
            }

            var signedPhaseInstance = SignedPhaseInstanceHelper.GetSignedPhaseInstanceViewModel(uow, globalProps.FormSetVersionID, globalProps.PhaseInstanceID);
            if (signedPhaseInstance.HasSignedPhaseInstance)
            {
                viewModel.SignedPhaseInstanceDate = signedPhaseInstance.Date.ToPointDateHourDisplay();
                viewModel.SignedByEmployeeFunction = signedPhaseInstance.SignedByEmployeeFunction;
                viewModel.SignedByEmployeeName = signedPhaseInstance.SignedByEmployeeName;
                viewModel.SignedPhaseInstancePhaseInstanceID = signedPhaseInstance.PhaseInstanceID;
            }

            return viewModel;
        }

        public static MsvtViewModel GetMsvtViewModel(IUnitOfWork uow, GlobalProperties globalProps)
        {
            var phaseDefinition = PhaseDefinitionCacheBL.GetByID(uow, globalProps.PhaseDefinitionID);
            var viewModel = CreateMsvtViewModel(uow, globalProps, FlowFormType.MSVT_Uitvoeringsverzoek, phaseDefinition, showHistory: true);

            if (!viewModel.Prefill)
            {
                return viewModel;
            }

            PrefillDoctor(uow, globalProps.FlowWebFields, globalProps.TransferID);

            return viewModel;
        }

        public static MsvtViewModel GetMsvtINDViewModel(IUnitOfWork uow, GlobalProperties globalProps, Controller controller)
        {
            var phasedefinition = PhaseDefinitionCacheBL.GetByFlowDefinitionAndFormType(uow, globalProps.FlowInstance.FlowDefinitionID, (int)FlowFormType.MSVTIndicatiestelling);
            var viewModel = CreateMsvtViewModel(uow, globalProps, FlowFormType.MSVT_Indicatiestelling, phasedefinition, showHistory: true, fillSignedPhaseInstance: true);

            if (viewModel.Prefill)
            {
                var requestformids = new[] { (int)FlowFormType.AanvraagFormulierZHVVT };


                var behandelendArtsID = viewModel.Global.FlowWebFields.FirstOrDefault(fwf => fwf.Name == FlowFieldConstants.Name_BehandelendArtsID);
                var behandelendArtsIDRequestForm = FlowFieldValueBL.GetByTransferIDAndFieldID(uow, globalProps.TransferID, FlowFieldConstants.ID_BehandelendArtsID)
                    .FirstOrDefault(it => requestformids.Contains(it.FormSetVersion.FormTypeID));
                if (behandelendArtsID != null && behandelendArtsIDRequestForm != null)
                {
                    FlowWebFieldBL.SetValue(behandelendArtsID, behandelendArtsIDRequestForm.Value, Source.POINT_PREFILL);
                }

                var behandelaarNaamprefill = viewModel.Global.FlowWebFields.FirstOrDefault(fwf => fwf.Name == FlowFieldConstants.Name_BehandelaarNaam);
                var behandelaarNaamRequestForm = FlowFieldValueBL.GetByTransferIDAndFieldID(uow, globalProps.TransferID, FlowFieldConstants.ID_BehandelaarNaam)
                    .FirstOrDefault(it => requestformids.Contains(it.FormSetVersion.FormTypeID));
                if (behandelaarNaamprefill != null && behandelaarNaamRequestForm != null)
                {
                    behandelaarNaamprefill.DisplayValue = behandelaarNaamRequestForm.Value;
                    FlowWebFieldBL.SetSource(behandelaarNaamprefill, Source.POINT_PREFILL);
                }

                var behandelaarAGBprefill = viewModel.Global.FlowWebFields.FirstOrDefault(fwv => fwv.Name == FlowFieldConstants.Name_BehandelaarAGB);
                var behandelaarAGBRequestForm = FlowFieldValueBL.GetByTransferIDAndFieldID(uow, globalProps.TransferID, FlowFieldConstants.ID_BehandelaarAGB)
                    .FirstOrDefault(it => requestformids.Contains(it.FormSetVersion.FormTypeID));
                if (behandelaarAGBprefill != null && behandelaarAGBRequestForm != null)
                {
                    behandelaarAGBprefill.DisplayValue = behandelaarAGBRequestForm.Value;
                    FlowWebFieldBL.SetSource(behandelaarAGBprefill, Source.POINT_PREFILL);
                }

                var behandelaarBIGprefill = viewModel.Global.FlowWebFields.FirstOrDefault(fwv => fwv.Name == FlowFieldConstants.Name_BehandelaarBIG);
                var behandelaarBIGRequestForm = FlowFieldValueBL.GetByTransferIDAndFieldID(uow, globalProps.TransferID, FlowFieldConstants.ID_BehandelaarBIG)
                    .FirstOrDefault(it => requestformids.Contains(it.FormSetVersion.FormTypeID));
                if (behandelaarBIGprefill != null && behandelaarBIGRequestForm != null)
                {
                    behandelaarBIGprefill.DisplayValue = behandelaarBIGRequestForm.Value;
                    FlowWebFieldBL.SetSource(behandelaarBIGprefill, Source.POINT_PREFILL);
                }

                var behandelaarSpecialismeprefill = viewModel.Global.FlowWebFields.FirstOrDefault(fwv => fwv.Name == FlowFieldConstants.Name_BehandelaarSpecialisme);
                var behandelaarSpecialismeRequestForm = FlowFieldValueBL.GetByTransferIDAndFieldID(uow, globalProps.TransferID, FlowFieldConstants.ID_BehandelaarSpecialisme)
                    .FirstOrDefault(it => requestformids.Contains(it.FormSetVersion.FormTypeID));
                if (behandelaarSpecialismeprefill != null && behandelaarSpecialismeRequestForm != null)
                {
                    FlowWebFieldBL.SetValue(behandelaarSpecialismeprefill, behandelaarSpecialismeRequestForm.Value, Source.POINT_PREFILL);
                }

                var behandelaarTelefoonPieperprefill = viewModel.Global.FlowWebFields.FirstOrDefault(fwv => fwv.Name == FlowFieldConstants.Name_BehandelaarTelefoonPieper);
                var behandelaarTelefoonPieperRequestForm = FlowFieldValueBL.GetByTransferIDAndFieldID(uow, globalProps.TransferID, FlowFieldConstants.ID_BehandelaarTelefoonPieper)
                    .FirstOrDefault(it => requestformids.Contains(it.FormSetVersion.FormTypeID));
                if (behandelaarTelefoonPieperprefill != null && behandelaarTelefoonPieperRequestForm != null)
                {
                    behandelaarTelefoonPieperprefill.DisplayValue = behandelaarTelefoonPieperRequestForm.Value;
                    FlowWebFieldBL.SetSource(behandelaarTelefoonPieperprefill, Source.POINT_PREFILL);
                }
            }

            viewModel.LoggedInEmployeeName = viewModel.Global.PointUserInfo.Employee.FullName();
            viewModel.LoggedInEmployeeFunction = viewModel.Global.PointUserInfo.Employee.Position;

            if (!string.IsNullOrWhiteSpace(viewModel.Global.PointUserInfo.Employee.BIG))
            {
                viewModel.LoggedInEmployeeName += " (BIG: " + viewModel.Global.PointUserInfo.Employee.BIG + ")";
            }

            var doctorNameAndAGB = "";
            var behandelaarNaam = viewModel.Global.FlowWebFields.FirstOrDefault(fwv => fwv.Name == FlowFieldConstants.Name_BehandelaarNaam);
            if (!string.IsNullOrWhiteSpace(behandelaarNaam?.Value))
            {
                doctorNameAndAGB = behandelaarNaam.Value;
            }
            var behandelaarAGB = viewModel.Global.FlowWebFields.FirstOrDefault(fwv => fwv.Name == FlowFieldConstants.Name_BehandelaarAGB);
            if (!string.IsNullOrWhiteSpace(behandelaarAGB?.Value))
            {
                doctorNameAndAGB = string.IsNullOrWhiteSpace(doctorNameAndAGB) ? behandelaarAGB.Value : $"{doctorNameAndAGB} {behandelaarAGB.Value}";
            }

            viewModel.GenoemdeArtsMedischSpecialist = doctorNameAndAGB;

            if (phasedefinition == null)
            {
                return viewModel;
            }

            bool displaylinktofrequency;
            var formsetversion = FormSetVersionBL.GetByTransferIDAndFormTypeID(uow, globalProps.TransferID, phasedefinition.FormTypeID).OrderByDescending(fsv => fsv.UpdateDate).FirstOrDefault();

            if (viewModel.Rights == Rights.R || viewModel.Rights == Rights.None)
            {
                displaylinktofrequency = formsetversion != null;
            }
            else
            {
                displaylinktofrequency = true;
            }

            if (!displaylinktofrequency)
            {
                return viewModel;
            }

            if (formsetversion != null)
            {
                viewModel.FrequencyUrl = controller.Url.Action(phasedefinition.GetActionName(), phasedefinition.GetControllerName(), new
                {
                    globalProps.TransferID,
                    globalProps.FlowInstance.FlowDefinitionID,
                    formsetversion.FormSetVersionID,
                    phasedefinition.PhaseDefinitionID,
                    PhaseInstanceID = formsetversion.PhaseInstanceID.Value,
                }, Utils.UriScheme());
            }
            else
            {
                viewModel.FrequencyUrl = controller.Url.Action(phasedefinition.GetActionName(), phasedefinition.GetControllerName(), new
                {
                    globalProps.TransferID,
                    globalProps.FlowInstance.FlowDefinitionID,
                    FormSetVersionID = -1,
                    phasedefinition.PhaseDefinitionID,
                    PhaseInstanceID = -1,
                    ParentFormSetVersionID = globalProps.FormSetVersionID
                }, Utils.UriScheme());
            }

            return viewModel;
        }

        private static MsvtViewModel CreateMsvtViewModel(IUnitOfWork uow, GlobalProperties globalProps, FlowFormType flowFormType, PhaseDefinitionCache phaseDefinition, bool showHistory = false, bool fillSignedPhaseInstance = false)
        {
            if (phaseDefinition == null)
            {
                throw new Exception("Geen phasedefinition");
            }

            var rights = PhaseDefinitionRightsBL.GetFormRightsPhaseDefinitionID(uow, globalProps.PointUserInfo, phaseDefinition, globalProps.FlowInstance);

            var viewModel = new MsvtViewModel
            {
                ClientID = globalProps.Transfer?.ClientID,
                Rights = rights,
                HideNewButtons = (rights == Rights.R || rights == Rights.None),
                ReceivingOrganization = "",
                SendingOrganization = ""
            };
            viewModel.SetGlobalProperties(globalProps);

            List<MutActionHealthInsurer> msvtactionhistory = null;

            var formtype = FormTypeBL.GetByURL(uow, globalProps.ControllerName, globalProps.ActionName);
            viewModel.FormSetVersions = FormSetVersionBL.GetByTransferIDAndFormTypeID(uow, globalProps.TransferID, formtype.FormTypeID);

            var actions = ActionCodeHealthInsurerBL.GetByRegionIDAndFormTypeID(uow, globalProps.PointUserInfo.Region.RegionID, (int)flowFormType);
            List<ActionHealthInsurer> actionvalues = null;
            List<MedicineCard> medicinevalues = null;
            if (globalProps.HasFormSetVersion)
            {
                actionvalues = ActionHealthInsurerBL.GetByFormSetVersionID(uow, globalProps.FormSetVersionID).ToList();
                medicinevalues = MedicineCardBL.GetByFormSetVersionID(uow, globalProps.FormSetVersionID).ToList();

                if (showHistory)
                {
                    msvtactionhistory = MutActionHealthInsurerBL.GetByFormSetVersionID(uow, globalProps.FormSetVersionID).ToList().Where(h => globalProps.HistoryFromDate == null || h.TimeStamp >= globalProps.HistoryFromDate).ToList();
                }
            }

            viewModel.TreatmentItems = MsvtTreatmentItemViewModel.GetByModelList(actions, actionvalues, history: msvtactionhistory);
            viewModel.MedicationItems = MedicationItemViewModel.GetByModelList(medicinevalues);

            var MSVTLeverendeOrganisatie = globalProps.FlowWebFields.FirstOrDefault(fwv => fwv.Name == FlowFieldConstants.Name_MSVTLeverendeOrganisatie);
            if (!string.IsNullOrEmpty(MSVTLeverendeOrganisatie?.Value))
            {
                if (int.TryParse(MSVTLeverendeOrganisatie.Value, out int msvtleverendeorganisatie))
                {
                    viewModel.VVTDepartmentIDMSVT = msvtleverendeorganisatie;
                }
            }

            var patientaccepted = PatientHelper.IsAccepted(uow, globalProps.FlowInstance.FlowInstanceID);
            if (patientaccepted ==  AcceptanceState.Acknowledged)
            {
                var receivingdepartment = OrganizationHelper.GetRecievingDepartment(uow, globalProps.FlowInstance.FlowInstanceID);
                var receivingemployee = OrganizationHelper.GetRecievingEmployee(uow, globalProps.FlowInstance.FlowInstanceID);

                if (receivingdepartment != null)
                {
                    viewModel.VVTDepartmentID = receivingdepartment.DepartmentID;
                    viewModel.ReceivingOrganization = receivingdepartment.FullName();
                }

                if (MSVTLeverendeOrganisatie != null && receivingdepartment != null && string.IsNullOrEmpty(MSVTLeverendeOrganisatie.Value))
                {
                    FlowWebFieldBL.SetValue(MSVTLeverendeOrganisatie, receivingdepartment.DepartmentID, Source.POINT_PREFILL);
                }

                if (receivingemployee != null)
                {
                    var MSVTLeverendeMedewerker = globalProps.FlowWebFields.FirstOrDefault(fwv => fwv.Name == FlowFieldConstants.Name_MSVTLeverendeMedewerker);
                    if (MSVTLeverendeMedewerker != null)
                    {
                        MSVTLeverendeMedewerker.DisplayValue = receivingemployee.FullName();
                        FlowWebFieldBL.SetSource(MSVTLeverendeMedewerker, Source.POINT_PREFILL);
                    }

                    var MSVTLeverendeMedewerkerBIG = globalProps.FlowWebFields.FirstOrDefault(fwv => fwv.Name == FlowFieldConstants.Name_MSVTLeverendeMedewerkerBIG);
                    if (MSVTLeverendeMedewerkerBIG != null)
                    {
                        MSVTLeverendeMedewerkerBIG.DisplayValue = receivingemployee.BIG;
                        FlowWebFieldBL.SetSource(MSVTLeverendeMedewerkerBIG, Source.POINT_PREFILL);
                    }

                    var MSVTLeverendeMedewerkerTelefoon = globalProps.FlowWebFields.FirstOrDefault(fwv => fwv.Name == FlowFieldConstants.Name_MSVTLeverendeMedewerkerTelefoon);
                    if (MSVTLeverendeMedewerkerTelefoon != null)
                    {
                        MSVTLeverendeMedewerkerTelefoon.DisplayValue = receivingemployee.PhoneNumber;
                        FlowWebFieldBL.SetSource(MSVTLeverendeMedewerkerTelefoon, Source.POINT_PREFILL);
                    }

                    var MSVTLeverendeMedewerkerEmail = globalProps.FlowWebFields.FirstOrDefault(fwv => fwv.Name == FlowFieldConstants.Name_MSVTLeverendeMedewerkerEmail);
                    if (MSVTLeverendeMedewerkerEmail != null)
                    {
                        MSVTLeverendeMedewerkerEmail.DisplayValue = receivingemployee.EmailAddress;
                        FlowWebFieldBL.SetSource(MSVTLeverendeMedewerkerEmail, Source.POINT_PREFILL);
                    }
                }
            }

            var sendingdep = globalProps.FlowInstance.StartedByDepartment;
            if (sendingdep?.Location?.Organization != null)
            {
                string agb = sendingdep.Location.Organization.AGB;
                if (!string.IsNullOrEmpty(sendingdep.AGB))
                {
                    agb = sendingdep.AGB;
                }
                viewModel.SendingOrganization = $"{sendingdep.Location.FullName()} {agb}";
            }

            var requestFormZHVVTType = FlowFieldValueBL.GetByFlowInstanceIDAndFieldID(uow, globalProps.FlowInstance.FlowInstanceID, FlowFieldConstants.ID_RequestFormZHVVTType).FirstOrDefault();
            if (requestFormZHVVTType != null && requestFormZHVVTType.Value == ((int)FlowFormType.RequestFormZHVVT_FormulierPoli).ToString())
            {
                globalProps.FlowWebFields.Where(it => it.Name == FlowFieldConstants.Name_MedischeSituatieDatumOpname).ToList().ForEach(it => it.ReadOnly = true);
            }

            if (!fillSignedPhaseInstance)
            {
                return viewModel;
            }
            var signedPhaseInstance = SignedPhaseInstanceHelper.GetSignedPhaseInstanceViewModel(uow, globalProps.FormSetVersionID, globalProps.PhaseInstanceID);
            if (!signedPhaseInstance.HasSignedPhaseInstance)
            {
                return viewModel;
            }
            viewModel.SignedPhaseInstanceDate = signedPhaseInstance.Date.ToPointDateHourDisplay();
            viewModel.SignedByEmployeeFunction = signedPhaseInstance.SignedByEmployeeFunction;
            viewModel.SignedByEmployeeName = signedPhaseInstance.SignedByEmployeeName;
            viewModel.SignedPhaseInstancePhaseInstanceID = signedPhaseInstance.PhaseInstanceID;

            return viewModel;
        }

        private static void GenerateGVpInfoLine(IUnitOfWork uow, int flowInstanceID, StringBuilder sb, string titel, int checkedid, int perdagid, int perweekid, int infoid)
        {
            var values = FlowFieldValueBL.GetByFlowInstanceIDAndFieldIDs(uow, flowInstanceID, new int[] { checkedid, perdagid, perweekid, infoid }).ToList();
            if (!values.Any(it => !String.IsNullOrEmpty(it.Value)))
            {
                return;
            }
            var actief = values.GetValueOrDefault(checkedid, "").ToLower();
            var perdag = values.GetValueOrDefault(perdagid, "");
            var perweek = values.GetValueOrDefault(perweekid, "");
            var info = values.GetValueOrDefault(infoid, "");

            if (!string.IsNullOrEmpty(perdag))
            {
                sb.AppendLine($"• {titel}: {perdag}x per dag {info}<br/>".Replace("  ", " "));
            }
            else if (!string.IsNullOrEmpty(perweek))
            {
                sb.AppendLine($"• {titel}: {perweek}x per week {info}<br/>".Replace("  ", " "));
            }
            else if (actief == "true")
            {
                sb.AppendLine($"• {titel}: {info}<br/>".Replace("  ", " "));
            }
        }

        internal static void PrefillDoctor(IUnitOfWork uow, List<FlowWebField> flowWebFields, int transferID)
        {
            var requestFormIds = new[] { (int)FlowFormType.AanvraagFormulierZHVVT };

            foreach (var fwfid in new[] { FlowFieldConstants.ID_BehandelendArtsID, FlowFieldConstants.ID_BehandelaarNaam, FlowFieldConstants.ID_BehandelaarAGB, FlowFieldConstants.ID_BehandelaarBIG, FlowFieldConstants.ID_BehandelaarSpecialisme, FlowFieldConstants.ID_BehandelaarTelefoonPieper })
            {
                var flowWebField = flowWebFields.FirstOrDefault(fwf => fwf.FlowFieldID == fwfid);
                var flowFieldValueField = FlowFieldValueBL.GetByTransferIDAndFieldID(uow, transferID, fwfid).FirstOrDefault(it => requestFormIds.Contains(it.FormSetVersion.FormTypeID));
                if (flowWebField == null || flowFieldValueField == null)
                {
                    continue;
                }

                FlowWebFieldBL.SetValue(flowWebField, flowFieldValueField.Value, Source.POINT_PREFILL);
            }
        }

        public static FormTypeBaseViewModel GetGRZFormViewModel(IUnitOfWork uow, GlobalProperties globalProps)
        {
            var transfer = globalProps.Transfer;
            var viewModel = new FormTypeBaseViewModel
            {
                FlowDefinitionID = globalProps.FlowInstance.FlowDefinitionID,
                ClientID = transfer?.ClientID
            };
            viewModel.SetGlobalProperties(globalProps);

            if (globalProps.HasFormSetVersion || globalProps.IsReadMode || transfer == null)
            {
                return viewModel;
            }

            var datumIngevuld = viewModel.Global.FlowWebFields.FirstOrDefault(fwf => fwf.Name == FlowFieldConstants.Name_DatumIngevuld);
            if (datumIngevuld != null)
            {
                FlowWebFieldBL.SetValue(datumIngevuld, DateTime.Now.ToPointDateTimeDatabase(), Source.POINT_PREFILL);
            }

            var burgerlijkeStaat = viewModel.Global.FlowWebFields.FirstOrDefault(fwf => fwf.Name == FlowFieldConstants.Name_BurgerlijkeStaat);
            if (burgerlijkeStaat != null && transfer.Client.CivilClass.HasValue)
            {
                FlowWebFieldBL.SetValue(burgerlijkeStaat, (int)transfer.Client.CivilClass, Source.POINT_PREFILL);
            }

            var compositionHousekeeping = viewModel.Global.FlowWebFields.FirstOrDefault(fwf => fwf.Name == FlowFieldConstants.Name_CompositionHousekeeping);
            if (compositionHousekeeping != null && transfer.Client.CompositionHousekeeping.HasValue)
            {
                FlowWebFieldBL.SetValue(compositionHousekeeping, (int)transfer.Client.CompositionHousekeeping, Source.POINT_PREFILL);
            }

            var kinderenAanwezigAantal = viewModel.Global.FlowWebFields.FirstOrDefault(fwf => fwf.Name == FlowFieldConstants.Name_KinderenAanwezigAantal);
            if (kinderenAanwezigAantal != null)
            {
                FlowWebFieldBL.SetValue(kinderenAanwezigAantal, transfer.Client.ChildrenInHousekeeping.GetValueOrDefault(0), Source.POINT_PREFILL);
            }

            var housingType = viewModel.Global.FlowWebFields.FirstOrDefault(fwf => fwf.Name == FlowFieldConstants.Name_HousingType);
            if (housingType != null && transfer.Client.HousingType.HasValue)
            {
                FlowWebFieldBL.SetValue(housingType, (int)transfer.Client.HousingType, Source.POINT_PREFILL);
            }

            return viewModel;
        }

        public static RealizedCareViewModel GetRealizedCareViewModel(IUnitOfWork uow, GlobalProperties globalProps)
        {
            var viewModel = new RealizedCareViewModel
            {
                FlowDefinitionID = globalProps.FlowInstance.FlowDefinitionID
            };
            viewModel.SetGlobalProperties(globalProps);

            var flowInstance = globalProps.FlowInstance;
            if (flowInstance == null)
            {
                return viewModel;
            }

            var receivingDepartment = OrganizationHelper.GetRecievingDepartment(uow, flowInstance.FlowInstanceID);
            if (receivingDepartment != null)
            {
                viewModel.ReceivingDepartmentID = receivingDepartment.DepartmentID.ToString();
                viewModel.ReceivingDepartmentName = receivingDepartment.FullName();
            }

            return viewModel;
        }

        public static FrequencyViewModel GetMsvtINDFrequencyViewModel(IUnitOfWork uow, GlobalProperties globalProps, int? parentFormSetVersionID, Controller controller)
        {
            var viewModel = FrequencyViewBL.FromType(uow, globalProps.TransferID, FrequencyType.MSVT);
            viewModel.SetGlobalProperties(globalProps);

            if (viewModel.FrequencyID <= 0)
            {
                var startDate = DateTime.Now;
                var endDate = startDate.AddMonths(3);

                if (parentFormSetVersionID.HasValue)
                {
                    var flowField = FlowFieldBL.GetByName(uow, FlowFieldConstants.Name_GeldigheidsDuurStartdatum);
                    if (flowField != null)
                    {
                        var flowFieldValue = FlowFieldValueBL.GetByTransferIDAndFormSetVersionIDAndFlowFieldID(uow, globalProps.TransferID, parentFormSetVersionID.Value, flowField.FlowFieldID);
                        if (!string.IsNullOrEmpty(flowFieldValue?.Value))
                        {
                            DateTime.TryParse(flowFieldValue.Value, out startDate);
                        }
                    }

                    flowField = FlowFieldBL.GetByName(uow, FlowFieldConstants.Name_GeldigheidsDuurEinddatum);
                    if (flowField != null)
                    {
                        var flowFieldValue = FlowFieldValueBL.GetByTransferIDAndFormSetVersionIDAndFlowFieldID(uow, globalProps.TransferID, parentFormSetVersionID.Value, flowField.FlowFieldID);
                        if (!string.IsNullOrEmpty(flowFieldValue?.Value))
                        {
                            DateTime.TryParse(flowFieldValue.Value, out endDate);
                        }
                    }
                }

                var phaseDefinition = PhaseDefinitionCacheBL.GetByFlowDefinitionAndFormType(uow, globalProps.FlowInstance.FlowDefinitionID, (int)FlowFormType.MSVTIndDoorldossier);

                viewModel.Name = phaseDefinition.PhaseName;
                viewModel.StartDate = startDate;
                viewModel.EndDate = endDate;
            }

            var formSetVersion = FormSetVersionBL.GetByTransferIDAndFormTypeID(uow, globalProps.TransferID, (int)FlowFormType.MSVTIndicatiestelling)
                    .OrderByDescending(fsv => fsv.UpdateDate)
                    .FirstOrDefault();
            if (formSetVersion != null)
            {
                var phaseInstance = formSetVersion.PhaseInstance;
                var phaseDefinition = phaseInstance.PhaseDefinition;
                viewModel.IndicatiestellingFormName = formSetVersion.FormType.Name;
                viewModel.IndicatiestellingURL = controller.Url.Action(phaseDefinition.FormType.GetActionName(), phaseDefinition.FormType.GetControllerName(), new
                {
                    globalProps.TransferID,
                    phaseDefinition.FlowDefinitionID,
                    formSetVersion.FormSetVersionID,
                    phaseDefinition.PhaseDefinitionID,
                    phaseInstance.PhaseInstanceID
                }, Utils.UriScheme());
            }

            // ViewBag.FrequencyID = viewModel.FrequencyID;     ????????

            viewModel.ClientID = globalProps.Transfer?.ClientID;

            return viewModel;
        }

        public static SendToHospitalViewModel GetSendToHospitalViewModel(IUnitOfWork uow, GlobalProperties globalProps)
        {
            var viewModel = new SendToHospitalViewModel( );
            viewModel.SetGlobalProperties(globalProps);

            var flowInstance = globalProps.FlowInstance;
            if (flowInstance == null)
            {
                viewModel.ErrorMessage = "Geen FlowInstance";
                return viewModel;
            }
            globalProps.FlowInstance = flowInstance;

            viewModel.SenderOrganizationTypeID = OrganizationHelper.GetSendingOrganization(uow, flowInstance.FlowInstanceID).OrganizationTypeID;
            viewModel.FlowDefinitionID = flowInstance.FlowDefinitionID;

            var receiver = OrganizationHelper.GetRecievingDepartment(uow, flowInstance.FlowInstanceID);
            viewModel.ReceivingOrganization = receiver != null ? receiver.FullName() : "";

            var phaseInstance = globalProps.PhaseInstance;
            if (phaseInstance != null)
            {
                var nextPhaseDefinition = PhaseDefinitionBL.GetNextPhaseDefinitions(uow, phaseInstance.PhaseDefinitionID).FirstOrDefault();
                if (nextPhaseDefinition != null)
                {
                    viewModel.Historyinvites = OrganizationInviteBL.GetByFlowInstanceIDAndPhaseDefinitionID(uow, phaseInstance.FlowInstanceID, nextPhaseDefinition.PhaseDefinitionID);
                    var activeInvite = viewModel.Historyinvites.FirstOrDefault(it => it.InviteStatus == InviteStatus.Active);
                    if (activeInvite != null)
                    {
                        viewModel.LastSentToHospitalLocationID = activeInvite.Department.LocationID;
                        viewModel.LastSentToHospitalDepartmentID = activeInvite.DepartmentID;
                    }
                }
                viewModel.PhaseInstanceStatus = phaseInstance.Status;
            }

            if (viewModel.SelectDepartment)
            {
                var destinationHospitalDepartment = viewModel.Global.FlowWebFields.FirstOrDefault(it => it.Name == FlowFieldConstants.Name_DestinationHospitalDepartment);

                if (destinationHospitalDepartment != null)
                {
                    destinationHospitalDepartment.Required = true;
                }
            }

            if (viewModel.Prefill)
            {
                var pointUserInfo = globalProps.PointUserInfo;

                if (flowInstance.FlowDefinitionID == FlowDefinitionID.ZH_ZH)
                {
                    var transferDate = viewModel.Global.FlowWebFields.FirstOrDefault(fwf => fwf.Name == FlowFieldConstants.Name_TransferDate);
                    if (transferDate != null)
                    {
                        var datumEindeBehandelingMedischSpecialistOK = FlowWebFieldBL.FromTransferIDAndName(uow, globalProps.TransferID, FlowFieldConstants.Name_DatumEindeBehandelingMedischSpecialistOK, pointUserInfo, globalProps.ViewData);
                        if (datumEindeBehandelingMedischSpecialistOK != null)
                        {
                            FlowWebFieldBL.SetValue(transferDate, datumEindeBehandelingMedischSpecialistOK.Value, Source.POINT_PREFILL);
                        }
                    }
                }
                FlowWebFieldBL.SetValue(viewModel.Global.FlowWebFields, FlowFieldConstants.ID_ToestemmingPatientIngevuldDoor, pointUserInfo.Employee.EmployeeID, Source.POINT_PREFILL);
            }

            var department = OrganizationHelper.GetSendingDepartment(uow, flowInstance.FlowInstanceID);
            if (department?.Location?.OrganizationID != null)
            {
                FlowWebFieldBL.SetValue(viewModel.Global.FlowWebFields, FlowFieldConstants.ID_SourceOrganization, department.Location.OrganizationID);
                FlowWebFieldBL.SetValue(viewModel.Global.FlowWebFields, FlowFieldConstants.ID_SourceOrganizationLocation, department.LocationID);
                FlowWebFieldBL.SetValue(viewModel.Global.FlowWebFields, FlowFieldConstants.ID_SourceOrganizationDepartment, department.DepartmentID);
                FlowWebFieldBL.SetValue(viewModel.Global.FlowWebFields, FlowFieldConstants.ID_SourceHealthCareProvider, department.DepartmentID);
            }

            viewModel.HasDestinationHospitalDepartment = viewModel.Global.FlowWebFields.HasValue("DestinationHospitalDepartment");
            viewModel.HasDestinationHospitalLocation = viewModel.Global.FlowWebFields.HasValue("DestinationHospitalLocation");

            return viewModel;
        }

        public static RequestFormZHVVTViewModel GetRequestFormZHVVTViewModel(IUnitOfWork uow, GlobalProperties globalProps)
        {
            var viewModel = new RequestFormZHVVTViewModel();
            viewModel.SetGlobalProperties(globalProps);

            var pointUserInfo = globalProps.PointUserInfo;
            var client = ClientBL.GetByTransferID(uow, globalProps.TransferID);
            if (client != null)
            {
                viewModel.ClientID = client.ClientID;
            }

            var flowInstance = viewModel.Global.FlowInstance;

            var endpointconfiguration = EndpointConfigurationBL.Get(uow, pointUserInfo.Organization.OrganizationID, globalProps.FormTypeID);
            viewModel.HasEndpoint = endpointconfiguration != null;

            viewModel.ShowRelatedTransfers = FlowInstanceBL.GetRelatedTransfersWithVVTByBSN(uow, client.CivilServiceNumber, pointUserInfo.Organization.OrganizationID, globalProps.TransferID).Any();
            viewModel.ZorginzetVoorDezeOpnameTypeZorginstelling = viewModel.Global.FlowWebFields.FirstOrDefault(x => x.FlowFieldAttributeID == FlowFieldConstants.ID_ZorginzetVoorDezeOpnameTypeZorginstelling)?.Value;

            var relatedTransferTakeOver = flowInstance.RelatedTransferID.HasValue && flowInstance.RelatedTransferID.Value != (int)TakeOverRelatedTransferStatus.DoNotTakeOver &&
                                          !globalProps.HasFormSetVersion && !globalProps.IsPrintRequest && !globalProps.IsReadMode && flowInstance.FlowDefinitionID == FlowDefinitionID.VVT_VVT;
            if (relatedTransferTakeOver)
            {
                var relatedFlowInstance = FlowInstanceBL.GetByTransferID(uow, flowInstance.RelatedTransferID.Value);
                var destinationFormType = flowInstance.FlowDefinition.FlowDefinitionFormType.Any(m => m.FormTypeID == (int)FlowFormType.RequestFormZHVVT_VVTVVT);
                if (relatedFlowInstance != null && destinationFormType)
                {
                    viewModel.TakeRelatedTransfer = TakeOverFromRelatedTransfer(uow, relatedFlowInstance, viewModel, (int)FlowFormType.RequestFormZHVVT_VVTVVT);                    
                }
            }
            var sendingDepartment = OrganizationHelper.GetSendingDepartment(uow, flowInstance.FlowInstanceID);

            var requestFormZHVVTType = viewModel.Global.FlowWebFields.FirstOrDefault(fwf => fwf.FlowFieldID == FlowFieldConstants.ID_RequestFormZHVVTType);

            if (viewModel.Prefill)
            {
                var formTypes = FormTypeBL.GetRequestFormsByOrganizationIDAndFlowDefinitionIDs(uow, pointUserInfo.Organization.OrganizationID, new FlowDefinitionID[] { flowInstance.FlowDefinitionID });
                if (formTypes.Count == 1)
                {
                    viewModel.AutoSelectFormTypeID = formTypes.FirstOrDefault().FormTypeID;
                }

                if (sendingDepartment != null)
                {
                    var createdByDepartmentTelephoneNumber = viewModel.Global.FlowWebFields.FirstOrDefault(fwf => fwf.Name == FlowFieldConstants.Name_CreatedByDepartmentTelephoneNumber);
                    if (createdByDepartmentTelephoneNumber != null && !string.IsNullOrEmpty(sendingDepartment.PhoneNumber))
                    {
                        FlowWebFieldBL.SetValue(createdByDepartmentTelephoneNumber, sendingDepartment.PhoneNumber, Source.POINT_PREFILL);
                    }
                }

                FlowWebFieldBL.SetValue(viewModel.Global.FlowWebFields, FlowFieldConstants.ID_DossierOwner, pointUserInfo.Employee.EmployeeID, Source.POINT_PREFILL, globalProps.ViewData);
                if (flowInstance.FlowDefinitionID == FlowDefinitionID.CVA)
                {
                    FlowWebFieldBL.TakeoverValue(uow, pointUserInfo, globalProps.TransferID, FlowFormType.InvullenCVARegistratieZH, FlowFieldConstants.Name_PatientWordtOpgenomenDatum, FlowFieldConstants.Name_MedischeSituatieDatumOpname, viewModel.Global.FlowWebFields, globalProps.ViewData);
                }

                FieldReceivedTempBL.SetFlowWebFieldValues(uow, viewModel.Global.FlowWebFields, pointUserInfo.Organization, client.PatientNumber, client.VisitNumber);

                FlowWebFieldBL.SetSource(requestFormZHVVTType, Source.USER);
            }
            else
            {
                var phaseInstance = globalProps.PhaseInstance;
                if (phaseInstance != null)
                {
                    viewModel.Status = phaseInstance.Status;
                    if (phaseInstance.Status != (int)Status.Active)
                    {
                        FlowWebFieldBL.SetReadOnly(viewModel.Global.FlowWebFields.FirstOrDefault(fwf => fwf.Name == FlowFieldConstants.Name_DestinationHealthCareProvider), true);
                    }
                }
            }

            if (sendingDepartment?.Location != null)
            {
                FlowWebFieldBL.SetValue(viewModel.Global.FlowWebFields, FlowFieldConstants.ID_SourceOrganization, sendingDepartment.Location.OrganizationID, Source.POINT_PREFILL);
                FlowWebFieldBL.SetValue(viewModel.Global.FlowWebFields, FlowFieldConstants.ID_SourceOrganizationLocation, sendingDepartment.LocationID, Source.POINT_PREFILL);
                FlowWebFieldBL.SetValue(viewModel.Global.FlowWebFields, FlowFieldConstants.ID_SourceOrganizationDepartment, sendingDepartment.DepartmentID, Source.POINT_PREFILL);

                viewModel.DepartmentName = sendingDepartment.Name;
                viewModel.LocationName = sendingDepartment.Location.Name;
                              
                if (!globalProps.HasFormSetVersion && flowInstance.FlowDefinitionID == FlowDefinitionID.VVT_VVT)
                {
                    FlowWebFieldBL.SetValue(viewModel.Global.FlowWebFields, FlowFieldConstants.ID_PatientOntvingZorgVoorOpnameJaNee, "ja", Source.POINT_PREFILL);
                    FlowWebFieldBL.SetValue(viewModel.Global.FlowWebFields, FlowFieldConstants.ID_ZorginzetVoorDezeOpnameTypeZorginstellingNaam, sendingDepartment.DepartmentID, Source.POINT_PREFILL);
                }
            }

            if (globalProps.IsPrintRequest)
            {
                if (!string.IsNullOrWhiteSpace(requestFormZHVVTType?.Value) && int.TryParse(requestFormZHVVTType.Value, out var formTypeID))
                {
                    var formType = FormTypeBL.GetByFormTypeID(uow, formTypeID);
                    viewModel.FormTransferType = $"{formType?.Name} / {flowInstance.FlowDefinition.Name}";
                }
            }

            var destinationHealthCareProvider = FlowFieldValueBL.GetByFlowInstanceIDAndFieldID(uow, flowInstance.FlowInstanceID, FlowFieldConstants.ID_DestinationHealthCareProvider).ToList();
            viewModel.ShowResetVVT = destinationHealthCareProvider.Any(it => !string.IsNullOrEmpty(it.Value));

            viewModel.Sections = RequestFormBL.GetZHVVTSections(flowInstance.FlowDefinitionID);
            viewModel.SerializedSectionData = new JavaScriptSerializer().Serialize(viewModel.Sections);

            if (!string.IsNullOrEmpty(requestFormZHVVTType?.Value) && viewModel.Sections.ContainsKey(requestFormZHVVTType?.Value))
            {
                viewModel.CurrentSection = viewModel.Sections[requestFormZHVVTType.Value];
            }

            var nextPhaseDefinition = PhaseDefinitionBL.GetNextPhaseDefinitions(uow, globalProps.PhaseDefinitionID).FirstOrDefault();
            if (nextPhaseDefinition != null)
            {
                var nextPhaseInstance = PhaseInstanceBL.GetByFlowInstanceIDAndPhaseDefinition(uow, flowInstance.FlowInstanceID, nextPhaseDefinition.PhaseDefinitionID);
                viewModel.HasActiveNextPhase = nextPhaseInstance != null;
            }

            var dbDrivenValidations = ValidationBL.GetAllDeserialized(uow).ToList();
            FlowWebFieldMappers.MapToFlowWebFields(viewModel.Global.FlowWebFields, dbDrivenValidations);

            if (viewModel.Prefill && viewModel.HasEndpoint)
            {
                var endpointhelper = new EndpointConfigurationHelper(endpointconfiguration, client);
                viewModel.Global.FlowWebFields = endpointhelper.Merge(viewModel.Global.FlowWebFields, new List<Source>() { Source.USER }).ToList();
            }

            return viewModel;
        }

        public static bool TakeOverFromRelatedTransfer(IUnitOfWork uow, FlowInstance relatedFlowInstance, RequestFormZHVVTViewModel viewModel, int destinationFormTypeID)
        {
            var result = false;
            if (relatedFlowInstance == null)
            {
                return false;
            }

            var source = FormSetVersionBL.GetByTransferID(uow, relatedFlowInstance.TransferID)?.Where(fsv => fsv.FormTypeID == (int)FlowFormType.AanvraagFormulierZHVVT).FirstOrDefault();
            if (source == null)
            {
                return false;
            }

            var destinationFlowInstance = viewModel.Global.FlowInstance;

            var sourceFlowFieldValues = uow.FlowFieldValueRepository.Get(ffv => ffv.FlowInstance.TransferID == relatedFlowInstance.TransferID && ffv.FormSetVersionID == source.FormSetVersionID, asNoTracking: true);
            var destinationFlowFields = uow.FlowDefinitionFormTypeFlowFieldRepository.Get(fff => fff.TakeOver && fff.FormTypeID == destinationFormTypeID && fff.FlowDefinitionID == (int)destinationFlowInstance.FlowDefinitionID);
            foreach (var flowFieldValue in sourceFlowFieldValues)
            {
                if (destinationFlowFields.Any(fff => fff.FlowFieldID == flowFieldValue.FlowFieldID))
                {
                    if (!string.IsNullOrEmpty(flowFieldValue.Value))
                    {
                        FlowWebFieldBL.SetValue(viewModel.Global.FlowWebFields, flowFieldValue.FlowFieldID, flowFieldValue.Value, Source.POINT_PREFILL);
                        result = true;
                    }
                }
            }
            return result;
        }
        public static AdditionalInfoRequestTPViewModel GetAdditionalInfoRequestTpViewModel(IUnitOfWork uow, GlobalProperties globalProps)
        {
            var viewModel = new AdditionalInfoRequestTPViewModel();
            viewModel.SetGlobalProperties(globalProps);

            var client = ClientBL.GetByTransferID(uow, globalProps.TransferID);
            if (client == null)
            {
                return viewModel;
            }

            var oorspronkelijkeDatum = globalProps.FlowWebFields.FirstOrDefault(x => x.FlowFieldID == FlowFieldConstants.ID_OorspronkelijkeDatumEindeBehandelingMedischSpecialist);
            if (oorspronkelijkeDatum != null && string.IsNullOrEmpty(oorspronkelijkeDatum.Value))
            {
                var oorspronkelijkeDatumFromAanvraagFormulierZHVVT = FlowFieldValueBL.GetByTransferIDAndFormTypeIDAndFieldID(uow, globalProps.TransferID, (int) FlowFormType.AanvraagFormulierZHVVT, FlowFieldConstants.ID_OorspronkelijkeDatumEindeBehandelingMedischSpecialist);

                if (oorspronkelijkeDatumFromAanvraagFormulierZHVVT != null && !string.IsNullOrEmpty(oorspronkelijkeDatumFromAanvraagFormulierZHVVT.Value))
                {
                    FlowWebFieldBL.SetValue(globalProps.FlowWebFields, FlowFieldConstants.ID_OorspronkelijkeDatumEindeBehandelingMedischSpecialist, oorspronkelijkeDatumFromAanvraagFormulierZHVVT.Value);
                }
            }

            var pointUserInfo = globalProps.PointUserInfo;
            viewModel.ShowInformatieHuisartsenBericht = !string.IsNullOrWhiteSpace(client.AddressGPForZorgmail) &&
                                                        ((pointUserInfo.Organization?.MessageToGPByClose ?? false) || (pointUserInfo.Organization?.MessageToGPByDefinitive ?? false));

            return viewModel;
        }

        public static TransferRouteViewModel GetTransferRouteViewModel(IUnitOfWork uow, UrlHelper urlHelper, GlobalProperties globalProps)
        {
            var viewModel = new TransferRouteViewModel();
            viewModel.SetGlobalProperties(globalProps);

            var phaseInstance = PhaseInstanceBL.GetByTransferIDAndFormTypeID(uow, globalProps.TransferID, globalProps.FormTypeID);
            if (phaseInstance == null)
            {
                viewModel.ErrorMessage = "Geen phaseInstance";
                return viewModel;
            }

            var iiPhaseDefinition = PhaseDefinitionCacheBL.GetByFlowDefinitionIDAndFlowHandling(uow, phaseInstance.FlowInstance.FlowDefinitionID, FlowHandling.IndicatingCIZ);
            viewModel.PhaseDefinitionIIID = iiPhaseDefinition?.PhaseDefinitionID;

            var grzPhaseDefinition = PhaseDefinitionCacheBL.GetByFlowDefinitionIDAndFlowHandling(uow, phaseInstance.FlowInstance.FlowDefinitionID, FlowHandling.IndicatingGRZ);
            viewModel.PhaseDefinitionGRZID = grzPhaseDefinition?.PhaseDefinitionID;

            if(viewModel.PhaseDefinitionGRZID.HasValue)
            {
                var grztriagephaseinstance = PhaseInstanceBL.GetByFlowInstanceIDAndFormTypeID(uow, phaseInstance.FlowInstanceID, (int)FlowFormType.GRZFormulier);
                viewModel.GRZCanBeSent = grztriagephaseinstance?.Status == (int)Status.Done;
            }

            var healthCareProviders = OrganizationInviteBL.GetByFlowInstanceIDAndInviteType(uow, phaseInstance.FlowInstanceID, InviteType.Regular);
            var grzInvites = OrganizationInviteBL.GetByFlowInstanceIDAndInviteType(uow, phaseInstance.FlowInstanceID, InviteType.IndicatingGRZ);
            var cizInvites = OrganizationInviteBL.GetByFlowInstanceIDAndInviteType(uow, phaseInstance.FlowInstanceID, InviteType.IndicatingCIZ);

            var vvtPhaseDefinition = PhaseDefinitionCacheBL.GetByFlowDefinitionAndFormType(uow, phaseInstance.FlowInstance.FlowDefinitionID, (int)FlowFormType.InBehandelingVVTZHVVT);
            if (vvtPhaseDefinition != null)
            {
                viewModel.PhaseDefinitionZAID = vvtPhaseDefinition.PhaseDefinitionID;
            }

            var multiVVTPhaseDefinition = PhaseDefinitionCacheBL.GetByFlowDefinitionAndFormType(uow, phaseInstance.FlowInstance.FlowDefinitionID, (int)FlowFormType.VastleggenBesluitVVTZHVVT);
            if (multiVVTPhaseDefinition != null)
            {
                viewModel.PhaseDefinitionMultiZAID = multiVVTPhaseDefinition.PhaseDefinitionID;
            }

            viewModel.DateTimeIND = cizInvites.FirstOrDefault(it => it.InviteStatus == InviteStatus.Active)?.InviteSend.ToPointDayMonthTime();
            viewModel.DateTimeGRZ = grzInvites.FirstOrDefault(it => it.InviteStatus == InviteStatus.Active)?.InviteSend.ToPointDayMonthTime();

            viewModel.HealthCareProviders = healthCareProviders;
            viewModel.CIZOrganizations = cizInvites;
            viewModel.GRZOrganizations = grzInvites;

            viewModel.FlowInstanceID = phaseInstance.FlowInstanceID;
            viewModel.FlowDefinitionID = phaseInstance.FlowInstance.FlowDefinitionID;
            globalProps.PhaseDefinitionID = phaseInstance.PhaseDefinitionID;
            globalProps.PhaseInstanceID = phaseInstance.PhaseInstanceID;

            var destinationHealthCareProvider = globalProps.FlowWebFields.GetValueOrDefault(FlowFieldConstants.Name_DestinationHealthCareProvider, "");
            if (int.TryParse(destinationHealthCareProvider, out int destinationHealthCareProviderID))
            {
                var destinationHealthCareProviderDepartment = DepartmentBL.GetByDepartmentID(uow, destinationHealthCareProviderID);
                if (destinationHealthCareProviderDepartment != null)
                {
                    var participation = FlowDefinitionParticipationBL.GetByLocationIDAndDepartmentIDAndFlowDefinitionID(uow, destinationHealthCareProviderDepartment.LocationID, destinationHealthCareProviderDepartment.DepartmentID, globalProps.FlowInstance.FlowDefinitionID);
                    viewModel.ShowPointParticipatorWarning = ( participation?.Participation != Participation.Active && participation?.Participation != Participation.ActiveButInvisibleInSearch) ;
                }
            }

            if (viewModel.Prefill)
            {
                var pointUserInfo = globalProps.PointUserInfo;

                switch (phaseInstance.FlowInstance.FlowDefinitionID)
                {
                    case FlowDefinitionID.ZH_VVT:
                    case FlowDefinitionID.VVT_VVT:
                        FlowWebFieldBL.TakeoverValue(uow, pointUserInfo, globalProps.TransferID,
                            FlowFormType.AanvraagFormulierZHVVT, FlowFieldConstants.Name_ZorgInZorginstellingVoorkeurPatient1,
                            FlowFieldConstants.Name_ZorgInZorginstellingVoorkeurPatient1, globalProps.FlowWebFields, viewData: globalProps.ViewData);
                        break;
                    case FlowDefinitionID.CVA:
                        FlowWebFieldBL.TakeoverValue(uow, pointUserInfo, globalProps.TransferID,
                            FlowFormType.CVARouteSelection, FlowFieldConstants.Name_ZorgInZorginstellingVoorkeur1,
                            FlowFieldConstants.Name_SelectedHealthCareProvider, globalProps.FlowWebFields, viewData: globalProps.ViewData);

                        FlowWebFieldBL.TakeoverValue(uow, pointUserInfo, globalProps.TransferID,
                            FlowFormType.CVARouteSelection, FlowFieldConstants.Name_ZorgInZorginstellingVoorkeurPatient1,
                            FlowFieldConstants.Name_ZorgInZorginstellingVoorkeurPatient1, globalProps.FlowWebFields, globalProps.ViewData, FlowFormType.AanvraagFormulierZHVVT);
                        break;
                }
            }

            var zorgInZorginstellingVoorkeurPatient1 = globalProps.FlowWebFields.FirstOrDefault(fwf => fwf.Name == FlowFieldConstants.Name_ZorgInZorginstellingVoorkeurPatient1);
            if (!string.IsNullOrWhiteSpace(zorgInZorginstellingVoorkeurPatient1?.Value))
            {
                viewModel.HasGewensteVVT = true;
                if (int.TryParse(zorgInZorginstellingVoorkeurPatient1.Value, out var departmentID))
                {
                    var department = DepartmentBL.GetByDepartmentID(uow, departmentID);
                    if (department != null)
                    {
                        var participation = FlowDefinitionParticipationBL.GetByLocationIDAndDepartmentIDAndFlowDefinitionID(uow, department.LocationID, department.DepartmentID, globalProps.FlowInstance.FlowDefinitionID);
                        viewModel.VVTPreferenceParticipation = (int)(participation?.Participation ?? Participation.None);
                    }
                }
            }

            var flowFormType = FlowFormType.NoScreen;
            var requestFormZHVVTType = FlowFieldValueBL.GetByTransferIDAndFlowFieldID(uow, globalProps.TransferID, FlowFieldConstants.ID_RequestFormZHVVTType);
            if (requestFormZHVVTType != null)
            {
                Enum.TryParse<FlowFormType>(requestFormZHVVTType.Value, out flowFormType);
            }
            viewModel.RequestFormZHVVTType = flowFormType;
            viewModel.ShowResetVVT = viewModel.Global.FlowWebFields.HasValue("DestinationHealthCareProvider");
            viewModel.IsVVTSelected = globalProps.FlowWebFields.HasValue("SelectedHealthCareProvider");
            viewModel.ShowButtonCancelVVT = globalProps.FlowWebFields.HasValue("DestinationHealthCareProvider") && !globalProps.IsReadMode;
            viewModel.IsQueuedSelected = globalProps.FlowWebFields.HasValue("QueuedHealthCareProvider");

            viewModel.TransferRouteInvitesViewModel = GetTransferRouteInvitesViewModel(uow, urlHelper, viewModel.Global.TransferID, viewModel.FlowInstanceID, viewModel.FlowDefinitionID);

            return viewModel;
        }

        public static TransferRouteViewModel GetTransferRouteHAVVTViewModel(IUnitOfWork uow, GlobalProperties globalProps)
        {
            var viewModel = new TransferRouteViewModel();
            viewModel.SetGlobalProperties(globalProps);

            var vvtPhaseDefinition = PhaseDefinitionCacheBL.GetByFlowDefinitionAndFormType(uow, globalProps.FlowInstance.FlowDefinitionID, (int)FlowFormType.InBehandelingVVTZHVVT);
            var healthcareProviders = OrganizationInviteBL.GetByFlowInstanceIDAndPhaseDefinitionID(uow, globalProps.FlowInstance.FlowInstanceID, vvtPhaseDefinition.PhaseDefinitionID);

            viewModel.HealthCareProviders = healthcareProviders;
            viewModel.FlowInstanceID = globalProps.FlowInstance.FlowInstanceID;
            viewModel.PhaseDefinitionZAID = vvtPhaseDefinition?.PhaseDefinitionID;

            var transferRoutePhaseDefinition = PhaseDefinitionCacheBL.GetByFlowDefinitionAndFormType(uow, globalProps.FlowInstance.FlowDefinitionID, (int)FlowFormType.TransferRouteHAVVT);
            var transferRouteInvite = OrganizationInviteBL.GetAcceptedOrActiveInvitesByFlowInstanceIDAndPhaseDefinitionID(uow, globalProps.FlowInstance.FlowInstanceID, transferRoutePhaseDefinition?.PhaseDefinitionID ?? 0).FirstOrDefault();

            //Laat melding zien als afhandelijk dient te gebeuren door RegionaalCoordinatiepunt
            viewModel.ShowTussenorgWarning = globalProps.PointUserInfo?.Organization?.OrganizationID != transferRouteInvite?.OrganizationID &&
                                           transferRouteInvite?.Organization?.OrganizationTypeID == (int)OrganizationTypeID.RegionaalCoordinatiepunt;

            return viewModel;
        }

        public static TransferRouteViewModel GetTransferRouteRzTPViewModel(IUnitOfWork uow, GlobalProperties globalProps)
        {
            var viewModel = new TransferRouteViewModel();
            viewModel.SetGlobalProperties(globalProps);

            var phaseInstance = globalProps.PhaseInstance;
            if (phaseInstance == null)
            {
                viewModel.ErrorMessage = "Geen phaseInstance";
                return viewModel;
            }

            var nextPhaseDefinitions = PhaseDefinitionBL.GetNextPhaseDefinitions(uow, phaseInstance.PhaseDefinitionID);
            var nextPhaseDefinition = nextPhaseDefinitions.FirstOrDefault();
            if (nextPhaseDefinition != null)
            {
                viewModel.PhaseDefinitionZAID = nextPhaseDefinition.PhaseDefinitionID;
                viewModel.HealthCareProviders = OrganizationInviteBL.GetByFlowInstanceIDAndInviteType(uow, phaseInstance.FlowInstanceID, InviteType.Regular);
            }

            viewModel.FlowInstanceID = phaseInstance.FlowInstanceID;
            viewModel.FlowDefinitionID = phaseInstance.FlowInstance.FlowDefinitionID;
            globalProps.PhaseDefinitionID = phaseInstance.PhaseDefinitionID;
            globalProps.PhaseInstanceID = phaseInstance.PhaseInstanceID;
            globalProps.FormTypeID = (int)FlowFormType.RouterenDossierRzTP;

            var selectedHealthCareProviderValue = globalProps.FlowWebFields.GetValueOrDefault(FlowFieldConstants.Name_SelectedHealthCareProvider, "");
            if (!int.TryParse(selectedHealthCareProviderValue, out int selectedHealthCareProviderID))
            {
                return viewModel;
            }

            var selectedHealthCareProvider = DepartmentBL.GetByDepartmentID(uow, selectedHealthCareProviderID);
            if (selectedHealthCareProvider == null)
            {
                return viewModel;
            }

            var participation = FlowDefinitionParticipationBL.GetByLocationIDAndDepartmentIDAndFlowDefinitionID(uow, selectedHealthCareProvider.LocationID, selectedHealthCareProvider.DepartmentID, globalProps.FlowInstance.FlowDefinitionID);
            viewModel.ShowPointParticipatorWarning = (participation?.Participation != Participation.Active && participation?.Participation != Participation.ActiveButInvisibleInSearch);

            return viewModel;
        }

        public static MoveToDepartmentViewModel GetMoveToDepartmentViewModel(IUnitOfWork uow, GlobalProperties globalProps)
        {
            var viewModel = new MoveToDepartmentViewModel();
            viewModel.SetGlobalProperties(globalProps);

            var destinationHospitalLocation = FlowWebFieldBL.FromTransferIDAndName(uow, globalProps.TransferID, FlowFieldConstants.Name_DestinationHospitalLocation, globalProps.PointUserInfo, globalProps.ViewData);
            if (destinationHospitalLocation != null)
            {
                if (int.TryParse(destinationHospitalLocation.Value, out var locationID))
                {
                    viewModel.CurrentLocationID = locationID;

                    if (viewModel.Prefill)
                    {
                        var department = DepartmentBL.GetTransferPointByLocation(uow, locationID);
                        if (department != null)
                        {
                            var destinationHospitalDepartment = viewModel.Global.FlowWebFields.FirstOrDefault(fwf => fwf.Name == FlowFieldConstants.Name_DestinationHospitalDepartment);
                            if (destinationHospitalDepartment != null)
                            {
                                FlowWebFieldBL.SetValue(destinationHospitalDepartment, department.DepartmentID, Source.POINT_PREFILL);
                            }
                        }
                    }
                }
            }

            if (globalProps.FlowInstance != null)
            {
                viewModel.FlowDefinitionID = (FlowDefinitionID) globalProps.FlowInstance?.FlowDefinitionID;
            }

            return viewModel;
        }

        public static FormTypeBaseViewModel GetAcceptAtHospitalViewModel(IUnitOfWork uow, GlobalProperties globalProps)
        {
            var viewModel = new FormTypeBaseViewModel();
            viewModel.SetGlobalProperties(globalProps);

            if (!viewModel.Prefill)
            {
                return viewModel;
            }

            var acceptedBy = globalProps.FlowWebFields.FirstOrDefault(fwf => fwf.Name == FlowFieldConstants.Name_AcceptedBy);
            if (acceptedBy != null)
            {
                FlowWebFieldBL.SetValue(acceptedBy, globalProps.PointUserInfo.Employee.EmployeeID, Source.POINT_PREFILL, globalProps.ViewData);
            }

            return viewModel;
        }

        public static SignatureViewModel GetSignatureViewModel(IUnitOfWork uow, GlobalProperties globalProps)
        {
            var viewModel = new SignatureViewModel();
            viewModel.SetGlobalProperties(globalProps);

            var transfer = globalProps.Transfer;
            var flowInstance = globalProps.FlowInstance;

            viewModel.FullName = flowInstance.Transfer.Client.FullName(flowInstance, globalProps.PointUserInfo?.Organization?.OrganizationID);
            // TODO: Why check on client through Transfer and choose it from FlowInstance?
            if (transfer.Client.BirthDate != null)
            {
                viewModel.BirthDate = flowInstance.Transfer.Client.BirthDate.HasValue ? transfer.Client.BirthDate.Value.ToString("dd-MM-yyyy") : "";
            }

            viewModel.Gender = flowInstance.Transfer.Client.Gender;
            viewModel.Location = OrganizationHelper.GetSendingLocation(uow, flowInstance.FlowInstanceID);
            viewModel.Department = OrganizationHelper.GetSendingDepartment(uow, flowInstance.FlowInstanceID);
            viewModel.MdwTP = OrganizationHelper.GetTransferPointEmployee(uow, flowInstance.FlowInstanceID);
            viewModel.MdwCIZ = OrganizationHelper.GetCIZEmployee(uow, globalProps.TransferID);
            viewModel.MdwVVT = OrganizationHelper.GetVVTEmployee(uow, globalProps.TransferID);

            var redenGeenToestemming = FlowFieldValueBL.GetByTransferIDAndFieldID(uow, globalProps.TransferID, FlowFieldConstants.ID_GeenToestemmingPatientVanReden).FirstOrDefault();
            viewModel.GeenToestemmingPatientVanReden = redenGeenToestemming?.Value ?? "";

            var kamerNummer = FlowFieldValueBL.GetByTransferIDAndFieldID(uow, globalProps.TransferID, FlowFieldConstants.ID_Kamernummer).FirstOrDefault();
            viewModel.KamerNummer = kamerNummer?.Value ?? "";

            return viewModel;
        }

        public static FormTypeBaseViewModel GetEvaluateDecisionViewModel(IUnitOfWork uow, GlobalProperties globalProps)
        {
            var viewModel = new FormTypeBaseViewModel();
            viewModel.SetGlobalProperties(globalProps);
            return viewModel;
        }

        public static TransferPatientZHVVTViewModel GetTransferPatientZHVVTViewModel(IUnitOfWork uow, GlobalProperties globalProps)
        {
            var viewModel = new TransferPatientZHVVTViewModel();
            viewModel.SetGlobalProperties(globalProps);

            var phaseDefinition = PhaseDefinitionCacheBL.GetByID(uow, globalProps.PhaseDefinitionID);
            if (phaseDefinition != null)
            {
                var nextPhaseDefinitions = PhaseDefinitionBL.GetNextPhaseDefinitions(uow, globalProps.PhaseDefinitionID);
                if (nextPhaseDefinitions != null)
                {
                    viewModel.NextPhaseJa = nextPhaseDefinitions.FirstOrDefault(pd => pd.Phase > phaseDefinition.Phase && pd.FlowHandling != FlowHandling.ReceiveEvaluateDecision)?.PhaseDefinitionID;
                    viewModel.NextPhaseJaMaar = nextPhaseDefinitions.FirstOrDefault(pd => pd.Phase > phaseDefinition.Phase && pd.FlowHandling == FlowHandling.ReceiveEvaluateDecision)?.PhaseDefinitionID;
                    viewModel.NextPhaseNee = nextPhaseDefinitions.FirstOrDefault(pd => pd.Phase < phaseDefinition.Phase)?.PhaseDefinitionID;
                }
            }
            
            var sendingorganization = OrganizationHelper.GetSendingOrganization(uow, globalProps.FlowInstance.FlowInstanceID);
            if(sendingorganization.OrganizationTypeID == (int)OrganizationTypeID.GeneralPracticioner)
            {
                //Huisarts niet lastigvallen met beoordelen alternatieve datum. Dus direct naar afsluiten fase
                //Dit behoudt de logica in het scherm en biedt mogelijkheid voor het vullen van een alternatieve datum
                viewModel.NextPhaseJaMaar = viewModel.NextPhaseJa;
            }

            if (phaseDefinition?.FlowDefinitionID == FlowDefinitionID.HA_VVT)
            {
                var flowFieldCareBeginDate = FlowFieldValueBL.GetByTransferIDAndFieldID(uow, globalProps.TransferID, FlowFieldConstants.ID_CareBeginDate).FirstOrDefault();
                if (!string.IsNullOrEmpty(flowFieldCareBeginDate?.Value))
                {
                    FlowWebFieldBL.SetValue(viewModel.Global.FlowWebFields, FlowFieldConstants.ID_CareBeginDate, flowFieldCareBeginDate.Value);
                }
                else
                {
                    var flowFieldGewensteIngangsdatum = FlowFieldValueBL.GetByTransferIDAndFieldID(uow, globalProps.TransferID, FlowFieldConstants.ID_GewensteIngangsdatum).FirstOrDefault();
                    if (!string.IsNullOrEmpty(flowFieldGewensteIngangsdatum?.Value))
                    {
                        FlowWebFieldBL.SetValue(viewModel.Global.FlowWebFields, FlowFieldConstants.ID_CareBeginDate, flowFieldGewensteIngangsdatum.Value);
                    }
                }
            }

            if (globalProps.PhaseInstance?.Status == (int)Status.Done)
            {
                var vvtDecision = viewModel.Global.FlowWebFields.FirstOrDefault(x => x.FlowFieldID == FlowFieldConstants.ID_DischargePatientAcceptedYNByVVT);
                if (vvtDecision != null)
                {
                    vvtDecision.ReadOnly = true;
                }

                var vvtdateDecision = viewModel.Global.FlowWebFields.FirstOrDefault(x => x.FlowFieldID == FlowFieldConstants.ID_PatientAcceptedAtDateTimeYN);
                if (vvtdateDecision != null)
                {
                    vvtdateDecision.ReadOnly = true;
                }
            }

            viewModel.ClientID = globalProps.Transfer?.ClientID;

            viewModel.HasCareBeginDate = viewModel.Global.FlowWebFields.HasValue("CareBeginDate");
            viewModel.HasAfterCareCategoryDefinitive = viewModel.Global.FlowWebFields.HasValue("AfterCareCategoryDefinitive");

            return viewModel;
        }

        public static TransferRouteInvitesViewModel GetTransferRouteInvitesViewModel(IUnitOfWork uow, UrlHelper url, int transferID, int flowInstanceID, FlowDefinitionID flowDefinitionID)
        {
            var viewModel = new TransferRouteInvitesViewModel();
            viewModel.SearchUrl = url.Action("DepartmentSearch", "OrganizationSearch", new { transferID, flowDefinitionID, OrganizationTypeID = (int)OrganizationTypeID.HealthCareProvider });
            viewModel.CurrentInvites = OrganizationInviteBL.GetByFlowInstanceIDAndInviteType(uow, flowInstanceID, InviteType.Regular).Where(o => TransferRouteViewModel.CurrentStatuses.Contains(o.InviteStatus)).ToList();
            viewModel.TransferID = transferID;
            viewModel.CanAdd = FlowInstanceBL.HasRegistrationEndedWithoutTransfer(uow, flowInstanceID) ? false : true;

            return viewModel;
        }

        public static CIZOverviewViewModel GetCizOverviewViewModel(IUnitOfWork uow, GlobalProperties globalProps)
        {
            var viewModel = new CIZOverviewViewModel( );
            viewModel.SetGlobalProperties(globalProps);

            var formSetVersions = FormSetVersionBL.GetByTransferIDAndFormTypeID(uow, globalProps.TransferID, (int)FlowFormType.GVp);
            if (formSetVersions == null)
            {
                return viewModel;
            }

            var gvpDuration = new List<GvpDuration>();

            foreach (var form in formSetVersions)
            {
                var msvtFlowWebFields = FlowWebFieldBL.GetFieldsFromFormSetVersionID(uow, form.FormSetVersionID, null).ToList();
                var msvtDurationsTemp = new GvpDuration();
                var definitive = "";
                if (form.FormType.TypeID == (int)TypeID.FlowTransferForm && form.PhaseInstance.Status == (int)Status.Done)
                {
                    definitive = "definitief";
                }

                msvtDurationsTemp.Name = !string.IsNullOrEmpty(form.Description)
                    ? form.Description + (definitive != "" ? " (" + definitive + ")" : "")
                    : "";
                var geldigheidsDuurStartdatum = msvtFlowWebFields.FirstOrDefault(fwf => fwf.Name == FlowFieldConstants.Name_VerwachteDuurStartdatum);
                if (geldigheidsDuurStartdatum != null)
                {
                    msvtDurationsTemp.DateBegin = geldigheidsDuurStartdatum.DisplayValue;
                }

                var geldigheidsDuurEinddatum = msvtFlowWebFields.FirstOrDefault(fwf => fwf.Name == FlowFieldConstants.Name_VerwachteDuurEinddatum);
                if (geldigheidsDuurEinddatum != null)
                {
                    msvtDurationsTemp.DateEnd = geldigheidsDuurEinddatum.DisplayValue;
                }

                gvpDuration.Add(msvtDurationsTemp);
            }

            viewModel.GvpDuration = gvpDuration;

            viewModel.ShowAttachment = globalProps.FlowWebFields.IsTrue("WMOBegeleiding") ||
                                       globalProps.FlowWebFields.IsTrue("WMOHuishoudelijkeondersteuning") ||
                                       globalProps.FlowWebFields.IsTrue("WMOJeugdzorg") ||
                                       globalProps.FlowWebFields.IsTrue("WlzGrondslagSom") ||
                                       globalProps.FlowWebFields.IsTrue("WlzGrondslagPG") ||
                                       globalProps.FlowWebFields.IsTrue("WlzAnders");

            viewModel.ShowGrondslag = globalProps.FlowWebFields.IsTrue("WlzGrondslagSom") ||
                                      globalProps.FlowWebFields.IsTrue("WlzGrondslagPG") ||
                                      globalProps.FlowWebFields.IsTrue("WlzAnders") ||
                                      globalProps.FlowWebFields.IsTrue("WlzProfiel") ||
                                      globalProps.FlowWebFields.HasValue("AanvraagBOPZ");

            return viewModel;
        }

        public static CVARouteSelectionViewModel GetCVARouteSelectionViewModel(IUnitOfWork uow, GlobalProperties globalProps)
        {
            var viewModel = new CVARouteSelectionViewModel();
            viewModel.SetGlobalProperties(globalProps);

            var phaseInstance = globalProps.PhaseInstance;
            if (phaseInstance == null)
            {
                viewModel.ErrorMessage = "Geen phaseInstance";
                return viewModel;
            }

            viewModel.ResendAvailable = phaseInstance.IsResendAvailable();

            var ontslagBestemming = globalProps.FlowWebFields.FirstOrDefault(fwf => fwf.Name == FlowFieldConstants.Name_Ontslagbestemming);
            if (viewModel.Prefill)
            {
                var beloopOpname = FlowFieldValueBL.GetByTransferIDAndFieldID(uow, globalProps.TransferID, FlowFieldConstants.ID_BeloopOpname)
                                    .FirstOrDefault(it => it.FormSetVersion.FormTypeID == (int)FlowFormType.InvullenCVARegistratieZH);
                if (beloopOpname != null && beloopOpname.Value == FlowFieldConstants.Value_BeloopOpname_Patientisoverleden && ontslagBestemming != null)
                {
                    FlowWebFieldBL.SetValue(ontslagBestemming, FlowFieldConstants.Value_Ontslagbestemming_Dossiersluiten, Source.POINT_PREFILL);
                }
            }

            PhaseDefinitionCache receivingPhaseDefinition = null;
            if (!string.IsNullOrWhiteSpace(ontslagBestemming?.Value))
            {
                var codeGroup = "VVTGROUP";
                if (ontslagBestemming.Value == FlowFieldConstants.Value_Ontslagbestemming_Revalidatiecentrum)
                {
                    codeGroup = "SENDVO";
                }
                receivingPhaseDefinition = PhaseDefinitionCacheBL.GetByFlowDefinitionIDAndCodeGroup(uow, phaseInstance.FlowInstance.FlowDefinitionID, codeGroup).FirstOrDefault();
            }

            var healthCareProviders = new List<OrganizationInvite>();
            if (receivingPhaseDefinition != null)
            {
                healthCareProviders = OrganizationInviteBL.GetByFlowInstanceIDAndPhaseDefinitionID(uow, phaseInstance.FlowInstanceID, receivingPhaseDefinition.PhaseDefinitionID);
            }
            viewModel.HealthCareProviders = healthCareProviders.Skip(1);

            var nextPhaseDefinitions = PhaseDefinitionBL.GetNextPhaseDefinitions(uow, globalProps.PhaseDefinitionID);

            viewModel.PhaseAfdAanvraag = nextPhaseDefinitions.FirstOrDefault(pd => pd.FormType.TypeID == (int)TypeID.FlowAanvraag)?.PhaseDefinitionID;
            viewModel.PhaseAfdVO = nextPhaseDefinitions.FirstOrDefault(pd => pd.FormTypeID == (int)FlowFormType.VersturenVO)?.PhaseDefinitionID;
            viewModel.PhaseAfsluiten = nextPhaseDefinitions.FirstOrDefault(pd => pd.EndFlow)?.PhaseDefinitionID;

            var selectedHealthCareProviderValue = globalProps.FlowWebFields.GetValueOrDefault(FlowFieldConstants.Name_SelectedHealthCareProvider, "");
            if (!int.TryParse(selectedHealthCareProviderValue, out int selectedHealthCareProviderID))
            {
                return viewModel;
            }

            var selectedHealthCareProvider = DepartmentBL.GetByDepartmentID(uow, selectedHealthCareProviderID);
            if (selectedHealthCareProvider == null)
            {
                return viewModel;
            }

            var participation = FlowDefinitionParticipationBL.GetByLocationIDAndDepartmentIDAndFlowDefinitionID(uow, selectedHealthCareProvider.LocationID, selectedHealthCareProvider.DepartmentID, globalProps.FlowInstance.FlowDefinitionID);
            viewModel.ShowPointParticipatorWarning = ( participation?.Participation != Participation.Active && participation?.Participation != Participation.ActiveButInvisibleInSearch);

            return viewModel;
        }

        private static void SetExternalSupplierProperties(HulpmiddelenViewModel viewModel, SupplierID targetSupplierID)
        {
            var supplier = viewModel.Suppliers.FirstOrDefault(x => x.SupplierID == (int)targetSupplierID);
            if (supplier == null)
            {
                return;
            }

            switch (targetSupplierID)
            {
                case SupplierID.Vegro:
                    viewModel.SupplierNameVegro = supplier.Name;
                    viewModel.SupplierPhoneNumberVegro = supplier.PhoneNumber;
                    break;
                case SupplierID.MediPoint:
                    viewModel.SupplierNameMediPoint = supplier.Name;
                    viewModel.SupplierPhoneNumberMediPoint = supplier.PhoneNumber;
                    break;
                case SupplierID.Point:
                    viewModel.SupplierNamePoint = supplier.Name;
                    viewModel.SupplierPhoneNumberPoint = supplier.PhoneNumber;
                    break;
            }

            if (supplier.SupplierID == viewModel.SupplierID)
            {
                viewModel.CurrentSupplierName = supplier.Name;
                viewModel.CurrentSupplierPhoneNumber = supplier.PhoneNumber;
            }
        }

        private static string GetJsonAddress(JavaScriptSerializer jsSerializer, AddressViewModel address)
        {
            if (address == null)
            {
                address = new AddressViewModel();
            }
            return jsSerializer.Serialize(address);
        }
        public static HulpmiddelenViewModel GetHulpmiddelenViewModel(FormTypeController controller, GlobalProperties globalProps)
        {
            var jsSerializer = new JavaScriptSerializer();

            var viewModel = new HulpmiddelenViewModel
            {
                Suppliers = AidProductBL.GetSuppliers(controller.uow)
            };

            if (globalProps.FormSetVersionID <= 0)
            {
                var formSetVersion = controller.SaveFormsetAndValidFields(globalProps.FlowWebFields, globalProps.FormTypeID, globalProps.PhaseInstanceID);
                if (formSetVersion != null)
                {
                    globalProps.FormSetVersionID = formSetVersion.FormSetVersionID;
                }
            }
            
            viewModel.SetGlobalProperties(globalProps);

            var afleverTijd = viewModel.Global.FlowWebFields.FirstOrDefault(f => f.FlowFieldID == FlowFieldConstants.ID_AfleverTijd);
            if (afleverTijd == null)
            {
                return viewModel;
            }
            viewModel.AfleverTijden = afleverTijd.GetDropdownMultiSelectViewModel(controller.uow, globalProps.FormSetVersionID, true, "supplier-vegro supplier-point required");
            viewModel.AfleverTijden.SetTrimSegmentsForShort(new[] { "Tussen ", " " });
            viewModel.AfleverTijden.SetSelectedNoneText("Kies een tijd (max. 3)");
            

            var flowInstance = globalProps.FlowInstance;

            if (flowInstance == null)
            {
                return viewModel;
            }

            var receivingLocation = OrganizationHelper.GetRecievingLocation(controller.uow, flowInstance.FlowInstanceID);
            var sendingLocation = OrganizationHelper.GetSendingLocation(controller.uow, flowInstance.FlowInstanceID);

            var orderItems = new List<AidProductOrderItem>();
            var answers = new List<AidProductOrderAnswer>();

            if (globalProps.FormSetVersionID > 0)
            {
                orderItems = AidProductBL.GetOrderItemsByFormSetVersionID(controller.uow, globalProps.FormSetVersionID);
                viewModel.AidProductIDs = string.Join(",", orderItems.Select(it => it.AidProductID));

                answers = AidProductBL.GetAnswersByFormSetVersionID(controller.uow, globalProps.FormSetVersionID);
            }

            if (answers.Any())
            {
                var firstItemKey = answers.FirstOrDefault().UniqueFormKey;
                viewModel.UniqueFormKey = firstItemKey;
            }

            if (viewModel.UniqueFormKey == 0)
            {
                viewModel.UniqueFormKey = new Random(DateTime.Now.Millisecond).Next(100000, 999999);
            }

            viewModel.OrderItems = AidProductOrderItemViewModel.FromModelList(orderItems);

            var client = flowInstance?.Transfer?.Client;
            AddressViewModel clientAdres = null;

            var supplierIDVal = viewModel.Global.FlowWebFields.GetValueOrDefault("SupplierID", SupplierID.Point.ToString());
            if (int.TryParse(supplierIDVal, out var supplierID))
            {
                viewModel.SupplierID = supplierID;
            }

            SetExternalSupplierProperties(viewModel, SupplierID.Vegro);
            SetExternalSupplierProperties(viewModel, SupplierID.MediPoint);
            SetExternalSupplierProperties(viewModel, SupplierID.Point);

            if (client != null)
            {
                clientAdres = client.FullAdress();
                var jsonPatientAdres = GetJsonAddress(jsSerializer, clientAdres);
                viewModel.PatientAdres = jsonPatientAdres;
                viewModel.PatientBSN = client.CivilServiceNumber;

                int? insuranceId = client.HealthInsuranceCompanyID;
                if (insuranceId.HasValue)
                {
                    var insurer = HealthInsurerBL.GetByID(controller.uow, insuranceId.Value);
                    if (insurer != null)
                    {
                        viewModel.VegroCode = insurer.VegroCode;
                        viewModel.MediPointCode = insurer.MediPointCode;
                        viewModel.HealthInsurerUzovi = insurer.UZOVI ?? 0;
                    }
                }

                viewModel.PatientEmail = (client.Email ?? string.Empty).Trim();
            }

            viewModel.TransferPuntEmail = (globalProps.PointUserInfo.Employee?.EmailAddress ?? string.Empty).Trim();
            viewModel.ZorginstellingAdres = GetJsonAddress(jsSerializer, receivingLocation?.FullAdress());
            viewModel.HospitalAdres = GetJsonAddress(jsSerializer, sendingLocation?.FullAdress());
            viewModel.ConfirmationEmail = viewModel.Global.FlowWebFields.GetValueOrDefault(FlowFieldConstants.Name_OntvangerBevestigingsEmailAdres, "");

            var sendingDepartment = globalProps.PointUserInfo.Department;
            var bestellerAfdeling = viewModel.Global.FlowWebFields.GetValueOrDefault(FlowFieldConstants.Name_BestellerAfdeling, "");
            if (string.IsNullOrEmpty(bestellerAfdeling))
            {
                FlowWebFieldBL.SetValue(viewModel.Global.FlowWebFields, FlowFieldConstants.ID_BestellerAfdeling, sendingDepartment.FullName());
                FlowWebFieldBL.SetValue(viewModel.Global.FlowWebFields, FlowFieldConstants.ID_SupplierID, (int)SupplierID.Point);

                if (clientAdres != null)
                {
                    FlowWebFieldBL.SetValue(viewModel.Global.FlowWebFields, FlowFieldConstants.ID_OntvangerStraat, clientAdres.Street, Source.POINT_PREFILL);
                    FlowWebFieldBL.SetValue(viewModel.Global.FlowWebFields, FlowFieldConstants.ID_OntvangerHuisnummer, clientAdres.HouseNumber, Source.POINT_PREFILL);
                    FlowWebFieldBL.SetValue(viewModel.Global.FlowWebFields, FlowFieldConstants.ID_OntvangerHuisnummerExtra, clientAdres.HouseNumberExtra, Source.POINT_PREFILL);
                    FlowWebFieldBL.SetValue(viewModel.Global.FlowWebFields, FlowFieldConstants.ID_OntvangerPostcode, clientAdres.PostalCode, Source.POINT_PREFILL);
                    FlowWebFieldBL.SetValue(viewModel.Global.FlowWebFields, FlowFieldConstants.ID_OntvangerPlaats, clientAdres.City, Source.POINT_PREFILL);
                }
            }

            var authorizationFormAttachmentId = viewModel.Global.FlowWebFields.GetValueOrDefault(FlowFieldConstants.Name_AuthorizationFormAttachmentID, "");
            if (!string.IsNullOrEmpty(authorizationFormAttachmentId))
            {
                viewModel.AuthorizationFormAttachment = TransferAttachmentBL.GetByID(controller.uow, authorizationFormAttachmentId.ConvertTo<int>());
            }

            var formSetVersions = FormSetVersionBL.GetByTransferIDAndFormTypeID(controller.uow, globalProps.TransferID, (int)FlowFormType.Hulpmiddelen).OrderBy(fsv => fsv.FormSetVersionID).ToList();

            var orderStatusCurrent = DashboardMenuItemViewBL.UNKNOWN;

            var currentFormSetversionDescription = string.Empty;
            var isCurrentOrderDone = false;

            int currentFormSetPhaseInstanceID = globalProps.PhaseInstanceID;

            const string formSetversionDescriptionPrefix = "Hulpmiddelen ";

            var counter = 0;
            foreach (var formSetversion in formSetVersions)
            {              
                var isCurrentFormSetversion = formSetversion.FormSetVersionID == globalProps.FormSetVersionID;

                counter++;
                var description = formSetversion.Description;
                if (string.IsNullOrEmpty(description))
                {
                    description = $"{formSetversionDescriptionPrefix}{counter}";
                }
                
                var orderStatusResult = AidProductBL.GetOrderStatusWithMessageIndication(controller.uow, formSetversion.FormSetVersionID);
                var orderStatus = orderStatusResult.OrderStatus;

                if (!string.IsNullOrEmpty(orderStatus))
                {
                    description += $" ({orderStatus})";

                    if (isCurrentFormSetversion)
                    {
                        orderStatusCurrent = orderStatus;
                        isCurrentOrderDone = orderStatus == DashboardMenuItemViewBL.ORDER_PROCESSING || orderStatus == DashboardMenuItemViewBL.ORDER_CONFIRMED || orderStatus == DashboardMenuItemViewBL.SENT;
                        viewModel.ShowOrderStatusMessage = orderStatusResult.ShowStatusMessage;
                    }
                }

                var formSetVersionViewModel = new FormSetVersionViewModel
                {
                    Description = description,
                    FormSetVersionID = formSetversion.FormSetVersionID,
                    IsSelected = globalProps.FormSetVersionID == formSetversion.FormSetVersionID,
                    Url = controller.Url.Action("Hulpmiddelen", "FormType", new {globalProps.TransferID, globalProps.PhaseDefinitionID, globalProps.PhaseInstanceID, formSetversion.FormSetVersionID})
                };
                viewModel.FormSetVersions.Add(formSetVersionViewModel);

                if (isCurrentFormSetversion)
                {
                    currentFormSetversionDescription = formSetversion.Description;
                    currentFormSetPhaseInstanceID = formSetversion.PhaseInstanceID ?? -1;
                }
            }
            
            viewModel.OrderStatus = orderStatusCurrent;
            viewModel.IsCurrentOrderDone = globalProps.IsReadMode || isCurrentOrderDone;

            // TODO: Remove when use of ViewBag.AreYouSure inn _PoihtDefaultLayoutBootstrap.cshtml is removed
            controller.ViewBag.AreYousure = "false";

            controller.PhaseInstanceID = currentFormSetPhaseInstanceID;     // TODO: We shouldn't be using those controller variables...but until we properly fix that dependency (on determining ReadMode in this case)

            if (string.IsNullOrEmpty(currentFormSetversionDescription))
            {
                currentFormSetversionDescription = $"{formSetversionDescriptionPrefix}{formSetVersions.Count + 1}";
            }

            viewModel.CurrentFormSetversionDescription = currentFormSetversionDescription;

            if (viewModel.IsExternalSupplier)
            {
                viewModel.Questions = AidProductQuestionHelper.GetQuestions(controller.uow, viewModel.SupplierID, globalProps.TransferID, viewModel.OrderItems.Select(it => it.AidProductID).ToArray(), viewModel.ExternalSupplierInsurerCode, viewModel.UniqueFormKey, globalProps.FormSetVersionID);

                var questionsAnswered = AidProductQuestionsAnsweredAll.NotNecessary;  // "2"; //1: Ja gedaan, 2: Nee niet nodig
                if (viewModel.Questions.Count > 0)
                {
                    questionsAnswered = !AidProductQuestionViewModelHelper.GetUnansweredQuestions(viewModel.SupplierID, viewModel.Questions).Any() ? AidProductQuestionsAnsweredAll.Yes : "";
                }

                viewModel.QuestionsAnsweredAll = questionsAnswered;
                FlowWebFieldBL.SetValue(viewModel.Global.FlowWebFields, FlowFieldConstants.ID_QuestionsAnswered, questionsAnswered);
            }

            var phaseDefinition = PhaseDefinitionCacheBL.GetByFlowDefinitionAndFormType(controller.uow, globalProps.FlowInstance.FlowDefinitionID, (int)FlowFormType.Hulpmiddelen);
            var rights = PhaseDefinitionRightsBL.GetFormRightsPhaseDefinitionID(controller.uow, globalProps.PointUserInfo, phaseDefinition, globalProps.FlowInstance);
            viewModel.HideNewButtons = (rights == Rights.R || rights == Rights.None);
            viewModel.ShowQuestionWarning = !viewModel.IsCurrentOrderDone && viewModel.HasQuestionList && !AidProductQuestionViewModelHelper.AreAllQuestionsAnswered(viewModel.SupplierID, viewModel.Questions);
            viewModel.HastransportInfo = viewModel.Global.FlowWebFields.HasValue(FlowFieldConstants.Name_TransportGeduld) || viewModel.Global.FlowWebFields.HasValue(FlowFieldConstants.Name_TransportHuisnummer);

            return viewModel;
        }
        
        public static InvullenCVARegistratie3MndViewModel GetInvullenCVARegistratie3MndViewModel(IUnitOfWork uow, GlobalProperties globalProps)
        {
            var viewModel = new InvullenCVARegistratie3MndViewModel();
            viewModel.SetGlobalProperties(globalProps);

            var frequency = FrequencyBL.GetByType(uow, globalProps.TransferID, FrequencyType.CVA);
            viewModel.FrequencyID = frequency?.FrequencyID ?? -1;
            viewModel.FrequencyStatus = frequency?.Status ?? FrequencyStatus.NotActive;
            viewModel.ClientID = globalProps.Transfer?.ClientID;

            return viewModel;
        }

        public static RequestFormZHZHViewModel GetRequestFormZHZHViewModel(IUnitOfWork uow, GlobalProperties globalProps)
        {
            var viewModel = new RequestFormZHZHViewModel();
            viewModel.SetGlobalProperties(globalProps);

            if (!viewModel.Prefill && !globalProps.IsPrintRequest)
            {
                return viewModel;
            }

            var pointUserInfo = globalProps.PointUserInfo;
            var flowInstance = globalProps.FlowInstance;
            var sendingDepartment = OrganizationHelper.GetSendingDepartment(uow, flowInstance.FlowInstanceID);
            if (sendingDepartment?.Location != null)
            {
                viewModel.DepartmentName = sendingDepartment.Name;
                viewModel.LocationName = sendingDepartment.Location.Name;

                FlowWebFieldBL.SetValue(viewModel.Global.FlowWebFields, FlowFieldConstants.ID_SourceOrganization, sendingDepartment.Location.OrganizationID, Source.POINT_PREFILL);
                FlowWebFieldBL.SetValue(viewModel.Global.FlowWebFields, FlowFieldConstants.ID_SourceOrganizationLocation, sendingDepartment.LocationID, Source.POINT_PREFILL);
                FlowWebFieldBL.SetValue(viewModel.Global.FlowWebFields, FlowFieldConstants.ID_SourceOrganizationDepartment, sendingDepartment.DepartmentID, Source.POINT_PREFILL);

                var createdByDepartmentTelephoneNumber = viewModel.Global.FlowWebFields.FirstOrDefault(fwf => fwf.Name == FlowFieldConstants.Name_CreatedByDepartmentTelephoneNumber);
                if (createdByDepartmentTelephoneNumber != null && !string.IsNullOrEmpty(sendingDepartment.PhoneNumber))
                {
                    FlowWebFieldBL.SetValue(createdByDepartmentTelephoneNumber, sendingDepartment.PhoneNumber);
                }
            }

            FlowWebFieldBL.SetValue(viewModel.Global.FlowWebFields, FlowFieldConstants.ID_DossierOwner, pointUserInfo.Employee.EmployeeID, Source.POINT_PREFILL, globalProps.ViewData);
            FlowWebFieldBL.SetValue(viewModel.Global.FlowWebFields, FlowFieldConstants.ID_DossierOwnerTelephoneNumber, pointUserInfo.Employee.PhoneNumber, Source.POINT_PREFILL);

            return viewModel;
        }

        public static RequestFormHAVVTViewModel GetRequestFormHAVVTViewModel(IUnitOfWork uow, GlobalProperties globalProps)
        {
            var viewModel = new RequestFormHAVVTViewModel();
            viewModel.SetGlobalProperties(globalProps);

            var flowInstance = globalProps.FlowInstance;

            var pointUserInfo = globalProps.PointUserInfo;
            var sendingDepartment = OrganizationHelper.GetSendingDepartment(uow, flowInstance.FlowInstanceID);

            if (viewModel.Prefill)
            {
                FlowWebFieldBL.SetValue(viewModel.Global.FlowWebFields, FlowFieldConstants.ID_DossierOwner, pointUserInfo.Employee.EmployeeID, Source.POINT_PREFILL, globalProps.ViewData);

                if (sendingDepartment == null)
                {
                    return viewModel;
                }

                var huisartsNaamAdres = viewModel.Global.FlowWebFields.FirstOrDefault(fwf => fwf.Name == WellKnownFieldNames.HuisartsNaamAdres);
                if (huisartsNaamAdres != null)
                {
                    if (OrganizationHelper.GetSendingOrganization(uow, flowInstance.FlowInstanceID).IsGeneralPracticioner())
                    {
                        FlowWebFieldBL.SetValue(huisartsNaamAdres, sendingDepartment?.FullName(), Source.POINT_PREFILL);
                    }
                    else
                    {
                        var client = ClientBL.GetByTransferID(uow, globalProps.TransferID);
                        if (client != null)
                        {
                            FlowWebFieldBL.SetValue(huisartsNaamAdres, $"{client.GeneralPractitionerName} ({client.GeneralPractitionerPhoneNumber})", Source.POINT_PREFILL);
                        }
                    }
                }
                

                if (sendingDepartment?.Location != null)
                {
                    FlowWebFieldBL.SetValue(viewModel.Global.FlowWebFields, FlowFieldConstants.ID_SourceOrganization, sendingDepartment.Location.OrganizationID, Source.POINT_PREFILL);
                    FlowWebFieldBL.SetValue(viewModel.Global.FlowWebFields, FlowFieldConstants.ID_SourceOrganizationLocation, sendingDepartment.LocationID, Source.POINT_PREFILL);
                    FlowWebFieldBL.SetValue(viewModel.Global.FlowWebFields, FlowFieldConstants.ID_SourceOrganizationDepartment, sendingDepartment.DepartmentID, Source.POINT_PREFILL);
                }
            }

            viewModel.DepartmentName = sendingDepartment?.Name;
            viewModel.LocationName = sendingDepartment?.Location?.Name;
            viewModel.ClientID = globalProps.FlowInstance?.Transfer?.ClientID;

            return viewModel;
        }

        public static FormTypeBaseViewModel GetEmployeeIIViewModel(IUnitOfWork uow, GlobalProperties globalProps)
        {
            var viewModel = new FormTypeBaseViewModel();
            viewModel.SetGlobalProperties(globalProps);

            if (!viewModel.Prefill)
            {
                return viewModel;
            }

            var phaseInstance = globalProps.PhaseInstance;
            if (phaseInstance != null)
            {
                var transferDateII = globalProps.FlowWebFields.FirstOrDefault(fwf => fwf.Name == FlowFieldConstants.Name_TransferDateII);
                if (transferDateII != null)
                {
                    FlowWebFieldBL.SetValue(transferDateII, phaseInstance.StartProcessDate, Source.POINT_PREFILL);
                }
            }

            if (globalProps.HasFormSetVersion)
            {
                return viewModel;
            }

            var acceptedBy = viewModel.Global.FlowWebFields.FirstOrDefault(it => it.Name == FlowFieldConstants.Name_AcceptedByII);
            FlowWebFieldBL.SetValue(acceptedBy, (int)viewModel.Global.PointUserInfo?.Employee?.EmployeeID, Source.POINT_PREFILL, globalProps.ViewData);

            return viewModel;
        }

        public static FormTypeBaseViewModel GetCIZResultViewModel(IUnitOfWork uow, GlobalProperties globalProps)
        {
            var viewModel = new FormTypeBaseViewModel();
            viewModel.SetGlobalProperties(globalProps);

            if (globalProps.IsReadMode)
            {
                return viewModel;
            }

            var indicationGivenByField = viewModel.Global.FlowWebFields.FirstOrDefault(f => f.Name == "IndicationGivenBy");
            if (indicationGivenByField != null && string.IsNullOrEmpty(indicationGivenByField.Value))
            {
                FlowWebFieldBL.SetValue(indicationGivenByField, viewModel.Global.PointUserInfo?.Employee?.EmployeeID.ToString(), Source.POINT_PREFILL);
            }

            return viewModel;
        }

        public static FormTypeBaseViewModel GetAcceptAtVVTViewModel(IUnitOfWork uow, GlobalProperties globalProps)
        {
            var viewModel = new FormTypeBaseViewModel();
            viewModel.SetGlobalProperties(globalProps);

            if (!viewModel.Prefill)
            {
                return viewModel;
            }

            var acceptedBy = globalProps.FlowWebFields.FirstOrDefault(fwf => fwf.Name == FlowFieldConstants.Name_AcceptedByVVT);
            if (acceptedBy != null)
            {
                FlowWebFieldBL.SetValue(acceptedBy, globalProps.PointUserInfo.Employee?.EmployeeID.ToString(), Source.POINT_PREFILL, globalProps.ViewData);
            }

            if (globalProps.PhaseInstanceID <= 0)
            {
                return viewModel;
            }

            var phaseInstance = globalProps.PhaseInstance;
            if (phaseInstance == null)
            {
                return viewModel;
            }

            var transferDateII = globalProps.FlowWebFields.FirstOrDefault(fwf => fwf.Name == FlowFieldConstants.Name_TransferDateVVT);
            if (transferDateII != null)
            {
                FlowWebFieldBL.SetValue(transferDateII, phaseInstance.StartProcessDate, Source.POINT_PREFILL);
            }

            return viewModel;
        }

        public static FormTypeBaseViewModel GetGRZResultViewModel(IUnitOfWork uow, GlobalProperties globalProps)
        {
            var viewModel = new FormTypeBaseViewModel();
            viewModel.SetGlobalProperties(globalProps);

            viewModel.ClientID = globalProps.Transfer?.ClientID;

            if (globalProps.HasFormSetVersion || globalProps.IsReadMode)
            {
                return viewModel;
            }

            var resultTriageBy = viewModel.Global.FlowWebFields.FirstOrDefault(it => it.Name == FlowFieldConstants.Name_ResultaatTriageDoorSOG);
            if (resultTriageBy != null)
            {
                FlowWebFieldBL.SetValue(resultTriageBy, globalProps.PointUserInfo?.Employee?.FullName());
            }

            return viewModel;
        }

        public static FrequencyViewModel GetDoorlopendDossierViewModel(IUnitOfWork uow, GlobalProperties globalProps)
        {
            var viewModel = FrequencyViewBL.FromType(uow, globalProps.TransferID, FrequencyType.General);
            viewModel.SetGlobalProperties(globalProps);
            viewModel.ClientID = globalProps.Transfer?.ClientID;

            return viewModel;
        }

        public static FormTypeBaseViewModel GetInvullenCVARegistratieVVTViewModel(IUnitOfWork uow, GlobalProperties globalProps)
        {
            var viewModel = new FormTypeBaseViewModel();
            viewModel.SetGlobalProperties(globalProps);

            viewModel.FlowDefinitionID = globalProps.FlowInstance.FlowDefinitionID;

            return viewModel;
        }

        public static FormTypeBaseViewModel GetInvullenCVARegistratieZHViewModel(IUnitOfWork uow, GlobalProperties globalProps)
        {
            var viewModel = new FormTypeBaseViewModel();
            viewModel.SetGlobalProperties(globalProps);
            return viewModel;
        }

        public static ClientCareFormViewModel GetClientCareFormViewModel(IUnitOfWork uow, GlobalProperties globalProps, bool regenerate)
        {
            var viewModel = ClientCareFormViewModelBL.Get(uow, globalProps.FlowInstance, globalProps.PointUserInfo, globalProps.HasFormSetVersion && regenerate);
            viewModel.SetGlobalProperties(globalProps);

            return viewModel;
        }
    }
}