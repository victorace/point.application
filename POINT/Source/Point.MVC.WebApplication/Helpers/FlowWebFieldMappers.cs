﻿using System.Collections.Generic;
using System.Linq;
using Point.Database.Models;
using Point.Database.Models.ViewModels;

namespace Point.MVC.WebApplication.Helpers
{
    public class FlowWebFieldMappers
    {
        /// <summary>
        /// Maps dbDrivenValidations onto flowWebFields.
        /// </summary>
        public static void MapToFlowWebFields(
            List<FlowWebField> flowWebFields,
            List<DbDrivenValidation> dbDrivenValidations)
        {
            foreach (var dbDrivenValidation in dbDrivenValidations)
            {
                MapToFlowWebFields(flowWebFields, dbDrivenValidation);
            }
        }

        private static void MapToFlowWebFields(
            IReadOnlyCollection<FlowWebField> flowWebFields,
            DbDrivenValidation dbDrivenValidation)
        {
            // At the moment this method contains a single statement, but it can contain more.
            MapDbDrivenValidationToFlowWebFields(flowWebFields, dbDrivenValidation);
        }

        /// <summary>
        /// Maps a single dbDrivenValidation onto flowWebFields.
        /// </summary>
        private static void MapDbDrivenValidationToFlowWebFields(
            IReadOnlyCollection<FlowWebField> flowWebFields,
            DbDrivenValidation dbDrivenValidation)
        {
            var allFieldAttributesFoundInFlowWebFields = true;
            var flowWebFieldsFound = new List<FlowWebField>();

            // PASS 1: Look for this validation's field attributes inside this flow
            foreach (var fieldAttribute in dbDrivenValidation.FieldAttributes)
            {
                var found = false;
                foreach (var flowWebField in flowWebFields)
                {
                    if (fieldAttribute == flowWebField.FlowFieldAttributeID)
                    {
                        flowWebFieldsFound.Add(flowWebField);
                        found = true;
                        break;
                    }
                }

                if (!found)
                {
                    // If any field attribute of this validation not found on this flow, then quit this validation
                    allFieldAttributesFoundInFlowWebFields = false;
                }
            }

            // If this validation's field attributes are not all found in this flow, then skip this validation
            if (!allFieldAttributesFoundInFlowWebFields)
            {
                return;
            }

            // Convert: "{0} en {1} liggen te ver uit elkaar."
            // to: "Datum opname en Gewenste ontslagdatum liggen te ver uit elkaar."
            // (string.Format requires an object[], otherwise we may get a run-time exception.)
            // (https://stackoverflow.com/questions/40885239/using-an-array-as-argument-for-string-format)
            var labels = flowWebFieldsFound.Select(flowWebField => (object) flowWebField.Label).ToArray();
            var substitutedMessages = dbDrivenValidation.Messages
                .Select(message => string.Format(message, labels))
                .ToList();

            // PASS 2: Copy the validation to the flow's field attributes we've just found
            var fieldIndex = 0;
            foreach (var fieldAttribute in dbDrivenValidation.FieldAttributes)
            {
                foreach (var flowWebField in flowWebFieldsFound)
                {
                    if (fieldAttribute == flowWebField.FlowFieldAttributeID)
                    {
                        flowWebField.DbDrivenValidation = new DbDrivenValidation
                        {
                            Method = dbDrivenValidation.Method,
                            FieldAttributes = dbDrivenValidation.FieldAttributes,
                            WarningTriggers = dbDrivenValidation.WarningTriggers,
                            Messages = substitutedMessages,
                            FieldAttributeIndexWithinValidation = fieldIndex
                        };
                        break;
                    }
                }
                fieldIndex++;
            }
        }
    }
}