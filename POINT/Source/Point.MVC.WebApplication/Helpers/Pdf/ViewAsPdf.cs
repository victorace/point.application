﻿using Point.Business.Pdf;
using Point.Infrastructure;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Point.Business.BusinessObjects;

namespace Point.MVC.WebApplication.Helpers.Pdf
{
    public class ViewAsPdf : AsPdfResultBase
    {
        private string _viewName;
        

        public string ViewName
        {
            get { return _viewName ?? ""; }
            set { _viewName = value; }
        }

        public ViewAsPdf(byte[] extraPdf = null)
        {
            MasterName = "";
            ViewName = "";
            Model = null;
            ExtraPdfData = extraPdf;
        }

        public ViewAsPdf(string viewName, byte[] extraPdf = null, string header = "", string footer = "")
            : this(extraPdf)
        {
            ViewName = viewName;
            Header = header;
            Footer = footer;
        }

        public ViewAsPdf(object model, byte[] extraPdf = null, string header = "", string footer = "")
            : this(extraPdf)
        {
            Model = model;
            Header = header;
            Footer = footer;
        }

        public ViewAsPdf(string viewName, object model, byte[] extraPdf = null, string header = "", string footer = "")
            : this(extraPdf)
        {
            ViewName = viewName;
            Model = model;
            Header = header;
            Footer = footer;
        }

        public ViewAsPdf(string viewName, string masterName, object model, string header = "", string footer = "")
            : this(viewName, model)
        {
            MasterName = masterName;
            Header = header;
            Footer = footer;
        }

        protected override string GetUrl(ControllerContext context)
        {
            return "";
        }

        protected virtual ViewEngineResult GetView(ControllerContext context, string viewName, string masterName)
        {
            return ViewEngines.Engines.FindView(context, ViewName, MasterName);
        }

        protected override GeneratePdfBO.GeneratePdfResult CallTheDriver(ControllerContext context)
        {
            bool renderHTML = true;
            List<GeneratePdfBO.GeneratePdfAttachment> attachments = null;

            if (Model is String)
            {
                return GeneratePDF.GenerateFromHTML((String) Model, null, Orientation);
            }

            if (Model is Database.Models.ViewModels.PrintMultipleViewModel)
            {
                var printModel = (Database.Models.ViewModels.PrintMultipleViewModel) Model;
                if (printModel.FormSetVersions.Count == 0)
                {
                    renderHTML = false;
                }

                if (printModel.Attachments != null && printModel.Attachments?.Count > 0)
                {
                    attachments = new List<GeneratePdfBO.GeneratePdfAttachment>();
                    foreach (var attachment in printModel.Attachments)
                    {
                        attachments.Add(new GeneratePdfBO.GeneratePdfAttachment(attachment.GenericFile.FileData, attachment.GenericFile.FileName));
                    }
                }
            }

            context.Controller.ViewData.Model = Model;

            // use action name if the view name was not provided
            if (String.IsNullOrEmpty(ViewName))
            {
                ViewName = context.RouteData.GetRequiredString("action");
            }

            using (var sw = new StringWriter())
            {
                StringBuilder html = sw.GetStringBuilder();
                if (renderHTML)
                {
                    ViewEngineResult viewResult = GetView(context, ViewName, MasterName);

                    // view not found, throw an exception with searched locations
                    if (viewResult.View == null)
                    {
                        var locations = new StringBuilder();
                        locations.AppendLine();

                        foreach (string location in viewResult.SearchedLocations)
                        {
                            locations.AppendLine(location);
                        }

                        throw new InvalidOperationException(String.Format(
                            "The view '{0}' or its master was not found, searched locations: {1}", ViewName,
                            locations));
                    }

                    var viewContext = new ViewContext(context, viewResult.View, context.Controller.ViewData,
                        context.Controller.TempData, sw);
                    viewResult.View.Render(viewContext, sw);

                    // replace href and src attributes with full URLs
                    string baseUrl = String.Format("{0}://{1}", Utils.UriScheme(),
                        HttpContext.Current.Request.Url.Authority);
                    html.Replace(" href=\"/", String.Format(" href=\"{0}/", baseUrl));
                    html.Replace(" src=\"/", String.Format(" src=\"{0}/", baseUrl));
                }
                
                var generatePdfResult = GeneratePDF.GenerateFromHTML(html.ToString(), header_html: Header, footer_html: Footer,
                    extraPdfData: null, attachments: attachments);

                generatePdfResult.AbortPrint = !renderHTML && generatePdfResult.AttachmentsTotalProcessedSuccess == 0;

                return generatePdfResult;
            }
        }
    }
}