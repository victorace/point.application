﻿using Point.Business.Pdf;
using Point.Infrastructure.Helpers;
using Point.MVC.WebApplication.Controllers;
using System;
using System.Web;
using System.Web.Mvc;
using Point.Business.BusinessObjects;
using Point.Infrastructure.Extensions;

namespace Point.MVC.WebApplication.Helpers.Pdf
{
    public abstract class AsPdfResultBase : SecureActionResult
    {
        private const string ContentType = "application/pdf";

        public int? TransferID { get; set; }
        public string FormName { get; set; }
        public string Header { get; set; }
        public string Footer { get; set; }
        public GeneratePDF.Orientation Orientation { get; set; } = GeneratePDF.Orientation.Portrait;

        public byte[] ExtraPdfData { get; set; }

        /// <summary>
        /// Custom name of authentication cookie used by forms authentication.
        /// </summary>
        public string FormsAuthenticationCookieName { get; set; }

        protected AsPdfResultBase()
        {
            FormsAuthenticationCookieName = ".ASPXAUTH";
        }

        protected abstract string GetUrl(ControllerContext context);

        protected virtual GeneratePdfBO.GeneratePdfResult CallTheDriver(ControllerContext context)
        {
            var switches = GetUrl(context);
            return GeneratePDF.GenerateFromURL(switches);
        }

        public GeneratePdfBO.GeneratePdfResult BuildPdf(ControllerContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }

            var fileContent = CallTheDriver(context);

            return fileContent;
        }

        public override void ExecuteResult(ControllerContext context)
        {

            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            var generatePdfResult = BuildPdf(context);

            if (generatePdfResult.AbortPrint)
            {
                var urlHelper = new UrlHelper(context.RequestContext);
                var transferID = context.RequestContext.HttpContext.Request.GetParam("TransferID");
                var generalActionID = context.RequestContext.HttpContext.Request.GetParam("GeneralActionID");
                context.HttpContext.Response.Redirect(urlHelper.Action("Print", "PrintAction", new { TransferID = transferID, GeneralActionID = generalActionID, ErrorMessage = generatePdfResult.ErrorMessages }));
                return;
            }

            var response = PrepareResponse(context.HttpContext.Response);
            response.OutputStream.Write(generatePdfResult.FileData, 0, generatePdfResult.FileData.Length);
        }

        private string createFileName()
        {
            if (string.IsNullOrEmpty(FormName) && TransferID == null)
            {
                return "";
            }

            var filename = string.Format("POINT_{0}{1}{2}{3}{4:yyyyMMddHHmmss}.pdf",
                TransferID,
                TransferID != null ? "_" : "",
                FormName.Replace(" ", ""),
                string.IsNullOrEmpty(FormName) ? "" : "_",
                DateTime.Now);

            return FileHelper.GetValidFileName(filename);
        }

        protected HttpResponseBase PrepareResponse(HttpResponseBase response)
        {
            response.ContentType = ContentType;

            var filename = createFileName();

            if (!string.IsNullOrEmpty(filename))
            {
                response.AddHeader("Content-Disposition", $"attachment; filename=\"{filename}\"");
            }

            response.AddHeader("Content-Type", ContentType);

            return response;
        }
    }
}