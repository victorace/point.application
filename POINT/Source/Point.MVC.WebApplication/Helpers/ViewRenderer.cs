﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Point.MVC.WebApplication.Helpers
{

    //https://github.com/RickStrahl/WestwindToolkit/blob/master/Westwind.Web.Mvc/Utils/ViewRenderer.cs
    public class ViewRenderer
    {

        public static T CreateController<T>(RouteData routeData = null) where T : Controller, new()
        {
            T controller = new T();

            // Create an MVC Controller Context
            var wrapper = new HttpContextWrapper(System.Web.HttpContext.Current);

            if (routeData == null)
                routeData = new RouteData();

            if (!routeData.Values.ContainsKey("controller") && !routeData.Values.ContainsKey("Controller"))
                routeData.Values.Add("controller", controller.GetType().Name.ToLower().Replace("controller", ""));

            controller.ControllerContext = new ControllerContext(wrapper, routeData, controller);
            return controller;
        }

    }

    public class StringResult : ViewResult
    {
        public StringResult()
        {
        }

        public StringResult(string viewName)
        {
            ViewName = viewName;
        }

        public string Output
        {
            get;
            private set;
        }

        public virtual void ExecuteResult(ControllerContext context, string resultControllerName)
        {
            //remember the controller name
            var controllerName = context.RouteData.Values["controller"];

            //temporarily change it 
            context.RouteData.Values["controller"] = resultControllerName;
            try
            {
                ExecuteResult(context);
            }
            finally
            {
                //restore the controller name
                context.RouteData.Values["controller"] = controllerName;
            }
        }

        public override void ExecuteResult(ControllerContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }
            if (String.IsNullOrEmpty(ViewName))
            {
                throw new ArgumentNullException("ViewName of StringResult cannot be null or empty");
            }

            ViewEngineResult result = null;

            if (View == null)
            {
                result = FindView(context);
                View = result.View;
            }

            System.Text.StringBuilder stringBuilder = new System.Text.StringBuilder();
            System.IO.TextWriter writer = new System.IO.StringWriter(stringBuilder);

            ViewContext viewContext = new ViewContext(context, View, ViewData, TempData, writer);
            View.Render(viewContext, writer);

            this.Output = stringBuilder.ToString();
        }
    }



}