﻿using System;
using System.Collections.Specialized;
using System.Web;

namespace Point.MVC.CapacityManagement.Helpers
{
    public static class RefererHelper
    {
        public const string paramReferer = "ref";

        public static void ClearReferer()
        {
            if (HttpContext.Current.Session[paramReferer] != null)
            {
                HttpContext.Current.Session.Remove(paramReferer);
            }
        }

        public static string GetReferer()
        {
            string referer = "";

            if (HttpContext.Current.Session[paramReferer] != null)
            {
                referer = HttpContext.Current.Session[paramReferer].ToString();
            }

            return referer;
        }

        public static bool HaveReferer()
        {
            return !String.IsNullOrEmpty(GetReferer());
        }
        
        public static void SetReferer(NameValueCollection parameters)
        {
            if (parameters == null || parameters.Count == 0)
            {
                return;
            }
            if (HaveReferer())
            {
                return; // keep the one received before
            }

            if (parameters[paramReferer] != null)
            {
                HttpContext.Current.Session[paramReferer] = parameters[paramReferer];
            }
        }

    }
}
