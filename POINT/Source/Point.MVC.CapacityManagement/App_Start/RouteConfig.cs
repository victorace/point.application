﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Point.MVC.CapacityManagement
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "CapacityTiles",
                url: "CapacityTiles",
                defaults: new { controller = "Capacity", action = "Tile" },
                namespaces: new[] { "Point.MVC.CapacityManagement.Controllers" }
            );

            routes.MapRoute(
                name: "CapacityList",
                url: "CapacityList",
                defaults: new { controller = "Capacity", action = "List" },
                namespaces: new[] { "Point.MVC.CapacityManagement.Controllers" }
            );

            routes.MapRoute(
                name: "CapacityMap",
                url: "CapacityMap",
                defaults: new { controller = "Capacity", action = "Map" },
                namespaces: new[] { "Point.MVC.CapacityManagement.Controllers" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{action}",
                defaults: new { controller = "Capacity", action = "Tile", id = UrlParameter.Optional },
                namespaces: new[] { "Point.MVC.CapacityManagement.Controllers" }
            );

        }
    }
}
