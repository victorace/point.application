﻿using System.Web.Optimization;

namespace Point.MVC.CapacityManagement
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/Bundles/Scripts/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/Bundles/Scripts/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/Bundles/Scripts/bootstrap").Include(
                      "~/Scripts/Bootstrap/bootstrap.js",
                      "~/Scripts/respond.js"
                ));

            //bundles.Add(new StyleBundle("~/Bundles/Styles/css").Include(
            //    //"~/Content/bootstrap.css",
            //    "~/Content/Bootstrap-Buro26/bootstrap.css",
            //    "~/Content/Site.css",
            //    "~/Content/Print.css",
            //    "~/Content/dataTables.bootstrap.css"

            //));


            bundles.Add(new StyleBundle("~/Bundles/Styles/capacity")
                .Include("~/Content/Capacity/_Linked/capacity-tile-list.css", new CssRewriteUrlTransform())
                .Include("~/Content/Site.css", new CssRewriteUrlTransform())
                .Include("~/Content/bootstrap-multiselect-point.css", new CssRewriteUrlTransform())
             );

            bundles.Add(new ScriptBundle("~/Bundles/Scripts/capacity").Include(
                "~/Scripts/Bootstrap/bootstrap-multiselect.js",
                "~/Scripts/MVC/_Linked/CapMan.js"));

            bundles.Add(new StyleBundle("~/Bundles/Styles/bootstrap")
                .Include("~/Fonts/muli-v11-latin-regular.css", new CssRewriteUrlTransform())
                .Include("~/Content/Bootstrap-Buro26/bootstrap.css", new CssRewriteUrlTransform())
                .Include("~/Content/Print.css", new CssRewriteUrlTransform())
                .Include("~/Content/dataTables.bootstrap.css", new CssRewriteUrlTransform())
            );

        }
    }
}

