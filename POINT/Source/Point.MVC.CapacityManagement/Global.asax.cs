﻿using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Point.MVC.CapacityManagement;
using Point.Database.Context;
using Point.Log.Context;
//using Point.CapacityManagement.Database.Context;

namespace Point.MVC.CapacityManagement
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            //System.Data.Entity.Database.SetInitializer<CapacityManagementContext>(null);

            System.Data.Entity.Database.SetInitializer<PointLogContext>(null);
            System.Data.Entity.Database.SetInitializer<PointContext>(null);

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}
