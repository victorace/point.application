﻿using Point.MVC.CapacityManagement.ViewModels;
using System.Web.Mvc;

namespace Point.MVC.CapacityManagement.Controllers
{
    public class ErrorController : BaseController
    {
        public ActionResult ErrorPage(ErrorViewModel model)
        {
            return View(model);
        }
    }
}