﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Routing;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Models.Enums;
using Point.MVC.Shared.Constants;

namespace Point.MVC.CapacityManagement.Controllers
{
    public sealed class CapacityController : CapacityDisplayController
    {
        private PointUserInfo _pointUserInfo;
        private Point.MVC.Shared.Controllers.CapacityController _sharedCapacityController;

        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);

            _pointUserInfo = GetPointUserInfo();
            _sharedCapacityController = new Shared.Controllers.CapacityController(this, uow, _pointUserInfo, showMutadedByUserFields: false, isPublicCapacityVersion: true);
        }

        private PointUserInfo GetPointUserInfo()
        {
            if (_pointUserInfo != null)
            {
                return _pointUserInfo;
            }

            _pointUserInfo = new PointUserInfo
            {
                Employee = new PointUserInfo.PointUserInfoEmployee {FunctionRoles = new List<FunctionRole>()},
                Region = new Region {RegionID = 0, CapacityBedsPublic = true}
            };
            var role = new FunctionRole { FunctionRoleTypeID = FunctionRoleTypeID.Reporting };
            _pointUserInfo.Employee.FunctionRoles.Add(role);
            
            return _pointUserInfo;
        }

        public ActionResult Index()
        {
            return _sharedCapacityController.Index();
        }

        public ActionResult Tile(bool showByRegion = true, bool withCapacityOnly = true, int[] regionIDs = null)
        {
            return _sharedCapacityController.Tile(showByRegion, withCapacityOnly, regionIDs);
        }

        [HttpPost]
        public JsonResult GetTileResults(bool showByRegion = true, bool withCapacityOnly = true, int[] regionIDs = null)
        {
            return _sharedCapacityController.GetTileResults(showByRegion, withCapacityOnly, regionIDs);
        }

        public ActionResult List(bool showByRegion = true, string name = null, string city = null,
            int? afterCareGroupID = null, int? afterCareTypeID = null, bool withCapacityOnly = true, int[] regionIDs = null,
            bool searched = false, int? afterCareTileGroupID = null, int? organizationTypeID = null, int pageNo = 1, string postalCode = null, int? postalCodeRadius = null)
        {
            return _sharedCapacityController.List(showByRegion, name, city, afterCareGroupID, afterCareTypeID, withCapacityOnly, regionIDs, searched, afterCareTileGroupID, organizationTypeID, pageNo, postalCode, postalCodeRadius);
        }

        [HttpPost]
        public JsonResult GetListResults(bool showByRegion = true, string name = null, string city = null,
            int? afterCareGroupID = null, int? afterCareTypeID = null, bool withCapacityOnly = true, int[] regionIDs = null, int pageNo = 1, string postalCode = null, int? postalCodeRadius = null)
        {
            return _sharedCapacityController.GetListResults(showByRegion, name, city, afterCareGroupID, afterCareTypeID, withCapacityOnly, regionIDs, pageNo, postalCode, postalCodeRadius);
        }

        [HttpPost]
        public JsonResult GetAfterCareGroupsAndTypes(bool withCapacityOnly = true, int[] regionIDs = null, int? afterCareGroupID = null, int? afterCareTypeID = null, int? organizationTypeID = null, string currentTabView = TabMenuItems.Other)
        {
            return _sharedCapacityController.GetAfterCareGroupsAndTypes(withCapacityOnly, regionIDs, afterCareGroupID, afterCareTypeID, organizationTypeID, currentTabView);
        }

        public ActionResult AfterCareTypeDescriptionsModal()
        {
            return _sharedCapacityController.AfterCareTypeDescriptionsModal();
        }

        public async Task<ActionResult> AfterCareTypeDescriptions(int organizationTypeID = (int)OrganizationTypeID.HealthCareProvider, int afterCareTileGroupID = -1)
        {
            return await _sharedCapacityController.AfterCareTypeDescriptions(organizationTypeID, afterCareTileGroupID);
        }

        public async Task<ActionResult> AfterCareTypeDescriptionsByCategory(int[] ids)
        {
            return await _sharedCapacityController.AfterCareTypeDescriptionsByCategory(ids);
        }

        public ActionResult Map(int[] regionIDs = null, int? afterCareGroupID = null, int? afterCareTypeID = null, bool withCapacityOnly = true, int? organizationTypeID = null)
        {
            return _sharedCapacityController.Map(regionIDs, afterCareGroupID, afterCareTypeID, withCapacityOnly, organizationTypeID);
        }

        [HttpPost]
        public JsonResult GetMapResults(int[] regionIDs = null, int? afterCareGroupID = null, int? afterCareTypeID = null, bool withCapacityOnly = true, int? organizationTypeID = null)
        {
            return _sharedCapacityController.GetMapJsonResults(regionIDs, afterCareGroupID, afterCareTypeID, withCapacityOnly, organizationTypeID);
        }

    }

}