﻿using System;
using System.IO;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Point.Infrastructure.Helpers;

namespace Point.MVC.CapacityManagement.Controllers
{
    [RequireHttps]
    public class BaseController : Controller
    {
        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            logReadAction(filterContext);

            base.OnActionExecuted(filterContext);
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            if (filterContext.ExceptionHandled)
            {
                return;
            }

            var errorNumber = logException(filterContext.Exception, filterContext.HttpContext);

            var forceerrorhandling = ConfigHelper.GetAppSettingByName<bool>("ForceErrorHandling");
            if (!HttpContext.IsDebuggingEnabled || forceerrorhandling)
            {
                filterContext.HttpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                filterContext.ExceptionHandled = true;

                if (filterContext.HttpContext.Request.IsAjaxRequest())
                {
                    filterContext.Result =
                        Json(new {ErrorMessage = filterContext.Exception.Message, ErrorNumber = errorNumber},
                            JsonRequestBehavior.AllowGet);
                }
                else
                {
                    filterContext.Result = new RedirectResult("~/error/errorpage?errornumber=" + errorNumber);
                }
            }
        }

        private int logException(Exception exception, HttpContextBase httpcontext)
        {
            if (httpcontext?.Request == null)
            {
                return -1;
            }

            return 0;

            // TODO: XXX
            //var httpRequestBase = httpcontext.Request;
            //var postdata = getPostData(httpRequestBase);
            //var cookies = getAllCookies(httpRequestBase);
            //var url = httpRequestBase.Url?.AbsoluteUri;
            //var queryString = httpRequestBase.Url?.Query;
            //return ExceptionHelper.SendMessageException(exception, url, queryString, postdata, cookies, httpRequestBase.UserAgent, httpRequestBase.IsAjaxRequest());
        }

        private void logReadAction(ActionExecutedContext filterContext)
        {
            // TODO: XXX
            //var logReadActions = ConfigHelper.GetAppSettingByName<bool>("LogReadActions");
            //if (filterContext != null && !filterContext.IsChildAction && logReadActions)
            //{
            //    if (filterContext.HttpContext?.Request != null)
            //    {
            //        var httpRequestBase = filterContext.HttpContext.Request;
            //        var postData = getPostData(httpRequestBase);
            //        var cookies = getAllCookies(httpRequestBase);
            //        //using (var context = new CapacityManagementContext())
            //        using (var context = new UnitOfWork<PointContext>())
            //        {
            //            var logRead = new LogReadBL(context);
            //            var url = httpRequestBase.Url?.AbsoluteUri;
            //            var urlReferrer = httpRequestBase.UrlReferrer?.AbsoluteUri;
            //            var queryString = httpRequestBase.Url?.Query;
            //            logRead.Insert(url, urlReferrer, queryString, postData, cookies, httpRequestBase.UserAgent, httpRequestBase.IsAjaxRequest());
            //        }
            //    }
            //}
        }

        private string getAllCookies(HttpRequestBase httpRequestBase)
        {
            if (httpRequestBase.Cookies == null)
            {
                return null;
            }

            string cookies = null;
            foreach (var cookiekey in httpRequestBase.Cookies.AllKeys)
            {
                var httpCookie = httpRequestBase.Cookies[cookiekey];
                if (httpCookie != null)
                {
                    cookies += $"{cookiekey}={httpCookie.Value}&";
                }
            }
            cookies = cookies?.TrimEnd('&');

            return cookies;
        }

        private string getPostData(HttpRequestBase httpRequestBase)
        {
            if (httpRequestBase?.InputStream == null)
            {
                return null;
            }

            var streamreader = new StreamReader(httpRequestBase.InputStream);
            return streamreader.ReadToEnd();
        }
    }
}
