﻿using System.Web.Mvc;
using Point.Database.Context;
using Point.Database.Repository;
using Point.MVC.CapacityManagement.Helpers;

namespace Point.MVC.CapacityManagement.Controllers
{
    /// <summary>
    /// Controllers for displaying the capacity data (as opposed to updating it).
    /// </summary>
    public class CapacityDisplayController : BaseController
    {
        public readonly UnitOfWork<PointContext> uow = new UnitOfWork<PointContext>();

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            RefererHelper.SetReferer(filterContext.HttpContext.Request.Params);

            base.OnActionExecuting(filterContext);
        }

    }

}
