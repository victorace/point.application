using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using Point.Infrastructure;
using Point.Infrastructure.Extensions;
using Point.Database.Models;
using Point.Database.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Database.Extensions;
using Point.Business.Logic;

namespace Point.Business
{
    public static class TransferExpressions
    {
        public static Expression<Func<Transfer, bool>> ByType(DossierType type, int employeeId)
        {
            switch (type)
            {
                case DossierType.All:
                default:
                    return t => true;
            }
        }

        public static Expression<Func<Transfer, bool>> ByHandling(DossierHandling handling, int employeeId, IEnumerable<int> departmentIDs)
        {
            switch (handling)
            {
                // FUTURE like FlowInstanceExpressions.ByHandling but for Legacy Transfer
                default:
                    return t => true;
            }
        }

        public static Expression<Func<Transfer, bool>> ByStatus(DossierStatus status)
        {
            int iDone = (int)Status.Done;
            int iActive = (int)Status.Active;

            switch (status)
            {
                case DossierStatus.Active:
                    return t => t.Phase10 < iDone;

                case DossierStatus.Interrupted:
                    return t => t.DischargeInterruptionTransferProcess.HasValue && !t.DischargeResumeTransferProcess.HasValue;

                case DossierStatus.CanBeTransferred:
                    return t => t.Phase9 == iActive && t.DischargePatientAcceptedYN.StartsWith("0");

                case DossierStatus.CannotBeTransferred:
                    return t => t.Phase8 == 2 && t.Phase10 == 0 && t.DischargePatientAcceptedYN.StartsWith("1");

                case DossierStatus.CanBeClosed:
                    return t => t.Phase10 == iActive;

                case DossierStatus.IsClosed:
                    return t => t.Phase10 == iDone;

                case DossierStatus.ActiveOrClosedWithin14days:
                    DateTime lessthen14days = DateTime.Now.Date.AddDays(-14);
                    return t => (t.Phase10 == iDone && t.DischargeDecisionKnownDate > lessthen14days)
                                ||
                                t.Phase10 != iDone;

                case DossierStatus.All:
                default:
                    return t => true;
            }
        }

        public static Expression<Func<Transfer, bool>> ByTransfer(int transferID)
        {
            return t => t.TransferID == transferID;
        }

        public static Expression<Func<Transfer, bool>> ByUserRole(PointUserInfo pointUserInfo)
        {
            Aspnet_Roles aspnetRoles = AspNet_RolesBL.GetPointRole(pointUserInfo.Roles);

            if (aspnetRoles == null)
                return t => false;

            Role role;
            Enum.TryParse<Role>(aspnetRoles.RoleName, true, out role);

            IEnumerable<int> departments;
            IEnumerable<int> organizations;

            int[] formTypeIDs = new int[] {28, 29};

            switch (role)
            {
                case Role.TransferpointEmployee:
                case Role.TransferpointServicesEmployee:
                case Role.TransferpointAdministrator:
                    departments = (role == Role.TransferpointServicesEmployee ? pointUserInfo.EmployeeDepartmentIDs : Enumerable.Empty<int>());
                    departments = departments.Concat<int>(new[] { pointUserInfo.Department.DepartmentID });
                    return t => (departments.Contains((int)t.DepartmentID) || departments.Contains((int)t.RequestDepartmentID));

                case Role.HealthCareProviderOrganizationAdministrator:
                case Role.HealthCareProviderAdministrator:
                case Role.HealthCareProviderEmployee:
                    return t => (t.Phase6 > 1 || (t.FormSet.Any(fs => fs.FormType.TypeID == 1)
                                                  && formTypeIDs.Contains(t.FormSet.Where(fs => fs.FormType.TypeID == 1).OrderByDescending(fs => fs.TimeStamp).FirstOrDefault().FormTypeID)))
                                 && t.Agreement == true
                                 && (t.DischargeRealizedOrganizationID == pointUserInfo.Organization.OrganizationID 
                                     || t.RequestDesiredOrganisationID1 == pointUserInfo.Organization.OrganizationID);

                case Role.HealthCareProviderClientServices:
                    organizations = pointUserInfo.EmployeeOrganizationIDs;
                    organizations = organizations.Concat<int>(new[] { pointUserInfo.Organization.OrganizationID });
                    return t => (t.Phase7 > 0 || (t.FormSet.Any(fs => fs.FormType.TypeID == 1)
                                                  && formTypeIDs.Contains(t.FormSet.Where(fs => fs.FormType.TypeID == 1).OrderByDescending(fs => fs.TimeStamp).FirstOrDefault().FormTypeID)))
                                 && t.Agreement == true
                                 && (organizations.Contains((int)t.DischargeRealizedOrganizationID) 
                                     || organizations.Contains((int)t.RequestDesiredOrganisationID1));
                    
                case Role.HospitalDepartmentAdministrator:
                    return t => t.DepartmentID == pointUserInfo.Department.DepartmentID;

                case Role.OrganizationAdministrator:
                    return t => t.Department.Location.OrganizationID == pointUserInfo.Organization.OrganizationID;

                case Role.PointAdministrator:
                    var regionID = pointUserInfo.Region.RegionID;
                    return t => pointUserInfo.IsTechxxAdmin ? true : t.Department.Location.Organization.RegionID == regionID;

                case Role.CIZOrganizationAdministrator:
                case Role.CIZAdministrator:
                case Role.CIZEmployee:
                    return t => t.Phase4 >= 1
                                && t.Agreement == true
                                && t.IndicationOrganizationID == pointUserInfo.Organization.OrganizationID;

                case Role.HospitalDepartmentEmployee:
                    departments = pointUserInfo.EmployeeDepartmentIDs;
                    departments = departments.Concat<int>(new[] { pointUserInfo.Department.DepartmentID });
                    return t => departments.Contains((int)t.DepartmentID) || departments.Contains((int)t.RequestDepartmentID);

                case Role.HealthCareProviderIntensiveRehabilitationEmployee:
                    var organizationID = pointUserInfo.Organization.OrganizationID;
                    return t => (t.Phase91 >= 1
                                 || t.Phase6 > 1
                                 || (t.FormSet.Any(fs => fs.FormType.TypeID == 1)
                                     && formTypeIDs.Contains(t.FormSet.Where(fs => fs.FormType.TypeID == 1).OrderByDescending(fs => fs.TimeStamp).FirstOrDefault().FormTypeID)))
                                && t.Agreement == true
                                && (t.DischargeRealizedOrganizationID == organizationID || t.RequestDesiredOrganisationID1 == organizationID);

                default:
                    return t => false;
            }
        }
    }
}
