﻿using Point.Models.Enums;
using Point.Infrastructure.Exceptions;
using System;
using System.Web;
using System.Web.Mvc;

namespace Point.Business.Attributes
{
    public class AuthorizeRoles : AuthorizeAttribute
    {

        private readonly Role[] roles;
        private readonly string exceptionmessage = "";

        public AuthorizeRoles(params Role[] roles)
        {
            this.roles = roles;
        }

        public AuthorizeRoles(string exceptionMessage, params Role[] roles)
        {
            this.roles = roles;
            exceptionmessage = exceptionMessage;
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {

            if (!base.AuthorizeCore(httpContext)) return false;

            var user = httpContext.User;

            foreach (var role in roles)
            {
                if (user.IsInRole(role.ToString())) return true;
            }

            return false;

        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (String.IsNullOrWhiteSpace(exceptionmessage))
                throw new PointSecurityException(ExceptionType.Default);
            throw new PointSecurityException(exceptionmessage, ExceptionType.Default);
        }
    }
}
