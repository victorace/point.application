﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Ionic.Zip;
using System.Reflection;
using System.Linq;
using Fasterflect;
using Point.Database.Attributes;
using Point.Database.Models.ViewModels;
using Point.Infrastructure.Helpers;
using Point.Models.Enums;
using Point.Business.Logic;
using Point.Infrastructure.Extensions;

namespace Point.Business.Helper
{
    public class CSVHelper
    {
        private const char _columnSeparator = ';';

        public class Column
        {
            public PropertyInfo PropertyInfo { get; set; }
            public int Position { get; set; }
        }

        private List<Column> GetColumns(IEnumerable<PropertyInfo> properties, int organizationID, CSVTemplateTypeID csvTemplateTypeID)
        {
            int i = 0;
            var columns = new List<Column>();

            foreach (var property in properties)
            {
                var ignoreAttribute = property.GetCustomAttribute<CSVIgnoreAttribute>();
                if (ignoreAttribute != null)
                {
                    continue;
                }

                i++;
                columns.Add(new Column() { PropertyInfo = property, Position = i });
            }

            columns = OrganizationCSVTemplateBL.GetTemplatedColumns(columns, organizationID, csvTemplateTypeID);

            return columns.OrderBy(c => c.Position).ToList();
        }

        private string GetHeaderRowFromColumns(List<Column> columns)
        {
            var header = "";
            foreach (var column in columns)
            {
                header += "\"" + column.PropertyInfo.Name + "\"" + _columnSeparator;
            }
            return header;
        }

        public byte[] GenerateCSVFromEnumarable(IEnumerable<object> data, Type type, int organizationID, CSVTemplateTypeID csvTemplateTypeID)
        {
            var encoding = Encoding.GetEncoding("Windows-1250");

            using (var ms = new MemoryStream())
            {
                using (var sw = new StreamWriter(ms, encoding, 1024))
                {
                    //create text file.
                    sw.AutoFlush = true;
                    sw.WriteLine("sep=" + _columnSeparator);
                    var properties = type.GetProperties();
                    var columns = GetColumns(properties, organizationID, csvTemplateTypeID);
                    GenerateGetters(columns, type);
                    var headerrow = GetHeaderRowFromColumns(columns);

                    sw.WriteLine(headerrow);
                    foreach (var row in data)
                    {
                        sw.WriteLine(CreateRowTransferForDump(columns, row));
                    }

                    return ms.ToArray();
                }
            }
        }

        public byte[] GenerateCSVZipFromEnumarable(IEnumerable<object> data, Type type, string fileName, int organizationID, CSVTemplateTypeID csvTemplateTypeID)
        {
            var encoding = Encoding.GetEncoding("Windows-1250");

            using (var ms = new MemoryStream())
            {
                using (var sw = new StreamWriter(ms, encoding, 1024))
                {
                    //create text file.
                    sw.AutoFlush = true;
                    sw.WriteLine("sep=" + _columnSeparator);
                    var properties = type.GetProperties();
                    var columns = GetColumns(properties, organizationID, csvTemplateTypeID);
                    GenerateGetters(columns, type);
                    var headerrow = GetHeaderRowFromColumns(columns);

                    sw.WriteLine(headerrow);
                    foreach (var row in data)
                    {
                        sw.WriteLine(CreateRowTransferForDump(columns, row));
                    }

                    ms.Position = 0;
                    //create zip file.
                    using (MemoryStream outputMS = new MemoryStream())
                    {
                        using (ZipOutputStream zipOutput = new ZipOutputStream(outputMS))
                        {
                            zipOutput.PutNextEntry(fileName);
                            zipOutput.Write(ms.ToArray(), 0, Convert.ToInt32(ms.Length));
                        }

                        return outputMS.ToArray();
                    }
                }
            }
        }

        private Dictionary<string, MemberGetter> GetterCache = new Dictionary<string, MemberGetter>();
        private void GenerateGetters(List<Column> columns, Type type)
        {
            foreach (var column in columns)
            {
                GetterCache.Add(column.PropertyInfo.Name, type.DelegateForGetPropertyValue(column.PropertyInfo.Name));
            }
            
        }

        private string CreateRowTransferForDump(IEnumerable<Column> columns, object row)
        {
            var GetterCacheArray = GetterCache.ToArray();

            string rowTransfer = "";
            int i = 0;
            foreach (var column in columns)
            {
                var value = GetterCacheArray[i].Value(row);
                if (value != null)
                {
                    rowTransfer += '"' + value.ConvertTo<string>() + '"' + _columnSeparator;
                }
                else
                {
                    rowTransfer += "\"\"" + _columnSeparator;
                }

                i++;
            }

            return rowTransfer;
        }


        public static List<Dictionary<string, object>> ReadFromLocalStorage(List<CsvColumnIdentifier> columnIndentifiers, string fileFullName, bool deleteOnCompletion = false)
        {
            if (string.IsNullOrEmpty(fileFullName) || !File.Exists(fileFullName))
            {
                return null;
            }

            var isFirst = true;
            var csvColumnIndices = new Dictionary<string, int>();
            var lines = new List<Dictionary<string, object>>();

            foreach (var line in File.ReadLines(fileFullName))
            {
                if (isFirst)
                {
                    // Line 0 contains the column headers. Determine the columns' indices.
                    csvColumnIndices = MapColumnIndices(columnIndentifiers, line);
                    if (csvColumnIndices == null)
                    {
                        // Could not map columns;
                        return null;
                    }
                    isFirst = false;
                }
                else
                {
                    // Content
                    var lineValues = MapLineValues(csvColumnIndices, line);

                    if (lineValues != null)
                    {
                        lines.Add(lineValues);
                    }
                }
            }

            if (deleteOnCompletion)
            {
                FileHelper.DeleteFileAsync(fileFullName);
            }

            return lines;
        }
        
        private static bool ValidateSeparator(int numberOfColumns, string csvLine)
        {
            var minCount = numberOfColumns - 1;

            if (csvLine.Count(x => x == _columnSeparator) < minCount)
            {
                // Could not determine used separator in csv file
                return false;
            }

            return true;
        }

        private static Dictionary<string, int> MapColumnIndices(List<CsvColumnIdentifier> columnIndentifiers, string csvLine)
        {
            var csvColumnIndices = new Dictionary<string, int>();
            var fieldDefinitionsFromCsvLine = csvLine.ToUpperInvariant().Split(_columnSeparator).ToList();

            foreach (var col in columnIndentifiers)
            {
                var index = fieldDefinitionsFromCsvLine.IndexOf(col.NameUpper);
                if (index > -1)
                {
                    csvColumnIndices.Add(fieldDefinitionsFromCsvLine[index], index);
                }
                else
                {
                    if (col.IsRequired)
                    {
                        return null;
                    }
                }
            }

            return csvColumnIndices;
        }

        private static Dictionary<string, object> MapLineValues(Dictionary<string, int> csvColumnIndices, string csvLine)
        {
            if (!ValidateSeparator(csvColumnIndices.Count, csvLine))
            {
                return null;
            }

            var fieldsFromCsvLine = csvLine.Split(_columnSeparator);

            if (csvColumnIndices.Count != fieldsFromCsvLine.Length)
            {
                // Wrong number of fields
                return null;
            }

            var fieldValues = new Dictionary<string, object>();

            foreach (var col in csvColumnIndices)
            {
                fieldValues.Add(col.Key, fieldsFromCsvLine[col.Value]);
            }

            return fieldValues;
        }
        
    }
}
