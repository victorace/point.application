﻿using System;
using System.Collections.Generic;
using System.Linq;
using HtmlAgilityPack;
using Point.Infrastructure.Constants;

namespace Point.Business.Helper
{
    public class HtmlFilterHelper
    {
        private static string[] valueclasses = { "flowfield", "pointfield", "form-control", "print-control" };

        public static string FilterEmptyValues(string inputHtml)
        {
            string output = inputHtml;
            

            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(inputHtml);

            //Cant render input-group-addons
            foreach (HtmlNode formgroup in doc.DocumentNode.Descendants().Where(desc => desc.GetAttributeValue("class", "").Contains("form-group")).ToList())
            {
                bool formgroupcontainer = false;

                if (formgroup.GetAttributeValue("class","").Contains("form-group-container"))
                {
                    formgroupcontainer = true;
                }

                var parentignorefilter = FindParentByClassName(formgroup, "ignore-printfilter");
                var classValue = formgroup.GetAttributeValue("class", "");
                if (parentignorefilter != null || classValue.Contains("ignore-printfilter"))
                {
                    continue;
                }

                if (classValue.Contains(ViewFunctionalClasses.NoFilter))
                {
                    continue;
                }
                
                bool candelete = true;
                
                foreach (HtmlNode flowcontrol in formgroup.Descendants().Where(desc => desc.GetAttributeValue("class", "").Split(' ').Any(it => valueclasses.Contains(it))).ToList())
                {
                    string val = flowcontrol.GetAttributeValue("value", "");
                    string id = flowcontrol.GetAttributeValue("id", "");
                    string type = flowcontrol.GetAttributeValue("type", "");
                    string innerval = flowcontrol.InnerText;
                    string name = flowcontrol.Name;

                    if (type == "radio" || type == "checkbox")
                    {
                        if (flowcontrol.Attributes.Contains("checked"))
                        {
                            candelete = false;
                        }
                        else if (formgroupcontainer)
                        {
                            var parentdiv = flowcontrol?.ParentNode?.ParentNode;
                            if (parentdiv != null)
                            {
                                if (type == "checkbox" ||
                                   (type == "radio" && !flowcontrol.ParentNode.ParentNode.Descendants().Any(desc => desc.Attributes.Any(a => a.Name == "checked"))))
                                {
                                    parentdiv.Remove();
                                }
                            }
                        }
                    }
                    else if (name == "span")
                    {
                        if (!String.IsNullOrEmpty(innerval))
                        {
                            candelete = false;
                        }
                    }
                    else if (name == "select")
                    {
                        if (flowcontrol.Descendants().Where(desc => desc.Attributes.Contains("selected") && desc.GetAttributeValue("value", "") != "").Count() > 0)
                        {
                            candelete = false;
                        }
                    }
                    else
                    {
                        if (!String.IsNullOrWhiteSpace(innerval) || !String.IsNullOrWhiteSpace(val))
                        {
                            candelete = false;
                        }
                    }
                }

                if (candelete)
                {
                    formgroup.Remove();
                }
            }

            //Cant render buttons
            foreach (HtmlNode flowcontrol in doc.DocumentNode.Descendants().Where(desc => (desc.GetAttributeValue("type", "") == "button") || desc.GetAttributeValue("class", "").Contains("btn ")).ToList())
            {
                flowcontrol.Remove();
            }

            //Cant render input-group-addons
            foreach (HtmlNode flowcontrol in doc.DocumentNode.Descendants().Where(desc => desc.GetAttributeValue("class", "").Contains("input-group-addon")).ToList())
            {
                flowcontrol.Remove();
            }

            //Clear tables where there are no form-groups left
            foreach (HtmlNode table in doc.DocumentNode.Descendants().Where(desc => desc.Name.ToLower() == "table").ToList())
            {
                if (FindChildrenValues(table).Count == 0)
                {
                    table.Remove();
                }
            }

            //Clear all empty h1,2,3 "groups" (See Eoverdracht)
            foreach (HtmlNode flowcontrol in doc.DocumentNode.Descendants().Where(desc => (new string[] { "h1", "h2", "h3", "h4" }).Contains(desc.Name.ToLower())).ToList())
            {
                if (FindChildrenByClassName(flowcontrol.ParentNode, "form-group").Count == 0 && FindChildrenValues(flowcontrol.ParentNode).Count == 0)
                {
                    flowcontrol.ParentNode.Remove();
                }
            }

            //Clean entire Section's where there are no form-groups are present (See Eoverdracht)
            foreach (HtmlNode section in doc.DocumentNode.Descendants().Where(desc => desc.Name.ToLower() == "section" || desc.GetAttributeValue("class", "").Contains("section")).ToList())
            {
                if (FindChildrenByClassName(section, "form-group").Count == 0 && FindChildrenValues(section).Count == 0)
                {
                    section.Remove();
                }
            }

            //Vervang alle form-controls die over blijven naar print-controls (vervangt inputs naar text/div)
            foreach (HtmlNode flowcontrol in doc.DocumentNode.Descendants().Where(desc => desc.GetAttributeValue("class", "").Contains("form-control")).ToList())
            {
                var name = flowcontrol.Name.ToLower();
                var type = flowcontrol.GetAttributeValue("type", "");
                var classvalue = flowcontrol.GetAttributeValue("class", "");

                if (name == "input" && type == "text")
                {
                    string value = flowcontrol.GetAttributeValue("value", "");

                    flowcontrol.Name = "div";
                    if (!String.IsNullOrEmpty(classvalue))
                    {
                        flowcontrol.Attributes.FirstOrDefault(at => at.Name == "class").Value = classvalue.Replace("form-control", "print-control");
                    }

                    flowcontrol.InnerHtml = value;
                }

                if (name == "textarea")
                {
                    flowcontrol.Name = "div";
                    if (!String.IsNullOrEmpty(classvalue))
                    {
                        flowcontrol.Attributes.FirstOrDefault(at => at.Name == "class").Value = classvalue.Replace("form-control", "print-control");
                    }
                }
            }

            return doc.DocumentNode.OuterHtml;
        }
        
        private static List<HtmlNode> FindChildrenValues(HtmlNode node)
        {
            return node.Descendants().Where(desc => desc.GetAttributeValue("class", "").Split(' ').Any(it => valueclasses.Contains(it))).ToList();
        }

        private static List<HtmlNode> FindChildrenByClassName(HtmlNode node, string classname)
        {
            return node.Descendants().Where(desc => desc.GetAttributeValue("class", "").Contains(classname)).ToList();
        }

        private static HtmlNode FindParentByClassName(HtmlNode node, string classname)
        {
            if (node.ParentNode == null)
            {
                return null;
            }
            if (node.ParentNode.GetAttributeValue("class", "").Contains(classname))
            {
                return node.ParentNode;
            }

            if (node.ParentNode.ParentNode == null)
            {
                return null;
            }
            if (node.ParentNode.ParentNode.GetAttributeValue("class", "").Contains(classname))
            {
                return node.ParentNode.ParentNode;
            }

            if (node.ParentNode.ParentNode.ParentNode == null)
            {
                return null;
            }
            if (node.ParentNode.ParentNode.ParentNode.GetAttributeValue("class", "").Contains(classname))
            {
                return node.ParentNode.ParentNode.ParentNode;
            }

            if (node.ParentNode.ParentNode.ParentNode.ParentNode == null)
            {
                return null;
            }
            if (node.ParentNode.ParentNode.ParentNode.ParentNode.GetAttributeValue("class", "").Contains(classname))
            {
                return node.ParentNode.ParentNode.ParentNode.ParentNode;
            }

            if (node.ParentNode.ParentNode.ParentNode.ParentNode.ParentNode == null)
            {
                return null;
            }
            if (node.ParentNode.ParentNode.ParentNode.ParentNode.ParentNode.GetAttributeValue("class", "").Contains(classname))
            {
                return node.ParentNode.ParentNode.ParentNode.ParentNode.ParentNode;
            }

            if (node.ParentNode.ParentNode.ParentNode.ParentNode.ParentNode.ParentNode == null)
            {
                return null;
            }
            if (node.ParentNode.ParentNode.ParentNode.ParentNode.ParentNode.ParentNode.GetAttributeValue("class", "").Contains(classname))
            {
                return node.ParentNode.ParentNode.ParentNode.ParentNode.ParentNode.ParentNode;
            }

            return null;
        }
    }
}
