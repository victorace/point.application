﻿using Point.Business.Logic;
using Point.Models.Enums;
using Point.Database.Repository;
using System.Linq;

namespace Point.Business.Helper
{
    public class PatientHelper
    {
        public static AcceptanceState? IsAccepted(IUnitOfWork uow, int flowinstanceid)
        {
            var lastOrganizationInvite = OrganizationInviteBL.GetInvitesToBeAcknowledgedByFlowInstanceID(uow, flowinstanceid).OrderByDescending(oi => oi.InviteSend).FirstOrDefault();
            switch (lastOrganizationInvite?.InviteStatus)
            {
                case InviteStatus.Acknowledged:
                    return AcceptanceState.Acknowledged;

                case InviteStatus.AcknowledgedPartially:
                    return AcceptanceState.AcknowledgedPartially;

                case InviteStatus.Refused:
                    return AcceptanceState.Refused;

                default:
                    return null;
            }
        }
    }
}
