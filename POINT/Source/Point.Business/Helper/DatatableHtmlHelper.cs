﻿using System;
using System.Data;
using System.Text;
using Point.Business.Logic;
using Point.Database.Extensions;
using Point.Database.Repository;

namespace Point.Business.Helper
{
    public static class DatatableHtmlHelper
    {
        public static string GenerateGenericHistoryHTMLTable(IUnitOfWork uow, DataTable dataTable)
        {
            StringBuilder html = new StringBuilder();

            if ((dataTable.Rows.Count > 0))
            {
                int rowCount = dataTable.Rows.Count - 1;
                int columnCount = dataTable.Columns.Count - 1;

                html.Append("<table id='HistoryTable'><thead><tr>");

                for (int x = 0; x <= columnCount; x++)
                {
                    string header = "<div>" + dataTable.Columns[x].Caption + "</div>";
                    html.Append("<th class='historyheader' title='" + dataTable.Columns[x].Caption + "'>" + header + "</th>");
                }

                html.Append("</tr></thead><tbody>");

                for (int y = 0; y <= rowCount; y++)
                {
                    if ((y == rowCount) || !isRowEqual(dataTable, dataTable.Rows[y], dataTable.Rows[y + 1]))
                    {
                        html.Append("<tr>");

                        for (int x = 0; x <= columnCount; x++)
                        {
                            var classname = "cell";
                            if ((x > 1))
                            {
                                if ((y < rowCount))
                                {
                                    if (dataTable.Rows[y][x] != null && dataTable.Rows[y][x] != DBNull.Value)
                                    {
                                        classname += " changed";
                                    }
                                }
                            }

                            string value = null; 
                            if(dataTable.Rows[y][x] != DBNull.Value)
                            {
                                value = dataTable.Rows[y][x].ToString();
                            }

                            if (dataTable.Columns[x].ColumnName == "DestinationHospitalLocation")
                            {
                                if (int.TryParse(dataTable.Rows[y][x].ToString(), out int destinationhospitallocation))
                                {
                                    value = LocationBL.GetByID(uow, destinationhospitallocation).Name;
                                }

                            }
                            if (dataTable.Columns[x].ColumnName == "DestinationHospital")
                            {
                                if (int.TryParse(dataTable.Rows[y][x].ToString(), out int destinationhospital))
                                {
                                    value = OrganizationBL.GetByOrganizationID(uow, destinationhospital).Name;
                                }
                            }
                            if (dataTable.Columns[x].ColumnName == "DossierOwner")
                            {
                                if (int.TryParse(dataTable.Rows[y][x].ToString(), out int dossierowner))
                                {
                                    value = EmployeeBL.GetByID(uow, dossierowner).FullName();
                                }
                            }

                            if (value == null) value = "-";
                            var title = value;
                            if (title.Contains("href="))
                            {
                                title = "Download";
                            }

                            html.Append("<td class='historyvalue " + classname + "' title='" + title + "'>");
                            string historyvalue = "<div>" + value + "</div>";
                            html.Append(historyvalue);
                            html.Append("</td>");
                        }

                        html.Append("</tr>");
                    }
                }
                html.Append("</tbody></table>");

            }

            return html.ToString();
        }


        public static string GenerateHistoryHTMLTable(IUnitOfWork uow, DataTable dataTable)
        {
            StringBuilder html = new StringBuilder();

            if ((dataTable.Rows.Count > 0))
            {
                int rowCount = dataTable.Rows.Count - 1;
                int columnCount = dataTable.Columns.Count - 1;

                html.Append("<table id='HistoryTable' class='table table-striped table-condensed'><thead><tr>");

                for (int x = 0; x <= columnCount; x++)
                {
                    string header = "<div class='historyheader'>" + dataTable.Columns[x].Caption + "</div>";
                    html.Append("<th title='" + dataTable.Columns[x].Caption + "'>" + header + "</th>");
                }

                html.Append("</tr></thead><tbody>");

                for (int y = 0; y <= rowCount; y++)
                {
                    if ((y == rowCount) || !isRowEqual(dataTable, dataTable.Rows[y], dataTable.Rows[y + 1]))
                    {
                        html.Append("<tr>");

                        for (int x = 0; x <= columnCount; x++)
                        {
                            var classname = "cell";
                            if ((x > 1))
                            {
                                if ((y < rowCount))
                                {
                                    if ((dataTable.Rows[y][x].ToString() != dataTable.Rows[y + 1][x].ToString()))
                                        classname += " changed";
                                }                              
                            }

                            string value = dataTable.Rows[y][x].ToString();
                            if (dataTable.Columns[x].ColumnName == "DestinationHospitalLocation")
                            {
                                int destinationhospitallocation = 0;
                                if (int.TryParse(dataTable.Rows[y][x].ToString(), out destinationhospitallocation))
                                {
                                    value = LocationBL.GetByID(uow, destinationhospitallocation).Name;
                                }

                            }
                            if (dataTable.Columns[x].ColumnName == "DestinationHospital")
                            {
                                int destinationhospital = 0;
                                if (int.TryParse(dataTable.Rows[y][x].ToString(), out destinationhospital))
                                {
                                    value = OrganizationBL.GetByOrganizationID(uow, destinationhospital).Name;
                                }
                            }
                            if (dataTable.Columns[x].ColumnName == "DossierOwner")
                            {
                                int dossierowner = 0;
                                if (int.TryParse(dataTable.Rows[y][x].ToString(), out dossierowner))
                                {
                                    value = EmployeeBL.GetByID(uow, dossierowner).FullName();
                                }
                            }

                            if (String.IsNullOrEmpty(value)) value = "-";
                            var title = value;

                            if(title.Contains("href="))
                            {
                                title = "Download";
                            }

                            html.Append("<td class='" + classname + "' title='" + title + "'>");
                            string historyvalue = "<div class='historyvalue'>" + value + "</div>";
                            html.Append(historyvalue);
                            html.Append("</td>");
                        }

                        html.Append("</tr>");
                    }
                }
                html.Append("</tbody></table>");

            }

            return html.ToString();
        }

        private static bool isRowEqual(DataTable table, DataRow row1, DataRow row2)
        {
            foreach (DataColumn column in table.Columns)
            {
                if (column.ColumnName != "FlowFieldID" || column.ColumnName != "ModificationDate" || column.ColumnName != "ModifiedBy")
                {
                    if (row1[column].ToString() != row2[column].ToString()) return false;
                }
            }

            return true;
        }
    }
}
