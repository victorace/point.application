﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Models.Enums;

namespace Point.Business.Helper
{
    public class FlowWebFieldValidator
    {
        private FlowWebField flowWebField;
        private IEnumerable<FlowWebField> flowWebFieldsTotal;

        public string ErrorMessage
        {
            get;
            private set;
        }

        public FlowWebFieldValidator(FlowWebField flowWebField, IEnumerable<FlowWebField> flowWebFieldsTotal)
        {
            this.flowWebField = flowWebField;
            this.flowWebFieldsTotal = flowWebFieldsTotal;
        }

        public bool IsValid()
        {
            ErrorMessage = null;

            if (flowWebField == null) throw new ArgumentNullException();

            if (flowWebField.Type == FlowFieldType.None || flowWebField.Type >= FlowFieldType.Group) return true;

            if (flowWebField.ReadOnly || !flowWebField.Visible) return true;

            if (!flowWebField.Required && String.IsNullOrWhiteSpace(flowWebField.DisplayValue)) return true;

            if (flowWebField.Required && String.IsNullOrWhiteSpace(flowWebField.DisplayValue))
            {
                bool skiprequired = false;
                if (!String.IsNullOrEmpty(flowWebField.RequiredBy))
                {
                    var requiredby = flowWebFieldsTotal.FirstOrDefault(fwft => fwft.Name == flowWebField.RequiredBy);
                    if (requiredby != null)
                    {
                        List<string> requiredbyvalues = new List<string>();

                        if (!String.IsNullOrEmpty(flowWebField.RequiredByValue))
                        {
                            try
                            {
                                var requiredbyvalue = JsonConvert.DeserializeObject<RequiredByValue>(flowWebField.RequiredByValue);
                                requiredbyvalues = requiredbyvalue.Values.ToList();
                            }
                            catch
                            {
                                throw new Exception(string.Format("Unable to parse the 'RequiredByValue' property for the field '{0}'.", flowWebField.Name));
                            }
                        }

                        //Parent moet simpelweg een waarde hebben en deze is leeg
                        if (!requiredbyvalues.Any() && String.IsNullOrEmpty(requiredby.Value))
                            skiprequired = true;

                        if (requiredbyvalues.Any() && !requiredbyvalues.Contains(requiredby.Value))
                            skiprequired = true;

                        //Parent matcht met verwachte waarde maar ingevulde waarde van child is leeg
                        if (requiredbyvalues.Contains(requiredby.Value) && !String.IsNullOrEmpty(flowWebField.DisplayValue))
                        {
                            ErrorMessage = BaseEntity.ErrorMessage(BaseEntity.errorRequiredBy, new object[] { flowWebField.Label, requiredby.Label });
                            return false;
                        }

                        //Geen verdere validatie omdat hij niet required is en leeg
                        if (skiprequired && String.IsNullOrWhiteSpace(flowWebField.DisplayValue))
                            return true;

                        if (!skiprequired)
                        {
                            ErrorMessage = BaseEntity.ErrorMessage(BaseEntity.errorRequired, new object[] { flowWebField.Label });
                            return false;
                        }
                    }
                }
            }

            if (flowWebField.MaxLength.HasValue)
            {
                if (flowWebField.DisplayValue.Length > flowWebField.MaxLength.Value)
                {
                    ErrorMessage = BaseEntity.ErrorMessage(BaseEntity.errorMaxLength, new object[] { flowWebField.Label, flowWebField.MaxLength });
                    return false;
                }
            }

            if (!String.IsNullOrWhiteSpace(flowWebField.Regexp))
            {
                Match match = Regex.Match(flowWebField.DisplayValue, flowWebField.Regexp);
                if (!match.Success)
                {
                    ErrorMessage = BaseEntity.ErrorMessage(BaseEntity.errorRegexp, new object[] { flowWebField.Label });
                    return false;
                }
            }
            else
            {
                if (flowWebField.Type == FlowFieldType.String)
                {
                }
                else if (flowWebField.Type == FlowFieldType.DateTime)
                {
                    string format = (!String.IsNullOrWhiteSpace(flowWebField.Format) ? flowWebField.Format : FlowWebField.DEFAULT_DATETIME_FORMAT);
                    DateTime dt;
                    if (DateTime.TryParseExact(flowWebField.DisplayValue, format, System.Threading.Thread.CurrentThread.CurrentCulture, DateTimeStyles.None, out dt) == false)
                    {
                        ErrorMessage = BaseEntity.ErrorMessage(BaseEntity.errorDate, new object[] { flowWebField.Label, flowWebField.Format });
                        return false;
                    }
                }
                else if (flowWebField.Type == FlowFieldType.Date)
                {
                    string format = (!String.IsNullOrWhiteSpace(flowWebField.Format) ? flowWebField.Format : FlowWebField.DEFAULT_DATE_FORMAT);
                    DateTime dt;
                    if (DateTime.TryParseExact(flowWebField.DisplayValue, format, System.Threading.Thread.CurrentThread.CurrentCulture, DateTimeStyles.None, out dt) == false)
                    {
                        ErrorMessage = BaseEntity.ErrorMessage(BaseEntity.errorDate, new object[] { flowWebField.Label, flowWebField.Format });
                        return false;
                    }
                }
                else if (flowWebField.Type == FlowFieldType.Time)
                {
                    Match match = Regex.Match(flowWebField.DisplayValue, "^(?:0?[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$");
                    if (!match.Success)
                    {
                        ErrorMessage = BaseEntity.ErrorMessage(BaseEntity.errorDate, new object[] { flowWebField.Label, "00:00" });
                        return false;
                    }
                }
                else if (flowWebField.Type == FlowFieldType.Number)
                {
                    double number;
                    if (double.TryParse(flowWebField.DisplayValue, NumberStyles.Any, CultureInfo.GetCultureInfo("nl-NL"), out number) == false)
                    {
                        ErrorMessage = BaseEntity.ErrorMessage(BaseEntity.errorNumber, new object[] { flowWebField.Label });
                        return false;
                    }
                }
                else if (flowWebField.Type == FlowFieldType.RadioButton)
                {
                    return validateFlowFieldDataSource();
                }
                else if (flowWebField.Type == FlowFieldType.Checkbox)
                {
                    if (flowWebField.DisplayValue.Equals("true", StringComparison.InvariantCultureIgnoreCase) == false &&
                        flowWebField.DisplayValue.Equals("false", StringComparison.InvariantCultureIgnoreCase) == false) return false;
                }
            }

            return true;
        }

        private bool validateFlowFieldDataSource()
        {
            if (flowWebField.FlowFieldDataSource == null || (flowWebField.FlowFieldDataSource != null && flowWebField.FlowFieldDataSource.Type == FlowFieldDataSourceType.None))
                return false;

            if (flowWebField.FlowFieldDataSource.Type == FlowFieldDataSourceType.List || flowWebField.FlowFieldDataSource.Type == FlowFieldDataSourceType.VerticalList)
            {
                var option = flowWebField.FlowFieldDataSource.Options.Where(o => o.Value == flowWebField.Value).FirstOrDefault();
                if (option == null)
                {
                    ErrorMessage = BaseEntity.ErrorMessage(BaseEntity.errorNotInDataSet, new object[] { flowWebField.Label });
                    return false;
                }
            }
            else if (flowWebField.FlowFieldDataSource.Type == FlowFieldDataSourceType.IDText)
            {
                // no validation here as we don't want another database hit just for validation
            }
            else if (flowWebField.FlowFieldDataSource.Type == FlowFieldDataSourceType.Entity)
            {
                // no validation here as we don't want another database hit just for validation
            }

            return true;
        }
    }
}
