﻿using Point.Business.Logic;
using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Repository;
using System;

namespace Point.Business.Helper
{
    public class ScopeTypeHelper
    {
        public static ScopeType ByFlowInstance(IUnitOfWork uow, FlowInstance flowinstance)
        {
            if(flowinstance == null)
            {
                return ScopeType.None;
            }

            if (flowinstance.Interrupted)
            {
                return ScopeType.Interrupted;
            }

            var endphase = PhaseInstanceBL.EndPhaseStatus(uow, flowinstance.FlowInstanceID);
            if (endphase == Status.Done)
            {
                return ScopeType.Closed;
            }
            else if (endphase == Status.Active)
            {
                return ScopeType.Definitive;
            }

            var patientacceptedstate = PatientHelper.IsAccepted(uow, flowinstance.FlowInstanceID);
            if (patientacceptedstate.HasValue)
            {
                switch (patientacceptedstate)
                {
                    case AcceptanceState.Acknowledged:
                        return ScopeType.Accepted;

                    case AcceptanceState.AcknowledgedPartially:
                        return ScopeType.PartiallyAccepted;

                    case AcceptanceState.Refused:
                        return ScopeType.NotAccepted;
                }
            }

            return ScopeType.None;
        }

        public static ScopeType ByFlowInstanceID(IUnitOfWork uow, int flowInstanceID)
        {
            var flowinstance = FlowInstanceBL.GetByID(uow, flowInstanceID);
            return ByFlowInstance(uow, flowinstance);
        }

        public static ScopeType ByFrequencyID(IUnitOfWork uow, int frequencyid)
        {
            var today = DateTime.Now;
            var freqency = FrequencyBL.GetByID(uow, frequencyid);

            if (freqency.Status == FrequencyStatus.Active)
            {
                return ((freqency.StartDate <= today && freqency.EndDate >= today) ? ScopeType.FeedbackActiveInRange : ScopeType.FeedbackActiveOutRange);
            }
            else if (freqency.Status == FrequencyStatus.Done)
            {
                return ScopeType.FeedbackClosed;
            }

            return ScopeType.None;
        }

        public static ScopeType From(DossierStatus status, FrequencyDossierStatus frequencyStatus, DossierHandling handling, DossierType type, SearchType searchType)
        {
            if (searchType == SearchType.Frequency)
            {
                switch (frequencyStatus)
                {
                    case FrequencyDossierStatus.All:
                        break;
                    case FrequencyDossierStatus.Active:
                        return ScopeType.FeedbackActiveInRange;
                    case FrequencyDossierStatus.Open:
                        return ScopeType.FeedbackActiveOutRange;
                    case FrequencyDossierStatus.Close:
                        return ScopeType.Closed;
                    default:
                        break;
                }

                return ScopeType.None;
            }

            if (status != DossierStatus.All)
            {
                switch (status)
                {
                    case DossierStatus.Interrupted:
                        return ScopeType.Interrupted;
                    case DossierStatus.CanBeClosed:
                        return ScopeType.Definitive;
                    case DossierStatus.IsClosed:
                        return ScopeType.Closed;
                    case DossierStatus.CanBeTransferred:
                        return ScopeType.Accepted;
                    case DossierStatus.CanPerhapsBeTransferred:
                        return ScopeType.PartiallyAccepted;
                    case DossierStatus.CannotBeTransferred:
                        return ScopeType.NotAccepted;
                    case DossierStatus.ActiveOrClosedWithin14days:
                        return ScopeType.LessThan14Days;
                    default:
                        break;
                }
            }

            if (handling != DossierHandling.All)
            {
                switch (handling)
                {
                    case DossierHandling.NotHandling:
                        return ScopeType.NotProcessing;
                    case DossierHandling.Handling:
                        return ScopeType.Handled;
                    case DossierHandling.HandledByMe:
                        return ScopeType.HandledByMe;

                    case DossierHandling.OwnedByMe:
                        return ScopeType.OwnedByMe;
                    case DossierHandling.OwnedByMyDepartment:
                        return ScopeType.OwnedByMyDepartment;

                    case DossierHandling.CreatedByMe:
                        return ScopeType.CreatedByMe;
                    case DossierHandling.CreatedByDepartment:
                        return ScopeType.CreatedByDepartment;
                    default:
                        break;
                }
            }

            if (type != DossierType.All)
            {
                // FUTURE CVA & MSVT etc.
            }

            if (status == DossierStatus.Active)
            {
                return ScopeType.AllTransfers;
            }

            return ScopeType.None;
        }

        public static ScopeType From(FrequencyDossierStatus frequencyStatus)
        {
            return From(DossierStatus.All, frequencyStatus, DossierHandling.All, DossierType.All, SearchType.Frequency);
        }

        public static ScopeType From(DossierStatus status)
        {
            return From(status, FrequencyDossierStatus.All, DossierHandling.All, DossierType.All, SearchType.Transfer);
        }
    }
}