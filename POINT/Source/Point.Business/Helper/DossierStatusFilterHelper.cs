﻿using System.Collections.Generic;
using System.Linq;
using Point.Models.Enums;

namespace Point.Business.Helper
{
    public static class DossierStatusFilterHelper
    {
        private static IEnumerable<DossierStatus> _activeStati = new DossierStatus[] { DossierStatus.Active, DossierStatus.Interrupted, DossierStatus.CanBeClosed, DossierStatus.CanBeTransferred, DossierStatus.CanPerhapsBeTransferred, DossierStatus.CannotBeTransferred };
        private static IEnumerable<DossierStatus> _closedStati = new DossierStatus[] { DossierStatus.IsClosed };
        private static IEnumerable<DossierStatus> _bothStati = new DossierStatus[] { DossierStatus.All, DossierStatus.ActiveOrClosedWithin14days };
        private static IEnumerable<DossierStatus> _scopeStati = new DossierStatus[] { DossierStatus.Interrupted, DossierStatus.CanBeClosed, DossierStatus.CanBeTransferred, DossierStatus.CanPerhapsBeTransferred, DossierStatus.CannotBeTransferred, DossierStatus.ActiveOrClosedWithin14days };

        public static bool IsActiveStatusFilter(DossierStatus status)
        {
            return _activeStati.Contains(status);
        }

        public static bool IsClosedStatusFilter(DossierStatus status)
        {
            return _closedStati.Contains(status);
        }

        public static bool IsBothStatusFilter(DossierStatus status)
        {
            return _bothStati.Contains(status);
        }

        public static bool IsScopeStatusFilter(DossierStatus status)
        {
            return _scopeStati.Contains(status);
        }
    }
}
