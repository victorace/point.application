﻿using System;
using System.Collections.Generic;
using System.Linq;
using Point.Business.Logic;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Extensions;
using Point.Models.Enums;

namespace Point.Business.Helper
{
    // Intended as instance
    public class FlowFieldExplanationHelper
    {
        private const string _enter = "\r\n";
        private List<FlowFieldValue> _flowFieldValues;
        private List<FlowWebField> _flowWebFields;

        public FlowFieldExplanationHelper(IEnumerable<FlowWebField> flowWebFields, IEnumerable<FlowFieldValue> flowFieldValues)
        {
            SetFlowWebFieldsAndValues(flowWebFields, flowFieldValues);
        }

        public void SetFlowWebFieldsAndValues(IEnumerable<FlowWebField> flowWebFields, IEnumerable<FlowFieldValue> flowFieldValues)
        {
            SetFlowWebFields(flowWebFields);
            SetFlowFieldValues(flowFieldValues);
        }

        public void SetFlowWebFields(IEnumerable<FlowWebField> flowWebFields)
        {
            _flowWebFields = flowWebFields!=null ? flowWebFields.ToList() : new List<FlowWebField>();
        }

        public void SetFlowFieldValues(IEnumerable<FlowFieldValue> flowFieldValues)
        {
            _flowFieldValues = flowFieldValues != null ? flowFieldValues.ToList() : new List<FlowFieldValue>();
        }

        public List<FlowWebField> GetUpdatedFlowWebFields()
        {
            return _flowWebFields;
        }

        public string CreateDagWeekToelichting(string caption, int dagflowfieldid, int weekflowfieldid, int toelichtingflowfieldid)
        {
            string toelichting = string.Empty;

            var dag = FlowFieldValueBL.GetSourceFieldValue(_flowFieldValues, dagflowfieldid);
            if (!string.IsNullOrWhiteSpace(dag?.Value))
            {
                toelichting += dag.Value.Trim() + " X per dag, ";
            }

            var week = FlowFieldValueBL.GetSourceFieldValue(_flowFieldValues, weekflowfieldid);
            if (!string.IsNullOrWhiteSpace(week?.Value))
            {
                toelichting += week.Value.Trim() + " X Per week, ";
            }

            var toelichtingSource = FlowFieldValueBL.GetSourceFieldValue(_flowFieldValues, toelichtingflowfieldid);
            if (!string.IsNullOrWhiteSpace(toelichtingSource?.Value))
            {
                toelichting += toelichtingSource.Value.Trim();
            }

            if (!string.IsNullOrWhiteSpace(toelichting))
            {
                toelichting = (!string.IsNullOrEmpty(caption) ? caption + toelichting.Trim() : toelichting.Trim()) + _enter;
            }

            return toelichting;
        }

        public void CreateDagWeekToelichting(int flowFieldID, string caption, int dagflowfieldid, int weekflowfieldid, int toelichtingflowfieldid)
        {
            var flowWebField = _flowWebFields.FirstOrDefault(fwf => fwf.FlowFieldID == flowFieldID);
            if (flowWebField == null)
            {
                return;
            }

            var toelichting = CreateDagWeekToelichting(caption, dagflowfieldid, weekflowfieldid, toelichtingflowfieldid);

            if (!string.IsNullOrWhiteSpace(toelichting))
            {
                FlowWebFieldBL.SetValue(flowWebField, toelichting, Source.POINT_PREFILL);
            }
        }

        public void CreateAlgemeneToelichting(int flowFieldID, Dictionary<int, string> flowFieldIDsAndCaptions)
        {
            var flowWebField = _flowWebFields.FirstOrDefault(fwf => fwf.FlowFieldID == flowFieldID);
            if (flowWebField == null)
            {
                return;
            }

            var toelichting = string.Empty;

            foreach (var keyPair in flowFieldIDsAndCaptions)
            {
                toelichting += CreateAlgemeneToelichting(keyPair.Value, keyPair.Key);
            }

            if (!string.IsNullOrEmpty(toelichting.Trim()))
            {
                FlowWebFieldBL.SetValue(flowWebField, toelichting, Source.POINT_PREFILL);
            }
        }

        public string CreateAlgemeneToelichting(string caption, int toelichtingflowfieldid)
        {
            string toelichting = "";

            var toelichtingValue = FlowFieldValueBL.GetSourceFieldValue(_flowFieldValues, toelichtingflowfieldid);
            toelichting += !string.IsNullOrWhiteSpace(toelichtingValue?.Value) ? caption + toelichtingValue.Value.Trim() + _enter : "";

            return toelichting;
        }

        public string CreateMantelZorgBijdrage(IUnitOfWork uow, int transferID)
        {
            string toelichting = "", toelichtingbijdrage = "";
            var grzformulier = (int)FlowFormType.GRZFormulier;

            var mantelZorg = FlowFieldValueBL.GetByTransferIDAndFieldID(uow, transferID, FlowFieldConstants.ID_MantelzorgerAanwezig).FirstOrDefault(it => it.FormSetVersion.FormTypeID == grzformulier);
            toelichting += !string.IsNullOrEmpty(mantelZorg?.Value) ? "Mantelzorger aanwezig: " + mantelZorg.Value + _enter : "";

            var sourceFieldValuemantelZorg = FlowFieldValueBL.GetByTransferIDAndFieldID(uow, transferID, FlowFieldConstants.ID_BijdrageMantelzorgerPersoonlijkeVerzorging).FirstOrDefault(it => it.FormSetVersion.FormTypeID == grzformulier);
            toelichtingbijdrage += sourceFieldValuemantelZorg?.Value.StringToBool() == true ? sourceFieldValuemantelZorg.FlowField.Label + "," : "";

            var bijdrageMantelzorgerHuishouden = FlowFieldValueBL.GetByTransferIDAndFieldID(uow, transferID, FlowFieldConstants.ID_BijdrageMantelzorgerHuishouden).FirstOrDefault(it => it.FormSetVersion.FormTypeID == grzformulier);
            toelichtingbijdrage += bijdrageMantelzorgerHuishouden?.Value.StringToBool() == true ? bijdrageMantelzorgerHuishouden.FlowField.Label + "," : "";

            var bijdrageMantelzorgerVervoer = FlowFieldValueBL.GetByTransferIDAndFieldID(uow, transferID, FlowFieldConstants.ID_BijdrageMantelzorgerVervoer).FirstOrDefault(it => it.FormSetVersion.FormTypeID == grzformulier);
            toelichtingbijdrage += bijdrageMantelzorgerVervoer?.Value.StringToBool() == true ? bijdrageMantelzorgerVervoer.FlowField.Label + "," : "";

            var bijdrageMantelzorgerOverig = FlowFieldValueBL.GetByTransferIDAndFieldID(uow, transferID, FlowFieldConstants.ID_BijdrageMantelzorgerOverig).FirstOrDefault(it => it.FormSetVersion.FormTypeID == grzformulier);
            toelichtingbijdrage += bijdrageMantelzorgerOverig?.Value.StringToBool() == true ? bijdrageMantelzorgerOverig.FlowField.Label + "," : "";

            toelichtingbijdrage = (!string.IsNullOrEmpty(toelichtingbijdrage) ? "Bijdrage mantelzorger: " + toelichtingbijdrage.Trim() : "");

            toelichting = toelichting + toelichtingbijdrage;
            return toelichting;
        }

        public void SetDestinationFormulier(int destflowfieldid, int srcflowfieldid)
        {
            // TODO: why is null not a valid value? - PP
            var sourceFieldValue = FlowFieldValueBL.GetSourceFieldValue(_flowFieldValues, srcflowfieldid);
            if (!string.IsNullOrWhiteSpace(sourceFieldValue?.Value))
            {
                FlowWebFieldBL.SetValue(_flowWebFields, destflowfieldid, sourceFieldValue, Source.POINT_PREFILL);
            }
        }

        // FlowField wrapper functions to facilitate/simplify calling existing methods
        public FlowFieldValue GetSourceFieldValue(int flowFieldID)
        {
            return FlowFieldValueBL.GetSourceFieldValue(_flowFieldValues, flowFieldID);
        }

        public bool GetSourceFieldValueBoolean(int flowFieldID)
        {
            return FlowFieldValueBL.GetSourceFieldValue(_flowFieldValues, flowFieldID)?.Value.StringToBool() == true;
        }

        public FlowWebField GetFlowWebField(int flowFieldID)
        {
            return _flowWebFields.FirstOrDefault(fwf => fwf.FlowFieldID == flowFieldID);
        }

        public bool FlowWebFieldSetValue(FlowWebField flowWebField, string value)
        {
            if (flowWebField == null || string.IsNullOrWhiteSpace(value))
            {
                return false;
            }
            FlowWebFieldBL.SetValue(flowWebField, value, Source.POINT_PREFILL);
            return true;

        }
        public bool FlowWebFieldSetValueAndTimestampFromSourceValue(int targetFlowFieldID1, int sourceFlowFieldID, int targetFlowFieldID2)
        {
            var flowWebField = GetFlowWebField(targetFlowFieldID1);
            if (flowWebField == null)
            {
                return false;
            }

            var flowFieldValue = GetSourceFieldValue(sourceFlowFieldID);
            if (flowFieldValue != null && FlowWebFieldSetValue(flowWebField, flowFieldValue.Value))
            {
                var flowWebField2 = GetFlowWebField(targetFlowFieldID2);
                if (flowWebField2 != null)
                {
                    FlowWebFieldSetValue(flowWebField2, (DateTime)flowFieldValue.Timestamp);
                }
            }

            return true;
        }
        public bool FlowWebFieldSetValue(FlowWebField flowWebField, bool value)
        {
            if (flowWebField == null)
            {
                return false;
            }
            FlowWebFieldBL.SetValue(flowWebField, value, Source.POINT_PREFILL);
            return true;

        }
        public bool FlowWebFieldSetValue(FlowWebField flowWebField, DateTime? value)
        {
            if (flowWebField == null || !value.HasValue)
            {
                return false;
            }
            FlowWebFieldBL.SetValue(flowWebField, value.Value, Source.POINT_PREFILL);
            return true;
        }


        public void FlowWebFieldFillValue(int flowFieldID, int? value)
        {
            if(value == null)
            {
                return;
            }

            FlowWebFieldBL.SetValue(_flowWebFields, flowFieldID, (int)value, Source.POINT_PREFILL);
        }

        public void FlowWebFieldFillValue(int flowFieldID, string value)
        {
            FlowWebFieldBL.SetValue(_flowWebFields, flowFieldID, value, Source.POINT_PREFILL);
        }

        /// <summary>
        /// Returns the first FlowFieldValue item with a value.
        /// </summary>
        /// <param name="flowFieldIDs">Array of flowFieldIDs to be checked in the order given. First not-empty value gets returned</param>
        /// <returns></returns>
        public string GetFirstFlowFieldValueNotEmpty(IUnitOfWork uow, int transferID, int[] flowFieldIDs)
        {
            FlowFieldValue flowFieldValue;

            foreach (var flowFieldID in flowFieldIDs)
            {
                flowFieldValue = FlowFieldValueBL.GetByTransferIDAndFieldID(uow, transferID, flowFieldID).FirstOrDefault();
                if (!string.IsNullOrEmpty(flowFieldValue?.Value))
                {
                    return flowFieldValue.Value;
                }
            }

            return null;
        }
    }
}
