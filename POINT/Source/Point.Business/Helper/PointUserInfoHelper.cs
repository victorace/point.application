﻿using System;
using System.Linq;
using System.Web.Security;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Caching;
using Point.Infrastructure.Extensions;
using Point.Infrastructure.ProxiedMembership;
using Point.Infrastructure.Helpers;
using Point.Models.Enums;
using Point.Business.Logic;
using System.Web;
using Point.Infrastructure.Constants;

namespace Point.Business.Helper
{
    public static class PointUserInfoHelper
    {
        public static PointUserInfo GetPointUserInfo(IUnitOfWork uow)
        {
            var user = MembershipProxy.Instance.GetUser();
            return user == null ? new PointUserInfo() : GetPointUserInfo(uow, user.UserId);
        }

        public static bool GetPasswordExpired(PointUserInfo pointUserInfo, bool isSSORequest)
        {
            string cachestring = $"PasswordExpired_{pointUserInfo.Employee.EmployeeID}";
            var cachedexpired = CacheService.Instance.Get<bool?>(cachestring);
            if (cachedexpired.HasValue)
            {
                return cachedexpired.Value;
            }

            if (!isSSORequest && pointUserInfo.Employee.ChangePasswordByEmployee)
            {
                cachedexpired = true;
            }
            else if (pointUserInfo.Organization.ChangePasswordPeriod.GetValueOrDefault(0) <= 0)
            {
                cachedexpired = false;
            }
            else
            {
                var user = Membership.GetUser();
                var changeperiod = pointUserInfo.Organization.ChangePasswordPeriod.GetValueOrDefault(0);
                var lastpasswordchange = user?.LastPasswordChangedDate ?? default(DateTime);
                cachedexpired = (lastpasswordchange.AddMonths(changeperiod) < DateTime.Now);
            }

            CacheService.Instance.Insert(cachestring, cachedexpired, TimeSpan.FromSeconds(3));

            return cachedexpired ?? false;
        }

        public static bool IsMFARequired(PointUserInfo pointUserInfo, string clientIP)
        {
            string cachestring = $"IsMFARequired_{pointUserInfo.Employee.EmployeeID}";
            var cachedismfarequired = CacheService.Instance.Get<bool?>(cachestring);
            if (cachedismfarequired.HasValue)
            {
                return cachedismfarequired.Value;
            }

            if (pointUserInfo.Employee != null && pointUserInfo.Employee.MFARequired && !isIPValid(pointUserInfo.Employee.IPAddresses, clientIP))
            {
                cachedismfarequired = checkEmployeeMFA(pointUserInfo);
            }
            else if (pointUserInfo.Organization.MFARequired && !isIPValid(pointUserInfo.Organization.IPAddresses, clientIP))
            {
                cachedismfarequired = checkEmployeeMFA(pointUserInfo);
            }
            else
            {
                cachedismfarequired = false;
            }

            CacheService.Instance.Insert(cachestring, cachedismfarequired, TimeSpan.FromSeconds(3));

            return cachedismfarequired ?? false;
        }

        private static bool checkEmployeeMFA(PointUserInfo pointUserInfo)
        {
            if (pointUserInfo == null || pointUserInfo.Employee == null)
            {
                return false;
            }
            return (String.IsNullOrEmpty(pointUserInfo.Employee.MFANumber) && String.IsNullOrEmpty(pointUserInfo.Employee.MFAKey)) || !pointUserInfo.Employee.MFAConfirmedDate.HasValue;
        }

        public static string GetMFAAction(PointUserInfo pointUserInfo, string viewPrefix)
        {
            string viewname = "";
            switch(pointUserInfo.Organization.MFAAuthType)
            {
                case MFAAuthType.Google:
                    viewname = $"{viewPrefix}Google";
                    break;
                case MFAAuthType.SMS:
                    viewname = $"{viewPrefix}SMS";
                    break;
                default:
                    viewname = "";
                    break;
            }
            return viewname;

            


        }

        private static bool isIPValid(string validIPs, string clientIP)
        {
            return IPAddressHelper.IsIPAddressValid(clientIP, validIPs);
        }

        public static bool HasValidMFA(IUnitOfWork uow, PointUserInfo pointUserInfo, string clientIp)
        {
            if ((pointUserInfo.Organization.MFARequired && !isIPValid(pointUserInfo.Organization.IPAddresses,clientIp)) ||
               ((pointUserInfo.Employee?.MFARequired??false) && !isIPValid(pointUserInfo.Employee?.IPAddresses, clientIp)))
            {

                string cachestring = $"HasValidMFA_{pointUserInfo.Employee.EmployeeID}";
                var cachedhasvalidmfa = CacheService.Instance.Get<bool?>(cachestring);
                if (cachedhasvalidmfa.HasValue && cachedhasvalidmfa == true)
                {
                    return true;
                }

                int validhours = 0;
                if (pointUserInfo.Organization.MFATimeout != null && pointUserInfo.Organization.MFATimeout > 0)
                {
                    validhours = pointUserInfo.Organization.MFATimeout.ConvertTo<int>() * 24;
                }
                var validmfaperiod = DateTime.Now.AddHours(-validhours).AddMinutes(-5); // give some extra minutes to prevent looping
                cachedhasvalidmfa = uow.UsedMFACodeRepository.Exists(umc => umc.EmployeeID == pointUserInfo.Employee.EmployeeID && umc.MFANumber == pointUserInfo.Employee.MFANumber && umc.ClaimDateTime >= validmfaperiod); // Both MFATypes are valid. After registration no re-entering is needed for the specified period

                CacheService.Instance.Insert(cachestring, cachedhasvalidmfa, TimeSpan.FromHours(validhours));

                return cachedhasvalidmfa ?? false;
            }
            else
            {
                return true;
            }

        }

        public static bool HasMultipleDepartments(IUnitOfWork uow, PointUserInfo pointUserInfo)
        {
            if (pointUserInfo?.Employee.PointRole == null)
            {
                return false;
            }

            Role role = pointUserInfo.Employee.PointRole;

            if (role > Role.Department)
            {
                var departments = DepartmentBL.GetByLocationID(uow, pointUserInfo.Department.LocationID);
                return departments.Count() > 1 || pointUserInfo.EmployeeLocationIDs.Count > 1;
            }
            else
            {
                return (pointUserInfo.EmployeeDepartmentIDs != null && pointUserInfo.EmployeeDepartmentIDs.Count > 1);
            }
        }

        public static bool HasMultipleLocations(IUnitOfWork uow, PointUserInfo pointUserInfo)
        {
            if (pointUserInfo?.Employee.PointRole == null)
            {
                return false;
            }

            var role = pointUserInfo.Employee.PointRole;

            if (role > Role.Location)
            {
                var locations = LocationBL.GetActiveByOrganisationID(uow, pointUserInfo.Location.OrganizationID);
                return locations.Count() > 1 || pointUserInfo.EmployeeOrganizationIDs.Count > 1;
            }

            return (pointUserInfo.EmployeeLocationIDs != null && pointUserInfo.EmployeeLocationIDs.Count > 1);
        }

        public static bool IsDepartmentIDsValid(IUnitOfWork uow, PointUserInfo pointUserInfo, FunctionRoleLevelID functionRoleLevelID, int[] departmentIDs)
        {
            switch (functionRoleLevelID)
            {
                case FunctionRoleLevelID.Department:
                    if (pointUserInfo.EmployeeDepartmentIDs == null || !departmentIDs.Any(id => pointUserInfo.EmployeeDepartmentIDs.Contains(id)))
                    {
                        return false;
                    }
                    break;
                case FunctionRoleLevelID.Location:
                {
                    if (pointUserInfo.EmployeeLocationIDs == null)
                    {
                        return false;
                    }
                    var locationIDs = LocationBL.GetActiveByDepartmentIDs(uow, departmentIDs).Select(l => l.LocationID).ToList();
                    if (!locationIDs.ToArray().Any(id => pointUserInfo.EmployeeLocationIDs.Contains(id)))
                    {
                        return false;
                    }
                    break;
                }
                case FunctionRoleLevelID.Organization:
                {
                    if (pointUserInfo.EmployeeOrganizationIDs == null)
                    {
                        return false;
                    }
                    var organizationIDs = OrganizationBL.GetActiveByDepartmentIDs(uow, departmentIDs).Select(l => l.OrganizationID).ToList();
                    if (!organizationIDs.Any(id => pointUserInfo.EmployeeOrganizationIDs.Contains(id)))
                    {
                        return false;
                    }
                    break;
                }
            }

            return true;
        }

        public static bool IsLocationIDsValid(IUnitOfWork uow, PointUserInfo pointUserInfo, FunctionRoleLevelID functionRoleLevelID, int[] locationIDs)
        {
            if (functionRoleLevelID <= FunctionRoleLevelID.Location)
            {
                if (locationIDs.Any(id => pointUserInfo.EmployeeLocationIDs.Contains(id) == false))
                {
                    return false;
                }
            }
            if (functionRoleLevelID == FunctionRoleLevelID.Organization)
            {
                var organizationids = OrganizationBL.GetActiveByLocationIDs(uow, locationIDs).Select(l => l.OrganizationID).ToArray();
                if (organizationids.Any(id => pointUserInfo.EmployeeOrganizationIDs.Contains(id) == false))
                {
                    return false;
                }
            }
            if (functionRoleLevelID == FunctionRoleLevelID.Region)
            {
                var regionids = RegionBL.GetByLocationIDs(uow, locationIDs).Select(l => l.RegionID).ToArray();
                if (regionids.Any(id => pointUserInfo.Region.RegionID != id))
                {
                    return false;
                }
            }

            return true;
        }

        public static bool IsOrganizationIDsValid(IUnitOfWork uow, PointUserInfo pointUserInfo, FunctionRoleLevelID functionRoleLevelID, int[] organizationIDs)
        {
            if (functionRoleLevelID <= FunctionRoleLevelID.Organization)
            {
                if (organizationIDs.Any(id => pointUserInfo.EmployeeOrganizationIDs.Contains(id) == false))
                {
                    return false;
                }
            }
            if (functionRoleLevelID == FunctionRoleLevelID.Region)
            {
                var regionids = RegionBL.GetByOrganizationIDs(uow, organizationIDs).Select(l => l.RegionID).ToArray();
                if (regionids.Any(id => pointUserInfo.Region.RegionID != id))
                {
                    return false;
                }
            }

            return true;
        }

        public static bool IsRegionIDValid(IUnitOfWork uow, PointUserInfo pointUserInfo, FunctionRoleLevelID functionRoleLevelID, int regionID)
        {
            if (functionRoleLevelID <= FunctionRoleLevelID.Region)
            {
                if(regionID != pointUserInfo.Region.RegionID)
                {
                    return false;
                }
            }

            return true;
        }

        public static void ClearCache()
        {
            var user = MembershipProxy.Instance.GetUser();
            if (user != null)
            {
                PointUserInfoHelper.ClearCache(user.UserId);
            }
        }
        public static void ClearCache(Guid userId)
        {
            string cachestring = $"PointUserInfo_{userId}";
            CacheService.Instance.Remove(cachestring);
        }

        public static PointUserInfo GetPointUserInfo(IUnitOfWork uow, Guid userId)
        {
            PointUserInfo pointuserinfo;

            if (userId == Guid.Empty)
            {
                var user = MembershipProxy.Instance.GetUser();
                if (user == null)
                    return new PointUserInfo();
                userId = user.UserId;
            }
            
            string cachestring = $"PointUserInfo_{userId}";
            pointuserinfo = CacheService.Instance.Get<PointUserInfo>(cachestring);

            if (pointuserinfo != null) return pointuserinfo;

            pointuserinfo = GetPointUserInfoNoCache(uow, userId);
            CacheService.Instance.Insert(cachestring, pointuserinfo);

            return pointuserinfo;
        }

        private const string cachestringCancelSecurityQuestion = "CancelSecretQuestion_{0}";

        public static bool GetCancelSecurityQuestion(PointUserInfo userinfo)
        {
            var cachestring = String.Format(cachestringCancelSecurityQuestion, userinfo?.Employee?.EmployeeID);
            return CacheService.Instance.Get<bool?>(cachestring) == true;
        }

        public static void SetCancelSecurityQuestion(PointUserInfo userinfo)
        {
            var cachestring = String.Format(cachestringCancelSecurityQuestion, userinfo?.Employee?.EmployeeID);

            int validhours = 8;
            var validperiod = DateTime.Now.AddHours(-validhours);
            CacheService.Instance.Insert(cachestring, true, TimeSpan.FromHours(validhours));
        }

        public static PointUserInfo GetPointUserInfoNoCache(IUnitOfWork uow)
        {
            var user = MembershipProxy.Instance.GetUser();
            return user == null ? new PointUserInfo() : GetPointUserInfoNoCache(uow, user.UserId);
        }

        public static Guid GetSavedProviderUserKey()
        {
            var session = HttpContext.Current?.Session;
            if(session == null)
            {
                return Guid.Empty;
            }

            if (!(session[SessionIdentifiers.Authorization.ProviderUserKey] is string sessionkey))
            {
                return Guid.Empty;
            }

            return Guid.TryParse(sessionkey, out Guid tempguid) ? tempguid : Guid.Empty;
        }

        public static void SaveProviderUserKey(Guid? guid)
        {
            if(guid == null)
            {
                return;
            }

            var session = HttpContext.Current?.Session;
            if (session == null)
            {
                return;
            }

            session[SessionIdentifiers.Authorization.ProviderUserKey] = guid.ToString();
        }

        public static PointUserInfo GetPointUserInfoNoCache(IUnitOfWork uow, Guid userId)
        {
            var pointuserinfo = new PointUserInfo();
            if (userId == Guid.Empty) return null;

            var employee = uow.EmployeeRepository.Get(empl => empl.UserId == userId).FirstOrDefault();
            if (employee != null)
                pointuserinfo = GetPointUserInfoByEmployeeID(uow, employee.EmployeeID);

            return pointuserinfo;
        }

        public static PointUserInfo GetPointUserInfoByEmployeeID(IUnitOfWork uow, int employeeID)
        {
            var roleglobal = Role.Global.ToString();

            var pointUserInfoEmployee = uow.EmployeeRepository.Get(emp => emp.EmployeeID == employeeID).Select(emp =>
                new PointUserInfo.PointUserInfoEmployee()
                {
                    FirstName = emp.FirstName,
                    MiddleName = emp.MiddleName,
                    LastName = emp.LastName,
                    EmailAddress = emp.EmailAddress,
                    PhoneNumber = emp.PhoneNumber,
                    Position = emp.Position,
                    BIG = emp.BIG,
                    DigitalSignatureID = emp.DigitalSignatureID,
                    EmployeeID = employeeID,
                    DepartmentID = emp.DepartmentID,
                    UserID = emp.UserId,
                    MFARequired = emp.MFARequired,
                    MFAKey = emp.MFAKey,
                    MFANumber = emp.MFANumber,
                    MFAConfirmedDate = emp.MFAConfirmedDate,
                    ChangePasswordByEmployee = emp.ChangePasswordByEmployee,
                    EditAllForms = emp.EditAllForms,
                    ReadOnlyReciever = emp.ReadOnlyReciever,
                    ReadOnlySender = emp.ReadOnlySender,
                    IPAddresses = emp.IPAddresses
                }
            ).FirstOrDefault();

            var pointuserinfo = new PointUserInfo()
            {
                Employee = pointUserInfoEmployee
            };

            pointuserinfo.Employee.PointRole = Authorization.GetPointFlowRole(uow, pointuserinfo.Employee.UserID);
            pointuserinfo.Employee.FunctionRoles = uow.FunctionRoleRepository.Get(fr => fr.EmployeeID == employeeID).ToList();

            MembershipUser membershipuser = pointuserinfo.Employee.UserID.HasValue ? Membership.GetUser(pointuserinfo.Employee.UserID) : null;
            pointuserinfo.Employee.UserName = membershipuser?.UserName;
            pointuserinfo.Employee.IsLockedOut = membershipuser?.IsLockedOut == true;

            pointuserinfo.Department = uow.DepartmentRepository.Get(dep => dep.DepartmentID == pointuserinfo.Employee.DepartmentID).FirstOrDefault();
            pointuserinfo.Location = uow.LocationRepository.Get(loc => loc.LocationID == pointuserinfo.Department.LocationID).FirstOrDefault();
            pointuserinfo.Organization = uow.OrganizationRepository.Get(org => org.OrganizationID == pointuserinfo.Location.OrganizationID).FirstOrDefault();
            pointuserinfo.Region = uow.RegionRepository.Get(reg => reg.RegionID == pointuserinfo.Organization.RegionID).FirstOrDefault();

            var otherdepartments = (from department in uow.EmployeeDepartmentRepository.Get(ed => ed.EmployeeID == employeeID)
                                    select department.Department).ToList();

            pointuserinfo.EmployeeDepartmentIDs = (from department in otherdepartments
                                                   select department.DepartmentID).ToList();

            pointuserinfo.DepartmentTypeIDS = (from department in otherdepartments
                                               select department.DepartmentTypeID).Distinct().ToList();

            if (pointuserinfo.Department != null && !pointuserinfo.EmployeeDepartmentIDs.Contains(pointuserinfo.Department.DepartmentID))
            {
                pointuserinfo.EmployeeDepartmentIDs.Add(pointuserinfo.Department.DepartmentID);
            }
            if (pointuserinfo.Department != null && !pointuserinfo.DepartmentTypeIDS.Contains(pointuserinfo.Department.DepartmentTypeID))
            {
                pointuserinfo.DepartmentTypeIDS.Add(pointuserinfo.Department.DepartmentTypeID);
            }
            pointuserinfo.EmployeeLocationIDs = uow.DepartmentRepository.Get(dep => pointuserinfo.EmployeeDepartmentIDs.Contains(dep.DepartmentID)).Select(it => it.LocationID).Distinct().ToList();
            if (pointuserinfo.Location != null && !pointuserinfo.EmployeeLocationIDs.Contains(pointuserinfo.Location.LocationID))
            {
                pointuserinfo.EmployeeLocationIDs.Add(pointuserinfo.Location.LocationID);
            }
            pointuserinfo.EmployeeOrganizationIDs = uow.LocationRepository.Get(loc => pointuserinfo.EmployeeLocationIDs.Contains(loc.LocationID)).Select(it => it.OrganizationID).Distinct().ToList();

            return pointuserinfo;
        }

        public static int GetSystemEmployeeID()
        {
            int systemEmployeeID = ConfigHelper.GetAppSettingByName("SystemEmployeeID", -1);
            if (systemEmployeeID == -1)
            {
                throw new Exception("SystemEmployeeID is not specified in the config");
            }
            return systemEmployeeID;
        }

        public static PointUserInfo GetSystemEmployee(IUnitOfWork uow)
        {
            var systemEmployeeID = GetSystemEmployeeID();
            var systemEmployee = GetPointUserInfoByEmployeeID(uow, systemEmployeeID);
            if (systemEmployee == null)
            {
                throw new Exception(string.Concat("SystemEmployee not found, ID provided: ", systemEmployeeID));
            }
            return systemEmployee;
        }
    }
}