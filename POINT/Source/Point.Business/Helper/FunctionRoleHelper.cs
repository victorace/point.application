﻿using System.Collections.Generic;
using Point.Business.Logic;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using System.Linq;

namespace Point.Business.Helper
{
    public class FunctionRoleHelper
    {
        public static FunctionRoleAccessModel GetAccessLevel(IUnitOfWork uow, FunctionRoleTypeID functionroletypeid, PointUserInfo pointuserinfo, IList<int> organizationTypeIDsFilter)
        {
            var accesslevel = new FunctionRoleAccessModel();

            if (functionroletypeid == FunctionRoleTypeID.Reporting)
            {
                accesslevel.MaxLevel = pointuserinfo.GetReportFunctionRoleLevelID();
            }
            else
            {
                accesslevel.MaxLevel = pointuserinfo.GetFunctionRoleLevelID(functionroletypeid);
            }

            var organizationTypesCount = organizationTypeIDsFilter.Count;
            var multiOrganizationTypes = (organizationTypesCount > 1);

            switch (accesslevel.MaxLevel)
            {
                case FunctionRoleLevelID.None:
                    accesslevel.LocationIDs = new int[0];
                    accesslevel.OrganizationIDs = new int[0];
                    accesslevel.RegionIDs = new int[0];
                    break;

                case FunctionRoleLevelID.Department:
                    accesslevel.LocationIDs = LocationBL.GetActiveByDepartmentIDs(uow, pointuserinfo.EmployeeDepartmentIDs).Select(it => it.LocationID).ToArray();
                    accesslevel.OrganizationIDs = OrganizationBL.GetByLocationIDs(uow, accesslevel.LocationIDs).Select(it => it.OrganizationID).ToArray();
                    accesslevel.RegionIDs = RegionBL.GetByOrganizationIDs(uow, accesslevel.OrganizationIDs).Select(it => it.RegionID).ToArray();
                    break;

                case FunctionRoleLevelID.Location:
                    accesslevel.LocationIDs = pointuserinfo.EmployeeLocationIDs.ToArray();
                    accesslevel.OrganizationIDs = OrganizationBL.GetByLocationIDs(uow, accesslevel.LocationIDs).Select(it => it.OrganizationID).ToArray();
                    accesslevel.RegionIDs = RegionBL.GetByOrganizationIDs(uow, accesslevel.OrganizationIDs).Select(it => it.RegionID).ToArray();
                    break;

                case FunctionRoleLevelID.Organization:
                    accesslevel.LocationIDs = LocationBL.GetActiveByOrganisationIDs(uow, pointuserinfo.EmployeeOrganizationIDs).Select(it => it.LocationID).ToArray();
                    accesslevel.OrganizationIDs = pointuserinfo.EmployeeOrganizationIDs.ToArray();
                    accesslevel.RegionIDs = RegionBL.GetByOrganizationIDs(uow, accesslevel.OrganizationIDs).Select(it => it.RegionID).ToArray();
                    break;

                case FunctionRoleLevelID.Region:
                    accesslevel.RegionIDs = new int[] { pointuserinfo.Region.RegionID };
                    var organizations = organizationTypesCount == 0
                        ? OrganizationBL.GetActiveByRegionID(uow, pointuserinfo.Region.RegionID)
                        : 
                        (multiOrganizationTypes ? OrganizationBL.GetActiveByRegionIDAndOrganizationTypeIDs(uow, pointuserinfo.Region.RegionID, organizationTypeIDsFilter)
                            : OrganizationBL.GetActiveByRegionIDAndOrganizationTypeID(uow, pointuserinfo.Region.RegionID, organizationTypeIDsFilter[0]));
                    accesslevel.OrganizationIDs = organizations.Select(it => it.OrganizationID).ToArray();
                    accesslevel.LocationIDs = LocationBL.GetActiveByOrganisationIDs(uow, accesslevel.OrganizationIDs).Select(it => it.LocationID).ToArray();
                    break;

                case FunctionRoleLevelID.Global:
                    accesslevel.RegionIDs = RegionBL.GetAll(uow).Select(it => it.RegionID).ToArray();
                    var organizationsglobal = organizationTypesCount == 0
                        ? OrganizationBL.GetActive(uow)
                        : (multiOrganizationTypes ? OrganizationBL.GetActiveByOrganizationTypeIDs(uow, organizationTypeIDsFilter) 
                            : OrganizationBL.GetActiveByOrganizationTypeID(uow, organizationTypeIDsFilter[0]));
                    accesslevel.OrganizationIDs = organizationsglobal.Select(it => it.OrganizationID).ToArray();

                    var locationsglobal = organizationTypesCount == 0
                        ? LocationBL.GetActive(uow)
                        : (multiOrganizationTypes ? LocationBL.GetActiveByOrganizationTypeIDs(uow, organizationTypeIDsFilter)
                            : LocationBL.GetActiveByOrganizationTypeID(uow, organizationTypeIDsFilter[0]));
                    accesslevel.LocationIDs = locationsglobal.Select(it => it.LocationID).ToArray();
                    break;
            }

            return accesslevel;

        }

        public static FunctionRoleAccessModel GetAccessLevel(IUnitOfWork uow, FunctionRoleTypeID functionroletypeid,
            PointUserInfo pointuserinfo, OrganizationTypeID? organizationtypeidfilter = null)
        {
            var organizationTypesFilter = new List<int>();
            if (organizationtypeidfilter != null)
            {
                organizationTypesFilter.Add((int)organizationtypeidfilter);
            }
            
            return GetAccessLevel(uow, functionroletypeid, pointuserinfo, organizationTypesFilter);
        }
    }
}