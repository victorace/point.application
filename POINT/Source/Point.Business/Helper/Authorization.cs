﻿using System.Collections.Generic;
using System.Linq;
using Point.Models.Enums;
using System;
using Point.Database.Repository;

namespace Point.Business.Helper
{
    public class Authorization
    {
        public static readonly List<Role> PointFlowRoles = new List<Role> {
            Role.None,
            Role.Department,
            Role.Location,
            Role.Organization,
            Role.Region,
            Role.Global,
        };

        public static Role GetPointFlowRole(IUnitOfWork uow, Guid? userId)
        {
            if(userId == null)
            {
                return Role.None;
            }

            var roles = uow.Aspnet_RolesRepository.Get(r => r.aspnet_Users.Any(u => u.UserId == userId)).Select(r => r.RoleName);

            foreach (var role in roles)
            {
                var first = PointFlowRoles.FirstOrDefault(it => it.ToString() == role);
                if (first != default(Role))
                    return first;
            }

            return Role.None;
        }
    }
}
