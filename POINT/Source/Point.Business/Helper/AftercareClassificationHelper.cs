﻿using System.Collections.Generic;
using System.Linq;
using Point.Database.Models;
using Point.Database.Models.ViewModels;

namespace Point.Business.Helper
{
    public class AftercareClassificationHelper
    {
        public static bool TransferPointValidateAftercareClassification(List<FlowWebField> model)
        {
            var aftercare = model.FirstOrDefault(m => m.FlowFieldID == FlowFieldConstants.ID_AfterCareDefinitive);
            return !string.IsNullOrEmpty(aftercare?.Value);
        }
    }
}
