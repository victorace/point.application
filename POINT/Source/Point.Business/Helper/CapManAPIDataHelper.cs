﻿using Point.Business.BusinessObjects;
using Point.Business.Logic;
using Point.Database.Context;
using Point.Database.Models;
using Point.Database.Repository;
using Point.Infrastructure.Attributes;
using Point.Infrastructure.Extensions;
using Point.Infrastructure.Helpers;
using Point.Log4Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Point.Business.Helper
{
    public class CapManAPIDataHelper
    {
        private static readonly ILogger Logger = LogManager.GetLogger(typeof(CapManAPIDataHelper));

        private static string getCapManUrl()
        {
            return ConfigHelper.GetAppSettingByName<string>("CapacityManagementAPIUrl", "");
        }

        public static bool HaveCapManUrl()
        {
            return !String.IsNullOrEmpty(getCapManUrl());
        }

        public static DepartmentCapacityAPI MapToAPIObject(Department department) => new DepartmentCapacityAPI
        {
            DepartmentCapacityID = 0,
            AfterCareTypeID = department.AfterCareType1ID,
            AfterCareTypeName = department.AfterCareType1?.Name,
            AfterCareGroupID = department.AfterCareType1?.AfterCareGroupID,
            RegionID = department.Location?.Organization?.RegionID.Value ?? 0,
            RegionName = department.Location?.Organization?.Region?.Name,
            OrganizationID = department.Location?.OrganizationID ?? 0,
            OrganizationName = department.Location?.Organization?.Name,
            LocationID = department.LocationID,
            LocationNumber = department.Location?.Number,
            LocationPostalCode = department.Location?.PostalCode,
            LocationStreet = department.Location?.StreetName,
            LocationName = department.Location?.Name,
            LocationCity = department.Location?.City,
            DepartmentID = department.DepartmentID,
            DepartmentEmailAddress = department.EmailAddress,
            DepartmentFaxNumber = department.FaxNumber,
            DepartmentName = department.Name,
            DepartmentPhoneNumber = department.PhoneNumber,
            IsActive = !department.Inactive,
            AdjustmentCapacityDate = department.AdjustmentCapacityDate,
            Capacity1 = department.CapacityDepartment,
            Capacity2 = department.CapacityDepartmentNext,
            Capacity3 = department.CapacityDepartment3,
            Capacity4 = department.CapacityDepartment4,
            Information = department.Information,
            ModifiedOn = DateTime.Now,
            CapacityFunctionality = department.CapacityFunctionality,
            AlternativeRegionIDs = department.Location?.LocationRegion?.Select(it => it.RegionID).ToList(),
        };

        public static async Task UpdateCapacityInCapMan(List<DepartmentCapacityAPI> departments)
        {
            try
            {
                var valueToSend = new DepartmentCapacityUpdateAPI();
                if (departments.Any())
                {
                    valueToSend.DigestValue = departments.Count();
                    valueToSend.Capacities = departments;
                    await UpdateDataInCapMan(valueToSend, "UpdateCapacity", "DigestValue", departments.Count(), AntiFiddleInjection.CreateDigest(valueToSend.DigestValue));
                }
            }
            catch (Exception exc)
            {
                Logger.Error(exc);
            }
            
        }

        public static async Task UpdateRegionInCapMan(int regionid)
        {
            try
            {
                RegionCapacityUpdateAPI valueToSend = null;
                using (var uow = new UnitOfWork<PointContext>())
                {
                    var region = RegionBL.GetByRegionID(uow, regionid);
                    if (region != null)
                    {
                        valueToSend = new RegionCapacityUpdateAPI();
                        valueToSend.RegionID = region.RegionID;
                        valueToSend.Name = region.Name;
                        valueToSend.CapacityBeds = (bool)region.CapacityBeds;
                    }
                }
                if (valueToSend == null)
                {
                    return;
                }

                await UpdateDataInCapMan(valueToSend, "UpdateRegion", "RegionID", regionid, AntiFiddleInjection.CreateDigest(regionid));
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
        }

        public static async Task UpdateOrganizationInCapMan(int organizationId)
        {
            try
            {
                OrganizationCapacityUpdateAPI valueToSend = null;
                using (var uow = new UnitOfWork<PointContext>())
                {
                    var org = OrganizationBL.GetByOrganizationID(uow, organizationId);
                    if (org != null)
                    {
                        valueToSend = new OrganizationCapacityUpdateAPI();
                        valueToSend.OrganizationID = org.OrganizationID;
                        valueToSend.OrganizationName = org.Name;
                        valueToSend.RegionID = org.Region.RegionID;
                        valueToSend.RegionName = org.Region.Name;
                        valueToSend.IsActive = !org.Inactive;
                        valueToSend.ModifiedOn = DateTime.Now;
                    }
                }
                if (valueToSend == null)
                {
                    return;
                }

                await UpdateDataInCapMan(valueToSend, "UpdateOrganization", "OrganizationID", organizationId, AntiFiddleInjection.CreateDigest(organizationId));
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
        }

        public static async Task UpdateLocationInCapMan(int locationId)
        {
            try
            {
                LocationCapacityUpdateAPI valueToSend = null;
                using (var uow = new UnitOfWork<PointContext>())
                {
                    var loc = LocationBL.GetByID(uow, locationId);
                    if (loc != null)
                    {
                        valueToSend = new LocationCapacityUpdateAPI();
                        valueToSend.LocationID = loc.LocationID;
                        valueToSend.LocationCity = loc.City;
                        valueToSend.LocationName = loc.Name;
                        valueToSend.LocationNumber = loc.Number;
                        valueToSend.LocationPostalCode = loc.PostalCode;
                        valueToSend.LocationStreet = loc.StreetName;
                        valueToSend.IsActive = !loc.Inactive;
                        valueToSend.ModifiedOn = DateTime.Now;
                        valueToSend.AlternativeRegionIDs = loc.LocationRegion?.Select(it => it.RegionID).ToList();
                    }
                }
                if (valueToSend == null)
                {
                    return;
                }

                await UpdateDataInCapMan(valueToSend, "UpdateLocation", "LocationID", locationId, AntiFiddleInjection.CreateDigest(locationId));
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
        }

        public static async Task UpdateDepartmentInCapMan(int departmentId)
        {
            try
            {
                DepartmentCapacityAPI valueToSend = null;

                using (var uow = new UnitOfWork<PointContext>())
                {
                    var dep = DepartmentBL.GetByDepartmentID(uow, departmentId);
                    if (dep != null)
                    {
                        valueToSend = MapToAPIObject(dep);
                    }
                }
                if (valueToSend == null)
                {
                    return;
                }
                await UpdateDataInCapMan(valueToSend, "UpdateDepartment", "DepartmentID", departmentId, AntiFiddleInjection.CreateDigest(departmentId));
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
        }

        [HttpPost]
        private static async Task UpdateDataInCapMan(object data, string actionName, string paramName, int paramValue, string digest = "")
        {
            if (!HaveCapManUrl())
            {
                return;
            }
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add(paramName, paramValue.ToString());
                client.DefaultRequestHeaders.Add("Digest", digest);

                var apiUrl = getCapManUrl().UrlCombine(actionName);
                var response = await client.PostAsJsonAsync(apiUrl, data).ConfigureAwait(continueOnCapturedContext: false);
                if (response.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    var responseMsg = await response.Content.ReadAsStringAsync();
                    throw new Exception(string.Format("API Action name={0} .Response message={1}", actionName, responseMsg));
                }
            }
        }
    }
}
