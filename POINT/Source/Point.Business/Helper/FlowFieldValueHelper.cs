﻿using Point.Business.Logic;
using Point.Database.Extensions;
using Point.Database.Models.ViewModels;
using Point.Infrastructure;
using Point.Infrastructure.Extensions;
using Point.Infrastructure.Helpers;
using Point.Models.Enums;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Web;
using Newtonsoft.Json;
using Point.Database.Repository;

namespace Point.Business.Helper
{
    public static class FlowFieldValueHelper
    {
        public static List<Option> GetOptionsMultiSelect(IUnitOfWork uow, int formSetVersionID, int flowFieldID, bool addSelectedNone, bool selectedOnly)
        {
            var flowField = FlowFieldBL.GetByFlowFieldID(uow, flowFieldID);

            if (flowField == null)
            {
                return null;
            }

            if (flowField.Type != FlowFieldType.DropDownListMultiSelect || string.IsNullOrEmpty(flowField.FlowFieldDataSource?.JsonData))
            {
                return null;
            }

            List<string> selectedItems = null;

            if (formSetVersionID > -1)
            {
                var flowFieldValue = FlowFieldValueBL.GetByFormsetVersionIDAndFlowFieldID(uow, formSetVersionID, flowFieldID);


                if (flowFieldValue != null)
                {
                    selectedItems = (flowFieldValue.Value ?? "").Replace(" ", "").Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();
                }
            }

            var datasourceList = JsonConvert.DeserializeObject<FlowFieldDataSourceViewModel>(flowField.FlowFieldDataSource?.JsonData);

            if (datasourceList == null)
            {
                return null;
            }

            var options = new List<Option>();
            var isFirst = true;

            foreach (var option in datasourceList.Options)
            {
                var selected = selectedItems != null && selectedItems.Contains(option.Value);
                if (addSelectedNone && isFirst || !string.IsNullOrEmpty(option.Value) && (selected || !selectedOnly))
                {
                    option.Selected = selected;
                    options.Add(option);
                }
                isFirst = false;
            }

            return options;
        }

        public static string GetDisplayValue(FlowWebField flowWebField)
        {
            if (string.IsNullOrWhiteSpace(flowWebField.Value))
            {
                return "";
            }

            if (flowWebField.Type == FlowFieldType.DateTime || flowWebField.Type == FlowFieldType.Date)
            {
                if (DateTime.TryParseExact(flowWebField.Value, "yyyy-MM-dd HH:mm:ss.fff", Thread.CurrentThread.CurrentCulture, DateTimeStyles.None, out DateTime dt))
                {
                    string format = (!String.IsNullOrWhiteSpace(flowWebField.Format)
                        ? flowWebField.Format
                        : (flowWebField.Type == FlowFieldType.DateTime ? FlowWebField.DEFAULT_DATETIME_FORMAT : FlowWebField.DEFAULT_DATE_FORMAT));

                    return dt.ToString(format, CultureInfo.InvariantCulture);
                }
            }

            if (flowWebField.FlowFieldDataSource == null)
            {
                return flowWebField.Value;
            }

            var selectedOptions = flowWebField.FlowFieldDataSource.Options.Where(o => o.Selected).ToList();

            return string.Join(", ", selectedOptions.Select(x => x.Text));
        }

        private static LookUpBL getLookupBL(IDictionary<string, object> viewData)
        {
            if(viewData == null)
            {
                return new LookUpBL();
            }

            var key = "LookUpBL";

            var lookupbl = viewData.ContainsKey(key) ? (viewData[key] as LookUpBL) : null;
            if (lookupbl == null)
            {
                lookupbl = new LookUpBL();
                if (viewData != null && !viewData.ContainsKey(key))
                {
                    viewData.Add(key, lookupbl);
                }
            }

            return lookupbl;
        }

        public static string GetDisplayValueMultiSelect(IUnitOfWork uow, int formSetVersionID, int flowFieldID, bool addSelectedNone, bool selectedOnly)
        {
            var options = GetOptionsMultiSelect(uow, formSetVersionID, flowFieldID, addSelectedNone, selectedOnly);

            if (options == null)
            {
                return string.Empty;
            }

            var optionsString = "";
            foreach (var option in options)
            {
                optionsString += option.Text + ",";
            }

            return optionsString.TrimEnd(',');
        }

        public static List<Option> GetOptions(FlowFieldDataSourceViewModel datasourceviewmodel, IDictionary<string, object> viewData, string value)
        {
            var options = new List<Option>();

            if (datasourceviewmodel == null || datasourceviewmodel.Type == FlowFieldDataSourceType.None)
            {
                return options;
            }

            if (string.IsNullOrEmpty(datasourceviewmodel.MethodName))
            {
                options = datasourceviewmodel.Options;
            }
            else
            {
                var lookupBL = getLookupBL(viewData);

                var controllerName = datasourceviewmodel.MethodName.GetURLComponent(1);
                var actionName = datasourceviewmodel.MethodName.GetURLComponent(2);
                if (string.IsNullOrWhiteSpace(controllerName) || string.IsNullOrWhiteSpace(actionName))
                {
                    return options;
                }

                var actionMethod = lookupBL.GetType().GetMethods().FirstOrDefault(method => method.IsPublic && method.Name.Equals(actionName, StringComparison.InvariantCultureIgnoreCase));
                if (actionMethod != null)
                {
                    NameValueCollection parameterinfos = null;
                    try
                    {
                        parameterinfos = GetParameters(datasourceviewmodel, viewData, value);
                        var invokeParameters = GetInvokeParameters(actionMethod, parameterinfos);

                        options = (List<Option>)actionMethod.Invoke(lookupBL, invokeParameters);
                    }
                    catch (Exception exc)
                    {
                        //Toevoegen data voor debugging
                        Uri uri = Uri.TryCreate(ConfigHelper.GetAppSettingByName("WebServer", "") + datasourceviewmodel.MethodName, UriKind.Absolute, out uri) ? uri : null;
                        var postdata = string.Format("{0}", parameterinfos?.AsQueryString());

                        ExceptionHelper.SendMessageException(exc, uri: uri, postdata: postdata);
                    }
                }
            }

            return options;
        }

        private static string GetValue(this IDictionary<string, object> viewData, string key)
        {
            if (viewData == null) return "";

            var keys = viewData.Keys.ToArray();
            if(keys.Contains(key))
            {
                return viewData[key].ToString();
            }

            return "";
        }

        private static NameValueCollection GetParameters(FlowFieldDataSourceViewModel datasourceviewmodel, IDictionary<string, object> viewData, string value)
        {
            var parameters = HttpUtility.ParseQueryString(datasourceviewmodel?.MethodName.GetURLComponent(3), Encoding.UTF8);
            foreach (var key in parameters.AllKeys)
            {
                switch (key.ToLower())
                {
                    case "formtypeid":
                        parameters["formtypeid"] = viewData.GetValue("FormTypeID");
                        break;
                    case "phaseinstanceid":
                        parameters["phaseinstanceid"] = viewData.GetValue("PhaseInstanceID");
                        break;
                    case "flowdefinitionid":
                        parameters["flowdefinitionid"] = viewData.GetValue("FlowDefinitionID");
                        break;
                    case "transferid":
                        parameters["transferid"] = viewData.GetValue("TransferID");
                        break;
                    case "phasedefinitionid":
                        parameters["phasedefinitionid"] = viewData.GetValue("PhaseDefinitionID");
                        break;
                    case "currentvalue":
                        parameters["currentvalue"] = value;
                        break;
                    default:
                        //Could be better...
                        if (parameters[key] == "{0}" || parameters[key] == "{1}" || parameters[key] == "{2}")
                        {
                            parameters[key] = value;
                        }
                        break;
                }
            }

            return parameters;
        }

        public static object[] GetInvokeParameters(MethodInfo actionMethod, NameValueCollection parameters)
        {
            if (parameters.Count == 0)
            {
                return null;
            }

            var parameterinfos = actionMethod.GetParameters();
            if (!parameterinfos.Any())
            {
                return null;
            }

            var invokeParameters = new object[parameters.AllKeys.Length];
            var i = 0;

            foreach (var parameterkey in parameters.AllKeys)
            {
                var value = parameters[parameterkey];

                var parameterinfo = parameterinfos.FirstOrDefault(pi => pi.Name.Equals(parameterkey, StringComparison.InvariantCultureIgnoreCase));
                if (parameterinfo == null)
                {
                    continue;
                }

                invokeParameters[i++] = value.ConvertTo<object>(parameterinfo.ParameterType);
            }

            return invokeParameters;
        }
    }
}