﻿using Point.Database.Models.ViewModels;
using Point.Globals;
using Point.Infrastructure.Extensions;
using Point.Infrastructure.Helpers;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Point.Business
{
    public class FlowFieldDataSourceExecutor
    {
        private readonly FlowWebField flowWebField;
        private readonly ViewDataDictionary ViewData;

        public FlowFieldDataSourceExecutor(FlowWebField flowWebField, ViewDataDictionary viewData)
        {
            this.flowWebField = flowWebField;
            ViewData = viewData;
        }

        public bool IsVertical => flowWebField.FlowFieldDataSource != null && flowWebField.FlowFieldDataSource.Type == FlowFieldDataSourceType.VerticalList;

        public IList<Option> Execute()
        {
            var options = new List<Option>();

            if(ViewData != null && ViewData.ContainsKey(flowWebField.Name))
            {
                var viewdatadata = ViewData[flowWebField.Name];
                if (viewdatadata is List<Option>)
                    return viewdatadata as List<Option>;
            }

            if (flowWebField.FlowFieldDataSource == null || flowWebField.FlowFieldDataSource != null && flowWebField.FlowFieldDataSource.Type == FlowFieldDataSourceType.None) 
                return options;

            if (flowWebField.FlowFieldDataSource.Type == FlowFieldDataSourceType.List || flowWebField.FlowFieldDataSource.Type == FlowFieldDataSourceType.VerticalList)
                options = flowWebField.FlowFieldDataSource.Options.ToList();
            else
            {
                var controllerName = flowWebField.FlowFieldDataSource.MethodName.GetURLComponent(1);
                var actionName = flowWebField.FlowFieldDataSource.MethodName.GetURLComponent(2);
                if (string.IsNullOrWhiteSpace(controllerName) || string.IsNullOrWhiteSpace(actionName))
                    return options;
                
                var parameters = HttpUtility.ParseQueryString(flowWebField.FlowFieldDataSource.MethodName.GetURLComponent(3), Encoding.UTF8);

                foreach (var key in parameters.AllKeys)
                {
                    switch (key.ToLower())
                    {
                        case "formtypeid":
                            parameters["formtypeid"] = ViewData?["FormTypeID"].ToString();
                            break;
                        case "phaseinstanceid":
                            parameters["phaseinstanceid"] = ViewData?["PhaseInstanceID"].ToString();
                            break;
                        case "flowdefinitionid":
                            parameters["flowdefinitionid"] = ViewData?["FlowDefinitionID"].ToString();
                            break;
                        case "transferid":
                            parameters["transferid"] = ViewData?["TransferID"].ToString();
                            break;
                        case "phasedefinitionid":
                            parameters["phasedefinitionid"] = ViewData?["PhaseDefinitionID"].ToString();
                            break;
                        default:
                            //Could be better...
                            if (parameters[key] == "{0}" || parameters[key] == "{1}" || parameters[key] == "{2}")
                                parameters[key] = flowWebField.Value;

                            break;
                    }
                }

                //NameValueCollection parameters = HttpUtility.ParseQueryString(queryString);
                options = invoke(controllerName, actionName, parameters);
            }

            foreach (var option in options)
            {
                if (option.Value == null)
                    option.Value = option.Text;
            }

            return options;
        }

        public string GetDisplayValue()
        {
            if (string.IsNullOrWhiteSpace(flowWebField.Value)) return "";

            var options = Execute().ToList();

            options.ForEach(it => it.Selected = it.Selected || it.Value == flowWebField.Value);
            var selectedoptions = options.Where(it => it.Selected).ToList();

            return string.Join(", ", selectedoptions.Select(x => x.Text));
        }

        private static List<Option> invoke(string controllerName, string actionName, NameValueCollection parameters)
        {
            var options = new List<Option>();

            try
            {
                var sControllerTypeName = string.Concat(AssemblyNames.PointMvcWebApplicationControllers, ".",
                    controllerName, "Controller");
                var controllerType = GetTypeByNameHelper.GetType(sControllerTypeName);
                var currentassembly = controllerType.Assembly;
                var actionMethod = controllerType.GetMethods().FirstOrDefault(method => method.IsPublic && method.Name.Equals(actionName, StringComparison.InvariantCultureIgnoreCase));
                if (actionMethod != null)
                {
                    var parameterinfos = actionMethod.GetParameters();
                    if (!parameterinfos.Any())
                    {
                        var instance = currentassembly.CreateInstance(controllerType.FullName);
                        options = (List<Option>)actionMethod.Invoke(instance, null);
                        if(instance is IDisposable)
                        {
                            ((IDisposable)instance).SafeDispose();
                        }
                    }
                    else
                    {
                        var invokeParameters = new object[parameters.AllKeys.Length];
                        var i = 0;

                        foreach (var parameterkey in parameters.AllKeys)
                        {
                            var value = parameters[parameterkey];

                            var parameterinfo =
                                parameterinfos.FirstOrDefault(
                                    pi => pi.Name.Equals(parameterkey, StringComparison.InvariantCultureIgnoreCase));
                            if (parameterinfo == null) continue;

                            if (parameterinfo.ParameterType.IsGenericType &&
                                parameterinfo.ParameterType.GetGenericTypeDefinition() == typeof(Nullable<>))
                            {
                                if (!string.IsNullOrEmpty(value))
                                {
                                    var type = Nullable.GetUnderlyingType(parameterinfo.ParameterType) ??
                                                parameterinfo.ParameterType;
                                    invokeParameters[i++] = Convert.ChangeType(value, type);
                                }
                                else
                                {
                                    invokeParameters[i++] = null;
                                }
                            }
                            else if (parameterinfo.ParameterType == typeof(string))
                            {
                                invokeParameters[i++] = value;
                            }
                            else if (!String.IsNullOrEmpty(value))
                            {
                                invokeParameters[i++] = Convert.ChangeType(value, parameterinfo.ParameterType);
                            }
                        }

                        if (i == parameterinfos.Length)
                        {
                            var instance = currentassembly.CreateInstance(controllerType.FullName);
                            options = (List<Option>)actionMethod.Invoke(instance,invokeParameters);
                            if (instance is IDisposable)
                            {
                                ((IDisposable)instance).SafeDispose();
                            }
                        }
                    }
                }
            }
            catch
            {
                /* ignore */
            }

            return options;
        }
    }
}