﻿using Point.Infrastructure.Extensions;
using System;
using System.Web;

namespace Point.Business.Helper
{
    public static class ExternalPatientHelper
    {
        public const string paramPatExternalReference1 = "patexternalreference1";           // Used by common 'koppeling'
        public const string paramPatExternalReferenceBSN1 = "patexternalreferencebsn1";     // Used by common 'koppeling'
        public const string paramExternalReference1 = "externalreference1";                 // Used by webservice-customers (Spaarne/Haga)
        public const string paramPatExternalReference = "patExternalReference";             // Used internally
        public const string paramIsAuth = "isauth";                                         // POINT.Auth has been used for authentication

        public static string GetPatExternalReference()
        {
            string patExternalReference = "";

            if (HttpContext.Current.Session[paramPatExternalReference] != null)
            {
                patExternalReference = HttpContext.Current.Session[paramPatExternalReference].ToString();
            }
            return patExternalReference;
        }

        public static string GetPatExternalReferenceBSN()
        {
            string patExternalReferenceBSN = "";

            if (HttpContext.Current.Session[paramPatExternalReferenceBSN1] != null)
            {
                patExternalReferenceBSN = HttpContext.Current.Session[paramPatExternalReferenceBSN1].ToString();
            }
            return patExternalReferenceBSN;
        }

        public static void SetPatExternalReference(string patExternalReference)
        {
            HttpContext.Current.Session[paramPatExternalReference] = patExternalReference.ToAlphanumeric();
        }

        public static void SetPatExternalReferenceBSN(string patExternalReferencebsn)
        {
            HttpContext.Current.Session[paramPatExternalReferenceBSN1] = patExternalReferencebsn.ToAlphanumeric();
        }

        public static bool HavePatExternalReference()
        {
            return !String.IsNullOrWhiteSpace(GetPatExternalReference()) || !String.IsNullOrWhiteSpace(GetPatExternalReferenceBSN());
        }

        public static bool HavePatExternalReferenceBSN()
        {
            return !String.IsNullOrWhiteSpace(GetPatExternalReferenceBSN());
        }

        public static void ClearPatExternalReference(bool hl7ConnectionDifferentMenu)
        {
            if (!hl7ConnectionDifferentMenu) // keep 'patientnumber' and the "focus" on the patient
            {
                if (HttpContext.Current.Session[paramPatExternalReference] != null)
                    HttpContext.Current.Session.Remove(paramPatExternalReference);
            }
        }

        public static void ClearPatExternalReferenceBSN(bool hl7ConnectionDifferentMenu)
        {
            if (!hl7ConnectionDifferentMenu) // keep 'BSN' and the "focus" on the patient
            {
                if (HttpContext.Current.Session[paramPatExternalReferenceBSN1] != null)
                    HttpContext.Current.Session.Remove(paramPatExternalReferenceBSN1);
            }
        }

        public static bool HaveConnectionDifferentMenu(bool hl7ConnectionDifferentMenu)
        {
            if (!hl7ConnectionDifferentMenu)
                return false;
            else
                return HavePatExternalReference();

        }

        public static string ReadPatExternalReferenceFromRequest(HttpRequestBase httpRequestBase, bool isSSO = false)
        {
            string patexternalreference = "";

            if (HavePatExternalReferenceFromRequest(httpRequestBase))
            {
                patexternalreference = httpRequestBase.Params.GetValueOrDefault<string>(paramPatExternalReference1, "");
                if (String.IsNullOrEmpty(patexternalreference) && !isSSO)
                {
                    patexternalreference = httpRequestBase.Params.GetValueOrDefault<string>(paramExternalReference1, "");
                }
                if (String.IsNullOrEmpty(patexternalreference))
                {
                    patexternalreference = httpRequestBase.Params.GetValueOrDefault<string>(paramPatExternalReference, "");
                }
            }

            return patexternalreference;
        }

        public static string ReadPatExternalReferenceBSNFromRequest(HttpRequestBase httpRequestBase, bool isSSO = false)
        {
            string patexternalreference = "";

            if (HavePatExternalReferenceBSNFromRequest(httpRequestBase))
            {
                patexternalreference = httpRequestBase.Params.GetValueOrDefault<string>(paramPatExternalReferenceBSN1, "");
            }

            return patexternalreference;
        }

        public static bool HavePatExternalReferenceFromRequest(HttpRequestBase httpRequestBase)
        {
            return (httpRequestBase.Params.ContainsKey(paramPatExternalReference1)
                    || httpRequestBase.Params.ContainsKey(paramExternalReference1)
                    || httpRequestBase.Params.ContainsKey(paramPatExternalReference));

        }

        public static bool HavePatExternalReferenceBSNFromRequest(HttpRequestBase httpRequestBase)
        {
            return httpRequestBase.Params.ContainsKey(paramPatExternalReferenceBSN1);
        }


        public static bool GetIsAuth()
        {
            if (HaveIsAuth())
            {
                return HttpContext.Current.Session[paramIsAuth].ToString().StringToBool();
            }
            return false;
        }

        public static void SetIsAuth(bool isauth)
        {
            HttpContext.Current.Session[paramIsAuth] = isauth;
        }

        public static bool HaveIsAuth()
        {
            return HttpContext.Current.Session[paramIsAuth] != null;
        }

        public static void ClearIsAuth()
        {
            if (HaveIsAuth())
            {
                HttpContext.Current.Session.Remove(paramIsAuth);
            }
        }

        public static bool ReadIsAuthFromRequest(HttpRequestBase httpRequestBase)
        {
            if (HaveIsAuthFromRequest(httpRequestBase))
            {
                return httpRequestBase.Params.GetValueOrDefault<bool>(paramIsAuth, false);
            }

            return false;
        }

        public static bool HaveIsAuthFromRequest(HttpRequestBase httpRequestBase)
        {
            return httpRequestBase.Params.ContainsKey(paramIsAuth);
        }
    }
}