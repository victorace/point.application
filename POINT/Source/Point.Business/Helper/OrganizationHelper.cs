﻿using Point.Business.BusinessObjects;
using Point.Business.Logic;
using Point.Database.Extensions;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Extensions;
using Point.Models.Enums;
using Point.ServiceBus.Models;
using System;
using System.Collections.Generic;
using Point.Infrastructure.Constants;
using System.Linq;

namespace Point.Business.Helper
{
    public class OrganizationHelper
    {
        public static Organization GetSendingOrganization(IUnitOfWork uow, int flowinstanceid)
        {
            var flowinstance = FlowInstanceBL.GetByID(uow, flowinstanceid);

            return flowinstance?.StartedByOrganization;
        }

        public static int? GetSendingOrganizationId(IUnitOfWork uow, int? flowInstanceId)
        {
            if (flowInstanceId == null)
            {
                return null;
            }

            var flowInstance = FlowInstanceBL.GetByID(uow, flowInstanceId.Value);
            return flowInstance?.StartedByOrganization?.OrganizationID;
        }

        public static Organization GetReceivingOrganization(IUnitOfWork uow, int flowinstanceid)
        {
            var invite = OrganizationInviteBL.GetAcceptedOrActiveInvitesByFlowInstanceIDAndInviteType(uow, flowinstanceid, InviteType.Regular).FirstOrDefault();
            return invite?.Organization;
        }

        public static Location GetSendingLocation(IUnitOfWork uow, int flowinstanceid)
        {
            var department = GetSendingDepartment(uow, flowinstanceid);
            return department?.Location;
        }

        public static Location GetRecievingLocation(IUnitOfWork uow, int flowinstanceid)
        {
            var department = GetRecievingDepartment(uow, flowinstanceid);
            return department?.Location;
        }

        public static Employee GetRecievingEmployee(IUnitOfWork uow, int flowinstanceid)
        {
            FlowFieldValue employeeflowfield = null;
            var flowinstance = FlowInstanceBL.GetByID(uow, flowinstanceid);

            if (flowinstance.FlowDefinitionID == FlowDefinitionID.ZH_VVT)
            {
                employeeflowfield = FlowFieldValueBL.GetByFlowInstanceIDAndFieldID(uow, flowinstanceid, FlowFieldConstants.ID_AcceptedByVVT).FirstOrDefault();
            }

            if (flowinstance.FlowDefinitionID == FlowDefinitionID.VVT_ZH)
            {
                employeeflowfield = FlowFieldValueBL.GetByFlowInstanceIDAndFieldID(uow, flowinstanceid, FlowFieldConstants.ID_AcceptedBy).FirstOrDefault();
            }

            return string.IsNullOrEmpty(employeeflowfield?.Value) 
                ? null 
                : EmployeeBL.GetByID(uow, Convert.ToInt32(employeeflowfield.Value));
        }

        public static Employee GetSendingEmployee(IUnitOfWork uow, int flowinstanceid)
        {
            var flowinstance = FlowInstanceBL.GetByID(uow, flowinstanceid);

            return flowinstance?.StartedByEmployee;
        }

        public static PhaseInstance GetTransferPointPhaseInstance(IUnitOfWork uow, int flowinstanceid)
        {
            return PhaseInstanceBL.GetByFlowInstanceIDAndFormTypeID(uow, flowinstanceid, (int)FlowFormType.InBehandelingTP);
        }

        public static Employee GetTransferPointEmployee(IUnitOfWork uow, int flowInstanceID)
        {
            var employeeflowfield = FlowFieldValueBL.GetByFlowInstanceIDAndFormTypeIDAndFieldIDs(uow, flowInstanceID, (int)FlowFormType.InBehandelingTP, new int[] { FlowFieldConstants.ID_AcceptedBy }).FirstOrDefault();

            return string.IsNullOrEmpty(employeeflowfield?.Value) 
                ? null 
                : EmployeeBL.GetByID(uow, Convert.ToInt32(employeeflowfield.Value));
        }

        public static Employee GetCIZEmployee(IUnitOfWork uow, int transferid)
        {
            var employeeflowfield = FlowFieldValueBL.GetByTransferIDAndFormTypeIDAndFieldID(uow, transferid, (int)FlowFormType.InBehandelingIIZHVVT, FlowFieldConstants.ID_AcceptedByII);

            return string.IsNullOrEmpty(employeeflowfield?.Value) 
                ? null 
                : EmployeeBL.GetByID(uow, Convert.ToInt32(employeeflowfield.Value));
        }

        public static Employee GetVVTEmployee(IUnitOfWork uow, int transferid)
        {
            var flowinstance = FlowInstanceBL.GetByTransferID(uow, transferid);
            FlowFieldValue employeeflowfield = null;

            if (flowinstance.FlowDefinitionID == FlowDefinitionID.VVT_ZH)
            {
                var formsetversion = FormSetVersionBL.GetByTransferIDAndFormTypeID(uow, transferid, (int)FlowFormType.VersturenNaarZH).FirstOrDefault();
                if (formsetversion != null)
                {
                    return EmployeeBL.GetByID(uow, formsetversion.CreatedByID);
                }
            }

            if (flowinstance.FlowDefinitionID == FlowDefinitionID.ZH_VVT || flowinstance.FlowDefinitionID == FlowDefinitionID.CVA)
            {
                employeeflowfield = FlowFieldValueBL.GetByTransferIDAndFormTypeIDAndFieldID(uow, transferid, (int)FlowFormType.InBehandelingVVTZHVVT, FlowFieldConstants.ID_AcceptedByVVT);
            }

            return string.IsNullOrEmpty(employeeflowfield?.Value) 
                ? null 
                : EmployeeBL.GetByID(uow, Convert.ToInt32(employeeflowfield.Value));
        }

        public static Department GetSendingDepartment(IUnitOfWork uow, int flowinstanceid)
        {
            var flowinstance = FlowInstanceBL.GetByID(uow, flowinstanceid);
            return flowinstance?.StartedByDepartment;
        }

        public static Department GetRecievingDepartment(IUnitOfWork uow, int flowinstanceid)
        {
            var invite = OrganizationInviteBL.GetAcceptedOrActiveInvitesByFlowInstanceIDAndInviteType(uow, flowinstanceid, InviteType.Regular).FirstOrDefault();
            return invite?.Department;
        }

        public static Department GetPreviousReceivingDepartment(IUnitOfWork uow, int flowinstanceid)
        {
            var activeinvite = OrganizationInviteBL.GetAcceptedOrActiveInvitesByFlowInstanceIDAndInviteType(uow, flowinstanceid, InviteType.Regular).FirstOrDefault();
            if(activeinvite == null)
            {
                var allinvites = OrganizationInviteBL.GetByFlowInstanceIDAndInviteType(uow, flowinstanceid, InviteType.Regular).ToList();
                if (allinvites.Any())
                {
                    var previous = allinvites.OrderByDescending(it => it.InviteSend).FirstOrDefault();
                    return previous?.Department;
                }
            } else
            {
                var allinvites = OrganizationInviteBL.GetByFlowInstanceIDAndInviteType(uow, flowinstanceid, InviteType.Regular).Where(i => i.OrganizationInviteID != activeinvite.OrganizationInviteID).ToList();
                if (allinvites.Any())
                {
                    var previous = allinvites.OrderByDescending(it => it.InviteSend).FirstOrDefault();
                    return previous?.Department;
                }
            }

            return null;
        }

        public static void CreateCVARegistratie(IUnitOfWork uow, int flowinstanceid, LogEntry logentry)
        {
            var flowinstance = FlowInstanceBL.GetByID(uow, flowinstanceid);
            var formtypeid = (int)FlowFormType.InvullenCVARegistratie3mnd;

            var phasedefinition = PhaseDefinitionCacheBL.GetByFlowDefinitionAndFormType(uow, flowinstance.FlowDefinitionID, formtypeid);
            if (phasedefinition == null)
            {
                return;
            }

            //Creeer fase alleen 1x
            var phaseinstance = PhaseInstanceBL.GetByFlowInstanceIDAndFormTypeID(uow, flowinstanceid, formtypeid);
            if(phaseinstance == null)
            {
                phaseinstance = PhaseInstanceBL.Create(uow, flowinstanceid, phasedefinition.PhaseDefinitionID, logentry, Status.Active);
            } else if(phaseinstance.Status == (int)Status.Done)
            {
                PhaseInstanceBL.SetStatusActive(uow, phaseinstance, logentry);
            }

            FrequencyBL.CancelByType(uow, flowinstance.TransferID, FrequencyType.CVA, logentry);

            var startdate = DateTime.Today;
            var enddate = startdate.AddMonths(2);
            var quantity = 1;
            FrequencyBL.Create(uow, FrequencyType.CVA, flowinstance.TransferID, phasedefinition.PhaseName, startdate, enddate, quantity, logentry);

            uow.Save();
        }

        public static bool NeedsAcknowledgement(int formTypeID)
        {
            return formTypeID != (int)FlowFormType.InBehandelingIIZHVVT;
        }

        public static void SendInvite(IUnitOfWork uow, OrganizationInvite invite, string inviteUrl)
        {
            ClientBL.SetAnonymous(uow, invite.FlowInstanceID);

            invite.InviteSend = DateTime.Now;
            if(invite.InviteStatus == InviteStatus.Queued)
            {
                invite.InviteStatus = InviteStatus.Active;
            }

            uow.Save();

            var usesauth = OrganizationSettingBL.GetValue<bool>(uow, invite.OrganizationID, OrganizationSettingBL.UsesAuth);
            ServiceBusBL.SendMessage(uow, MessageQueues.PhaseQueue, "InviteNotification", new InviteNotification
            {
                TimeStamp = DateTime.Now,
                UniqueID = DateTime.Now.Ticks + invite.OrganizationInviteID,
                TransferID = invite.FlowInstance.TransferID,
                OrganizationInviteID = invite.OrganizationInviteID,
                EmployeeID = invite.CreatedByID,
                Url = inviteUrl,
                RedirectToAuth = usesauth,
                AuthOrganizationID = usesauth ? invite.OrganizationID : (int?)null
            });
        }

        public static void Invite(IUnitOfWork uow, int flowinstanceid, int sendingphasedefinitionid, int recievingphasedefinitionid, int departmentid, bool needsacknowledgement, InviteType inviteType, LogEntry logentry, string inviteUrl, InviteStatus inviteStatus = InviteStatus.Active)
        {
            var newinvite = OrganizationInviteBL.Add(uow, flowinstanceid, sendingphasedefinitionid, recievingphasedefinitionid, departmentid, needsacknowledgement, inviteType, logentry);
            OrganizationInviteBL.SetStatus(uow, newinvite, inviteStatus);

            SendInvite(uow, newinvite, inviteUrl);
        }
        
        public static void ClearDestinationField(IUnitOfWork uow, int flowinstanceid)
        {
            var flowinstance = FlowInstanceBL.GetByID(uow, flowinstanceid);

            switch (flowinstance.FlowDefinition.FlowTypeID)
            {
                case (int)FlowTypeID.VVT_ZH:
                case (int)FlowTypeID.ZH_ZH:
                    SetDestinationHospitalFields(uow, flowinstanceid, null, null);
                    break;

                default:
                    SetDestinationField(uow, flowinstanceid, null);
                    break;
            }
        }

        public static void SetDestinationHospitalFields(IUnitOfWork uow, int flowinstanceid, int? destinationLocationID, int? destinationDepartmentID)
        {
            var destinationHospitalLocationflowfieldvalues = FlowFieldValueBL.GetByFlowInstanceIDAndFieldID(uow, flowinstanceid, FlowFieldConstants.ID_DestinationHospitalLocation).ToList();
            foreach (var item in destinationHospitalLocationflowfieldvalues)
            {
                FlowFieldValueBL.SetValue(item, destinationLocationID.HasValue ? destinationLocationID.ToString() : "");
            }

            var destinationHospitalDepartmentflowfieldvalues = FlowFieldValueBL.GetByFlowInstanceIDAndFieldID(uow, flowinstanceid, FlowFieldConstants.ID_DestinationHospitalDepartment).ToList();
            foreach (var item in destinationHospitalDepartmentflowfieldvalues)
            {
                FlowFieldValueBL.SetValue(item, destinationDepartmentID.HasValue ? destinationDepartmentID.ToString() : "");
            }

            uow.Save();
        }

        public static void SetDestinationField(IUnitOfWork uow, int flowinstanceid, int? destinationdepartmentid)
        {
            var actuelevvtflowfieldvalues = FlowFieldValueBL.GetByFlowInstanceIDAndFieldID(uow, flowinstanceid, FlowFieldConstants.ID_DestinationHealthCareProvider).ToList();
            foreach (var item in actuelevvtflowfieldvalues)
            {
                FlowFieldValueBL.SetValue(item, destinationdepartmentid.HasValue ? destinationdepartmentid.ToString() : "");
            }
            uow.Save();
        }

        public static void CancelReceivingPhases(IUnitOfWork uow, int flowInstanceID, int recievingphasedefinitionid, LogEntry logentry)
        {
            CancelReceivingPhases(uow, flowInstanceID, new int[] { recievingphasedefinitionid }, logentry);
        }

        public static void CancelReceivingPhases(IUnitOfWork uow, int flowInstanceID, int[] recievingphasedefinitionids, LogEntry logentry)
        {
            var flowinstance = FlowInstanceBL.GetByID(uow, flowInstanceID);
            if (flowinstance != null)
            {
                var transferid = flowinstance.TransferID;
                var recievingphaseinstances = PhaseInstanceBL.GetByFlowinstanceIDAndPhaseDefinitionIDs(uow, flowinstance.FlowInstanceID, recievingphasedefinitionids).ToList();
                foreach (var recievingphaseinstance in recievingphaseinstances)
                {
                    if (!String.IsNullOrEmpty(recievingphaseinstance?.PhaseDefinition?.CodeGroup))
                    {
                        //Verwijder alles wat in dezelfde codegroup zit
                        PhaseInstanceBL.CancelByCodeGroup(uow, transferid, recievingphaseinstance.PhaseDefinition.CodeGroup, logentry);
                    }
                    else if (recievingphaseinstance != null)
                    {
                        //Niet in groep maar wel enkele phase cancellen
                        PhaseInstanceBL.Cancel(uow, recievingphaseinstance, logentry);
                    }
                }
                //Doorsturen naar afdeling ook cancellen
                PhaseInstanceBL.CancelByCodeGroup(uow, transferid, "SENDTODEP", logentry);
                //Afsluitfase ook cancellen
                PhaseInstanceBL.CancelByCodeGroup(uow, transferid, "CLOSE", logentry);
                //Ook eventuele VO cancellen zie pbi 10983
                PhaseInstanceBL.CancelByCodeGroup(uow, transferid, "SENDVO", logentry);
            }
        }

        public static void CancelInvite(IUnitOfWork uow, int flowinstanceid, int recievingphasedefinitionid, PointUserInfo pointUserInfo, bool sendnotication = true)
        {
            var oldinvites = OrganizationInviteBL.GetAcceptedOrActiveInvitesByFlowInstanceIDAndPhaseDefinitionID(uow, flowinstanceid, recievingphasedefinitionid);
            foreach (var oldinvite in oldinvites)
            {
                CancelInvite(uow, oldinvite, pointUserInfo, sendnotication);
            }
        }

        public static void CancelInvite(IUnitOfWork uow, OrganizationInvite invite, PointUserInfo pointUserInfo, bool sendnotication = true, string reason = null)
        {
            if (invite != null)
            {               
                //Cancel email
                if (sendnotication)
                {
                    ServiceBusBL.SendMessage(uow, MessageQueues.PhaseQueue, "CancelNotification", new CancelNotification
                    {
                        TimeStamp = DateTime.Now,
                        UniqueID = DateTime.Now.Ticks + invite.OrganizationInviteID,
                        OrganizationInviteID = invite.OrganizationInviteID,
                        SendingEmployeeID = pointUserInfo.Employee.EmployeeID,
                    });
                }                
                OrganizationInviteBL.SetHandlingData(uow, invite, pointUserInfo, reason);
                OrganizationInviteBL.SetStatus(uow, invite, InviteStatus.Cancelled);
            }
        }

        public static OrganizationInvite Queue(IUnitOfWork uow, int flowinstanceid, int sendingphasedefinitionid, int recievingphasedefinitionid, int departmentid, bool needsacknowledgement, InviteType inviteType, LogEntry logentry)
        {
            var invite = OrganizationInviteBL.Add(uow, flowinstanceid, sendingphasedefinitionid, recievingphasedefinitionid, departmentid, needsacknowledgement, inviteType, logentry);
            OrganizationInviteBL.SetStatus(uow, invite, InviteStatus.Queued);

            return invite;
        }

        public static void CheckAndSendVerpleegkundigeOverdracht(IUnitOfWork uow, FlowInstance flowInstance, PointUserInfo pointuserinfo, LogEntry logentry, bool forceresend)
        {
            var verpleegkundigeoverdrachtbo = new VerpleegkundigeOverdrachtBO(uow, flowInstance, pointuserinfo);
            verpleegkundigeoverdrachtbo.Send(forceresend);
            verpleegkundigeoverdrachtbo.HandleNextPhase(logentry);
        }

        public static void CheckAndSendSurvey(IUnitOfWork uow, FlowInstance flowinstance, PointUserInfo pointuserinfo)
        {
            var sendingorganization = OrganizationHelper.GetSendingOrganization(uow, flowinstance.FlowInstanceID);
            if (sendingorganization == null)
            {
                return;
            }

            var campaignid = OrganizationSettingBL.GetValue<string>(uow, sendingorganization.OrganizationID, OrganizationSettingBL.SurveyCampaignID);
            if (string.IsNullOrEmpty(campaignid))
            {
                return;
            }

            if (FlowInstanceBL.CanSendVO(uow, flowinstance))
            {
                var verpleegkundigeoverdrachtbo = new VerpleegkundigeOverdrachtBO(uow, flowinstance, pointuserinfo);
                if (verpleegkundigeoverdrachtbo.IsDefinitive())
                {
                    ServiceBusBL.SendMessage(uow, "Survey", "SendSurveyInvite", new SendSurveyInvite
                    {
                        TransferID = flowinstance.TransferID,
                        TimeStamp = DateTime.Now,
                        UniqueID = DateTime.Now.Ticks + flowinstance.TransferID
                    });
                }
            }
        }

        public static void CheckAndSendMedVRI(IUnitOfWork uow, FlowInstance flowInstance, PointUserInfo pointUserInfo)
        {
            // HA bericht: wordt op verkeerd moment verzonden https://linkassist.visualstudio.com/POINT.Application/_workitems/edit/14276
            if (PatientHelper.IsAccepted(uow, flowInstance.FlowInstanceID) == AcceptanceState.Acknowledged)
            {
                if (flowInstance.FlowDefinition.FlowTypeID == (int)FlowTypeID.HA_VVT)
                {
                    var receivingdepartment = OrganizationHelper.GetRecievingDepartment(uow, flowInstance.FlowInstanceID);
                    MedVRIBL.SendMedvriHAVVT(uow, flowInstance, FlowFormType.VastleggenBesluitVVTZHVVT, pointUserInfo, TemplateTypeID.ZorgMailHAVVTJa, receivingdepartment);
                }
                else if (flowInstance.FlowDefinition.FlowTypeID == (int)FlowTypeID.ZH_VVT)
                {
                    var verpleegkundigeoverdrachtbo = new VerpleegkundigeOverdrachtBO(uow, flowInstance, pointUserInfo);
                    if (verpleegkundigeoverdrachtbo.IsDefinitive() && MessageToGPByDefinitive(uow, flowInstance))
                    {
                        MedVRIBL.SendMedvri(uow, flowInstance, FlowFormType.VastleggenBesluitVVTZHVVT, pointUserInfo);
                    }
                }
            }
        }

        public static void Acknowledge(IUnitOfWork uow, OrganizationInvite inviteToAccept, PointUserInfo pointuserinfo, string remark = null)
        {
            if(inviteToAccept == null)
            {
                return;
            }

            OrganizationInviteBL.SetHandlingData(uow, inviteToAccept, pointuserinfo, remark);
            OrganizationInviteBL.SetStatus(uow, inviteToAccept, InviteStatus.Acknowledged);
        }

        public static void AcknowledgePartially(IUnitOfWork uow, OrganizationInvite inviteToAccept, LogEntry logentry, PointUserInfo pointuserinfo, string dashboardurl, bool voresend = false, string remark = null)
        {
            if (inviteToAccept == null)
            {
                return;
            }

            OrganizationInviteBL.SetHandlingData(uow, inviteToAccept, pointuserinfo, remark);
            OrganizationInviteBL.SetStatus(uow, inviteToAccept, InviteStatus.AcknowledgedPartially);
        }

        public static OrganizationInvite InviteQueuedInvite(IUnitOfWork uow, OrganizationInvite quedinvite, string dashboardUrl)
        {
            if (quedinvite != null)
            {
                SendInvite(uow, quedinvite, dashboardUrl);

                return quedinvite;
            }

            return null;
        }

        public static OrganizationInvite InviteQueued(IUnitOfWork uow, FlowInstance flowInstance, PointUserInfo pointUserInfo, string dashboardUrl)
        {
            var quedinvites = OrganizationInviteBL.GetQueuedInvitesByFlowInstanceIDAndInviteType(uow, flowInstance.FlowInstanceID, InviteType.Regular);
            if (quedinvites.Any())
            {
                return InviteQueuedInvite(uow, quedinvites.FirstOrDefault(), dashboardUrl);
            }

            return null;
        }

        public static void Refuse(IUnitOfWork uow, OrganizationInvite inviteToRefuse, PointUserInfo pointuserinfo, string reason)
        {
            if (inviteToRefuse == null)
            {
                return;
            }

            OrganizationInviteBL.SetHandlingData(uow, inviteToRefuse, pointuserinfo, reason);
            OrganizationInviteBL.SetStatus(uow, inviteToRefuse, InviteStatus.Refused);

            uow.Save();
        }

        public static void HandleTransferMemo(IUnitOfWork uow, FlowInstance flowInstance, List<FlowWebField> flowfieldvalues, PointUserInfo pointUserInfo)
        {

            var patientaccepted = flowfieldvalues.GetValueOrDefault(FlowFieldConstants.Name_DischargePatientAcceptedYNByVVT, ""); 
            var answer = "?";
            var comments = "";
            var date = string.Empty;

            if (patientaccepted.Equals("ja", StringComparison.InvariantCultureIgnoreCase))
            {
                answer = "Ja";
                comments = flowfieldvalues.GetValueOrDefault(FlowFieldConstants.Name_DischargeRemarks, "");
                var dischargeProposedStartDate = flowfieldvalues.GetValueOrDefault(FlowFieldConstants.Name_DischargeProposedStartDate, "");
                if (!string.IsNullOrWhiteSpace(dischargeProposedStartDate))
                {
                    date = $"Met ingang van: {dischargeProposedStartDate}";
                    var dischargeProposedTime = flowfieldvalues.GetValueOrDefault(FlowFieldConstants.Name_DischargePatientExpectedTime, "");
                    if (!string.IsNullOrEmpty(dischargeProposedTime))
                    {
                        date = string.Concat(date, " om ", dischargeProposedTime, " uur");
                    }

                    var dischargeHomecareBeginTime = flowfieldvalues.GetValueOrDefault(FlowFieldConstants.Name_DischargeHomecareBeginTime, "");
                    var dischargeHomecareEndTime = flowfieldvalues.GetValueOrDefault(FlowFieldConstants.Name_DischargeHomecareEndTime, "");
                    if (!string.IsNullOrWhiteSpace(dischargeHomecareBeginTime) || !string.IsNullOrWhiteSpace(dischargeHomecareEndTime))
                    {
                        date = string.Concat(date, " voor het eerste zorgmoment tussen ", dischargeHomecareBeginTime, " en ", dischargeHomecareEndTime);
                    }
                }

                var vvthasaccepteddate = flowfieldvalues.GetValueOrDefault(FlowFieldConstants.Name_PatientAcceptedAtDateTimeYN, "").ToLogical();
                if(vvthasaccepteddate == false)
                {
                    answer = "Ja, echter wel met voorstel voor een andere ingangsdatum zorg";
                }
            }
            else if (patientaccepted.Equals("nee", StringComparison.InvariantCultureIgnoreCase))
            {
                answer = "Nee";
                comments = flowfieldvalues.GetValueOrDefault(FlowFieldConstants.Name_DischargeReason, "");
            }

            string message = $"Patiënt kan overgenomen worden/in zorg genomen worden: {answer}";
            if (!string.IsNullOrWhiteSpace(date))
            {
                message += $"{Environment.NewLine}{date}";
            }
            if (!string.IsNullOrWhiteSpace(comments))
            {
                message += $"{Environment.NewLine}{comments}";
            }

            TransferMemoViewBL.Insert(uow, flowInstance.TransferID, message, TransferMemoTypeID.Transferpunt);
            SignaleringBL.HandleSignalByFlowDefinitionFormTypeAction(uow, flowInstance, (int)FlowFormType.CommunicationJournal, "SaveTransferMemo", pointUserInfo, message);
        }

        public static bool MessageToGPByDefinitive(IUnitOfWork uow, FlowInstance flowinstance)
        {
            var organization = GetSendingOrganization(uow, flowinstance.FlowInstanceID);

            if ((bool)organization?.MessageToGPByDefinitive)
            {
                return MedVRIBL.CheckMedvri(uow, flowinstance.TransferID);
            }

            return false;
        }

        public static bool MessageToGPByClose(IUnitOfWork uow, FlowInstance flowinstance)
        {
            var organization = GetSendingOrganization(uow, flowinstance.FlowInstanceID);

            if (organization?.MessageToGPByClose ?? false)
            {
                return MedVRIBL.CheckMedvri(uow, flowinstance.TransferID);
            }

            return false;
        }

        public static bool IsValidNedapOrganization(Organization organization)
        {
            return (organization != null && organization.NedapCryptoCertificateID.ConvertTo<int>() > 0 && !String.IsNullOrEmpty(organization.NedapMedewerkernummer));
        }

        public static bool IsValidNedapOrganization(IUnitOfWork uow, Organization organization, AttachmentTypeID attachmenttypeid)
        {
            if (IsValidNedapOrganization(organization))
            {
                if (SendAttachmentCacheBL.HasAttachmentTypeID(uow, organization.OrganizationID, SendDestinationType.Nedap, attachmenttypeid))
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsValidNedapOrganization(IUnitOfWork uow, Organization organization, FlowFormType flowformtype)
        {
            if (IsValidNedapOrganization(organization))
            {
                if (SendFormTypeCacheBL.HasFlowFormType(uow, organization.OrganizationID, SendDestinationType.Nedap, flowformtype))
                {
                    return true;
                }
            }
            return false;
        }
    }
}