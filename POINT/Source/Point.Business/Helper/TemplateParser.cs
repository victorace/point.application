﻿using System;
using System.Collections.Generic;
using System.Data;

namespace Point.Business.Helper
{
    public class TemplateParser
    {
        private class ConditionPart
        {
            private string _Part = "";

            public string Part
            {
                get
                {
                    return _Part;
                }
            }

            public bool IsOpenTag
            {
                get
                {
                    if (_Part != "")
                    {
                        if ((_Part[0] == '{') && (_Part[1] != '/') && (_Part[_Part.Length - 1] == '}'))
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
            }

            public bool IsCloseTag
            {
                get
                {
                    if (_Part != "")
                    {
                        if ((_Part[0] == '{') && (_Part[1] == '/') && (_Part[_Part.Length - 1] == '}'))
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
            }

            public bool ConditionResult
            {
                get
                {
                    if (IsOpenTag)
                    {
                        string condition = _Part.ToLower().Replace("{condition:", "").Replace("}", "");
                        if (condition.Contains("||"))
                        {
                            string[] conditionParts = condition.Split(new string[] { "||" }, StringSplitOptions.RemoveEmptyEntries);
                            foreach (string cp in conditionParts)
                            {
                                if (DetermineConditionResult(cp.Trim()))
                                {
                                    return true;
                                }
                            }
                            // Neither parts of the condition where true.
                            return false;
                        }
                        else if (condition.Contains("&&"))
                        {
                            string[] conditionParts = condition.Split(new string[] { "&&" }, StringSplitOptions.RemoveEmptyEntries);
                            foreach (string cp in conditionParts)
                            {
                                if (!DetermineConditionResult(cp.Trim()))
                                {
                                    return false;
                                }
                            }
                            // all parts true
                            return true;
                        }
                        else
                        {
                            return DetermineConditionResult(condition.Trim());
                        }
                    }
                    else
                    {
                        return true;
                    }
                }
            }

            private bool DetermineConditionResult(string condition)
            {
                //Condition always consists from two parts: FieldValue = ConditionToCheck
                //We just ignore all '=' characters in FieldValue
                //and break up the Condition on two parts by last '=' sign in the string
                int index = condition.LastIndexOf('=');
                if (index == -1) return false;
                string[] conditionComponents = new string[2];
                conditionComponents[0] = condition.Substring(0, index);
                conditionComponents[1] = condition.Substring(index, condition.Length - index).Remove(0, 1);

                if (conditionComponents[1].Equals("EMPTY", StringComparison.InvariantCultureIgnoreCase))
                {
                    if (conditionComponents[0] == "")
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else if (conditionComponents[1].Equals("!EMPTY", StringComparison.InvariantCultureIgnoreCase))
                {
                    if (conditionComponents[0] == "")
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                else if (conditionComponents[1].StartsWith("!"))
                {
                    string clearedValue = conditionComponents[1].Substring(1);
                    if (!conditionComponents[0].Equals(clearedValue, StringComparison.InvariantCultureIgnoreCase))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else if (conditionComponents[0].Equals(conditionComponents[1], StringComparison.InvariantCultureIgnoreCase))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            public ConditionPart(string part)
            {
                _Part = part;
            }
        }

        public string NewLine { get; private set; }

        private string templatetext;
        private string parsedtemplatetext;
        private DataSet dataSet;

        public TemplateParser(string templatetext, DataSet dataset) : this(templatetext, dataset, Environment.NewLine)
        {
        }

        public TemplateParser(string templatetext, DataSet dataset, string newline)
        {
            this.templatetext = templatetext;
            this.dataSet = dataset;

            this.NewLine = newline;
        }

        public string Parse()
        {
            bool inDataTag = false;
            string dataTag = "";
            List<string> dataTags = new List<string>();

            parsedtemplatetext = templatetext;

            for (int i = 0; i < parsedtemplatetext.Length; i++)
            {
                if (parsedtemplatetext[i] == '[')
                {
                    inDataTag = true;
                }
                else if (parsedtemplatetext[i] == ']')
                {
                    dataTags.Add(dataTag);
                    dataTag = "";
                    inDataTag = false;
                }
                else
                {
                    if (inDataTag)
                    {
                        dataTag += parsedtemplatetext[i];
                    }
                }
            }

            foreach (string iDataTag in dataTags)
            {
                string value = "";
                string[] dataTagComponents = iDataTag.Split('.');

                // Table.Column
                if (dataTagComponents.Length == 2)
                {
                    // Dataset table field
                    if (dataSet.Tables.Contains(dataTagComponents[0]) && dataSet.Tables[dataTagComponents[0]].Columns.Contains(dataTagComponents[1]))
                    {
                        if (dataSet.Tables[dataTagComponents[0]].Rows.Count > 0)
                        {
                            if (dataSet.Tables[dataTagComponents[0]].Rows[0][dataTagComponents[1]] != null)
                            {
                                value = dataSet.Tables[dataTagComponents[0]].Rows[0][dataTagComponents[1]].ToString();
                            }
                        }
                    }
                }

                parsedtemplatetext = parsedtemplatetext.Replace(String.Format("[{0}]", iDataTag), value);
            }

            processConditions();

            return parsedtemplatetext.Replace("\r\n", NewLine);
        }

        private void processConditions()
        {
            List<ConditionPart> conditionParts = new List<ConditionPart>();

            string component = "";
            for (int i = 0; i < parsedtemplatetext.Length; i++)
            {
                if (parsedtemplatetext[i] == '{')
                {
                    if (component != "")
                    {
                        ConditionPart part = new ConditionPart(component);
                        conditionParts.Add(part);

                        component = "";
                    }

                    component += parsedtemplatetext[i];
                }
                else if (parsedtemplatetext[i] == '}')
                {
                    component += parsedtemplatetext[i];
                    ConditionPart conditionpart = new ConditionPart(component);
                    conditionParts.Add(conditionpart);

                    component = "";
                }
                else
                {
                    component += parsedtemplatetext[i];
                }
            }

            ConditionPart newPart = new ConditionPart(component);
            conditionParts.Add(newPart);

            List<bool> conditionList = new List<bool>();

            string result = "";
            foreach (ConditionPart iConditionPart in conditionParts)
            {
                if (iConditionPart.IsOpenTag)
                {
                    conditionList.Add(iConditionPart.ConditionResult);
                }
                else if (iConditionPart.IsCloseTag)
                {
                    conditionList.RemoveAt(conditionList.Count - 1);
                }
                else
                {
                    bool show = true;
                    for (int i = conditionList.Count; i > 0; i--)
                    {
                        if (conditionList[i - 1] == false)
                        {
                            show = false;
                        }
                    }

                    if (show)
                    {
                        result += iConditionPart.Part;
                    }
                }
            }

            parsedtemplatetext = result;
        }
    }
}
