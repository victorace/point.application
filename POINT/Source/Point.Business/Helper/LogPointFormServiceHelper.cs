﻿using Point.Business.Logic;
using Point.Database.Context;
using Point.Database.Repository;
using Point.Infrastructure;
using Point.Infrastructure.Extensions;
using System;
using System.Web;

namespace Point.Business.Helper
{
    public static class LogPointFormServiceHelper
    {
        public static void Log(int organizationid, string hash, HttpRequest httprequest)
        {
            using (var uow = new UnitOfWork<PointContext>())
            {
                try
                {
                    var logpointformservice = OrganizationSettingBL.GetValue<bool>(uow, organizationid, OrganizationSettingBL.LogPointFormService);
                    if (logpointformservice)
                    {
                        var uri = httprequest.Url.AbsoluteUri;
                        var xmlrequest = httprequest.GetXmlRequest();
                        LogPointFormServiceBL.Insert(uow, organizationid, hash, uri.ToString(), xmlrequest.OuterXml);
                    }
                }
                catch (Exception exception)
                {
                    ExceptionHelper.SendMessageException(exception, uri: httprequest?.Url);
                }
            }
        }
    }
}
