﻿using System;
using System.Net;
using System.Net.Http;
using System.Web;

namespace Point.Business.Helper
{
    public class IPAddressHelper
    {
        private static bool IsIPAddressInSubnetRange(string ipAddress, string cidr)
        {
            string[] parts = cidr.Split('/');
            int baseAddress = BitConverter.ToInt32(IPAddress.Parse(parts[0]).GetAddressBytes(), 0);
            int address = BitConverter.ToInt32(IPAddress.Parse(ipAddress).GetAddressBytes(), 0);
            int mask = IPAddress.HostToNetworkOrder(-1 << (32 - int.Parse(parts[1])));

            return ((baseAddress & mask) == (address & mask));
        }

        private static string[] GetIpAddresses(string ipAddresses)
        {
            return (ipAddresses ?? "").Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
        }
        
        private static string GetClientIp(HttpRequestMessage request)
        {
            const string httpContextKey = "MS_HttpContext";
            if (request.Properties.ContainsKey(httpContextKey))
            {
                return ((HttpContextWrapper)request.Properties[httpContextKey]).Request.UserHostAddress;
            }

            const string remoteEndpointMessageKey = "System.ServiceModel.Channels.RemoteEndpointMessageProperty";
            if (!request.Properties.ContainsKey(remoteEndpointMessageKey))
            {
                return null;
            }
            dynamic remoteEndpoint = request.Properties[remoteEndpointMessageKey];
            return remoteEndpoint != null ? (string)remoteEndpoint.Address : null;
        }

        public static bool IsIPAddressValid(string clientIPAddress, string allowedIPAddresses)
        {
            return IsIPAddressValid(clientIPAddress, GetIpAddresses(allowedIPAddresses));
        }
        public static bool IsIPAddressValid(string clientIPAddress, string[] allowedIPAddresses)
        {
            if (string.IsNullOrEmpty(clientIPAddress) || allowedIPAddresses.Length == 0) return false;

            clientIPAddress = clientIPAddress.Trim();

            foreach (var iPAddressOrRange in allowedIPAddresses)
            {
                var iPAddressOrRangeCleaned = iPAddressOrRange.Trim();
                if (iPAddressOrRangeCleaned.Contains("/"))
                {
                    // It's a CIDR format
                    if (IsIPAddressInSubnetRange(clientIPAddress, iPAddressOrRangeCleaned))
                    {
                        return true;
                    }
                }
                
                if (iPAddressOrRangeCleaned == "*" || clientIPAddress.Equals(iPAddressOrRangeCleaned.Replace("*", ""), StringComparison.InvariantCultureIgnoreCase) || // 127.0.0.1
                    (iPAddressOrRange.EndsWith("*") && clientIPAddress.StartsWith(iPAddressOrRangeCleaned)) || // 127.0.*
                    (iPAddressOrRange.StartsWith("*") && clientIPAddress.EndsWith(iPAddressOrRangeCleaned))) // *.0.1
                {
                    return true;
                }
            }

            return false;
        }

        public static bool IsIPAddressValid(HttpRequestMessage request, string[] allowedIPAddresses)
        {
            return IsIPAddressValid(GetClientIp(request), allowedIPAddresses);
        }

    }
}
