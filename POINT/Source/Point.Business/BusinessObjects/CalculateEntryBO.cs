using Point.Database.Extensions;
using Point.Database.Models;
using Point.Models.LDM.EOverdracht30.Enums;
using System;

namespace Point.Business.BusinessObjects
{
    public class CalculateEntryBO
    {
        public class ClientModel
        {
            public bool ClientIsAnonymous { get; set; }
            public DateTime? BirthDate { get; set; }
            public Geslacht Gender { get; set; }
            public string CivilServiceNumber { get; set; }
            public string PatientNumber { get; set; }
            public string VisitNumber { get; set; }
            public string PostalCode { get; set; }


            public int? HealthInsuranceCompanyID { get; set; }

            public string Initials { get; set; }
            public string FirstName { get; set; }
            public string MiddleName { get; set; }
            public string LastName { get; set; }

            public string FullName()
            {
                return new NameComponents()
                {
                    Initials = Initials,
                    FirstName = FirstName,
                    MiddleName = MiddleName,
                    LastName = LastName
                }.FullName();
            }
        }

        public int? CopyOfTransferID { get; set; }
        public ClientModel Client { get; set; }
        public DateTime? LastTransferMemoDate { get; set; }
        public DateTime? LastAdditionalTPMemoDate { get; set; }
        public DateTime? LastTransferAttachmentDate { get; set; }
        public FormSetVersion LastGrzFormSetVersion { get; set; }
        public TransferTask LastTransferTask { get; set; }
    }
}
