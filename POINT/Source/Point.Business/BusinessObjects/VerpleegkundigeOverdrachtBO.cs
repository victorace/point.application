﻿using Point.Business.Helper;
using Point.Business.Logic;
using Point.Database.CacheModels;
using Point.Database.Extensions;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Extensions;
using Point.Infrastructure.Helpers;
using Point.Models.Enums;
using Point.ServiceBus.Models;
using System;
using System.Linq;
using Point.Infrastructure.Constants;

namespace Point.Business.BusinessObjects
{
    public class VerpleegkundigeOverdrachtBO
    {
        private const string PDF = "Pdf"; 
        private readonly IUnitOfWork uow;
        private readonly FlowInstance flowinstance;
        private readonly PointUserInfo pointuserinfo;
        private readonly PhaseDefinitionCache phasedefinition;
        private PhaseInstance phaseinstance;
        private FormSetVersion formsetversion;

        public VerpleegkundigeOverdrachtBO(IUnitOfWork uow, FlowInstance flowinstance, PointUserInfo pointuserinfo)
        {
            this.uow = uow;
            this.flowinstance = flowinstance ?? throw new ArgumentNullException(nameof(flowinstance));
            this.pointuserinfo = pointuserinfo ?? throw new ArgumentNullException(nameof(pointuserinfo));
            phasedefinition = PhaseDefinitionCacheBL.GetByFlowDefinitionAndFormType(uow, flowinstance.FlowDefinitionID, (int)FlowFormType.VerpleegkundigeOverdrachtFlow);
            if (phasedefinition == null)
            {
                return;
            }

            phaseinstance = PhaseInstanceBL.GetByFlowInstanceIDAndPhaseDefinition(uow, flowinstance.FlowInstanceID, phasedefinition.PhaseDefinitionID);
            if (phaseinstance == null)
            {
                return;
            }

            formsetversion = FormSetVersionBL.GetByPhaseInstanceID(uow, phaseinstance.PhaseInstanceID).OrderByDescending(pi => pi.UpdateDate).FirstOrDefault();
            HasAttachment = GetAttachmentID(uow, this.flowinstance.FlowInstanceID).HasValue;
        }

        public static int? GetAttachmentID(IUnitOfWork uow, int flowinstanceid)
        {
            var flowFieldValues = FlowFieldValueBL.GetByFlowInstanceIDAndFieldIDs(uow, flowinstanceid, new int[] { FlowFieldConstants.ID_VOFormulierType, FlowFieldConstants.ID_VOFormulierTransferAttachmentID });
            if (flowFieldValues == null || !flowFieldValues.Any())
            {
                return null;
            }

            var voformuliertype = flowFieldValues.LastOrDefault(ffv => ffv.FlowFieldID == FlowFieldConstants.ID_VOFormulierType);
            if (voformuliertype != null && voformuliertype.Value.Equals(PDF, StringComparison.InvariantCultureIgnoreCase))
            {
                var voformuliertransferattachmentid = flowFieldValues.LastOrDefault(ffv => ffv.FlowFieldID ==  FlowFieldConstants.ID_VOFormulierTransferAttachmentID);
                if (int.TryParse(voformuliertransferattachmentid?.Value, out int attachmentid))
                {
                    return attachmentid;
                }
            }

            return null;
        }

        public bool HasAttachment { get; private set; }

        public bool IsDone()
        {
            return phaseinstance.IsDone();
        }

        public bool IsDefinitive()
        {
            var flowdefinitionid = flowinstance.FlowDefinitionID;

            if (flowdefinitionid == FlowDefinitionID.VVT_ZH || flowdefinitionid == FlowDefinitionID.ZH_VVT || flowdefinitionid == FlowDefinitionID.RzTP || flowdefinitionid == FlowDefinitionID.VVT_VVT)
            {
                return phaseinstance.IsDone();
            }

            if (flowdefinitionid == FlowDefinitionID.ZH_ZH)
            {
                return FlowInstanceBL.GetLastPhaseNumByFlowInstanceID(uow, flowinstance.FlowInstanceID) >= 3;
            }

            if (flowdefinitionid == FlowDefinitionID.CVA)
            {
                if (FlowInstanceBL.GetOntslagbestemming(uow, flowinstance) == FlowFieldConstants.Value_Ontslagbestemming_Revalidatiecentrum ||
                    FlowInstanceBL.IsOntslagbestemmingStandardRoute(uow, flowinstance))
                {
                    return phaseinstance.IsDone();
                }
            }

            return false;
        }

        public void Send(bool isresend)
        {
            Send(isresend, CommunicationLogType.None);
        }

        public void Send(bool isresend, CommunicationLogType mailtype)
        {
            if (formsetversion == null || !FlowInstanceBL.CanSendVO(uow, flowinstance) || !IsDefinitive())
            {
                return;
            }

            if (phaseinstance == null)
            {
                return;
            }

            var receivingorganization = OrganizationHelper.GetReceivingOrganization(uow, flowinstance.FlowInstanceID);
            var receivingdepartment = OrganizationHelper.GetRecievingDepartment(uow, flowinstance.FlowInstanceID);
            if (receivingorganization == null || receivingdepartment == null)
            {
                return;
            }

            string url = $"{ConfigHelper.GetAppSettingByName<string>("WebServer").TrimEnd('/')}/FlowMain/DashBoard?TransferID={flowinstance.TransferID}";
            
            var timestamp = DateTime.Now;
            ServiceBusBL.SendMessage(uow, MessageQueues.PhaseQueue, "FormSetVersionNotification",
                new FormSetVersionNotification
                {
                    TimeStamp = timestamp,
                    UniqueID = timestamp.Ticks,
                    TransferID = flowinstance.TransferID,
                    FormSetVersionID = formsetversion.FormSetVersionID,
                    RecievingDepartmentID = receivingdepartment.DepartmentID,
                    RecievingOrganizationID = receivingorganization.OrganizationID,
                    SendingEmployeeID = phaseinstance.RealizedByEmployeeID ?? pointuserinfo.Employee.EmployeeID,
                    IsAltered = isresend,
                    Url = url,
                    MailType = mailtype,
                    IsVO = true
                }
            );
        }

        public PhaseInstance HandleNextPhase(LogEntry logEntry)
        {
            if (!FlowInstanceBL.CanSendVO(uow, flowinstance) || !IsDefinitive())
            {
                return null;
            }

            // When VVT replied 'JA' there should be a phase 8 "Versturen EOverdracht". Set this phase to the next automatically
            // If the VVT hasn't replied or replied 'NO' this phase will not exist and this method will be called whenever they do
            var versturenvophasedef = PhaseDefinitionCacheBL.GetByFlowDefinitionAndFormType(uow, flowinstance.FlowDefinitionID, (int)FlowFormType.VersturenVO);
            if (versturenvophasedef != null)
            {
                var nextphasedefinitions = PhaseDefinitionBL.GetNextPhaseDefinitions(uow, versturenvophasedef.PhaseDefinitionID);
                var nextphasedefinition = nextphasedefinitions.FirstOrDefault();
                if (nextphasedefinition == null)
                {
                    return null;
                }

                var versturenphaseinstance = PhaseInstanceBL.GetByFlowInstanceIDAndPhaseDefinition(uow, flowinstance.FlowInstanceID, versturenvophasedef.PhaseDefinitionID);
                if (versturenphaseinstance == null)
                {
                    return null;
                }

                var existingnextphaseinstance = PhaseInstanceBL.GetByFlowInstanceIDAndPhaseDefinition(uow, flowinstance.FlowInstanceID, nextphasedefinition.PhaseDefinitionID);
                if (existingnextphaseinstance == null || existingnextphaseinstance.Status == (int)Status.Done)
                {
                    return PhaseInstanceBL.HandleNextPhaseWithoutGeneralAction(uow, flowinstance.FlowInstanceID, versturenphaseinstance.PhaseInstanceID, nextphasedefinition.PhaseDefinitionID, logEntry);
                }
            }

            return null;
        }

        public PhaseInstance HandleNextPhase(AttachmentReceivedTemp attachmentreceivedtemp, LogEntry logEntry)
        {
            if (phasedefinition.Phase != -1)
            {
                // We are dealing with a FlowDefinition that is using the VO as a process form, so we can't auto handle the next phase
                return null;
            }

            if (attachmentreceivedtemp == null || attachmentreceivedtemp.AttachmentTypeID != AttachmentTypeID.ExternVODocument)
            {
                return null;
            }

            bool isresend = false;
            if (phaseinstance == null)
            {
                phaseinstance = PhaseInstanceBL.Create(uow, flowinstance.FlowInstanceID, phasedefinition.PhaseDefinitionID, logEntry, Status.Done);
            }
            else
            {
                if (formsetversion != null)
                {
                    TransferAttachmentBL.DeleteByFormSetVersionID(uow, formsetversion.FormSetVersionID, logEntry);
                    FormSetVersionBL.DeleteByIDAndTransferID(uow, formsetversion.FormSetVersionID, flowinstance.TransferID, logEntry);
                }

                phaseinstance.Status = (int)Status.Done;
                phaseinstance.RealizedDate = DateTime.Now;
                phaseinstance.RealizedByEmployeeID = logEntry.EmployeeID;

                LoggingBL.FillLogging(uow, phaseinstance, logEntry);

                isresend = true;
            }

            formsetversion = FormSetVersionBL.CreateFormSetVersion(uow, flowinstance.TransferID, (int)FlowFormType.VerpleegkundigeOverdrachtFlow,
                phaseinstance.PhaseInstanceID, pointuserinfo, logEntry, null, null, null);

            var transferattachment = TransferAttachmentBL.InsertAndSave(uow, flowinstance.TransferID, formsetversion.FormSetVersionID, attachmentreceivedtemp, logEntry);
            if (transferattachment == null)
            {
                // just throw an exception - no way to recover from this as we may have 
                // deleted the previous FormSetVersion and TransferAttachment records
                throw new Exception($"Unable to attach ExternVODocument to Transfer({flowinstance.TransferID}).");
            }

            HasAttachment = true;

            FlowFieldValueBL.UpdateOrInsert(uow, flowinstance.FlowInstanceID, formsetversion.FormSetVersionID, 
                FlowFieldConstants.ID_VOFormulierType, PDF, Source.WEBSERVICE, logEntry);
            FlowFieldValueBL.UpdateOrInsert(uow, flowinstance.FlowInstanceID, formsetversion.FormSetVersionID, 
                FlowFieldConstants.ID_VOFormulierTransferAttachmentID, transferattachment.AttachmentID.ToString(), Source.WEBSERVICE, logEntry);
            FlowFieldValueBL.UpdateOrInsert(uow, flowinstance.FlowInstanceID, formsetversion.FormSetVersionID, 
                FlowFieldConstants.ID_VOFormulierTransferFileName, transferattachment.GenericFile.FileName, Source.WEBSERVICE, logEntry);
            FlowFieldValueBL.UpdateOrInsert(uow, flowinstance.FlowInstanceID, formsetversion.FormSetVersionID, 
                FlowFieldConstants.ID_VOFormulierUploadDate, transferattachment.TimeStamp.ToPointDateTimeDatabase(), Source.WEBSERVICE, logEntry);
            FlowFieldValueBL.UpdateOrInsert(uow, flowinstance.FlowInstanceID, formsetversion.FormSetVersionID, 
                FlowFieldConstants.ID_VOFormulierEmployeeName, pointuserinfo.Employee.FullName(), Source.WEBSERVICE, logEntry);

            // create next phaseinstance (if vvt has accepted)
            PhaseInstance nextphaseinstance = HandleNextPhase(logEntry);

            // notify with email via the service bus
            Send(isresend);

            uow.Save();

            return nextphaseinstance;
        }
    }
}