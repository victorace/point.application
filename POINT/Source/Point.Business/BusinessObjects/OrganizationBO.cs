﻿using Point.Business.Logic;
using Point.Database.Context;
using Point.Database.Repository;
using Point.Infrastructure.Extensions;
using System;
using System.Security.Cryptography;
using System.Text;

namespace Point.Business.BusinessObjects
{
    public class OrganizationBO
    {
        protected OrganizationBO() { }

        public static bool AuthenticateHash(Guid guid, int organizationid, string senderusername, DateTime senddate, string patientnumber, string hash)
        {
            string hashedString = string.Empty;
            using (var uow = new UnitOfWork<PointContext>())
            {
                string hashprefix = OrganizationBL.GetHashPrefix(uow, organizationid);
                if (String.IsNullOrEmpty(hashprefix))
                {
                    return false;
                }

                var hashType = OrganizationSettingBL.GetValue<string>(uow, organizationid, OrganizationSettingBL.HashTypeAPI);                  
                var hashAlgorithm = GetAlgorithmFromString(hashType);

                //hash: <PREFIX>_<GUID>_<OrganisationID>_<SenderUserName>_<SendDate>_<PatientNumber>
                byte[] hashBytes = Encoding.UTF8.GetBytes($"{hashprefix}_{guid}_{organizationid}_{senderusername}_{senddate.ToString("yyyyMMdd")}_{patientnumber}");

                byte[] computedHash = hashAlgorithm.ComputeHash(hashBytes);
                
                byte b;
                foreach (byte b_loopVariable in computedHash)
                {
                    b = b_loopVariable;
                    hashedString += StringExtensions.Right("0" + b.ToString("X").ToLower(), 2);
                }
            }

            return (hashedString.ToLower() == hash);
        }

        private static HashAlgorithm GetAlgorithmFromString(string algorithm)
        {
            HashAlgorithm hashAlgorithm = new SHA256Managed();
            if (String.IsNullOrEmpty(algorithm) || algorithm.Equals("SHA1", StringComparison.InvariantCultureIgnoreCase))
            {
                hashAlgorithm = new SHA1Managed();
            }
            return hashAlgorithm;
        }
    }
}