﻿namespace Point.Business.BusinessObjects
{
    public class FieldBO
    {
        public string FieldValue { get; set; }
        public string FieldName { get; set; }

        public FieldBO() { }

        public FieldBO(string name, string value)
        {
            FieldName = name;
            FieldValue = value;
        }
    }
}