﻿using Point.Models.Enums;

namespace Point.Business.BusinessObjects
{
    public class AttachmentReceivedTempBO
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public AttachmentTypeID AttachmentTypeID { get; set; }
        public string Base64EncodedData { get; set; }
    }
}