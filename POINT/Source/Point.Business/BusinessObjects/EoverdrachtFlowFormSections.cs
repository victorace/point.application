﻿
namespace Point.Business.BusinessObjects
{
    public class EoverdrachtFlowFormSections
    {
        public string Section { get; set; }
        public string Subsection { get; set; }
        public string FormGroup { get; set; }
        public int FlowFieldId { get; set; }

        public override string ToString()
        {
            return $"{Section}#{Subsection}#{FormGroup}#{FlowFieldId}";
        }
    }
}
