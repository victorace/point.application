﻿using Point.Business.Logic;
using Point.Database.Context;
using Point.Database.Extensions;
using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Extensions;
using System;
using System.Linq;

namespace Point.Business.BusinessObjects
{
    public class DossierBO
    {
        private IUnitOfWork uow = new UnitOfWork<PointSearchContext>();
        private string bsn { get; set; }
        private string patientNumber { get; set; }
        private PatientReceivedTemp patientReceivedTemp { get; set; }
        private int employeeid { get; set; }
        private Department department { get; set; }
        private int? lastFormTypeID { get; set; }
        private FlowInstance flowinstance { get; set; }

        public string Error { get; private set; }

        public class DossierParameters
        {
            public int TransferID { get; set; }
            public int FlowInstanceID { get; set; }
            public int LastFormTypeID { get; set; }
        }

        public DossierBO(PointUserInfo pointuserinfo, string bsn, int departmentID, string patientNumber)
        {
            this.bsn = bsn;
            this.patientNumber = patientNumber;
            employeeid = pointuserinfo.Employee.EmployeeID;

            department = DepartmentBL.GetByDepartmentID(uow, departmentID);
        }

        public DossierBO(int employeeID, string bsn, int departmentID, string patientNumber)
        {
            this.bsn = bsn;
            this.patientNumber = patientNumber;
            employeeid = employeeID;

            department = DepartmentBL.GetByDepartmentID(uow, departmentID);
        }

        public DossierParameters CreateDossierAPI(FlowDefinitionID flowDefinitionID, LogEntry logEntry)
        {
            PatientReceivedTemp patientReceivedTemp = null;

            if (!string.IsNullOrEmpty(bsn) && !string.IsNullOrEmpty(patientNumber))
            {
                patientReceivedTemp = PatientReceivedTempBL.GetByBSNAndPatientNumber(uow, bsn, patientNumber, department.Location.OrganizationID);
            }
            else if (!string.IsNullOrEmpty(bsn))
            {
                patientReceivedTemp = PatientReceivedTempBL.GetByBSN(uow, bsn, department.Location.OrganizationID);
            }
            else if (!string.IsNullOrEmpty(patientNumber))
            {
                patientReceivedTemp = PatientReceivedTempBL.Get(uow, department.Location.OrganizationID, patientNumber);
            }

            if (patientReceivedTemp == null)
            {
                throw new Exception(string.Format("Geen patientreceivedtemp data met BSN {0}, Patientnummer {1}", bsn, patientNumber));
            }

            var client = ClientBL.FromModel(uow, patientReceivedTemp);

            return CreateDossier(flowDefinitionID, client, logEntry);
        }

        public DossierParameters CreateDossier(FlowDefinitionID flowDefinitionID, Client client, LogEntry logentry)
        {
            if (department == null)
            {
                throw new Exception(string.Format("Geen afdeling opgegeven"));
            }

            if (flowDefinitionID == FlowDefinitionID.HA_VVT)
            {
                if(string.IsNullOrEmpty(department?.AddressGPForZorgmail))
                {
                    throw new Exception(string.Format("{0} heeft geen Zorgmail adres", department?.FullName()));
                }

                client.AddressGPForZorgmail = department?.AddressGPForZorgmail;
                client.GeneralPractitionerName = department?.FullName();
                client.GeneralPractitionerPhoneNumber = department?.PhoneNumber;
            }

            try
            {
                ClientBL.Save(uow, client);
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return new DossierParameters() { FlowInstanceID = -1, TransferID = -1 };
            }

            if (client?.ClientID == null)
            {
                throw new Exception(string.Format("ClientID kon niet worden gegenereerd (BSN {0})", bsn));
            }

            var transfer = TransferBL.Generate(uow, client.ClientID, logentry);
            if (transfer?.TransferID == null)
            {
                throw new Exception(string.Format("TransferID kon niet worden gegenereerd (BSN {0})", bsn));
            }

            flowinstance = FlowInstanceBL.NewFlowInstance(uow, flowDefinitionID, transfer.TransferID, logentry, department.Location.OrganizationID, department.DepartmentID);
            if (flowinstance?.FlowInstanceID == null)
            {
                throw new Exception(string.Format("FlowInstanceID kon niet worden gegenereerd (BSN {0})", bsn));
            }

            FlowInstanceSearchValuesBL.Create(uow, flowinstance.FlowInstanceID, transfer.TransferID, client.FullName(), department.DepartmentID);

            var phasedefinitionbeginflow = PhaseDefinitionCacheBL.GetPhaseDefinitionBeginFlow(uow, flowDefinitionID);
            if (phasedefinitionbeginflow != null)
            {
                var firstphaseinstance = PhaseInstanceBL.Create(uow, flowinstance.FlowInstanceID, phasedefinitionbeginflow.PhaseDefinitionID, logentry, Status.Done);
                var nextPhaseDefintions = PhaseDefinitionBL.GetNextPhaseDefinitions(uow, phasedefinitionbeginflow.PhaseDefinitionID).ToList();
                foreach (var nextPhaseDefintion in nextPhaseDefintions)
                {
                    PhaseInstanceBL.HandleNextPhase(uow, flowinstance.FlowInstanceID, firstphaseinstance.PhaseInstanceID, nextPhaseDefintion.PhaseDefinitionID, employeeid, logentry);

                    lastFormTypeID = nextPhaseDefintion.FormTypeID;
                }
                return new DossierParameters() { FlowInstanceID = flowinstance.FlowInstanceID, TransferID = flowinstance.TransferID, LastFormTypeID = lastFormTypeID.Value };
            }
            else
            {
                throw new Exception(string.Format("Eerste fase kon niet worden gestart (BSN {0})", bsn));
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~DossierBO()
        {
            Dispose(false);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (uow != null)
                {
                    uow.SafeDispose();
                    uow = null;
                }
            }
        }
    }
}
