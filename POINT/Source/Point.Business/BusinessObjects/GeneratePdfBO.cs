﻿using System.Collections.Generic;
using System.Linq;

namespace Point.Business.BusinessObjects
{
    public class GeneratePdfBO
    {
        public class GeneratePdfAttachment
        {
            public GeneratePdfAttachment(byte[] fileData, string fileName)
            {
                FileData = fileData;
                FileName = fileName;
            }
            public byte[] FileData { get; set; }
            public string FileName { get; set; }

        }

        public class GeneratePdfResult
        {
            private readonly IList<string> _errorMessages;
            public GeneratePdfResult()
            {
                _errorMessages = new List<string>();
            }
            public bool AbortPrint { get; set; }
            public int AttachmentsTotalProcessedSuccess { get; set; }
            public string ErrorMessages => _errorMessages.Any() ? string.Join(", ", _errorMessages.ToArray()) : "";
            public byte[] FileData { get; set; }
            public bool HasErrors => _errorMessages.Any();

            public void AddErrorMessage(string errorMessage)
            {
                _errorMessages.Add(errorMessage);
            }
        }
    }
}
