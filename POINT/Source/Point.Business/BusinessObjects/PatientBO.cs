﻿using System;

namespace Point.Business.BusinessObjects
{
    public class PatientBO
    {
        public string CivilServiceNumber { get; set; }
        public string Gender { get; set; }
        public string Salutation { get; set; }
        public string Initials { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string MaidenName { get; set; }
        public DateTime? BirthDate { get; set; }
        public string StreetName { get; set; }
        public string Number { get; set; }

        private string _postcalcode;
        public string PostalCode
        {
            get
            {
                return _postcalcode;
            }
            set
            {
                if (!String.IsNullOrWhiteSpace(value))
                {
                    int lengthpostalcode = 6;
                    string postalcode = value.Replace(" ", string.Empty);
                    _postcalcode = postalcode.Substring(0, value.Length <= lengthpostalcode ? value.Length : lengthpostalcode);
                }
                else
                {
                    _postcalcode = string.Empty;
                }
            }
        }

        public string City { get; set; }
        public string PhoneNumberGeneral { get; set; }
        public string PhoneNumberMobile { get; set; }
        public string PhoneNumberWork { get; set; }
        public string ContactPersonName { get; set; }
        public string ContactPersonRelationType { get; set; }
        public DateTime? ContactPersonBirthDate { get; set; }
        public string ContactPersonPhoneNumberGeneral { get; set; }
        public string ContactPersonPhoneNumberMobile { get; set; }
        public string ContactPersonPhoneNumberWork { get; set; }
        public int HealthInsuranceCompanyUZOVICode { get; set; }
        public string InsuranceNumber { get; set; }
        public string GeneralPractitionerName { get; set; }
        public string GeneralPractitionerPhoneNumber { get; set; }
        public string CivilClass { get; set; }
        public string CompositionHousekeeping { get; set; }
        public int ChildrenInHousekeeping { get; set; }
        public string HousingType { get; set; }
        public string AddressGPForZorgmail { get; set; }
        public string VisitNumber { get; set; }
        public string PharmacyName { get; set; }
        public string PharmacyPhoneNumber { get; set; }
    }
}