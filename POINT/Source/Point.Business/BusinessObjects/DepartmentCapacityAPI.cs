﻿using System;
using System.Collections.Generic;

namespace Point.Business.BusinessObjects
{
    public class RegionCapacityUpdateAPI
    {
        public int RegionID { get; set; }
        public string Name { get; set; }
        public bool CapacityBeds { get; set; }
    }

    public class OrganizationCapacityUpdateAPI
    {
        public int OrganizationID { get; set; }
        public string OrganizationName { get; set; }
        public int RegionID { get; set; }
        public string RegionName { get; set; }
        public bool IsActive { get; set; }
        public DateTime? ModifiedOn { get; set; }
    }

    public class LocationCapacityUpdateAPI
    {
        public LocationCapacityUpdateAPI()
        {
            AlternativeRegionIDs = new List<int>();
        }

        public int LocationID { get; set; }
        public string LocationName { get; set; }
        public string LocationStreet { get; set; }
        public string LocationNumber { get; set; }
        public string LocationPostalCode { get; set; }
        public string LocationCity { get; set; }
        public bool IsActive { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public List<int> AlternativeRegionIDs { get; set; }
    }

    public class DepartmentCapacityAPI
    {
        public DepartmentCapacityAPI()
        {
            AlternativeRegionIDs = new List<int>();
        }

        public int DepartmentCapacityID { get; set; }
        public int? AfterCareTypeID { get; set; }
        public string AfterCareTypeName { get; set; }
        public int? AfterCareGroupID { get; set; }
        public int RegionID { get; set; }
        public string RegionName { get; set; }
        public int OrganizationID { get; set; }
        public string OrganizationName { get; set; }
        public int LocationID { get; set; }
        public string LocationName { get; set; }
        public string LocationStreet { get; set; }
        public string LocationNumber { get; set; }
        public string LocationPostalCode { get; set; }
        public string LocationCity { get; set; }
        public string LocationEmailAddress { get; set; }
        public int DepartmentID { get; set; }
        public string DepartmentName { get; set; }
        public string DepartmentPhoneNumber { get; set; }
        public string DepartmentFaxNumber { get; set; }
        public string DepartmentEmailAddress { get; set; }
        public bool IsActive { get; set; }
        public DateTime? AdjustmentCapacityDate { get; set; }
        public int? Capacity1 { get; set; }
        public int? Capacity2 { get; set; }
        public int? Capacity3 { get; set; }
        public int? Capacity4 { get; set; }
        public string Information { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public bool? CapacityFunctionality { get; set; }
        public List<int> AlternativeRegionIDs { get; set; }
    }

    public class DepartmentCapacityUpdateAPI
    {
        public int DigestValue { get; set; }
        public List<DepartmentCapacityAPI> Capacities { get; set; }
    }
}
