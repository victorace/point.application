﻿using Point.Business.Logic;
using Point.Infrastructure.Extensions;
using System;
using System.Globalization;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace Point.Business.BusinessObjects
{
    public class SSOLoginBO
    {
        public string ExternalReference { get; set; }
        public string OrgExternalReference { get; set; }
        public string AutoCreateCode { get; set; }
        public DateTime? Date { get; set; }
        public string DateString { get; set; }
        public string Sign { get; set; }
        public string EmplReference { get; set; }
        public string DepartmentReference { get; set; }
        public string ReturnUrl { get; set; } //Note: only used for reading the parameters on it since almost all customers (still pass this when logging in). Not used for "redirecting" 
        public string PatExternalReference { get; set; }
        public string PatExternalReferenceBSN { get; set; }
        public string ClientIP { get; set; }
        public string Uri { get; set; }
        public bool IsAuth { get; set; }

        public SSOLoginBO FromRequest(HttpRequestBase request)
        {
            ExternalReference = request.Params.GetValueOrDefault<string>("ExternalReference1", "");
            OrgExternalReference = request.Params.GetValueOrDefault<string>("OrgExternalReference1", "");
            if(String.IsNullOrEmpty(OrgExternalReference))
            {
                OrgExternalReference = request.Params.GetValueOrDefault<string>("OrgCareExternalReference1", "");
            }

            ReturnUrl = request.Params.GetValueOrDefault<string>("ReturnUrl", "");
            if (!string.IsNullOrEmpty(ReturnUrl) && ReturnUrl.Contains("!"))
            {
                var returnurlquerystring = HttpUtility.ParseQueryString(ReturnUrl.Substring(Math.Max(0, ReturnUrl.IndexOf("?"))).Replace("!", "&"));
                DateString = returnurlquerystring.GetValueOrDefault<string>("Date", "");
                Sign = returnurlquerystring.GetValueOrDefault<string>("Sign", "");
                EmplReference = returnurlquerystring.GetValueOrDefault<string>("EmplReference1", "");
                DepartmentReference = returnurlquerystring.GetValueOrDefault<string>("DepartmentReference1", "");
                PatExternalReference = returnurlquerystring.GetValueOrDefault<string>("PatExternalReference1", "");
                PatExternalReferenceBSN = returnurlquerystring.GetValueOrDefault<string>("PatexternalReferenceBSN1", "");
                ReturnUrl = HttpContext.Current.Server.UrlDecode(returnurlquerystring.GetValueOrDefault<string>("ReturnUrl", ""));
            }
            else
            {
                DateString = request.Params.GetValueOrDefault<string>("Date", "");
                Sign = request.Params.GetValueOrDefault<string>("Sign", "");
                EmplReference = request.Params.GetValueOrDefault<string>("EmplReference1", "");
                DepartmentReference = request.Params.GetValueOrDefault<string>("DepartmentReference1", "");
                PatExternalReference = request.Params.GetValueOrDefault<string>("PatExternalReference1", "");
                PatExternalReferenceBSN = request.Params.GetValueOrDefault<string>("PatexternalReferenceBSN1", "");
                ReturnUrl = HttpContext.Current.Server.UrlDecode(request.Params.GetValueOrDefault<string>("ReturnUrl", ""));
            }
            DateTime parseddate = default(DateTime);
            if (DateTime.TryParseExact(DateString, "yyyyMMddHHmmss", CultureInfo.InvariantCulture, DateTimeStyles.AssumeLocal, out parseddate))
            {
                Date = parseddate;
            }

            ClientIP = LoginBL.GetClientIP(request);
            Uri = LoginBL.GetSSOUri(request);

            IsAuth = request.Params.GetValueOrDefault<bool>("IsAuth", false);

            return this;
        }


        public string CalculatedSign(string ssoKey, HashAlgorithm hashAlgorithm) 
        {
            byte[] hashbytes = Encoding.UTF8.GetBytes(String.Format("{0}_{1}_{2}_{3}", ssoKey, OrgExternalReference, ExternalReference, DateString));
            byte[] computedhash = hashAlgorithm.ComputeHash(hashbytes);
            string calculatedsign = "";
            foreach(byte b in computedhash)
            {
                calculatedsign += b.ToString("X2");
            }
            return calculatedsign.ToLower();
        }

        public int OrganizationID()
        {
            int organizationid = -1;
            Int32.TryParse(OrgExternalReference, out organizationid);
            return organizationid;
        }
    }
}
