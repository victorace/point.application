﻿using Point.Business.Helper;
using Point.Business.Logic;
using Point.Database.Context;
using Point.Database.Extensions;
using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Caching;
using Point.Infrastructure.Extensions;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using static Point.Business.BusinessObjects.CalculateEntryBO;

namespace Point.Business.BusinessObjects
{
    public class FlowInstanceBO : IDisposable
    {
        public FlowInstanceBO()
        {
            CacheService.Reset();
        }

        #region Fields
        private IUnitOfWork uow = new UnitOfWork<PointSearchContext>();
        private CalculateEntryBO CalculateEntry { get; set; }
        private FlowInstanceSearchValues ReadModel { get; set; }
        private FlowInstanceReportValues ReportModel { get; set; }
        private List<FlowWebField> FlowWebFieldValues { get; set; }
        private Dictionary<string, object> ViewData { get; set; } = new Dictionary<string, object>();

        private static readonly List<string> targetmemotypes = new List<string>()
        {
            TransferMemoTypeID.Zorgprofessional.ToString(),
            TransferMemoTypeID.Transferpunt.ToString(),
            TransferMemoTypeID.Patient.ToString()
        };

        private static readonly List<string> feedbacktargetmemotypes = new List<string>
        {
            TransferMemoTypeID.FrequencyMSVT.ToString(),
            TransferMemoTypeID.FrequencyGeneral.ToString(),
            TransferMemoTypeID.FrequencyCVA.ToString()
        };

        private static readonly int grzformtype = (int)FlowFormType.GRZFormulier;
        #endregion

        #region Private imp.
        private void SetReportModel(int flowInstanceID)
        {
            var exists = uow.FlowInstanceReportValuesRepository.Exists(r => r.FlowInstanceID == flowInstanceID);
            if (exists)
            {
                ReportModel = new FlowInstanceReportValues() { FlowInstanceID = flowInstanceID };
                uow.FlowInstanceReportValuesRepository.Attach(ReportModel);
                uow.DatabaseContext.Entry(ReportModel).State = EntityState.Modified;
            }
            else
            {
                ReportModel = new FlowInstanceReportValues() { FlowInstanceID = flowInstanceID };
                uow.DatabaseContext.Entry(ReportModel).State = EntityState.Added;
            }
        }

        private void SetReadModel(int flowInstanceID)
        {
            var exists = uow.FlowInstanceSearchValuesRepository.Exists(r => r.FlowInstanceID == flowInstanceID);
            if (exists)
            {
                ReadModel = new FlowInstanceSearchValues() { FlowInstanceID = flowInstanceID };
                uow.FlowInstanceSearchValuesRepository.Attach(ReadModel);
                uow.DatabaseContext.Entry(ReadModel).State = EntityState.Modified;
            }
            else
            {
                ReadModel = new FlowInstanceSearchValues() { FlowInstanceID = flowInstanceID };
                uow.DatabaseContext.Entry(ReadModel).State = EntityState.Added;
            }
        }

        private void SetAttachmentDetails(FlowInstance flowinstance)
        {
            var attachmenttypeids = TransferAttachmentBL.GetAttachmentTypeIDsByTransferID(uow, flowinstance.TransferID);
            foreach (var attachmenttypeid in attachmenttypeids)
            {
                var attachmentdetails = new FlowInstanceReportValuesAttachmentDetail()
                {
                    FlowInstanceID = flowinstance.FlowInstanceID,
                    AttachmentTypeID = attachmenttypeid
                };

                ReportModel.FlowInstanceReportValuesAttachmentDetail.Add(attachmentdetails);

                uow.DatabaseContext.Entry(attachmentdetails).State = EntityState.Added;
            }
        }

        private DateTime? SetDateIfHigher(DateTime? target, DateTime? source)
        {
            if (source == null)
            {
                return target;
            }

            if (target == null || source > target)
            {
                return source;
            }

            return target;
        }

        private void SetDumpDetails(int flowInstanceID)
        {
            InviteStatus[] activestatuses = new InviteStatus[] { InviteStatus.Active, InviteStatus.Acknowledged, InviteStatus.Realized };
            ReportModel.FlowInstanceReportValuesDump = uow.FlowInstanceReportValuesDumpRepository.FirstOrDefault(d => d.FlowInstanceID == flowInstanceID);

            if (ReportModel.FlowInstanceReportValuesDump == null)
            {
                ReportModel.FlowInstanceReportValuesDump = new FlowInstanceReportValuesDump()
                {
                    FlowInstanceID = flowInstanceID
                };
                uow.DatabaseContext.Entry(ReportModel.FlowInstanceReportValuesDump).State = EntityState.Added;
            } else
            {
                uow.DatabaseContext.Entry(ReportModel.FlowInstanceReportValuesDump).State = EntityState.Modified;
            }

            var flowInstanceReportValuesDump = ReportModel.FlowInstanceReportValuesDump;

            var handledphaseinstancedetails = PhaseInstanceBL.GetCurrentPhaseInstanceDetailsWithHandling(uow, flowInstanceID);
            foreach (var handledphaseinstancegrp in handledphaseinstancedetails.GroupBy(it => it.FlowHandling))
            {
                foreach (var handledphaseinstance in handledphaseinstancegrp)
                {
                    switch (handledphaseinstancegrp.Key)
                    {
                        case FlowHandling.IndicatingCIZ:
                            flowInstanceReportValuesDump.IndicationCIZReceiveFormStartDate = handledphaseinstance.StartProcessDate;
                            flowInstanceReportValuesDump.IndicationCIZReceiveFormRealizeDate = handledphaseinstance.RealizedDate;
                            break;
                        case FlowHandling.IndicatingCIZDecision:
                            flowInstanceReportValuesDump.IndicationCIZDecisionFormStartDate = handledphaseinstance.StartProcessDate;
                            flowInstanceReportValuesDump.IndicationCIZDecisionFormRealizeDate = handledphaseinstance.RealizedDate;
                            break;
                        case FlowHandling.IndicatingGRZ:
                            flowInstanceReportValuesDump.IndicationGRZReceiveFormStartDate = handledphaseinstance.StartProcessDate;
                            flowInstanceReportValuesDump.IndicationGRZReceiveFormRealizeDate = handledphaseinstance.RealizedDate;
                            break;
                        case FlowHandling.IndicatingGRZDecision:
                            flowInstanceReportValuesDump.IndicationGRZDecisionFormStartDate = handledphaseinstance.StartProcessDate;
                            flowInstanceReportValuesDump.IndicationGRZDecisionFormRealizeDate = handledphaseinstance.RealizedDate;
                            break;
                        case FlowHandling.Receive:
                            flowInstanceReportValuesDump.ReceiveFormStartDate = handledphaseinstance.StartProcessDate;
                            flowInstanceReportValuesDump.ReceiveFormRealizedDate = handledphaseinstance.RealizedDate;
                            break;
                        case FlowHandling.ReceiveDecision:
                            flowInstanceReportValuesDump.DecisionFormStartDate = handledphaseinstance.StartProcessDate;
                            flowInstanceReportValuesDump.DecisionFormRealizedDate = handledphaseinstance.RealizedDate;
                            break;
                        case FlowHandling.RequestForm:
                            flowInstanceReportValuesDump.RequestFormStartDate = handledphaseinstance.StartProcessDate;
                            flowInstanceReportValuesDump.RequestFormRealizedDate = handledphaseinstance.RealizedDate;
                            break;

                        case FlowHandling.OpstellenVO:
                            flowInstanceReportValuesDump.OpstellenVODate = handledphaseinstance.RealizedDate ?? handledphaseinstance.LastChangeDate;
                            flowInstanceReportValuesDump.OpstellenVOStatus = handledphaseinstance.Status;
                            break;

                        case FlowHandling.MSVTUVV:
                            flowInstanceReportValuesDump.MSVTUVVDate = SetDateIfHigher(flowInstanceReportValuesDump.MSVTUVVDate, handledphaseinstance.RealizedDate ?? handledphaseinstance.LastChangeDate);
                            flowInstanceReportValuesDump.MSVTUVVCount += 1;
                            break;

                        case FlowHandling.MSVTIND:
                            flowInstanceReportValuesDump.MSVTINDDate = SetDateIfHigher(flowInstanceReportValuesDump.MSVTINDDate, handledphaseinstance.RealizedDate ?? handledphaseinstance.LastChangeDate);
                            break;

                        case FlowHandling.GRZ:
                            flowInstanceReportValuesDump.GRZDate = SetDateIfHigher(flowInstanceReportValuesDump.GRZDate, handledphaseinstance.RealizedDate ?? handledphaseinstance.LastChangeDate);
                            break;

                        case FlowHandling.ELV:
                            flowInstanceReportValuesDump.ELVDate = SetDateIfHigher(flowInstanceReportValuesDump.ELVDate, handledphaseinstance.RealizedDate ?? handledphaseinstance.LastChangeDate);
                            break;

                        case FlowHandling.DoorlDossier:
                            flowInstanceReportValuesDump.DoorlDossierCount += 1;
                            break;

                        case FlowHandling.Aanvullen:
                            flowInstanceReportValuesDump.AanvullenFirstDate = handledphaseinstance.StartProcessDate;
                            flowInstanceReportValuesDump.AanvullenLastDate = SetDateIfHigher(flowInstanceReportValuesDump.AanvullenLastDate, handledphaseinstance.RealizedDate ?? handledphaseinstance.LastChangeDate);
                            break;

                        case FlowHandling.RequestInBehandeling:
                            flowInstanceReportValuesDump.RequestInBehandelingStartDate = handledphaseinstance.StartProcessDate;
                            flowInstanceReportValuesDump.RequestInBehandelingRealizeDate = handledphaseinstance.RealizedDate ?? handledphaseinstance.LastChangeDate;
                            flowInstanceReportValuesDump.RequestInBehandelingStatus = handledphaseinstance.Status;
                            break;

                        case FlowHandling.Forward:
                            flowInstanceReportValuesDump.ForwardDate = SetDateIfHigher(flowInstanceReportValuesDump.ForwardDate, handledphaseinstance.RealizedDate);
                            break;

                        case FlowHandling.CloseDossier:
                            flowInstanceReportValuesDump.TransferClosedDate = SetDateIfHigher(flowInstanceReportValuesDump.TransferClosedDate, handledphaseinstance.RealizedDate);
                            break;
                    }
                }
            }

            var receivedata = ReadModel.FlowInstanceOrganization.ToList();

            var activeregular = receivedata.Where(d => activestatuses.Contains(d.InviteStatus)).FirstOrDefault(d => d.InviteType == InviteType.Regular);
            var activeindicating = receivedata.Where(d => activestatuses.Contains(d.InviteStatus)).FirstOrDefault(d => d.InviteType == InviteType.IndicatingCIZ);
            var activeindicatinggrz = receivedata.Where(d => activestatuses.Contains(d.InviteStatus)).FirstOrDefault(d => d.InviteType == InviteType.IndicatingGRZ);
            var activedoorldossier = receivedata.Where(d => activestatuses.Contains(d.InviteStatus)).FirstOrDefault(d => d.InviteType == InviteType.DoorlDossier);

            flowInstanceReportValuesDump.ReceiveCount = receivedata.Count(d => d.InviteType == InviteType.Regular);
            flowInstanceReportValuesDump.ReceivingDate = activeregular?.InviteSend;
            flowInstanceReportValuesDump.ReceivingRegionName = activeregular?.Organization?.Region?.Name;
            flowInstanceReportValuesDump.ReceivingOrganizationName = activeregular?.Organization?.Name;
            flowInstanceReportValuesDump.ReceivingLocationName = activeregular?.Department?.Location?.Name;
            flowInstanceReportValuesDump.ReceivingDepartmentName = activeregular?.Department?.Name;
            flowInstanceReportValuesDump.ReceivingArea = activeregular?.Department?.Location?.Area;
            flowInstanceReportValuesDump.ReceivingPostalCode = activeregular?.Department?.Location?.PostalCode;
            flowInstanceReportValuesDump.IndiceringCIZOrganizationName = activeindicating?.Organization?.Name;
            flowInstanceReportValuesDump.IndiceringCIZLocationName = activeindicating?.Department?.Location?.Name;
            flowInstanceReportValuesDump.IndiceringCIZDepartmentName = activeindicating?.Department?.Name;
            flowInstanceReportValuesDump.IndiceringGRZOrganizationName = activeindicatinggrz?.Organization?.Name;
            flowInstanceReportValuesDump.IndiceringGRZLocationName = activeindicatinggrz?.Department?.Location?.Name;
            flowInstanceReportValuesDump.IndiceringGRZDepartmentName = activeindicatinggrz?.Department?.Name;
            flowInstanceReportValuesDump.DoorlopendDossierOrganizationName = activedoorldossier?.Organization?.Name;
            flowInstanceReportValuesDump.DoorlopendDossierLocationName = activedoorldossier?.Department?.Location?.Name;
            flowInstanceReportValuesDump.DoorlopendDossierDepartmentName = activedoorldossier?.Department?.Name;
        }

        private CalculateEntryBO GetCalculateEntry(int flowInstanceID)
        {
            var sAanvullendeGegevensBevindingenMedewerkerTransferpunt = TransferMemoTypeID.AanvullendeGegevensBevindingenMedewerkerTransferpunt.ToString();

            var fi = uow.FlowInstanceRepository.Get(it => it.FlowInstanceID == flowInstanceID, includeProperties: "Transfer").FirstOrDefault();
            if (fi == null)
            {
                return null;
            }

            var calcentry = new CalculateEntryBO()
            {
                CopyOfTransferID = fi.Transfer.OriginalTransferID,

                Client = uow.ClientRepository.Get(c => c.ClientID == fi.Transfer.ClientID).Select(c => new ClientModel()
                {
                    Initials = c.Initials,
                    BirthDate = c.BirthDate,
                    CivilServiceNumber = c.CivilServiceNumber,
                    ClientIsAnonymous = c.ClientIsAnonymous,
                    FirstName = c.FirstName,
                    Gender = c.Gender,
                    HealthInsuranceCompanyID = c.HealthInsuranceCompanyID,
                    LastName = c.LastName,
                    MiddleName = c.MiddleName,
                    PatientNumber = c.PatientNumber,
                    VisitNumber = c.VisitNumber,
                    PostalCode = c.PostalCode
                }).FirstOrDefault(),

                //FlowInstance = fi,

                LastTransferMemoDate = fi.Transfer.TransferMemos.Where(m =>
                    targetmemotypes.Contains(m.Target) &&
                    m.Deleted == false)
                    .OrderByDescending(m => m.TransferMemoID)
                    .Select(m => m.MemoDateTime).FirstOrDefault(),

                LastAdditionalTPMemoDate = fi.Transfer.TransferMemos.Where(m =>
                    m.Target == sAanvullendeGegevensBevindingenMedewerkerTransferpunt &&
                    m.Deleted == false)
                    .OrderByDescending(m => m.TransferMemoID)
                    .Select(m => m.MemoDateTime).FirstOrDefault(),

                LastTransferAttachmentDate = fi.Transfer.TransferAttachments.Where(a =>
                    a.AttachmentSource == AttachmentSource.None &&
                    a.Deleted != true)
                    .OrderByDescending(a => a.AttachmentID)
                    .Select(a => (DateTime?)a.UploadDate).FirstOrDefault(),

                LastGrzFormSetVersion = fi.Transfer.FormSetVersions.FirstOrDefault(f =>
                    grzformtype == f.FormTypeID &&
                    f.Deleted != true),

                LastTransferTask = fi.Transfer.TransferTasks.Where(t =>
                    t.Status == Status.Active &&
                    t.Inactive == false)
                    .OrderBy(t => t.DueDate)
                    .FirstOrDefault()
            };

            return calcentry;
        }

        private void DeleteReadModel(int flowInstanceID)
        {
            ReadModel = uow.FlowInstanceSearchValuesRepository.GetByID(flowInstanceID);
            if (ReadModel != null)
            {
                uow.DatabaseContext.Entry(ReadModel).State = EntityState.Deleted;
            }
        }

        private void DeleteReportModel(int flowInstanceID)
        {
            foreach (var item in uow.FlowInstanceReportValuesRepository.Get(it => it.FlowInstanceID == flowInstanceID).ToList())
            {
                uow.DatabaseContext.Entry(item).State = EntityState.Deleted;
            }

            foreach (var item in uow.FlowInstanceReportValuesDumpRepository.Get(it => it.FlowInstanceID == flowInstanceID).ToList())
            {
                uow.DatabaseContext.Entry(item).State = EntityState.Deleted;
            }
        }

        private void DeleteReferencedEntities(int flowInstanceID)
        {
            foreach (var item in uow.FlowInstanceOrganizationRepository.Get(it => it.FlowInstanceID == flowInstanceID).ToList())
            {
                uow.DatabaseContext.Entry(item).State = EntityState.Deleted;
            }
            foreach (var item in uow.FlowInstanceDoorlopendDossierSearchValuesRepository.Get(it => it.FlowInstanceID == flowInstanceID).ToList())
            {
                uow.DatabaseContext.Entry(item).State = EntityState.Deleted;
            }
            foreach (var item in uow.FlowInstanceReportValuesProjectsRepository.Get(it => it.FlowInstanceID == flowInstanceID).ToList())
            {
                uow.DatabaseContext.Entry(item).State = EntityState.Deleted;
            }
            foreach (var item in uow.FlowInstanceReportValuesAttachmentDetailRepository.Get(it => it.FlowInstanceID == flowInstanceID).ToList())
            {
                uow.DatabaseContext.Entry(item).State = EntityState.Deleted;
            }
        }

        private void addDepartmentsToReadmodel(FlowInstance flowInstance)
        {
            var flowinstanceid = flowInstance.FlowInstanceID;

            foreach (var item in uow.FlowInstanceOrganizationRepository.Get(it => it.FlowInstanceID == flowinstanceid).ToList())
            {
                uow.DatabaseContext.Entry(item).State = EntityState.Deleted;
            }

            var starteddepartmeent = new FlowInstanceOrganization()
            {
                FlowInstanceID = flowinstanceid,
                FlowDirection = FlowDirection.Sending,
                OrganizationID = flowInstance.StartedByOrganizationID.Value,
                Organization = flowInstance.StartedByOrganization,
                LocationID = flowInstance.StartedByDepartment.LocationID,
                Location = flowInstance.StartedByDepartment?.Location,
                DepartmentID = flowInstance.StartedByDepartmentID,
                Department = flowInstance.StartedByDepartment,
                InviteType = InviteType.None,
                InviteStatus = InviteStatus.Active
            };

            ReadModel.FlowInstanceOrganization.Add(starteddepartmeent);
            uow.DatabaseContext.Entry(starteddepartmeent).State = EntityState.Added;

            var invites = OrganizationInviteBL.GetByFlowInstanceID(uow, flowinstanceid);
            foreach (var invite in invites.GroupBy(i => new { i.DepartmentID, i.InviteStatus }).Select(g => g.FirstOrDefault()))
            {
                var recievingdepartment = new FlowInstanceOrganization()
                {
                    FlowInstanceID = flowinstanceid,
                    FlowDirection = FlowDirection.Receiving,
                    OrganizationID = invite.OrganizationID,
                    Organization = invite.Organization,
                    LocationID = invite.Department.LocationID,
                    Location = invite.Department.Location,
                    DepartmentID = invite.DepartmentID,
                    Department = invite.Department,
                    InviteType = invite.InviteType,
                    InviteSend = invite.InviteSend,
                    InviteStatus = invite.InviteStatus,
                    ClientIsAnonymous = invite.OrganizationID != flowInstance.StartedByOrganizationID && CalculateEntry.Client.ClientIsAnonymous,
                    HandlingBy = invite.HandlingBy,
                    HandlingRemark = invite.HandlingRemark,
                    HandlingDateTime = invite.HandlingDateTime
                };

                ReadModel.FlowInstanceOrganization.Add(recievingdepartment);
                uow.DatabaseContext.Entry(recievingdepartment).State = EntityState.Added;
            }
        }

        private void addDoorlopendDossierData(int flowInstanceID)
        {
            var frequencies = FrequencyBL.GetByFlowInstanceID(uow, flowInstanceID).Where(f => f.Cancelled == false).ToList();
            if (frequencies.Count > 0)
            {
                foreach (var item in frequencies)
                {
                    int frequencyid = item.FrequencyID;

                    var readmodelrow = new FlowInstanceDoorlopendDossierSearchValues()
                    {
                        FlowInstanceSearchValues = ReadModel,
                        FlowInstanceID = flowInstanceID,
                        Frequency = item,
                        FrequencyID = frequencyid,
                        FrequencyType = item.Type,
                        FrequencyStartDate = item.StartDate,
                        FrequencyEndDate = item.EndDate,
                        FrequencyName = item.Name
                    };

                    var lastTransferMemo = TransferMemoBL.GetByFrequencyID(uow, frequencyid).OrderByDescending(it => it.TransferMemoID).FirstOrDefault();
                    if (lastTransferMemo != null)
                    {
                        readmodelrow.LastFrequencyTransferMemoID = lastTransferMemo.TransferMemoID;
                        readmodelrow.LastFrequencyTransferMemoDateTime = lastTransferMemo.MemoDateTime;
                        readmodelrow.LastFrequencyTransferMemoEmployeeName = lastTransferMemo.EmployeeName;

                        if (Enum.TryParse(lastTransferMemo.Target, out TransferMemoTypeID target))
                        {
                            readmodelrow.LastFrequencyTransferMemoTarget = target;
                        }
                    }

                    var lastTransferAttachment = TransferAttachmentBL.GetByFrequencyID(uow, frequencyid).OrderByDescending(it => it.AttachmentID).FirstOrDefault();
                    if (lastTransferAttachment != null)
                    {
                        readmodelrow.LastFrequencyAttachmentID = lastTransferAttachment.AttachmentID;
                        readmodelrow.LastFrequencyAttachmentUploadDate = lastTransferAttachment.UploadDate;
                        readmodelrow.LastFrequencyAttachmentSource = lastTransferAttachment.AttachmentSource;
                    }

                    var scopetype = ScopeTypeHelper.ByFrequencyID(uow, frequencyid);
                    readmodelrow.FrequencyScopeType = (int)scopetype;

                    uow.DatabaseContext.Entry(readmodelrow).State = EntityState.Added;
                }
            }
        }
        
        private void SetFlowFieldValues(int flowInstanceID)
        {
            FlowWebFieldValues = FlowWebFieldBL.GetFastReadModelValuesByFlowInstance(uow, flowInstanceID, ViewData).Where(it => !String.IsNullOrEmpty(it.Value)).ToList();
        }

        private void CalculateSearchValues(int flowInstanceID)
        {
            CalculateEntry = GetCalculateEntry(flowInstanceID);
            if (CalculateEntry == null)
            {
                return;
            }

            var flowinstance = uow.FlowInstanceRepository.FirstOrDefault(f => f.FlowInstanceID == flowInstanceID);
            if (flowinstance?.Deleted == true)
            {
                DeleteReadModel(flowInstanceID);
                return;
            }

            var scope = ScopeTypeHelper.ByFlowInstanceID(uow, flowInstanceID);

            SetReadModel(flowInstanceID);

            ReadModel.TransferID = flowinstance.TransferID;
            ReadModel.FlowDefinitionID = flowinstance.FlowDefinitionID;
            ReadModel.IsInterrupted = flowinstance.Interrupted;
            ReadModel.CopyOfTransferID = CalculateEntry.CopyOfTransferID;
            ReadModel.ActivePhaseText = scope == ScopeType.Closed ? "Afgesloten" : FlowInstanceBL.GetActivePhaseTextByFlowInstance(uow, flowinstance);
            ReadModel.AcceptedBy = FlowWebFieldValues.GetFlowFieldDisplayValue(FlowFieldConstants.ID_AcceptedBy);
            ReadModel.AcceptedByID = FlowWebFieldValues.GetFlowFieldValue(FlowFieldConstants.ID_AcceptedBy).ConvertTo<int?>();
            ReadModel.AcceptedByVVT = FlowWebFieldValues.GetFlowFieldDisplayValue(FlowFieldConstants.ID_AcceptedByVVT);
            ReadModel.AcceptedByVVTID = FlowWebFieldValues.GetFlowFieldValue(FlowFieldConstants.ID_AcceptedByVVT).ConvertTo<int?>();
            ReadModel.AcceptedByII = FlowWebFieldValues.GetFlowFieldDisplayValue(FlowFieldConstants.ID_AcceptedByII);
            ReadModel.AcceptedByIIID = FlowWebFieldValues.GetFlowFieldValue(FlowFieldConstants.ID_AcceptedByII).ConvertTo<int?>();
            ReadModel.AcceptedByTelephoneNumber = FlowWebFieldValues.GetFlowFieldDisplayValue(FlowFieldConstants.ID_AcceptedByTelephoneNumber);
            ReadModel.AcceptedDateTP = FlowWebFieldValues.GetFlowFieldDateTimeValue(FlowFieldConstants.ID_AcceptedDateTP);
            ReadModel.BehandelaarSpecialisme = FlowWebFieldValues.GetFlowFieldDisplayValue(FlowFieldConstants.ID_BehandelaarSpecialisme);
            ReadModel.CareBeginDate = FlowWebFieldValues.GetFlowFieldDateTimeValue(FlowFieldConstants.ID_CareBeginDate);
            ReadModel.ClientBirthDate = CalculateEntry.Client.BirthDate;
            ReadModel.ClientFullname = CalculateEntry.Client.FullName();
            ReadModel.ClientGender = CalculateEntry.Client.Gender.GetDescription();
            ReadModel.DatumEindeBehandelingMedischSpecialist = FlowWebFieldValues.GetFlowFieldDateTimeValue(FlowFieldConstants.ID_DatumEindeBehandelingMedischSpecialist);

            ReadModel.DepartmentName = flowinstance.StartedByDepartment.Name;
            ReadModel.FullDepartmentName = flowinstance.StartedByDepartment.FullName();
            if (flowinstance.StartedByDepartment.Location != null)
            {
                ReadModel.LocationName = flowinstance.StartedByDepartment.Location.Name;
                if (flowinstance.StartedByOrganizationID != null)
                {
                    ReadModel.OrganizationName = flowinstance.StartedByOrganization.Name;
                }
            }

            ReadModel.DesiredHealthCareProvider = FlowWebFieldValues.GetFlowFieldDisplayValue(FlowFieldConstants.ID_DestinationHealthCareProvider);
            ReadModel.DestinationHospital = FlowWebFieldValues.GetFlowFieldDisplayValue(FlowFieldConstants.ID_DestinationHospital);
            ReadModel.DestinationHospitalDepartment = FlowWebFieldValues.GetFlowFieldDisplayValue(FlowFieldConstants.ID_DestinationHospitalDepartment);

            var patientacceptedstate = PatientHelper.IsAccepted(uow, flowInstanceID);
            if (patientacceptedstate.HasValue)
            {
                if (patientacceptedstate == AcceptanceState.Acknowledged || patientacceptedstate == AcceptanceState.AcknowledgedPartially)
                {
                    ReadModel.DischargePatientAcceptedYNByVVT = FlowFieldConstants.Value_Ja;
                    ReadModel.DischargeProposedStartDate = FlowWebFieldValues.GetFlowFieldDateTimeValue(FlowFieldConstants.ID_DischargeProposedStartDate);
                }

                if (patientacceptedstate ==  AcceptanceState.Refused)
                {
                    ReadModel.DischargePatientAcceptedYNByVVT = FlowFieldConstants.Value_Nee;
                    ReadModel.DischargeProposedStartDate = FlowWebFieldValues.GetFlowFieldDateTimeValue(FlowFieldConstants.ID_DischargeProposedStartDateByNo);
                    ReadModel.AcceptedByVVT = null;
                    ReadModel.AcceptedByVVTID = null;
                }
            }

            ReadModel.LastAdditionalTPMemoDateTime = CalculateEntry.LastAdditionalTPMemoDate;
            ReadModel.LastGrzFormDateTime = CalculateEntry.LastGrzFormSetVersion?.UpdateDate ?? CalculateEntry.LastGrzFormSetVersion?.CreateDate;
            ReadModel.LastTransferAttachmentUploadDate = CalculateEntry.LastTransferAttachmentDate;
            ReadModel.LastTransferMemoDateTime = CalculateEntry.LastTransferMemoDate;
            ReadModel.RequestFormZHVVTType = FlowWebFieldValues.GetFlowFieldDisplayValue(FlowFieldConstants.ID_RequestFormZHVVTType);
            ReadModel.TransferCreatedDate = flowinstance.CreatedDate;
            ReadModel.TransferDate = FlowWebFieldValues.GetFlowFieldDateTimeValue(FlowFieldConstants.ID_TransferDate);

            // TransferDateVVT is also a flowfield name (wich is filled at the wrong time)
            var transferDateVVTphaseinstance = PhaseInstanceBL.GetByFlowInstanceIDAndFormTypeID(uow, flowInstanceID, (int)FlowFormType.InBehandelingVVTZHVVT);
            if (transferDateVVTphaseinstance != null)
            {
                ReadModel.TransferDateVVT = transferDateVVTphaseinstance.StartProcessDate;
            }

            var aftercarefield = this.FlowWebFieldValues.FirstOrDefault(ffv => ffv.FlowFieldID == FlowFieldConstants.ID_AfterCareDefinitive && !string.IsNullOrEmpty(ffv.Value)) ??
                                 this.FlowWebFieldValues.FirstOrDefault(ffv => ffv.FlowFieldID == FlowFieldConstants.ID_AfterCare && !string.IsNullOrEmpty(ffv.Value));
            if (aftercarefield != null)
            {
                if (int.TryParse(aftercarefield.Value, out int aftercareint))
                {
                    var aftercare = AfterCareTypeBL.GetCachedByID(uow, aftercareint);
                    ReadModel.TyperingNazorgCombined = aftercare?.ReportCode;
                }
            }

            ReadModel.ScopeType = (int)scope;
            ReadModel.FlowDefinitionName = flowinstance.FlowDefinition.Name;

            var transferpointstep = PhaseInstanceBL.GetByFlowInstanceIDAndFormTypeID(uow, flowInstanceID, (int)FlowFormType.InBehandelingTP);
            if (transferpointstep != null)
            {
                ReadModel.RequestTransferPointIntakeDate = transferpointstep.StartProcessDate;
            }

            ReadModel.ClientCivilServiceNumber = CalculateEntry.Client.CivilServiceNumber;

            var startedby = flowinstance.StartedByEmployee;
            if (startedby != null)
            {
                ReadModel.TransferCreatedByID = startedby.EmployeeID;
                ReadModel.TransferCreatedBy = startedby.FullName();
            }

            ReadModel.DestinationHospitalLocation = FlowWebFieldValues.GetFlowFieldDisplayValue(FlowFieldConstants.ID_DestinationHospitalLocation);
            ReadModel.BehandelaarNaam = FlowWebFieldValues.GetFlowFieldDisplayValue(FlowFieldConstants.ID_BehandelaarNaam);
            ReadModel.MedischeSituatieDatumOpname = FlowWebFieldValues.GetFlowFieldDateTimeValue(FlowFieldConstants.ID_MedischeSituatieDatumOpname);
            ReadModel.AcceptedByTelephoneVVT = FlowWebFieldValues.GetFlowFieldDisplayValue(FlowFieldConstants.ID_AcceptedByTelephoneVVT);
            ReadModel.ZorgInZorginstellingVoorkeurPatient1 = FlowWebFieldValues.GetFlowFieldDisplayValue(FlowFieldConstants.ID_ZorgInZorginstellingVoorkeurPatient1);

            if (CalculateEntry.Client.HealthInsuranceCompanyID.HasValue)
            {
                var healthinsurer = uow.HealthInsurerRepository.GetByID(CalculateEntry.Client.HealthInsuranceCompanyID);
                if (healthinsurer != null)
                {
                    ReadModel.HealthInsuranceCompany = healthinsurer.Name;
                    ReadModel.HealthInsuranceCompanyID = healthinsurer.HealthInsurerID;
                }
            }

            ReadModel.GewensteIngangsdatum = FlowWebFieldValues.GetFlowFieldDateTimeValue(FlowFieldConstants.ID_GewensteIngangsdatum);
            ReadModel.PatientNumber = CalculateEntry.Client.PatientNumber;
            ReadModel.TransferTaskDueDate = CalculateEntry.LastTransferTask?.DueDate;
            ReadModel.ReadModelTimeStamp = DateTime.Now;

            addDoorlopendDossierData(flowInstanceID);
            addDepartmentsToReadmodel(flowinstance);
        }

        private void CalculateReportValues(int flowInstanceID)
        {
            var flowinstance = FlowInstanceBL.GetByID(uow, flowInstanceID);
            if (flowinstance == null)
            {
                DeleteReportModel(flowInstanceID);
                return;
            }

            SetReportModel(flowInstanceID);
            SetAttachmentDetails(flowinstance);
            SetDumpDetails(flowInstanceID);

            ReportModel.ClientName = CalculateEntry.Client.FullName();
            ReportModel.ClientBirthDate = CalculateEntry.Client.BirthDate;
            ReportModel.ClientBSN = CalculateEntry.Client.CivilServiceNumber;
            ReportModel.ClientHealthCareInsurer = ReadModel.HealthInsuranceCompany;
            ReportModel.ClientGender = CalculateEntry.Client.Gender.GetDescription();
            ReportModel.ClientPatientNumber = CalculateEntry.Client.PatientNumber;
            ReportModel.ClientVisitNumber = CalculateEntry.Client.VisitNumber;
            ReportModel.ClientPostalCode = CalculateEntry.Client.PostalCode;
            ReportModel.TimeStamp = DateTime.Now;
            ReportModel.TransferCreatedDate = flowinstance.CreatedDate;
            ReportModel.TransferCreatedBy = flowinstance.StartedByEmployee.FullName();
            ReportModel.FlowDefinitionID = flowinstance.FlowDefinitionID;
            ReportModel.FlowDefinitionName = flowinstance.FlowDefinition.Name;
            ReportModel.ActivePhaseText = ReadModel.ActivePhaseText;
            ReportModel.AcceptedBy = FlowWebFieldValues.GetFlowFieldDisplayValue(FlowFieldConstants.ID_AcceptedBy);
            ReportModel.RequestFormZHVVTType = FlowWebFieldValues.GetFlowFieldDisplayValue(FlowFieldConstants.ID_RequestFormZHVVTType);
            ReportModel.PatientOntvingZorgVoorOpnameJaNee = FlowWebFieldValues.GetFlowFieldValue(FlowFieldConstants.ID_PatientOntvingZorgVoorOpnameJaNee).StringToNullableBool();
            ReportModel.ZorginzetVoorDezeOpnameTypeZorginstelling = FlowWebFieldValues.GetFlowFieldDisplayValue(FlowFieldConstants.ID_ZorginzetVoorDezeOpnameTypeZorginstelling);
            ReportModel.ScopeType = (ScopeType)ReadModel.ScopeType;
            ReportModel.SpecialismeBehandelaar = FlowWebFieldValues.GetFlowFieldDisplayValue(FlowFieldConstants.ID_BehandelaarSpecialisme);
            ReportModel.PrognoseVerwachteOntwikkeling = FlowWebFieldValues.GetFlowFieldDisplayValue(FlowFieldConstants.ID_PrognoseVerwachteOntwikkeling);
            ReportModel.KwetsbareOudereJaNee = FlowWebFieldValues.GetFlowFieldValue(FlowFieldConstants.ID_KwetsbareOudereJaNee).StringToNullableBool();
            ReportModel.InterruptionDays = FlowInstanceStatusBL.GetInterruptionDays(uow, flowInstanceID);
            ReportModel.AfterCareFinancing = FlowWebFieldValues.GetFlowFieldDisplayValue(FlowFieldConstants.ID_AfterCareFinancing);
            ReportModel.FirstInitialDischargeDate = FlowWebFieldValues.GetFlowFieldDateTimeValue(FlowFieldConstants.ID_OorspronkelijkeDatumEindeBehandelingMedischSpecialist);
            ReportModel.InitialDischargeDate = FlowWebFieldValues.GetFlowFieldDateTimeValue(FlowFieldConstants.ID_DatumEindeBehandelingMedischSpecialist);
            ReportModel.DischargeProposedStartDate = FlowWebFieldValues.GetFlowFieldDateTimeValue(FlowFieldConstants.ID_DischargeProposedStartDate);
            ReportModel.RealizedDischargeDate = FlowWebFieldValues.GetFlowFieldDateTimeValue(FlowFieldConstants.ID_RealizedDischargeDate);
            ReportModel.DatumOpname = FlowWebFieldValues.GetFlowFieldDateTimeValue(FlowFieldConstants.ID_MedischeSituatieDatumOpname);
            ReportModel.CareBeginDate = FlowWebFieldValues.GetFlowFieldDateTimeValue(FlowFieldConstants.ID_CareBeginDate);
            ReportModel.DateIndicationDecision = FlowWebFieldValues.GetFlowFieldDateTimeValue(FlowFieldConstants.ID_DateIndicationDecision);
            ReportModel.RegistrationEndedWithoutTransfer = FlowWebFieldValues.GetFlowFieldValue(FlowFieldConstants.ID_RegistrationEndedWithoutTransfer).StringToNullableBool();
            ReportModel.ReasonEndRegistration = flowinstance.FlowDefinitionID == FlowDefinitionID.HA_VVT
                ? FlowWebFieldValues.GetFlowFieldDisplayValue(FlowFieldConstants.ID_ReasonEndRegistrationHA)
                : FlowWebFieldValues.GetFlowFieldDisplayValue(FlowFieldConstants.ID_ReasonEndRegistration);
            ReportModel.AdjustmentStandingCare = FlowWebFieldValues.GetFlowFieldDisplayValue(FlowFieldConstants.ID_AdjustmentStandingCare);
            ReportModel.AcceptedByVVT = FlowWebFieldValues.GetFlowFieldDisplayValue(FlowFieldConstants.ID_AcceptedByVVT);
            ReportModel.AcceptedByII = FlowWebFieldValues.GetFlowFieldDisplayValue(FlowFieldConstants.ID_AcceptedByII);
            ReportModel.PatientAccepted = PatientHelper.IsAccepted(uow, flowInstanceID);
            ReportModel.DestinationHospitalDepartment = FlowWebFieldValues.GetFlowFieldDisplayValue(FlowFieldConstants.ID_DestinationHospitalDepartment);
            ReportModel.PatientWordtOpgenomenDatum = FlowWebFieldValues.GetFlowFieldDateTimeValue(FlowFieldConstants.ID_PatientWordtOpgenomenDatum);
            ReportModel.FirstReceiverDate = flowinstance.OrganizationInvite.Where(it => it.InviteType == InviteType.Regular).Min(it => it.InviteSend);
            ReportModel.ZorgAdviesToevoegen = FlowWebFieldValues.GetFlowFieldValue(FlowFieldConstants.ID_ZorgAdviesToevoegen).StringToNullableBool() ?? false;

            if(flowinstance.IsAcute)
            {
                ReportModel.IsVoorrang = true;
            } else
            {
                ReportModel.IsVoorrang = AfterCareTypeBL.IsPalliatief(FlowWebFieldValues);
            }

            ReportModel.VOSendDate = CommunicationLogBL.GetVoSendDate(uow, flowinstance.TransferID);

            //Samengevoegd (uit aanvraagform en o.a. "stuur naar VVT" form)
            var aftercarefield = FlowWebFieldValues.GetFlowFieldValue(FlowFieldConstants.ID_AfterCareDefinitive) ?? FlowWebFieldValues.GetFlowFieldValue(FlowFieldConstants.ID_AfterCare);
            if (int.TryParse(aftercarefield, out int aftercareint))
            {
                var aftercare = AfterCareTypeBL.GetCachedByID(uow, aftercareint);
                ReportModel.AfterCareCategoryDossier = aftercare?.AfterCareCategory?.Name;
                ReportModel.AfterCareTypeDossier = aftercare?.ReportCode;
            }
        }
        #endregion
        
        public void Save()
        {
            uow.Save();
        }

        public bool CalculateSearchValuesByFlowInstanceID(int flowinstanceID)
        {
            DeleteReferencedEntities(flowinstanceID);

            SetFlowFieldValues(flowinstanceID);

            CalculateSearchValues(flowinstanceID);
            CalculateReportValues(flowinstanceID);

            return true;
        }

        //Correct handling of dispose
        //See https://msdn.microsoft.com/en-us/library/ms244737.aspx
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~FlowInstanceBO()
        {
            Dispose(false);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (uow != null)
                {
                    this.FlowWebFieldValues = null;
                    this.CalculateEntry = null;
                    this.ReportModel = null;
                    this.ReadModel = null;
                    this.CalculateEntry = null;

                    uow.SafeDispose();
                    uow = null;
                }
            }
        }
    }
}
