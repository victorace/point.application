﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Point.Business.Logic;
using Point.Database.Context;
using Point.Database.Repository;
using Point.Infrastructure.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Point.Business.BusinessObjects
{
    public class EmployeeParameters
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string PhoneNumber { get; set; }
        public string Location { get; set; }
        public string Department { get; set; }
        public string CheckKey { get; set; }
        public string ExternID { get; set; }
        public string AutoCreateCode { get; set; }
    }

    public class EmployeeParametersBO
    {
        public int OrganizationID { get; set; }
        public EmployeeParameters EmployeeParameters { get; set; }
        public List<string> Errors = new List<string>();

        public EmployeeParametersBO(string emplReference, string externalReference, int organizationID) 
        {
            OrganizationID = organizationID;
            if(OrganizationID < 1)
            {
                Errors.Add(string.Format("Missing OrgExternalReference1: [{0}]", organizationID));
            }

            if (!emplReference.IsBase64String())
            {
                Errors.Add(string.Format("Provided emplReference is not Base64: [{0}]", emplReference));
            }

            string base64decoded = Encoding.UTF8.GetString(Convert.FromBase64String(emplReference));
            EmployeeParameters = JsonConvert.DeserializeObject<EmployeeParameters>(base64decoded, new JsonSerializerSettings {
                Error = delegate (object sender, ErrorEventArgs args) {
                    Errors.Add(args.ErrorContext.Error.Message); args.ErrorContext.Handled = true;
                }
            });

            if(EmployeeParameters == null)
            {
                Errors.Add("Couldnt Deserialize EmployeeParameters");
                return;
            }

            if(CheckKey() == false)
            {
                Errors.Add(string.Format("CheckKey [{0}] komt niet overeen met gecalculeerde key.", EmployeeParameters.CheckKey));
            }

            EmployeeParameters.ExternID = externalReference;
        }

        private bool CheckKey()
        {
            using (var uow = new UnitOfWork<PointSearchContext>())
            {
                var organization = OrganizationBL.GetByOrganizationID(uow, OrganizationID);
                if(organization == null)
                {
                    Errors.Add(string.Format("No organization with ID: [{0}]", OrganizationID));
                    return false;
                }

                byte[] hashbytes = Encoding.UTF8.GetBytes(String.Format("{0}_{1}_{2}", organization.SSOKey, EmployeeParameters?.LastName, EmployeeParameters?.Location));
                var hashAlgorithm = SecurityBL.GetAlgorithmFromString(organization.SSOHashType); 
                byte[] computedhash =  hashAlgorithm.ComputeHash(hashbytes);
                string calculatedcheckkey = "";
                foreach (byte b in computedhash)
                {
                    calculatedcheckkey += b.ToString("X2");
                }

                return calculatedcheckkey.ToLower() == EmployeeParameters?.CheckKey.ToLower();
            }
        }
    }
}