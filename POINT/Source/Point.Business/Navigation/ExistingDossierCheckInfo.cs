﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Point.Business.Logic;
using Point.Database.Context;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Extensions;

namespace Point.Business.Navigation
{
    public class ExistingDossierCheckInfo
    {
        private readonly PointUserInfo _pointUserInfo;
        private int _usersDepartmentExistingDossierCount;
        private List<string> _dossiersExistAtOtherDepartments = new List<string>();

        private ExistingDossierCheckInfo(PointUserInfo pointUserInfo)
        {
            _pointUserInfo = pointUserInfo;
        }

        public static ExistingDossierCheckInfo CreateForUser(IUnitOfWork uow, PointUserInfo pointUserInfo, string patientNumber, string bsnNumber)
        {
            var res = new ExistingDossierCheckInfo(pointUserInfo);

            var departmentIDs = new HashSet<int>();

            var userDepartments = DepartmentBL.GetByOrganizationID(uow, pointUserInfo.Organization.OrganizationID);
            
            if (userDepartments.Count() > 0)
            {
                departmentIDs.AddRange(from d in userDepartments select d.DepartmentID);

                var departments = DepartmentBL.GetDepartmentsByExternalNumber(uow, patientNumber, bsnNumber, departmentIDs);

                foreach (var department in departments)
                {
                    if (department.DepartmentID == pointUserInfo.Department.DepartmentID)
                        res._usersDepartmentExistingDossierCount++;
                    else
                        res._dossiersExistAtOtherDepartments.Add(department.Name);
                }
            }

            return res;
        }

        public static ExistingDossierCheckInfo CreateForNoNeed()
        {
            return new ExistingDossierCheckInfo(null);
        }

        public bool NeedToShowWarning
        {
            get
            {
                bool res = _pointUserInfo != null && (this.UsersDepartmentExistingDossierCount > 0 || DossiersExistAtOtherDepartments.Count() > 0);

                return res;
            }
        }

        public bool UserHasMultipleDepartments
        {
            get
            {
                return _pointUserInfo != null && _pointUserInfo.EmployeeDepartmentIDs != null && _pointUserInfo.EmployeeDepartmentIDs.Count() > 0;
            }
        }

        public int UsersDepartmentExistingDossierCount
        {
            get
            {
                return _usersDepartmentExistingDossierCount;
            }
        }

        public IEnumerable<string> DossiersExistAtOtherDepartments
        {
            get
            {
                return _dossiersExistAtOtherDepartments;
            }
        }

        public string DossiersExistAtOtherDepartmentsText
        {
            get
            {
                return string.Join(",", this.DossiersExistAtOtherDepartments);
            }
        }

        public string WarningText
        {
            get
            {
                if (!this.NeedToShowWarning)
                {
                    return string.Empty;
                }

                var sb = new StringBuilder();

                if (this.UsersDepartmentExistingDossierCount > 0)
                {
                    sb.Append("Er bestaan reeds een of meerdere dossiers van deze patient op uw afdeling");

                    if (this.UserHasMultipleDepartments)
                    {
                        sb.Append("(en)");
                    }

                    if (this.DossiersExistAtOtherDepartments.Count() > 0)
                    {
                        sb.Append(" en ook op volgende afdeling(en): ");
                    }
                }
                else
                {
                    sb.Append("Er bestaan reeds een of meerdere dossiers van deze patient op volgende afdeling(en): ");
                }

                if (this.DossiersExistAtOtherDepartments.Count() > 0)
                {
                    sb.Append(this.DossiersExistAtOtherDepartmentsText);
                }

                sb.AppendLine(".");

                return sb.ToString();
            }
        }

        public string WarningTextHtml
        {
            get
            {
                return this.WarningText
                                .Replace(Environment.NewLine, "\n")
                                .Replace("\n", @"<br/>");
            }
        }
    }
}
