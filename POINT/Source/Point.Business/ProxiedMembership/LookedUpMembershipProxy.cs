﻿using Point.Database.Repository;
using Point.Infrastructure.ProxiedMembership;

namespace Point.Business.ProxiedMembership
{
    public class LookedUpMembershipProxy : MembershipProxy
    {
        protected LookedUpMembershipProxy()
            : base()
        {
        }

        public static void SetLookedUp(IUnitOfWork uow, string userName)
        {
            MembershipProxy.Set(new LookedUpMembership(uow, userName));
        }

        public static void SetLookedUp(IUnitOfWork uow, int employeeId)
        {
            MembershipProxy.Set(new LookedUpMembership(uow, employeeId));
        }
    }
}
