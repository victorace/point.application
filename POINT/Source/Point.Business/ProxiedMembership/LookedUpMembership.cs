﻿using System;
using System.Linq;
using Point.Database.Repository;
using Point.Infrastructure.ProxiedMembership;

namespace Point.Business.ProxiedMembership
{
    public class LookedUpMembership : FixedMembership
    {
        public LookedUpMembership(IUnitOfWork uow, string userName)
            : base(LookupUserIdByName(uow, userName), userName)
        {
        }

        public LookedUpMembership(IUnitOfWork uow, int employeeId)
            : base(LookupUserByEmployee(uow, employeeId))
        {
        }

        private static Guid LookupUserIdByName(IUnitOfWork uow, string userName)
        {
            var au = uow.Aspnet_UsersRepository.Get(u => u.UserName == userName).FirstOrDefault();

            return au.UserId;
        }

        private static User LookupUserByEmployee(IUnitOfWork uow, int employeeId)
        {
            var emp = uow.EmployeeRepository.Get(e => e.EmployeeID == employeeId).FirstOrDefault();

            if (emp == null)
                throw new ApplicationException(string.Concat("Lookup employee ID ", employeeId, " not found"));

            if (!emp.UserId.HasValue)
                throw new ApplicationException(string.Concat("Looked up employee ID ", employeeId, " is not associated with a user"));

            var au = uow.Aspnet_UsersRepository.Get(u => u.UserId == emp.UserId.Value).FirstOrDefault();

            var res = new User(au.UserId, au.UserName);

            return res;
        }
    }
}
