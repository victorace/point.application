﻿using System;
using System.Collections.Generic;
using System.Linq;
using Point.Database.Extensions;
using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;

namespace Point.Business.Logic
{
    public class CommunicationLogViewBL
    {

        public static IEnumerable<CommunicationLogViewModel> FromModelList(IUnitOfWork uow, IEnumerable<CommunicationLog> communicationLogs)
        {
            List<CommunicationLogViewModel> result = new List<CommunicationLogViewModel>();

            var organizations = OrganizationBL.GetByOrganizationIDs(uow, communicationLogs.Where(cl => cl.OrganizationID.HasValue).Select(cl => (int)cl.OrganizationID)).ToList<Organization>();
            var employees = EmployeeBL.GetByIDs(uow, communicationLogs.Where(cl => cl.EmployeeID.HasValue).Select(cl => (int)cl.EmployeeID)).ToList<Employee>();

            foreach (CommunicationLog communicationLog in communicationLogs)
            {
                result.Add(CommunicationLogViewBL.FromModel(uow, communicationLog, organizations, employees));
            }
            return result;
        }

        public static CommunicationLogViewModel FromModel(IUnitOfWork uow, CommunicationLog communicationLog, List<Organization> organizations = null, List<Employee> employees = null)
        {
            var communicationLogViewModel = new CommunicationLogViewModel() { CommunicationLog = communicationLog };

            communicationLogViewModel.CommunicationLogType = communicationLog.MailType ?? CommunicationLogType.None;
            if (communicationLogViewModel.CommunicationLogType == CommunicationLogType.None && !String.IsNullOrEmpty(communicationLog.Description))
            {
                if (communicationLog.Description.Equals("Zorgmail.SendMedvri()", StringComparison.InvariantCultureIgnoreCase))
                {
                    communicationLogViewModel.CommunicationLogType = CommunicationLogType.ZorgmailMedvri;
                }
                else if (communicationLog.Description.Equals("Zorgmail.SendPDF()", StringComparison.InvariantCultureIgnoreCase))
                {
                    communicationLogViewModel.CommunicationLogType = CommunicationLogType.ZorgmailSendPDF;
                }
            }

            Organization organization = null;
            if (communicationLog.OrganizationID.HasValue)
            {
                if (organizations != null)
                {
                    organization = organizations.FirstOrDefault(org => org.OrganizationID == communicationLog.OrganizationID.Value);
                }
                else
                {
                    organization = OrganizationBL.GetByOrganizationID(uow, communicationLog.OrganizationID.Value);
                }
            }
            if (organization != null)
            {
                communicationLogViewModel.OrganizationName = organization.Name;
            }


            Employee employee = null;
            if (communicationLog.EmployeeID.HasValue)
            {
                if (employees != null)
                {
                    employee = employees.FirstOrDefault(em => em.EmployeeID == communicationLog.EmployeeID.Value);
                }
                else
                {
                    employee = EmployeeBL.GetByID(uow, communicationLog.EmployeeID.Value);
                }
            }
            if (employee != null)
            { 
                communicationLogViewModel.EmployeeName = employee.FullName();
            }

            return communicationLogViewModel;
        }
    }
}
