﻿using System.Collections.Generic;
using System.Linq;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;

namespace Point.Business.Logic
{
    public class DoctorBL
    {
        public static Doctor GetByID(IUnitOfWork uow, int doctorID)
        {
            return uow.DoctorRepository.Get(doc => doc.DoctorID == doctorID, includeProperties: "Organization,DoctorLocation").FirstOrDefault();
        }

        public static void DeleteByID(IUnitOfWork uow, int doctorID)
        {
            var doctor = GetByID(uow, doctorID);
            doctor.DoctorLocation.Clear();
            doctor.Inactive = true;

            uow.Save();
        }

        public static Doctor Save(IUnitOfWork uow, DoctorViewModel viewmodel)
        {
            var model = GetByID(uow, viewmodel.DoctorID) ?? new Doctor { OrganizationID = viewmodel.OrganizationID };

            if(model.DoctorID == 0)
                uow.DoctorRepository.Insert(model);

            model.Name = viewmodel.Name;
            model.SearchName = viewmodel.SearchName;
            model.SpecialismID = viewmodel.SpecialismID;
            model.AGB = viewmodel.AGB;
            model.BIG = viewmodel.BIG;
            model.PhoneNumber = viewmodel.PhoneNumber;

            model.DoctorLocation.Clear();

            if (viewmodel.LocationIDS != null)
            {
                foreach (var locationid in viewmodel.LocationIDS)
                {
                    model.DoctorLocation.Add(new DoctorLocation() {LocationID = locationid});
                }
            }

            uow.Save();

            return model;
        }

        public static IQueryable<Doctor> GetByOrganizationID(IUnitOfWork uow, int organizationID)
        {
            return uow.DoctorRepository.Get(doc => doc.OrganizationID == organizationID && (doc.Inactive == false), includeProperties: "Specialism").OrderBy(doc => doc.SearchName);
        }
    }
}
