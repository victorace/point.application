﻿using System;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Extensions;
using Point.Models.Enums;

namespace Point.Business.Logic
{
    public static class ClientViewBL
    {
        public static ClientViewModel FromModel(IUnitOfWork uow, Client client, PointUserInfo userinfo, FlowDefinitionID? flowdefinitionid)
        {
            if (client == null || client == default(Client))
                return new ClientViewModel();

            var clientFullViewModel = new ClientViewModel();
            clientFullViewModel.Merge<ClientViewModel>(client);

            var flowinstance = FlowInstanceBL.GetByClientID(uow, client.ClientID);
            clientFullViewModel.ClientShowAnonymous = ClientBL.ShowAnonymous(flowinstance, userinfo.Organization?.OrganizationID);

            if (clientFullViewModel.ContactPerson == null)
                clientFullViewModel.ContactPerson = new ContactPersonViewModel();

            clientFullViewModel.ContactPerson.LastName = client.ContactPersonName;
            Int32 relationType;
            if (Int32.TryParse(client.ContactPersonRelationType, out relationType))
            {
                clientFullViewModel.ContactPerson.CIZRelationID = relationType;
            }
            clientFullViewModel.ContactPerson.PhoneNumberGeneral = client.ContactPersonPhoneNumberGeneral;
            clientFullViewModel.ContactPerson.PhoneNumberMobile = client.ContactPersonPhoneNumberMobile;
            clientFullViewModel.ContactPerson.PhoneNumberWork = client.ContactPersonPhoneWork;
            clientFullViewModel.ShowSearchBSN = flowdefinitionid != FlowDefinitionID.HA_VVT;
            clientFullViewModel.ShowSearchComplete = !clientFullViewModel.ShowSearchBSN;

            return clientFullViewModel;
        }
    }
}
