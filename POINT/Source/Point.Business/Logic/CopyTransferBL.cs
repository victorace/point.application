﻿using Point.Database.Extensions;
using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using Point.Database.CacheModels;

namespace Point.Business.Logic
{
    public static class CopyTransferBL
    {
        public static IEnumerable<TransferMemoTypeID> CopyableTransferMemoTargets = new List<TransferMemoTypeID>
        {
            TransferMemoTypeID.Zorgprofessional,
            TransferMemoTypeID.Transferpunt,
            TransferMemoTypeID.Patient
        };

        public static IEnumerable<TransferMemoTypeID> CopyableAdditionalInfoRequestTPTransferMemoTargets = new List<TransferMemoTypeID>
        {
            TransferMemoTypeID.AanvullendeGegevensBevindingenMedewerkerTransferpunt,
            TransferMemoTypeID.AanvullendeGegevensAfspraken,
            TransferMemoTypeID.IntercollegiateCommunications
        };

        public static Transfer Copy(IUnitOfWork uow, CopyTransferViewModel copytransferviewmodel, PointUserInfo pointuserinfo, LogEntry logentry)
        {
            if (copytransferviewmodel == null)
            {
                return null;
            }

            var transferid = copytransferviewmodel.TransferID;
            var flowinstance = FlowInstanceBL.GetByTransferID(uow, transferid);
            if (flowinstance == null)
            {
                return null;
            }

            if (copytransferviewmodel.CopyTransferForms.Any())
            {
                var vm = CopyTransferViewBL.Get(uow, flowinstance);
                copytransferviewmodel.CopyTransferForms = copytransferviewmodel.CopyTransferForms
                    .Intersect(vm.CopyTransferForms, (x, y) => x.FormSetVersionID == y.FormSetVersionID).ToList();
            }

            var flowinstancecopy = FlowInstanceBL.Copy(uow, flowinstance, logentry);
            var copiedtransferid = flowinstancecopy.TransferID;

            FlowInstanceSearchValuesBL.Create(uow, flowinstancecopy.FlowInstanceID, copiedtransferid, flowinstance.Transfer.Client.FullName(), flowinstancecopy.StartedByDepartmentID);
            uow.Save(); 

            copyPhaseInstances(uow, flowinstance, flowinstancecopy, logentry, pointuserinfo);

            copyDossierForms(uow, flowinstance, flowinstancecopy, copytransferviewmodel.CopyTransferForms.Select(ctf => ctf.FormSetVersionID), logentry, pointuserinfo);

            createTransferAttachments(uow, flowinstance, flowinstancecopy, logentry);

            if (copytransferviewmodel.CopyTransferMemos)
            {
                TransferMemoBL.CopyTransferMemos(uow, flowinstance.Transfer, flowinstancecopy.Transfer, CopyableTransferMemoTargets);
            }

            if (copytransferviewmodel.CopyTransferAttachments)
            {
                TransferAttachmentBL.CopyToTransfer(uow, flowinstance.Transfer, flowinstancecopy.Transfer);
            }

            if (copytransferviewmodel.CopyDossierTags)
            {
                FlowInstanceTagBL.CopyToFlowInstance(uow, flowinstance.FlowInstanceID, flowinstancecopy.FlowInstanceID);
            }

            uow.Save();
            return flowinstancecopy?.Transfer;
        }

        public static PhaseDefinitionCache GetStopAtPhase(IUnitOfWork uow, FlowDefinitionID flowDefinitionID)
        {
            var phaseDefinitions = PhaseDefinitionCacheBL.GetByFlowDefinitionID(uow, flowDefinitionID);
            var phaseDefinition = phaseDefinitions.FirstOrDefault(pd => pd.BeginFlow);
            if (phaseDefinition == null)
            {
                throw new Exception("No BeginFlow PhaseDefinition.");
            }

            var formType = (FlowFormType)phaseDefinition.FormTypeID;

            switch (flowDefinitionID)
            {
                case FlowDefinitionID.VVT_ZH :
                    formType = FlowFormType.GegevensPatient;
                    break;
                case FlowDefinitionID.ZH_VVT :
                    formType = FlowFormType.InBehandelingTP;
                    break;
                case FlowDefinitionID.RzTP :
                    formType = FlowFormType.AanvraagFormulierRzTP;
                    break;
                case FlowDefinitionID.CVA :
                    formType = FlowFormType.CVARouteSelection;
                    break;
                case FlowDefinitionID.ZH_ZH :
                    formType = FlowFormType.AanvraagFormulierZHZH;
                    break;
                case FlowDefinitionID.HA_VVT :
                    formType = FlowFormType.RouterenDossierZHVVT;
                    break;
                case FlowDefinitionID.VVT_VVT :
                    formType = FlowFormType.AanvraagFormulierZHVVT;
                    break;
                default:
                    // FormType from PhaseDefinition with BeginFlow == true
                    break;
            }

            phaseDefinition = phaseDefinitions.FirstOrDefault(pd => pd.FormTypeID == (int)formType);
            if (phaseDefinition == null)
            {
                throw new Exception("No PhaseDefinition found.");
            }
            return phaseDefinition;
        }

        private static void createTransferAttachments(IUnitOfWork uow, FlowInstance source, FlowInstance destination, LogEntry logentry)
        {
            TransferAttachmentBL.InsertFormAsPDFAndSave(uow, source.TransferID, destination.TransferID,
                new List<int> { (int)FlowFormType.VerpleegkundigeOverdrachtFlow }, logentry);
        }

        private static void copyDossierForms(IUnitOfWork uow, FlowInstance source, FlowInstance destination, IEnumerable<int> formsetversionids,
            LogEntry logentry, PointUserInfo pointuserinfo)
        {
            if (formsetversionids != null)
            {
                var formsetversions = FormSetVersionBL.GetByTransferID(uow, source.TransferID)
                    .Where(fsv => formsetversionids.Contains(fsv.FormSetVersionID)).ToList();
                foreach (var sourceformsetversion in formsetversions)
                {
                    var phaseinstance = PhaseInstanceBL.Create(uow, destination.FlowInstanceID,
                        sourceformsetversion.PhaseInstance.PhaseDefinition.PhaseDefinitionID,
                        logentry, (Status)sourceformsetversion.PhaseInstance.Status);

                    var destinationformsetversion = FormSetVersionBL.CreateFormSetVersion(uow, destination.TransferID,
                        sourceformsetversion.FormTypeID, phaseinstance.PhaseInstanceID,
                        pointuserinfo, logentry, sourceformsetversion.Description);

                    FlowFieldValueBL.CopyTo(uow, sourceformsetversion, destinationformsetversion);

                    if (sourceformsetversion.FormTypeID == (int)FlowFormType.MSVTUitvoeringsverzoek || sourceformsetversion.FormTypeID == (int)FlowFormType.MSVTIndicatiestelling)
                    {
                        ActionHealthInsurerBL.CopyTo(uow, sourceformsetversion, destinationformsetversion);
                        MedicineCardBL.CopyTo(uow, sourceformsetversion, destinationformsetversion);
                    }
                    else if (sourceformsetversion.FormTypeID == (int)FlowFormType.ZorgAdvies)
                    {
                        handleZorgAdviesToevoegen(uow, source, destination, logentry, pointuserinfo);
                        ZorgAdviesItemValueBL.CopyTo(uow, source, destination);
                    }
                    else if (sourceformsetversion.FormTypeID == (int)FlowFormType.AanvullendeGegevensTPZHVVT)
                    {
                        TransferMemoBL.CopyTransferMemos(uow, source.Transfer, destination.Transfer, CopyableAdditionalInfoRequestTPTransferMemoTargets);
                    }
                }
            }
        }

        private static void handleZorgAdviesToevoegen(IUnitOfWork uow, FlowInstance source, FlowInstance destination,
            LogEntry logentry, PointUserInfo pointuserinfo)
        {
            var aanvullendegegevenstp = PhaseDefinitionCacheBL.GetByFlowDefinitionAndFormType(uow, source.FlowDefinitionID, (int)FlowFormType.AanvullendeGegevensTPZHVVT);
            if (aanvullendegegevenstp == null)
            {
                return;
            }

            var phaseInstance = PhaseInstanceBL.Create(uow, destination.FlowInstanceID, aanvullendegegevenstp.PhaseDefinitionID, logentry);

            var formsetversion = FormSetVersionBL.CreateFormSetVersion(uow, destination.TransferID,
                (int)FlowFormType.AanvullendeGegevensTPZHVVT, phaseInstance.PhaseInstanceID, pointuserinfo, logentry);
            var flowfieldvalue = FlowFieldValueBL.UpdateOrInsert(uow, destination.FlowInstanceID, formsetversion.FormSetVersionID,
                FlowFieldConstants.ID_ZorgAdviesToevoegen, FlowFieldConstants.Value_Ja, logentry);

            LoggingBL.FillLogging(uow, flowfieldvalue, logentry);

            uow.Save();
        }

        private static void copyPhaseInstances(IUnitOfWork uow, FlowInstance source, FlowInstance destination,
            LogEntry logentry, PointUserInfo pointuserinfo)
        {
            // Create PhaseInstances UPTO AND INCLUDING a Phase (different per FlowDefinition)
            var stopAtPhaseDefinition = GetStopAtPhase(uow, source.FlowDefinitionID);
            var stopAtPhase = stopAtPhaseDefinition.Phase;

            var phaseDefinitionBeginFlow = PhaseDefinitionCacheBL.GetPhaseDefinitionBeginFlow(uow, destination.FlowDefinitionID);

            var beginPhaseStatus = phaseDefinitionBeginFlow.Phase == stopAtPhase ? Status.Active : Status.Done;
            var phaseInstance = PhaseInstanceBL.Create(uow, destination.FlowInstanceID, phaseDefinitionBeginFlow.PhaseDefinitionID, logentry, beginPhaseStatus);

            var lastPhaseInstance = PhaseInstanceBL
                .GetByFlowInstanceId(uow, source.FlowInstanceID)
                .LastOrDefault(pi => pi.PhaseDefinition.Phase >= 0);

            if (phaseDefinitionBeginFlow != null)
            {
                var nextPhaseDefintions = PhaseDefinitionBL.GetNextPhaseDefinitions(uow, phaseDefinitionBeginFlow.PhaseDefinitionID)
                    .Where(pd => lastPhaseInstance != null && (pd.Phase <= lastPhaseInstance.PhaseDefinition.Phase && pd.Phase <= stopAtPhase)).ToList();

                var hasMorePhaseDefinitions = nextPhaseDefintions.Any();
                while (hasMorePhaseDefinitions)
                {
                    foreach (var nextPhaseDefintion in nextPhaseDefintions)
                    {
                        phaseInstance = PhaseInstanceBL.HandleNextPhaseWithoutGeneralAction(uow,
                            destination.FlowInstanceID,
                            phaseInstance.PhaseInstanceID,
                            nextPhaseDefintion.PhaseDefinitionID,
                            logentry);

                        // copy FlowFieldValues from PhaseDefinition.FormTypes UPTO Phase (the last phase will be 
                        // active but no FlowFieldValues will have been copied)
                        if (nextPhaseDefintion.Phase < stopAtPhase)
                        {
                            copyFlowFieldValues(uow, source, destination, phaseInstance, nextPhaseDefintion, logentry, pointuserinfo);
                        }
                    }

                    if (phaseInstance != null)
                    {
                        nextPhaseDefintions = PhaseDefinitionBL.GetNextPhaseDefinitions(uow, phaseInstance.PhaseDefinitionID)
                            .Where(pd => lastPhaseInstance != null && pd.Phase <= lastPhaseInstance.PhaseDefinition.Phase && pd.Phase <= stopAtPhase).ToList();
                        hasMorePhaseDefinitions = nextPhaseDefintions.Any();
                    }
                }
            }
        }

        private static void copyFlowFieldValues(IUnitOfWork uow, FlowInstance source, FlowInstance destination, PhaseInstance phaseInstance,
            PhaseDefinition nextPhaseDefintion, LogEntry logentry, PointUserInfo pointuserinfo)
        {
            var formSetVersionSource =
                FormSetVersionBL.GetByTransferIDAndFormTypeID(uow, source.TransferID, nextPhaseDefintion.FormTypeID)
                    .OrderByDescending(fsv => fsv.CreateDate)
                    .FirstOrDefault();
            if (formSetVersionSource == null)
            {
                return;
            }

            var formSetVersionDestination = FormSetVersionBL.CreateFormSetVersion(uow, destination.TransferID,
                nextPhaseDefintion.FormTypeID, phaseInstance.PhaseInstanceID, pointuserinfo, logentry,
                formSetVersionSource.Description);
            FlowFieldValueBL.CopyTo(uow, formSetVersionSource, formSetVersionDestination);
        }
    }
}
