﻿using Newtonsoft.Json;
using Point.Database.Models;
using Point.Database.Repository;
using Point.Infrastructure.Helpers;
using Point.ServiceBus.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Point.Business.Logic
{
    public class ServiceBusLogBL
    {
        public static void Add(IUnitOfWork uow, IPointServiceBusModel message, string queue, string label)
        {
            var loggingmodel = message as ILoggingServiceBusModel;
            if (loggingmodel != null)
            {
                var json = JsonConvert.SerializeObject(message);
                var servicebuslog = new ServiceBusLog {
                    ObjectData = json,
                    ObjectType = message.GetType().Name,
                    SentDateTime = loggingmodel.TimeStamp,
                    Queue = queue,
                    Label = label,
                    UniqueID = loggingmodel.UniqueID
                };
                uow.ServiceBusLogRepository.Insert(servicebuslog);
                uow.Save();
            }
        }

        private static void SetExpired(IUnitOfWork uow)
        {
            int expirehours = ConfigHelper.GetAppSettingByName<int>("ServiceBusExpireHours", 6);
            var maxdate = DateTime.Now.AddHours(0 - expirehours);
            
            var itemstoexpire = uow.ServiceBusLogRepository.Get(it => !it.Processed && !it.Expired && it.SentDateTime < maxdate).ToList();
            itemstoexpire.ForEach(it => it.Expired = true);
            uow.Save();
        }

        public static IEnumerable<ServiceBusLog> GetExpiredItems(IUnitOfWork uow)
        {
            SetExpired(uow);

            int alertminutes = ConfigHelper.GetAppSettingByName<int>("ServiceBusAlertMinutes", 5);
            var maxdate = DateTime.Now.AddMinutes(0 - alertminutes);

            return uow.ServiceBusLogRepository.Get(it => !it.Processed && !it.Expired && it.SentDateTime < maxdate);
        }

        public static void CompleteByUniqueID(IUnitOfWork uow, long uniqueid)
        {
            var currentitem = uow.ServiceBusLogRepository.Get(it => it.UniqueID == uniqueid).FirstOrDefault();
            if (currentitem != null)
            {
                currentitem.Processed = true;
                currentitem.ProcessedDateTime = DateTime.Now;
                uow.Save();
            }
        }
    }
}
