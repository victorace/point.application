﻿using System.Linq;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;

namespace Point.Business.Logic
{
    public static class PersonDataBL
    {
        public static PersonData GetByID(IUnitOfWork uow, int persondataid)
        {
            return uow.PersonDataRepository.GetByID(persondataid);
        }

		public static PersonData FromModel(IUnitOfWork uow, ContactPersonViewModel contactPersonViewModel)
        {
            var persondata = GetByID(uow, contactPersonViewModel.PersonDataID);
            if(persondata == null)
            {
                persondata = new PersonData();
                uow.PersonDataRepository.Insert(persondata);
            }

            persondata.Gender = contactPersonViewModel.Gender;
            persondata.Initials = contactPersonViewModel.Initials;
            persondata.FirstName = contactPersonViewModel.FirstName;
            persondata.LastName = contactPersonViewModel.LastName;
            persondata.PhoneNumberGeneral = contactPersonViewModel.PhoneNumberGeneral;
            persondata.PhoneNumberMobile = contactPersonViewModel.PhoneNumberMobile;
            persondata.PhoneNumberWork = contactPersonViewModel.PhoneNumberWork;
            persondata.Email = contactPersonViewModel.Email;

            return persondata;
        }

        public static PersonData Copy(IUnitOfWork uow, PersonData source)
        {
            var personcopy =  uow.PersonDataRepository.Get(pd => pd.PersonDataID == source.PersonDataID, asNoTracking: true).FirstOrDefault();
            personcopy.PersonDataID = 0;
            uow.PersonDataRepository.Insert(personcopy);

            return personcopy;
        }
    }
}
