﻿using System;
using System.Linq;
using Point.Business.Helper;
using Point.Database.Models;
using Point.Database.Repository;
using Point.Infrastructure.Caching;

namespace Point.Business.Logic
{
    public class TransferBL
    {
        public static Transfer GetByTransferID(IUnitOfWork uow, int transferID)
        {
            return uow.TransferRepository.GetByID(transferID);
        }

        public static Transfer GetByClientID(IUnitOfWork uow, int clientID)
        {
            return uow.TransferRepository.Get(tr => tr.ClientID == clientID).FirstOrDefault();
        }

        public static Transfer GetCachedByTransferID(IUnitOfWork uow, int transferID)
        {
            var cachestring = String.Concat("CachedTransfer_", transferID);
            var transfer = CacheService.Instance.Get<Transfer>(cachestring);
            if (transfer != null) return transfer;

            transfer = GetByTransferID(uow, transferID);
            if(transfer != null)
                CacheService.Instance.Insert(cachestring, transfer, "CacheShort");

            return transfer;
        }

        public static Transfer Generate(IUnitOfWork uow, int clientID, LogEntry logentry)
        {
            var transfer = new Transfer()
            {
                ClientID = clientID,
            };

            LoggingBL.FillLoggingWithID(uow, transfer, logentry);

            Save(uow, transfer);

            return transfer;
        }

        public static int GetPageLockTimeout(IUnitOfWork uow, int transferID)
        {
            var flowinstance = FlowInstanceBL.GetByTransferID(uow, transferID);
            return flowinstance.StartedByOrganization.PageLockTimeout ?? 0;
        }

        public static Transfer Copy(IUnitOfWork uow, Transfer transfer)
        {
            var pointUserInfo = PointUserInfoHelper.GetPointUserInfo(uow);

            var clientCopy = ClientBL.Copy(uow, transfer.Client, pointUserInfo);

            ContactPersonBL.CopyTo(uow, transfer.Client, clientCopy);

            var transferCopy = uow.TransferRepository.Get(t => t.TransferID == transfer.TransferID, asNoTracking: true).FirstOrDefault();
            if (transferCopy == null) return null;

            transferCopy.TransferID = 0;
            transferCopy.ClientID = clientCopy.ClientID;
            transferCopy.OriginalTransferID = transfer.TransferID;

            uow.TransferRepository.Insert(transferCopy);
            Save(uow, transferCopy);

            return transferCopy;
        }

        private static void Upsert(IUnitOfWork uow, Transfer instance)
        {
            if (instance.TransferID == 0)
            {
                uow.TransferRepository.Insert(instance);
            }
        }

        public static void Save(IUnitOfWork uow, Transfer instance)
        {
            Upsert(uow, instance);

            uow.Save();
        }
    }
}
