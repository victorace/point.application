﻿using Point.Database.Models;
using Point.Database.Repository;
using Point.Infrastructure.Caching;
using Point.Models.Enums;
using System.Collections.Generic;
using System.Linq;

namespace Point.Business.Logic
{
    public class FlowFieldAttributeBL
    {
        public static List<FlowFieldAttribute> GetAllCached(IUnitOfWork uow)
        {
            string cachestring = "GetFlowFieldAttributes_Global";

            List<FlowFieldAttribute> flowfieldattributes = CacheService.Instance.Get<List<FlowFieldAttribute>>(cachestring);

            if (flowfieldattributes == null)
            {
                flowfieldattributes = uow.FlowFieldAttributeRepository.Get(includeProperties: "FlowField,FlowFieldAttributeException,RequiredByFlowField").ToList();

                if (flowfieldattributes.Any())
                {
                    CacheService.Instance.Insert(cachestring, flowfieldattributes, "CacheLong");
                }
            }
            return flowfieldattributes;
        }

        public static FlowFieldAttribute GetByID(IUnitOfWork uow, int id)
        {
            var flowFieldAttribute = (from f in GetAllCached(uow)
                                      where f.FlowFieldAttributeID == id
                                      select f).FirstOrDefault();

            return flowFieldAttribute;
        }

        public static FlowFieldAttribute GetByName(IUnitOfWork uow, string name)
        {
            var flowFieldAttribute = (from f in GetAllCached(uow)
                                      where f.FlowField.Name == name
                                      select f).FirstOrDefault();

            return flowFieldAttribute;
        }

        public static FlowFieldAttribute GetFirstByFlowFieldID(IUnitOfWork uow, int flowFieldID)
        {
            return GetByFlowFieldID(uow, flowFieldID).FirstOrDefault();
        }

        public static IEnumerable<FlowFieldAttribute> GetByFlowFieldIDs(IUnitOfWork uow, IEnumerable<int> flowFieldIDs)
        {
            return GetAllCached(uow).Where(ffa => flowFieldIDs.Contains(ffa.FlowFieldID));
        }

        public static IEnumerable<FlowFieldAttribute> GetReadModelAttributes(IUnitOfWork uow, FlowDefinitionID flowdefinitionid)
        {
            string cachestring = "GetReadModelFlowFieldAttributes_ByFlowDefinitionID" + flowdefinitionid;
            List<FlowFieldAttribute> flowfieldattributes = CacheService.Instance.Get<List<FlowFieldAttribute>>(cachestring);
            if (flowfieldattributes == null)
            {
                flowfieldattributes = uow.FlowFieldAttributeRepository.Get(ffa => ffa.FlowField.UseInReadModel && ffa.FormType.PhaseDefinition.Any(pd => pd.FlowDefinitionID == flowdefinitionid), includeProperties: "FlowField,FormType").ToList();
                if (flowfieldattributes.Any())
                {
                    CacheService.Instance.Insert(cachestring, flowfieldattributes, "CacheLong");
                }
            }

            return flowfieldattributes;
        }

        public static IEnumerable<FlowFieldAttribute> GetByFlowDefinitionID(IUnitOfWork uow, FlowDefinitionID flowdefinitionid)
        {
            string cachestring = "GetFlowFieldAttributes_ByFlowDefinitionID" + flowdefinitionid;
            List<FlowFieldAttribute> flowfieldattributes = CacheService.Instance.Get<List<FlowFieldAttribute>>(cachestring);
            if(flowfieldattributes == null)
            {
                flowfieldattributes = uow.FlowFieldAttributeRepository.Get(ffa => ffa.FormType.PhaseDefinition.Any(pd => pd.FlowDefinitionID == flowdefinitionid), includeProperties: "FlowField,FormType").ToList();
                if (flowfieldattributes.Any())
                {
                    CacheService.Instance.Insert(cachestring, flowfieldattributes, "CacheLong");
                }
            }

            return flowfieldattributes;
        }

        public static IEnumerable<FlowFieldAttribute> GetByFlowFieldIDsAndFormTypeID(IUnitOfWork uow, IEnumerable<int> flowFieldIDs, int formTypeID)
        {
            return GetAllCached(uow).Where(ffa => flowFieldIDs.Contains(ffa.FlowFieldID) && ffa.FormTypeID == formTypeID);
        }

        public static IEnumerable<FlowFieldAttribute> GetByFormTypeID(IUnitOfWork uow, int FormTypeID)
        {
            return GetAllCached(uow).Where(ffa => ffa.FormTypeID == FormTypeID && ffa.IsActive);
        }

        public static IEnumerable<FlowFieldAttribute> GetByFlowFieldID(IUnitOfWork uow, int flowFieldID)
        {
            return GetAllCached(uow).Where(ffa => ffa.FlowFieldID == flowFieldID && ffa.IsActive);
        }

        public static bool IsVisible(IUnitOfWork uow, int regionid, int organizationid, int formtypeid, int flowfieldid)
        {
            var flowfieldattribute = GetAllCached(uow).FirstOrDefault(ffa => ffa.FlowFieldID == flowfieldid && ffa.FormTypeID == formtypeid);

            if (flowfieldattribute != null && !flowfieldattribute.FlowFieldAttributeException.Any())
            {
                return flowfieldattribute.Visible;
            }

            var flowfieldattributeexception = flowfieldattribute?.FlowFieldAttributeException.FirstOrDefault(ffae => ffae.RegionID == regionid || ffae.OrganizationID == organizationid);
            if (flowfieldattributeexception != null)
            {
                return flowfieldattributeexception.Visible;
            }

            return flowfieldattribute != null && flowfieldattribute.Visible;
        }
    }
}
