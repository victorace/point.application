﻿using Point.Business.Helper;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Extensions;
using Point.Models.Enums;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Point.Business.Logic
{
    public class PhaseInstanceDetails
    {
        public int FlowInstanceID { get; set; }
        public int FormTypeID { get; set; }
        public Status Status { get; set; }
        public DateTime StartProcessDate { get; set; }
        public DateTime? RealizedDate { get; set; }
        public DateTime? LastChangeDate { get; set; }
        public FlowHandling? FlowHandling { get; set; }
    }

    public static class PhaseInstanceBL
    {
        public static List<PhaseInstanceDetails> GetCurrentPhaseInstanceDetailsWithHandling(IUnitOfWork uow, int flowInstanceID)
        {
            return uow.PhaseInstanceRepository.Get(ph => 
                ph.FlowInstanceID == flowInstanceID && 
                ph.PhaseDefinition.FlowHandling != null && 
                ph.Cancelled != true)
                .Select(ph =>
                    new PhaseInstanceDetails()
                    {
                        FlowInstanceID = ph.FlowInstanceID,
                        FormTypeID = ph.PhaseDefinition.FormTypeID,
                        Status = (Status)ph.Status,
                        StartProcessDate = ph.StartProcessDate,
                        RealizedDate = ph.RealizedDate,
                        LastChangeDate = ph.FormSetVersion.Max(fs => fs.UpdateDate),
                        FlowHandling = ph.PhaseDefinition.FlowHandling
                    })
                .ToList();
        }

        public static PhaseInstance GetVoPhaseInstance(IUnitOfWork uow, FlowInstance flowinstance)
        {
            var voPhaseDefinition = PhaseDefinitionCacheBL.GetByFlowDefinitionAndFormType(uow, flowinstance.FlowDefinitionID, (int)FlowFormType.VerpleegkundigeOverdrachtFlow);
            if (voPhaseDefinition == null)
                return null;

            var voPhaseInstance = PhaseInstanceBL.GetByFlowInstanceIDAndPhaseDefinition(uow, flowinstance.FlowInstanceID, voPhaseDefinition.PhaseDefinitionID);

            return voPhaseInstance;
        }

        public static int CleanupDoublePhases(IUnitOfWork uow, int? transferid = null)
        {
            uow.DatabaseConfiguration.AutoDetectChangesEnabled = false;
            //int[] closedformtypestofix = new int[] { (int)FlowFormType.RouterenDossierZHVVT, (int)FlowFormType.RouterenDossierRzTP, (int)FlowFormType.AfsluitenDossierRzTP, (int)FlowFormType.AfsluitenDossier };
            var flowinstances =
                uow.FlowInstanceRepository.Get(fi => (transferid == null || fi.TransferID == transferid) &&

                                                     //&& closedformtypestofix.Contains(pi.PhaseDefinition.FormTypeID)
                                                     fi.PhaseInstances.Where(
                                                         pi => !pi.Cancelled && pi.PhaseDefinition.Phase >= 0)
                                                         .GroupBy(pi => pi.PhaseDefinitionID)
                                                         .Any(grp => grp.Count() > 1)
                    , includeProperties: "PhaseInstances.PhaseDefinition, PhaseInstances.FormSetVersion").ToList();
            foreach (var flowinstance in flowinstances)
            {
                var multiplephasegroups =
                    flowinstance.PhaseInstances.Where(pi => !pi.Cancelled && pi.PhaseDefinition.Phase >= 0)
                        .GroupBy(pi => pi.PhaseDefinitionID)
                        .Where(grp => grp.Count() > 1);
                foreach (var multiphase in multiplephasegroups)
                {
                    //int phasedefinitionid = multiphase.Key;

                    var phaseinstanceidwithformsetversion =
                        multiphase.SelectMany(pi => pi.FormSetVersion)
                            .OrderByDescending(pi => pi.PhaseInstance.Status)
                            .ThenByDescending(fs => fs.FormSetVersionID)
                            .FirstOrDefault();
                    var phaseinstanceidtoexclude = phaseinstanceidwithformsetversion != null
                        ? phaseinstanceidwithformsetversion.PhaseInstanceID
                        : multiphase.Select(pi => pi.PhaseInstanceID).LastOrDefault();
                    var tocancel = multiphase.Where(pi => phaseinstanceidtoexclude != pi.PhaseInstanceID).ToList();
                    foreach (var item in tocancel)
                    {
                        // TODO: #15466 Technical Debt: Het cancellen van PhaseInstances zou ook cancellen van formsetversions moeten zijn.
                        item.Cancelled = true;
                        uow.DatabaseContext.Entry(item).State = EntityState.Modified;
                    }
                }
            }

            uow.Save();
            uow.DatabaseConfiguration.AutoDetectChangesEnabled = true;

            return flowinstances.Count;
        }

        public static IEnumerable<PhaseInstance> GetByFlowInstanceAndPhase(IUnitOfWork uow, int flowinstanceID, decimal phase, bool includeInactive = false)
        {
            if (includeInactive)
            {
                return uow.PhaseInstanceRepository.Get(pi => pi.FlowInstanceID == flowinstanceID && pi.PhaseDefinition.Phase == phase);
            }
            return uow.PhaseInstanceRepository.Get(pi => pi.FlowInstanceID == flowinstanceID && pi.PhaseDefinition.Phase == phase && pi.Cancelled == includeInactive);
        }

        public static PhaseInstance GetByFlowInstanceIDAndFormTypeID(IUnitOfWork uow, int flowinstanceid, int formtypeid)
        {
            return GetAllByFlowInstanceIDAndFormTypeID(uow, flowinstanceid, formtypeid).FirstOrDefault();
        }

        public static IEnumerable<PhaseInstance> GetAllByFlowInstanceIDAndFormTypeID(IUnitOfWork uow, int flowinstanceid, int formtypeid, bool includeInactive = false)
        {
            return uow.PhaseInstanceRepository.Get(pi => pi.FlowInstanceID == flowinstanceid
                && pi.PhaseDefinition.FormTypeID == formtypeid
                && (includeInactive || !(bool)pi.Cancelled));
        }

        public static PhaseInstance GetByTransferIDAndFormTypeID(IUnitOfWork uow, int transferID, int formtypeid)
        {
            return uow.PhaseInstanceRepository.Get(pi =>
                pi.FlowInstance.TransferID == transferID &&
                !pi.Cancelled &&
                pi.PhaseDefinition.FormTypeID == formtypeid
                ).FirstOrDefault();
        }

        public static PhaseInstance GetByTransferIDAndControllerAndAction(IUnitOfWork uow, int transferID, string controller, string action)
        {
            var formtype = FormTypeBL.GetByURL(uow, controller, action);
            if (formtype == null) return null;

            return GetByTransferIDAndFormTypeID(uow, transferID, formtype.FormTypeID);
        }

        public static PhaseInstance GetByTransferIDAndControllerAndAction(IUnitOfWork uow, GlobalProperties globalProps)
        {
            return GetByTransferIDAndControllerAndAction(uow, globalProps.TransferID, globalProps.ControllerName, globalProps.ActionName);
        }

        public static IEnumerable<PhaseInstance> GetCurrentPhaseInstances(IUnitOfWork uow, int flowinstanceID, FlowDefinitionID flowdefinitionID)
        {
            var phaseinstances = GetByFlowInstanceId(uow, flowinstanceID);
            return phaseinstances.Where(pi => pi.Status == (int) Status.Active).OrderByDescending(pi => pi.Timestamp);
        }

        public static PhaseInstance GetByFormsetVersionID(IUnitOfWork uow, int formsetVersionID)
        {
            var formsetversion = uow.FormSetVersionRepository.Get(fs => fs.FormSetVersionID == formsetVersionID, null, "PhaseInstance").FirstOrDefault();
            return formsetversion?.PhaseInstance;
        }

        public static PhaseInstance SetStatusDone(IUnitOfWork uow, PhaseInstance phaseInstance, LogEntry logentry)
        {
            phaseInstance.Status = (int)Status.Done;
            phaseInstance.RealizedDate = DateTime.Now;
            phaseInstance.RealizedByEmployeeID = logentry.EmployeeID;

            LoggingBL.FillLogging(uow, phaseInstance, logentry);

            Save(uow, phaseInstance);

            return phaseInstance; 
        }

        public static PhaseInstance SetStatusActive(IUnitOfWork uow, PhaseInstance phaseInstance, LogEntry logentry)
        {
            phaseInstance.Status = (int)Status.Active;
            phaseInstance.RealizedDate = null;
            phaseInstance.RealizedByEmployeeID = null;

            LoggingBL.FillLogging(uow, phaseInstance, logentry);

            Save(uow, phaseInstance);

            return phaseInstance;
        }

        public static bool HasEndFlowRights(IUnitOfWork uow, FlowDefinitionID flowdefinitionID, string controllerName,
            string actionName, PointUserInfo userinfo)
        {
            var phaseDefinition = PhaseDefinitionCacheBL.GetEndFlowPhaseDefinition(uow, flowdefinitionID);

            var generalaction = GeneralActionBL.GetByURL(uow, controllerName, actionName);
            if(generalaction == null)
            {
                return false;
            }

            Enum.TryParse(generalaction.Rights, out Rights generalactionRights);

            var phaseRights = PhaseDefinitionRightsBL.GetFormRightsPhaseDefinitionID(uow, userinfo, phaseDefinition, null);

            // Menu: Disabled = generalactionRights > phaseRights; 
            if (generalactionRights > phaseRights)
            {
                return false;
            }

            return true;
        }

        public static bool HasPhaseRights(IUnitOfWork uow, FlowDefinitionID flowdefinitionID, int phase, string controllerName, string actionName)
        {
            var phaseDefinition = PhaseDefinitionCacheBL.GetByFlowDefinitionIDAndPhase(uow, flowdefinitionID, phase);

            var generalaction = GeneralActionBL.GetByURL(uow, controllerName, actionName);
            if (generalaction == null)
            {
                return false;
            }

            Enum.TryParse(generalaction.Rights, out Rights generalactionRights);

            var userinfo = PointUserInfoHelper.GetPointUserInfo(uow);
            var phaseRights = PhaseDefinitionRightsBL.GetFormRightsPhaseDefinitionID(uow, userinfo, phaseDefinition, null);

            return generalactionRights <= phaseRights;
        }

        public static void HandleNextPhase(IUnitOfWork uow, int flowInstanceID, int currentPhaseInstanceID, int nextPhaseDefinitionID, int employeeID, LogEntry logentry)
        {
            var currentPhaseInstance = GetById(uow, currentPhaseInstanceID);

            //Update the current phase:
            SetStatusDone(uow, currentPhaseInstance, logentry);

            var nextphasedefinition = PhaseDefinitionCacheBL.GetByID(uow, nextPhaseDefinitionID);

            // check for the existance of an already active phase - prevent multiple PhaseInstances
            var phaseinstances = PhaseInstanceBL.GetByFlowInstanceAndPhase(uow, flowInstanceID, nextphasedefinition.Phase).ToList();
            if (!phaseinstances.Any(pi => pi.Status <= (int)Status.Active))
            {
                if (currentPhaseInstance.PhaseDefinition.EndFlow && currentPhaseInstance.Status == (int)Status.Done)
                {
                    if (nextPhaseDefinitionID > 0)
                    {
                        //Re-activate the flow by creating a new phaseinstance, being the last definition
                        Create(uow, flowInstanceID, nextphasedefinition.PhaseDefinitionID, logentry);
                    }
                }
                else
                {
                    //And create the next/new phase:
                    Create(uow, flowInstanceID, nextphasedefinition.PhaseDefinitionID, logentry);
                }
                uow.Save();
            }
        }

        public static PhaseInstance HandleNextPhaseWithoutGeneralAction(IUnitOfWork uow, int flowInstanceID,
            int currentPhaseInstanceID, int nextPhaseDefinitionID,
            LogEntry logentry)
        {
            var currentPhaseInstance = GetById(uow, currentPhaseInstanceID);

            // Update the current phase:
            SetStatusDone(uow, currentPhaseInstance, logentry);

            PhaseInstance phaseinstance = null;

            // check for the existance of an already active phase - prevent multiple PhaseInstances
            var phaseinstances = PhaseInstanceBL.GetByFlowInstanceAndPhase(uow, flowInstanceID, currentPhaseInstance.PhaseDefinition.Phase).ToList();
            if (!phaseinstances.Any(pi => pi.Status <= (int)Status.Active))
            {
                // And create the next/new phase:
                phaseinstance = Create(uow, flowInstanceID, nextPhaseDefinitionID, logentry);

                uow.Save();
            }
            else
            {
                phaseinstance = phaseinstances
                    .OrderByDescending(pi => pi.PhaseInstanceID)
                    .FirstOrDefault();
            }

            return phaseinstance;
        }

        public static PhaseInstance GetById(IUnitOfWork uow, int phaseinstanceID)
        {
            return uow.PhaseInstanceRepository.Get(pi => pi.PhaseInstanceID == phaseinstanceID, includeProperties: "PhaseDefinition").FirstOrDefault();
        }

        public static IEnumerable<PhaseInstance> GetByFlowInstanceId(IUnitOfWork uow, int flowInstanceID)
        {
            return uow.PhaseInstanceRepository.Get(pi => pi.FlowInstanceID == flowInstanceID && pi.Cancelled == false,
                includeProperties: "PhaseDefinition");
        }

        public static PhaseInstance GetLastPhaseInstance(IUnitOfWork uow, int flowInstanceID)
        {
            return
                uow.PhaseInstanceRepository.Get(pi => pi.FlowInstanceID == flowInstanceID && pi.Cancelled == false)
                    .OrderByDescending(pi => pi.PhaseInstanceID)
                    .FirstOrDefault();
        }

        public static IEnumerable<PhaseInstance> GetByTransferIDAndCodeGroup(IUnitOfWork uow, int transferid,
            string codeGroup, bool includeinactive = false)
        {
            return
                uow.PhaseInstanceRepository.Get(
                    pi =>
                        pi.FlowInstance.TransferID == transferid && pi.PhaseDefinition.CodeGroup == codeGroup &&
                        (includeinactive || !pi.Cancelled));
        }

        public static IEnumerable<PhaseInstance> GetByFlowinstanceIDAndCodeGroup(IUnitOfWork uow, int flowinstanceID,
            string codeGroup, bool includeinactive = false)
        {
            return
                uow.PhaseInstanceRepository.Get(
                    pi =>
                        pi.FlowInstanceID == flowinstanceID && pi.PhaseDefinition.CodeGroup == codeGroup &&
                        (includeinactive || !pi.Cancelled));
        }

        public static IEnumerable<PhaseInstance> GetByFlowinstanceIDAndPhaseDefinitionIDs(IUnitOfWork uow,
            int flowinstanceID, int[] phaseDefinitionIDs)
        {
            return
                uow.PhaseInstanceRepository.Get(
                    pi =>
                        pi.FlowInstanceID == flowinstanceID && !pi.Cancelled &&
                        phaseDefinitionIDs.Contains(pi.PhaseDefinitionID));
        }

        public static bool EndPhaseIsDone(IUnitOfWork uow, int flowInstanceID)
        {
            return EndPhaseStatus(uow, flowInstanceID) == Status.Done;
        }

        public static Status? EndPhaseStatus(IUnitOfWork uow, int flowInstanceID)
        {
            return (Status?)uow.PhaseInstanceRepository.Get(pi => pi.FlowInstanceID == flowInstanceID && !pi.Cancelled && pi.PhaseDefinition.EndFlow).Select(pi => pi.Status)?.FirstOrDefault();
        }

        public static PhaseInstance GetEndPhaseDone(IUnitOfWork uow, int flowInstanceID)
        {
            return uow.PhaseInstanceRepository.FirstOrDefault(pi => pi.FlowInstanceID == flowInstanceID && !pi.Cancelled && pi.PhaseDefinition.EndFlow && pi.Status == (int)Status.Done);
        }

        public static PhaseInstance GetEndPhase(IUnitOfWork uow, int flowInstanceID)
        {
            return uow.PhaseInstanceRepository.FirstOrDefault(pi => pi.FlowInstanceID == flowInstanceID && !pi.Cancelled && pi.PhaseDefinition.EndFlow);
        }


        public static PhaseInstance Create(IUnitOfWork uow, int flowInstanceID, int phaseDefinitionID, LogEntry logentry, Status status = Status.Active)
        {
            var now = DateTime.Now;
            var phaseinstance = new PhaseInstance
            {
                EmployeeID = logentry.EmployeeID,
                FlowInstanceID = flowInstanceID,
                PhaseDefinitionID = phaseDefinitionID,
                StartProcessDate = now,
                StartedByEmployeeID = logentry.EmployeeID,
                RequestDate = null,
                RealizedDate = status == Status.Done ? now : (DateTime?) null,
                RealizedByEmployeeID = status == Status.Done ? logentry.EmployeeID : (int?) null,
                ScreenName = logentry.Screenname,
                Timestamp = logentry.Timestamp,
                Status = (int) status,
                Cancelled = false
            };

            Save(uow, phaseinstance);

            return phaseinstance;
        }

        public static IQueryable<PhaseInstance> GetActiveByFlowInstanceID(IUnitOfWork uow, int flowinstanceid)
        {
            return uow.PhaseInstanceRepository.Get(pi => pi.FlowInstanceID == flowinstanceid && pi.Status == (int)Status.Active && !(bool)pi.Cancelled);
        }
        
        public static PhaseInstance GetByFlowInstanceIDAndTypeID(IUnitOfWork uow, int flowInstanceID, int typeID)
        {
            return uow.PhaseInstanceRepository.Get(phi => phi.FlowInstanceID == flowInstanceID && phi.PhaseDefinition.FormType.TypeID == typeID && phi.Cancelled != true).FirstOrDefault();
        }

        public static PhaseInstance GetByFlowInstanceIDAndFlowHandling(IUnitOfWork uow, int flowInstanceID, FlowHandling flowHandling)
        {
            return uow.PhaseInstanceRepository.Get(phi => phi.FlowInstanceID == flowInstanceID && phi.PhaseDefinition.FlowHandling == flowHandling && phi.Cancelled != true).FirstOrDefault();
        }

        public static IEnumerable<PhaseInstance> GetAllByTransferAndPhaseDefinition(IUnitOfWork uow, int transferID, int phasedefinitionID)
        {
            return uow.PhaseInstanceRepository.Get(pi => pi.FlowInstance.TransferID == transferID && pi.PhaseDefinitionID == phasedefinitionID && !pi.Cancelled);
        }

        public static PhaseInstance GetLastByTransferAndPhaseDefinition(IUnitOfWork uow, int transferID, int phasedefinitionID)
        {
            return uow.PhaseInstanceRepository.Get(pi => pi.FlowInstance.TransferID == transferID && pi.PhaseDefinitionID == phasedefinitionID && !pi.Cancelled)
                .OrderByDescending(it => it.PhaseInstanceID)
                .FirstOrDefault();
        }

        public static void CancelByCodeGroup(IUnitOfWork uow, int transferID, string codeGroup, LogEntry logentry)
        {
            var phaseInstances = uow.PhaseInstanceRepository.Get(pi => pi.FlowInstance.TransferID == transferID &&
                                                                       pi.PhaseDefinition.CodeGroup == codeGroup &&
                                                                       !pi.Cancelled).ToList();
            Cancel(uow, phaseInstances, logentry);
        }

        public static void CancelExceptNewPhase(IUnitOfWork uow, int transferid, int startatphase, int newactivephasedefinitionid, LogEntry logentry)
        {
            var currentsavedformsetversions = FormSetVersionBL.GetByTransferIDAndPhaseDefinitionID(uow, transferid, newactivephasedefinitionid);
            foreach (var formsetversion in currentsavedformsetversions)
            {
                formsetversion.Deleted = true;
                LoggingBL.FillLoggingWithID(uow, formsetversion, logentry, forceupdate: true);
            }

            var phaseinstances = uow.PhaseInstanceRepository.Get(pi => pi.FlowInstance.TransferID == transferid && pi.PhaseDefinition.Phase >= startatphase && pi.PhaseDefinitionID != newactivephasedefinitionid);
            foreach (var phaseinstance in phaseinstances)
            {
                phaseinstance.Cancelled = true;
                LoggingBL.FillLogging(uow, phaseinstance, logentry, forceupdate: true);
            }

            var currentnewphaseinstance = GetLastByTransferAndPhaseDefinition(uow, transferid, newactivephasedefinitionid);
            if(currentnewphaseinstance?.Status == (int)Status.Done)
            {
                currentnewphaseinstance.Status = (int)Status.Active;
            }

            uow.Save();
        }

        public static void Cancel(IUnitOfWork uow, IEnumerable<PhaseInstance> phaseinstances, LogEntry logentry)
        {
            foreach (var phaseinstance in phaseinstances)
            {
                Cancel(uow, phaseinstance, logentry);
            }
        }

        public static void Cancel(IUnitOfWork uow, PhaseInstance phaseinstance, LogEntry logentry)
        {
            if (phaseinstance != null)
            {
                foreach (var formsetversion in phaseinstance.FormSetVersion.Where(fsv => fsv.Deleted == false))
                {
                    formsetversion.Deleted = true;
                    LoggingBL.FillLoggingWithID(uow, formsetversion, logentry, forceupdate: true);
                }

                phaseinstance.Cancelled = true;
                LoggingBL.FillLogging(uow, phaseinstance, logentry, forceupdate: true);

                uow.Save();
            }
        }

        public static void Upsert(IUnitOfWork uow, PhaseInstance instance)
        {
            if (instance.PhaseInstanceID == 0)
            {
                uow.PhaseInstanceRepository.Insert(instance);
            }
        }

        public static void Save(IUnitOfWork uow, PhaseInstance instance)
        {
            Upsert(uow, instance);

            uow.Save();
        }

        public static PhaseInstance GetByFlowInstanceIDAndPhaseDefinition(IUnitOfWork uow, int flowInstanceID,
            int phaseDefinitionID)
        {
            return uow.PhaseInstanceRepository.Get(pi => pi.FlowInstanceID == flowInstanceID && pi.PhaseDefinitionID == phaseDefinitionID && !pi.Cancelled)
                .OrderByDescending(pi => pi.PhaseInstanceID)
                .FirstOrDefault();
        }
    }
}