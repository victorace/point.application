﻿using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Point.Business.Logic
{
    public static class FlowFieldDashboardBL
    {
        public static IEnumerable<FlowWebField> GetForTransfer(IUnitOfWork uow, int flowinstanceid, IDictionary<string, object> viewData)
        {
            var flowinstance = FlowInstanceBL.GetByID(uow, flowinstanceid);
            var dashboardvalues = (from val in uow.FlowFieldValueRepository.Get()
                                  join form in uow.FormSetVersionRepository.Get() on val.FormSetVersionID equals form.FormSetVersionID
                                  join phase in uow.PhaseInstanceRepository.Get() on form.PhaseInstanceID equals phase.PhaseInstanceID 
                                  join dash in uow.FlowFieldDashboardRepository.Get() on val.FlowFieldID equals dash.FlowFieldID
                                  where 
                                    dash.FlowDefinitionID == flowinstance.FlowDefinitionID && 
                                    val.FlowInstanceID == flowinstanceid &&
                                    form.FormTypeID == dash.FormTypeID &&
                                    form.Deleted != true && 
                                    phase.Cancelled != true
                                   select val).ToList();

            var flowfieldids = uow.FlowFieldDashboardRepository.Get(it => it.FlowDefinitionID == flowinstance.FlowDefinitionID).Select(it=> it.FlowFieldID).ToArray();

            var dashboardflowfieldattributes = uow.FlowFieldAttributeRepository.Get(ffa => flowfieldids.Contains(ffa.FlowFieldID)).ToList();
            var rawflowwebfields = FlowWebFieldBL.FromModelList(uow, dashboardflowfieldattributes, new List<FlowFieldAttributeException>(), dashboardvalues, viewData: viewData);
            var unorderedfields = rawflowwebfields.GroupBy(pi => pi.FlowFieldID).Select(it => it.OrderByDescending(it2 => it2.DisplayValue).FirstOrDefault());

            List<FlowWebField> orderedlist = new List<FlowWebField>();
            foreach (var flowfieldid in flowfieldids)
            {
                var ordereditem = unorderedfields.Where(it => it.FlowFieldID == flowfieldid).FirstOrDefault();
                if (ordereditem != null)
                {
                    orderedlist.Add(ordereditem);
                }
            }

            return orderedlist;
        }
    }
}
