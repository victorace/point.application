﻿using System.Linq;
using Point.Database.Models;
using Point.Database.Repository;
using System.Collections.Generic;

namespace Point.Business.Logic
{
    public class GPSCoordinaatBL
    {
        public static List<string> GetUniqueFirst4Chars(List<string> allpostcodes)
        {
            return allpostcodes.GroupBy(it => it.Substring(0,4)).Select(it => it.Key).ToList();
        }

        public static List<string> GetPostcodesByStartAndRange(IUnitOfWork uow, string startingpostalcode, int range)
        {
            if (startingpostalcode.Length < 4) return null;

            double gpsrange = (double)1 / 111 * range;

            var startcoordinates = uow.GPSCoordinaatRepository.FirstOrDefault(it => it.Postcode.StartsWith(startingpostalcode));
            if (startcoordinates == null) return new List<string>();

            double maxlongitude = startcoordinates.Longitude.GetValueOrDefault(0) + gpsrange;
            double minlongitude = startcoordinates.Longitude.GetValueOrDefault(0) - gpsrange;

            double maxlatitude = startcoordinates.Latitude.GetValueOrDefault(0) + gpsrange;
            double minlatitude = startcoordinates.Latitude.GetValueOrDefault(0) - gpsrange;

            var matchingrows = uow.GPSCoordinaatRepository.Get(it =>
                    it.Longitude > minlongitude && it.Longitude < maxlongitude &&
                    it.Latitude > minlatitude && it.Latitude < maxlatitude
                ).Select(gps => gps.Postcode);

            return matchingrows.ToList();
        }

        public static List<string> GetPostcodesByStartAndRangeB(IUnitOfWork uow, string startingpostalcode, int range)
        {
            if (startingpostalcode.Length < 4) return null;

            double gpsrange = (double)1 / 111 * range;

            var startcoordinates = uow.GPSCoordinaatRepository.FirstOrDefault(it => it.Postcode.StartsWith(startingpostalcode));
            if (startcoordinates == null) return new List<string>();

            double maxlongitude = startcoordinates.Longitude.GetValueOrDefault(0) + gpsrange;
            double minlongitude = startcoordinates.Longitude.GetValueOrDefault(0) - gpsrange;

            double maxlatitude = startcoordinates.Latitude.GetValueOrDefault(0) + gpsrange;
            double minlatitude = startcoordinates.Latitude.GetValueOrDefault(0) - gpsrange;

            var matchingrows = uow.GPSCoordinaatRepository.Get(it =>
                    it.Longitude > minlongitude && it.Longitude < maxlongitude &&
                    it.Latitude > minlatitude && it.Latitude < maxlatitude
                ); //.Select(gps => gps.Postcode);

            matchingrows = matchingrows?.Join(uow.OrganizationSearchRepository.GetAll(), coord => coord.Postcode, search => search.LocationPostcode, (coord, search) => new { coord, search }).Select(com =>com.coord);

            return matchingrows.Select(c => c.Postcode).ToList();

            //return matchingrows.ToList();
        }

        public static GPSCoordinaat GetStartCoordinate(IUnitOfWork uow, string postalCode)
        {
            GPSCoordinaat startCoordinate = null;
            if (!string.IsNullOrEmpty(postalCode))
            {
                postalCode = postalCode.ToUpperInvariant();
                startCoordinate = postalCode.Length == 6 ? uow.GPSCoordinaatRepository.FirstOrDefault(it => it.Postcode != null && it.Postcode.Equals(postalCode)) 
                    : uow.GPSCoordinaatRepository.FirstOrDefault(it => it.Postcode != null && it.Postcode.StartsWith(postalCode));
            }

            return startCoordinate ?? new GPSCoordinaat {Latitude = 0, Longitude = 0, Postcode = ""};
        }
    }
}
