﻿using System.Collections.Generic;
using System.Linq;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;

namespace Point.Business.Logic
{
    public class ServiceAreaBL
    {
        public static IEnumerable<ServiceArea> GetByOrganizationID(IUnitOfWork uow, int organizationid)
        {
            return uow.ServiceAreaRepository.Get(it => it.OrganizationID == organizationid && it.Inactive != true);
        }

        public static ServiceArea Add(IUnitOfWork uow, ServiceAreaViewModel viewmodel, LogEntry logentry)
        {
            var model = new ServiceArea
            {
                Description = viewmodel.Description,
                OrganizationID = viewmodel.OrganizationID
            };

            uow.ServiceAreaRepository.Insert(model);

            LoggingBL.FillLoggable(uow, model, logentry);

            return model;
        }

        public static ServiceAreaPostalCode GetServiceAreaPostalCodeByID(IUnitOfWork uow, int serviceareapostalcodeid)
        {
            return uow.ServiceAreaPostalCodeRepository.GetByID(serviceareapostalcodeid);
        }

        public static ServiceAreaPostalCode AddPostalCode(IUnitOfWork uow, ServiceAreaPostalCodeViewModel viewmodel, LogEntry logentry)
        {
            var model = new ServiceAreaPostalCode();
            var postalcode = new PostalCode();
            model.PostalCode = postalcode;
            model.PostalCode.Description = viewmodel.Description;
            model.PostalCode.StartPostalCode = viewmodel.StartPostalCode;
            model.PostalCode.EndPostalCode = viewmodel.EndPostalCode;
            model.ServiceAreaID = viewmodel.ServiceAreaID;
            model.DepartmentID = viewmodel.DepartmentID;

            uow.ServiceAreaPostalCodeRepository.Insert(model);

            LoggingBL.FillLoggable(uow, postalcode, logentry);

            return model;
        }

        public static ServiceAreaPostalCode UpdatePostalCode(IUnitOfWork uow, ServiceAreaPostalCodeViewModel viewmodel, LogEntry logentry)
        {
            var model = ServiceAreaBL.GetServiceAreaPostalCodeByID(uow, viewmodel.ServiceAreaPostalCodeID);
            if (model == null) return null;

            model.PostalCode.Description = viewmodel.Description;
            model.PostalCode.StartPostalCode = viewmodel.StartPostalCode;
            model.PostalCode.EndPostalCode = viewmodel.EndPostalCode;

            LoggingBL.FillLoggable(uow, model.PostalCode, logentry);
            
            return model;
        }

        public static void UpdateDepartmentServiceArea(IUnitOfWork uow, int departmentid, int serviceareaid, bool selected, LogEntry logentry)
        {
            if (selected == false)
            {
                uow.ServiceAreaPostalCodeRepository.Delete(it => it.DepartmentID == departmentid && it.ServiceAreaID == serviceareaid);
            } else
            {
                uow.ServiceAreaPostalCodeRepository.Insert(
                    new ServiceAreaPostalCode() {
                        DepartmentID = departmentid,
                        ServiceAreaID = serviceareaid,
                        ModifiedTimeStamp = logentry.Timestamp
                    });
            }
        }

        public static void DeletePostalCode(IUnitOfWork uow, int serviceAreaPostalCodeID, LogEntry logentry)
        {
            var serviceAreaPostalcode = GetServiceAreaPostalCodeByID(uow, serviceAreaPostalCodeID);
            LoggingBL.FillLoggable(uow, serviceAreaPostalcode.PostalCode, logentry, forceupdate: true);

            uow.PostalCodeRepository.Delete(serviceAreaPostalcode.PostalCode);
            uow.ServiceAreaPostalCodeRepository.Delete(serviceAreaPostalcode);
        }

        public static void DeleteServiceArea(IUnitOfWork uow, int serviceAreaID, LogEntry logentry)
        {
            var servicearea = GetByServiceAreaID(uow, serviceAreaID);
            LoggingBL.FillLoggable(uow, servicearea, logentry, forceupdate: true);

            var postalcodeids = uow.ServiceAreaPostalCodeRepository.Get(it => it.ServiceAreaID == serviceAreaID).Select(it => it.PostalCodeID).ToArray();

            uow.PostalCodeRepository.Delete(it => postalcodeids.Contains(it.PostalCodeID));
            uow.ServiceAreaPostalCodeRepository.Delete(it => it.ServiceAreaID == serviceAreaID);
            uow.ServiceAreaRepository.Delete(servicearea);
        }

        public static ServiceArea GetByServiceAreaID(IUnitOfWork uow, int serviceareaid)
        {
            return uow.ServiceAreaRepository.GetByID(serviceareaid);
        }
    }
}
