﻿using Point.Business.Helper;
using Point.Database.Extensions;
using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using System;
using System.Collections.Generic;
using System.Linq;


namespace Point.Business.Logic
{
    public class CommunicationJournalViewBL
    {

        //TO DO: is this function still needed?
        public static IEnumerable<CommunicationJournalViewModel> GetCommunicationJournalByTransferID(
           IUnitOfWork uow, int transferID)
        {
            return GetByTransferIDAndTransferMemoTargetType(uow, transferID);
        }

        public static IEnumerable<TransferMemoChanges> GetCommunicationJournalHistoryByTransferMemoID(IUnitOfWork uow, int transferMemoID)
        {
            IEnumerable<TransferMemoChanges> result = null;
            result = uow.TransferMemoChangesRepository.Get(t => t.TransferMemoID == transferMemoID).OrderByDescending(m => m.TransferMemoChangesID );
            return result;
        }


        //TO DO: is this function still needed?
        private static IEnumerable<CommunicationJournalViewModel> GetByTransferIDAndTransferMemoTargetType(IUnitOfWork uow, int transferID)
        {
            IEnumerable<CommunicationJournalViewModel> result = null;
                var targets = new List<TransferMemoTypeID>
                {
                    TransferMemoTypeID.Zorgprofessional,
                    TransferMemoTypeID.Transferpunt,
                    TransferMemoTypeID.Patient
                };

                var strTargets = targets.Select(t => t.ToString()).ToList();
                result = uow.TransferMemoRepository.Get(tm => tm.TransferID == transferID &&
                    tm.Deleted == false && strTargets.Contains(tm.Target))
                    .OrderByDescending(tm => tm.TransferMemoID).ToList()
                    .Select(transferMemo => new CommunicationJournalViewModel
                    {
                        TransferMemoID = transferMemo.TransferMemoID,
                        EmployeeID = transferMemo.EmployeeID,
                        EmployeeName = transferMemo.EmployeeName,
                        MemoDateTime = transferMemo.MemoDateTime,
                        MemoContent = transferMemo.MemoContent,
                        TransferID = transferMemo.TransferID
                    }).ToList();
            
            return result;
        }
    }
}
