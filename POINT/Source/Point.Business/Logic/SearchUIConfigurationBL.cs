﻿using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Caching;
using System.Collections.Generic;
using System.Linq;

namespace Point.Business.Logic
{
    public static class SearchUIConfigurationBL
    {
        public const int DEFAULT_PAGESIZE = 50;
        public const int DEFAULT_AUTOREFRESHSECONDS = 300;
        private const string WIDTH_ICON = "search-cell-icon";
        private const string WIDTH_X_SMALL = "search-cell-x-small";
        private const string WIDTH_SMALL = "search-cell-small";
        private const string WIDTH_MEDIUM = "search-cell-medium";
        private const string WIDTH_LARGE = "search-cell-large";
        private const string WIDTH_X_LARGE = "search-cell-x-large";

        public static void DeleteDefaultByFlowDefinitionID(IUnitOfWork uow, FlowDefinitionID flowDefinitionID)
        {
            uow.SearchUIConfigurationRepository.Delete(it => it.FlowDefinitionID == flowDefinitionID && it.OrganizationID == null && it.DepartmentID == null);
            uow.Save();
        }

        public static SearchUIConfiguration GetConfigurationOrDefault(IUnitOfWork uow, FlowDefinitionID[] flowDefinitionIDs, SearchType searchtype, PointUserInfo pointUserInfo)
        {
            int? departmentID = (pointUserInfo.Department != null ? pointUserInfo.Department.DepartmentID : (int?)null);
            int? organizationID = (pointUserInfo.Organization != null ? pointUserInfo.Organization.OrganizationID : (int?)null);

            var searchuiconfiguration = GetConfigurationOrDefault(uow, flowDefinitionIDs.FirstOrDefault(), searchtype, organizationID, departmentID, true);

            return searchuiconfiguration;
        }

        public static SearchUIConfiguration GetConfigurationOrDefault(IUnitOfWork uow, FlowDefinitionID flowdefinitionid, SearchType searchtype, int? organizationid, int? departmentid, bool useCache)
        {
            string cacheKey = string.Concat("SearchUIConfiguration_", flowdefinitionid, "_", organizationid, "_", departmentid, "_", searchtype.ToString());
            SearchUIConfiguration result = null;

            if (useCache)
            {
                result = CacheService.Instance.Get<SearchUIConfiguration>(cacheKey);
                if (result != null)
                {
                    return result;
                }
            }

            var allvalid = uow.SearchUIConfigurationRepository.Get(it =>
                                    it.FlowDefinitionID == flowdefinitionid &&
                                    it.SearchType == searchtype.ToString() &&
                                    (it.OrganizationID == null || it.OrganizationID == organizationid), includeProperties: "Fields");

            if (allvalid.FirstOrDefault(it => it.OrganizationID == null) == null)
            {
                result = GetCodeDefault(flowdefinitionid, searchtype);
                if (flowdefinitionid > 0)
                {
                    uow.SearchUIConfigurationRepository.Insert(result);
                    uow.Save();
                }
            }

            if (result == null && departmentid.HasValue)
            {
                result = allvalid.FirstOrDefault(it => it.DepartmentID == departmentid);
            }

            if (result == null && organizationid.HasValue)
            {
                result = allvalid.FirstOrDefault(it => it.OrganizationID == organizationid);
            }

            if (result == null)
            {
                result = allvalid.FirstOrDefault(it => it.OrganizationID == null);
            }

            CacheService.Instance.Insert(cacheKey, result);
            return result;
        }

        public static SearchUIConfiguration GetConfiguration(IUnitOfWork uow, FlowDefinitionID flowdefinitionid, int organizationid, SearchType searchtype)
        {
            return uow.SearchUIConfigurationRepository.GetAll()
                .FirstOrDefault(it => it.FlowDefinitionID == flowdefinitionid && it.OrganizationID == organizationid
                    && it.SearchType == searchtype.ToString());
        }

        private static SearchUIFieldConfiguration newSearchUIField(bool field_IsWellknown, string field_Name, string Column_FullName,
            string Column_HeaderName, string column_CssClass, bool column_IsVisible, int column_Index,
            bool icon_ShowAsIcon = false, string icon_GlyphName = null, string icon_GlyphNameFalse = null,
            bool icon_TreatEmptyAsNull = true, bool icon_TreatNullAsFalse = false, bool useInSearchFilter = false,
            SearchDestination clickDestination = SearchDestination.Dashboard, SearchPropertyType searchPropertyType = SearchPropertyType.String,
            SearchFieldOwningType searchfieldowningtype = SearchFieldOwningType.Transfer)
        {
            SearchUIFieldConfiguration searchuifieldconfiguration = new SearchUIFieldConfiguration();
            searchuifieldconfiguration.Column = new ColumnInfo()
            {
                Index = column_Index,
                IsVisible = column_IsVisible,
                IsSortable = true,
                CssClass = column_CssClass,
                FullName = Column_FullName,
                HeaderName = Column_HeaderName
            };

            searchuifieldconfiguration.Field = new FieldInfo()
            {
                IsWellKnown = field_IsWellknown,
                Name = field_Name,

            };

            searchuifieldconfiguration.Icon = new IconInfo()
            {
                GlyphName = icon_GlyphName,
                GlyphNameFalse = icon_GlyphNameFalse,
                ShowAsIcon = icon_ShowAsIcon,
                TreatEmptyAsNull = icon_TreatEmptyAsNull,
                TreatNullAsFalse = icon_TreatNullAsFalse
            };

            searchuifieldconfiguration.UseInSearchFilter = useInSearchFilter;
            searchuifieldconfiguration.SearchDestination = clickDestination;
            searchuifieldconfiguration.SearchPropertyType = searchPropertyType;
            searchuifieldconfiguration.SearchFieldOwningType = searchfieldowningtype;

            return searchuifieldconfiguration;
        }

        public static SearchUIConfiguration GetCodeDefault(FlowDefinitionID flowdefinitionid, SearchType searchType = SearchType.Transfer)
        {
            var searchuiconfiguration = new SearchUIConfiguration()
            {
                SortOrder = new SortOrderInfo()
                {
                    Field = new FieldInfo()
                    {
                        IsWellKnown = true,
                        Name = WellKnownFieldNames.TransferCreatedDate
                    },
                    OrderByDescending = true
                },
                DossierMenus = new DossierMenus()
                {
                    OnlyMSVT = true,
                    AllAcceptMSVT = true
                },
                AutoRefreshSeconds = DEFAULT_AUTOREFRESHSECONDS,
                PageSize = DEFAULT_PAGESIZE,
                FlowDefinitionID = flowdefinitionid,
                SearchType = searchType.ToString()
            };

            searchuiconfiguration.Fields = new List<SearchUIFieldConfiguration>();

            if (searchType == SearchType.Frequency)
            {
                searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.ClientFullname, "Patiëntnaam", "Patiëntnaam", WIDTH_LARGE, true, 0, useInSearchFilter: true));
                searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.PatientNumber, "Patiëntnummer", "Patiëntnr", WIDTH_MEDIUM, false, 1, useInSearchFilter: true));
                searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.FlowDefinitionName, "Flow", "Flow", WIDTH_SMALL, false, 2));
                searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.ClientBirthDate, "Geboortedatum", "Geboorte-datum", WIDTH_SMALL, true, 3
                    , useInSearchFilter: true, searchPropertyType: SearchPropertyType.Date));
                searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.LastFrequencyAttachmentUploadDate, "Laatste Bijlage", "Laatste Bijlage", WIDTH_ICON, true, 4
                    , icon_ShowAsIcon: true, icon_GlyphName: "glyphicon glyphicon-paperclip", searchPropertyType: SearchPropertyType.Date
                    , clickDestination: SearchDestination.FeedbackAttachment, searchfieldowningtype: SearchFieldOwningType.Frequency));
                searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.ClientGender, "Geslacht (M/V)", "M/V", WIDTH_X_SMALL, true, 5));
                searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.FrequencyName, "Doorlopend dossier", "doorl. dossier", WIDTH_LARGE, true, 6, searchfieldowningtype: SearchFieldOwningType.Frequency));
                searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.FrequencyType, "Type", "Type", WIDTH_SMALL, true, 7, searchfieldowningtype: SearchFieldOwningType.Frequency));
                searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.FrequencyStartDate, "Startdatum", "Start", WIDTH_SMALL, true, 8, searchfieldowningtype: SearchFieldOwningType.Frequency));
                searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.FrequencyEndDate, "Einddatum", "Eind", WIDTH_SMALL, true, 9, searchfieldowningtype: SearchFieldOwningType.Frequency));
                searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.LastFrequencyTransferMemoDateTime, "Communicatiejournaal", "Comm. Journaal", WIDTH_LARGE, true, 10
                    , clickDestination: SearchDestination.FeedbackTransferMemo, searchfieldowningtype: SearchFieldOwningType.Frequency));
                searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.LastFrequencyTransferMemoEmployeeName, "Medewerker Communicatiejournaal", "Mdw. Comm.", WIDTH_SMALL, true, 11
                    , clickDestination: SearchDestination.FeedbackTransferMemo, searchfieldowningtype: SearchFieldOwningType.Frequency));

                if (flowdefinitionid == FlowDefinitionID.ZH_VVT || flowdefinitionid == FlowDefinitionID.CVA)
                {
                    searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.DesiredHealthCareProvider, "Geselecteerde VVT", "Geselecteerde VVT", WIDTH_LARGE, true, 12
                        , clickDestination: SearchDestination.TransferRoute));
                    searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.OrganizationName, "Organisatie", "Organisatie", WIDTH_SMALL, true, 13
                        , useInSearchFilter: true, clickDestination: SearchDestination.RequestFormZHVVT));
                    searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.LocationName, "Locatie", "Locatie", WIDTH_SMALL, true, 14
                        , useInSearchFilter: true, clickDestination: SearchDestination.RequestFormZHVVT));
                    searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.DepartmentName, "Afdeling", "Afdeling", WIDTH_SMALL, true, 15
                        , useInSearchFilter: true, clickDestination: SearchDestination.RequestFormZHVVT));
                }

                if (flowdefinitionid == FlowDefinitionID.RzTP)
                {
                    searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.DesiredHealthCareProvider, "Geselecteerde VVT", "Geselecteerde VVT", WIDTH_LARGE, true, 12
                        , clickDestination: SearchDestination.TransferRoute));
                    searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.OrganizationName, "Organisatie", "Organisatie", WIDTH_SMALL, true, 13
                        , useInSearchFilter: true, clickDestination: SearchDestination.RequestFormRzTP));
                    searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.LocationName, "Locatie", "Locatie", WIDTH_SMALL, true, 14
                        , useInSearchFilter: true, clickDestination: SearchDestination.RequestFormRzTP));
                    searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.DepartmentName, "Afdeling", "Afdeling", WIDTH_SMALL, true, 15
                        , useInSearchFilter: true, clickDestination: SearchDestination.RequestFormRzTP));
                }
            }
            else if (searchType == SearchType.PatientOverview)
            {
                searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.ClientFullname, "Patiëntnaam", "Patiëntnaam", WIDTH_LARGE, true, 1));
                searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.ClientGender, "Geslacht (M/V)", "M/V", WIDTH_X_SMALL, true, 2));
                searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.DepartmentName, "Afdeling", "Afdeling", WIDTH_SMALL, true, 3));
                searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.LastTransferMemoDateTime, "Communicatiejournaal", "Comm. Journaal", WIDTH_MEDIUM, true, 4));
                searchuiconfiguration.Fields.Add(newSearchUIField(false, WellKnownFieldNames.DatumEindeBehandelingMedischSpecialist, "Gewenste ontslagdatum", "Gewenste ontslagdatum", WIDTH_SMALL, true, 5, searchPropertyType: SearchPropertyType.Date));
                searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.RequestTransferPointIntakeDate, "Bij transferpunt", "Bij transferpunt", WIDTH_SMALL, true, 6, searchPropertyType: SearchPropertyType.Date));
                searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.ActivePhaseText, "Actieve Fase(n)", "Actieve Fase(n)", WIDTH_MEDIUM, true, 7));
                searchuiconfiguration.Fields.Add(newSearchUIField(false, WellKnownFieldNames.CareBeginDate, "Patient moet zorg ontvangen vanaf", "TP gewenste zorg", WIDTH_SMALL, true, 8, searchPropertyType: SearchPropertyType.Date));
                searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.DesiredHealthCareProvider, "Geselecteerde VVT", "Geselecteerde VVT", WIDTH_LARGE, true, 29));
                searchuiconfiguration.Fields.Add(newSearchUIField(false, WellKnownFieldNames.DischargeProposedStartDate, "Patient kan overgenomen/in zorg genomen worden met ingang van", "VVT overname", WIDTH_SMALL, true, 10, searchPropertyType: SearchPropertyType.Date));
                searchuiconfiguration.Fields.Add(newSearchUIField(false, WellKnownFieldNames.AcceptedByTelephoneNumber, "Telefoonnummer", "Tel. mdw. TP", WIDTH_SMALL, true, 11));
                searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.FlowDefinitionName, "Flow", "Flow", WIDTH_SMALL, false, 99));
            }
            else if (searchType == SearchType.Transfer)
            {
                searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.ClientFullname, "Patiëntnaam", "Patiëntnaam", WIDTH_LARGE, true, 0, useInSearchFilter: true));
                searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.PatientNumber, "Patiëntnummer", "Patiëntnr", WIDTH_MEDIUM, false, 1, useInSearchFilter: true));
                searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.FlowDefinitionName, "Flow", "Flow", WIDTH_SMALL, false, 2));
                searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.LastTransferAttachmentUploadDate, "Laatste Bijlage", "Laatste Bijlage", WIDTH_ICON, true, 3
                    , icon_ShowAsIcon: true, icon_GlyphName: "glyphicon glyphicon-paperclip", searchPropertyType: SearchPropertyType.Date
                    , clickDestination: SearchDestination.TransferAttachment));
                searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.ClientBirthDate, "Geboortedatum", "Geboorte-datum", WIDTH_SMALL, true, 4
                    , useInSearchFilter: true, searchPropertyType: SearchPropertyType.Date));
                searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.ClientGender, "Geslacht (M/V)", "M/V", WIDTH_X_SMALL, true, 5));
                searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.CopyOfTransferID, "Kopie", "Kopie", WIDTH_ICON, true, 6
                    , icon_ShowAsIcon: true, icon_GlyphName: "glyphicon glyphicon-duplicate"));
                searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.ActivePhaseText, "Actieve Fase(n)", "Actieve Fase(n)", WIDTH_MEDIUM, true, 7));
                searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.LastTransferMemoDateTime, "Communicatiejournaal", "Comm. Journaal", WIDTH_MEDIUM, true, 9
                    , clickDestination: SearchDestination.CommunicationJournal));
                searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.LastTransferAttachmentUploadDate, "Laatste Bijlage", "Laatste Bijlage", WIDTH_MEDIUM, false, 10, clickDestination: SearchDestination.TransferAttachment));
                searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.TransferCreatedDate, "Aangemaakt", "Aangemaakt", WIDTH_SMALL, true, 11
                    , useInSearchFilter: true, searchPropertyType: SearchPropertyType.Date));
                searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.ClientCivilServiceNumber, "BSN", "BSN", WIDTH_MEDIUM, false, 22
                    , useInSearchFilter: true));

                searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.TransferTaskDueDate, "Taken", "Taken", WIDTH_MEDIUM, false, 9, clickDestination: SearchDestination.AdditionalInfoRequestTP));

                searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.GewensteIngangsdatum, "Gewenste ingangsdatum zorg", "Gewenste ingangsdatum zorg", WIDTH_SMALL, false, 24));

                if (flowdefinitionid == FlowDefinitionID.VVT_ZH)
                {
                    // And a few fields for the VVT-ZH
                    searchuiconfiguration.Fields.Add(newSearchUIField(false, WellKnownFieldNames.TransferDate, "Datum Transfer (gepland)", "Datum Transfer", WIDTH_SMALL, true, 16));
                    searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.OrganizationName, "Organisatie", "Organisatie", WIDTH_SMALL, true, 17
                        , useInSearchFilter: true));
                    searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.TransferCreatedBy, "VVT Medewerker", "Mdw. VVT", WIDTH_SMALL, true, 18
                        , useInSearchFilter: true));
                    searchuiconfiguration.Fields.Add(newSearchUIField(false, WellKnownFieldNames.DestinationHospital, "Ziekenhuis", "Ziekenhuis", WIDTH_MEDIUM, true, 19));
                    searchuiconfiguration.Fields.Add(newSearchUIField(false, WellKnownFieldNames.DestinationHospitalLocation, "Locatie", "Locatie", WIDTH_MEDIUM, true, 20));
                    searchuiconfiguration.Fields.Add(newSearchUIField(false, WellKnownFieldNames.DestinationHospitalDepartment, "Afdeling", "Afdeling", WIDTH_MEDIUM, true, 21));
                }

                if (flowdefinitionid == FlowDefinitionID.ZH_VVT || flowdefinitionid == FlowDefinitionID.RzTP || flowdefinitionid == FlowDefinitionID.CVA)
                {
                    searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.LastGrzFormDateTime, "GRZ", "GRZ", WIDTH_MEDIUM, true, 8, clickDestination: SearchDestination.GRZForm));
                    searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.RequestFormZHVVTType, "Aanvraag type", "Aanvraag type", WIDTH_MEDIUM, true, 12
                        , useInSearchFilter: true, searchPropertyType: SearchPropertyType.Lookup));
                    searchuiconfiguration.Fields.Add(newSearchUIField(false, WellKnownFieldNames.DatumEindeBehandelingMedischSpecialist, "Gewenste ontslagdatum", "Gewenste ontslagdatum", WIDTH_SMALL, true, 13
                        , useInSearchFilter: true, searchPropertyType: SearchPropertyType.Date));
                    searchuiconfiguration.Fields.Add(newSearchUIField(false, WellKnownFieldNames.GewensteIngangsdatum, "Poli Gewenste zorgdatum", "Poli zorgdatum", WIDTH_SMALL, false, 14
                        , useInSearchFilter: false, searchPropertyType: SearchPropertyType.Date));

                    var searchdestinationrequestform = SearchDestination.RequestFormZHVVT;
                    if (flowdefinitionid == FlowDefinitionID.RzTP)
                    {
                        searchdestinationrequestform = SearchDestination.RequestFormRzTP;
                    }
                    searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.OrganizationName, "Organisatie", "Organisatie", WIDTH_SMALL, false, 15
                        , useInSearchFilter: true, clickDestination: searchdestinationrequestform));
                    searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.LocationName, "Locatie", "Locatie", WIDTH_SMALL, false, 16
                        , useInSearchFilter: true, clickDestination: searchdestinationrequestform));
                    searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.DepartmentName, "Afdeling", "Afdeling", WIDTH_SMALL, true, 17
                        , useInSearchFilter: true, clickDestination: searchdestinationrequestform));
                    searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.TyperingNazorgCombined, "Typering Nazorg uit Aanvraag", "Nazorg", WIDTH_SMALL, true, 18
                        , useInSearchFilter: true, clickDestination: searchdestinationrequestform));

                    var searchdestinationtransferroute = SearchDestination.TransferRoute;
                    if (flowdefinitionid == FlowDefinitionID.RzTP)
                    {
                        searchdestinationtransferroute = SearchDestination.TransferRouteRzTP;
                    }
                    searchuiconfiguration.Fields.Add(newSearchUIField(false, WellKnownFieldNames.AcceptedBy, "Aanvraag in behandeling genomen door", "Mdw. TP", WIDTH_LARGE, true, 19
                        , useInSearchFilter: true, searchPropertyType: SearchPropertyType.Lookup, clickDestination: SearchDestination.AcceptAtHospital));
                    searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.DesiredHealthCareProvider, "Geselecteerde VVT", "Geselecteerde VVT", WIDTH_LARGE, true, 20
                        , clickDestination: searchdestinationtransferroute));
                    searchuiconfiguration.Fields.Add(newSearchUIField(false, WellKnownFieldNames.AcceptedDateTP, "Datum aanvraag in behandeling genomen", "TP in beh.", WIDTH_SMALL, true, 21
                        , useInSearchFilter: true, searchPropertyType: SearchPropertyType.Date));
                    searchuiconfiguration.Fields.Add(newSearchUIField(false, WellKnownFieldNames.CareBeginDate, "Patient moet zorg ontvangen vanaf", "TP gewenste zorg", WIDTH_SMALL, true, 22
                        , useInSearchFilter: true, searchPropertyType: SearchPropertyType.Date));
                    searchuiconfiguration.Fields.Add(newSearchUIField(false, WellKnownFieldNames.BehandelaarSpecialisme, "Specialisme", "Specialisme", WIDTH_LARGE, true, 23
                        , useInSearchFilter: true, searchPropertyType: SearchPropertyType.Lookup));
                    searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.ZorgInZorginstellingVoorkeurPatient1, "VVT 1e keuze", "VVT 1e keuze", WIDTH_LARGE, true, 24
                        , clickDestination: searchdestinationtransferroute));
                    searchuiconfiguration.Fields.Add(newSearchUIField(false, WellKnownFieldNames.TransferDateVVT, "Aanvraag bij VVT", "Aanvraag bij VVT", WIDTH_SMALL, true, 25
                        , useInSearchFilter: true, searchPropertyType: SearchPropertyType.Date));
                    searchuiconfiguration.Fields.Add(newSearchUIField(false, WellKnownFieldNames.DischargeProposedStartDate, "Patient kan overgenomen/in zorg genomen worden met ingang van", "VVT overname", WIDTH_SMALL, true, 26
                        , useInSearchFilter: true, searchPropertyType: SearchPropertyType.Date));

                    var searchdestinationadditionalinforequesttp = SearchDestination.AdditionalInfoRequestTP;
                    if (flowdefinitionid == FlowDefinitionID.RzTP)
                    {
                        searchdestinationadditionalinforequesttp = SearchDestination.Dashboard;
                    }
                    searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.LastAdditionalTPMemoDateTime, "Aanvullende gegevens TP", "Aanvull. geg TP", WIDTH_SMALL, true, 27
                        , clickDestination: searchdestinationadditionalinforequesttp));
                    searchuiconfiguration.Fields.Add(newSearchUIField(false, WellKnownFieldNames.AcceptedByTelephoneNumber, "Telefoonnummer", "Tel. mdw. TP", WIDTH_SMALL, true, 28
                        , useInSearchFilter: true));
                    searchuiconfiguration.Fields.Add(newSearchUIField(false, WellKnownFieldNames.DischargePatientAcceptedYNByVVT, "Besluit overname", "Besluit overname", "", false, 29));
                    searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.VOStatus, "VO status", "VO status", "", false, 30));
                    searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.IsInterrupted, "Onderbroken", "Onderbroken", "", false, 31));
                    searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.AcceptedByID, "Geaccepteerd door ID", "Geaccepteerd door ID", "", false, 33));
                    searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.AcceptedByVVTID, "Geaccepteerd door VVT ID", "Geaccepteerd door VVT ID", "", false, 34));
                    searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.AcceptedByIIID, "Geaccepteerd door II ID", "Geaccepteerd door II ID", "", false, 35));
                    searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.RequestTransferPointIntakeDate, "Bij transferpunt", "Bij transferpunt", WIDTH_SMALL, true, 36
                        , useInSearchFilter: true, searchPropertyType: SearchPropertyType.Date));
                    searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.HealthInsuranceCompany, "Zorgverzekeraar", "Zorgverzekeraar", WIDTH_MEDIUM, false, 37));
                    searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.HealthInsuranceCompanyID, "Zorgverzekeraar ID", "Zorgverzekeraar ID", "", false, 38));
                    searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.BehandelaarNaam, "Naam behandelend arts", "Beh. arts", WIDTH_MEDIUM, false, 40));
                    searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.MedischeSituatieDatumOpname, "Datum opname", "Datum opname", WIDTH_SMALL, false, 41));
                    searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.AcceptedByVVT, "Medewerker VVT", "Mdw. VVT", WIDTH_MEDIUM, false, 42));
                    searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.AcceptedByTelephoneVVT, "Telefoonnummer VVT", "Tel. mdw. VVT", WIDTH_MEDIUM, false, 43));
                }

                if (flowdefinitionid == FlowDefinitionID.ZH_ZH)
                {
                    //ZH-ZH
                }

                if (flowdefinitionid == FlowDefinitionID.HA_VVT)
                {
                    var searchdestinationrequestform = SearchDestination.RequestFormZHVVT; 

                    searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.OrganizationName, "Organisatie", "Organisatie", WIDTH_SMALL, true, 12
                        , useInSearchFilter: true, clickDestination: searchdestinationrequestform));
                    searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.LocationName, "Locatie", "Locatie", WIDTH_SMALL, true, 13
                        , useInSearchFilter: true, clickDestination: searchdestinationrequestform));
                    searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.DepartmentName, "Afdeling", "Afdeling", WIDTH_SMALL, true, 14
                        , useInSearchFilter: true, clickDestination: searchdestinationrequestform));

                    var searchdestinationtransferroute = SearchDestination.TransferRoute; 
                    searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.DesiredHealthCareProvider, "Actuele VVT", "Actuele VVT", WIDTH_MEDIUM, true, 15
                        , useInSearchFilter: true, clickDestination: searchdestinationtransferroute));
                    searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.TransferCreatedBy, "Medewerker", "Mdw.", WIDTH_SMALL, true, 16
                        , useInSearchFilter: true));

                    searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.TyperingNazorgCombined, "Typering Nazorg", "Nazorg", WIDTH_SMALL, true, 18
                        , useInSearchFilter: true, clickDestination: searchdestinationtransferroute));

                }

                if (flowdefinitionid == FlowDefinitionID.VVT_VVT)
                {
                    var searchdestinationrequestform = SearchDestination.RequestFormZHVVT; //Deze word ook gebruikt in VVT-VVT

                    searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.OrganizationName, "Organisatie", "Organisatie", WIDTH_SMALL, true, 12
                        , useInSearchFilter: true, clickDestination: searchdestinationrequestform));
                    searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.LocationName, "Locatie", "Locatie", WIDTH_SMALL, true, 13
                        , useInSearchFilter: true, clickDestination: searchdestinationrequestform));
                    searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.DepartmentName, "Afdeling", "Afdeling", WIDTH_SMALL, true, 14
                        , useInSearchFilter: true, clickDestination: searchdestinationrequestform));

                    var searchdestinationtransferroute = SearchDestination.TransferRoute; //Deze word ook gebruikt in VVT-VVT
                    searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.DesiredHealthCareProvider, "Actuele VVT", "Actuele VVT", WIDTH_MEDIUM, true, 15
                        , useInSearchFilter: true, clickDestination: searchdestinationtransferroute));
                }


            }
            else if (searchType == SearchType.Mobile)
            {
                searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.ClientFullname, "Patiëntnaam", "Patiëntnaam", WIDTH_LARGE, true, 0, useInSearchFilter: true));
                searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.ClientGender, "Geslacht (M/V)", "M/V", WIDTH_X_SMALL, true, 1));
                searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.ClientBirthDate, "Geboortedatum", "Geboorte-datum", WIDTH_SMALL, true, 2, useInSearchFilter: true, searchPropertyType: SearchPropertyType.Date));
                searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.FlowDefinitionName, "Flow", "Flow", WIDTH_SMALL, true, 3));
                searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.TransferCreatedDate, "Aangemaakt", "Aangemaakt", WIDTH_SMALL, true, 4, useInSearchFilter: true, searchPropertyType: SearchPropertyType.Date));
                searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.ActivePhaseText, "Actieve Fase(n)", "Actieve Fase(n)", WIDTH_MEDIUM, true, 5));
                searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.FullDepartmentName, "Versturende organisatie", "Organisatie", WIDTH_X_LARGE, true, 6, useInSearchFilter: true));
                searchuiconfiguration.Fields.Add(newSearchUIField(true, WellKnownFieldNames.TyperingNazorgCombined, "Typering Nazorg uit Aanvraag", "Nazorg", WIDTH_SMALL, true, 7, useInSearchFilter: true));
                searchuiconfiguration.Fields.Add(newSearchUIField(false, WellKnownFieldNames.CareBeginDate, "Patient moet zorg ontvangen vanaf", "Start", WIDTH_SMALL, true, 8, useInSearchFilter: true, searchPropertyType: SearchPropertyType.Date));
            }
            return searchuiconfiguration;
        }
    }
}
