﻿using System.Linq;
using Point.Database.Models;
using Point.Database.Repository;

namespace Point.Business.Logic
{
    public class RazorTemplateBL
    {
        public static RazorTemplate GetByCode(IUnitOfWork uow, string code)
        {
            return uow.RazorTemplateRepository.Get(rt => rt.TemplateCode == code).FirstOrDefault();
        }
    }
}