﻿using Point.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using Point.Infrastructure.Extensions;

namespace Point.Business.Logic
{
    public class EnumBL
    {
        public static List<ListValue> GetEnumIntListValues<TEnum>() where TEnum : Enum
        {
            var enumlist = ((TEnum[])Enum.GetValues(typeof(TEnum))).ToList();
            return enumlist.Select(v => new ListValue()
            {
                Text = v.GetDescription(),
                Value = v.ToString("d")
            }).ToList();
        }

        public static List<ListValue> GetEnumCodeListValues<TEnum>() where TEnum : Enum
        {
            var enumlist = ((TEnum[])Enum.GetValues(typeof(TEnum))).ToList();
            return enumlist.Select(v => new ListValue()
            {
                Text = v.GetDescription(),
                Value = v.ToString()
            }).ToList();
        }
    }
}
