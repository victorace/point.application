﻿using System.Collections.Generic;
using System.Linq;
using Point.Database.Models;
using Point.Database.Repository;

namespace Point.Business.Logic
{
    public class OrganizationOutsidePointBL
    {

        public static IEnumerable<OrganizationOutsidePoint> GetAll(IUnitOfWork uow)
        {
            return uow.OrganizationOutsidePointRepository.GetAll().OrderBy(ocr => ocr.Name);
        }

        public static OrganizationOutsidePoint GetByID(IUnitOfWork uow, int organizationOutsidePointID)
        {
            return uow.OrganizationOutsidePointRepository.GetByID(organizationOutsidePointID);
        }

        public static OrganizationOutsidePoint GetByOrganizationID(IUnitOfWork uow, int organizationID)
        {
            return uow.OrganizationOutsidePointRepository.FirstOrDefault(o => o.OrganizationID == organizationID);
        }
    }
}
