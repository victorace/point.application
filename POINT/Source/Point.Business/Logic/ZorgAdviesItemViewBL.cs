﻿using System;
using System.Collections.Generic;
using System.Linq;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;

namespace Point.Business.Logic
{
    public class ZorgAdviesItemViewBL
    {
        public static List<ZorgAdviesItemViewModel> GetViewModel(IUnitOfWork uow, int flowinstanceid)
        {
            var zorgadviesitems = ZorgAdviesItemBL.GetAll(uow).ToList();
            var zorgadviesitemvalues = ZorgAdviesItemValueBL.GetByFlowInstanceID(uow, flowinstanceid).ToList();
            
            List<ZorgAdviesItemViewModel> viewmodel = new List<ZorgAdviesItemViewModel>();
            foreach (var item in zorgadviesitems)
	        {
	            var viewmodelitem = new ZorgAdviesItemViewModel
	            {
	                Section =
	                    item.ZorgAdviesItemGroup.ParentZorgAdviesItemGroupID != null
	                        ? item.ZorgAdviesItemGroup.ZorgAdviesItemGroupParent.GroupName
	                        : "",
	                Category = item.ZorgAdviesItemGroup.GroupName,
	                Label = item.Label,
	                NeedsFrequencyAndTime = item.NeedsFrequencyAndTime,
	                SubCategory = item.SubCategory,
	                ZorgAdviesItemID = item.ZorgAdviesItemID,
	                ActionBy = new ZorgAdviesItemViewModel.By[] {}
	            };

	            var value = zorgadviesitemvalues.FirstOrDefault(zpiv => zpiv.ZorgAdviesItemID == item.ZorgAdviesItemID);
                if(value != null)
                {
                    if (!String.IsNullOrEmpty(value.ActionBy))
                    {
                        var items = Array.ConvertAll(value.ActionBy.Split(','), s => (ZorgAdviesItemViewModel.By)Enum.Parse(typeof(ZorgAdviesItemViewModel.By), s));
                        viewmodelitem.ActionBy = items;
                    }

                    viewmodelitem.ActionFrequency = value.ActionFrequency;
                    viewmodelitem.ActionTime = value.ActionTime;
                    viewmodelitem.ActionTimespan = (ZorgAdviesItemViewModel.Timespan?)value.ActionTimespan;
                    viewmodelitem.Text = value.Text;
                    viewmodelitem.ZorgAdviesItemValueID = value.ZorgAdviesItemValueID;
                }

                viewmodel.Add(viewmodelitem);
	        }

            return viewmodel;
        }
    }
}
