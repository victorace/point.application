﻿using Point.Database.Models;
using Point.Database.Repository;
using Point.Infrastructure.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Point.Business.Logic
{
    public static class LogPrivacyBL
    {
        public static IEnumerable<int> GetDoneAttachmentIDsByOrganization(IUnitOfWork uow, int organizationID)
        {
            return uow.LogPrivacyRepository.Get(lp => lp.OrganizationID == organizationID && lp.AttachmentID != null).Select(lp => (int)lp.AttachmentID);
        }

        public static IEnumerable<int> GetDoneClientIDsByOrganization(IUnitOfWork uow, int organizationID)
        {
            return uow.LogPrivacyRepository.Get(lp => lp.OrganizationID == organizationID && lp.ClientID != null).Select(lp => (int)lp.ClientID);
        }

        public static string ClearData(IUnitOfWork uow)
        {
            string result = "";

            var organizationsettings = OrganizationSettingBL.GetAllByName(uow, OrganizationSettingBL.SetPrivacyAfterDays);

            foreach (var organizationsetting in organizationsettings.ToList())
            {
                result += $"OrganizationID: {organizationsetting.OrganizationID}, PrivacyAfterDays: {organizationsetting.Value}." + Environment.NewLine;
                int setprivacyafterdays = -1;
                if (Int32.TryParse(organizationsetting.Value, out setprivacyafterdays))
                {
                    var realizedbefore = DateTime.Now.AddDays(-setprivacyafterdays);
                    var flowinstances = FlowInstanceBL.GetClosedBeforeDateFlowInstancesByOrganizationID(uow, realizedbefore, organizationsetting.OrganizationID);
                    var transferids = flowinstances.Select(fi => fi.TransferID).AsEnumerable();

                    var doneattachmentids = LogPrivacyBL.GetDoneAttachmentIDsByOrganization(uow, organizationsetting.OrganizationID);
                    var attachmentidsforprivacy = TransferAttachmentBL.GetIDsByTransferIDs(uow, transferids);
                    attachmentidsforprivacy = attachmentidsforprivacy.Where(at => !doneattachmentids.Contains(at)).ToList();

                    result += "AttachmentIDs: ";
                    foreach(var attachmentid in attachmentidsforprivacy.OrderBy(at => at))
                    {
                        try
                        {
                            ClearAttachment(uow, attachmentid);
                            var logPrivacy = new LogPrivacy()
                            {
                                Action = $"Cleared attachment {attachmentid}",
                                AttachmentID = attachmentid,
                                OrganizationID = organizationsetting.OrganizationID,
                                Timestamp = DateTime.Now
                            };
                            Upsert(uow, logPrivacy);
                            result += $"{attachmentid},";
                        }
                        catch { /* ignore */ }
                    }
                    result += Environment.NewLine;

                    result += "ClientIDs: ";
                    var doneclientids = LogPrivacyBL.GetDoneClientIDsByOrganization(uow, organizationsetting.OrganizationID);
                    var clientidsforprivacy = ClientBL.GetIDsByTransferIDs(uow, transferids);
                    clientidsforprivacy = clientidsforprivacy.Where(cl => !doneclientids.Contains(cl)).ToList();
                    foreach(var clientid in clientidsforprivacy.OrderBy(cl => cl))
                    {
                        try
                        {
                            var flowinstanceid = flowinstances.Where(fi => fi.Transfer.ClientID == clientid).Select(fi => fi.FlowInstanceID).FirstOrDefault();

                            ObfuscateClient(uow, clientid, flowinstanceid);
                            var logPrivacy = new LogPrivacy()
                            {
                                Action = $"Obfuscated client {clientid}",
                                ClientID = clientid,
                                OrganizationID = organizationsetting.OrganizationID,
                                Timestamp = DateTime.Now
                            };
                            Upsert(uow, logPrivacy);
                            result += $"{clientid},";
                        }
                        catch { /* ignore */ }
                    }

                    foreach (var flowinstance in flowinstances.Where(fi => !fi.Deleted))
                    {
                        try
                        {
                            FlowInstanceSearchValuesBL.CalculateSearchValues(flowinstance.FlowInstanceID);
                        }
                        catch { /* ignore */ }
                    }

                    result += Environment.NewLine;
                }
            }


            return result;

        }

        private static void ObfuscateClient(IUnitOfWork uow, int clientID, int flowinstanceid)
        {
            var client = ClientBL.GetByClientID(uow, clientID);

            client.AddressGPForZorgmail = "0";
            client.BirthDate = new DateTime(1900, 1, 1, 1, 1, 1);
            client.City = client.City.Obfuscate();
            //client.CivilClass = client.CivilClass.Obfuscate(); //cant do that to enum
            client.CivilServiceNumber = client.CivilServiceNumber.Obfuscate();
            //client.CompositionHousekeeping = client.CompositionHousekeeping.Obfuscate(); //cant do that to enum
            client.Country = client.Country.Obfuscate();
            client.Email = client.Email.Obfuscate();
            client.FirstName = client.FirstName.Obfuscate();
            client.GeneralPractitionerName = client.GeneralPractitionerName.Obfuscate();
            client.GeneralPractitionerPhoneNumber = client.GeneralPractitionerPhoneNumber.Obfuscate();
            client.HealthCareProvider = client.HealthCareProvider.Obfuscate();
            client.HealthInsuranceCompanyID = 106;
            //client.HousingType = client.HousingType.Obfuscate(); //cant do that to enum
            client.HuisnummerToevoeging = client.HuisnummerToevoeging.Obfuscate();
            client.Initials = client.Initials.Obfuscate();
            client.InsuranceNumber = client.InsuranceNumber.Obfuscate();
            client.LastName = client.LastName.Obfuscate();
            client.MaidenName = client.MaidenName.Obfuscate();
            client.MiddleName = client.MiddleName.Obfuscate();
            client.Number = client.Number.Obfuscate();
            client.PartnerMiddleName = client.PartnerMiddleName.Obfuscate();
            client.PartnerName = client.PartnerName.Obfuscate();
            //client.PatientNumber <== keep
            client.PharmacyName = client.PharmacyName.Obfuscate();
            client.PharmacyPhoneNumber = client.PharmacyPhoneNumber.Obfuscate();
            client.PhoneNumberGeneral = client.PhoneNumberGeneral.Obfuscate();
            client.PhoneNumberMobile = client.PhoneNumberMobile.Obfuscate();
            client.PhoneNumberWork = client.PhoneNumberWork.Obfuscate();
            client.PostalCode = client.PostalCode.Obfuscate();
            client.Postbus = client.Postbus.Obfuscate();
            client.Salutation = client.Salutation.Obfuscate();
            client.StreetName = client.StreetName.Obfuscate();
            client.TemporaryCity = client.TemporaryCity.Obfuscate();
            client.TemporaryCountry = client.TemporaryCountry.Obfuscate();
            client.TemporaryNumber = client.TemporaryNumber.Obfuscate();
            client.TemporaryPostalCode = client.TemporaryPostalCode.Obfuscate();
            client.TemporaryStreetName = client.TemporaryStreetName.Obfuscate();
            //client.VisitNumber <== keep 

            foreach(var contactperson in ContactPersonBL.GetByClientID(uow, client.ClientID))
            {
                contactperson.PersonData.BirthDate = new DateTime(1900, 1, 1, 1, 1, 1);
                contactperson.PersonData.Email = contactperson.PersonData.Email.Obfuscate();
                contactperson.PersonData.FirstName = contactperson.PersonData.FirstName.Obfuscate();
                contactperson.PersonData.Initials = contactperson.PersonData.Initials.Obfuscate();
                contactperson.PersonData.LastName = contactperson.PersonData.LastName.Obfuscate();
                contactperson.PersonData.MaidenName = contactperson.PersonData.MaidenName.Obfuscate();
                contactperson.PersonData.MiddleName = contactperson.PersonData.MiddleName.Obfuscate();
                contactperson.PersonData.PartnerMiddleName = contactperson.PersonData.PartnerMiddleName.Obfuscate();
                contactperson.PersonData.PartnerName = contactperson.PersonData.PartnerName.Obfuscate();
                contactperson.PersonData.PhoneNumberGeneral = contactperson.PersonData.PhoneNumberGeneral.Obfuscate();
                contactperson.PersonData.PhoneNumberMobile = contactperson.PersonData.PhoneNumberMobile.Obfuscate();
                contactperson.PersonData.PhoneNumberWork = contactperson.PersonData.PhoneNumberWork.Obfuscate();
                contactperson.PersonData.Salutation = contactperson.PersonData.Salutation.Obfuscate();
            }

            uow.Save();

            return;
        }

        private static void ClearAttachment(IUnitOfWork uow, int attachmentID)
        {
            var attachment = TransferAttachmentBL.GetByID(uow, attachmentID);

            attachment.Name = (attachment.Name + " (verwijderd)").Truncate(50);
            attachment.GenericFile.FileData = new byte[0];
        
            uow.Save();
        }

        public static void Upsert(IUnitOfWork uow, LogPrivacy logPrivacy)
        {
            if (logPrivacy.LogPrivacyID <= 0)
            {
                uow.LogPrivacyRepository.Insert(logPrivacy);
            }
            
            uow.Save();
        }


    }
}
