﻿using Newtonsoft.Json;
using Point.Database.Context;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using System.Linq;

namespace Point.Business.Logic
{
    public class FormAdminViewModelBL
    {
        public static FormAdminViewModel GetMain()
        {
            using (var uow = new UnitOfWork<PointSearchContext>())
            {
                var fieldAdminViewModel = new FormAdminViewModel
                {
                    Items = uow.FlowFieldAttributeExceptionRepository.GetAll()
                    .Select(it => it.FlowFieldAttribute.FormType).Distinct()
                    .Select(it => new FormAdminViewModel.Item()
                    {
                        Name = it.Name,
                        FormTypeID = it.FormTypeID
                    }).ToList()
                };

                return fieldAdminViewModel;
            }
        }

        public static FormAdminFormDetailsViewModel GetFormDetails(int formTypeID)
        {
            using (var uow = new UnitOfWork<PointSearchContext>())
            {
                var fieldAdminFormDetailsViewModel = new FormAdminFormDetailsViewModel
                {
                    FormTypeID = formTypeID,
                    Name = uow.FormTypeRepository.FirstOrDefault(it => it.FormTypeID == formTypeID)?.Name,
                    Items = uow.FlowFieldAttributeExceptionRepository.Get(it => it.FlowFieldAttribute.FormTypeID == formTypeID)
                    .Select(it => new FormAdminFormDetailsViewModel.Item()
                    {
                        PhaseDefinitionID = it.PhaseDefinitionID,
                        RegionID = it.RegionID,
                        FlowName = it.PhaseDefinition.FlowDefinition.Name,
                        OrganizationID = it.OrganizationID,
                        PhaseName = it.PhaseDefinition.PhaseName,
                        RegionName = it.Region.Name,
                        OrganizationName = it.Organization.Name
                    }).Distinct().ToList()
                };

                return fieldAdminFormDetailsViewModel;
            }
        }

        public static FormAdminEditViewModel GetFieldAdminEdit(int formTypeID, int? phaseDefinitionID, int? regionID, int? organizationID)
        {
            using (var uow = new UnitOfWork<PointSearchContext>())
            {
                var viewmodel = new FormAdminEditViewModel
                {
                    FormTypeID = formTypeID,
                    PhaseDefinitionID = phaseDefinitionID,
                    RegionID = regionID,
                    OrganizationID = organizationID,
                    Items = uow.FlowFieldAttributeExceptionRepository.Get(it => 
                            it.FlowFieldAttribute.FormTypeID == formTypeID &&
                            it.PhaseDefinitionID == phaseDefinitionID &&
                            it.RegionID == regionID &&
                            it.OrganizationID == organizationID
                        ).ToList().Select(it => 
                            new FormAdminEditViewModel.Item()
                            {
                                FlowFieldAttributeExceptionID = it.FlowFieldAttributeExceptionID,
                                FlowFieldAttributeID = it.FlowFieldAttributeID,
                                Name = it.FlowFieldAttribute.FlowField.Name,
                                Label = it.FlowFieldAttribute.FlowField.Label,
                                OriginalReadOnly = it.FlowFieldAttribute.ReadOnly,
                                ReadOnly = it.ReadOnly,
                                OriginalVisible = it.FlowFieldAttribute.Visible,
                                Visible = it.Visible,
                                OriginalRequired = it.FlowFieldAttribute.Required,
                                Required = it.Required,
                                OriginalSignalOnChange = it.FlowFieldAttribute.SignalOnChange,
                                SignalOnChange = it.SignalOnChange,
                                OriginalRequiredByFlowFieldID = it.FlowFieldAttribute.RequiredByFlowFieldID,
                                RequiredByFlowFieldID = it.RequiredByFlowFieldID,
                                OriginalRequiredByFlowFieldValue = it.FlowFieldAttribute.RequiredByFlowFieldValue,
                                RequiredByFlowFieldValue = it.RequiredByFlowFieldValue
                            }
                        ).ToList()
                };

                return viewmodel;
            }
        }

        public static AttributeEditViewModel GetAttributeEdit(int flowFieldAttributeExceptionID)
        {
            using (var uow = new UnitOfWork<PointSearchContext>())
            {
                var item = uow.FlowFieldAttributeExceptionRepository.FirstOrDefault(it => it.FlowFieldAttributeExceptionID == flowFieldAttributeExceptionID);

                var flowfieldattributes = FlowFieldAttributeBL.GetByFormTypeID(uow, item.FlowFieldAttribute.FormTypeID).ToList();
                var flowfieldattributeOptions = flowfieldattributes.Select(a => new Option()
                {
                    Value = a.FlowFieldAttributeID.ToString(),
                    Text = a.FlowField.Label + " (" + a.FlowField.Name + ")"
                }).OrderBy(f => f.Text).ToList();

                var requiredByFlowField = flowfieldattributes.Select(a => new Option() {
                    Value = a.FlowFieldID.ToString(),
                    Text = a.FlowField.Label + " (" + a.FlowField.Name + ")"
                }).OrderBy(f => f.Text).ToList();
                requiredByFlowField.Insert(0, new Option() { Value = "", Text = " - Geen - " });

                var viewmodel = new AttributeEditViewModel
                {
                    FlowFieldAttributeExceptionID = item.FlowFieldAttributeExceptionID,
                    FlowFieldAttributeID = item.FlowFieldAttributeID,
                    FlowFieldAttributeOptions = flowfieldattributeOptions,
                    Name = item.FlowFieldAttribute.FlowField.Name,
                    Label = item.FlowFieldAttribute.FlowField.Label,
                    ReadOnly = item.ReadOnly,
                    Visible = item.Visible,
                    Required = item.Required,
                    SignalOnChange = item.SignalOnChange,
                    RequiredByFlowFieldID = item.RequiredByFlowFieldID,
                    RequiredByFlowFieldValue = item.RequiredByFlowFieldValue, 
                    RequiredByFlowFieldOptions = requiredByFlowField
                };

                return viewmodel;
            }
        }

        public static AttributeAddViewModel GetAttributeAdd(int formTypeID, int? phaseDefinitionID, int? regionID, int? organizationID)
        {
            using (var uow = new UnitOfWork<PointSearchContext>())
            {
                var flowfieldattributes = FlowFieldAttributeBL.GetByFormTypeID(uow, formTypeID).ToList();
                var flowfieldattributeOptions = flowfieldattributes.Select(a => new Option()
                {
                    Value = a.FlowFieldAttributeID.ToString(),
                    Text = a.FlowField.Label + " (" + a.FlowField.Name + ")"
                }).OrderBy(f => f.Text).ToList();

                var requiredByFlowField = flowfieldattributes.Select(a => new Option()
                {
                    Value = a.FlowFieldID.ToString(),
                    Text = a.FlowField.Label + " (" + a.FlowField.Name + ")"
                }).OrderBy(f => f.Text).ToList();
                requiredByFlowField.Insert(0, new Option() { Value = "", Text = " - Geen - " });

                var viewmodel = new AttributeAddViewModel
                {
                    FormTypeID = formTypeID,
                    PhaseDefinitionID = phaseDefinitionID,
                    RegionID = regionID,
                    OrganizationID = organizationID,
                    FlowFieldAttributeOptions = flowfieldattributeOptions,
                    RequiredByFlowFieldOptions = requiredByFlowField
                };

                return viewmodel;
            }
        }

        public static AddFormDetailsViewModel GetAddFormDetails(int formTypeID)
        {
            using (var uow = new UnitOfWork<PointSearchContext>())
            {
                var flowFieldAttributeoptions = FlowFieldAttributeBL.GetByFormTypeID(uow, formTypeID).ToList()
                    .Select(a => new Option()
                    {
                        Value = a.FlowFieldAttributeID.ToString(),
                        Text = a.FlowField.Label + " (" + a.FlowField.Name + ")"
                    }).OrderBy(f => f.Text).ToList();

                var phasedefinitionsoptions = PhaseDefinitionCacheBL.GetPhaseDefinitions(uow)
                    .Where(p => p.FormTypeID == formTypeID)
                    .OrderBy(d => d.FlowDefinitionID).ThenBy(d => d.Phase)
                    .Select(a => new Option()
                    {
                        Value = a.PhaseDefinitionID.ToString(),
                        Text = a.FlowDefinitionID.ToString() + " - " + a.PhaseName
                    }).ToList();
                    phasedefinitionsoptions.Insert(0, new Option() { Value = "", Text = " - Geen - " });

                var regionoptions = RegionBL.GetAll(uow)
                    .Select(a => new Option()
                    {
                        Value = a.RegionID.ToString(),
                        Text = a.Name
                    }).OrderBy(f => f.Text).ToList();
                    regionoptions.Insert(0, new Option() { Value = "", Text = " - Geen - " });

                var organizationsoptions = OrganizationBL.GetActive(uow)
                    .Select(a => new Option()
                    {
                        Value = a.OrganizationID.ToString(),
                        Text = a.Name
                    }).OrderBy(f => f.Text).ToList();
                    organizationsoptions.Insert(0, new Option() { Value = "", Text = " - Geen - " });

                var fieldAdminFormDetailsViewModel = new AddFormDetailsViewModel
                {
                    FlowFieldAttributeOptions = flowFieldAttributeoptions,
                    PhaseDefinitionOptions = phasedefinitionsoptions,
                    RegionOptions = regionoptions,
                    OrganizationOptions = organizationsoptions,
                    FormTypeID = formTypeID,
                };

                return fieldAdminFormDetailsViewModel;
            }
        }
    }
}
