﻿using Point.Database.Extensions;
using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Point.Business.Logic
{
    public class DashboardMenuItemViewBL
    {
        public const string EMPTY           = "";
        public const string NOT_APPLICABLE  = "Nog niet ingevuld of nvt";
        public const string ACTUAL          = "Actueel";
        public const string DONE            = "Definitief";
        public const string ORDER_CONFIRMED = "Order bevestigd";
        public const string NOT_DONE        = "Nog niet definitief";
        public const string SENT            = "Verzonden";
        public const string NOT_SENT        = "Nog niet verzonden";
        public const string CLOSED          = "Gesloten";
        public const string UNKNOWN         = "Onbekend";
        public const string NEED_UPDATE     = "NeedUpdateOrderStatus";
        public const string ORDER_FAILED    = "Order mislukt";
        public const string ORDER_PROCESSING = "In behandeling";

        public static IEnumerable<DashboardMenuItemViewModel> Get(IUnitOfWork uow, FlowInstance flowinstance, PointUserInfo pointuserinfo)
        {
            var dashboard = new List<DashboardMenuItemViewModel>();

            var selectablephasedefinitions = PhaseDefinitionCacheBL.GetByFlowDefinitionID(uow, flowinstance.FlowDefinitionID)
                                             .Where(pd => pd.DashboardMenuItemType != MenuItemType.None).ToList();

            var selectablephasedefinitionids = selectablephasedefinitions.Select(pd => pd.PhaseDefinitionID).ToArray();

            var selectablephaseinstances = PhaseInstanceBL.GetByFlowinstanceIDAndPhaseDefinitionIDs(uow,flowinstance.FlowInstanceID, selectablephasedefinitionids).ToList();

            var phasedefinitionidswithphaseinstance = selectablephaseinstances
                .Select(pi => pi.PhaseDefinitionID)
                .ToList();

            foreach (var phasedefinition in selectablephasedefinitions.Where(pd => !phasedefinitionidswithphaseinstance.Contains(pd.PhaseDefinitionID)))
            {
                var menuitemstatus = MenuBL.GetMenuItemStatus(uow, phasedefinition, new List<PhaseInstance>(), pointuserinfo, MenuItemDestination.Dashboard);
                if (menuitemstatus == MenuItemStatus.Visible)
                {
                    dashboard.Add(new DashboardMenuItemViewModel()
                    {
                        TransferID = flowinstance.TransferID,
                        PhaseDefinitionID = phasedefinition.PhaseDefinitionID,
                        FormTypeID = phasedefinition.FormTypeID,
                        DashboardMenuItemType = phasedefinition.DashboardMenuItemType,
                        Status = (int)Status.NotActive,
                        Label = phasedefinition.PhaseName,
                        Text = NOT_APPLICABLE,
                        Order = phasedefinition.OrderNumber,
                    });
                }
            }

            foreach (var phaseinstance in selectablephaseinstances)
            {
                DateTime? processtimestamp = phaseinstance.GetProcessDateTime();

                var formsetversion = phaseinstance.FormSetVersion.FirstOrDefault();

                dashboard.Add(new DashboardMenuItemViewModel()
                {
                    TransferID = flowinstance.TransferID,
                    PhaseDefinitionID = phaseinstance.PhaseDefinitionID,
                    FormTypeID = phaseinstance.PhaseDefinition.FormTypeID,
                    PhaseInstanceID = phaseinstance.PhaseInstanceID,
                    DashboardMenuItemType = phaseinstance.PhaseDefinition.DashboardMenuItemType,
                    Status = (int)phaseinstance?.Status,
                    Label = getFormName(phaseinstance),
                    Text = getText(uow, phaseinstance, processtimestamp),
                    ProcessDateTime = processtimestamp,
                    LastModifiedOn = formsetversion?.UpdateDate ?? formsetversion?.CreateDate,
                    LastModifiedBy = formsetversion?.UpdatedBy ?? formsetversion?.CreatedBy,
                    HasPhaseInstance = true,
                    Order = phaseinstance.PhaseDefinition.OrderNumber,
                });
            }

            return dashboard.OrderBy(dmi => dmi.Order)
                .ThenBy(dmi => dmi.LastModifiedOn);
        }
        private static string getText(IUnitOfWork uow, PhaseInstance phaseinstance, DateTime? processdate)
        {
            string text = NOT_APPLICABLE;

            if (phaseinstance == null)
                return text;

            int status = phaseinstance.Status;
            decimal phase = phaseinstance.PhaseDefinition.Phase;
            bool hasdefinitivebutton = phaseinstance.PhaseDefinition.HasDefinitiveButton();

            bool isfrequencyform = false;
            bool isorderform = false;

            var formsetversion = phaseinstance.FormSetVersion.FirstOrDefault();
            if (formsetversion != null)
            {
                isfrequencyform = formsetversion.FormType.TypeID == (int)TypeID.FlowDoorlopendForm;
                isorderform = formsetversion.FormType.FormTypeID == (int)FlowFormType.Hulpmiddelen;
            }

            if (status == (int)Status.Active)
            {
                if (phase == -1 && hasdefinitivebutton)
                {
                    text = (isfrequencyform ? ACTUAL : NOT_DONE);
                }
                else if (phase == -1 && !hasdefinitivebutton)
                {
                    text = EMPTY;
                }
                else
                {
                    text = NOT_SENT;
                }
            }
            else if (status == (int)Status.Done)
            {
                if (phase == -1 && hasdefinitivebutton)
                {
                    if (isfrequencyform)
                    {
                        text = CLOSED;
                    }
                    else
                    {
                        if (isorderform)
                        {
                            string orderstatus = FlowFieldValueBL.GetValueByFlowFieldIDAndFormSetVersionID(uow, FlowFieldConstants.ID_OrderStatus, formsetversion.FormSetVersionID);
                            if (String.IsNullOrEmpty(orderstatus))
                            {
                                orderstatus = ORDER_CONFIRMED;
                            }

                            text = orderstatus + (processdate == null ? "" : string.Format(" ({0})", processdate.ToPointDateHourDisplay()));
                        }
                        else
                        {
                            text = DONE + (processdate == null ? "" : string.Format(" ({0})", processdate.ToPointDateHourDisplay()));
                        }
                    }
                }
                else if (phase == -1 && !hasdefinitivebutton)
                {
                    text = EMPTY;
                }
                else
                {
                    text = SENT + (processdate == null ? "" : string.Format(" ({0})", processdate.ToPointDateHourDisplay()));
                }
            }

            return text;
        }

        private static string getFormName(PhaseInstance phaseinstance)
        {
            if (phaseinstance == null)
                return null;

            var formsetversion = phaseinstance.FormSetVersion.FirstOrDefault();
            if (formsetversion != null)
            {
                if (!string.IsNullOrWhiteSpace(formsetversion.Description))
                {
                    return formsetversion.Description;
                }
            }

            return phaseinstance?.PhaseDefinition?.PhaseName;
        }
    }
}