﻿using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Point.Business.Logic
{
    public class FormTypesForRegionBL
    {
        public static List<FormTypeForRegion> GetByRegionIDAndFlowDefinitionID(IUnitOfWork uow, int regionID, FlowDefinitionID flowDefinitionID)
        {
            var selectedFormTypes = FormTypeRegionBL.GetFormTypeRegionsByRegionIDAndFlowDefinitionID(uow, regionID, flowDefinitionID).ToList();

            var formTypeRegions = new List<FormTypeForRegion>();
            foreach (var ss in FlowDefinitionFormTypeBL.GetByFlowDefinitionID(uow, flowDefinitionID))
            {
                var cm = new FormTypeForRegion
                {
                    FormTypeID = ss.FormTypeID,
                    Name = ss.FormType.Name,
                    IsSelected = selectedFormTypes.Any(sft => ss.FormTypeID == sft.FormTypeID)
                };
                formTypeRegions.Add(cm);
            }

            return formTypeRegions;
        }

        public static Dictionary<FlowDefinitionID, List<FormTypeForRegion>> GetByRegionID(IUnitOfWork uow, int regionID)
        {
            // create a list of new form types and set which form type is acive for this region. (in edit mode)
            var dict = new Dictionary<FlowDefinitionID, List<FormTypeForRegion>>();

            var allflowdefinitions = FlowDefinitionBL.GetActiveFlowDefinitions(uow).ToList();
            foreach (var flow in allflowdefinitions)
            {
                dict.Add(flow.FlowDefinitionID, GetByRegionIDAndFlowDefinitionID(uow, regionID, flow.FlowDefinitionID));
            }

            return dict;
        }
    }
}
