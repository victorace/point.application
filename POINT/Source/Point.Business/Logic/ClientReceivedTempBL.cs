﻿using System.Linq;
using Point.Database.Models;
using Point.Database.Repository;

namespace Point.Business.Logic
{
    public static class ClientReceivedTempBL
    {
        public static ClientReceivedTemp Get(IUnitOfWork uow, int organisationId, string patientNumber)
        {
            return uow.ClientReceivedTempRepository.GetAll().OrderByDescending(cr => cr.TimeStamp).FirstOrDefault(cr => cr.PatientNumber == patientNumber && cr.OrganisationID == organisationId);
        }
    }
}
