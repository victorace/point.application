﻿using System.Collections.Generic;
using System.Linq;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;

namespace Point.Business.Logic
{
    public class RegionViewModelBL
    {
        private static RegionViewModel FromModel(IUnitOfWork uow, Region region)
        {
            if (region == null)
            {
                return null;
            }

            var vmRegion = new RegionViewModel
            {
                RegionID = region.RegionID,
                Name = region.Name,
                CapacityBeds = region.CapacityBeds,
                CapacityBedsPublic = region.CapacityBedsPublic
            };

            return vmRegion;
        }
        
        public static List<RegionViewModel> FromModelList(IUnitOfWork uow, string name)
        {
            var viewmodel = new List<RegionViewModel>();

            var regions = RegionBL.GetAll(uow);
            if (!string.IsNullOrEmpty(name))
            {
                regions = regions.Where(reg => reg.Name.ToLower().Contains(name.Trim().ToLower())).ToList();
            }

            foreach (var reg in regions)
            {
                viewmodel.Add(FromModel(uow, reg));
            }
            return viewmodel.OrderBy(reg => reg.Name).ToList();
        }

        public static RegionViewModel GetViewModelByID(IUnitOfWork uow, int regionID)
        {
            var region = RegionBL.GetByRegionID(uow, regionID);
            return FromModel(uow, region);
        }

        public static RegionViewModel GetViewModelByName(IUnitOfWork uow, string name)
        {
            var region = RegionBL.GetByRegionName(uow, name);
            return FromModel(uow,region);
        }

        public static RegionViewModel Save(IUnitOfWork uow, RegionViewModel viewmodel, PointUserInfo pointUserInfo, LogEntry logEntry)
        {
            
            var model = RegionBL.Save(uow, viewmodel, pointUserInfo, logEntry);

            return FromModel(uow, model);            
        }
    }
}
