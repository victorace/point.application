﻿using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Point.Business.Logic
{
    public class FlowDefinitionFormTypeBL
    {
        public static List<FlowDefinitionFormType> GetByFlowDefinitionID(IUnitOfWork uow, FlowDefinitionID flowDefinitionID)
        {
            return uow.FlowDefinitionFormTypeRepository.Get(fdft => fdft.FlowDefinitionID == flowDefinitionID).ToList();
        }
    }
}
