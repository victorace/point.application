﻿using System.Collections.Generic;
using System.Linq;
using Point.Database.Models;
using Point.Database.Repository;

namespace Point.Business.Logic
{
    public class NationalityBL
    {
        public static IEnumerable<Nationality> GetActiveNationalities(IUnitOfWork uow)
        {
            return uow.NationalityRepository.Get(n => !n.InActive).OrderBy(n => n.Name);
        }
    }
}
