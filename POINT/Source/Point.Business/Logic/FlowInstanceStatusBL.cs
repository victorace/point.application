﻿using System;
using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using System.Linq;

namespace Point.Business.Logic
{
    public class FlowInstanceStatusBL
    {
        #region privates
        private static FlowInstanceStatus createFlowInstanceStatus(IUnitOfWork uow, int flowinstanceid, DateTime statusdate, string comment,
            FlowInstanceStatusTypeID flowinstancestatustypeid, LogEntry logentry)
        {
            if (statusdate.TimeOfDay == TimeSpan.Zero)
                statusdate = statusdate + DateTime.Now.TimeOfDay;

            var model = new FlowInstanceStatus
            {
                StatusDate = statusdate,
                Comment = comment,
                EmployeeID = logentry.EmployeeID,
                FlowInstanceID = flowinstanceid,
                FlowInstanceStatusTypeID = flowinstancestatustypeid,
            };

            uow.FlowInstanceStatusRepository.Insert(model);

            LoggingBL.FillLogging(uow, model, logentry);
            uow.Save();

            return model;
        }
        #endregion

        public static FlowInstanceStatus CreateReopenTransfer(IUnitOfWork uow, FlowInstance flowInstance, LogEntry logentry)
        {
            return createFlowInstanceStatus(uow, flowInstance.FlowInstanceID, DateTime.Now, "", FlowInstanceStatusTypeID.Heropenen, logentry);
        }

        public static FlowInstanceStatus CreateCloseTransfer(IUnitOfWork uow, FlowInstance flowInstance, LogEntry logentry)
        {
            if (flowInstance.Interrupted)
            {
                CreateResume(uow, flowInstance.FlowInstanceID, new FlowResumeViewModel
                    {
                        ResumeDate = DateTime.Now,
                        ResumeReason = "Hervatten voordat dossier kan worden afgesloten",
                        SignalOnSave = false
                    }, logentry);
            }
            return createFlowInstanceStatus(uow, flowInstance.FlowInstanceID, DateTime.Now, "", FlowInstanceStatusTypeID.Afsluiten, logentry);
        }

        public static FlowInstanceStatus CreateInterruption(IUnitOfWork uow, int flowinstanceid, FlowInterruptionViewModel viewmodel, LogEntry logentry)
        {
            var flowinstance = FlowInstanceBL.GetByID(uow, flowinstanceid);
            flowinstance.Interrupted = true;
            flowinstance.InterruptedDate = viewmodel.InterruptionDate;
            flowinstance.InterruptedByEmployeeID = logentry.EmployeeID;

            return createFlowInstanceStatus(uow, flowinstanceid, viewmodel.InterruptionDate, viewmodel.InterruptionReason, FlowInstanceStatusTypeID.Onderbreken, logentry);
        }

        public static FlowInstanceStatus CreateResume(IUnitOfWork uow, int flowinstanceid, FlowResumeViewModel viewmodel, LogEntry logentry)
        {
            var flowinstance = FlowInstanceBL.GetByID(uow, flowinstanceid);
            flowinstance.Interrupted = false;
            flowinstance.InterruptedDate = null;
            flowinstance.InterruptedByEmployeeID = null;

            return createFlowInstanceStatus(uow, flowinstanceid, viewmodel.ResumeDate, viewmodel.ResumeReason, FlowInstanceStatusTypeID.Hervatten, logentry);
        }

        public static FlowInstanceStatus GetLastClosed(IUnitOfWork uow, int flowinstanceid)
        {
            return uow.FlowInstanceStatusRepository.Get(fis => fis.FlowInstanceID == flowinstanceid && fis.FlowInstanceStatusTypeID == FlowInstanceStatusTypeID.Afsluiten)
                .OrderByDescending(fis => fis.FlowInstanceStatusID).FirstOrDefault();
        }
        
        public static double? GetInterruptionDays(IUnitOfWork uow, int flowinstanceid)
        {
            var interruptiontypes = new FlowInstanceStatusTypeID[] { FlowInstanceStatusTypeID.Onderbreken, FlowInstanceStatusTypeID.Hervatten };
            var allstatussesbytime = uow.FlowInstanceStatusRepository.Get(s => 
                s.FlowInstanceID == flowinstanceid && interruptiontypes.Contains(s.FlowInstanceStatusTypeID))
                .OrderBy(s => s.FlowInstanceStatusID).ToList();

            double? totalinterruptiondays = null;
            DateTime? startdatetime = null;
            foreach (var item in allstatussesbytime)
            {
                if(item.FlowInstanceStatusTypeID == FlowInstanceStatusTypeID.Onderbreken)
                {
                    startdatetime = item.StatusDate;
                }
                if (item.FlowInstanceStatusTypeID == FlowInstanceStatusTypeID.Hervatten && startdatetime.HasValue)
                {
                    if(totalinterruptiondays == null)
                    {
                        totalinterruptiondays = 0;
                    }

                    totalinterruptiondays += (item.StatusDate - startdatetime.Value).TotalHours / 24.0;
                    startdatetime = null;
                }
            }

            return totalinterruptiondays;
        }
    }
}
