﻿using Point.Infrastructure.Extensions;
using Point.Infrastructure.Helpers;
using System.Collections.Generic;
using System.Data;

namespace Point.Business.Logic.TemplateDataSets
{
    public class AsyncReportMail
    {
        private string downloadlink;
        public AsyncReportMail(string downloadlink)
        {
            this.downloadlink = downloadlink;
        }

        public DataSet Get()
        {
            var dataset = new DataSet();
            dataset.SafeAddTable(addDownloadLinkTable());
            return dataset;
        }

        private DataTable addDownloadLinkTable()
        {
            DataTable datatable = DataTableHelper.CreateWithColumnList("DownloadLink", new List<string>() { "AsyncReportDownloadLink" });
            datatable.FirstRow().SetColumnValue("AsyncReportDownloadLink", this.downloadlink);

            datatable.AcceptChanges();
            return datatable;
        }

    }
}
