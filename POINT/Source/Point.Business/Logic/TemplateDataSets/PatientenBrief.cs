﻿using Newtonsoft.Json;
using Point.Business.Helper;
using Point.Database.Extensions;
using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Extensions;
using Point.Infrastructure.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Point.Business.Logic.TemplateDataSets
{
    public class PatientenBrief
    {
        private class Delivery
        {
            public int FormSetVersionID { get; set; }
            public DateTime Date { get; set; }
            public string Time { get; set; }

            public Delivery(int formsetversionid, DateTime date, string time)
            {
                FormSetVersionID = formsetversionid;
                Date = date;
                Time = time;
            }

            public new string ToString()
            {
                var result = "";

                if (Date != null && Date != DateTime.MinValue)
                {
                    result = $"{Date.ToPointLeterDate()}";
                    if (!string.IsNullOrEmpty(result) && !string.IsNullOrEmpty(Time))
                    {
                        result = $"{result} {Time}";
                    }
                }

                return result;
            }
        }

        private IUnitOfWork uow;
        private FlowInstance flowinstance;
        private PointUserInfo pointuserinfo;

        public PatientenBrief(IUnitOfWork uow, FlowInstance flowinstance, PointUserInfo pointuserinfo)
        {
            this.uow = uow ?? throw new ArgumentException(nameof(uow));
            this.flowinstance = flowinstance ?? throw new ArgumentException(nameof(flowinstance));
            this.pointuserinfo = pointuserinfo ?? throw new ArgumentException(nameof(pointuserinfo));
        }

        public DataSet Get()
        {
            var dataset = new DataSet();

            dataset.SafeAddTable(addClientTable());

            dataset.SafeAddTable(addSendingOrganizationTable());
            dataset.SafeAddTable(addSendingLocationTable());
            dataset.SafeAddTable(addSendingDepartmentTable());
            dataset.SafeAddTable(addSendingEmployee());

            dataset.SafeAddTable(addReceivingDepartmentTable());

            dataset.SafeAddTable(addAfterCareCategory());
            dataset.SafeAddTable(addAfterCareFinancing());

            dataset.SafeAddTable(addNazorg());

            dataset.SafeAddTable(addAidProductOrderItem());

            return dataset;
        }

        private DataTable addClientTable()
        {
            var client = flowinstance?.Transfer?.Client;
            if (client == null)
            {
                return null;
            }

            var fullname = client?.FullName();
            var datatable = client.ConvertToDataTable("Client", new List<string>() { "ClientNameFormatted", "FullName" });
            datatable.FirstRow().SetColumnValue("ClientNameFormatted", fullname);
            datatable.FirstRow().SetColumnValue("FullName", fullname);
            datatable.AcceptChanges();
            return datatable;
        }

        private DataTable addSendingDepartmentTable()
        {
            var department = flowinstance?.StartedByDepartment;
            if (department == null)
            {
                return null;
            }

            var datatable = department.ConvertToDataTable("Department", new List<string>() { "FullName" });
            datatable.FirstRow().SetColumnValue("FullName", department?.FullName());
            datatable.AcceptChanges();
            return datatable;
        }

        private DataTable addSendingLocationTable()
        {
            var location = flowinstance?.StartedByDepartment?.Location;
            if (location == null) return null;

            var datatable = location.ConvertToDataTable("Location", new List<string>() { "FullName" });
            datatable.FirstRow().SetColumnValue("FullName", location?.FullName());
            datatable.AcceptChanges();
            return datatable;
        }

        private DataTable addSendingOrganizationTable()
        {
            var organization = flowinstance.StartedByOrganization;
            if (organization == null)
            {
                return null;
            }

            var datatable = organization.ConvertToDataTable("Organization");
            datatable.AcceptChanges();
            return datatable;
        }

        private DataTable addSendingEmployee()
        {
            var organization = pointuserinfo?.Organization;
            var department = pointuserinfo?.Department;
            var location = department?.Location;
            var employee = pointuserinfo?.Employee;

            DataTable datatable = DataTableHelper.CreateWithColumnList("SendingEmployee", new List<string>() {
                "OrganizationName", "OrganizationFullName", "LocationName", "LocationFullName",
                "DepartmentName", "DepartmentFullName", "DepartmentPhoneNumber", "DepartmentEmailAddress",
                "EmployeeFullName", "EmployeePhoneNumber", "EmployeeEmailAddress"
            });

            datatable.FirstRow().SetColumnValue("OrganizationName", organization?.Name);
            datatable.FirstRow().SetColumnValue("LocationName", location?.Name);
            datatable.FirstRow().SetColumnValue("LocationFullName", location?.FullName());
            datatable.FirstRow().SetColumnValue("DepartmentName", department?.Name);
            datatable.FirstRow().SetColumnValue("DepartmentFullName", department?.FullName());
            datatable.FirstRow().SetColumnValue("DepartmentPhoneNumber", department?.PhoneNumber);
            datatable.FirstRow().SetColumnValue("DepartmentEmailAddress", department?.EmailAddress);
            datatable.FirstRow().SetColumnValue("EmployeeFullName", employee?.FullName());
            datatable.FirstRow().SetColumnValue("EmployeePhoneNumber", employee?.PhoneNumber);
            datatable.FirstRow().SetColumnValue("EmployeeEmailAddress", employee?.EmailAddress);

            return datatable;
        }

        private DataTable addReceivingDepartmentTable()
        {
            var department = OrganizationHelper.GetRecievingDepartment(uow, flowinstance.FlowInstanceID);
            if (department == null)
            {
                return null;
            }

            var datatable = department.ConvertToDataTable("ReceivingDepartment", new List<string>() { "FullName" });
            datatable.FirstRow().SetColumnValue("FullName", department?.FullName());
            datatable.AcceptChanges();
            return datatable;
        }

        private DataTable addAfterCareCategory()
        {
            AfterCareCategory aftercarecategory = null;

            var aftercarecategorydefinitive = FlowFieldValueBL.GetByFlowInstanceIDAndFieldID(uow, flowinstance.FlowInstanceID, FlowFieldConstants.ID_AfterCareCategoryDefinitive).FirstOrDefault();
            if (aftercarecategorydefinitive != null)
            {
                int aftercarecategoryid = int.TryParse(aftercarecategorydefinitive.Value, out aftercarecategoryid) ? aftercarecategoryid : 0;
                aftercarecategory = AfterCareCategoryBL.Get(uow, aftercarecategoryid);
            }

            if (aftercarecategory == null)
            {
                return null;
            }

            string startdate = null;
            var dischargeproposedstartdate = FlowFieldValueBL.GetByFlowInstanceIDAndFieldID(uow, flowinstance.FlowInstanceID, FlowFieldConstants.ID_DischargeProposedStartDate).FirstOrDefault();
            if (!string.IsNullOrEmpty(dischargeproposedstartdate?.Value))
            {
                DateTime date = DateTime.MinValue;
                if (DateTime.TryParse(dischargeproposedstartdate?.Value, out date))
                {
                    startdate = $"{date.ToPointLeterDate()}";
                    if (!string.IsNullOrEmpty(startdate))
                    {
                        if (aftercarecategory.AfterCareCategoryID == (int)AfterCareCategoryID.Thuiszorg)
                        {
                            var dischargehomecarebegintime = FlowFieldValueBL.GetByFlowInstanceIDAndFieldID(uow, flowinstance.FlowInstanceID, FlowFieldConstants.ID_DischargeHomecareBeginTime).FirstOrDefault();
                            var dischargehomecareendtime = FlowFieldValueBL.GetByFlowInstanceIDAndFieldID(uow, flowinstance.FlowInstanceID, FlowFieldConstants.ID_DischargeHomecareEndTime).FirstOrDefault();
                            if (!string.IsNullOrEmpty(dischargehomecarebegintime?.Value) && !string.IsNullOrEmpty(dischargehomecareendtime?.Value))
                            {
                                startdate = $"{startdate} tussen {dischargehomecarebegintime.Value} en {dischargehomecareendtime.Value}";
                            }
                        }
                        else if (aftercarecategory.AfterCareCategoryID == (int)AfterCareCategoryID.Zorginstelling ||
                            aftercarecategory.AfterCareCategoryID == (int)AfterCareCategoryID.Hospice)
                        {
                            var dischargepatientexpectedtime = FlowFieldValueBL.GetByFlowInstanceIDAndFieldID(uow, flowinstance.FlowInstanceID, FlowFieldConstants.ID_DischargePatientExpectedTime).FirstOrDefault();
                            if (!string.IsNullOrEmpty(dischargepatientexpectedtime?.Value))
                            {
                                startdate = $"{startdate} om {dischargepatientexpectedtime.Value}";
                            }
                        }
                    }
                }
            }

            DataTable datatable = DataTableHelper.CreateWithColumnList("AfterCareCategory", new List<string>() { "StartDate" });
            datatable.FirstRow().SetColumnValue("StartDate", startdate);

            datatable.AcceptChanges();
            return datatable;
        }

        private DataTable addAfterCareFinancing()
        {
            AfterCareFinancing aftercarefinancing = null;

            var flowwebfield = FlowFieldValueBL.GetByFlowInstanceIDAndFieldID(uow, flowinstance.FlowInstanceID, FlowFieldConstants.ID_AfterCareFinancing).FirstOrDefault();
            if (flowwebfield != null)
            {
                int aftercarefinancingid = int.TryParse(flowwebfield.Value, out aftercarefinancingid) ? aftercarefinancingid : 0;
                aftercarefinancing = AfterCareFinancingBL.GetById(uow, aftercarefinancingid);
            }

            if (aftercarefinancing == null)
            {
                return null;
            }

            var datatable = aftercarefinancing.ConvertToDataTable("AfterCareFinancing");
            datatable.AcceptChanges();
            return datatable;
        }

        private DataTable addNazorg()
        {
            var nazorgItems = new List<string>();

            var nazorgfieldvalues = FlowFieldValueBL.GetByFlowInstanceIDAndFormTypeIDAndFieldIDs(uow, flowinstance.FlowInstanceID, (int)FlowFormType.AanvraagFormulierZHVVT,
                new int[] {
                    FlowFieldConstants.ID_BasiszorgPersoonlijkeVerzorging,
                    FlowFieldConstants.ID_BasiszorgTransferBedStoel,
                    FlowFieldConstants.ID_BasiszorgMobiliteit,
                    FlowFieldConstants.ID_BasiszorgEtenEnDrinken,
                    FlowFieldConstants.ID_BasiszorgToiletgang,
                    FlowFieldConstants.ID_MedicatieToediening,
                    FlowFieldConstants.ID_MedicatieToezichtOpInname,
                    FlowFieldConstants.ID_ControleLichaamsfunctiesZuurstoftoediening,
                    FlowFieldConstants.ID_ControleLichaamsfunctiesBloedsuikerControle,
                    FlowFieldConstants.ID_ControleLichaamsfunctiesAnders,
                    FlowFieldConstants.ID_Wondverzorging,
                    FlowFieldConstants.ID_Stomaverzorging,
                    FlowFieldConstants.ID_Drainverzorging,
                    FlowFieldConstants.ID_CatheterVerzorging,
                    FlowFieldConstants.ID_Infusietherapie,
                    FlowFieldConstants.ID_Sondevoeding,
                    FlowFieldConstants.ID_NierfunctievervangendeTherapie,
                    FlowFieldConstants.ID_TransfusiesBloed,
                    FlowFieldConstants.ID_OverigeVerrichtingen });

            foreach (var item in nazorgfieldvalues.ToList().Where(it => it.Value.ToLogical()).OrderBy(it => it.Value))
            {
                var itemtext = $" • {item.FlowField.Label}";
                var subvalues = Enumerable.Empty<FlowFieldValue>();

                if (item.FlowFieldID == FlowFieldConstants.ID_MedicatieToediening)
                {
                    subvalues = FlowFieldValueBL.GetByFlowInstanceIDAndFormTypeIDAndFieldIDs(uow, flowinstance.FlowInstanceID, (int)FlowFormType.AanvraagFormulierZHVVT,
                        new int[] {
                            FlowFieldConstants.ID_MedicatieToedieningPerOs,
                            FlowFieldConstants.ID_MedicatieToedieningPerInjectie,
                            FlowFieldConstants.ID_MedicatieToedieningInhalatieVerneveling,
                            FlowFieldConstants.ID_MedicatieToedieningDruppelen,
                            FlowFieldConstants.ID_MedicatieToedieningZalven });
                }
                else if (item.FlowFieldID == FlowFieldConstants.ID_CatheterVerzorging)
                {
                    subvalues = FlowFieldValueBL.GetByFlowInstanceIDAndFormTypeIDAndFieldIDs(uow, flowinstance.FlowInstanceID, (int)FlowFormType.AanvraagFormulierZHVVT,
                        new int[] {
                            FlowFieldConstants.ID_CatheterVerzorgingSuprapubisCatheter,
                            FlowFieldConstants.ID_CatheterVerzorgingBlaascatheter });
                }
                else if (item.FlowFieldID == FlowFieldConstants.ID_Infusietherapie)
                {
                    subvalues = FlowFieldValueBL.GetByFlowInstanceIDAndFormTypeIDAndFieldIDs(uow, flowinstance.FlowInstanceID, (int)FlowFormType.AanvraagFormulierZHVVT,
                        new int[] {
                            FlowFieldConstants.ID_InfusietherapieParenteraleToedieningMedicatie,
                            FlowFieldConstants.ID_InfusietherapieParenteraleVoeding,
                            FlowFieldConstants.ID_InfusietherapiePICC,
                            FlowFieldConstants.ID_InfusietherapieCentraalVeneuzeLijn });
                }

                if (subvalues.Any())
                {
                    var labels = subvalues.ToList().Where(it => it.Value.ToLogical()).OrderBy(it => it.Value).Select(it => it.FlowField.Label).ToArray();
                    if (labels.Any())
                    {
                        itemtext += ": " + String.Join(", ", labels);
                    }
                }

                nazorgItems.Add(itemtext);
            }

            DataTable datatable = DataTableHelper.CreateWithColumnList("Nazorg", new List<string>() { "Agreement" });
            datatable.FirstRow().SetColumnValue("Agreement", String.Join("\r\n", nazorgItems));
            datatable.AcceptChanges();
            return datatable;
        }

        private DataTable addAidProductOrderItem()
        {
            var orderflowfieldids = new int[] { FlowFieldConstants.ID_AfleverDatum, FlowFieldConstants.ID_AfleverTijd, FlowFieldConstants.ID_OrderStatus };
            // we don't use flowwebfields because no FormSetVersionID is available
            var orderfields = FlowFieldValueBL.GetByFlowInstanceIDAndFieldIDs(uow, flowinstance.FlowInstanceID, orderflowfieldids)?.ToList();

            var failedorders = orderfields.Where(ffv => ffv.Value == FlowFieldConstants.Value_OrderMislukt).Select(ffv => ffv.FormSetVersionID);

            List<AidProductOrderItem> ordereditems = AidProductBL.GetOrderItemsByTransferID(uow, flowinstance.TransferID);
            ordereditems.RemoveAll(ap => failedorders.Contains(ap.FormSetVersionID));
            if (!ordereditems.Any())
            {
                return null;
            }

            List<Delivery> deliveries = getAidProductOrderItemDeliveries(uow, orderfields);

            // lets reorder everything based on the 'Gewenste aflever datum / tijd'
            ordereditems = ordereditems.OrderBySequence(
                deliveries.OrderBy(d => d.Date).ThenBy(d => d.FormSetVersionID).Select(d => d.FormSetVersionID),
                d => d.FormSetVersionID).ToList();

            var lines = new List<string>();
            foreach (var orderitem in ordereditems.GroupBy(grp => grp.FormSetVersionID))
            {
                int formsetversionid = orderitem.FirstOrDefault().FormSetVersionID;
                var aidproduct = orderitem.FirstOrDefault().AidProduct;

                if (aidproduct.SupplierID != (int)SupplierID.Point && !string.IsNullOrEmpty(aidproduct?.Supplier?.Name))
                {
                    lines.Add($"We hebben de onderstaande hulpmiddelen voor u besteld bij {aidproduct.Supplier.Name}");
                }
                else
                {
                    lines.Add("De onderstaande hulpmiddelen zijn benodigd");
                }

                var delivery = deliveries.FirstOrDefault(d => d.FormSetVersionID == formsetversionid);
                if (delivery != null)
                {
                    var expecteddelivery = delivery.ToString();
                    if (!string.IsNullOrEmpty(expecteddelivery))
                    {
                        lines.Add($"Gewenste aflever datum / tijd {expecteddelivery}");
                        var phonenumber = aidproduct?.Supplier?.PhoneNumber;
                        if (!string.IsNullOrEmpty(phonenumber))
                        {
                            lines.Add($"Voor vragen bel {phonenumber}.");
                        }
                    }
                }

                foreach (var product in orderitem)
                {
                    lines.Add($" • {product.Quantity} x {product.AidProduct.Name}");
                }

                lines.Add(null);
            }

            DataTable datatable = DataTableHelper.CreateWithColumnList("AidProductOrderItem", new List<string>() { "Orders" });
            datatable.FirstRow().SetColumnValue("Orders", string.Join("\r\n", lines));
            datatable.AcceptChanges();

            return datatable;
        }

        private static List<Delivery> getAidProductOrderItemDeliveries(IUnitOfWork uow, IEnumerable<FlowFieldValue> orderfields)
        {
            var deliveries = new List<Delivery>();
            foreach (var order in orderfields.GroupBy(ffv => ffv.FormSetVersionID))
            {
                if (!order.Key.HasValue)
                {
                    continue;
                }

                var deliverydate = orderfields.FirstOrDefault(ffv => ffv.FlowFieldID == FlowFieldConstants.ID_AfleverDatum && ffv.FormSetVersionID == order.Key);
                var deliverytime = orderfields.FirstOrDefault(ffv => ffv.FlowFieldID == FlowFieldConstants.ID_AfleverTijd && ffv.FormSetVersionID == order.Key);

                DateTime date = DateTime.MinValue;
                string time = null;
                if (DateTime.TryParse(deliverydate?.Value, out date))
                {
                    if (deliverytime != null)
                    {
                        time = FlowFieldValueHelper.GetDisplayValueMultiSelect(uow, order.Key.Value, deliverytime.FlowFieldID, addSelectedNone: false, selectedOnly:true);
                        time = time.Replace(" - ", "-").ToLowerInvariant();
                    }                   
                }

                deliveries.Add(new Delivery(order.Key.Value, date, time));
            }

            return deliveries;
        }
    }
}