﻿using Point.Infrastructure.Extensions;
using Point.Infrastructure.Helpers;
using System.Collections.Generic;
using System.Data;


namespace Point.Business.Logic.TemplateDataSets
{
    public class ResetPassword
    {
        private string newpassword;     
        public ResetPassword(string newpassword)
        {                   
            this.newpassword = newpassword;
        }

        public DataSet Get()
        {
            var dataset = new DataSet();
            dataset.SafeAddTable(addResetPasswordTable());
            return dataset;
        }

        private DataTable addResetPasswordTable()
        {
            DataTable datatable = DataTableHelper.CreateWithColumnList("ResetPassword", new List<string>() { "NewPassword"});
            datatable.FirstRow().SetColumnValue("NewPassword", this.newpassword);

            datatable.AcceptChanges();
            return datatable;
        }

    }
}
