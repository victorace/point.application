﻿using Point.Infrastructure.Extensions;
using Point.Infrastructure.Helpers;
using System.Collections.Generic;
using System.Data;
using Point.Database.Models;
using System.Text.RegularExpressions;

namespace Point.Business.Logic.TemplateDataSets
{
    public class UnblockUserMail
    {
        private Employee employee, admin = null;
        private string otherAdminsEmail = "";
        public UnblockUserMail(Employee employee, Employee admin, string otherAdminsEmail)
        {            
            this.employee = employee;
            this.admin = admin;
            this.otherAdminsEmail = otherAdminsEmail;
        }

        public DataSet Get()
        {
            var dataset = new DataSet();
            dataset.SafeAddTable(addUnblockEmailTable());
            return dataset;
        }

        private DataTable addUnblockEmailTable()
        {
            DataTable datatable = DataTableHelper.CreateWithColumnList("UnblockEmail", new List<string>() { "EditEmployeePage" ,
                "EmployeeFullName", "EmployeeDepartment", "EmployeeUserName", "EmployeePhone", "EmployeeEmail", "EmployeeDepartmentPhone",
                "AdminUserName","OtherAdminEmail"});

            string editemployeepage = ConfigHelper.GetAppSettingByName<string>("WebServer") + "/management/employee";
            datatable.FirstRow().SetColumnValue("EditEmployeePage", editemployeepage );

            string fullname = employee.FirstName + " " + employee.MiddleName + " " + employee.LastName;
            fullname = Regex.Replace(fullname, @"\s+", " ");
            datatable.FirstRow().SetColumnValue("EmployeeFullName", fullname);

            datatable.FirstRow().SetColumnValue("EmployeeDepartment", employee.DefaultDepartment.Name);
            datatable.FirstRow().SetColumnValue("EmployeeUserName", employee.aspnet_Users.UserName);
            datatable.FirstRow().SetColumnValue("EmployeePhone", employee.PhoneNumber);
            datatable.FirstRow().SetColumnValue("EmployeeEmail", employee.EmailAddress);
            datatable.FirstRow().SetColumnValue("EmployeeDepartmentPhone", employee.DefaultDepartment.PhoneNumber);
            datatable.FirstRow().SetColumnValue("AdminUserName", admin?.aspnet_Users?.UserName??"");
            datatable.FirstRow().SetColumnValue("OtherAdminEmail", otherAdminsEmail);

            datatable.AcceptChanges();
            return datatable;
        }
    }
}
