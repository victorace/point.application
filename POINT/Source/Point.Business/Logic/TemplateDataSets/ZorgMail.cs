﻿using Point.Business.Helper;
using Point.Database.Extensions;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Extensions;
using Point.Infrastructure.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Point.Business.Logic.TemplateDataSets
{
    public class ZorgMail
    {
        private IUnitOfWork uow;
        private FlowInstance flowinstance;
        private PointUserInfo pointuserinfo;
        private IEnumerable<FlowWebField> flowwebfields;

        public ZorgMail(IUnitOfWork uow, FlowInstance flowInstance, PointUserInfo pointUserInfo)
        {
            this.uow = uow ?? throw new ArgumentException(nameof(uow));
            flowinstance = flowInstance ?? throw new ArgumentException(nameof(flowInstance));
            pointuserinfo = pointUserInfo ?? throw new ArgumentException(nameof(pointUserInfo));
            flowwebfields = FlowWebFieldBL.GetByTransferID(uow, flowInstance.TransferID, pointUserInfo) ?? Enumerable.Empty<FlowWebField>();
        }

        public DataSet Get()
        {
            var dataset = new DataSet();

            dataset.SafeAddTable(AddClientTable());

            dataset.SafeAddTable(AddSendingOrganizationTable());
            dataset.SafeAddTable(AddSendingLocationTable());
            dataset.SafeAddTable(AddSendingDepartmentTable());

            dataset.SafeAddTable(AddFieldValuesTable());

            dataset.SafeAddTable(AddDateFieldsTable());
            dataset.SafeAddTable(AddOtherDetailsTable());
            dataset.SafeAddTable(AddWlzTable());

            return dataset;
        }

        private DataTable AddClientTable()
        {
            var client = flowinstance?.Transfer?.Client;
            if (client == null)
            {
                return null;
            }

            var fullname = client?.FullName();
            var datatable = client.ConvertToDataTable("Client", new List<string>() { "ClientNameFormatted", "FullName" });
            datatable.FirstRow().SetColumnValue("ClientNameFormatted", fullname);
            datatable.FirstRow().SetColumnValue("FullName", fullname);
            datatable.AcceptChanges();
            return datatable;
        }

        private DataTable AddSendingDepartmentTable()
        {
            var department = flowinstance?.StartedByDepartment;
            if (department == null)
            {
                return null;
            }

            var datatable = department.ConvertToDataTable("Department", new List<string>() { "FullName" });
            datatable.FirstRow().SetColumnValue("FullName", department?.FullName());
            datatable.AcceptChanges();
            return datatable;
        }

        private DataTable AddSendingLocationTable()
        {
            var location = flowinstance?.StartedByDepartment?.Location;
            if (location == null)
            {
                return null;
            }

            var datatable = location.ConvertToDataTable("Location", new List<string>() { "FullName" });
            datatable.FirstRow().SetColumnValue("FullName", location?.FullName());
            datatable.AcceptChanges();
            return datatable;
        }

        private DataTable AddSendingOrganizationTable()
        {
            var organization = flowinstance?.StartedByOrganization;
            if (organization == null)
            {
                return null;
            }

            var datatable = organization.ConvertToDataTable("Organization");
            datatable.AcceptChanges();
            return datatable;
        }

        private DataTable AddWlzTable()
        {
            DataTable datatable = DataTableHelper.CreateWithColumnList("Wlz", new List<string>()
                    { "WlzGrondslagSom", "WlzGrondslagSomFrom", "WlzGrondslagSomUntill", "WlzGrondslagPG", "WlzGrondslagPGFrom", "WlzGrondslagPGUntill",
                      "WlzAnders", "WlzAndersFrom", "WlzAndersUntill", "WlzAndersOpmerkingen", "WlzProfiel", "WlzProfielOpmerkingen", "InformatieHuisartsenbericht" });

            FillRow(datatable, "WlzGrondslagSom", DataRowExtensionFieldType.Bool);
            FillRow(datatable, "WlzGrondslagSomFrom", DataRowExtensionFieldType.Date);
            FillRow(datatable, "WlzGrondslagSomUntill", DataRowExtensionFieldType.Date);
            FillRow(datatable, "WlzGrondslagPG", DataRowExtensionFieldType.Bool);
            FillRow(datatable, "WlzGrondslagPGFrom", DataRowExtensionFieldType.Date);
            FillRow(datatable, "WlzGrondslagPGUntill", DataRowExtensionFieldType.Date);
            FillRow(datatable, "WlzAnders", DataRowExtensionFieldType.Bool);
            FillRow(datatable, "WlzAndersFrom", DataRowExtensionFieldType.Date);
            FillRow(datatable, "WlzAndersUntill", DataRowExtensionFieldType.Date);
            FillRow(datatable, "WlzAndersOpmerkingen", DataRowExtensionFieldType.Char);
            FillRow(datatable, "WlzProfiel", DataRowExtensionFieldType.Bool);
            FillRow(datatable, "WlzProfielOpmerkingen", DataRowExtensionFieldType.Char);
            FillRow(datatable, "InformatieHuisartsenbericht", DataRowExtensionFieldType.Char);
            datatable.AcceptChanges();
            return datatable;
        }

        private DataTable AddOtherDetailsTable()
        {
            DataTable datatable = DataTableHelper.CreateWithColumnList("OtherDetails", new List<string>()
                    { "AfterCareDefinitive", "ELVConclusieAfweging", "DossierUrl", "Nazorg", "PreviousNazorg" });

            FillRow(datatable, "AfterCareDefinitive", DataRowExtensionFieldType.Char);
            FillRow(datatable, "ELVConclusieAfweging", DataRowExtensionFieldType.Char);
            datatable.FirstRow().SetColumnValue("DossierUrl", $"{ConfigHelper.GetAppSettingByName<string>("WebServer")}/FlowMain/Dashboard?TransferID={flowinstance.TransferID}");

            var department = OrganizationHelper.GetRecievingDepartment(uow, flowinstance.FlowInstanceID);
            datatable.FirstRow().SetColumnValue("Nazorg", department?.FullName());

            var previousdepartment = OrganizationHelper.GetPreviousReceivingDepartment(uow, flowinstance.FlowInstanceID);
            datatable.FirstRow().SetColumnValue("PreviousNazorg", previousdepartment?.FullName());

            datatable.AcceptChanges();
            return datatable;
        }

        private DataTable AddFieldValuesTable()
        {
            DataTable datatable = DataTableHelper.CreateWithColumnList("FieldValue", new List<string>()
                    { "BehandelaarNaam", "PrognoseVerwachteOntwikkeling", "AfterCareFinancing", "DischargeRemarks", "DischargeProposedStartDate", "DischargeHomecareBeginTime", "DischargeHomecareEndTime", "DischargePatientExpectedTime" });

            FillRow(datatable, "BehandelaarNaam", DataRowExtensionFieldType.Char);
            FillRow(datatable, "PrognoseVerwachteOntwikkeling", DataRowExtensionFieldType.DropDown);
            FillRow(datatable, "AfterCareFinancing", DataRowExtensionFieldType.DropDown);
            FillRow(datatable, "DischargeRemarks", DataRowExtensionFieldType.Char);
            FillRow(datatable, "DischargeProposedStartDate", DataRowExtensionFieldType.Date);
            FillRow(datatable, "DischargeHomecareBeginTime", DataRowExtensionFieldType.Date);
            FillRow(datatable, "DischargeHomecareEndTime", DataRowExtensionFieldType.Date);
            FillRow(datatable, "DischargePatientExpectedTime", DataRowExtensionFieldType.Date);
            datatable.AcceptChanges();
            return datatable;
        }

        private DataTable AddDateFieldsTable()
        {
            DataTable datatable = DataTableHelper.CreateWithColumnList("DateFields", new List<string>()
                    { "BirthDate", "BirthDateValue", "MedischeSituatieDatumOpname", "PatiëntWordtOpgenomenDatum",  "DischargeRealizedDate", "CareBeginDate" });

            var birthdate = flowinstance?.Transfer?.Client?.BirthDate == null ? null : flowinstance?.Transfer?.Client?.BirthDate.GetValueOrDefault().ToString("dd-MM-yyyy");
            datatable.FirstRow().SetColumnValue("BirthDate", birthdate);
            datatable.FirstRow().SetColumnValue("BirthDateValue", birthdate);
            FillRow(datatable, "MedischeSituatieDatumOpname", DataRowExtensionFieldType.Date);
            FillRow(datatable, "PatiëntWordtOpgenomenDatum", DataRowExtensionFieldType.Date);
            FillRow(datatable, "RealizedDischargeDate", DataRowExtensionFieldType.Date, columnName: "DischargeRealizedDate");
            FillRow(datatable, "CareBeginDate", DataRowExtensionFieldType.Date);
            datatable.AcceptChanges();
            return datatable;
        }

        private void FillRow(DataTable dataTable, string fieldName, DataRowExtensionFieldType dataRowExtensionFieldType, string columnName = null)
        {
            var flowwebfield = flowwebfields.FirstOrDefault(f => f.Name == fieldName);
            var displayvalue = flowwebfield == null ? "" : FlowFieldValueHelper.GetDisplayValue(flowwebfield);

            dataTable.FirstRow().SetColumnValue(columnName ?? fieldName, displayvalue);
        }
    }
}
