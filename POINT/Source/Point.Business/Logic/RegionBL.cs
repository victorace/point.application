﻿using System.Collections.Generic;
using Point.Business.Helper;
using Point.Database.Extensions;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using System.Linq;
using System.Threading.Tasks;

namespace Point.Business.Logic
{
    public class RegionBL
    {
        public static Region GetByRegionID(IUnitOfWork uow, int regionID)
        {
            return uow.RegionRepository.GetByID(regionID);
        }

        public static Region Save(IUnitOfWork uow, RegionViewModel viewmodel, PointUserInfo pointUserInfo, LogEntry logEntry)
        {
            var model = GetByRegionID(uow, viewmodel.RegionID);

            if (model == null)
            {
                model = new Region();
                uow.RegionRepository.Insert(model);
            }

            model.WriteSecureValue(m => m.Name, viewmodel, v => v.Name, pointUserInfo);
            model.WriteSecureValue(m => m.CapacityBeds, viewmodel, v => v.CapacityBeds, pointUserInfo);
            model.WriteSecureValue(m => m.CapacityBedsPublic, viewmodel, v => v.CapacityBedsPublic, pointUserInfo);

            FormTypeRegionBL.Save(uow, viewmodel, logEntry);

            LoggingBL.FillLoggable(uow, model, logEntry, true);

            Task.Run(async () => await DepartmentCapacityPublicBL.UpdateOnRegionChange(model));

            uow.Save(logEntry);

            return model;
        }

        public static IEnumerable<Region> GetByRegionIDs(IUnitOfWork uow, IEnumerable<int> regionIDs)
        {
            return uow.RegionRepository.Get(it => regionIDs.Contains(it.RegionID));
        }

        public static Region GetByRegionName(IUnitOfWork uow, string regionname)
        {
            return uow.RegionRepository.Get(it => it.Name.Trim().ToLower() == regionname.Trim().ToLower()).FirstOrDefault() ;
        }

        public static IEnumerable<Region> GetByOrganizationIDs(IUnitOfWork uow, IEnumerable<int> organizationIDs)
        {
            return uow.RegionRepository.Get(it => it.Organization.Any(org => organizationIDs.Contains(org.OrganizationID)));
        }

        public static IEnumerable<Region> GetByLocationIDs(IUnitOfWork uow, IEnumerable<int> locationIDs)
        {
            return uow.RegionRepository.Get(it => 
                it.Organization.Any(org => 
                    !org.Inactive && 
                    org.Location.Any(loc =>
                        !loc.Inactive &&
                        locationIDs.Contains(loc.LocationID)
                    )
                )
            );
        }

        public static IEnumerable<Region> GetAll(IUnitOfWork uow)
        {
            return uow.RegionRepository.GetAll();
        }

        public static bool InSameRegion(IUnitOfWork uow, int employeeID, PointUserInfo pointUserInfo)
        {
            PointUserInfo employee = PointUserInfoHelper.GetPointUserInfoByEmployeeID(uow, employeeID);
            return employee.Organization.RegionID == pointUserInfo.Organization.RegionID;
        }
    }
}
