﻿using Point.Database.Models;
using Point.Database.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Point.Business.Logic
{
    public class SystemMessageViewBL
    {
        public static SystemMessageViewModel FromModel(SystemMessage model)
        {
            if (model == null)
            {
                var today = DateTime.Now;
                return new SystemMessageViewModel
                {
                    StartDate = today.Date,
                    StartTime = today.TimeOfDay,
                    EndDate = today.Date,
                    EndTime = today.TimeOfDay
                };
            }

            return new SystemMessageViewModel
            {
                SystemMessageID = model.SystemMessageID,
                Title = model.Title,
                Message = model.Message,
                LinkReference = model.LinkReference,
                LinkText = model.LinkText,
                StartDate = model.StartDate.Date,
                StartTime = model.StartDate.TimeOfDay,
                EndDate = model.EndDate.Date,
                EndTime = model.EndDate.TimeOfDay,
                FunctionCode = model.FunctionCode,
                IsPermanent = model.IsPermanent,
                IsActive = model.IsActive
            };
        }

        public static IEnumerable<SystemMessageViewModel> FromModel(IEnumerable<SystemMessage> model)
        {
            return model.Select(FromModel).ToList();
        }
    }
}