﻿using System;
using System.Collections.Generic;
using System.Linq;
using Point.Database.Models;
using Point.Database.Repository;

namespace Point.Business.Logic
{
    public class ActionCodeHealthInsurerBL
    {
        public static IEnumerable<ActionCodeHealthInsurer> GetByRegionIDAndFormTypeID(IUnitOfWork uow, int regionID, int formTypeID)
        {
            var totallist = uow.ActionCodeHealthInsurerRepository.Get(
                achi => achi.ActionCodeHealthInsurerList.Any(achil => achil.ActionCodeHealthInsurerRegio.RegionID == regionID 
                                                                   && (achil.ActionCodeHealthInsurerRegio.DateBegin <= DateTime.Now 
                                                                       && (achil.ActionCodeHealthInsurerRegio.DateEnd == null || achil.ActionCodeHealthInsurerRegio.DateEnd > DateTime.Now)))
                     && achi.ActionCodeHealthInsurerFormType.Any(achift => achift.FormTypeID == formTypeID)
                ).OrderBy(achi => achi.ActionCodeHealthInsurerNumberOnScreen);

            return totallist;
        }
    }
}
