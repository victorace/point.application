﻿using Point.Database.Models;
using Point.Database.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using Point.Models.Enums;

namespace Point.Business.Logic
{
    public class HealthInsurerBL
    {
        public static IEnumerable<HealthInsurer> GetAllHealthInsurers(IUnitOfWork uow, bool includeinactive = false)
        {
            return uow.HealthInsurerRepository.Get(hi => includeinactive || !hi.InActive).OrderBy(hi => hi.InActive).ThenBy(hi => hi.SortOrder).ThenBy(hi => hi.Name);
        }

        public static HealthInsurer GetByID(IUnitOfWork uow, int healthInsurerID)
        {
            return uow.HealthInsurerRepository.GetByID(healthInsurerID);
        }

        public static HealthInsurer GetByUzovi(IUnitOfWork uow, string uzovi)
        {
            int uzovinumber = -1;
            if (!Int32.TryParse(uzovi, out uzovinumber))
            {
                return default(HealthInsurer);
            }

            return GetByUzovi(uow, uzovinumber);
        }

        public static HealthInsurer GetByUzovi(IUnitOfWork uow, int uzovi)
        {
            return uow.HealthInsurerRepository.FirstOrDefault(hi => hi.UZOVI == uzovi);
        }

        public static string GetExternalSupplierCode(HealthInsurer healthInsurer, int supplierID)
        {
            if (healthInsurer == null || supplierID == (int)SupplierID.Point)
            {
                return null;
            }

            var insurerSupplierCode = string.Empty;
            switch (supplierID)
            {
                case (int)SupplierID.MediPoint:
                    insurerSupplierCode = healthInsurer.MediPointCode;
                    break;
                case (int)SupplierID.Vegro:
                    insurerSupplierCode = healthInsurer.VegroCode;
                    break;
            }

            return insurerSupplierCode;
        }
    }
}
