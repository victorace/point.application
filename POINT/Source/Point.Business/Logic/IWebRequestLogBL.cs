﻿namespace Point.Business.Logic
{
    public interface IWebRequestLogBL
    {
        void LogWebRequest(object sentMessage, object receivedMessage, string exceptionMessage);
    }
}