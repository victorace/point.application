﻿using System;
using System.Linq;
using Point.Database.Models;
using Point.Database.Repository;
using Point.Infrastructure.Extensions;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Point.Business.Logic
{
    public class ClientReceivedTempHL7BL
    {
        public static IEnumerable<ClientReceivedTempHL7> GetClientReceivedTempHL7(IUnitOfWork uow, int interfaceNumber, string patientNumber, string messageID)
        {
            var sql =
                $@"select * from [ClientReceivedTempHL7_{
                        interfaceNumber
                    }] where PatientNumber = @PatientNumber and MessageType = @MessageType and MessageId = @MessageId";

            SqlParameter[] sqlParameters = {
                new SqlParameter("@PatientNumber", patientNumber),
                new SqlParameter("@MessageType", "A19"),
                new SqlParameter("@MessageID", messageID)
            };

            var result = uow.DatabaseContext.Database.SqlQuery<ClientReceivedTempHL7>(sql, sqlParameters);

            return result.ToList();
        }

        // Using this old/existing stored procedures since some logic is handled in them, and don't want to disrupt the "external" koppeling-processes to much
        public static void InsertClientQRY(IUnitOfWork uow, int interfaceNumber, ClientQRY clientQRY)
        {
            string sql = "InsertClientQRY @MESSAGE_ID, @PARENT_ID, @PatientID, @MsgID_1, @InterfaceNumber";

            SqlParameter[] sqlParameters = {
                new SqlParameter("@MESSAGE_ID", clientQRY.MESSAGE_ID),
                new SqlParameter("@PARENT_ID", clientQRY.PARENT_ID),
                new SqlParameter("@PatientID", clientQRY.PatientID),
                new SqlParameter("@MsgID_1", clientQRY.MsgID_1),
                new SqlParameter("@InterfaceNumber", interfaceNumber)
            };

            uow.DatabaseContext.Database.ExecuteSqlCommand(sql, sqlParameters);
        }

        public static string SaveClientQRY(IUnitOfWork uow, Organization organization, string patientNumber)
        {
            var clientQRY = fillClientQRY(organization, patientNumber);

            InsertClientQRY(uow, organization.InterfaceNumber.ConvertTo<int>(), clientQRY);

            return clientQRY.MESSAGE_ID;
        }

        public static ClientReceivedTempHL7 GetClientReceivedTempHL7(IUnitOfWork uow, Organization organization, string patientNumber, string messageID)
        {
            var clientReceivedTempHL7s = GetClientReceivedTempHL7(uow, organization.InterfaceNumber.ConvertTo<int>(), patientNumber, messageID);

            if (clientReceivedTempHL7s == null || !clientReceivedTempHL7s.Any())
            {
                return default(ClientReceivedTempHL7);
            }

            return clientReceivedTempHL7s.FirstOrDefault();
        }

        private static ClientQRY fillClientQRY(Organization organization, string patientNumber)
        {
            var clientQRY = new ClientQRY();

            if ( organization.ShortConnectionID.ConvertTo<bool>() )
            {
                var now = DateTime.Now;
                // Kept formatting as original (ClientDetails.aspx.vb), do have trailing zeroes on the msecs
                clientQRY.MESSAGE_ID = now.ToString("yyyyMMddHHmmFFF"); 
                clientQRY.PARENT_ID = now.ToString("yyyyMMddHHmmFFF");
                clientQRY.PatientID = patientNumber;
                clientQRY.MsgID_1 = now.ToString("MdHmss");
            }
            else
            {
                var guid = Guid.NewGuid().ToString();
                clientQRY.MESSAGE_ID = guid;
                clientQRY.PARENT_ID = guid;
                clientQRY.PatientID = patientNumber;
                clientQRY.MsgID_1 = guid;
            }

            return clientQRY;
        }
    }
}
