﻿using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Exceptions;
using Point.Infrastructure.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using Point.Database.Extensions;
using Point.Business.Helper;

namespace Point.Business.Logic
{
    public class EmployeeViewBL
    {
        public static EmployeeViewModel FromModel(IUnitOfWork uow, Employee model)
        {
            if (model == null)
            {
                return CreateNew();
            }

            var rolenames = model.aspnet_Users?.aspnet_Roles.Select(it => it.RoleName);

            var maxdossieradminfunctions = model.FunctionRole.Where(it => it.FunctionRoleTypeID == FunctionRoleTypeID.MaxDossierLevelAdmin);
            var reportingfunctions = model.FunctionRole.Where(it => it.FunctionRoleTypeID == FunctionRoleTypeID.Reporting);
            var functions = model.FunctionRole.Where(it => it.FunctionRoleTypeID != FunctionRoleTypeID.Reporting && it.FunctionRoleTypeID != FunctionRoleTypeID.MaxDossierLevelAdmin);

            return new EmployeeViewModel()
            {
                Inactive = model.InActive,

                DefaultDepartment = model.DefaultDepartment,

                OrganizationID = model.DefaultDepartment.Location.OrganizationID,
                LocationID = model.DefaultDepartment.LocationID,
                DepartmentID = model.DepartmentID,

                OtherDepartments = model.EmployeeDepartment.OrderBy(it => it.Department.Location.Name).ThenBy(it => it.Department.Name).Select(od => od.Department).ToList(),
                OtherDepartmentID = model.EmployeeDepartment.Select(it => it.DepartmentID).ToArray(),
                OtherDepartmentsToShow = String.Join(",", (model.EmployeeDepartment.OrderBy(it => it.Department.Location.Name).ThenBy(it => it.Department.Name)?.Select(od => od.Department.Location.Name +"/ "+ od.Department.Name + " ")?.ToArray())),

                EditAllForms = model.EditAllForms,
                ReadOnlyReciever = model.ReadOnlyReciever,
                ReadOnlySender = model.ReadOnlySender,

                EmployeeID = model.EmployeeID,
                FirstName = model.FirstName,
                MiddleName = model.MiddleName,
                LastName = model.LastName,
                Gender = model.Gender,
                Function = model.Position,
                PhoneNumber = model.PhoneNumber,
                EmailAddress = model.EmailAddress,
                ZISID = model.ExternID,
                BIGNR = model.BIG,
                ChangePasswordOnNextLogin = model.ChangePasswordByEmployee,

                DossierLevelID = Authorization.GetPointFlowRole(uow, model.UserId).ToFunctionRoleLevelID(),
                MaxDossierLevelAdminLevelID = maxdossieradminfunctions.Any() ? maxdossieradminfunctions.Max(it => it.FunctionRoleLevelID) : FunctionRoleLevelID.None,
                FunctionRoleLevelID = functions.Any() ? functions.Max(it => it.FunctionRoleLevelID) : FunctionRoleLevelID.None,
                FunctionRoleTypeIDs = functions.Select(it => it.FunctionRoleTypeID).ToArray(),

                ReportingFunction = reportingfunctions.Any(),
                ReportingLevelID = reportingfunctions.Any() ? reportingfunctions.Max(it => it.FunctionRoleLevelID) : FunctionRoleLevelID.None,

                CreateDate = model.aspnet_Users?.aspnet_Membership?.CreateDate,
                LastLoginDate = model.LastRealLoginDate,
                EmployeeIsLocked = model.aspnet_Users?.aspnet_Membership?.IsLockedOut == true,
                UserName = model.aspnet_Users?.UserName,

                MFANumber = model.MFANumber,
                MFAKey = model.MFAKey,

                EnableDigitalSignature = model.DefaultDepartment.Location.Organization.UseDigitalSignature,
                DigitalSignatureID = model.DigitalSignatureID,

                MFARequired = model.MFARequired,
                IPAddresses = model.IPAddresses, 

                AutoLockAccountDate = model.AutoLockAccountDate,
                AutoEmailSignalering = model.AutoEmailSignalering
            };
        }

        public static List<EmployeeViewModel> FromModelList(IUnitOfWork uow, List<Employee> modellist)
        {
            var viewmodellist = new List<EmployeeViewModel>();
            foreach (var item in modellist)
            {
                viewmodellist.Add(FromModel(uow, item));
            }
            return viewmodellist;
        }

        public static EmployeeViewModel CreateNew()
        {
            return new EmployeeViewModel()
            {
                ChangePasswordOnNextLogin = true
            };
        }

        public static void ValidateViewModel(IUnitOfWork uow, PointUserInfo userinfo, EmployeeViewModel viewmodel)
        {
            var myfunctionrolelevelid = userinfo.GetFunctionRoleLevelID(FunctionRoleTypeID.ManageEmployees);
            var myfunctionroletypeids = userinfo.Employee.FunctionRoles.Select(it => it.FunctionRoleTypeID);

            var mymaxreportlevel = userinfo.GetReportFunctionRoleLevelID();
            var mymaxdossierlevel = userinfo.GetMaxDossierLevelAdminLevelID();

            //Check change password
            if (!String.IsNullOrEmpty(viewmodel.Password))
            {
                if (viewmodel.DossierLevelID > mymaxdossierlevel || viewmodel.FunctionRoleLevelID > myfunctionrolelevelid)
                {
                    throw new PointSecurityException("U heeft geen machtigingen om het wachtwoord aan te passen voor deze gebruiker", ExceptionType.ChangePassword);
                }

                if (viewmodel.Password2 != viewmodel.Password)
                {
                    throw new PointSecurityException("Wachtwoord 2 matcht niet met wachtwoord 1", ExceptionType.ChangePassword);
                }

                var passworderrors = SecurityBL.TestValidPassword(viewmodel.Password);
                if(passworderrors.Any())
                {
                    throw new PointSecurityException(String.Join(", ", passworderrors), ExceptionType.ChangePassword);
                }
            }

            var currentmodel = EmployeeBL.GetByID(uow, viewmodel.EmployeeID);
            var currentviewmodel = FromModel(uow, currentmodel);

            //Check change dossierrole
            if (currentviewmodel?.DossierLevelID != viewmodel.DossierLevelID)
            {
                if (mymaxdossierlevel < viewmodel.DossierLevelID)
                {
                    throw new PointSecurityException("U heeft onvoldoende machtigingen om deze actie uit te voeren", ExceptionType.AccessDenied);
                }
            }

            //Check functie level
            if (currentviewmodel?.FunctionRoleLevelID != viewmodel.FunctionRoleLevelID)
            {
                if (myfunctionrolelevelid < viewmodel.FunctionRoleLevelID)
                {
                    throw new PointSecurityException("U heeft onvoldoende machtigingen om deze actie uit te voeren", ExceptionType.AccessDenied);
                }
            }

            //Check functie types
            if (myfunctionroletypeids.Contains(FunctionRoleTypeID.ManageAll) == false && viewmodel.FunctionRoleTypeIDs.All(it => myfunctionroletypeids.Contains(it)) == false)
            {
                throw new PointSecurityException("U heeft onvoldoende machtigingen om deze actie uit te voeren", ExceptionType.AccessDenied);
            }

            //Check report level
            if (viewmodel.ReportingLevelID != currentviewmodel?.ReportingLevelID)
            {
                if (mymaxreportlevel < viewmodel.ReportingLevelID)
                {
                    throw new PointSecurityException("U heeft onvoldoende machtigingen om deze actie uit te voeren", ExceptionType.AccessDenied);
                }
            }

            //Check afdelingen te koppelen (alleen functielevel lager dan regio)
            if (myfunctionrolelevelid < FunctionRoleLevelID.Region)
            {
                var alloweddepartmentids = currentmodel?.EmployeeDepartment.Select(it => it.DepartmentID).ToList();
                if (alloweddepartmentids == null)
                {
                    alloweddepartmentids = new List<int>();
                }

                switch (myfunctionrolelevelid)
                {
                    case FunctionRoleLevelID.Organization:
                        alloweddepartmentids.AddRange(DepartmentBL.GetByOrganizationID(uow, userinfo.EmployeeOrganizationIDs).Select(it => it.DepartmentID));
                        break;

                    case FunctionRoleLevelID.Location:
                        alloweddepartmentids.AddRange(DepartmentBL.GetAllActiveByLocationIDs(uow, userinfo.EmployeeLocationIDs).Select(it => it.DepartmentID));
                        break;
                    case FunctionRoleLevelID.Department:
                        alloweddepartmentids.AddRange(userinfo.EmployeeDepartmentIDs.ToArray());
                        break;
                }

                if (!alloweddepartmentids.Contains(viewmodel.DepartmentID))
                {
                    throw new PointSecurityException("U heeft geen toegang tot deze afdeling", ExceptionType.AccessDenied);
                }

                foreach (var otherdepartmentid in viewmodel.OtherDepartmentID)
                {
                    if (!alloweddepartmentids.Contains(otherdepartmentid))
                    {
                        throw new PointSecurityException("U heeft geen toegang tot deze overige afdeling", ExceptionType.AccessDenied);
                    }
                }
             
            }

        }
    }
}
