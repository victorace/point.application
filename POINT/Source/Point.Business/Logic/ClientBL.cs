﻿using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Caching;
using Point.Infrastructure.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using Point.Models.Enums;
using Point.Models.LDM.EOverdracht30.Enums;

namespace Point.Business.Logic
{
    public class ClientBL
    {
        public static void SetAnonymous(IUnitOfWork uow, int flowinstanceid)
        {
            var flowinstance = FlowInstanceBL.GetByID(uow, flowinstanceid);
            if (flowinstance == null) return;

            //Do nothing for not supported flows
            if (FlowInstanceBL.CanUseAnonymousClient(flowinstance.FlowDefinitionID) == false) return;

            if (flowinstance.StartedByOrganizationID.HasValue)
            {
                // use StartedByOrganizationID as during FlowInstance creation the 
                // StartedByOrganization can be null
                var sendingorganization = OrganizationBL.GetByOrganizationID(uow, flowinstance.StartedByOrganizationID.Value);
                if (sendingorganization != null && sendingorganization.UseAnonymousClient)
                {
                    var client = flowinstance.Transfer.Client;
                    client.ClientIsAnonymous = true;
                    uow.Save();
                }
            }
        }

        public static bool ShowAnonymous(FlowInstance flowinstance, int? organizationid)
        {
            if (flowinstance == null)
            {
                return false;
            }

            if (organizationid.HasValue && flowinstance.StartedByOrganizationID == organizationid)
            {
                return false;
            }
            else
            {
                return flowinstance.Transfer.Client.ClientIsAnonymous;
            }
        }

        public static Client FromModel(IUnitOfWork uow, PatientReceivedTemp patientReceivedTemp)
        {
            if (patientReceivedTemp == null)
            {
                return new Client();
            }

            var client = new Client();
            client.Merge(patientReceivedTemp);

            if (patientReceivedTemp.HealthInsuranceCompanyUZOVICode > 0)
            {
                var healthinsurer = HealthInsurerBL.GetByUzovi(uow, patientReceivedTemp.HealthInsuranceCompanyUZOVICode);
                if (healthinsurer != null)
                {
                    client.HealthInsuranceCompanyID = healthinsurer.HealthInsurerID;
                }
            }

            // TODO: #16784 Technical Debt: WS, API and HL7 all have their own ZIB handling for the Client
            // HACK: #16785 ZGT CreateDossierAPI ZIB issues
            try
            {
                var gender = Geslacht.OUN;
                var fieldreceivedvaluemapping = FieldReceivedValueMappingBL.MapValue(uow, nameof(client.Gender), patientReceivedTemp.Gender);
                if (fieldreceivedvaluemapping != null)
                {
                    gender = fieldreceivedvaluemapping.TargetValue.ConvertTo<Geslacht>();
                }
                client.Gender = gender;

                fieldreceivedvaluemapping = FieldReceivedValueMappingBL.MapValue(uow, nameof(client.CivilClass), patientReceivedTemp.CivilClass);
                if (fieldreceivedvaluemapping != null)
                {
                    client.CivilClass = fieldreceivedvaluemapping.TargetValue.ConvertTo<BurgerlijkeStaat>();
                }

                fieldreceivedvaluemapping = FieldReceivedValueMappingBL.MapValue(uow, nameof(client.HousingType), patientReceivedTemp.HousingType);
                if (fieldreceivedvaluemapping != null)
                {
                    client.HousingType = fieldreceivedvaluemapping.TargetValue.ConvertTo<WoningType>();
                }

                fieldreceivedvaluemapping = FieldReceivedValueMappingBL.MapValue(uow, nameof(client.CompositionHousekeeping), patientReceivedTemp.CompositionHousekeeping);
                if (fieldreceivedvaluemapping != null)
                {
                    client.CompositionHousekeeping = fieldreceivedvaluemapping.TargetValue.ConvertTo<Gezinssamenstelling>();
                }
            }
            catch { /* slient because we have no logging here */ }

            return client;
        }

        public static Client FromModel(IUnitOfWork uow, ClientSimpleViewModel clientSimpleViewModel)
        {
            if (clientSimpleViewModel == null)
            {
                return new Client();
            }

            var client = ClientBL.GetByClientID(uow, clientSimpleViewModel.ClientID);
            if (client == null)
            {
                client = new Client();
            }

            client.Merge(clientSimpleViewModel);

            return client;
        }

        public static Client FromModel(IUnitOfWork uow, ClientFullViewModel clientFullViewModel)
        {
            if (clientFullViewModel == null)
            {
                return new Client();
            }

            var client = ClientBL.GetByClientID(uow, clientFullViewModel.ClientID);
            if (client == null)
            {
                client = new Client();
            }

            client.Merge(clientFullViewModel);

            return client;
        }

        public static Client FromModel(IUnitOfWork uow, ClientViewModel clientViewModel)
        {
            if (clientViewModel == null)
            {
                return new Client();
            }

            var client = GetByClientID(uow, clientViewModel.ClientID);
            if (client == null)
            {
                client = new Client();
            }

            client.Merge(clientViewModel);

            return client;
        }

        public static Client GetByClientID(IUnitOfWork uow, int clientID)
        {
            return uow.ClientRepository.GetByID(clientID);
        }

        public static Client GetByTransferID(IUnitOfWork uow, int transferID)
        {
            return uow.TransferRepository.GetByID(transferID)?.Client;
        }

        public static IEnumerable<int> GetIDsByTransferIDs(IUnitOfWork uow, IEnumerable<int> transferIDs)
        {
            return uow.TransferRepository.Get(tr => transferIDs.Contains(tr.TransferID)).Select(tr => tr.ClientID);
        }

        private static void Upsert(IUnitOfWork uow, Client client)
        {
            if (client.ClientID == 0)
            {
                uow.ClientRepository.Insert(client);
            }

            string cachestring = $"Client_{client.ClientID}";
            CacheService.Instance.Remove(cachestring); //invalidate cache
        }

        public static void Save(IUnitOfWork uow, Client client)
        {
            Upsert(uow, client);
            uow.Save();
        }

        public static void SetModifiedFields(IUnitOfWork uow, Client client, PointUserInfo pointuserinfo)
        {
            var timestamp = DateTime.Now;

            if ( client.ClientID <= 0 )
            {
                client.ClientCreatedBy = pointuserinfo.Employee.EmployeeID;
                client.ClientCreatedDate = timestamp;
            }

            client.ClientModifiedBy = pointuserinfo.Employee.EmployeeID;
            client.ClientModifiedDate = timestamp;

        }

        public static Client Copy(IUnitOfWork uow, Client client, PointUserInfo pointuserinfo)
        {
            var clientCopy = uow.ClientRepository.Get(c => c.ClientID == client.ClientID, asNoTracking: true).FirstOrDefault();

            if (clientCopy == null) return null;

            clientCopy.ClientID = 0;
            clientCopy.OriginalClientID = client.ClientID;
            SetModifiedFields(uow, clientCopy, pointuserinfo);

            uow.ClientRepository.Insert(clientCopy);
            Save(uow, clientCopy);

            return clientCopy;
        }

        public static IEnumerable<Client> GetClientByBSN(IUnitOfWork uow, string bsn)
        {
            return uow.ClientRepository.Get(c => c.CivilServiceNumber == bsn); 
        }

        public static bool HasClientOtherOpenTrnsfers(IUnitOfWork uow, string bsn, int transferID, PointUserInfo pointUserInfo, FlowDefinitionID[] flowDefinitionIDs = null)
        {                      
            return GetClientOtherOpenTrnsfers(uow, bsn,transferID,pointUserInfo)?.Any() ?? false;
        }

        public static List<FlowInstanceSearchValues> GetClientOtherOpenTrnsfers(IUnitOfWork uow, string bsn, int transferID, PointUserInfo pointUserInfo, FlowDefinitionID[] flowDefinitionIDs = null)
        {            
            var searchClientOpenTransfers = SearchQueryBL.GetSearchQuerySimple(uow);
            searchClientOpenTransfers.ApplyCivilServiceNumberFilter(bsn, true);
            searchClientOpenTransfers.ApplyFilter(pointUserInfo, DossierStatus.Active, FlowDirection.Sending);
            return  searchClientOpenTransfers?.Queryable?.Where(fi => fi.FlowInstanceSearchValues.TransferID != transferID)?.Select(x=> x.FlowInstanceSearchValues)?.ToList();           
        }
    }
}
