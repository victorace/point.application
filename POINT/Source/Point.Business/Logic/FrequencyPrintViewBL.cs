﻿using System.Linq;
using Point.Database.Extensions;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;

namespace Point.Business.Logic
{
    public class FrequencyPrintViewBL
    {
        public static FrequencyPrintViewModel Get(IUnitOfWork uow, int transferID, int? frequencyID, PointUserInfo pointuserinfo)
        {
            var flowinstance = FlowInstanceBL.GetByTransferID(uow, transferID);
            var client = ClientBL.GetByTransferID(uow, transferID);
            if (client == null) return null;

            var showanonymous = ClientBL.ShowAnonymous(flowinstance, pointuserinfo.Organization?.OrganizationID);

            var frequencyPrintViewMode = new FrequencyPrintViewModel()
            {
                TransferID = transferID,
                ClientID = client.ClientID,
                ClientFullName = client.FullName(flowinstance, pointuserinfo.Organization?.OrganizationID),
                ClientFullGender = client.FullGender(),
                ClientBirthDate = client.BirthDate,
                ClientCivilServiceNumber = showanonymous ? "Anoniem" : client.CivilServiceNumber
            };

            frequencyPrintViewMode.FrequencyTransferMemos =
                (from frequency in uow.FrequencyRepository.Get(f => f.FrequencyID == frequencyID || frequencyID == null)
                 select new FrequencyTransferMemoViewModel
                 {
                     FrequencyID = frequency.FrequencyID,
                     Name = frequency.Name,
                     StartDate = frequency.StartDate,
                     EndDate = frequency.EndDate,
                     Frequency = frequency.Quantity,
                     TransferMemos = TransferMemoViewBL.GetByFrequencyID(uow, frequency.FrequencyID, pointuserinfo, true)
                 });

            return frequencyPrintViewMode;
        }
    }
}
