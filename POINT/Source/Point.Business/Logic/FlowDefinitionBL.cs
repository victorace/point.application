﻿using System;
using System.Collections.Generic;
using System.Linq;
using Point.Business.Helper;
using Point.Database.Models;
using Point.Database.Repository;
using Point.Infrastructure.Caching;
using Point.Models.Enums;

namespace Point.Business.Logic
{
    public class FlowDefinitionBL
    {
        public static IEnumerable<FlowDefinition> GetSendingFlowDefinitionsByLocationIDDepartmentID(IUnitOfWork uow, int locationid, int departmentid)
        {
            var participation = FlowDefinitionParticipationBL.GetByLocationIDAndDepartmentID(uow, locationid, departmentid, usecache: false);
            if (participation == null) return Enumerable.Empty<FlowDefinition>();

            return participation.Where(it => 
                    it.Participation >= Participation.FaxMail &&
                    (it.FlowDirection == FlowDirection.Any || it.FlowDirection == FlowDirection.Sending)
                ).Select(it => it.FlowDefinition).OrderBy(it => it.FlowDefinitionID).ToList();
        }

        public static IEnumerable<FlowDefinition> GetRecievingFlowDefinitionsByLocationIDDepartmentID(IUnitOfWork uow, int locationid, int departmentid)
        {
            var participation = FlowDefinitionParticipationBL.GetByLocationIDAndDepartmentID(uow, locationid, departmentid, usecache: false);
            if (participation == null) return Enumerable.Empty<FlowDefinition>();

            return participation.Where(it =>
                    it.Participation >= Participation.FaxMail &&
                    (it.FlowDirection == FlowDirection.Any || it.FlowDirection == FlowDirection.Receiving)
                ).Select(it => it.FlowDefinition).OrderBy(it => it.FlowDefinitionID).ToList();
        }

        public static IEnumerable<FlowDefinition> GetFlowDefinitions(IUnitOfWork uow, bool usecache = true)
        {
            string cachestring = "FlowDefinition_Global";
            IList<FlowDefinition> flowDefinitions = null;

            if (usecache)
                flowDefinitions = CacheService.Instance.Get<IList<FlowDefinition>>(cachestring);

            if (flowDefinitions == null)
            {
                flowDefinitions = uow.FlowDefinitionRepository.GetAll().ToList();
                if (flowDefinitions.Any())
                    CacheService.Instance.Insert(cachestring, flowDefinitions, "CacheLong");
            }
            return flowDefinitions;
        }

        public static IEnumerable<FlowDefinition> GetActiveFlowDefinitions(IUnitOfWork uow)
        {
            return GetFlowDefinitions(uow).Where(fd => !fd.InActive && DateTime.Compare(fd.DateBegin, DateTime.Now) <= 0 && DateTime.Compare(fd.DateEnd, DateTime.Now) >= 0);
        }

        public static IEnumerable<FlowDefinition> GetFlowDefinitionsByFlowType(IUnitOfWork uow, FlowTypeID flowTypeID)
        {
            return GetFlowDefinitions(uow).Where(fd => fd.FlowTypeID == (int)flowTypeID);
        }
    }
}
