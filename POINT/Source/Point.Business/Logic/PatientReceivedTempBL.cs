﻿using Point.Business.BusinessObjects;
using Point.Database.Models;
using Point.Database.Repository;
using Point.Infrastructure.Extensions;
using System;
using System.Linq;

namespace Point.Business.Logic
{
    public static class PatientReceivedTempBL
    {
        public static PatientReceivedTemp Get(IUnitOfWork uow, int organizationID, string patientNumber)
        {
            return uow.PatientReceivedTempRepository.Get(prt => prt.PatientNumber == patientNumber && prt.OrganisationID == organizationID)
                                                    .OrderByDescending(prt => prt.TimeStamp).FirstOrDefault();
        }

        public static PatientReceivedTemp Get(IUnitOfWork uow, int organizationID, string patientNumber, string messageID)
        {
            return uow.PatientReceivedTempRepository.Get(prt => prt.PatientNumber == patientNumber && prt.OrganisationID == organizationID && prt.GUID.ToString() == messageID)
                                                    .OrderByDescending(prt => prt.TimeStamp).FirstOrDefault();
        }

        public static PatientReceivedTemp GetByBSNAndPatientNumber(IUnitOfWork uow, string bsn, string patientNumber, int organizationId)
        {
            return uow.PatientReceivedTempRepository.Get(prt => prt.CivilServiceNumber == bsn && prt.PatientNumber == patientNumber && prt.OrganisationID == organizationId)
                                                    .OrderByDescending(prt => prt.TimeStamp).FirstOrDefault();
        }

        public static PatientReceivedTemp GetByBSN(IUnitOfWork uow, string bsn, int organizationId)
        {
            return uow.PatientReceivedTempRepository.Get(prt => prt.CivilServiceNumber == bsn && prt.OrganisationID == organizationId)
                                                    .OrderByDescending(prt => prt.TimeStamp).FirstOrDefault();
        }

        public static int Delete(IUnitOfWork uow, int organizationID, string patientNumber)
        {
            var patientreceivedtemps = uow.PatientReceivedTempRepository.Get(prt => prt.PatientNumber == patientNumber && prt.OrganisationID == organizationID);
            foreach (var patientreceivedtemp in patientreceivedtemps)
            {
                uow.PatientReceivedTempRepository.Delete(patientreceivedtemp);
            }
            uow.Save();
            return patientreceivedtemps.Count();
        }

        public static PatientReceivedTemp Insert(IUnitOfWork uow, Guid guid, int organizationID, string senderUsername, DateTime sendDate, string patientNumber, string hash, PatientBO patientBO)
        {
            var patientreceivedtemp = new PatientReceivedTemp();
            patientreceivedtemp.Merge(patientBO);

            return Insert(uow, guid, organizationID, senderUsername, sendDate, patientNumber, hash, patientreceivedtemp);

        }

        public static PatientReceivedTemp Insert(IUnitOfWork uow, Guid guid, int organizationID, string senderUsername, DateTime sendDate, string patientNumber, string hash, PatientReceivedTemp patientReceivedTemp)
        {

            patientReceivedTemp.TimeStamp = DateTime.Now;
            patientReceivedTemp.GUID = guid;
            patientReceivedTemp.OrganisationID = organizationID;
            patientReceivedTemp.SenderUserName = senderUsername;
            patientReceivedTemp.SendDate = sendDate;
            patientReceivedTemp.PatientNumber = patientNumber;
            patientReceivedTemp.Hash = hash;

            uow.PatientReceivedTempRepository.Insert(patientReceivedTemp);
            uow.Save();

            return patientReceivedTemp;
        }


    }
}