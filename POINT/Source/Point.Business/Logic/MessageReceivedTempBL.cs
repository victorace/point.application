﻿using Point.Database.Models;
using Point.Database.Repository;
using Point.Infrastructure.Extensions;
using Point.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Point.Business.Logic
{
    public class MessageReceivedTempBL
    {
        public static IEnumerable<MessageReceivedTemp> Get(IUnitOfWork uow, int[] organizations, DateTime dateFrom, DateTime dateTo)
        {
            return uow.MessageReceivedTempRepository.Get(mrt => organizations.Contains(mrt.OrganisationID) && mrt.SendDate >= dateFrom && mrt.SendDate <= dateTo)
                .OrderByDescending(mrt => mrt.MessageReceivedTempID);
        }

        public static MessageReceivedTemp Get(IUnitOfWork uow, int organizationid, string patientNumber, string visitNumber)
        {
            var messagereceivedtemp = uow.MessageReceivedTempRepository
                .Get(mrt => mrt.OrganisationID == organizationid &&
                    mrt.PatientNumber == patientNumber &&
                    (string.IsNullOrEmpty(visitNumber) || string.IsNullOrEmpty(mrt.VisitNumber) || mrt.VisitNumber == visitNumber))
                .OrderByDescending(mrt => mrt.TimeStamp).FirstOrDefault();
            return messagereceivedtemp;
        }

        public static MessageReceivedTemp Insert(IUnitOfWork uow, Guid guid, int organizationid, string senderusername, DateTime senddate, 
            string patientnumber, string visitnumber, string hash, string type, Source source)
        {
            var timestamp = DateTime.Now;

            var messagereceivedtemp = new MessageReceivedTemp()
            {
                TimeStamp = timestamp,
                GUID = guid,
                OrganisationID = organizationid,
                SenderUserName = senderusername,
                SendDate = senddate,
                PatientNumber = patientnumber,
                VisitNumber = visitnumber,
                Hash = hash,
                Type = type,
                Source = source
            };

            uow.MessageReceivedTempRepository.Insert(messagereceivedtemp);
            uow.Save();

            return messagereceivedtemp;
        }
    }       
}