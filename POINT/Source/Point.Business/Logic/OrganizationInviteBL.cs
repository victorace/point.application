﻿using Point.Business.Helper;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Point.Business.Logic
{
    public class OrganizationInviteBL
    {
        public static OrganizationInvite GetMyInvite(IUnitOfWork uow, int flowInstanceID, PointUserInfo pointUserInfo)
        {
            var currentactiveinvites = GetAcceptedOrActiveInvitesByFlowInstanceIDAndInviteType(uow, flowInstanceID, InviteType.Regular);
            OrganizationInvite myInvite = null;

            //Find out wich one to accept were having multiple ones
            if (currentactiveinvites.Count > 1)
            {
                //on department level
                myInvite = currentactiveinvites.FirstOrDefault(inv => pointUserInfo.EmployeeDepartmentIDs.Contains(inv.DepartmentID));

                if (myInvite == null)
                {
                    //on organization level
                    myInvite = currentactiveinvites.FirstOrDefault(inv => pointUserInfo.EmployeeOrganizationIDs.Contains(inv.OrganizationID));
                }

                if (myInvite == null)
                {
                    //probably techxxadmin or rb
                    //Just take the first one
                    myInvite = currentactiveinvites.OrderBy(inv => inv.InviteCreated).FirstOrDefault();
                }
            } else
            {
                myInvite = currentactiveinvites.OrderBy(inv => inv.InviteCreated).FirstOrDefault();
            }

            return myInvite;
        }

        public static void SetHandlingData(IUnitOfWork uow, OrganizationInvite invite, PointUserInfo pointuserinfo, string remark)
        {
            if (invite == null) return;

            invite.HandlingDateTime = DateTime.Now;
            invite.HandlingBy = pointuserinfo?.Employee?.FullName();
            invite.HandlingRemark = (remark != null && remark.Length > 200) ? remark.Substring(0, 200) : remark;
        }

        public static void SetStatus(IUnitOfWork uow, OrganizationInvite invite, InviteStatus status)
        {
            if (invite == null) return;

            invite.InviteStatus = status;

            uow.Save();
        }

        public static OrganizationInvite GetByID(IUnitOfWork uow, int organizationinviteid)
        {
            return uow.OrganizationInviteRepository.GetByID(organizationinviteid);
        }

        public static List<OrganizationInvite> GetByFlowInstanceIDAndInviteType(IUnitOfWork uow, int flowinstanceid, InviteType inviteType)
        {
            return uow.OrganizationInviteRepository.Get(it => 
                it.FlowInstanceID == flowinstanceid && 
                it.InviteType == inviteType
                , includeProperties: "Organization,Department")
                .OrderByDescending(it => it.InviteStatus == InviteStatus.Realized)
                .ThenByDescending(it => it.InviteStatus == InviteStatus.Acknowledged)
                .ThenByDescending(it => it.InviteStatus == InviteStatus.AcknowledgedPartially)
                .ThenByDescending(it => it.InviteStatus == InviteStatus.Active)
                .ThenByDescending(it => it.InviteStatus == InviteStatus.Queued)
                .ThenByDescending(it => it.InviteSend)
                .ThenBy(it => it.InviteCreated)
                .ToList();
        }

        public static List<OrganizationInvite> GetByFlowInstanceIDAndPhaseDefinitionID(IUnitOfWork uow, int flowinstanceid, int phasedefinitionid)
        {
            return uow.OrganizationInviteRepository.Get(it => it.FlowInstanceID == flowinstanceid && it.RecievingPhaseDefinitionID == phasedefinitionid, includeProperties: "Organization,Department")
                .OrderByDescending(it => it.InviteStatus == InviteStatus.Realized)
                .ThenByDescending(it => it.InviteStatus == InviteStatus.Acknowledged)
                .ThenByDescending(it => it.InviteStatus == InviteStatus.AcknowledgedPartially)
                .ThenByDescending(it => it.InviteStatus == InviteStatus.Active)
                .ThenByDescending(it => it.InviteStatus == InviteStatus.Queued)
                .ThenByDescending(it => it.InviteOrder)
                .ThenByDescending(it => it.InviteCreated).ToList();
        }

        public static List<OrganizationInvite> GetQueuedInvitesByFlowInstanceIDAndInviteType(IUnitOfWork uow, int flowinstanceid, InviteType inviteType)
        {
            return uow.OrganizationInviteRepository.Get(it => it.FlowInstanceID == flowinstanceid && it.SendingPhaseDefinitionID != null && it.InviteType == inviteType && it.InviteStatus == InviteStatus.Queued).ToList();
        }

        public static List<OrganizationInvite> GetAcceptedOrActiveInvitesByFlowInstanceIDAndInviteType(IUnitOfWork uow, int flowinstanceid, InviteType inviteType)
        {
            var activestatuses = new List<InviteStatus>() { InviteStatus.Active, InviteStatus.Acknowledged, InviteStatus.Realized };
            return uow.OrganizationInviteRepository.Get(it => it.FlowInstanceID == flowinstanceid && it.InviteType == inviteType && activestatuses.Contains(it.InviteStatus)).ToList();
        }

        public static List<OrganizationInvite> GetAcceptedOrActiveInvitesByFlowInstanceIDAndAccessGroup(IUnitOfWork uow, int flowinstanceid, string accessGroup)
        {
            var activestatuses = new List<InviteStatus>() { InviteStatus.Active, InviteStatus.Acknowledged, InviteStatus.Realized };
            return uow.OrganizationInviteRepository.Get(it => it.FlowInstanceID == flowinstanceid && activestatuses.Contains(it.InviteStatus) && it.RecievingPhaseDefinition.AccessGroup == accessGroup).ToList();
        }

        public static List<OrganizationInvite> GetAcceptedOrActiveInvitesByFlowInstanceIDAndPhaseDefinitionID(IUnitOfWork uow, int flowinstanceid, int phasedefinitionid)
        {
            var activestatuses = new List<InviteStatus>() { InviteStatus.Active, InviteStatus.Acknowledged, InviteStatus.Realized };
            return uow.OrganizationInviteRepository.Get(it => it.FlowInstanceID == flowinstanceid && it.RecievingPhaseDefinitionID == phasedefinitionid && activestatuses.Contains(it.InviteStatus)).ToList();
        }

        public static List<OrganizationInvite> GetByFlowInstanceID(IUnitOfWork uow, int flowinstanceid)
        {
            return uow.OrganizationInviteRepository.Get(it => it.FlowInstanceID == flowinstanceid).ToList();
        }

        public static OrganizationInvite GetPartiallyAcknowledgedByFlowInstanceID(IUnitOfWork uow, int flowinstanceid)
        {
            return uow.OrganizationInviteRepository.FirstOrDefault(it => it.FlowInstanceID == flowinstanceid && it.InviteStatus == InviteStatus.AcknowledgedPartially);
        }

        public static List<OrganizationInvite> GetPreviousInvitesToBeAcknowledgedByFlowInstanceID(IUnitOfWork uow, int flowinstanceid)
        {
            return uow.OrganizationInviteRepository.Get(it => it.FlowInstanceID == flowinstanceid && it.NeedsAcknowledgement && it.InviteStatus >= InviteStatus.Active).ToList();
        }

        public static List<OrganizationInvite> GetInvitesToBeAcknowledgedByFlowInstanceID(IUnitOfWork uow, int flowinstanceid)
        {
            return uow.OrganizationInviteRepository.Get(it => it.FlowInstanceID == flowinstanceid && it.NeedsAcknowledgement).ToList();
        }

        public static bool Delete(IUnitOfWork uow, int organizationinviteid)
        {
            var invitetodelete = GetByID(uow, organizationinviteid);
            
            //only delete if its not allready sent
            if(invitetodelete.InviteSend == null)
            {
                uow.OrganizationInviteRepository.Delete(it => it.OrganizationInviteID == organizationinviteid && it.InviteSend == null);
                uow.Save();
                return true;
            } 

            return false;
        }

        public static OrganizationInvite Add(IUnitOfWork uow, int flowinstanceid, int sendingPhaseDefinitionID, int recievingPhaseDefinitionID, int departmentid, bool needsacknowledgement, InviteType inviteType, LogEntry logEntry)
        {
            var department = DepartmentBL.GetByDepartmentID(uow, departmentid);
            if(department?.Location?.OrganizationID == null)
            {
                throw new Exception("Afdeling " + departmentid + " heeft geen gekoppelde organisatie.");
            }

            var invite = new OrganizationInvite() {
                FlowInstanceID = flowinstanceid,
                SendingPhaseDefinitionID = sendingPhaseDefinitionID,
                RecievingPhaseDefinitionID = recievingPhaseDefinitionID,
                OrganizationID = department.Location.OrganizationID,
                DepartmentID = departmentid,
                InviteCreated = logEntry.Timestamp,
                CreatedByID = logEntry.EmployeeID,
                NeedsAcknowledgement = needsacknowledgement,
                InviteType = inviteType
            };

            uow.OrganizationInviteRepository.Insert(invite);
            uow.Save();

            return invite;
        }

        public static void CancelInviteHandleNextInQueue(IUnitOfWork uow, FlowInstance flowInstance, PhaseInstance sendingPhase, PointUserInfo pointUserInfo, string dashboardUrl, string cancelReason = "")
        {
            if (flowInstance == null || sendingPhase == null)
            {
                return;
            }

            var logentry = LogEntryBL.Create(FlowFormType.NoScreen, pointUserInfo);
            var currentinvite = OrganizationInviteBL.GetAcceptedOrActiveInvitesByFlowInstanceIDAndInviteType(uow, flowInstance.FlowInstanceID, InviteType.Regular).FirstOrDefault();
            if(currentinvite == null)
            {
                return;
            }

            OrganizationHelper.CancelInvite(uow, currentinvite, pointUserInfo, true, cancelReason);
            OrganizationHelper.CancelReceivingPhases(uow, flowInstance.FlowInstanceID, currentinvite?.RecievingPhaseDefinitionID ?? 0, logentry);
            OrganizationHelper.ClearDestinationField(uow, flowInstance.FlowInstanceID);
            SignaleringBL.ExpireSignaleringByFlowInstanceIDAndDepartmentID(uow, flowInstance.FlowInstanceID, currentinvite.DepartmentID);

            //Eventuele invite die in de wacht staan activeren.
            var quedinvite = OrganizationHelper.InviteQueued(uow, flowInstance, pointUserInfo, dashboardUrl);
            if (quedinvite != null)
            {
                PhaseInstanceBL.Create(uow, flowInstance.FlowInstanceID, quedinvite.RecievingPhaseDefinitionID, logentry);
                OrganizationHelper.SetDestinationField(uow, quedinvite.FlowInstanceID, quedinvite.DepartmentID);

                if (flowInstance.FlowDefinition.FlowTypeID == (int)FlowTypeID.HA_VVT)
                {
                    MedVRIBL.SendMedvriHAVVT(uow, flowInstance, FlowFormType.VastleggenBesluitVVTZHVVT, pointUserInfo, TemplateTypeID.ZorgMailHAVVTNeeEnNieuweVVT, currentinvite.Department);
                }

                if (sendingPhase.Status == (int)Status.Active)
                {
                    sendingPhase.Status = (int)Status.Done;
                }
            }
            else
            {
                OrganizationHelper.ClearDestinationField(uow, flowInstance.FlowInstanceID);
                if (sendingPhase.Status == (int)Status.Done)
                {
                    sendingPhase.Status = (int)Status.Active;
                }
            }

            uow.Save();
        }

        public static void CancelAllInvitesInQueue(IUnitOfWork uow, FlowInstance flowInstance, InviteType inviteType)
        {
            if (flowInstance == null)
            {
                return;
            }
            var organizationinvites = GetQueuedInvitesByFlowInstanceIDAndInviteType(uow, flowInstance.FlowInstanceID, inviteType);

            if (organizationinvites == null || !organizationinvites.Any())
            {
                return;
            }         
            
            foreach (var organizationinvite in organizationinvites)
            {
                organizationinvite.InviteStatus = InviteStatus.Cancelled;
            }

            uow.Save();
        }


    }
}
