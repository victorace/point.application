﻿using System;
using System.Data.Entity;
using System.Linq;
using Point.Database.Models;
using Point.Database.Repository;

namespace Point.Business.Logic
{
    public class ScheduleHistoryBL
    {
        public static bool Exists(IUnitOfWork uow, int frequencyid, DateTime timestamp)
        {
            return uow.ScheduleHistoryRepository.Get(sh => sh.FrequencyID == frequencyid && DbFunctions.TruncateTime(sh.Timestamp) == timestamp).Any();
        }
        
        public static void Create(IUnitOfWork uow, int frequencyid, DateTime timestamp)
        {
            var schedule = new ScheduleHistory()
            {
                FrequencyID = frequencyid,
                Timestamp = timestamp
            };

            uow.ScheduleHistoryRepository.Insert(schedule);
            uow.Save();
        }
    }
}
