﻿using Point.Business.Helper;
using Point.Database.Context;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Extensions;
using Point.Infrastructure.Helpers;
using Point.Models.Enums;
using Point.ServiceBus.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Point.Business.Logic
{
    public static class TransferAttachmentBL
    {
        public static TransferAttachment GetByID(IUnitOfWork uow, int transferAttachmentID)
        {
            return uow.TransferAttachmentRepository.GetByID(transferAttachmentID);
        }

        public static GenericFile GetFileByID(IUnitOfWork uow, int transferAttachmentID)
        {
            return GetByID(uow, transferAttachmentID)?.GenericFile;
        }

        public static IEnumerable<TransferAttachment> GetByTransferID(IUnitOfWork uow, int transferID)
        {
            return uow.TransferAttachmentRepository.Get(ta => ta.TransferID == transferID && !ta.Deleted);
        }

        public static List<AttachmentTypeID> GetAttachmentTypeIDsByTransferID(IUnitOfWork uow, int transferID)
        {
            return uow.TransferAttachmentRepository.Get(ta => ta.TransferID == transferID && !ta.Deleted)
                .Select(ta => ta.AttachmentTypeID).Distinct().ToList();
        }

        public static IEnumerable<int> GetIDsByTransferIDs(IUnitOfWork uow, IEnumerable<int> transferIDs)
        {
            return uow.TransferAttachmentRepository.Get(ta => ta.TransferID != null && transferIDs.Contains((int)ta.TransferID)).Select(ta => ta.AttachmentID);
        }

        public static void DeleteExistingVOAttachments(IUnitOfWork uow, int transferID, LogEntry logentry)
        {
            var attachments = uow.TransferAttachmentRepository.Get(ta =>
                ta.TransferID == transferID &&
                ta.AttachmentTypeID == AttachmentTypeID.ExternVODocument &&
                ta.Deleted == false
            );

            foreach (var attachment in attachments)
            {
                attachment.FormSetVersionID = null;
                attachment.Deleted = true;

                LoggingBL.FillLoggingWithID(uow, attachment, logentry);
            }

            uow.Save();
        }

        public static void DeleteByFormSetVersionID(IUnitOfWork uow, int formSetVersionID, LogEntry logentry)
        {
            var attachmentstodelete = uow.TransferAttachmentRepository.Get(it => it.FormSetVersionID == formSetVersionID).ToList();
            foreach (var attachment in attachmentstodelete)
            {
                attachment.Deleted = true;
                if (attachment.GenericFile != null)
                {
                    attachment.GenericFile.Deleted = true;
                }
                LoggingBL.FillLoggingWithID(uow, attachment, logentry);
            }
            uow.Save();
        }

        public static void Insert(IUnitOfWork uow, TransferAttachment transferAttachment, LogEntry logentry)
        {
            transferAttachment.UploadDate = DateTime.Now;
            LoggingBL.FillLoggingWithID(uow, transferAttachment, logentry);
            uow.TransferAttachmentRepository.Insert(transferAttachment);
        }

        public static void FillFileData(TransferAttachment transferAttachment, HttpPostedFileBase httpPostedFileBase)
        {
            byte[] bytes = new byte[httpPostedFileBase.ContentLength];
            httpPostedFileBase.InputStream.Read(bytes, 0, httpPostedFileBase.ContentLength);

            FillFileData(transferAttachment, bytes, httpPostedFileBase.ContentType, httpPostedFileBase.FileName);
        }

        public static void FillFileData(TransferAttachment transferAttachment, byte[] bytes, string contenttype, string filename)
        {
            if (transferAttachment.GenericFile == null)
            {
                transferAttachment.GenericFile = new GenericFile
                {
                    GenericFileID = Guid.NewGuid()
                };
            }

            transferAttachment.GenericFile.ContentType = contenttype;
            transferAttachment.GenericFile.FileData = bytes;
            transferAttachment.GenericFile.FileName = filename;
            transferAttachment.GenericFile.FileSize = bytes.Length;
            transferAttachment.GenericFile.TimeStamp = DateTime.Now;
        }

        public static void DeleteByID(IUnitOfWork uow, int transferAttachmentID, LogEntry logentry)
        {
            TransferAttachment transferAttachment = uow.TransferAttachmentRepository.GetByID(transferAttachmentID);
            if (transferAttachment != null)
            {
                transferAttachment.Deleted = true;
                LoggingBL.FillLoggingWithID(uow, transferAttachment, logentry);
            }
        }

        public static bool HasRightsUpdateOrDeleteTransferAttachment(IUnitOfWork uow, int transferid, int employeeID, PointUserInfo pointuserinfo)
        {
            if (transferid < 1)
            {
                return false;
            }

            if (employeeID == pointuserinfo.Employee.EmployeeID)
            {
                return true;
            }

            var flowinstance = FlowInstanceBL.GetByTransferID(uow, transferid);
            if (flowinstance != null)
            {
                var systememployee = PointUserInfoHelper.GetSystemEmployee(uow);
                if (systememployee.Employee.EmployeeID == employeeID)
                {
                    var organization = OrganizationHelper.GetSendingOrganization(uow, flowinstance.FlowInstanceID);
                    if (organization?.OrganizationID == pointuserinfo?.Organization.OrganizationID)
                    {
                        return true;
                    }
                }
            }

            if (pointuserinfo?.IsTransferEmployee ?? false) // #14449 Verwijderen bijlage quick fix
            {
                return true;
            }

            bool hasRightsUpdateItem = false;
            var role = pointuserinfo.Employee.PointRole;
            switch (role)
            {
                case Role.Global:
                    hasRightsUpdateItem = true;
                    break;
                case Role.Region:
                    hasRightsUpdateItem = RegionBL.InSameRegion(uow, employeeID, pointuserinfo);
                    break;
                case Role.Organization:
                    hasRightsUpdateItem = OrganizationBL.InSameOrganization(uow, employeeID, pointuserinfo);
                    break;
                case Role.Location:
                    hasRightsUpdateItem = LocationBL.InSameLocation(uow, employeeID, pointuserinfo);
                    break;
                case Role.Department:
                    hasRightsUpdateItem = DepartmentBL.InSameDepartment(uow, employeeID, pointuserinfo);
                    break;
                default:
                    hasRightsUpdateItem = false;
                    break;
            }
            return hasRightsUpdateItem;
        }

        public static void DeleteAndSave(IUnitOfWork uow, TransferAttachment transferAttachment, LogEntry logentry)
        {
            DeleteByID(uow, transferAttachment.AttachmentID, logentry);

            uow.Save();
        }

        public static IQueryable<TransferAttachment> GetByFrequencyID(IUnitOfWork uow, int frequencyid)
        {
            return uow.TransferAttachmentRepository.Get(ta => ta.FrequencyTransferAttachment.Any(fta => fta.FrequencyID == frequencyid));
        }

        public static bool TakeOverTransferAttachments(IUnitOfWork uow, FlowInstance relatedFlowInstance, int transferID, PointUserInfo pointUserInfo)
        {
            var result = false;
            if (relatedFlowInstance == null || relatedFlowInstance.FlowDefinitionID != FlowDefinitionID.VVT_VVT)
            {
                return result;
            }
            //get all transfer attachments. even deleted memo's. (they could be added previously and then removed by user. so you dont want to insert them again)
            if (uow.TransferAttachmentRepository.Get(ta => ta.TransferID == transferID).Any())
            {
                return result;
            }
            if (!relatedFlowInstance.RelatedTransferID.HasValue || relatedFlowInstance.RelatedTransferID.Value == (int)TakeOverRelatedTransferStatus.DoNotTakeOver)
            {
                return result;
            }

            var sourceAttachments = uow.TransferAttachmentRepository.Get(ta => ta.TransferID == relatedFlowInstance.RelatedTransferID &&
            (!ta.Deleted) && ta.AttachmentSource == AttachmentSource.None && ta.FormSetVersionID == null ,asNoTracking: true).ToList();

            foreach (var attachment in sourceAttachments)
            {
                var destAttachment = new TransferAttachment
                {
                    TransferID = transferID,
                    Name = attachment.Name,
                    Description = attachment.Description,
                    UploadDate = DateTime.Now,
                    EmployeeID = pointUserInfo.Employee.EmployeeID,
                    Deleted = attachment.Deleted,
                    TimeStamp = attachment.TimeStamp,
                    ScreenID = attachment.ScreenID,
                    AttachmentTypeID = attachment.AttachmentTypeID,
                    ShowInAttachmentList = attachment.ShowInAttachmentList,
                    AttachmentSource = attachment.AttachmentSource,
                    FormSetVersionID = attachment.FormSetVersionID,
                    RelatedAttachmentID = attachment.AttachmentID,
                    GenericFile = new GenericFile
                    {
                        GenericFileID = Guid.NewGuid(),
                        FileName = attachment.GenericFile.FileName,
                        FileSize = attachment.GenericFile.FileSize,
                        FileData = attachment.GenericFile.FileData,
                        ContentType = attachment.GenericFile.ContentType,
                        TimeStamp = DateTime.Now,
                        Deleted = attachment.GenericFile.Deleted,
                    },
                };
                uow.TransferAttachmentRepository.Insert(destAttachment);
                result = true;
            }

            uow.Save();
            return result;
        }

        public static void CopyToTransfer(IUnitOfWork uow, Transfer source, Transfer destination)
        {
            var sourceAttachments = uow.TransferAttachmentRepository.Get(ta => ta.TransferID == source.TransferID &&
                (!ta.Deleted) && ta.AttachmentSource == AttachmentSource.None,
                asNoTracking: true).ToList();

            foreach (var attachment in sourceAttachments)
            {
                attachment.AttachmentID = 0;
                attachment.TransferID = destination.TransferID;
                attachment.RelatedAttachmentID = null;

                attachment.GenericFile = new GenericFile
                {
                    GenericFileID = Guid.NewGuid(),
                    FileName = attachment.GenericFile.FileName,
                    FileSize = attachment.GenericFile.FileSize,
                    FileData = attachment.GenericFile.FileData,
                    ContentType = attachment.GenericFile.ContentType,
                    TimeStamp = attachment.GenericFile.TimeStamp,
                    Deleted = attachment.GenericFile.Deleted
                };

                uow.TransferAttachmentRepository.Insert(attachment);
            }

            uow.Save();
        }

        public static void InsertFormAsPDFAndSave(int transferid, int formsetversionid, string description, PointUserInfo pointuserinfo, LogEntry logentry)
        {
            using (var uow = new UnitOfWork<PointContext>())
            {
                var formsetversion = FormSetVersionBL.GetByID(uow, formsetversionid);
                if (formsetversion == null) return;

                var contents = FormsetVersionPdfHelper.GetPDFData(formsetversion.FormSetVersionID, pointuserinfo.Employee.EmployeeID);
                if (contents != null)
                {
                    var transferAttachment = new TransferAttachment()
                    {
                        Name = formsetversion.FormType.Name,
                        Description = description,
                        AttachmentSource = AttachmentSource.None,
                        AttachmentTypeID = formTypeIDToAttachmentTypeID(formsetversion.FormTypeID),
                        TransferID = transferid,
                    };

                    FillFileData(transferAttachment, contents, "application/pdf", $"{formsetversion.FormType.Name}.pdf");

                    InsertAndSave(uow, transferAttachment, logentry);
                }

                return;
            }
        }

        public static IEnumerable<TransferAttachment> InsertFormAsPDFAndSave(IUnitOfWork uow, int sourceTransferID, int destinationTransferID, IEnumerable<int> formTypeIDs, LogEntry logentry)
        {
            var transferAttachments = new List<TransferAttachment>();
            foreach (int formTypeID in formTypeIDs)
            {
                transferAttachments.AddRange(insertFormAsPDFAndSave(uow, sourceTransferID, destinationTransferID, formTypeID, logentry));
            }
            return transferAttachments;
        }

        public static void InsertAndSave(IUnitOfWork uow, TransferAttachment transferAttachment, LogEntry logentry)
        {
            Insert(uow, transferAttachment, logentry);
            uow.Save();
        }

        public static bool HasAttachment(IUnitOfWork uow, int transferID)
        {
            return uow.TransferAttachmentRepository.Exists(ta => ta.TransferID == transferID && (!ta.Deleted));
        }

        public static bool HasAttachment(IUnitOfWork uow, int transferID, AttachmentTypeID attachmentTypeID)
        {
            return uow.TransferAttachmentRepository.Exists(ta => ta.TransferID == transferID && ta.AttachmentTypeID == attachmentTypeID && !ta.Deleted);
        }

        #region AttachmentReceivedTemp
        public static void Upsert(IUnitOfWork uow, int transferid, AttachmentReceivedTemp attachmentreceivedtemp, LogEntry logentry)
        {
            Upsert(uow, transferid, new List<AttachmentReceivedTemp>() { attachmentreceivedtemp }, logentry);
        }

        public static void Upsert(IUnitOfWork uow, int transferid, IEnumerable<AttachmentReceivedTemp> attachmentreceivedtemps, LogEntry logentry)
        {
            var attachments = new List<TransferAttachment>();

            // exclude the actual file data, only interested in TimeStamp, Name and AttachmentTypeID 
            var simpletransferattachments = uow.TransferAttachmentRepository.Get(ta => ta.TransferID == transferid && !ta.Deleted)
                .OrderByDescending(ta => ta.TimeStamp)
                .Select(ta => new
                {
                    TransferAttachmentID = ta.AttachmentID,
                    ta.Name,
                    TransferAttachmentType = ta.AttachmentTypeID,
                    Timestamp = ta.TimeStamp
                });

            foreach (var attachmentreceivedtemp in attachmentreceivedtemps)
            {
                // IH: Definitie: een document is een update van een reeds eerder toegestuurd document als de naam en het type hetzelfde zijn(en uiteraard ook opnamenummer, patientnummer en geb.datum).
                // Als een update document wordt gestuurd naar een dossier dat reeds bestaat dan vervangt de update het reeds bestaande document.
                // Als een update document  wordt gestuurd in het geval een dossier nog niet bestaat dat vervangt de update het reeds aanwezige document in de temp tabel.
                // Een document dat geen update is vervangt nooit een bestaand document.
                var simpleattachment = simpletransferattachments.FirstOrDefault(sa => sa.Name == attachmentreceivedtemp.Name &&
                    sa.TransferAttachmentType == attachmentreceivedtemp.AttachmentTypeID);

                byte[] filedata = Convert.FromBase64String(attachmentreceivedtemp.Base64EncodedData);

                if (simpleattachment == null)
                {
                    // insert
                    var genericfile = new GenericFile()
                    {
                        GenericFileID = Guid.NewGuid(),
                        ContentType = MimeMapping.GetMimeMapping(attachmentreceivedtemp.FileName),
                        FileData = filedata,
                        FileName = attachmentreceivedtemp.FileName,
                        FileSize = filedata.Length,
                        TimeStamp = attachmentreceivedtemp.TimeStamp
                    };

                    var transferattachment = new TransferAttachment()
                    {
                        Name = attachmentreceivedtemp.AttachmentTypeID.GetDescription(),
                        Description = attachmentreceivedtemp.Description,
                        AttachmentSource = AttachmentSource.None,
                        AttachmentTypeID = attachmentreceivedtemp.AttachmentTypeID,
                        UploadDate = attachmentreceivedtemp.TimeStamp,
                        TransferID = transferid,
                        Deleted = false,
                        ShowInAttachmentList = attachmentreceivedtemp.AttachmentReceivedTempID != (int)AttachmentTypeID.ExternVODocument,
                        FormSetVersionID = null,
                        GenericFile = genericfile
                    };

                    LoggingBL.FillLoggingWithID(uow, transferattachment, logentry);

                    uow.TransferAttachmentRepository.Insert(transferattachment);

                    attachments.Add(transferattachment);
                }
                else
                {
                    //update
                    var transferattachment = GetByID(uow, simpleattachment.TransferAttachmentID);
                    if (transferattachment != null)
                    {
                        var genericfile = transferattachment.GenericFile;
                        genericfile.ContentType = MimeMapping.GetMimeMapping(attachmentreceivedtemp.FileName);
                        genericfile.FileData = filedata;
                        genericfile.FileSize = filedata.Length;
                        genericfile.TimeStamp = attachmentreceivedtemp.TimeStamp;

                        transferattachment.Description = attachmentreceivedtemp.Description;
                        transferattachment.UploadDate = attachmentreceivedtemp.TimeStamp;
                        transferattachment.Deleted = false;
                        transferattachment.FormSetVersionID = null;
                        transferattachment.GenericFile = genericfile;

                        LoggingBL.FillLoggingWithID(uow, transferattachment, logentry);

                        attachments.Add(transferattachment);
                    }
                }
            }

            AttachmentReceivedTempBL.Delete(uow, transferid, attachments);

            uow.Save();
        }

        public static TransferAttachment InsertAndSave(IUnitOfWork uow, int transferid, int? formsetversionid,
            AttachmentReceivedTemp attachmentreceivedtemp, LogEntry logentry)
        {
            if (attachmentreceivedtemp == null)
            {
                return null;
            }

            byte[] filedata = Convert.FromBase64String(attachmentreceivedtemp.Base64EncodedData);

            var genericfile = new GenericFile()
            {
                GenericFileID = Guid.NewGuid(),
                ContentType = MimeMapping.GetMimeMapping(attachmentreceivedtemp.FileName),
                FileData = filedata,
                FileName = attachmentreceivedtemp.FileName,
                FileSize = filedata.Length,
                TimeStamp = attachmentreceivedtemp.TimeStamp
            };

            var transferattachment = new TransferAttachment()
            {
                Name = attachmentreceivedtemp.Name,
                Description = attachmentreceivedtemp.Description,
                AttachmentSource = AttachmentSource.None,
                AttachmentTypeID = attachmentreceivedtemp.AttachmentTypeID,
                UploadDate = attachmentreceivedtemp.TimeStamp,
                TransferID = transferid,
                Deleted = false,
                ShowInAttachmentList = attachmentreceivedtemp.ShowInAttachmentList,
                FormSetVersionID = formsetversionid,
                GenericFile = genericfile
            };

            LoggingBL.FillLoggingWithID(uow, transferattachment, logentry);

            uow.TransferAttachmentRepository.Insert(transferattachment);

            AttachmentReceivedTempBL.Delete(uow, transferid, transferattachment);

            uow.Save();

            return transferattachment;
        }

        public static bool ExistsAsAttachment(IUnitOfWork uow, int transferid, AttachmentReceivedTemp attachmentreceivedtemp)
        {
            // exclude the actual file data, only interested in TimeStamp, Name and AttachmentTypeID 
            var simpletransferattachments = uow.TransferAttachmentRepository                .Get(ta => ta.TransferID == transferid)
                .OrderByDescending(ta => ta.TimeStamp)
                .Select(ta => new
                {
                    TransferAttachmentID = ta.AttachmentID,
                    Name = ta.Name,
                    TransferAttachmentType = ta.AttachmentTypeID,
                    Timestamp = ta.TimeStamp
                });

            return simpletransferattachments.Any(sa => sa.Name == attachmentreceivedtemp.Name &&
                    sa.TransferAttachmentType == attachmentreceivedtemp.AttachmentTypeID);
        }
        #endregion

        private static AttachmentTypeID formTypeIDToAttachmentTypeID(int formTypeID)
        {
            AttachmentTypeID attachmentTypeID;

            switch (formTypeID)
            {
                case (int)FlowFormType.MSVTUitvoeringsverzoek:
                    attachmentTypeID = AttachmentTypeID.FullfillmentRequest;
                    break;
                case (int)FlowFormType.GRZFormulier:
                    attachmentTypeID = AttachmentTypeID.GRZForm;
                    break;
                case (int)FlowFormType.VerpleegkundigeOverdrachtFlow:
                case (int)FlowFormType.VersturenVO:
                    attachmentTypeID = AttachmentTypeID.NurseTransfer;
                    break;
                case (int)FlowFormType.MSVTIndicatiestelling:
                    attachmentTypeID = AttachmentTypeID.MSVTIndication;
                    break;
                case (int)FlowFormType.GVp:
                    attachmentTypeID = AttachmentTypeID.GVpForm;
                    break;
                default:
                    attachmentTypeID = AttachmentTypeID.None;
                    break;
            }

            return attachmentTypeID;
        }

        private static IEnumerable<TransferAttachment> insertFormAsPDFAndSave(IUnitOfWork uow, int sourceTransferID, int destinationTransferID, int formTypeID, LogEntry logentry)
        {
            var transferAttachments = new List<TransferAttachment>();

            var pointUserInfo = PointUserInfoHelper.GetPointUserInfo(uow);

            var versions = FormSetVersionBL.GetByTransferIDAndFormTypeID(uow, sourceTransferID, formTypeID).ToList();
            foreach (var version in versions)
            {
                var contents = FormsetVersionPdfHelper.GetPDFData(version.FormSetVersionID, pointUserInfo.Employee.EmployeeID);
                if (contents != null)
                {
                    var transferAttachment = new TransferAttachment()
                    {
                        Name = version.FormType.Name,
                        Description = string.Format("{0} - Overgenomen uit dossier {1}", version.FormType.Name, sourceTransferID.ToString()),
                        AttachmentSource = AttachmentSource.None,
                        AttachmentTypeID = formTypeIDToAttachmentTypeID(formTypeID),
                        TransferID = destinationTransferID,
                    };

                    FillFileData(transferAttachment, contents, "application/pdf", string.Format("{0}.pdf", version.FormType.Name));

                    InsertAndSave(uow, transferAttachment, logentry);

                    transferAttachments.Add(transferAttachment);
                }
            }

            return transferAttachments;
        }

        #region DumpReport
        public static TransferAttachment CreateZipTransferAttachment(int employeeId, byte[] fileData, string fileName, AttachmentTypeID attachmentTypeID)
        {
            using (var uow = new UnitOfWork<PointContext>())
            {
                var oldFiles = uow.TransferAttachmentRepository.Get(t => t.EmployeeID == employeeId && !t.Deleted && t.AttachmentTypeID == attachmentTypeID);
                foreach (var dump in oldFiles)
                {
                    dump.Deleted = true;
                    dump.TimeStamp = DateTime.Now;
                    dump.GenericFile.Deleted = true;
                    dump.GenericFile.FileData = new byte[0];
                }

                var ta = new TransferAttachment
                {
                    Name = attachmentTypeID.GetDescription(),
                    UploadDate = DateTime.Now,
                    EmployeeID = employeeId,
                    ShowInAttachmentList = false,
                    AttachmentSource = AttachmentSource.None,
                    AttachmentTypeID = attachmentTypeID
                };

                FillFileData(ta, fileData, "application/zip", fileName);

                uow.TransferAttachmentRepository.Insert(ta);
                uow.Save();

                return ta;
            }
        }
        #endregion

        public static void FromModel(TransferAttachment attachment, TransferAttachmentViewModel model, int employeeID, int transferID)
        {
            attachment.AttachmentSource = model.AttachmentSource;
            attachment.AttachmentTypeID = model.AttachmentTypeID;
            attachment.Description = model.Description;
            attachment.EmployeeID = employeeID;
            attachment.Name = model.Name;
            attachment.ShowInAttachmentList = model.ShowInAttachmentList;
            attachment.TransferID = transferID;
            attachment.UploadDate = DateTime.Now;
        }

        public static void HandleThirdParty(IUnitOfWork uow, TransferAttachment transferAttachment, FlowInstance flowInstance, PointUserInfo pointUserInfo, int TransferID)
        {
            if (transferAttachment == null)
            {
                return;
            }

            var receivingorganization = OrganizationHelper.GetReceivingOrganization(uow, flowInstance.FlowInstanceID);
            if (receivingorganization == null)
            {
                return;
            }

            if (FlowInstanceBL.IsValidNedapDossierStatus(uow, flowInstance, pointUserInfo) && OrganizationHelper.IsValidNedapOrganization(uow, receivingorganization, transferAttachment.AttachmentTypeID))
            {
                var receivingdepartment = OrganizationHelper.GetRecievingDepartment(uow, flowInstance.FlowInstanceID);

                ServiceBusBL.SendMessage(uow, "FlowInstance", "SendTransferAttachment",
                    new SendTransferAttachment()
                    {
                        MailType = CommunicationLogType.NedapAttachment,
                        RecievingDepartmentID = receivingdepartment?.DepartmentID,
                        RecievingOrganizationID = receivingorganization.OrganizationID,
                        SendingEmployeeID = pointUserInfo.Employee.EmployeeID,
                        TimeStamp = DateTime.Now,
                        UniqueID = DateTime.Now.Ticks,
                        TransferAttachmentID = transferAttachment.AttachmentID,
                        TransferID = TransferID
                    });
            }
        }
    }
}
