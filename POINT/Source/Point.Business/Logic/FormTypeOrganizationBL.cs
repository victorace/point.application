﻿using System;
using System.Collections.Generic;
using System.Linq;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;

namespace Point.Business.Logic
{
    public class FormTypePerOrganizationComparer : IEqualityComparer<FormTypePerOrganization>
    {
        public bool Equals(FormTypePerOrganization x, FormTypePerOrganization y)
        {
            if (x.IsSelected != y.IsSelected)
            {
                return false;
            }

            if (x.FormTypeID != y.FormTypeID)
            {
                return false;
            }

            if (x.FlowDefinitionID != y.FlowDefinitionID)
            {
                return false;
            }

            if (x.RegionID != y.RegionID)
            {
                return false;
            }

            return true;
        }

        public int GetHashCode(FormTypePerOrganization obj)
        {
            var hashstring = $"{obj.RegionID}{(int)obj.FlowDefinitionID}{obj.FormTypeID}{(obj.IsSelected ? 1 : 0)}";

            return Convert.ToInt32(hashstring);
        }
    }

    public class FormTypeOrganizationBL
    {
        public static List<FormTypeOrganization> GetByOrganizationID(IUnitOfWork uow, int organizationId)
        {
            return uow.FormTypeOrganizationRepository.Get(fto => fto.OrganizationID == organizationId).ToList();
        }
        
        public static void Save(IUnitOfWork uow, OrganizationViewModel viewmodel, LogEntry logEntry)
        {
            if(viewmodel.OrganizationID == 0)
            {
                return;
            }

            var organization = OrganizationBL.GetByOrganizationID(uow, viewmodel.OrganizationID);

            var newformtypes = viewmodel.FormTypesOfOrganization.Where(ftr => ftr.IsSelected).OrderBy(ftr => ftr.FlowDefinitionID).ThenBy(ftr => ftr.FormTypeID).ToList();
            var oldformtypes = OrganizationViewBL.CreateFormTypeSet(organization).OrderBy(ftr => ftr.FlowDefinitionID).ThenBy(ftr => ftr.FormTypeID);

            var isEqual = oldformtypes.SequenceEqual(newformtypes, new FormTypePerOrganizationComparer());
            if(isEqual)
            {
                return;
            }

            var organizationformtypes = organization.FormTypeOrganization.ToList();

            var toDelete = organizationformtypes.Where(oft => !newformtypes.Any(nft => nft.FlowDefinitionID == oft.FlowDefinitionID && nft.FormTypeID == oft.FormTypeID)).ToList();
            var toAdd = newformtypes.Where(nft => !organizationformtypes.Any(oft => oft.FlowDefinitionID == nft.FlowDefinitionID && oft.FormTypeID == nft.FormTypeID)).ToList();


            foreach (var itemtodelete in toDelete)
            {
                uow.FormTypeOrganizationRepository.Delete(itemtodelete);
            }

            foreach (var itemtoadd in toAdd)
            {
                var formtypeorganization = new FormTypeOrganization()
                {
                    OrganizationID = viewmodel.OrganizationID,
                    FlowDefinitionID = itemtoadd.FlowDefinitionID,
                    FormTypeID = itemtoadd.FormTypeID,
                    ModifiedTimeStamp = logEntry.Timestamp
                };

                uow.FormTypeOrganizationRepository.Insert(formtypeorganization);
            }
        }
    }
}
