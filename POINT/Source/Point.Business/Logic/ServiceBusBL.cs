﻿using Point.Database.Context;
using Point.Database.Repository;
using Point.ServiceBus;
using Point.ServiceBus.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Point.Business.Logic
{
    public class ServiceBusBL
    {
        public static void SendMessage(string queue, string label, IPointServiceBusModel serializableobject, bool processlogging = true)
        {
            using (var uow = new UnitOfWork<PointContext>())
            {
                SendMessage(uow, queue, label, serializableobject, processlogging);
            }
        }

        public static void SendMessage(IUnitOfWork uow, string queue, string label, IPointServiceBusModel serializableobject, bool processlogging = true)
        {
            if (processlogging)
            {
                try
                {
                    ServiceBusLogBL.Add(uow, serializableobject, queue, label);
                }
                catch
                {
                    /* ignore */
                }
            }

            Task.Run(async () =>
            {
                try
                {
                    await Helpers.SendMessage(queue, label, serializableobject);
                }
                catch(Exception exc)
                {
                    /* ignore */
                }
            });
        }
    }
}
