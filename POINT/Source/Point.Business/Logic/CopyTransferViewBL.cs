﻿using Point.Database.Extensions;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Extensions;
using Point.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Point.Business.Logic
{
    public static class CopyTransferViewBL
    {
        public static CopyTransferViewModel Get(IUnitOfWork uow, FlowInstance flowinstance)
        {
            if (flowinstance == null)
            {
                throw new ArgumentNullException(nameof(flowinstance));
            }

            var formtypes = FormTypeBL.GetByTypeIDs(uow, new int[] { (int)TypeID.FlowTransferForm, (int)TypeID.FlowAanvraag }).ToList();
            if (FlowFieldValueBL.GetByTransferIDAndFieldID(uow, flowinstance.TransferID, FlowFieldConstants.ID_ZorgAdviesToevoegen).FirstOrDefault().GetValueOrDefault("Nee").StringToBool())
            {
                formtypes.ToList().Remove(formtypes.FirstOrDefault(ft => ft.FormTypeID == (int)FlowFormType.ZorgAdvies));
            }

            // Remove any Forms that have a higher Phase than our stop at Phase
            var phasedefinitioncache = PhaseDefinitionCacheBL.GetByFlowDefinitionID(uow, flowinstance.FlowDefinitionID);
            var stopatphasedefintion = CopyTransferBL.GetStopAtPhase(uow, flowinstance.FlowDefinitionID);
            formtypes.RemoveAll(ft => 
                phasedefinitioncache.FirstOrDefault(pdc => pdc.FormTypeID == ft.FormTypeID)?.Phase > stopatphasedefintion.Phase);

            var formtypeids = formtypes.Select(ft => ft.FormTypeID).ToList();
            var formsetversions = FormSetVersionBL.GetByTransferID(uow, flowinstance.TransferID).Where(fsv => formtypeids.Contains(fsv.FormTypeID)).ToList();

            List<CopyTransferFormViewModel> copytransferforms = new List<CopyTransferFormViewModel>();
            foreach (var formsetversion in formsetversions.Where(fsv => formtypeids.Contains(fsv.FormTypeID)).ToList())
            {
                copytransferforms.Add(new CopyTransferFormViewModel()
                {
                    FormSetVersionID = formsetversion.FormSetVersionID,
                    FormTypeID = formsetversion.FormType.FormTypeID,
                    TypeID = formsetversion.FormType.TypeID.Value,
                    Name = formsetversion.FormType.Name,
                    Description = formsetversion.Description,
                    Timestamp = formsetversion.UpdateDate ?? formsetversion.CreateDate,
                    EmployeeID = formsetversion.UpdatedByID ?? formsetversion.CreatedByID,
                    EmployeeName = formsetversion.UpdatedBy ?? formsetversion.CreatedBy,
                    Selected = formsetversion.FormType.TypeID == (int)TypeID.FlowAanvraag
                });
            }

            var vm = new CopyTransferViewModel()
            {
                TransferID = flowinstance.TransferID,
                CopyTransferForms = copytransferforms.OrderBy(ctf => ctf.Timestamp).ToList()
            };

            return vm;
        }
    }
}