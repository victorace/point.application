﻿using System.Collections.Generic;
using System.Linq;
using Point.Business.Helper;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;

namespace Point.Business.Logic
{
    /// <summary>
    /// Assigns modify/delete rights of:
    /// - communication journals,
    /// - attachments.
    /// </summary>
    public class EditableBL
    {
        /// <summary>
         /// 15995: Communicatiejournaal alleen wijzigbaar door jezelf en medewerkers uit je organisatie.
         /// </summary>  
        public static void AssignRights(
            IUnitOfWork uow,
            IEnumerable<IEditable> editables, // communication journals
            PointUserInfo pointuserinfo)
        {
            foreach (var editable in editables.ToList())
            {
                editable.OrganizationID = OrganizationBL.GetByEmployeeID(uow, editable.EmployeeID).OrganizationID;
                editable.IsEditable = pointuserinfo.EmployeeOrganizationIDs.Contains(editable.OrganizationID);
            }
        }
        public static void AssignRights(
            IUnitOfWork uow,
            IEnumerable<IEditable> editables, // attachments
            int transferid,
            PointUserInfo pointuserinfo)
        {
            var flowinstance = FlowInstanceBL.GetByTransferID(uow, transferid);
            var sendingOrganizationId = OrganizationHelper.GetSendingOrganizationId(uow, flowinstance?.FlowInstanceID);
            var systemEmployeeId = PointUserInfoHelper.GetSystemEmployee(uow)?.Employee.EmployeeID;

            foreach (var editable in editables.ToList())
            {
                editable.OrganizationID = OrganizationBL.GetByEmployeeID(uow, editable.EmployeeID).OrganizationID;
                editable.IsEditable = IsEditable(uow, editable, pointuserinfo, sendingOrganizationId, systemEmployeeId);
            }
        }

        /// <summary>
        /// Whether a single specific attachment 
        /// Rules defined by issue 14282 & 15196.
        /// </summary>
        public static bool IsEditable(
            IUnitOfWork uow,
            IEditable editable, // attachment
            PointUserInfo user,
            int? attachmentSendingOrganizationId,
            int? systemEmployeeId)
        {
            var usersOrgIsTheSender = attachmentSendingOrganizationId != null
                                      && attachmentSendingOrganizationId == user.Organization.OrganizationID;

            // RULE 1: ATTACHMENT ADDED BY THIS USER
            if (editable.EmployeeID == user.Employee.EmployeeID)
            {
                return true;
            }

            // Aanpassing rules voor bewerken comm.journaal (en bijlagen)
            // https://linkassist.visualstudio.com/POINT.Application/_workitems/edit/15196
            //// RULE 2: USER IS TP-USER, AND USER'S ORG IS THE SENDER
            if (user.IsTransferEmployee && usersOrgIsTheSender)
            {
                return true;
            }

            // RULE 3: ATTACHMENT ADDED BY: A "NON-TP" USER FROM THIS USER'S ORG
            if (editable.OrganizationID == user.Organization.OrganizationID
                && !EmployeeBL.EmployeeIsOfDepartmentType(uow, editable.EmployeeID, DepartmentTypeID.Transferpunt))
            {
                return true;
            }

            // RULE 4: USER'S ORG IS THE SENDER, AND ATTACHMENT ADDED BY SYSTEM
            if (usersOrgIsTheSender && systemEmployeeId == editable.EmployeeID)
            {
                return true;
            }

            return false;
        }
    }
}