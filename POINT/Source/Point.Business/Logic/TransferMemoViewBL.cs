﻿using Point.Business.Helper;
using Point.Database.Extensions;
using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using Point.Infrastructure.Extensions;

namespace Point.Business.Logic
{
    public class TransferMemoViewBL
    {
        public static TransferMemoEditViewModel GetByTransferMemoID(IUnitOfWork uow, int transfermemoid)
        {
            var transfermemo = TransferMemoBL.GetByID(uow, transfermemoid);
            var model = new TransferMemoEditViewModel
            {
                TransferID = transfermemo.TransferID.GetValueOrDefault(0),
                TransferMemoID = transfermemoid,
                MemoContent = transfermemo.MemoContent
            };

            var frequency = FrequencyTransferMemoBL.GetByTransferMemoID(uow, transfermemoid);
            if (frequency != null)
            {
                model.FrequencyID = frequency.FrequencyID;
            }

            if (Enum.TryParse(transfermemo.Target, out TransferMemoTypeID target))
            {
                model.Target = target;
            }

            return model;
        }

        public static IEnumerable<TransferMemoViewModel> GetByFrequencyID(IUnitOfWork uow, int frequencyID, PointUserInfo pointuserinfo, 
            bool readOnly)
        {
            var list = uow.FrequencyTransferMemoRepository.Get(ftm => ftm.FrequencyID == frequencyID && 
                ftm.Frequency.Cancelled == false && ftm.TransferMemo.Deleted == false)
                .OrderByDescending(tm => tm.TransferMemo.TransferMemoID)
                .ToList();
            return getFromList(uow, list, readOnly, pointuserinfo);
        }

        public static bool HasAdditionalInfoRequestTPMemos(IUnitOfWork uow, int transferID, PointUserInfo pointuserinfo)
        {
            List<TransferMemoTypeID> targets = new List<TransferMemoTypeID>()
            {
                TransferMemoTypeID.AanvullendeGegevensBevindingenMedewerkerTransferpunt,
                TransferMemoTypeID.AanvullendeGegevensAfspraken,
                TransferMemoTypeID.IntercollegiateCommunications
            };

            return GetByTransferIDAndTransferMemoTargetType(uow, transferID, targets, pointuserinfo, false, false).Any();
        }

        public static IEnumerable<TransferMemoViewModel> GetCommunicationJournalByTransferID(
            IUnitOfWork uow, int transferID, PointUserInfo pointuserinfo, bool readOnly)
        {
            var targets = new List<TransferMemoTypeID>
            {
                TransferMemoTypeID.Zorgprofessional,
                TransferMemoTypeID.Transferpunt,
                TransferMemoTypeID.Patient
            };

            return GetByTransferIDAndTransferMemoTargetType(uow, transferID, targets, pointuserinfo, readOnly, false);
        }

        public static IEnumerable<TransferMemoViewModel> GetByTransferIDAndTransferMemoTargetType(IUnitOfWork uow, 
            int transferID, List<TransferMemoTypeID> targets, PointUserInfo pointuserinfo, bool readOnly, bool createtask)
        {
            var strTargets = targets.Select(t => t.ToString()).ToList();
            var communicationJournalEntries = uow.TransferMemoRepository.Get(tm => tm.TransferID == transferID && 
                tm.Deleted == false && strTargets.Contains(tm.Target))
                .OrderByDescending(tm => tm.TransferMemoID).ToList()
                .Select(transferMemo => new TransferMemoViewModel
                {
                    TransferMemo = new TransferMemo
                    {
                        TransferMemoID = transferMemo.TransferMemoID,
                        EmployeeID = transferMemo.EmployeeID,
                        EmployeeName = transferMemo.EmployeeName,
                        MemoDateTime = transferMemo.MemoDateTime,
                        MemoContent = transferMemo.MemoContent,
                        Target = transferMemo.Target,
                        TransferID = transferMemo.TransferID,
                        RelatedMemoID= transferMemo.RelatedMemoID,
                    },
                    EmployeeID = transferMemo.EmployeeID ?? 0,
                    IsEditable = false,
                    CanCreateTask = createtask,
                    RelatedMemoOwner = GetRelatedOwnerData(uow, transferMemo.RelatedMemoID)
                }).ToList();

            communicationJournalEntries = handleOrganization(uow, communicationJournalEntries).ToList();
            if (!readOnly)
            {
                EditableBL.AssignRights(uow, communicationJournalEntries, pointuserinfo);
            }

            return communicationJournalEntries;
        }

        private static string GetRelatedOwnerData(IUnitOfWork uow, int? relatedMemoID)
        {
            var relatedData = "";
            if (relatedMemoID.HasValue)
            {
                var relatedMemo = TransferMemoBL.GetByID(uow, relatedMemoID.Value);
                if (relatedMemo != null && relatedMemo.EmployeeID.HasValue)
                {
                    var relatedOrganization = OrganizationBL.GetByEmployeeID(uow, relatedMemo.EmployeeID.Value);
                    relatedData = string.Format("Origineel aangemaakt door: {0}.<br>Datum: {1}.<br>Organisatie: {2}", relatedMemo.EmployeeName, relatedMemo.MemoDateTime, relatedOrganization?.Name);
                }
            }
            return relatedData;
        }

        public static IEnumerable<TransferMemoViewModel> GetByTransferIDAndTransferMemoTargetType(IUnitOfWork uow, 
            int transferID, TransferMemoTypeID target, PointUserInfo pointuserinfo, bool readOnly, bool createtask)
        {
            var targets = new List<TransferMemoTypeID> { target };
            return GetByTransferIDAndTransferMemoTargetType(uow, transferID, targets, pointuserinfo, readOnly, createtask);
        }

        public static TransferMemo Insert(IUnitOfWork uow, int transferID, string memoContent, TransferMemoTypeID transferMemoTargetType)
        {
            PointUserInfo pointUserInfo = PointUserInfoHelper.GetPointUserInfo(uow);

            TransferMemo transferMemo = new TransferMemo()
            {
                EmployeeID = pointUserInfo.Employee.EmployeeID,
                EmployeeName = pointUserInfo.Employee.FullName(),
                MemoDateTime = DateTime.Now,
                MemoContent = memoContent,
                Target = transferMemoTargetType.ToString(),
                TransferID = transferID,
            };
            uow.TransferMemoRepository.Insert(transferMemo);
            return transferMemo;
        }

        public static void Update(IUnitOfWork uow, int transferMemoID, string memoContent)
        {
            PointUserInfo pointUserInfo = PointUserInfoHelper.GetPointUserInfo(uow);

            var transferMemo = uow.TransferMemoRepository.GetByID(transferMemoID);
            TransferMemoChanges transferMemoChange = new TransferMemoChanges()
            {
                TransferMemoID = transferMemo.TransferMemoID,
                OriginalContent = transferMemo.MemoContent,
                EmployeeID = pointUserInfo.Employee.EmployeeID,
                EmployeeName = pointUserInfo.Employee.FullName(),
                DateTimeChanged = DateTime.Now
            };
            uow.TransferMemoChangesRepository.Insert(transferMemoChange);

            transferMemo.MemoContent = memoContent;

            uow.Save();
        }

        private static IEnumerable<TransferMemoViewModel> handleOrganization(IUnitOfWork uow, List<TransferMemoViewModel> communicationJournalEntries)
        {
            foreach (var communicationJournalEntry in communicationJournalEntries)
            {
                if (communicationJournalEntry.TransferMemo.EmployeeID.HasValue)
                {
                    var employee = EmployeeBL.GetByID(uow, communicationJournalEntry.TransferMemo.EmployeeID.Value);

                    communicationJournalEntry.EmployeeContactInfo = SetEmployeeContactInfo(employee);

                    var memoChanges = getHistory(uow, communicationJournalEntry.TransferMemo?.TransferMemoID);
                    communicationJournalEntry.TransferMemoChanges = memoChanges;
                    communicationJournalEntry.LastChangedInfo = setLastChangedInfo(memoChanges?.FirstOrDefault());

                    var department = employee?.DefaultDepartment;
                    if (department != null)
                    {
                        communicationJournalEntry.DepartmentFullName = department.FullName();
                        communicationJournalEntry.DepartmentContactInfo = SetDepartmentContactInfo(department);
                    }
                }
            }
            return communicationJournalEntries;
        }

        private static IEnumerable<TransferMemoViewModel> handleFrequencyControls(IUnitOfWork uow, List<TransferMemoViewModel> communicationJournalEntries,
         PointUserInfo pointuserinfo, bool readOnly)
        {
            foreach (var communicationJournalEntry in communicationJournalEntries)
            {
                if (communicationJournalEntry.TransferMemo.EmployeeID.HasValue)
                {
                    var transfermemoemployeeid = communicationJournalEntry.TransferMemo.EmployeeID.Value;

                    var isEditable = DepartmentBL.InSameDepartment(uow, transfermemoemployeeid, pointuserinfo);
                    communicationJournalEntry.IsEditable = !readOnly && isEditable;

                    var employee = EmployeeBL.GetByID(uow, transfermemoemployeeid);
                    communicationJournalEntry.EmployeeContactInfo = SetEmployeeContactInfo(employee);

                    var memoChanges = getHistory(uow, communicationJournalEntry.TransferMemo?.TransferMemoID);
                    communicationJournalEntry.TransferMemoChanges = memoChanges;
                    communicationJournalEntry.LastChangedInfo = setLastChangedInfo(memoChanges?.FirstOrDefault());

                    var department = employee?.DefaultDepartment;
                    if (department != null)
                    {
                        communicationJournalEntry.DepartmentFullName = department.FullName();
                        communicationJournalEntry.DepartmentContactInfo = SetDepartmentContactInfo(department);
                    }
                }
            }

            return communicationJournalEntries;
        }

        private static string SetEmployeeContactInfo(Employee employee)
        {
            var contactInfo = new List<string>();
            if (employee != null)
            {
                if (!string.IsNullOrEmpty(employee.EmailAddress))
                {
                    contactInfo.Add("email:" + employee.EmailAddress);
                }
                if (!string.IsNullOrEmpty(employee.PhoneNumber))
                {
                    contactInfo.Add("telefoon:" + employee.PhoneNumber);
                }
            }

            return contactInfo.Any() ? string.Join(Environment.NewLine, contactInfo) : "Gegevens zijn niet beschikbaar";
        }

        private static string SetDepartmentContactInfo(Department department)
        {
            var contactInfo = new List<string>();
            if (department != null)
            {
                if (!string.IsNullOrEmpty(department?.EmailAddress))
                {
                    contactInfo.Add("email:" + department.EmailAddress);
                }
                if (!string.IsNullOrEmpty(department?.PhoneNumber))
                {
                    contactInfo.Add("telefoon:" + department.PhoneNumber);
                }
            }

            return contactInfo.Any() ? string.Join(Environment.NewLine, contactInfo) : "Gegevens zijn niet beschikbaar";
        }

        private static string setLastChangedInfo(TransferMemoChanges memochanges)
        {
            var result = "";
            if (memochanges != null)
            {
                result = string.Format("Laatst gewijzigd op {0}",memochanges.DateTimeChanged.ToPointDateHourDisplay());
            }          
            return result;
        }

        private static IEnumerable<TransferMemoChanges> getHistory(IUnitOfWork uow, int? transferMemoID)
        {
            return CommunicationJournalViewBL.GetCommunicationJournalHistoryByTransferMemoID(uow, transferMemoID?? 0);
          
        }


        private static IEnumerable<TransferMemoViewModel> getFromList(IUnitOfWork uow, List<FrequencyTransferMemo> list, 
            bool readOnly, PointUserInfo pointuserinfo)
        {
            var vm = list.Select(frequencytransfermemo => new TransferMemoViewModel()
            { 
                TransferMemo = new TransferMemo()
                {
                    TransferMemoID = frequencytransfermemo.TransferMemoID,
                    EmployeeID = frequencytransfermemo.TransferMemo.EmployeeID,
                    EmployeeName = frequencytransfermemo.TransferMemo.EmployeeName,
                    MemoDateTime = frequencytransfermemo.TransferMemo.MemoDateTime,
                    MemoContent = frequencytransfermemo.TransferMemo.MemoContent,
                    Target = frequencytransfermemo.TransferMemo.Target,
                    TransferID = frequencytransfermemo.TransferMemo.TransferID,
                    RelatedMemoID = frequencytransfermemo.TransferMemo.RelatedMemoID
                    
                },
                IsEditable = false
            }).ToList();

            vm = handleFrequencyControls(uow, vm, pointuserinfo, readOnly).ToList();
            return vm;
        }
    }
}