﻿using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Models.Enums;
using Point.Database.Repository;
using System.Collections.Generic;
using System.Linq;
using System;

namespace Point.Business.Logic
{
    public class TransferMemoBL
    {
        public static IEnumerable<TransferMemoTemplate> GetTemplatesByOrganizationID(IUnitOfWork uow, int? organizationid)
        {
            if (organizationid == null)
            {
                return Enumerable.Empty<TransferMemoTemplate>();
            }

            return uow.TransferMemoTemplateRepository.Get(it => it.OrganizationID == organizationid);
        }

        public static void DeleteTemplateByID(IUnitOfWork uow, int transfermemotemplateid)
        {
            uow.TransferMemoTemplateRepository.Delete(transfermemotemplateid);
            uow.Save();
        }

        public static TransferMemoTemplate GetTemplateByID(IUnitOfWork uow, int transfermemotemplateid)
        {
            return uow.TransferMemoTemplateRepository.FirstOrDefault(it => it.TransferMemoTemplateID == transfermemotemplateid);
        }

        public static IEnumerable<TransferMemoTemplate> GetTemplatesByOrganizationIDAndMemoTypeID(IUnitOfWork uow, int organizationid, TransferMemoTypeID transferMemoTypeID)
        {
            return uow.TransferMemoTemplateRepository.Get(it => it.OrganizationID == organizationid && it.TransferMemoTypeID == transferMemoTypeID);
        }

        public static void SaveTemplate(IUnitOfWork uow, TransferMemoTemplate model)
        {
            var current = GetTemplateByID(uow, model.TransferMemoTemplateID);
            if(current == null)
            {
                current = new TransferMemoTemplate();
            }

            current.Name = model.Name;
            current.OrganizationID = model.OrganizationID;
            current.TemplateText = model.TemplateText;
            current.TransferMemoTypeID = model.TransferMemoTypeID;

            if(current.TransferMemoTemplateID == 0)
            {
                uow.TransferMemoTemplateRepository.Insert(current);
            }
            uow.Save();
        }

        public static TransferMemo GetByID(IUnitOfWork uow, int transfermemoid)
        {
            return uow.TransferMemoRepository.GetByID(transfermemoid);
        }

        public static IEnumerable<TransferMemo> GetByTransferID(IUnitOfWork uow, int transferID)
        {
            return uow.TransferMemoRepository.Get(tm => tm.TransferID == transferID && tm.Deleted == false);
        }

        public static bool TakeOverTransferMemos(IUnitOfWork uow, FlowInstance relatedFlowInstance, Transfer destination, PointUserInfo pointUserInfo)
        {
            var result = false;
            if (relatedFlowInstance == null || relatedFlowInstance.FlowDefinitionID != FlowDefinitionID.VVT_VVT)
            {
                return result;
            }         
            if (!relatedFlowInstance.RelatedTransferID.HasValue || relatedFlowInstance.RelatedTransferID.Value == (int)TakeOverRelatedTransferStatus.DoNotTakeOver)
            {
                return result;
            }
            //get all transfer memo's. even deleted memo's. (they could be added previously and then removed by user. so you dont want to insert them again)
            if (uow.TransferMemoRepository.Get(tm => tm.TransferID == destination.TransferID).Any())
            {
                return result;
            }

            var stringtargets = new List<TransferMemoTypeID>
                        {
                            TransferMemoTypeID.Zorgprofessional,                          
                    }.Select(t => t.ToString()).ToList();

            var sourcememos = uow.TransferMemoRepository.Get(tm => tm.TransferID == relatedFlowInstance.RelatedTransferID.Value &&
                tm.Deleted == false && stringtargets.Contains(tm.Target), asNoTracking: true)
                .OrderBy(tm => tm.TransferMemoID)
                .ToList();

            foreach (var memo in sourcememos)
            {
                memo.TransferID = destination.TransferID;
                memo.RelatedMemoID = memo.TransferMemoID;
                memo.Target = TransferMemoTypeID.Zorgprofessional.ToString();
                memo.EmployeeID = pointUserInfo.Employee?.EmployeeID;
                memo.EmployeeName = pointUserInfo.Employee?.FullName();
                memo.MemoDateTime = DateTime.Now;
                memo.TransferMemoID = 0;

                uow.TransferMemoRepository.Insert(memo);

                destination.TransferMemos.Add(memo);
                result = true;
            }
            uow.Save();
            return result;
        }

        public static void DeleteRelatedMemo(IUnitOfWork uow, int transferMemoID)
        {
            TransferMemo transferMemo = uow.TransferMemoRepository.GetByID(transferMemoID);
            if (transferMemo != null)
            {
                transferMemo.Deleted = true;
                // LoggingBL.FillLoggingWithID(uow, transferMemo, logentry);
            }
            uow.Save();
        }

        public static void CopyTransferMemos(IUnitOfWork uow, Transfer source, Transfer destination, IEnumerable<TransferMemoTypeID> targets)
        {
            List<string> stringtargets = targets.Select(t => t.ToString()).ToList();
            var sourcememos = uow.TransferMemoRepository.Get(tm => tm.TransferID == source.TransferID && 
                tm.Deleted == false && stringtargets.Contains(tm.Target), asNoTracking: true)
                .OrderBy(tm => tm.TransferMemoID)
                .ToList();
        
            foreach (var memo in sourcememos)
            {
                memo.TransferMemoID = 0;
                memo.TransferID = destination.TransferID;
                memo.RelatedMemoID = null;
                uow.TransferMemoRepository.Insert(memo);
                destination.TransferMemos.Add(memo);
            }

            uow.Save();
        }

        public static IQueryable<TransferMemo> GetByFrequencyID(IUnitOfWork uow, int frequencyid)
        {
            return uow.TransferMemoRepository.Get(it => it.FrequencyTransferMemo.Any(ftm => ftm.FrequencyID == frequencyid));
        }
    }
}
