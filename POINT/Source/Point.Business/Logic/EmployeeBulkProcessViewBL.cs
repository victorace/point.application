﻿using Point.Database.Extensions;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using System.Collections.Generic;
using System.Linq;

namespace Point.Business.Logic
{
    public class EmployeeBulkProcessViewBL
    {
        public static EmployeeBulkProcessViewModel FromModel(Employee model)
        {
            return new EmployeeBulkProcessViewModel
            {
                EmployeeID = model.EmployeeID,
                LastLoginDate = model.aspnet_Users?.aspnet_Membership?.UTCToLocalTime(),
                EmployeeIsLocked = model.aspnet_Users?.aspnet_Membership?.IsLockedOut,
                EmployeeSelected = false,
                FirstName = model.FirstName,
                MiddleName = model.MiddleName,
                LastName = model.LastName,
                UserName = model.aspnet_Users?.UserName,
                Inactive = model.InActive
            };
        }

        public static List<EmployeeBulkProcessViewModel> FromModelList(IEnumerable<Employee> employees)
        {
            var result = new List<EmployeeBulkProcessViewModel>();
            foreach (var item in employees)
            {
                result.Add(FromModel(item));
            }
            return result;
        }

        public static void ProcessBulkRequest(IUnitOfWork uow, BulkProcessViewModel viewmodel, LogEntry logentry)
        {
            var employeeids = viewmodel?.Employees?.Where(emp => emp.EmployeeSelected).Select(em => em.EmployeeID);
            var employees = EmployeeBL.GetByIDs(uow, employeeids).ToList();
            foreach (var employee in employees)
            {
                EmployeeBL.LockOrUnlockEmployee(employee, (viewmodel.ProcessType == "BlockUsers"), logentry);
            }
            uow.Save(logentry);
        }
    }
}
