﻿using Point.Database.Extensions;
using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Point.Business.Logic
{
    public static class TransferTaskBL
    {
        public static List<TransferTask> GetByTransferID(IUnitOfWork uow, int transferid, bool showinactive)
        {
            return uow.TransferTaskRepository.Get(t => t.TransferID == transferid && (showinactive == true || !t.Inactive))
                .OrderBy(t => t.DueDate)
                .ToList();
        }

        public static TransferTask GetByTransferTaskID(IUnitOfWork uow, int transfertaskid)
        {
            return uow.TransferTaskRepository.FirstOrDefault(t => t.TransferTaskID == transfertaskid);
        }

        public static TransferTask SetStatus(IUnitOfWork uow, int transfertaskid, Status status, LogEntry logentry)
        {
            var transfertask = GetByTransferTaskID(uow, transfertaskid);
            bool mutation = transfertask.Status != status;
            setStatus(transfertask, status, logentry.EmployeeID);
            return save(uow, transfertask, logentry, mutation);
        }

        public static bool Delete(IUnitOfWork uow, int transfertaskid, LogEntry logentry)
        {
            var transfertask = GetByTransferTaskID(uow, transfertaskid);
            if (transfertask != null)
            {
                transfertask.Inactive = true;
                save(uow, transfertask, logentry, true);
                return true;
            }
            return false;
        }

        public static TransferTask Save(IUnitOfWork uow, TransferTaskViewModel viewmodel, LogEntry logentry)
        {
            DateTime now = DateTime.Now;
            int employeeid = logentry.EmployeeID;

            bool haschanged = false;

            var transfertask = GetByTransferTaskID(uow, viewmodel.TransferTaskID);
            if (transfertask == null)
            {
                transfertask = new TransferTask
                {
                    TransferID = viewmodel.TransferID,
                    CreatedDate = now,
                    CreatedByEmployeeID = employeeid,
                    Status = Status.Active
                };
            }
            else
            {
                haschanged = !equals(transfertask, viewmodel);
            }

            transfertask.Description = viewmodel.Description;
            transfertask.Comments = viewmodel.Comments;
            transfertask.DueDate = viewmodel.DueDate.Value;

            if (viewmodel.Completed)
            {
                transfertask.Status = Status.Done;
                transfertask.CompletedByEmployeeID = employeeid;
                transfertask.CompletedDate = now;
            }
            else
            {
                transfertask.Status = Status.Active;
                transfertask.CompletedByEmployeeID = null;
                transfertask.CompletedDate = null;
            }

            return save(uow, transfertask, logentry, haschanged);
        }

        private static bool equals(TransferTask model, TransferTaskViewModel viewmodel)
        {
            if (model != null && viewmodel == null) return false;
            if (model == null && viewmodel != null) return false;

            if (model.TransferTaskID != viewmodel.TransferTaskID) return false;
            if (model.TransferID != viewmodel.TransferID) return false;

            if (model.Status != viewmodel.Status) return false;

            if (!string.Equals(model.Description, viewmodel.Description) ||
                !string.Equals(model.Comments, viewmodel.Comments))
                return false;

            if (model.DueDate != viewmodel.DueDate) return false;

            if (model.CompletedDate != viewmodel.CompletedDate) return false;
            if (model.CompletedByEmployee.FullName() != viewmodel.CompletedByEmployee) return false;

            return true;
        }

        private static TransferTask save(IUnitOfWork uow, TransferTask transfertask, LogEntry logentry, bool mutation)
        {
            if (transfertask != null)
            {
                if (transfertask.TransferTaskID <= 0)
                {
                    uow.TransferTaskRepository.Insert(transfertask);
                }

                if (mutation)
                {
                    var muttransfertask = new MutTransferTask();
                    muttransfertask.Merge(transfertask);

                    muttransfertask.Timestamp = logentry.Timestamp;
                    muttransfertask.EmployeeID = logentry.EmployeeID;
                    muttransfertask.ScreenName = logentry.Screenname;

                    uow.MutTransferTaskRepository.Insert(muttransfertask);
                }

                uow.Save();
            }

            return transfertask;
        }

        private static void setStatus(TransferTask transfertask, Status status, int employeeid)
        {
            if (transfertask != null)
            {
                if (status == Status.Active)
                {
                    transfertask.CompletedByEmployee = null;
                    transfertask.CompletedByEmployeeID = null;
                    transfertask.CompletedDate = null;
                }
                else if (status == Status.Done)
                {
                    transfertask.CompletedByEmployee = null;
                    transfertask.CompletedByEmployeeID = employeeid;
                    transfertask.CompletedDate = DateTime.Now;
                }

                transfertask.Status = status;
            }
        }
    }
}