﻿using Point.Database.Models;
using Point.Database.Repository;
using System;
using System.Linq;

namespace Point.Business.Logic
{
    public static class SignedPhaseInstanceBL
    {
        public static bool IsSigned(IUnitOfWork uow, int phaseinstanceid)
        {
            return uow.SignedPhaseInstanceRepository.Any(spi => spi.PhaseInstanceID == phaseinstanceid);
        }

        public static SignedPhaseInstance GetByPhaseInstanceID(IUnitOfWork uow, int phaseinstanceid)
        {
            return uow.SignedPhaseInstanceRepository.Get(spi => spi.PhaseInstanceID == phaseinstanceid)
                .FirstOrDefault();
        }

        public static void SignPhaseInstance(IUnitOfWork uow, int phaseinstanceid, int employeeid, LogEntry logentry)
        {
            if (!IsSigned(uow, phaseinstanceid))
            {
                var employee = EmployeeBL.GetByID(uow, employeeid);
                if (employee != null && employee.DigitalSignatureID.HasValue)
                {
                    var signedphaseinstance = new SignedPhaseInstance()
                    {
                        PhaseInstanceID = phaseinstanceid,
                        SignedByID = employee.EmployeeID,
                        DigitalSignatureID = employee.DigitalSignatureID.Value,
                    };

                    LoggingBL.FillLogging(uow, signedphaseinstance, logentry);

                    uow.SignedPhaseInstanceRepository.Insert(signedphaseinstance);

                    uow.Save();
                }
            }
        }
    }
}
