﻿using System.Collections.Generic;
using System.Linq;
using Point.Business.Helper;
using Point.Database.Extensions;
using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;

namespace Point.Business.Logic
{
    public class TempletViewBL
    {
        public static Templet ToModel(TempletViewModel templetviewmodel, IUnitOfWork uow)
        {
            var templet = new Templet()
            {
                TempletID = templetviewmodel.TempletID,
                TempletTypeID = (int)templetviewmodel.TempletTypeID,

                Name = templetviewmodel.Name,
                Description = templetviewmodel.Description,

                EmployeeID = templetviewmodel.EmployeeID,

                FileName = templetviewmodel.FileName,
                ContentType = templetviewmodel.ContentType,
                FileSize = templetviewmodel.FileSize,

                RegionID = templetviewmodel.RegionID,

                UploadDate = templetviewmodel.UploadDate,

                Deleted = templetviewmodel.Deleted,                
                SortOrder = templetviewmodel.SortOrder.HasValue? templetviewmodel.SortOrder : TempletBL.GetSortOrder(uow, templetviewmodel.RegionID ?? 0),
            };

            return templet;
        }

        public static TempletViewModel FromModel(IUnitOfWork uow, Templet templet)
        {
            PointUserInfo pointUserInfo = PointUserInfoHelper.GetPointUserInfo(uow);

                var templetView = new TempletViewModel()
                {
                    Name = templet.Name,
                    Description = templet.Description,

                    EmployeeID = templet.EmployeeID,
                    EmployeeName = templet.Employee.FullName(),

                    FileName = templet.FileName,
                    ContentType = templet.ContentType,
                    FileSize = templet.FileSize,

                    RegionID = templet.RegionID,

                    TempletID = templet.TempletID,
                    TempletTypeID = (TempletTypeID)templet.TempletTypeID.GetValueOrDefault(0),

                    UploadDate = templet.UploadDate,

                    Deleted = templet.Deleted,
                    SortOrder = templet.SortOrder.HasValue? templet.SortOrder : TempletBL.GetSortOrder(uow, templet.RegionID?? 0) ,
                };
                
                    
                if (templet.EmployeeID.HasValue)
                {
                    Organization organization = OrganizationBL.GetByEmployeeID(uow, templet.EmployeeID.Value);
                    if (organization != null)
                        templetView.OrganizationName = organization.Name;
                }

                return templetView;
        }

        public static IEnumerable<TempletViewModel> FromModelList(IUnitOfWork uow, IEnumerable<Templet> templets)
        {
            PointUserInfo pointUserInfo = PointUserInfoHelper.GetPointUserInfo(uow);

            List<TempletViewModel> templeViews = new List<TempletViewModel>();
            foreach (Templet templet in templets)
            {
                var templetView = new TempletViewModel() {
                    Name = templet.Name,
                    Description = templet.Description,
                    
                    EmployeeID = templet.EmployeeID,
                    EmployeeName = templet.Employee.FullName(),
                    
                    FileName = templet.FileName,
                    ContentType = templet.ContentType,
                    FileSize = templet.FileSize,
                    
                    RegionID = templet.RegionID,
                    
                    TempletID = templet.TempletID,
                    TempletTypeID = (TempletTypeID)templet.TempletTypeID.GetValueOrDefault(0),
                    
                    UploadDate = templet.UploadDate,
                    
                    Deleted = templet.Deleted,
                    SortOrder = templet.SortOrder,// templet.SortOrder.HasValue ? templet.SortOrder : TempletBL.GetSortOrder(uow, templet.RegionID ?? 0),
                };

                if (templet.EmployeeID.HasValue)
                {
                    Organization organization = OrganizationBL.GetByEmployeeID(uow, templet.EmployeeID.Value);
                    if (organization != null)
                        templetView.OrganizationName = organization.Name;
                }

                templeViews.Add(templetView);
            }

            return templeViews.OrderBy(tmp => tmp.SortOrder);
        }
    }
}
