﻿using System.Collections.Generic;
using System.Linq;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Extensions;

namespace Point.Business.Logic
{
    public class LoginHistoryViewBL
    {
        public static LoginHistoryViewModel GetLoginHistoryViewModel(IUnitOfWork uow, PointUserInfo pointUserInfo) 
        {
            if (pointUserInfo.Organization == null)
            {
                return null;
            }
            var loginName = pointUserInfo.Employee.UserName;
            var loginUserName = LoginHistoryBL.getLoginUserName(loginName, pointUserInfo);
            var orgName = pointUserInfo.Organization.Name;

            const int maxCount = 10;
            var logHistoryRows = uow.LoginHistoryRepository.Get(lh => !lh.LoginFailed && lh.LoginOrganizationName != null && lh.LoginOrganizationName == orgName && lh.LoginUsername != null && lh.LoginUsername == loginUserName)
                                    .OrderByDescending(lh => lh.LoginDateTime).Take(maxCount);

            var model = new LoginHistoryViewModel();

            IList<LoginHistoryItemViewModel> items = new List<LoginHistoryItemViewModel>();

            if (logHistoryRows.Any())
            {
                foreach (var loghistoryrow in logHistoryRows)
                {
                    items.Add(new LoginHistoryItemViewModel { LoginDateTime = loghistoryrow.LoginDateTime.ToPointDateHourDisplay(), ClientIP = loghistoryrow.ClientIP });
                }
            }

            model.Title = $"{loginUserName} - {pointUserInfo.Organization.Name}";
            model.Items = items;

            return model;
        }
    }

}
