﻿using Point.Database.Models;
using Point.Database.Repository;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace Point.Business.Logic
{
    public class ValidationBL
    {
        public static List<DbDrivenValidation> GetAllDeserialized(IUnitOfWork uow)
        {
            var validations = GetAll(uow).ToList();

            return validations.Select(validation => new DbDrivenValidation
                {
                    Method = validation.Method,
                    FieldAttributes = ListDeserializer<int>.Deserialize(validation.FieldAttributes),
                    WarningTriggers = ListDeserializer<string>.Deserialize(validation.WarningTriggers),
                    Messages = ListDeserializer<string>.Deserialize(validation.Messages)
                })
                .ToList();
        }

        private static IEnumerable<Validation> GetAll(IUnitOfWork uow)
        {
            return uow.ValidationRepository.GetAll().ToList();
        }

    }

    public class ListDeserializer<T>
    {
        public List<T> Members { get; set; }

        public ListDeserializer(List<T> members)
        {
            Members = members;
        }

        /// <summary>
        /// Input string: "{ Members : [149, 73]}"
        /// Output list : 149, 73
        /// </summary>
        public static List<T> Deserialize(string json)
        {
            return JsonConvert.DeserializeObject<ListDeserializer<T>>(json).Members;
        }
    }
}
