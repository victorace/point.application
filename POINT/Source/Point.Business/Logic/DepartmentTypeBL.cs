﻿using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Repository;
using Point.Infrastructure.Extensions;
using System.Collections.Generic;
using System.Linq;

namespace Point.Business.Logic
{
    public class DepartmentTypeBL
    {
        public static IEnumerable<DepartmentType> GetAll(IUnitOfWork uow)
        {
            return uow.DepartmentTypeRepository.GetAll();
        }

        public static IEnumerable<DepartmentType> GetByOrganizationTypeID(IUnitOfWork uow, int organizationtypeid)
        {
            var departmenttypes = GetAll(uow).ToList();
            switch ( (OrganizationTypeID)organizationtypeid)
            {
                case OrganizationTypeID.Hospital:
                    departmenttypes = departmenttypes.Where(dt => dt.DepartmentTypeID == (int)DepartmentTypeID.Afdeling || dt.DepartmentTypeID == (int)DepartmentTypeID.Transferpunt).ToList();
                    break;
                case OrganizationTypeID.HealthCareProvider:
                    departmenttypes = departmenttypes.Where(dt => dt.DepartmentTypeID == (int)DepartmentTypeID.Afdeling || dt.DepartmentTypeID == (int)DepartmentTypeID.CentraalMeldpunt).ToList();
                    break;
                default:
                    departmenttypes = departmenttypes.Where(dt => dt.DepartmentTypeID == (int)DepartmentTypeID.Afdeling).ToList();
                    break;
            }
            
            return departmenttypes;
        }
    }
}