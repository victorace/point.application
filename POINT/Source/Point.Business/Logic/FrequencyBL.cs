﻿using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using Point.Infrastructure.Extensions;

namespace Point.Business.Logic
{
    public class FrequencyBL
    {
        public static IEnumerable<Frequency> GetByStatus(IUnitOfWork uow, FrequencyStatus status)
        {
            return uow.FrequencyRepository.Get(f => f.Status == status && !f.Cancelled);
        }

        public static IEnumerable<Frequency> GetByStatusNotExpired(IUnitOfWork uow, FrequencyStatus status)
        {
            DateTime today = DateTime.Now.Date;
            return uow.FrequencyRepository.Get(f => f.Status == status && !f.Cancelled && f.EndDate >= today);
        }

        public static Frequency GetByID(IUnitOfWork uow, int frequencyid)
        {
            return uow.FrequencyRepository.GetByID(frequencyid);
        }

        public static Frequency GetByTransferID(IUnitOfWork uow, int transferID)
        {
            return uow.FrequencyRepository.Get(f => f.TransferID == transferID).FirstOrDefault();
        }
        
        public static IQueryable<Frequency> GetByFlowInstanceID(IUnitOfWork uow, int flowInstanceID)
        {
            return uow.FrequencyRepository.Get(f => f.Transfer.FlowInstances.Any(fi => fi.FlowInstanceID == flowInstanceID));
        }

        public static IEnumerable<Frequency> GetAllByType(IUnitOfWork uow, int transferid, FrequencyType frequencytype)
        {
            return uow.FrequencyRepository.Get(f => f.TransferID == transferid && f.Type == frequencytype && !f.Cancelled).ToList();
        }

        public static Frequency GetByType(IUnitOfWork uow, int transferid, FrequencyType frequencytype)
        {
            return uow.FrequencyRepository
                .Get(f => f.TransferID == transferid && f.Type == frequencytype && !f.Cancelled)
                .OrderByDescending(f => f.Timestamp)
                .FirstOrDefault();
        }

        public static Frequency Create(IUnitOfWork uow, FrequencyType type, int transferid, string name,
            DateTime startdate, DateTime enddate, int quantity, LogEntry logentry,
            FrequencyStatus status = FrequencyStatus.Active)
        {
            var frequency = new Frequency()
            {
                TransferID = transferid,
                Name = name,
                StartDate = startdate,
                EndDate = enddate,
                Quantity = quantity,
                Type = type,
                Status = status
            };

            LoggingBL.FillLogging(uow, frequency, logentry);

            uow.FrequencyRepository.Insert(frequency);

            return (frequency);
        }
        
        public static void SetStatus(IUnitOfWork uow, Frequency frequency, FrequencyStatus status, LogEntry logentry)
        {
            if (frequency != null)
            {
                frequency.Status = status;
                Upsert(uow, frequency, logentry);
            }
        }

        public static void CancelByType(IUnitOfWork uow, int transferid, FrequencyType frequencytype, LogEntry logentry)
        {
            var frequencies = GetAllByType(uow, transferid, frequencytype);
            foreach (var frequency in frequencies)
            {
                cancel(uow, frequency, logentry);
            }
        }

        public static void CancelByTransferID(IUnitOfWork uow, int transferid, LogEntry logentry)
        {
            var frequency = GetByTransferID(uow, transferid);
            cancel(uow, frequency, logentry);
        }

        public static void Insert(IUnitOfWork uow, Frequency frequency, LogEntry logentry)
        {
            frequency.Status = FrequencyStatus.Active;
            uow.FrequencyRepository.Insert(frequency);
        }
        
        public static void Upsert(IUnitOfWork uow, Frequency frequency, LogEntry logentry)
        {
            if (frequency.FrequencyID <= 0)
            {
                Insert(uow, frequency, logentry);
            }

            LoggingBL.FillLogging(uow, frequency, logentry);
            uow.Save();
        }
        
        private static void cancel(IUnitOfWork uow, Frequency frequency, LogEntry logentry)
        {
            if (frequency == null)
            {
                return;
            }

            frequency.Cancelled = true;
            Upsert(uow, frequency, logentry);
        }
    }
}
