﻿using System.Linq;
using Point.Database.Models;
using Point.Database.Repository;

namespace Point.Business.Logic
{
    public class FrequencyTransferMemoBL
    {
        public static FrequencyTransferMemo GetByTransferMemoID(IUnitOfWork uow, int transfermemoid)
        {
            return uow.FrequencyTransferMemoRepository.Get(ftm => ftm.TransferMemoID == transfermemoid).FirstOrDefault();
        }

        public static FrequencyTransferMemo Insert(IUnitOfWork uow, Frequency frequency, TransferMemo transfermemo)
        {
            var frequencyTransferMemo = new FrequencyTransferMemo
            {
                Frequency = frequency,
                TransferMemo = transfermemo
            };
            uow.FrequencyTransferMemoRepository.Insert(frequencyTransferMemo);
            return frequencyTransferMemo;
        }
    }
}
