﻿using Point.Database.Models;
using Point.Database.Repository;
using System.Collections.Generic;

namespace Point.Business.Logic
{
    public static class EndpointConfigurationBL
    {
        public static IEnumerable<EndpointConfiguration> Get(IUnitOfWork uow, int organizationid)
        {
            return uow.EndpointConfigurationRepository.Get(ec => ec.OrganizationID == organizationid && !ec.Inactive);
        }

        public static EndpointConfiguration Get(IUnitOfWork uow, int organizationid, int formtypeid)
        {
            return uow.EndpointConfigurationRepository.FirstOrDefault(ec => ec.OrganizationID == organizationid && ec.FormTypeID == formtypeid && !ec.Inactive);
        }
    }
}
