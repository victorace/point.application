﻿using Point.Business.Helper;
using Point.Business.Logic.TemplateDataSets;
using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Extensions;
using System.Linq;

namespace Point.Business.Logic
{
    public class ClientCareFormViewModelBL
    {
        public static ClientCareFormViewModel Get(IUnitOfWork uow, FlowInstance flowinstance, PointUserInfo pointuserinfo, bool forcetemplate = false)
        {
            var viewmodel = new ClientCareFormViewModel
            {
                Client = flowinstance.Transfer.Client,
                ContactPerson = ContactPersonBL.GetByClientID(uow, flowinstance.Transfer.ClientID).FirstOrDefault()
            };

            var flowfieldvalue = FlowFieldValueBL.GetByFlowInstanceIDAndFormTypeIDAndFieldIDs(uow, flowinstance.FlowInstanceID, (int)FlowFormType.PatientenBrief, new int[] { FlowFieldConstants.ID_Patientbrief }).FirstOrDefault();
            if (!forcetemplate && flowfieldvalue != null)
            {
                viewmodel.Patientbrief = flowfieldvalue.Value;
            }
            else
            {
                var templatetext = TemplateBL.GetTextOrDefault(uow, flowinstance.StartedByOrganizationID, (int)TemplateTypeID.PatientenBrief);
                if (!string.IsNullOrEmpty(templatetext))
                {
                    var patientbriefdataset = new PatientenBrief(uow, flowinstance, pointuserinfo);
                    var templateparser = new TemplateParser(templatetext, patientbriefdataset.Get());
                    viewmodel.Patientbrief = templateparser.Parse();
                }
            }

            return viewmodel;
        }
    }
}
