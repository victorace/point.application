﻿using Point.Business.Helper;
using Point.Database.Extensions;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Extensions;
using Point.Models.Enums;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Point.Business.Logic
{
    public class OrganizationBL
    {
        public static IEnumerable<Organization> GetActiveByDepartmentIDs(IUnitOfWork uow, IEnumerable<int> departmentIDs)
        {
            if (departmentIDs == null)
            {
                return Enumerable.Empty<Organization>();
            }

            return uow.OrganizationRepository.Get(o => 
                !o.Inactive && !OrganizationTypeBL.NoneselectableOrganizationTypeIDs.Contains(o.OrganizationTypeID) &&
                o.Location.Any(l => !l.Inactive && 
                    l.Department.Any(d => !d.Inactive && departmentIDs.Contains(d.DepartmentID))
                )
            );
        }

        public static IEnumerable<Organization> GetActiveByLocationIDs(IUnitOfWork uow, IEnumerable<int> locationIDs)
        {
            if (locationIDs == null)
            {
                return Enumerable.Empty<Organization>();
            }

            return uow.OrganizationRepository.Get(o =>
                !o.Inactive && !OrganizationTypeBL.NoneselectableOrganizationTypeIDs.Contains(o.OrganizationTypeID) &&
                o.Location.Any(l => !l.Inactive && locationIDs.Contains(l.LocationID))
            );
        }

        public static string GetHashPrefix(IUnitOfWork uow, int organizationid)
        {
            return uow.OrganizationRepository.FirstOrDefault(o => o.OrganizationID == organizationid)?.HashPrefix;
        }

        public static Organization GetByLocationID(IUnitOfWork uow, int locationid)
        {
            return GetByLocationIDs(uow, new List<int>() { locationid }).FirstOrDefault();
        }

        public static IEnumerable<Organization> GetByLocationIDs(IUnitOfWork uow, IEnumerable<int> locationids)
        {
            return uow.OrganizationRepository.Get(o =>
                !OrganizationTypeBL.NoneselectableOrganizationTypeIDs.Contains(o.OrganizationTypeID) &&
                o.Location.Any(l => locationids.Contains(l.LocationID))).OrderBy(o => o.Name);
        }

        public static Organization GetByOrganizationID(IUnitOfWork uow, int organizationID)
        {
            return uow.OrganizationRepository.GetByID(organizationID);
        }

        public static IEnumerable<Organization> GetByOrganizationIDs(IUnitOfWork uow, IEnumerable<int> organizationIDs, OrganizationTypeID? organizationTypeID = null)
        {
            var orgs = uow.OrganizationRepository.Get(org => organizationIDs.Contains(org.OrganizationID));

            if (organizationTypeID != null)
            {
                orgs = orgs.Where(o => o.OrganizationTypeID == (int) organizationTypeID);
            }

            return orgs.OrderBy(o => o.Name);
        }

        public static Organization GetByEmployeeID(IUnitOfWork uow, int employeeID)
        {
            Employee employee = uow.EmployeeRepository.GetByID(employeeID);
            var organizationID = employee.DefaultDepartment.Location.OrganizationID;
            return uow.OrganizationRepository.Get(o => o.OrganizationID == organizationID).FirstOrDefault();
        }

        public static IEnumerable<Organization> GetActiveByOrganizationTypeID(IUnitOfWork uow, int organizationTypeID)
        {
            return uow.OrganizationRepository.Get(o => o.OrganizationTypeID == organizationTypeID && !(bool)o.Inactive).OrderBy(o => o.Name);
        }

        public static IEnumerable<Organization> GetActiveByOrganizationTypeIDs(IUnitOfWork uow, IEnumerable<int> organizationTypeIDs)
        {
            return uow.OrganizationRepository.Get(o => organizationTypeIDs.Contains(o.OrganizationTypeID) && !(bool)o.Inactive).OrderBy(o => o.Name);
        }

        public static bool InSameOrganization(IUnitOfWork uow, int employeeID, PointUserInfo pointUserInfo)
        {
            PointUserInfo employee = PointUserInfoHelper.GetPointUserInfoByEmployeeID(uow, employeeID);
            return employee.Organization.OrganizationID == pointUserInfo.Organization.OrganizationID;
        }

        public static IEnumerable<Organization> GetActive(IUnitOfWork uow)
        {
            return uow.OrganizationRepository.Get(o => !OrganizationTypeBL.NoneselectableOrganizationTypeIDs.Contains(o.OrganizationTypeID) &&
                o.Inactive == false).OrderBy(o => o.Name);
        }

        public static IEnumerable<Organization> GetActiveByRegionID(IUnitOfWork uow, int regionid)
        {
            return GetActiveByRegionIDs(uow, new List<int>() { regionid });
        }

        public static IEnumerable<Organization> GetOrganizationsWithCapacityEnabledByRegionID(IUnitOfWork uow, int regionid)
        {
            return uow.OrganizationRepository.Get(o => !OrganizationTypeBL.NoneselectableOrganizationTypeIDs.Contains(o.OrganizationTypeID) &&
                (o.RegionID == regionid || o.Location.Any(l => l.LocationRegion.Any(lr => lr.RegionID == regionid))) && o.Inactive == false &&
                o.Location.Any(l => l.Department.Any(d => d.CapacityFunctionality))).OrderBy(o => o.Name);
        }

        public static IEnumerable<Organization> GetActiveByRegionIDs(IUnitOfWork uow, IEnumerable<int> regionids)
        {
            return uow.OrganizationRepository.Get(o => !OrganizationTypeBL.NoneselectableOrganizationTypeIDs.Contains(o.OrganizationTypeID) &&
                o.RegionID.HasValue && regionids.Contains(o.RegionID.Value) && o.Inactive == false).OrderBy(o => o.Name);
        }

        public static IEnumerable<Organization> GetActiveByRegionIDsAndOrganizationTypeID(IUnitOfWork uow, IEnumerable<int> regionids, int organizationtypeid)
        {
            return uow.OrganizationRepository.Get(o => !OrganizationTypeBL.NoneselectableOrganizationTypeIDs.Contains(o.OrganizationTypeID) && 
                o.RegionID.HasValue && regionids.Contains(o.RegionID.Value) && o.OrganizationTypeID == organizationtypeid && o.Inactive == false).OrderBy(o => o.Name);
        }

        public static IEnumerable<Organization> GetActiveByRegionIDsAndOrganizationTypeIDs(IUnitOfWork uow, IEnumerable<int> regionids, IEnumerable<int> organizationtypeids)
        {
            return uow.OrganizationRepository.Get(o => !OrganizationTypeBL.NoneselectableOrganizationTypeIDs.Contains(o.OrganizationTypeID) &&
                                                       o.RegionID.HasValue && regionids.Contains(o.RegionID.Value) && organizationtypeids.Contains(o.OrganizationTypeID) && o.Inactive == false).OrderBy(o => o.Name);
        }

        public static IEnumerable<Organization> GetActiveByRegionIDAndOrganizationTypeID(IUnitOfWork uow, int regionid, int organizationtypeid)
        {
            return GetActiveByRegionIDsAndOrganizationTypeID(uow, new List<int>() { regionid }, organizationtypeid);
        }
        public static IEnumerable<Organization> GetActiveByRegionIDAndOrganizationTypeIDs(IUnitOfWork uow, int regionid, IEnumerable<int> organizationtypeids)
        {
            return GetActiveByRegionIDsAndOrganizationTypeIDs(uow, new List<int>() { regionid }, organizationtypeids);
        }

        public static Organization GetByAGB(IUnitOfWork uow, string agb)
        {
            return uow.OrganizationRepository.Get(o => o.AGB.ToLower() == agb.ToLower() && o.Inactive == false).FirstOrDefault();
        }

        public static IEnumerable<MutOrganization> GetHistoryByOrganizationID(IUnitOfWork uow, int organizationid)
        {
            return uow.MutOrganizationRepository.Get(it => it.OrganizationID == organizationid);
        }

        public static Organization Save(IUnitOfWork uow, OrganizationViewModel viewmodel, LogEntry logentry)
        {
            var pointuserinfo = PointUserInfoHelper.GetPointUserInfo(uow);
            var model = GetByOrganizationID(uow, viewmodel.OrganizationID) ?? new Organization();

            //var needCapManUpdate = model.OrganizationID > 0 && model.Inactive != viewmodel.Inactive;

            model.WriteSecureValue(m => m.AGB, viewmodel, v => v.AGB, pointuserinfo);
            model.WriteSecureValue(m => m.Name, viewmodel, v => v.Name, pointuserinfo);
            model.WriteSecureValue(m => m.OrganizationTypeID, viewmodel, v => v.OrganizationTypeID, pointuserinfo);
            model.WriteSecureValue(m => m.RegionID, viewmodel, v => v.RegionID, pointuserinfo);
            model.WriteSecureValue(m => m.Inactive, viewmodel, v => v.Inactive, pointuserinfo);
            model.WriteSecureValue(m => m.ChangePasswordPeriod, viewmodel, v => v.ChangePasswordPeriod, pointuserinfo);
            model.WriteSecureValue(m => m.ShowUnloadMessage, viewmodel, v => v.ShowUnloadMessage, pointuserinfo);
            model.WriteSecureValue(m => m.PageLockTimeout, viewmodel, v => v.PageLockTimeout, pointuserinfo);
            model.WriteSecureValue(m => m.UseAnonymousClient, viewmodel, v => v.UseAnonymousClient, pointuserinfo);
            model.WriteSecureValue(m => m.UseDigitalSignature, viewmodel, v => v.UseDigitalSignature, pointuserinfo);

            if (viewmodel.SaveAdminSettings)
            {
                if (model.MFAAuthType == MFAAuthType.SMS && viewmodel.MFAAuthType != model.MFAAuthType)
                {
                    var employees = EmployeeBL.GetByOrganizationID(uow, model.OrganizationID);
                    if (employees.Any())
                    {
                        foreach (var employee in employees.Where(emp => !string.IsNullOrEmpty(emp.MFANumber) || emp.MFAConfirmedDate != null))
                        {
                            employee.MFANumber = "";
                            employee.MFAConfirmedDate = null;                            
                        }
                    }
                }

                model.WriteSecureValue(m => m.UsesPalliativeCareInVO, viewmodel, v => v.UsesPalliativeCareInVO, pointuserinfo);
                model.WriteSecureValue(m => m.MessageToGPByClose, viewmodel, v => v.MessageToGPByClose, pointuserinfo);
                model.WriteSecureValue(m => m.MessageToGPByDefinitive, viewmodel, v => v.MessageToGPByDefinitive, pointuserinfo);

                model.WriteSecureValue(m => m.MFARequired, viewmodel, v => v.MFARequired, pointuserinfo);
                model.WriteSecureValue(m => m.MFAAuthType, viewmodel, v => v.MFAAuthType, pointuserinfo);
                model.WriteSecureValue(m => m.MFATimeout, viewmodel, v => v.MFATimeout, pointuserinfo);
                model.WriteSecureValue(m => m.IPAddresses, viewmodel, v => v.IPAddresses, pointuserinfo);
                model.WriteSecureValue(m => m.UseTasks, viewmodel, v => v.UseTasks, pointuserinfo);
                model.WriteSecureValue(m => m.NedapCryptoCertificateID, viewmodel, v => v.NedapCryptoCertificateID, pointuserinfo);

                model.WriteSecureValue(m => m.NedapMedewerkernummer, viewmodel, v => v.NedapMedewerkerNummer, pointuserinfo);
                model.WriteSecureValue(m => m.ZorgmailUsername, viewmodel, v => v.ZorgmailUsername, pointuserinfo);
                model.WriteSecureValue(m => m.ZorgmailPassword, viewmodel, v => v.ZorgmailPassword, pointuserinfo);
                model.WriteSecureValue(m => m.HL7ConnectionDifferentMenu, viewmodel, v => v.HL7ConnectionDifferentMenu, pointuserinfo);
                model.WriteSecureValue(m => m.HL7ConnectionCallFromPoint, viewmodel, v => v.HL7ConnectionCallFromPoint, pointuserinfo);
                if (viewmodel.CanWriteValue(vm => vm.PatientOverview, pointuserinfo))
                {
                    OrganizationSettingBL.SetValue(uow, model.OrganizationID, OrganizationSettingBL.PatientOverview, viewmodel.PatientOverview, logentry);
                }

                if (viewmodel.CanWriteValue(vm => vm.UsesAuth, pointuserinfo))
                {
                    OrganizationSettingBL.SetValue(uow, model.OrganizationID, OrganizationSettingBL.UsesAuth, viewmodel.UsesAuth, logentry);
                }

                model.WriteSecureValue(m => m.DeBlockEmailsToOrganization, viewmodel, v => v.DeBlockEmailsToOrganization, pointuserinfo);
                model.WriteSecureValue(m => m.DeBlockEmails, viewmodel, v => v.DeBlockEmails, pointuserinfo);
            
                if (viewmodel.CanWriteValue(vm => vm.AutoCloseTransferAfterDays, pointuserinfo))
                {
                    OrganizationSettingBL.SetValue(uow, model.OrganizationID, OrganizationSettingBL.AutoCloseTransferAfterDays, viewmodel.AutoCloseTransferAfterDays, logentry);
                }
            }

            if (viewmodel.SaveCouplingSettings)
            {
                model.WriteSecureValue(m => m.SSO, viewmodel, v => v.UsesSSO, pointuserinfo);

                if (viewmodel.UsesSSO == false)
                {
                    viewmodel.SSOUsesAutoCeateAccount = false;
                    viewmodel.SSOKey = null;
                    viewmodel.SSOHashType = null;
                }

                model.WriteSecureValue(m => m.CreateUserViaSSO, viewmodel, v => v.SSOUsesAutoCeateAccount, pointuserinfo);
                if(viewmodel.CanWriteValue(m => m.SSOKey, pointuserinfo)) //Niet proberen te overschrijven met null door lagere rol
                {
                    model.WriteSecureValue(m => m.SSOKey, viewmodel, v => v.SSOKey, pointuserinfo);
                }
                if (viewmodel.CanWriteValue(m => m.SSOHashType, pointuserinfo)) //Niet proberen te overschrijven met null door lagere rol
                {
                    model.WriteSecureValue(m => m.SSOHashType, viewmodel, v => v.SSOHashTypeName, pointuserinfo);
                }
            }

            FormTypeOrganizationBL.Save(uow, viewmodel, logentry);

            if (viewmodel.OrganizationProjects != null)
            {
                // deleted
                var neworganizationprojectids = viewmodel.OrganizationProjects.Select(it => it.OrganizationProjectID).ToArray();
                uow.OrganizationProjectRepository.Delete(op => op.OrganizationID == model.OrganizationID && !neworganizationprojectids.Contains(op.OrganizationProjectID));

                // modify existing projects
                var existingorganizationprojectids = model.OrganizationProjects.Select(it => it.OrganizationProjectID).ToArray();
                var existinginviewmodel = viewmodel.OrganizationProjects.Where(op => existingorganizationprojectids.Contains(op.OrganizationProjectID)).ToList();
                existinginviewmodel.ForEach(existingvm =>
                {
                    var project = model.OrganizationProjects.FirstOrDefault(mp => mp.OrganizationProjectID == existingvm.OrganizationProjectID);
                    if (project != null)
                    {
                        project.Name = existingvm.Name;
                        project.ModifiedTimeStamp = logentry.Timestamp;
                    }
                });

                // add new projects
                model.OrganizationProjects.AddRange(viewmodel.OrganizationProjects
                .Where(vp => vp.OrganizationProjectID == -1)
                .Select(p => new OrganizationProject { Name = p.Name, ModifiedTimeStamp = logentry.Timestamp }));
            }

            if (model.OrganizationID <= 0)
            {
                uow.OrganizationRepository.Insert(model);
            }

            LoggingBL.FillLoggable(uow, model, logentry, forceupdate: true);

            Task.Run(async () => await DepartmentCapacityPublicBL.UpdateOnOrganizationChange(model));

            uow.Save(logentry);

            return model;
        }

        public static IEnumerable<OrganizationLocationDepartmentViewModel> Search(
            IUnitOfWork uow, string search,
            int? regionid, int[] organizationids, int[] locationids, int[] departmentids, bool searchdeleted)
        {
            organizationids = organizationids ?? new int[0];
            locationids = locationids ?? new int[0];
            departmentids = departmentids ?? new int[0];

            var searchlist = new List<string>();
            if (!string.IsNullOrEmpty(search))
            {
                searchlist = search.ToLower().Split(' ').Where(it => !string.IsNullOrEmpty(it)).ToList();
            }

            var regions = uow.RegionRepository.GetAll();
            var organizations = uow.OrganizationRepository.Get(o => searchdeleted || !(bool) o.Inactive && !OrganizationTypeBL.NoneselectableOrganizationTypeIDs.Contains(o.OrganizationTypeID));
            var locations = uow.LocationRepository.Get(l => searchdeleted || !(bool) l.Inactive);
            var departments = uow.DepartmentRepository.Get(d => searchdeleted || !(bool) d.Inactive);
            var orgnizationtypes = uow.OrganizationTypeRepository.Get(o => !OrganizationTypeBL.NoneselectableOrganizationTypeIDs.Contains(o.OrganizationTypeID));

            var orglocafd = from organization in organizations
                join organizationtype in orgnizationtypes on organization.OrganizationTypeID equals organizationtype
                    .OrganizationTypeID
                join region in regions on organization.RegionID equals region.RegionID
                join location in locations on organization.OrganizationID equals location.OrganizationID into
                    groupLocation
                from location in groupLocation.DefaultIfEmpty()
                join department in departments on location.LocationID equals department.LocationID into groupDepartment
                from department in groupDepartment.DefaultIfEmpty()
                where
                    (!regionid.HasValue || region.RegionID == regionid) &&
                    (!organizationids.Any() || organizationids.Contains(organization.OrganizationID)) &&
                    (!locationids.Any() || locationids.Contains(location.LocationID)) &&
                    (!departmentids.Any() || departmentids.Contains(department.DepartmentID)) &&
                    (!searchlist.Any() || searchlist.All(sl =>
                         organization.Name.Contains(sl) ||
                         location.Name.Contains(sl) || department.Name.Contains(sl)))
                select new OrganizationLocationDepartmentViewModel
                {
                    RegionID = region.RegionID,
                    RegionName = region.Name,
                    OrganizationID = organization.OrganizationID,
                    OrganizationName = organization.Name,
                    OrganizationInactive = organization.Inactive == true,
                    OrganizationTypeID = organizationtype.OrganizationTypeID,
                    OrganizationType = organizationtype.Name,
                    LocationID = location.LocationID,
                    LocationName = location.Name,
                    LocationInactive = location.Inactive == true,
                    DepartmentID = department.DepartmentID,
                    DepartmentName = department.Name,
                    DepartmentInactive = department.Inactive == true,
                    FlowDefinitions = location.FlowDefinitionParticipation
                        .Select(it => new FlowDefinitionViewModel
                        {
                            Name = it.FlowDefinition.Name,
                            FlowDefinitionID = it.FlowDefinitionID,
                            FlowDirection = it.FlowDirection,
                            Participation = it.Participation
                        }).OrderBy(it => it.FlowDefinitionID).ToList()
                };

            return orglocafd
                .OrderBy(o => o.OrganizationName)
                .ThenBy(l => l.LocationName)
                .ThenBy(d => d.DepartmentName)
                .ToList();
        }

        public static IEnumerable<OrganizationViewModel> SearchByPhraseOrID(IUnitOfWork uow, string searchPhraseOrID, bool searchDeleted)
        {
            List<OrganizationViewModel> organizations = null;
            if (string.IsNullOrEmpty(searchPhraseOrID))
            {
                return organizations;
            }

            searchPhraseOrID = searchPhraseOrID.ToLowerInvariant();

            if (int.TryParse(searchPhraseOrID, out var organizationID))
            {
                organizations = uow.OrganizationRepository
                    .Get(o => (searchDeleted || !o.Inactive && !OrganizationTypeBL.NoneselectableOrganizationTypeIDs.Contains(o.OrganizationTypeID)) && o.OrganizationID == organizationID)
                    .Select(o => new OrganizationViewModel { OrganizationID = o.OrganizationID, Name = o.Name }).OrderBy(x => x.OrganizationID).ToList();
            }
            else
            {
                organizations = uow.OrganizationRepository
                    .Get(o => (searchDeleted || !o.Inactive && !OrganizationTypeBL.NoneselectableOrganizationTypeIDs.Contains(o.OrganizationTypeID)) && o.Name.Contains(searchPhraseOrID))
                    .Select(o => new OrganizationViewModel { OrganizationID = o.OrganizationID, Name = o.Name }).OrderBy(x => x.OrganizationID).ToList();
            }

            return organizations;
        }
    }
}