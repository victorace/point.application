﻿using Point.Database.Extensions;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Point.Business.Logic
{
    public class FormHistoryBL
    {
        public static IEnumerable<FormHistoryForm> GetForms(IUnitOfWork uow, int transferID)
        {
            var formSetVersions = FormSetVersionBL.GetByTransferID(uow, transferID).Where(fsv => fsv.FormType.IsVisibleInHistory);

            var formHistoryForms = formSetVersions.Select(fsv => new FormHistoryForm()
            {
                FormTypeID = fsv.FormType.FormTypeID,
                FormSetVersionID = fsv.FormSetVersionID,
                Name = (fsv.PhaseInstance.PhaseDefinition.Subcategory != null ? fsv.PhaseInstance.PhaseDefinition.Subcategory + " - " : "") + fsv.FormType.Name,
                Description = fsv.Description,
                CreationDate = fsv.CreateDate,
                CreatedBy = fsv.CreatedBy,
                ModificationDate = fsv.UpdateDate ?? fsv.CreateDate,
                ModifiedBy = fsv.UpdatedBy ?? fsv.CreatedBy
            }).ToList();

            return formHistoryForms.OrderByDescending(fhf => fhf.ModificationDate).ThenBy(fhf => fhf.Name);
        }

        public static IEnumerable<FormHistoryField> GetFields(IUnitOfWork uow, int formsetversionid)
        {
            var formhistoryfields = new List<FormHistoryField>();
            var flowfields = uow.FlowFieldRepository.Get(ff => ff.FlowFieldValue.Any(ffv => ffv.FormSetVersionID == formsetversionid));
            var duplicatelabels = flowfields.GetDuplicates(ff => ff.Label);

            foreach (var flowfield in flowfields)
            {
                formhistoryfields.Add(new FormHistoryField()
                {
                    FlowFieldID = flowfield.FlowFieldID,
                    Name = flowfield.Name,
                    Label = generateUniqueLabel(flowfield, duplicatelabels)
                });
            }

            return formhistoryfields.OrderBy(fhf => fhf.Label);
        }
        
        public static IEnumerable<MutFlowFieldValue> GetMutFlowFieldValues(IUnitOfWork uow, int formsetversionid, IEnumerable<int> flowfieldids)
        {
            var formHistoryFieldValues = uow.MutFlowFieldValueRepository.Get(ffv =>
                                                ffv.FormSetVersionID == formsetversionid &&
                                                flowfieldids.Contains(ffv.FlowFieldID))
                                            .OrderByDescending(fhfv => fhfv.MutFlowFieldValueID)
                                            .OrderBySequence(flowfieldids, ffv => ffv.FlowFieldID);
            return formHistoryFieldValues;
        }

        public static IEnumerable<FormHistoryFieldValue> GetValues(IUnitOfWork uow, int formsetversionid, IEnumerable<int> flowfieldids)
        {
            var mutflowfieldvalues = GetMutFlowFieldValues(uow, formsetversionid, flowfieldids);
            var flowfields = uow.FlowFieldRepository.Get(ff => flowfieldids.Contains(ff.FlowFieldID) &&
                ff.FlowFieldValue.Any(ffv => ffv.FormSetVersionID == formsetversionid));
            var duplicatelabels = flowfields.GetDuplicates(ff => ff.Label);

            var formhistoryfieldvalues = new List<FormHistoryFieldValue>();
            foreach (var mutflowfieldvalue in mutflowfieldvalues)
            {
                formhistoryfieldvalues.Add(new FormHistoryFieldValue()
                {
                    Timestamp = mutflowfieldvalue.Timestamp ?? default(DateTime),
                    EmployeeFullName = mutflowfieldvalue.Employee.FullName(),
                    FlowFieldID = mutflowfieldvalue.FlowFieldID,
                    FlowField = mutflowfieldvalue.FlowField,
                    Value = mutflowfieldvalue.Value,
                    Name = mutflowfieldvalue.FlowField.Name,
                    Label = generateUniqueLabel(mutflowfieldvalue.FlowField, duplicatelabels),
                    Source = mutflowfieldvalue.Source
                });
            }

            // list is already orderd using GetMutFlowFieldValues
            return formhistoryfieldvalues;
        }

        private static string generateUniqueLabel(FlowField flowfield, IEnumerable<string> duplicatelabels)
        {
            if (flowfield == null)
            {
                throw new ArgumentNullException(nameof(flowfield));

            }

            duplicatelabels = duplicatelabels ?? new List<String>();

            var flowfieldid = flowfield.FlowFieldID;
            var name = flowfield.Name;
            var label = flowfield.Label;

            if (string.IsNullOrEmpty(label))
            {
                label = $"{name} ({flowfieldid})";
            }
            else if (duplicatelabels.Contains(label))
            {
                label = $"{label} - {name} ({flowfieldid})";
            }

            return label;
        }
    }
}
