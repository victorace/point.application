﻿using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Exceptions;
using Point.Infrastructure.Extensions;
using System;
using System.Linq;
using System.Collections.Generic;

namespace Point.Business.Logic
{
    public class OrganizationViewBL
    {
        public static void ValidateViewModel(IUnitOfWork uow, PointUserInfo userinfo, OrganizationViewModel viewmodel)
        {
            var myfunctionrolelevelid = userinfo.GetFunctionRoleLevelID(FunctionRoleTypeID.ManageOrganization);
            var currentmodel = OrganizationBL.GetByOrganizationID(uow, viewmodel.OrganizationID);
            if (myfunctionrolelevelid <= FunctionRoleLevelID.Region)
            {
                if(viewmodel.OrganizationID == 0)
                {
                    if(userinfo.Region.RegionID != viewmodel.RegionID)
                    {
                        throw new PointSecurityException("U heeft onvoldoende machtigingen om deze actie uit te voeren", ExceptionType.AccessDenied);
                    }
                }

                if (currentmodel != null && currentmodel.RegionID != viewmodel.RegionID)
                {
                    throw new PointSecurityException("U heeft onvoldoende machtigingen om deze actie uit te voeren", ExceptionType.AccessDenied);
                }
            }
        }    
        
        public static List<FormTypePerOrganization> CreateFormTypeSet(Organization model)
        {          
            var resultList = new List<FormTypePerOrganization>();

            var formtyperegions = model.Region.FormTypeRegion.Where(ftr => ftr.FormTypeID > 100).ToList();
            var organizationFormTypes = model.FormTypeOrganization.Where(fto => fto.FormTypeID > 100).ToList();

            foreach (var formtyperegion in formtyperegions)
            {
                var formTypeOrganization = organizationFormTypes.FirstOrDefault(fto => 
                    fto.FormTypeID == formtyperegion.FormTypeID && 
                    fto.FlowDefinitionID == formtyperegion.FlowDefinitionID);

                var res = new FormTypePerOrganization
                {
                    RegionID = formtyperegion.RegionID,
                    FlowDefinitionID = formtyperegion.FlowDefinitionID,
                    FormTypeID = formtyperegion.FormTypeID,
                    IsSelected = organizationFormTypes.Any() && formTypeOrganization == null ? false : true,
                    Name = formtyperegion.FormType.Name
                };
                resultList.Add(res);
            }
            return resultList;
        }

        private static string GetOrganizationInterfaceCode(ICollection<OrganizationSetting> organizationSetting)
        {
            var result = "Geen HL7 koppeling.";
            var os = organizationSetting?.FirstOrDefault(s => s.Name == OrganizationSettingBL.InterfaceCode);
            if (!string.IsNullOrEmpty(os?.Value))
            {
                result = string.Format("HL7 koppeling map: {0}", os.Value);
            }            
            return result;
        }

        public static OrganizationViewModel FromModel(IUnitOfWork uow,Organization model)
        {
            if (model == null)
            {
                return new OrganizationViewModel { ShowUnloadMessage = true };
            }

            return new OrganizationViewModel
            {
                OrganizationID = model.OrganizationID,
                Name = model.Name,
                OrganizationTypeID = model.OrganizationTypeID,
                EnableServiceAreas = model.OrganizationTypeID != (int)OrganizationTypeID.Hospital,
                RegionID = model.RegionID ?? 0,
                ChangePasswordPeriod = model.ChangePasswordPeriod,
                PageLockTimeout = model.PageLockTimeout,
                HL7Description = GetOrganizationInterfaceCode(model.OrganizationSetting),
                WSInterface = model.WSInterface,
                ShowUnloadMessage = model.ShowUnloadMessage,
                MessageToGPByDefinitive = model.MessageToGPByDefinitive,
                MessageToGPByClose = model.MessageToGPByClose,
                UsesPalliativeCareInVO = model.UsesPalliativeCareInVO,
                AGB = model.AGB,
                UseAnonymousClient = model.UseAnonymousClient,
                Inactive = model.Inactive,

                SSOHashType = model.SSOHashType == null ? HashAlgorythm.SHA1 : (HashAlgorythm)Enum.Parse(typeof(HashAlgorythm), model.SSOHashType, true),
                SSOKey = model.SSOKey,
                UsesSSO = model.SSO == true,
                SSOUsesAutoCeateAccount = model.CreateUserViaSSO == true,

                MFARequired = model.MFARequired,
                MFAAuthType = model.MFAAuthType,
                MFATimeout = model.MFATimeout,
                IPAddresses = model.IPAddresses,
                DeBlockEmails = model.DeBlockEmails,
                DeBlockEmailsToOrganization = model.DeBlockEmailsToOrganization,

                NedapCryptoCertificateID = model.NedapCryptoCertificateID,
                NedapMedewerkerNummer = model.NedapMedewerkernummer,
                ZorgmailPassword = model.ZorgmailPassword,
                ZorgmailUsername = model.ZorgmailUsername,
                HL7ConnectionDifferentMenu = model.HL7ConnectionDifferentMenu,
                HL7ConnectionCallFromPoint = model.HL7ConnectionCallFromPoint,
                PatientOverview = OrganizationSettingBL.GetValue<bool?>(uow, model.OrganizationID, OrganizationSettingBL.PatientOverview),
                UsesAuth = OrganizationSettingBL.GetValue<bool>(uow, model.OrganizationID, OrganizationSettingBL.UsesAuth),

                OrganizationProjects = model.OrganizationProjects.Where(p => !p.InActive).ToList(),

                UseDigitalSignature = model.UseDigitalSignature,

                UseTasks = model.UseTasks,

                EnableFormTypeOrganizationTab = model.RegionID != null,
                FormTypesOfOrganization = CreateFormTypeSet(model),

                AutoCloseTransferAfterDays = OrganizationSettingBL.GetValue<int?>(uow, model.OrganizationID, OrganizationSettingBL.AutoCloseTransferAfterDays),
                Image = model.Image,
                OrganizationLogoID = model.OrganizationLogoID,
            };
        }
    }
}