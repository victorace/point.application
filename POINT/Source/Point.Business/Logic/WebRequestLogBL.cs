﻿using System;
using System.IO;
using System.Xml.Serialization;
using Newtonsoft.Json;
using Point.Database.Repository;
using Point.Infrastructure.Helpers;

namespace Point.Business.Logic
{
    public class WebRequestLogBL : IWebRequestLogBL
    {
        private IUnitOfWork _uow;

        public WebRequestLogBL(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public void LogWebRequest(object sentMessage, object receivedMessage, string exceptionMessage = null)
        {
            bool logWebRequests = ConfigHelper.GetAppSettingByName<bool>("LogWebRequests", true);

            if (logWebRequests)
            {
                _uow.WebRequestLogRepository.Insert(new Database.Models.WebRequestLog()
                {
                    Received = SerializeObject(receivedMessage),
                    Sent = SerializeObject(sentMessage),
                    ExceptionMessage = exceptionMessage,
                    TimeStamp = DateTime.Now
                });

                _uow.Save();
            }
        }   
        
        public void LogWebRequestJson(object sentMessage, object receivedMessage, string exceptionMessage = null)
        {
            bool logWebRequests = ConfigHelper.GetAppSettingByName<bool>("LogWebRequests", true);

            if (logWebRequests)
            {
                _uow.WebRequestLogRepository.Insert(new Database.Models.WebRequestLog()
                {
                    Received = JsonConvert.SerializeObject(receivedMessage),
                    Sent = JsonConvert.SerializeObject(sentMessage),
                    ExceptionMessage = exceptionMessage,
                    TimeStamp = DateTime.Now
                });

                _uow.Save();
            }
        }

        private static string SerializeObject(object toSerialize)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(toSerialize.GetType());

            using (StringWriter textWriter = new StringWriter())
            {
                xmlSerializer.Serialize(textWriter, toSerialize);
                return textWriter.ToString();
            }
        }
    }
}
