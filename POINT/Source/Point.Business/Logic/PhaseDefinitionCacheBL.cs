﻿using Point.Database.CacheModels;
using Point.Models.Enums;
using Point.Database.Repository;
using Point.Infrastructure.Caching;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Point.Business.Logic
{
    public class PhaseDefinitionCacheBL
    {
        public static List<PhaseDefinitionCache> GetPhaseDefinitions(IUnitOfWork uow)
        {
            string cachestring = "GetPhaseDefinitions_Global";
            List<PhaseDefinitionCache> phasedefinitions = null;

            phasedefinitions = CacheService.Instance.Get<List<PhaseDefinitionCache>>(cachestring);

            if (phasedefinitions == null)
            {
                phasedefinitions = uow.PhaseDefinitionRepository.GetAll().Select(pd => new PhaseDefinitionCache()
                {
                    PhaseDefinitionID = pd.PhaseDefinitionID,
                    CodeGroup = pd.CodeGroup,
                    AccessGroup = pd.AccessGroup,
                    PhaseName = pd.PhaseName,
                    Description = pd.Description,
                    Phase = pd.Phase,
                    FormTypeID = pd.FormTypeID,
                    FormTypeName = pd.FormType.Name,
                    FormTypeURL = pd.FormType.URL,
                    BeginFlow = pd.BeginFlow,
                    EndFlow = pd.EndFlow,
                    FlowDefinitionID = pd.FlowDefinitionID,
                    OrderNumber = pd.OrderNumber,
                    Maincategory = pd.Maincategory,
                    Subcategory = pd.Subcategory,
                    Executor = pd.Executor,
                    ShowReadOnlyAndEmpty = pd.ShowReadOnlyAndEmpty,
                    MenuItemType = pd.MenuItemType,
                    DashboardMenuItemType = pd.DashboardMenuItemType,
                    FlowHandling = pd.FlowHandling
                }).ToList();

                if (phasedefinitions.Any())
                {
                    CacheService.Instance.Insert(cachestring, phasedefinitions, "CacheLong");
                }
            }
            return phasedefinitions;
        }

        public static IEnumerable<PhaseDefinitionCache> GetByFlowDefinitionID(IUnitOfWork uow, FlowDefinitionID flowdefinitionID)
        {
            return GetPhaseDefinitions(uow).Where(pd => pd.FlowDefinitionID == flowdefinitionID).OrderBy(pd => pd.OrderNumber);
        }

        public static IEnumerable<PhaseDefinitionCache> GetByFlowDefinitionIDs(IUnitOfWork uow, FlowDefinitionID[] flowDefinitionIDs)
        {
            return GetPhaseDefinitions(uow).Where(pd => flowDefinitionIDs.Contains(pd.FlowDefinitionID)).OrderBy(pd => pd.FlowDefinitionID).ThenBy(pd => pd.Phase);
        }

        public static IEnumerable<PhaseDefinitionCache> GetByFlowDefinitionIDAndCodeGroup(IUnitOfWork uow, FlowDefinitionID flowdefinitionid, string codeGroup)
        {
            return GetPhaseDefinitions(uow).Where(pd => pd.FlowDefinitionID == flowdefinitionid && pd.CodeGroup == codeGroup).ToList();
        }

        public static IEnumerable<PhaseDefinitionCache> GetEndPhaseDefinitions(IUnitOfWork uow)
        {
            return GetPhaseDefinitions(uow).Where(pd => pd.EndFlow).AsEnumerable();
        }
        
        public static PhaseDefinitionCache GetPhaseDefinitionBeginFlow(IUnitOfWork uow, FlowDefinitionID flowDefinitionID)
        {
            return GetPhaseDefinitions(uow).FirstOrDefault(pd => pd.FlowDefinitionID == flowDefinitionID && pd.BeginFlow);
        }

        public static PhaseDefinitionCache GetByID(IUnitOfWork uow, int phasedefinitionID)
        {
            return GetPhaseDefinitions(uow).FirstOrDefault(pd => pd.PhaseDefinitionID == phasedefinitionID);
        }

        public static PhaseDefinitionCache GetEndPhaseDefinition(IUnitOfWork uow, FlowDefinitionID flowDefinitionID)
        {
            return GetPhaseDefinitions(uow).FirstOrDefault(pd => pd.FlowDefinitionID == flowDefinitionID && pd.EndFlow);
        }

        public static IEnumerable<PhaseDefinitionCache> GetPhaseDefinitionsBySubcategory(IUnitOfWork uow, string subcategory, FlowDefinitionID flowDefinitionID)
        {
            if (String.IsNullOrWhiteSpace(subcategory))
                return Enumerable.Empty<PhaseDefinitionCache>();

            return GetPhaseDefinitions(uow).Where(pd => pd.FlowDefinitionID == flowDefinitionID && pd.Subcategory == subcategory);
        }

        public static PhaseDefinitionCache GetByFlowDefinitionAndFormType(IUnitOfWork uow, FlowDefinitionID flowDefinitionID, int formTypeID)
        {
            return GetPhaseDefinitions(uow).FirstOrDefault(pd => pd.FlowDefinitionID == flowDefinitionID && pd.FormTypeID == formTypeID);
        }

        public static PhaseDefinitionCache GetBeginFlowPhaseDefinition(IUnitOfWork uow, FlowDefinitionID flowDefinitionID)
        {
            return GetPhaseDefinitions(uow).FirstOrDefault(pd => pd.BeginFlow && pd.FlowDefinitionID == flowDefinitionID);
        }

        public static PhaseDefinitionCache GetEndFlowPhaseDefinition(IUnitOfWork uow, FlowDefinitionID flowDefinitionID)
        {
            return GetPhaseDefinitions(uow).FirstOrDefault(pd => pd.EndFlow && pd.FlowDefinitionID == flowDefinitionID);
        }

        public static PhaseDefinitionCache GetByFlowDefinitionIDAndPhase(IUnitOfWork uow, FlowDefinitionID flowdefinitionID, int phase)
        {
            return GetPhaseDefinitions(uow).FirstOrDefault(pd => pd.FlowDefinitionID == flowdefinitionID && pd.Phase == phase);
        }

        public static PhaseDefinitionCache GetByFlowDefinitionIDAndFlowHandling(IUnitOfWork uow, FlowDefinitionID flowDefinitionID, FlowHandling flowHandling)
        {
            return GetPhaseDefinitions(uow).FirstOrDefault(pd => pd.FlowDefinitionID == flowDefinitionID && pd.FlowHandling == flowHandling);
        }
    }
}
