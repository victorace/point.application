﻿using Point.Database.Models;
using Point.Database.Repository;
using Point.Infrastructure.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;

namespace Point.Business.Logic
{
    public class CryptoCertificateBL
    {
        public static CryptoCertificate GetByID(IUnitOfWork uow, int cryptoCerticateID)
        {
            return uow.CryptoCertificateRepository.GetByID(cryptoCerticateID);
        }

        public static CryptoCertificate GetByFriendlyName(IUnitOfWork uow, string friendlyName)
        {
            return uow.CryptoCertificateRepository.Get(cc => cc.FriendlyName == friendlyName).FirstOrDefault();
        }

        public static IEnumerable<CryptoCertificate> GetAll(IUnitOfWork uow)
        {
            return uow.CryptoCertificateRepository.GetAll();
        }

        public static CryptoCertificate Save(IUnitOfWork uow, string fileName, string privateKeyPlain, byte[] fileData, string description = "")
        {
            var x509certificatefromfiledata = GetX509FromFileData(fileData, privateKeyPlain);
            if (x509certificatefromfiledata == null)
            {
                return null;
            }

            var cryptocertificate = GetByFriendlyName(uow, x509certificatefromfiledata.FriendlyName);
            if (cryptocertificate == null)
            {
                cryptocertificate = new CryptoCertificate();
            }

            cryptocertificate.FileName = fileName;
            cryptocertificate.PrivateKey = MachineKey.Protect(Encoding.UTF8.GetBytes(Convert.ToString(privateKeyPlain)), "Encryption").ByteArrayToHexString();
            cryptocertificate.FileData = fileData;
            cryptocertificate.CertificateType = "X509Certificate2";
            cryptocertificate.Description = description;
            cryptocertificate.ExpiryDate = x509certificatefromfiledata.NotAfter;
            cryptocertificate.FriendlyName = x509certificatefromfiledata.FriendlyName;

            return Upsert(uow, cryptocertificate);
        }

        private static CryptoCertificate Upsert(IUnitOfWork uow, CryptoCertificate cryptoCertificate)
        {
            if (cryptoCertificate.CryptoCertificateID <= 0)
            {
                uow.CryptoCertificateRepository.Insert(cryptoCertificate);
            }
            
            uow.Save();
            return cryptoCertificate;
        }

        public static X509Certificate2 GetX509FromFileData(byte[] fileData, string privateKeyPlain)
        {
            return new X509Certificate2(fileData, privateKeyPlain, X509KeyStorageFlags.PersistKeySet);
        }

        public static X509Certificate2 GetX509FromCryptoCertificate(CryptoCertificate cryptoCertificate)
        {
            byte[] privatekey = cryptoCertificate.PrivateKey.HexStringToByteArray();
            return new X509Certificate2(cryptoCertificate.FileData, Encoding.UTF8.GetString(MachineKey.Unprotect(privatekey, "Encryption")), X509KeyStorageFlags.PersistKeySet);
        }
    }
}
