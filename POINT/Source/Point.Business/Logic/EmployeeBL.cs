﻿using Point.Business.Helper;
using Point.Database.Extensions;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Caching;
using Point.Infrastructure.Extensions;
using Point.Models.Enums;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlTypes;
using System.Linq;
using System.Web.Security;

namespace Point.Business.Logic
{
    public class EmployeeBL
    {
        public static List<Employee> GetUnlockEmployees(IUnitOfWork uow, PointUserInfo userinfo, int transferid)
        {
            var unlockemployees = EmployeeBL.GetByDepartmentAndFunctionRole(uow, userinfo.Department.DepartmentID, FunctionRoleTypeID.UnlockForms).ToList();
            unlockemployees.AddRange(EmployeeBL.GetByLocationAndFunctionRole(uow, userinfo.Location.LocationID, FunctionRoleTypeID.UnlockForms).ToList());
            unlockemployees.AddRange(EmployeeBL.GetByOrganizationAndFunctionRole(uow, userinfo.Location.OrganizationID, FunctionRoleTypeID.UnlockForms).ToList());

            //Check of ze ook bij het dossier kunnen komen...
            var searchq = SearchQueryBL.GetSearchQuerySimple(uow);
            searchq.ApplyTransferFilter(transferid);
            var flowinstancesearchviewmodel = searchq.Queryable.FirstOrDefault();

            var dossierdepartmentids = flowinstancesearchviewmodel.FlowInstanceSearchValues.FlowInstanceOrganization.
                                        Select(it => it.DepartmentID).Distinct().ToArray();
            var dossierlocationids = flowinstancesearchviewmodel.FlowInstanceSearchValues.FlowInstanceOrganization.
                                        Select(it => it.LocationID).Distinct().ToArray();
            var dossierorganizationids = flowinstancesearchviewmodel.FlowInstanceSearchValues.FlowInstanceOrganization.
                                        Select(it => it.OrganizationID).Distinct().ToArray();
            var dossierregionids = flowinstancesearchviewmodel.FlowInstanceSearchValues.FlowInstanceOrganization.
                                        Select(it => it.Organization.RegionID).Distinct().ToArray();

            List<Employee> filteredlist = new List<Employee>();

            foreach (var employee in unlockemployees)
            {
                //Dossierlevel bepalen
                Role role = Authorization.GetPointFlowRole(uow, employee.UserId);
                if (role == Role.None || employee?.aspnet_Users?.aspnet_Membership?.IsLockedOut == true || userinfo.Employee.EmployeeID == employee.EmployeeID)
                {
                    //dont add
                    continue;
                }

                if (role == Role.Global)
                {
                    filteredlist.Add(employee);
                }

                if (role == Role.Department && dossierdepartmentids.Any(ddid => employee.DepartmentID == ddid || employee.EmployeeDepartment.Any(dep => dep.DepartmentID == ddid)))
                {
                    filteredlist.Add(employee);
                }

                if (role == Role.Location && dossierlocationids.Any(dlid => employee.DefaultDepartment.LocationID == dlid || employee.EmployeeDepartment.Any(dep => dep.Department.LocationID == dlid)))
                {
                    filteredlist.Add(employee);
                }

                if (role == Role.Organization && dossierorganizationids.Any(doid => employee.DefaultDepartment.Location.OrganizationID == doid || employee.EmployeeDepartment.Any(dep => dep.Department.Location.OrganizationID == doid)))
                {
                    filteredlist.Add(employee);
                }

                if (role == Role.Region && dossierregionids.Any(drid => employee.DefaultDepartment.Location.Organization.RegionID == drid || employee.EmployeeDepartment.Any(dep => dep.Department.Location.Organization.RegionID == drid)))
                {
                    filteredlist.Add(employee);
                }
            }

            //Max 3
            return filteredlist.Take(3).ToList();
        }

        public static IQueryable<Employee> GetByDepartmentAndFunctionRole(IUnitOfWork uow, int departmentid, FunctionRoleTypeID functionroletypeid)
        {
            return uow.FunctionRoleRepository.Get(fr =>
                (fr.Employee.InActive != true && fr.Employee.DefaultDepartment.Inactive != true && fr.Employee.DefaultDepartment.Location.Inactive != true && 
                    fr.Employee.DefaultDepartment.Location.Organization.Inactive != true && !OrganizationTypeBL.NoneselectableOrganizationTypeIDs.Contains(fr.Employee.DefaultDepartment.Location.Organization.OrganizationTypeID)) &&
                (fr.Employee.DepartmentID == departmentid || fr.Employee.EmployeeDepartment.Any(d => d.DepartmentID == departmentid)) && 
                (fr.FunctionRoleTypeID == functionroletypeid || fr.FunctionRoleTypeID == FunctionRoleTypeID.ManageAll), 
                includeProperties: "Employee.aspnet_Users.aspnet_Roles").Select(fr => fr.Employee);
        }

        public static IQueryable<Employee> GetByLocationAndFunctionRole(IUnitOfWork uow, int locationid, FunctionRoleTypeID functionroletypeid)
        {
            return uow.FunctionRoleRepository.Get(fr =>
                (fr.Employee.InActive != true && fr.Employee.DefaultDepartment.Inactive != true && fr.Employee.DefaultDepartment.Location.Inactive != true && 
                    fr.Employee.DefaultDepartment.Location.Organization.Inactive != true && !OrganizationTypeBL.NoneselectableOrganizationTypeIDs.Contains(fr.Employee.DefaultDepartment.Location.Organization.OrganizationTypeID)) &&
                (fr.Employee.DefaultDepartment.LocationID == locationid || fr.Employee.EmployeeDepartment.Any(d => d.Department.LocationID == locationid)) && 
                (fr.FunctionRoleTypeID == functionroletypeid || fr.FunctionRoleTypeID == FunctionRoleTypeID.ManageAll), 
                includeProperties: "Employee.aspnet_Users.aspnet_Roles").Select(fr => fr.Employee);
        }

        public static IQueryable<Employee> GetByOrganizationAndFunctionRole(IUnitOfWork uow, int organizationid, FunctionRoleTypeID functionroletypeid)
        {
            return uow.FunctionRoleRepository.Get(fr =>
                (fr.Employee.InActive != true && fr.Employee.DefaultDepartment.Inactive != true && fr.Employee.DefaultDepartment.Location.Inactive != true && 
                    fr.Employee.DefaultDepartment.Location.Organization.Inactive != true && !OrganizationTypeBL.NoneselectableOrganizationTypeIDs.Contains(fr.Employee.DefaultDepartment.Location.Organization.OrganizationTypeID)) &&
                (fr.Employee.DefaultDepartment.Location.OrganizationID == organizationid || fr.Employee.EmployeeDepartment.Any(d => d.Department.Location.OrganizationID == organizationid)) && 
                (fr.FunctionRoleTypeID == functionroletypeid || fr.FunctionRoleTypeID == FunctionRoleTypeID.ManageAll), 
                includeProperties: "Employee.aspnet_Users.aspnet_Roles").Select(fr => fr.Employee);
        }

        public static List<Employee> GetByAutoCreateSetID(IUnitOfWork uow, int autocreatesetid)
        {
            return uow.EmployeeRepository.Get(it => it.CreatedWithAutoCreateSetID == autocreatesetid, includeProperties: "aspnet_Users.aspnet_Roles").ToList();
        }

        public static IEnumerable<MutEmployee> GetHistoryByEmployeeID(IUnitOfWork uow, int employeeid)
        {
            return uow.MutEmployeeRepository.Get(it => it.EmployeeID == employeeid);
        }

        public static IQueryable<Employee> Search(IUnitOfWork uow, string name, string organization, int? regionid, int[] organizationids,
            int[] locationids, int[] departmentids, bool searchdeleted, DateTime? lastLogin)
        {
            var names = string.IsNullOrWhiteSpace(name) ? new string[0] : name.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            var nameCount = names?.Length ?? 0;

            organizationids = organizationids ?? new int[0];
            locationids = locationids ?? new int[0];
            departmentids = departmentids ?? new int[0];

            var organizationlist = new List<string>();
            if (!string.IsNullOrEmpty(organization))
                organizationlist = organization.Split(' ')
                    .Where(it => !string.IsNullOrEmpty(it)).ToList();

            return uow.EmployeeRepository.Get(emp =>
                (
                    regionid == null ||
                    emp.DefaultDepartment.Location.Organization.RegionID == regionid
                )
                &&
                (
                    !organizationids.Any() ||
                    organizationids.Contains(emp.DefaultDepartment.Location.OrganizationID)
                )
                &&
                (
                    !locationids.Any() ||
                    locationids.Contains(emp.DefaultDepartment.LocationID)
                )
                &&
                (
                    !departmentids.Any() ||
                    departmentids.Contains(emp.DepartmentID)
                )
                &&
                (
                    searchdeleted ||
                    !emp.InActive
                )
                &&
                (
                    nameCount == 0 ||
                    names.All(n =>
                        emp.FirstName.Contains(n) ||
                        emp.MiddleName.Contains(n) ||
                        emp.LastName.Contains(n) ||
                        emp.ExternID.Equals(n) ||
                        emp.aspnet_Users.UserName.Contains(n))
                )
                &&
                (
                    !organizationlist.Any() ||
                    organizationlist.All(ns =>
                        emp.DefaultDepartment.Name.Contains(ns) ||
                        emp.DefaultDepartment.Location.Name.Contains(ns) ||
                        emp.DefaultDepartment.Location.Organization.Name.Contains(ns))
                )
                &&
                (
                    lastLogin == null ||
                    emp.aspnet_Users.LastActivityDate <= lastLogin
                )
                &&
                (
                    // Exclude Beheer organizations
                    !OrganizationTypeBL.NoneselectableOrganizationTypeIDs.Contains(emp.DefaultDepartment.Location.Organization.OrganizationTypeID)
                )
            );
        }

        public static IEnumerable<Employee> SearchBulkEmployee(
           IUnitOfWork uow, int? regionid, int[] organizationids,
           int[] locationids, int[] departmentids, bool blockedUsers, DateTime? lastLogin)
        {
            organizationids = organizationids ?? new int[0];
            locationids = locationids ?? new int[0];
            departmentids = departmentids ?? new int[0];

            return uow.EmployeeRepository.Get(emp =>
                    (
                        regionid == null ||
                        emp.DefaultDepartment.Location.Organization.RegionID == regionid
                    )
                    &&
                    (
                        !organizationids.Any() ||
                        organizationids.Contains(emp.DefaultDepartment.Location.OrganizationID)
                    )
                    &&
                    (
                        !locationids.Any() ||
                        locationids.Contains(emp.DefaultDepartment.LocationID)
                    )
                    &&
                    (
                        !departmentids.Any() ||
                        departmentids.Contains(emp.DepartmentID)
                    )                  
                    &&
                    (
                        lastLogin == null ||
                        emp.aspnet_Users.LastActivityDate <= lastLogin
                    )
                    &&
                    ( 
                        (blockedUsers && !emp.aspnet_Users.aspnet_Membership.IsLockedOut) ||
                        (!blockedUsers && emp.aspnet_Users.aspnet_Membership.IsLockedOut)
                        
                    )
                    &&
                    (
                        !(emp.InActive)
                    )
                    &&
                    (
                        // Exclude Beheer organizations
                        !OrganizationTypeBL.NoneselectableOrganizationTypeIDs.Contains(emp.DefaultDepartment.Location.Organization.OrganizationTypeID)
                    )

                , includeProperties: "aspnet_Users.aspnet_Membership"
            );
        }

        public static void DeleteByID(IUnitOfWork uow, int employeeID)
        {
            var employee = GetByID(uow, employeeID);
            var username = employee.aspnet_Users?.UserName;

            employee.UserId = null;
            employee.InActive = true;

            FunctionRoleBL.DeleteByEmployeeID(uow, employeeID);
            
            uow.Save();

            if(!string.IsNullOrEmpty(username))
            {                
                Membership.DeleteUser(username);
            }
        }
        
        public static void LockOrUnlockEmployee(Employee employee, bool lockemployee, LogEntry logentry)
        {
            if (employee.aspnet_Users?.aspnet_Membership != null)
            {
                if (lockemployee != employee.aspnet_Users.aspnet_Membership.IsLockedOut)
                {
                    employee.aspnet_Users.aspnet_Membership.IsLockedOut = lockemployee;
                    if(!lockemployee)
                    {
                        employee.aspnet_Users.aspnet_Membership.FailedPasswordAttemptCount =  0;
                    }
                    employee.aspnet_Users.aspnet_Membership.LastLockoutDate = (lockemployee ? logentry.Timestamp : SqlDateTime.MinValue.Value);

                    employee.LockAction = lockemployee ? "locked" : "unlocked";
                }
                else
                {
                    employee.LockAction = "";
                }
            }
        }

        private static void UpdateFunctionRoles(IUnitOfWork uow, Employee employee, FunctionRoleTypeID[] functionroletypeids, FunctionRoleLevelID functionrolelevelid, LogEntry logentry)
        {
            if (functionroletypeids != null)
            {
                var currentfunctionroletypeids = employee.FunctionRole.Select(it => it.FunctionRoleTypeID);

                var nottodelete = new FunctionRoleTypeID[] { FunctionRoleTypeID.Reporting, FunctionRoleTypeID.MaxDossierLevelAdmin };
                var todelete = currentfunctionroletypeids.Where(id => !functionroletypeids.Contains(id) && !nottodelete.Contains(id)).ToArray();
                uow.FunctionRoleRepository.Delete(fr => fr.EmployeeID == employee.EmployeeID && todelete.Contains(fr.FunctionRoleTypeID));

                var toadd = functionroletypeids.Where(id => !currentfunctionroletypeids.Contains(id)).ToList();
                toadd.ForEach(id => employee.FunctionRole.Add(new FunctionRole() { FunctionRoleTypeID = id, FunctionRoleLevelID = functionrolelevelid, ModifiedTimeStamp = logentry.Timestamp }));

                foreach (var item in functionroletypeids)
                {
                    var current = employee.FunctionRole.FirstOrDefault(it => it.FunctionRoleTypeID == item);
                    if(current != null && current.FunctionRoleLevelID != functionrolelevelid)
                    {
                        current.FunctionRoleLevelID = functionrolelevelid;
                        current.ModifiedTimeStamp = logentry.Timestamp;
                    }
                }
            }
        }

        private static void UpdateReportingFunction(IUnitOfWork uow, Employee employee, FunctionRoleLevelID functionrolelevelid, LogEntry logentry)
        {
            if (functionrolelevelid == FunctionRoleLevelID.None)
            {
                uow.FunctionRoleRepository.Delete(f => f.EmployeeID == employee.EmployeeID && f.FunctionRoleTypeID == FunctionRoleTypeID.Reporting);
            } else
            {
                var existingreportingfunction = employee.FunctionRole.FirstOrDefault(it => it.FunctionRoleTypeID == FunctionRoleTypeID.Reporting);
                if(existingreportingfunction == null)
                {
                    employee.FunctionRole.Add(new FunctionRole() { FunctionRoleTypeID = FunctionRoleTypeID.Reporting, FunctionRoleLevelID = functionrolelevelid, ModifiedTimeStamp = logentry.Timestamp });
                } else if(existingreportingfunction.FunctionRoleLevelID != functionrolelevelid)
                {
                    existingreportingfunction.FunctionRoleLevelID = functionrolelevelid;
                    existingreportingfunction.ModifiedTimeStamp = logentry.Timestamp;
                }
            }
        }

        private static void UpdateMaxDossierAdminLevel(IUnitOfWork uow, Employee employee, FunctionRoleLevelID functionrolelevelid, LogEntry logentry)
        {
            if (functionrolelevelid == FunctionRoleLevelID.None)
            {
                uow.FunctionRoleRepository.Delete(f => f.EmployeeID == employee.EmployeeID && f.FunctionRoleTypeID == FunctionRoleTypeID.MaxDossierLevelAdmin);
            }
            else
            {
                var existingreportingfunction = employee.FunctionRole.FirstOrDefault(it => it.FunctionRoleTypeID == FunctionRoleTypeID.MaxDossierLevelAdmin);
                if (existingreportingfunction == null)
                {
                    employee.FunctionRole.Add(new FunctionRole() { FunctionRoleTypeID = FunctionRoleTypeID.MaxDossierLevelAdmin, FunctionRoleLevelID = functionrolelevelid, ModifiedTimeStamp = logentry.Timestamp });
                }
                else if (existingreportingfunction.FunctionRoleLevelID != functionrolelevelid)
                {
                    existingreportingfunction.FunctionRoleLevelID = functionrolelevelid;
                    existingreportingfunction.ModifiedTimeStamp = logentry.Timestamp;
                }
            }
        }

        private static void UpdateRole(IUnitOfWork uow, Employee employee, string username,  FunctionRoleLevelID dossierLevelID, int departmentid)
        {
            if (employee.aspnet_Users != null)
            {
                var previousPointFlowRole = Authorization.GetPointFlowRole(uow, employee.UserId);
                if (previousPointFlowRole != Role.None)
                {
                    Roles.RemoveUserFromRole(username, previousPointFlowRole.ToString());
                }
            }

            if (employee.aspnet_Users == null)
            {
                //Pointoud rol toevoegen

                var department = DepartmentBL.GetByDepartmentID(uow, departmentid);
                var organization = department?.Location?.Organization;
                if (organization != null)
                {
                    var oldrolename = "";

                    switch ((OrganizationTypeID)organization.OrganizationTypeID)
                    {
                        case OrganizationTypeID.Hospital:
                            if (department.DepartmentTypeID == (int)DepartmentTypeID.Afdeling)
                            {
                                oldrolename = "HospitalDepartmentEmployee";
                            }
                            else if (department.DepartmentTypeID == (int)DepartmentTypeID.Transferpunt)
                            {
                                oldrolename = "TransferpointEmployee";
                            }
                            break;
                        case OrganizationTypeID.HealthCareProvider:
                            oldrolename = "HealthCareProviderEmployee";
                            break;
                        case OrganizationTypeID.CIZ:
                            oldrolename = "CIZEmployee";
                            break;
                    }

                    if (oldrolename != "")
                    {
                        Roles.AddUserToRole(username, oldrolename);
                    }
                }
            }

            if (dossierLevelID != FunctionRoleLevelID.None)
            {
                Roles.AddUserToRole(username, dossierLevelID.ToString());
            }
        }

        private static void UpdatePassword(Employee employee, string password)
        {
            if (!string.IsNullOrWhiteSpace(password) && employee.aspnet_Users != null)
            {
                var membershipuser = Membership.GetUser(employee.aspnet_Users.UserName);
                membershipuser.ChangePassword(membershipuser.ResetPassword(), password);
            }
        }

        public static void UpdateChangePassword(IUnitOfWork uow, PointUserInfo pointUserInfo, bool changePasswordByEmployee)
        {
            var employee = GetByID(uow, pointUserInfo.Employee.EmployeeID);
            employee.ChangePasswordByEmployee = changePasswordByEmployee;
            pointUserInfo.Employee.ChangePasswordByEmployee = changePasswordByEmployee;
            uow.Save();
        }

        public static void LogTooManyBadLogins(IUnitOfWork uow, MembershipUser membershipuser)
        {
            if (membershipuser == null)
            {
                return;
            }

            var employee = GetByUserName(uow, membershipuser.UserName);
            if (employee != null)
            {
                var logentry = LogEntryBL.Create(FlowFormType.NoScreen, employee.EmployeeID);
                if (membershipuser.IsLockedOut && membershipuser.LastLockoutDate > DateTime.UtcNow.AddSeconds(-1))
                {
                    employee.LockAction = "locked (too many bad logins)";
                    LoggingBL.FillLoggable(uow, employee, logentry, forceupdate: true);
                    uow.Save(logentry);
                }
            }
        }

        public static Employee Save(IUnitOfWork uow, EmployeeViewModel viewmodel, LogEntry logentry, PointUserInfo pointuserinfo)
        {
            var model = GetByID(uow, viewmodel.EmployeeID) ?? new Employee();
            MembershipUser membershipuser = null;
            bool createnewuser = false;

            //This is needed since the membershipuser is deleted when u deactivate an employee to free-up the username
            if(model.aspnet_Users == null && !string.IsNullOrEmpty(viewmodel.UserName))
            {
                createnewuser = true;
                membershipuser = Membership.CreateUser(viewmodel.UserName, viewmodel.Password);
            }
            else if(!string.IsNullOrEmpty(viewmodel.UserName))
            {
                membershipuser = Membership.GetUser(viewmodel.UserName);
            }
            else
            {
                throw new Exception("Not enough data to create or update user");
            }

            if (string.IsNullOrEmpty(model.UserId?.ToString()))
            {
                model.UserId = (Guid?)membershipuser?.ProviderUserKey;
                if (model.EmployeeID == 0)
                {
                    uow.EmployeeRepository.Insert(model);
                }                
            }
            
            var currentdepartmentids = model.EmployeeDepartment.Select(it => it.DepartmentID).ToArray();
            if(!currentdepartmentids.SequenceEqual(viewmodel.OtherDepartmentID))
            {
                var deleteddepartmentids = currentdepartmentids.Where(cdid => !viewmodel.OtherDepartmentID.Contains(cdid));
                var newdepartmentids = viewmodel.OtherDepartmentID.Where(oid => !currentdepartmentids.Contains(oid)).ToList();

                uow.EmployeeDepartmentRepository.Delete(ed => ed.EmployeeID == model.EmployeeID && deleteddepartmentids.Contains(ed.DepartmentID));
                newdepartmentids.ForEach(d => model.EmployeeDepartment.Add(new EmployeeDepartment() { DepartmentID = d, ModifiedTimeStamp = logentry.Timestamp }));
            }

            model.WriteSecureValue(m => m.InActive, viewmodel, v => v.Inactive, pointuserinfo);
            model.WriteSecureValue(m => m.DepartmentID, viewmodel, v => v.DepartmentID, pointuserinfo);
            model.WriteSecureValue(m => m.FirstName, viewmodel, v => v.FirstName, pointuserinfo);
            model.WriteSecureValue(m => m.MiddleName, viewmodel, v => v.MiddleName, pointuserinfo);
            model.WriteSecureValue(m => m.LastName, viewmodel, v => v.LastName, pointuserinfo);
            model.WriteSecureValue(m => m.Gender, viewmodel, v => v.Gender, pointuserinfo);
            model.WriteSecureValue(m => m.Position, viewmodel, v => v.Function, pointuserinfo);
            model.WriteSecureValue(m => m.PhoneNumber, viewmodel, v => v.PhoneNumber, pointuserinfo);
            model.WriteSecureValue(m => m.EmailAddress, viewmodel, v => v.EmailAddress, pointuserinfo);
            model.WriteSecureValue(m => m.ExternID, viewmodel, v => v.ZISID, pointuserinfo);
            model.WriteSecureValue(m => m.BIG, viewmodel, v => v.BIGNR, pointuserinfo);
            model.WriteSecureValue(m => m.ChangePasswordByEmployee, viewmodel, v => v.ChangePasswordOnNextLogin, pointuserinfo);
            model.WriteSecureValue(m => m.EditAllForms, viewmodel, v => v.EditAllForms, pointuserinfo);
            model.WriteSecureValue(m => m.ReadOnlyReciever, viewmodel, v => v.ReadOnlyReciever, pointuserinfo);
            model.WriteSecureValue(m => m.ReadOnlySender, viewmodel, v => v.ReadOnlySender, pointuserinfo);
            model.WriteSecureValue(m => m.MFANumber, viewmodel, v => v.MFANumber, pointuserinfo);
            model.WriteSecureValue(m => m.MFAKey, viewmodel, v => v.MFAKey, pointuserinfo);
            model.WriteSecureValue(m => m.MFAConfirmedDate, viewmodel, v => v.MFAConfirmedDate, pointuserinfo);
            model.WriteSecureValue(m => m.MFARequired, viewmodel, v => v.MFARequired, pointuserinfo);
            model.WriteSecureValue(m => m.IPAddresses, viewmodel, v => v.IPAddresses, pointuserinfo);
            model.WriteSecureValue(m => m.AutoLockAccountDate, viewmodel, v => v.AutoLockAccountDate, pointuserinfo);
            model.WriteSecureValue(m => m.AutoEmailSignalering, viewmodel, v => v.AutoEmailSignalering, pointuserinfo);

            LockOrUnlockEmployee(model, viewmodel.EmployeeIsLocked, logentry);
            UpdateFunctionRoles(uow, model, viewmodel.FunctionRoleTypeIDs, viewmodel.FunctionRoleLevelID, logentry);
            UpdateReportingFunction(uow, model, viewmodel.ReportingLevelID, logentry);
            UpdateMaxDossierAdminLevel(uow, model, viewmodel.MaxDossierLevelAdminLevelID, logentry);
            UpdateRole(uow, model, viewmodel.UserName, viewmodel.DossierLevelID, viewmodel.DepartmentID);
            UpdatePassword(model, viewmodel.Password);

            //Daarna pas de normale logging
            LoggingBL.FillLoggable(uow, model, logentry, forceupdate: true);

            try
            {
                uow.Save(logentry);
            }
            catch
            {
                if (createnewuser)
                {
                    //Delete membership user if something went wrong when creating a new user
                    //So that the username stays available
                    Membership.DeleteUser(viewmodel.UserName);
                }
                throw;
            }

            return model;
        }

        public static Employee GetByID(IUnitOfWork uow, int employeeID)
        {
            return uow.EmployeeRepository.GetByID(employeeID);
        }

        public static IEnumerable<Employee> GetByIDs(IUnitOfWork uow, IEnumerable<int> employeeIDs)
        {
            return uow.EmployeeRepository.Get(emp => employeeIDs.Contains(emp.EmployeeID));
        }

        public static Employee GetByUserName(IUnitOfWork uow, string username)
        {
            var searchvalue = username.ToLower();
            return uow.EmployeeRepository.Get(emp => emp.aspnet_Users.LoweredUserName == searchvalue).FirstOrDefault();
        }

        public static Employee GetByUserName(IUnitOfWork uow, string username, int organizationID)
        {
            var searchvalue = username.ToLower();
            return (from employees in uow.EmployeeRepository.Get()
                    join departmens in uow.DepartmentRepository.Get() on employees.DepartmentID equals departmens.DepartmentID
                    join locations in uow.LocationRepository.Get() on departmens.LocationID equals locations.LocationID
                    where locations.OrganizationID == organizationID
                    && employees.aspnet_Users.LoweredUserName == searchvalue
                    && !(employees.InActive)
                    select employees).FirstOrDefault();
        }

        public static Employee GetByExternID(IUnitOfWork uow, string externID, int organizationID)
        {
            return (from employees in uow.EmployeeRepository.Get()
                   join departmens in uow.DepartmentRepository.Get() on employees.DepartmentID equals departmens.DepartmentID
                   join locations in uow.LocationRepository.Get() on departmens.LocationID equals locations.LocationID
                   where locations.OrganizationID == organizationID
                   && employees.ExternID == externID
                   && !(employees.InActive)
                   select employees).FirstOrDefault();
        }

        public static List<Option> GetCachedByDepartmentID(IUnitOfWork uow, int departmentID)
        {
            var cacheKey = "EmployeeOptionByDepartmentID_" + departmentID;
            var options = CacheService.Instance.Get<List<Option>>(cacheKey);
            if(options == null)
            {
                options = new List<Option>();

                var employees = uow.EmployeeRepository.Get(emp => emp.DepartmentID == departmentID && !(bool)emp.InActive);
                employees.ToList().ForEach(emp => options.Add(new Option() { Value = emp.EmployeeID.ToString(), Text = emp.FullName() }));

                CacheService.Instance.Insert(cacheKey, options);
            }

            return options;
        }

        public static IEnumerable<Employee> GetByDepartmentID(IUnitOfWork uow, int departmentID, bool includeInactive = false)
        {
            return uow.EmployeeRepository.Get(emp => emp.DepartmentID == departmentID && (includeInactive ? true : !(bool)emp.InActive));
        }

        public static List<Option> GetCachedByDepartmentIDIncludingConnected(IUnitOfWork uow, int departmentID)
        {
            var cacheKey = "EmployeeOptionByDepartmentIDIncludingConnected_" + departmentID;
            var options = CacheService.Instance.Get<List<Option>>(cacheKey);
            if (options == null)
            {
                options = new List<Option>();

                var employees = uow.EmployeeRepository.Get(emp => (emp.DepartmentID == departmentID || emp.EmployeeDepartment.Any(dep => dep.DepartmentID == departmentID)) && !(bool)emp.InActive);
                employees.ToList().ForEach(emp => options.Add(new Option() { Value = emp.EmployeeID.ToString(), Text = emp.FullName() }));

                CacheService.Instance.Insert(cacheKey, options);
            }

            return options.AsClone();
        }

        public static IEnumerable<Employee> GetTransferPointEmployeesByOrganizationID(IUnitOfWork uow, int organizationid)
        {
            return uow.EmployeeRepository.Get(emp => !(bool)emp.InActive && emp.DefaultDepartment.DepartmentTypeID == (int)DepartmentTypeID.Transferpunt && emp.DefaultDepartment.Location.OrganizationID == organizationid);
        }

        public static bool EmployeeIsOfDepartmentType(IUnitOfWork uow, int employeeId, DepartmentTypeID departmentTypeID)
        {
            var department = DepartmentBL.GetByEmployeeID(uow, employeeId);
            return department.DepartmentTypeID == (int)departmentTypeID;
        }

        public static IEnumerable<Employee> GetByLocationID(IUnitOfWork uow, int locationID)
        {
            return from employees in uow.EmployeeRepository.Get()
                   join departmens in uow.DepartmentRepository.Get() on employees.DepartmentID equals departmens.DepartmentID
                   join locations in uow.LocationRepository.Get() on departmens.LocationID equals locations.LocationID
                   where locations.LocationID == locationID
                   select employees;
        }

        public static IEnumerable<Employee> GetByOrganizationID(IUnitOfWork uow, int organizationID)
        {
            return from employees in uow.EmployeeRepository.Get()
                   join departmens in uow.DepartmentRepository.Get() on employees.DepartmentID equals departmens.DepartmentID
                   join locations in uow.LocationRepository.Get() on departmens.LocationID equals locations.LocationID
                   join organisations in uow.OrganizationRepository.Get() on locations.OrganizationID equals organisations.OrganizationID
                   where organisations.OrganizationID == organizationID && employees.InActive != true
                   select employees;
        }
        
        public static void Upsert(IUnitOfWork uow, Employee employee)
        {
            if (employee.EmployeeID == 0)
            {
                uow.EmployeeRepository.Insert(employee);
            }
            //else
            //{
            //    uow.EmployeeRepository.Update(employee);
            //}

        }

        public static void Save(IUnitOfWork uow, Employee employee)
        {
            Upsert(uow, employee);
            uow.Save();
        }

        public static IQueryable<Employee> ExcludeAdminsWithEmailsContaining(IQueryable<Employee> employees, string[] exclude)
        {
            //exclude following email domains from support list           
            return employees?.Where(emp => string.IsNullOrEmpty(emp.EmailAddress) || !exclude.Any(exc => emp.EmailAddress.Contains(exc)));
        }

        public static IQueryable<Employee> GetDepartmentAdmins(IUnitOfWork uow, PointUserInfo pointUser)
        {
            //get direct department admin.
            return uow.EmployeeRepository.Get(emp => pointUser.EmployeeDepartmentIDs.Contains(emp.DepartmentID) && 
            emp.FunctionRole.Any(role => ( role.FunctionRoleTypeID == FunctionRoleTypeID.ManageEmployees || role.FunctionRoleTypeID == FunctionRoleTypeID.ManageAll) && role.FunctionRoleLevelID == FunctionRoleLevelID.Department) &&            
            !(emp.InActive) && !(emp.aspnet_Users != null && emp.aspnet_Users.aspnet_Membership != null  && emp.aspnet_Users.aspnet_Membership.IsLockedOut));            
        }

        public static IQueryable<Employee> GetLocationAdmins(IUnitOfWork uow, PointUserInfo pointUser)
        {
              return  from employees in uow.EmployeeRepository.Get()
                      join departments in uow.DepartmentRepository.Get() on employees.DepartmentID equals departments.DepartmentID
                      join locations in uow.LocationRepository.Get() on departments.LocationID equals locations.LocationID
                      where pointUser.EmployeeLocationIDs.Contains(locations.LocationID) && 
                      employees.FunctionRole.Any(role => (role.FunctionRoleTypeID == FunctionRoleTypeID.ManageEmployees || role.FunctionRoleTypeID == FunctionRoleTypeID.ManageAll) && role.FunctionRoleLevelID == FunctionRoleLevelID.Location) &&                      
                      !(employees.InActive) && 
                      !(employees.aspnet_Users != null && employees.aspnet_Users.aspnet_Membership != null && employees.aspnet_Users.aspnet_Membership.IsLockedOut)

                      select employees;
            
        }

        public static IQueryable<Employee> GetOrganizationAdmins(IUnitOfWork uow, PointUserInfo pointUser)
        {
            return from employees in uow.EmployeeRepository.Get()
                      join departments in uow.DepartmentRepository.Get() on employees.DepartmentID equals departments.DepartmentID
                      join locations in uow.LocationRepository.Get() on departments.LocationID equals locations.LocationID
                      join organizations in uow.OrganizationRepository.Get() on locations.OrganizationID equals organizations.OrganizationID
                      where pointUser.EmployeeOrganizationIDs.Contains(organizations.OrganizationID) && 
                      employees.FunctionRole.Any(role => (role.FunctionRoleTypeID == FunctionRoleTypeID.ManageEmployees || role.FunctionRoleTypeID == FunctionRoleTypeID.ManageAll) && role.FunctionRoleLevelID == FunctionRoleLevelID.Organization) &&                      
                      !(employees.InActive) &&
                      !(employees.aspnet_Users != null && employees.aspnet_Users.aspnet_Membership != null && employees.aspnet_Users.aspnet_Membership.IsLockedOut)
                      select employees;
                                   
        }

        public static IQueryable<Employee> GetRegionalAdmins(IUnitOfWork uow, PointUserInfo pointUser)
        {
            return from employees in uow.EmployeeRepository.Get()
                      join departments in uow.DepartmentRepository.Get() on employees.DepartmentID equals departments.DepartmentID
                      join locations in uow.LocationRepository.Get() on departments.LocationID equals locations.LocationID
                      join organizations in uow.OrganizationRepository.Get() on locations.OrganizationID equals organizations.OrganizationID
                      join regions in uow.RegionRepository.Get() on organizations.RegionID equals regions.RegionID
                      where pointUser.Region.RegionID == regions.RegionID && 
                      employees.FunctionRole.Any(role => (role.FunctionRoleTypeID == FunctionRoleTypeID.ManageEmployees || role.FunctionRoleTypeID == FunctionRoleTypeID.ManageAll) && role.FunctionRoleLevelID == FunctionRoleLevelID.Region) &&                      
                      !(employees.InActive) &&
                      !(employees.aspnet_Users != null && employees.aspnet_Users.aspnet_Membership != null && employees.aspnet_Users.aspnet_Membership.IsLockedOut)
                   select employees;            
        }

        public static List<Employee> GetAdminsEmails(IUnitOfWork uow, int employeeId)
        {

            var userInfo = PointUserInfoHelper.GetPointUserInfoByEmployeeID(uow, employeeId);
            if (userInfo == null)
            {
                return null;
            }

            IQueryable<Employee> admins;

            if (!userInfo.Employee.FunctionRoles.Any() || userInfo.Employee.FunctionRoles.Where(role => role.FunctionRoleLevelID < FunctionRoleLevelID.Department).Any())
            {
                admins = GetDepartmentAdmins(uow, userInfo)?.Where(m => !string.IsNullOrEmpty(m.EmailAddress) && m.EmployeeID != userInfo.Employee.EmployeeID);
                
                if (admins != null && admins.Count() > 0)
                {
                    return admins.ToList();
                }
            }

            if (!userInfo.Employee.FunctionRoles.Any() || userInfo.Employee.FunctionRoles.Where(role => role.FunctionRoleLevelID < FunctionRoleLevelID.Location).Any())
            {
                admins = GetLocationAdmins(uow, userInfo)?.Where(m => !string.IsNullOrEmpty(m.EmailAddress) && m.EmployeeID != userInfo.Employee.EmployeeID);
                //beheerder medewerker admin in location (if avaliable)
                if (admins != null && admins.Count() > 0)
                {
                    return admins.ToList();
                }
            }

            if (!userInfo.Employee.FunctionRoles.Any() || userInfo.Employee.FunctionRoles.Where(role => role.FunctionRoleLevelID < FunctionRoleLevelID.Organization).Any())
            {
                admins = GetOrganizationAdmins(uow, userInfo)?.Where(m => !string.IsNullOrEmpty(m.EmailAddress) && m.EmployeeID != userInfo.Employee.EmployeeID);
                //beheerder medewerker admin in Organization (if avaliable)
                if (admins != null && admins.Count() > 0)
                {
                    return admins.ToList();
                }
            }

            if (!userInfo.Employee.FunctionRoles.Any() || userInfo.Employee.FunctionRoles.Where(role => role.FunctionRoleLevelID < FunctionRoleLevelID.Region).Any())
            {
                admins = GetRegionalAdmins(uow, userInfo)?.Where(m => !string.IsNullOrEmpty(m.EmailAddress) && m.EmployeeID != userInfo.Employee.EmployeeID);
                //beheerder medewerker admin in Region (if avaliable)
                if (admins != null && admins.Count() > 0)
                {
                    return admins.ToList();
                }
            }

            return null;
        }

        public static string AutoLockEmployee(IUnitOfWork uow)
        {
            var result = "";
            var employeesToBeLocked = uow.EmployeeRepository.Get(emp => emp.UserId.HasValue && !emp.InActive && !emp.aspnet_Users.aspnet_Membership.IsLockedOut &&
                                                                 emp.AutoLockAccountDate.HasValue && emp.AutoLockAccountDate <= DateTime.Now);
            try {

                int systememployeeid = PointUserInfoHelper.GetSystemEmployeeID();
                var logEntry = new LogEntry
                {
                    EmployeeID = PointUserInfoHelper.GetSystemEmployeeID(),
                    Timestamp = DateTime.Now,
                    Screenname = null
                };

                foreach (var emp in employeesToBeLocked)
                {
                    LockOrUnlockEmployee(emp, true, logEntry);
                }
                uow.Save(logEntry);
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return result;
        }
    }
}
