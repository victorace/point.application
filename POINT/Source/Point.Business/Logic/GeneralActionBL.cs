﻿using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Repository;
using Point.Infrastructure.Caching;
using Point.Infrastructure.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Point.Business.Logic
{
    public class GeneralActionBL
    {
        private static IEnumerable<GeneralAction> getGeneralActions(IUnitOfWork uow, bool usecache = true)
        {
            string cachestring = "GeneralAction_Global";
            IList<GeneralAction> generalactions = null;

            if (usecache)
                generalactions = CacheService.Instance.Get<IList<GeneralAction>>(cachestring);

            if (generalactions == null)
            {
                generalactions = uow.GeneralActionRepository.GetAll().ToList();
                if (generalactions.Any())
                    CacheService.Instance.Insert(cachestring, generalactions, "CacheLong");
            }
            return generalactions;
        }
        
        public static IEnumerable<GeneralAction> GetByPhaseDefinitionAndTypeName(IUnitOfWork uow, int phaseDefinitionID, GeneralActionTypeName name, Status? currentstatus = null)
        {
            GeneralActionType generalActionType = GeneralActionTypeBL.GetByName(uow, name);
            if (generalActionType == null || generalActionType == default(GeneralActionType))
                return Enumerable.Empty<GeneralAction>();

            IEnumerable<GeneralActionPhase> generalActionPhases = GeneralActionPhaseBL.GetByPhaseDefinitionID(uow, phaseDefinitionID);

            return generalActionPhases.Where(gap => 
                gap.GeneralAction.GeneralActionTypeID == generalActionType.GeneralActionTypeID &&
                (gap.PhaseStatusBegin == null || gap.PhaseStatusBegin <= (int?)currentstatus) && 
                (gap.PhaseStatusEnd == null || gap.PhaseStatusEnd >= (int?)currentstatus)
                
                ).Select(gap => gap.GeneralAction);
        }

        public static GeneralAction GetByURL(IUnitOfWork uow, Uri uri)
        {
            if (uri != null && !String.IsNullOrWhiteSpace(uri.AbsolutePath))
            {
                var pathParts = uri.AbsolutePath.Split('/').TakeLast<string>(2).ToList();
                if (pathParts.Count() == 2)
                {
                    return GetByURL(uow, pathParts[0], pathParts[1]);
                }
            }
            return null;
        }

        public static GeneralAction GetByURL(IUnitOfWork uow, string controllerName, string actionName)
        {
            string url = String.Concat(controllerName, "/", actionName);
            return getGeneralActions(uow).Where(ga => ga.MethodName == url).FirstOrDefault();
        }

        public static GeneralAction GetByID(IUnitOfWork uow, int generalActionID)
        {
            return getGeneralActions(uow).FirstOrDefault(ga => ga.GeneralActionID == generalActionID);
        }
    }
}
