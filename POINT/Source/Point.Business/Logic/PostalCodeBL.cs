﻿using Point.Database.Models;
using Point.Database.Repository;

namespace Point.Business.Logic
{
    public class PostalCodeBL
    {
        public static PostalCode GetByID(IUnitOfWork uow, int postalcodeid)
        {
            return uow.PostalCodeRepository.GetByID(postalcodeid);
        }
    }
}
