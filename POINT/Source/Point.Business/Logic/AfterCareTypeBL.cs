﻿using System;
using System.Collections.Generic;
using System.Linq;
using Point.Database.Models;
using Point.Database.Repository;
using Point.Infrastructure.Caching;
using Point.Database.Context;
using Point.Models.Interfaces;
using Point.Database.Extensions;
using Point.Models.Enums;
using afterCareTypes = Point.Models.Enums.AfterCareType;
using AfterCareType = Point.Database.Models.AfterCareType;

namespace Point.Business.Logic
{
    public class AfterCareTypeBL
    {
        private const string cacheKeyFormatID = "AfterCareTypes_ID_{0}";
        private const string cacheKeyFormat = "AfterCareTypes_FormTypeAfterCareCategory_{0}_{1}";
        private const string cacheKeyFormatOrganizationType = "AfterCareTypes_OrganizationType_{0}";

        public static AfterCareType GetCachedByID(IUnitOfWork uow, int aftercaretypeid)
        {
            var cacheKey = String.Format(cacheKeyFormatID, aftercaretypeid);
            var afterCareType = CacheService.Instance.Get<AfterCareType>(cacheKey);
            if(afterCareType == null)
            {
                afterCareType = uow.AfterCareTypeRepository.FirstOrDefault(a => a.AfterCareTypeID == aftercaretypeid, includeProperties: "AfterCareCategory");
                CacheService.Instance.Insert(cacheKey, afterCareType, profile: "CacheLong");
            }

            return afterCareType;
        }

        public static List<AfterCareType> GetByCurrentValue(IUnitOfWork uow, int? aftercaretypeid, int? formtypeid)
        {
            if (aftercaretypeid == null)
            {
                return new List<AfterCareType>();
            }

            var currentaftercaretype = uow.AfterCareTypeRepository.GetByID(aftercaretypeid.Value);
            if (currentaftercaretype == null)
            {
                return new List<AfterCareType>();
            }

            if (formtypeid == null)
            {
                return new List<AfterCareType>() { currentaftercaretype };
            }

            var categoryitems = GetByFormTypeAndCategory(uow, formtypeid.Value, currentaftercaretype.AfterCareCategoryID).ToList();
            if (categoryitems.All(it => it.AfterCareTypeID != currentaftercaretype.AfterCareTypeID))
            {
                categoryitems.Add(currentaftercaretype);
            }

            return categoryitems;
        }

        public static IEnumerable<AfterCareType> GetByOrganizationTypeID(int organizationtypeid)
        {
            var cacheKey = String.Format(cacheKeyFormatOrganizationType, organizationtypeid);
            var afterCareTypes = CacheService.Instance.Get<IList<AfterCareType>>(cacheKey);
            if (afterCareTypes == null)
            {
                using (var uow = new UnitOfWork<PointContext>())
                {
                    afterCareTypes = uow.AfterCareTypeRepository.Get(x => x.OrganizationTypeID == organizationtypeid && !x.Inactive).OrderBy(x => x.Sortorder).ToList();
                    CacheService.Instance.Insert(cacheKey, afterCareTypes, profile: "CacheLong");
                }
            }

            return afterCareTypes;
        }

        public static List<AfterCareType> GetByFormTypeAndCategory(IUnitOfWork uow, int formTypeID, int? afterCareCategoryID)
        {
            var cacheKey = String.Format(cacheKeyFormat, formTypeID, afterCareCategoryID);
            var afterCareTypes = CacheService.Instance.Get<List<AfterCareType>>(cacheKey);

            if (afterCareTypes == null)
            {
                afterCareTypes = uow.AfterCareTypeRepository
                    .Get(x => x.AfterCareTypeFormType.Any(aft => aft.FormTypeID == formTypeID) && x.AfterCareCategoryID == afterCareCategoryID)
                    .OrderBy(x => x.Sortorder)
                    .ToList();
                CacheService.Instance.Insert(cacheKey, afterCareTypes, profile: "CacheLong");
            }

            return afterCareTypes;
        }

        public static Dictionary<int, string> GetAllNamesById(IUnitOfWork uow)
        {
            return uow.AfterCareTypeRepository.GetAll().ToDictionary(x => x.AfterCareTypeID, x => x.Name);
        }

        public static bool IsPalliatief(IUnitOfWork uow, int transferID)
        {
            var flowwebfields = FlowFieldValueBL.GetByTransferIDAndFieldIDs(uow, transferID, new int[] { FlowFieldConstants.ID_AfterCare, FlowFieldConstants.ID_AfterCareDefinitive, FlowFieldConstants.ID_PrognoseVerwachteOntwikkeling });
            return IsPalliatief(flowwebfields);
        }

        public static bool IsPalliatief(IEnumerable<IFlowFieldValue> flowFieldValues)
        {

            var palliativeIDs = new[] { (int)afterCareTypes.ELVPalliatief, (int)afterCareTypes.Hospice,
                (int)afterCareTypes.PalliatieveZorgThuiszorg, (int)afterCareTypes.PalliatieveZorgIntramuraal };

            bool parsedOK;

            var aftercareValue = flowFieldValues.GetFlowFieldValue(FlowFieldConstants.ID_AfterCareDefinitive);
            if (int.TryParse(aftercareValue, out int palliativeIntValue))
            {
                parsedOK = true;
            }
            else
            {
                aftercareValue = flowFieldValues.GetFlowFieldValue(FlowFieldConstants.ID_AfterCare);
                parsedOK = int.TryParse(aftercareValue, out palliativeIntValue);
            }

            if (parsedOK && palliativeIDs.Contains(palliativeIntValue))
            {
                return true;
            }

            var prognoseVerwachteOntwikkeling = flowFieldValues.GetFlowFieldValue(FlowFieldConstants.ID_PrognoseVerwachteOntwikkeling);
            return prognoseVerwachteOntwikkeling == FlowFieldConstants.Value_PrognoseVerwachteOntwikkeling_ExpectedLifeLessThan3Months ||
                   prognoseVerwachteOntwikkeling == FlowFieldConstants.Value_PrognoseVerwachteOntwikkeling_ExpectedLifeLessThan1Year;
        }

        public static IQueryable<AfterCareType> GetAfterCareTypeDescriptions(IUnitOfWork uow, int organizationTypeID, int afterCareTileGroupID)
        {
            var orgTypeHospital = (int) OrganizationTypeID.Hospital;
            if (organizationTypeID == orgTypeHospital || afterCareTileGroupID == (int)AfterCareTileGroupID.Ziekenhuis)
            {
                return uow.AfterCareTypeRepository.GetAll().Where(x => x.OrganizationTypeID == orgTypeHospital && !x.Inactive).OrderBy(x => x.Name);
            }

            var groups = AfterCareGroupBL.GetAll(uow).Where(acg => afterCareTileGroupID == -1 || acg.AfterCareTileGroupID == afterCareTileGroupID).Select(acg => acg.AfterCareGroupID).ToList();
            return uow.AfterCareTypeRepository.GetAll(). Where(act => groups.Contains((int)act.AfterCareGroupID)).OrderBy(x => x.Name);
        }

        public static IQueryable<AfterCareType> GetAfterCareTypeDescriptionsByCategoryIDs(IUnitOfWork uow, int[] categoryIDs)
        {
            return GetByAfterCareCategoryIDs(uow, categoryIDs);
        }

        public static IQueryable<int> GetAfterCareTypeIDsByCategoryIDs(IUnitOfWork uow, int[] categoryIDs)
        {
            return GetByAfterCareCategoryIDs(uow, categoryIDs).Select(act => act.AfterCareTypeID);
        }

        public static IQueryable<AfterCareType> GetByAfterCareCategoryIDs(IUnitOfWork uow, int[] afterCareCategoryIDs)
        {
            return uow.AfterCareTypeRepository.GetAll().Where(act => act.AfterCareGroupID != null && afterCareCategoryIDs.Contains((int)act.AfterCareCategoryID));
        }
    }
}
