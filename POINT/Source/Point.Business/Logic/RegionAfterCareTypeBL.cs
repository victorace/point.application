﻿using Point.Database.Models;
using System.Collections.Generic;
using System.Linq;
using Point.Database.Repository;

namespace Point.Business.Logic
{
    public class RegionAfterCareTypeBL
    {
        private static IEnumerable<RegionAfterCareType> _regionAfterCareTypes;

        public static List<RegionAfterCareType> GetAll(IUnitOfWork uow)
        {
            return GetAllForRegionIDs(uow, null).ToList();
        }

        public static IEnumerable<RegionAfterCareType> GetAllForRegionIDs(IUnitOfWork uow, IEnumerable<int> regionIDs)
        {
            if (_regionAfterCareTypes == null || !_regionAfterCareTypes.Any())
            {
                _regionAfterCareTypes = uow.RegionAfterCareTypeRepository.GetAll().ToList();
            }

            if (regionIDs != null && regionIDs.Any())
            {
                return _regionAfterCareTypes.Where(x => regionIDs.Contains(x.RegionID)).ToList();
            }
            return _regionAfterCareTypes;
        }
    }
}
