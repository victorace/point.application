﻿using Point.Database.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Point.Business.Logic
{
    public class FunctionRoleBL
    {
        public static void DeleteByEmployeeID(IUnitOfWork uow, int employeeid)
        {
            uow.FunctionRoleRepository.Delete(it => it.EmployeeID == employeeid);
        }
    }
}
