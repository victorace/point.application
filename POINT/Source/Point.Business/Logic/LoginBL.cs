﻿using Point.Business.BusinessObjects;
using Point.Business.Helper;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Extensions;
using System;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace Point.Business.Logic
{
    public static class LoginBL
    {
        public const string paramDepartmentReference1 = "departmentreference1";   // Used by common 'koppeling'
        private const string paramDepartmentReference = "departmentReference";     // Used internally 
        private const string paramDepartmentIDReference = "departmentIDReference"; // Used internally storing the departmentID
        private const string paramIsSSO = "isSSO"; //Used internally

        public static string GetClientIP(HttpRequestBase request)
        {
            return request.UserHostAddress;
        }

        public static string GetSSOUri(HttpRequestBase request)
        {
            return request.Url.AbsoluteUri;
        }

        public static bool IsSSORequest(HttpRequestBase request)
        {
            if (request.Params == null) return false;

            bool issso = false;
            
            string returnurl = request.Params.GetValueOrDefault<string>("ReturnUrl", "");
            if (!String.IsNullOrEmpty(returnurl) && returnurl.Contains("!") && !returnurl.Contains("&"))
            {
                var returnurlquerystring = HttpUtility.ParseQueryString(returnurl.Substring(Math.Max(0, returnurl.IndexOf("?"))).Replace("!", "&"));
                issso = (returnurlquerystring.ContainsKey("Sign") && returnurlquerystring.GetValueOrDefault<string>("Sign", "") != "");
            }
            else if (request.HttpMethod == "POST" || request.HttpMethod == "GET")
            {
                issso = (request.Params.ContainsKey("Sign") && request.Params.GetValueOrDefault<string>("Sign", "") != "");
            }

            return issso;
        }

        public static bool IsRadiusRequest(LoginViewModel loginViewModel)
        {
            if (loginViewModel == null || String.IsNullOrEmpty(loginViewModel.Username))
            {
                return false;
            }
            return loginViewModel.Username.EndsWith("@vumc.nl", StringComparison.InvariantCultureIgnoreCase);
        }

        public static string CheckOrganization(PointUserInfo pointUserInfo)
        {
            string check = "";
            if(pointUserInfo.Department == null || pointUserInfo.Organization == null)
            {
                return "U kunt niet worden aangemeld want u heeft geen gekoppelde organisatie/afdeling.";
            }

            if(pointUserInfo.Department.Inactive == true || pointUserInfo.Department.Location.Inactive == true || pointUserInfo.Organization.Inactive == true)
            {
                return "U kunt niet worden aangemeld want uw afdeling, locatie of organisatie is niet actief.";
            }

            return check;
        }

        public static string CheckPatExternalReference(HttpRequestBase request)
        {
            string check = "";
            if (ExternalPatientHelper.HavePatExternalReferenceFromRequest(request) && String.IsNullOrWhiteSpace(ExternalPatientHelper.ReadPatExternalReferenceFromRequest(request)))
            {
                check = "Let op: er is geen patient geselecteerd in het ZIS. Ga svp terug naar het ZIS om eerst een patient te selecteren.";
            }
            return check;
        }

        public static void CheckAndSetIsSSO(HttpRequestBase request)
        {
            if(IsSSORequest(request))
            {
                //Save for entire session to limit ability to login
                HttpContext.Current.Session[paramIsSSO] = true;
            }
        }

        public static bool GetIsSSO()
        {
            return (bool?)HttpContext.Current.Session[paramIsSSO] == true;
        }

        public static void ClearIsSSO()
        {
            HttpContext.Current.Session[paramIsSSO] = null;
        }

        public static string GetAndSetPatExternalReference(HttpRequestBase request)
        {
            string patexternalreference = "";
            var issso = IsSSORequest(request);
            if (issso)
            {
                var ssologinbo = new SSOLoginBO().FromRequest(request);
                patexternalreference = ssologinbo.PatExternalReference;
                ExternalPatientHelper.SetIsAuth(ssologinbo.IsAuth);
            }

            if (String.IsNullOrEmpty(patexternalreference))
            {
                patexternalreference = ExternalPatientHelper.ReadPatExternalReferenceFromRequest(request, issso);
            }

            if (!String.IsNullOrEmpty(patexternalreference))
            {
                ExternalPatientHelper.SetPatExternalReference(patexternalreference);
            }

            return patexternalreference;
        }

        public static string GetAndSetPatExternalReferenceBSN(HttpRequestBase request)
        {
            string patexternalreferencebsn = "";
            var issso = IsSSORequest(request);
            if (issso)
            {
                var ssologinbo = new SSOLoginBO().FromRequest(request);
                patexternalreferencebsn = ssologinbo.PatExternalReferenceBSN;
                ExternalPatientHelper.SetIsAuth(ssologinbo.IsAuth);
            }

            if (String.IsNullOrEmpty(patexternalreferencebsn))
            {
                patexternalreferencebsn = ExternalPatientHelper.ReadPatExternalReferenceBSNFromRequest(request, issso);
            }

            if (!String.IsNullOrEmpty(patexternalreferencebsn))
            {
                ExternalPatientHelper.SetPatExternalReferenceBSN(patexternalreferencebsn);
            }

            return patexternalreferencebsn;
        }

        public static int? GetDepartmentIDReference()
        {
            return (int?)HttpContext.Current.Session[paramDepartmentIDReference] ?? (int?)null;
        }

        public static void SetDepartmentReference(IUnitOfWork uow, HttpRequestBase request, PointUserInfo pointUserInfo = null)
        {
            string departmentreference = "";
            var issso = IsSSORequest(request);
            if (issso)
            {
                var ssologinbo = new SSOLoginBO().FromRequest(request);
                departmentreference = ssologinbo.DepartmentReference;
            }
            if (String.IsNullOrEmpty(departmentreference))
            {
                departmentreference = request.Params.GetValueOrDefault<string>(paramDepartmentReference1, "");
            }
            if (String.IsNullOrEmpty(departmentreference))
            {
                departmentreference = request.Params.GetValueOrDefault<string>(paramDepartmentReference, "");
            }

            int departmentidreference = -1;
            if (!String.IsNullOrEmpty(departmentreference) && pointUserInfo != null)
            {
                departmentreference = HttpUtility.HtmlDecode(departmentreference);
                HttpContext.Current.Session[paramDepartmentReference] = departmentreference;

                var departments = DepartmentBL.GetLinkedDepartmentsByRoleForSearch(uow, pointUserInfo);
                if (departments != null)
                {
                    var department = departments.FirstOrDefault(dep => dep.Name.Equals(departmentreference, StringComparison.InvariantCultureIgnoreCase) && !(bool)dep.Inactive);
                    if (department != null)
                    {
                        departmentidreference = department.DepartmentID;
                    }
                }
            }

            if (departmentidreference == -1)
            {
                departmentidreference = request.Params.GetValueOrDefault<int>(paramDepartmentIDReference, -1);
            }

            if (departmentidreference > -1)
            {
                HttpContext.Current.Session[paramDepartmentIDReference] = departmentidreference;
            }
        }

        public static string CheckIP(PointUserInfo pointUserInfo, string clientip)
        {
            var check = "";
            if (pointUserInfo.Location == null || !pointUserInfo.Location.IPCheck.ConvertTo<bool>() || string.IsNullOrEmpty(pointUserInfo.Location.IPAddress))
            {
                return check;
            }
            if (!IPAddressHelper.IsIPAddressValid(clientip, pointUserInfo.Location.IPAddress))
            {
                check = $"U kan niet worden aangemeld. U kunt zich alleen aanmelden vanuit uw werklocatie (uw huidige locatie: {clientip}). Neem s.v.p contact op met uw regiobeheerder.";
            }
            return check;
        }

        public static string CheckLockedOut(MembershipUser user)
        {
            string check = "";
            if (user.IsLockedOut)
            {
                check = "Uw account is uitgeschakeld doordat het maximum aantal incorrecte aanmeldpogingen is bereikt of doordat uw account is vergrendeld door de POINT beheerder binnen uw organisatie of regiobeheerder. Neem contact op met de beheerder om uw account vrij te geven of gebruik het <u>Wachtwoord vergeten?</u> scherm.";
            }
            return check;
        }
    }
}
