﻿using System.Collections.Generic;
using System.Linq;
using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;

namespace Point.Business.Logic
{
    public class FormTypeRegionBL
    {
        public static List<FormTypeRegion> GetFormTypeRegionsByRegionIDAndFlowDefinitionID(IUnitOfWork uow, int regionID, FlowDefinitionID flowDefinitionID)
        {            
            return uow.FormTypeRegionRepository.Get(ftr => ftr.RegionID == regionID && ftr.FlowDefinitionID == flowDefinitionID).ToList();
        }

        public static void Save(IUnitOfWork uow, RegionViewModel viewmodel, LogEntry logEntry)
        {
            var newformtypeids = viewmodel.FormTypesForRegion.Where(ftr => ftr.IsSelected).Select(it => it.FormTypeID).ToList();

            var oldFormTypesRegion = GetFormTypeRegionsByRegionIDAndFlowDefinitionID(uow, viewmodel.RegionID, viewmodel.DefaultFlowDefinitionID).ToList();
            var oldformtypeids = oldFormTypesRegion.Select(it => it.FormTypeID);

            var toDelete = oldFormTypesRegion.Where(ftr => !newformtypeids.Contains(ftr.FormTypeID)).ToList();
            var toAdd = newformtypeids.Where(nft => !oldformtypeids.Contains(nft)).ToList();

            foreach (var itemtodelete in toDelete)
            {
                itemtodelete.ModifiedTimeStamp = logEntry.Timestamp;

                uow.FormTypeRegionRepository.Delete(itemtodelete);
            }

            foreach (var formtypeidtoadd in toAdd)
            {
                var formtyperegion = new FormTypeRegion()
                {
                    RegionID = viewmodel.RegionID,
                    FlowDefinitionID = viewmodel.DefaultFlowDefinitionID,
                    FormTypeID = formtypeidtoadd,
                    ModifiedTimeStamp = logEntry.Timestamp
                };

                uow.FormTypeRegionRepository.Insert(formtyperegion);
            }
        }
    }
}
