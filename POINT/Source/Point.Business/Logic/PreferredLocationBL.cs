﻿using System.Collections.Generic;
using System.Linq;
using Point.Database.Models;
using Point.Database.Repository;
using Point.Infrastructure.Caching;

namespace Point.Business.Logic
{
    public class PreferredLocationBL
    {
        private const string cachePrefix = "PreferredLocation_Location_{0}";

        public static IEnumerable<PreferredLocation> GetByLocationID(IUnitOfWork uow, int locationID, bool usecache = true)
        {
            string cachestring = string.Format(cachePrefix, locationID);
            List<PreferredLocation> preferredOrganizations = null;

            if (usecache)
            {
                preferredOrganizations = CacheService.Instance.Get<List<PreferredLocation>>(cachestring);
            }

            if (preferredOrganizations == null)
            {
                preferredOrganizations = uow.PreferredLocationRepository.Get(po => po.LocationID == locationID, includeProperties: "Location.Organization").ToList();
                CacheService.Instance.Insert(cachestring, preferredOrganizations);
            }
            return preferredOrganizations;
        }

        public static void Delete(IUnitOfWork uow, int locationID, int preferredLocationID)
        {
            uow.PreferredLocationRepository.Delete(it => it.LocationID == locationID && it.PreferredLocationID == preferredLocationID);
            uow.Save();
            ClearCache(locationID);
        }

        public static PreferredLocation Insert(IUnitOfWork uow, int locationID, int preferredLocationID)
        {
            var preferredlocations = GetByLocationID(uow, locationID, false);
            var preferredlocation = preferredlocations.FirstOrDefault(pl => pl.PreferredLocationID == preferredLocationID);
            if (preferredlocation == null)
            {
                preferredlocation = new PreferredLocation { LocationID = locationID, PreferredLocationID = preferredLocationID };
                uow.PreferredLocationRepository.Insert(preferredlocation);
                uow.Save();
                ClearCache(locationID);
            }
            return preferredlocation;
        }

        private static void ClearCache(object locationID)
        {
            string cachestring = string.Format(cachePrefix, locationID);
            CacheService.Instance.Remove(cachestring);
        }
    }
}
