﻿using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Exceptions;
using System;

namespace Point.Business.Logic
{
    public class LogEntryBL
    {
        public static LogEntry Create(FlowFormType screenID, PointUserInfo pointUserInfo)
        {
            return Create(screenID, pointUserInfo.Employee.EmployeeID);
        }

        public static LogEntry Create(FlowFormType screenID, int employeeid)
        {
            return new LogEntry()
            {
                ScreenID = (int)screenID,
                Screenname = screenID.ToString(),
                EmployeeID = employeeid,
                Timestamp = DateTime.Now
            };
        }

        public static LogEntry Create(IUnitOfWork uow, int formtypeid, PointUserInfo pointuserinfo)
        {
            var formtype = FormTypeBL.GetByFormTypeID(uow, formtypeid);
            if (formtype == null)
            {
                throw new PointJustLogException("LogEntry creation failed, no FormType");
            }

            return Create(formtype, pointuserinfo);
        }

        public static LogEntry Create(FormType formtype, PointUserInfo pointUserInfo)
        {
            if (formtype == null)
            {
                throw new ApplicationException("LogEntry creation failed, no FormType");
            }

            return new LogEntry()
            {
                ScreenID = formtype.FormTypeID,
                Screenname = formtype.Name,
                EmployeeID = pointUserInfo.Employee.EmployeeID,
                Timestamp = DateTime.Now
            };
        }
    }
}
