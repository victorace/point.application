﻿using Point.Database.Extensions;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Point.Business.Logic
{
    public static class TransferAttachmentViewBL
    {
        public static IEnumerable<TransferAttachmentViewModel> GetByFrequencyID(IUnitOfWork uow, int transferid, int frequencyID, PointUserInfo pointuserinfo, bool readOnly = false)
        {
            var attachments = uow.FrequencyTransferAttachmentRepository.Get(fta => fta.FrequencyID == frequencyID && !fta.TransferAttachment.Deleted)
                .OrderByDescending(fta => fta.TransferAttachment.UploadDate)
                .ToList();

            var closedon = getEndPhaseRealizedDate(uow, transferid);

            var vm = attachments.Select(fa => MapToTransferAttachmentViewModel(fa.TransferAttachment, closedon, false, uow)).ToList();

            vm = handleOwner(uow, vm).ToList();
            if (!readOnly)
            {
                EditableBL.AssignRights(uow, vm, transferid, pointuserinfo);
            }

            return vm;
        }

        public static IEnumerable<TransferAttachmentViewModel> GetByEmployeeID(IUnitOfWork uow, int employeeId, AttachmentTypeID attachmentType)
        {
            return uow.TransferAttachmentRepository.Get(ta => ta.AttachmentTypeID == attachmentType && !ta.Deleted && ta.EmployeeID == employeeId).ToList()
                    .OrderByDescending(ta => ta.AttachmentID).Take(100)
                    .Select(ta => MapToTransferAttachmentViewModel(ta, null, true, uow));
        }

        public static IEnumerable<TransferAttachmentViewModel> GetByTransferIDAndSource(IUnitOfWork uow, int transferid, AttachmentSource attachmentsource, PointUserInfo pointuserinfo, bool readOnly = false, bool skipPdf = false)
        {
            var closedon = getEndPhaseRealizedDate(uow, transferid);

            var vm = uow.TransferAttachmentRepository.Get(ta => ta.TransferID == transferid &&
                ta.AttachmentSource == attachmentsource && !ta.Deleted &&
                (!skipPdf || ta.GenericFile != null && ta.GenericFile.ContentType != "application/pdf"))
                .OrderByDescending(ta => ta.UploadDate)
                .ToList()
                .Select(ta => MapToTransferAttachmentViewModel(ta, closedon, false, uow)).ToList();

            vm = handleOwner(uow, vm).ToList();
            if (!readOnly)
            {
                EditableBL.AssignRights(uow, vm, transferid, pointuserinfo);
            }

            return vm;
        }

        public static TransferAttachmentViewModel GetByAttachmentID(IUnitOfWork uow, int attachmentid)
        {
            DateTime? closedon = null;

            var transferattachment = uow.TransferAttachmentRepository.Get(ta => ta.AttachmentID == attachmentid).FirstOrDefault();

            if (transferattachment == null)
            {
                transferattachment = new TransferAttachment();
            }
            else
            {
                if (transferattachment.TransferID.HasValue)
                {
                    closedon = getEndPhaseRealizedDate(uow, transferattachment.TransferID.Value);
                }
            }

            var vm = MapToTransferAttachmentViewModel(transferattachment, closedon, true, uow);

            var frequency = FrequencyTransferAttachmentBL.GetByAttachmentID(uow, attachmentid);
            if (frequency != null)
            {
                vm.FrequencyID = frequency.FrequencyID;
            }

            return vm;
        }

        private static TransferAttachmentViewModel MapToTransferAttachmentViewModel(TransferAttachment attachment, DateTime? closedOn, bool isEditable, IUnitOfWork uow)
        {
            return new TransferAttachmentViewModel
            {
                AttachmentID = attachment.AttachmentID,
                Name = attachment.Name,
                Description = attachment.Description,
                AttachmentSource = attachment.AttachmentSource,
                AttachmentTypeID = attachment.AttachmentTypeID,
                ShowInAttachmentList = attachment.ShowInAttachmentList,
                DateTime = attachment.UploadDate,
                FileName = attachment.GenericFile?.FileName,
                EmployeeID = attachment.EmployeeID.GetValueOrDefault(),
                TransferID = attachment.TransferID.GetValueOrDefault(),
                IsEditable = isEditable,
                AttachedAfterClose = closedOn.HasValue && attachment.UploadDate > closedOn.Value,
                RelatedAttachmentID = attachment.RelatedAttachmentID ,
                RelatedAttachmentOwner = GetRelatedOwnerData(uow, attachment.RelatedAttachmentID)
            };
        }

        private static IEnumerable<TransferAttachmentViewModel> handleOwner(IUnitOfWork uow, List<TransferAttachmentViewModel> flowattachments)
        {
            var employeeOrganization = new Dictionary<int, Organization>();
            var employees = new Dictionary<int, Employee>();

            foreach (var attachment in flowattachments)
            {
                var employeeID = attachment.EmployeeID;

                Organization organization;
                if (employeeOrganization.ContainsKey(employeeID))
                    organization = employeeOrganization[employeeID];
                else
                {
                    organization = OrganizationBL.GetByEmployeeID(uow, employeeID);
                    if (organization != null)
                        employeeOrganization.Add(employeeID, organization);
                }
                if (organization != null)
                {
                    attachment.OrganizationName = organization.Name;
                }

                Employee employee;
                if (employees.ContainsKey(employeeID))
                {
                    employee = employees[employeeID];
                }
                else
                {
                    employee = EmployeeBL.GetByID(uow, employeeID);
                    if (employee != null)
                        employees.Add(employeeID, employee);
                }
                if (employee != null)
                {
                    attachment.EmployeeName = employee.FullName();
                }
            }

            return flowattachments;
        }

        private static string GetRelatedOwnerData(IUnitOfWork uow, int? relatedAttachmentID)
        {
            var relatedData = "";
            if (relatedAttachmentID.HasValue)
            {
                var relatedAttachment = TransferAttachmentBL.GetByID(uow, relatedAttachmentID.Value);
                if (relatedAttachment != null && relatedAttachment.Employee != null)
                {
                    var relatedOrganization = OrganizationBL.GetByEmployeeID(uow, relatedAttachment.EmployeeID.Value);
                    relatedData = string.Format("Origineel toegevoed door: {0}.<br>Datum: {1}<br>Organisatie: {2}", relatedAttachment.Employee.FullName(),relatedAttachment.UploadDate, relatedOrganization?.Name);
                }
            }
            return relatedData;
        }
        private static DateTime? getEndPhaseRealizedDate(IUnitOfWork uow, int transferid)
        {
            var flowinstance = FlowInstanceBL.GetByTransferID(uow, transferid);
            if (flowinstance != null)
            {
                var endphase = PhaseInstanceBL.GetEndPhase(uow, flowinstance.FlowInstanceID);
                if (endphase?.Status == (int)Status.Done)
                {
                    return endphase.RealizedDate;
                }
            }
            return null;
        }
    }
}
