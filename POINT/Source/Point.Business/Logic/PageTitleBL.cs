﻿using System;
using Point.Database.Models;

namespace Point.Business.Logic
{
    public class PageTitleBL
    {
        private static string prefix = "Point";
        private static string defaulttitle = "Verzorgde overdracht";

        public static string GetTitleFromFormType(FormType formtype)
        {
            if (formtype != null)
            {
                return String.Format("{0} - {1}", prefix, formtype.Name);
            }

            return null;
        }

        public static string GetTitleFromTitle(string shorttitle)
        {
            if (!String.IsNullOrEmpty(shorttitle))
            {
                return String.Format("{0} - {1}", prefix, shorttitle);
            }

            return null;
        }

        public static string GetTitle(string pageTitle, string pageHeader)
        {
            return GetTitle(pageTitle, pageHeader, null);
        }

        public static string GetTitle(string pageTitle, string pageHeader, FormType formtype)
        {
            var title = PageTitleBL.GetTitleFromTitle(pageTitle);
            if (String.IsNullOrEmpty(title) && formtype != null)
            {
                title = PageTitleBL.GetTitleFromFormType(formtype);
            }

            //Set to header if nothing else worked
            if (String.IsNullOrEmpty(title) && !String.IsNullOrEmpty(pageHeader))
            {
                title = PageTitleBL.GetTitleFromTitle(pageHeader);
            }

            //Else put to default
            if (String.IsNullOrEmpty(title))
            {
                title = PageTitleBL.GetTitleFromTitle(defaulttitle);
            }

            return title;

        }
    }
}
