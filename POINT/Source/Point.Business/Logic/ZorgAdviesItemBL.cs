﻿using System.Collections.Generic;
using System.Linq;
using Point.Database.Models;
using Point.Database.Repository;

namespace Point.Business.Logic
{
    public class ZorgAdviesItemBL
    {
        public static IEnumerable<ZorgAdviesItem> GetAll(IUnitOfWork uow)
        {
            return uow.ZorgAdviesItemRepository.GetAll().OrderBy(zpi => zpi.ZorgAdviesItemGroup.OrderNumber).ThenBy(zpi => zpi.OrderNumber);
        }
    }
}
