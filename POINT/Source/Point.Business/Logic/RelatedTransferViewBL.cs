﻿using System;
using System.Linq;
using Point.Business.Helper;
using Point.Database.Extensions;
using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Extensions;

namespace Point.Business.Logic
{
    public class RelatedTransferViewBL
    {
        public static RelatedTransferViewModel FromModel(IUnitOfWork uow, FlowInstance flowInstance, int organizationID)
        {
            var relatedtransferviewmodel = new RelatedTransferViewModel();

            var recievingdepartment = OrganizationHelper.GetRecievingDepartment(uow, flowInstance.FlowInstanceID);
            var sendingdepartment = OrganizationHelper.GetSendingDepartment(uow, flowInstance.FlowInstanceID);

            if (recievingdepartment == null || sendingdepartment == null)
            {
                return relatedtransferviewmodel;
            }

            var client = ClientBL.GetByTransferID(uow, flowInstance.TransferID);

            relatedtransferviewmodel.TransferID = flowInstance.TransferID;
            relatedtransferviewmodel.ClientName = client.FullName(flowInstance, organizationID);
            relatedtransferviewmodel.CivilServiceNumber = ClientBL.ShowAnonymous(flowInstance, organizationID) ? "Anoniem" : client.CivilServiceNumber;
            relatedtransferviewmodel.CreateDate = flowInstance.CreatedDate;
            relatedtransferviewmodel.TypeFlow = flowInstance.FlowDefinition.Name;

            var flowfieldvalues = FlowFieldValueBL.GetByFlowInstanceIDWithFlowField(uow, flowInstance.FlowInstanceID);

            var transferdate = flowfieldvalues.Where(ffv => ffv.FlowFieldID == FlowFieldConstants.ID_TransferDate && !String.IsNullOrEmpty(ffv.Value)).OrderByDescending(ffv => ffv.FlowFieldID).FirstOrDefault();
            if (transferdate != null)
            {
                relatedtransferviewmodel.TransferDate = DateTime.Parse(transferdate.Value);
            }


            if (sendingdepartment.Location.OrganizationID == organizationID)
            {
                if (recievingdepartment.Location.Organization.OrganizationTypeID == (int)OrganizationTypeID.HealthCareProvider)
                {
                    relatedtransferviewmodel.PreferredVVT = recievingdepartment.FullName();
                    relatedtransferviewmodel.PreferredVVTID = recievingdepartment.DepartmentID;
                }
            }
            if (recievingdepartment.Location.OrganizationID == organizationID)
            {
                if (sendingdepartment.Location.Organization.OrganizationTypeID == (int)OrganizationTypeID.HealthCareProvider)
                {
                    relatedtransferviewmodel.SendingVVT = sendingdepartment.FullName();
                    relatedtransferviewmodel.SendingVVTID = sendingdepartment.DepartmentID;
                }
            }

            return relatedtransferviewmodel;
        }

        public static RelatedTransferViewModel FromFlowInstanceSearchModel(IUnitOfWork uow, FlowInstanceSearchValues flowInstancevalues)
        {
            var relatedtransferviewmodel = new RelatedTransferViewModel();

            relatedtransferviewmodel.ClientName = flowInstancevalues.ClientFullname;
            relatedtransferviewmodel.TransferID = flowInstancevalues.TransferID;
            relatedtransferviewmodel.CreateDate = flowInstancevalues.TransferCreatedDate?? DateTime.MinValue;
            relatedtransferviewmodel.TypeFlow = flowInstancevalues.FlowDefinitionName;
            relatedtransferviewmodel.Hospital = flowInstancevalues.OrganizationName;
            relatedtransferviewmodel.SendingVVT = flowInstancevalues.DesiredHealthCareProvider;

            return relatedtransferviewmodel;
        }
    }
}