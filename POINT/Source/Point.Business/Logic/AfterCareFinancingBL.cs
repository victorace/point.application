﻿using System.Collections.Generic;
using System.Linq;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;

namespace Point.Business.Logic
{
    public class AfterCareFinancingBL
    {
        public static AfterCareFinancing GetById(IUnitOfWork uow, int id)
        {
            return uow.AfterCareFinancingRepository.GetByID(id);
        }

        public static IEnumerable<AfterCareFinancing> GetByAfterCareTypeID(IUnitOfWork uow, int aftercaretypeid)
        {
            return uow.AfterCareFinancingRepository.Get(it => it.AfterCareTypeAfterCareFinancing.Any(actaf => actaf.AfterCareTypeID == aftercaretypeid));
        }
    }
}
