﻿using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Extensions;

namespace Point.Business.Logic
{
    public class TemplateViewBL
    {
        public static TemplateViewModel Get(IUnitOfWork uow, TemplateTypeID templatetypeid, int? organizationid)
        {
            var templatetypes = TemplateTypeBL.GetAll(uow);
            var organizationtypeid = TemplateDefaultBL.GetByTemplateTypeID(uow, (int)templatetypeid)?.OrganizationTypeID;
            var organizations = organizationtypeid == null ? OrganizationBL.GetActive(uow) : OrganizationBL.GetActiveByOrganizationTypeID(uow, organizationtypeid.Value);

            var templatedefault = uow.TemplateDefaultRepository.FirstOrDefault(td => td.TemplateTypeID == (int)templatetypeid);
            var template = uow.TemplateRepository.FirstOrDefault(t => t.TemplateTypeID == (int)templatetypeid && t.OrganizationID == organizationid);
            var templateViewModel = new TemplateViewModel()
            {
                TemplateTypes = templatetypes,
                TemplateTypeID = templatetypeid,
                TemplateID = template == null ? 0 : template.TemplateID,
                TemplateDefaultID = templatedefault.TemplateDefaultID,
                OrganizationTypeID = templatedefault.OrganizationTypeID,
                Organizations = organizations,
                OrganizationID = organizationid,
                IsDefault = template == null,
                TemplateText = template == null ? templatedefault.TemplateDefaultText : template?.TemplateText,
                CanBeOrganizationSpecific = templatedefault.TemplateType.CanBeOrganizationSpecific
            };

            return templateViewModel;
        }
    }
}
