﻿using System.Collections.Generic;
using System.Linq;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;

namespace Point.Business.Logic
{
    public class AfterCareGroupBL
    {
        public static IEnumerable<AfterCareGroup> GetAll(IUnitOfWork uow)
        {
            return uow.AfterCareGroupRepository.GetAll();
        }
    }
}
