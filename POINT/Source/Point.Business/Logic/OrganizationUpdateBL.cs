﻿using Point.Business.Helper;
using Point.Database.Context;
using Point.Database.Models;
using Point.Database.Repository;
using Point.Infrastructure.Extensions;
using Point.Models.Enums;
using Point.ServiceBus.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Point.Business.Logic
{
    public class OrganizationUpdateBL
    {
        public static void CalculateAllAsync()
        {
            ServiceBusBL.SendMessage("OrganizationSearch", "UpdateAll", new UpdateOrganizationSearch());
        }

        //public static void CalculateForRegionAsync(int regionID)
        //{
        //    // no service bus yet for OrganizationSearch RegionID

        //    if (CapManAPIDataHelper.HaveCapManUrl())
        //    {
        //        Task.Run(async () => { await CapManAPIDataHelper.UpdateRegionInCapMan(regionID); });
        //    }
        //}

        public static void CalculateForOrganizationAsync(int organizationid)
        {
            ServiceBusBL.SendMessage("OrganizationSearch", "UpdateOrganization_" + organizationid, new UpdateOrganizationSearch() { OrganizationID = organizationid });
        }

        //public static void CapManForOrganizationAsync(int organizationid)
        //{
        //    if (CapManAPIDataHelper.HaveCapManUrl())
        //    {
        //        Task.Run(async () => { await CapManAPIDataHelper.UpdateOrganizationInCapMan(organizationid); });
        //    }
        //}

        public static void CalculateForLocationAsync(int locationid)
        {
            ServiceBusBL.SendMessage("OrganizationSearch", "UpdateLocation_" + locationid, new UpdateOrganizationSearch() { LocationID = locationid });
        }

        //public static void CapManForLocationAsync(int locationid)
        //{
        //    if (CapManAPIDataHelper.HaveCapManUrl())
        //    {
        //        Task.Run(async () => { await CapManAPIDataHelper.UpdateLocationInCapMan(locationid); });
        //    }
        //}

        public static void CalculateForDepartmentAsync(int departmentid)
        {
            ServiceBusBL.SendMessage("OrganizationSearch", "UpdateDepartment_" + departmentid, new UpdateOrganizationSearch() { DepartmentID = departmentid });
        }

        //public static void CapManForDepartmentAsync(int departmentid)
        //{
        //    if (CapManAPIDataHelper.HaveCapManUrl())
        //    {
        //        Task.Run(async () => { await CapManAPIDataHelper.UpdateDepartmentInCapMan(departmentid); });
        //    }
        //}


        public static void UpdateReadModelByOrganizationID(UnitOfWork<PointSearchContext> uow, int organizationid)
        {
            deleteAllInactiveByOrganizationID(uow, organizationid);

            var locations = uow.LocationRepository.Get(it => it.OrganizationID == organizationid && !(bool)it.Inactive && !(bool)it.Organization.Inactive && it.Department.Any(dep => !(bool)dep.Inactive));
            foreach (var location in locations)
                UpdateReadModelByLocation(uow, location);

            uow.Save();
        }

        private static void deleteAll(UnitOfWork<PointSearchContext> uow)
        {
            uow.DatabaseContext.Database.ExecuteSqlCommand("delete from OrganizationSearchParticipation");
            uow.DatabaseContext.Database.ExecuteSqlCommand("delete from OrganizationSearchPostalCode");
            uow.DatabaseContext.Database.ExecuteSqlCommand("delete from OrganizationSearchRegion");
            uow.DatabaseContext.Database.ExecuteSqlCommand("delete from OrganizationSearch");
        }

        private static void deleteAllInactiveByDepartmentID(UnitOfWork<PointSearchContext> uow, int departmentid)
        {
            var departmentids = uow.DepartmentRepository.Get(dep => dep.DepartmentID == departmentid && ((bool)dep.Inactive || (bool)dep.Location.Inactive)).Select(dep => dep.DepartmentID).ToArray();
            var items = uow.OrganizationSearchRepository.Get(orgsearch => departmentids.Contains(orgsearch.DepartmentID));
            foreach (var item in items)
                deleteItemFromReadModel(uow, item);
        }

        private static void deleteAllInactiveByLocationID(UnitOfWork<PointSearchContext> uow, int locationid)
        {
            var locationids = uow.LocationRepository.Get(loc => loc.LocationID == locationid && ((bool)loc.Inactive || (bool)loc.Organization.Inactive || loc.Department.All(dep => (bool)dep.Inactive))).Select(loc => loc.LocationID).ToArray();
            var items = uow.OrganizationSearchRepository.Get(orgsearch => locationids.Contains(orgsearch.LocationID));
            foreach (var item in items)
                deleteItemFromReadModel(uow, item);
        }

        private static void deleteAllInactiveByOrganizationID(UnitOfWork<PointSearchContext> uow, int organizationid)
        {
            var locationids = uow.LocationRepository.Get(loc => loc.OrganizationID == organizationid && ((bool)loc.Inactive || (bool)loc.Organization.Inactive || loc.Department.All(dep => (bool)dep.Inactive))).Select(loc => loc.LocationID).ToArray();
            var items = uow.OrganizationSearchRepository.Get(orgsearch => locationids.Contains(orgsearch.LocationID));
            foreach (var item in items)
                deleteItemFromReadModel(uow, item);
        }

        private static void deleteItemFromReadModel(UnitOfWork<PointSearchContext> uow, OrganizationSearch item)
        {
            foreach (var postalcode in item.OrganizationSearchPostalCode.ToList())
                uow.DbContext.Entry(postalcode).State = System.Data.Entity.EntityState.Deleted;

            foreach (var region in item.OrganizationSearchRegion.ToList())
                uow.DbContext.Entry(region).State = System.Data.Entity.EntityState.Deleted;

            foreach (var participation in item.OrganizationSearchParticipation.ToList())
                uow.DbContext.Entry(participation).State = System.Data.Entity.EntityState.Deleted;

            uow.DbContext.Entry(item).State = System.Data.Entity.EntityState.Deleted;
        }

        public static void UpdateReadModelByDepartmentID(UnitOfWork<PointSearchContext> uow, int departmentid)
        {
            var department = uow.DepartmentRepository.GetByID(departmentid);
            if (department == null) return;

            deleteAllInactiveByDepartmentID(uow, departmentid);

            if (department.Inactive != true)
            {
                UpdateReadModelByDepartment(uow, department);
            }

            uow.Save();
        }

        public static void UpdateReadModelByLocationID(UnitOfWork<PointSearchContext> uow, int locationid)
        {
            var organization = OrganizationBL.GetByLocationID(uow, locationid);
            if (organization == null) return;
            
            deleteAllInactiveByLocationID(uow, locationid);

            var location = uow.LocationRepository.Get(it => it.LocationID == locationid && !(bool)it.Inactive && it.Organization != null && !(bool)it.Organization.Inactive && it.Department.Any(dep => !(bool)dep.Inactive)).FirstOrDefault();
            if(location != null)
                UpdateReadModelByLocation(uow, location);

            uow.Save();
        }

        public static void UpdateCompleteReadModel(UnitOfWork<PointSearchContext> uow)
        {
            deleteAll(uow);
            
            var temporaryOS = uow.OrganizationSearchRepository.Get(includeProperties: "OrganizationSearchRegion,OrganizationSearchPostalCode,OrganizationSearchParticipation").ToList();
            var temporatyFDP = uow.FlowDefinitionParticipationRepository.GetAll().ToList();

            var departments = uow.DepartmentRepository.Get(it => 
                    !(bool)it.Inactive && 
                    it.Location != null && it.Location.Organization != null && !(bool)it.Location.Inactive && 
                    !(bool)it.Location.Organization.Inactive && !OrganizationTypeBL.NoneselectableOrganizationTypeIDs.Contains(it.Location.Organization.OrganizationTypeID), 

                    includeProperties: "Location.Organization,ServiceAreaPostalCode.PostalCode,ServiceAreaPostalCode.ServiceArea.ServiceAreaPostalCode.PostalCode,AfterCareType1.AfterCareCategory"
                ).ToList();

            foreach (var department in departments)
            {
                UpdateReadModelByDepartment(uow, department, temporaryOS, temporatyFDP);
            }

            uow.Save();
        }

        public static void UpdateReadModelByLocation(UnitOfWork<PointSearchContext> uow, Location location)
        {
            foreach (var department in location.Department.Where(dep => !(bool)dep.Inactive).ToList())
            {
                UpdateReadModelByDepartment(uow, department);
            }
        }

        public static void UpdateReadModelByDepartment(UnitOfWork<PointSearchContext> uow, Department department, List<OrganizationSearch> tempOrganizationSearch = null, List<FlowDefinitionParticipation> tempFlowDefinitionParticipation = null)
        {
            OrganizationSearch readmodel = null;
            if(tempOrganizationSearch != null)
            {
                readmodel = tempOrganizationSearch.FirstOrDefault(it => it.DepartmentID == department.DepartmentID);
            } else
            {
                readmodel = OrganizationSearchBL.GetByDepartmentID(uow, department.DepartmentID);
            }
            
            if (readmodel == null)
            {
                readmodel = new OrganizationSearch();
                readmodel.OrganizationID = department.Location.OrganizationID;
                readmodel.LocationID = department.LocationID;

                uow.OrganizationSearchRepository.Insert(readmodel);
                uow.DbContext.Entry(readmodel).State = System.Data.Entity.EntityState.Added;
            } else {
                uow.DbContext.Entry(readmodel).State = System.Data.Entity.EntityState.Modified;
            }

            var organization = department.Location.Organization;
            //var department = location.Department.FirstOrDefault(dep => !(bool)dep.Inactive);

            readmodel.DepartmentID = department.DepartmentID;
            readmodel.DepartmentTypeID = department.DepartmentTypeID;
            readmodel.DepartmentName = department.Name;
            readmodel.LocationName = department.Location.Name;
            readmodel.LocationPostcode = department.Location.PostalCode?.Length > 6 ? department.Location.PostalCode.Substring(0,6) : department.Location.PostalCode;
            readmodel.City = string.Format("{0}", department.Location.City);
            readmodel.OrganizationName = organization.Name;
            readmodel.OrganizationTypeID = organization.OrganizationTypeID;
            readmodel.IsRehabilitationCenter = department.RehabilitationCenter;
            readmodel.IsIntensiveRehabilitationNursingHome = department.IntensiveRehabilitationNursinghome;

            readmodel.AfterCareType = department.AfterCareType1?.Abbreviation;
            readmodel.AfterCareCategory = department.AfterCareType1?.AfterCareCategory?.Abbreviation;

            readmodel.Capacity0 = department.CapacityFunctionality == true ? department.CapacityDepartment : null;
            readmodel.Capacity1 = department.CapacityFunctionality == true ? department.CapacityDepartmentNext : null;
            readmodel.Capacity2 = department.CapacityFunctionality == true ? department.CapacityDepartment3 : null;
            readmodel.Capacity3 = department.CapacityFunctionality == true ? department.CapacityDepartment4 : null;

            foreach (var item in readmodel.OrganizationSearchParticipation.ToList())
            {
                readmodel.OrganizationSearchParticipation.Remove(item);
                uow.DbContext.Entry(item).State = System.Data.Entity.EntityState.Deleted;
            }
            
            foreach (var item in readmodel.OrganizationSearchRegion.ToList())
            {
                readmodel.OrganizationSearchRegion.Remove(item);
                uow.DbContext.Entry(item).State = System.Data.Entity.EntityState.Deleted;
            }

            foreach (var item in readmodel.OrganizationSearchPostalCode.ToList())
            {
                readmodel.OrganizationSearchPostalCode.Remove(item);
                uow.DbContext.Entry(item).State = System.Data.Entity.EntityState.Deleted;
            }


            List<FlowDefinitionParticipation> participations = null;
            if (tempFlowDefinitionParticipation != null)
            {
                participations = tempFlowDefinitionParticipation.Where(it => (it.LocationID == department.LocationID && it.DepartmentID == null) || it.DepartmentID == department.DepartmentID).GroupBy(it => it.FlowDefinitionID).Select(it => it.OrderByDescending(it2 => it2.DepartmentID).FirstOrDefault()).Where(it => it.Participation > Participation.None).ToList();
            }
            else
            {
                participations = FlowDefinitionParticipationBL.GetByLocationIDAndDepartmentID(uow, department.LocationID, department.DepartmentID).Where(it => it.Participation > Participation.None).ToList();
            }

            foreach (var participation in participations)
            {
                var item = new OrganizationSearchParticipation()
                {
                    OrganizationSearch = readmodel,
                    FlowDirection = participation.FlowDirection,
                    FlowDefinitionID = participation.FlowDefinitionID,
                    Participation = participation.Participation
                };

                readmodel.OrganizationSearchParticipation.Add(item);
                uow.DbContext.Entry(item).State = System.Data.Entity.EntityState.Added;
            }

            if(!readmodel.OrganizationSearchParticipation.Any())
            {
                //Delete or cancel entire row if there is no participation)
                if (uow.DbContext.Entry(readmodel).State == System.Data.Entity.EntityState.Added)
                {
                    uow.OrganizationSearchRepository.Delete(readmodel);
                    uow.DbContext.Entry(readmodel).State = System.Data.Entity.EntityState.Detached;
                }

                if (uow.DbContext.Entry(readmodel).State == System.Data.Entity.EntityState.Modified)
                {
                    uow.OrganizationSearchRepository.Delete(readmodel);
                    uow.DbContext.Entry(readmodel).State = System.Data.Entity.EntityState.Deleted;
                }

                return;
            } 

            if (organization.RegionID.HasValue)
            {
                var item = new OrganizationSearchRegion() { OrganizationSearch = readmodel, RegionID = organization.RegionID.Value, RegionName = organization.Region.Name };
                readmodel.OrganizationSearchRegion.Add(item);
                uow.DbContext.Entry(item).State = System.Data.Entity.EntityState.Added;
            }

            foreach (LocationRegion locationregion in department.Location.LocationRegion)
            {
                var item = new OrganizationSearchRegion() { OrganizationSearch = readmodel, RegionID = locationregion.RegionID, RegionName = locationregion.Region.Name };
                readmodel.OrganizationSearchRegion.Add(item);
                uow.DbContext.Entry(item).State = System.Data.Entity.EntityState.Added;
            }

            var departmentserviceeareapostalcodes = department.ServiceAreaPostalCode.ToList();
            if(departmentserviceeareapostalcodes != null && departmentserviceeareapostalcodes.Count() > 0)
            {
                List<PostalCode> postalcodes = departmentserviceeareapostalcodes.Where(sap => sap.PostalCode != null).Select(sapc => sapc.PostalCode).ToList();
                var postalcodesfromarea = departmentserviceeareapostalcodes.Where(sapc => sapc.ServiceArea != null).SelectMany(sapc => sapc.ServiceArea.ServiceAreaPostalCode.Where(sap => sap.PostalCode != null).Select(sap => sap.PostalCode));
                postalcodes.AddRange(postalcodesfromarea);

                foreach (PostalCode postalcode in postalcodes)
                {
                    var item = new OrganizationSearchPostalCode() { OrganizationSearch = readmodel, StartPostalCode = postalcode.StartPostalCode, EndPostalCode = postalcode.EndPostalCode };
                    readmodel.OrganizationSearchPostalCode.Add(item);
                    uow.DbContext.Entry(item).State = System.Data.Entity.EntityState.Added;
                }
            }

            return;
        }
    }
}
