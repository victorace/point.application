﻿using Point.Database.Models;
using Point.Database.Repository;
using Point.Infrastructure.Extensions;
using System.Collections.Generic;

namespace Point.Business.Logic
{
    public class OrganizationSettingBL
    {
        public static string InterfaceCode = "InterfaceCode";
        public static string InterfaceExtraFields = "InterfaceExtraFields";
        public static string ORUHuisartsBericht = "ORUHuisartsBericht";
        public static string LogPointFormService = "LogPointFormService";
        public static string SetPrivacyAfterDays = "SetPrivacyAfterDays";
        public static string SetPrivacyOutgoingMail = "SetPrivacyOutgoingMail";
        public static string AutoCloseTransferAfterDays = "AutoCloseTransferAfterDays";
        public static string SurveyCampaignID = "SurveyCampaignID";
        public static string PatientOverview = "PatientOverview";
        public static string UsesAuth = "UsesAuth";
        public static string HashTypeAPI = "HashTypeAPI";
        
        public static T GetValue<T>(IUnitOfWork uow, int organizationID, string name)
        {
            return GetValue(uow, organizationID, name).ConvertTo<T>();
        }

        public static void SetValue<T>(IUnitOfWork uow, int organizationID, string name, T value, LogEntry logEntry)
        {
            var valuestring = value.ConvertTo<string>();
            if (string.IsNullOrEmpty(valuestring))
            {
                DeleteValue(uow, organizationID, name, logEntry);
                return;
            }

            SetValue(uow, organizationID, name, valuestring, logEntry);
        }

        private static void DeleteValue(IUnitOfWork uow, int organizationID, string name, LogEntry logEntry)
        {
            var setting = uow.OrganizationSettingRepository.FirstOrDefault(s => s.OrganizationID == organizationID && s.Name == name);
            if (setting != null)
            {
                uow.OrganizationSettingRepository.Delete(setting);
                LoggingBL.FillLoggable(uow, setting, logEntry);
            }
        }

        private static void SetValue(IUnitOfWork uow, int organizationID, string name, string value, LogEntry logEntry)
        {
            var setting = uow.OrganizationSettingRepository.FirstOrDefault(s => s.OrganizationID == organizationID && s.Name == name);
            if(setting == null)
            {
                setting = new OrganizationSetting() {
                    Name = name,
                    OrganizationID = organizationID
                };
                uow.OrganizationSettingRepository.Insert(setting);
            }

            setting.Value = value;

            LoggingBL.FillLoggable(uow, setting, logEntry);
        }

        private static string GetValue(IUnitOfWork uow, int organizationid, string name)
        {
            return uow.OrganizationSettingRepository.FirstOrDefault(os => os.OrganizationID == organizationid && os.Name == name)?.Value;
        }

        public static IEnumerable<OrganizationSetting> GetAllByName(IUnitOfWork uow, string name)
        {
            return uow.OrganizationSettingRepository.Get(os => os.Name == name);
        }
    }
}
