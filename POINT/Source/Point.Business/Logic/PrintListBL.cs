﻿using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using System.Collections.Generic;
using System.Linq;

namespace Point.Business.Logic
{
    public class PrintListBL
    {
        public static PrintList GetByTransferID(IUnitOfWork uow, int transferID, PointUserInfo pointUserInfo)
        {
            var printlist = new PrintList
            {
                TransferID = transferID,
                PrintListTypes = new List<PrintListType>()
            };

            // Formuliertypes bepalen
            var formtypes = FormTypeBL.GetByTransferID(uow, transferID).Where(ft => ft.IsPrintable).ToList();

            var zorgAdviesToevoegen = FlowFieldValueBL.GetByTransferIDAndFieldID(uow, transferID, FlowFieldConstants.ID_ZorgAdviesToevoegen).FirstOrDefault();
            if (zorgAdviesToevoegen != null)
            {
                if (zorgAdviesToevoegen.Value.ToLower() == "nee" || zorgAdviesToevoegen.Value.ToLower() == "")
                {
                    formtypes.RemoveAll(ft => ft.FormTypeID == (int)FlowFormType.ZorgAdvies);
                }
            }
            else // Ook als deze nog niet bestaat of is verwijderd
            {
                formtypes.RemoveAll(ft => ft.FormTypeID == (int)FlowFormType.ZorgAdvies);
            }

            var eOverdrachtFormType = FormTypeBL.GetByURL(uow, "FormType/EOverdracht");
            if (eOverdrachtFormType != null && !formtypes.Any(ft => ft.FormTypeID == eOverdrachtFormType.FormTypeID))
            {
                formtypes.Add(eOverdrachtFormType);
            }

            var allformsetversions = FormSetVersionBL.GetByTransferID(uow, transferID).Where(fsv => fsv.PhaseInstance != null && !fsv.PhaseInstance.Cancelled);
            
            // Standaard formulieren
            foreach (var item in formtypes)
            {
                var formsetversions = allformsetversions.Where(it => it.FormTypeID == item.FormTypeID).ToList();

                if (formsetversions.Any())
                {
                    printlist.PrintListTypes.Add(new PrintListType { FormType = item, FormsetVersions = formsetversions });
                }
            }

            var flowinstance = FlowInstanceBL.GetByTransferID(uow, transferID);
            // this hack only for ZH_VVT flows
            if(flowinstance?.FlowDefinition?.FlowTypeID == (int)FlowTypeID.ZH_VVT)
            {
                // Transferdossier groep
                var transferdossierformtypes = new int[] { (int)FlowFormType.InBehandelingTP, (int)FlowFormType.AanvullendeGegevensTPZHVVT, (int)FlowFormType.RouterenDossierZHVVT, (int)FlowFormType.RealizedCare }.ToList();
                bool containstransferdossierforms = allformsetversions.Select(it => it.FormTypeID).Any(ft => transferdossierformtypes.Contains(ft));
                if (containstransferdossierforms)
                {
                    var transferdossierforms = allformsetversions.Where(fsv => transferdossierformtypes.Contains(fsv.FormTypeID)).OrderBy(it => it.FormType.Order).ToList();
                    printlist.PrintListTypes.Add(new PrintListType { CombinedFormName = "Aanvullende gegevens Transferpunt", CombinedForms = transferdossierforms });
                }

                printlist.PrintListTypes.RemoveAll(it => transferdossierformtypes.Contains((it.FormType?.FormTypeID).GetValueOrDefault(0)));
            }

            // Uitzondering voor AdditionalInfoRequestTP memovelden die altijd geprint moeten kunnen worden
            if (allformsetversions.Any(it => it.FormTypeID == (int)FlowFormType.AanvullendeGegevensTPZHVVT) == false)
            {
                if (TransferMemoViewBL.HasAdditionalInfoRequestTPMemos(uow, transferID, pointUserInfo))
                {
                    var formtype = FormTypeBL.GetByFormTypeID(uow, (int)FlowFormType.AanvullendeGegevensTPZHVVT);
                    printlist.PrintListTypes.Add(new PrintListType { FormType = formtype });
                }
            }

            // Communicatiejournaal
            var transfermemos = TransferMemoViewBL.GetCommunicationJournalByTransferID(uow, transferID, pointUserInfo, true);
            bool containsCommunicationJournal = transfermemos.Any();
            if (containsCommunicationJournal)
            {
                var formtype = FormTypeBL.GetByFormTypeID(uow, (int)FlowFormType.CommunicationJournal);
                printlist.PrintListTypes.Add(new PrintListType { FormType = formtype });
            }

            if (flowinstance != null)
            {
                // Handtekening alleen voor CVA / ZHVVT
                if (flowinstance.FlowDefinitionID == FlowDefinitionID.ZH_VVT || flowinstance.FlowDefinitionID == FlowDefinitionID.CVA)
                {
                    var formtype = FormTypeBL.GetByFormTypeID(uow, (int) FlowFormType.Signature);
                    printlist.PrintListTypes.Add(new PrintListType {FormType = formtype});
                }
            }

            // VVT Employees don't get to see the MSVT and UVV forms, they need to print the signed documents
            if (pointUserInfo.Organization.OrganizationTypeID == (int)OrganizationTypeID.HealthCareProvider)
            {
                List<int> ignore = new List<int>
                {
                    (int)FlowFormType.MSVTUitvoeringsverzoek,
                    (int)FlowFormType.MSVTIndicatiestelling
                };

                printlist.PrintListTypes.RemoveAll(x => x.FormType != null && ignore.Contains(x.FormType.FormTypeID));
            }
            // Sort items separate from attachments
            printlist.PrintListTypes = printlist.PrintListTypes.OrderBy(p => p.FormType != null ? p.FormType.Name : p.CombinedFormName).ToList();

            // Bijlages
            var attachments = TransferAttachmentBL.GetByTransferID(uow, transferID).Where(it => it.ShowInAttachmentList).ToList();
            foreach (var attachment in attachments.OrderBy(a => a.Name.TrimStart()))
            {
                printlist.PrintListTypes.Add(new PrintListType { TransferAttachment = attachment });
            }

            return printlist;
        }
    }
}
