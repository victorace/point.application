﻿using System.Collections.Generic;
using System.Linq;
using Point.Database.Models;
using Point.Infrastructure;
using Point.Database.Repository;
using System;
using Point.Models.Enums;

namespace Point.Business.Logic
{
    public class AspNet_RolesBL
    {
        public static IEnumerable<Aspnet_Roles> GetAspnetRolesFromRoleIds(IUnitOfWork uow, IEnumerable<Guid> roleIds)
        {
            return uow.Aspnet_RolesRepository.Get(ar => roleIds.Contains(ar.RoleId));
        }
    }
}
