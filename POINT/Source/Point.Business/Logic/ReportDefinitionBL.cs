﻿using Point.Models.Enums;
using Point.Database.Repository;
using System.Collections.Generic;
using System.Linq;
using ReportDefinition = Point.Database.Models.ReportDefinition;

namespace Point.Business.Logic
{
    public class ReportDefinitionBL
    {
        public static List<ReportDefinition> GetAll(IUnitOfWork uow)
        {
            return uow.ReportDefinitionRepository.Get(r => r.Visible).ToList();
        }

        public static ReportDefinition GetByReportDefinitionID(IUnitOfWork uow, int reportDefinitionID)
        {
            return uow.ReportDefinitionRepository.GetByID(reportDefinitionID);
        }

        public static List<ReportDefinition> GetByOrganizationIDs(IUnitOfWork uow, List<int> organizationids)
        {
            var locations = LocationBL.GetActiveByOrganisationIDs(uow, organizationids);
            var participations = FlowDefinitionParticipationBL.GetByOrganizationIDs(uow, organizationids);

            return uow.ReportDefinitionRepository.Get(rd =>
                    rd.Visible &&
                    rd.ReportDefinitionFlowDefinition.Any(ro => 
                    participations.Any(p => 
                        p.FlowDefinition == ro.FlowDefinition &&
                        (rd.FlowDirection == FlowDirection.Any || p.FlowDirection == FlowDirection.Any || rd.FlowDirection == p.FlowDirection)
                    ))).ToList();
        }

        public static List<ReportDefinition> GetByLocationIDs(IUnitOfWork uow, List<int> locationids)
        {
            var locations = LocationBL.GetActiveByLocationIDs(uow, locationids);
            var participations = FlowDefinitionParticipationBL.GetByLocationIDs(uow, locationids);

            return uow.ReportDefinitionRepository.Get(rd =>
                    rd.Visible &&
                    rd.ReportDefinitionFlowDefinition.Any(ro =>
                    participations.Any(p =>
                        p.FlowDefinition == ro.FlowDefinition &&
                        (rd.FlowDirection == FlowDirection.Any || p.FlowDirection == FlowDirection.Any || rd.FlowDirection == p.FlowDirection)
                    ))).ToList();
        }
    }
}
