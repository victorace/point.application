﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Exceptions;

namespace Point.Business.Logic
{
    public class CommunicationLogBL
    {
        public static CommunicationLog GetByID(IUnitOfWork uow, int communicationLogID)
        {
            return uow.CommunicationLogRepository.GetByID(communicationLogID);
        }

        public static IEnumerable<CommunicationLog> GetByTransferID(IUnitOfWork uow, int transferID)
        {
            return uow.CommunicationLogRepository.Get(cl => cl.TransferID == transferID);
        }

        public static IEnumerable<CommunicationLog> GetByTransferIDAndDate(IUnitOfWork uow, int transferID, DateTime? dateFrom, DateTime? dateTo)
        {
            return uow.CommunicationLogRepository.Get(cl => cl.TransferID == transferID && cl.Timestamp >= (dateFrom ?? DateTime.MinValue) && cl.Timestamp <= (dateTo ?? DateTime.MaxValue));
        }

        public static IEnumerable<CommunicationLog> GetByDate(IUnitOfWork uow, DateTime? dateFrom, DateTime? dateTo)
        {
            return uow.CommunicationLogRepository.Get(cl => cl.Timestamp >= (dateFrom??DateTime.MinValue) && cl.Timestamp <= (dateTo??DateTime.MaxValue));
        }

        public static IEnumerable<CommunicationLog> GetByTransferIDAndMailType(IUnitOfWork uow, int transferID, CommunicationLogType communicationLogType)
        {
            return uow.CommunicationLogRepository.Get(cl => cl.TransferID == transferID && cl.MailType == communicationLogType);
        }

        public static DateTime? GetVoSendDate(IUnitOfWork uow, int transferID)
        {
            return uow.CommunicationLogRepository.Get(cl => cl.TransferID == transferID && cl.MailType == CommunicationLogType.NotifyVO).Min(cl => cl.Timestamp);
        }

        public static IEnumerable<CommunicationLog> GetByOrganizationIDAndMailTypeAndDate(IUnitOfWork uow, int organizationID, CommunicationLogType[] communicationLogTypes, DateTime? date)
        {
            return uow.CommunicationLogRepository.Get(cl => cl.OrganizationID == organizationID && communicationLogTypes.Contains(cl.MailType.Value) && (date == null || cl.Timestamp >= date));
        }

        public static bool HaveLogByTransferIDAndMailTypes(IUnitOfWork uow, int transferID, CommunicationLogType[] communicationLogTypes)
        {
            return uow.CommunicationLogRepository.Any(cl => cl.TransferID == transferID && communicationLogTypes.Contains(cl.MailType.Value));
        }

        public static IEnumerable<CommunicationLog> GetForManagementSearch(IUnitOfWork uow, FunctionRoleAccessModel accessLevel, DateTime? dateFrom, DateTime? dateTo, int? transferID = null, CommunicationLogType? communicationlogtype = null)
        {
            var logquery = uow.CommunicationLogRepository.GetAll();

            switch (accessLevel.MaxLevel)
            {
                case FunctionRoleLevelID.Region:
                    logquery = (from lr in logquery
                                join f in uow.FlowInstanceRepository.GetAll() on lr.TransferID equals f.TransferID
                                join o in uow.OrganizationInviteRepository.GetAll() on f.FlowInstanceID equals o.FlowInstanceID
                                where accessLevel.RegionIDs.Contains((int)o.Organization.RegionID)
                                select lr).Distinct();
                    break;

                case FunctionRoleLevelID.Organization:
                    logquery = (from lo in logquery
                                join f in uow.FlowInstanceRepository.GetAll() on lo.TransferID equals f.TransferID
                                join o in uow.OrganizationInviteRepository.GetAll() on f.FlowInstanceID equals o.FlowInstanceID
                                where accessLevel.OrganizationIDs.Contains(o.OrganizationID)
                                select lo).Distinct();
                    break;
                
                case FunctionRoleLevelID.Location:
                
                    logquery = (from l in logquery
                                join f in uow.FlowInstanceRepository.GetAll() on l.TransferID equals f.TransferID
                                join o in uow.OrganizationInviteRepository.GetAll() on f.FlowInstanceID equals o.FlowInstanceID
                                where accessLevel.LocationIDs.Contains(o.Department.LocationID)
                                select l).Distinct();
                    break;
                case FunctionRoleLevelID.Department:
                    throw new PointSecurityException("U dient minimaal locatie niveau te hebben voor deze beheerfunctie", ExceptionType.NoRole);

                default:
                    break;
            }

            logquery = logquery.Where(l =>
                    (dateFrom == null || l.Timestamp > dateFrom) &&
                    (dateTo == null || l.Timestamp < dateTo.Value) &&
                    (transferID == null || l.TransferID == transferID) &&
                    (communicationlogtype == null || l.MailType == communicationlogtype)
                );

            if (dateFrom.HasValue)
            {
                logquery = logquery.Where(l => l.Timestamp > dateFrom);
            }

            if (dateTo.HasValue)
            {
                logquery = logquery.Where(l => l.Timestamp < dateTo.Value);
            }

            if (transferID.HasValue)
            {
                logquery = logquery.Where(l => l.TransferID == transferID);
            }

            if (communicationlogtype.HasValue)
            {
                logquery = logquery.Where(l => l.MailType == communicationlogtype);
            }

            return logquery;
        }

        public static CommunicationLog Insert(IUnitOfWork uow, int? transferID,
            PointUserInfo pointUserInfo, MailMessage mailMessage, CommunicationLogType communicationLogType = CommunicationLogType.None)
        {
            
            var communicationLog = new CommunicationLog()
            {
                Timestamp = DateTime.Now,
                EmployeeID = pointUserInfo?.Employee.EmployeeID,
                OrganizationID = pointUserInfo?.Organization.OrganizationID
            };

            if (transferID.HasValue)
            {
                var transfer = TransferBL.GetByTransferID(uow, transferID.Value);
                if (transfer != null)
                {
                    communicationLog.ClientID = transfer.ClientID;
                    communicationLog.TransferID = transfer.TransferID;
                }
            }

            if (mailMessage != null)
            {
                communicationLog.Content = mailMessage.Body;
                communicationLog.MailType = communicationLogType;
                communicationLog.FromAddress = mailMessage.From.ToString();
                communicationLog.Subject = mailMessage.Subject;
                communicationLog.ToAddress = mailMessage.To.ToString();
                communicationLog.ToName = string.Join(", ", Array.ConvertAll(mailMessage.To.ToArray(), mm => mm.DisplayName.ToString()));
            }

            return communicationLog;
        }

        private static void Upsert(IUnitOfWork uow, CommunicationLog communicationLog)
        {
            if (communicationLog.CommunicationLogID == 0)
            {
                uow.CommunicationLogRepository.Insert(communicationLog);
            }
        }

        public static void Save(IUnitOfWork uow, CommunicationLog communicationLog)
        {
            Upsert(uow, communicationLog);
            uow.Save();
        }
    }
}
