﻿using System;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Extensions;
using Point.Models.Enums;

namespace Point.Business.Logic
{
    public static class ClientSimpleViewBL
    {
        public static ClientSimpleViewModel FromModel(IUnitOfWork uow, Client client, PointUserInfo userinfo, FlowDefinitionID? flowdefinitionid)
        {
            if (client == null || client == default(Client))
                return new ClientSimpleViewModel();

            var clientSimpleViewModel = new ClientSimpleViewModel();
            clientSimpleViewModel.Merge(client);

            var flowinstance = FlowInstanceBL.GetByClientID(uow, client.ClientID);
            clientSimpleViewModel.ClientShowAnonymous = ClientBL.ShowAnonymous(flowinstance, userinfo.Organization?.OrganizationID);
            clientSimpleViewModel.ShowSearchBSN = flowdefinitionid != FlowDefinitionID.HA_VVT;
            clientSimpleViewModel.ShowSearchComplete = !clientSimpleViewModel.ShowSearchBSN;

            return clientSimpleViewModel;
        }
    }
}
