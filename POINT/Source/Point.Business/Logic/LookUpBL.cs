﻿using Point.Business.Helper;
using Point.Database.Context;
using Point.Database.Models;
using Point.Database.Extensions;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using Point.Infrastructure.Caching;
using Point.Models.ViewModels;
using Point.Database.Models.ListValues;

namespace Point.Business.Logic
{
    //Watch out with refactoring these functions are invoked by reflection so the have mostly 0 references.
    //They are invoked based upon the datasource property of the FlowField table
    public class LookUpBL : IDisposable
    {
        //We dont need to detect changes (50% increase in speed)
        private UnitOfWork<PointSearchContext> uow = new UnitOfWork<PointSearchContext>();
        public static Option OptionEmpty = new Option(" - Maak uw keuze - ", "");

        bool userinfoLoaded = false;
        PointUserInfo userinfo = null;
        private PointUserInfo PointUserInfo
        {
            get
            {
                if (userinfoLoaded == false)
                {
                    userinfo = PointUserInfoHelper.GetPointUserInfo(uow);
                    userinfoLoaded = true;
                }

                return userinfo;
            }
        }

        private List<Option> GetSelectOption()
        {
            return new List<Option> { OptionEmpty };
        }


        public IList<Option> GetGRZIntensity()
        {
            var options = new List<Option>();
            var data = FlowListValues.GRZIntensity();
            data.ForEach(rt => options.Add(new Option { Text = rt.Text, Value = rt.Value }));
            return options;
        }

        public IList<Option> GetVerwijzingTypeByFormTypeID(int? formtypeid = null)
        {
            List<Option> options = new List<Option>();
            if (formtypeid != null)
            {
                options = VerwijzingTypeValues.GetOptionList((FlowFormType)formtypeid);
                options.ForEach(it => { it.Selected = false; });
            }
            return options;
        }

        public List<Option> GetVerwijzingType(string currentvalue = "")
        {
            return VerwijzingTypeValues.GetOptionList(currentvalue);
        }

        public IList<Option> GetDecubitus()
        {
            var options = GetSelectOption();
            var data = FlowListValues.Decubitus();
            data.ForEach(rt => options.Add(new Option { Text = rt.Text, Value = rt.Value }));
            return options;
        }

        public IList<Option> GetVOFormTypeSelection(int transferID)
        {
            var data = FormTypeSelectionBL.ByTransferIDAndTypeID(transferID, TypeID.FlowOverdracht);
            return data.Select(rt => 
                new Option {
                    Text = rt.Name,
                    Value = rt.Value
                }).ToList();
        }

        public IList<Option> GetWondSoort()
        {
            var options = GetSelectOption();
            var data = FlowListValues.WondSoort();
            data.ForEach(rt => options.Add(new Option { Text = rt.Text, Value = rt.Value }));
            return options;
        }

        public IList<Option> GetStoornis()
        {
            var options = GetSelectOption();
            var data = FlowListValues.Stoornis();
            data.ForEach(rt => options.Add(new Option { Text = rt.Text, Value = rt.Value }));
            return options;
        }

        public IList<Option> GetUrineKetheterSoort()
        {
            var options = GetSelectOption();
            var data = FlowListValues.UrineKetheterSoort();
            data.ForEach(rt => options.Add(new Option { Text = rt.Text, Value = rt.Value }));
            return options;
        }

        public IList<Option> GetBeperkingen()
        {
            var options = GetSelectOption();
            var data = FlowListValues.Beperkingen();
            data.ForEach(rt => options.Add(new Option { Text = rt.Text, Value = rt.Value }));
            return options;
        }

        public IList<Option> GetHulpmiddelenZien()
        {
            var options = GetSelectOption();
            var data = FlowListValues.HulpmiddelenZien();
            data.ForEach(rt => options.Add(new Option { Text = rt.Text, Value = rt.Value }));
            return options;
        }

        public IList<Option> GetHulpmiddelenHoren()
        {
            var options = GetSelectOption();
            var data = FlowListValues.HulpmiddelenGehoor();
            data.ForEach(rt => options.Add(new Option { Text = rt.Text, Value = rt.Value }));
            return options;
        }

        public IList<Option> GetPijn()
        {
            var options = GetSelectOption();
            var data = FlowListValues.Pijn();
            data.ForEach(rt => options.Add(new Option { Text = rt.Text, Value = rt.Value }));
            return options;
        }

        public IList<Option> GetJuridischeStatus()
        {
            var options = GetSelectOption();
            var data = FlowListValues.JuridischeStatus();
            data.ForEach(rt => options.Add(new Option { Text = rt.Text, Value = rt.Value }));
            return options;
        }

        public IList<Option> GetDwangEnDrang()
        {
            var options = GetSelectOption();
            var data = FlowListValues.DwangEnDrang();
            data.ForEach(rt => options.Add(new Option { Text = rt.Text, Value = rt.Value }));
            return options;
        }

        public IList<Option> GetBurgerlijkeStaat()
        {
            var options = GetSelectOption();
            var data = EnumBL.GetEnumIntListValues<Models.LDM.EOverdracht30.Enums.BurgerlijkeStaat>();
            data.ForEach(rt => options.Add(new Option { Text = rt.Text, Value = rt.Value }));
            return options;
        }

        public IList<Option> GetGezinssamenstelling()
        {
            var options = GetSelectOption();
            var data = EnumBL.GetEnumIntListValues<Models.LDM.EOverdracht30.Enums.Gezinssamenstelling>();
            data.ForEach(rt => options.Add(new Option { Text = rt.Text, Value = rt.Value }));
            return options;
        }

        public IList<Option> GetWoningType()
        {
            var options = GetSelectOption();
            var data = EnumBL.GetEnumIntListValues<Models.LDM.EOverdracht30.Enums.WoningType>();
            data.ForEach(rt => options.Add(new Option { Text = rt.Text, Value = rt.Value }));
            return options;
        }

        public IList<Option> GetLevensovertuiging()
        {
            var options = GetSelectOption();
            var data = EnumBL.GetEnumCodeListValues<Models.LDM.EOverdracht30.Enums.Levensovertuiging>();
            data.ForEach(rt => options.Add(new Option { Text = rt.Text, Value = rt.Value }));
            return options;
        }

        public IList<Option> GetSoortHulp()
        {
            var options = GetSelectOption();
            var data = EnumBL.GetEnumCodeListValues<Models.LDM.EOverdracht30.Enums.SoortHulp>();
            data.ForEach(rt => options.Add(new Option { Text = rt.Text, Value = rt.Value }));
            return options;
        }

        public IList<Option> GetCommunicatieTaal()
        {
            var options = GetSelectOption();
            var data = EnumBL.GetEnumCodeListValues<Models.LDM.EOverdracht30.Enums.CommunicatieTaal>();
            data.ForEach(rt => options.Add(new Option { Text = rt.Text, Value = rt.Value }));
            return options;
        }

        public IList<Option> GetTaalvaardigheidBegrijpen()
        {
            var options = new List<Option>();
            var data = EnumBL.GetEnumCodeListValues<Models.LDM.EOverdracht30.Enums.TaalvaardigheidBegrijpen>();
            data.ForEach(rt => options.Add(new Option { Text = rt.Text, Value = rt.Value }));
            return options;
        }

        public IList<Option> GetTaalvaardigheidSpreken()
        {
            var options = new List<Option>();
            var data = EnumBL.GetEnumCodeListValues<Models.LDM.EOverdracht30.Enums.TaalvaardigheidSpreken>();
            data.ForEach(rt => options.Add(new Option { Text = rt.Text, Value = rt.Value }));
            return options;
        }

        public IList<Option> GetTaalvaardigheidLezen()
        {
            var options = new List<Option>();
            var data = EnumBL.GetEnumCodeListValues<Models.LDM.EOverdracht30.Enums.TaalvaardigheidLezen>();
            data.ForEach(rt => options.Add(new Option { Text = rt.Text, Value = rt.Value }));
            return options;
        }

        public IList<Option> GetGeslacht()
        {
            var options = new List<Option>();
            var data = EnumBL.GetEnumCodeListValues<Models.LDM.EOverdracht30.Enums.Geslacht>();
            data.ForEach(rt => options.Add(new Option { Text = rt.Text, Value = rt.Value }));
            return options;
        }

        public IList<Option> GetOrganisatieType()
        {
            var options = new List<Option>();
            var data = EnumBL.GetEnumCodeListValues<Models.LDM.EOverdracht30.Enums.OrganisatieType>();
            data.ForEach(rt => options.Add(new Option { Text = rt.Text, Value = rt.Value }));
            return options;
        }

        public IList<Option> GetZorgverlenersRol()
        {
            var options = new List<Option>();
            var data = EnumBL.GetEnumCodeListValues<Models.LDM.EOverdracht30.Enums.ZorgverlenersRol>();
            data.ForEach(rt => options.Add(new Option { Text = rt.Text, Value = rt.Value }));
            return options;
        }

        public IList<Option> GetHealthInsurers()
        {
            var cacheKey = "HealthInsurers";
            var options = CacheService.Instance.Get<IList<Option>>(cacheKey);
            if (options == null)
            {
                options = GetSelectOption();
                var data = HealthInsurerBL.GetAllHealthInsurers(uow, true).ToList();
                data.ForEach(rt => options.Add(new Option { Text = rt.InActive ? "(Niet actief) " + rt.Name : rt.Name, Value = rt.HealthInsurerID.ToString() }));

                CacheService.Instance.Insert(cacheKey, options);
            }

            return options.AsClone();
        }

        public IList<int> GetHealthInsurerIDsNotInsured()
        {
            var cacheKey = "HealthInsurerIDsNotInsured";
            var ids = CacheService.Instance.Get<IList<int>>(cacheKey);
            if (ids == null)
            {
                var data = new HealthInsurer[] { HealthInsurerBL.GetByUzovi(uow, 106), HealthInsurerBL.GetByUzovi(uow, 9999) };
                ids = data.Select(hi => hi.HealthInsurerID).ToList();
                CacheService.Instance.Insert(cacheKey, ids);
            }

            return ids;

        }

        public IList<Option> GetNationalities()
        {
            var cacheKey = "ActiveNationalities";
            var options = CacheService.Instance.Get<IList<Option>>(cacheKey);
            if (options == null)
            {
                options = GetSelectOption();
                var data = NationalityBL.GetActiveNationalities(uow).ToList();
                data.ForEach(rt => options.Add(new Option { Text = rt.Name, Value = rt.NationalityID.ToString() }));

                CacheService.Instance.Insert(cacheKey, options);
            }
            
            return options.AsClone();
        }

        //public IList<Option> GetHousingType()
        //{
        //    var options = GetSelectOption();
        //    var data = FlowListValues.HousingType();
        //    data.ForEach(rt => options.Add(new Option { Text = rt.Text, Value = rt.Value }));
        //    return options;
        //}

        public IList<Option> GetInstemming()
        {
            var options = GetSelectOption();
            var data = FlowListValues.Instemming();
            data.ForEach(rt => options.Add(new Option { Text = rt.Text, Value = rt.Value }));
            return options;
        }

        public IList<Option> GetInfuusSoort()
        {
            var options = GetSelectOption();
            var data = FlowListValues.InfuusSoort();
            data.ForEach(rt => options.Add(new Option { Text = rt.Text, Value = rt.Value }));
            return options;
        }

        public IList<Option> GetRelationTypes()
        {
            var options = GetSelectOption();
            var data = FlowListValues.RelationsType();
            data.ForEach(rt => options.Add(new Option { Text = rt.Text, Value = rt.Value }));
            return options;
        }

        public IList<Option> GetHealthCareOrganizationTypes()
        {
            var options = GetSelectOption();
            var data = FlowListValues.HealthCareOrganizationType();
            data.ForEach(rt => options.Add(new Option { Text = rt.Text, Value = rt.Value }));
            return options;
        }

        public IList<Option> GetOndervoedingMeetMethodes()
        {
            var options = new List<Option>();
            var data = FlowListValues.OndervoedingMeetMethode();
            data.ForEach(rt => options.Add(new Option { Text = rt.Text, Value = rt.Value }));
            return options;
        }

        public IList<Option> GetOndervoedingsScore(string currentvalue)
        {
            if (string.IsNullOrEmpty(currentvalue))
            {
                return new List<Option> { new Option("Selecteer eerst type ondervoeding score", "") };
            }
            var data = new List<ListValue>();

            if (currentvalue.ToLower().StartsWith("s"))
            {
                data = FlowListValues.OndervoedingsScoreSNAQ();
            }

            if (currentvalue.ToLower().StartsWith("m"))
            {
                data = FlowListValues.OndervoedingsScoreMUST();
            }

            return data.Select(rt => new Option { Text = rt.Text, Value = rt.Value }).ToList();
        }

        public IList<Option> GetYesNoUnknown()
        {
            var options = new List<Option>();
            var data = FlowListValues.YesNoUnknown();
            data.ForEach(rt => options.Add(new Option { Text = rt.Text, Value = rt.Value }));
            return options;
        }

        public IList<Option> GetCIZRelations()
        {
            var cacheKey = "CIZRelations";
            var options = CacheService.Instance.Get<IList<Option>>(cacheKey);
            if(options == null)
            {
                options = GetSelectOption();
                CIZRelationBL.GetCIZRelations(uow).ToList().ForEach(ciz => options.Add(new Option { Text = ciz.Description, Value = ciz.CIZRelationID.ToString() }));

                CacheService.Instance.Insert(cacheKey, options);
            }
            
            return options.AsClone();
        }

        public IList<Option> GetFullNameByDepartmentID(int? departmentID)
        {
            var options = new List<Option>();
            if (departmentID == null)
            {
                return options;
            }

            var department = DepartmentBL.GetByDepartmentID(uow, departmentID.Value);
            if (department == null)
            {
                return options;
            }

            string fullname = department.FullName();

            options.Add(new Option(fullname, departmentID.ToString()));
            return options;
        }

        public IList<Option> GetDepartmentNameByDepartmentID(int departmentID)
        {
            var options = new List<Option>();
            var department = DepartmentBL.GetByDepartmentID(uow, departmentID);

            if (department == null)
            {
                return options;
            }

            options.Add(new Option(department.Name, departmentID.ToString()));
            return options;
        }

        public IList<Option> GetLocationNameByLocationID(int locationID)
        {
            var options = new List<Option>();
            var location = LocationBL.GetByID(uow, locationID);
            if (location == null)
            {
                return options;
            }

            options.Add(new Option { Text = location.Name, Value = locationID.ToString() });
            return options;
        }

        public IList<Option> GetDoctorsByOrganization(int? currentvalue = null)
        {
            var options = GetSelectOption();
            if (currentvalue.HasValue)
            {
                var currentdoctor = DoctorBL.GetByID(uow, currentvalue.Value);
                options.Add(new Option
                {
                    Value = currentdoctor.DoctorID.ToString(),
                    Text = $"{currentdoctor.SearchName} - {currentdoctor.Name} - {currentdoctor.Specialism.Name}"
                });
            }

            var pointuserinfo = PointUserInfo;
            if (pointuserinfo?.Organization == null)
            {
                return options;
            }

            var cacheKey = "DoctorsByOrganizationID_" + pointuserinfo.Organization.OrganizationID;
            var otheroptions = CacheService.Instance.Get<IList<Option>>(cacheKey);
            if(otheroptions == null)
            {
                otheroptions = DoctorBL.GetByOrganizationID(uow, pointuserinfo.Organization.OrganizationID)
                    .Select(d => 
                        new Option() {
                            Value = d.DoctorID.ToString(),
                            Text = d.SearchName + " - " + d.Name + " - " + d.Specialism.Name
                        }).ToList();

                CacheService.Instance.Insert(cacheKey, otheroptions);
            }


            options.AddRange(otheroptions.Where(doc => options.All(op => op.Value != doc.Value)));
            
            return options.AsClone();
        }

        public IList<Option> GetEndRegistrationReasons()
        {
            var staticoptions = new[] { "overlijden", "medische/nieuwe behandeling", "patiënt heeft geen zorg nodig", "naar huis alleen met hulpmiddelen" };

            var options = GetSelectOption();
            foreach (var option in staticoptions)
            {
                options.Add(new Option
                {
                    Value = Array.IndexOf(staticoptions, option).ToString(),
                    Text = option
                });
            }
            return options;
        }

        public IList<Option> GetSecretQuestions(string current = "")
        {
            var options = new List<Option>();

            options.Add(new Option(" - Selecteer een vraag - ", ""));
            options.Add(new Option("In welke stad heb je gestudeerd?", "In welke stad heb je gestudeerd?"));
            options.Add(new Option("Waar ging je voor de eerste keer op vakantie?", "Waar ging je voor de eerste keer op vakantie?"));
            options.Add(new Option("Waar ging je heen op huwelijksreis?", "Waar ging je heen op huwelijksreis?"));
            options.Add(new Option("Waar heb je je partner ontmoet?", "Waar heb je je partner ontmoet?"));
            options.Add(new Option("Wat is de naam van je oudste kind?", "Wat is de naam van je oudste kind?"));
            options.Add(new Option("Wat is de naam van je jongste kind", "Wat is de naam van je jongste kind"));
            options.Add(new Option("Wat is de voornaam van je favoriete tante?", "Wat is de voornaam van je favoriete tante?"));
            options.Add(new Option("Wat is de voornaam van je favoriete oom?", "Wat is de voornaam van je favoriete oom?"));
            options.Add(new Option("In welke stad is je vader geboren?", "In welke stad is je vader geboren?"));
            options.Add(new Option("In welke stad is je moeder geboren?", "In welke stad is je moeder geboren?"));

            options.ForEach(it => it.Selected = string.Equals(it.Value, current, StringComparison.CurrentCultureIgnoreCase));

            return options;
        }

        public IList<Option> GetSpecialismsByOrganizationType(int organizationtypeID)
        {
            var cacheKey = "SpecialismByOrganizationTypeID_" + organizationtypeID;
            var options = CacheService.Instance.Get<IList<Option>>(cacheKey);
            if (options == null)
            {
                options = GetSelectOption();
                var specialisms = SpecialismBL.GetByOrganizationTypeID(uow, organizationtypeID);
                specialisms.ToList().ForEach(spec => options.Add(new Option { Value = spec.SpecialismID.ToString(), Text = spec.Name }));

                CacheService.Instance.Insert(cacheKey, options);
            }
            
            return options.AsClone();
        }

        public IList<Option> GetOrganizationNameByOrganizationID(int? organizationID)
        {
            if (organizationID == null)
            {
                return new List<Option>();
            }

            var cacheKey = "OrganizationNameByOrganizationID_" + organizationID;
            var options = CacheService.Instance.Get<IList<Option>>(cacheKey);
            if(options == null)
            {
                options = new List<Option>();
                var organization = OrganizationBL.GetByOrganizationID(uow, organizationID.Value);
                if (organization != null)
                {
                    options.Add(new Option(organization.Name, organizationID.ToString()));
                }

                CacheService.Instance.Insert(cacheKey, options);
            }
            
            return options.AsClone();
        }

        public IList<Option> GetTransferPointEmployees(int? employeeID, int? transferID = null)
        {
            var options = GetSelectOption();
            var pointuserinfo = PointUserInfo;

            if (employeeID.HasValue)
            {
                var currentemployee = EmployeeBL.GetByID(uow, employeeID.Value);
                options.Add(MapEmployeeToOption(currentemployee));
            }
            if (pointuserinfo?.Employee != null)
            {
                if (options.All(it => it.Value != pointuserinfo.Employee.EmployeeID.ToString()))
                {
                    var employee = EmployeeBL.GetByID(uow, pointuserinfo.Employee.EmployeeID);
                    options.Add(MapEmployeeToOption(employee));
                }
            }

            if (transferID == null)
            {
                return options;
            }

            var flowinstance = FlowInstanceBL.GetByTransferID(uow, transferID.Value);
            if (flowinstance == null)
            {
                return options;
            }

            IEnumerable<Department> transferpoints = null;

            var sendinglocation = OrganizationHelper.GetSendingLocation(uow, flowinstance.FlowInstanceID);
            if (sendinglocation != null && sendinglocation.Organization.OrganizationTypeID == (int)OrganizationTypeID.Hospital)
            {
                transferpoints = DepartmentBL.GetTransferPointsByLocation(uow, sendinglocation.LocationID);
            }
            else
            {
                var recievinglocation = OrganizationHelper.GetRecievingLocation(uow, flowinstance.FlowInstanceID);
                if (recievinglocation != null && recievinglocation.Organization.OrganizationTypeID == (int)OrganizationTypeID.Hospital)
                {
                    transferpoints = DepartmentBL.GetTransferPointsByLocation(uow, recievinglocation.LocationID);
                }
            }

            if (transferpoints == null)
            {
                return options.AsClone();
            }

            foreach (var transferpoint in transferpoints.ToList())
            {
                var transferpointemployees = EmployeeBL.GetCachedByDepartmentIDIncludingConnected(uow, transferpoint.DepartmentID);
                options.AddRange(transferpointemployees.Where(tp => options.Any(op => op.Value == tp.Value) == false));
            }

            return options.AsClone();
        }

        public IList<Option> GetEmployeeName(int? employeeID)
        {
            var options = new List<Option>();
            if (employeeID.HasValue)
            {
                var employee = EmployeeBL.GetByID(uow, employeeID.Value);
                options.Add(MapEmployeeToOption(employee));
            }

            return options;
        }

        public IList<Option> GetEmployeesByOwnDepartment(int? employeeID)
        {
            var options = GetSelectOption();
            var pointuserinfo = PointUserInfo; ;

            if (employeeID.HasValue)
            {
                var employee = EmployeeBL.GetByID(uow, employeeID.Value);
                var departmentemployees = EmployeeBL.GetCachedByDepartmentID(uow, employee.DepartmentID);

                options.AddRange(departmentemployees.Where(de => options.Any(op => op.Value == de.Value) == false));
            }

            if (pointuserinfo?.Employee == null) return options.OrderBy(x => x.Text).ToList().AsClone();

            var pointuserdepartments = pointuserinfo.EmployeeDepartmentIDs.ToList();
            foreach (var item in pointuserdepartments)
            {
                var employees = EmployeeBL.GetCachedByDepartmentID(uow, item);
                options.AddRange(employees.Where(de => options.Any(op => op.Value == de.Value) == false));
            }

            if (options.All(opt => opt.Value != pointuserinfo.Employee.EmployeeID.ToString()))
            {
                var employee = EmployeeBL.GetByID(uow, pointuserinfo.Employee.EmployeeID);
                options.Add(MapEmployeeToOption(employee));
            }

            return options.OrderBy(x => x.Text).ToList().AsClone();
        }

        private static Option MapEmployeeToOption(Employee e)
        {
            return new Option(e.FullName(), e.EmployeeID.ToString());
        }

        public IList<Option> GetLocationsByLocationOrganizationID(int? locationID)
        {
            if (locationID == null)
            {
                return new List<Option>();
            }

            var cacheKey = "LocationsByLocationOrganizationID_" + locationID;
            var options = CacheService.Instance.Get<List<Option>>(cacheKey);
            if(options == null)
            {
                var currentlocation = LocationBL.GetByID(uow, locationID.Value);
                if (currentlocation == null)
                {
                    options = new List<Option>();
                }
                else
                {
                    var locations = LocationBL.GetActiveByOrganisationID(uow, currentlocation.OrganizationID);
                    options = (from location in locations
                               orderby location.Name ascending
                               select new Option(location.Name, location.LocationID.ToString())).ToList();
                }

                CacheService.Instance.Insert(cacheKey, options);
            }
            
            return options.AsClone();
        }

        public IList<Option> GetFormType(int currentvalue)
        {
            var options = new List<Option>();
            var formType = FormTypeBL.GetByFormTypeID(uow, currentvalue);
            if (formType != null)
            {
                options.Add(new Option { Text = formType.Name, Value = formType.FormTypeID.ToString() });
            }
            return options;
        }

        public IList<Option> GetRequestFormZHVVTTypes(int? transferid = null, int? currentValue = null)
        {
            var options = GetSelectOption();

            List <FormType> formtypes = new List<FormType>();
            var userinfo = PointUserInfo;

            if(transferid.HasValue && userinfo?.Organization != null)
            {
                var flowinstance = FlowInstanceBL.GetByTransferID(uow, transferid.Value);
                if (flowinstance != null)
                {
                    formtypes = FormTypeBL.GetRequestFormsByOrganizationIDAndFlowDefinitionIDs(uow, userinfo.Organization.OrganizationID, new FlowDefinitionID[] { flowinstance.FlowDefinitionID });
                }
            }

            FormType currentformtype = null;
            if(currentValue.HasValue)
            {
                currentformtype = FormTypeBL.GetByFormTypeID(uow, currentValue.Value);
                var selected = formtypes.FirstOrDefault(it => it.FormTypeID == currentformtype.FormTypeID);
                if (selected == null)
                {
                    formtypes.Add(currentformtype);
                }
            }

            formtypes.OrderBy(ft => ft.Order).ToList().ForEach(formtype => 
                options.Add(new Option() { Text = formtype.Name, Value = formtype.FormTypeID.ToString()})
            );

            return options;
        }

        public List<Option> GetSiblingDepartments(int? departmentID)
        {
            if (departmentID == null)
            {
                return new List<Option>();
            }

            var cacheKey = "SiblingDepartmentsByDepartmentID_" + departmentID;
            var options = CacheService.Instance.Get<List<Option>>(cacheKey);
            if (options == null)
            {
                var department = DepartmentBL.GetByDepartmentID(uow, departmentID.Value);
                options = department == null ? new List<Option>() : GetDepartmentsByLocationID(department.LocationID);

                CacheService.Instance.Insert(cacheKey, options);
            }

            return options.AsClone();   
        }

        private List<Option> GetDepartmentsByLocationID(int? locationID)
        {
            if (locationID == null)
            {
                return new List<Option>();
            }

            var cacheKey = "DepartmentsByLocationID_" + locationID;
            var options = CacheService.Instance.Get<List<Option>>(cacheKey);
            if (options == null)
            {
                var departments = DepartmentBL.GetByLocationID(uow, locationID.Value);
                options = (from department in departments
                                  orderby department.Name
                                  select new Option(department.Name, department.DepartmentID.ToString())).ToList();

                CacheService.Instance.Insert(cacheKey, options);
            }
            

            return options.AsClone();
        }
        
        public IList<Option> GetOrganizationOutsidePoint(int? organizationOutsidePointID)
        {
            var cacheKey = "OrganizationOutsidePoint_" + organizationOutsidePointID.GetValueOrDefault();
            var options = CacheService.Instance.Get<List<Option>>(cacheKey);
            if (options == null)
            {
                options = GetSelectOption();

                var organizationoutsidepoints = OrganizationOutsidePointBL.GetAll(uow);
                options.AddRange((from organizationoutsidepoint in organizationoutsidepoints
                                  orderby organizationoutsidepoint.Name
                                  select new Option(organizationoutsidepoint.Name, organizationoutsidepoint.OrganizationOutsidePointID.ToString(), organizationoutsidepoint.OrganizationOutsidePointID == organizationOutsidePointID)).ToList());

                CacheService.Instance.Insert(cacheKey, options);
            }
            

            return options.AsClone();
        }

        public List<Option> GetOrganizationProjectByTransferID(string currentvalue, int transferid)
        {
            var options = new List<Option>();

            if (!string.IsNullOrEmpty(currentvalue))
            {
                var projects = OrganizationProjectBL.GetByIDs(uow, currentvalue.Split(',').Where(it => !string.IsNullOrEmpty(it)).Select(it => Convert.ToInt32(it)).ToArray());
                options = projects.Select(l => new Option { Text = l.Name, Value = l.OrganizationProjectID.ToString(), Selected = true }).ToList();
            }

            var flowinstance = FlowInstanceBL.GetByTransferID(uow, transferid);
            if (flowinstance == null)
            {
                return options;
            }

            var flowinstanceid = flowinstance.FlowInstanceID;

            var sendingorganization = OrganizationHelper.GetSendingOrganization(uow, flowinstanceid);
            if (sendingorganization == null)
            {
                return options;
            }

            var organizationprojectoptions = OrganizationProjectBL.GetActiveByOrganizationID(uow, sendingorganization.OrganizationID)
                   .Select(l => new Option { Text = l.Name, Value = l.OrganizationProjectID.ToString() }).ToList();

            options.AddRange(organizationprojectoptions.Where(lo => options.All(ol => ol.Value != lo.Value)));

            return options.OrderBy(it => it.Text).ToList();
        }

        public IList<Option> GetHealthAidDevices()
        {
            var cacheKey = "HealthAidDevices";
            var options = CacheService.Instance.Get<List<Option>>(cacheKey);

            if(options == null)
            {
                var healthAidDeviceLogic = new HealthAidDeviceBL();
                var healthAidDevices = healthAidDeviceLogic.GetAll(uow);
                options = healthAidDevices.Select(x => new Option(x.Name, x.HealthAidDeviceId.ToString())).ToList();

                CacheService.Instance.Insert(cacheKey, options);
            }
            
            return options.AsClone();
        }

        public IList<Option> GetDeliveryLocations()
        {
            var cacheKey = "DeliveryLocations";
            var options = CacheService.Instance.Get<List<Option>>(cacheKey);

            if (options == null)
            {
                var deliveryLocationLogic = new DeliveryLocationBL();
                var deliveryLocations = deliveryLocationLogic.GetAll();
                options = deliveryLocations.Select(x => new Option(x, x)).ToList();

                CacheService.Instance.Insert(cacheKey, options);
            }

            return options.AsClone();
        }

        public IList<Option> GetEmployeesForDepartmentForCurrentTransfer(int? employeeid = null, int? transferid = null)
        {
            var employees = GetSelectOption();

            var department = DepartmentBL.GetSendingDepartmentByTransferID(uow, transferid.GetValueOrDefault());
            if (department != null)
            {
                employees = EmployeeBL.GetCachedByDepartmentID(uow, department.DepartmentID);
            }

            if (employeeid.HasValue && employees.All(emp => emp.Value != employeeid.ToString()))
            {
                var employee = EmployeeBL.GetByID(uow, employeeid.Value);
                if (employee != null)
                {
                    employees.Add(MapEmployeeToOption(employee));
                }
            }

            try
            {
                var userinfo = PointUserInfo;
                if (userinfo?.Employee != null)
                {
                    if (employees.FirstOrDefault(e => e.Value == userinfo.Employee.EmployeeID.ToString()) == null)
                    {
                        var employee = EmployeeBL.GetByID(uow, userinfo.Employee.EmployeeID);
                        employees.Add(MapEmployeeToOption(employee));
                    }
                }
            }
            catch (Exception)
            {
                // ignored
            }

            return employees.AsClone();
        }

        public IList<Option> GetAfterCareCategories()
        {
            var cacheKey = "AfterCareCategoryOptions";
            var options = CacheService.Instance.Get<List<Option>>(cacheKey);
            if(options == null)
            {
                options = AfterCareCategoryBL.GetAll(uow).Select(x => new Option(x.Name, x.AfterCareCategoryID)).ToList();
                CacheService.Instance.Insert(cacheKey, options);
            }

            return options.AsClone();
        }

        public IList<Option> GetAfterCareFinancing(int? currentvalue, int? aftercaretypeid = null)
        {
            var options = new List<Option>();
            if (currentvalue != null)
            {
                var currentaftercarefinancing = AfterCareFinancingBL.GetById(uow, currentvalue.Value);
                if (currentaftercarefinancing != null)
                {
                    options.Add(new Option(currentaftercarefinancing.Name,
                        currentaftercarefinancing.AfterCareFinancingID.ToString())
                    { Selected = true });
                }
            }

            if (aftercaretypeid == null) return options;

            var cacheKey = "AfterCareCategoriesByAfterCareTypeID_" + aftercaretypeid.Value;
            var groupitems = CacheService.Instance.Get<List<Option>>(cacheKey);

            if(groupitems == null)
            {
                var groupdata = AfterCareFinancingBL.GetByAfterCareTypeID(uow, aftercaretypeid.Value).ToList();
                var autoselect = groupdata.Count == 1;
                groupitems = groupdata.Select(it => new Option(it.Name, it.AfterCareFinancingID.ToString()) { Selected = autoselect }).ToList();

                CacheService.Instance.Insert(cacheKey, groupitems);
            }

            options.AddRange(groupitems.Where(gi => options.All(ol => ol.Value != gi.Value)));

            return options.AsClone();
        }

        public IList<Option> GetAfterCareTypesByValue(int? currentvalue, int? formtypeid = null)
        {
            var aftercaretypes = AfterCareTypeBL.GetByCurrentValue(uow, currentvalue, formtypeid).ToList();
            return aftercaretypes.Select(x => new Option(x.Name, x.AfterCareTypeID)).ToList();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~LookUpBL()
        {
            Dispose(false);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (uow != null)
                {
                    uow.Dispose();
                    uow = null;
                }
            }
        }
    }
}
