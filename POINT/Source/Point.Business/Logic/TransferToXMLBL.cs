﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Linq;
using System.Text;
using System.Xml;
using Point.Database.Repository;
using System.Web.Mvc;

namespace Point.Business.Logic
{
    public class TransferToXMLBL
    {
        public static XmlDocument TransferToXML(IUnitOfWork uow, int transferID, ViewDataDictionary viewData)
        {

            XmlDocument xmlDoc = new XmlDocument();
            XmlDeclaration xmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", "UTF-8", null);
            XmlElement root = xmlDoc.DocumentElement;
            xmlDoc.InsertBefore(xmlDeclaration, root);

            try { 
            
            XmlElement rootNode = xmlDoc.CreateElement("Data");
            xmlDoc.AppendChild(rootNode);

            var formSetVersions = FormSetVersionBL.GetByTransferID(uow, transferID).ToList();
            foreach (var formSetVersion in formSetVersions)
            {
                XmlElement formSetVersionNode = xmlDoc.CreateElement(CleanInvalidXmlChars(formSetVersion.FormType.Name));

                var flowWebFields = FlowWebFieldBL.GetFieldsFromFormSetVersionID(uow, formSetVersion.FormSetVersionID, viewData).ToList();
                foreach (var flowWebField in flowWebFields)
                {
                    if (!string.IsNullOrEmpty(flowWebField.Value) && flowWebField.Visible && !string.IsNullOrEmpty(flowWebField.Label?.Trim()) )
                    {                      
                        XmlElement flowWebFieldNood = xmlDoc.CreateElement(CleanInvalidXmlChars(flowWebField.Label));
                        flowWebFieldNood.InnerText = flowWebField?.DisplayValue ?? "";
                        formSetVersionNode.AppendChild(flowWebFieldNood);
                    }
                }
                rootNode.AppendChild(formSetVersionNode);
            }


            }
            catch (Exception ex)
            {
                var str = ex.Message;
            }
               return xmlDoc;
        }

        private static string CleanInvalidXmlChars(string text)
        {                      
            if (string.IsNullOrEmpty(text)) return "";
            text = Regex.Replace(text, "\xA0", String.Empty);
            return Regex.Replace(text, "[^a-zA-Z0-9\x80-\xA5]", String.Empty) ?? "";            
        }
    }
}
