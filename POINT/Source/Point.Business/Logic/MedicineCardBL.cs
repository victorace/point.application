﻿using System;
using System.Collections.Generic;
using System.Linq;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;

namespace Point.Business.Logic
{
    public class MedicineCardBL
    {
        public const int NotRegisteredMedicineID = 2091;

        public static IEnumerable<MedicineCard> GetByFormSetVersionID(IUnitOfWork uow, int formsetversionid)
        {
            return uow.MedicineCardRepository.Get(mc => mc.FormSetVersionID == formsetversionid);
        }

        public static void SaveByViewModelAndFormSetVersionID(IUnitOfWork uow, List<MedicationItemViewModel> items, int formsetversionid, int transferid, int employeeid)
        {
            var timestamp = DateTime.Now;

            var currentitems = GetByFormSetVersionID(uow, formsetversionid).ToList();
            for (int i = 0; i < items.Count; i++)
            {
                var item = items[i];
                var currentitem = currentitems.Count() > i ? currentitems[i] : null;
                if (currentitem == null)
                {
                    currentitem = new MedicineCard();
                    currentitem.MedicineID = NotRegisteredMedicineID;
                    currentitem.FormSetVersionID = formsetversionid;
                    currentitem.TransferID = transferid;

                    uow.MedicineCardRepository.Insert(currentitem);
                }

                currentitem.EmployeeID = employeeid;
                currentitem.NotRegisteredMedicine = item.Medication;
                currentitem.Prescription = item.DeliveryMethod;
                currentitem.Dosage = item.Dosage;
                currentitem.Frequence = item.Frequency;
                currentitem.TimeStamp = timestamp;
            }
            
            foreach (var item in currentitems.Skip(currentitems.Count))
	        {
                uow.MedicineCardRepository.Delete(item);
	        }
                        
            uow.Save();
        }

        public static IEnumerable<MedicineCard> CopyTo(IUnitOfWork uow, FormSetVersion source, FormSetVersion destination)
        {
            List<MedicineCard> destinationMedicineCards = new List<MedicineCard>();

            var sourceMedicineCards = uow.MedicineCardRepository.Get(mc => mc.FormSetVersionID == source.FormSetVersionID, asNoTracking: true);
            foreach (var mc in sourceMedicineCards)
            {
                mc.MedicineCardID = 0;
                mc.TransferID = destination.TransferID;
                mc.FormSetVersionID = destination.FormSetVersionID;

                uow.MedicineCardRepository.Insert(mc);
                destinationMedicineCards.Add(mc);
            }

            if (destinationMedicineCards.Any())
                uow.Save();

            return destinationMedicineCards;
        }
    }
}
