﻿using System;
using System.Collections.Generic;
using System.Linq;
using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Repository;
using Point.Infrastructure.Caching;
using Point.Infrastructure.Extensions;

namespace Point.Business.Logic
{
    public class FormTypeBL
    {
        public static IEnumerable<FormType> GetFormTypes(IUnitOfWork uow, bool usecache = true)
        {
            string cachestring = "FormType_Global";
            IList<FormType> formtypes = null;

            if (usecache)
                formtypes = CacheService.Instance.Get<IList<FormType>>(cachestring);

            if (formtypes == null)
            {
                formtypes = uow.FormTypeRepository.GetAll().ToList();
                if (formtypes.Any())
                    CacheService.Instance.Insert(cachestring, formtypes, profile: "CacheLong");
            }
            return formtypes;
        }

        public static FormType GetByFormTypeID(IUnitOfWork uow, int formtypeID)
        {
            return GetFormTypes(uow).Where(ft => ft.FormTypeID == formtypeID).FirstOrDefault();
        }

        public static IEnumerable<FormType> GetByTypeID(IUnitOfWork uow, int typeID)
        {
            return GetFormTypes(uow).Where(ft => ft.TypeID == typeID);
        }

        public static IEnumerable<FormType> GetByTypeIDs(IUnitOfWork uow, int[] typeIDs)
        {
            return GetFormTypes(uow).Where(ft => ft.TypeID != null && typeIDs.Contains(ft.TypeID.Value));
        }

        public static FormType GetByPhaseDefinitionID(IUnitOfWork uow, int phasedefinitionid)
        {
            string cachestring = "FormType_PhaseDefinitionID" + phasedefinitionid;
            var formtype = CacheService.Instance.Get<FormType>(cachestring);
            if(formtype == null)
            {
                formtype = uow.FormTypeRepository.FirstOrDefault(ft => ft.PhaseDefinition.Any(pd => pd.PhaseDefinitionID == phasedefinitionid));
                CacheService.Instance.Insert(cachestring, formtype, profile: "CacheLong");
            }

            return formtype;
        }

        public static IEnumerable<FormType> GetByTransferID(IUnitOfWork uow, int transferID)
        {
            return uow.FormSetVersionRepository.Get(fsv => fsv.TransferID == transferID && !(bool)fsv.Deleted).Select(ffv => ffv.FormType).Distinct().ToList();
        }

        public static FormType GetByURL(IUnitOfWork uow, Uri uri)
        {
            if (uri != null && !String.IsNullOrWhiteSpace(uri.AbsolutePath))
            {
                var pathParts = uri.AbsolutePath.Split('/').TakeLast<string>(2).ToList();
                if (pathParts.Count() == 2)
                    return FormTypeBL.GetByURL(uow, pathParts[0], pathParts[1]);
            }
            return null;
        }

        public static FormType GetByURL(IUnitOfWork uow, string url)
        {
            return GetFormTypes(uow).Where(ft => ft.URL == url).FirstOrDefault();
        }

        public static FormType GetByURL(IUnitOfWork uow, string controllerName, string actionName)
        {
            string url = String.Concat(controllerName, "/", actionName);
            return GetByURL(uow, url);
        }

        public static List<FormType> GetByOrganizationIDAndFlowdefinitionIDs(IUnitOfWork uow, int organizationid, FlowDefinitionID[] flowdefinitionids)
        {
            var formtypes = uow.FormTypeRepository.Get(it => it.FormTypeOrganization.Any(fr => fr.OrganizationID == organizationid && flowdefinitionids.Contains(fr.FlowDefinitionID))).ToList();
            return formtypes;
        }

        public static List<FormType> GetByRegionIDAndFlowdefinitionIDs(IUnitOfWork uow, int regionid, FlowDefinitionID[] flowdefinitionids)
        {

            var formtypes = uow.FormTypeRepository.Get(it => it.FormTypeRegion.Any(fr => fr.RegionID == regionid && flowdefinitionids.Contains(fr.FlowDefinitionID))).ToList();
            return formtypes;
        }

        public static List<FormType> GetRequestFormsByOrganizationIDAndFlowDefinitionIDs(IUnitOfWork uow, int organizationid, FlowDefinitionID[] flowdefinitionids)
        {
            var formTypes = new List<FormType>();
            var typeid = (int)TypeID.FlowAanvraag;
            var organization = OrganizationBL.GetByOrganizationID(uow, organizationid);

            if (organization == null) return formTypes;

            var formTypeOrganizations = GetByOrganizationIDAndFlowdefinitionIDs(uow, organization.OrganizationID, flowdefinitionids);
            foreach (var formtype in formTypeOrganizations)
            {
                if (formtype.TypeID == typeid)
                    formTypes.Add(formtype);
            }

            if (formTypes.Count() == 0 && organization.RegionID.HasValue) // None found on orga-level, get it on region-level
            {
                var formTypesRegion = GetByRegionIDAndFlowdefinitionIDs(uow, organization.RegionID.Value, flowdefinitionids);
                if (formTypesRegion.Any())
                {
                    foreach (var formType in formTypesRegion)
                    {
                        if (formType.TypeID == typeid)
                            formTypes.Add(formType);
                    }
                }
            }

            return formTypes.OrderBy(ft => ft.Order).ThenBy(ft => ft.Name).ToList();
        }
    }

    public static class FormTypeExtensions
    {
        public static string GetControllerName(this FormType formType)
        {
            if (String.IsNullOrWhiteSpace(formType.URL)) return "";
            string[] components = formType.URL.Split('/');

            if (components.Count() >= 1)
                return formType.URL.Split('/')[0];
            else
                return "";
        }
        
        public static string GetPrintName(this FormType formType)
        {
            if (String.IsNullOrWhiteSpace(formType.PrintName)) return formType.Name;

            return formType.PrintName;
        }
        
        public static string GetActionName(this FormType formType)
        {
            if (String.IsNullOrWhiteSpace(formType.URL)) return "";
            string[] components = formType.URL.Split('/');

            if (components.Count() >= 2)
                return formType.URL.Split('/')[1];
            else
                return "";
        }
    }
}