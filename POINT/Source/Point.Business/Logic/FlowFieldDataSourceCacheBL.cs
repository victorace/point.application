﻿using Newtonsoft.Json;
using Point.Business.Helper;
using Point.Database.Extensions;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Caching;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Point.Business.Logic
{
    public class FlowFieldDataSourceCacheBL
    {
        private static string GetJsonDataByFlowFieldDataSourceID(IUnitOfWork uow, int? flowFieldDataSourceID)
        {
            if (flowFieldDataSourceID == null)
            {
                return null;
            }

            string cachestring = "FlowFieldDataSource_" + flowFieldDataSourceID;
            string jsonstring = CacheService.Instance.Get<string>(cachestring);

            if (jsonstring == null)
            {
                var datasource = uow.FlowFieldDataSourceRepository.GetByID(flowFieldDataSourceID);
                jsonstring = datasource?.JsonData;

                CacheService.Instance.Insert(cachestring, jsonstring, "CacheLong");
            }

            return jsonstring;
        }

        private static FlowFieldDataSourceViewModel GetByFlowFieldDataSourceID(IUnitOfWork uow, int? flowFieldDataSourceID)
        {
            if (flowFieldDataSourceID == null)
            {
                return null;
            }

            var jsondata = GetJsonDataByFlowFieldDataSourceID(uow, flowFieldDataSourceID);
            if(jsondata == null)
            {
                return null;
            }

            var viewmodel = JsonConvert.DeserializeObject<FlowFieldDataSourceViewModel>(jsondata);

            return viewmodel;
        }

        public static FlowFieldDataSourceViewModel GetByFlowFieldDataSourceID(IUnitOfWork uow, int? flowFieldDataSourceID, string value, IDictionary<string, object> viewdata)
        {
            var datasource = GetByFlowFieldDataSourceID(uow, flowFieldDataSourceID);
            if (datasource == null)
            {
                return null;
            }

            if (datasource.Options == null || datasource.Options.Count == 0)
            {
                datasource.Options = FlowFieldValueHelper.GetOptions(datasource, viewdata, value);
            }

            if (datasource.Options != null && datasource.Options.Any())
            {
                datasource.Options = datasource.Options.AsClone();

                if (!string.IsNullOrEmpty(value))
                {
                    if (value.Contains(","))
                    {
                        var values = value.Split(',').Where(v => v != null);
                        datasource.Options.ForEach(it => it.Selected = values.Any(v => string.Equals(it.Value, v, StringComparison.CurrentCultureIgnoreCase)));
                    }
                    else
                    {
                        datasource.Options.ForEach(it => it.Selected = string.Equals(it.Value, value, StringComparison.CurrentCultureIgnoreCase));
                    }
                }
            }

            return datasource;
        }
    }
}
