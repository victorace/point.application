﻿using Point.Database.Extensions;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Models.Enums;

namespace Point.Business.Logic
{
    public static class PageLockViewBL
    {
        public static PageLockViewModel FromModel(IUnitOfWork uow, PageLock pageLock, PointUserInfo pointuserinfo)
        {
            PageLockViewModel pageLockViewModel = null;

            if (pageLock != null)
            {
                var formType = FormTypeBL.GetByURL(uow, pageLock.URL);
                if (formType != null && formType.HasPageLocking)
                {
                    var unlockemployees = EmployeeBL.GetUnlockEmployees(uow, pointuserinfo, pageLock.TransferID);
                    pageLockViewModel = new PageLockViewModel()
                    {
                        PageLockID = pageLock.PageLockID,
                        Owner = pageLock.Employee.FullNameWithTelephone(),
                        Timestamp = pageLock.TimeStamp,
                        Url = formType.URL,
                        FormName = formType.Name,
                        PageLockTimeout = TransferBL.GetPageLockTimeout(uow, pageLock.TransferID),
                        PageUnlockEmployees = unlockemployees,
                        CanRemovePageLock = pointuserinfo.HasFunctionRole(FunctionRoleTypeID.ManageAll, FunctionRoleTypeID.UnlockForms)
                    };
                }
            }

            return pageLockViewModel;
        }
    }
}
