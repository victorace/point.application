﻿using Point.Business.Expressions;
using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Models.ViewModels;
using System;
using System.Linq;
using Point.Database.Context;
using Point.Infrastructure.Exceptions;
using Point.Business.Helper;

namespace Point.Business.Logic
{
    public static class FlowInstanceReportValuesBL
    {
        public static IQueryable<FlowInstanceReportValues> GetQuery(IUnitOfWork uow)
        {
            return uow.FlowInstanceReportValuesRepository.Get();
        }

        public static IQueryable<FlowInstanceReportValues> GetFilteredQuery(IUnitOfWork uow, FilterViewModel filterViewmodel, FlowDirection flowDirection, PointUserInfo pointUserInfo)
        {
            var query = GetQuery(uow);
            return query.ApplyFilterViewModel(filterViewmodel, flowDirection, pointUserInfo);
        }

        public static bool VerifyFilter(FilterViewModel filterViewModel, PointUserInfo pointUserInfo)
        {
            var maxlevel = pointUserInfo.GetReportFunctionRoleLevelID();
            if (maxlevel == FunctionRoleLevelID.None)
            {
                return false;
            }

            if(maxlevel < FunctionRoleLevelID.Global)
            {
                using (var uow = new UnitOfWork<PointSearchContext>())
                {
                    if(filterViewModel.DepartmentIDs.Any() && maxlevel >= FunctionRoleLevelID.Department)
                    {
                        return PointUserInfoHelper.IsDepartmentIDsValid(uow, pointUserInfo, maxlevel, filterViewModel.DepartmentIDs);
                    }
                    if (filterViewModel.LocationIDs.Any() && maxlevel >= FunctionRoleLevelID.Location)
                    {
                        return PointUserInfoHelper.IsLocationIDsValid(uow, pointUserInfo, maxlevel, filterViewModel.LocationIDs);
                    }
                    if (filterViewModel.OrganizationIDs.Any() && maxlevel >= FunctionRoleLevelID.Organization)
                    {
                        return PointUserInfoHelper.IsOrganizationIDsValid(uow, pointUserInfo, maxlevel, filterViewModel.OrganizationIDs);
                    }
                    if (filterViewModel.RegionID.HasValue && maxlevel == FunctionRoleLevelID.Region)
                    {
                        return PointUserInfoHelper.IsRegionIDValid(uow, pointUserInfo, maxlevel, filterViewModel.RegionID.Value);
                    }
                    if(filterViewModel.DepartmentIDs.Any() || filterViewModel.LocationIDs.Any() || filterViewModel.OrganizationIDs.Any() || filterViewModel.RegionID.HasValue)
                    {
                        return false;
                    }
                }
            }

            //Todo
            return true;
        }

        public static IQueryable<FlowInstanceReportValues> ApplyFilterViewModel(this IQueryable<FlowInstanceReportValues> query, FilterViewModel filterViewModel, FlowDirection flowDirection, PointUserInfo pointUserInfo, bool applyDateTimeFilter = true, ReceiveStateFilter? receiveStateFilter = null)
        {
            if (VerifyFilter(filterViewModel, pointUserInfo) == false)
            {
                throw new PointSecurityException("U heeft geen bevoegdheid om dit rapport te zien.", ExceptionType.AccessDenied);
            }


            if (applyDateTimeFilter)
            {
                //Datum tijd fix (t/m verhaal....)
                var startdatetime = new DateTime(filterViewModel.StartDate.Year, filterViewModel.StartDate.Month, filterViewModel.StartDate.Day);
                var enddatetime = new DateTime(filterViewModel.EndDate.Year, filterViewModel.EndDate.Month, filterViewModel.EndDate.Day).AddDays(1).AddSeconds(-1);
                query = query.Where(FlowInstanceReportValuesExpressions.ByDateRange(startdatetime, enddatetime));
            }

            //Flows
            if (filterViewModel.FlowDefinitionIDs.Any())
            {
                query = query.Where(FlowInstanceReportValuesExpressions.ByFlowDefinitionIDs(filterViewModel.FlowDefinitionIDs));
            }

            //DossierStatus
            if (filterViewModel.DossierStatus.HasValue)
            {
                query = query.Where(FlowInstanceReportValuesExpressions.ByDossierStatus(filterViewModel.DossierStatus.Value));
            }

            //Rapprtage niveau
            var maxlevel = pointUserInfo.GetReportFunctionRoleLevelID();
            if (filterViewModel.DepartmentIDs.Any())
            {
                switch (receiveStateFilter)
                {
                    case ReceiveStateFilter.CancelledOrRefused:
                        query = query.Where(FlowInstanceReportValuesExpressions.RefusedInviteByDepartmentIDs(filterViewModel.DepartmentIDs, flowDirection));
                        break;
                    case ReceiveStateFilter.CancelledOrRefusedAndActive:
                        query = query.Where(FlowInstanceReportValuesExpressions.ActiveOrRefusedInviteByDepartmentIDs(filterViewModel.DepartmentIDs, flowDirection));
                        break;
                    default:
                        query = query.Where(FlowInstanceReportValuesExpressions.ActiveInviteByDepartmentIDs(filterViewModel.DepartmentIDs, flowDirection));
                        break;
                }
            }
            else if (filterViewModel.LocationIDs.Any())
            {
                switch (receiveStateFilter)
                {
                    case ReceiveStateFilter.CancelledOrRefused:
                        query = query.Where(FlowInstanceReportValuesExpressions.RefusedInviteByLocationIDs(filterViewModel.LocationIDs, flowDirection));
                        break;
                    case ReceiveStateFilter.CancelledOrRefusedAndActive:
                        query = query.Where(FlowInstanceReportValuesExpressions.ActiveOrRefusedInviteByLocationIDs(filterViewModel.LocationIDs, flowDirection));
                        break;
                    default:
                        query = query.Where(FlowInstanceReportValuesExpressions.ActiveInviteByLocationIDs(filterViewModel.LocationIDs, flowDirection));
                        break;
                }
            }
            else if (filterViewModel.OrganizationIDs.Any())
            {
                switch (receiveStateFilter)
                {
                    case ReceiveStateFilter.CancelledOrRefused:
                        query = query.Where(FlowInstanceReportValuesExpressions.RefusedInviteByOrganizationIDs(filterViewModel.OrganizationIDs, flowDirection));
                        break;
                    case ReceiveStateFilter.CancelledOrRefusedAndActive:
                        query = query.Where(FlowInstanceReportValuesExpressions.ActiveOrRefusedInviteByOrganizationIDs(filterViewModel.OrganizationIDs, flowDirection));
                        break;
                    default:
                        query = query.Where(FlowInstanceReportValuesExpressions.ActiveInviteByOrganizationIDs(filterViewModel.OrganizationIDs, flowDirection));
                        break;
                }
            }
            else if (filterViewModel.RegionID.HasValue)
            {
                switch (receiveStateFilter)
                {
                    case ReceiveStateFilter.CancelledOrRefused:
                        query = query.Where(FlowInstanceReportValuesExpressions.RefusedInviteByRegionID(filterViewModel.RegionID.Value, flowDirection, filterViewModel.OrganizationTypeIDs));
                        break;
                    case ReceiveStateFilter.CancelledOrRefusedAndActive:
                        query = query.Where(FlowInstanceReportValuesExpressions.ActiveOrRefusedInviteByRegionID(filterViewModel.RegionID.Value, flowDirection, filterViewModel.OrganizationTypeIDs));
                        break;
                    default:
                        query = query.Where(FlowInstanceReportValuesExpressions.ActiveInviteByRegionID(filterViewModel.RegionID.Value, flowDirection, filterViewModel.OrganizationTypeIDs));
                        break;
                }
            }
            else
            {
                //Als niks is opgegeven dan filter wat je maximaal mag zien                
                if (maxlevel == FunctionRoleLevelID.Region)
                {
                    switch (receiveStateFilter)
                    {
                        case ReceiveStateFilter.CancelledOrRefused:
                            query = query.Where(FlowInstanceReportValuesExpressions.RefusedInviteByRegionID(pointUserInfo.Region.RegionID, flowDirection, filterViewModel.OrganizationTypeIDs));
                            break;
                        case ReceiveStateFilter.CancelledOrRefusedAndActive:
                            query = query.Where(FlowInstanceReportValuesExpressions.ActiveOrRefusedInviteByRegionID(pointUserInfo.Region.RegionID, flowDirection, filterViewModel.OrganizationTypeIDs));
                            break;
                        default:
                            query = query.Where(FlowInstanceReportValuesExpressions.ActiveInviteByRegionID(pointUserInfo.Region.RegionID, flowDirection, filterViewModel.OrganizationTypeIDs));
                            break;
                    }
                }
                else if (maxlevel == FunctionRoleLevelID.Organization)
                {
                    switch (receiveStateFilter)
                    {
                        case ReceiveStateFilter.CancelledOrRefused:
                            query = query.Where(FlowInstanceReportValuesExpressions.RefusedInviteByOrganizationIDs(pointUserInfo.EmployeeOrganizationIDs.ToArray(), flowDirection));
                            break;
                        case ReceiveStateFilter.CancelledOrRefusedAndActive:
                            query = query.Where(FlowInstanceReportValuesExpressions.ActiveOrRefusedInviteByOrganizationIDs(pointUserInfo.EmployeeOrganizationIDs.ToArray(), flowDirection));
                            break;
                        default:
                            query = query.Where(FlowInstanceReportValuesExpressions.ActiveInviteByOrganizationIDs(pointUserInfo.EmployeeOrganizationIDs.ToArray(), flowDirection));
                            break;
                    }
                }
                else if (maxlevel == FunctionRoleLevelID.Location)
                {
                    switch (receiveStateFilter)
                    {
                        case ReceiveStateFilter.CancelledOrRefused:
                            query = query.Where(FlowInstanceReportValuesExpressions.RefusedInviteByLocationIDs(pointUserInfo.EmployeeLocationIDs.ToArray(), flowDirection));
                            break;
                        case ReceiveStateFilter.CancelledOrRefusedAndActive:
                            query = query.Where(FlowInstanceReportValuesExpressions.ActiveOrRefusedInviteByLocationIDs(pointUserInfo.EmployeeLocationIDs.ToArray(), flowDirection));
                            break;
                        default:
                            query = query.Where(FlowInstanceReportValuesExpressions.ActiveInviteByLocationIDs(pointUserInfo.EmployeeLocationIDs.ToArray(), flowDirection));
                            break;
                    }
                }
                else if (maxlevel == FunctionRoleLevelID.Department)
                {
                    switch (receiveStateFilter)
                    {
                        case ReceiveStateFilter.CancelledOrRefused:
                            query = query.Where(FlowInstanceReportValuesExpressions.RefusedInviteByDepartmentIDs(pointUserInfo.EmployeeDepartmentIDs.ToArray(), flowDirection));
                            break;
                        case ReceiveStateFilter.CancelledOrRefusedAndActive:
                            query = query.Where(FlowInstanceReportValuesExpressions.ActiveOrRefusedInviteByDepartmentIDs(pointUserInfo.EmployeeDepartmentIDs.ToArray(), flowDirection));
                            break;
                        default:
                            query = query.Where(FlowInstanceReportValuesExpressions.ActiveInviteByDepartmentIDs(pointUserInfo.EmployeeDepartmentIDs.ToArray(), flowDirection));
                            break;
                    }
                }
            }

            //RB heeft in het geheel geen toegang wanneer de versturende organisatie zich buiten haar regio bevindt.
            if (maxlevel == FunctionRoleLevelID.Region)
            {
                query = query.Where(fi => fi.FlowInstance.StartedByOrganization.RegionID == pointUserInfo.Region.RegionID);
            }
            //Projecten
            if (filterViewModel.OrganizationProjects.Any())
            {
                query = query.Where(FlowInstanceReportValuesExpressions.ByTags(filterViewModel.OrganizationProjects));
            }

            return query;
        }
    }
}
