﻿using System.Collections.Generic;
using System.Linq;
using Point.Database.CacheModels;
using Point.Database.Extensions;
using Point.Database.Models;
using Point.Database.Models.ViewModels;

namespace Point.Business.Logic
{
    public class FlowPhaseViewBL
    {
        public static List<FlowPhaseViewModel> FromModelList(List<PhaseDefinitionCache> definitions, List<PhaseInstance> instances)
        {
            List<FlowPhaseViewModel> output = new List<FlowPhaseViewModel>();
            foreach (PhaseInstance instance in instances)
            {
                PhaseDefinitionCache definition = definitions.Where(def => def.PhaseDefinitionID == instance.PhaseDefinitionID).FirstOrDefault();
                var viewmodel = FromModel(definition, instance);
                if (viewmodel == null) continue;

                output.Add(viewmodel);
            }

            var restDefinitions = definitions.Where(it => instances.Select(inst=> inst.PhaseDefinition.Phase).Contains(it.Phase) == false);
            foreach (var restDefinition in restDefinitions)
            {
                var viewmodel = FromModel(restDefinition, null);
                if (viewmodel == null) continue;

                output.Add(viewmodel);
            }

            return output;
        }

        public static FlowPhaseViewModel FromModel(PhaseDefinitionCache definition, PhaseInstance instance)
        {
            if (definition == null) return null;

            var state = instance != null ? (FlowPhaseViewModel.PhaseState)instance.Status : FlowPhaseViewModel.PhaseState.NotActive;
            FlowPhaseViewModel viewmodel = new FlowPhaseViewModel()
            {
                FlowDefinitionID = definition.FlowDefinitionID,
                PhaseDefinitionID = definition.PhaseDefinitionID,
                PhaseInstanceID = state > FlowPhaseViewModel.PhaseState.NotActive ? instance.PhaseInstanceID : (int?)null,
                PhaseDefinitionEndFlow = definition.EndFlow,
                PhaseNum = definition.Phase,
                PhaseName = definition.PhaseName,
                PhaseDescription = definition.Description,
                State = state,
                StateDate = state == FlowPhaseViewModel.PhaseState.Done ? instance.RealizedDate : null,
                LastUpdateBy = state == FlowPhaseViewModel.PhaseState.Done && instance.RealizedByEmployee != null ? (instance.RealizedByEmployee.FullName() + " (" + instance.RealizedByEmployee.PhoneNumber + ")") : "",
                Inactive = (instance != null ? instance.Cancelled : false),
                Executor = definition.Executor ?? "",
                FormTypeID = definition.FormTypeID
            };

            return viewmodel;
        }
    }
}
