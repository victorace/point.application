﻿using System;
using System.Collections.Generic;
using System.Linq;
using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Repository;
using Point.Infrastructure.Caching;

namespace Point.Business.Logic
{
    public class GeneralActionPhaseBL
    {
        private static IEnumerable<GeneralActionPhase> getGeneralActionPhases(IUnitOfWork uow, bool usecache = true)
        {
            string cachestring = "GeneralActionPhase_Global";
            IList<GeneralActionPhase> generalactionphases = null;

            if (usecache)
                generalactionphases = CacheService.Instance.Get<IList<GeneralActionPhase>>(cachestring);

            if (generalactionphases == null)
            {
                generalactionphases = uow.GeneralActionPhaseRepository.Get(gap => true, null, "GeneralAction,PhaseDefinition").ToList();
                if (generalactionphases.Any())
                    CacheService.Instance.Insert(cachestring, generalactionphases, "CacheLong");
            }
            return generalactionphases;
        }
        
        public static IEnumerable<GeneralActionPhase> GetByPhaseDefinitionID(IUnitOfWork uow, int phaseDefinitionID, int? organizationTypeID = null)
        {
            return getGeneralActionPhases(uow).Where(gap => gap.PhaseDefinitionID == phaseDefinitionID && (gap.OrganizationTypeID == organizationTypeID || gap.OrganizationTypeID == null));
        }

        public static IEnumerable<GeneralActionPhase> GetByGeneralActionID(IUnitOfWork uow, int generalActionID)
        {
            return getGeneralActionPhases(uow).Where(gap => gap.GeneralActionID == generalActionID);
        }
    }
}
