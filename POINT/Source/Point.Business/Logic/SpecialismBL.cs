﻿using System.Collections.Generic;
using System.Linq;
using Point.Database.Models;
using Point.Database.Repository;
using Point.Database.Context;

namespace Point.Business.Logic
{
    public class SpecialismBL
    {
        public static Specialism GetBySpecialismID(IUnitOfWork uow, int specialismID)
        {
            return uow.SpecialismRepository.GetByID(specialismID);
        }
        
        public static IEnumerable<Specialism> GetByOrganizationTypeID(IUnitOfWork uow, int organizationtypeID)
        {
            return uow.SpecialismRepository.Get(spec => spec.OrganizationTypeID == organizationtypeID && spec.Inactive != true).OrderBy(spec=>spec.Name);
        }
    }
}
