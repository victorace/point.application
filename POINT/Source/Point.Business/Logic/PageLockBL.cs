﻿using System;
using System.Linq;
using Point.Business.Helper;
using Point.Database.Models;
using Point.Database.Repository;

namespace Point.Business.Logic
{
    public class PageLockBL
    {
        public static PageLock GetByID(IUnitOfWork uow, int pageLockID)
        {
            return uow.PageLockRepository.Get(pl => pl.PageLockID == pageLockID).FirstOrDefault();
        }

        public static PageLock GetByTransferIDAndFormType(IUnitOfWork uow, int transferID, FormType formType)
        {
            if (formType != null && formType.HasPageLocking)
            {
                return uow.PageLockRepository.Get(pl => pl.TransferID == transferID && pl.URL == formType.URL).OrderByDescending(pl => pl.TimeStamp).FirstOrDefault();
            }

            return null;
        }

        public static void DeleteByEmployeeID(IUnitOfWork uow, int employeeID)
        {
            var pageLocks = uow.PageLockRepository.Get(pl => pl.EmployeeID == employeeID);
            foreach (var pageLock in pageLocks)
            {
                uow.PageLockRepository.Delete(pageLock);
            }
            uow.Save();
        }

        public static void DeleteByPageLockID(IUnitOfWork uow, int pagelockid)
        {
            uow.PageLockRepository.Delete(it => it.PageLockID == pagelockid);
            uow.Save();
        }

        public static void DeleteBySessionID(IUnitOfWork uow, string sessionID)
        {
            uow.PageLockRepository.Delete(pl => pl.SessionID == sessionID);
            uow.Save();
        }

        public static PageLock Insert(IUnitOfWork uow, int transferID, FormType formType, string sessionID)
        {
            var pageLock = new PageLock()
            {
                TransferID = transferID,
                EmployeeID = PointUserInfoHelper.GetPointUserInfo(uow).Employee.EmployeeID,
                URL = formType.URL,
                SessionID = sessionID,
                TimeStamp = DateTime.Now
            };

            uow.PageLockRepository.Insert(pageLock);
            uow.Save();

            return pageLock;
        }
    }
}
