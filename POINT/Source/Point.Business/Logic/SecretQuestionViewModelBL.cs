﻿using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Exceptions;
using System.Web.Security;

namespace Point.Business.Logic
{
    public class SecretQuestionViewModelBL
    {
        public static void Save(IUnitOfWork uow, SecretQuestionViewModel viewmodel)
        {
            var dbmodel = EmployeeBL.GetByID(uow, viewmodel.EmployeeID);
            if (dbmodel != null)
            {
                if (Membership.ValidateUser(dbmodel.aspnet_Users.UserName, viewmodel.PasswordCheck))
                {
                    dbmodel.EmailAddress = viewmodel.EmailAdress;         
                    
                    var member = Membership.GetUser(dbmodel.UserId);
                    member.ChangePasswordQuestionAndAnswer(viewmodel.PasswordCheck, viewmodel.SecretQuestion, viewmodel.SecretQuestionAnswer);
                    
                    uow.Save();
                }
                else
                {
                    throw new PointSecurityException("Het opgegeven wachtwoord is niet correct", ExceptionType.Default);
                }
            }
        }

        public static SecretQuestionViewModel FromModel(Employee employee)
        {
            var member = Membership.GetUser(employee.UserId);

            var viewmodel = new SecretQuestionViewModel();

            
            viewmodel.EmployeeID = employee.EmployeeID;
            viewmodel.EmailAdress = employee.EmailAddress;
            viewmodel.SecretQuestion = member.PasswordQuestion;
            viewmodel.SecretQuestionEnabled = employee.DefaultDepartment?.Location?.Organization?.SSO != true;

            return viewmodel;
        }
    }
}
