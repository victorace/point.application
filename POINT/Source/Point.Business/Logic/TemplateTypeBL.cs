﻿using Point.Database.Models;
using Point.Database.Repository;
using System.Collections.Generic;

namespace Point.Business.Logic
{
    public class TemplateTypeBL
    {
        public static IEnumerable<TemplateType> GetAll(IUnitOfWork uow)
        {
            return uow.TemplateTypeRepository.GetAll();
        }
    }
}
