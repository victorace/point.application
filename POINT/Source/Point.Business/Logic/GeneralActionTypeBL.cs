﻿using System.Collections.Generic;
using System.Linq;
using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Repository;
using Point.Infrastructure.Caching;

namespace Point.Business.Logic
{
    public class GeneralActionTypeBL
    {
        private static IEnumerable<GeneralActionType> getGeneralActionTypes(IUnitOfWork uow, bool usecache = true)
        {
            string cachestring = "GeneralActionType_Global";
            IList<GeneralActionType> generalactiontypes = null;

            if (usecache)
                generalactiontypes = CacheService.Instance.Get<IList<GeneralActionType>>(cachestring);

            if (generalactiontypes == null)
            {
                generalactiontypes = uow.GeneralActionTypeRepository.GetAll().ToList();
                if (generalactiontypes.Any())
                    CacheService.Instance.Insert(cachestring, generalactiontypes, "CacheLong");
            }
            return generalactiontypes;
        }


        public static GeneralActionType GetByName(IUnitOfWork uow, GeneralActionTypeName name)
        {
            return GetByName(uow, name.ToString());
        }

        public static GeneralActionType GetByName(IUnitOfWork uow, string name)
        {
            return getGeneralActionTypes(uow).Where(it => it.Name == name).FirstOrDefault();
        }
    }
}
