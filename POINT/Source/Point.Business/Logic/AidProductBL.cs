﻿using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace Point.Business.Logic
{
    public class AidProductBL
    {
        public static AidProduct GetProductByID(IUnitOfWork uow, int aidproductid)
        {
            return uow.AidProductRepository.GetByID(aidproductid);
        }

        public static IQueryable<AidProduct> GetProductsByIDs(IUnitOfWork uow, int[] aidproductids)
        {
            return uow.AidProductRepository.Get(it => aidproductids.Contains(it.AidProductID));
        }

        public static byte[] GetProductImageByID(IUnitOfWork uow, int aidproductid)
        {
            var image = uow.AidProductRepository.Get(it => it.AidProductID == aidproductid).Select(it => it.ImageData).First();

            if (image == null)
            {
                ImageConverter converter = new ImageConverter();
                image = (byte[])converter.ConvertTo(Properties.Resources.no_img, typeof(byte[]));
            }

            return image;
        }
        
        public static List<AidProductGroup> GetGroupsByProductIDs(IUnitOfWork uow, int[] aidproductids)
        {
            return uow.AidProductGroupRepository.Get(it => it.AidProducts.Any(ap => aidproductids.Contains(ap.AidProductID))).ToList();
        }

       
        public static List<AidProductOrderItem> GetOrderItemsByFormSetVersionID(IUnitOfWork uow, int formsetversionid)
        {
            return uow.AidProductOrderItemRepository.Get(it => it.FormSetVersionID == formsetversionid).ToList();
        }

        public static List<AidProductOrderItem> GetOrderItemsByTransferID(IUnitOfWork uow, int transferid)
        {
            return uow.AidProductOrderItemRepository.Get(it => it.FormSetVersion.TransferID == transferid && !it.FormSetVersion.Deleted).ToList();
        }

        public static void SaveOrderItems(IUnitOfWork uow, List<AidProductOrderItemViewModel> orderitems, int formsetversionid)
        {
            //Delete any product in previous orders where the productid is no longer in the list of the viewmodel
            var neworderids = orderitems.Select(vmi => vmi.AidProductOrderItemID).ToArray();
            uow.AidProductOrderItemRepository.Delete(pi => pi.FormSetVersionID == formsetversionid && neworderids.Contains(pi.AidProductOrderItemID) == false);

            foreach (var viewmodel in orderitems)
            {
                var existingmodel = viewmodel.AidProductOrderItemID == 0 ? null : uow.AidProductOrderItemRepository.GetByID(viewmodel.AidProductOrderItemID);
                if (existingmodel == null)
                {
                    existingmodel = new AidProductOrderItem();
                    existingmodel.FormSetVersionID = formsetversionid;

                    uow.AidProductOrderItemRepository.Insert(existingmodel);
                }

                existingmodel.AidProductID = viewmodel.AidProductID;
                existingmodel.Quantity = viewmodel.Quantity;
            }

            uow.Save();
        }

        public static void SaveAnswer(IUnitOfWork uow, AidProductQuestionViewModel viewmodel, int uniqueFormKey, int? formsetversionid = null)
        {
            AidProductOrderAnswer existingmodel = GetProductOrderAnswer(uow, viewmodel.QuestionID, uniqueFormKey, formsetversionid);
            if (existingmodel == null)
            {
                existingmodel = new AidProductOrderAnswer()
                {
                    FormSetVersionID = formsetversionid,
                    QuestionID = viewmodel.QuestionID,
                    UniqueFormKey = uniqueFormKey,
                };
                uow.AidProductOrderAnswerRepository.Insert(existingmodel);
            }

            existingmodel.ProductGroupCode = viewmodel.ProductGroupCode;
            existingmodel.FormSetVersionID = formsetversionid;
            existingmodel.Value = viewmodel.CurrentValue;

            uow.Save();
        }

        public static void SaveAnswer(IUnitOfWork uow, AidProductAnswerViewModel viewmodel, int uniqueFormKey, int? formsetversionid = null)
        {
            AidProductOrderAnswer existingmodel = GetProductOrderAnswer(uow, viewmodel.QuestionID, uniqueFormKey, formsetversionid);
            if (existingmodel != null && viewmodel.Delete)
            {
                uow.AidProductOrderAnswerRepository.Delete(existingmodel);
                uow.Save();
                return;
            }
            if(existingmodel == null)
            {
                existingmodel = new AidProductOrderAnswer
                {
                    FormSetVersionID = formsetversionid,
                    QuestionID = viewmodel.QuestionID,
                    UniqueFormKey = uniqueFormKey
                };
                uow.AidProductOrderAnswerRepository.Insert(existingmodel);
            }

            existingmodel.ExtraItems = viewmodel.ExtraItems;
            existingmodel.ProductGroupCode = viewmodel.ProductGroupCode;
            existingmodel.ExtraItemsGroupCode = viewmodel.ExtraItemsGroupCode;
            existingmodel.Value = viewmodel.Value;

            uow.Save();
        }

        private static AidProductOrderAnswer GetProductOrderAnswer(IUnitOfWork uow, string questionID, int uniqueFormKey, int? formsetversionid)
        {
            AidProductOrderAnswer existingmodel = null;
            if (formsetversionid.HasValue && formsetversionid > 0)
            {
                existingmodel = uow.AidProductOrderAnswerRepository.FirstOrDefault(it => it.QuestionID == questionID && it.FormSetVersionID == formsetversionid);
            }
            return existingmodel ?? uow.AidProductOrderAnswerRepository.FirstOrDefault(it => it.QuestionID == questionID && it.UniqueFormKey == uniqueFormKey);
        }

        public static void DeleteAllAnswers(IUnitOfWork uow, int uniqueFormKey)
        {           
            uow.AidProductOrderAnswerRepository.Delete(it => it.UniqueFormKey == uniqueFormKey);
            uow.Save();
        }

        public static void UpdateAnswersSetFormSetVersionID(IUnitOfWork uow, int uniqueformkey, int formsetversionid)
        {
            var answers = uow.AidProductOrderAnswerRepository.Get(it => it.UniqueFormKey == uniqueformkey && (it.FormSetVersionID == null || it.FormSetVersionID < 0)).ToList();
            answers.ForEach(it => it.FormSetVersionID = formsetversionid);
            uow.Save();
        }

        public static List<AidProductOrderAnswer> GetAnswersByFormSetVersionID(IUnitOfWork uow, int formsetversionid)
        {
            return uow.AidProductOrderAnswerRepository.Get(it => it.FormSetVersionID == formsetversionid).ToList();

        }
        public static List<AidProductOrderAnswer> GetAnswers(IUnitOfWork uow, int uniqueformkey, int? formSetVersionID)
        {
            IQueryable<AidProductOrderAnswer> answers = null;
            if (formSetVersionID.HasValue && formSetVersionID > 0)
            {
                answers = uow.AidProductOrderAnswerRepository.Get(it => it.FormSetVersionID == formSetVersionID);
            }
            if (answers == null)
            {
                answers = uow.AidProductOrderAnswerRepository.Get(it => it.UniqueFormKey == uniqueformkey);
            }
            return answers.ToList();
        }

        public static List<Supplier> GetSuppliers(IUnitOfWork uow)
        {
            return uow.SupplierRepository.Get().ToList();
        }

        public static string GetOrderStatus(IUnitOfWork uow, int formSetVersionID, bool updateStatus = false)
        {
            return GetOrderStatusWithMessageIndication(uow, formSetVersionID, updateStatus).OrderStatus;
        }

        public static OrderStatusResult GetOrderStatusWithMessageIndication(IUnitOfWork uow, int formSetVersionID, bool updateStatus = false)
        {
            var OrderStatusResult = new OrderStatusResult();
            var orderStatus = DashboardMenuItemViewBL.EMPTY;

            var flowFieldValues = FlowFieldValueBL.GetByFlowFieldIDAndFormSetVersionID(uow, FlowFieldConstants.ID_OrderStatus, formSetVersionID).ToList();
            if (flowFieldValues.Any())
            {
                orderStatus = DashboardMenuItemViewBL.UNKNOWN;
                var flowFieldValue = flowFieldValues.First();
                var flowFieldValueOrderStatus = flowFieldValue.Value;
                if (!string.IsNullOrEmpty(flowFieldValueOrderStatus))
                {
                    var timeSinceLastStatusSet = new TimeSpan(0);
                    if (flowFieldValue.Timestamp.HasValue)
                    {
                        timeSinceLastStatusSet = DateTime.Now.Subtract(flowFieldValue.Timestamp.Value);
                    }

                    if (timeSinceLastStatusSet.TotalMinutes < 3)
                    {
                        OrderStatusResult.ShowStatusMessage = true;
                    }
                    if (flowFieldValueOrderStatus == FlowFieldConstants.Value_OrderBevestigd)
                    {
                        orderStatus = DashboardMenuItemViewBL.ORDER_CONFIRMED;
                    }
                    if (flowFieldValueOrderStatus == FlowFieldConstants.Value_OrderMislukt)
                    {
                        orderStatus = DashboardMenuItemViewBL.ORDER_FAILED;
                    }
                    if (flowFieldValueOrderStatus == FlowFieldConstants.Value_InBehandeling)
                    {
                        orderStatus = DashboardMenuItemViewBL.ORDER_PROCESSING;
                    }
                    if (flowFieldValueOrderStatus == FlowFieldConstants.Value_OrderVerstuurd)
                    {
                        orderStatus = DashboardMenuItemViewBL.SENT;

                        if (timeSinceLastStatusSet.TotalHours > 6)
                        {
                            //orderStatus = DashboardMenuItemViewBL.NEED_UPDATE;
                            OrderStatusResult.NeedOrderUpdate = true;
                        }
                    }
                }
            }

            OrderStatusResult.OrderStatus = orderStatus;

            return OrderStatusResult;
        }

        public class OrderStatusResult
        {
            public string OrderStatus { get; set; }
            public bool ShowStatusMessage { get; set; }
            public bool NeedOrderUpdate { get; set; }
        }


    }
}