﻿using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Exceptions;
using System.Linq;
using Point.ServiceBus.Models;

namespace Point.Business.Logic
{
    public class RelocateDossierViewModelBL
    {
        public static RelocateDossierViewModel GetViewModelByFlowInstanceID(IUnitOfWork uow, int flowinstanceid)
        {
            var flowinstance = FlowInstanceBL.GetByID(uow, flowinstanceid);
            var viewmodel = new RelocateDossierViewModel()
            {
                OrganizationTypeID = flowinstance.StartedByOrganization.OrganizationTypeID,
                FlowDefinitionID = flowinstance.FlowDefinitionID,
                FlowInstanceID = flowinstance.FlowInstanceID,
                TransferID = flowinstance.TransferID,
                SourceOrganizationName = flowinstance.StartedByOrganization.Name,
                SourceOrganizationID = flowinstance.StartedByOrganizationID.GetValueOrDefault(),
                SourceLocationName = flowinstance.StartedByDepartment.Location.Name,
                SourceLocationID = flowinstance.StartedByDepartment.LocationID,
                SourceDepartmentName = flowinstance.StartedByDepartment.Name,
                SourceDepartmentID = flowinstance.StartedByDepartmentID
            };

            var fvKamernummer = FlowFieldValueBL.GetByFlowInstanceIDAndFieldID(uow, flowinstance.FlowInstanceID, FlowFieldConstants.ID_Kamernummer).FirstOrDefault();
            if (fvKamernummer != null)
            {
                viewmodel.CanHaveNewDestinationRoomNumber = true;
                viewmodel.SourceRoomNumber = fvKamernummer.Value;
            }

            return viewmodel;
        }

        public static void RelocateToNewDepartment(IUnitOfWork uow, int flowinstanceid, RelocateDossierViewModel viewmodel, PointUserInfo pointuserinfo)
        {
            var flowinstance = FlowInstanceBL.GetByID(uow, flowinstanceid);
            // Update
            var logentry = LogEntryBL.Create(FlowFormType.RelocateDepartment, pointuserinfo);

            var newDepartment = DepartmentBL.GetByDepartmentID(uow, viewmodel.DestinationDepartmentID);

            if(newDepartment.LocationID != flowinstance.StartedByDepartment.LocationID)
            {
                throw new PointJustLogException("U kunt het dossier alleen verplaatsen binnen uw eigen Locatie. Gebruik Verplaatsen naar andere organisatie als u het dossier naar een andere organisatie/locatie wilt verplaatsen.");
            }

            ServiceBusBL.SendMessage("Notification", "RelocateToNewDepartment", new RelocateNotification() {
                TransferID = flowinstance.TransferID,
                EmployeeID = pointuserinfo.Employee.EmployeeID,
                OrganizationID = pointuserinfo.Organization.OrganizationID,
                SourceDepartmentID = flowinstance.StartedByDepartmentID,
                DestinationDepartmentID = viewmodel.DestinationDepartmentID,
                RelocateReason = viewmodel.ReasonForRelocation
            });

            flowinstance.StartedByDepartmentID = viewmodel.DestinationDepartmentID;

            var formsetversion = FormSetVersionBL.GetByTransferIDAndTypeID(uow, flowinstance.TransferID, (int)TypeID.FlowAanvraag).FirstOrDefault();
            if(formsetversion != null)
            {
                int formsetversionid = formsetversion.FormSetVersionID;
                FlowFieldValueBL.UpdateOrInsert(uow, flowinstance.FlowInstanceID, formsetversionid, FlowFieldConstants.ID_SourceOrganization, newDepartment.Location.OrganizationID.ToString(), logentry);
                FlowFieldValueBL.UpdateOrInsert(uow, flowinstance.FlowInstanceID, formsetversionid, FlowFieldConstants.ID_SourceOrganizationLocation, newDepartment.LocationID.ToString(), logentry);
                FlowFieldValueBL.UpdateOrInsert(uow, flowinstance.FlowInstanceID, formsetversionid, FlowFieldConstants.ID_SourceOrganizationDepartment, newDepartment.DepartmentID.ToString(), logentry);

                if (viewmodel.CanHaveNewDestinationRoomNumber)
                {
                    FlowFieldValueBL.UpdateOrInsert(uow, flowinstance.FlowInstanceID, formsetversionid, FlowFieldConstants.ID_Kamernummer, viewmodel.DestinationRoomNumber, logentry);
                }

                FormSetVersionBL.FillUpdateData(uow, formsetversion, logentry, pointuserinfo.Employee.FullName());
            }

            uow.Save(); // NOTE commits UoW (and updates RM etc.)
        }

        public static void RelocateToNewOrganization(IUnitOfWork uow, int flowinstanceid, RelocateDossierViewModel viewmodel, PointUserInfo pointuserinfo)
        {
            // Update
            var flowinstance = FlowInstanceBL.GetByID(uow, flowinstanceid);
            var logentry = LogEntryBL.Create(FlowFormType.RelocateOrganization, pointuserinfo);

            ServiceBusBL.SendMessage("Notification", "RelocateToNewOrganization", new RelocateNotification()
            {
                TransferID = flowinstance.TransferID,
                EmployeeID = pointuserinfo.Employee.EmployeeID,
                OrganizationID = pointuserinfo.Organization.OrganizationID,
                SourceDepartmentID = flowinstance.StartedByDepartmentID,
                DestinationDepartmentID = viewmodel.DestinationDepartmentID,
                RelocateReason = viewmodel.ReasonForRelocation
            });

            flowinstance.StartedByOrganizationID = viewmodel.DestinationOrganizationID;
            flowinstance.StartedByDepartmentID = viewmodel.DestinationDepartmentID;

            var formsetversion = FormSetVersionBL.GetByTransferIDAndTypeID(uow, flowinstance.TransferID, (int)TypeID.FlowAanvraag).FirstOrDefault();
            if (formsetversion != null)
            {
                var newdepartment = DepartmentBL.GetByDepartmentID(uow, viewmodel.DestinationDepartmentID);
                int formsetversionid = formsetversion.FormSetVersionID;
                FlowFieldValueBL.UpdateOrInsert(uow, flowinstance.FlowInstanceID, formsetversionid, FlowFieldConstants.ID_SourceOrganization, newdepartment.Location.OrganizationID.ToString(), logentry);
                FlowFieldValueBL.UpdateOrInsert(uow, flowinstance.FlowInstanceID, formsetversionid, FlowFieldConstants.ID_SourceOrganizationLocation, newdepartment.LocationID.ToString(), logentry);
                FlowFieldValueBL.UpdateOrInsert(uow, flowinstance.FlowInstanceID, formsetversionid, FlowFieldConstants.ID_SourceOrganizationDepartment, newdepartment.DepartmentID.ToString(), logentry);

                if (viewmodel.CanHaveNewDestinationRoomNumber)
                {
                    FlowFieldValueBL.UpdateOrInsert(uow, flowinstance.FlowInstanceID, formsetversionid, FlowFieldConstants.ID_Kamernummer, viewmodel.DestinationRoomNumber, logentry);
                }

                FormSetVersionBL.FillUpdateData(uow, formsetversion, logentry, pointuserinfo.Employee.FullName());
            }

            LoggingBL.FillLoggingWithID(uow, flowinstance.Transfer, logentry);

            uow.Save(); // NOTE commits UoW (and updates RM etc.)
        }
    }
}
