﻿using Point.Database.Extensions;
using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Extensions;
using System.Collections.Generic;
using System.Linq;

namespace Point.Business.Logic
{
    public static class TransferTaskViewBL
    {
        public static TransferTaskListViewModel GetByTransferID(IUnitOfWork uow, int transferid, int formtypeid,
            bool readOnly, bool ispartial)
        {
            var transfertasks = TransferTaskBL.GetByTransferID(uow, transferid, false).ToList();
            return TransferTaskViewBL.fromModel(transferid, formtypeid, transfertasks, readOnly, ispartial);
        }

        public static TransferTaskViewModel GetByTransferTaskID(IUnitOfWork uow, int transfertaskid,
            int formtypeid, bool readOnly)
        {
            var transfertask = TransferTaskBL.GetByTransferTaskID(uow, transfertaskid);
            return fromModel(transfertask, formtypeid, readOnly);
        }

        private static TransferTaskViewModel fromModel(TransferTask transfertask, int formtypeid, bool readOnly)
        {
            var completed = transfertask.Status == Status.Done;
            var vm = new TransferTaskViewModel()
            {
                TransferTaskID = transfertask.TransferTaskID,
                TransferID = transfertask.TransferID,
                FormTypeID = formtypeid,
                Description = transfertask.Description,
                Comments = transfertask.Comments,
                CreatedDate = transfertask.CreatedDate,
                CreatedByEmployee = transfertask.CreatedByEmployee.FullName(),
                DueDate = transfertask.DueDate,
                CompletedDate = transfertask.CompletedDate,
                CompletedByEmployee = transfertask.CompletedByEmployee.FullName(),
                Status = transfertask.Status,
                StatusDescription = transfertask.Status.GetDescription(),
                Completed = completed,
                IsReadOnly = readOnly,
            };

            return vm;
        }

        private static TransferTaskListViewModel fromModel(int transferid, int formtypeid,
            List<TransferTask> transfertasks,
            bool readOnly, bool ispartial)
        {
            var transfertaskviewmodel = new List<TransferTaskViewModel>();
            foreach (var transfertask in transfertasks)
            {
                transfertaskviewmodel.Add(fromModel(transfertask, formtypeid, readOnly));
            }

            var transfertasklistviewmodel = new TransferTaskListViewModel()
            {
                TransferID = transferid,
                IsReadOnly = readOnly,
                IsPartial = ispartial,
                TransferTasks = transfertaskviewmodel
            };

            return transfertasklistviewmodel;
        }
    }
}