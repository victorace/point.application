﻿using System.Collections.Generic;
using System.Linq;
using Point.Database.Models;
using Point.Database.Repository;

namespace Point.Business.Logic
{
    public class HealthAidDeviceBL
    {
        public IEnumerable<HealthAidDevice> GetAll(IUnitOfWork uow)
        {
            return uow.HealthAidDeviceRepository.GetAll().OrderBy(x => x.Sortorder);
        }
    }
}
