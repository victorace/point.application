﻿using Point.Business.BusinessObjects;
using Point.Business.Helper;
using Point.Database.Extensions;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Extensions;
using Point.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Security;

namespace Point.Business.Logic
{
    public class AutoCreateSetBL
    {
        public static AutoCreateSet GetPreferredByLocationOrDefault(IEnumerable<AutoCreateSet> autoCreateSets, string location)
        {
            var autoCreateSetLocationSet = autoCreateSets.Where(it => it.Location.Name == location);
            if (autoCreateSetLocationSet == null || !autoCreateSetLocationSet.Any())
            {
                autoCreateSetLocationSet = autoCreateSets.Where(it => it.IsDefaultForOrganization);
            }

            if(autoCreateSetLocationSet != null)
            {
                return autoCreateSetLocationSet.FirstOrDefault();
            }

            return null;
        }

        public static AutoCreateSet GetByAutoCreateCode(IEnumerable<AutoCreateSet> autoCreateSets, string autocreatecode)
        {
            if(string.IsNullOrEmpty(autocreatecode))
            {
                return null;
            }

            return autoCreateSets.FirstOrDefault(it => it.AutoCreateCode == autocreatecode);
        }


        public static IEnumerable<AutoCreateSet> GetSettingsCreateUserByOrganizationID(IUnitOfWork uow, int organizationID)
        {
            return uow.AutoCreateSetRepository.Get(scu => scu.Location.OrganizationID == organizationID).ToList();
        }
        
        private static string GetRandomHexCode()
        {
            Random rand = new Random((int)DateTime.Now.Ticks);
            var intnumber = rand.Next(4369, 65535);
            return intnumber.ToString("X");
        }

        public static AutoCreateSet CreateNewSet(IUnitOfWork uow, int locationid, string name, bool isdefault = false)
        {
            var code = GetRandomHexCode();

            var model = new AutoCreateSet() { LocationID = locationid, Name = name, IsDefaultForOrganization = isdefault, AutoCreateCode = code };
            uow.AutoCreateSetRepository.Insert(model);

            return model;
        }

        public static AutoCreateSet GetByID(IUnitOfWork uow, int autocreatesetid)
        {
            return uow.AutoCreateSetRepository.GetByID(autocreatesetid);
        }

        public static void UpdateExistingEmployees(IUnitOfWork uow, int autocreatesetid, LogEntry logentry)
        {
            var autocreateset = AutoCreateSetBL.GetByID(uow, autocreatesetid);

            var departments = uow.AutoCreateSetDepartmentRepository.Get(it => it.AutoCreateSetID == autocreatesetid, includeProperties: "Department").Select(it => it.Department).ToList();
            var departmentidlist = departments.Select(it => it.DepartmentID).OrderBy(it => it).ToArray();

            var employees = EmployeeBL.GetByAutoCreateSetID(uow, autocreatesetid);
            if(departmentidlist.Any() && employees.Any())
            {
                foreach (var employee in employees)
                {
                    //Fix readonly 
                    employee.ReadOnlyReciever = autocreateset.ReadOnlyReciever;
                    employee.ReadOnlySender = autocreateset.ReadOnlySender;

                    //Fix roles
                    if (employee.aspnet_Users != null && !employee.IsInRole(autocreateset.DefaultDossierLevel))
                    {
                        var previousPointFlowRole = Authorization.GetPointFlowRole(uow, employee.UserId);
                        if (previousPointFlowRole != Role.None)
                        {
                            Roles.RemoveUserFromRole(employee.aspnet_Users.UserName, previousPointFlowRole.ToString());
                        }

                        if (Enum.TryParse<Role>(autocreateset.DefaultDossierLevel, out Role defaultdossierlevel) && defaultdossierlevel != Role.None)
                        {
                            Roles.AddUserToRole(employee.aspnet_Users.UserName, autocreateset.DefaultDossierLevel);
                        }
                    }

                    var otherdepartments = departments.Where(it => it.DepartmentID != employee.DepartmentID);
                    var otherdepartmentidlist = otherdepartments.Select(it => it.DepartmentID).ToArray();
                    var employeeotherdepartmentidlist = employee.EmployeeDepartment.OrderBy(it => it.DepartmentID).Select(it => it.DepartmentID).ToArray();
                    
                    //Fix departments
                    if(!otherdepartmentidlist.SequenceEqual(employeeotherdepartmentidlist))
                    {
                        employee.EmployeeDepartment.Clear();

                        foreach (var otherdepartment in otherdepartments.ToList())
                        {
                            employee.EmployeeDepartment.Add(new EmployeeDepartment() { DepartmentID = otherdepartment.DepartmentID, ModifiedTimeStamp = logentry.Timestamp });
                        }
                    }
                }
            }

            uow.Save(logentry);
        }

        public static void Save(IUnitOfWork uow, AutoCreateSetViewModel viewmodel, LogEntry logentry, PointUserInfo pointUserInfo)
        {
            bool updateexistingemployees = false;

            var existing = GetByID(uow, viewmodel.AutoCreateSetID);
            if(existing == null)
            {
                existing = new AutoCreateSet() { LocationID = viewmodel.LocationID };
                uow.AutoCreateSetRepository.Insert(existing);
            } else
            {
                updateexistingemployees = true;
            }

            if (!String.IsNullOrEmpty(viewmodel.DefaultPassword))
            {
                if(viewmodel.CanWriteValue(v => v.DefaultPassword, pointUserInfo))
                {
                    viewmodel.DefaultPassword = MachineKey.Protect(Encoding.UTF8.GetBytes(Convert.ToString(viewmodel.DefaultPassword)), "DefaultPassword").ByteArrayToHexString();
                }
            }

            existing.WriteSecureValue(m => m.AutoCreateCode, viewmodel, v => v.AutoCreateCode, pointUserInfo);
            existing.WriteSecureValue(m => m.ChangePasswordByEmployee, viewmodel, v => v.ChangePasswordByEmployee, pointUserInfo);
            existing.WriteSecureValue(m => m.DefaultDossierLevel, viewmodel, v => v.DefaultDossierLevelString, pointUserInfo);
            existing.WriteSecureValue(m => m.DefaultEmailAddress, viewmodel, v => v.DefaultEmailAddress, pointUserInfo);
            existing.WriteSecureValue(m => m.DefaultGender, viewmodel, v => v.DefaultGender, pointUserInfo);
            existing.WriteSecureValue(m => m.DefaultPassword, viewmodel, v => v.DefaultPassword, pointUserInfo);
            existing.WriteSecureValue(m => m.DefaultPhoneNumber, viewmodel, v => v.DefaultPhoneNumber, pointUserInfo);
            existing.WriteSecureValue(m => m.IsDefaultForOrganization, viewmodel, v => v.IsDefaultForOrganization, pointUserInfo);
            existing.WriteSecureValue(m => m.Name, viewmodel, v => v.Name, pointUserInfo);
            existing.WriteSecureValue(m => m.UsernamePrefix, viewmodel, v => v.UsernamePrefix, pointUserInfo);
            existing.WriteSecureValue(m => m.ReadOnlyReciever, viewmodel, v => v.ReadOnlyReciever, pointUserInfo);
            existing.WriteSecureValue(m => m.ReadOnlySender, viewmodel, v => v.ReadOnlySender, pointUserInfo);


            if (existing.AutoCreateSetDepartment.Any())
            {
                foreach (var item in existing.AutoCreateSetDepartment.ToList())
                {
                    uow.DatabaseContext.Entry(item).State = System.Data.Entity.EntityState.Deleted;
                }
            }

            if(viewmodel.DefaultDepartmentIDs != null && viewmodel.DefaultDepartmentIDs.Any())
            {
                foreach (int departmentid in viewmodel.DefaultDepartmentIDs)
                {
                    existing.AutoCreateSetDepartment.Add(new AutoCreateSetDepartment() { DepartmentID = departmentid, ModifiedTimeStamp = logentry.Timestamp });
                }
            }

            LoggingBL.FillLoggable(uow, existing, logentry);

            var location = LocationBL.GetByID(uow, existing.LocationID);
            location.ModifiedTimeStamp = logentry.Timestamp;

            uow.Save(logentry);

            if(updateexistingemployees)
            {
                UpdateExistingEmployees(uow, existing.AutoCreateSetID, logentry);
            }
        }

        public static AutoCreateSetViewModel FromModel(AutoCreateSet model, Location location, Role maxDossierRole)
        {
            Role defaultDossierLevel = Enum.TryParse(model.DefaultDossierLevel, out defaultDossierLevel) ? defaultDossierLevel : Role.None;

            var encodedpassword = model.DefaultPassword;
            string decodedpassword = null;

            if (!String.IsNullOrEmpty(encodedpassword))
            {
                try
                {
                    byte[] passwordbytes = encodedpassword.HexStringToByteArray();
                    decodedpassword = Encoding.UTF8.GetString(MachineKey.Unprotect(passwordbytes, "DefaultPassword"));
                }
                catch
                {
                    decodedpassword = encodedpassword;
                }
            }

            var allowedDossierRoles = Enum.GetValues(typeof(Role)).Cast<Role>().Where(r => r <= maxDossierRole).ToList();

            var viewmodel = new AutoCreateSetViewModel()
            {
                AutoCreateSetID = model.AutoCreateSetID,
                LocationID = model.LocationID,
                AutoCreateCode = model.AutoCreateCode,
                ChangePasswordByEmployee = model.ChangePasswordByEmployee,
                DefaultDossierLevel = defaultDossierLevel,
                AllowedDossierLevels = allowedDossierRoles,
                DefaultEmailAddress = model.DefaultEmailAddress,
                DefaultGender = model.DefaultGender,
                DefaultPassword = decodedpassword,
                DefaultPhoneNumber = model.DefaultPhoneNumber,
                IsDefaultForOrganization = model.IsDefaultForOrganization,
                Name = model.Name,
                UsernamePrefix = model.UsernamePrefix,
                DefaultDepartmentIDs = model.AutoCreateSetDepartment.Select(it => it.DepartmentID).ToList(),
                LocationDepartments = location.Department.Where(it => it.Inactive != true).ToList(),
                ReadOnlyReciever = model.ReadOnlyReciever,
                ReadOnlySender = model.ReadOnlySender
            };

            return viewmodel;
        }

        public static List<AutoCreateSetViewModel> FromModelList(List<AutoCreateSet> modellist, Location location, Role maxDossierRole)
        {
            return modellist.Select(it => FromModel(it, location, maxDossierRole)).ToList();
        }
    }
}
