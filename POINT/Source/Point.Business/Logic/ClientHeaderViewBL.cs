﻿using Point.Database.Extensions;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Helpers;
using Point.Models.Enums;

namespace Point.Business.Logic
{
    public class ClientHeaderViewBL
    {
        public static ClientHeaderViewModel GetViewModel(IUnitOfWork uow, int transferID, int? organizationID)
        {
            if (transferID == -1)
            {
                return null;
            }

            var flowinstance = FlowInstanceBL.GetByTransferID(uow, transferID);
            if (flowinstance == null)
            {
                return null;
            }

            var client = ClientBL.GetByTransferID(uow, transferID);
            if (client == null)
            {
                return null;
            }

            var startedbyorganization = flowinstance.StartedByOrganization;
            var isanonymous = ClientBL.ShowAnonymous(flowinstance, organizationID);
            string webserver = ConfigHelper.GetAppSettingByName<string>("WebServer").TrimEnd('/');

            var vm = new ClientHeaderViewModel()
            {
                Logo = startedbyorganization?.OrganizationLogoID != null ? (webserver + "/OrganizationLogo/Logo?organizationlogoid=" + startedbyorganization?.OrganizationLogoID) : "",
                ClientID = client.ClientID,
                Name = isanonymous ? flowinstance.Transfer.ClientAnonymousName() : client.LastName,
                Gender = client.FullGender(),
                CivilServiceNumber = isanonymous ? "Anoniem" : client.CivilServiceNumber,
                BirthDate = client.BirthDate,
                IsCopy = flowinstance.Transfer.OriginalTransferID.HasValue,
                OriginalTransferID = flowinstance.Transfer.OriginalTransferID,
                FlowInstance = flowinstance,
                RelatedTransferID = flowinstance.RelatedTransferID,
                HasRelatedTransfer = flowinstance.RelatedTransferID.HasValue && flowinstance.RelatedTransferID.Value != (int)TakeOverRelatedTransferStatus.DoNotTakeOver
            };

            return vm;
        }     
    }
}
