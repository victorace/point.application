﻿using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure;
using Point.Infrastructure.Extensions;
using System;

namespace Point.Business.Logic
{
    public class TemplateBL
    {
        public static Template FromModel(IUnitOfWork uow, TemplateViewModel templateviewmodel)
        {
            var model = uow.TemplateRepository.GetByID(templateviewmodel.TemplateID);
            if(model == null)
            {
                model = new Template();
                uow.TemplateRepository.Insert(model);
            }

            model.TemplateTypeID = (int)templateviewmodel.TemplateTypeID;
            model.OrganizationID = templateviewmodel.OrganizationID;
            model.TemplateText = templateviewmodel.TemplateText;

            return model;
        }

        public static Template GetTemplate(IUnitOfWork uow, int organizationid, int templatetypeid)
        {
            return uow.TemplateRepository.FirstOrDefault(t => t.OrganizationID == organizationid && t.TemplateTypeID == templatetypeid);
        }

        public static string GetTextOrDefault(IUnitOfWork uow, int? organizationid, int templatetypeid)
        {
            var templatetext = string.Empty;

            var template = uow.TemplateRepository.FirstOrDefault(t => t.OrganizationID == (organizationid ?? 0) && t.TemplateTypeID == templatetypeid);
            if (template != null)
            {
                templatetext = template.TemplateText;
            }
            else
            {
                templatetext = TemplateDefaultBL.GetByTemplateTypeID(uow, templatetypeid)?.TemplateDefaultText;
            }

            return templatetext;
        }

        public static void Save(IUnitOfWork uow, Template template)
        {
            try
            {
                if (template.TemplateID == 0)
                {
                    uow.TemplateRepository.Insert(template);
                }

                uow.Save();
            }
            catch(Exception ex)
            {
                ExceptionHelper.SendMessageException(ex);
            }
        }

        public static void Delete(IUnitOfWork uow, int templateid)
        {
            try
            {
                uow.TemplateRepository.Delete(templateid);
                uow.Save();
            }
            catch (Exception ex)
            {
                ExceptionHelper.SendMessageException(ex);
            }
        }
    }
}
