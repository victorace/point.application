﻿using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Extensions;

namespace Point.Business.Logic
{
    public class ContactInformationViewBL
    {
        public static ContactInformationViewModel FromModel(IUnitOfWork uow, Department department)
        {
            if (department == null || department == default(Department))
                return new ContactInformationViewModel();

            var model = new ContactInformationViewModel();
            model.Merge<ContactInformationViewModel>(department.Location);

            model.Name = department.Name;
            model.FullName = department.FullName();
            model.EmailAddress = department.EmailAddress;
            model.PhoneNumber = department.PhoneNumber;
            model.FaxNumber = department.FaxNumber;

            return model;
        }

        public static ContactInformationViewModel FromModel(IUnitOfWork uow, Location location)
        {
            if (location == null || location == default(Location))
                return new ContactInformationViewModel();

            var model = new ContactInformationViewModel();
            model.Merge<ContactInformationViewModel>(location);
            return model;
        }
    }
}
