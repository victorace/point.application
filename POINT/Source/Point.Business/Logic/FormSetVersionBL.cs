﻿using Point.Database.Extensions;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Point.Business.Logic
{
    public static class FormSetVersionBL
    {
        public static FormSetVersion CreateFormSetVersion(IUnitOfWork uow, int transferID, int formTypeID, int phaseInstanceID,
            PointUserInfo pointUserInfo, LogEntry logentry, string description = null, int? parentFormSetVersionID = null, int? createdbyemployeeid = null)
        {
            FormSetVersion formsetversion = new FormSetVersion
            {
                TransferID = transferID,
                FormTypeID = formTypeID,
                PhaseInstanceID = phaseInstanceID,
                Description = description
            };

            int createdbyid = pointUserInfo.Employee.EmployeeID;
            string createdby = pointUserInfo.Employee.FullName();
            if (createdbyemployeeid.HasValue)
            {
                var employee = EmployeeBL.GetByID(uow, createdbyemployeeid.Value);
                if (employee != null)
                {
                    createdbyid = employee.EmployeeID;
                    createdby = employee.FullName();
                }
            }
            formsetversion.CreatedByID = createdbyid;
            formsetversion.CreatedBy = createdby;
            formsetversion.CreateDate = DateTime.Now;

            formsetversion.Deleted = false;

            LoggingBL.FillLoggingWithID(uow, formsetversion, logentry);

            uow.FormSetVersionRepository.Insert(formsetversion);

            return formsetversion;
        }

        public static FormSetVersion GetByID(IUnitOfWork uow, int formsetVersionID)
        {
            return uow.FormSetVersionRepository.GetByID(formsetVersionID);
        }

        public static FormSetVersion GetByTransferIDAndFormsetVersionID(IUnitOfWork uow, int transferID, int formsetVersionID)
        {
            return uow.FormSetVersionRepository.Get(fsv => fsv.TransferID == transferID && fsv.FormSetVersionID == formsetVersionID && fsv.Deleted == false).FirstOrDefault();
        }

        public static IQueryable<FormSetVersion> GetByTransferID(IUnitOfWork uow, int transferID)
        {
            return uow.FormSetVersionRepository.Get(fsv => fsv.TransferID == transferID && fsv.Deleted == false);
        }

        public static IQueryable<FormSetVersion> GetByTransferIDs(IUnitOfWork uow, IQueryable<int> transferIDs)
        {
            return uow.FormSetVersionRepository.Get(fsv => transferIDs.Contains(fsv.TransferID));
        }

        public static IEnumerable<FormSetVersion> GetByPhaseInstanceID(IUnitOfWork uow, int phaseInstanceID)
        {
            return uow.FormSetVersionRepository.Get(fsv => fsv.PhaseInstanceID == phaseInstanceID && fsv.Deleted == false);
        }

        public static IEnumerable<FormSetVersion> GetByTransferIDAndTypeID(IUnitOfWork uow, int transferID, int typeID)
        {
            return uow.FormSetVersionRepository.Get(fsv => fsv.TransferID == transferID && fsv.FormType.TypeID == typeID && fsv.Deleted == false);
        }

        public static IEnumerable<FormSetVersion> GetByTransferIDAndFormTypeID(IUnitOfWork uow, int transferID, int formtypeID)
        {
            return uow.FormSetVersionRepository.Get(fsv => fsv.TransferID == transferID && fsv.FormTypeID == formtypeID && fsv.Deleted == false);
        }
        
        public static IEnumerable<FormSetVersion> GetByTransferIDAndFormTypeIDs(IUnitOfWork uow, int transferID, IEnumerable<int> formtypeIDs)
        {
            return uow.FormSetVersionRepository.Get(fsv => fsv.TransferID == transferID && formtypeIDs.Contains(fsv.FormTypeID) && fsv.Deleted == false);
        }

        public static FormSetVersion DeleteByIDAndTransferID(IUnitOfWork uow, int formSetVersionID, int transferID, LogEntry logEntry)
        {
            var formsetversion = uow.FormSetVersionRepository.Get(fsv => fsv.TransferID == transferID && fsv.FormSetVersionID == formSetVersionID).FirstOrDefault();

            if (formsetversion != null)
            {
                formsetversion.Deleted = true;
                LoggingBL.FillLoggingWithID(uow, formsetversion, logEntry);
                uow.Save();
            }

            return formsetversion;
        }

        public static void FillUpdateData(IUnitOfWork uow, FormSetVersion formSetVersion, LogEntry logEntry, string employeename, string description = null)
        {
            if (formSetVersion != null)
            {
                formSetVersion.UpdatedBy = employeename;
                if (description != null)
                {
                    formSetVersion.Description = description;
                }
                LoggingBL.FillLoggingWithID(uow, formSetVersion, logEntry, forceupdate: true);
            }
        }

        public static IEnumerable<FormSetVersion> GetByPhaseInstanceAndFormType(IUnitOfWork uow, int phaseInstanceID, int formtypeID)
        {
            return uow.FormSetVersionRepository.Get(fsv => fsv.PhaseInstanceID == phaseInstanceID && fsv.FormTypeID == formtypeID && fsv.Deleted == false);
        }

        public static IEnumerable<FormSetVersion> GetByTransferIDAndPhaseDefinitionID(IUnitOfWork uow, int transferID, int phaseDefinitionID)
        {
            return uow.FormSetVersionRepository.Get(fsv => fsv.TransferID == transferID && fsv.PhaseInstance.PhaseDefinitionID == phaseDefinitionID && !fsv.Deleted && !fsv.PhaseInstance.Cancelled);
        }

        public static FormSetVersion GetLastByTransferIDAndPhaseDefinitionID(IUnitOfWork uow, int transferID, int phaseDefinitionID)
        {
            var formsetversions = GetByTransferIDAndPhaseDefinitionID(uow, transferID, phaseDefinitionID);
            if (formsetversions == null || !formsetversions.Any())
            {
                return null;
            }
            return formsetversions.OrderByDescending(fsv => fsv.FormSetVersionID).FirstOrDefault();



        }

    }

    public static class FormSetVersionExtensions
    {
        public static string GetPrintName(this FormSetVersion formsetversion)
        {
            bool iscustomname = !String.IsNullOrEmpty(formsetversion.Description);
            return iscustomname ? formsetversion.Description : (formsetversion.FormType.PrintName ?? formsetversion.FormType.Name);
        }
    }
}
