﻿using Point.Database.Models;
using Point.Database.Repository;
using Point.Infrastructure.Caching;
using System.Collections.Generic;
using System.Linq;

namespace Point.Business.Logic
{
    public static class FieldReceivedValueMappingBL
    {
        private static IEnumerable<FieldReceivedValueMapping> GetAll(IUnitOfWork uow)
        {
            string cacheString = "FieldReceivedValueMapping_Global";

            IEnumerable<FieldReceivedValueMapping> mappings = CacheService.Instance.Get<IEnumerable<FieldReceivedValueMapping>>(cacheString);

            if (mappings == null)
            {
                mappings = uow.FieldReceivedValueMappingRepository.GetAll().ToList();
                CacheService.Instance.Insert(cacheString, mappings, "CacheLong");
            }

            return mappings;
        }

        public static FieldReceivedValueMapping MapValue(IUnitOfWork uow, string fieldName, string sourceValue)
        {
            var mappings = GetAll(uow);
            return mappings.FirstOrDefault(frmv =>
                frmv.FieldName.Equals(fieldName, System.StringComparison.InvariantCultureIgnoreCase) &&
                (frmv.SourceValue == sourceValue ||
                !string.IsNullOrEmpty(frmv.SourceValue) && frmv.SourceValue.Equals(sourceValue, System.StringComparison.InvariantCultureIgnoreCase)));
        }
    }
}
