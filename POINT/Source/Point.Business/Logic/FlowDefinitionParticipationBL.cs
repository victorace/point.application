﻿using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Repository;
using Point.Infrastructure.Caching;
using System.Collections.Generic;
using System.Linq;

namespace Point.Business.Logic
{
    public class FlowDefinitionParticipationBL
    {
        public static void DeleteDefaultByLocationID(IUnitOfWork uow, int locationid)
        {
            uow.FlowDefinitionParticipationRepository.Delete(it => it.LocationID == locationid && it.DepartmentID == null);
        }

        public static IEnumerable<FlowDefinitionParticipation> GetByLocationID(IUnitOfWork uow, int locationid)
        {
            return uow.FlowDefinitionParticipationRepository.Get(it => it.LocationID == locationid && it.DepartmentID == null);
        }

        public static IEnumerable<FlowDefinitionParticipation> GetByOrganizationIDs(IUnitOfWork uow, List<int> organizationids)
        {
            return uow.FlowDefinitionParticipationRepository.Get(it => organizationids.Contains(it.Location.OrganizationID) && it.DepartmentID == null);
        }

        public static IEnumerable<FlowDefinitionParticipation> GetByLocationIDs(IUnitOfWork uow, List<int> locationids)
        {
            return uow.FlowDefinitionParticipationRepository.Get(it => locationids.Contains(it.LocationID) && it.DepartmentID == null);
        }

        public static void DeleteByDepartmentID(IUnitOfWork uow, int departmentid)
        {
            uow.FlowDefinitionParticipationRepository.Delete(it => it.DepartmentID == departmentid);
        }

        public static List<FlowDefinitionParticipation> GetByLocationIDAndDepartmentID(IUnitOfWork uow, int locationid, int departmentid, bool usecache = true)
        {
            string cacheKey = string.Concat("FlowDefinitionParticipation_", locationid, "_", departmentid);
            List<FlowDefinitionParticipation> result = usecache == false ? null : CacheService.Instance.Get<List<FlowDefinitionParticipation>>(cacheKey);
            if (result == null)
            {
                result = uow.FlowDefinitionParticipationRepository.Get(it => (it.LocationID == locationid && it.DepartmentID == null) || it.DepartmentID == departmentid, includeProperties: "FlowDefinition").GroupBy(it => it.FlowDefinitionID).Select(it => it.OrderByDescending(it2 => it2.DepartmentID).FirstOrDefault()).ToList();
                CacheService.Instance.Insert(cacheKey, result);
            }

            return result;
        }

        public static FlowDefinitionParticipation GetByLocationIDAndDepartmentIDAndFlowDefinitionID(IUnitOfWork uow, int locationid, int departmentid, FlowDefinitionID flowdefinitionid, bool usecache = true)
        {
            var tempresult = GetByLocationIDAndDepartmentID(uow, locationid, departmentid, usecache);
            if (tempresult == null) return null;

            return tempresult.FirstOrDefault(it => it.FlowDefinitionID == flowdefinitionid);
        }
    }
}
