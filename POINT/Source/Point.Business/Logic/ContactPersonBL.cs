﻿using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using System.Collections.Generic;
using System.Linq;

namespace Point.Business.Logic
{
    public class ContactPersonBL
    {
        public static ContactPerson GetByID(IUnitOfWork uow, int contactPersonID)
        {
            return uow.ContactPersonRepository.Get(cp => cp.ContactPersonID == contactPersonID).FirstOrDefault();
        }

        public static IEnumerable<ContactPerson> GetByClientID(IUnitOfWork uow, int clientID)
        {
            return uow.ContactPersonRepository.Get(cp => cp.ClientID == clientID);
        }

        public static void Insert(IUnitOfWork uow, ContactPersonViewModel contactPersonViewModel)
        {
            var contactPerson = new ContactPerson
            {
                ClientID = contactPersonViewModel.ClientID,
                PersonData = PersonDataBL.FromModel(uow, contactPersonViewModel),
                CIZRelationID = contactPersonViewModel.CIZRelationID
            };

            uow.ContactPersonRepository.Insert(contactPerson);

            uow.Save();
        }

        public static void Update(IUnitOfWork uow, ContactPersonViewModel contactPersonViewModel)
        {
            var contactPerson = GetByID(uow, contactPersonViewModel.ContactPersonID);

            contactPerson.CIZRelationID = contactPersonViewModel.CIZRelationID;
            contactPerson.PersonData = PersonDataBL.FromModel(uow, contactPersonViewModel);
            contactPerson.CIZRelationID = contactPerson.CIZRelationID;

            uow.Save();
        }

        public static bool Delete(IUnitOfWork uow, ContactPerson contactPerson)
        {
            var personData = PersonDataBL.GetByID(uow, contactPerson.PersonDataID);
            if (personData != null)
            {
                uow.PersonDataRepository.Delete(personData);
                uow.ContactPersonRepository.Delete(contactPerson);
                uow.Save();
                return true;
            }

            return false;
        }

        public static void CopyTo(IUnitOfWork uow, Client source, Client destination)
        {
            foreach (var contact in GetByClientID(uow, source.ClientID))
            {
                var copyPersonData = PersonDataBL.Copy(uow, contact.PersonData);
                var copyContactPerson = new ContactPerson()
                {
                    ClientID = destination.ClientID,
                    PersonData = copyPersonData,
                    CIZRelationID = contact.CIZRelationID
                };
                
                uow.ContactPersonRepository.Insert(copyContactPerson);
            }

            uow.Save();
        }
    }
}
