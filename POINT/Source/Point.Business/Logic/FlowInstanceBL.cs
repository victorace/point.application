﻿using Point.Business.BusinessObjects;
using Point.Business.Helper;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Exceptions;
using Point.Infrastructure.Extensions;
using Point.Models.Enums;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Point.Business.Logic
{
    public static class FlowInstanceBL
    {
        public static string GetActivePhaseTextByFlowInstance(IUnitOfWork uow, FlowInstance flowInstance)
        {
            if(flowInstance == null)
            {
                return "";
            }

            if(flowInstance.Interrupted)
            {
                return "TO (" + flowInstance.InterruptedDate.ToPointDateDisplay() + ")";
            }
            
            var activephases = PhaseInstanceBL.GetActiveByFlowInstanceID(uow, flowInstance.FlowInstanceID)
                .Where(pi => pi.PhaseDefinition.Phase > -1)
                .OrderBy(pi => pi.PhaseDefinition.Phase)
                .Select(pi => pi.PhaseDefinition.PhaseName)
                .ToList();

            return string.Join(",", activephases);
        }

        public static bool CanUseAnonymousClient(FlowDefinitionID flowDefinitionID)
        {
            return flowDefinitionID == FlowDefinitionID.ZH_VVT || flowDefinitionID == FlowDefinitionID.CVA;
        }

        private static void InternalSave(IUnitOfWork uow)
        {
            uow.Save();
        }

        public static FlowInstance GetByClientID(IUnitOfWork uow, int clientID)
        {
            return uow.FlowInstanceRepository.FirstOrDefault(it => it.Transfer.ClientID == clientID);
        }

        public static FlowInstance GetByClientDetails(IUnitOfWork uow, int organizationID, string patientNumber, string visitNumber, DateTime birthDate)
        {
            if (String.IsNullOrWhiteSpace(patientNumber))
            {
                return null;
            }

            var flowinstance = uow.FlowInstanceRepository
                .Get(f => !f.Deleted && f.StartedByOrganizationID == organizationID && 
                    f.Transfer.Client.PatientNumber == patientNumber &&
                    f.Transfer.Client.VisitNumber == visitNumber &&
                    DbFunctions.TruncateTime(f.Transfer.Client.BirthDate) == DbFunctions.TruncateTime(birthDate), 
                    includeProperties: "Transfer.Client")
                .OrderByDescending(f => f.Timestamp)
                .FirstOrDefault();

            return flowinstance;
        }

        public static decimal GetLastPhaseNumByFlowInstanceID(IUnitOfWork uow, int flowInstanceID)
        {
            return uow.PhaseInstanceRepository.Get(it => it.FlowInstanceID == flowInstanceID && !it.Cancelled).Select(it => it.PhaseDefinition.Phase).OrderByDescending(it => it).FirstOrDefault();
        }

        public static bool AccessByAccessGroup(IUnitOfWork uow, FlowInstance flowInstance, string accessGroup, PointUserInfo pointUserInfo)
        {
            if (accessGroup == null || flowInstance == null)
            {
                return true;
            }

            var myorganizationids = pointUserInfo.EmployeeOrganizationIDs;
            if (myorganizationids.Contains(flowInstance.StartedByOrganizationID.GetValueOrDefault(0)))
            {
                return true;
            }

            var invites = OrganizationInviteBL.GetAcceptedOrActiveInvitesByFlowInstanceIDAndAccessGroup(uow, flowInstance.FlowInstanceID, accessGroup);
            return invites.Any(inv => myorganizationids.Contains(inv.OrganizationID));
        }

        public static FlowInstance GetByTransferIDWithDeleted(IUnitOfWork uow, int transferID)
        {
            return uow.FlowInstanceRepository.FirstOrDefault(fi => fi.TransferID == transferID);
        }

        //[Obsolete("GetByTransferID is deprecated, please use GetByFlowInstanceID instead.")]
        /// <summary>
        /// GetByTransferID is deprecated, please use GetByFlowInstanceID instead.
        /// </summary>
        public static FlowInstance GetByTransferID(IUnitOfWork uow, int transferID)
        {
            //Include "Transfer.FormSetVersions" niet meer terugzetten!
            //(als je de formsetversion nodig hebt ophalen via formsetversionbl (veel sneller)
            return uow.FlowInstanceRepository.FirstOrDefault(fi => fi.TransferID == transferID && !fi.Deleted);
        }

        public static FlowInstance SaveRelatedTransfer(IUnitOfWork uow, int transferID, int relatedTransferID)
        {
            var flowinstance = GetByTransferID(uow, transferID);
            if (flowinstance != null)
            {
                flowinstance.RelatedTransferID = relatedTransferID;
                InternalSave(uow);
            }
            return flowinstance;
        }

        public static IQueryable<FlowInstance> GetRelatedTransfersWithVVTByBSN(IUnitOfWork uow, string bsn, int organizationID, int? currentTransferID)
        {
            InviteStatus[] activestatuses = new InviteStatus[] { InviteStatus.Active, InviteStatus.Acknowledged, InviteStatus.Realized };

            return uow.FlowInstanceSearchValuesRepository.Get(fisv => fisv.ClientCivilServiceNumber == bsn && !fisv.FlowInstance.Deleted && (currentTransferID == null || fisv.TransferID != currentTransferID) && fisv.FlowInstanceOrganization.Any(fisvd => activestatuses.Contains(fisvd.InviteStatus) && fisvd.OrganizationID == organizationID), null, "FlowInstance", true).Select(fisv => fisv.FlowInstance);
        }

        public static List<FlowInstanceSearchViewModel> GetRelatedTransfersVVT_VVTByBSN(IUnitOfWork uow, string bsn, int? transferID, PointUserInfo pointUserInfo)
        {
            var searchClientOpenTransfers = SearchQueryBL.GetSearchQuerySimple(uow);
            searchClientOpenTransfers.ApplyCivilServiceNumberFilter(bsn, true);
            searchClientOpenTransfers.ApplyFilter(pointUserInfo, FlowDirection.Receiving);
            searchClientOpenTransfers.ApplyStatusFilter(DossierStatus.IsClosed);
            return searchClientOpenTransfers?.Queryable?.Where(fi => fi.FlowInstanceSearchValues.TransferID != transferID)?.ToList();                   
        }

        public static IQueryable<FlowInstance> QueryableByTransferID(IUnitOfWork uow, int transferID)
        {
            return uow.FlowInstanceRepository.Get(fi => fi.TransferID == transferID).AsQueryable();
        }
        
        public static FlowInstance GetByID(IUnitOfWork uow, int flowInstanceID)
        {
            return uow.FlowInstanceRepository.Get(fi => fi.FlowInstanceID == flowInstanceID && !fi.Deleted).FirstOrDefault();
        }

        public static IQueryable<FlowInstance> GetByIDs(IUnitOfWork uow, IEnumerable<int> flowInstanceIDs)
        {
            return uow.FlowInstanceRepository.Get(fi => flowInstanceIDs.Contains(fi.FlowInstanceID));
        }

        public static IQueryable<FlowInstance> GetByStartedByOrganizationId(IUnitOfWork uow, int organizationID)
        {
            return uow.FlowInstanceRepository.Get(fi => fi.StartedByOrganizationID == organizationID);
        }

        public static bool IsFlowClosed(IUnitOfWork uow, int flowInstanceID)
        {
            return PhaseInstanceBL.EndPhaseIsDone(uow, flowInstanceID);
        }

        public static bool IsFlowClosed(IUnitOfWork uow, FlowInstance flowInstance)
        {
            if (flowInstance == null)
            {
                return false;
            }

            return PhaseInstanceBL.EndPhaseIsDone(uow, flowInstance.FlowInstanceID);
        }

        public static IQueryable<FlowInstance> GetClosedBeforeDateFlowInstancesByOrganizationID(IUnitOfWork uow, DateTime realizedBefore, int organizationID)
        {
            var phasedefinitionids = PhaseDefinitionCacheBL.GetEndPhaseDefinitions(uow).Select(pd => pd.PhaseDefinitionID).ToArray();

            return uow.FlowInstanceRepository.Get(fi => fi.StartedByOrganizationID == organizationID && 
                                                  fi.PhaseInstances.Any(pi => !pi.Cancelled && pi.Status == (int)Status.Done && phasedefinitionids.Contains(pi.PhaseDefinitionID) && pi.RealizedDate < realizedBefore)).AsQueryable(); 
        }

        public static FlowInstance Copy(IUnitOfWork uow, FlowInstance flowInstance, LogEntry logEntry)
        {
            var transferCopy = TransferBL.Copy(uow, flowInstance.Transfer);

            var flowInstanceCopy = uow.FlowInstanceRepository.Get(fi => fi.FlowInstanceID == flowInstance.FlowInstanceID, asNoTracking: true).FirstOrDefault();
            if (flowInstanceCopy == null)
            {
                return null;
            }

            flowInstanceCopy.FlowInstanceID = 0;
            flowInstanceCopy.TransferID = transferCopy.TransferID;
            flowInstanceCopy.CreatedDate = DateTime.Now;
            flowInstanceCopy.EmployeeID = logEntry.EmployeeID;
            flowInstanceCopy.FlowDefinitionID = flowInstance.FlowDefinitionID;
            flowInstanceCopy.OriginalFlowInstanceID = flowInstance.FlowInstanceID;
            flowInstanceCopy.ScreenName = logEntry.Screenname;
            flowInstanceCopy.Timestamp = DateTime.Now;
            flowInstanceCopy.Deleted = false;            
            flowInstanceCopy.RelatedTransferID = -1;

            uow.FlowInstanceRepository.Insert(flowInstanceCopy);
            InternalSave(uow);

            return flowInstanceCopy;
        }

        public static FlowInstance NewFlowInstance(IUnitOfWork uow, FlowDefinitionID flowDefinitionID, int transferID, LogEntry logEntry, int organizationID, int departmentID)
        {
            var flowinstance = GetByTransferID(uow, transferID);
            if (flowinstance == null)
            {
                flowinstance = Insert(uow, flowDefinitionID, transferID, logEntry, organizationID, departmentID);
                InternalSave(uow);
            }

            return flowinstance;
        }
        
        public static void CloseFlowInstance(IUnitOfWork uow, int flowInstanceID, int employeeID, LogEntry logEntry)
        {
            var flowinstance = GetByID(uow, flowInstanceID);
            if (flowinstance == null)
            {
                throw new PointJustLogException("Flowinstance niet gevonden met FlowInstanceID: " + flowInstanceID);
            }

            var endphase = PhaseInstanceBL.GetEndPhase(uow, flowinstance.FlowInstanceID);
            if (endphase?.Status == (int)Status.Done)
            {
                throw new PointJustLogException("Dit dossier/deze flow is reeds afgesloten.");
            }
            if (endphase != null)
            {
                PhaseInstanceBL.SetStatusDone(uow, endphase, logEntry);
            }
            else
            {
                var endPhaseDefinition = PhaseDefinitionCacheBL.GetEndFlowPhaseDefinition(uow, flowinstance.FlowDefinitionID);
                if (endPhaseDefinition == null)
                {
                    throw new PointJustLogException("Eindfase niet gevonden voor FlowDefinitionID: " + flowinstance.FlowDefinitionID);
                }
                PhaseInstanceBL.Create(uow, flowInstanceID, endPhaseDefinition.PhaseDefinitionID, logEntry, status: Status.Done);
            }

            FlowInstanceStatusBL.CreateCloseTransfer(uow, flowinstance, logEntry);
        }

        public static void ReOpenFlowInstance(IUnitOfWork uow, int flowInstanceID, LogEntry logEntry)
        {
            var flowinstance = GetByID(uow, flowInstanceID);
            var phaseInstances = PhaseInstanceBL.GetByFlowInstanceId(uow, flowInstanceID).ToList();

            if (IsFlowClosed(uow, flowInstanceID) == false)
            {
                throw new PointJustLogException("Dossier is al geopend");
            }

            //Deactivate the EndPhase and make a new EndPhase
            var endphase = PhaseInstanceBL.GetEndPhaseDone(uow, flowInstanceID);
            if (endphase != null)
            {
                PhaseInstanceBL.Cancel(uow, endphase, logEntry);

                //Dont create a new (close)phase if there are allready active ones (closed in the middle of a flow)
                var activephases = phaseInstances.Where(pi => pi.Status == (int)Status.Active && pi.PhaseDefinition.Phase > -1).ToList();
                if (activephases.Count == 0)
                {
                    if (flowinstance.FlowDefinitionID == FlowDefinitionID.CVA)
                    {
                        if (!FlowInstanceBL.IsOntslagbestemmingStandardRoute(uow, flowinstance))
                        {
                            var ontslagbestemmingphasedefinition = PhaseDefinitionCacheBL.GetByFlowDefinitionAndFormType(uow, FlowDefinitionID.CVA, (int)FlowFormType.CVARouteSelection);
                            if (ontslagbestemmingphasedefinition == null)
                            {
                                throw new PointJustLogException("Ontslagbestemming niet gevonden voor FlowDefinitionID: " + flowinstance.FlowDefinitionID);
                            }

                            var previousphaseinstance = PhaseInstanceBL.GetByFlowInstanceIDAndPhaseDefinition(uow, flowinstance.FlowInstanceID, ontslagbestemmingphasedefinition.PhaseDefinitionID);
                            PhaseInstanceBL.Cancel(uow, previousphaseinstance, logEntry);

                            PhaseInstanceBL.Create(uow, flowInstanceID, ontslagbestemmingphasedefinition.PhaseDefinitionID, logEntry);
                        }
                    }
                    else
                    {
                        var endPhaseDefinition = PhaseDefinitionCacheBL.GetEndFlowPhaseDefinition(uow, flowinstance.FlowDefinitionID);
                        if (endPhaseDefinition == null)
                        {
                            throw new PointJustLogException("Eindfase niet gevonden voor FlowDefinitionID: " + flowinstance.FlowDefinitionID);
                        }
                        PhaseInstanceBL.Create(uow, flowInstanceID, endPhaseDefinition.PhaseDefinitionID, logEntry);
                    }
                }
            }

            InternalSave(uow);

            FlowInstanceStatusBL.CreateReopenTransfer(uow, flowinstance, logEntry);
        }

        public static void DeleteFlowInstance(IUnitOfWork uow, int transferID)
        {
            var flowinstance = GetByTransferID(uow, transferID);

            flowinstance.Deleted = true;
            flowinstance.DeletedByEmployeeID = PointUserInfoHelper.GetPointUserInfo(uow).Employee.EmployeeID;
            flowinstance.DeletedDate = DateTime.Now;

            InternalSave(uow);

        }

        public static bool HasRegistrationEndedWithoutTransfer(IUnitOfWork uow, int flowInstanceID)
        {
            var registrationEndedWithoutTransfer = FlowFieldValueBL.GetByFlowInstanceIDAndFieldID(uow, flowInstanceID, FlowFieldConstants.ID_RegistrationEndedWithoutTransfer).OrderByDescending(it => it.FormSetVersionID).FirstOrDefault();
            if (registrationEndedWithoutTransfer == null) return false;

            return registrationEndedWithoutTransfer.Value.Equals("true", StringComparison.InvariantCultureIgnoreCase);
        }

        public static bool HasCareBeginDate(IUnitOfWork uow, FlowInstance flowInstance)
        {
            var aanvullendeGegevens = PhaseDefinitionCacheBL.GetByFlowDefinitionAndFormType(uow, flowInstance.FlowDefinitionID, (int)FlowFormType.AanvullendeGegevensTPZHVVT);
            if (aanvullendeGegevens == null)
            {
                return false;
            }

            var aanvullendeGegevensPhaseInstance = PhaseInstanceBL.GetByFlowInstanceIDAndPhaseDefinition(uow, flowInstance.FlowInstanceID, aanvullendeGegevens.PhaseDefinitionID);
            if (aanvullendeGegevensPhaseInstance == null)
            {
                return false;
            }

            var aanvullendeGegevensFormSetVersion = FormSetVersionBL.GetByPhaseInstanceID(uow, aanvullendeGegevensPhaseInstance.PhaseInstanceID).FirstOrDefault();
            if (aanvullendeGegevensFormSetVersion == null)
            {
                return false;
            }

            var flowfield = FlowFieldBL.GetByName(uow, FlowFieldConstants.Name_CareBeginDate);
            if (flowfield == null)
            {
                return false;
            }

            var carebeginDate = FlowFieldValueBL.GetByFlowInstanceIDAndFormSetVersionIDAndFlowFieldID(uow, flowInstance.FlowInstanceID, aanvullendeGegevensFormSetVersion.FormSetVersionID, flowfield.FlowFieldID);

            return !String.IsNullOrWhiteSpace(carebeginDate?.Value);
        }

        public static bool HasDischargeRealizedDate(IUnitOfWork uow, FlowInstance flowInstance)
        {
            var realizedCare = PhaseDefinitionCacheBL.GetByFlowDefinitionAndFormType(uow, flowInstance.FlowDefinitionID, (int)FlowFormType.RealizedCare);
            if (realizedCare == null)
            {
                return false;
            }

            var realizedCarePhaseInstance = PhaseInstanceBL.GetByFlowInstanceIDAndPhaseDefinition(uow, flowInstance.FlowInstanceID, realizedCare.PhaseDefinitionID);
            if (realizedCarePhaseInstance == null)
            {
                return false;
            }

            var realizedCareFormSetVersion = FormSetVersionBL.GetByPhaseInstanceID(uow, realizedCarePhaseInstance.PhaseInstanceID).FirstOrDefault();
            if (realizedCareFormSetVersion == null)
                return false;

            var flowfield = FlowFieldBL.GetByName(uow, FlowFieldConstants.Name_RealizedDischargeDate);
            if (flowfield == null)
            {
                return false;
            }

            var realizedDischargeDate = FlowFieldValueBL.GetByFlowInstanceIDAndFormSetVersionIDAndFlowFieldID(uow, flowInstance.FlowInstanceID, realizedCareFormSetVersion.FormSetVersionID, flowfield.FlowFieldID);

            return !String.IsNullOrWhiteSpace(realizedDischargeDate?.Value);
        }

        public static bool CanSendVO(IUnitOfWork uow, FlowInstance flowInstance)
        {
            var flowdefinitionid = (FlowDefinitionID)flowInstance.FlowDefinitionID;

            if (flowdefinitionid == FlowDefinitionID.VVT_ZH || flowdefinitionid == FlowDefinitionID.RzTP)
            {
                return true;
            }

            if (flowdefinitionid == FlowDefinitionID.ZH_VVT || flowdefinitionid == FlowDefinitionID.HA_VVT || flowdefinitionid == FlowDefinitionID.VVT_VVT)
            {
                var patientaccepted = PatientHelper.IsAccepted(uow, flowInstance.FlowInstanceID);
                return patientaccepted ==  AcceptanceState.Acknowledged;
            }

            if (flowdefinitionid == FlowDefinitionID.ZH_ZH)
            {
                return GetLastPhaseNumByFlowInstanceID(uow, flowInstance.FlowInstanceID) >= 3;
            }

            if (flowdefinitionid == FlowDefinitionID.CVA)
            {
                // #15359 Double PhaseInstances : Invullen CVA registratie VVT
                if (GetLastPhaseNumByFlowInstanceID(uow, flowInstance.FlowInstanceID) <= 10)
                { 
                    return (GetOntslagbestemming(uow, flowInstance) == FlowFieldConstants.Value_Ontslagbestemming_Revalidatiecentrum || IsOntslagbestemmingStandardRoute(uow, flowInstance));
                }
            }

            return false;
        }

        public static string GetOntslagbestemming(IUnitOfWork uow, FlowInstance flowInstance)
        {
            if (flowInstance == null)
            {
                throw new NullReferenceException("flowinstance is null");
            }

            var flowfieldvalueOntslagbestemming = FlowFieldValueBL.GetByFlowInstanceIDAndFieldID(uow, flowInstance.FlowInstanceID, FlowFieldConstants.ID_Ontslagbestemming).FirstOrDefault();
            return String.IsNullOrWhiteSpace(flowfieldvalueOntslagbestemming?.Value) ? null : flowfieldvalueOntslagbestemming.Value;
        }

        public static bool IsOntslagbestemmingStandardRoute(IUnitOfWork uow, FlowInstance flowInstance)
        {
            if (flowInstance == null)
            {
                throw new NullReferenceException("flowinstance is null");
            }

            var ontslagbestemming = GetOntslagbestemming(uow, flowInstance);
            var ontslagbestemmingthuis = GetOntslagbestemmingThuis(uow, flowInstance);
            return IsOntslagbestemmingStandardRoute(ontslagbestemming, ontslagbestemmingthuis);
        }

        public static bool IsOntslagbestemmingStandardRoute(string ontslagbestemming, string ontslagbestemmingthuis)
        {
            var isstandardroute = false;

            if (ontslagbestemming == FlowFieldConstants.Value_Ontslagbestemming_GRZCVAverpleeghuis || ontslagbestemming == FlowFieldConstants.Value_Ontslagbestemming_Verpleeghuis)
            {
                isstandardroute = true;
            }
            else if (ontslagbestemming == FlowFieldConstants.Value_Ontslagbestemming_Huis)
            {
                if (ontslagbestemmingthuis == FlowFieldConstants.Value_OntslagbestemmingThuis_Thuismetzorg || ontslagbestemmingthuis == FlowFieldConstants.Value_OntslagbestemmingThuis_Thuismetzorgenbehandeling)
                {
                    isstandardroute = true;
                }
            }

            return isstandardroute;
        }

        public static string AutoCloseTransfers(IUnitOfWork uow, IDictionary<string, object> viewData)
        {
            var result = "";
            var now = DateTime.Now;
            var endflowphasedefinitionids = uow.PhaseDefinitionRepository.Get(pd => pd.EndFlow).Select(pd => pd.PhaseDefinitionID);

            var phaseinstances = Enumerable.Empty<PhaseInstance>();

            var organizationsettings = OrganizationSettingBL.GetAllByName(uow, OrganizationSettingBL.AutoCloseTransferAfterDays).ToList();
            foreach (var organizationsetting in organizationsettings)
            {
                result += $"OrganizationID: {organizationsetting.OrganizationID}, AutoCloseTransferAfterDays: {organizationsetting.Value}." + Environment.NewLine;
                var days = organizationsetting.Value.ConvertTo<int?>();
                if (days != null && days > 0)
                {
                    var endphasestartedbefore = now.AddDays(-days.Value);
                    var organizationphaseinstances = uow.PhaseInstanceRepository.Get(pi => pi.Status == (int)Status.Active && endflowphasedefinitionids.Contains(pi.PhaseDefinitionID)
                                                         && !pi.Cancelled && !pi.FlowInstance.Deleted && pi.FlowInstance.StartedByOrganizationID == organizationsetting.OrganizationID
                                                         && pi.StartProcessDate < endphasestartedbefore).ToList();
                    if (organizationphaseinstances != null && organizationphaseinstances.Any())
                    {
                        result += $"PhaseInstanceIDs to close: {string.Join(", ", organizationphaseinstances.Select(pi => pi.PhaseInstanceID))}." + Environment.NewLine + Environment.NewLine;
                        phaseinstances = phaseinstances.Union(organizationphaseinstances);
                    }
                }
            }

            int systememployeeid = PointUserInfoHelper.GetSystemEmployeeID();
            var logEntry = new LogEntry
            {
                EmployeeID = systememployeeid,
                Timestamp = DateTime.Now,
                Screenname = null
            };

            var successids = new List<int>();
            foreach (var phaseinstance in phaseinstances)
            {
                bool success = false;
                try
                {
                    success = CloseFlowInstanceIfRequiredFieldsFilledIn(uow, phaseinstance, systememployeeid, logEntry, viewData);
                }
                catch { /* ignore */ }

                if (success)
                {
                    successids.Add(phaseinstance.PhaseInstanceID);
                }
            }

            result += Environment.NewLine + "Succesvol afgesloten: " + String.Join(", ", successids);

            return result;
        }

        private static bool CloseFlowInstanceIfRequiredFieldsFilledIn(IUnitOfWork uow, PhaseInstance phaseInstance, int systemEmployeeID, LogEntry logEntry, IDictionary<string, object> viewData)
        {
            var flowWebFields = FlowWebFieldBL.GetFieldsFromFormTypeID(uow, phaseInstance.PhaseDefinition.FormTypeID, phaseInstance.PhaseDefinitionID,
                                    phaseInstance.FlowInstance.TransferID, 
                                    phaseInstance.FlowInstance.StartedByOrganizationID, 
                                    phaseInstance.FlowInstance.StartedByOrganization.RegionID,
                                    viewData: viewData).ToList();

            if (flowWebFields != null)
            {
                foreach (var flowWebField in flowWebFields)
                {
                    var validator = new FlowWebFieldValidator(flowWebField, flowWebFields);
                    if (!validator.IsValid())
                    {
                        return false;
                    }
                }
            }

            bool success = false;
            try
            {

                CloseFlowInstance(uow, phaseInstance.FlowInstanceID, systemEmployeeID, logEntry);
                FlowInstanceSearchValuesBL.CalculateReadValuesByFlowinstanceIDAsync(uow, phaseInstance.FlowInstanceID);
                success = true;
            }
            catch { /* ignore */ }

            return success;
        }

        public static bool IsValidNedapDossierStatus(IUnitOfWork uow, FlowInstance flowInstance, PointUserInfo pointUserInfo)
        {
            var verpleegkundigebo = new VerpleegkundigeOverdrachtBO(uow, flowInstance, pointUserInfo);
            if (PatientHelper.IsAccepted(uow, flowInstance.FlowInstanceID) ==  AcceptanceState.Acknowledged && verpleegkundigebo.IsDefinitive())
            {
                return true;
            }

            return false;
        }

        private static string GetOntslagbestemmingThuis(IUnitOfWork uow, FlowInstance flowInstance)
        {
            if (flowInstance == null) throw new NullReferenceException("flowinstance is null");

            var flowfieldvalueOntslagbestemmingThuis = FlowFieldValueBL.GetByFlowInstanceIDAndFieldID(uow, flowInstance.FlowInstanceID, FlowFieldConstants.ID_OntslagbestemmingThuis).FirstOrDefault();
            return String.IsNullOrWhiteSpace(flowfieldvalueOntslagbestemmingThuis?.Value) ? null : flowfieldvalueOntslagbestemmingThuis.Value;
        }
        
        private static FlowInstance Insert(IUnitOfWork uow, FlowDefinitionID flowDefinitionID, int transferID, LogEntry logEntry, int organizationID, int departmentID)
        {
            var department = DepartmentBL.GetByDepartmentID(uow, departmentID);

            FlowInstance flowinstance = new FlowInstance()
            {
                CreatedDate = DateTime.Now,
                Deleted = false,
                FlowDefinitionID = flowDefinitionID,
                TransferID = transferID,
                StartedByEmployeeID = logEntry.EmployeeID,
                StartedByDepartmentID = departmentID,
                StartedByOrganizationID = organizationID,
                IsAcute = department?.IsAcute == true
            };

            LoggingBL.FillLogging(uow, flowinstance, logEntry);

            uow.FlowInstanceRepository.Insert(flowinstance);

            return flowinstance;
        }
    }
}
