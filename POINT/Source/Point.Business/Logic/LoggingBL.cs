﻿using Point.Database.Models;
using Point.Database.Repository;
using Point.Log.Interfaces;

namespace Point.Business.Logic
{
    public static class LoggingBL
    {
        public static void FillLogging<TEntity>(IUnitOfWork uow, TEntity entity, LogEntry logEntry, bool forceupdate = false) where TEntity : class, Point.Database.Repository.ILogEntry
        {
            if (entity != null && (uow.DatabaseContext.Entry(entity).State != System.Data.Entity.EntityState.Unchanged || forceupdate))
            {
                entity.EmployeeID = logEntry.EmployeeID;
                entity.ScreenName = logEntry.Screenname;
                entity.Timestamp = logEntry.Timestamp;
            }
        }

        public static void FillLoggingWithID<TEntity>(IUnitOfWork uow, TEntity entity, LogEntry logEntry, bool forceupdate = false) where TEntity : class, Point.Database.Repository.ILogEntryWithID
        {
            if (entity != null && (uow.DatabaseContext.Entry(entity).State != System.Data.Entity.EntityState.Unchanged || forceupdate))
            {
                entity.EmployeeID = logEntry.EmployeeID;
                entity.ScreenID = logEntry.ScreenID;
                entity.TimeStamp = logEntry.Timestamp;
            }
        }

        public static void FillLoggable(IUnitOfWork uow, ILoggable entity, LogEntry logEntry, bool forceupdate = false)
        {
            if (entity != null && (uow.DatabaseContext.Entry(entity).State != System.Data.Entity.EntityState.Unchanged || forceupdate))
            {
                //entity.ModifiedByEmployeeID = logEntry.EmployeeID;
                //entity.ModifiedByScreenID = logEntry.ScreenID;
                entity.ModifiedTimeStamp = logEntry.Timestamp;
            }
        }
    }
}
