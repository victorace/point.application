﻿using Point.Business.BusinessObjects;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Extensions;
using Point.Models.Enums;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Point.Business.Logic
{
    public static class FieldReceivedTempBL
    {
        private class FieldReceivedTempValue
        {
            public int FlowFieldID { get; set; }
            public List<string> Values { get; set; } = new List<string>();
            public string Value { get; set; }
        }

        // TODO: this extra logic will be removed once it has been communicated with 
        // the client (MST, ZGT) - PP
        private static readonly List<string> subcategories = new List<string>()
        {
            "InfectieIsolatie",
            "Verslaving",
            "VrijheidsbeperkendeMaatregelen"
        };

        public static IEnumerable<FieldReceivedTemp> GetByMessageReceivedTempID(IUnitOfWork uow, int messageReceivedTempID)
        {
            return uow.FieldReceivedTempRepository.Get(frt => frt.MessageReceivedTempID == messageReceivedTempID).OrderBy(frt => frt.FieldName);
        }

        public static void SetFlowWebFieldValues(IUnitOfWork uow, IEnumerable<FlowWebField> flowWebFields, Organization organization, string patientNumber, string visitNumber)
        {
            if (string.IsNullOrWhiteSpace(patientNumber))
            {
                return;
            }

            var messagereceivedtemp = MessageReceivedTempBL.Get(uow, organization.OrganizationID, patientNumber, visitNumber);
            if (messagereceivedtemp == null)
            {
                return;
            }

            var fieldReceivedTemps = GetByMessageReceivedTempID(uow, messagereceivedtemp.MessageReceivedTempID);

            var fieldreceivedtempvalues = new List<FieldReceivedTempValue>();

            foreach (var fieldReceivedTemp in fieldReceivedTemps)
            {
                var skipField = false;
                var fieldName = fieldReceivedTemp.FieldName;
                if (string.IsNullOrEmpty(fieldName)) continue;
                var value = fieldReceivedTemp.FieldValue?.Trim();

                if (fieldName.Equals(FlowFieldConstants.Name_SourceOrganizationDepartment, StringComparison.InvariantCultureIgnoreCase))
                {
                    var departments = DepartmentBL.GetActiveByOrganizationID(uow, organization.OrganizationID);
                    var department = departments.FirstOrDefault(d => d.ExternID == value);
                    if (department == null && int.TryParse(value, out int departmentid))
                    {
                        department = departments.FirstOrDefault(d => d.DepartmentID == departmentid);
                    }
                    value = department?.DepartmentID.ToString();
                }
                // TODO: this extra logic will be removed once it has been communicated with 
                // the client (MST, ZGT) - PP
                else if (fieldName.StartsWith(FlowFieldConstants.Name_InfectieIsolatieSoortInfectie, StringComparison.InvariantCultureIgnoreCase) ||
                    fieldName.StartsWith(FlowFieldConstants.Name_InfectieIsolatieSoortIsolatie, StringComparison.InvariantCultureIgnoreCase))
                {
                    int length = fieldName.Length;
                    if (fieldName.StartsWith(FlowFieldConstants.Name_InfectieIsolatieSoortInfectie, StringComparison.InvariantCultureIgnoreCase))
                    {
                        length = FlowFieldConstants.Name_InfectieIsolatieSoortInfectie.Length;
                    }
                    else if (fieldName.StartsWith(FlowFieldConstants.Name_InfectieIsolatieSoortIsolatie, StringComparison.InvariantCultureIgnoreCase))
                    {
                        length = FlowFieldConstants.Name_InfectieIsolatieSoortIsolatie.Length;
                    }

                    var fieldnamewithoutoption = fieldName.Substring(0, length);
                    if (value == "1" || value.ToLower() == "ja" || value.ToLower() == "true")
                    {
                        value = fieldName.Substring(length);
                        fieldName = fieldnamewithoutoption;
                    }
                }

                var flowWebField = flowWebFields.FirstOrDefault(fwf => fieldName.Equals(fwf.Name, StringComparison.InvariantCultureIgnoreCase));
                if (flowWebField != null)
                {
                    var mainField = subcategories.FirstOrDefault(x => fieldName.StartsWith(x, StringComparison.InvariantCultureIgnoreCase) &&
                        !fieldName.Equals(x, StringComparison.InvariantCultureIgnoreCase));
                    var mainFlowWebField = flowWebFields.FirstOrDefault(x => x.Name == mainField);

                    var fieldreceivedtempvalue = fieldreceivedtempvalues.FirstOrDefault(x => x.FlowFieldID == flowWebField.FlowFieldID);
                    if (fieldreceivedtempvalue == null)
                    {
                        fieldreceivedtempvalue = new FieldReceivedTempValue() { FlowFieldID = flowWebField.FlowFieldID };
                        fieldreceivedtempvalues.Add(fieldreceivedtempvalue);
                    }

                    if (flowWebField.Type == FlowFieldType.String)
                    {
                    }
                    else if (flowWebField.Type == FlowFieldType.Number)
                    {
                        if (!Double.TryParse(value, out double _))
                        {
                            skipField = true;
                        }
                    }
                    else if (flowWebField.Type == FlowFieldType.RadioButton)
                    {
                    }
                    else if (flowWebField.Type == FlowFieldType.Checkbox)
                    {
                        if (value == "0" || value.ToLower() == "nee" || value.ToLower() == "false")
                        {
                            skipField = true;
                        }
                    }
                    else if (flowWebField.Type == FlowFieldType.DropDownList)
                    {
                        var mappedValue = GetMappedValue(uow, fieldName, value);
                        if (mappedValue != null)
                        {
                            value = mappedValue;
                        }
                    }
                    else if (flowWebField.Type == FlowFieldType.CheckboxList)
                    {
                        fieldreceivedtempvalue.Values.Add(value);
                        value = string.Join(",", fieldreceivedtempvalue.Values);
                    }
                    else if (flowWebField.Type == FlowFieldType.DateTime || flowWebField.Type == FlowFieldType.Date)
                    {
                        var formats = new List<string>()
                        {
                            "yyyyMMdd", // WSInterface
                            "yyyyMMddHHmmss", // HL7 FieldReceivedTemp format
                            "dd-MM-yyyy HH:mm:ss",
                            "yyyy-MM-dd HH:mm:ss.fff",
                            "yyyy-MM-dd HH:mm:ss",
                            "dd-MM-yyyy",
                            "yyyy-MM-dd"
                        };
                        if (!string.IsNullOrWhiteSpace(flowWebField.Format))
                        {
                            formats.Insert(0, flowWebField.Format);
                        }
                        if (!DateTime.TryParseExact(value, formats.ToArray(), CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime datetime))
                        {
                            skipField = true;
                        }
                    }

                    if (!string.IsNullOrWhiteSpace(value) && !skipField)
                    {
                        value = fixFlowFieldDataSourceValueCase(fieldName, value, flowWebField);

                        fieldreceivedtempvalue.Value = value;
                        fieldReceivedTemp.Status = "OK";

                        FlowWebFieldBL.SetValue(flowWebField, fieldreceivedtempvalue.Value, messagereceivedtemp.Source);

                        FlowWebFieldBL.SetValue(mainFlowWebField, "true", messagereceivedtemp.Source);
                    }
                }
            }

            uow.Save();
        }

        public static IEnumerable<FieldReceivedTemp> Insert(IUnitOfWork uow, MessageReceivedTemp messagereceivedtemp, List<FieldBO> fields)
        {
            var result = new List<FieldReceivedTemp>();

            if (messagereceivedtemp != null)
            {
                var timestamp = DateTime.Now;
                foreach (var field in fields)
                {
                    var fieldreceivedtemp = new FieldReceivedTemp()
                    {
                        Timestamp = timestamp,
                        FieldName = field.FieldName,
                        FieldValue = field.FieldValue,
                        MessageReceivedTempID = messagereceivedtemp.MessageReceivedTempID
                    };

                    uow.FieldReceivedTempRepository.Insert(fieldreceivedtemp);

                    result.Add(fieldreceivedtemp);
                }

                uow.Save();
            }

            return result;
        }

        // TODO: because of the way some values are sent FlowFieldNameValue (InfectieIsolatie etc)
        // then the case of the value can be different to the FlowFieldDataSource value - PP
        private static string fixFlowFieldDataSourceValueCase(string fieldName, string value, FlowWebField flowWebField)
        {
            if (flowWebField?.FlowFieldDataSource?.Options != null && flowWebField.FlowFieldDataSource.Options.Any())
            {
                if (value.Contains(","))
                {
                    var values = value.Split(',').Where(v => v != null);
                    var options = flowWebField.FlowFieldDataSource.Options
                        .Where(it => values.Any(v => string.Equals(it.Value, v, StringComparison.CurrentCultureIgnoreCase)));
                    if (options != null && options.Any())
                    {
                        value = string.Join(",", options.Select(it => it.Value));
                    }
                }
                else
                {
                    var option = flowWebField.FlowFieldDataSource.Options
                        .Where(it => it.Selected = string.Equals(it.Value, value, StringComparison.CurrentCultureIgnoreCase))
                        .FirstOrDefault();
                    if (option != null)
                    {
                        value = option.Value;
                    }
                }
            }
            return value;
        }

        private static string GetMappedValue(IUnitOfWork uow, string fieldName, string sourceValue)
        {
            var mapping = FieldReceivedValueMappingBL.MapValue(uow, fieldName, sourceValue);
            if (mapping != null)
            {
                return mapping.TargetValue;
            }
            return null;
        }
    }
}