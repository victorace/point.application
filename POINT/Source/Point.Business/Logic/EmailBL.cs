﻿using Point.Business.Helper;
using Point.Database.Extensions;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Helpers;
using Point.Models.Enums;
using System;
using System.Collections.Generic;

namespace Point.Business.Logic
{
    public class EmailBL
    {
        public enum EmailType
        {
            Email = 0,
            TransferEmail = 1,
            Zorgmail = 2
        }

        public static string GetEmailAddress(Department department, EmailType emailtype)
        {
            if (emailtype == EmailType.TransferEmail && department.RecieveEmailTransferSend && !String.IsNullOrEmpty(department.EmailAddressTransferSend))
            {
                return department.EmailAddressTransferSend;
            }
            else if (emailtype == EmailType.Email && department.RecieveEmail && !String.IsNullOrEmpty(department.EmailAddress))
            {
                return department.EmailAddress;
            }
            else if (emailtype == EmailType.Zorgmail && department.RecieveZorgmailTransferSend && !String.IsNullOrEmpty(department.ZorgmailTransferSend))
            {
                return department.ZorgmailTransferSend;
            }

            return null;
        }

        private static bool skipSend(Department department, EmailType emailtype)
        {
            return String.IsNullOrEmpty(GetEmailAddress(department, emailtype));
        }

        private static string getCancelReason(string cancelReason)
        {
           return string.IsNullOrEmpty(cancelReason) ? "" : "Reden intrekken dossier: " + cancelReason; ;
        }

        public static EmailVVTZHViewModel GetEmailVVTZH(IUnitOfWork uow, int transferid, int departmentid, int employeeID, string dossierurl, EmailType emailtype)
        {
            Department receivingdepartment = DepartmentBL.GetByDepartmentID(uow, departmentid);

            if (skipSend(receivingdepartment, emailtype)) return null;

            Employee employee = EmployeeBL.GetByID(uow, employeeID);
            Department sendingdepartment = DepartmentBL.GetByEmployeeID(uow, employeeID);

            var flowinstance = FlowInstanceBL.GetByTransferID(uow, transferid);
            Client client = flowinstance.Transfer.Client;

            var viewmodel = new EmailVVTZHViewModel();

            viewmodel.RecievingDepartment = receivingdepartment.Name;
            viewmodel.RecievingEmailAdress = GetEmailAddress(receivingdepartment, emailtype);

            viewmodel.SendingUser = employee.FullName();
            viewmodel.SendingLocation = sendingdepartment.FullName();
            viewmodel.SendingTelephoneNumber = employee.PhoneNumber;

            viewmodel.TransferID = transferid;
            viewmodel.PatientName = client.FullName(flowinstance, receivingdepartment.Location?.OrganizationID);
            viewmodel.DossierUrl = dossierurl;

            viewmodel.OrganizationID = receivingdepartment.Location.OrganizationID;
            viewmodel.EmployeeID = employeeID;

            return viewmodel;
        }

        public static EmailZHZHViewModel GetEmailZHZH(IUnitOfWork uow, int transferid, int departmentid, int employeeID, string dossierurl, EmailType emailtype)
        {
            Department receivingdepartment = DepartmentBL.GetByDepartmentID(uow, departmentid);

            if (skipSend(receivingdepartment, emailtype)) return null;

            Employee employee = EmployeeBL.GetByID(uow, employeeID);
            Department sendingdepartment = DepartmentBL.GetByEmployeeID(uow, employeeID);

            var flowinstance = FlowInstanceBL.GetByTransferID(uow, transferid);

            Client client = flowinstance.Transfer.Client;
            
            var viewmodel = new EmailZHZHViewModel();

            viewmodel.RecievingDepartment = receivingdepartment.Name;
            viewmodel.RecievingLocation = receivingdepartment.Location?.Name;
            viewmodel.RecievingOrganization = receivingdepartment.Location?.Organization?.Name;
            viewmodel.RecievingEmailAdress = GetEmailAddress(receivingdepartment, emailtype);

            viewmodel.SendingUser = employee.FullName();
            viewmodel.SendingLocation = sendingdepartment.FullName();
            viewmodel.SendingTelephoneNumber = employee.PhoneNumber;

            viewmodel.TransferID = transferid;
            viewmodel.PatientName = client.FullName(flowinstance, receivingdepartment.Location?.OrganizationID);
            viewmodel.DossierUrl = dossierurl;

            viewmodel.OrganizationID = receivingdepartment.Location.OrganizationID;
            viewmodel.EmployeeID = employeeID;

            return viewmodel;
        }

        public static EmailZHCISViewModel GetEmailZHCIS(IUnitOfWork uow, int transferID, int departmentID, int employeeID, string dossierurl, EmailType emailtype)
        {
            var recievingdepartment = DepartmentBL.GetByDepartmentID(uow, departmentID);
            if (recievingdepartment?.Location?.Organization == null) return null;

            if (skipSend(recievingdepartment, emailtype)) return null;

            var employee = EmployeeBL.GetByID(uow, employeeID);
            var employeedepartment = DepartmentBL.GetByEmployeeID(uow, employeeID);

            var flowinstance = FlowInstanceBL.GetByTransferID(uow, transferID);
            Client client = flowinstance.Transfer.Client;

            var viewmodel = new EmailZHCISViewModel();

            viewmodel.RecievingOrganization = recievingdepartment.Location.Organization.Name;
            viewmodel.RecievingEmailAdress = GetEmailAddress(recievingdepartment, emailtype);

            viewmodel.SendingUser = employee.FullName();
            viewmodel.SendingLocation = employeedepartment.FullName();
            viewmodel.SendingTelephoneNumber = employee.PhoneNumber;

            viewmodel.TransferID = transferID;
            viewmodel.PatientName = client.FullName(flowinstance, recievingdepartment.Location?.OrganizationID);
            viewmodel.DossierUrl = dossierurl;

            viewmodel.EmployeeID = employeeID;
            viewmodel.OrganizationID = recievingdepartment.Location.OrganizationID;

            return viewmodel;
        }

        public static EmailRelocateDossierViewModel GetEmailRelocateDossierViewModel(IUnitOfWork uow, int transferID, int responsibleEmployeeID, int sourceDepartmentID, int destinationDepartmentID, string relocateReason)
        {
            if(responsibleEmployeeID <= 0)
            {
                return null;
            }

            var flowinstance = FlowInstanceBL.GetByTransferID(uow, transferID);
            if(flowinstance == null)
            {
                return null;
            }

            var sourceDepartment = DepartmentBL.GetByDepartmentID(uow, sourceDepartmentID);
            var destinationDepartment = DepartmentBL.GetByDepartmentID(uow, destinationDepartmentID);

            if(destinationDepartment == null)
            {
                return null;
            }

            var destinationTransferPointDepartment = DepartmentBL.GetTransferPointByLocation(uow, destinationDepartment.LocationID);

            var responsibleEmployee = EmployeeBL.GetByID(uow, responsibleEmployeeID);
            var responsibleEmployeeDepartment = DepartmentBL.GetByEmployeeID(uow, responsibleEmployeeID);

            var emailAdress = GetEmailAddress(destinationDepartment, EmailType.Email);
            var emailAdressTransferPoint = GetEmailAddress(destinationTransferPointDepartment, EmailType.Email);

            if (string.IsNullOrEmpty(emailAdress) && string.IsNullOrEmpty(emailAdressTransferPoint))
            {
                return null;
            }

            return new EmailRelocateDossierViewModel()
            {
                ReceivingEmailAdress = emailAdress,
                ReceivingTransferPointEmailAdress = emailAdressTransferPoint,
                ClientName = flowinstance.Transfer.Client.FullName(flowinstance, destinationDepartment?.Location?.OrganizationID),
                DossierUrl = $"{ConfigHelper.GetAppSettingByName<string>("WebServer")}/FlowMain/Dashboard?TransferID={transferID}",
                SourceDepartment = sourceDepartment.FullName(),
                DestinationDepartment = destinationDepartment.FullName(),
                RelocateReason = relocateReason,
                ResponsibleEmployee = responsibleEmployee.FullName(),
                ResponsibleDepartment = responsibleEmployeeDepartment.FullName(),
                ResponsibleTelephoneNumber = responsibleEmployee?.PhoneNumber,
                OrganizationID = responsibleEmployeeDepartment.Location.OrganizationID,
                EmployeeID = responsibleEmployee.EmployeeID
            };
        }

        public static EmailZHVVTViewModel GetEmailZHVVT(IUnitOfWork uow, int transferID, int departmentid, int employeeID, string dossierurl, EmailType emailtype, bool forceclientname = false, string cancelReason = null)
        {
            var receivingdepartment = DepartmentBL.GetByDepartmentID(uow, departmentid);
            if (receivingdepartment?.Location?.Organization == null) return null;

            var receivingorganization = receivingdepartment.Location.Organization;

            if (skipSend(receivingdepartment, emailtype)) return null;

            var employee = EmployeeBL.GetByID(uow, employeeID);
            var employeedepartment = DepartmentBL.GetByEmployeeID(uow, employeeID);

            var flowinstance = FlowInstanceBL.GetByTransferID(uow, transferID);
            Client client = flowinstance.Transfer.Client;

            var viewmodel = new EmailZHVVTViewModel();

            viewmodel.RecievingOrganization = receivingorganization.Name;
            viewmodel.ReceivingDepartment = receivingdepartment.Name;
            viewmodel.ReceivingDepartmentFullName = receivingdepartment.FullName();
            viewmodel.ReceivingLocation = receivingdepartment.Location.Name;
            viewmodel.RecievingEmailAdress = GetEmailAddress(receivingdepartment, emailtype);
            viewmodel.SendingUser = employee.FullName();
            viewmodel.SendingLocation = employeedepartment.FullName();
            viewmodel.SendingTelephoneNumber = employee.PhoneNumber;
            viewmodel.CancelReason = getCancelReason(cancelReason);

            viewmodel.TransferID = transferID;
            viewmodel.PatientName = forceclientname == true ? client.FullName() : client.FullName(flowinstance, receivingdepartment.Location?.OrganizationID);

            var sendingorganization = OrganizationHelper.GetSendingOrganization(uow, flowinstance.FlowInstanceID);
            var setprivacyoutgoingmail = OrganizationSettingBL.GetValue<bool>(uow, sendingorganization.OrganizationID, OrganizationSettingBL.SetPrivacyOutgoingMail);
            if (setprivacyoutgoingmail)
            {
                viewmodel.PatientName = client.PrivacyInitials();
            }

            viewmodel.DossierUrl = dossierurl;

            viewmodel.EmployeeID = employeeID;
            viewmodel.OrganizationID = receivingorganization.OrganizationID;

            return viewmodel;
        }

        public static EmailViewModel GetGenericEmail(IUnitOfWork uow, int transferID, int receivingdepartmentid, int sendingemployeeID, string dossierurl)
        {
            var receivingdepartment = DepartmentBL.GetByDepartmentID(uow, receivingdepartmentid);
            if (receivingdepartment?.Location?.Organization == null) return null;

            var receivingorganization = receivingdepartment.Location.Organization;

            if (skipSend(receivingdepartment, EmailType.Email)) return null;

            var employee = EmployeeBL.GetByID(uow, sendingemployeeID);
            var employeedepartment = DepartmentBL.GetByEmployeeID(uow, sendingemployeeID);

            var flowinstance = FlowInstanceBL.GetByTransferID(uow, transferID);
            Client client = flowinstance.Transfer.Client;

            var viewmodel = new EmailViewModel
            {
                ReceivingOrganization = receivingorganization.Name,
                ReceivingDepartment = receivingdepartment.FullName(),
                ReceivingLocation = receivingdepartment.Location.FullName(),
                ReceivingEmailAdress = GetEmailAddress(receivingdepartment, EmailType.Email),

                SendingUser = employee.FullName(),
                SendingLocation = employeedepartment.FullName(),
                SendingDepartment = employeedepartment.Location.FullName(),
                SendingOrganization = employeedepartment.Location.Organization.Name,
                SendingTelephoneNumber = employee.PhoneNumber,

                TransferID = transferID,
                PatientName = client.FullName(flowinstance, receivingdepartment.Location?.OrganizationID),
                DossierUrl = dossierurl,

                EmployeeID = sendingemployeeID,
                OrganizationID = receivingorganization.OrganizationID
            };

            return viewmodel;
        }

        public static EmailHAVVTViewModel GetEmailHAVVT(IUnitOfWork uow, int transferID, int departmentid, int employeeID, string dossierurl, EmailType emailtype, bool forceclientname = false)
        {
            var receivingdepartment = DepartmentBL.GetByDepartmentID(uow, departmentid);
            if (receivingdepartment?.Location?.Organization == null) return null;

            var receivingorganization = receivingdepartment.Location.Organization;

            if (skipSend(receivingdepartment, emailtype)) return null;

            var employee = EmployeeBL.GetByID(uow, employeeID);
            var employeedepartment = DepartmentBL.GetByEmployeeID(uow, employeeID);

            var flowinstance = FlowInstanceBL.GetByTransferID(uow, transferID);
            Client client = flowinstance.Transfer.Client;

            var viewmodel = new EmailHAVVTViewModel
            {
                RecievingOrganization = receivingorganization.Name,
                ReceivingDepartment = receivingdepartment.Name,
                ReceivingDepartmentFullName = receivingdepartment.FullName(),
                ReceivingLocation = receivingdepartment.Location.Name,
                RecievingEmailAdress = GetEmailAddress(receivingdepartment, emailtype),
                SendingUser = employee.FullName(),
                SendingLocation = employeedepartment.FullName(),
                SendingTelephoneNumber = employee.PhoneNumber,
                DossierUrl = dossierurl,
                EmployeeID = employeeID,
                OrganizationID = receivingorganization.OrganizationID,
                TransferID = transferID,
                PatientName = forceclientname == true ? client.FullName() : client.FullName(flowinstance, receivingdepartment.Location?.OrganizationID)
            };

            var sendingorganization = OrganizationHelper.GetSendingOrganization(uow, flowinstance.FlowInstanceID);
            var setprivacyoutgoingmail = OrganizationSettingBL.GetValue<bool>(uow, sendingorganization.OrganizationID, OrganizationSettingBL.SetPrivacyOutgoingMail);
            if (setprivacyoutgoingmail)
            {
                viewmodel.PatientName = client.PrivacyInitials();
            }

            return viewmodel;
        }

        public static EmailHATOViewModel GetEmailHATO(IUnitOfWork uow, int transferID, int departmentid, int employeeID, string dossierurl, EmailType emailtype, bool forceclientname = false)
        {
            var receivingdepartment = DepartmentBL.GetByDepartmentID(uow, departmentid);
            if (receivingdepartment?.Location?.Organization == null) return null;

            var receivingorganization = receivingdepartment.Location.Organization;

            if (skipSend(receivingdepartment, emailtype)) return null;

            var employee = EmployeeBL.GetByID(uow, employeeID);
            var employeedepartment = DepartmentBL.GetByEmployeeID(uow, employeeID);

            var flowinstance = FlowInstanceBL.GetByTransferID(uow, transferID);
            Client client = flowinstance.Transfer.Client;

            var viewmodel = new EmailHATOViewModel
            {
                RecievingOrganization = receivingorganization.Name,
                ReceivingDepartment = receivingdepartment.Name,
                ReceivingDepartmentFullName = receivingdepartment.FullName(),
                ReceivingLocation = receivingdepartment.Location.Name,
                RecievingEmailAdress = GetEmailAddress(receivingdepartment, emailtype),
                SendingUser = employee.FullName(),
                SendingLocation = employeedepartment.FullName(),
                SendingTelephoneNumber = employee.PhoneNumber,
                TransferID = transferID,
                PatientName = forceclientname == true ? client.FullName() : client.FullName(flowinstance, receivingdepartment.Location?.OrganizationID),
                DossierUrl = dossierurl,
                EmployeeID = employeeID,
                OrganizationID = receivingorganization.OrganizationID
            };

            return viewmodel;
        }

        public static EmailVVTVVTViewModel GetEmailVVTVVT(IUnitOfWork uow, int transferID, int departmentid, int employeeID, string dossierurl, EmailType emailtype, bool forceclientname = false)
        {
            var receivingdepartment = DepartmentBL.GetByDepartmentID(uow, departmentid);
            if (receivingdepartment?.Location?.Organization == null) return null;

            var receivingorganization = receivingdepartment.Location.Organization;

            if (skipSend(receivingdepartment, emailtype)) return null;

            var employee = EmployeeBL.GetByID(uow, employeeID);
            var employeedepartment = DepartmentBL.GetByEmployeeID(uow, employeeID);

            var flowinstance = FlowInstanceBL.GetByTransferID(uow, transferID);
            Client client = flowinstance.Transfer.Client;

            var viewmodel = new EmailVVTVVTViewModel();

            viewmodel.RecievingOrganization = receivingorganization.Name;
            viewmodel.ReceivingDepartment = receivingdepartment.Name;
            viewmodel.ReceivingDepartmentFullName = receivingdepartment.FullName();
            viewmodel.ReceivingLocation = receivingdepartment.Location.Name;
            viewmodel.RecievingEmailAdress = GetEmailAddress(receivingdepartment, emailtype);
            viewmodel.SendingUser = employee.FullName();
            viewmodel.SendingLocation = employeedepartment.FullName();
            viewmodel.SendingTelephoneNumber = employee.PhoneNumber;

            viewmodel.TransferID = transferID;
            viewmodel.PatientName = forceclientname == true ? client.FullName() : client.FullName(flowinstance, receivingdepartment.Location?.OrganizationID);
            viewmodel.DossierUrl = dossierurl;

            viewmodel.EmployeeID = employeeID;
            viewmodel.OrganizationID = receivingorganization.OrganizationID;

            return viewmodel;
        }

        public static EmailDossierFeedbackViewModel GetEmailDossierFeedback(IUnitOfWork uow, int frequencyid, DateTime timestamp, string url)
        {
            EmailDossierFeedbackViewModel viewmodel = null;
            var frequency = FrequencyBL.GetByID(uow, frequencyid);
            if (frequency != null)
            {
                List<DateTime> dates = frequency.GetPeriodDates();
                var flowinstance = FlowInstanceBL.GetByTransferID(uow, frequency.TransferID);
                if(flowinstance == null)
                {
                    return null;
                }

                var recievingdepartment = OrganizationHelper.GetRecievingDepartment(uow, flowinstance.FlowInstanceID);
                if(recievingdepartment == null)
                {
                    return null;
                }

                var emailtype = EmailType.TransferEmail;

                if (skipSend(recievingdepartment, emailtype))
                {
                    return null;
                }

                if (frequency.EmployeeID.HasValue)
                {
                    var pointuserinfo = PointUserInfoHelper.GetPointUserInfoByEmployeeID(uow, frequency.Employee.EmployeeID);

                    viewmodel = new EmailDossierFeedbackViewModel()
                    {

                        TransferID = frequency.TransferID,
                        ClientName = frequency.Transfer.Client.FullName(flowinstance, pointuserinfo.Organization?.OrganizationID),
                        FormTypeID = (int)frequency.GetFlowFormType(),
                        Type = frequency.Name,
                        Title = frequency.Name,
                        SendingEmployeeName = pointuserinfo.Employee.FullName(),
                        SendingEmployeeTelephoneNumber = pointuserinfo.Employee.PhoneNumber,
                        SendingLocationName = pointuserinfo.Location.Name,
                        SendingOrganizationName = pointuserinfo.Organization.Name,
                        StartDate = frequency.StartDate,
                        EndDate = frequency.EndDate,
                        Dates = dates,
                        Quantity = frequency.Quantity,
                        RecievingDepartmentID = recievingdepartment.DepartmentID,
                        FrequencyID = frequency.FrequencyID,
                        Timestamp = timestamp,
                        EmployeeID = pointuserinfo.Employee.EmployeeID,
                        OrganizationID = pointuserinfo.Organization.OrganizationID,
                        MailType = CommunicationLogType.Frequency,
                        RecievingEmailAdress = GetEmailAddress(recievingdepartment, emailtype),
                        DossierUrl = url
                    };

                    return viewmodel;
                }
            }

            return null;
        }
    }
}
