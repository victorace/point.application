﻿using Point.Business.Helper;
using Point.Database.Context;
using Point.Database.Extensions;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Extensions;
using Point.Models.Enums;
using Point.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web.Mvc;

namespace Point.Business.Logic
{
    public static class FlowWebFieldBL
    {
        public static FlowWebField FromModel(IUnitOfWork uow, FlowFieldAttribute flowFieldAttribute, FlowFieldAttributeException flowFieldAttributeException)
        {
            return FromModel(uow, flowFieldAttribute, flowFieldAttributeException, null, Source.DEFAULT, null);
        }

        //without viewdata or lookupBL
        public static FlowWebField FromModel(IUnitOfWork uow, FlowField flowField, string value, Source source, IDictionary<string, object> viewData)
        {
            FlowWebField fwf = new FlowWebField
            {
                FlowFieldID = flowField.FlowFieldID,
                FlowFieldAttributeID = -1,
                Name = flowField.Name,
                Description = flowField.Description,
                Label = flowField.Label,
                Type = flowField.Type,
                Required = false,
                Visible = true,
                ReadOnly = false,
                SignalOnChange = false,
                Format = flowField.Format,
                Regexp = flowField.Regexp,
                MaxLength = flowField.MaxLength,
                Value = value,
                Source = source,
                FlowFieldDataSource = FlowFieldDataSourceCacheBL.GetByFlowFieldDataSourceID(uow, flowField.FlowFieldDataSourceID, value, viewData)
            };

            fwf.DisplayValue = FlowFieldValueHelper.GetDisplayValue(fwf);

            return fwf;
        }

        public static FlowWebField FromModel(IUnitOfWork uow, FlowFieldAttribute flowFieldAttribute, FlowFieldAttributeException flowFieldAttributeException, FlowFieldValue flowFieldValue, IDictionary<string, object> viewData)
        {
            var source = flowFieldValue == null ? Source.DEFAULT : flowFieldValue.Source;
            return FromModel(uow, flowFieldAttribute, flowFieldAttributeException, flowFieldValue?.Value, source, viewData);
        }

        public static FlowWebField FromModel(IUnitOfWork uow, FlowFieldAttribute flowFieldAttribute, FlowFieldAttributeException flowFieldAttributeException, string value, Source source, IDictionary<string, object> viewData)
        {
            var isreadonly = flowFieldAttribute.ReadOnly;
            var visible = flowFieldAttribute.Visible;
            var required = flowFieldAttribute.Required;
            var signalonchange = flowFieldAttribute.SignalOnChange;
            var requiredByFlowFieldName = flowFieldAttribute.RequiredByFlowField?.Name;
            var requiredByFlowFieldValue = flowFieldAttribute.RequiredByFlowFieldValue;
            var requiredByFlowFieldLabel = flowFieldAttribute.RequiredByFlowField?.Label;

            if (flowFieldAttributeException != null)
            {
                isreadonly = flowFieldAttributeException.ReadOnly;
                visible = flowFieldAttributeException.Visible;
                required = flowFieldAttributeException.Required;
                signalonchange = flowFieldAttributeException.SignalOnChange;
                requiredByFlowFieldName = flowFieldAttributeException.RequiredByFlowField?.Name;
                requiredByFlowFieldValue = flowFieldAttributeException.RequiredByFlowFieldValue;
                requiredByFlowFieldLabel = flowFieldAttributeException.RequiredByFlowField?.Label;
            }

            FlowWebField fwf = new FlowWebField
            {
                FlowFieldID = flowFieldAttribute.FlowFieldID,
                FlowFieldAttributeID = flowFieldAttribute.FlowFieldAttributeID,
                Name = flowFieldAttribute.FlowField.Name,
                RequiredBy = requiredByFlowFieldName,
                RequiredByValue = requiredByFlowFieldValue,
                RequiredByLabel = requiredByFlowFieldLabel,
                Description = flowFieldAttribute.FlowField.Description,
                Label = flowFieldAttribute.FlowField.Label,
                Type = flowFieldAttribute.FlowField.Type,
                Required = required,
                Visible = visible,
                ReadOnly = isreadonly,
                SignalOnChange = signalonchange,
                Format = flowFieldAttribute.FlowField.Format,
                Regexp = flowFieldAttribute.FlowField.Regexp,
                MaxLength = flowFieldAttribute.FlowField.MaxLength,
                Value = value,
                Source = source,
                FlowFieldDataSource = FlowFieldDataSourceCacheBL.GetByFlowFieldDataSourceID(uow, flowFieldAttribute.FlowField.FlowFieldDataSourceID, value, viewData)
            };

            fwf.DisplayValue = FlowFieldValueHelper.GetDisplayValue(fwf);

            return fwf;
        }

        public static List<FlowWebField> FromModelList(IUnitOfWork uow, List<FlowFieldAttribute> flowFieldAttributes, List<FlowFieldAttributeException> flowFieldAttributeExceptions, List<FlowFieldValue> flowFieldValues, IDictionary<string, object> viewData)
        {
            List<FlowWebField> flowwebfields = new List<FlowWebField>();
            foreach (var flowFieldAttribute in flowFieldAttributes)
            {
                var flowfieldvalue = flowFieldValues.LastOrDefault(ffv => ffv.FlowFieldID == flowFieldAttribute.OriginFlowFieldID) ??
                                     flowFieldValues.LastOrDefault(ffv => ffv.FlowFieldID == flowFieldAttribute.FlowFieldID);

                var flowFieldAttributeException = flowFieldAttributeExceptions?.FilterSingleByFlowFieldAttributeID(flowFieldAttribute.FlowFieldAttributeID);

                flowwebfields.Add(FromModel(uow, flowFieldAttribute, flowFieldAttributeException, flowfieldvalue, viewData));
            }

            return flowwebfields;
        }

        public static List<FlowWebField> GetFastDICAValuesByFlowInstance(IUnitOfWork uow, int flowinstancid)
        {
            //Highly optimized(only primarily needed data from the database)
            var flowFieldValueList = FlowFieldValueBL.GetDICAFlowFieldValues(uow, flowinstancid)
                .Select(ffv =>
                new FlowFieldValueModel()
                {
                    Value = ffv.Value,
                    FlowFieldID = ffv.FlowFieldID,
                    FlowFieldDataSourceID = ffv.FlowField.FlowFieldDataSourceID
                }).ToList();

            List<FlowWebField> flowWebFields = flowFieldValueList.Select(ffv => new FlowWebField()
            {
                Value = ffv.Value,
                FlowFieldID = ffv.FlowFieldID,
                DisplayValue = ffv.Value
            }).ToList();

            //Displayvalue niet nodig

            return flowWebFields;
        }

        public static IEnumerable<FlowWebField> GetFastReadModelValuesByFlowInstance(IUnitOfWork uow, int flowinstancid, IDictionary<string, object> viewData)
        {
            //Highly optimized(only primarily needed data from the database)
            var flowFieldValueList = FlowFieldValueBL.GetReadmodelFlowFieldValues(uow, flowinstancid).ToList();
            
            var flowWebFields = flowFieldValueList.Select(ffv => new FlowWebField()
            {
                Value = ffv.Value,
                DisplayValue = ffv.Value,
                FlowFieldID = ffv.FlowFieldID,
                FlowFieldDataSource = FlowFieldDataSourceCacheBL.GetByFlowFieldDataSourceID(uow, ffv.FlowFieldDataSourceID, ffv.Value, viewData)
            }).ToList();

            flowWebFields.ForEach(fwf =>
            {
                fwf.DisplayValue = FlowFieldValueHelper.GetDisplayValue(fwf);
            });

            return flowWebFields;
        }
        
        public static List<FlowWebField> GetByTransferID(IUnitOfWork uow, int transferID, PointUserInfo userinfo)
        {
            var flowinstance = FlowInstanceBL.GetByTransferID(uow, transferID);
            if (flowinstance == null) return new List<FlowWebField>();

            List<FlowFieldAttribute> flowFieldAttributeList = FlowFieldAttributeBL.GetByFlowDefinitionID(uow, flowinstance.FlowDefinitionID).ToList();
            List<FlowFieldValue> flowFieldValueList = FlowFieldValueBL.GetByTransferID(uow, transferID).Where(it => !String.IsNullOrEmpty(it.Value)).ToList();
            List<FlowFieldAttributeException> flowFieldAttributeexceptions = FlowFieldAttributeExceptionBL.GetByRegioIDAndOrganizationID(uow, userinfo.Region.RegionID, userinfo.Organization.OrganizationID).ToList();

            var fakeviewdata = new Dictionary<string, object>() { { "TransferID", transferID } };

            List<FlowWebField> flowWebFields = FromModelList(uow, flowFieldAttributeList, flowFieldAttributeexceptions, flowFieldValueList, fakeviewdata);

            var dictionary = new Dictionary<string, object>();
            foreach (var flowWebField in flowWebFields)
            {
                flowWebField.DisplayValue = FlowFieldValueHelper.GetDisplayValue(flowWebField);
            }

            return flowWebFields;
        }

        public static IEnumerable<FlowWebField> GetFieldsFromFormSetVersionID(IUnitOfWork uow, int formSetVersionID, ViewDataDictionary viewData)
        {
            var pointuser = PointUserInfoHelper.GetPointUserInfo(uow);
            int regionID = pointuser.Region?.RegionID ?? -1;
            int organizationID = pointuser.Organization?.OrganizationID ?? -1;

            var formSetVersion = FormSetVersionBL.GetByID(uow, formSetVersionID);
            int formTypeID = formSetVersion.FormTypeID;

            List<FlowFieldAttribute> flowFieldAttributeList = FlowFieldAttributeBL.GetByFormTypeID(uow, formTypeID).ToList();
            List<FlowFieldAttributeException> flowFieldAttributeEceptionList = FlowFieldAttributeExceptionBL.GetByFormTypeIDPhaseDefinitionIDAndRegioIDAndOrganizationID(uow, formTypeID, formSetVersion.PhaseInstance.PhaseDefinitionID, regionID, organizationID).ToList();
            // Get the values to adopt from 'OriginFormTypeID'
            List<FlowFieldValue> flowFieldValueListOrigin = getFlowFieldValueListOriginFormType(uow, flowFieldAttributeList, formSetVersion.TransferID, formTypeID);

            // Get the values for the current formset and add previous values 
            List<FlowFieldValue> flowFieldValueList = FlowFieldValueBL.GetByFormSetVersionID(uow, formSetVersionID).ToList();
            flowFieldValueList.AddRange(flowFieldValueListOrigin);

            var frommodellist = FromModelList(uow, flowFieldAttributeList, flowFieldAttributeEceptionList, flowFieldValueList, viewData);

            // Set FlowFieldValue's coming from the from 'OriginFormTypeID' to ReadOnly
            var flowFieldIDsOfOriginReadOnly = flowFieldValueListOrigin.Select(ffv => ffv.FlowFieldID);
            frommodellist.Where(fwf => flowFieldIDsOfOriginReadOnly.Contains(fwf.FlowFieldID)).ToList().ForEach(fwf => { fwf.ReadOnly = true;});
            var flowFieldIDsWithOriginFormTypeID = flowFieldAttributeList.Where(ffa => ffa.FormTypeID == formTypeID && ffa.OriginFormTypeID != null).Select(ffv => ffv.FlowFieldID);
            frommodellist.Where(fwf => flowFieldIDsWithOriginFormTypeID.Contains(fwf.FlowFieldID)).ToList().ForEach(fwf => { fwf.ReadOnly = true; });

            return frommodellist;
        }

        // Overload
        public static IEnumerable<FlowWebField> GetFieldsFromFormTypeID(IUnitOfWork uow, int formTypeID, int phaseDefinitionID, int transferID, IDictionary<string, object> viewData)
        {
            var userinfo = PointUserInfoHelper.GetPointUserInfo(uow);

            return GetFieldsFromFormTypeID(uow, formTypeID, phaseDefinitionID, transferID,
                userinfo.Organization.OrganizationID, userinfo.Region.RegionID, viewData);
        }

        public static IEnumerable<FlowWebField> GetFieldsFromFormTypeID(IUnitOfWork uow, int formTypeID, int phaseDefinitionID, int transferID, int? organizationID, int? regionID, IDictionary<string, object> viewData)
        {
            var flowfieldattributes = FlowFieldAttributeBL.GetByFormTypeID(uow, formTypeID).ToList();

            var flowFieldAttributeEceptionList = regionID.HasValue && organizationID.HasValue
                    ? FlowFieldAttributeExceptionBL.GetByFormTypeIDPhaseDefinitionIDAndRegioIDAndOrganizationID(
                        uow, formTypeID, phaseDefinitionID, regionID.Value, organizationID.Value).ToList()
                    : new List<FlowFieldAttributeException>();

            // Get the values to adopt from 'OriginFormTypeID'
            var flowFieldValueListOrigin = getFlowFieldValueListOriginFormType(uow, flowfieldattributes, transferID, formTypeID);

            // Get the values to adopt from 'OriginFlowFieldID'
            var flowFieldAttributesFromOriginFlowField = flowfieldattributes.Where(ffa => ffa.FormTypeID == formTypeID && (ffa.OriginFlowFieldID > 0 || ffa.OriginCopyToAll)).ToList();

            // Get the OrginalFlowFieldID's we're dealing with
            var flowFieldValueListToCopy = getFlowFieldValueListOriginFlowField(uow, flowFieldAttributesFromOriginFlowField, transferID);

            var frommodellist = FromModelList(uow, flowfieldattributes, flowFieldAttributeEceptionList, flowFieldValueListOrigin, viewData);

            // Set FlowFieldValue's coming from the from OriginFormType to ReadOnly
            var flowFieldIDsOfOriginReadOnly = flowFieldValueListOrigin.Select(ffv => ffv.FlowFieldID);
            frommodellist.Where(fwf => flowFieldIDsOfOriginReadOnly.Contains(fwf.FlowFieldID)).ToList().ForEach(fwf => { fwf.ReadOnly = true; });
            var flowFieldIDsWithOriginFormTypeID = flowfieldattributes.Where(ffa => ffa.FormTypeID == formTypeID && ffa.OriginFormTypeID != null).Select(ffv => ffv.FlowFieldID);
            frommodellist.Where(fwf => flowFieldIDsWithOriginFormTypeID.Contains(fwf.FlowFieldID)).ToList().ForEach(fwf => { fwf.ReadOnly = true; });

            // Copy the values from the originFields to the current set
            foreach (var flowFieldAttributeFromOrigin in flowFieldAttributesFromOriginFlowField)
            {
                var flowFieldValueToCopy = flowFieldValueListToCopy.FirstOrDefault(ffv => ffv.FlowFieldID == flowFieldAttributeFromOrigin.OriginFlowFieldID) ??
                                           flowFieldValueListToCopy.FirstOrDefault(ffv => ffv.FlowFieldID == flowFieldAttributeFromOrigin.FlowFieldID && flowFieldAttributeFromOrigin.OriginCopyToAll);

                if (flowFieldValueToCopy == null) continue;

                frommodellist.Where(fwf => fwf.FlowFieldID == flowFieldAttributeFromOrigin.FlowFieldID && String.IsNullOrEmpty(fwf.Value)).ToList().ForEach(fwf =>
                {
                    SetValue(fwf, flowFieldValueToCopy.Value);
                });
            }

            return frommodellist;
        }

        private static List<FlowFieldValue> getFlowFieldValueListOriginFormType(IUnitOfWork uow, IEnumerable<FlowFieldAttribute> flowFieldAttributes, int transferID, int formTypeID)
        {
            var flowFieldAttributesFromOriginForm = flowFieldAttributes.Where(ffa => ffa.FormTypeID == formTypeID && ffa.OriginFormTypeID > 0).ToList();
            // Get the OriginalFormTypeID's we're dealing with
            var originFormTypeIDs = flowFieldAttributesFromOriginForm.Select(ffa => ffa.OriginFormTypeID.ConvertTo<int>()).Distinct().ToArray();
            // Get their corresponding FormSetVersion's which aren't inactive.
            var formSetVersionsNotInactive = FormSetVersionBL.GetByTransferIDAndFormTypeIDs(uow, transferID, originFormTypeIDs).ToList();

            List<FlowFieldValue> flowFieldValueListOrigin = new List<FlowFieldValue>();

            foreach (var originFormTypeID in originFormTypeIDs)
            {
                var formSetVersionOfOriginFormTypeID = formSetVersionsNotInactive.LastOrDefault(fsv => fsv.FormTypeID == originFormTypeID);
                if (formSetVersionOfOriginFormTypeID == null) continue;
                var flowFieldIDsOfOrigin = flowFieldAttributesFromOriginForm.Where(ffa => ffa.OriginFormTypeID == originFormTypeID).Select(ffa => ffa.OriginFlowFieldID.GetValueOrDefault(ffa.FlowFieldID)).ToArray();
                var originFlowFieldValues = FlowFieldValueBL.GetByFormsetVersionIDAndFlowFieldIDs(uow, formSetVersionOfOriginFormTypeID.FormSetVersionID, flowFieldIDsOfOrigin);
                flowFieldValueListOrigin.AddRange(originFlowFieldValues);
            }

            return flowFieldValueListOrigin;
        }

        private static List<FlowFieldValue> getFlowFieldValueListOriginFlowField(IUnitOfWork uow, List<FlowFieldAttribute> flowFieldAttributesFromOriginFlowField, int transferID)
        {
            var originFlowFieldIDs = flowFieldAttributesFromOriginFlowField.Where(ffa => ffa.OriginFlowFieldID != null).Select(ffa => ffa.OriginFlowFieldID.ConvertTo<int>())
                                    .Union(flowFieldAttributesFromOriginFlowField.Where(ffa => ffa.OriginCopyToAll).Select(ffa => ffa.FlowFieldID));
            // Get their corresponding FlowFieldValue's on forms/phases which aren't inactive.
            var flowFieldAttributesOrigin = FlowFieldAttributeBL.GetByFlowFieldIDs(uow, originFlowFieldIDs.Distinct()).ToArray();
            var originFormTypeIDs = flowFieldAttributesOrigin.Select(ffa => ffa.FormTypeID).Distinct().ToArray();
            var formSetVersionsNotInactive = FormSetVersionBL.GetByTransferIDAndFormTypeIDs(uow, transferID, originFormTypeIDs).ToList();
            var flowFieldValueListToCopy = new List<FlowFieldValue>();
            foreach (var originFormTypeID in originFormTypeIDs)
            {
                var formSetVersionOfOriginFormTypeID = formSetVersionsNotInactive.LastOrDefault(fsv => fsv.FormTypeID == originFormTypeID);
                if (formSetVersionOfOriginFormTypeID == null) continue;
                var flowFieldIDsOfOrigin = flowFieldAttributesOrigin.Where(ffa => ffa.FormTypeID == originFormTypeID).Select(ffa => ffa.FlowFieldID).ToArray();
                var originFlowFieldValues = FlowFieldValueBL.GetByFormsetVersionIDAndFlowFieldIDs(uow, formSetVersionOfOriginFormTypeID.FormSetVersionID, flowFieldIDsOfOrigin);
                flowFieldValueListToCopy.AddRange(originFlowFieldValues);
            }
            return flowFieldValueListToCopy;
        }

        public static List<FlowWebField> HandleOriginFormTypeFields(IUnitOfWork uow, List<FlowWebField> model, int formTypeID)
        {
            var flowFieldAttributes = FlowFieldAttributeBL.GetByFlowFieldIDsAndFormTypeID(uow, model.Select(m => m.FlowFieldID), formTypeID);
            foreach (var flowFieldAttribute in flowFieldAttributes.Where(ffa => ffa.OriginFormTypeID != null))
            {
                model.Where(fwf => fwf.FlowFieldID == flowFieldAttribute.FlowFieldID).ToList().ForEach(fwf => fwf.ReadOnly = true); // Values from other formtype/flowfield are stored there, treat them as readonly i.e. pass validation
            }
            return model;
        }

        public static void SetValuesOriginCopyToAll(IUnitOfWork uow, List<FlowWebField> model, int transferID, int originFormTypeID, LogEntry logentry, PointUserInfo userinfo, IDictionary<string, object> viewData, bool closeDossier = false)
        {
            var flowInstance = FlowInstanceBL.GetByTransferID(uow, transferID);
            var pointUserInfo = PointUserInfoHelper.GetPointUserInfo(uow);

            var flowFieldAttributes = FlowFieldAttributeBL.GetByFlowDefinitionID(uow, flowInstance.FlowDefinitionID)
                .Where(ffa => model.Select(m => m.FlowFieldID).Contains(ffa.FlowFieldID)).ToList();

            List<FlowFieldValue> allFlowFieldValues = FlowFieldValueBL.GetByFlowInstanceID(uow, flowInstance.FlowInstanceID).ToList();
            List<FlowFieldAttributeException> flowFieldAttributeEceptionList = FlowFieldAttributeExceptionBL.GetByRegioIDAndOrganizationID(uow, userinfo.Region.RegionID, userinfo.Organization.OrganizationID).ToList();
            List<PhaseInstance> allPhaseInstances = PhaseInstanceBL.GetByFlowInstanceId(uow, flowInstance.FlowInstanceID).ToList();
            List<FormSetVersion> allformsetversions = FormSetVersionBL.GetByTransferID(uow, transferID).ToList();

            // Only updating the fields in another form
            var attributestofill = flowFieldAttributes.Where(ffa => ffa.OriginCopyToAll && ffa.FormTypeID != originFormTypeID);

            foreach (var flowFieldAttribute in attributestofill)
            {
                var formSetVersions = allformsetversions.Where(fsv => fsv.FormTypeID == flowFieldAttribute.FormTypeID).ToList(); //FormSetVersionBL.GetByTransferIDAndFormTypeIDsNotInactive(uow, transferID, new int[] { flowFieldAttribute.FormTypeID }).ToList();
                if (closeDossier)
                {
                    //Alleen opslaan in de volgende formulieren
                    int[] limitedFormtypesToSave = {
                        (int)FlowFormType.AanvraagFormulierZHVVT,
                        (int)FlowFormType.RealizedCare,
                        (int)FlowFormType.RealizedCareHA,
                        (int)FlowFormType.AanvullendeGegevensTPZHVVT,
                        (int)FlowFormType.VastleggenBesluitVVTZHVVT,
                        (int)FlowFormType.AanvraagFormulierRzTP,
                        (int)FlowFormType.RouterenDossierZHVVT,
                        (int)FlowFormType.RouterenDossierRzTP
                    };
                    if (limitedFormtypesToSave.Contains(flowFieldAttribute.FormTypeID))
                    {
                        // we have FlowFields from another Form (Dossier formulieren) that have not yet been saved
                        var formSetVersion = formSetVersions.LastOrDefault();

                        var phaseDefinition = PhaseDefinitionCacheBL.GetByFlowDefinitionAndFormType(uow, flowInstance.FlowDefinitionID, flowFieldAttribute.FormTypeID);
                        if (phaseDefinition != null)
                        {
                            var phaseInstance = allPhaseInstances.Where(pi => pi.PhaseDefinitionID == phaseDefinition.PhaseDefinitionID).OrderByDescending(pi => pi.PhaseInstanceID).FirstOrDefault(); //PhaseInstanceBL.GetByFlowInstanceIDAndPhaseDefinition(uow, flowInstance.FlowInstanceID, phaseDefinition.PhaseDefinitionID);

                            ////Alleen van Dosserforms een nieuwe phaseinstance maken als deze niet bestaat
                            int[] limitedTypesToCreate = { (int)TypeID.FlowTransferForm, (int)TypeID.FlowTransferScreen };
                            if (phaseInstance == null && limitedTypesToCreate.Contains(flowFieldAttribute.FormType.TypeID.GetValueOrDefault(0)))
                            {
                                phaseInstance = PhaseInstanceBL.Create(uow, flowInstance.FlowInstanceID, phaseDefinition.PhaseDefinitionID, logentry);
                                //Otherwise it would create multiple
                                allPhaseInstances.Add(phaseInstance);

                                formSetVersion = FormSetVersionBL.CreateFormSetVersion(uow, transferID, flowFieldAttribute.FormTypeID, phaseInstance.PhaseInstanceID, pointUserInfo, logentry);
                                //Otherwise it would create multiple
                                allformsetversions.Add(formSetVersion);
                            }

                            //Als phaseinstance al bestaat mag een nieuwe formsetversion gemaakt worden
                            if (formSetVersion == null && phaseInstance != null)
                            {
                                formSetVersion = FormSetVersionBL.CreateFormSetVersion(uow, transferID, flowFieldAttribute.FormTypeID, phaseInstance.PhaseInstanceID, pointUserInfo, logentry);
                                //Otherwise it would create multiple
                                allformsetversions.Add(formSetVersion);
                            }
                        }

                        if (formSetVersion != null)
                        {
                            var flowWebFields = model.Where(fwf => fwf.FlowFieldID == flowFieldAttribute.FlowFieldID).ToList();
                            if(flowWebFields.Any())
                            {
                                var formsetflowfieldvalues = allFlowFieldValues.Where(it => it.FormSetVersionID == formSetVersion.FormSetVersionID && it.FlowFieldID == flowFieldAttribute.FlowFieldID).ToList();
                                SaveValues(uow, flowInstance.FlowInstanceID, formsetflowfieldvalues, flowWebFields, formSetVersion.FormSetVersionID, logentry);
                            }
                        }
                    }
                }

                foreach (var formSetVersion in formSetVersions)
                {
                    List<FlowWebField> flowWebFields = new List<FlowWebField>();
                    var formsetflowfieldvalues = allFlowFieldValues.Where(it => it.FormSetVersionID == formSetVersion.FormSetVersionID && it.FlowFieldID == flowFieldAttribute.FlowFieldID).ToList();
                    foreach (var flowFieldValue in formsetflowfieldvalues)
                    {
                        var flowFieldAttributeEception = flowFieldAttributeEceptionList.FilterSingleByFlowFieldAttributeID(flowFieldAttribute.FlowFieldAttributeID);
                        var flowWebField = FromModel(uow, flowFieldAttribute, flowFieldAttributeEception, flowFieldValue, viewData);
                        if (flowWebField != null)
                        {
                            var originFlowWebField = model.FirstOrDefault(fwf => fwf.FlowFieldID == flowFieldAttribute.FlowFieldID);
                            SetValue(flowWebField, originFlowWebField?.Value);
                            flowWebFields.Add(flowWebField);
                        }
                    }

                    if (flowWebFields.Any())
                    {
                        SaveValues(uow, flowInstance.FlowInstanceID, formsetflowfieldvalues, flowWebFields, formSetVersion.FormSetVersionID, logentry);
                    }
                }
            }
        }

        public static FlowWebField FromTransferIDAndName(IUnitOfWork uow, int transferID, string name, PointUserInfo userinfo, IDictionary<string, object> viewData)
        {
            //FlowField flowField = FlowFieldBL.GetByName(uow, name);
            FlowFieldAttribute flowFieldAttribute = FlowFieldAttributeBL.GetByName(uow, name);
            if (flowFieldAttribute == null)
                return null;

            FlowFieldValue flowFieldValue = FlowFieldValueBL.GetByTransferIDAndFlowFieldID(uow, transferID, flowFieldAttribute.FlowFieldID);
            if (flowFieldValue == null)
                return null;

            var flowfieldattributeexception = flowFieldAttribute.FlowFieldAttributeException.FilterSingleByPointUser(userinfo);

            return FromModel(uow, flowFieldAttribute, flowfieldattributeexception, flowFieldValue, viewData);
        }

        public static FlowWebField FromTransferIDAndFlowFieldID(IUnitOfWork uow, int transferID, int flowFieldID, PointUserInfo userinfo, IDictionary<string, object> viewData)
        {
            FlowFieldAttribute flowFieldAttribute = FlowFieldAttributeBL.GetFirstByFlowFieldID(uow, flowFieldID);
            if (flowFieldAttribute == null)
                return null;

            FlowFieldValue flowFieldValue = FlowFieldValueBL.GetByTransferIDAndFlowFieldID(uow, transferID, flowFieldAttribute.FlowFieldID);
            if (flowFieldValue == null)
                return null;

            var flowfieldattributeexception = flowFieldAttribute.FlowFieldAttributeException.FilterSingleByPointUser(userinfo);

            return FromModel(uow, flowFieldAttribute, flowfieldattributeexception, flowFieldValue, viewData);
        }

        public static FlowWebField FromNameAndUserinfo(IUnitOfWork uow, string name, PointUserInfo userinfo)
        {
            //FlowField flowField = FlowFieldBL.GetByName(uow, name);
            FlowFieldAttribute flowFieldAttribute = FlowFieldAttributeBL.GetByName(uow, name);
            if (flowFieldAttribute == null)
                return null;

            var flowfieldattributeexception = flowFieldAttribute.FlowFieldAttributeException.FilterSingleByPointUser(userinfo);

            return FromModel(uow, flowFieldAttribute, flowfieldattributeexception);
        }

        // SetValue
        public static void SetValue(IEnumerable<FlowWebField> flowWebFields, int flowFieldID, DateTime value)
        {
            SetValue(flowWebFields, flowFieldID, value, Source.USER);
        }

        public static void SetValue(IEnumerable<FlowWebField> flowWebFields, int flowFieldID, DateTime value, Source source)
        {
            SetValue(flowWebFields, flowFieldID, value.ToPointDateTimeDatabase());
        }

        public static void SetValue(IEnumerable<FlowWebField> flowWebFields, int flowFieldID, int value)
        {
            SetValue(flowWebFields, flowFieldID, value, Source.USER);
        }

        public static void SetValue(IEnumerable<FlowWebField> flowWebFields, int flowFieldID, int value, Source source)
        {
            SetValue(flowWebFields, flowFieldID, value.ToString(), source);
        }

        public static void SetValue(IEnumerable<FlowWebField> flowWebFields, int flowFieldID, int value, Source source, IDictionary<string, object> viewData)
        {
            SetValue(flowWebFields, flowFieldID, value.ToString(), source, viewData);
        }

        public static void SetValue(IEnumerable<FlowWebField> flowWebFields, int flowFieldID, string value)
        {
            var flowwebfield = flowWebFields.FirstOrDefault(fwf => fwf.FlowFieldID == flowFieldID);
            if (flowwebfield != null)
            {
                SetValue(flowwebfield, value);
            }
        }

        public static void SetValue(IEnumerable<FlowWebField> flowWebFields, int flowFieldID, string value, Source source)
        {
            var flowwebfield = flowWebFields.FirstOrDefault(fwf => fwf.FlowFieldID == flowFieldID);
            if (flowwebfield != null)
            {
                SetValue(flowwebfield, value, source);
            }
        }

        public static void SetValue(IEnumerable<FlowWebField> flowWebFields, int flowFieldID, string value, Source source, IDictionary<string, object> viewData)
        {
            var flowwebfield = flowWebFields.FirstOrDefault(fwf => fwf.FlowFieldID == flowFieldID);
            if (flowwebfield != null)
            {
                SetValue(flowwebfield, value, source, viewData);
            }
        }

        public static void SetValue(IEnumerable<FlowWebField> flowWebFields, int flowFieldID, FlowFieldValue value, Source source)
        {
            var flowwebfield = flowWebFields.FirstOrDefault(fwf => fwf.FlowFieldID == flowFieldID);
            if (flowwebfield != null)
            {
                SetValue(flowwebfield, value, source);
            }
        }

        public static void SetValue(FlowWebField flowwebfield, int value)
        {
            SetValue(flowwebfield, value.ToString(), Source.USER);
        }

        public static void SetValue(FlowWebField flowwebfield, int value, Source source)
        {
            SetValue(flowwebfield, value.ToString(), source);
        }

        public static void SetValue(FlowWebField flowwebfield, int value, Source source, IDictionary<string, object> viewData)
        {
            SetValue(flowwebfield, value.ToString(), source, viewData);
        }

        public static void SetValue(FlowWebField flowwebfield, DateTime value)
        {
            SetValue(flowwebfield, value.ToPointDateTimeDatabase(), Source.USER);
        }

        public static void SetValue(FlowWebField flowwebfield, DateTime value, Source source)
        {
            SetValue(flowwebfield, value.ToPointDateTimeDatabase(), source);
        }

        public static void SetValue(FlowWebField flowwebfield, bool value)
        {
            SetValue(flowwebfield, value.ToString(), Source.USER);
        }

        public static void SetValue(FlowWebField flowwebfield, bool value, Source source)
        {
            SetValue(flowwebfield, value.ToString(), source);
        }

        public static void SetValue(FlowWebField flowwebfield, FlowFieldValue value)
        {
            SetValue(flowwebfield, value?.Value, Source.USER);
        }

        public static void SetValue(FlowWebField flowwebfield, FlowFieldValue value, Source source)
        {
            SetValue(flowwebfield, value?.Value, source);
        }

        public static void SetValue(FlowWebField flowWebField, string value)
        {
            SetValue(flowWebField, value, Source.USER);
        }

        public static void SetValue(FlowWebField flowWebField, string value, Source source)
        {
            SetValue(flowWebField, value, source, new Dictionary<string, object>());
        }

        public static void SetValue(FlowWebField flowWebField, string value, Source source, IDictionary<string, object> viewData)
        {
            if (flowWebField == null)
            {
                return;
            }

            if (value == null)
            {
                flowWebField.Value = null;
                flowWebField.DisplayValue = null;
                flowWebField.Source = source;
                return;
            }

            value = value.Trim();

            var parsedValue = value;
            if (flowWebField.Type == FlowFieldType.Checkbox)
            {
                if (value == "1" || value.ToLower() == "ja" || value.ToLower() == "true")
                {
                    parsedValue = "true";
                }
                else if (value == "0" || value.ToLower() == "nee" || value.ToLower() == "false")
                {
                    parsedValue = "false";
                }
            }
            else if (flowWebField.Type == FlowFieldType.RadioButton)
            {
                if (value == "1" || value.ToLower() == "ja" || value.ToLower() == "true")
                {
                    parsedValue = "ja";
                }
                else if (value == "0" || value.ToLower() == "nee" || value.ToLower() == "false")
                {
                    parsedValue = "nee";
                }
            }
            else if (flowWebField.Type == FlowFieldType.Date || flowWebField.Type == FlowFieldType.DateTime)
            {
                var formats = new List<string>()
                {
                    "yyyyMMddHHmmss", // HL7 FieldReceivedTemp format
                    "dd-MM-yyyy HH:mm:ss",
                    "yyyy-MM-dd HH:mm:ss.fff",
                    "yyyy-MM-dd HH:mm:ss",
                    "dd-MM-yyyy",
                    "yyyy-MM-dd"
                };
                if (!string.IsNullOrWhiteSpace(flowWebField.Format))
                {
                    formats.Insert(0, flowWebField.Format);
                }
                if (DateTime.TryParseExact(value, formats.ToArray(), CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime datetime))
                { 
                    parsedValue = datetime.ToPointDateTimeDatabase();
                }
            }

            if (!string.IsNullOrEmpty(flowWebField.FlowFieldDataSource?.MethodName))
            {
                using (var uow = new UnitOfWork<PointSearchContext>())
                {
                    var flowfield = FlowFieldBL.GetByFlowFieldID(uow, flowWebField.FlowFieldID);
                    flowWebField.FlowFieldDataSource = FlowFieldDataSourceCacheBL.GetByFlowFieldDataSourceID(uow, flowfield.FlowFieldDataSourceID, parsedValue, viewData);
                }
            }

            if (flowWebField.FlowFieldDataSource?.Options != null && flowWebField.FlowFieldDataSource?.Options.Any() == true)
            {
                if (!string.IsNullOrEmpty(parsedValue))
                {
                    if (parsedValue.Contains(","))
                    {
                        var values = parsedValue.Split(',').Where(v => v != null);
                        flowWebField.FlowFieldDataSource.Options.ForEach(it => it.Selected = values.Any(v => string.Equals(it.Value, v, StringComparison.CurrentCultureIgnoreCase)));
                    }
                    else
                    {
                        flowWebField.FlowFieldDataSource.Options.ForEach(it => it.Selected = string.Equals(it.Value, parsedValue, StringComparison.CurrentCultureIgnoreCase));
                    }
                }
            }

            flowWebField.Source = source;
            flowWebField.Value = parsedValue;
            flowWebField.DisplayValue = FlowFieldValueHelper.GetDisplayValue(flowWebField);
        }
        // SetValue

        public static void SaveValues(IUnitOfWork uow, int flowInstanceID, List<FlowFieldValue> flowFieldValues, IEnumerable<FlowWebField> flowWebFields, int formSetVersionID, LogEntry logentry)
        {
            uow.DatabaseConfiguration.AutoDetectChangesEnabled = false;

            string value = null;
            FlowField flowfield = null;

            foreach (var modelFlowWebField in flowWebFields)
            {
                flowfield = FlowFieldBL.GetByFlowFieldID(uow, modelFlowWebField.FlowFieldID);

                if (flowfield != null)
                {
                    // the FlowWebField.Format isn't sent to the page, so we don't get it back in the model.
                    // we set it here as it will be required when writing data back to the database
                    modelFlowWebField.Format = flowfield.Format;

                    value = modelFlowWebField.ToDatebaseValue();

                    FlowFieldValueBL.UpdateOrInsert(uow, flowInstanceID, formSetVersionID, flowfield.FlowFieldID,
                            value, modelFlowWebField.Source, logentry);
                }
            }

            uow.Save();
            uow.DatabaseConfiguration.AutoDetectChangesEnabled = true;
        }

        public static void SaveValues(IUnitOfWork uow, int flowInstanceID, List<FlowWebField> flowWebFields, int formSetVersionID, LogEntry logentry)
        {
            var flowFieldValues = FlowFieldValueBL.GetByFlowInstanceIDAndFormSetVersionID(uow, flowInstanceID, formSetVersionID).ToList();
            SaveValues(uow, flowInstanceID, flowFieldValues, flowWebFields, formSetVersionID, logentry);
        }

        public static void SetReadOnly(FlowWebField flowwebfield, bool isReadOnly)
        {
            if (flowwebfield == null) return;
            flowwebfield.ReadOnly = isReadOnly;
        }

        // TODO: maybe look at the caller and replace the individual calls for setting a value
        // to a single SetValue, removing the seperate call to SetSource - PP
        //  #16067
        //  Technical debt: Too many functions that set a FlowWebField/FlowFieldValue Value
        //  https://linkassist.visualstudio.com/POINT.Application/_workitems/edit/16067
        public static void SetSource(FlowWebField flowwebfield, Source source)
        {
            if (flowwebfield == null)
            {
                return;
            }

            flowwebfield.Source = source;
        }

        public static FlowWebFieldModelState Validate(IUnitOfWork uow, List<FlowWebField> flowWebFields, List<FlowFieldAttributeException> flowfieldattributeexceptions, ViewDataDictionary viewData)
        {
            FlowWebFieldModelState modelState = new FlowWebFieldModelState();

            int count = flowWebFields.ToList().Count;
            for (int i = 0; i < count; i++)
            {
                FlowWebField flowWebField = flowWebFields.ElementAt(i);
                FlowFieldAttribute flowFieldAttribute = FlowFieldAttributeBL.GetByID(uow, flowWebField.FlowFieldAttributeID);
                var flowfieldattributeexception = flowfieldattributeexceptions.FilterSingleByFlowFieldAttributeID(flowFieldAttribute.FlowFieldAttributeID);
                flowWebField = FromModel(uow, flowFieldAttribute, flowfieldattributeexception, flowWebField.Value, flowWebField.Source, viewData);

                FlowWebFieldValidator flowWebFieldValidator = new FlowWebFieldValidator(flowWebField, flowWebFields);
                if (flowWebFieldValidator.IsValid() == false)
                {
                    modelState.AddError(flowWebField.Name, flowWebFieldValidator.ErrorMessage);
                }
            }

            return modelState;
        }

        public static void TakeoverValue(IUnitOfWork uow, PointUserInfo pointuserinfo, int transferid, FlowFormType sourceformtype, 
            string sourceflowfieldname, string destinationflowfieldname, IEnumerable<FlowWebField> model, IDictionary<string, object> viewData, FlowFormType sourceformtype2 = FlowFormType.NoScreen)
        {
            FlowField sourceflowfield = FlowFieldBL.GetByName(uow, sourceflowfieldname);
            if (sourceflowfield == null)
            {
                return;
            }

            var destinationflowwebfield = model.FirstOrDefault(fwf => fwf.Name == destinationflowfieldname);
            if (destinationflowwebfield == null)
            {
                return;
            }

            var sourceflowfields = FlowFieldValueBL.GetByTransferIDAndFieldID(uow, transferid, sourceflowfield.FlowFieldID);

            var flowFieldValues = sourceflowfields as FlowFieldValue[] ?? sourceflowfields.ToArray();
            var sourceflowfieldvalue = flowFieldValues.FirstOrDefault(ffv => ffv.FormSetVersion.FormTypeID == (int)sourceformtype);
            if ((sourceflowfieldvalue == null || string.IsNullOrEmpty(sourceflowfieldvalue.Value)) && sourceformtype2 != FlowFormType.NoScreen)
            {
                // if the value from sourceformtype is empty extract the value from sourceformtype2
                sourceflowfieldvalue = flowFieldValues.FirstOrDefault(ffv => ffv.FormSetVersion.FormTypeID == (int)sourceformtype2);
            }

            if (sourceflowfieldvalue == null)
            {
                return;
            }

            var sourceflowwebfield = FromModel(uow, sourceflowfield, sourceflowfieldvalue.Value, sourceflowfieldvalue.Source, viewData);
            SetValue(destinationflowwebfield, sourceflowwebfield.Value);
        }

        public static void SyncValue(IUnitOfWork uow, PointUserInfo pointuserinfo, int transferid, int formsetversionid,
            FlowFormType sourceformtype, string sourceflowfieldname, FlowFormType destinationformtype, string destinationflowfieldname, LogEntry logentry, ViewDataDictionary viewData)
        {
            FlowField sourceflowfield = FlowFieldBL.GetByName(uow, sourceflowfieldname);
            if (sourceflowfield == null) return;

            FlowField destinationflowfield = FlowFieldBL.GetByName(uow, destinationflowfieldname);
            if (destinationflowfield == null) return;

            var formsetversions = FormSetVersionBL.GetByTransferIDAndFormTypeID(uow, transferid, (int)destinationformtype).Where(fsv => !fsv.PhaseInstance.Cancelled).ToList();

            var flowfieldvalue = FlowFieldValueBL.GetByFlowFieldIDAndFormSetVersionID(uow, sourceflowfield.FlowFieldID, formsetversionid)
                .FirstOrDefault(ffv => ffv.FormSetVersion.FormTypeID == (int)sourceformtype);
            if (flowfieldvalue == null) return;

            FlowWebField sourceflowwebfield = FromModel(uow, sourceflowfield, flowfieldvalue.Value, flowfieldvalue.Source, viewData);
            FlowWebField destinationflowwebfield = FromModel(uow, destinationflowfield, sourceflowwebfield.Value, sourceflowwebfield.Source, viewData);
            var flowinstance = FlowInstanceBL.GetByTransferID(uow, transferid);
            foreach (var formsetversion in formsetversions)
            {
                List<FlowWebField> flowwebfields = new List<FlowWebField> { destinationflowwebfield };
                SaveValues(uow, flowinstance.FlowInstanceID, flowwebfields, formsetversion.FormSetVersionID, logentry);
            }
        }
    }

    public static class FlowWebFieldExtensions
    {
        public static string ToDatebaseValue(this FlowWebField flowWebField)
        {
            string value = flowWebField.Value;
            if (!String.IsNullOrWhiteSpace(value))
            {
                if (flowWebField.Type == FlowFieldType.DateTime || flowWebField.Type == FlowFieldType.Date)
                {
                    string format = (!String.IsNullOrWhiteSpace(flowWebField.Format) ? flowWebField.Format :
                        (flowWebField.Type == FlowFieldType.DateTime ? FlowWebField.DEFAULT_DATETIME_FORMAT : FlowWebField.DEFAULT_DATE_FORMAT));
                    if (DateTime.TryParseExact(value, format, Thread.CurrentThread.CurrentCulture, DateTimeStyles.None, out DateTime dt))
                    {
                        value = dt.ToPointDateTimeDatabase();
                    }
                }
                else if (flowWebField.Type == FlowFieldType.Time)
                {
                    value = flowWebField.Value;
                }
                else if (flowWebField.Type == FlowFieldType.Checkbox)
                {
                    // handle hidden checkbox value contain 'true, false'
                    value = (value.Contains("true") ? "true" : "false");
                }
            }
            return value;
        }

        public static DropdownMultiSelectViewModel GetDropdownMultiSelectViewModel(this FlowWebField flowWebField, IUnitOfWork uow, int? formSetVersionID, bool isRequired, string classNames)
        {
            var options = FlowFieldValueHelper.GetOptionsMultiSelect(uow, formSetVersionID ?? -1, flowWebField.FlowFieldID, addSelectedNone: true, selectedOnly: false);

            if (options == null)
            {
                return new DropdownMultiSelectViewModel(null, null, null, isRequired, classNames);
            }

            var viewModel = new DropdownMultiSelectViewModel(flowWebField.Name, options.FirstOrDefault()?.Text, null, isRequired, classNames);

            foreach (var item in options.Where(i => !string.IsNullOrEmpty(i.Value)))
            {
                viewModel.Items.Add(new DropdownMultiSelectViewModel.DropdownMultiSelectItem { KeyValue = item.Value, DisplayTextFull = item.Text, IsSelected = item.Selected });
            }

            return viewModel;
        }

    }

    public class DoorlopendDossierModelState : FlowWebFieldModelState
    {
        public DoorlopendDossierModelState() : base()
        {
        }

        public DoorlopendDossierModelState(ModelStateDictionary modelstatedictionary, bool createNewDoorlopendDossier, int? sendingOrganizationID, string clientBSN) : base(modelstatedictionary)
        {
            if(createNewDoorlopendDossier)
            {
                using (var uow = new UnitOfWork<PointSearchContext>())
                {
                    var searchQuery = SearchQueryBL.GetSearchQueryDoorlopendDossier(uow);

                    if (sendingOrganizationID.HasValue)
                    {
                        searchQuery.ApplyOrganizationFilter(sendingOrganizationID.Value);
                    }

                    searchQuery.ApplyCivilServiceNumberFilter(clientBSN);
                    searchQuery.ApplyFrequencyStatusFilter(FrequencyDossierStatus.Active);

                    if (searchQuery.Queryable.Any())
                    {
                        modelstatedictionary.AddModelError("alreadyExists", "Een doorlopend dossier bestaat al voor deze patient. Ga naar het POINT patient overzicht-scherm, kies voor [Doorlopende dossiers] en zoek op patient naam om de patient te selecteren en bestaande DoorlopendeDossier bij te werken.");
                    }
                }
            }
        }
    }

    public class FlowWebFieldModelState : ModelStateDictionary
    {
        public FlowWebFieldModelState() : base()
        {
        }

        public FlowWebFieldModelState(ModelStateDictionary modelstatedictionary) : base(modelstatedictionary)
        {
        }

        public List<string> Errors => (from item in Values
                                       from error in item.Errors
                                       select error.ErrorMessage).ToList();

        public void AddError(string fieldName, string error)
        {
            base.AddModelError(fieldName, error);
        }
    }
}
