﻿using Point.Business.Helper;
using Point.Database.Context;
using Point.Database.Models;
using Point.Database.Repository;
using Point.Models.Enums;
using System.Collections.Generic;
using System.Linq;

namespace Point.Business.Logic
{
    public class FormTypeSelectionBL
    {
        public static IQueryable<FormTypeSelection> ByFlowDefinitionIDAndTypeID(IUnitOfWork uow, FlowDefinitionID flowDefinitionID, TypeID typeID) { 
            return uow.FormTypeSelectionRepository.Get(fts => fts.FlowDefinitionID == flowDefinitionID && fts.TypeID == typeID);
        }

        public static IQueryable<FormTypeSelection> ByFlowDefinitionIDAndTypeIDAndOrganizationID(IUnitOfWork uow, FlowDefinitionID flowDefinitionID, TypeID typeID, int organizationID)
        {
            return uow.FormTypeSelectionOrganizationRepository.Get(fto =>
                fto.OrganizationID == organizationID &&
                fto.FormTypeSelection.FlowDefinitionID == flowDefinitionID &&
                fto.FormTypeSelection.TypeID == typeID).Select(fto => fto.FormTypeSelection);
        }

        public static List<FormTypeSelection> ByTransferIDAndTypeID(int transferID, TypeID typeID)
        {
            using (var uow = new UnitOfWork<PointSearchContext>())
            {
                var flowInstance = FlowInstanceBL.GetByTransferIDWithDeleted(uow, transferID);
                var organizationselection = ByFlowDefinitionIDAndTypeIDAndOrganizationID(uow, flowInstance.FlowDefinitionID, typeID, flowInstance.StartedByOrganizationID ?? 0);
                if(organizationselection.Any())
                {
                    return organizationselection.ToList();
                }

                return ByFlowDefinitionIDAndTypeID(uow, flowInstance.FlowDefinitionID, typeID).ToList();
            }
        }
    }
}
