﻿using Point.Database.CacheModels;
using Point.Models.Enums;
using Point.Database.Repository;
using Point.Infrastructure.Caching;
using System.Collections.Generic;
using System.Linq;

namespace Point.Business.Logic
{
    public class SendFormTypeCacheBL
    {

        public static List<SendFormTypeCache> GetSendFormTypes(IUnitOfWork uow)
        {
            string cachestring = "GetSendFormTypes_Global";
            List<SendFormTypeCache> sendformtypes = null;

            sendformtypes = CacheService.Instance.Get<List<SendFormTypeCache>>(cachestring);

            if (sendformtypes == null)
            {
                sendformtypes = uow.SendFormTypeRepository.GetAll().Select(sft => new SendFormTypeCache()
                {
                    Allowed = sft.Allowed,
                    FormTypeID = sft.FormTypeID,
                    OrganizationID = sft.OrganizationID,
                    SendDestinationType = sft.SendDestinationType,
                    SendFormTypeID = sft.SendFormTypeID
                }).ToList();

                if (sendformtypes.Any())
                {
                    CacheService.Instance.Insert(cachestring, sendformtypes, "CacheLong");
                }
            }

            return sendformtypes;
        }

        public static IEnumerable<SendFormTypeCache> GetByOrganizationID(IUnitOfWork uow, int organizationID, SendDestinationType sendDestinationType)
        {
            var sendformtypes = GetSendFormTypes(uow).Where(sft => sft.OrganizationID == organizationID);
            if (sendformtypes.Any() == false)
            {
                sendformtypes = GetSendFormTypes(uow).Where(sft => sft.OrganizationID == null && sft.SendDestinationType == sendDestinationType);
            }
            return sendformtypes;
        }

        public static IEnumerable<SendFormTypeCache> GetByOrganizationIDAllowed(IUnitOfWork uow, int organizationID, SendDestinationType sendDestinationType)
        {
            var sendformtypes = GetByOrganizationID(uow, organizationID, sendDestinationType);
            return sendformtypes.Where(sft => sft.Allowed);
        }

        public static bool HasFlowFormType(IUnitOfWork uow, int organizationid, SendDestinationType sendDestinationType, FlowFormType flowFormType)
        {
            var sendformtypes = GetByOrganizationID(uow, organizationid, sendDestinationType);
            return sendformtypes.Any(sft => sft.FormTypeID == flowFormType && sft.Allowed);
        }

    }
}