﻿using System;
using System.Collections.Generic;
using System.Linq;
using Point.Business.Helper;
using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Helpers;

namespace Point.Business.Logic
{
    public class SignaleringBL
    {
        public static Signalering GetById(IUnitOfWork uow, int signaleringID)
        {
            return uow.SignaleringRepository.GetByID(signaleringID);
        }

        public static void CloseMultiple(IUnitOfWork uow, int[] signaleringID, int employeeID)
        {
            if (signaleringID == null)
            {
                return;
            }

            var datetime = DateTime.Now;

            var signaleringitems = uow.SignaleringRepository.Get(s => signaleringID.Contains(s.SignaleringID)).ToList();

            signaleringitems.ForEach(it =>
                {
                    it.Status = SignaleringStatus.Closed;
                    it.Completed = datetime;
                    it.CompletedByEmployeeID = employeeID;
                    it.LastUpdated = datetime;
                    it.LastUpdatedByEmployeeID = employeeID;
                }
            );

            uow.Save();
        }

        public static void SaveSignalering(IUnitOfWork uow, int signaleringID, string comment, SignaleringStatus status, PointUserInfo pointUserInfo)
        {
            var signalering = GetById(uow, signaleringID);
            var datetime = DateTime.Now;

            if (status == SignaleringStatus.Closed && signalering.Status != status)
            {
                signalering.Completed = datetime;
                signalering.CompletedByEmployeeID = pointUserInfo.Employee.EmployeeID;
            }

            signalering.LastUpdated = datetime;
            signalering.LastUpdatedByEmployeeID = pointUserInfo.Employee.EmployeeID;
            signalering.Comment = comment;
            signalering.Status = status;

            uow.Save();
        }

        public static void ExpireSignaleringByFlowInstanceID(IUnitOfWork uow, int flowinstanceID)
        {
            var signaleringitems = uow.SignaleringRepository.Get(si =>
                si.FlowInstanceID == flowinstanceID &&
                si.Status == SignaleringStatus.Open
                ).ToList();

            foreach (var signalering in signaleringitems)
            {
                signalering.Status = SignaleringStatus.Expired;
            }

            uow.Save();
        }

        public static void ExpireSignaleringByFlowInstanceIDAndDepartmentID(IUnitOfWork uow, int flowinstanceID, int departmentID)
        {
            var signaleringitems = uow.SignaleringRepository.Get(si =>
                si.FlowInstanceID == flowinstanceID &&
                si.Status == SignaleringStatus.Open &&
                si.SignaleringDestination.All(so => so.DepartmentID == departmentID)
                ).ToList();

            foreach (var signalering in signaleringitems)
            {
                signalering.Status = SignaleringStatus.Expired;
            }

            uow.Save();
        }

        public static IEnumerable<Signalering> GetOpenSignalsByFlowinstanceID(IUnitOfWork uow, int flowinstanceID)
        {
            var signals = uow.SignaleringRepository.Get(it =>
                    it.FlowInstanceID == flowinstanceID &&
                    it.Status == SignaleringStatus.Open
                );

            return signals;
        }
        public static EmailSignaleringReminder GetSignalsByEmployee(IUnitOfWork uow, Employee employee, IEnumerable<Signalering> signals)
        {
            var signalReminder = new EmailSignaleringReminder();
            IEnumerable<Signalering> userSignals = Enumerable.Empty<Signalering>();

            List<int> departmentids = null;
            List<int> locationids = null;
            List<int> organizationids = null;

            var role = Authorization.GetPointFlowRole(uow, employee.UserId);

            if (role == Role.None)
            {
                return signalReminder;
            }

            departmentids = employee.EmployeeDepartment.Select(x => x.DepartmentID)?.ToList();
            if (employee.DefaultDepartment != null && !departmentids.Contains(employee.DefaultDepartment.DepartmentID))
            {
                departmentids.Add(employee.DepartmentID);
            }

            locationids = uow.DepartmentRepository.Get(dep => departmentids.Contains(dep.DepartmentID)).Select(it => it.LocationID).Distinct().ToList();
            if (employee.DefaultDepartment.Location != null && !locationids.Contains(employee.DefaultDepartment.LocationID))
            {
                locationids.Add(employee.DefaultDepartment.LocationID);
            }

            organizationids = uow.LocationRepository.Get(loc => locationids.Contains(loc.LocationID)).Select(it => it.OrganizationID).Distinct().ToList();

            switch (role)
            {
                case Role.Global:
                case Role.Region:
                case Role.Organization:
                    userSignals = signals.Where(s =>
                    s.SignaleringDestination.Any(dest =>
                        organizationids.Any(orgid => dest.Location.OrganizationID == orgid)) &&
                        (s.SignaleringTarget.DepartmentTypeID == null || s.SignaleringTarget.DepartmentTypeID == employee.DefaultDepartment.DepartmentTypeID)
                    );
                    break;

                case Role.Location:
                    userSignals = signals.Where(s =>
                    s.SignaleringDestination.Any(dest =>
                        locationids.Any(locid => dest.LocationID == locid)) &&
                        (s.SignaleringTarget.DepartmentTypeID == null || s.SignaleringTarget.DepartmentTypeID == employee.DefaultDepartment.DepartmentTypeID)
                    );
                    break;
                case Role.Department:
                    userSignals = signals.Where(s =>
                    s.SignaleringDestination.Any(dest =>
                       departmentids.Contains((int)dest.DepartmentID)) &&
                       (s.SignaleringTarget.DepartmentTypeID == null || s.SignaleringTarget.DepartmentTypeID == employee.DefaultDepartment.DepartmentTypeID)
                   );
                   break;
            }

            if (userSignals.Any())
            {
                signalReminder.Url = ConfigHelper.GetAppSettingByName<string>("WebServer");
                signalReminder.SignalCount = userSignals.Count();
                signalReminder.LatestSignal = userSignals.Max(si => si.Created).ToString("dd-MM-yyyy HH:mm:ss");

             foreach (var si in userSignals)
                {
                    signalReminder.SenderSignalering.Add(new SenderSignalering
                    {
                        SignleringID = si.SignaleringID,
                        OrganizationName = si.CreatedByEmployee?.DefaultDepartment?.Location?.Organization?.Name,
                        SignaleringDateTime = si.Created.ToString("dd-MM-yyyy HH:mm:ss"),
                        SignaleringTypeComment = si.SignaleringTarget?.SignaleringTrigger?.Name //For extra info add: si.Message
                    });
                }
                
            }
            return signalReminder;
        }

        public static IEnumerable<Signalering> GetCurrentByPointUserInfo(IUnitOfWork uow, PointUserInfo pointUserInfo, SignaleringStatus? status = null, List<int> departmentIdFilter = null)
        {
            int? primarydepartmentid = pointUserInfo.Department?.DepartmentID;

            List<int> departmentids = null;
            List<int> locationids = null;
            List<int> organizationids = null;

            var primarydepartmenttypeid = pointUserInfo.Department.DepartmentTypeID;

            var role = pointUserInfo.Employee.PointRole;

            if (role == Role.None)
            {
                return Enumerable.Empty<Signalering>();
            }

            switch (role)
            {
                case Role.Global:
                case Role.Region:
                case Role.Organization:
                    organizationids = pointUserInfo.EmployeeOrganizationIDs;
                    break;

                case Role.Location:
                    locationids = pointUserInfo.EmployeeLocationIDs;
                    break;

                case Role.Department:
                    departmentids = pointUserInfo.EmployeeDepartmentIDs;
                    break;
            }
        
            if(departmentIdFilter != null && departmentIdFilter.Any())
            {
                departmentids = departmentIdFilter;
            }

            var signals = uow.SignaleringRepository.Get(s =>
                (status == null || s.Status == status) &&
                s.CreatedByDepartmentID != primarydepartmentid
            );

            if(departmentids != null && departmentids.Any())
            {
                signals = signals.Where(s =>
                    s.SignaleringDestination.Any(dest => 
                        departmentids.Contains((int)dest.DepartmentID)) && 
                        (s.SignaleringTarget.DepartmentTypeID == null || s.SignaleringTarget.DepartmentTypeID == primarydepartmenttypeid)
                    );
            }

            if (locationids != null && locationids.Any())
            {
                signals = signals.Where(s =>
                    s.SignaleringDestination.Any(dest => 
                        locationids.Any(locid => dest.LocationID == locid)) &&
                        (s.SignaleringTarget.DepartmentTypeID == null || s.SignaleringTarget.DepartmentTypeID == primarydepartmenttypeid)
                    );
            }

            if (organizationids != null && organizationids.Any())
            {
                signals = signals.Where(s =>
                    s.SignaleringDestination.Any(dest =>
                        organizationids.Any(orgid => dest.Location.OrganizationID == orgid)) &&
                        (s.SignaleringTarget.DepartmentTypeID == null || s.SignaleringTarget.DepartmentTypeID == primarydepartmenttypeid)
                    );
            }

            var bsn = ExternalPatientHelper.GetPatExternalReferenceBSN();
            var patientnr = ExternalPatientHelper.GetPatExternalReference();

            if (!String.IsNullOrEmpty(bsn))
            {
                signals = signals.Where(it => it.FlowInstance.Transfer.Client.CivilServiceNumber == bsn);
            }
            if (!String.IsNullOrEmpty(patientnr))
            {
                signals = signals.Where(it => it.FlowInstance.Transfer.Client.PatientNumber == patientnr);
            }

            return signals.OrderByDescending(it => it.Created);
        }

        public static IEnumerable<SignaleringEmployee> GetEmployees(IUnitOfWork uow, int[] employeeIDs)
        {

            return (from employees in uow.EmployeeRepository.Get(empl => employeeIDs.Contains(empl.EmployeeID))
                       join departmens in uow.DepartmentRepository.Get() on employees.DepartmentID equals departmens.DepartmentID
                       join locations in uow.LocationRepository.Get() on departmens.LocationID equals locations.LocationID
                       join organizations in uow.OrganizationRepository.Get() on locations.OrganizationID equals organizations.OrganizationID
                       select new SignaleringEmployee { Employee = employees, OrganizationName = organizations.Name });

        }

        public static IEnumerable<SignaleringClient> GetClients(IUnitOfWork uow, int[] flowInstanceIDs)
        {
            return (from flowinstances in uow.FlowInstanceRepository.Get(fi => flowInstanceIDs.Contains(fi.FlowInstanceID))
                    join transfers in uow.TransferRepository.Get() on flowinstances.TransferID equals transfers.TransferID
                    join clients in uow.ClientRepository.Get() on transfers.ClientID equals clients.ClientID
                    select new SignaleringClient { Client = clients, FlowInstance = flowinstances });
        }


        public static IEnumerable<Signalering> GetCurrentByFlowinstanceIDAndPointUserInfo(IUnitOfWork uow, int flowInstanceID, PointUserInfo pointUserInfo)
        {
            int? primarydepartmentid = pointUserInfo.Department?.DepartmentID;

            List<int> departmentids = null;
            List<int> locationids = null;
            List<int> organizationids = null;

            var primarydepartmenttypeid = pointUserInfo.Department.DepartmentTypeID;

            var role = pointUserInfo.Employee.PointRole;

            if (role == Role.None)
            {
                return Enumerable.Empty<Signalering>();
            }

            switch (role)
            {
                case Role.Global:
                case Role.Region:
                case Role.Organization:
                    organizationids = pointUserInfo.EmployeeOrganizationIDs;
                    break;

                case Role.Location:
                    locationids = pointUserInfo.EmployeeLocationIDs;
                    break;

                case Role.Department:
                    departmentids = pointUserInfo.EmployeeDepartmentIDs;
                    break;
            }

            var signals = uow.SignaleringRepository.Get(s =>
                s.FlowInstanceID == flowInstanceID &&
                s.Status == SignaleringStatus.Open &&
                s.CreatedByDepartmentID != primarydepartmentid
            );

            if (departmentids != null && departmentids.Any())
            {
                signals = signals.Where(s =>
                    s.SignaleringDestination.Any(dest => 
                        departmentids.Contains((int)dest.DepartmentID)) &&
                        (s.SignaleringTarget.DepartmentTypeID == null || s.SignaleringTarget.DepartmentTypeID == primarydepartmenttypeid)
                    );
            }

            if (locationids != null && locationids.Any())
            {
                signals = signals.Where(s =>
                    s.SignaleringDestination.Any(dest =>
                        locationids.Any(locid => dest.LocationID == locid)) &&
                        (s.SignaleringTarget.DepartmentTypeID == null || s.SignaleringTarget.DepartmentTypeID == primarydepartmenttypeid)
                    );
            }

            if (organizationids != null && organizationids.Any())
            {
                signals = signals.Where(s =>
                    s.SignaleringDestination.Any(dest =>
                        organizationids.Any(orgid => dest.Location.OrganizationID == orgid)) &&
                        (s.SignaleringTarget.DepartmentTypeID == null || s.SignaleringTarget.DepartmentTypeID == primarydepartmenttypeid)
                    );
            }

            return signals.OrderByDescending(it => it.Created);
        }

        public static void HandleSignalByFlowDefinitionFormTypeAction(IUnitOfWork uow, FlowInstance flowinstance, int formtypeid, string action, PointUserInfo pointUserInfo, string message)
        {
            decimal lastphase = FlowInstanceBL.GetLastPhaseNumByFlowInstanceID(uow, flowinstance.FlowInstanceID);
            var targets = SignaleringTargetBL.GetByFlowDefinitionFormTypeAction(uow, flowinstance.FlowDefinitionID, lastphase, formtypeid, action);
            if (targets != null)
            {
                fillSignalering(uow, flowinstance, targets, pointUserInfo, message);
            }
        }

        public static void HandleSignalByFlowDefinitionGeneralAction(IUnitOfWork uow, FlowInstance flowinstance, int generalactionid, PointUserInfo pointUserInfo, string message)
        {
            decimal lastphase = FlowInstanceBL.GetLastPhaseNumByFlowInstanceID(uow, flowinstance.FlowInstanceID);
            var targets = SignaleringTargetBL.GetByFlowDefinitionGeneralAction(uow, flowinstance.FlowDefinitionID, lastphase, generalactionid);
            if (targets != null)
            {
                fillSignalering(uow, flowinstance, targets, pointUserInfo, message);
            }
        }

        public static void HandleSignalByFlowDefinitionAction(IUnitOfWork uow, FlowInstance flowinstance, string action, PointUserInfo pointUserInfo, string message)
        {
            decimal lastphase = FlowInstanceBL.GetLastPhaseNumByFlowInstanceID(uow, flowinstance.FlowInstanceID);
            var targets = SignaleringTargetBL.GetByFlowDefinitionAction(uow, flowinstance.FlowDefinitionID, lastphase, action);
            if (targets != null)
            {
                fillSignalering(uow, flowinstance, targets, pointUserInfo, message);
            }
        }

        public static void HandleSignalByTransferAttachmentVM(IUnitOfWork uow, FlowInstance flowInstance, TransferAttachmentViewModel viewmodel, PointUserInfo pointUserInfo)
        {
            var generalaction = GeneralActionBL.GetByURL(uow, "TransferAttachment", "List");
            var generalactionid = generalaction?.GeneralActionID ?? default(int);

            if (viewmodel.SignalOnSave)
            {
                if (generalactionid > 0)
                {
                    var message = $"Bijlage '{viewmodel.Name}' toegevoegd";
                    HandleSignalByFlowDefinitionGeneralAction(uow, flowInstance, generalactionid, pointUserInfo, message);
                }
            }
        }

        private static void fillSignalering(IUnitOfWork uow, FlowInstance flowinstance, IEnumerable<SignaleringTarget> targets, PointUserInfo pointUserInfo, string message)
        {
            var currentitems = SignaleringBL.GetOpenSignalsByFlowinstanceID(uow, flowinstance.FlowInstanceID).ToList();
            
            foreach (var target in targets)
            {
                currentitems.Where(it =>
                        it.SignaleringTargetID == target.SignaleringTargetID &&
                        it.Message == message
                    ).ToList().ForEach(it => it.Status = SignaleringStatus.Expired);

                var signalering = new Signalering()
                {
                    Created = DateTime.Now,
                    CreatedByEmployeeID = pointUserInfo.Employee.EmployeeID,
                    CreatedByDepartmentID = pointUserInfo.Department.DepartmentID,
                    FlowInstanceID = flowinstance.FlowInstanceID,
                    SignaleringTargetID = target.SignaleringTargetID,
                    Message = message,
                    Status = SignaleringStatus.Open
                };

                var connecteddepartments = DepartmentBL.GetConnectedDepartments(uow, flowinstance.FlowInstanceID, target.OrganizationTypeID, target.FlowDirection);
                if (connecteddepartments.Count <= 0) continue;

                foreach (var department in connecteddepartments)
                {
                    signalering.SignaleringDestination.Add(new SignaleringDestination() { LocationID = department.LocationID, DepartmentID = department.DepartmentID });
                }

                uow.SignaleringRepository.Insert(signalering);
            }

            uow.Save();
        }
    }
}
