﻿using System.Collections.Generic;
using System.Linq;
using Point.Database.Models;
using Point.Database.Repository;

namespace Point.Business.Logic
{
    public class MutActionHealthInsurerBL
    {
        public static IEnumerable<MutActionHealthInsurer> GetByFormSetVersionID(IUnitOfWork uow, int formsetversionid)
        {
            return uow.MutActionHealthInsurerRepository.Get(mahi => mahi.FormSetVersionID == formsetversionid).OrderByDescending(it => it.MutActionHealthInsurerID);
        }
    }
}
