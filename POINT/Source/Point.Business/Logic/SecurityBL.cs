﻿using Point.Business.BusinessObjects;
using Point.Business.Helper;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Extensions;
using Point.Models.Enums;
using Point.Models.LDM.EOverdracht30.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Security;

namespace Point.Business.Logic
{
    public static class SecurityBL
    {
        public static List<string> TestValidPassword(string password)
        {
            List<string> errorList = new List<string>();
            if (password == null)
            {
                errorList.Add(String.Format("Het wachtwoord is niet gevuld"));
                return errorList;
            }

            var membership = Membership.Providers["SqlMembershipProviderWithSecurityQuestion"];

            //MinRequiredPasswordLength must be set in config, otherwise default value 7 is used.
            //MinRequiredPasswordLength and MinRequiredNonAlphanumericCharacters can be set in config. so they can have their own error message.
            //The rest of password conditions such as min one number and min one capital letter must be set in regex.

            if (password.Length < membership.MinRequiredPasswordLength)
            {
                errorList.Add(String.Format("Het wachtwoord is te kort, minimale lengte is {0}", membership.MinRequiredPasswordLength));
            }
            if (password.Count(c => !char.IsLetterOrDigit(c)) < membership.MinRequiredNonAlphanumericCharacters)
            {
                errorList.Add(String.Format("Het wachtwoord dient minimaal {0} speciale tekens te bevatten", membership.MinRequiredNonAlphanumericCharacters));
            }
            if (!new Regex(membership.PasswordStrengthRegularExpression).IsMatch(password)) //Note: text can differ from the specified regex, since it's in a different place...
            {
                errorList.Add("Het wachtwoord voldoet niet aan de volgende eisen: <ul><li>minimaal één cijfer</li><li>minimaal één hoofdletter</li></ul>");
            }

            return errorList;
        }

        public static void InvalidateHashCode(IUnitOfWork uow, string hashCode, bool isAuth, PointUserInfo pointUserInfo = null)
        {
            if (pointUserInfo?.Employee?.EmployeeID == null)
            {
                return;
            }

            uow.UsedHashCodesRepository.Insert(new UsedHashCodes
            {
                UsedHashCodeId = Guid.NewGuid(),
                CreatedDate = DateTime.Now,
                HashCode = hashCode,
                IsAuth = isAuth,
                EmployeeID = pointUserInfo.Employee.EmployeeID
            });
            uow.Save();
        }

        public static bool ValidateUsingHash(IUnitOfWork uow, SSOLoginBO ssoLoginBO)
        {
            var organization = OrganizationBL.GetByOrganizationID(uow, ssoLoginBO.OrganizationID());

            if (!ssoLoginBO.Date.IsToday() && !ssoLoginBO.Date.IsYesterday())
            {
                return false;
            }
            if (!organization.SSO.ConvertTo<bool>())
            {
                return false;
            }
            if (uow.UsedHashCodesRepository.Exists(uhc => uhc.HashCode == ssoLoginBO.Sign))
            {
                return false;
            }
            if (ssoLoginBO.Sign != ssoLoginBO.CalculatedSign(organization.SSOKey, GetAlgorithmFromString(organization.SSOHashType)))
            {
                return false;
            }

            return true;
        }

        public static PointUserInfo GetUserUsingSSO(IUnitOfWork uow, string externID, int organizationID)
        {
            if (String.IsNullOrEmpty(externID) || organizationID <= 0)
            {
                return null;
            }
            PointUserInfo pointUserInfo = null;
            var employee = EmployeeBL.GetByExternID(uow, externID, organizationID);
            if (employee != null)
            {
                pointUserInfo = PointUserInfoHelper.GetPointUserInfoByEmployeeID(uow, employee.EmployeeID);
            }
            return pointUserInfo;
        }

        public static PointUserInfo CreateNewAccountSSO(IUnitOfWork uow, SSOLoginBO ssoLoginBO)
        {
            LogCreateUserHelper logCreateUserHelper = new Logic.LogCreateUserHelper() { UnitOfWork = uow, SSOLoginBO = ssoLoginBO };
            logCreateUserHelper.InsertInit();

            if (ssoLoginBO == null || string.IsNullOrEmpty(ssoLoginBO.ExternalReference))
            {
                logCreateUserHelper.InsertError(string.Format("Parameters voor SSO zijn niet juist en/of 'ExternalReference' is leeg."));
                return null;
            }

            var epBO = new EmployeeParametersBO(ssoLoginBO.EmplReference, ssoLoginBO.ExternalReference, ssoLoginBO.OrganizationID());
            if (epBO.Errors.Any())
            {
                logCreateUserHelper.InsertError(string.Format("Parameters uit url zijn niet juist! Errors: {0}.", String.Join(";", epBO.Errors)));
                return null;
            }

            var employeeparameters = epBO.EmployeeParameters;
            var autoCreateSets = AutoCreateSetBL.GetSettingsCreateUserByOrganizationID(uow, ssoLoginBO.OrganizationID());
            if (autoCreateSets == null || autoCreateSets.Count() == 0)
            {
                logCreateUserHelper.InsertError(string.Format("Geen autocreate settings gedefinieerd."));
                return null;
            }

            var autoCreateSet = AutoCreateSetBL.GetByAutoCreateCode(autoCreateSets, employeeparameters.AutoCreateCode);
            if (!string.IsNullOrEmpty(employeeparameters.AutoCreateCode) && autoCreateSet == null)
            {
                logCreateUserHelper.InsertError(string.Format("Er bestaat geen autocreateset met AutoCreateCode: [{0}].", employeeparameters.AutoCreateCode));
            }

            if (autoCreateSet == null)
            {
                autoCreateSet = AutoCreateSetBL.GetPreferredByLocationOrDefault(autoCreateSets, employeeparameters.Location);
                if (autoCreateSet == null)
                {
                    logCreateUserHelper.InsertError(string.Format("Er bestaat geen autocreateset bij deze organisatie welke voldoen aan IsDefaultForOrganization of doorgegeven LocationName [{0}].", employeeparameters.Location));
                    return null;
                }
            }

            Department defaultDepartment = null;

            if (autoCreateSet.DefaultDossierLevel == Role.Department.ToString() && !string.IsNullOrEmpty(employeeparameters.Department))
            {
                //Department level (match alleen op afdelingen in setje)
                defaultDepartment = autoCreateSet.AutoCreateSetDepartment.Select(it => it.Department).FirstOrDefault(dep => dep.Name == employeeparameters.Department);
            }
            else if (!string.IsNullOrEmpty(autoCreateSet.DefaultDossierLevel) && !string.IsNullOrEmpty(employeeparameters.Department))
            {
                // >= Location level (mag matchen met afdelingen van lokatie)
                defaultDepartment = autoCreateSet.Location.Department.FirstOrDefault(dep => dep.Name == employeeparameters.Department);
            }

            if (defaultDepartment == null)
            {
                //Als geen is opgegeven dan de eerste afdeling uit setje
                defaultDepartment = autoCreateSet.AutoCreateSetDepartment.Select(it => it.Department).FirstOrDefault();
            }

            if (defaultDepartment == null)
            {
                logCreateUserHelper.InsertError(string.Format("Er bestaan geen afdelingen welke voldoen aan de doorgegeven afdeling [{0}] of de default afdeling.", employeeparameters.Department));
                return null;
            }

            var password = autoCreateSet.DefaultPassword;
            try
            {
                byte[] passwordBytes = password.HexStringToByteArray();
                password = Encoding.UTF8.GetString(MachineKey.Unprotect(passwordBytes, "DefaultPassword"));
            }
            catch
            {
                //Waarschijnlijk unprotected password
                password = autoCreateSet.DefaultPassword;
            }

            if (TestValidPassword(password).Count() > 0)
            {
                logCreateUserHelper.InsertError(string.Format("Default password [{0}] voldoet niet aan Membership-eisen [{1}]", password, string.Join(";", TestValidPassword(password))));
            }

            Role role = Role.None;
            if (!Enum.TryParse<Role>(autoCreateSet.DefaultDossierLevel, out role))
            {
                logCreateUserHelper.InsertError(string.Format("Rolename [{0}] is niet geldig.", autoCreateSet.DefaultDossierLevel));
                return null;
            }

            string userName = string.Concat(autoCreateSet.UsernamePrefix, employeeparameters.ExternID);
            if (Membership.GetUser(userName) != null)
            {
                logCreateUserHelper.InsertError(string.Format("Gebruikersnaam [{0}] bestaat al.", userName));
                return null;
            }

            PointUserInfo pointUserInfo = GetUserUsingSSO(uow, userName, ssoLoginBO.OrganizationID());
            if (pointUserInfo != null)
            {
                logCreateUserHelper.InsertError(string.Format("Gebruiker [{0}] bestaat al met employeeID: {1}", userName, pointUserInfo.Employee?.EmployeeID));
                return pointUserInfo;
            }

            var membershipUser = Membership.CreateUser(userName, password);
            if (role != Role.None)
            {
                Roles.AddUserToRole(userName, role.ConvertTo<string>());
            }

            var employee = fillEmployee(employeeparameters, autoCreateSet, defaultDepartment);
            employee.UserId = Guid.Parse(membershipUser.ProviderUserKey.ToString());

            EmployeeBL.Save(uow, employee);

            if (employee.EmployeeID <= 0)
            {
                logCreateUserHelper.InsertError(string.Format("Het is niet gelukt de gebruiker aan te maken"));
                return null;
            }
            else
            {
                pointUserInfo = PointUserInfoHelper.GetPointUserInfoByEmployeeID(uow, employee.EmployeeID);
                logCreateUserHelper.InsertDone(pointUserInfo);
                return pointUserInfo;
            }
        }

        public static Employee CreateNewAccount(IUnitOfWork uow, int organizationID, string autoCreateCode, string externID,
            string firstName, string middleName, string lastName, Geslacht? gender, string position, string phoneNumber, string email, string big)
        {
            var timestamp = DateTime.Now;
            Employee employee = null;

            try
            {
                var autoCreateSets = AutoCreateSetBL.GetSettingsCreateUserByOrganizationID(uow, organizationID);
                if (autoCreateSets != null)
                {
                    var autoCreateSet = autoCreateSets.FirstOrDefault(acs => acs.AutoCreateCode == autoCreateCode);
                    if (autoCreateSet == null)
                    {
                        // fall back to the Organization default AutoCreateSet (if there is one)
                        autoCreateSet = autoCreateSets.FirstOrDefault(acs => acs.IsDefaultForOrganization);
                    }

                    if (autoCreateSet == null)
                    {
                        LogCreateUserBL.Insert(uow, organizationID, autoCreateCode, externID, $"Er bestaat geen autocreateset met AutoCreateCode: [{autoCreateCode}].", false, timestamp);
                        return null;
                    }

                    Department department = autoCreateSet.AutoCreateSetDepartment.Select(d => d.Department).FirstOrDefault();
                    if (department == null)
                    {
                        LogCreateUserBL.Insert(uow, organizationID, autoCreateCode, externID, $"Department is niet geldig.", false, timestamp);
                        return null;
                    }

                    var password = autoCreateSet.DefaultPassword;
                    try
                    {
                        byte[] passwordBytes = password.HexStringToByteArray();
                        password = Encoding.UTF8.GetString(MachineKey.Unprotect(passwordBytes, "DefaultPassword"));
                    }
                    catch
                    {
                        // Waarschijnlijk unprotected password
                        password = autoCreateSet.DefaultPassword;
                    }

                    if (TestValidPassword(password).Any())
                    {
                        var failedPasswordConstraints = string.Join(";", TestValidPassword(password));
                        LogCreateUserBL.Insert(uow, organizationID, autoCreateCode, externID, $"Default password [{password}] voldoet niet aan Membership-eisen [{failedPasswordConstraints}].", false, timestamp);
                        return null;
                    }

                    if (!Enum.TryParse<Role>(autoCreateSet.DefaultDossierLevel, out Role role))
                    {
                        LogCreateUserBL.Insert(uow, organizationID, autoCreateCode, externID, $"Rolename [{autoCreateSet.DefaultDossierLevel}] is niet geldig.", false, timestamp);
                        return null;
                    }

                    if (string.IsNullOrEmpty(externID))
                    {
                        LogCreateUserBL.Insert(uow, organizationID, autoCreateCode, null, $"ExternID is niet geldig.", false, timestamp);
                        return null;
                    }

                    string username = $"{autoCreateSet.UsernamePrefix}{externID}";
                    if (Membership.GetUser(username) != null)
                    {
                        // TODO maybe we can create addtional usernames with a postfix and try again?
                        LogCreateUserBL.Insert(uow, organizationID, autoCreateCode, externID, $"Gebruikersnaam [{username}] bestaat al.", false, timestamp);
                        return null;
                    }

                    PointUserInfo pointUserInfo = GetUserUsingSSO(uow, username, organizationID);
                    if (pointUserInfo != null)
                    {
                        LogCreateUserBL.Insert(uow, organizationID, autoCreateCode, externID, $"Gebruiker [{username}] bestaat al met employeeID [pointUserInfo.Employee?.EmployeeID]", false, timestamp);
                        return EmployeeBL.GetByID(uow, pointUserInfo.Employee.EmployeeID);
                    }

                    var membershipUser = Membership.CreateUser(username, password);

                    if (role != Role.None)
                    {
                        Roles.AddUserToRole(username, role.ConvertTo<string>());
                    }

                    employee = fillEmployee(autoCreateSet,
                            department,
                            firstName,
                            middleName,
                            lastName,
                            gender == null ? autoCreateSet.DefaultGender : gender,
                            position,
                            string.IsNullOrEmpty(phoneNumber) ? autoCreateSet.DefaultPhoneNumber : phoneNumber,
                            string.IsNullOrEmpty(email) ? autoCreateSet.DefaultEmailAddress : email,
                            false,
                            externID,
                            autoCreateSet.ChangePasswordByEmployee,
                            big,
                            autoCreateSet.ReadOnlySender,
                            autoCreateSet.ReadOnlyReciever
                        );

                    employee.UserId = Guid.Parse(membershipUser.ProviderUserKey.ToString());

                    EmployeeBL.Save(uow, employee);

                    LogCreateUserBL.Insert(uow, organizationID, autoCreateCode, externID, $"Gebruiker [{username}] is aangemaakt. EmployeeID [{employee.EmployeeID}].", true, timestamp);
                }
            }
            catch (Exception ex)
            {
                string errormessage = ex.Message;
                if (!string.IsNullOrEmpty(ex.StackTrace))
                {
                    errormessage = $"{errormessage} {ex.StackTrace}";
                }
                LogCreateUserBL.Insert(uow, organizationID, autoCreateCode, externID, $"ERROR {errormessage}", false, timestamp);
                employee = new Employee();
            }

            return employee;
        }

        private static Employee fillEmployee(EmployeeParameters employeeParameters, AutoCreateSet autoCreateSet, Department department)
        {
            return fillEmployee(autoCreateSet,
                department,
                employeeParameters.FirstName,
                employeeParameters.MiddleName,
                employeeParameters.LastName,
                autoCreateSet.DefaultGender,
                null,
                (String.IsNullOrEmpty(employeeParameters.PhoneNumber) ? autoCreateSet.DefaultPhoneNumber : employeeParameters.PhoneNumber),
                autoCreateSet.DefaultEmailAddress,
                false,
                employeeParameters.ExternID,
                autoCreateSet.ChangePasswordByEmployee,
                null,
                autoCreateSet.ReadOnlySender,
                autoCreateSet.ReadOnlyReciever
            );
        }

        private static Employee fillEmployee(AutoCreateSet autoCreateSet, Department department,
            string firstName, string middleName, string lastName, Geslacht? gender, string position, string phoneNumber, string email,
            bool inactive, string externID, bool changePasswordByEmployee, string big, bool readonlysender = false, bool readonlyreceiver = false)
        {
            var employee = new Employee()
            {
                FirstName = firstName,
                MiddleName = middleName,
                LastName = lastName,
                PhoneNumber = phoneNumber,
                ExternID = externID,
                Gender = gender,
                InActive = inactive,
                ChangePasswordByEmployee = changePasswordByEmployee,
                EmailAddress = email,
                Position = position,
                DepartmentID = department.DepartmentID,
                CreatedWithAutoCreateSetID = autoCreateSet.AutoCreateSetID,
                BIG = big,
                ReadOnlySender = readonlysender,
                ReadOnlyReciever = readonlyreceiver
            };

            var timestamp = DateTime.Now;
            foreach (var extradepartment in autoCreateSet.AutoCreateSetDepartment.Where(it => it.DepartmentID != department.DepartmentID).ToList())
            {
                employee.EmployeeDepartment.Add(new EmployeeDepartment()
                {
                    DepartmentID = extradepartment.DepartmentID,
                    ModifiedTimeStamp = timestamp
                });
            }

            return employee;
        }

        public static HashAlgorithm GetAlgorithmFromString(string algorithm)
        {
            HashAlgorithm hashAlgorithm = new SHA256Managed();
            if (String.IsNullOrEmpty(algorithm) || algorithm.Equals("SHA1", StringComparison.InvariantCultureIgnoreCase))
            {
                hashAlgorithm = new SHA1Managed();
            }
            return hashAlgorithm;
        }
    }
}
