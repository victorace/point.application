﻿using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure;
using Point.Infrastructure.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Point.Business.Logic
{
    public class TemplateDefaultBL
    {
        public static TemplateDefault FromViewModel(IUnitOfWork uow, TemplateViewModel templateviewmodel)
        {
            var model = uow.TemplateDefaultRepository.GetByID(templateviewmodel.TemplateDefaultID);
            if(model == null)
            {
                model = new TemplateDefault();
                uow.TemplateDefaultRepository.Insert(model);
            }

            model.TemplateTypeID = (int)templateviewmodel.TemplateTypeID;
            model.TemplateDefaultText = templateviewmodel.TemplateText;

            return model;
        }

        public static TemplateDefault GetByTemplateTypeID(IUnitOfWork uow, int templatetypeid)
        {
            return uow.TemplateDefaultRepository.FirstOrDefault(td => td.TemplateTypeID == templatetypeid);
        }

        public static List<TemplateDefault> GetByOrganizationTypeID(IUnitOfWork uow, int organizationtypeid)
        {
            return uow.TemplateDefaultRepository.Get(td => td.OrganizationTypeID == organizationtypeid).ToList();
        }

        public static List<TemplateDefault> GetAll(IUnitOfWork uow)
        {
            return uow.TemplateDefaultRepository.GetAll().ToList();
        }

        public static void Save(IUnitOfWork uow, TemplateDefault templatedefault)
        {
            try
            {
                uow.Save();
            }
            catch (Exception ex)
            {
                ExceptionHelper.SendMessageException(ex);
            }
        }
    }
}
