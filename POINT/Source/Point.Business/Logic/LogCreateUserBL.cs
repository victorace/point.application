﻿using Point.Business.BusinessObjects;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Extensions;
using System;

namespace Point.Business.Logic
{
    public class LogCreateUserHelper
    {
        public IUnitOfWork UnitOfWork { get; set; }
        public SSOLoginBO SSOLoginBO { get; set; }

        private string notCreated = "Gebruiker is niet aangemaakt!";

        public void InsertError(string comment)
        {
            comment = notCreated + " " + comment;
            LogCreateUserBL.Insert(UnitOfWork, SSOLoginBO, comment);
        }

        public LogCreateUser InsertInit()
        {
            return LogCreateUserBL.Insert(UnitOfWork, SSOLoginBO, "Parameters uit url zijn ingelezen.");
        }

        public LogCreateUser InsertDone(PointUserInfo pointUserInfo)
        {
            string comment = String.Format("Gebruiker [{0}] is aangemaakt. EmployeeID: {1}", pointUserInfo.Employee.UserName, pointUserInfo.Employee.EmployeeID);
            return LogCreateUserBL.Insert(UnitOfWork, SSOLoginBO, comment, pointUserInfo);
        }
    }

    public class LogCreateUserBL
    {
        public static LogCreateUser Insert(IUnitOfWork uow, SSOLoginBO ssoLoginBO, string comment, PointUserInfo pointUserInfo = null)
        {
            var logCreateUser = new LogCreateUser()
            {
                OrganizationID = ssoLoginBO.OrganizationID(),
                ExternID = ssoLoginBO.ExternalReference,
                UrlInlog = ssoLoginBO.Uri,
                ExtraParametersUrl = ssoLoginBO.EmplReference,
                HashCode = ssoLoginBO.Sign,
                IP = ssoLoginBO.ClientIP,
                Comment = comment,
                TimeStamp = DateTime.Now
            };

            if (pointUserInfo != null)
            {
                logCreateUser.Merge(pointUserInfo.Employee);
                logCreateUser.DepartmentID = pointUserInfo.Department.DepartmentID.ToString();
                logCreateUser.DepartmentName = pointUserInfo.Department.Name;
                logCreateUser.LocationID = pointUserInfo.Location.LocationID.ToString();
                logCreateUser.LocationName = pointUserInfo.Location.Name;
            }

            uow.LogCreateUserRepository.Insert(logCreateUser);
            uow.Save();

            return logCreateUser;
        }

        public static LogCreateUser Insert(IUnitOfWork uow, int organizationID, string autoCreateCode, string externID, string comment, bool userCreated, DateTime timestamp)
        {
            var logCreateUser = new LogCreateUser()
            {
                OrganizationID = organizationID,
                ExternID = externID,
                Comment = comment,
                UserCreated = userCreated,
                TimeStamp = DateTime.Now
            };

            uow.LogCreateUserRepository.Insert(logCreateUser);
            uow.Save();

            return logCreateUser;
        }
    }
}
