﻿using System.Linq;
using Point.Database.Models;
using Point.Database.Repository;

namespace Point.Business.Logic
{
    public class SignaleringTriggerBL
    {
        public static SignaleringTrigger GetByFlowDefinitionIDAndFormTypeID(IUnitOfWork uow, FlowInstance flowInstance, int formTypeID)
        {
            if (flowInstance == null)
            {
                return null;
            }

            decimal activephase = FlowInstanceBL.GetLastPhaseNumByFlowInstanceID(uow, flowInstance.FlowInstanceID);

            return uow.SignaleringTriggerRepository.Get(it => 
                it.FlowDefinitionID == flowInstance.FlowDefinitionID && 
                it.FormTypeID == formTypeID && 
                it.MinActivePhase <= activephase && 
                it.MaxActivePhase >= activephase).FirstOrDefault();
        }

        public static SignaleringTrigger GetByFlowDefinitionIDAndGeneralActionID(IUnitOfWork uow, FlowInstance flowInstance, int generalActionID)
        {
            if (flowInstance == null)
            {
                return null;
            }

            decimal activephase = FlowInstanceBL.GetLastPhaseNumByFlowInstanceID(uow, flowInstance.FlowInstanceID);

            return uow.SignaleringTriggerRepository.Get(it =>
                it.FlowDefinitionID == flowInstance.FlowDefinitionID &&
                it.GeneralActionID == generalActionID &&
                it.MinActivePhase <= activephase &&
                it.MaxActivePhase >= activephase).FirstOrDefault();
        }
    }
}
