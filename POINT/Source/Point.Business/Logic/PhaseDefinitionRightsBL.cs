﻿using Point.Database.CacheModels;
using Point.Database.Extensions;
using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Caching;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Point.Business.Logic
{
    public class PhaseDefinitionRightsBL
    {
        private static IEnumerable<PhaseDefinitionRights> getPhaseDefinitionRights(IUnitOfWork uow, bool usecache = true)
        {
            string cachestring = "GetPhaseDefinitionRights_Global";
            IList<PhaseDefinitionRights> phasedefinitionrights = null;

            if (usecache)
            {
                phasedefinitionrights = CacheService.Instance.Get<IList<PhaseDefinitionRights>>(cachestring);
            }

            if (phasedefinitionrights == null)
            {
                phasedefinitionrights = uow.PhaseDefinitionRightsRepository.GetAll().ToList();
                if (phasedefinitionrights.Any())
                {
                    CacheService.Instance.Insert(cachestring, phasedefinitionrights, "CacheLong");
                }
            }
            return phasedefinitionrights;
        }

        public static Rights GetBasicFlowRightsByFlowInstance(IUnitOfWork uow, PointUserInfo pointuserinfo, FlowInstance flowinstance)
        {
            if (flowinstance == null)
            {
                return Rights.None;
            }

            if (pointuserinfo.IsRegioAdmin || pointuserinfo.Employee.EditAllForms)
            {
                return Rights.X;
            }

            if (pointuserinfo.Employee.ReadOnlyReciever || pointuserinfo.Employee.ReadOnlySender)
            {
                var issender = flowinstance.IsSender(pointuserinfo);
                var isreceiver = flowinstance.IsReceiver(pointuserinfo);

                if ((isreceiver && pointuserinfo.Employee.ReadOnlyReciever) || (issender && pointuserinfo.Employee.ReadOnlySender))
                {
                    return Rights.R;
                }
            }

            return Rights.X;
        }

        public static Rights GetFormRightsPhaseDefinitionID(IUnitOfWork uow, PointUserInfo pointuserinfo, PhaseDefinitionCache phasedefinition, FlowInstance flowinstance)
        {
            if (phasedefinition == null)
            {
                return Rights.None;
            }

            if (pointuserinfo.IsRegioAdmin || pointuserinfo.Employee.EditAllForms)
            {
                return Rights.X;
            }

            if (pointuserinfo.Employee.ReadOnlyReciever || pointuserinfo.Employee.ReadOnlySender)
            {
                var sendingflowdefinitions = FlowDefinitionParticipationBL.GetByLocationIDAndDepartmentID(uow, pointuserinfo.Location.LocationID, pointuserinfo.Department.DepartmentID);

                var issender = sendingflowdefinitions.Any(it =>
                        it.FlowDefinitionID == phasedefinition.FlowDefinitionID &&
                        (it.FlowDirection == FlowDirection.Any || it.FlowDirection == FlowDirection.Sending));
                var isreceiver = sendingflowdefinitions.Any(it =>
                        it.FlowDefinitionID == phasedefinition.FlowDefinitionID &&
                        (it.FlowDirection == FlowDirection.Any || it.FlowDirection == FlowDirection.Receiving));

                if ((isreceiver && pointuserinfo.Employee.ReadOnlyReciever) || (issender && pointuserinfo.Employee.ReadOnlySender))
                {
                    return Rights.R;
                }
            }

            if (FlowInstanceBL.AccessByAccessGroup(uow, flowinstance, phasedefinition?.AccessGroup, pointuserinfo) == false)
            {
                return Rights.R;
            }

            var departmenttypeids = pointuserinfo.DepartmentTypeIDS;
            var organizationtypeid = pointuserinfo.Organization.OrganizationTypeID;

            var rights = Rights.None;
            var phasedefinitionrights = getPhaseDefinitionRights(uow).Where(frp => frp.PhaseDefinitionID == phasedefinition.PhaseDefinitionID);

            foreach (var phasedefinitionright in phasedefinitionrights.Where(frp => departmenttypeids.Contains(frp.DepartmentTypeID) && (frp.OrganizationTypeID == null || frp.OrganizationTypeID == organizationtypeid)))
            {
                Rights currentright = Rights.None;

                if(flowinstance != null)
                {
                    if (phasedefinitionright.FlowDirection == FlowDirection.Sending)
                    {
                        if (flowinstance.IsSender(pointuserinfo) == false)
                        {
                            //Dont process
                            continue;
                        }

                    }
                    else if (phasedefinitionright.FlowDirection == FlowDirection.Receiving)
                    {
                        if (flowinstance.IsReceiver(pointuserinfo) == false)
                        {
                            //Dont process
                            continue;
                        }
                    }
                }

                if (Enum.TryParse<Rights>(phasedefinitionright.MaxRights, out currentright))
                {
                    if (currentright > rights) // get the highest right configured for the combi roleid+organizationtypeid
                    {
                        rights = currentright;
                    }
                }

            }

            return rights;
        }
    }
}
