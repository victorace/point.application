﻿using Point.Database.Models;
using Point.Database.Repository;
using Point.Models.Iguana;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Point.Business.Logic
{
    public class IguanaChannelBL
    {
        public static string GetIguanaChannelStatus(IUnitOfWork uow)
        {
            var problems = uow.IguanaChannelRepository.Get(c => c.Monitor && (c.ChannelOn == false));
            var messagesqueued = problems.Any() ? problems.Sum(p => p.MessagesQueued) : 0;

            return problems.Any() ? string.Format("NOK {0} queued", messagesqueued) : "OK";
        }

        public static void UpdateIguanaChannelsFromIguanaStatus(IUnitOfWork uow, IguanaStatus iguanaStatus)
        {
            var currentchannelids = uow.IguanaChannelRepository.Get().Select(it => it.ChannelID).ToArray();
            var newchannelids = iguanaStatus.Channel.Select(c => c.Guid).ToArray();

            var idstodelete = currentchannelids.Where(c => newchannelids.Contains(c) == false).ToArray();
            uow.IguanaChannelRepository.Delete(c => idstodelete.Contains(c.ChannelID));

            var idstoupdate = currentchannelids.Where(c => newchannelids.Contains(c)).ToArray();
            foreach (var guid in idstoupdate)
            {
                var newchannelstatus = iguanaStatus.Channel.FirstOrDefault(n => n.Guid == guid.ToString());
                var current = uow.IguanaChannelRepository.GetByID(guid);
                current.ChannelName = newchannelstatus.Name;
                current.ChannelOn = newchannelstatus.Status == "on";
                current.Source = newchannelstatus.Source;
                current.SourceOn = newchannelstatus.SourceStatus == "on";
                current.Destination = newchannelstatus.Destination;
                current.DestinationOn = newchannelstatus.DestinationStatus == "on";
                current.MessagesQueued = string.IsNullOrEmpty(newchannelstatus.MessagesQueued) ? 0 : Convert.ToInt32(newchannelstatus.MessagesQueued);
                current.TimeStamp = DateTime.Now;
            }

            var idstoadd = newchannelids.Where(n => currentchannelids.Contains(n) == false);
            foreach (var guid in idstoadd)
            {
                var newchannelstatus = iguanaStatus.Channel.FirstOrDefault(n => n.Guid == guid.ToString());
                var iguanaChannel = new IguanaChannel
                {
                    ChannelID = guid,
                    ChannelName = newchannelstatus.Name,
                    ChannelOn = newchannelstatus.Status == "on",
                    Source = newchannelstatus.Source,
                    SourceOn = newchannelstatus.SourceStatus == "on",
                    Destination = newchannelstatus.Destination,
                    DestinationOn = newchannelstatus.DestinationStatus == "on",
                    MessagesQueued = string.IsNullOrEmpty(newchannelstatus.MessagesQueued) ? 0 : Convert.ToInt32(newchannelstatus.MessagesQueued),
                    TimeStamp = DateTime.Now
                };
                uow.IguanaChannelRepository.Insert(iguanaChannel);
            }

            uow.Save();
        }
    }
}