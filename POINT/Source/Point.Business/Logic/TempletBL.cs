﻿using System;
using System.Collections.Generic;
using System.IO;
using Point.Business.Helper;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using System.Linq;

namespace Point.Business.Logic
{
    public class TempletBL
    {
        public static Templet GetByID(IUnitOfWork uow, int templetID)
        {
            return uow.TempletRepository.GetByID(templetID);
        }

        public static IEnumerable<Templet> GetTempletsByRegionID(IUnitOfWork uow, int regionID)
        {
            return uow.TempletRepository.Get(t => t.RegionID == regionID && t.Deleted == false);
        }

        public static void DeleteByID(IUnitOfWork uow, int templetID)
        {
            Templet templet = uow.TempletRepository.GetByID(templetID);
            if (templet != null)
            {
                templet.Deleted = true;
                uow.Save();
            }
        }

        public static void FillFileData(Templet transferAttachment, System.Web.HttpPostedFileBase httpPostedFileBase)
        {
            byte[] bytes = new byte[httpPostedFileBase.ContentLength];
            httpPostedFileBase.InputStream.Read(bytes, 0, httpPostedFileBase.ContentLength);
            transferAttachment.ContentType = httpPostedFileBase.ContentType;
            transferAttachment.FileData = bytes;
            transferAttachment.FileName = Path.GetFileName(httpPostedFileBase.FileName);
            transferAttachment.FileSize = httpPostedFileBase.ContentLength;
        }

        public static int GetSortOrder(IUnitOfWork uow, int regionId )
        {
            var maxnum = GetTempletsByRegionID(uow, regionId)?.Max(mx => mx.SortOrder);
            return (maxnum.HasValue ? maxnum.Value + 1 : 1);
        }

        public static void Insert(IUnitOfWork uow, Templet templet)
        {
            templet.UploadDate = DateTime.Now;

            uow.TempletRepository.Insert(templet);
        }
    }
}
