﻿using Point.Database.Models;
using Point.Database.Repository;
using Point.Infrastructure.Extensions;
using Point.Models.Enums;
using Point.Models.ViewModels;
using System.Collections.Generic;
using System.Linq;

namespace Point.Business.Logic
{
    public class FlowFieldValueBL
    {
        public static IQueryable<FlowFieldValueModel> GetReadmodelFlowFieldValues(IUnitOfWork uow, int flowInstanceID)
        {
            return from 
                ffv in uow.FlowFieldValueRepository.Get()
                    join ff in uow.FlowFieldRepository.Get() on ffv.FlowFieldID equals ff.FlowFieldID
                    join fsv in uow.FormSetVersionRepository.Get() on ffv.FormSetVersionID equals fsv.FormSetVersionID
                    join ph in uow.PhaseInstanceRepository.Get() on fsv.PhaseInstanceID equals ph.PhaseInstanceID
                   where ffv.FlowInstanceID == flowInstanceID && ff.UseInReadModel && !fsv.Deleted &&!ph.Cancelled

            select new FlowFieldValueModel()
            {
                FlowFieldID = ff.FlowFieldID,
                Value = ffv.Value,
                FlowFieldDataSourceID = ff.FlowFieldDataSourceID
            };
        }

        public static IQueryable<FlowFieldValue> GetDICAFlowFieldValues(IUnitOfWork uow, int flowinstanceid)
        {
            int[] dicaflowfieldids = new int[] {
                FlowFieldConstants.ID_InZHIntraveneusGetrombolyseerd,
                FlowFieldConstants.ID_InZHIntraArterieelBehandeld,
                FlowFieldConstants.ID_Diagnose,
                FlowFieldConstants.ID_DICALand,
                FlowFieldConstants.ID_DiagnoseAankomstSEHDatum,
                FlowFieldConstants.ID_DiagnoseAankomstSEHTijd,
                FlowFieldConstants.ID_TrombolyseIntraveneusDatum,
                FlowFieldConstants.ID_TrombolyseIntraveneusTijd,
                FlowFieldConstants.ID_TrombolyseArterieelDatum,
                FlowFieldConstants.ID_TrombolyseArterieelTijd,
                FlowFieldConstants.ID_RedenTrombolyseVerlaatUitgevoerd,
                FlowFieldConstants.ID_FollowUpDatum,
                FlowFieldConstants.ID_ModifiedRankingScale,
                FlowFieldConstants.ID_FollowUpNietGedaan,
                FlowFieldConstants.ID_MedischeSituatieNIHSSBijOpname,
                FlowFieldConstants.ID_VerwezenUitAnderZiekenhuis,
                FlowFieldConstants.ID_DiagnoseEersteKlachtDatumBekend,
                FlowFieldConstants.ID_DiagnoseEersteKlachtDatum,
                FlowFieldConstants.ID_DiagnoseEersteKlachtTijd,
                FlowFieldConstants.ID_BeloopOpname,
                FlowFieldConstants.ID_PatientOntslagenDatum
            };

            return uow.FlowFieldValueRepository.Get(ffv => ffv.FlowInstanceID == flowinstanceid && !ffv.FormSetVersion.Deleted && dicaflowfieldids.Contains(ffv.FlowFieldID))
                .OrderByDescending(ffv => ffv.FlowFieldValueID);
        }

        public static IEnumerable<FlowFieldValue> GetByFormSetVersionID(IUnitOfWork uow, int formSetVersionID)
        {
            return uow.FlowFieldValueRepository.Get(ffv => ffv.FormSetVersionID == formSetVersionID);
        }

        public static IEnumerable<FlowFieldValue> GetByFlowInstanceIDAndFormTypeID(IUnitOfWork uow, int flowInstanceID, int formTypeID)
        {
            return uow.FlowFieldValueRepository.Get(ffv => ffv.FlowInstanceID == flowInstanceID && ffv.FormSetVersion.FormTypeID == formTypeID && !ffv.FormSetVersion.Deleted);
        }

        public static IEnumerable<FlowFieldValue> GetByTransferIDAndFormTypeID(IUnitOfWork uow, int transferID, int formTypeID)
        {
            return uow.FlowFieldValueRepository.Get(ffv => ffv.FormSetVersion.TransferID == transferID && ffv.FormSetVersion.FormTypeID == formTypeID && !ffv.FormSetVersion.Deleted);
        }

        public static IEnumerable<FlowFieldValue> GetByFlowInstanceIDAndFormSetVersionID(IUnitOfWork uow, int flowInstanceID, int formSetVersionID)
        {
            return uow.FlowFieldValueRepository.Get(ffv => ffv.FlowInstanceID == flowInstanceID && ffv.FormSetVersionID == formSetVersionID);
        }

        public static IQueryable<FlowFieldValue> GetByFlowInstanceIDWithFlowField(IUnitOfWork uow, int flowInstanceID)
        {
            return uow.FlowFieldValueRepository.Get(ffv => ffv.FlowInstanceID == flowInstanceID && !(bool)ffv.FormSetVersion.PhaseInstance.Cancelled, includeProperties: "FlowField");
        }

        public static IQueryable<FlowFieldValue> GetByFlowInstanceID(IUnitOfWork uow, int flowInstanceID)
        {
            return uow.FlowFieldValueRepository.Get(ffv => ffv.FlowInstanceID == flowInstanceID && !(bool)ffv.FormSetVersion.PhaseInstance.Cancelled);
        }

        public static IQueryable<FlowFieldValue> GetByTransferID(IUnitOfWork uow, int transferID)
        {
            return uow.FlowFieldValueRepository.Get(ffv => ffv.FlowInstance.TransferID == transferID && ffv.FormSetVersion.PhaseInstance.Cancelled == false)
                .OrderByDescending(ffv => ffv.FormSetVersionID);
        }

        public static IEnumerable<FlowFieldValue> GetByFlowFieldIDAndFormSetVersionID(IUnitOfWork uow, int flowFieldID, int formSetVersionID)
        {
            return uow.FlowFieldValueRepository.Get(ffv => ffv.FlowFieldID == flowFieldID && ffv.FormSetVersionID == formSetVersionID);
        }

        public static string GetValueByFlowFieldIDAndFormSetVersionID(IUnitOfWork uow, int flowFieldID, int formSetVersionID)
        {
            var flowfieldvalues = GetByFlowFieldIDAndFormSetVersionID(uow, flowFieldID, formSetVersionID);
            string value = "";
            if (flowfieldvalues != null && flowfieldvalues.Any())
            {
                value = flowfieldvalues.OrderByDescending(ffv => ffv.FlowFieldValueID).FirstOrDefault().Value;
            }
            return value;
        }

        public static IEnumerable<FlowFieldValue> GetByTransferIDAndFieldID(IUnitOfWork uow, int transferID, int fieldID)
        {
            return uow.FlowFieldValueRepository.Get(ffv => ffv.FormSetVersion.TransferID == transferID && ffv.FlowFieldID == fieldID && ffv.FormSetVersion.PhaseInstance.Cancelled == false);
        }

        public static IEnumerable<FlowFieldValue> GetByTransferIDAndFieldIDs(IUnitOfWork uow, int transferID, int[] fieldIDs)
        {
            return uow.FlowFieldValueRepository.Get(ffv => ffv.FormSetVersion.TransferID == transferID && fieldIDs.Contains(ffv.FlowFieldID) && ffv.FormSetVersion.PhaseInstance.Cancelled == false);
        }

        public static IEnumerable<FlowFieldValue> GetByFlowInstanceIDAndFieldID(IUnitOfWork uow, int flowInstanceID, int fieldID)
        {
            return uow.FlowFieldValueRepository.Get(ffv => ffv.FlowInstanceID == flowInstanceID && ffv.FlowFieldID == fieldID && ffv.FormSetVersion.PhaseInstance.Cancelled == false);
        }

        public static IEnumerable<FlowFieldValue> GetByFlowInstanceIDAndFieldIDs(IUnitOfWork uow, int flowInstanceID, int[] fieldIDs)
        {
            return uow.FlowFieldValueRepository.Get(ffv => ffv.FlowInstanceID == flowInstanceID && fieldIDs.Contains(ffv.FlowFieldID) && ffv.FormSetVersion.PhaseInstance.Cancelled == false);
        }

        public static FlowFieldValue GetByTransferIDAndFlowFieldID(IUnitOfWork uow, int transferID, int flowFieldID)
        {
            return uow.FlowFieldValueRepository.Get(ffv => ffv.FlowInstance.TransferID == transferID && ffv.FlowFieldID == flowFieldID && ffv.FormSetVersion.PhaseInstance.Cancelled == false)
                      .OrderByDescending(ffv => ffv.FormSetVersionID).FirstOrDefault();
        }

        public static FlowFieldValue GetByTransferIDAndFormTypeIDAndFieldID(IUnitOfWork uow, int transferID, int formTypeID, int fieldID)
        {
            return uow.FlowFieldValueRepository.Get(ffv => ffv.FormSetVersion.TransferID == transferID && ffv.FormSetVersion.FormTypeID == formTypeID && !ffv.FormSetVersion.Deleted && !ffv.FormSetVersion.PhaseInstance.Cancelled && ffv.FlowFieldID == fieldID).FirstOrDefault();
        }

        public static IEnumerable<FlowFieldValue> GetByTransferIDAndFormTypeIDAndFieldIDs(IUnitOfWork uow, int transferID, int formTypeID, int[] fieldIDs)
        {
            return uow.FlowFieldValueRepository.Get(ffv => ffv.FormSetVersion.TransferID == transferID && ffv.FormSetVersion.FormTypeID == formTypeID && !ffv.FormSetVersion.Deleted && !ffv.FormSetVersion.PhaseInstance.Cancelled && fieldIDs.Contains(ffv.FlowFieldID));
        }

        public static IEnumerable<FlowFieldValue> GetByFlowInstanceIDAndFormTypeIDAndFieldIDs(IUnitOfWork uow, int flowInstanceID, int formTypeID, int[] fieldIDs)
        {
            return uow.FlowFieldValueRepository.Get(ffv => ffv.FlowInstanceID == flowInstanceID && ffv.FormSetVersion.FormTypeID == formTypeID && !ffv.FormSetVersion.Deleted && !ffv.FormSetVersion.PhaseInstance.Cancelled && fieldIDs.Contains(ffv.FlowFieldID));
        }

        public static FlowFieldValue GetByFlowInstanceIDAndFormSetVersionIDAndFlowFieldID(IUnitOfWork uow, int flowInstanceID, int formSetVersionID, int flowFieldID)
        {
            return uow.FlowFieldValueRepository.Get(ffv => ffv.FlowInstanceID == flowInstanceID && ffv.FlowFieldID == flowFieldID && ffv.FormSetVersionID == formSetVersionID).OrderByDescending(ffv => ffv.FormSetVersionID).FirstOrDefault();
        }

        public static FlowFieldValue GetByTransferIDAndFormSetVersionIDAndFlowFieldID(IUnitOfWork uow, int transferID, int formSetVersionID, int flowFieldID)
        {
            return uow.FlowFieldValueRepository.Get(ffv => ffv.FlowInstance.TransferID == transferID && ffv.FlowFieldID == flowFieldID && ffv.FormSetVersionID == formSetVersionID).OrderByDescending(ffv => ffv.FormSetVersionID).FirstOrDefault();
        }

        public static IEnumerable<FlowFieldValue> GetByFormsetVersionIDAndFlowFieldIDs(IUnitOfWork uow, int formSetVersionID, int[] flowFieldIDs)
        {
            return uow.FlowFieldValueRepository.Get(ffv => ffv.FormSetVersionID == formSetVersionID && flowFieldIDs.Contains(ffv.FlowFieldID));
        }

        public static FlowFieldValue GetByFormsetVersionIDAndFlowFieldID(IUnitOfWork uow, int formSetVersionID, int flowFieldID)
        {
            return uow.FlowFieldValueRepository.Get(ffv => ffv.FormSetVersionID == formSetVersionID && ffv.FlowFieldID == flowFieldID).FirstOrDefault();
        }

        public static void SetValue(FlowFieldValue flowfieldvalue, string value)
        {
            SetValue(flowfieldvalue, value, Source.USER);
        }

        public static void SetValue(FlowFieldValue flowfieldvalue, string value, Source source)
        {
            if (flowfieldvalue != null)
            {
                flowfieldvalue.Value = value;
            }
            flowfieldvalue.Source = source;
        }

        public static FlowFieldValue GetSourceFieldValue(List<FlowFieldValue> flowFieldValues, int flowfieldid)
        {
            return flowFieldValues.FirstOrDefault(afv => afv.FlowFieldID == flowfieldid);
        }

        public static MutFlowFieldValue AddMutation(IUnitOfWork uow, FlowFieldValue flowFieldValue, LogEntry logEntry)
        {
            MutFlowFieldValue mutFlowFieldValue = new MutFlowFieldValue();
            mutFlowFieldValue.Merge(flowFieldValue); //No FillLogging on this one, since we want to know de details of the original record
            mutFlowFieldValue.FlowFieldValue = flowFieldValue;

            LoggingBL.FillLogging(uow, mutFlowFieldValue, logEntry);

            uow.MutFlowFieldValueRepository.Insert(mutFlowFieldValue);

            return mutFlowFieldValue;
        }

        public static FlowFieldValue UpdateOrInsert(IUnitOfWork uow, int flowInstanceID, int formSetVersionID, int flowFieldID, string value, LogEntry logEntry)
        {
            return UpdateOrInsert(uow, flowInstanceID, formSetVersionID, flowFieldID, value, Source.USER, logEntry);
        }

        public static FlowFieldValue UpdateOrInsert(IUnitOfWork uow, int flowInstanceID, int formSetVersionID, int flowFieldID, string value, 
            Source source, LogEntry logEntry)
        {
            var flowFieldValue = GetByFlowInstanceIDAndFormSetVersionIDAndFlowFieldID(uow, flowInstanceID, formSetVersionID, flowFieldID);
            if (flowFieldValue == null || flowFieldValue == default(FlowFieldValue))
            {
                flowFieldValue = new FlowFieldValue()
                {
                    FlowInstanceID = flowInstanceID,
                    FlowFieldID = flowFieldID,
                    FormSetVersionID = formSetVersionID,
                    Value = value,
                    Source = source
                };

                uow.FlowFieldValueRepository.Insert(flowFieldValue);

                LoggingBL.FillLogging(uow, flowFieldValue, logEntry);

                AddMutation(uow, flowFieldValue, logEntry);
            }
            else if (flowFieldValue.Value != value)
            {
                flowFieldValue.Value = value;
                flowFieldValue.Source = source;

                // forceupdate is true because AutoDetectChangesEnabled could
                // be false causing FillLogging to not see changes - PP
                LoggingBL.FillLogging(uow, flowFieldValue, logEntry, forceupdate: true);

                AddMutation(uow, flowFieldValue, logEntry);
            }

            return flowFieldValue;
        }

        public static IEnumerable<FlowFieldValue> CopyTo(IUnitOfWork uow, FormSetVersion source, FormSetVersion destination)
        {
            if (source == null)
            {
                return new List<FlowFieldValue>();
            }

            var sourceFlowFieldValues = uow.FlowFieldValueRepository.Get(ffv => ffv.FlowInstance.TransferID == source.TransferID && ffv.FormSetVersionID == source.FormSetVersionID, asNoTracking: true);

            var flowInstance = FlowInstanceBL.GetByTransferID(uow, destination.TransferID);

            var destinationFlowFieldValues = new List<FlowFieldValue>();
            foreach (var flowFieldValue in sourceFlowFieldValues)
            {
                flowFieldValue.FlowFieldValueID = 0;
                flowFieldValue.FlowInstanceID = flowInstance.FlowInstanceID;
                flowFieldValue.FormSetVersionID = destination.FormSetVersionID;
                
                uow.FlowFieldValueRepository.Insert(flowFieldValue);
                destinationFlowFieldValues.Add(flowFieldValue);
            }

            if (destinationFlowFieldValues.Any())
            {
                uow.Save();
            }

            return destinationFlowFieldValues;
        }
    }
}
