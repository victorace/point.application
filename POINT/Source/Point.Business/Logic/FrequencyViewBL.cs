﻿using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using System.Linq;

namespace Point.Business.Logic
{
    public class FrequencyViewBL
    {
        public static FrequencyViewModel FromType(IUnitOfWork uow, int transferid, FrequencyType frequencytype)
        {
            var vm = new FrequencyViewModel();

            var frequency = FrequencyBL.GetByType(uow, transferid, frequencytype);
            if (frequency != null)
            {
                vm = new FrequencyViewModel()
                {
                    FrequencyID = frequency.FrequencyID,
                    TransferID = frequency.TransferID,
                    Name = frequency.Name,
                    StartDate = frequency.StartDate,
                    EndDate = frequency.EndDate,
                    Quantity = frequency.Quantity,
                    Status = frequency.Status,
                    Type = frequency.Type,
                    FeedbackType = frequency.FeedbackType
                };
            }
            else
            {
                vm.TransferID = transferid;
                vm.Type = frequencytype;
                vm.Status = FrequencyStatus.Active;
            }

            return vm;
        }

        public static Frequency ToEntity(IUnitOfWork uow, FrequencyViewModel vm)
        {
            var frequency = FrequencyBL.GetByID(uow, vm.FrequencyID);
            if(frequency == null)
            {
                frequency = new Frequency();
                uow.FrequencyRepository.Insert(frequency);
            }

            frequency.FrequencyID = vm.FrequencyID;
            frequency.TransferID = vm.TransferID;
            frequency.Name = vm.Name;
            frequency.StartDate = vm.StartDate.Value;
            frequency.EndDate = vm.EndDate.Value;
            frequency.Quantity = vm.Quantity;
            frequency.Status = vm.Status;
            frequency.Type = vm.Type;
            frequency.FeedbackType = vm.FeedbackType;

            return frequency;
        }
    }
}
