﻿using System.Collections.Generic;
using System.Linq;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using System;
using Point.Database.Extensions;
using Point.Models.Enums;
using System.Threading.Tasks;
using System.Text;
using System.IO;
using Ionic.Zip;
using Point.Business.Helper;
namespace Point.Business.Logic
{
    public class LogReadCSVFileBL
    {
        private class LogReadCSVFields
        {
        public string EmployeeName { get; set; } = "";
        public string OrganizationType { get; set; } = "";
        public string EmployeeOLA { get; set; } = "";
        public string ScreenTitle { get; set; } = "";
        public string Date { get; set; } = "";
        public string Time { get; set; } = "";
        public string TransferID { get; set; } = "";
        public List<ScreenType> ScreenTypes { get; set; }
        public string PatienName { get; set; } = "";
        public string BSN { get; set; } = "";
        public string Sender { get; set; } = "";
        public string Receiver { get; set; } = "";
        public string ScreenTypePerson { get; set; } = "";
        public string ScreenTypeSystemObject { get; set; } = "";
        public string ScreenTypeOrganisation { get; set; } = "";
        public string ScreenTypeMedicalData { get; set; } = "";
         public string ScreenTypeOthers { get; set; } = "";
        }
        private const string separator = ";";

        private static List<string> csvHeaders = new List<string> { "Medewerker", "Medewerkersorganisatie", "Organisatietype", "Scherm", "Datum", "Tijd", "Transferdossiernummer", "Naam v.d patient", "BSN", "Verzender", "Ontvanger", "Formtype persoon", "Formtype systeemobject", "Formtype organisatie", "Formtype medische gegevens", "Formtype overig" };

        private static LogReadCSVFields FromModel(LogRead logread, IUnitOfWork uow, IEnumerable<Employee> employees, IEnumerable<Screen> screens, int adminRegionID, FunctionRoleLevelID maxRoleLevel )
        {
            var flowinstance = TransferBL.GetByTransferID(uow, logread.TransferID ?? 0)?.FlowInstances?.FirstOrDefault();

            switch (maxRoleLevel)
            {
                case FunctionRoleLevelID.Global:
                    break;
                //as a region admin you may see those logreads that sender organization is in your region, so:
                case FunctionRoleLevelID.Region:
                    if (flowinstance?.StartedByOrganization?.RegionID != adminRegionID)
                    {
                        return null;
                    }
                    break;                   
                default:
                    return null;                    
            }           
            //Deze is erg duur als het kan vervallen            
            var ontvanger = OrganizationHelper.GetRecievingDepartment(uow, flowinstance?.FlowInstanceID ?? -1);
            var client = ClientBL.GetByTransferID(uow, logread.TransferID ?? 0);
            var screenTypes = getScreenTypes(logread, screens.FirstOrDefault(sc => sc.ScreenID == logread.ScreenID)).ToList();
            var employee = employees.FirstOrDefault(emp => emp.EmployeeID == logread.EmployeeID);
            
            var viewmodel = new LogReadCSVFields()
            {
                TransferID = logread.TransferID.HasValue ? logread.TransferID.ToString() : "",
                OrganizationType = getOrganizationType(employee) ?? "",
                EmployeeName = getEmployeeName(employee) ?? "",
                EmployeeOLA = employee?.DefaultDepartment?.FullName() ??"",
                ScreenTitle = getScreenTitle(logread, screens.FirstOrDefault(sc => sc.ScreenID == logread.ScreenID)) ?? "",               
                Date = logread.TimeStamp?.ToShortDateString() ?? "",
                Time = logread.TimeStamp?.ToShortTimeString () ?? "",
                Sender = flowinstance?.StartedByDepartment?.FullName() ?? "",
                Receiver = ontvanger?.FullName() ?? "",                
                PatienName = client?.FullName() ?? "",
                BSN = client?.CivilServiceNumber ?? "" ,
                ScreenTypePerson = screenTypes?.Contains(ScreenType.Persoon) ?? false ? "1" : "0",
                ScreenTypeSystemObject = screenTypes?.Contains(ScreenType.SysteemObject) ?? false ? "1" : "0",
                ScreenTypeOrganisation = screenTypes?.Contains(ScreenType.Organisatie) ?? false ? "1" : "0",
                ScreenTypeMedicalData = screenTypes?.Contains(ScreenType.MedischeGegevens) ?? false ? "1" : "0",
                ScreenTypeOthers = screenTypes?.Contains(ScreenType.Overig) ?? false ? "1" : "0"
            };

            return viewmodel;
        }


        private static string getOrganizationType(Employee employee) =>
            employee?.DefaultDepartment?.Location?.Organization?.OrganizationType?.Name;

        private static string getScreenTitle(LogRead logread, Screen screen) 
        {
            string screentitle = "";
            if (screen != null)
            {
                if (!string.IsNullOrEmpty(screen.ScreenTitle))
                {
                    screentitle = screen.ScreenTitle;
                }
                else if (screen.FormTypeID != null)
                {
                    screentitle = screen.FormType?.Name;
                }
                else if (screen.GeneralActionID != null)
                {
                    screentitle = screen.GeneralAction?.Name;
                }

            }

            if (String.IsNullOrEmpty(screentitle))
            {
                string[] components = logread.Url.Split(new[] { '/', }, StringSplitOptions.RemoveEmptyEntries);

                string controller = "";
                string action = "";
                if (components.Length >= 2)
                {
                    controller = components[components.Length - 2];
                    action = components[components.Length - 1];
                }
                else if (components.Length >= 1)
                {
                    controller = components[components.Length - 1];
                }


                if (logread.Url.IndexOf("Transfer/Search", StringComparison.InvariantCultureIgnoreCase) >= 0)
                {
                    screentitle = "Zoekscherm";
                }
                else if (logread.Url.IndexOf("/Dashboard", StringComparison.InvariantCultureIgnoreCase) >= 0 || logread.Url.IndexOf("/OverviewFlow", StringComparison.InvariantCultureIgnoreCase) >= 0)
                {
                    screentitle = "Dashboard";
                }
                else if (logread.Url.IndexOf("/LogRead", StringComparison.InvariantCultureIgnoreCase) >= 0)
                {
                    screentitle = "Bekijk leesacties";
                }
                else if (logread.Url.IndexOf("/Management", StringComparison.InvariantCultureIgnoreCase) >= 0)
                {
                    screentitle = String.Format("Beheerpagina ({0}/{1})", controller, action);
                }
                else if (logread.Url.IndexOf("CommunicationLog", StringComparison.InvariantCultureIgnoreCase) >= 0)
                {
                    screentitle = "Communicatielog";
                }
                else if (logread.Url.IndexOf("FlowMain", StringComparison.InvariantCultureIgnoreCase) >= 0)
                {
                    screentitle = "Flowmain";
                }
                else if (logread.Url.IndexOf("History", StringComparison.InvariantCultureIgnoreCase) >= 0)
                {
                    screentitle = "History";
                }
                else
                {
                    screentitle = String.Format("({0}/{1})", controller, action);
                }
            }

            return screentitle;
        }


        private static IEnumerable<ScreenType> getScreenTypes(LogRead logread, Screen screen)
        {
            List<ScreenType> screentypeslist = new List<ScreenType>();

            if (screen != null)
            {
                var screentypes = screen.ScreenTypes;
                foreach (string st in screentypes.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    ScreenType screentype;
                    if (Enum.TryParse<ScreenType>(st, out screentype))
                    {
                        screentypeslist.Add(screentype);
                    }
                }
            }

            return screentypeslist;
        }

        //private static IEnumerable<ScreenType> getScreenTypes(LogRead logread)
        //{
        //    List<ScreenType> screentypeslist = new List<ScreenType>();
        //    var screen = logread.Screen;

        //    if (screen != null)
        //    {
        //        var screentypes = screen.ScreenTypes;
        //        foreach (string st in screentypes.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
        //        {
        //            if (Enum.TryParse<ScreenType>(st, out ScreenType screentype))
        //            {
        //                screentypeslist.Add(screentype);
        //            }
        //        }
        //    }

        //    return screentypeslist;
        //}

        private static string getEmployeeName(Employee employee)
        {
            if (employee == null)
            {
                return "";
            }
            else
            {
                return employee.FullName();
            }
        }

        public static async Task<TransferAttachment> CreateExtendedReport(PointUserInfo userInfo,  LogReadExtendedViewModel searchviewmodel )
        {
            
            TransferAttachment transferAttachment = null;
            try
            {
                var clientIds = new List<int>();
                int transferId = 0;
                int employeeId = 0;
                LogReadCSVFields ff = new LogReadCSVFields();

                var maxlevel = userInfo.GetFunctionRoleLevelID(FunctionRoleTypeID.ManageLogs);
                var adminRegionID = userInfo.Region.RegionID;

                using (var uow = new UnitOfWork<Point.Database.Context.PointContext>())
                {
                    if (searchviewmodel.TransferID.HasValue)
                    {
                        transferId = searchviewmodel.TransferID.Value;
                    }
                    if (!string.IsNullOrEmpty(searchviewmodel.BSN?.Trim()))
                    {
                        clientIds = ClientBL.GetClientByBSN(uow, searchviewmodel.BSN.Trim())?.Select(cl => cl.ClientID)?.ToList();
                    }

                    if (searchviewmodel.EmployeeID.HasValue)
                    {
                        employeeId = EmployeeBL.GetByID(uow, searchviewmodel.EmployeeID ?? 0)?.EmployeeID ?? 0;
                    }

                    var logreads = LogReadBL.SearchLogRead(uow,
                        searchviewmodel.DateFrom,
                        searchviewmodel.DateTm,
                        transferId,
                        clientIds,
                        employeeId);

                    var encoding = Encoding.GetEncoding("Windows-1250");

                    string filename = $"LogReadCSV_{DateTime.Now.ToString("yyyyMMddHHmmss")}.csv";

                    using (var ms = new MemoryStream())
                    {
                        using (var sw = new StreamWriter(ms, encoding, 1024))
                        {
                            //create text file.
                            sw.AutoFlush = true;
                            sw.WriteLine("sep=" + separator);
                            sw.WriteLine(generateHeaderRow());
                            
                            var employees = EmployeeBL.GetByIDs(uow, logreads?.Where(lr => lr.EmployeeID != null)?.Select(lr => (int)lr.EmployeeID));
                            var screens = ScreenBL.GetScreens(uow);
                            // paging on logreads.

                            int tenThousandNr = (logreads?.Count() / 500) ?? 0;

                            for (int i = 0; i <= tenThousandNr; i++)
                            {
                                var logReadPagin = logreads.Skip(i * 500).Take(500);
                                foreach (var logRead in logReadPagin)
                                {                                  
                                    var csvObj = FromModel(logRead, uow,  employees, screens, adminRegionID, maxlevel);
                                    if (csvObj != null)
                                    {
                                        sw.WriteLine(createLogReadRow(csvObj));
                                    }                                    
                                }
                            }

                            //foreach (var logRead in logreads)
                            //{
                            //    //  var flw = TransferBL.GetByTransferID(uow, logRead.TransferID ?? 0)?.FlowInstances?.FirstOrDefault();
                            //    var csvObj = FromModel(logRead, uow, roles, employees, screens);
                            //    sw.WriteLine(createLogReadRow(csvObj));
                            //    // i++;
                            //}
                            ms.Position = 0;

                            //create zip file.
                            MemoryStream outputMS = new System.IO.MemoryStream();

                            ZipOutputStream zipOutput = new ZipOutputStream(outputMS);
                            zipOutput.PutNextEntry(filename);
                            zipOutput.Write(ms.ToArray(), 0, Convert.ToInt32(ms.Length));
                            zipOutput.Close();

                            //create byte array to save to db.
                            byte[] byteArrayOut = outputMS.ToArray();

                            transferAttachment = TransferAttachmentBL.CreateZipTransferAttachment(userInfo.Employee.EmployeeID , byteArrayOut, filename, AttachmentTypeID.LogRead);

                            outputMS.Close();
                        }
                    }
                }

            }
            finally
            {
                GC.Collect();
            }

            return await Task.FromResult(transferAttachment);
        }

        private static string generateHeaderRow()
        {
            var header = "";
            foreach (var hd in csvHeaders)
            {
                header += "\"" + hd + "\"" + separator;
            }
            return header;
        }

        private static string createLogReadRow(LogReadCSVFields csvObj)
        {
            var row = "";
            row += createCsvField(csvObj.EmployeeName);
            row += createCsvField(csvObj.EmployeeOLA);
            row += createCsvField(csvObj.OrganizationType);
            row += createCsvField(csvObj.ScreenTitle);
            row += createCsvField(csvObj.Date);
            row += createCsvField(csvObj.Time);
            row += createCsvField(csvObj.TransferID);
            row += createCsvField(csvObj.PatienName);
            row += createCsvField(csvObj.BSN);
            row += createCsvField(csvObj.Sender);
            row += createCsvField(csvObj.Receiver);
            row += createCsvField(csvObj.ScreenTypePerson);
            row += createCsvField(csvObj.ScreenTypeSystemObject);
            row += createCsvField(csvObj.ScreenTypeOrganisation);
            row += createCsvField(csvObj.ScreenTypeMedicalData);
            row += createCsvField(csvObj.ScreenTypeOthers, true);


            return row;
        }

        private static string createCsvField(string field, bool lastFieldInRow = false)
        {
            var csvField='"' + field.Replace("\"", "\"\"") + '"' + (lastFieldInRow ? "" : separator);
            return csvField;
        }

        public static string DownloadLogRead(IUnitOfWork uow, int employeeId, int attachmentId)
        {    
            //employee may see only his/her own log read files, so => 
            var ta = TransferAttachmentBL.GetByID(uow, attachmentId);

            //GetAsyncReport(uow, employeeId, attachmentId, AttachmentTypeID.LogRead);
            if (ta == null || ta?.GenericFile == null || ta.Deleted || ta.EmployeeID != employeeId)
            {
                throw new Exception($"Leesactie bestand niet gevonden.");
            }

            var pathtemp = System.Configuration.ConfigurationManager.AppSettings["PathTemp"];
            if (string.IsNullOrWhiteSpace(pathtemp) || !Directory.Exists(pathtemp))
            {
                throw new Exception($"PathTemp niet gevonden. ({pathtemp})");
            }

            var oldDumpReports = Directory.GetFiles(pathtemp, @"LogReadCSV_Active_*");
            foreach (var dump in oldDumpReports)
            {
                File.Delete(Path.Combine(pathtemp, dump));
            }

            string filePath = "";

            var inputMS = new MemoryStream(ta.GenericFile.FileData);

            var zipInput = new ZipInputStream(inputMS, true);

            zipInput.Position = 0;
            zipInput.GetNextEntry();

            filePath = Path.Combine(pathtemp, ta.GenericFile.FileName);

            using (var streamWriter = File.Create(filePath))
            {
                //stukje code van hier:https://www.codeproject.com/Questions/1116512/Unzipping-files-using-zipinputstream

                int size = 1024 * 10;
                Byte[] data = new Byte[size];
                while (true)
                {
                    size = zipInput.Read(data, 0, data.Length);
                    if (size > 0)
                    {
                        streamWriter.Write(data, 0, (int)size);
                    }
                    else break;
                }
            }
            return filePath;
            // send it to controller
        }
    }
}
                    
