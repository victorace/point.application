﻿using Point.Database.Models;
using Point.Database.Repository;
using System;

namespace Point.Business.Logic
{
    public class LogPointFormServiceBL
    {
        public static void Insert(IUnitOfWork uow, int organizationid, string hash, string url, string request)
        {
            var logpoinformserver = new LogPointFormService()
            {
                Timestamp = DateTime.Now,
                OrganizationID = organizationid,
                Hash = hash,
                URL = url,
                Request = request
            };
            uow.LogPointFormServiceRepository.Insert(logpoinformserver);
            uow.Save();
        }
    }
}
