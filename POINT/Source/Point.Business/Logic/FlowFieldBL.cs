﻿using System.Collections.Generic;
using System.Linq;
using Point.Database.Context;
using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Caching;

namespace Point.Business.Logic
{
    public class FlowFieldBL
    {
        private static IEnumerable<FlowField> getFlowFields(IUnitOfWork uow)
        {
            string cachestring = "GetFlowFields_Global";
            IList<FlowField> flowFields = CacheService.Instance.Get<IList<FlowField>>(cachestring);
            if (flowFields == null)
            {
                flowFields = uow.FlowFieldRepository.Get(includeProperties: "FlowFieldDataSource").ToList();

                CacheService.Instance.Insert(cachestring, flowFields, "CacheLong");
            }
            return flowFields;
        }

        public static FlowField GetByFlowFieldID(IUnitOfWork uow, int flowFieldID)
        {
            var flowField = (from f in getFlowFields(uow)
                             where f.FlowFieldID == flowFieldID
                             select f).FirstOrDefault();

            return flowField;
        }

        public static FlowField GetByName(IUnitOfWork uow, string name)
        {
            var flowField = (from f in getFlowFields(uow)
                             where f.Name == name
                             select f).FirstOrDefault();

            return flowField;
        }
    }
}
