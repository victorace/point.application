﻿using Point.Business.Helper;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Models.LDM.EOverdracht30;
using Point.Models.LDM.EOverdracht30.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Point.Business.Logic
{
    public class EOverdracht30ConvertBL
    {
        private IUnitOfWork UnitOfWork { get; set; }
        public EOverdracht30 EOverdracht30 { get; set; }
        private PointUserInfo PointUserInfo { get; set; }
        private int TransferID { get; set; }

        private Transfer Transfer { get; set; }

        public EOverdracht30ConvertBL(IUnitOfWork uow, PointUserInfo pointUserInfo, int transferID)
        {
            TransferID = transferID;
            Transfer = TransferBL.GetByTransferID(uow, transferID);
            UnitOfWork = uow;
            PointUserInfo = pointUserInfo;
        }

        private Patient PatientFromClient(Client client)
        {
            if (client == null)
            {
                return null;
            }

            var patient = new Patient
            {
                Naamgegevens = NaamgegevensFromClient(client)
            };

            if (!String.IsNullOrEmpty(client.PartnerName))
            {
                patient.Naamgegevens.GeslachtsnaamPartner = new GeslachtsnaamPartner()
                {
                    AchternaamPartner = client.PartnerName,
                    VoorvoegselsPartner = client.PartnerMiddleName
                };
            }
            if (!String.IsNullOrEmpty(client.StreetName))
            {
                patient.Adresgegevens.Add(new Adresgegevens()
                {
                    Straat = client.StreetName,
                    Huisnummer = client.Number,
                    Huisnummertoevoeging = client.HuisnummerToevoeging,
                    AdresSoort = AdresSoort.HP,
                    Postcode = client.PostalCode,
                    Woonplaats = client.City,
                    Land = client.Country
                });
            }
            if (!String.IsNullOrEmpty(client.TemporaryStreetName))
            {
                patient.Adresgegevens.Add(new Adresgegevens()
                {
                    Straat = client.TemporaryStreetName,
                    Huisnummer = client.TemporaryNumber,
                    AdresSoort = AdresSoort.TMP,
                    Postcode = client.TemporaryPostalCode,
                    Woonplaats = client.TemporaryCity,
                    Land = client.TemporaryCountry
                });
            }
            if (!String.IsNullOrEmpty(client.PhoneNumberGeneral) || !String.IsNullOrEmpty(client.Email))
            {
                patient.Contactgegevens = new Contactgegevens();

                if (!String.IsNullOrEmpty(client.PhoneNumberGeneral))
                {
                    patient.Contactgegevens.Telefoonnummers.Add(new Telefoonnummers()
                    {
                        TelecomType = TelecomType.LL,
                        Telefoonnummer = client.PhoneNumberGeneral
                    });
                }

                if (!String.IsNullOrEmpty(client.PhoneNumberMobile))
                {
                    patient.Contactgegevens.Telefoonnummers.Add(new Telefoonnummers()
                    {
                        TelecomType = TelecomType.LL,
                        Telefoonnummer = client.PhoneNumberMobile
                    });
                }

                if (!String.IsNullOrEmpty(client.PhoneNumberWork))
                {
                    patient.Contactgegevens.Telefoonnummers.Add(new Telefoonnummers()
                    {
                        TelecomType = TelecomType.LL,
                        Telefoonnummer = client.PhoneNumberWork
                    });
                }

                if (!String.IsNullOrEmpty(client.Email))
                {
                    patient.Contactgegevens.EmailAdressen.Add(new EmailAdressen()
                    {
                        EmailSoort = EmailSoort.HP,
                        EmailAdres = client.Email
                    });
                }
            }
            if (!String.IsNullOrEmpty(client.CivilServiceNumber))
            {
                patient.Identificatienummer.Add(new Identificatie()
                {
                    Identificatienummer = client.CivilServiceNumber,
                    IdentificatieSoort = IdentificatieSoort.BSN
                });
            }
            if (!String.IsNullOrEmpty(client.PatientNumber))
            {
                patient.Identificatienummer.Add(new Identificatie()
                {
                    Identificatienummer = client.PatientNumber,
                    IdentificatieSoort = IdentificatieSoort.PATIENTNR
                });
            }
            if (!String.IsNullOrEmpty(client.VisitNumber))
            {
                patient.Identificatienummer.Add(new Identificatie()
                {
                    Identificatienummer = client.VisitNumber,
                    IdentificatieSoort = IdentificatieSoort.OPNAMENR
                });
            }
            if (client.BirthDate.HasValue)
            {
                patient.Geboortedatum = client.BirthDate.Value;
            }

            patient.Geslacht = client.Gender;
            patient.BurgerlijkeStaat = client.CivilClass;

            return patient;
        }

        private Zorgaanbieder ZorgaanbiederFromDepartment(Department department)
        {
            if (department == null)
            {
                return null;
            }

            var zorgaanbieder = new Zorgaanbieder()
            {
                Adresgegevens = AdresgegevensFromDepartment(department),
                AfdelingSpecialisme = department.AfterCareType1?.Name,
                Contactgegevens = ContactgegevensFromDepartment(department),
                OrganisatieLocatie = department.FullName(),
                OrganisatieNaam = department.Location.FullName()
            };

            zorgaanbieder.ZorgaanbiederIdentificatienummer = new List<string>();

            if (!String.IsNullOrEmpty(department.AGB))
            {
                zorgaanbieder.ZorgaanbiederIdentificatienummer.Add(department.AGB);
            }
            if (!String.IsNullOrEmpty(department.Location?.Organization?.AGB))
            {
                zorgaanbieder.ZorgaanbiederIdentificatienummer.Add(department.Location.Organization.AGB);
            }

            return zorgaanbieder;
        }

        private Adresgegevens AdresgegevensFromDepartment(Department department)
        {
            var adresgegevens = new Adresgegevens();

            if (department?.Location == null)
            {
                return null;
            }

            adresgegevens.AdresSoort = AdresSoort.HP;
            adresgegevens.Huisnummer = department.Location.Number;
            adresgegevens.Straat = department.Location.StreetName;
            adresgegevens.Land = department.Location.Country;
            adresgegevens.Postcode = department.Location.PostalCode;
            adresgegevens.Woonplaats = department.Location.City;

            return adresgegevens;
        }

        private Zorgverlener ZorgverlenerFromEmployee(Employee employee)
        {
            if (employee == null)
            {
                return null;
            }

            var zorgverlener = new Zorgverlener()
            {
                ZorgverlenerIdentificatienummer = employee.BIG,
                Contactgegevens = ContactgegevensFromEmployee(employee),
                Naamgegevens = NaamgegevensFromEmployee(employee),
                Adresgegevens = AdresgegevensFromDepartment(employee.DefaultDepartment)
            };

            return zorgverlener;
        }

        private List<Contactgegevens> ContactgegevensFromEmployee(Employee employee)
        {
            if (employee == null)
            {
                return null;
            }

            var contactgegevens = new List<Contactgegevens>()
            {
                new Contactgegevens()
                {
                    EmailAdressen = String.IsNullOrEmpty(employee.EmailAddress) ? null : new List<EmailAdressen>() { new EmailAdressen() { EmailAdres = employee.EmailAddress, EmailSoort = EmailSoort.WP } },
                    Telefoonnummers = String.IsNullOrEmpty(employee.PhoneNumber) ? null : new List<Telefoonnummers>() { new Telefoonnummers() { NummerSoort = NummerSoort.WP, Telefoonnummer = employee.PhoneNumber } }
                }
            };

            return contactgegevens;
        }

        private Contactgegevens ContactgegevensFromDepartment(Department department)
        {
            if (department == null)
            {
                return null;
            }

            var contactgegevens = new Contactgegevens()
            {
                EmailAdressen = String.IsNullOrEmpty(department.EmailAddress) ? null : new List<EmailAdressen>() { new EmailAdressen() { EmailAdres = department.EmailAddress, EmailSoort = EmailSoort.WP } },
                Telefoonnummers = String.IsNullOrEmpty(department.PhoneNumber) ? null : new List<Telefoonnummers>() { new Telefoonnummers() { NummerSoort = NummerSoort.WP, Telefoonnummer = department.PhoneNumber } }
            };

            return contactgegevens;
        }

        private List<Naamgegevens> NaamgegevensFromEmployee(Employee employee)
        {
            if (employee == null)
            {
                return null;
            }

            var naamgegevens = new List<Naamgegevens>()
            {
                new Naamgegevens()
                {
                    Geslachtsnaam = new Geslachtsnaam() { Achternaam = employee.LastName, Voorvoegsels = employee.MiddleName },
                    Voornamen = employee.FirstName
                }
            };

            return naamgegevens;
        }

        private Naamgegevens NaamgegevensFromClient(Client client)
        {
            if (client == null)
            {
                return null;
            }

            var naamgegevens = new Naamgegevens()
            {
                Voornamen = client.FirstName,
                Initialen = client.Initials,
                Geslachtsnaam = new Geslachtsnaam() { Achternaam = client.LastName }
            };

            return naamgegevens;
        }

        private Contactpersoon ContactpersoonFromClient(Client client)
        {
            if (!client.ContactPerson.Any())
            {
                return null;
            }

            var contact = client.ContactPerson.FirstOrDefault();
            var contactpersoon = new Contactpersoon()
            {
                Contactgegevens = new Contactgegevens()
                {
                    EmailAdressen = String.IsNullOrEmpty(contact.PersonData.Email) ? null : new List<EmailAdressen>() { new EmailAdressen() { EmailAdres = contact.PersonData.Email } },
                    Telefoonnummers = String.IsNullOrEmpty(contact.PersonData.PhoneNumberGeneral) ? null : new List<Telefoonnummers>() { new Telefoonnummers() { Telefoonnummer = contact.PersonData.PhoneNumberGeneral } },
                },
                Naamgegevens = new Naamgegevens()
                {
                    Geslachtsnaam = new Geslachtsnaam() { Achternaam = contact.PersonData.LastName },
                    Initialen = contact.PersonData.Initials,
                    Voornamen = contact.PersonData.FirstName,
                }
                //W.I.P.: Relatie
            };
            return contactpersoon;
        }

        private void FillAdministratieveGegevens(FlowInstance flowInstance, IEnumerable<FlowWebField> flowWebFields, ViewDataDictionary viewData)
        {
            //W.I.P.: flowWebFields.GetFlowFieldDateTimeValue()...
            if (flowInstance.Transfer.Client == null)
            {
                return;
            }

            var dischargeProposedStartDate = FlowWebFieldBL.FromTransferIDAndName(UnitOfWork, flowInstance.TransferID, FlowFieldConstants.Name_DischargeProposedStartDate, PointUserInfo, viewData);
            if (dischargeProposedStartDate != null)
            {
                if (DateTime.TryParse(dischargeProposedStartDate.Value, out var datetime))
                {
                    EOverdracht30.Administratieve_Gegevens.DatumOverplaatsing = datetime;
                }
            }

            EOverdracht30.Administratieve_Gegevens.Patient = PatientFromClient(flowInstance.Transfer.Client);
            EOverdracht30.Administratieve_Gegevens.Contactpersoon = ContactpersoonFromClient(flowInstance.Transfer.Client);

            var healthinsurer = HealthInsurerBL.GetByID(UnitOfWork, flowInstance.Transfer.Client.HealthInsuranceCompanyID.GetValueOrDefault());
            if (healthinsurer != null)
            {
                EOverdracht30.Administratieve_Gegevens.Zorgverzekeraar = new Betaler()
                {
                    Verzekeraar = new List<Verzekeraar>() { new Verzekeraar()
                    {
                        IdentificatieNummer = healthinsurer.UZOVI.HasValue ? healthinsurer.UZOVI.ToString() : null ,
                        VerzekerdeNummer = flowInstance.Transfer.Client.InsuranceNumber,
                        OrganisatieNaam = healthinsurer.Name
                    }}
                };
            }

            var sendingdepartment = OrganizationHelper.GetSendingDepartment(UnitOfWork, flowInstance.FlowInstanceID);
            EOverdracht30.Administratieve_Gegevens.SturendeZorgaanbieder = ZorgaanbiederFromDepartment(sendingdepartment);

            var sendingemployee = OrganizationHelper.GetSendingEmployee(UnitOfWork, flowInstance.FlowInstanceID);
            EOverdracht30.Administratieve_Gegevens.SturendeZorgverlener = ZorgverlenerFromEmployee(sendingemployee);

            var recievingdepartment = OrganizationHelper.GetRecievingDepartment(UnitOfWork, flowInstance.FlowInstanceID);
            EOverdracht30.Administratieve_Gegevens.OntvangendeZorgaanbieder = ZorgaanbiederFromDepartment(recievingdepartment);

            var recievingemployee = OrganizationHelper.GetRecievingEmployee(UnitOfWork, flowInstance.FlowInstanceID);
            EOverdracht30.Administratieve_Gegevens.OntvangendeZorgverlener = ZorgverlenerFromEmployee(recievingemployee);
        }
    }
}
