﻿using System.Collections.Generic;
using System.Linq;
using Point.Database.CacheModels;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Caching;

namespace Point.Business.Logic
{
    public class FlowPhaseAttributeBL
    {
        private static IEnumerable<FlowPhaseAttribute> getFlowPhaseAttributes(IUnitOfWork uow, bool usecache = true)
        {
            string cachestring = "GetFlowPhaseAttributes_Global";
            IList<FlowPhaseAttribute> flowphaseattributes = null;

            if (usecache)
                flowphaseattributes = CacheService.Instance.Get<IList<FlowPhaseAttribute>>(cachestring);

            if (flowphaseattributes == null)
            {
                flowphaseattributes = uow.FlowPhaseAttributeRepository.GetAll().ToList();
                if (flowphaseattributes.Any())
                    CacheService.Instance.Insert(cachestring, flowphaseattributes, "CacheLong");
            }
            return flowphaseattributes;
        }

        public static string GetClassNameMenuItem(IUnitOfWork uow, PointUserInfo pointuserinfo, PhaseDefinitionCache phasedefinition)
        {
            var organizationtypeid = pointuserinfo.Organization.OrganizationTypeID;

            var flowphaseattributes = getFlowPhaseAttributes(uow).Where(fpa => fpa.PhaseDefinitionID == phasedefinition.PhaseDefinitionID);

            var classname = "";

            while(classname == "")
            {
                var role = pointuserinfo.Employee.PointRole.ToString();

                //Try to match from most detailed to least
                var flowphaseattribute = flowphaseattributes.FirstOrDefault(fpa => role == fpa.RoleID && fpa.OrganizationTypeID == organizationtypeid);
                if (flowphaseattribute != null)
                {
                    classname = flowphaseattribute.ClassNameMenuItem;
                    break;
                }

                flowphaseattribute = flowphaseattributes.FirstOrDefault(fpa => role == fpa.RoleID);
                if (flowphaseattribute != null)
                {
                    classname = flowphaseattribute.ClassNameMenuItem;
                    break;
                }

                flowphaseattribute = flowphaseattributes.FirstOrDefault(fpa => fpa.OrganizationTypeID == organizationtypeid);
                if (flowphaseattribute != null)
                {
                    classname = flowphaseattribute.ClassNameMenuItem;
                    break;
                }

                flowphaseattribute = flowphaseattributes.FirstOrDefault(fpa => fpa.RoleID == null && fpa.OrganizationTypeID == null);
                if (flowphaseattribute != null)
                {
                    classname = flowphaseattribute.ClassNameMenuItem;
                    break;
                }

                break;
            }

            return classname;
        }
    }
}
