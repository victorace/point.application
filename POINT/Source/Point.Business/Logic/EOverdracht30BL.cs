﻿using Point.Business.Helper;
using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure;
using Point.Infrastructure.Extensions;
using Point.Models.LDM.EOverdracht30.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Point.Business.Logic
{
    public class EOverdracht30BL
    {
        private IUnitOfWork UnitOfWork { get; set; }
        private PointUserInfo PointUserInfo { get; set; }
        private FlowInstance FlowInstance { get; set; }
        private Transfer Transfer { get; set; }

        public EOverdracht30BL(IUnitOfWork uow, PointUserInfo pointUserInfo, int flowInstanceID)
        {
            FlowInstance = FlowInstanceBL.GetByID(uow, flowInstanceID);
            Transfer = FlowInstance?.Transfer;
            UnitOfWork = uow;
            PointUserInfo = pointUserInfo;
        }

        #region display helpers
        //Example
        //private MvcHtmlString VersturendeZorgverlenerDisplayHelper(HtmlHelper htmlHelper, FormFieldGroup formFieldGroup)
        //{
        //    return htmlHelper.Partial("_Zorgverlener", versturendeMedewerker);
        //}
        #endregion

        #region Import helpers
        private OrganisatieType? OrganisatieTypeFromAfdeling(Department department)
        {
            if (department?.Location?.Organization?.OrganizationTypeID == (int)OrganizationTypeID.Hospital)
            {
                return OrganisatieType.V4;
            }
            else if (ontvangendeAfdeling?.Location?.Organization?.OrganizationTypeID == (int)OrganizationTypeID.GeneralPracticioner)
            {
                return OrganisatieType.Z3;
            }
            else if (ontvangendeAfdeling?.Location?.Organization?.OrganizationTypeID == (int)OrganizationTypeID.HealthCareProvider)
            {
                switch (ontvangendeAfdeling?.AfterCareType1?.AfterCareCategoryID)
                {
                    case 1: //Thuiszorg
                        return OrganisatieType.T2;
                    case 2: //Zorginstelling
                    case 3: //Hospice
                        return OrganisatieType.X3;
                }
            }

            return null;
        }

        private void PointPatientImportHelper(FormField formfield)
        {
            switch (formfield.FlowFieldName)
            {
                case FlowFieldConstants.Name_PatientVoornamen:
                    formfield.ImportValue = Transfer.Client?.FirstName;
                    break;
                case FlowFieldConstants.Name_PatientInitialen:
                    formfield.ImportValue = Transfer.Client?.Initials;
                    break;
                case FlowFieldConstants.Name_PatientVoorvoegsels:
                    formfield.ImportValue = Transfer.Client?.MiddleName;
                    break;
                case FlowFieldConstants.Name_PatientAchternaam:
                    formfield.ImportValue = Transfer.Client?.LastName;
                    break;
                case FlowFieldConstants.Name_PatientVoorvoegselsPartner:
                    formfield.ImportValue = Transfer.Client?.PartnerMiddleName;
                    break;
                case FlowFieldConstants.Name_PatientAchternaamPartner:
                    formfield.ImportValue = Transfer.Client?.PartnerName;
                    break;
                case FlowFieldConstants.Name_PatientStraat:
                    formfield.ImportValue = Transfer.Client?.StreetName;
                    break;
                case FlowFieldConstants.Name_PatientHuisnummer:
                    formfield.ImportValue = Transfer.Client?.Number;
                    break;
                case FlowFieldConstants.Name_PatientHuisnummertoevoeging:
                    formfield.ImportValue = Transfer.Client?.HuisnummerToevoeging;
                    break;
                case FlowFieldConstants.Name_PatientPostcode:
                    formfield.ImportValue = Transfer.Client?.PostalCode;
                    break;
                case FlowFieldConstants.Name_PatientWoonplaats:
                    formfield.ImportValue = Transfer.Client?.City;
                    break;
                case FlowFieldConstants.Name_PatientLand:
                    formfield.ImportValue = Transfer.Client?.Country;
                    break;
                case FlowFieldConstants.Name_PatientTijdelijkVerblijfStraat:
                    formfield.ImportValue = Transfer.Client?.TemporaryStreetName;
                    break;
                case FlowFieldConstants.Name_PatientTijdelijkVerblijfHuisnummer:
                    formfield.ImportValue = Transfer.Client?.TemporaryNumber;
                    break;
                case FlowFieldConstants.Name_PatientTijdelijkVerblijfHuisnummertoevoeging:
                    //Bestaat niet in client
                    break;
                case FlowFieldConstants.Name_PatientTijdelijkVerblijfPostcode:
                    formfield.ImportValue = Transfer.Client?.TemporaryPostalCode;
                    break;
                case FlowFieldConstants.Name_PatientTijdelijkVerblijfWoonplaats:
                    formfield.ImportValue = Transfer.Client?.TemporaryCity;
                    break;
                case FlowFieldConstants.Name_PatientTijdelijkVerblijfLand:
                    formfield.ImportValue = Transfer.Client?.TemporaryCountry;
                    break;
                case FlowFieldConstants.Name_PatientTelefoonnummer:
                    formfield.ImportValue = Transfer.Client?.PhoneNumberGeneral;
                    break;
                case FlowFieldConstants.Name_PatientEmailAdres:
                    formfield.ImportValue = Transfer.Client?.Email;
                    break;
                case FlowFieldConstants.Name_PatientIdentificatienummer:
                    formfield.ImportValue = Transfer.Client?.CivilServiceNumber;
                    break;
                case FlowFieldConstants.Name_PatientGeboortedatum:
                    formfield.ImportValue = Transfer.Client?.BirthDate.ToPointDateDisplay();
                    break;
                case FlowFieldConstants.Name_PatientGeslacht:
                    formfield.ImportValue = Transfer.Client?.Gender.ToString();
                    break;
                default:
                    break;
            }
        }
        private void PointBurgerlijkeStaatImportHandler(FormField formField)
        {
            var client = Transfer.Client;
            var oldvalue = client.CivilClass;
            if(oldvalue.HasValue)
            {
                formField.ImportValue = ((int)oldvalue).ToString();
            }
        }
        private void PointGezinssamenstellingImportHandler(FormField formField)
        {
            var client = Transfer.Client;
            var oldvalue = client.CompositionHousekeeping;
            if (oldvalue.HasValue)
            {
                formField.ImportValue = ((int)oldvalue).ToString();
            }
        }
        private void PointAantalKinderenImportHandler(FormField formField)
        {
            var client = Transfer.Client;
            if ((client?.ChildrenInHousekeeping).HasValue)
            {
                formField.ImportValue = client.ChildrenInHousekeeping.ToString();
            }
        }
        private void PointWoningTypeImportHandler(FormField formField)
        {
            var client = Transfer.Client;
            var oldvalue = client?.HousingType;

            if (oldvalue.HasValue)
            {
                formField.ImportValue = ((int)oldvalue).ToString();
            }
        }
        //Caching field versturendeAfdeling
        private Department versturendeAfdeling;
        private Employee versturendeMedewerker;
        private void PointSturendeImportHandler(FormField formField)
        {
            if(versturendeAfdeling == null)
            {
                versturendeAfdeling = OrganizationHelper.GetSendingDepartment(UnitOfWork, FlowInstance.FlowInstanceID);
                versturendeMedewerker = OrganizationHelper.GetSendingEmployee(UnitOfWork, FlowInstance.FlowInstanceID);
            }

            switch (formField.FlowFieldName)
            {
                case FlowFieldConstants.Name_VersturendeAfdeling:
                    var versturendededeparmentid = versturendeAfdeling?.DepartmentID;
                    if (versturendededeparmentid.HasValue)
                    {
                        formField.ImportValue = versturendededeparmentid.ToString();
                    }
                    break;
                case FlowFieldConstants.Name_VersturendeContactgegevensTelefoonnummer:
                    formField.ImportValue = versturendeAfdeling?.PhoneNumber;
                    break;
                case FlowFieldConstants.Name_VersturendeContactgegevensEmailAdres:
                    formField.ImportValue = versturendeAfdeling?.EmailAddress;
                    break;
                case FlowFieldConstants.Name_VersturendeAdresgegevensStraat:
                    formField.ImportValue = versturendeAfdeling?.Location?.StreetName;
                    break;
                case FlowFieldConstants.Name_VersturendeAdresgegevensHuisnummer:
                    formField.ImportValue = versturendeAfdeling?.Location?.Number;
                    break;
                case FlowFieldConstants.Name_VersturendeAdresgegevensPostcode:
                    formField.ImportValue = versturendeAfdeling?.Location?.PostalCode;
                    break;
                case FlowFieldConstants.Name_VersturendeAdresgegevensWoonplaats:
                    formField.ImportValue = versturendeAfdeling?.Location?.City;
                    break;
                case FlowFieldConstants.Name_VersturendeAdresgegevensLand:
                    formField.ImportValue = versturendeAfdeling?.Location?.Country;
                    break;
                case FlowFieldConstants.Name_VersturendeOrganisatieType:
                    var organisatietype = OrganisatieTypeFromAfdeling(versturendeAfdeling);
                    if (organisatietype.HasValue)
                    {
                        formField.ImportValue = organisatietype.ToString();
                    }
                    break;
                case FlowFieldConstants.Name_VersturendeZorgverlenerNaam:
                    formField.ImportValue = versturendeMedewerker?.EmployeeID.ConvertTo<string>();
                    break;
                case FlowFieldConstants.Name_VersturendeZorgverlenerTelefoonnummer:
                    formField.ImportValue = versturendeMedewerker?.PhoneNumber;
                    break;
                case FlowFieldConstants.Name_VersturendeZorgverlenerEmailAdres:
                    formField.ImportValue = versturendeMedewerker?.EmailAddress;
                    break;
            }
        }
        //Caching field ontvangendeAfdeling
        private Department ontvangendeAfdeling;
        private Employee ontvangendeMedewerker;
        private bool skipOntvangendeOphalen;
        private void PointOntvangendeImportHandler(FormField formField)
        {
            //Maar 1x ophalen en check voor null (skipOntvangende)
            if (ontvangendeAfdeling == null && skipOntvangendeOphalen == false)
            {
                ontvangendeAfdeling = OrganizationHelper.GetRecievingDepartment(UnitOfWork, FlowInstance.FlowInstanceID);
                ontvangendeMedewerker = OrganizationHelper.GetRecievingEmployee(UnitOfWork, FlowInstance.FlowInstanceID);
                skipOntvangendeOphalen = ontvangendeAfdeling == null;
            }

            switch (formField.FlowFieldName)
            {
                case FlowFieldConstants.Name_OntvangendeAfdeling:
                    var ontvangendedeparmentid = OrganizationHelper.GetRecievingDepartment(UnitOfWork, FlowInstance.FlowInstanceID)?.DepartmentID;
                    if (ontvangendedeparmentid.HasValue)
                    {
                        formField.ImportValue = ontvangendedeparmentid.ToString();
                    }
                    break;
                case FlowFieldConstants.Name_OntvangendeContactgegevensTelefoonnummer:
                    formField.ImportValue = ontvangendeAfdeling?.PhoneNumber;
                    break;
                case FlowFieldConstants.Name_OntvangendeContactgegevensEmailAdres:
                    formField.ImportValue = ontvangendeAfdeling?.EmailAddress;
                    break;
                case FlowFieldConstants.Name_OntvangendeAdresgegevensStraat:
                    formField.ImportValue = ontvangendeAfdeling?.Location?.StreetName;
                    break;
                case FlowFieldConstants.Name_OntvangendeAdresgegevensHuisnummer:
                    formField.ImportValue = ontvangendeAfdeling?.Location?.Number;
                    break;
                case FlowFieldConstants.Name_OntvangendeAdresgegevensPostcode:
                    formField.ImportValue = ontvangendeAfdeling?.Location?.PostalCode;
                    break;
                case FlowFieldConstants.Name_OntvangendeAdresgegevensWoonplaats:
                    formField.ImportValue = ontvangendeAfdeling?.Location?.City;
                    break;
                case FlowFieldConstants.Name_OntvangendeAdresgegevensLand:
                    formField.ImportValue = ontvangendeAfdeling?.Location?.Country;
                    break;
                case FlowFieldConstants.Name_OntvangendeOrganisatieType:
                    var organisatietype = OrganisatieTypeFromAfdeling(ontvangendeAfdeling);
                    if (organisatietype.HasValue)
                    {
                        formField.ImportValue = organisatietype.ToString();
                    }
                    break;
                case FlowFieldConstants.Name_OntvangendeZorgverlenerNaam:
                    formField.ImportValue = ontvangendeMedewerker?.EmployeeID.ConvertTo<string>(); ;
                    break;
                case FlowFieldConstants.Name_OntvangendeZorgverlenerTelefoonnummer:
                    formField.ImportValue = ontvangendeMedewerker?.PhoneNumber;
                    break;
                case FlowFieldConstants.Name_OntvangendeZorgverlenerEmailAdres:
                    formField.ImportValue = ontvangendeMedewerker?.EmailAddress;
                    break;
            }
        }
        #endregion

        //Sections
        private FormSection AdministratieveGegevens()
        {
            return new FormSection()
            {
                Name = "Administratieve gegevens",
                Visible = true,
                PrintOnly = true,
                ClassName = "gray",
                SubSections = new List<FormSubSection>() {
                    new FormSubSection()
                    {
                        Name = "Persoonsgegevens",
                        Visible = false,
                        Switchable = false,
                        FormFieldGroups = new List<FormFieldGroup>() {
                            new FormFieldGroup() {
                                FormFields = new List<FormField>() {
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_PatientVoornamen,
                                        ImportProperties = new List<ImportProperty>() {
                                            new ImportProperty(ImportSourceType.Point, PointPatientImportHelper, overwriteValue: true)
                                        }
                                    },
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_PatientInitialen,
                                        ImportProperties = new List<ImportProperty>() {
                                            new ImportProperty(ImportSourceType.Point, PointPatientImportHelper, overwriteValue: true)
                                        }
                                    },
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_PatientVoorvoegsels,
                                        ImportProperties = new List<ImportProperty>() {
                                            new ImportProperty(ImportSourceType.Point, PointPatientImportHelper, overwriteValue: true)
                                        }
                                    },
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_PatientAchternaam,
                                        ImportProperties = new List<ImportProperty>() {
                                            new ImportProperty(ImportSourceType.Point, PointPatientImportHelper, overwriteValue: true)
                                        }
                                    },
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_PatientVoorvoegselsPartner,
                                        ImportProperties = new List<ImportProperty>() {
                                            new ImportProperty(ImportSourceType.Point, PointPatientImportHelper, overwriteValue: true)
                                        }
                                    },
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_PatientAchternaamPartner,
                                        ImportProperties = new List<ImportProperty>() {
                                            new ImportProperty(ImportSourceType.Point, PointPatientImportHelper, overwriteValue: true)
                                        }
                                    },
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_PatientStraat,
                                        ImportProperties = new List<ImportProperty>() {
                                            new ImportProperty(ImportSourceType.Point, PointPatientImportHelper, overwriteValue: true)
                                        }
                                    },
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_PatientHuisnummer,
                                        ImportProperties = new List<ImportProperty>() {
                                            new ImportProperty(ImportSourceType.Point, PointPatientImportHelper, overwriteValue: true)
                                        }
                                    },
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_PatientHuisnummertoevoeging,
                                        ImportProperties = new List<ImportProperty>() {
                                            new ImportProperty(ImportSourceType.Point, PointPatientImportHelper, overwriteValue: true)
                                        }
                                    },
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_PatientPostcode,
                                        ImportProperties = new List<ImportProperty>() {
                                            new ImportProperty(ImportSourceType.Point, PointPatientImportHelper, overwriteValue: true)
                                        }
                                    },
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_PatientWoonplaats,
                                        ImportProperties = new List<ImportProperty>() {
                                            new ImportProperty(ImportSourceType.Point, PointPatientImportHelper, overwriteValue: true)
                                        }
                                    },
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_PatientLand,
                                        ImportProperties = new List<ImportProperty>() {
                                            new ImportProperty(ImportSourceType.Point, PointPatientImportHelper, overwriteValue: true)
                                        }
                                    },
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_PatientTijdelijkVerblijfStraat,
                                        ImportProperties = new List<ImportProperty>() {
                                            new ImportProperty(ImportSourceType.Point, PointPatientImportHelper, overwriteValue: true)
                                        }
                                    },
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_PatientTijdelijkVerblijfHuisnummer,
                                        ImportProperties = new List<ImportProperty>() {
                                            new ImportProperty(ImportSourceType.Point, PointPatientImportHelper, overwriteValue: true)
                                        }
                                    },
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_PatientTijdelijkVerblijfHuisnummertoevoeging,
                                        ImportProperties = new List<ImportProperty>() {
                                            new ImportProperty(ImportSourceType.Point, PointPatientImportHelper, overwriteValue: true)
                                        }
                                    },
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_PatientTijdelijkVerblijfPostcode,
                                        ImportProperties = new List<ImportProperty>() {
                                            new ImportProperty(ImportSourceType.Point, PointPatientImportHelper, overwriteValue: true)
                                        }
                                    },
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_PatientTijdelijkVerblijfWoonplaats,
                                        ImportProperties = new List<ImportProperty>() {
                                            new ImportProperty(ImportSourceType.Point, PointPatientImportHelper, overwriteValue: true)
                                        }
                                    },
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_PatientTijdelijkVerblijfLand,
                                        ImportProperties = new List<ImportProperty>() {
                                            new ImportProperty(ImportSourceType.Point, PointPatientImportHelper, overwriteValue: true)
                                        }
                                    },
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_PatientTelefoonnummer,
                                        ImportProperties = new List<ImportProperty>() {
                                            new ImportProperty(ImportSourceType.Point, PointPatientImportHelper, overwriteValue: true)
                                        }
                                    },
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_PatientEmailAdres,
                                        ImportProperties = new List<ImportProperty>() {
                                            new ImportProperty(ImportSourceType.Point, PointPatientImportHelper, overwriteValue: true)
                                        }
                                    },
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_PatientIdentificatienummer,
                                        ImportProperties = new List<ImportProperty>() {
                                            new ImportProperty(ImportSourceType.Point, PointPatientImportHelper, overwriteValue: true)
                                        }
                                    },
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_PatientGeboortedatum,
                                        ImportProperties = new List<ImportProperty>() {
                                            new ImportProperty(ImportSourceType.Point, PointPatientImportHelper, overwriteValue: true)
                                        }
                                    },
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_PatientGeslacht,
                                        ImportProperties = new List<ImportProperty>() {
                                            new ImportProperty(ImportSourceType.Point, PointPatientImportHelper, overwriteValue: true)
                                        }
                                    }
                                }
                            }
                        }
                    },
                    new FormSubSection()
                    {
                        Name = "Sturende organisatie",
                        Visible = true,
                        Switchable = false,
                        FormFieldGroups = new List<FormFieldGroup>() {
                            new FormFieldGroup() {
                                FormFields = new List<FormField>() {
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_VersturendeAfdeling,
                                        ImportProperties = new List<ImportProperty>() {
                                            new ImportProperty(ImportSourceType.Point, PointSturendeImportHandler, overwriteValue: true)
                                        }
                                    },
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_VersturendeContactgegevensTelefoonnummer,
                                        ImportProperties = new List<ImportProperty>() {
                                            new ImportProperty(ImportSourceType.Point, PointSturendeImportHandler, overwriteValue: true)
                                        }
                                    },
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_VersturendeContactgegevensEmailAdres,
                                        ImportProperties = new List<ImportProperty>() {
                                            new ImportProperty(ImportSourceType.Point, PointSturendeImportHandler, overwriteValue: true)
                                        }
                                    },
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_VersturendeAdresgegevensStraat,
                                        ImportProperties = new List<ImportProperty>() {
                                            new ImportProperty(ImportSourceType.Point, PointSturendeImportHandler, overwriteValue: true)
                                        }
                                    },
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_VersturendeAdresgegevensHuisnummer,
                                        ImportProperties = new List<ImportProperty>() {
                                            new ImportProperty(ImportSourceType.Point, PointSturendeImportHandler, overwriteValue: true)
                                        }
                                    },
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_VersturendeAdresgegevensPostcode,
                                        ImportProperties = new List<ImportProperty>() {
                                            new ImportProperty(ImportSourceType.Point, PointSturendeImportHandler, overwriteValue: true)
                                        }
                                    },
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_VersturendeAdresgegevensWoonplaats,
                                        ImportProperties = new List<ImportProperty>() {
                                            new ImportProperty(ImportSourceType.Point, PointSturendeImportHandler, overwriteValue: true)
                                        }
                                    },
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_VersturendeAdresgegevensLand,
                                        ImportProperties = new List<ImportProperty>() {
                                            new ImportProperty(ImportSourceType.Point, PointSturendeImportHandler, overwriteValue: true)
                                        }
                                    },
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_VersturendeOrganisatieType,
                                        ImportProperties = new List<ImportProperty>() {
                                            new ImportProperty(ImportSourceType.Point, PointSturendeImportHandler, overwriteValue: true)
                                        }
                                    }
                                }
                            },
                            new FormFieldGroup()
                            {
                                FormFields = new List<FormField>() {
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_VersturendeZorgverlenerNaam,
                                        ImportProperties = new List<ImportProperty>() {
                                            new ImportProperty(ImportSourceType.Point, PointSturendeImportHandler, overwriteValue: true)
                                        }
                                    },
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_VersturendeZorgverlenerTelefoonnummer,
                                        ImportProperties = new List<ImportProperty>() {
                                            new ImportProperty(ImportSourceType.Point, PointSturendeImportHandler, overwriteValue: true)
                                        }
                                    },
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_VersturendeZorgverlenerEmailAdres,
                                        ImportProperties = new List<ImportProperty>() {
                                            new ImportProperty(ImportSourceType.Point, PointSturendeImportHandler, overwriteValue: true)
                                        }
                                    }
                                }
                            }
                        }
                    },
                    new FormSubSection()
                    {
                        Name = "Ontvangende organisatie",
                        Visible = true,
                        Switchable = false,
                        FormFieldGroups = new List<FormFieldGroup>() {
                            new FormFieldGroup() {
                                FormFields = new List<FormField>() {
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_OntvangendeAfdeling,
                                        ImportProperties = new List<ImportProperty>() {
                                            new ImportProperty(ImportSourceType.Point, PointOntvangendeImportHandler, overwriteValue: true)
                                        }
                                    },
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_OntvangendeContactgegevensTelefoonnummer,
                                        ImportProperties = new List<ImportProperty>() {
                                            new ImportProperty(ImportSourceType.Point, PointOntvangendeImportHandler, overwriteValue: true)
                                        }
                                    },
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_OntvangendeContactgegevensEmailAdres,
                                        ImportProperties = new List<ImportProperty>() {
                                            new ImportProperty(ImportSourceType.Point, PointOntvangendeImportHandler, overwriteValue: true)
                                        }
                                    },
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_OntvangendeAdresgegevensStraat,
                                        ImportProperties = new List<ImportProperty>() {
                                            new ImportProperty(ImportSourceType.Point, PointOntvangendeImportHandler, overwriteValue: true)
                                        }
                                    },
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_OntvangendeAdresgegevensHuisnummer,
                                        ImportProperties = new List<ImportProperty>() {
                                            new ImportProperty(ImportSourceType.Point, PointOntvangendeImportHandler, overwriteValue: true)
                                        }
                                    },
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_OntvangendeAdresgegevensPostcode,
                                        ImportProperties = new List<ImportProperty>() {
                                            new ImportProperty(ImportSourceType.Point, PointOntvangendeImportHandler, overwriteValue: true)
                                        }
                                    },
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_OntvangendeAdresgegevensWoonplaats,
                                        ImportProperties = new List<ImportProperty>() {
                                            new ImportProperty(ImportSourceType.Point, PointOntvangendeImportHandler, overwriteValue: true)
                                        }
                                    },
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_OntvangendeAdresgegevensLand,
                                        ImportProperties = new List<ImportProperty>() {
                                            new ImportProperty(ImportSourceType.Point, PointOntvangendeImportHandler, overwriteValue: true)
                                        }
                                    },
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_OntvangendeOrganisatieType,
                                        ImportProperties = new List<ImportProperty>() {
                                            new ImportProperty(ImportSourceType.Point, PointOntvangendeImportHandler, overwriteValue: true)
                                        }
                                    }
                                }
                            },
                            new FormFieldGroup()
                            {
                                FormFields = new List<FormField>{
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_OntvangendeZorgverlenerNaam,
                                        ImportProperties = new List<ImportProperty>() {
                                            new ImportProperty(ImportSourceType.Point, PointOntvangendeImportHandler, overwriteValue: true)
                                        }
                                    },
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_OntvangendeZorgverlenerTelefoonnummer,
                                        ImportProperties = new List<ImportProperty>() {
                                            new ImportProperty(ImportSourceType.Point, PointOntvangendeImportHandler, overwriteValue: true)
                                        }
                                    },
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_OntvangendeZorgverlenerEmailAdres,
                                        ImportProperties = new List<ImportProperty>() {
                                            new ImportProperty(ImportSourceType.Point, PointOntvangendeImportHandler, overwriteValue: true)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            };
        }
        private FormSection AlgemenePatientenContext()
        {
            return new FormSection()
            {
                Name = "Algemene patiënten context",
                Visible = false,
                ClassName = "blue",
                SubSections = new List<FormSubSection>() {
                    new FormSubSection()
                    {
                        Name = "Gezinssituatie",
                        Visible = false,
                        Switchable = true,
                        FormFieldGroups = new List<FormFieldGroup>() {
                            new FormFieldGroup() {
                                FormFields = new List<FormField>() {
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_GezinsSituatieBurgerlijkeStaat,
                                        ImportProperties = new List<ImportProperty>() {
                                            new ImportProperty(ImportSourceType.Point, PointBurgerlijkeStaatImportHandler)
                                        }
                                    },
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_GezinsSituatieGezinssamenstelling,
                                        ImportProperties = new List<ImportProperty>() {
                                            new ImportProperty(ImportSourceType.Point, PointGezinssamenstellingImportHandler)
                                        }
                                    },
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_GezinsSituatieAantalKinderen,
                                        ImportProperties = new List<ImportProperty>() {
                                            new ImportProperty(ImportSourceType.Point, PointAantalKinderenImportHandler)
                                        }
                                    }
                                }
                            }
                        }
                    },
                    new FormSubSection()
                    {
                        Name = "Woonomgeving",
                        Visible = false,
                        Switchable = true,
                        FormFieldGroups = new List<FormFieldGroup>() {
                            new FormFieldGroup() {
                                FormFields = new List<FormField>() {
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_WoonomgevingWoningType,
                                        ImportProperties = new List<ImportProperty>() {
                                            new ImportProperty(ImportSourceType.Point, PointWoningTypeImportHandler)
                                        }
                                    },
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_WoonomgevingToelichting
                                    }
                                }
                            }
                        }
                    },
                    new FormSubSection()
                    {
                        Name = "Levensovertuiging",
                        Visible = false,
                        Switchable = true,
                        FormFieldGroups = new List<FormFieldGroup>() {
                            new FormFieldGroup() {
                                FormFields = new List<FormField>() {
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_LevensovertuigingLevensovertuiging
                                    }
                                }
                            }
                        }
                    },
                    new FormSubSection()
                    {
                        Name = "Participatie in maatschappij",
                        Visible = false,
                        Switchable = true,
                        FormFieldGroups = new List<FormFieldGroup>() {
                            new FormFieldGroup() {
                                FormFields = new List<FormField>() {
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_ParticipatieInMaatschappijSociaalNetwerk
                                    },
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_ParticipatieInMaatschappijVrijetijdsbesteding
                                    },
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_ParticipatieInMaatschappijArbeidssituatie
                                    },
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_ParticipatieInMaatschappijToelichting
                                    }
                                }
                            }
                        }
                    },
                    new FormSubSection()
                    {
                        Name = "Hulp van anderen",
                        Visible = false,
                        Switchable = true,
                        FormFieldGroups = new List<FormFieldGroup>() {
                            new FormFieldGroup() {
                                FormFields = new List<FormField>() {
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_HulpVanAnderenHulpverlenerZorgverlener
                                    },
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_HulpVanAnderenHulpverlenerMantelzorger
                                    },
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_HulpVanAnderenHulpverlenerZorgaanbieder
                                    },
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_HulpVanAnderenAard
                                    },
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_HulpVanAnderenFrequentie
                                    },
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_HulpVanAnderenSoortHulp
                                    },
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_HulpVanAnderenToelichting
                                    }
                                }
                            }
                        }
                    },
                    new FormSubSection()
                    {
                        Name = "Communicatie",
                        Visible = false,
                        Switchable = true,
                        FormFieldGroups = new List<FormFieldGroup>() {
                            new FormFieldGroup() {
                                FormFields = new List<FormField>() {
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_CommunicatieCommunicatieTaal
                                    },
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_CommunicatieTaalvaardigheidBegrijpen
                                    },
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_CommunicatieTaalvaardigheidSpreken
                                    },
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_CommunicatieTaalvaardigheidLezen
                                    },
                                    new FormField()
                                    {
                                        FlowFieldName = FlowFieldConstants.Name_CommunicatieToelichting
                                    }
                                }
                            }
                        }
                    }
                }
            };
        }
        private FormSection MedischeContext()
        {
            return new FormSection()
            {
                Name = "Medische context",
                Visible = false,
                ClassName = "orange",
                SubSections = new List<FormSubSection>() {
                    new FormSubSection()
                    {
                        Name = "Behandelaar",
                        Visible = false,
                        Switchable = true
                    },
                    new FormSubSection()
                    {
                        Name = "Medische diagnose",
                        Visible = false,
                        Switchable = true
                    },
                    new FormSubSection()
                    {
                        Name = "Voorgeschiedenis",
                        Visible = false,
                        Switchable = true
                    },
                    new FormSubSection()
                    {
                        Name = "Allergie",
                        Visible = false,
                        Switchable = true
                    },
                    new FormSubSection()
                    {
                        Name = "Let op",
                        Visible = false,
                        Switchable = true
                    },
                    new FormSubSection()
                    {
                        Name = "Vrijheidsbeperking",
                        Visible = false,
                        Switchable = true
                    },
                    new FormSubSection()
                    {
                        Name = "Meetwaarden",
                        Visible = false,
                        Switchable = true
                    },
                    new FormSubSection()
                    {
                        Name = "Pijnscore",
                        Visible = false,
                        Switchable = true
                    }
                }
            };
        }
        private FormSection VerpleegkundigeContextZorgplan()
        {
            return new FormSection()
            {
                Name = "Verpleegkundige context: zorgplan",
                Visible = false,
                ClassName = "green",
                SubSections = new List<FormSubSection>() {
                    new FormSubSection()
                    {
                        Name = "Actuele patiëntproblemen",
                        Visible = false,
                        Switchable = true
                    },
                    new FormSubSection()
                    {
                        Name = "Zorgresultaat",
                        Visible = false,
                        Switchable = true
                    },
                    new FormSubSection()
                    {
                        Name = "Afspraken patiënt",
                        Visible = false,
                        Switchable = true
                    },
                    new FormSubSection()
                    {
                        Name = "Wensen en behoeften patiënt en/of naasten",
                        Visible = false,
                        Switchable = true
                    }
                }
            };
        }
        private FormSection VerpleegkundigeContextSpecGezondheidsToestand()
        {
            return new FormSection()
            {
                Name = "Verpleegkundige context: specificatie gezondheidstoestand",
                Visible = false,
                ClassName = "purple",
                SubSections = new List<FormSubSection>() {
                    new FormSubSection()
                    {
                        Name = "Zelfzorg",
                        Visible = false,
                        Switchable = true
                    },
                    new FormSubSection()
                    {
                        Name = "Mobiliteit",
                        Visible = false,
                        Switchable = true
                    },
                    new FormSubSection()
                    {
                        Name = "Voeding/vocht",
                        Visible = false,
                        Switchable = true
                    },
                    new FormSubSection()
                    {
                        Name = "Uitscheiding",
                        Visible = false,
                        Switchable = true
                    },
                    new FormSubSection()
                    {
                        Name = "Huid",
                        Visible = false,
                        Switchable = true
                    },
                    new FormSubSection()
                    {
                        Name = "Zintuigen",
                        Visible = false,
                        Switchable = true
                    },
                    new FormSubSection()
                    {
                        Name = "Mentale status / ziektebeleving",
                        Visible = false,
                        Switchable = true
                    },
                    new FormSubSection()
                    {
                        Name = "Voortplanting",
                        Visible = false,
                        Switchable = true
                    }
                }
            };
        }

        public void StartImport(List<FlowWebField> flowWebFields, List<FormSection> sections, ImportSourceType importSourceType)
        {
            var formFields = sections.SelectMany(s => s.SubSections.SelectMany(ss => ss.FormFieldGroups.SelectMany(ffg => ffg.FormFields.Where(ff => ff.ImportProperties.Any(ip => ip.ImportSourceType == importSourceType)))));
            using (var lookupBL = new LookUpBL())
            {
                foreach (var formField in formFields)
                {
                    //Invoke handler if needed
                    var importproperty = formField.ImportProperties.FirstOrDefault(ip => ip.ImportSourceType == importSourceType);
                    if (importproperty?.ImportHandler != null)
                    {
                        var flowWebField = flowWebFields.FirstOrDefault(fwf => fwf.Name == formField.FlowFieldName && (String.IsNullOrEmpty(fwf.Value) || importproperty.OverwriteValue));
                        if (flowWebField != null)
                        {
                            importproperty?.ImportHandler.Invoke(formField);

                            // TODO: FlowWebFieldBL.SetValue? - PP
                            if (!String.IsNullOrEmpty(formField.ImportValue))
                            {
                                flowWebField.Source = Source.POINT_PREFILL;
                            }

                            if (flowWebField.Value != formField.ImportValue)
                            {
                                flowWebField.Value = formField.ImportValue;
                                flowWebField.DisplayValue = FlowFieldValueHelper.GetDisplayValue(flowWebField);
                            }
                        }
                    }
                }
            }
        }

        //Public functions
        public List<FormSection> GetSections(EOverdrachtType type, ref List<FlowWebField> flowWebFields)
        {
            List<FormSection> sections = null;
            switch (type)
            {
                case EOverdrachtType.Volwassenen:
                    sections = new List<FormSection>()
                    {
                        AdministratieveGegevens(),
                        AlgemenePatientenContext(),
                        MedischeContext(),
                        VerpleegkundigeContextZorgplan(),
                        VerpleegkundigeContextSpecGezondheidsToestand()
                    };
                    break;

                default:
                    return null;
            }

            //Allways import/prefill Point fields
            //Updates patientdata etc.
            StartImport(flowWebFields, sections, ImportSourceType.Point);

            //Get fields wich are (pre)filled
            var filledfieldnames = flowWebFields.Where(fwf => !String.IsNullOrEmpty(fwf.Value)).Select(fwf => fwf.Name);
            var filledsections = sections.SelectMany(s =>
                s.SubSections.SelectMany(ss =>
                    ss.FormFieldGroups.Select(ffg => new  {
                        Section = s,
                        SubSection = ss,
                        FilledFormFields = ffg.FormFields.Where(ff => filledfieldnames.Contains(ff.FlowFieldName)).ToList()
                    })
                )).Where(anon => anon.FilledFormFields.Any()).ToList();

            //Make sections visible wich are (pre)filled
            //Also mark them as filled/prefilled
            filledsections.ForEach(fs => {
                fs.Section.Visible = true;
                fs.SubSection.Visible = true;
                fs.FilledFormFields.ForEach(ff => {
                    ff.IsFilled = true;
                    ff.Source = !String.IsNullOrEmpty(ff.ImportValue) ? Source.POINT_PREFILL : Source.DEFAULT;
                });
            });

            return sections;
        }
    }
}
