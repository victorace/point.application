﻿using System.Collections.Generic;
using System.Linq;
using Point.Database.Context;
using Point.Database.Models;
using Point.Database.Repository;

namespace Point.Business.Logic
{
    public class FlowFieldAttributeExceptionBL
    {
        public static IEnumerable<FlowFieldAttributeException> GetByFormTypeIDPhaseDefinitionIDAndRegioIDAndOrganizationID(IUnitOfWork uow, int formtypeid, int phasedefinitionid, int regionid, int organizationid)
        {
            return uow.FlowFieldAttributeExceptionRepository.Get(it => it.FlowFieldAttribute.FormTypeID == formtypeid && (it.PhaseDefinitionID == null || it.PhaseDefinitionID == phasedefinitionid) && (it.RegionID == regionid || it.OrganizationID == organizationid || (it.OrganizationID == null && it.RegionID == null)));
        }

        public static IEnumerable<FlowFieldAttributeException> GetByRegioIDAndOrganizationID(IUnitOfWork uow, int regionid, int organizationid)
        {
            return uow.FlowFieldAttributeExceptionRepository.Get(it => (it.RegionID == regionid || it.OrganizationID == organizationid || (it.OrganizationID == null && it.RegionID == null)));
        }

        public static IQueryable<FlowFieldAttributeException> GetByOrganizationID(IUnitOfWork uow, int organizationId)
        {
            return uow.FlowFieldAttributeExceptionRepository.Get(ffae => ffae.OrganizationID == organizationId);
        }

        public static void UpdateAttribute(int flowFieldAttributeExceptionID, bool readOnly, bool visible, bool required, bool signalOnChange, int? requiredByFlowFieldID, string requiredByFlowFieldValue)
        {
            using (var uow = new UnitOfWork<PointContext>())
            {
                var existing = uow.FlowFieldAttributeExceptionRepository.FirstOrDefault(it => it.FlowFieldAttributeExceptionID == flowFieldAttributeExceptionID);
                if(existing != null)
                {
                    existing.ReadOnly = readOnly;
                    existing.Visible = visible;
                    existing.Required = required;
                    existing.SignalOnChange = signalOnChange;
                    existing.RequiredByFlowFieldID = requiredByFlowFieldID;
                    existing.RequiredByFlowFieldValue = requiredByFlowFieldValue;
                }
                uow.Save();
            }
        }

        public static void AddAttributes(int[] FlowFieldAttributeIDs, int? phaseDefinitionID, int? regionID, int? organizationID)
        {
            using (var uow = new UnitOfWork<PointContext>())
            {
                var existing = uow.FlowFieldAttributeRepository.Get(it => FlowFieldAttributeIDs.Contains(it.FlowFieldAttributeID));
                if (existing.Any())
                {
                    foreach (var newitem in existing)
                    {
                        uow.FlowFieldAttributeExceptionRepository.Insert(new FlowFieldAttributeException() {
                            FlowFieldAttributeID = newitem.FlowFieldAttributeID,
                            PhaseDefinitionID = phaseDefinitionID,
                            RegionID = regionID,
                            OrganizationID = organizationID,

                            ReadOnly = newitem.ReadOnly,
                            Required = newitem.Required,
                            Visible = newitem.Visible,
                            SignalOnChange = newitem.SignalOnChange
                        });
                    }
                }
                uow.Save();
            }
        }

        public static void AddAttribute(int FlowFieldAttributeID, int? phaseDefinitionID, int? regionID, int? organizationID, bool readOnly, bool required, bool visible, bool signalOnChange, int? requiredByFlowFieldID, string requiredByFlowFieldValue)
        {
            using (var uow = new UnitOfWork<PointContext>())
            {
                uow.FlowFieldAttributeExceptionRepository.Insert(new FlowFieldAttributeException()
                {
                    FlowFieldAttributeID = FlowFieldAttributeID,
                    PhaseDefinitionID = phaseDefinitionID,
                    RegionID = regionID,
                    OrganizationID = organizationID,

                    ReadOnly = readOnly,
                    Required = required,
                    Visible = visible,
                    SignalOnChange = signalOnChange,

                    RequiredByFlowFieldID = requiredByFlowFieldID,
                    RequiredByFlowFieldValue = requiredByFlowFieldValue
                });
                uow.Save();
            }
        }

        public static void DeleteAttributes(int formTypeID, int? phaseDefinitionID, int? regionID, int? organizationID)
        {
            using (var uow = new UnitOfWork<PointContext>())
            {
                uow.FlowFieldAttributeExceptionRepository.Delete(e =>
                    e.FlowFieldAttribute.FormTypeID == formTypeID &&
                    e.PhaseDefinitionID == phaseDefinitionID &&
                    e.RegionID == regionID &&
                    e.OrganizationID == organizationID
                );
                uow.Save();
            }
        }

        public static void DeleteAttribute(int flowFieldAttributeExceptionID)
        {
            using (var uow = new UnitOfWork<PointContext>())
            {
                uow.FlowFieldAttributeExceptionRepository.Delete(flowFieldAttributeExceptionID);
                uow.Save();
            }
        }
    }
}
