using Point.Business.BusinessObjects;
using Point.Database.Models;
using Point.Database.Repository;
using Point.ServiceBus.Models;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Data.Entity;
using System.Globalization;
using Point.Database.Models.ViewModels;
using Point.Models.Enums;

namespace Point.Business.Logic
{
    public class FlowInstanceSearchValuesBL
    {
        public static void Create(IUnitOfWork uow, int flowinstanceid, int transferid, string clientname, int? creatingdepartmentid)
        {
            var searchvalues = new FlowInstanceSearchValues() { FlowInstanceID = flowinstanceid, TransferID = transferid, ClientFullname = clientname };

            if (creatingdepartmentid.HasValue)
            {
                var department = DepartmentBL.GetByDepartmentID(uow, creatingdepartmentid.Value);
                if (department?.Location != null)
                {
                    var searchvaluedepartment = new FlowInstanceOrganization
                    {
                        OrganizationID = department.Location.OrganizationID,
                        LocationID = department.LocationID,
                        DepartmentID = department.DepartmentID,
                        FlowDirection = FlowDirection.Sending,
                        InviteType = InviteType.None,
                        InviteStatus = InviteStatus.Active
                    };

                    searchvalues.FlowInstanceOrganization.Add(searchvaluedepartment);
                }
            }
            
            uow.FlowInstanceSearchValuesRepository.Insert(searchvalues);
        }

        public static void CalculateAllReadValuesAsync(IUnitOfWork uow)
        {
            ServiceBusBL.SendMessage(uow, "ReadModel", "UpdateAll", new UpdateReadModel());
        }

        public static void CalculateReadValuesByTransferIDAsync(IUnitOfWork uow, int transferID)
        {
            var flowinstance = FlowInstanceBL.GetByTransferIDWithDeleted(uow, transferID);
            if (flowinstance == null) return;
            CalculateReadValuesByFlowinstanceIDAsync(uow, flowinstance.FlowInstanceID);
        }

        public static void CalculateReadValuesByFlowinstanceIDAsync(IUnitOfWork uow, int flowinstanceID)
        {
            ServiceBusBL.SendMessage(uow, "ReadModel", "UpdateFlowInstanceID" + flowinstanceID, new UpdateReadModel() { FlowInstanceID = flowinstanceID });
        }

        public static void CalculateReadValuesBatchAsync(IUnitOfWork uow, int[] flowinstanceIDs)
        {
            ServiceBusBL.SendMessage(uow, "ReadModelBatch", "UpdateReadModelBatch", new UpdateReadModelBatch() { FlowInstanceIDs = flowinstanceIDs  });
        }

        public static void CalculateSearchValues(int[] flowInstanceIDs)
        {
            using (var bo = new FlowInstanceBO())
            {
                foreach (var flowInstanceID in flowInstanceIDs)
                {
                    bo.CalculateSearchValuesByFlowInstanceID(flowInstanceID);
                }
                bo.Save();
            }
        }

        public static void CalculateSearchValues(int flowInstanceID)
        {
            using (var bo = new FlowInstanceBO())
            {
                bo.CalculateSearchValuesByFlowInstanceID(flowInstanceID);
                bo.Save();
            }
        }
        
        private static DateTime[] validateSearchCriteriaDateFormat(ref string[] sList)
        {
            List<DateTime> validDtList = new List<DateTime>();  
            //filter search string for date.
            var lst = sList.Where(m => m.Contains("-"));

            foreach (var s in lst)
            {
                DateTime outDt;                
                if (DateTime.TryParseExact(s, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out outDt) ||
                    DateTime.TryParseExact(s, "d-M-yy", CultureInfo.InvariantCulture, DateTimeStyles.None, out outDt))
                {
                    validDtList.Add(outDt);
                    //if it's a valid date, it must be removed from search string. otherwise it will be compared with full name and BSN too. so:
                    sList = sList.Where(st => st != s).ToArray();
                }                
            }
          return validDtList.ToArray();
        }

        public static List<FlowInstanceSearchValues> GetBySearchString(IUnitOfWork uow, PointUserInfo userinfo, string search)
        {          
            if (string.IsNullOrEmpty(search))
            {
                return null;
            }

            var searchlist = search.Split(' ').Where(it => !String.IsNullOrEmpty(it)).ToArray();
            var dtList = validateSearchCriteriaDateFormat(ref searchlist);

            var searchquerybl = SearchQueryBL.GetSearchQuerySimple(uow);
            searchquerybl.ApplyUserRoleFilter(userinfo, FlowDirection.Any);

            var list = searchquerybl.Queryable.Where(fisvm => 
            (searchlist.Count() == 0 || (searchlist.All(sl => fisvm.FlowInstanceSearchValues.ClientFullname.ToLower().Contains(sl) || fisvm.FlowInstanceSearchValues.ClientCivilServiceNumber.ToLower().Contains(sl)))) &&
            (dtList.Count()==0 ||(dtList.All(dt => DbFunctions.TruncateTime(fisvm.FlowInstanceSearchValues.ClientBirthDate) == dt))));
        
            return list.Select(it => it.FlowInstanceSearchValues).ToList();
        }
    }
}
