﻿using System.Collections.Generic;

namespace Point.Business.Logic
{
    public class DeliveryLocationBL
    {
        public IEnumerable<string> GetAll()
        {
            var locations = new List<string>();

            locations.Add("Thuis");
            locations.Add("Ziekenhuis");
            locations.Add("Ander adres, nl.:");

            return locations;
        }
    }
}
