﻿using System.Collections.Generic;
using System.Linq;
using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Caching;
using System;
using Point.Infrastructure.Extensions;

namespace Point.Business.Logic
{
    public class ScreenBL
    {

        public static IEnumerable<Screen> GetScreens(IUnitOfWork uow, bool usecache = true)
        {
            string cachestring = "Screen_Global";
            IList<Screen> screens = null;

            if (usecache)
            {
                screens = CacheService.Instance.Get<IList<Screen>>(cachestring);
            }

            if (screens == null)
            {
                screens = uow.ScreenRepository.Get(scr => scr.ScreenID>0, includeProperties: "FormType,GeneralAction").ToList();
                if (screens.Any())
                {
                    CacheService.Instance.Insert(cachestring, screens, "CacheLong");
                }
            }
            return screens;
        }

        public static Screen GetByFormTypeID(IUnitOfWork uow, int formTypeID)
        {
            return GetScreens(uow).FirstOrDefault(sc => sc.FormTypeID == formTypeID);
        }

        public static Screen GetByGeneralActionID(IUnitOfWork uow, int generalActionID)
        {
            return GetScreens(uow).FirstOrDefault(sc => sc.GeneralActionID == generalActionID);
        }

        public static Screen GetByURL(IUnitOfWork uow, Uri uri)
        {
            if (uri != null && !String.IsNullOrWhiteSpace(uri.AbsolutePath))
            {
                var pathParts = uri.AbsolutePath.Split('/').TakeLast<string>(2).ToList();
                if (pathParts.Count() == 2)
                    return ScreenBL.GetByURL(uow, pathParts[0], pathParts[1]);
            }
            return null;
        }

        public static Screen GetByURL(IUnitOfWork uow, string url)
        {
            return GetScreens(uow).FirstOrDefault(sc => sc.URL == url);
        }

        public static Screen GetByURL(IUnitOfWork uow, string controllerName, string actionName)
        {
            string url = String.Concat(controllerName, "/", actionName);
            return GetByURL(uow, url);
        }

        public static Screen ToNewModel(IUnitOfWork uow, string url, string screenTypes, string screenTitle, int? formTypeID= null, int? generalActionID = null )
        {
            var screen = new Screen
            {
                URL = url,
                ScreenTypes = screenTypes,
                ScreenTitle = screenTitle,
                FormTypeID = formTypeID,
                GeneralActionID = generalActionID
            };

            uow.ScreenRepository.Insert(screen);

            return screen;
        }

        public static Screen ToModel(ScreenViewModel viewModel, Screen screen)
        {
            screen.ScreenID = viewModel.ScreenID;
            screen.URL = viewModel.URL;
            screen.ScreenTypes = string.Join(",", viewModel.ScreenTypeLogs.Where(st => st.IsSelected).Select(st => st.ScreenTypeID).ToList());
            screen.ScreenTitle = viewModel.ScreenTitle;
            //FormType and GeneralAction wont be changed via Edit Screen page.
            //screen.FormTypeID = viewModel.FormTypeID;
            //screen.GeneralActionID = viewModel.GeneralActionID;
            return screen;
        }

        public static Screen GetModelByID(IUnitOfWork uow, int? screenID)
        {
            return uow.ScreenRepository.Get(scr => scr.ScreenID == screenID, includeProperties: "FormType,GeneralAction").FirstOrDefault();
        }

        public static IEnumerable<Screen> GetModelList(IUnitOfWork uow)
        {
            return uow.ScreenRepository.Get(scr => scr.ScreenID > 0, includeProperties: "FormType,GeneralAction");   
        }

        public static Screen Save(IUnitOfWork uow, Screen screen)
        {
            if (screen.ScreenID == 0)
            {
                uow.ScreenRepository.Insert(screen);               
            }
            
            uow.Save();

            //update screens cache
            var screens = uow.ScreenRepository.Get(scr => scr.ScreenID > 0, includeProperties: "FormType,GeneralAction").ToList();
            string cachestring = "Screen_Global";
            if (screens.Any())
            {
                CacheService.Instance.Remove(cachestring);
                CacheService.Instance.Insert(cachestring, screens);
            }
            return screen;
        }

    }
}
