﻿using Point.Database.Models;
using Point.Database.Repository;
using Point.Infrastructure.Caching;
using Point.Models.Enums;
using System.Collections.Generic;
using System.Linq;

namespace Point.Business.Logic
{
    public class OrganizationTypeBL
    {
        public static readonly List<int> NoneselectableOrganizationTypeIDs = new List<int>() {
            (int)OrganizationTypeID.Beheer
        };

        public static IEnumerable<OrganizationType> GetOrganizationTypes(IUnitOfWork uow, bool usecache = true)
        {
            string cachestring = "OrganizationType_Global";
            IList<OrganizationType> organizationtypes = null;

            if (usecache)
            {
                organizationtypes = CacheService.Instance.Get<IList<OrganizationType>>(cachestring);
            }

            if (organizationtypes == null)
            {
                organizationtypes = uow.OrganizationTypeRepository.GetAll().ToList();
                if (organizationtypes.Any())
                {
                    CacheService.Instance.Insert(cachestring, organizationtypes, "CacheLong");
                }
            }

            return organizationtypes.Where(ot => !NoneselectableOrganizationTypeIDs.Contains(ot.OrganizationTypeID));
        }
    }
}