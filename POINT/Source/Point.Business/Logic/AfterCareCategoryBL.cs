﻿using Point.Database.Models;
using Point.Database.Repository;
using Point.Infrastructure.Caching;
using System.Collections.Generic;
using System.Linq;

namespace Point.Business.Logic
{
    public class AfterCareCategoryBL
    {
        private const string cacheKey = "AfterCareCategories";

        public static List<AfterCareCategory> GetAll(IUnitOfWork uow)
        {
            var afterCareCategories = CacheService.Instance.Get<List<AfterCareCategory>>(cacheKey);
            if (afterCareCategories != null)
            { 
                return afterCareCategories;
            }

            afterCareCategories = uow.AfterCareCategoryRepository.GetAll().OrderBy(x => x.Sortorder).ToList();
            CacheService.Instance.Insert(cacheKey, afterCareCategories, "CacheLong");

            return afterCareCategories;
        }

        public static AfterCareCategory Get(IUnitOfWork uow, int afterCareCategoryID)
        {
            var afterCareCategories = GetAll(uow);
            return afterCareCategories.FirstOrDefault(acc => acc.AfterCareCategoryID == afterCareCategoryID);
        }
    }
}
