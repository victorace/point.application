﻿using System.Collections.Generic;
using System.Linq;
using Point.Database.Models;
using Point.Database.Repository;
using Point.Infrastructure.Extensions;
using System;
using System.Web;
using Point.Database.Models.ViewModels;

namespace Point.Business.Logic
{
    public class HtmlTextBL
    {
        public static HtmlText GetHtmlTextByRegion(IUnitOfWork uow, int regionID, bool decode = false)
        {
            var htmltext = uow.HtmlTextRepository.Get(ht => ht.RegionID == regionID).FirstOrNew();
            if (!String.IsNullOrEmpty(htmltext.HtmlText1) && decode)
            {
                htmltext.HtmlText1 = HttpUtility.HtmlDecode(htmltext.HtmlText1);
            }
            return htmltext;
        }

        public static void Upsert(IUnitOfWork uow, int regionID, string htmlText, PointUserInfo pointUserInfo)
        {
            var htmltext = uow.HtmlTextRepository.Get(ht => ht.RegionID == regionID).FirstOrDefault();
            if ( htmltext == null)
            {
                htmltext = new HtmlText()
                {
                    CreatedByEmployeeID = pointUserInfo.Employee.EmployeeID,
                    CreatedDate = DateTime.Now,
                    RegionID = regionID
                };
                uow.HtmlTextRepository.Insert(htmltext);
            }
            
            htmltext.HtmlText1 = htmlText;
            htmltext.ModifiedByEmployeeID = pointUserInfo.Employee.EmployeeID;
            htmltext.ModifiedDate = DateTime.Now;

            uow.Save();
        }
    }
}
