﻿using Point.Database.Models;
using Point.Database.Models.ViewModels;
using System.Collections.Generic;

namespace Point.Business.Logic
{
    public class PreferredLocationViewBL
    {
        public static List<PreferredLocationViewModel> FromModel(List<PreferredLocation> preferredlocations)
        {
            var viewmodel = new List<PreferredLocationViewModel>();

            foreach (var preferredlocation in preferredlocations)
            {
                viewmodel.Add(new PreferredLocationViewModel()
                {
                    PreferredLocationID = preferredlocation.PreferredLocationID,
                    LocationID = preferredlocation.LocationID,
                    FullName = preferredlocation.Location?.FullName(),
                    Inactive = ((preferredlocation?.Location?.Inactive ?? false) || (preferredlocation?.Location?.Organization?.Inactive ?? false))
                });
            }

            return viewmodel;
        }

        public static PreferredLocationViewModel FromModel(PreferredLocation preferredlocation)
        {
            var viewmodel = new PreferredLocationViewModel()
            {
                PreferredLocationID = preferredlocation.PreferredLocationID,
                LocationID = preferredlocation.LocationID,
                FullName = preferredlocation.Location?.FullName(),
                Inactive = ((preferredlocation?.Location?.Inactive ?? false) || (preferredlocation?.Location?.Organization?.Inactive ?? false))
            };

            return viewmodel;
        }
    }
}
