﻿using System;
using System.Collections.Generic;
using System.Linq;
using Point.Database.Models;
using Point.Database.Repository;

namespace Point.Business.Logic
{
    public class MutClientBL
    {
        public static IEnumerable<MutClient> GetByClientId(IUnitOfWork uow, int clientId)
        {
            return uow.MutClientRepository.Get(mc => mc.ClientID == clientId, null, "ClientModifiedByEmployee")
                      .OrderByDescending(mc => mc.ClientModifiedDate ?? new DateTime(1, 1, 1));
        }

        public static MutClient GetLatestByTransferId(IUnitOfWork uow, int transferId)
        {
            return GetByTransferId(uow, transferId).FirstOrDefault();
        }
        public static IEnumerable<MutClient> GetByTransferId(IUnitOfWork uow, int transferId)
        {
            int clientid = -1;
            var client = ClientBL.GetByTransferID(uow, transferId);
            if (client != null)
                clientid = client.ClientID;

            return GetByClientId(uow, clientid);

        }
    }
}
