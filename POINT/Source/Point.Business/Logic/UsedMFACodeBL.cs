﻿using System;
using Point.Database.Repository;
using Point.Database.Models.ViewModels;
using Point.Database.Models;
using Point.Models.Enums;
using System.Linq;

namespace Point.Business.Logic
{
    public class UsedMFACodeBL
    {
        public static MFAViewModel GetChangeMFAByPointUserInfo(IUnitOfWork uow, PointUserInfo pointUserInfo)
        {
            var changemfaviewmodel = new MFAViewModel();
            var employee = EmployeeBL.GetByID(uow, pointUserInfo.Employee.EmployeeID);
            if (employee != null)
            {
                changemfaviewmodel.EmployeeID = employee.EmployeeID;
                changemfaviewmodel.MFANumber = employee.MFANumber;
                changemfaviewmodel.MFAKey = employee.MFAKey;
                changemfaviewmodel.MFAConfirmedDate = employee.MFAConfirmedDate;
            }
            return changemfaviewmodel;
        }

        public static MFAViewModel GetLoginMFAByPointUserInfo(IUnitOfWork uow, PointUserInfo pointUserInfo)
        {
            var loginmfaviewmodel = new MFAViewModel();
            if (pointUserInfo.Employee != null)
            {
                loginmfaviewmodel.EmployeeID = pointUserInfo.Employee.EmployeeID;
                loginmfaviewmodel.MFANumber = pointUserInfo.Employee.MFANumber;
                loginmfaviewmodel.MFAKey = pointUserInfo.Employee.MFAKey;
                loginmfaviewmodel.MFAConfirmedDate = pointUserInfo.Employee.MFAConfirmedDate;
            }

            return loginmfaviewmodel;

        }

        public static void SaveMFANumber(IUnitOfWork uow, int employeeID, string mfaNumber)
        {
            var employee = EmployeeBL.GetByID(uow, employeeID);
            employee.MFANumber = mfaNumber;

            var logentry = LogEntryBL.Create(FlowFormType.NoScreen, employeeID);
            LoggingBL.FillLoggable(uow, employee, logentry);

            EmployeeBL.Save(uow, employee);
        }

        public static void SaveMFAKey(IUnitOfWork uow, int employeeID, string mfaKey)
        {
            var employee = EmployeeBL.GetByID(uow, employeeID);
            employee.MFAKey = mfaKey;

            var logentry = LogEntryBL.Create(FlowFormType.NoScreen, employeeID);
            LoggingBL.FillLoggable(uow, employee, logentry);

            EmployeeBL.Save(uow, employee);
        }

        public static string GetRandomMFACode()
        {
            Random rand = new Random((int)DateTime.Now.Ticks);
            return rand.Next(10000000, 99999999).ToString();
        }

        public static string GetRandomGoogleCode(int length)
        {
            const string chars = "ABEFGHJKMNPRTWXYZ0123456789";
            Random rand = new Random((int)DateTime.Now.Ticks);
            return new string(Enumerable.Repeat(chars, length).Select(s => s[rand.Next(s.Length)]).ToArray());
        }

        public static void CreateMFACode(IUnitOfWork uow, MFAViewModel mfaViewModel, MFACodeType mfaCodeType)
        {
            var organization = OrganizationBL.GetByEmployeeID(uow, mfaViewModel.EmployeeID);

            var usedmfacode = new UsedMFACode()
            {
                ClaimDateTime = null,
                EmployeeID = mfaViewModel.EmployeeID,
                MFACode = mfaViewModel.MFACode,
                MFACodeType = mfaCodeType,
                MFAKey = mfaViewModel.MFAKey,
                MFANumber = mfaViewModel.MFANumber,
                OrganizationID = organization.OrganizationID,
                SentDateTime = DateTime.Now
            };

            uow.UsedMFACodeRepository.Insert(usedmfacode);
            uow.Save();
        }

        public static void SetMFAConfirmed(IUnitOfWork uow, int employeeID)
        {
            var employee = EmployeeBL.GetByID(uow, employeeID);
            employee.MFAConfirmedDate = DateTime.Now;

            var logentry = LogEntryBL.Create(FlowFormType.NoScreen, employeeID);
            LoggingBL.FillLoggable(uow, employee, logentry);

            EmployeeBL.Save(uow, employee);
        }

        public static UsedMFACode GetUsedMFACodeByEmployeeID(IUnitOfWork uow, int employeeID, MFACodeType mfaCodeType)
        {
            var usedmfacode = uow.UsedMFACodeRepository.Get(umc => umc.EmployeeID == employeeID && umc.MFACodeType == mfaCodeType).OrderByDescending(umc => umc.UsedMFACodeID).FirstOrDefault();
            return usedmfacode;
        }

        public static void CheckConfirmMFAKey(IUnitOfWork uow, UsedMFACode usedMFACode)
        {
            usedMFACode.ClaimDateTime = DateTime.Now;
            uow.Save();
            SetMFAConfirmed(uow, usedMFACode.EmployeeID);
        }

        public static void CheckLoginMFAKey(IUnitOfWork uow, UsedMFACode usedMFACode)
        {
            usedMFACode.ClaimDateTime = DateTime.Now;
            uow.Save();
        }

        public static bool CheckConfirmMFACode(IUnitOfWork uow, MFAViewModel mfaViewModel)
        {
            var usedmfacode = uow.UsedMFACodeRepository.Get(umc => umc.EmployeeID == mfaViewModel.EmployeeID && umc.MFANumber == mfaViewModel.MFANumber && umc.MFACodeType == MFACodeType.Register).OrderByDescending(umc => umc.UsedMFACodeID).FirstOrDefault();
            if ( usedmfacode == null || usedmfacode.MFACode != mfaViewModel.MFACode )
            {
                return false; 
            }

            usedmfacode.ClaimDateTime = DateTime.Now;
            uow.Save();

            SetMFAConfirmed(uow, mfaViewModel.EmployeeID);
            return true;
        }

        public static bool CheckLoginMFACode(IUnitOfWork uow, MFAViewModel mfaViewModel)
        {
            var usedmfacode = uow.UsedMFACodeRepository.Get(umc => umc.EmployeeID == mfaViewModel.EmployeeID && umc.MFACodeType == MFACodeType.Login).OrderByDescending(umc => umc.UsedMFACodeID).FirstOrDefault();
            if (usedmfacode == null || usedmfacode.MFACode != mfaViewModel.MFACode)
            {
                return false;
            }

            usedmfacode.ClaimDateTime = DateTime.Now;
            uow.Save();

            return true;
        }


    }
}
