﻿using System;
using System.Collections.Generic;
using System.Linq;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Extensions;

namespace Point.Business.Logic
{
    public class ActionHealthInsurerBL
    {
        public static IEnumerable<ActionHealthInsurer> GetByFormSetVersionID(IUnitOfWork uow, int formsetversionid)
        {
            return uow.ActionHealthInsurerRepository.Get(ahi => ahi.FormSetVersionID == formsetversionid);
        }

        public static void SaveByViewModelAndFormSetVersionID(IUnitOfWork uow, IEnumerable<MsvtTreatmentItemViewModel> items, int formsetversionid, int transferid, int employeeid)
        {
            var timestamp = DateTime.Now;

            var currentitems = GetByFormSetVersionID(uow, formsetversionid).ToList();
            foreach (var item in items)
            {
                var currentitem = currentitems.FirstOrDefault(ci => ci.ActionCodeHealthInsurerID == item.Id);
                if (currentitem == null)
                {
                    currentitem = new ActionHealthInsurer
                    {
                        ActionCodeHealthInsurerID = item.Id,
                        FormSetVersionID = formsetversionid,
                        TransferID = transferid
                    };

                    uow.ActionHealthInsurerRepository.Insert(currentitem);
                }

                currentitem.EmployeeID = employeeid;
                currentitem.Amount = item.Frequency;
                currentitem.UnitFrequency = item.AmountUnitFrequency;
                currentitem.ExpectedTime = item.ExpectedTime;
                currentitem.Unit = item.FrequencyUnit?.GetDescription().ToLower() ?? "";
                currentitem.Definition = item.Definition;
                currentitem.TimeStamp = timestamp;
            }

            uow.Save();
        }

        public static void SaveByViewModelAndFormSetVersionID(IUnitOfWork uow, IEnumerable<GVPHandelingViewModel> items, int formsetversionid, int transferid, int employeeid)
        {
            var timestamp = DateTime.Now;

            var currentitems = GetByFormSetVersionID(uow, formsetversionid).ToList();
            foreach (var item in items)
            {
                var currentitem = currentitems.FirstOrDefault(ci => ci.ActionCodeHealthInsurerID == item.Id);
                if (currentitem == null)
                {
                    currentitem = new ActionHealthInsurer
                    {
                        ActionCodeHealthInsurerID = item.Id,
                        FormSetVersionID = formsetversionid,
                        TransferID = transferid
                    };

                    uow.ActionHealthInsurerRepository.Insert(currentitem);
                }

                currentitem.CustomFrequency = item.Frequency;
                currentitem.EmployeeID = employeeid;
                currentitem.Definition = item.Definition;
                currentitem.TimeStamp = timestamp;
            }

            uow.Save();
        }

        public static IEnumerable<ActionHealthInsurer> CopyTo(IUnitOfWork uow, FormSetVersion source, FormSetVersion destination)
        {
            List<ActionHealthInsurer> destinationActionHealthInsurers = new List<ActionHealthInsurer>();

            var sourceActionHealthInsurers = uow.ActionHealthInsurerRepository.Get(ahi => ahi.FormSetVersionID == source.FormSetVersionID, asNoTracking: true);
            foreach (var ahi in sourceActionHealthInsurers)
            {
                ahi.ActionHealthInsurerID = 0;
                ahi.TransferID = destination.TransferID;
                ahi.FormSetVersionID = destination.FormSetVersionID;

                uow.ActionHealthInsurerRepository.Insert(ahi);
                destinationActionHealthInsurers.Add(ahi);
            }

            if (destinationActionHealthInsurers.Any())
                uow.Save();

            return destinationActionHealthInsurers;
        }
    }
}
