using Point.Business.Expressions;
using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Point.Business.Logic
{
    public class SearchQueryBL
    {
        public static SearchQueryBL GetSearchQuerySimple(IUnitOfWork uow)
        {
            var qry = from i in uow.FlowInstanceSearchValuesRepository.Get(asNoTracking: true, includeProperties: "FlowInstanceSearchValueDepartments")
                      select new FlowInstanceSearchViewModel() { FlowInstanceSearchValues = i };
            
            return new SearchQueryBL() { Queryable = qry };
        }

        public static SearchQueryBL GetSearchQueryDoorlopendDossier(IUnitOfWork uow)
        {
            var qry = from i in uow.FlowInstanceDoorlopendDossierSearchValuesRepository.Get(asNoTracking: true)
                      select new FlowInstanceSearchViewModel() { FlowInstanceSearchValues = i.FlowInstanceSearchValues, FlowInstanceDoorlopendDossierSearchValues = i };

            return new SearchQueryBL() { Queryable = qry };
        }

        // Composites
        public void ApplyFilter(PointUserInfo pointUserInfo, FlowDirection flowdirection = FlowDirection.Any)
        {
            ApplyUserRoleFilter(pointUserInfo, flowdirection);
        }

        public void ApplyFilter(PointUserInfo pointUserInfo, DossierStatus status,
            FlowDirection flowdirection = FlowDirection.Any, FlowDefinitionID[] flowDefinitionIDs = null)
        {
            if (flowDefinitionIDs != null)
                ApplyFlowDefinitionsFilter(flowDefinitionIDs);
            ApplyStatusFilter(status);
            ApplyUserRoleFilter(pointUserInfo, flowdirection);
        }

        public void ApplyFilter(PointUserInfo pointUserInfo, int transferID)
        {
            ApplyTransferFilter(transferID);
            ApplyUserRoleFilter(pointUserInfo, FlowDirection.Any);
        }

        public void ApplyFilter(PointUserInfo pointUserInfo, FrequencyDossierStatus frequencyStatus,
            FlowDirection flowdirection = FlowDirection.Any, FlowDefinitionID[] flowDefinitionIDs = null)
        {
            if (flowDefinitionIDs != null)
                ApplyFlowDefinitionsFilter(flowDefinitionIDs);
            ApplyFrequencyStatusFilter(frequencyStatus);
            ApplyUserRoleFilter(pointUserInfo, flowdirection);
        }


        public void ApplyFilter(Expression<Func<FlowInstanceSearchViewModel, bool>> expression)
        {
            Queryable = Queryable.Where(expression);
        }
        
        public void ApplyTransferFilter(int transferID)
        {
            ApplyFilter(FlowInstanceExpressions.ByTransfer(transferID));
        }

        public void ApplyTransfersFilter(int[] transferIDs)
        {
            ApplyFilter(FlowInstanceExpressions.ByTransfers(transferIDs));
        }
        
        public void ApplyFlowDefinitionsFilter(FlowDefinitionID[] flowDefinitionIDs)
        {
            ApplyFilter(FlowInstanceExpressions.ByFlowDefinitions(flowDefinitionIDs));
        }

        public void ApplyHandlingFilter(DossierHandling handling, PointUserInfo pointuserinfo)
        {
            ApplyFilter(FlowInstanceExpressions.ByHandling(handling, pointuserinfo));
        }

        public void ApplyStatusFilter(DossierStatus status)
        {
            ApplyFilter(FlowInstanceExpressions.ByStatus(status));
        }

        public void ApplyUserRoleFilter(PointUserInfo pointUserInfo, FlowDirection flowdirection)
        {
            ApplyFilter(FlowInstanceExpressions.ByUserRole(pointUserInfo, flowdirection));
        }

        public void ApplyPhaseDefinitionsFilter(IEnumerable<int> phaseDefinitionIDs)
        {
            ApplyFilter(FlowInstanceExpressions.ByPhaseDefinitions(phaseDefinitionIDs));
        }
        
        public void ApplyDepartmentFilter(int departmentID)
        {
            ApplyFilter(FlowInstanceExpressions.ByDepartment(departmentID));
        }

        public void ApplyDepartmentFilter(int[] departmentIDs)
        {
            ApplyFilter(FlowInstanceExpressions.ByDepartments(departmentIDs));
        }

        public void ApplyLocationFilter(int locationID)
        {
            ApplyFilter(FlowInstanceExpressions.ByLocation(locationID));
        }

        public void ApplyOrganizationFilter(int organizationID)
        {
            ApplyFilter(FlowInstanceExpressions.ByOrganization(organizationID));
        }

        public void ApplyPatientNumberFilter(string patientNumber)
        {
            ApplyFilter(FlowInstanceExpressions.BySearchValuesClientPatientNumber(patientNumber));
        }

        public void ApplyFrequencyStatusFilter(FrequencyDossierStatus status)
        {
            ApplyFilter(FlowInstanceExpressions.ByFrequencyStatus(status));
        }

        public void ApplyCivilServiceNumberFilter(string civilServiceNumber, bool exactMatch = false)
        {
            ApplyFilter(FlowInstanceExpressions.BySearchValuesClientCivilServiceNumber(civilServiceNumber, exactMatch));
        }

        public void ApplyCivilServiceNumberOrPatientNumberFilter(string civilServiceNumber, string patientNumber)
        {
            ApplyFilter(FlowInstanceExpressions.BySearchValuesClientCivilServiceNumberOrPatientNumber(civilServiceNumber, patientNumber));
        }

        // Props
        public IQueryable<FlowInstanceSearchViewModel> Queryable { get; private set; }
    }
}