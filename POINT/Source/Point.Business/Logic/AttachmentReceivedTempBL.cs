﻿using Point.Business.BusinessObjects;
using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Point.Business.Logic
{
    public static class AttachmentReceivedTempBL
    {
        public static AttachmentReceivedTemp Insert(IUnitOfWork uow, Guid guid, int organizationid, string senderusername, DateTime senddate,
            string patientnumber, string visitnumber, string civilservicenumber, DateTime birthdate, 
            AttachmentReceivedTempBO transferattachmentreceivedtempbo, string hash)
        {
            var timestamp = DateTime.Now;

            var attachmentreceivedtemp = new AttachmentReceivedTemp()
            {
                TimeStamp = timestamp,
                GUID = guid,
                OrganisationID = organizationid, 
                SenderUserName = senderusername,
                SendDate = senddate,
                PatientNumber = patientnumber,
                CivilServiceNumber = civilservicenumber,
                VisitNumber = visitnumber,
                BirthDate = birthdate,
                FileName = transferattachmentreceivedtempbo.Name,
                Name = transferattachmentreceivedtempbo.AttachmentTypeID.GetDescription(),
                Description = transferattachmentreceivedtempbo.Description, 
                AttachmentTypeID = transferattachmentreceivedtempbo.AttachmentTypeID,
                Base64EncodedData = transferattachmentreceivedtempbo.Base64EncodedData,
                Hash = hash
            };

            uow.AttachmentReceivedTempRepository.Insert(attachmentreceivedtemp);
            uow.Save();

            return attachmentreceivedtemp;
        }

        public static IEnumerable<AttachmentReceivedTemp> GetLatestByClientDetails(IUnitOfWork uow, int organizationID, string patientNumber, string visitNumber, DateTime? birthdate)
        {
            var attachments = uow.AttachmentReceivedTempRepository.Get(art => art.OrganisationID == organizationID &&
                    art.PatientNumber == patientNumber && art.VisitNumber == visitNumber && (birthdate == null || art.BirthDate == birthdate))
                .GroupBy(art => new { art.AttachmentTypeID, art.FileName })
                .Select(s => s.OrderByDescending(x => x.TimeStamp).FirstOrDefault())
                .ToList();

            attachments.ForEach(art => art.ShowInAttachmentList = (art.AttachmentTypeID != AttachmentTypeID.ExternVODocument));

            return attachments;
        }

        public static void Delete(IUnitOfWork uow, int transferid, TransferAttachment attachment)
        {
            Delete(uow, transferid, new List<TransferAttachment>() { attachment });
        }

        public static void Delete(IUnitOfWork uow, int transferid, IEnumerable<TransferAttachment> attachments)
        {
            var flowInstance = FlowInstanceBL.GetByTransferID(uow, transferid);
            if (flowInstance == null)
            {
                return;
            }

            var organizationID = flowInstance.StartedByOrganizationID;
            var patientNumber = flowInstance?.Transfer?.Client.PatientNumber;
            var visitNumber = flowInstance?.Transfer?.Client.VisitNumber;
            var birthdate = flowInstance?.Transfer?.Client.BirthDate;

            // get all AttachmentReceivedTemp records that match OrganizationID, PatientNumber, VisitNumnber and BirthDate.
            // the returned records exclude actual file data, we are only interested in Name and AttachmentTypeID.
            var attachmentReceivedTemps = uow.AttachmentReceivedTempRepository.Get(art => art.OrganisationID == organizationID &&
                art.PatientNumber == patientNumber && art.VisitNumber == visitNumber && (birthdate == null || art.BirthDate == birthdate))
                .Select(art => new
                {
                    art.FileName,
                    art.AttachmentReceivedTempID,
                    art.AttachmentTypeID
                }).ToList();

            // match AttachmentReceivedTemp records against Attachments received
            //  i.e. Attachments that have been copied from AttachmentReceivedTemp to TransfetAttachment
            var usedAttachmentReceivedTempsIDs = from art in attachmentReceivedTemps
                join nameAndAttachmentTypeID in 
                    attachments.Select(ta => new
                    {
                        ta.GenericFile.FileName,
                        ta.AttachmentTypeID
                    }) 
                on new { art.FileName, art.AttachmentTypeID } equals new { nameAndAttachmentTypeID.FileName, nameAndAttachmentTypeID.AttachmentTypeID }
                select art.AttachmentReceivedTempID;

            uow.AttachmentReceivedTempRepository.Delete(art => usedAttachmentReceivedTempsIDs.Contains(art.AttachmentReceivedTempID));
        }

        public static void HandleAsVO(IUnitOfWork uow, FlowInstance flowinstance, AttachmentReceivedTemp attachmentReceivedTemp, PointUserInfo pointUserInfo, LogEntry logEntry)
        {
            var verpleegkundigeoverdrachtbo = new VerpleegkundigeOverdrachtBO(uow, flowinstance, pointUserInfo);
            verpleegkundigeoverdrachtbo.HandleNextPhase(attachmentReceivedTemp, logEntry);
        }
    }       
}