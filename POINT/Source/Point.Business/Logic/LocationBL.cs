﻿using Newtonsoft.Json;
using Point.Business.Helper;
using Point.Database.Extensions;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Caching;
using Point.Infrastructure.Extensions;
using Point.Models.Enums;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Point.Business.Logic
{
    public class LocationBL
    {
        public static string GetAllForTypeAhead(IUnitOfWork uow, IEnumerable<PreferredLocation> locations, int ownLocationID)
        {
            var preferredLocationIds = locations.Select(x => x.PreferredLocationID);
            var LocationsLookahead = uow.LocationRepository.Get(x => !(bool)x.Inactive && (!preferredLocationIds.Contains(x.LocationID) || x.LocationID != ownLocationID)).ToList()
                                                .Select(x => new { Name = x.FullName(), LocationId = x.LocationID })
                                                .OrderBy(x => x.Name);

            var locationsJson = JsonConvert.SerializeObject(LocationsLookahead,
                    Formatting.None,
                    new JsonSerializerSettings()
                    {
                        ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                    });

            return locationsJson;
        }

        public static IEnumerable<Location> GetActiveByRegionID(IUnitOfWork uow, int regionid)
        {
            return uow.LocationRepository.Get(loc => loc.Organization.RegionID == regionid && !OrganizationTypeBL.NoneselectableOrganizationTypeIDs.Contains(loc.Organization.OrganizationTypeID) &&
                !(bool)loc.Inactive);
        }

        public static IEnumerable<Location> GetActiveByOrganisationID(IUnitOfWork uow, int organizationID, int[] locationIDFilter = null)
        {
            if (locationIDFilter == null)
            {
                return uow.LocationRepository.Get(loc => loc.OrganizationID == organizationID && !OrganizationTypeBL.NoneselectableOrganizationTypeIDs.Contains(loc.Organization.OrganizationTypeID) &&
                    !(bool)loc.Inactive);
            }
            else
            {
                return uow.LocationRepository.Get(loc => loc.OrganizationID == organizationID && !OrganizationTypeBL.NoneselectableOrganizationTypeIDs.Contains(loc.Organization.OrganizationTypeID) &&
                    locationIDFilter.Contains(loc.LocationID) && !(bool)loc.Inactive);
            }
        }
        
        public static IEnumerable<Location> GetActiveByOrganisationAndFlowDefinitionID(IUnitOfWork uow, int organizationID, FlowDefinitionID flowdefinitionID)
        {
            return uow.LocationRepository.Get(loc => loc.OrganizationID == organizationID && !OrganizationTypeBL.NoneselectableOrganizationTypeIDs.Contains(loc.Organization.OrganizationTypeID)
                && !(bool)loc.Inactive
                && loc.FlowDefinitionParticipation.Any(ofd => ofd.FlowDefinitionID == flowdefinitionID && ( ofd.Participation == Participation.Active || ofd.Participation == Participation.ActiveButInvisibleInSearch))
            );
        }

        public static IEnumerable<Location> GetLocationsForSearch(IUnitOfWork uow, string search, int organizationID)
        {
            return uow.LocationRepository.Get(loc => (string.IsNullOrEmpty(search) || loc.Name.ToLower().Contains(search.ToLower())) && loc.OrganizationID == organizationID).OrderBy(it => it.Name);
        }

        public static Location GetByID(IUnitOfWork uow, int locationID)
        {
            return uow.LocationRepository.GetByID(locationID);
        }

        public static bool InSameLocation(IUnitOfWork uow, int employeeID, PointUserInfo pointUserInfo)
        {
            var employee = PointUserInfoHelper.GetPointUserInfoByEmployeeID(uow, employeeID);

            return employee.Location.LocationID == pointUserInfo.Location.LocationID;
        }

        public static IEnumerable<Location> GetActive(IUnitOfWork uow)
        {
            return uow.LocationRepository.Get(l => !(bool)l.Inactive);
        }

        public static IEnumerable<Location> GetAll(IUnitOfWork uow)
        {
            return uow.LocationRepository.GetAll();
        }

        public static IEnumerable<Location> GetActiveByOrganizationTypeID(IUnitOfWork uow, int organizationtypeid)
        {
            return uow.LocationRepository.Get(l => l.Organization.OrganizationTypeID == organizationtypeid && !(bool)l.Inactive);
        }

        public static IEnumerable<Location> GetActiveByOrganizationTypeIDs(IUnitOfWork uow, IEnumerable<int> organizationtypeids)
        {
            return uow.LocationRepository.Get(l => organizationtypeids.Contains(l.Organization.OrganizationTypeID) && !(bool)l.Inactive);
        }

        public static IEnumerable<Location> GetActiveByOrganisationIDs(IUnitOfWork uow, IEnumerable<int> organizationids)
        {
            if (organizationids == null)
            {
                return Enumerable.Empty<Location>();
            }
            return uow.LocationRepository.Get(l => organizationids.Contains(l.OrganizationID) && !(bool)l.Inactive);
        }

        public static IEnumerable<Location> GetByOrganisationIDs(IUnitOfWork uow, IEnumerable<int> organizationids)
        {
            if (organizationids == null)
            {
                return Enumerable.Empty<Location>();
            }
            return uow.LocationRepository.Get(l => organizationids.Contains(l.OrganizationID));
        }

        public static IEnumerable<Location> GetActiveByDepartmentIDs(IUnitOfWork uow, IEnumerable<int> departmentids)
        {
            if (departmentids == null)
            {
                return Enumerable.Empty<Location>();
            }
            return uow.LocationRepository.Get(l => l.Department.Any(dep => departmentids.Contains(dep.DepartmentID)) && !(bool)l.Inactive);
        }
        
        public static IEnumerable<Location> GetActiveByLocationIDs(IUnitOfWork uow, IEnumerable<int> locationids)
        {
            if (locationids == null)
            {
                return Enumerable.Empty<Location>();
            }
            return uow.LocationRepository.Get(l => locationids.Contains(l.LocationID) && !(bool)l.Inactive);
        }
        
        public static IEnumerable<MutLocation> GetHistoryByLocationID(IUnitOfWork uow, int locationid)
        {
            return uow.MutLocationRepository.Get(it => it.LocationID == locationid);
        }

        public static Location Save(IUnitOfWork uow, LocationViewModel viewmodel, LogEntry logentry, PointUserInfo pointUserInfo)
        {
            var model = GetByID(uow, viewmodel.LocationID) ?? new Location();

            model.WriteSecureValue(m => m.OrganizationID, viewmodel, v => v.OrganizationID, pointUserInfo);
            model.WriteSecureValue(m => m.Name, viewmodel, v => v.Name, pointUserInfo);
            model.WriteSecureValue(m => m.StreetName, viewmodel, v => v.StreetName, pointUserInfo);
            model.WriteSecureValue(m => m.Number, viewmodel, v => v.Number, pointUserInfo);
            model.WriteSecureValue(m => m.PostalCode, viewmodel, v => v.PostalCode, pointUserInfo);
            model.WriteSecureValue(m => m.City, viewmodel, v => v.City, pointUserInfo);
            model.WriteSecureValue(m => m.IPCheck, viewmodel, v => v.IPCheck, pointUserInfo);
            model.WriteSecureValue(m => m.IPAddress, viewmodel, v => v.IPAddress, pointUserInfo);
            model.WriteSecureValue(m => m.Area, viewmodel, v => v.Area, pointUserInfo);

            var previouslyinactive = model.Inactive;
            model.WriteSecureValue(m => m.Inactive, viewmodel, v => v.Inactive, pointUserInfo);
            if(model.Inactive == true && previouslyinactive == false)
            {
                var departments = DepartmentBL.GetByLocationID(uow, model.LocationID).ToList();
                foreach (var item in departments)
                {
                    if(item.Inactive == false)
                    {
                        item.Inactive = true;
                        LoggingBL.FillLoggable(uow, item, logentry, true);
                    }
                }
            }

            var currentregionids = model.LocationRegion.Select(it => it.RegionID).ToList();
            var newotherregionids = new List<int>();
            if (viewmodel.OtherRegionIDs != null)
            {
                foreach (var idString in viewmodel.OtherRegionIDs.Split(','))
                {
                    if (int.TryParse(idString, out var idInt))
                    {
                        newotherregionids.Add(idInt);
                    }
                }
            }

            var regionidstodelete = currentregionids.Where(it => !newotherregionids.Contains(it));
            uow.LocationRegionRepository.Delete(it => it.LocationID == model.LocationID && regionidstodelete.Contains(it.RegionID));

            var regionidstoadd = newotherregionids.Where(it => !currentregionids.Contains(it)).ToList();
            regionidstoadd.ForEach(rid => model.LocationRegion.Add(new LocationRegion() { RegionID = rid, ModifiedTimeStamp = logentry.Timestamp }));

            viewmodel.FlowParticipation = viewmodel.FlowParticipation.Where(it => it.Participation != Participation.None).ToList();
            
            var newflowdefinitionids = viewmodel.FlowParticipation.Select(pa => pa.FlowDefinitionID).ToArray();
            uow.FlowDefinitionParticipationRepository.Delete(it => it.LocationID == viewmodel.LocationID && newflowdefinitionids.Contains(it.FlowDefinitionID) == false);

            var existingparticipations = FlowDefinitionParticipationBL.GetByLocationID(uow, viewmodel.LocationID).ToList();
            foreach (var existingparticipation in existingparticipations)
            {
                var fromviewmodel = viewmodel.FlowParticipation.FirstOrDefault(it => it.FlowDefinitionID == existingparticipation.FlowDefinitionID);
                if(fromviewmodel != null)
                {
                    existingparticipation.FlowDirection = fromviewmodel.FlowDirection;
                    existingparticipation.Participation = fromviewmodel.Participation;

                    LoggingBL.FillLoggable(uow, existingparticipation, logentry);
                }
            }

            var newparticipations = viewmodel.FlowParticipation.Where(it => existingparticipations.Select(ex => ex.FlowDefinitionID).ToArray().Contains(it.FlowDefinitionID) == false);
            foreach (var newparticipation in newparticipations)
            {
                
                var newparticipationmodel = new FlowDefinitionParticipation
                {
                    FlowDefinitionID = newparticipation.FlowDefinitionID,
                    Participation = newparticipation.Participation,
                    FlowDirection = newparticipation.FlowDirection,
                };
                model.FlowDefinitionParticipation.Add(newparticipationmodel);

                LoggingBL.FillLoggable(uow, newparticipationmodel, logentry);
            }
            
            if (model.LocationID <= 0)
            {
                uow.LocationRepository.Insert(model);
            }

            //Force modified state to set modified timestamp (See LoggingBL.FillLoggable)
            if(uow.DatabaseContext.Entry(model).State == System.Data.Entity.EntityState.Unchanged)
            {
                uow.DatabaseContext.Entry(model).State = System.Data.Entity.EntityState.Modified;
            }
            LoggingBL.FillLoggable(uow, model, logentry);

            Task.Run(async () => await DepartmentCapacityPublicBL.UpdateOnLocationChange(model));

            uow.Save(logentry);
            
            CacheService.Instance.RemoveAll("FlowDefinitionParticipation_" + model.LocationID);

            return model;
        }

        private static string GetParticipationString(List<FlowParticipationViewModel> flowparticipations)
        {
            string functionroleString = "";
            flowparticipations.ForEach(fr => { functionroleString += ((FlowDefinitionID)fr.FlowDefinitionID).GetDescription() + "-" + fr.Participation.GetDescription() + "-" + fr.FlowDirection.GetDescription() + " \r\n"; });

            return functionroleString;
        }
    }
}
