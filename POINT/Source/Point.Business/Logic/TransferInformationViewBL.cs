﻿using Point.Business.Helper;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;

namespace Point.Business.Logic
{
    public class TransferInformationViewBL
    {
        public static TransferInformationViewModel FromModel(IUnitOfWork uow, Transfer transfer)
        {
            if (transfer == null)
            {
                return new TransferInformationViewModel();
            }

            var flowinstance = FlowInstanceBL.GetByTransferID(uow, transfer.TransferID);
            if (flowinstance == null)
            {
                return new TransferInformationViewModel();
            }

            int flowinstanceid = flowinstance.FlowInstanceID;

            var sourceorganization = OrganizationHelper.GetSendingOrganization(uow, flowinstanceid);
            var sourcelocation = OrganizationHelper.GetSendingLocation(uow, flowinstanceid);
            var sourcedepartment = OrganizationHelper.GetSendingDepartment(uow, flowinstanceid);

            var destinationorganization = OrganizationHelper.GetReceivingOrganization(uow, flowinstanceid);
            var destinationlocation = OrganizationHelper.GetRecievingLocation(uow, flowinstanceid);
            var destinationdepartment = OrganizationHelper.GetRecievingDepartment(uow, flowinstanceid);

            var model = new TransferInformationViewModel()
            {
                SourceOrganizationName = sourceorganization?.Name,
                SourceLocationName = sourcelocation?.Name,
                SourceDepartmentName = sourcedepartment?.Name,
                SourceDepartmentEmail = sourcedepartment?.EmailAddress,
                SourceDepartmentTelephonenumber = sourcedepartment?.PhoneNumber,
                SourceDepartmentFullName = sourcedepartment?.FullName(),

                DestinationOrganizationName = destinationorganization?.Name,
                DestinationLocationName = destinationlocation?.Name,
                DestinationDepartmentName = destinationdepartment?.Name,
                DestinationDepartmentEmail = destinationdepartment?.EmailAddress,
                DestinationDepartmentTelephonenumber = destinationdepartment?.PhoneNumber,
                DestinationDepartmentFullName = destinationdepartment?.FullName()
            };

            return model;
        }
    }
}
