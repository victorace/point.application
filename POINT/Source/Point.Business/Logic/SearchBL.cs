﻿using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Extensions;
using System;
using System.Data.Entity;
using System.Linq;

namespace Point.Business.Logic
{
    public static class SearchBL
    {
        public static FlowInstance GetSecurityTrimmedFlowInstance(IUnitOfWork uow, PointUserInfo pointUserInfo, int transferID)
        {
            var sq = SearchQueryBL.GetSearchQuerySimple(uow);
            sq.ApplyFilter(pointUserInfo, transferID);
            var res = ToFlowInstance(sq);
            return res;
        }

        public static Client GetSecurityTrimmedClientByCivilServiceNumber(IUnitOfWork uow, PointUserInfo pointUserInfo, string civilServiceNumber, bool exactMatch = false)
        {
            var sq = SearchQueryBL.GetSearchQuerySimple(uow);

            sq.ApplyFilter(pointUserInfo);
            sq.ApplyCivilServiceNumberFilter(civilServiceNumber, exactMatch);

            return sq.Queryable.OrderByDescending(q => q.FlowInstanceSearchValues.FlowInstanceID).FirstOrDefault()?.FlowInstanceSearchValues?.FlowInstance?.Transfer?.Client;
        }

        public static IQueryable<FlowInstanceSearchViewModel> ToSearchResultEntriesTable(IQueryable<FlowInstanceSearchViewModel> query, SearchStatement searchStatement, int pageSize)
        {
            bool sortdescending = (searchStatement.SortOrder == SortOrder.desc);

            if (searchStatement.SortColumn != null && searchStatement.SortColumn != "TransferCreatedDate")
            {
                var sortstring = sortdescending ? "desc" : "asc";

                var searchfieldowningtype = "FlowInstanceSearchValues";
                if (searchStatement.SearchFieldOwningType == SearchFieldOwningType.Frequency)
                {
                    searchfieldowningtype = "FlowInstanceDoorlopendDossierSearchValues";
                }

                query = query.OrderBy($"{searchfieldowningtype}.{searchStatement.SortColumn} {sortstring}").ThenByDescending(it => it.FlowInstanceSearchValues.TransferID);
            }
            else
            {
                query = query.OrderByDirection(fi => fi.FlowInstanceSearchValues.FlowInstanceID, sortdescending);
            }

            query = query.Skip((searchStatement.Page - 1) * pageSize).Take(pageSize);

            return query;
        }

        public static FlowInstance ToFlowInstance(SearchQueryBL searchQueryBL, Func<IQueryable<FlowInstanceSearchViewModel>, IQueryable<FlowInstanceSearchViewModel>> callback = null)
        {
            var qry = searchQueryBL.Queryable;

            if (callback != null)
            {
                qry = callback(qry);
            }

            var res = qry.FirstOrDefault();

            if (res == null)
            {
                var directResult = searchQueryBL.Queryable.FirstOrDefault();
                if (directResult != null)
                    return directResult.FlowInstanceSearchValues.FlowInstance;

                return null;
            }

            return res.FlowInstanceSearchValues.FlowInstance;
        }
    }
}