﻿using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Exceptions;
using System.Web.Security;

namespace Point.Business.Logic
{
    public class AccountDetailsViewModelBL
    {
        public static void Save(IUnitOfWork uow, AccountDetailsViewModel viewmodel, bool validationRequired)
        {
            var dbmodel = EmployeeBL.GetByID(uow, viewmodel.EmployeeID);
            if(dbmodel != null)
            {
                //validation is not required for sso users.
                if (!validationRequired || Membership.ValidateUser(dbmodel.aspnet_Users.UserName, viewmodel.PasswordCheck))
                {
                    dbmodel.FirstName = viewmodel.FirstName;
                    dbmodel.MiddleName = viewmodel.MiddleName;
                    dbmodel.LastName = viewmodel.LastName;
                    dbmodel.Position = viewmodel.Position;
                    dbmodel.PhoneNumber = viewmodel.TelephoneNumber;
                    dbmodel.EmailAddress = viewmodel.EmailAdress;
                    dbmodel.BIG = viewmodel.BIG;
                    dbmodel.AutoEmailSignalering = viewmodel.AutoEmailSignalering;
                    if (viewmodel.SetSecretQuestion)
                    {
                        var member = Membership.GetUser(dbmodel.UserId);
                        member.ChangePasswordQuestionAndAnswer(viewmodel.PasswordCheck, viewmodel.SecretQuestion, viewmodel.SecretQuestionAnswer);
                    }
                    uow.Save();
                } else
                {
                    throw new PointSecurityException("Het opgegeven wachtwoord is niet correct", ExceptionType.Default);
                }
            }
        }

        public static AccountDetailsViewModel FromModel(Employee employee)
        {
            var member = Membership.GetUser(employee.UserId);

            var viewmodel = new AccountDetailsViewModel
            {
                EmployeeID = employee.EmployeeID,
                FirstName = employee.FirstName,
                MiddleName = employee.MiddleName,
                LastName = employee.LastName,
                Position = employee.Position,
                TelephoneNumber = employee.PhoneNumber,
                EmailAdress = employee.EmailAddress,
                BIG = employee.BIG,
                AutoEmailSignalering = employee.AutoEmailSignalering,
                NotAutoLogin = employee.DefaultDepartment?.Location?.Organization?.SSO != true,
                SecretQuestion = member.PasswordQuestion
            };

            return viewmodel;
        }
    }
}
