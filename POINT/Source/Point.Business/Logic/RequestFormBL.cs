﻿using Point.Models.Enums;
using Point.Infrastructure.Extensions;
using System;
using System.Collections.Generic;

namespace Point.Business.Logic
{
    public class RequestFormBL
    {
        public static Dictionary<string, List<string>> GetZHVVTSections(FlowDefinitionID flowdefinitionid)
        {
            var sections = new Dictionary<string, List<string>>();

            if (flowdefinitionid == FlowDefinitionID.ZH_VVT || flowdefinitionid == FlowDefinitionID.CVA)
            {
                sections.Add("210", new List<string> {"Basisgegevens",
                "SituatieClient", "MedischeDiagnoses", "VrijheidBeperkendeMaatregelen", "AspectenZorgverlening", "PsychischFunctioneren", "Arts", "MedeBehandelaars",
                "Toestemming",
                "ZorgAdvies",
                "ZorgVraagBasis", "ZorgVraagContinuiteits", "ZorgVraagHulpmiddelen",
                "ZorgSoortOpname", "ZorgSoortTypering", "ZorgSoortZorginstellingVoorkeur"});

                sections.Add("211", new List<string> { "Basisgegevens",
                "SituatieClient", "Arts", "MedeBehandelaars", "MedischeDiagnoses", "VrijheidBeperkendeMaatregelen",
                "ZorgAdvies",
                "HervattenZorg",
                "ZorgSoortOpname"});

                sections.Add("212", new List<string> { "Basisgegevens",
                "Arts", "IngangsdatumZorg", "MedischeDiagnoses", 
                "Toestemming",
                "ZorgAdvies",
                "ZorgVraagPoli",
                "ZorgSoortAfwijkendLeveringAdres", "ZorgSoortZorginstellingVoorkeur" });

                sections.Add("213", new List<string> { "Basisgegevens",
                "SituatieClient", "Arts", "MedeBehandelaars", "MedischeDiagnoses", "AspectenZorgverlening",
                "Toestemming",
                "ZorgAdvies",
                "ZorgVraagBasis",
                "Inventarisatie", "InventarisatieOpmerkingAfdVPK",
                "ZorgSoortOpname", "ZorgSoortTypering", "ZorgSoortZorginstellingVoorkeur" });

                sections.Add("233", new List<String> {"Basisgegevens",
                "Arts", "MedischeDiagnoses", "Hulpmiddelen", "DatumOpname", "Toelichting"});
            }
            else if (flowdefinitionid == FlowDefinitionID.RzTP)
            {
                sections.Add("227", new List<string> {"Basisgegevens",
                "SituatieClient", "Arts", "MedeBehandelaars", "MedischeDiagnoses", "VrijheidBeperkendeMaatregelen", 
                "ZorgAdvies",
                "ZorgSoortTypering"});

                sections.Add("237", new List<string> { "Basisgegevens",
                "Arts", "IngangsdatumZorg", "MedischeDiagnoses", 
                "Toestemming",
                "ZorgAdvies",
                "ZorgVraagPoli",
                "ZorgSoortAfwijkendLeveringAdres", "ZorgSoortZorginstellingVoorkeur" });
            }
            else if (flowdefinitionid == FlowDefinitionID.VVT_VVT)
            {
                sections.Add("243", new List<string> {"Basisgegevens",
                "IngangsdatumZorg", "AspectenZorgverlening", "PsychischFunctioneren",
                "Toestemming",
                "ZorgVraagBasis", "ZorgVraagContinuiteits", "ZorgVraagHulpmiddelen",
                "ZorgSoortOpname", "ZorgSoortTypering", "ZorgSoortZorginstellingVoorkeur"});
            }
            else
            {
                throw new Exception($"Geen aanvraag secties gevonden");
            }

            return sections;
        }
    }
}
