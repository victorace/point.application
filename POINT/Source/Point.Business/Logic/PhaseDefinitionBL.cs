﻿using System;
using System.Collections.Generic;
using System.Linq;
using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Repository;
using Point.Infrastructure.Caching;
using Point.Infrastructure.Extensions;

namespace Point.Business.Logic
{
    public class PhaseDefinitionBL
    {
        public static PhaseDefinition GetByID(IUnitOfWork uow, int phasedefinitionID)
        {
            return uow.PhaseDefinitionRepository.GetByID(phasedefinitionID);
        }

        public static PhaseDefinition GetByFlowDefinitionIDAndFlowHandling(IUnitOfWork uow, FlowDefinitionID flowDefinitionID, FlowHandling flowHandling)
        {
            return uow.PhaseDefinitionRepository.FirstOrDefault(pd => pd.FlowDefinitionID == flowDefinitionID && pd.FlowHandling == flowHandling);
        }
        
        public static List<PhaseDefinition> GetNextPhaseDefinitions(IUnitOfWork uow, int phaseDefinitionID)
        {
            return uow.PhaseDefinitionRepository.Get(p => p.PreviousPhaseDefinitionNavigation.Any(pp => pp.PhaseDefinitionID == phaseDefinitionID && pp.ActionTypeID == ActionTypeID.Next)).ToList();
        }
    }
}