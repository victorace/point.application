﻿using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Point.Business.Logic
{
    public class SystemMessageBL
    {
        public static IQueryable<SystemMessage> GetLatest(IUnitOfWork uow, int employeeid)
        {
            var currentdate = DateTime.Now;
            var lastmessageread = uow.SystemMessageReadRepository.Get(r => r.EmployeeID == employeeid).OrderByDescending(it => it.DateRead).FirstOrDefault();
            var lastdateread = lastmessageread?.DateRead;

            return uow.SystemMessageRepository.Get(m =>
                m.IsActive &&
                m.StartDate <= currentdate &&
                m.EndDate >= currentdate && 
                (m.IsPermanent || (lastdateread == null || m.StartDate > lastdateread.Value))
                ).OrderByDescending(m => m.StartDate);
        }

        public static void InsertSystemMessageRead(IUnitOfWork uow, int employeeid)
        {
            uow.SystemMessageReadRepository.Delete(smr => smr.EmployeeID == employeeid);
            var newobject = new SystemMessageRead() { DateRead = DateTime.Now, EmployeeID = employeeid };
            uow.SystemMessageReadRepository.Insert(newobject);
            uow.Save();
        }

        public static SystemMessage GetByID(IUnitOfWork uow, int systemmessageid)
        {
            return uow.SystemMessageRepository.Get(sm => sm.SystemMessageID == systemmessageid).FirstOrDefault();
        }

        public static bool DeleteByID(IUnitOfWork uow, int systemmessageid)
        {
            var systemmessage = GetByID(uow, systemmessageid);
            if (systemmessage == null) return false;
            uow.SystemMessageRepository.Delete(systemmessage);
            uow.Save();
            return true;
        }

        public static IEnumerable<SystemMessage> GetAll(IUnitOfWork uow)
        {
            return uow.SystemMessageRepository.GetAll();
        }

        public static IEnumerable<SystemMessage> GetAllPastMessages(IUnitOfWork uow)
        {
            return uow.SystemMessageRepository.Get(it => it.IsActive && it.StartDate < DateTime.Now);
        }

        public static SystemMessage Save(IUnitOfWork uow, SystemMessageViewModel viewmodel)
        {
            var model = GetByID(uow, viewmodel.SystemMessageID) ?? new SystemMessage();

            if (model.SystemMessageID == 0)
            {
                uow.SystemMessageRepository.Insert(model);
            }

            model.Title = viewmodel.Title?.Trim();
            model.Message = viewmodel.Message?.Trim();

            model.LinkReference = AllowSimpleTextOnly(viewmodel.LinkReference?.Trim());
            model.LinkText = AllowSimpleTextOnly(viewmodel.LinkText?.Trim());

            model.StartDate = viewmodel.StartDate.Add(viewmodel.StartTime);
            model.EndDate = viewmodel.EndDate.Add(viewmodel.EndTime);
            model.FunctionCode = viewmodel.FunctionCode ?? "";
            model.IsPermanent = viewmodel.IsPermanent;
            model.IsActive = viewmodel.IsActive;

            uow.Save();

            return model;
        }

        private static string AllowSimpleTextOnly(string input)
        {
            return input == null
                ? ""
                : Regex.Replace(input, @"[^\w:/\ \.]+", "");
        }

    }
}