﻿using System.Collections.Generic;
using System.Linq;
using Point.Database.Models;
using Point.Database.Repository;

namespace Point.Business.Logic
{
    public class MutMedicineCardBL
    {
        public static IEnumerable<MutMedicineCard> GetByFormSetVersionID(IUnitOfWork uow, int formsetversionid)
        {
            return uow.MutMedicineCardRepository.Get(mahi => mahi.FormSetVersionID == formsetversionid).OrderByDescending(it => it.MutMedicineCardID);
        }
    }
}
