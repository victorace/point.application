﻿using System;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Extensions;
using Point.Models.Enums;

namespace Point.Business.Logic
{
    public static class ClientFullViewBL
    {
        public static ClientFullViewModel FromModel(IUnitOfWork uow, Client client, PointUserInfo userinfo, FlowDefinitionID? flowdefinitionid)
        {
            if (client == null || client == default(Client))
                return new ClientFullViewModel();

            var clientFullViewModel = new ClientFullViewModel();
            clientFullViewModel.Merge<ClientFullViewModel>(client);

            var flowinstance = FlowInstanceBL.GetByClientID(uow, client.ClientID);
            clientFullViewModel.ClientShowAnonymous = ClientBL.ShowAnonymous(flowinstance, userinfo.Organization?.OrganizationID);

            if (clientFullViewModel.ContactPerson == null)
                clientFullViewModel.ContactPerson = new ContactPersonViewModel();

            clientFullViewModel.ContactPerson.LastName = client.ContactPersonName;
            Int32 relationType;
            if (Int32.TryParse(client.ContactPersonRelationType, out relationType))
            {
                clientFullViewModel.ContactPerson.CIZRelationID = relationType;
            }
            clientFullViewModel.ContactPerson.PhoneNumberGeneral = client.ContactPersonPhoneNumberGeneral;
            clientFullViewModel.ContactPerson.PhoneNumberMobile = client.ContactPersonPhoneNumberMobile;
            clientFullViewModel.ContactPerson.PhoneNumberWork = client.ContactPersonPhoneWork;
            //   clientFullViewModel.ShowSearchBSN = flowdefinitionid != FlowDefinitionID.HA_VVT;
           // clientFullViewModel.ShowSearchComplete = true;// !clientFullViewModel.ShowSearchBSN;
            clientFullViewModel.HasNoCivilServiceNumber = client.HasNoCivilServiceNumber;

            return clientFullViewModel;
        }
    }
}
