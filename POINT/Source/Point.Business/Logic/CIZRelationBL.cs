﻿using System.Collections.Generic;
using System.Linq;
using Point.Database.Models;
using Point.Database.Repository;

namespace Point.Business.Logic
{
    public class CIZRelationBL
    {
        public static IEnumerable<CIZRelation> GetCIZRelations(IUnitOfWork uow)
        {
            return uow.CIZRelationRepository.GetAll().OrderBy(ciz => ciz.EnumerationValue);
        }
    }
}
