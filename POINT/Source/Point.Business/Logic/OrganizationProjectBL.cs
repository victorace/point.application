﻿using Point.Database.Models;
using Point.Database.Repository;
using Point.Infrastructure.Caching;
using System.Collections.Generic;
using System.Linq;

namespace Point.Business.Logic
{
    public class OrganizationProjectBL
    {
        public static IEnumerable<OrganizationProject> GetByIDs(IUnitOfWork uow, int[] organizationprojectids)
        {
            return uow.OrganizationProjectRepository.Get(op => organizationprojectids.Contains(op.OrganizationProjectID));
        }
        
        public static IEnumerable<OrganizationProject> GetActiveByOrganizationIDs(IUnitOfWork uow, int[] organizationids)
        {
            return uow.OrganizationProjectRepository.Get(op => organizationids.Contains(op.OrganizationID) && !op.InActive);
        }

        public static IEnumerable<OrganizationProject> GetActiveByOrganizationID(IUnitOfWork uow, int organizationid)
        {
            return uow.OrganizationProjectRepository.Get(op => op.OrganizationID == organizationid && !op.InActive);
        }

        public static bool HasOrganizationTagsDefined(IUnitOfWork uow, int organizationid)
        {
            return uow.OrganizationProjectRepository.Any(op => op.OrganizationID == organizationid && !op.InActive);
        }

        public static IEnumerable<OrganizationProject> GetAllByOrganizationIDs(IUnitOfWork uow, int[] organizationids)
        {
            return uow.OrganizationProjectRepository.Get(op => organizationids.Contains(op.OrganizationID));
        }
    }
}
