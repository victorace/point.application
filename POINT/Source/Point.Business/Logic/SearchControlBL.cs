﻿using Point.Database.Models.ViewModels;
using Point.Infrastructure.Extensions;
using System.Web;

namespace Point.Business.Logic
{
    public static class SearchControlBL
    {
        private const string sessionIdentifierSearchControl = "SearchControl";

        public static void DeleteState()
        {
            var httpcontext = HttpContext.Current;
            if (httpcontext == null || httpcontext.Session == null) return;

            httpcontext.Session[sessionIdentifierSearchControl] = null;
        }

        public static void SaveState(SearchControl searchcontrol, HttpContextBase httpcontextbase)
        {
            if (httpcontextbase == null || httpcontextbase.Session == null) return;

            //Filter specific fields (PBI 12569)
            var copy = new SearchControl();
            copy.Merge<SearchControl>(searchcontrol);
            copy.input = new string[2];
            copy.date = new string[2];
            copy.number = new string[2];
            copy.matchtype = new int[2];
            copy.propertyList = new string[2];

            httpcontextbase.Session[sessionIdentifierSearchControl] = copy;
        }

        public static SearchControl GetState(HttpContextBase httpcontextbase)
        {
            if (httpcontextbase == null || httpcontextbase.Session == null) return default(SearchControl);

            return httpcontextbase.Session[sessionIdentifierSearchControl] as SearchControl;
        }
    }
}
