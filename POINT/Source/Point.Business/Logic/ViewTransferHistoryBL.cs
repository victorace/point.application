﻿using Point.Database.Models;
using Point.Database.Repository;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using System;
using Point.Database.Context;
using System.Threading.Tasks;

namespace Point.Business.Logic
{
    public class ViewTransferHistoryBL
    {

        public static int[] GetLastViewTransferHistory(IUnitOfWork uow, int employeeID, int maxRecords = 10)
        {
            var viewtransferhistory = uow.ViewTransferHistoryRepository.Get(vth => vth.EmployeeID == employeeID).OrderByDescending(vth => vth.ViewDateTime).Take(maxRecords).Select(vth => vth.TransferID);
            if (viewtransferhistory == null || !viewtransferhistory.Any())
            {
                return new int[] { };
            }
            return viewtransferhistory.ToArray();
        }

        public static void Save(int employeeID, int transferID)
        {
            Task.Run(() =>
            {
               using (var uow = new UnitOfWork<PointContext>())
               {
                    var previous = uow.ViewTransferHistoryRepository.Get(vth => vth.EmployeeID == employeeID && vth.TransferID == transferID);
                    if (previous.Any())
                    {
                        foreach (var viewtransferhistory in previous)
                        {
                            viewtransferhistory.ViewDateTime = DateTime.Now;
                        }
                    }
                    else
                    {

                        uow.ViewTransferHistoryRepository.Insert(new ViewTransferHistory()
                        {
                            EmployeeID = employeeID,
                            TransferID = transferID,
                            ViewDateTime = DateTime.Now,
                        });
                    }
                    uow.Save();
               }

           }).ConfigureAwait(false);
        }

    }
}