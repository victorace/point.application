﻿using Point.Database.Models;
using Point.Database.Repository;
using System.Linq;

namespace Point.Business.Logic
{
    public class DigitalSignatureBL
    {
        public static DigitalSignature GetByID(IUnitOfWork uow, int digitalsignatureid)
        {
            return uow.DigitalSignatureRepository
                .Get(dsi => dsi.DigitalSignatureID == digitalsignatureid && dsi.InActive == false)
                .OrderByDescending(dsi => dsi.DigitalSignatureID)
                .FirstOrDefault();
        }

        public static void Save(IUnitOfWork uow, Employee employee, BaseDatabaseImage databaseimage, LogEntry logentry)
        {
            if (employee == null) return;
            if (databaseimage == null) return;
            if (logentry == null) return;

            if (employee.DigitalSignatureID.HasValue)
            {
                var previousdigitalsignature = DigitalSignatureBL.GetByID(uow, employee.DigitalSignatureID.Value);
                LoggingBL.FillLogging(uow, previousdigitalsignature, logentry);
                uow.DigitalSignatureRepository.Delete(previousdigitalsignature);
            }

            var digitalsignature = new DigitalSignature()
            {
                ContentLength = databaseimage.ContentLength,
                ContentType = databaseimage.ContentType,
                FileData = databaseimage.FileData,
                FileName = databaseimage.FileName,
            };
            uow.DigitalSignatureRepository.Insert(digitalsignature);
            LoggingBL.FillLogging(uow, digitalsignature, logentry);

            employee.DigitalSignatureID = digitalsignature.DigitalSignatureID;

            uow.Save();
        }

        public static void Delete(IUnitOfWork uow, Employee employee, LogEntry logentry)
        {
            if (employee == null) return;
            if (!employee.DigitalSignatureID.HasValue) return;
            if (logentry == null) return;

            var digitalsignature = GetByID(uow, employee.DigitalSignatureID.Value);
            if (digitalsignature == null) return;

            employee.DigitalSignatureID = null;
            LoggingBL.FillLoggable(uow, employee, logentry);

            uow.DigitalSignatureRepository.Delete(digitalsignature.DigitalSignatureID);

            uow.Save();
        }
    }
}