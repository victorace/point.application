﻿using Point.Database.Models;
using Point.Database.Repository;
using System;

namespace Point.Business.Logic
{
    public static class PatientRequestTempBL
    {
        public static PatientRequestTemp Create(IUnitOfWork uow, int organizationid, string patientnumber)
        {
            var patientrequesttemp = new PatientRequestTemp()
            {
                Timestamp = DateTime.Now,
                GUID = Guid.NewGuid(),
                InterfaceCode = null,
                OrganizationID = organizationid,
                PatientNumber = patientnumber,
                Handled = false
            };

            uow.PatientRequestTempRepository.Insert(patientrequesttemp);
            uow.Save();

            return patientrequesttemp;
        }
    }
}
