﻿using Point.Business.Helper;
using Point.Database.Context;
using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Repository;
using Point.Infrastructure.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Point.Business.Logic
{
    public class CommunicationQueueBL
    {
        public static IEnumerable<CommunicationQueue> GetByTransferID(IUnitOfWork uow, int transferID)
        {
            return uow.CommunicationQueueRepository.Get(cq => cq.TransferID == transferID);
        }

        public static IEnumerable<CommunicationQueue> GetUnhandledOfTypes(IUnitOfWork uow, CommunicationLogType[] communicationLogTypes)
        {
            return uow.CommunicationQueueRepository.Get(cq => !cq.Handled && communicationLogTypes.ToList().Contains((CommunicationLogType)cq.MailType));
        }
        
        public static void SetHandledByFormSetVersionID(IUnitOfWork uow, int formSetVersionID)
        {
            var communicationqueues = uow.CommunicationQueueRepository.Get(cq => cq.FormSetVersionID == formSetVersionID);
            setHandled(uow, communicationqueues);
        }

        public static void SetHandledByTransferAttachmentID(IUnitOfWork uow, int transferAttachmentID)
        {
            var communicationqueues = uow.CommunicationQueueRepository.Get(cq => cq.TransferAttachmentID == transferAttachmentID);
            setHandled(uow, communicationqueues);
        }
        
        public static void SetHandledByDays(IUnitOfWork uow, int numberOfDays)
        {
            var beforedate = DateTime.Now.AddDays(-numberOfDays);
            var communicationqueues = uow.CommunicationQueueRepository.Get(cq => !cq.Handled && cq.Timestamp < beforedate);
            setHandled(uow, communicationqueues);
        }

        public static IEnumerable<CommunicationQueue> GetUnhandledByFormSetVersionID(IUnitOfWork uow, int formSetVersionID)
        {
            return uow.CommunicationQueueRepository.Get(cq => !cq.Handled && cq.FormSetVersionID.Value == formSetVersionID);
        }

        public static async System.Threading.Tasks.Task<CommunicationQueue> AddToQueueAsync(int formSetVersionID, int employeeID)
        {
            CommunicationQueue communicationqueue = null;
            using (var uow = new UnitOfWork<PointContext>())
            {
                var formsetVersion = FormSetVersionBL.GetByID(uow, formSetVersionID);
                if (formsetVersion.FormTypeID == (int)FlowFormType.VerpleegkundigeOverdrachtFlow)
                {
                    return null;
                }

                var phaseinstance = PhaseInstanceBL.GetByFormsetVersionID(uow, formSetVersionID);
                var receivingorganization = OrganizationHelper.GetReceivingOrganization(uow, phaseinstance.FlowInstanceID);
                if (OrganizationHelper.IsValidNedapOrganization(uow, receivingorganization, (FlowFormType)formsetVersion.FormTypeID))
                {
                    var flowinstance = FlowInstanceBL.GetByID(uow, phaseinstance.FlowInstanceID);
                    if (FlowInstanceBL.IsValidNedapDossierStatus(uow, flowinstance, PointUserInfoHelper.GetPointUserInfoByEmployeeID(uow, employeeID)))
                    {
                        var communicationqueues = GetUnhandledByFormSetVersionID(uow, formSetVersionID);
                        if (communicationqueues.Any())
                        {
                            foreach(var commqueue in communicationqueues.ToList())
                            {
                                commqueue.ReasonNotSend = $"{commqueue.ReasonNotSend} Updated {DateTime.Now.ToString("dd-MM HH:mm:ss")}.".Truncate(4000);
                                Save(uow, commqueue);
                            }
                            communicationqueue = communicationqueues.LastOrDefault();
                        }
                        else
                        {
                            var receivingdepartment = OrganizationHelper.GetRecievingDepartment(uow, phaseinstance.FlowInstanceID);
                            var transfer = TransferBL.GetByTransferID(uow, flowinstance.TransferID);
                            communicationqueue = new CommunicationQueue()
                            {
                                ClientID = transfer?.ClientID,
                                EmployeeID = employeeID,
                                FormSetVersionID = formSetVersionID,
                                Handled = false,
                                MailType = CommunicationLogType.NedapForm,
                                ReasonNotSend = $"Initialized by AddToQueueAsync on {DateTime.Now.ToString("dd-MM HH:mm:ss")}.",
                                ReceivingDepartmentID = receivingdepartment.DepartmentID,
                                ReceivingOrganizationID = receivingorganization.OrganizationID,
                                Timestamp = DateTime.Now,
                                TransferAttachmentID = null,
                                TransferID = transfer?.TransferID
                            };
                            Save(uow, communicationqueue);
                        }
                    }
                }
            }

            return await System.Threading.Tasks.Task.FromResult(communicationqueue);
        }

        
        private static void setHandled(IUnitOfWork uow, IQueryable<CommunicationQueue> communicationqueues)
        {
            if (communicationqueues != null)
            {
                foreach (var communicationqueue in communicationqueues.ToList())
                {
                    communicationqueue.Handled = true;
                }
                uow.Save();
            }

        }

        private static void Upsert(IUnitOfWork uow, CommunicationQueue communicationQueue)
        {
            if (communicationQueue.CommunicationQueueID == 0)
            {
                uow.CommunicationQueueRepository.Insert(communicationQueue);
            }
        }

        public static void Save(IUnitOfWork uow, CommunicationQueue communicationQueue)
        {
            Upsert(uow, communicationQueue);
            uow.Save();
        }
    }
}
