﻿using System.Collections.Generic;
using System.Linq;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;

namespace Point.Business.Logic
{
    public class ContactPersonViewBL
    {
        public static ContactPersonViewModel FromModel(IUnitOfWork uow, ContactPerson contactPerson)
        {
            if (contactPerson == null || contactPerson == default(ContactPerson))
                return new ContactPersonViewModel();

            var personData = uow.PersonDataRepository.Get(pd => pd.PersonDataID == contactPerson.PersonDataID).FirstOrDefault();
            var model = new ContactPersonViewModel()
            {
                ContactPersonID = contactPerson.ContactPersonID,
                PersonDataID = contactPerson.PersonDataID,
                ClientID = contactPerson.ClientID,
                Gender = personData.Gender,
                Initials = personData.Initials,
                FirstName = personData.FirstName,
                LastName = personData.LastName,
                PhoneNumberGeneral = personData.PhoneNumberGeneral,
                PhoneNumberMobile = personData.PhoneNumberMobile,
                PhoneNumberWork = personData.PhoneNumberWork,
                Email = personData.Email,
            };

            var cizRelation = uow.CIZRelationRepository.Get(cizr => cizr.CIZRelationID == contactPerson.CIZRelationID).FirstOrDefault();
            if (cizRelation != null) {
                model.CIZRelationID = cizRelation.CIZRelationID;
                model.CIZDescription = cizRelation.Description;
            }

            return model;
        }

        public static IEnumerable<ContactPersonViewModel> FromModelList(IUnitOfWork uow, IEnumerable<ContactPerson> contactPeople)
        {
            var model = new List<ContactPersonViewModel>();

            foreach (var contactPerson in contactPeople)
            {
                model.Add(ContactPersonViewBL.FromModel(uow, contactPerson));
            }

            return model;
        }
    }
}
