﻿using Point.Business.Helper;
using Point.Database.Repository;
using Point.Infrastructure.Attributes;
using Point.Infrastructure.Exceptions;
using Point.Infrastructure.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;

namespace Point.Business.Logic
{
    public static class SurveyBL
    {
        public const string URL_PARAM_CAMPAIGNID = "c";
        public const string URL_PARAM_TRANSFERID = "t";
        public const string URL_PARAM_DEPARTMENTID = "d";
        public const string URL_PARAM_EMAIL = "e";
        public const string URL_PARAM_ORGANIZATION = "o";
        public const string URL_PARAM_DIGEST = "h";

        private static HttpClient httpclient = new HttpClient();

        public static void SendSurveyInvite(IUnitOfWork uow, int transferid)
        {
            var flowinstance = FlowInstanceBL.GetByTransferID(uow, transferid);
            if (flowinstance == null)
            {
                throw new PointJustLogException($"No FlowInstance found attached to TransferID({transferid}).");
            }

            var recievingdepartment = OrganizationHelper.GetRecievingDepartment(uow, flowinstance.FlowInstanceID);
            if (recievingdepartment == null)
            {
                throw new PointJustLogException($"No recieving department found attached to FlowInstanceID({flowinstance.FlowInstanceID}).");
            }

            var recievingdepartmentemail = EmailBL.GetEmailAddress(recievingdepartment, EmailBL.EmailType.Email);
            if (string.IsNullOrEmpty(recievingdepartmentemail))
            {
                throw new PointJustLogException($"Department({recievingdepartment.DepartmentID}) has no valid email address.");
            }

            var sendingorganization = OrganizationHelper.GetSendingOrganization(uow, flowinstance.FlowInstanceID);
            if (sendingorganization == null)
            {
                throw new PointJustLogException($"No sending organization found attached to FlowInstanceID({flowinstance.FlowInstanceID}).");
            }

            var sendingdepartment = OrganizationHelper.GetSendingDepartment(uow, flowinstance.FlowInstanceID);
            if (sendingdepartment == null)
            {
                throw new PointJustLogException($"No sending department found attached to FlowInstanceID({flowinstance.FlowInstanceID}).");
            }

            var webserversurvey = ConfigHelper.GetAppSettingByName<string>("WebServerSurvey");
            if (string.IsNullOrEmpty(webserversurvey))
            {
                throw new PointJustLogException($"The configuration item 'WebServerSurvey' is missing / or invalid.");
            }

            var campaignid = OrganizationSettingBL.GetValue<string>(uow, sendingorganization.OrganizationID, OrganizationSettingBL.SurveyCampaignID);
            if (string.IsNullOrEmpty(campaignid))
            {
                throw new PointJustLogException($"No {OrganizationSettingBL.SurveyCampaignID} organization setting found for OrganizationID({sendingorganization.OrganizationID}).");
            }

            string result = "";
            string exceptionmessage = "";
            var uri = new Uri($"{webserversurvey.TrimEnd('/')}/Campaign/Start");
            var postdata = new List<KeyValuePair<string, string>>()
            {
                new KeyValuePair<string, string>(URL_PARAM_CAMPAIGNID, campaignid),
                new KeyValuePair<string, string>(URL_PARAM_TRANSFERID, transferid.ToString()),
                new KeyValuePair<string, string>(URL_PARAM_DEPARTMENTID, recievingdepartment.DepartmentID.ToString()),
                new KeyValuePair<string, string>(URL_PARAM_EMAIL, recievingdepartmentemail),
                new KeyValuePair<string, string>(URL_PARAM_ORGANIZATION, sendingdepartment?.FullName()),
                new KeyValuePair<string, string>(URL_PARAM_DIGEST, 
                    AntiFiddleInjection.CreateDigest($"{campaignid}|{transferid}|{recievingdepartment.DepartmentID}|{recievingdepartment.EmailAddress}|{sendingdepartment?.FullName()}"))
            };
            var formdata = new FormUrlEncodedContent(postdata);

            try
            {
                HttpResponseMessage response = httpclient.PostAsync(uri, formdata).Result;
                result = response.Content.ReadAsStringAsync().Result;
                if (!response.IsSuccessStatusCode)
                {
                    throw new HttpRequestException($"{response.ReasonPhrase}");
                }
            }
            catch (Exception e)
            {
                exceptionmessage = e.Message;
            }
            finally
            {
                new WebRequestLogBL(uow).LogWebRequestJson(new { url = uri.ToString(), data = postdata.ToDictionary(pair => pair.Key, pair => pair.Value)}, result, exceptionmessage);
            }
        }
    }
}
