﻿using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Point.Business.Logic
{
    public class OrganizationSearchBL
    {
        public static OrganizationSearch GetByDepartmentID(IUnitOfWork uow, int departmentid)
        {
            return uow.OrganizationSearchRepository.Get(it => it.DepartmentID == departmentid).FirstOrDefault();
        }

        public static IEnumerable<OrganizationSearch> GetByViewModel(IUnitOfWork uow, OrganizationSearchViewModel viewmodel, int[] favoritelocations, int regionid)
        {
            bool searchInRegion = viewmodel.SearchType == "OwnRegion";
            bool searchInPostalcode = viewmodel.SearchType == "Postcode" && string.IsNullOrEmpty(viewmodel.PostalCode) == false && viewmodel.PostalCode.Length >= 4;
            bool searchInRange = viewmodel.SearchType == "Range" && string.IsNullOrEmpty(viewmodel.PostalCodeRange) == false && viewmodel.PostalCodeRange.Length >= 4 && viewmodel.MaxRange.GetValueOrDefault(0) > 0;

            if (viewmodel.NaamPlaats == null) viewmodel.NaamPlaats = "";
            if (viewmodel.OrganizationIDs == null) viewmodel.OrganizationIDs = new int[] { };
            if (viewmodel.LocationIDs == null) viewmodel.LocationIDs = new int[] { };

            var searchlist = viewmodel.NaamPlaats.Split(' ').Where(it => !String.IsNullOrEmpty(it)).ToArray();
            
            if (searchInPostalcode)
            {
                viewmodel.PostalCode = viewmodel.PostalCode.Replace(" ", "");
            }

            string[] postcodes = new string[] { };
            if (searchInRange)
            {
                viewmodel.PostalCodeRange = viewmodel.PostalCodeRange.Replace(" ", "");
                var allpostcodes = GPSCoordinaatBL.GetPostcodesByStartAndRange(uow, viewmodel.PostalCodeRange, viewmodel.MaxRange.Value);
                postcodes = GPSCoordinaatBL.GetUniqueFirst4Chars(allpostcodes).ToArray();
            }

            viewmodel.OrganizationIDs = viewmodel.OrganizationIDs.Where(it => it != 0).ToArray();
            viewmodel.LocationIDs = viewmodel.LocationIDs.Where(it => it != 0).ToArray();

            var data = uow.OrganizationSearchRepository.Get(rm =>

                rm.OrganizationSearchParticipation.Any(p => (viewmodel.FlowDefinitionID == null || p.FlowDefinitionID == viewmodel.FlowDefinitionID) && (p.FlowDirection == FlowDirection.Any || p.FlowDirection == viewmodel.FlowDirection)) &&                
                (viewmodel.OrganizationTypeID == null || rm.OrganizationTypeID == (int?)viewmodel.OrganizationTypeID) &&
                (viewmodel.AfterCareCategory == null || rm.AfterCareCategory == viewmodel.AfterCareCategory) &&
                (viewmodel.RehabilitationCenterType == null || (viewmodel.RehabilitationCenterType == RehabilitationCenterType.RehabilitationCenter && rm.IsRehabilitationCenter) || (viewmodel.RehabilitationCenterType == RehabilitationCenterType.IntensiveRehabilitationNursinghome && rm.IsIntensiveRehabilitationNursingHome)) &&
                (viewmodel.OrganizationIDs.Count() == 0 || viewmodel.OrganizationIDs.Contains(rm.OrganizationID)) &&
                (viewmodel.LocationIDs.Count() == 0 || viewmodel.LocationIDs.Contains(rm.LocationID)) &&
                (searchlist.Count() == 0 || (searchlist.All(ns => rm.OrganizationName.ToLower().Contains(ns) || rm.LocationName.ToLower().Contains(ns) || rm.DepartmentName.ToLower().Contains(ns) || rm.AfterCareType.ToLower().Contains(ns) || rm.City.ToLower().Contains(ns)))) &&
                (!searchInPostalcode || rm.OrganizationSearchPostalCode.Any(pc => viewmodel.PostalCode.CompareTo(pc.StartPostalCode) >= 0 && viewmodel.PostalCode.CompareTo(pc.EndPostalCode) <= 0)) &&
                (!searchInRange || postcodes.Any(pcs => rm.LocationPostcode.StartsWith(pcs))) &&
                (!searchInRegion || rm.OrganizationSearchRegion.Any(r => r.RegionID == regionid) || favoritelocations.Contains(rm.LocationID))

                , includeProperties: "OrganizationSearchRegion,OrganizationSearchParticipation");

            return data;
        }


        //For management departmentsearch
        public static IEnumerable<OrganizationSearch> GetDepartments(IUnitOfWork uow, int? regionid, int? organizationTypeID, int[] organizationids, int[] locationids, int[] departmentids, string namecitySearch = "")
        {
            var searchlist = namecitySearch.Split(' ').Where(it => !String.IsNullOrEmpty(it)).ToArray();



            if (organizationids == null) organizationids = new int[0];
            if (locationids == null) locationids = new int[0];
            if (departmentids == null) departmentids = new int[0];

            var data = uow.OrganizationSearchRepository.Get(rm =>
                (regionid == null || rm.OrganizationSearchRegion.Any(r => r.RegionID == regionid.Value)) &&
                (organizationTypeID == null || rm.OrganizationTypeID == organizationTypeID) &&
                (organizationids.Count() == 0 || organizationids.Contains(rm.OrganizationID)) &&
                (locationids.Count() == 0 || locationids.Contains(rm.LocationID)) &&
                (departmentids.Count() == 0 || (departmentids.Contains(rm.DepartmentID))) &&
                (searchlist.Count() == 0 || (searchlist.All(ns => rm.OrganizationName.ToLower().Contains(ns) || rm.LocationName.ToLower().Contains(ns) || rm.DepartmentName.ToLower().Contains(ns) || rm.City.ToLower().Contains(ns))))
                , includeProperties: "OrganizationSearchRegion");

            return data;
        }

        public static IEnumerable<OrganizationSearch> GetOrganizationsByOrganizationTypeForSearch(
            IUnitOfWork uow, PointUserInfo userinfo, int? organizationTypeID, string namecitySearch = "",
            string regionSearch = "", int? userRegionID = null, string postcode = "")
        {
            if (namecitySearch == null) { namecitySearch = ""; }

            bool searchInRegion = regionSearch == RegionForSearch.OwnRegion.ToString() && userRegionID.HasValue;
            var searchlist = namecitySearch.Split(' ').Where(it => !String.IsNullOrEmpty(it)).ToArray();

            int[] favoriteids = PreferredLocationBL.GetByLocationID(uow, userinfo.Location.LocationID).Select(it => it.PreferredLocationID).ToArray();

            var data = uow.OrganizationSearchRepository.Get(rm =>
                    rm.OrganizationTypeID == organizationTypeID
                    && (!searchlist.Any() || searchlist.All(ns =>
                            rm.OrganizationName.ToLower().Contains(ns) || rm.LocationName.ToLower().Contains(ns) ||
                            rm.DepartmentName.ToLower().Contains(ns) || rm.City.ToLower().Contains(ns) ||
                            rm.AfterCareType.ToLower().Contains(ns) ||
                            rm.OrganizationSearchRegion.Any(r => r.RegionName.ToLower().Contains(ns))))
                    && (postcode.Length < 4 || rm.OrganizationSearchPostalCode.Any(pc =>
                            postcode.CompareTo(pc.StartPostalCode) >= 0 && postcode.CompareTo(pc.EndPostalCode) <= 0))
                    && (!searchInRegion || rm.OrganizationSearchRegion.Any(r => r.RegionID == userRegionID.Value) ||
                        favoriteids.Contains(rm.LocationID))
                , includeProperties: "OrganizationSearchRegion,OrganizationSearchParticipation");

            return data;
        }
    }
}
