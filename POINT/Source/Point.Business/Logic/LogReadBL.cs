﻿using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Extensions;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Point.Business.Logic
{
    public class LogReadBL        
    {
        
        public static IEnumerable<LogRead> GetByTransferID(IUnitOfWork uow, int transferID)
        {
            return uow.LogReadRepository.Get(lr => lr.TransferID == transferID);
        }

        public static void Save(IUnitOfWork uow, HttpRequestBase httpRequestBase, PointUserInfo pointUserInfo, int? transferID)
        {
            var logread = new LogRead();

            if (transferID > 0)
            {
                var transfer = TransferBL.GetCachedByTransferID(uow, (int)transferID);
                if (transfer != null)
                {
                    logread.TransferID = transferID;
                    logread.ClientID = transfer.ClientID;
                }
            }

            if (pointUserInfo != null && pointUserInfo.Employee != null)
            {
                logread.EmployeeID = pointUserInfo.Employee.EmployeeID;
                logread.RoleName = pointUserInfo.Employee.PointRole.ToString();
            }

            if (httpRequestBase?.Url != null)
            {
                logread.Url = httpRequestBase.Url.LocalPath;
                logread.QueryString = httpRequestBase.Url.Query;

                Screen screen = null;
                GeneralAction generalAction = null;

                var formtype = FormTypeBL.GetByURL(uow, httpRequestBase.Url);
                if (formtype != null)
                {
                    screen = ScreenBL.GetByFormTypeID(uow, formtype.FormTypeID);
                }
                else
                {
                    generalAction = GeneralActionBL.GetByURL(uow, httpRequestBase.Url);
                    if (generalAction != null)
                    {
                        screen = ScreenBL.GetByGeneralActionID(uow, generalAction.GeneralActionID);
                    }
                }

                if (screen == null && (formtype != null || generalAction != null))
                {
                    if (httpRequestBase.Url != null && !String.IsNullOrWhiteSpace(httpRequestBase.Url.AbsolutePath))
                    {                       
                        var pathParts = httpRequestBase.Url.AbsolutePath.Split('/')?.TakeLast<string>(2)?.Where(st => !string.IsNullOrEmpty(st)).ToList();
                        if (pathParts.Count() == 2)
                        {
                            var url = String.Concat(pathParts[0], "/", pathParts[1]);
                            var name = formtype?.Name ?? generalAction?.Name;

                            screen = ScreenBL.ToNewModel(uow, url, "5", name, formtype?.FormTypeID, generalAction?.GeneralActionID);
                            screen = ScreenBL.Save(uow, screen);
                        }
                    }
                }

                logread.ScreenID = screen?.ScreenID;
            }

            logread.Action = "R";
            logread.TimeStamp = DateTime.Now;

            uow.LogReadRepository.Insert(logread);
            uow.Save();
        }

        public static List<LogRead> SearchLogRead(IUnitOfWork uow, DateTime from, DateTime to, int transferId, List<int> clientIds, int employeeId)
        {
            var result = uow.LogReadRepository.Get(lr => DbFunctions.TruncateTime(lr.TimeStamp) >= from.Date && DbFunctions.TruncateTime(lr.TimeStamp) <= to.Date &&
                lr.TransferID.HasValue &&
                (transferId == 0 || lr.TransferID == transferId) &&
                (!clientIds.Any() || ( lr.ClientID != null && clientIds.Contains(lr.ClientID.Value))) &&
                (employeeId == 0 || lr.EmployeeID == employeeId));
            return result.ToList();

        }

    }
}
