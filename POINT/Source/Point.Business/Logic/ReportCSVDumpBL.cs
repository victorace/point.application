﻿using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Repository;
using Point.ServiceBus.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using Point.Infrastructure;
using Ionic.Zip;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Point.Business.Logic
{
    public class ReportCSVDumpBL
    {
        private const string separator = ";";

        public static GenericFile GetDumpReport(IUnitOfWork uow, int employeeId, int attachmentId)
        {
            var ta = TransferAttachmentBL.GetByID(uow, attachmentId);
            if (ta == null || ta.GenericFile == null || ta.Deleted || ta.EmployeeID != employeeId)
            {
                throw new Exception($"Dumpreport bestand niet gevonden.");
            }

            return ta.GenericFile;
        }
    }
}
