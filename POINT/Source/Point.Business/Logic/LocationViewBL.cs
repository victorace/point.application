﻿using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Exceptions;
using System.Collections.Generic;
using System.Linq;

namespace Point.Business.Logic
{
    public class LocationViewBL
    {
        public static void ValidateViewModel(IUnitOfWork uow, PointUserInfo userinfo, LocationViewModel viewmodel)
        {
            var myfunctionrolelevelid = userinfo.GetFunctionRoleLevelID(FunctionRoleTypeID.ManageOrganization);
            var currentmodel = LocationBL.GetByID(uow, viewmodel.LocationID);

            if (myfunctionrolelevelid <= FunctionRoleLevelID.Organization)
            {
                if (currentmodel?.OrganizationID != viewmodel.OrganizationID)
                {
                    var neworganization = OrganizationBL.GetByOrganizationID(uow, viewmodel.OrganizationID);
                    if (neworganization != null && userinfo.EmployeeOrganizationIDs.Contains(neworganization.OrganizationID) == false)
                    {
                        throw new PointSecurityException("U heeft onvoldoende machtigingen om deze actie uit te voeren", ExceptionType.AccessDenied);
                    }
                }
            }

            if (myfunctionrolelevelid == FunctionRoleLevelID.Region)
            {
                if (currentmodel?.OrganizationID != viewmodel.OrganizationID)
                {
                    var neworganization = OrganizationBL.GetByOrganizationID(uow, viewmodel.OrganizationID);
                    var newregionid = neworganization?.RegionID;
                    if (userinfo.Region.RegionID != newregionid)
                    {
                        throw new PointSecurityException("U heeft onvoldoende machtigingen om deze actie uit te voeren", ExceptionType.AccessDenied);
                    }
                }
            }
        }

        public static LocationViewModel FromModel(Location model, List<FlowDefinition> flowdefinitions, IList<Region> regions, FunctionRoleLevelID maxFunctionRoleLevelID, Role maxDossierLevel)
        {
            var viewModel = new LocationViewModel();
            viewModel.Assisting.SetMaximumFunctionRoleLevel(maxFunctionRoleLevelID);
            viewModel.FlowParticipation = FlowParticipationViewModel.FromModelList(model, flowdefinitions, maxFunctionRoleLevelID);

            if (model == null)
            {
                return viewModel;
            }

            viewModel.OrganizationTypeID = model.Organization?.OrganizationTypeID;
            viewModel.Assisting.OtherRegions = model.LocationRegion.Select(it => it.Region).ToList();
            viewModel.OtherRegionIDs = string.Join(",", viewModel.Assisting.OtherRegions.Select(otherRegion => otherRegion.RegionID.ToString()));
            viewModel.Assisting.RegionsMinusOtherRegions = regions?.Where(region => viewModel.Assisting.OtherRegions.All(o => o.RegionID != region.RegionID)).ToList();
            viewModel.Assisting.EnableOtherRegions = model.Organization?.OrganizationTypeID == (int) OrganizationTypeID.HealthCareProvider;
            viewModel.LocationID = model.LocationID;
            viewModel.OrganizationID = model.OrganizationID;
            viewModel.OrganizationName = model.Organization?.Name;
            viewModel.Name = model.Name;
            viewModel.StreetName = model.StreetName;
            viewModel.Number = model.Number;
            viewModel.PostalCode = model.PostalCode;
            viewModel.City = model.City;
            viewModel.Inactive = model.Inactive;
            viewModel.IPCheck = model.IPCheck;
            viewModel.IPAddress = model.IPAddress;
            viewModel.Area = model.Area;
            viewModel.OrganizationRegionName = model.Organization?.Region?.Name;
            viewModel.AutoCreateSets = AutoCreateSetBL.FromModelList(model.AutoCreateSet.ToList(), model, maxDossierLevel);

            return viewModel;
        }
    }
}