﻿using Point.Database.Models;
using Point.Database.Repository;
using System.Linq;

namespace Point.Business.Logic
{
    public class OrganizationLogoBL
    {
        public static OrganizationLogo GetByID(IUnitOfWork uow, int organizationLogoId)
        {
            return uow.OrganizationLogoRepository
                .Get(ol => ol.OrganizationLogoID == organizationLogoId && !ol.InActive)
                .FirstOrDefault();
        }

        public static void Save(IUnitOfWork uow, Organization organization, BaseDatabaseImage databaseimage, LogEntry logentry)
        {
            if (organization == null || databaseimage == null || logentry == null)
            {
                return;
            }

            if (organization.OrganizationLogoID.HasValue)
            {
                var previousLogo = OrganizationLogoBL.GetByID(uow, organization.OrganizationLogoID.Value);
                LoggingBL.FillLoggable(uow, previousLogo, logentry);
                uow.OrganizationLogoRepository.Delete(previousLogo);
            }

            var organizationLogo = new OrganizationLogo()
            {
                ContentLength = databaseimage.ContentLength,
                ContentType = databaseimage.ContentType,
                FileData = databaseimage.FileData,
                FileName = databaseimage.FileName,
            };
            uow.OrganizationLogoRepository.Insert(organizationLogo);
            LoggingBL.FillLoggable(uow, organizationLogo, logentry);

            organization.OrganizationLogoID = organizationLogo.OrganizationLogoID;
            uow.Save();
        }

        public static void Delete(IUnitOfWork uow, Organization organization, LogEntry logentry)
        {
            if (organization == null || !organization.OrganizationLogoID.HasValue || logentry == null)
            {
                return;
            }

            var organizationLogo = GetByID(uow, organization.OrganizationLogoID.Value);
            if (organizationLogo == null)
            {
                return;
            }

            organization.OrganizationLogoID = null;
            LoggingBL.FillLoggable(uow, organization, logentry);

            uow.OrganizationLogoRepository.Delete(organizationLogo.OrganizationLogoID);

            uow.Save();
        }


    }
}
