﻿using Point.Business.Helper;
using Point.Database.Extensions;
using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.ServiceBus.Models;
using System;
using Point.Infrastructure.Constants;

namespace Point.Business.Logic
{
    public class MedVRIBL
    {
        public static bool CheckMedvri(IUnitOfWork uow, int transferID)
        {
            var flowinstance = FlowInstanceBL.GetByTransferID(uow, transferID);

            if (flowinstance.FlowDefinitionID == FlowDefinitionID.HA_VVT) //Kan meerdere communicatielogs hebben en geen VO daarom apart
            {
                return true;
            }

            if (!CheckOrganizationSettings(uow, flowinstance))
            {
                return false;
            }

            if (flowinstance.FlowDefinitionID == FlowDefinitionID.RzTP)
            {
                if (CheckHaveLogsAndAccepted(uow, transferID, flowinstance.FlowInstanceID))
                { 
                    var vophaseinstance = PhaseInstanceBL.GetVoPhaseInstance(uow, flowinstance);
                    return vophaseinstance.IsDone();
                }
            }

            if (flowinstance.FlowDefinitionID == FlowDefinitionID.ZH_VVT || (flowinstance.FlowDefinitionID == FlowDefinitionID.CVA && FlowInstanceBL.IsOntslagbestemmingStandardRoute(uow, flowinstance)))
            {
                if (CheckHaveLogsAndAccepted(uow, transferID, flowinstance.FlowInstanceID))
                {
                    if ((FlowInstanceBL.HasDischargeRealizedDate(uow, flowinstance) || FlowInstanceBL.HasCareBeginDate(uow, flowinstance)) 
                        && !FlowInstanceBL.HasRegistrationEndedWithoutTransfer(uow, flowinstance.FlowInstanceID))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        private static bool CheckHaveLogsAndAccepted(IUnitOfWork uow, int transferID, int flowInstanceID)
        {
            var havecommunicationlogs = CommunicationLogBL.HaveLogByTransferIDAndMailTypes(uow, transferID, new[] { CommunicationLogType.ZorgmailMedvri, CommunicationLogType.ORUNoticeDoctor });
            if (!havecommunicationlogs && PatientHelper.IsAccepted(uow, flowInstanceID) ==  AcceptanceState.Acknowledged)
            {
                return true;
            }
            return false;
        }

        private static bool CheckOrganizationSettings(IUnitOfWork uow, FlowInstance flowInstance)
        {
            var startedbyorganization = flowInstance?.StartedByOrganization;
            if (startedbyorganization == null)
            {
                return false;
            }

            if (OrganizationUsesORU(uow, startedbyorganization))
            {
                return true;
            }

            if (String.IsNullOrEmpty(flowInstance.StartedByOrganization?.ZorgmailUsername) || String.IsNullOrEmpty(flowInstance.StartedByOrganization?.ZorgmailPassword))
            {
                return false;
            }

            if (string.IsNullOrWhiteSpace(flowInstance?.Transfer?.Client?.AddressGPForZorgmail)) 
            {
                return false;
            }

            return true;
        }

        public static bool OrganizationUsesORU(IUnitOfWork uow, Organization organization)
        {
            if (organization == null)
            {
                return false;
            }

            var organizationsettingoru = OrganizationSettingBL.GetValue<bool>(uow, organization.OrganizationID, OrganizationSettingBL.ORUHuisartsBericht);
            var organizationsettinginterface = OrganizationSettingBL.GetValue<string>(uow, organization.OrganizationID, OrganizationSettingBL.InterfaceCode);
            if (organizationsettingoru && !string.IsNullOrEmpty(organizationsettinginterface))
            {
                return true;
            }

            return false;
        }

        public static void SendMedvriHAVVT(IUnitOfWork uow, FlowInstance flowInstance, FlowFormType flowFormType, PointUserInfo pointUserInfo, TemplateTypeID templateTypeID, Department receivingDepartment)
        {
            if (!CheckMedvri(uow, flowInstance.TransferID))
            {
                return;
            }

            if(receivingDepartment?.Location?.OrganizationID == null)
            {
                return;
            }


            var timestamp = DateTime.Now;
            ServiceBusBL.SendMessage(uow, MessageQueues.PhaseQueue, "SendMedvri", new SendMedvri
            {
                TimeStamp = timestamp,
                UniqueID = timestamp.Ticks,
                TransferID = flowInstance.TransferID,
                ClientID = flowInstance.Transfer.ClientID,
                SendingOrganizationID = receivingDepartment.Location.OrganizationID,
                SendingDepartmentID = receivingDepartment.DepartmentID,
                TemplateTypeID = templateTypeID,
                SendingEmployeeID = pointUserInfo.Employee.EmployeeID,
                ScreenID = (int)flowFormType
            });
        }

        public static void SendMedvri(IUnitOfWork uow, FlowInstance flowInstance, FlowFormType flowFormType, PointUserInfo pointUserInfo, TemplateTypeID templateTypeID = TemplateTypeID.ZorgMailZHVVTJa)
        {
            if (!CheckMedvri(uow, flowInstance.TransferID))
            {
                return;
            }

            var sendingDepartment = OrganizationHelper.GetSendingDepartment(uow, flowInstance.FlowInstanceID);
            if (sendingDepartment == null)
            {
                return;
            }

            var sendingorganization = sendingDepartment?.Location?.Organization;
            if (sendingorganization == null)
            {
                return;
            }

            Employee sendingemployee = OrganizationHelper.GetSendingEmployee(uow, flowInstance.FlowInstanceID);
            if (sendingemployee == null)
            {
                return;
            }

            //prefer the logged-in user only if the logged-in user if from the same organization
            if (sendingemployee.DefaultDepartment?.Location?.OrganizationID == pointUserInfo?.Organization?.OrganizationID)
            {
                sendingemployee = EmployeeBL.GetByID(uow, pointUserInfo.Employee.EmployeeID);
            }

            var timestamp = DateTime.Now;
            ServiceBusBL.SendMessage(uow, MessageQueues.PhaseQueue, "SendMedvri", new SendMedvri
            {
                TimeStamp = timestamp,
                UniqueID = timestamp.Ticks,
                TransferID = flowInstance.TransferID,
                ClientID = flowInstance.Transfer.ClientID,
                SendingOrganizationID = sendingorganization.OrganizationID,
                SendingDepartmentID = sendingDepartment.DepartmentID,
                TemplateTypeID = templateTypeID,
                SendingEmployeeID = sendingemployee.EmployeeID,
                ScreenID = (int)flowFormType
            });
        }
    }
}
