﻿using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Exceptions;
using Point.Infrastructure.Extensions;
using System.Collections.Generic;
using System.Linq;

namespace Point.Business.Logic
{
    public class DepartmentViewBL
    {
        public static void ValidateViewModel(IUnitOfWork uow, PointUserInfo userinfo, DepartmentViewModel viewmodel)
        {
            var myfunctionrolelevelid = userinfo.GetFunctionRoleLevelID(FunctionRoleTypeID.ManageOrganization);
            var currentmodel = DepartmentBL.GetByDepartmentID(uow, viewmodel.DepartmentID);

            if (myfunctionrolelevelid <= FunctionRoleLevelID.Location)
            {
                if (currentmodel?.LocationID != viewmodel.LocationID)
                {
                    if (userinfo.EmployeeLocationIDs.Contains(viewmodel.LocationID) == false)
                    {
                        throw new PointSecurityException("U heeft onvoldoende machtigingen om deze actie uit te voeren", ExceptionType.AccessDenied);
                    }
                }
            }

            if (myfunctionrolelevelid == FunctionRoleLevelID.Organization)
            {
                if (currentmodel?.LocationID != viewmodel.LocationID)
                {
                    var newlocation = LocationBL.GetByID(uow, viewmodel.LocationID);
                    if (newlocation != null && userinfo.EmployeeOrganizationIDs.Contains(newlocation.OrganizationID) == false)
                    {
                        throw new PointSecurityException("U heeft onvoldoende machtigingen om deze actie uit te voeren", ExceptionType.AccessDenied);
                    }
                }
            }

            if (myfunctionrolelevelid == FunctionRoleLevelID.Region)
            {
                if (currentmodel?.LocationID != viewmodel.LocationID)
                {
                    var newlocation = LocationBL.GetByID(uow, viewmodel.LocationID);
                    var newregionid = newlocation?.Organization?.RegionID;
                    if (userinfo.Region.RegionID != newregionid)
                    {
                        throw new PointSecurityException("U heeft onvoldoende machtigingen om deze actie uit te voeren", ExceptionType.AccessDenied);
                    }
                }
            }
        }

        public static DepartmentViewModel FromModel(Department model, List<FlowDefinition> flowdefinitions, List<AutoCreateSet> locationautocreatesets)
        {
            if (model == null)
                return new DepartmentViewModel() { FlowParticipation = FlowParticipationViewModel.FromModelList(model, flowdefinitions) };

            return new DepartmentViewModel()
            {
                DepartmentID = model.DepartmentID,
                Name = model.Name,
                OrganizationID = model.Location.OrganizationID,
                LocationID = model.LocationID,
                Location = model.Location,
                Organization = model.Location?.Organization,
                DepartmentTypeID = model.DepartmentTypeID.SafeParse<DepartmentTypeID>(),
                AfterCareType1ID = model.AfterCareType1ID.GetValueOrDefault(),
                AfterCareType1 = model.AfterCareType1,
                PhoneNumber = model.PhoneNumber,
                FaxNumber = model.FaxNumber,
                EmailAddress = model.EmailAddress,
                RecieveEmail = model.RecieveEmail,
                RehabilitationCenter = model.RehabilitationCenter,
                IntensiveRehabilitationNursinghome = model.IntensiveRehabilitationNursinghome,
                EmailAddressTransferSend = model.EmailAddressTransferSend,
                RecieveEmailTransferSend = model.RecieveEmailTransferSend,
                ZorgmailTransferSend = model.ZorgmailTransferSend,
                RecieveZorgmailTransferSend = model.RecieveZorgmailTransferSend,
                RecieveCDATransferSend = model.RecieveCDATransferSend,
                CapacityFunctionality = model.CapacityFunctionality,
                Inactive = model.Inactive,
                EnableServiceAreas = model.Location?.Organization?.OrganizationTypeID != (int)OrganizationTypeID.Hospital,
                AddressGPForZorgmail = model.AddressGPForZorgmail,
                AGB = model.AGB,
                SortOrder = model.SortOrder,
                ExternID = model.ExternID,
                AutoCreateSetIDs = model.AutoCreateSetDepartment.Select(it => it.AutoCreateSetID).ToList(),
                LocationAutoCreateSets = locationautocreatesets,
                FlowParticipation = FlowParticipationViewModel.FromModelList(model, flowdefinitions)
            };
        }
    }
}