﻿using Point.Database.Context;
using Point.Database.Repository;
using Point.Models.Enums;
using System.Collections.Generic;
using System.Linq;
using static Point.Business.Helper.CSVHelper;

namespace Point.Business.Logic
{
    public class OrganizationCSVTemplateBL
    {
        public static List<Column> GetTemplatedColumns(List<Column> columns, int organizationID, CSVTemplateTypeID csvTemplateTypeID)
        {
            using (var uow = new UnitOfWork<PointContext>())
            {
                var organizationColumns = uow.OrganizationCSVTemplateColumnRepository.Get(cols => 
                    cols.OrganizationCSVTemplate.CSVTemplateTypeID == csvTemplateTypeID && 
                    cols.OrganizationCSVTemplate.OrganizationID == organizationID).ToList();

                if(organizationColumns.Any())
                {
                    return organizationColumns.Select(col => 
                        new Column()
                        {
                            Position = col.Position,
                            PropertyInfo = columns.FirstOrDefault(c => c.PropertyInfo.Name == col.ColumnSource)?.PropertyInfo
                        }).ToList();
                }

                return columns;
            }
        }
    }
}
