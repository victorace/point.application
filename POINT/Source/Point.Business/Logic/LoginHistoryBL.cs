﻿using System;
using Point.Database.Models;
using Point.Database.Repository;
using System.Web;
using Point.Database.Models.ViewModels;
using Point.Infrastructure.Extensions;
using Point.Database.Extensions;

namespace Point.Business.Logic
{
    public static class LoginHistoryBL
    {
        public static void InsertLoginHistory(IUnitOfWork uow, LoginViewModel loginViewModel, HttpRequestBase httpRequest, bool validUser, PointUserInfo pointUserInfo = null)
        {
            var loginhistory = new LoginHistory()
            {
                ClientIP = httpRequest.ServerVariables["LOCAL_ADDR"].Truncate(50),
                LocalAddress = System.Environment.MachineName.Truncate(50),
                LoginBrowser = getBrowserCaps(httpRequest.Browser), 
                LoginDateTime = DateTime.Now,
                LoginFailed = !validUser,
                LoginInfo = httpRequest.UserAgent,
                LoginIP = httpRequest.UserHostAddress.Truncate(50),
                LoginOrganizationName = (pointUserInfo == null ? "" : pointUserInfo.Organization.Name).Truncate(50),
                LoginResolution = loginViewModel.Resolution.Truncate(50),
                LoginUsername = getLoginUserName(loginViewModel.Username, pointUserInfo)
            };

            uow.LoginHistoryRepository.Insert(loginhistory);
            uow.Save();

        }

        public static string getLoginUserName(string userName, PointUserInfo pointUserInfo = null)
        {
            return String.Format("{0} ({1})", (pointUserInfo == null ? "" : pointUserInfo.Employee.FullName()), userName).Trim().Truncate(50);
        }

        private static string getBrowserCaps(HttpBrowserCapabilitiesBase browser)
        {
            //Can also be determined by analysing the UserAgent (https://browscap.org/) Keeping this format for backward-comp. and performance:

            return String.Format("ActiveX: {0}; Cookies: {1}; VBScript: {2}; ECMAScript: {3}", browser.ActiveXControls, browser.Cookies, browser.VBScript, browser.EcmaScriptVersion);
        }

    }
}
