﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using Point.Database.Models;
using Point.Database.Repository;
using System.Threading.Tasks;
using Point.Database.Context;
using Point.Infrastructure.Constants;

namespace Point.Business.Logic
{
    public static class DepartmentCapacityPublicBL
    {
        public static IQueryable<DepartmentCapacityPublic> GetQuery(IUnitOfWork uow)
        {
            return uow.DepartmentCapacityPublicRepository.Get();
        }

        // Organization, Location, Department should be active and Capacity1 > 0
        public static IQueryable<DepartmentCapacityPublic> GetAllActive(IUnitOfWork uow, bool publicVersion)
        {
            return (from dp in uow.DepartmentCapacityPublicRepository.Get()
                join r in uow.RegionRepository.Get() on dp.RegionID equals r.RegionID
                join o in uow.OrganizationRepository.Get() on dp.OrganizationID equals o.OrganizationID
                join l in uow.LocationRepository.Get() on dp.LocationID equals l.LocationID
                where  dp.CapacityFunctionality && (publicVersion ? r.CapacityBedsPublic : r.CapacityBeds) && !o.Inactive && !l.Inactive && dp.IsActive && (dp.Capacity1 > 0)
                select dp);
        }


        public static IQueryable<DepartmentCapacityPublic> GetAllActiveForRegionIDs(IUnitOfWork uow, bool publicVersion, IEnumerable<int> regionIDs)
        {
            IQueryable<RegionAfterCareType> regionAfterCareTypes = null;
            if (publicVersion)
            {
                regionAfterCareTypes = uow.RegionAfterCareTypeRepository.GetAll();

                if (regionAfterCareTypes == null || !regionAfterCareTypes.Any())
                {
                    return new List<DepartmentCapacityPublic>().AsQueryable();
                }
            }

            var departmentCapacityPublics = GetAll(uow);

            if (regionIDs != null && regionIDs.Any())
            {
                departmentCapacityPublics = departmentCapacityPublics.Where(dc => (regionIDs.Contains(dc.RegionID) || (publicVersion &&  regionIDs.Any(selectedreg => dc.Location.LocationRegion.Select(ar => ar.RegionID).Contains(selectedreg)))));
            }

            if (publicVersion)
            {
                departmentCapacityPublics = departmentCapacityPublics.Where(dc => (regionAfterCareTypes.Any(reg => (reg.RegionID == dc.RegionID && reg.AfterCareTypeID == dc.AfterCareTypeID) ||
                                                                                                               (dc.Location.LocationRegion.Any(ar => ar.Region.CapacityBeds && regionAfterCareTypes.Any(reg2 => ar.RegionID == reg2.RegionID && dc.AfterCareTypeID == reg2.AfterCareTypeID))))));
            }

            return departmentCapacityPublics;
        }

        public static IQueryable<DepartmentCapacityPublic> GetAll(IUnitOfWork uow)
        {
            return uow.DepartmentCapacityPublicRepository.GetAll().Where(d => d.CapacityFunctionality && d.IsActive);
        }

        //async Task
        public static bool UpsertByDepartment(IUnitOfWork uow, Department department, string username, bool doSave = true, DateTime? modifiedOn = null, bool updateCapacityFieldsOnly = false)
        {
            if (department == null || department.DepartmentID == 0)
            {
                return false;
            }

            var isActive = !department.Inactive && !(department.Location?.Organization?.Inactive ?? false) && !(department.Location?.Inactive ?? false);
            var capacityFunctionality = department.CapacityFunctionality && ((department.Location?.Organization?.Region?.CapacityBeds ?? false) || (department.Location?.Organization?.Region?.CapacityBedsPublic ?? false));
            var isNew = false;
            var datetime = modifiedOn ?? DateTime.Now;
            var departmentCapacityPublic = uow.DepartmentCapacityPublicRepository.Get(x => x.DepartmentID == department.DepartmentID).FirstOrDefault();
            if (departmentCapacityPublic == null)
            {
                if (!isActive || !capacityFunctionality)
                {
                    return true;
                }

                departmentCapacityPublic = new DepartmentCapacityPublic
                {
                    DepartmentID = department.DepartmentID
                };
                isNew = true;
            }

            if (isNew || !updateCapacityFieldsOnly)
            {
                departmentCapacityPublic.AfterCareTypeID = department.AfterCareType1ID;
                departmentCapacityPublic.AfterCareTypeName = department.AfterCareType1?.Name;
                departmentCapacityPublic.AfterCareGroupID = department.AfterCareType1?.AfterCareGroupID;
                departmentCapacityPublic.RegionID = department.Location.Organization.RegionID.HasValue ? department.Location.Organization.RegionID.Value : 0;
                departmentCapacityPublic.RegionName = department.Location?.Organization?.Region?.Name;
                departmentCapacityPublic.OrganizationID = department.Location.OrganizationID;
                departmentCapacityPublic.OrganizationName = department.Location?.Organization.Name;
                departmentCapacityPublic.OrganizationTypeID = department.Location.Organization.OrganizationTypeID;
                departmentCapacityPublic.LocationID = department.LocationID;

                UpdateLocation(uow, department.Location, departmentCapacityPublic);
                departmentCapacityPublic.DepartmentID = department.DepartmentID;
                UpdateDepartment(department, departmentCapacityPublic);
                departmentCapacityPublic.IsActive = isActive;

                departmentCapacityPublic.Information = department.Information;
                departmentCapacityPublic.AdjustmentCapacityDate = department.AdjustmentCapacityDate; // datetime;
                departmentCapacityPublic.ModifiedOn = datetime;
                departmentCapacityPublic.ModifiedBy = username;
                departmentCapacityPublic.CapacityFunctionality = capacityFunctionality;
            }
            
            departmentCapacityPublic.Capacity1 = department.CapacityDepartment;
            departmentCapacityPublic.Capacity2 = department.CapacityDepartmentNext;
            departmentCapacityPublic.Capacity3 = department.CapacityDepartment3;
            departmentCapacityPublic.Capacity4 = department.CapacityDepartment4;
            
            if (isNew)
            {
                uow.DepartmentCapacityPublicRepository.Insert(departmentCapacityPublic);
            }

            if (doSave)
            {
                uow.Save();
            }
            else
            {
                uow.DatabaseContext.Entry(departmentCapacityPublic).State = EntityState.Modified;
            }

            return true;
        }

        // useStartDate false processes ALL
        public static string UpsertByDepartmentAll(IUnitOfWork uow, string userName, DateTime? startDateTime)
        {
            var countUpdatesActive = 0;
            var countUpdatesInactive = 0;
            var now = DateTime.Now;

            // First the actual active capacity
            var departments = DepartmentBL.GetAllWithActiveCapacity(uow, true);

            if (startDateTime.HasValue)
            {
                departments = departments.Where(x => x.AdjustmentCapacityDate != null || x.AdjustmentCapacityDate > startDateTime);
            }

            var departmentIDsFailed = new List<int>();
            var errors = new StringBuilder();

            bool success;

            foreach (var d in departments)
            {
                success = false;
                try
                {
                    success = UpsertByDepartment(uow, d, userName, false, now);

                    if (success)
                    {
                        countUpdatesActive++;
                    }
                }
                catch (Exception ex)
                {
                    errors.AppendLine($"DepartmentCapacityPublic: Upsert - DepartmentID: {d.DepartmentID} - Error: {ex.Message}");
                }

                if (!success)
                {
                    departmentIDsFailed.Add(d.DepartmentID);
                }
            }

            if (countUpdatesActive > 0)
            {
                try
                {
                    uow.Save();
                }
                catch (Exception ex)
                {
                    errors.AppendLine($"DepartmentCapacityPublic: Saving ACTIVE - Error: {ex.Message}");
                }
            }

            // Records which were not adjusted yet
            var departmentCapacityPublics = GetAll(uow).Where(x => DbFunctions.DiffSeconds(x.ModifiedOn, now) > 0 && x.IsActive);

            if (startDateTime.HasValue)
            {
                departmentCapacityPublics = departmentCapacityPublics.Where(x => x.AdjustmentCapacityDate > startDateTime);
            }

            if (departmentIDsFailed.Any())
            {
                departmentCapacityPublics = departmentCapacityPublics.Where(x => !departmentIDsFailed.Contains(x.DepartmentID));
            }

            foreach (var dp in departmentCapacityPublics)
            {
                dp.IsActive = false;
                dp.ModifiedOn = now;
                countUpdatesInactive++;
            }

            if (countUpdatesInactive > 0)
            {
                try
                {
                    uow.Save();
                }
                catch (Exception ex)
                {
                    errors.AppendLine($"DepartmentCapacityPublic: Saving INACTIVE - Error: {ex.Message}");
                }
            }
            // 
            var hasErrors = errors.Length > 0;
            var duration = DateTime.Now.Subtract(now);
            var message = (!hasErrors ? TaskReturnValues.Success : TaskReturnValues.Failed) + $"- Updated: [Active {countUpdatesActive}, Inactive {countUpdatesInactive}] - Duration: {duration.Hours}:{duration.Minutes}:{duration.Seconds}";
            if (hasErrors)
            {
                message += Environment.NewLine + "+++ ERRORS" + errors;
            }

            return message;
        }

        public static async Task<bool> UpdateOnDepartmentChange(Department department)
        {
            if (department.DepartmentID == 0)
            {
                return false;
            }

            using (var uow = new UnitOfWork<PointContext>())
            {
                var departmentCapacityPublic = await uow.DepartmentCapacityPublicRepository.Get(x => x.DepartmentID == department.DepartmentID).ToListAsync();

                if (!departmentCapacityPublic.Any()) return true;

                foreach (var dep in departmentCapacityPublic)
                {
                    UpdateDepartment(department, dep);
                }

                uow.Save();
            }
            return true;
        }

        public static async Task<bool> UpdateOnLocationChange(Location location)
        {
            if (location.LocationID == 0)
            {
                return false;
            }

            using (var uow = new UnitOfWork<PointContext>())
            {
                var departmentCapacityPublic = await uow.DepartmentCapacityPublicRepository.Get(x => x.LocationID == location.LocationID).ToListAsync();

                if (!departmentCapacityPublic.Any()) return true;

                foreach (var dp in departmentCapacityPublic)
                {
                    UpdateLocation(uow, location, dp);
                }

                uow.Save();
            }

            return true;
        }

        public static async Task<bool> UpdateOnOrganizationChange(Organization organization)
        {
            if (organization.OrganizationID == 0)
            {
                return false;
            }

            using (var uow = new UnitOfWork<PointContext>())
            {
                var departmentCapacityPublic = await uow.DepartmentCapacityPublicRepository.Get(x => x.OrganizationID == organization.OrganizationID).ToListAsync();

                if (!departmentCapacityPublic.Any()) return true;

                foreach (var dp in departmentCapacityPublic)
                {
                    dp.IsActive = !organization.Inactive;
                    dp.OrganizationName = organization.Name;
                }

                uow.Save();
            }
            
            return true;
        }

        public static async Task<bool> UpdateOnRegionChange(Region region, bool deleted = false)
        {
            if (region.RegionID == 0)
            {
                return false;
            }

            var active = !deleted && (region.CapacityBeds || region.CapacityBedsPublic);

            using (var uow = new UnitOfWork<PointContext>())
            {
                var departmentCapacityPublic = await uow.DepartmentCapacityPublicRepository.Get(x => x.RegionID == region.RegionID).ToListAsync();

                if (!departmentCapacityPublic.Any()) return true;

                foreach (var dp in departmentCapacityPublic)
                {
                    dp.IsActive = active;
                    dp.RegionName = region.Name;
                }

                uow.Save();
            }

            return true;
        }

        private static void UpdateLocation(IUnitOfWork uow, Location location, DepartmentCapacityPublic dep)
        {
            if (location == null || dep == null) return;

            dep.IsActive = !location.Inactive;
            dep.LocationNumber = location.Number;
            dep.LocationPostalCode = location.PostalCode;
            dep.LocationStreet = location.StreetName;
            dep.LocationName = location.Name;
            dep.LocationCity = location.City;
            var startCoordinate = GPSCoordinaatBL.GetStartCoordinate(uow, location.PostalCode);
            dep.Latitude = startCoordinate.Latitude;
            dep.Longitude = startCoordinate.Longitude;
        }

        private static void UpdateDepartment(Department department, DepartmentCapacityPublic dep)
        {
            if (department == null && dep == null) return;

            dep.IsActive = !department.Inactive;
            dep.DepartmentEmailAddress = department.EmailAddress;
            dep.DepartmentFaxNumber = department.FaxNumber;
            dep.DepartmentName = department.Name;
            dep.DepartmentPhoneNumber = department.PhoneNumber;
        }

    }
}
      