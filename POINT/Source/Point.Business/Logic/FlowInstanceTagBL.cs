﻿using Point.Database.Context;
using Point.Database.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Point.Business.Logic
{
    public class FlowInstanceTagBL
    {
        public static string[] GetAllUsedTags(int[] organizationIDs)
        {
            using (var uow = new UnitOfWork<PointContext>())
            {
                return uow.FlowInstanceTagRepository.Get(f => organizationIDs.Contains((int)f.FlowInstance.StartedByOrganizationID)).Select(f => f.Tag).Distinct().ToArray();
            }
        }

        public static string[] GetAllByTransferID(int transferID)
        {
            using (var uow = new UnitOfWork<PointContext>())
            {
                var flowInstanceId = uow.FlowInstanceRepository.FirstOrDefault(it => it.TransferID == transferID).FlowInstanceID;
                return uow.FlowInstanceTagRepository.Get(it => it.FlowInstanceID == flowInstanceId).Select(it => it.Tag).ToArray();
            }
        }

        public static void AddDossierTag(int transferID, string tag)
        {
            using (var uow = new UnitOfWork<PointContext>())
            {
                var flowInstanceId = uow.FlowInstanceRepository.FirstOrDefault(it => it.TransferID == transferID).FlowInstanceID;
                uow.FlowInstanceTagRepository.Insert(new Database.Models.FlowInstanceTag() {
                    FlowInstanceID = flowInstanceId,
                    Tag = tag
                });

                uow.Save();
            }
        }

        public static void DeleteDossierTag(int transferID, string tag)
        {
            using (var uow = new UnitOfWork<PointContext>())
            {
                var flowInstanceId = uow.FlowInstanceRepository.FirstOrDefault(it => it.TransferID == transferID).FlowInstanceID;
                uow.FlowInstanceTagRepository.Delete(fit => fit.FlowInstanceID == flowInstanceId && fit.Tag == tag);
                uow.Save();
            }
        }

        public static void CopyToFlowInstance(IUnitOfWork uow, int sourceFlowInsanceID, int targetFlowInstanceID)
        {
            var sourcetags = uow.FlowInstanceTagRepository.Get(it => it.FlowInstanceID == sourceFlowInsanceID).Select(it => it.Tag).ToArray();
            foreach (var tag in sourcetags)
            {
                uow.FlowInstanceTagRepository.Insert(new Database.Models.FlowInstanceTag() {
                    FlowInstanceID = targetFlowInstanceID,
                    Tag = tag
                });
            }

            uow.Save();
        }
    }
}
