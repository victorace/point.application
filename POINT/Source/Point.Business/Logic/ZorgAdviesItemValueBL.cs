﻿using System;
using System.Collections.Generic;
using System.Linq;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;

namespace Point.Business.Logic
{
    public class ZorgAdviesItemValueBL
    {
        public static IEnumerable<ZorgAdviesItemValue> GetByFlowInstanceID(IUnitOfWork uow, int flowinstanceid)
        {
            return uow.ZorgAdviesItemValueRepository.Get(zpi => zpi.FlowInstanceID == flowinstanceid);
        }

        public static void SaveByViewModelAndFlowInstanceID(IUnitOfWork uow, IEnumerable<ZorgAdviesItemViewModel> zorgadviesitems, int flowinstanceid)
        {
            var currentitems = GetByFlowInstanceID(uow, flowinstanceid);
            foreach (var item in zorgadviesitems)
            {
                var currentitem = currentitems.FirstOrDefault(ci => ci.ZorgAdviesItemValueID == item.ZorgAdviesItemValueID);
                if (currentitem == null)
                {
                    currentitem = new ZorgAdviesItemValue();
                    currentitem.ZorgAdviesItemID = item.ZorgAdviesItemID;
                    currentitem.FlowInstanceID = flowinstanceid;

                    uow.ZorgAdviesItemValueRepository.Insert(currentitem);
                }

                if (item.ActionBy != null)
                    currentitem.ActionBy = String.Join(",", item.ActionBy.Select(it => (int)it));
                else
                    currentitem.ActionBy = null;

                currentitem.ActionFrequency = item.ActionFrequency;
                currentitem.ActionTime = item.ActionTime;
                currentitem.ActionTimespan = (int?)item.ActionTimespan;
                currentitem.Text = item.Text;
            }

            uow.Save();
        }

        public static IEnumerable<ZorgAdviesItemValue> CopyTo(IUnitOfWork uow, FlowInstance source, FlowInstance destination)
        {
            List<ZorgAdviesItemValue> zorgadviseitemvalues = new List<ZorgAdviesItemValue>();

            var sourcezorgadviseitemvalues = uow.ZorgAdviesItemValueRepository.Get(zpi => zpi.FlowInstanceID == source.FlowInstanceID, asNoTracking: true);
            foreach (var zorgadviceitemvalue in sourcezorgadviseitemvalues)
            {

                zorgadviceitemvalue.ZorgAdviesItemValueID = 0;
                zorgadviceitemvalue.FlowInstanceID = destination.FlowInstanceID;
                uow.ZorgAdviesItemValueRepository.Insert(zorgadviceitemvalue);

                zorgadviseitemvalues.Add(zorgadviceitemvalue);
            }

            if (zorgadviseitemvalues.Any())
                uow.Save();

            return zorgadviseitemvalues;
        }
    }
}
