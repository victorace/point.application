﻿using Point.Database.CacheModels;
using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using System.Collections.Generic;
using System.Linq;

namespace Point.Business.Logic
{
    public class MenuBL
    {
        public static MenuItemStatus GetMenuItemStatus(IUnitOfWork uow, PhaseDefinitionCache phasedefinition, 
            List<PhaseInstance> activephaseinstances, PointUserInfo pointuserinfo, MenuItemDestination menuitemdestination)
        {
            var phaseRights = PhaseDefinitionRightsBL.GetFormRightsPhaseDefinitionID(uow, pointuserinfo, phasedefinition, null);

            var attachedphaseinstances = activephaseinstances.Where(it => it.PhaseDefinitionID == phasedefinition.PhaseDefinitionID).ToList();
            
            //Invisible when flow is not in that phase yet
            if (phaseRights >= Rights.W && phasedefinition.Phase > -1 && attachedphaseinstances.Count == 0)
            {
                return MenuItemStatus.Invisible;
            }

            //invisible als er geen formsetversion is en je enkel readonly rechten hebt
            if (phaseRights <= Rights.R && phasedefinition.ShowReadOnlyAndEmpty == false && 
                !attachedphaseinstances.Any(it => it.FormSetVersion.Any(fs => !(bool)it.Cancelled)))
            {
                return MenuItemStatus.Invisible;
            }

            if (menuitemdestination == MenuItemDestination.Menu || menuitemdestination == MenuItemDestination.FormTab)
            {
                //SelectExisting : MenuItem die alleen getoond mag worden als het formulier al eens is opgeslagen (zelfs als je rechten hebt om dat te doen, heel specifiek dus)
                //CreateNewInActiveSubcategory: MenuItem die alleen getoond mag worden als er een fase actief is

                if (phasedefinition.MenuItemType == MenuItemType.SelectExisting)
                {
                    if (attachedphaseinstances.Count == 0 || attachedphaseinstances.Any(it => it.FormSetVersion.Count() == 0))
                    {
                        return MenuItemStatus.Invisible;
                    }
                }
                else if (phasedefinition.MenuItemType == MenuItemType.CreateNewInActiveSubcategory)
                {
                    if (!attachedphaseinstances.Any())
                    {
                        return MenuItemStatus.Invisible;
                    }
                }
            }
            else if (menuitemdestination == MenuItemDestination.Dashboard)
            {
                //SelectExisting : MenuItem die alleen getoond mag worden als het formulier al eens is opgeslagen (zelfs als je rechten hebt om dat te doen, heel specifiek dus)
                if (phasedefinition.DashboardMenuItemType == MenuItemType.SelectExisting)
                {
                    if (attachedphaseinstances.Count == 0 || attachedphaseinstances.Any(it => it.FormSetVersion.Count() == 0))
                    {
                        return MenuItemStatus.Invisible;
                    }
                }
            }

            return MenuItemStatus.Visible;
        }
    }
}
