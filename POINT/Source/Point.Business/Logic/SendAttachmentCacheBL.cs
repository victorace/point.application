﻿using Point.Database.CacheModels;
using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Repository;
using Point.Infrastructure.Caching;
using System.Collections.Generic;
using System.Linq;

namespace Point.Business.Logic
{
    public static class SendAttachmentCacheBL
    {
        public static List<SendAttachmentCache> GetSendAttachments(IUnitOfWork uow)
        {
            string cachestring = "GetSendAttachments_Global";
            List<SendAttachmentCache> sendattachments = null;

            sendattachments = CacheService.Instance.Get<List<SendAttachmentCache>>(cachestring);

            if (sendattachments == null)
            {
                sendattachments = uow.SendAttachmentRepository.GetAll().Select(sa => new SendAttachmentCache()
                {
                    Allowed = sa.Allowed,
                    AttachmentTypeID = sa.AttachmentTypeID,
                    OrganizationID = sa.OrganizationID,
                    SendAttachmentID = sa.SendAttachmentID,
                    SendDestinationType = sa.SendDestinationType
                }).ToList();

                if (sendattachments.Any())
                {
                    CacheService.Instance.Insert(cachestring, sendattachments, "CacheLong");
                }
            }

            return sendattachments;
        }

        public static IEnumerable<SendAttachmentCache> GetByOrganizationID(IUnitOfWork uow, int organizationID, SendDestinationType sendDestinationType)
        {
            var sendattachments = GetSendAttachments(uow).Where(sa => sa.OrganizationID == organizationID);
            if (sendattachments.Any() == false)
            {
                sendattachments = GetSendAttachments(uow).Where(sa => sa.OrganizationID == null && sa.SendDestinationType == sendDestinationType);
            }
            return sendattachments;
        }

        public static IEnumerable<SendAttachmentCache> GetByOrganizationIDAllowed(IUnitOfWork uow, int organizationID, SendDestinationType sendDestinationType)
        {
            var sendattachments = GetByOrganizationID(uow, organizationID, sendDestinationType);
            return sendattachments.Where(sa => sa.Allowed);
        }

        public static bool HasAttachmentTypeID(IUnitOfWork uow, int organizationID, SendDestinationType sendDestinationType, AttachmentTypeID attachmentTypeID)
        {
            var sendattachments = GetByOrganizationID(uow, organizationID, sendDestinationType);
            return sendattachments.Any(sa => sa.AttachmentTypeID == attachmentTypeID && sa.Allowed);
        }
    }
}