﻿using System.Collections.Generic;
using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Repository;

namespace Point.Business.Logic
{
    public class SignaleringTargetBL
    {
        public static IEnumerable<SignaleringTarget> GetByFlowDefinitionFormTypeAction(IUnitOfWork uow, FlowDefinitionID flowdefinitionid, decimal lastphase, int formtypeid, string action)
        {
            return uow.SignaleringTargetRepository.Get(it =>
            it.SignaleringTrigger.FlowDefinitionID == flowdefinitionid &&
            it.SignaleringTrigger.FormTypeID == formtypeid &&
            it.Action == action &&
            lastphase >= it.MinActivePhase &&
            lastphase <= it.MaxActivePhase);
        }

        public static IEnumerable<SignaleringTarget> GetByFlowDefinitionAction(IUnitOfWork uow, FlowDefinitionID flowdefinitionid, decimal lastphase, string action)
        {
            return uow.SignaleringTargetRepository.Get(it =>
            it.SignaleringTrigger.FlowDefinitionID == flowdefinitionid &&
            it.Action == action &&
            lastphase >= it.MinActivePhase &&
            lastphase <= it.MaxActivePhase);
        }

        public static IEnumerable<SignaleringTarget> GetByFlowDefinitionGeneralAction(IUnitOfWork uow, FlowDefinitionID flowdefinitionid, decimal lastphase, int generalactionid)
        {
            return uow.SignaleringTargetRepository.Get(it => 
            it.SignaleringTrigger.FlowDefinitionID == flowdefinitionid && 
            it.SignaleringTrigger.GeneralActionID == generalactionid &&
            lastphase >= it.MinActivePhase &&
            lastphase <= it.MaxActivePhase);
        }
    }
}
