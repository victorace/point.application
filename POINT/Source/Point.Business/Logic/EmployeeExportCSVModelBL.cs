﻿using Point.Business.Helper;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Models.Enums;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace Point.Business.Logic
{
    public class EmployeeExportCSVModelBL
    {
        public static byte[] ExportEmployeesToCSV(IUnitOfWork uow, string name, string organization, int? regionid, int[] organizationids,
            int[] locationids, int[] departmentids, bool searchdeleted, DateTime? lastLogin, PointUserInfo pointUserInfo)
        {
            var employeeList = EmployeeBL.Search(uow, name, organization, regionid, organizationids, locationids, departmentids, searchdeleted, lastLogin);

            var model = employeeList.Select(FromModel);

            return new CSVHelper().GenerateCSVFromEnumarable(model, typeof(EmployeeExportCSVModel), pointUserInfo.Organization.OrganizationID, CSVTemplateTypeID.EmployeeExport);
        }

        public static Expression<Func<Employee, EmployeeExportCSVModel>> FromModel => employee =>
        new EmployeeExportCSVModel()
        {
            FirstName = employee.FirstName,
            MiddleName = employee.MiddleName,
            LastName = employee.LastName,
            Gender = employee.Gender,
            Position = employee.Position,
            PhoneNumber = employee.PhoneNumber,
            EmailAddress = employee.EmailAddress,
            ExternID = employee.ExternID,
            BIG = employee.BIG,
            Organization = employee.DefaultDepartment.Location.Organization.Name,
            Location = employee.DefaultDepartment.Location.Name,
            DefaultDepartment = employee.DefaultDepartment.Name,
            _EmployeeDepartments = employee.EmployeeDepartment.Select(od => od.Department.Location.Name + " - " + od.Department.Name).ToList(),
            UserName = employee.aspnet_Users.UserName,
            CreateDate = employee.aspnet_Users.aspnet_Membership.CreateDate,
            LastLoginDate = employee.LastRealLoginDate,
            IsLockedOut = employee.aspnet_Users.aspnet_Membership.IsLockedOut == true,
            InActive = employee.InActive,
            ChangePasswordByEmployee = employee.ChangePasswordByEmployee,
            Role = employee.aspnet_Users.aspnet_Roles.Select(r => r.RoleName).Where(r => Authorization.PointFlowRoles.Select(fr => fr.ToString()).Contains(r)).FirstOrDefault(),
            ReportingLevel = employee.FunctionRole.Where(it => it.FunctionRoleTypeID == FunctionRoleTypeID.Reporting).Max(it => it.FunctionRoleLevelID),
            FunctionLevel = employee.FunctionRole.Where(it => it.FunctionRoleTypeID != FunctionRoleTypeID.Reporting).Max(it => it.FunctionRoleLevelID),
            ManageDoctors = employee.FunctionRole.Any(it => it.FunctionRoleTypeID == FunctionRoleTypeID.ManageDoctors),
            ManageOrganization = employee.FunctionRole.Any(it => it.FunctionRoleTypeID == FunctionRoleTypeID.ManageOrganization),
            ManageEmployees = employee.FunctionRole.Any(it => it.FunctionRoleTypeID == FunctionRoleTypeID.ManageEmployees),
            ManageFavorites = employee.FunctionRole.Any(it => it.FunctionRoleTypeID == FunctionRoleTypeID.ManageFavorites),
            ManageSearch = employee.FunctionRole.Any(it => it.FunctionRoleTypeID == FunctionRoleTypeID.ManageSearch),
            ManageDocuments = employee.FunctionRole.Any(it => it.FunctionRoleTypeID == FunctionRoleTypeID.ManageDocuments),
            ManageForms = employee.FunctionRole.Any(it => it.FunctionRoleTypeID == FunctionRoleTypeID.ManageForms),
            ManageCapacity = employee.FunctionRole.Any(it => it.FunctionRoleTypeID == FunctionRoleTypeID.ManageCapacity),
            ManageServicePage = employee.FunctionRole.Any(it => it.FunctionRoleTypeID == FunctionRoleTypeID.ManageServicePage),
            ManageTransferMemoTemplates = employee.FunctionRole.Any(it => it.FunctionRoleTypeID == FunctionRoleTypeID.ManageTransferMemoTemplates),
            ManageLogs = employee.FunctionRole.Any(it => it.FunctionRoleTypeID == FunctionRoleTypeID.ManageLogs)
        };
    }
}
