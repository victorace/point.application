﻿using Point.Business.Helper;
using Point.Database.Extensions;
using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Database.Repository;
using Point.Infrastructure.Caching;
using Point.Infrastructure.Extensions;
using Point.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Point.Business.BusinessObjects;
using Point.Database.Context;
using Point.Infrastructure;

namespace Point.Business.Logic
{
    public static class DepartmentBL
    {
        public static IQueryable<Department> GetQuery(IUnitOfWork uow)
        {
            return uow.DepartmentRepository.Get();
        }

        public static IQueryable<DepartmentCapacity> GetCapacityQuery(IUnitOfWork uow)
        {
            return uow.DepartmentCapacityRepository.Get();
        }

        public static IEnumerable<Department> GetAll(IUnitOfWork uow)
        {
            return uow.DepartmentRepository.GetAll();
        }

        public static IQueryable<Department> GetAllDepartmentsByOrganizationID(IUnitOfWork uow, int organizationID)
        {
            return uow.DepartmentRepository.Get(dep => !(bool)dep.Inactive && dep.Location.OrganizationID == organizationID);
        }

        public static List<Department> GetConnectedDepartments(IUnitOfWork uow, int flowinstanceid, int organizationtypeid, FlowDirection flowdirection)
        {
            var sendingdepartment = (flowdirection == FlowDirection.Any || flowdirection == FlowDirection.Sending) ? OrganizationHelper.GetSendingDepartment(uow, flowinstanceid) : null;
            var recievingdepartment = (flowdirection == FlowDirection.Any || flowdirection == FlowDirection.Receiving) ? OrganizationHelper.GetRecievingDepartment(uow, flowinstanceid) : null;

            var connecteddepartments = new List<Department>();

            if (sendingdepartment != null && (organizationtypeid == sendingdepartment.Location.Organization.OrganizationTypeID))
                connecteddepartments.Add(sendingdepartment);
            if (recievingdepartment != null && (organizationtypeid == recievingdepartment.Location.Organization.OrganizationTypeID))
                connecteddepartments.Add(recievingdepartment);

            return connecteddepartments;
        }

        public static Department GetSendingDepartmentByTransferID(IUnitOfWork uow, int transferID)
        {
            var flowinstance = FlowInstanceBL.GetByTransferID(uow, transferID);
            return flowinstance?.StartedByDepartment;
        }

        public static Department GetByDepartmentID(IUnitOfWork uow, int departmentID)
        {
            return uow.DepartmentRepository.GetByID(departmentID);
        }

        public static IEnumerable<Department> GetActiveByDepartmentIDs(IUnitOfWork uow, int[] departmentIDs)
        {
            return uow.DepartmentRepository.Get(it => it.Inactive != true && departmentIDs.Contains(it.DepartmentID));
        }

        public static IEnumerable<Department> GetByDepartmentIDs(IUnitOfWork uow, int[] departmentIDs)
        {
            return uow.DepartmentRepository.Get(it => departmentIDs.Contains(it.DepartmentID)).ToList();
        }

        public static Department GetByEmployeeID(IUnitOfWork uow, int employeeID)
        {
            var department = (from employee in uow.EmployeeRepository.Get(emp => emp.EmployeeID == employeeID, null, "DefaultDepartment", false)
                              select employee.DefaultDepartment).FirstOrDefault();

            return department;
        }

        private static string EmptyIfNull(string value)
        {
            return (string.IsNullOrEmpty(value)? "":  value);
        }

        public static bool UpdateDepartmentCapacity(IUnitOfWork uow, Department department, int? capacity1, int? capacity2, int? capacity3, int? capacity4, string information, string username, int? employeeid, DateTime? adjustmentCapacityDate = null)
        {
            var isManualAdjustmentCapacityDate = adjustmentCapacityDate.HasValue && adjustmentCapacityDate.IsToday() && adjustmentCapacityDate != department.AdjustmentCapacityDate;
            if (department == null || (!isManualAdjustmentCapacityDate && (department.CapacityDepartment == capacity1 && department.CapacityDepartmentNext == capacity2 && department.CapacityDepartment3 == capacity3
                                       && department.CapacityDepartment4 == capacity4 && EmptyIfNull(department.Information) == EmptyIfNull(information))))
            {
                return false;
            }

            var datetime = DateTime.Now;
            department.CapacityDepartment = capacity1;
            department.CapacityDepartmentNext = capacity2;
            department.CapacityDepartment3 = capacity3;
            department.CapacityDepartment4 = capacity4;
            department.Information = information;
            department.AdjustmentCapacityDate = isManualAdjustmentCapacityDate ? adjustmentCapacityDate : datetime;
            department.ModifiedTimeStamp = datetime;
            // department.UserName = username;
            department.UserID = employeeid;

            return true;
        }

        public static IEnumerable<Department> GetByOrganizationID(IUnitOfWork uow, int organizationID)
        {
            return GetByOrganizationID(uow, new List<int>() { organizationID });
        }

        public static IEnumerable<Department> GetByLocationIDs(IUnitOfWork uow, IEnumerable<int> locationids, bool showinactive = false)
        {
            return uow.DepartmentRepository.Get(dep => locationids.Contains(dep.LocationID) && (showinactive || !(bool)dep.Inactive));
        }

        public static IEnumerable<Department> GetByLocationID(IUnitOfWork uow, int locationID, bool showinactive = false)
        {
            return uow.DepartmentRepository.Get(dep => dep.LocationID == locationID && (showinactive || !(bool)dep.Inactive));
        }

        public static IEnumerable<int> GetDepartmentIDsByLocationIDs(IUnitOfWork uow, IEnumerable<int> locationids)
        {
            return uow.DepartmentRepository.Get(dep => locationids.Contains(dep.LocationID)).Select(d => d.DepartmentID).Distinct();
        }

        public static IEnumerable<Department> GetByOrganizationIDSimple(IUnitOfWork uow, int organizationID)
        {
            return uow.LocationRepository.Get(loc => loc.OrganizationID == organizationID && !(bool)loc.Inactive).SelectMany(loc => loc.Department).Where(dep => !(bool)dep.Inactive);
        }

        public static IEnumerable<Department> GetByOrganizationID(IUnitOfWork uow, IEnumerable<int> organizationIDs)
        {
            var basicOrgList = uow.OrganizationRepository.Get(org =>
                !(bool)org.Inactive && !OrganizationTypeBL.NoneselectableOrganizationTypeIDs.Contains(org.OrganizationTypeID) &&
                organizationIDs.Contains(org.OrganizationID));

            IEnumerable<Department> organizationDepartments = Enumerable.Empty<Department>();

            foreach (var org in basicOrgList)
            {
                organizationDepartments = organizationDepartments.Union(uow.DepartmentRepository.Get(dep =>
                    !(bool)dep.Inactive &&
                    !(bool)dep.Location.Inactive &&
                    !(bool)dep.Location.Organization.Inactive &&
                    dep.Location.OrganizationID == org.OrganizationID
                    ));
            }

            return organizationDepartments;
        }

        public static IQueryable<Department> GetActiveByOrganizationID(IUnitOfWork uow, int organizationid)
        {
            return uow.DepartmentRepository.Get(d =>
                !d.Inactive &&
                !d.Location.Inactive &&
                !d.Location.Organization.Inactive && !OrganizationTypeBL.NoneselectableOrganizationTypeIDs.Contains(d.Location.Organization.OrganizationTypeID) &&
                d.Location.OrganizationID == organizationid
            );
        }

        public static IEnumerable<Department> GetDepartmentsByLocationIDForSearch(IUnitOfWork uow, int locationid, string search)
        {
            return uow.DepartmentRepository.Get(it => it.LocationID == locationid && it.Name.ToLower().Contains(search)).OrderBy(it => it.Name);
        }

        public static IEnumerable<Department> GetAllActiveByLocationIDs(IUnitOfWork uow, List<int> locationids)
        {
            return (from departments in uow.DepartmentRepository.Get()
                    join locations in uow.LocationRepository.Get() on departments.LocationID equals locations.LocationID
                    where locationids.Contains(locations.LocationID) && !(bool)departments.Inactive
                    select departments);
        }

        public static bool InSameDepartment(IUnitOfWork uow, int employeeID, PointUserInfo pointUserInfo)
        {
            var userInfoDepartment = pointUserInfo?.Department;
            if (userInfoDepartment == null)
            {
                return false;
            }

            var employeeDepartment = GetByEmployeeID(uow, employeeID);
            if (employeeDepartment == null)
            {
                return false;
            }

            if (employeeDepartment.DepartmentID == pointUserInfo.Department.DepartmentID)
            {
                return true;
            }

            if (pointUserInfo.Location != null)
            {
                var departments = GetTransferPointsByLocation(uow, pointUserInfo.Location.LocationID).ToList();
                if (departments.Any())
                {
                    return departments.Any(d => employeeDepartment.DepartmentID == d.DepartmentID);
                }
            }

            return false;
        }

        public static IEnumerable<Department> GetByOrganizationIDAndDepartmentTypeID(IUnitOfWork uow, IEnumerable<int> organizationIDs, DepartmentTypeID departmentType, bool includeinactive = false)
        {
            return uow.DepartmentRepository.Get(d => organizationIDs.Contains(d.Location.OrganizationID) && d.DepartmentTypeID == (int)departmentType && (includeinactive ? true : !d.Inactive));
        }

        public static IEnumerable<Department> GetByLocationIDAndDepartmentTypeID(IUnitOfWork uow, int locationID, DepartmentTypeID departmentType, bool includeinactive = false)
        {
            return uow.DepartmentRepository.Get(d => d.LocationID == locationID && d.DepartmentTypeID == (int)departmentType && (includeinactive ? true : !d.Inactive));
        }

        public static IEnumerable<Department> GetByLocationIDAndDepartmentTypeID(IUnitOfWork uow, IEnumerable<int> locationIDs, DepartmentTypeID departmentType, bool includeinactive = false)
        {
            return uow.DepartmentRepository.Get(d => locationIDs.Contains(d.LocationID) && d.DepartmentTypeID == (int)departmentType && (includeinactive ? true : !d.Inactive));
        }

        public static IEnumerable<Department> GetByDepartmentIDAndDepartmentTypeID(IUnitOfWork uow, IEnumerable<int> departmentIDs, DepartmentTypeID departmentType, bool includeinactive = false)
        {
            return uow.DepartmentRepository.Get(d => departmentIDs.Contains(d.DepartmentID) && d.DepartmentTypeID == (int)departmentType && (includeinactive ? true : !d.Inactive));
        }

        public static Department GetByAGB(IUnitOfWork uow, string agb)
        {
            return uow.DepartmentRepository.Get(o => o.AGB.ToLower() == agb.ToLower() && o.Inactive == false).FirstOrDefault();
        }

        public static Department GetTransferPointByLocation(IUnitOfWork uow, int locationID)
        {
            return uow.DepartmentRepository.Get(dep => dep.LocationID == locationID && dep.DepartmentTypeID == (int)DepartmentTypeID.Transferpunt).FirstOrDefault();
        }

        public static IEnumerable<Department> GetTransferPointsByLocation(IUnitOfWork uow, int locationID)
        {
            var transferpoints = uow.DepartmentRepository.Get(dep => dep.LocationID == locationID && dep.DepartmentTypeID == (int)DepartmentTypeID.Transferpunt);
            return transferpoints;
        }

        public static IEnumerable<Department> GetDepartmentsByExternalNumber(IUnitOfWork uow, string patientNumber, string bsnnumber, IEnumerable<int> limitingDepartmentIDs = null)
        {
            int closed = (int)ScopeType.Closed;

            IEnumerable<int> clientIDs = null;

            if(!string.IsNullOrEmpty(bsnnumber))
            {
                clientIDs = uow.ClientRepository.Get(cl => cl.CivilServiceNumber == bsnnumber).Select(cl => cl.ClientID);
            }
            else if (!string.IsNullOrEmpty(patientNumber))
            {
                clientIDs = uow.ClientRepository.Get(cl => cl.PatientNumber == patientNumber).Select(cl => cl.ClientID);
            }

            if ( clientIDs == null || clientIDs.Count() == 0 )
            {
                return Enumerable.Empty<Department>();
            }

            IEnumerable<int> qryDossierDepartmentIDs = (from t in uow.TransferRepository.GetAll()
                                                        let fi = t.FlowInstances.FirstOrDefault()
                                                        where clientIDs.Contains(t.ClientID)
                                                              && (fi.FlowInstanceSearchValues.ScopeType != closed && !fi.Deleted)
                                                        select (fi.StartedByDepartmentID)).Distinct().ToList();

            if ( qryDossierDepartmentIDs == null || qryDossierDepartmentIDs.Count() == 0 )
            {
                return Enumerable.Empty<Department>();
            }

            if (limitingDepartmentIDs != null && limitingDepartmentIDs.Any())
            {
                qryDossierDepartmentIDs = from d in qryDossierDepartmentIDs
                                         where limitingDepartmentIDs.Contains(d)
                                        select d;
            }

            var qry = from d in uow.DepartmentRepository.GetAll()
                      where qryDossierDepartmentIDs.Contains(d.DepartmentID)
                      select d;

            return qry.ToList();
        }
        
        public static IEnumerable<MutDepartment> GetHistoryByDepartmentID(IUnitOfWork uow, int departmentid)
        {
            return uow.MutDepartmentRepository.Get(it => it.DepartmentID == departmentid);
        }

        public static IEnumerable<Department> GetLinkedDepartmentsByRole(IUnitOfWork uow, PointUserInfo pointUserInfo)
        {
            switch (pointUserInfo.Employee.PointRole)
            {
                case Role.Location:
                    return GetByLocationIDs(uow, pointUserInfo.EmployeeLocationIDs);
                case Role.Organization:
                    return GetByOrganizationID(uow, pointUserInfo.EmployeeOrganizationIDs);
                default:
                    return GetByDepartmentIDs(uow, pointUserInfo.EmployeeDepartmentIDs.ToArray());
            }
        }

        public static IEnumerable<Department> GetLinkedDepartmentsByRoleForSearch(IUnitOfWork uow, PointUserInfo pointUserInfo)
        {
            switch (pointUserInfo.Employee.PointRole)
            {
                case Role.Organization:
                    return GetByOrganizationID(uow, pointUserInfo.EmployeeOrganizationIDs).ToList();
                case Role.Location:
                    return GetByLocationIDs(uow, pointUserInfo.EmployeeLocationIDs).ToList();
                case Role.Department:
                    return GetByDepartmentIDs(uow, pointUserInfo.EmployeeDepartmentIDs.ToArray()).ToList();
                default:
                    return null;
            }
        }

        public static Department Save(IUnitOfWork uow, DepartmentViewModel viewmodel, LogEntry logentry, PointUserInfo pointuserinfo)
        {
            var model = GetByDepartmentID(uow, viewmodel.DepartmentID) ?? new Department();

            model.WriteSecureValue(m => m.LocationID, viewmodel, v => v.LocationID, pointuserinfo);
            model.WriteSecureValue(m => m.DepartmentTypeID, viewmodel, v => v.DepartmentTypeID, pointuserinfo);
            model.WriteSecureValue(m => m.AfterCareType1ID, viewmodel, v => v.AfterCareType1ID, pointuserinfo);
            model.WriteSecureValue(m => m.Name, viewmodel, v => v.Name, pointuserinfo);
            model.WriteSecureValue(m => m.PhoneNumber, viewmodel, v => v.PhoneNumber, pointuserinfo);
            model.WriteSecureValue(m => m.FaxNumber, viewmodel, v => v.FaxNumber, pointuserinfo);
            model.WriteSecureValue(m => m.EmailAddress, viewmodel, v => v.EmailAddress, pointuserinfo);
            model.WriteSecureValue(m => m.RecieveEmail, viewmodel, v => v.RecieveEmail, pointuserinfo);
            model.WriteSecureValue(m => m.CapacityFunctionality, viewmodel, v => v.CapacityFunctionality, pointuserinfo);
            model.WriteSecureValue(m => m.RehabilitationCenter, viewmodel, v => v.RehabilitationCenter, pointuserinfo);
            model.WriteSecureValue(m => m.IntensiveRehabilitationNursinghome, viewmodel, v => v.IntensiveRehabilitationNursinghome, pointuserinfo);
            model.WriteSecureValue(m => m.EmailAddressTransferSend, viewmodel, v => v.EmailAddressTransferSend, pointuserinfo);
            model.WriteSecureValue(m => m.RecieveEmailTransferSend, viewmodel, v => v.RecieveEmailTransferSend, pointuserinfo);
            model.WriteSecureValue(m => m.RecieveZorgmailTransferSend, viewmodel, v => v.RecieveZorgmailTransferSend, pointuserinfo);
            model.WriteSecureValue(m => m.ZorgmailTransferSend, viewmodel, v => v.ZorgmailTransferSend, pointuserinfo);
            model.WriteSecureValue(m => m.RecieveCDATransferSend, viewmodel, v => v.RecieveCDATransferSend, pointuserinfo);
            model.WriteSecureValue(m => m.Inactive, viewmodel, v => v.Inactive, pointuserinfo);
            model.WriteSecureValue(m => m.AddressGPForZorgmail, viewmodel, v => v.AddressGPForZorgmail, pointuserinfo);
            model.WriteSecureValue(m => m.AGB, viewmodel, v => v.AGB, pointuserinfo);
            model.WriteSecureValue(m => m.SortOrder, viewmodel, v => v.SortOrder, pointuserinfo);
            model.WriteSecureValue(m => m.ExternID, viewmodel, v => v.ExternID, pointuserinfo);

            var originalautocreatesetids = model.AutoCreateSetDepartment.Select(it => it.AutoCreateSetID).OrderBy(it => it).ToArray();
            var newautocreatesetids = viewmodel.AutoCreateSetIDs.OrderBy(it => it).ToArray();

            List<int> autocreatesetstoupdate = new List<int>();

            if(!originalautocreatesetids.SequenceEqual(newautocreatesetids))
            {
                //Make sure the department is marked as changed (in case only the changesets are changed)
                LoggingBL.FillLoggable(uow, model, logentry, forceupdate: true);
                var todeleteids = originalautocreatesetids.Where(it => !newautocreatesetids.Contains(it));
                uow.AutoCreateSetDepartmentRepository.Delete(acs => acs.DepartmentID == model.DepartmentID && todeleteids.Contains(acs.AutoCreateSetID));

                var toaddids = newautocreatesetids.Where(it => !originalautocreatesetids.Contains(it)).ToList();
                toaddids.ForEach(acsid => 
                {
                    model.AutoCreateSetDepartment.Add(
                        new AutoCreateSetDepartment {
                            AutoCreateSetID = acsid,
                            ModifiedTimeStamp = logentry.Timestamp
                        });
                });
            }

            if (viewmodel.FlowParticipation != null)
            {
                var newtodisable = viewmodel.FlowParticipation.Where(it => it.DisableByDepartment).Select(it => it.FlowDefinitionID);
                var originaldisabled = model.FlowDefinitionParticipation.Where(it => it.Participation == Participation.None).Select(it => it.FlowDefinitionID);

                var newDepartmentFlows = viewmodel.FlowParticipation.Select(fp => fp.FlowDefinitionID);
                var oldDepartmentFlows = model.FlowDefinitionParticipation;

                var toRemove = new List<FlowDefinitionID>();
                var toInsert = new List<FlowDefinitionID>();
                foreach (var viewDepartmentFlow in viewmodel.FlowParticipation)
                {
                    LoggingBL.FillLoggable(uow, model, logentry, forceupdate: true);
                    var oldFlow = oldDepartmentFlows.Where(x => x.FlowDefinitionID == viewDepartmentFlow.FlowDefinitionID).FirstOrDefault();
                    //if it is already set in dep level
                    if (oldFlow != null)
                    {
                        //check if this is diffrent from what is in db.
                        if (oldFlow.Participation != viewDepartmentFlow.Participation)
                        {
                            toRemove.Add(oldFlow.FlowDefinitionID);
                        }
                    }

                    uow.FlowDefinitionParticipationRepository.Delete(it => it.DepartmentID == model.DepartmentID && toRemove.Contains(it.FlowDefinitionID));

                    var locationFlow = model?.Location?.FlowDefinitionParticipation.FirstOrDefault(it => it.FlowDefinitionID == viewDepartmentFlow.FlowDefinitionID
                       && it.DepartmentID == null && it.Participation != Participation.None);
                    if (locationFlow != null)
                    {
                        if (viewDepartmentFlow.Participation != locationFlow.Participation)
                        {
                            toInsert.Add(viewDepartmentFlow.FlowDefinitionID);
                            model.FlowDefinitionParticipation.Add(
                           new FlowDefinitionParticipation
                           {
                               FlowDefinitionID = viewDepartmentFlow.FlowDefinitionID,
                               LocationID = viewmodel.LocationID,
                               FlowDirection = locationFlow.FlowDirection,
                               Participation = viewDepartmentFlow.Participation,
                               ModifiedTimeStamp = logentry.Timestamp
                           });
                        }

                    }
                }

            }
          

            if (model.DepartmentID <= 0)
            {
                uow.DepartmentRepository.Insert(model);
            }

            LoggingBL.FillLoggable(uow, model, logentry);
            
            Task.Run(async () => await DepartmentCapacityPublicBL.UpdateOnDepartmentChange(model));

            uow.Save(logentry);

            if(autocreatesetstoupdate.Any())
            {
                foreach (var autocreatesetid in autocreatesetstoupdate)
                {
                    AutoCreateSetBL.UpdateExistingEmployees(uow, autocreatesetid, logentry);
                }
            }
            
            CacheService.Instance.RemoveAll("FlowDefinitionParticipation_" + model.LocationID);

            return model;
        }


        public static string UpdateCapacityPerDay()
        {
            int count = 0;

            try
            {
                var today = DateTime.Now;

                using (var unitOfWork = new UnitOfWork<PointSearchContext>())
                {
                    var departments = unitOfWork.DepartmentRepository.Get(d => !(bool)d.Inactive, includeProperties: "Location.Organization,Location.LocationRegion");
                    count = departments.Count();
                    var yesterday = DateTime.Now.AddDays(-1);

                    foreach (var department in departments)
                    {
                        bool modifiedcapacity = false;

                        if (department.CapacityDepartment.HasValue || department.CapacityDepartmentNext.HasValue || department.CapacityDepartment3.HasValue || department.CapacityDepartment4.HasValue)
                        {
                            department.CapacityDepartment = department.CapacityDepartmentNext;
                            department.CapacityDepartmentNext = department.CapacityDepartment3;
                            department.CapacityDepartment3 = department.CapacityDepartment4;
                            department.CapacityDepartment4 = null;
                            department.CapacityDepartmentDate = today;
                            department.CapacityDepartmentNextDate = today.AddDays(1);
                            department.CapacityDepartment3Date = today.AddDays(2);
                            department.CapacityDepartment4Date = today.AddDays(3);

                            modifiedcapacity = true;
                            unitOfWork.DatabaseContext.Entry(department).State = System.Data.Entity.EntityState.Modified;
                        }

                        if (modifiedcapacity || (department.ModifiedTimeStamp.HasValue && department.ModifiedTimeStamp > yesterday))
                        {
                            DepartmentCapacityPublicBL.UpsertByDepartment(unitOfWork, department, null, doSave:false, updateCapacityFieldsOnly: modifiedcapacity);
                        }
                    }

                    unitOfWork.Save();
                }

                OrganizationUpdateBL.CalculateAllAsync();
            }
            catch (Exception ex)
            {
                ExceptionHelper.SendMessageException(ex);
                return $"NOK: {ex.Message}";
            }

            return $"OK. Updated {count} departments.";
        }

        //public static string UpdateCapManAll(IUnitOfWork uow)
        //{
        //    if (!CapManAPIDataHelper.HaveCapManUrl())
        //    {
        //        return "NOK: No CapManAPI specified";
        //    }

        //    var apidepartments = new List<DepartmentCapacityAPI>();
        //    try
        //    {
        //        foreach(var department in uow.DepartmentRepository.Get(d => !d.Inactive))
        //        {
        //            apidepartments.Add(CapManAPIDataHelper.MapToAPIObject(department));
        //        }
        //        Task.Run(async () => { await CapManAPIDataHelper.UpdateCapacityInCapMan(apidepartments); });
        //    }
        //    catch (Exception ex)
        //    {
        //        ExceptionHelper.SendMessageException(ex);
        //        return $"NOK: {ex.Message}";
        //    }
        //    return $"OK. Updated {apidepartments.Count} departments.";
        //}

        public static IQueryable<Department> GetAllWithActiveCapacity(IUnitOfWork uow, bool internalAndPublic, bool publicVersion = false)
        {
            return (from d in uow.DepartmentRepository.Get()
                join l in uow.LocationRepository.Get() on d.LocationID equals l.LocationID
                join o in uow.OrganizationRepository.Get() on l.OrganizationID equals o.OrganizationID
                join r in uow.RegionRepository.Get() on o.RegionID equals r.RegionID
                where (internalAndPublic ? (r.CapacityBedsPublic || r.CapacityBeds) : (publicVersion ? r.CapacityBedsPublic : r.CapacityBeds))
                    && !o.Inactive && !l.Inactive && d.CapacityFunctionality && !d.Inactive
                select d);
        }
    }
}