﻿using System.Linq;
using Point.Database.Models;
using Point.Database.Repository;

namespace Point.Business.Logic
{
    public class FrequencyTransferAttachmentBL
    {
        public static FrequencyTransferAttachment GetByAttachmentID(IUnitOfWork uow, int attachmentid)
        {
            return uow.FrequencyTransferAttachmentRepository.Get(fta => fta.AttachmentID == attachmentid).FirstOrDefault();
        }

        public static FrequencyTransferAttachment InsertAndSave(IUnitOfWork uow, Frequency frequency, TransferAttachment transferattachment)
        {
            var frequencytransferattachment = new FrequencyTransferAttachment()
            {
                Frequency = frequency,
                TransferAttachment = transferattachment
            };
            uow.FrequencyTransferAttachmentRepository.Insert(frequencytransferattachment);
            uow.Save();
            return frequencytransferattachment;
        }
    }
}
