using Point.Business.Logic;
using Point.Database.Extensions;
using Point.Database.Models;
using Point.Database.Models.Enums;
using Point.Database.Models.ViewModels;
using Point.Infrastructure;
using Point.Infrastructure.Extensions;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Linq.Expressions;

namespace Point.Business
{
    public static class FlowInstanceExpressions
    {
        public static Expression<Func<FlowInstance, bool>> ByFlowDefinition(int flowDefinitionID)
        {
            return fi => fi.FlowDefinitionID == flowDefinitionID;
        }

        public static Expression<Func<FlowInstance, bool>> ByFlowDefinitions(int[] flowDefinitionIDs)
        {
            return fi => flowDefinitionIDs.Contains(fi.FlowDefinitionID);
        }

        public static Expression<Func<FlowInstance, bool>> ByType(DossierType type, int employeeId)
        {
            switch (type)
            {
                case DossierType.All:
                default:
                    return t => true;
            }
        }

        public static Expression<Func<FlowInstance, bool>> ByTyperingNazorgCombined(string typeringnazorg)
        {
            return t => t.FlowInstanceSearchValues.Any(fis => fis.TyperingNazorgCombined.Contains(typeringnazorg));
        }

        public static Expression<Func<FlowInstance, bool>> ByAcceptedBy(int acceptedby)
        {
            return t => t.FlowInstanceSearchValues.Any(fis => fis.AcceptedByID == acceptedby);
        }

        public static Expression<Func<FlowInstance, bool>> ByAcceptedByTelephoneNumber(string telephone)
        {
            return t => t.FlowInstanceSearchValues.Any(fis => fis.AcceptedByTelephoneNumber.Contains(telephone));
        }

        public static Expression<Func<FlowInstance, bool>> ByBehandelaarSpecialisme(string specialisme)
        {
            return t => t.FlowInstanceSearchValues.Any(fis => fis.BehandelaarSpecialisme == specialisme);
        }

        public static Expression<Func<FlowInstance, bool>> ByRequestFormZHVVTType(string requestformname)
        {
            return t => t.FlowInstanceSearchValues.Any(fis => fis.RequestFormZHVVTType.Contains(requestformname));
        }

        public static Expression<Func<FlowInstance, bool>> ByRequestTransferPointIntakeDate(DateTime tpdate, SearchCritera.MatchType matchingtype)
        {
            return t =>
            matchingtype == SearchCritera.MatchType.equal && t.FlowInstanceSearchValues.Any(fis => fis.RequestTransferPointIntakeDate.HasValue && DbFunctions.TruncateTime(fis.RequestTransferPointIntakeDate.Value) == tpdate)
            ||
            matchingtype == SearchCritera.MatchType.lesser && t.FlowInstanceSearchValues.Any(fis => (fis.RequestTransferPointIntakeDate.HasValue && DbFunctions.TruncateTime(fis.RequestTransferPointIntakeDate.Value) < tpdate))
            ||
            matchingtype == SearchCritera.MatchType.greater && t.FlowInstanceSearchValues.Any(fis => fis.RequestTransferPointIntakeDate.HasValue && DbFunctions.TruncateTime(fis.RequestTransferPointIntakeDate.Value) > tpdate)
            ;
        }

        public static Expression<Func<FlowInstance, bool>> ByDatumEindeBehandelingMedischSpecialist(DateTime date, SearchCritera.MatchType matchingtype)
        {
            return t =>
            matchingtype == SearchCritera.MatchType.equal && t.FlowInstanceSearchValues.Any(fis => fis.DatumEindeBehandelingMedischSpecialist.HasValue && DbFunctions.TruncateTime(fis.DatumEindeBehandelingMedischSpecialist.Value) == date)
            ||
            matchingtype == SearchCritera.MatchType.lesser && t.FlowInstanceSearchValues.Any(fis => (fis.DatumEindeBehandelingMedischSpecialist.HasValue && DbFunctions.TruncateTime(fis.DatumEindeBehandelingMedischSpecialist.Value) < date))
            ||
            matchingtype == SearchCritera.MatchType.greater && t.FlowInstanceSearchValues.Any(fis => fis.DatumEindeBehandelingMedischSpecialist.HasValue && DbFunctions.TruncateTime(fis.DatumEindeBehandelingMedischSpecialist.Value) > date)
            ;
        }

        public static Expression<Func<FlowInstance, bool>> ByAcceptedDateTP(DateTime date, SearchCritera.MatchType matchingtype)
        {
            return t =>
            matchingtype == SearchCritera.MatchType.equal && t.FlowInstanceSearchValues.Any(fis => fis.AcceptedDateTP.HasValue && DbFunctions.TruncateTime(fis.AcceptedDateTP.Value) == date)
            ||
            matchingtype == SearchCritera.MatchType.lesser && t.FlowInstanceSearchValues.Any(fis => (fis.AcceptedDateTP.HasValue && DbFunctions.TruncateTime(fis.AcceptedDateTP.Value) < date))
            ||
            matchingtype == SearchCritera.MatchType.greater && t.FlowInstanceSearchValues.Any(fis => fis.AcceptedDateTP.HasValue && DbFunctions.TruncateTime(fis.AcceptedDateTP.Value) > date)
            ;
        }
        
        public static Expression<Func<FlowInstance, bool>> ByTransferDateVVT(DateTime date, SearchCritera.MatchType matchingtype)
        {
            return t =>
            matchingtype == SearchCritera.MatchType.equal && t.FlowInstanceSearchValues.Any(fis => fis.TransferDateVVT.HasValue && DbFunctions.TruncateTime(fis.TransferDateVVT.Value) == date)
            ||
            matchingtype == SearchCritera.MatchType.lesser && t.FlowInstanceSearchValues.Any(fis => (fis.TransferDateVVT.HasValue && DbFunctions.TruncateTime(fis.TransferDateVVT.Value) < date))
            ||
            matchingtype == SearchCritera.MatchType.greater && t.FlowInstanceSearchValues.Any(fis => fis.TransferDateVVT.HasValue && DbFunctions.TruncateTime(fis.TransferDateVVT.Value) > date)
            ;
        }

        public static Expression<Func<FlowInstance, bool>> ByDischargeProposedStartDate(DateTime date, SearchCritera.MatchType matchingtype)
        {
            return t =>
            matchingtype == SearchCritera.MatchType.equal && t.FlowInstanceSearchValues.Any(fis => fis.DischargeProposedStartDate.HasValue && DbFunctions.TruncateTime(fis.DischargeProposedStartDate.Value) == date)
            ||
            matchingtype == SearchCritera.MatchType.lesser && t.FlowInstanceSearchValues.Any(fis => (fis.DischargeProposedStartDate.HasValue && DbFunctions.TruncateTime(fis.DischargeProposedStartDate.Value) < date))
            ||
            matchingtype == SearchCritera.MatchType.greater && t.FlowInstanceSearchValues.Any(fis => fis.DischargeProposedStartDate.HasValue && DbFunctions.TruncateTime(fis.DischargeProposedStartDate.Value) > date)
            ;
        }

        public static Expression<Func<FlowInstance, bool>> ByCareBeginDate(DateTime date, SearchCritera.MatchType matchingtype)
        {
            return t =>
            matchingtype == SearchCritera.MatchType.equal && t.FlowInstanceSearchValues.Any(fis => fis.CareBeginDate.HasValue && DbFunctions.TruncateTime(fis.CareBeginDate.Value) == date)
            ||
            matchingtype == SearchCritera.MatchType.lesser && t.FlowInstanceSearchValues.Any(fis => (fis.CareBeginDate.HasValue && DbFunctions.TruncateTime(fis.CareBeginDate.Value) < date))
            ||
            matchingtype == SearchCritera.MatchType.greater && t.FlowInstanceSearchValues.Any(fis => fis.CareBeginDate.HasValue && DbFunctions.TruncateTime(fis.CareBeginDate.Value) > date)
            ;
        }

        

        public static Expression<Func<FlowInstance, bool>> ByHandling(DossierHandling handling, int employeeId, IEnumerable<int> departmentIDs)
        {
            switch (handling)
            {
                case DossierHandling.Handling:
                    return fi => fi.FlowInstanceSearchValues.Any(fisv => fisv.AcceptedByID.HasValue || fisv.AcceptedByIIID.HasValue || fisv.AcceptedByVVTID.HasValue);

                case DossierHandling.NotHandling:
                    return fi => !fi.FlowInstanceSearchValues.Any(fisv => fisv.AcceptedByID.HasValue || fisv.AcceptedByIIID.HasValue || fisv.AcceptedByVVTID.HasValue);

                case DossierHandling.HandledByMe:
                    return fi => fi.FlowInstanceSearchValues.Any(fisv => fisv.AcceptedByID == employeeId || fisv.AcceptedByIIID == employeeId || fisv.AcceptedByVVTID == employeeId);

                case DossierHandling.CreatedByMe:
                    return fi => fi.PhaseInstances.Any(pi => pi.PhaseDefinition.Phase == 1 && pi.StartedByEmployeeID == employeeId &&
                                    !pi.Cancelled);

                case DossierHandling.CreatedByDepartment:
                    return fi => fi.PhaseInstances.Any(pi => pi.PhaseDefinition.Phase == 1 && pi.DepartmentID != null && departmentIDs.Contains((int)pi.DepartmentID) &&
                                    !pi.Cancelled);

                case DossierHandling.All:
                default:
                    return t => true;
            }
        }

        public static Expression<Func<FlowInstance, bool>> ByStatus(DossierStatus status)
        {
            string sWellKnown = ReadValueType.WellKnown.ToString();
            string sLookedUp = ReadValueType.LookedUp.ToString();

            string sDone = Status.Done.ToString();
            string sTrue = true.ToString();
            string sActive = Status.Active.ToString();

            switch (status)
            {
                case DossierStatus.Active:
                    return fi => fi.FlowInstanceSearchValues.Any(fis => fis.ScopeType != (int?)ScopeType.Closed);

                case DossierStatus.IsClosed:
                    return fi => fi.FlowInstanceSearchValues.Any(fis => fis.ScopeType == (int?)ScopeType.Closed);

                case DossierStatus.Interrupted:
                    return fi => fi.FlowInstanceSearchValues.Any(fis => fis.ScopeType == (int?)ScopeType.Interrupted);

                case DossierStatus.CanBeTransferred:
                    return fi => fi.FlowInstanceSearchValues.Any(fis => fis.ScopeType == (int?)ScopeType.Accepted);

                case DossierStatus.CannotBeTransferred:
                    return fi => fi.FlowInstanceSearchValues.Any(fis => fis.ScopeType == (int?)ScopeType.NotAccepted);

                case DossierStatus.CanBeClosed:
                    return fi => fi.FlowInstanceSearchValues.Any(fis => fis.ScopeType == (int?)ScopeType.Definitive);

                case DossierStatus.All:
                default:
                    return t => true;
            }
        }

        public static Expression<Func<FlowInstance, bool>> ByTransfer(int transferID)
        {
            return fi => fi.TransferID == transferID;
        }

        public static Expression<Func<FlowInstance, bool>> ByTransferCreatedDate(DateTime createdDate, SearchCritera.MatchType matchingtype)
        {
            return fi =>
            (matchingtype == SearchCritera.MatchType.equal && DbFunctions.TruncateTime(fi.Transfer.CreatedDate) == createdDate)
            ||
            (matchingtype == SearchCritera.MatchType.lesser && DbFunctions.TruncateTime(fi.Transfer.CreatedDate) < createdDate)
            ||
            (matchingtype == SearchCritera.MatchType.greater && DbFunctions.TruncateTime(fi.Transfer.CreatedDate) > createdDate)
            ;
        }

        public static Expression<Func<FlowInstance, bool>> ByCopyOfTransfer(int transferID)
        {
            return fi => fi.Transfer.OriginalTransferID != null && (int)fi.Transfer.OriginalTransferID == transferID;
        }

        public static Expression<Func<FlowInstance, bool>> ByFlowDefinitionName(string flowDefinitionName)
        {
            return fi => fi.FlowDefinition.Name.Contains(flowDefinitionName);
        }

        public static Expression<Func<FlowInstance, bool>> ByClientFullName(string fullName)
        {
            return fi => (fi.Transfer.Client.LastName != null && fi.Transfer.Client.LastName.Contains(fullName)
                || (fi.Transfer.Client.FirstName != null && fi.Transfer.Client.FirstName.Contains(fullName))
                || (fi.Transfer.Client.MiddleName != null && fi.Transfer.Client.MiddleName.Contains(fullName)));
        }

        public static Expression<Func<FlowInstance, bool>> ByClientCivilServiceNumber(string civilServiceNumber, bool exactMatch = false)
        {
            if (exactMatch)
            {
                return fi => fi.FlowInstanceSearchValues.Any(it => it.ClientCivilServiceNumber == civilServiceNumber);
            }

            return fi => fi.FlowInstanceSearchValues.Any(it => it.ClientCivilServiceNumber.Contains(civilServiceNumber));
        }

        public static Expression<Func<FlowInstance, bool>> ByClientGender(string gender)
        {
            return fi => fi.Transfer.Client.Gender == gender;
        }

        public static Expression<Func<FlowInstance, bool>> ByClientBirthDate(DateTime birthData, SearchCritera.MatchType matchingtype)
        {
            return fi =>
            (matchingtype == SearchCritera.MatchType.equal && DbFunctions.TruncateTime(fi.Transfer.Client.BirthDate) == birthData)
            ||
            (matchingtype == SearchCritera.MatchType.lesser && DbFunctions.TruncateTime(fi.Transfer.Client.BirthDate) < birthData)
            ||
            (matchingtype == SearchCritera.MatchType.greater && DbFunctions.TruncateTime(fi.Transfer.Client.BirthDate) > birthData);
        }

        public static Expression<Func<FlowInstance, bool>> ByClientInsuranceNumber(string insuranceNumber)
        {
            return fi => fi.Transfer.Client.InsuranceNumber.Contains(insuranceNumber);
        }

        public static Expression<Func<FlowInstance, bool>> ByClientPatientNumber(string patientNumber)
        {
            return fi => fi.Transfer.Client.PatientNumber.Contains(patientNumber);
        }

        public static Expression<Func<FlowInstance, bool>> ByDepartmentName(string departmentName)
        {
            return fi => fi.Transfer.Department.Name.Contains(departmentName);
        }

        public static Expression<Func<FlowInstance, bool>> ByFlowInstances(IEnumerable<int> flowInstanceIDs)
        {
            return fi => flowInstanceIDs.Contains(fi.FlowInstanceID);
        }

        public static Expression<Func<FlowInstance, bool>> ByUserRole(PointUserInfo pointUserInfo)
        {
            Aspnet_Roles aspnetRoles = AspNet_RolesBL.GetPointFlowRole(pointUserInfo.Roles);

            if (aspnetRoles == null)
                return fi => false;

            Role role;
            Enum.TryParse<Role>(aspnetRoles.RoleName, true, out role);

            var organizationID = pointUserInfo.Organization.OrganizationID;
            bool isHealthCareProvider = pointUserInfo.Organization.IsHealthCareProvider();
            var employeeDepartmentIDs = pointUserInfo.EmployeeDepartmentIDs;
            var organizationIDs = pointUserInfo.EmployeeOrganizationIDs;
            var departmentID = pointUserInfo.Department.DepartmentID;
            bool haveDepartmentIDs = (employeeDepartmentIDs != null && employeeDepartmentIDs.Count() > 0);

            switch (role)
            {
                case Role.PointAdministrator:
                    int regionID = pointUserInfo.Region.RegionID;
                    return fi => pointUserInfo.IsTechxxAdmin
                                    ? true
                                    : fi.PhaseInstances.Any(p => p.PhaseDefinition.BeginFlow && p.Organization.RegionID == regionID);

                case Role.AdministratorOrganization:
                    return fi => isHealthCareProvider
                                    ? fi.PhaseInstances.Any(p => organizationIDs.Contains((int)p.OrganizationID) && !p.Cancelled)
                                    : fi.PhaseInstances.Any(p => p.OrganizationID == organizationID && !p.Cancelled);

                case Role.TransferEmployee:
                    return fi => fi.PhaseInstances.Any(p => p.Department != null && pointUserInfo.EmployeeLocationIDs.Contains(p.Department.LocationID) && !p.Cancelled);

                case Role.Reader:
                case Role.AdministratorDepartment:
                case Role.Employee:
                    

                    return fi => fi.PhaseInstances.Any(p => p.DepartmentID == departmentID && !p.Cancelled)
                                 ||
                                 (isHealthCareProvider ? fi.PhaseInstances.Any(p => organizationIDs.Contains((int)p.OrganizationID) && !p.Cancelled) : false)
                                 ||
                                 (haveDepartmentIDs ? fi.PhaseInstances.Any(p => employeeDepartmentIDs.Contains((int)p.DepartmentID) && !p.Cancelled) : false);

                default:
                    return fi => false;
            }
        }

        public static Expression<Func<FlowInstance, bool>> SendToDifferentHCP(PointUserInfo pointUserInfo)
        {
            if (!pointUserInfo.Organization.IsHealthCareProvider())
                return fi => false;

            Aspnet_Roles aspnetRoles = AspNet_RolesBL.GetPointFlowRole(pointUserInfo.Roles);

            if (aspnetRoles == null)
                return fi => false;

            Role role;
            Enum.TryParse<Role>(aspnetRoles.RoleName, true, out role);
            var employeeDepartmentIDs = pointUserInfo.EmployeeDepartmentIDs;
            var organizationIDs = pointUserInfo.EmployeeOrganizationIDs;
            var departmentID = pointUserInfo.Department.DepartmentID;

            switch (role)
            {
                case Role.Reader:
                case Role.AdministratorDepartment:
                case Role.Employee:
                    return fi => fi.PhaseInstances.Any(p => p.DepartmentID == departmentID || employeeDepartmentIDs.Contains((int)p.DepartmentID) || organizationIDs.Contains((int)p.OrganizationID));
                default:
                    return fi => false;
            }
        }

        public static Expression<Func<FlowInstance, bool>> ByDepartment(int departmentID)
        {
            return fi => fi.PhaseInstances.Any(p => p.DepartmentID == departmentID && !p.Cancelled);
        }

        public static Expression<Func<FlowInstance, bool>> ByLocation(int locationID)
        {
            return fi =>
                
                fi.PhaseInstances.Any(p =>
                    //Temprorary fix until we have the right DepartmentID's in all the phaseinstances
                    new string[] { 
                            ((int)FlowFormType.InBehandelingTP).ToString(), 
                            ((int)FlowFormType.GegevensPatientZHVVT).ToString() 
                        }.Contains(p.PhaseDefinition.FormTypeIDs) &&

                    p.Department.LocationID == locationID && 
                    !p.Cancelled
                );
        }

        public static Expression<Func<FlowInstance, bool>> ByNonDeleted()
        {
            return fi => !fi.Deleted;
        }

        public static Expression<Func<FlowInstance, bool>> ByFrequencyStatus(FrequencyDossierStatus status)
        {
            DateTime today = DateTime.Now.Date;

            switch (status)
            {
                case FrequencyDossierStatus.Active:
                    return fi => fi.Transfer.Frequencies.Any(f => f.Status == FrequencyStatus.Active && 
                        DbFunctions.TruncateTime(f.StartDate) <= today && 
                        DbFunctions.TruncateTime(f.EndDate) >= today && 
                        !f.Cancelled);

                case FrequencyDossierStatus.Open:
                    return fi => fi.Transfer.Frequencies.Any(f => f.Status == FrequencyStatus.Active && 
                        !f.Cancelled);

                case FrequencyDossierStatus.Close:
                    return fi => fi.Transfer.Frequencies.Any(f => !f.Cancelled) && 
                        fi.Transfer.Frequencies.All(f => f.Status == FrequencyStatus.Done &&
                        !f.Cancelled);

                case FrequencyDossierStatus.All:
                default:
                    return fi => fi.Transfer.Frequencies.Any(f => !f.Cancelled);
            }
        }
    }
}
