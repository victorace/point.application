﻿using Point.Business.Helper;
using Point.Infrastructure;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using Point.Business.BusinessObjects;
using WebSupergoo.ABCpdf10;

namespace Point.Business.Pdf
{
    public class GeneratePDF
    {
        public enum Orientation
        {
            Landscape,
            Portrait
        }

        public static void SetLicense()
        {
            var abcpdflicensekey = ConfigurationManager.AppSettings["ABCPDF.LicenceKey"];
            if (String.IsNullOrEmpty(abcpdflicensekey) == false)
            {
                if (XSettings.InstallLicense(abcpdflicensekey) == false)
                {
                    throw new ConfigurationErrorsException("ABCPDF.LicenceKey is not valid. (" + XSettings.LicenseDescription + ")");
                }
            }
            else
            {
                throw new ConfigurationErrorsException("ABCPDF.LicenceKey is not Set");
            }
        }

        public static GeneratePdfBO.GeneratePdfResult GenerateFromHTML(string body_html, byte[] extraPdfData, Orientation orientation = Orientation.Portrait)
        {
            var generatePdfResult = GenerateFromHTML(body_html, "", "", "", "A4", 50, 25, 10, true, false, extraPdfData, orientation);
            return generatePdfResult;
        }

        public static GeneratePdfBO.GeneratePdfResult GenerateFromHTML(string body_html, string header_html = "", string footer_html = "", string prePageHtml = "", string pageSize = "A4", int headerHeight = 40, int footerHeight = 20, int borderMargin = 20, bool addPageNumbers = true, bool filterEmptyValues = true, byte[] extraPdfData = null, Orientation orientation = Orientation.Portrait, List<GeneratePdfBO.GeneratePdfAttachment> attachments = null)
        {
            if (filterEmptyValues)
            {
                body_html = HtmlFilterHelper.FilterEmptyValues(body_html);
            }

            var generatePdfResult = new GeneratePdfBO.GeneratePdfResult();

            if (String.IsNullOrEmpty(header_html))
            {
                headerHeight = 0;
            }

            if (String.IsNullOrEmpty(footer_html))
            {
                footerHeight = 0;
            }

            var pdfdoc = new Doc();
            pdfdoc.HtmlOptions.PageCacheEnabled = false;

            pdfdoc.HtmlOptions.BrowserWidth = 1024;

            if (orientation == Orientation.Landscape)
            {
                // apply a rotation transform
                var w = pdfdoc.MediaBox.Width;
                var h = pdfdoc.MediaBox.Height;
                var l = pdfdoc.MediaBox.Left;
                var b = pdfdoc.MediaBox.Bottom;
                pdfdoc.Transform.Rotate(90, l, b);
                pdfdoc.Transform.Translate(w, 0);
            }
            else
            {
                pdfdoc.Transform.Rotate(0, 0, 0);
            }

            var abcpdfengine = ConfigurationManager.AppSettings["ABCPDF.Engine"];
            if (abcpdfengine == "Gecko")
            {
                pdfdoc.HtmlOptions.Engine = EngineType.Gecko; //Kan alleen als ABCPDF geinstalleerd staat op de webserver
            }

            if (!String.IsNullOrEmpty(prePageHtml))
            {

                pdfdoc.MediaBox.String = pageSize;
                pdfdoc.Rect.String = borderMargin + " " + borderMargin + " " + (pdfdoc.MediaBox.Width - borderMargin) + " " + (pdfdoc.MediaBox.Height - borderMargin);
                int preid = pdfdoc.AddImageHtml(prePageHtml, true, pdfdoc.HtmlOptions.BrowserWidth, true);
                pdfdoc.SetInfo(preid, "/Rotate", "90");
                while (true)
                {
                    if (!pdfdoc.Chainable(preid))
                    {
                        break;
                    }
                    pdfdoc.Page = pdfdoc.AddPage();
                    preid = pdfdoc.AddImageToChain(preid);
                }

                pdfdoc.Page = pdfdoc.AddPage();
            }

            Int32 theCount = 0;
            Int32 theID = 0;
            pdfdoc.MediaBox.String = pageSize;
            if (orientation == Orientation.Portrait)
            {
                pdfdoc.Rect.String = borderMargin + " " + (footerHeight + borderMargin) + " " + (pdfdoc.MediaBox.Width - borderMargin) + " " + (pdfdoc.MediaBox.Height - headerHeight - borderMargin);
            }
            else
            {
                pdfdoc.Rect.String = borderMargin + " " + (footerHeight + borderMargin + 40) + " " + (pdfdoc.MediaBox.Height - borderMargin) + " " + (pdfdoc.MediaBox.Width - headerHeight - borderMargin);
            }
            if (!String.IsNullOrEmpty(body_html))
            {
                theID = pdfdoc.AddImageHtml(body_html, true, pdfdoc.HtmlOptions.BrowserWidth, true);
            }

            if (orientation == Orientation.Landscape)
            {
                pdfdoc.SetInfo(theID, "/Rotate", "90");
            }

            while (true)
            {
                if (!pdfdoc.Chainable(theID))
                {
                    break;
                }
                pdfdoc.Page = pdfdoc.AddPage();
                theID = pdfdoc.AddImageToChain(theID);
            }

            int startpageforheaderandfooter = String.IsNullOrEmpty(prePageHtml) ? 1 : 2;

            theCount = pdfdoc.PageCount;
            if (String.IsNullOrEmpty(header_html) == false)
            {
                var source = XRect.FromLtwh(0, headerHeight, pdfdoc.MediaBox.Width, headerHeight);
                var pdfheader = new Doc();

                pdfheader.MediaBox.SetRect(source);
                pdfheader.HtmlOptions.Engine = pdfdoc.HtmlOptions.Engine;
                pdfheader.Rect.SetRect(source);
                pdfheader.AddImageHtml(header_html, true, 1024, true);

                for (int i = startpageforheaderandfooter; i <= theCount; i++)
                {
                    pdfdoc.PageNumber = i;

                    XRect target = XRect.FromLtwh(borderMargin, pdfdoc.MediaBox.Height - borderMargin, pdfdoc.MediaBox.Width - 2 * borderMargin, headerHeight);
                    pdfdoc.Rect.SetRect(target);
                    pdfdoc.AddImageDoc(pdfheader, 1, null);
                }
            }
            if (String.IsNullOrEmpty(footer_html) == false)
            {
                var source = XRect.FromLtwh(0, footerHeight, pdfdoc.MediaBox.Width, footerHeight);
                var pdffooter = new Doc();

                pdffooter.MediaBox.SetRect(source);
                pdffooter.HtmlOptions.Engine = pdfdoc.HtmlOptions.Engine;
                pdffooter.Rect.SetRect(source);
                pdffooter.AddImageHtml(footer_html, true, 1024, true);

                for (int i = startpageforheaderandfooter; i <= theCount; i++)
                {
                    pdfdoc.PageNumber = i;

                    XRect target = XRect.FromLbwh(borderMargin, borderMargin, pdfdoc.MediaBox.Width - 2 * borderMargin, footerHeight);
                    pdfdoc.Rect.SetRect(target);
                    pdfdoc.AddImageDoc(pdffooter, 1, null);
                }
            }

            if (addPageNumbers)
            {
                pdfdoc.Rect.String = borderMargin + " " + borderMargin + " " + (pdfdoc.MediaBox.Width - borderMargin) + " " + (footerHeight + borderMargin);

                for (int i = startpageforheaderandfooter; i <= theCount; i++)
                {
                    pdfdoc.PageNumber = i;

                    pdfdoc.TextStyle.HPos = 1.0;
                    pdfdoc.TextStyle.VPos = 0.5;
                    pdfdoc.FontSize = 11;

                    int pagenr = String.IsNullOrEmpty(prePageHtml) ? i : i - 1;
                    pdfdoc.AddText("Pagina " + pagenr + " / " + theCount);
                }
            }

            if (extraPdfData != null)
            {
                var extraDoc = new Doc();
                try
                {
                    extraDoc.Read(extraPdfData);
                }
                catch (Exception ex)
                {
                    HandleException(generatePdfResult, extraDoc, ex, ex.Message);
                }
                pdfdoc.Append(extraDoc);
            }

            if (attachments != null)
            {
                foreach (var attachment in attachments)
                {
                    var attachmentDoc = new Doc();
                    try
                    {
                        attachmentDoc.Read(attachment.FileData);
                        generatePdfResult.AttachmentsTotalProcessedSuccess++;
                    }
                    catch (Exception ex)
                    {
                        HandleException(generatePdfResult, attachmentDoc, ex, ErrorFormat(attachment.FileName, ex));
                    }
                    pdfdoc.Append(attachmentDoc);
                }
            }

            generatePdfResult.FileData = pdfdoc.GetData();
            
            pdfdoc.Dispose();

            return generatePdfResult;
        }

        private static void HandleException(GeneratePdfBO.GeneratePdfResult generatePdfResult, Doc doc, Exception ex, string errorMessage)
        {
            doc.FontSize = 18;
            doc.TextStyle.VPos = 0.1;
            doc.TextStyle.HPos = 0.1;
            doc.AddText(Environment.NewLine + Environment.NewLine + $"<<{errorMessage}>>");
            generatePdfResult.AddErrorMessage(errorMessage);
            ExceptionHelper.SendMessageException(ex);
        }

        private static string ErrorFormat(string fileName, Exception exception)
        {
            return $"Kan bijlage {fileName} niet afdrukken. Reden: {exception.Message}";
        }

        public static GeneratePdfBO.GeneratePdfResult GenerateFromURL(string url)
        {
            WebRequest webrequest = WebRequest.Create(url);
            HttpWebResponse response = webrequest.GetResponse() as HttpWebResponse;
            Stream datastream = response.GetResponseStream();
            StreamReader reader = new StreamReader(datastream);
            string html_response = reader.ReadToEnd();

            reader.Close();
            datastream.Close();
            response.Close();
            datastream.Close();

            var generatePdfResult = GenerateFromHTML(html_response, "", "");
            return generatePdfResult;
        }

    }
}
