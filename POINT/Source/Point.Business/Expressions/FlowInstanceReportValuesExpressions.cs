﻿using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Models.ReportModels;
using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Data.Entity.SqlServer;
using Point.Models.LDM.EOverdracht30.Enums;
using Point.Infrastructure.Extensions;

namespace Point.Business.Expressions
{
    public class FlowInstanceReportValuesExpressions
    {
        private static InviteStatus[] activeorrefusedstatuses = new InviteStatus[] { InviteStatus.Active, InviteStatus.Acknowledged, InviteStatus.AcknowledgedPartially, InviteStatus.Refused, InviteStatus.Cancelled, InviteStatus.Realized };
        private static InviteStatus[] activestatuses = new InviteStatus[] { InviteStatus.Active, InviteStatus.Acknowledged, InviteStatus.AcknowledgedPartially, InviteStatus.Realized };
        private static InviteStatus[] refusedstatuses = new InviteStatus[] { InviteStatus.Refused, InviteStatus.Cancelled };

        private static string geslachtManValue = Geslacht.OM.GetDescription();
        private static string geslachtVrouwValue = Geslacht.OF.GetDescription();

        public static Expression<Func<FlowInstanceReportValues, DICAReportModel>> DICAPartialDatafromModel => r => new DICAReportModel()
        {
            OrganizationID = r.FlowInstance.StartedByOrganizationID,
            FlowInstanceID = r.FlowInstanceID,
            IDCode = r.ClientBSN,
            Upn = r.FlowInstance.TransferID,
            Gebdat = r.ClientBirthDate,
            Geslacht = (r.ClientGender == geslachtManValue) ? "1" : (r.ClientGender.ToLower() == geslachtVrouwValue) ? "2" : "",
            Pcode = r.ClientPostalCode,
            Voorl = r.FlowInstance.Transfer.Client.Initials,
            Tussen = r.FlowInstance.Transfer.Client.MiddleName,
            Naam = r.FlowInstance.Transfer.Client.LastName,
            Patientnummer = r.ClientPatientNumber
        };

        public static Expression<Func<FlowInstanceReportValues, DumpReportModel>> AllDataFromModel => r => new DumpReportModel()
        {
            AcceptedBy = r.AcceptedBy,
            AcceptedByVVT = r.AcceptedByVVT,
            AcceptedByII = r.AcceptedByII,
            AdjustmentStandingCare = r.AdjustmentStandingCare,
            AfterCareFinancing = r.AfterCareFinancing,
            ActivePhaseText = r.ActivePhaseText,
            AfterCareCategoryDossier = r.AfterCareCategoryDossier,
            AfterCareTypeDossier = r.AfterCareTypeDossier,
            ClientBirthDate = r.ClientBirthDate,
            ClientGender = r.ClientGender,
            ClientHealthCareInsurer = r.ClientHealthCareInsurer,
            ClientPatientNumber = r.ClientPatientNumber,
            ClientPostalCode = r.ClientPostalCode.ToUpper() ?? "",
            ClientCity = r.FlowInstance.Transfer.Client.City ?? "",
            ClientVisitNumber = r.ClientVisitNumber,
            DestinationHospitalDepartment = r.DestinationHospitalDepartment,
            DischargeProposedStartDate = r.DischargeProposedStartDate,
            FirstReceiverDate = r.FirstReceiverDate,
            InterruptionDays = r.InterruptionDays,
            PatientAccepted = r.PatientAccepted == AcceptanceState.Acknowledged,
            PatientOntvingZorgVoorOpnameJaNee = r.PatientOntvingZorgVoorOpnameJaNee,
            PrognoseVerwachteOntwikkeling = r.PrognoseVerwachteOntwikkeling,
            ReasonEndRegistration = r.ReasonEndRegistration,
            ReceiveCount = r.FlowInstanceReportValuesDump.ReceiveCount,
            RegistrationEndedWithoutTransfer = r.RegistrationEndedWithoutTransfer ?? false,
            RequestFormZHVVTType = r.RequestFormZHVVTType,
            VOSendDate = r.VOSendDate,
            ZorginzetVoorDezeOpnameTypeZorginstelling = r.ZorginzetVoorDezeOpnameTypeZorginstelling,
            DatumOpname = r.DatumOpname,
            CareBeginDate = r.CareBeginDate,
            DecisionFormStartDate = r.FlowInstanceReportValuesDump.DecisionFormStartDate,
            DecisionFormRealizedDate = r.FlowInstanceReportValuesDump.DecisionFormRealizedDate,
            OpstellenVODate = r.FlowInstanceReportValuesDump.OpstellenVODate,
            OpstellenVOStatus = r.FlowInstanceReportValuesDump.OpstellenVOStatus,
            MSVTUVVDate = r.FlowInstanceReportValuesDump.MSVTUVVDate,
            MSVTUVVCount = r.FlowInstanceReportValuesDump.MSVTUVVCount,
            MSVTINDDate = r.FlowInstanceReportValuesDump.MSVTINDDate,
            GRZDate = r.FlowInstanceReportValuesDump.GRZDate,
            ELVDate = r.FlowInstanceReportValuesDump.ELVDate,
            ZorgAdviesToevoegen = r.ZorgAdviesToevoegen,
            DoorlDossierCount = r.FlowInstanceReportValuesDump.DoorlDossierCount,
            AanvullenFirstDate = r.FlowInstanceReportValuesDump.AanvullenFirstDate,
            AanvullenLastDate = r.FlowInstanceReportValuesDump.AanvullenLastDate,
            RequestInBehandelingStartDate = r.FlowInstanceReportValuesDump.RequestInBehandelingStartDate,
            RequestInBehandelingRealizeDate = r.FlowInstanceReportValuesDump.RequestInBehandelingRealizeDate,
            RequestInBehandelingStatus = r.FlowInstanceReportValuesDump.RequestInBehandelingStatus,
            ForwardDate = r.FlowInstanceReportValuesDump.ForwardDate,
            TransferClosedDate = r.FlowInstanceReportValuesDump.TransferClosedDate,
            IndicationCIZReceiveFormStartDate = r.FlowInstanceReportValuesDump.IndicationCIZReceiveFormStartDate,
            IndicationCIZReceiveFormRealizeDate = r.FlowInstanceReportValuesDump.IndicationCIZReceiveFormRealizeDate,
            IndicationCIZDecisionFormStartDate = r.FlowInstanceReportValuesDump.IndicationCIZDecisionFormStartDate,
            IndicationCIZDecisionFormRealizeDate = r.FlowInstanceReportValuesDump.IndicationCIZDecisionFormRealizeDate,
            DateIndicationDecision = r.DateIndicationDecision,
            IndicationGRZReceiveFormStartDate = r.FlowInstanceReportValuesDump.IndicationGRZReceiveFormStartDate,
            IndicationGRZReceiveFormRealizeDate = r.FlowInstanceReportValuesDump.IndicationGRZReceiveFormRealizeDate,
            IndicationGRZDecisionFormStartDate = r.FlowInstanceReportValuesDump.IndicationGRZDecisionFormStartDate,
            IndicationGRZDecisionFormRealizeDate = r.FlowInstanceReportValuesDump.IndicationGRZDecisionFormRealizeDate,
            FlowDefinitionName = r.FlowDefinitionName,
            FirstInitialDischargeDate = r.FirstInitialDischargeDate,
            InitialDischargeDate = r.InitialDischargeDate,
            KwetsbareOudereJaNee = r.KwetsbareOudereJaNee,
            RealizedDischargeDate = r.RealizedDischargeDate,
            ReceiveFormStartDate = r.FlowInstanceReportValuesDump.ReceiveFormStartDate,
            ReceiveFormRealizedDate = r.FlowInstanceReportValuesDump.ReceiveFormRealizedDate,
            RequestFormStartDate = r.FlowInstanceReportValuesDump.RequestFormStartDate,
            RequestFormRealizedDate = r.FlowInstanceReportValuesDump.RequestFormRealizedDate,
            ScopeType = r.ScopeType,
            SpecialismeBehandelaar = r.SpecialismeBehandelaar,
            TransferCreatedDate = r.TransferCreatedDate,
            TransferCreatedBy = r.TransferCreatedBy,
            TransferID = r.FlowInstance.TransferID,
            OriginalTransferID = r.FlowInstance.Transfer.OriginalTransferID,
            SendingRegionName = r.FlowInstance.StartedByOrganization.Region.Name,
            SendingOrganizationName = r.FlowInstance.StartedByOrganization.Name,
            SendingLocationName = r.FlowInstance.StartedByDepartment.Location.Name,
            SendingDepartmentName = r.FlowInstance.StartedByDepartment.Name,
            ReceivingDate = r.FlowInstanceReportValuesDump.ReceivingDate,
            ReceivingRegionName = r.FlowInstanceReportValuesDump.ReceivingRegionName,
            ReceivingOrganizationName = r.FlowInstanceReportValuesDump.ReceivingOrganizationName,
            ReceivingLocationName = r.FlowInstanceReportValuesDump.ReceivingLocationName,
            ReceivingDepartmentName = r.FlowInstanceReportValuesDump.ReceivingDepartmentName,
            ReceivingArea = r.FlowInstanceReportValuesDump.ReceivingArea,
            ReceivingPostalCode = r.FlowInstanceReportValuesDump.ReceivingPostalCode,
            IndiceringCIZOrganizationName = r.FlowInstanceReportValuesDump.IndiceringCIZOrganizationName,
            IndiceringCIZLocationName = r.FlowInstanceReportValuesDump.IndiceringCIZLocationName,
            IndiceringCIZDepartmentName = r.FlowInstanceReportValuesDump.IndiceringCIZDepartmentName,
            IndiceringGRZOrganizationName = r.FlowInstanceReportValuesDump.IndiceringGRZOrganizationName,
            IndiceringGRZLocationName = r.FlowInstanceReportValuesDump.IndiceringGRZLocationName,
            IndiceringGRZDepartmentName = r.FlowInstanceReportValuesDump.IndiceringGRZDepartmentName,
            DoorlopendDossierOrganizationName = r.FlowInstanceReportValuesDump.DoorlopendDossierOrganizationName,
            DoorlopendDossierLocationName = r.FlowInstanceReportValuesDump.DoorlopendDossierLocationName,
            DoorlopendDossierDepartmentName = r.FlowInstanceReportValuesDump.DoorlopendDossierDepartmentName,
            Projects = r.FlowInstanceTag.Select(t => t.Tag).ToList(),
            AttachmentTypes = r.FlowInstanceReportValuesAttachmentDetail.Select(a => a.AttachmentTypeID).ToList()
        };

        public static Expression<Func<FlowInstanceReportValues, StroomReportModel>> StroomDataFromModel => r => new StroomReportModel()
        {
            TransferID = r.FlowInstance.TransferID,

            ClientName = r.ClientName,
            ClientBirthDate = r.ClientBirthDate,
            ClientBSN = r.ClientBSN,
            DateTimeCreated = r.TransferCreatedDate,

            AfterCareTypeDossier = r.AfterCareTypeDossier,
            SpecialismeBehandelaar = r.SpecialismeBehandelaar,
            FlowDefinitionName = r.FlowDefinitionName,
            ActivePhaseName = r.ActivePhaseText,

            SendingOrganizationName = r.FlowInstance.StartedByOrganization.Name,

            SendingLocationName = r.FlowInstance.StartedByDepartment.Location.Name,

            SendingDepartmentName = r.FlowInstance.StartedByDepartment.Name,

            SendingDepartmentAfterCareType = r.FlowInstance.StartedByDepartment.AfterCareType1.ReportCode,

            ReceivingOrganizationName = r.FlowInstanceReportValuesDump.ReceivingOrganizationName,
            ReceivingLocationName = r.FlowInstanceReportValuesDump.ReceivingLocationName,
            ReceivingDepartmentName = r.FlowInstanceReportValuesDump.ReceivingDepartmentName,

            ReceivingDepartmentAfterCareType = r.FlowInstanceOrganization
                        .Where(d => activestatuses.Contains(d.InviteStatus))
                        .FirstOrDefault(d => d.InviteType == InviteType.Regular).Department.AfterCareType1.ReportCode,

            IndiceringCIZOrganizationName = r.FlowInstanceReportValuesDump.IndiceringCIZOrganizationName,
            IndiceringCIZLocationName = r.FlowInstanceReportValuesDump.IndiceringCIZLocationName,
            IndiceringCIZDepartmentName = r.FlowInstanceReportValuesDump.IndiceringCIZDepartmentName,

            IndiceringCIZDepartmentAfterCareType = r.FlowInstanceOrganization
                        .Where(d => activestatuses.Contains(d.InviteStatus))
                        .FirstOrDefault(d => d.InviteType == InviteType.IndicatingCIZ).Department.AfterCareType1.ReportCode,

            IndiceringGRZOrganizationName = r.FlowInstanceReportValuesDump.IndiceringGRZOrganizationName,
            IndiceringGRZLocationName = r.FlowInstanceReportValuesDump.IndiceringGRZLocationName,
            IndiceringGRZDepartmentName = r.FlowInstanceReportValuesDump.IndiceringGRZDepartmentName,

            IndiceringGRZDepartmentAfterCareType = r.FlowInstanceOrganization
                        .Where(d => activestatuses.Contains(d.InviteStatus))
                        .FirstOrDefault(d => d.InviteType == InviteType.IndicatingGRZ).Department.AfterCareType1.ReportCode,

            DoorlopendDossierOrganizationName = r.FlowInstanceReportValuesDump.DoorlopendDossierOrganizationName,
            DoorlopendDossierLocationName = r.FlowInstanceReportValuesDump.DoorlopendDossierLocationName,
            DoorlopendDossierDepartmentName = r.FlowInstanceReportValuesDump.DoorlopendDossierDepartmentName,

            DoorlopendDossierDepartmentAfterCareType = r.FlowInstanceOrganization
                        .Where(d => activestatuses.Contains(d.InviteStatus))
                        .FirstOrDefault(d => d.InviteType == InviteType.DoorlDossier).Department.AfterCareType1.Abbreviation,

            Projects = r.FlowInstanceTag.Select(t => t.Tag).ToList()
        };

        public static Expression<Func<FlowInstanceReportValues, DischargeDatePassedReportModel>> DischargeDatePassedFromModel => r => new DischargeDatePassedReportModel()
        {
            VersturendeOrganisatie = r.FlowInstance.StartedByOrganization.Name,
            VersturendeLocatie = r.FlowInstance.StartedByDepartment.Location.Name,
            VersturendeAfdeling = r.FlowInstance.StartedByDepartment.Name,

            OntvangendeOrganisatie = r.FlowInstanceReportValuesDump.ReceivingOrganizationName,
            OntvangendeLocatie = r.FlowInstanceReportValuesDump.ReceivingLocationName,
            OntvangendeAfdeling = r.FlowInstanceReportValuesDump.ReceivingDepartmentName,

            Patient = r.ClientName,
            GewensteOntslagDatum = r.InitialDischargeDate,
            AantalKalenderDagen = DbFunctions.DiffDays(r.InitialDischargeDate, DateTime.Now)
        };

        public static Expression<Func<FlowInstanceReportValues, ControlInfoReportModel>> StuurInfoFromModel => r => new ControlInfoReportModel()
        {
            VersturendeOrganisatie = r.FlowInstance.StartedByOrganization.Name,
            VersturendeLocatie = r.FlowInstance.StartedByDepartment.Location.Name,
            VersturendeAfdeling = r.FlowInstance.StartedByDepartment.Name,

            OntvangendeOrganisatie = r.FlowInstanceReportValuesDump.ReceivingOrganizationName,
            OntvangendeLocatie = r.FlowInstanceReportValuesDump.ReceivingLocationName,
            OntvangendeAfdeling = r.FlowInstanceReportValuesDump.ReceivingDepartmentName,

            IndicerendeCIZOrganisatie = r.FlowInstanceReportValuesDump.IndiceringCIZOrganizationName,

            SpecialismeBehandelaar = r.SpecialismeBehandelaar,
            NazorgType = r.AfterCareTypeDossier,
            NazorgCategorie = r.AfterCareCategoryDossier,
            ClientName = r.ClientName,
            ClientBirthDate = r.ClientBirthDate,
            TransferID = r.FlowInstance.TransferID,
            DateTimeCreated = r.FlowInstance.CreatedDate,
            DatumOpname = r.DatumOpname,
            GewensteOntslagDatum = r.InitialDischargeDate,
            GerealiseerdeOntslagDatum = r.RealizedDischargeDate,
            OptimaleOpnameDuur = DbFunctions.DiffDays(r.FlowInstance.CreatedDate, r.InitialDischargeDate),
            DuurOnderbroken = r.InterruptionDays,

            BeschikbaarGesteldeDagenVoorOntslag = DbFunctions.DiffHours(
                    r.FlowInstanceReportValuesDump.RequestInBehandelingStartDate,
                    r.InitialDischargeDate) / 24,

            BewerkingduurTransferPunt = DbFunctions.DiffHours(
                    r.FlowInstanceReportValuesDump.RequestInBehandelingStartDate,
                    r.FlowInstanceReportValuesDump.ReceiveFormStartDate) / 24.0,

            BewerkingduurIndicerende = DbFunctions.DiffHours(
                    r.FlowInstanceReportValuesDump.IndicationCIZReceiveFormStartDate,
                    r.FlowInstanceReportValuesDump.IndicationCIZDecisionFormRealizeDate) / 24.0,

            BewerkingduurOntvangFase = DbFunctions.DiffHours(
                    r.FlowInstanceReportValuesDump.ReceiveFormStartDate,
                    r.FlowInstanceReportValuesDump.ReceiveFormRealizedDate) / 24.0,

            BewerkingduurBesluitFase = DbFunctions.DiffHours(
                    r.FlowInstanceReportValuesDump.DecisionFormStartDate,
                    r.FlowInstanceReportValuesDump.DecisionFormRealizedDate) / 24.0,

            BewerkingduurOverdracht = DbFunctions.DiffDays(
                r.DischargeProposedStartDate,
                r.RealizedDischargeDate) / 24.0,

            GerealiseerdeOpnameDuur = DbFunctions.DiffDays(r.DatumOpname, r.RealizedDischargeDate),

            VerkeerdeBedDagen = DbFunctions.DiffDays(r.InitialDischargeDate, r.RealizedDischargeDate),

            RequestInBehandelingStartDate = r.FlowInstanceReportValuesDump.RequestInBehandelingStartDate,
            IndicerendeStartDate = r.FlowInstanceReportValuesDump.IndicationCIZReceiveFormStartDate,
            IndicerendeEndDate = r.FlowInstanceReportValuesDump.IndicationCIZReceiveFormRealizeDate,
            OntvangStartDate = r.FlowInstanceReportValuesDump.ReceiveFormStartDate,
            OntvangEndDate = r.FlowInstanceReportValuesDump.ReceiveFormRealizedDate,
            OntvangBesluitStartDate = r.FlowInstanceReportValuesDump.DecisionFormStartDate,
            OntvangBesluitEndDate = r.FlowInstanceReportValuesDump.DecisionFormRealizedDate,
            DischargeProposedStartDate = r.DischargeProposedStartDate,
            DateIndicationDecision = r.DateIndicationDecision
        };

        public static Expression<Func<FlowInstanceReportValues, UitvalReportModel>> UitvalFromModel(int[] myOrganizationIDs) => r => new UitvalReportModel()
        {
            VersturendeOrganisatie = r.FlowInstance.StartedByOrganization.Name,
            VersturendeLocatie = r.FlowInstance.StartedByDepartment.Location.Name,
            VersturendeAfdeling = r.FlowInstance.StartedByDepartment.Name,
            OntvangendeRegio = r.FlowInstanceReportValuesDump.ReceivingRegionName,
            OntvangendeOrganisatie = r.FlowInstanceReportValuesDump.ReceivingOrganizationName,
            OntvangendeLocatie = r.FlowInstanceReportValuesDump.ReceivingLocationName,
            OntvangendeAfdeling = r.FlowInstanceReportValuesDump.ReceivingDepartmentName,
            ClientName = r.ClientName,
            ClientBirthDate = r.ClientBirthDate,
            ClientBSN = r.ClientBSN,
            TransferID = r.FlowInstance.TransferID,
            DateTimeCreated = r.FlowInstance.CreatedDate,
            FlowDefinitionName = r.FlowDefinitionName,
            InviteStatus = r.FlowInstanceOrganization
                .Where(d => activestatuses.Contains(d.InviteStatus))
                .FirstOrDefault(d => d.InviteType == InviteType.Regular).InviteStatus,
            MyInviteStatus = r.FlowInstanceOrganization
                .Where(d => refusedstatuses.Contains(d.InviteStatus))
                .Where(d => myOrganizationIDs.Contains(d.OrganizationID))
                .FirstOrDefault(d => d.InviteType == InviteType.Regular).InviteStatus,
            MyOrganizationName = r.FlowInstanceOrganization
                .Where(d => refusedstatuses.Contains(d.InviteStatus))
                .Where(d => myOrganizationIDs.Contains(d.OrganizationID))
                .FirstOrDefault(d => d.InviteType == InviteType.Regular).Organization.Name,
            MyLocationName = r.FlowInstanceOrganization
                .Where(d => refusedstatuses.Contains(d.InviteStatus))
                .Where(d => myOrganizationIDs.Contains(d.OrganizationID))
                .FirstOrDefault(d => d.InviteType == InviteType.Regular).Department.Location.Name,
            MyDepartmentName = r.FlowInstanceOrganization
                .Where(d => refusedstatuses.Contains(d.InviteStatus))
                .Where(d => myOrganizationIDs.Contains(d.OrganizationID))
                .FirstOrDefault(d => d.InviteType == InviteType.Regular).Department.Name,
            Medewerker = r.FlowInstanceOrganization
                .Where(d => refusedstatuses.Contains(d.InviteStatus))
                .Where(d => myOrganizationIDs.Contains(d.OrganizationID))
                .FirstOrDefault(d => d.InviteType == InviteType.Regular).HandlingBy,
            Opmerking = r.FlowInstanceOrganization
                .Where(d => refusedstatuses.Contains(d.InviteStatus))
                .Where(d => myOrganizationIDs.Contains(d.OrganizationID))
                .FirstOrDefault(d => d.InviteType == InviteType.Regular).HandlingRemark,
            DatumIngaveNee = r.FlowInstanceOrganization
                .Where(d => refusedstatuses.Contains(d.InviteStatus))
                .Where(d => myOrganizationIDs.Contains(d.OrganizationID))
                .FirstOrDefault(d => d.InviteType == InviteType.Regular).HandlingDateTime
        };

        public static Expression<Func<FlowInstanceReportValues, StroomTrendModel>> StroomTrendDataFromModel => StroomTrendDataFromModelWithSetName();

        public static Expression<Func<FlowInstanceReportValues, StroomTrendModel>> StroomTrendDataFromModelWithSetName(string SetName = "", string SetDetails = "") => r => new StroomTrendModel()
        {
            SetName = SetName,
            SetDetails = SetDetails,
            TransferCreatedDate = r.TransferCreatedDate,
            Jaar = r.TransferCreatedDate.Year,
            Maand = r.TransferCreatedDate.Month,
            Week = SqlFunctions.DatePart("week", r.TransferCreatedDate) ?? 0,
            VersturendeOrganisatie = r.FlowInstance.StartedByOrganization.Name,
            VersturendeLocatie = r.FlowInstance.StartedByDepartment.Location.Name,
            VersturendeAfdeling = r.FlowInstance.StartedByDepartment.Name,
            OntvangendeOrganisatie = r.FlowInstanceReportValuesDump.ReceivingOrganizationName,
            OntvangendeLocatie = r.FlowInstanceReportValuesDump.ReceivingLocationName,
            OntvangendeAfdeling = r.FlowInstanceReportValuesDump.ReceivingDepartmentName,
            SpecialismeBehandelaar = r.SpecialismeBehandelaar,
            TransferID = r.FlowInstance.TransferID,

            OntslagDatumVerschilDagen = DbFunctions.DiffDays(
                                            r.InitialDischargeDate,
                                            r.RealizedDischargeDate),

            OntslagDatumBehaald = (r.RealizedDischargeDate == null || r.FirstInitialDischargeDate == null)
                                  ? (bool?)null
                                  : (r.RealizedDischargeDate <= r.FirstInitialDischargeDate),

            PreTransferFaseDagen = DbFunctions.DiffHours(
                                      r.DatumOpname,
                                      r.FlowInstanceReportValuesDump.RequestFormStartDate) / 24.0,

            TransferFaseDagen = DbFunctions.DiffHours(
                    r.FlowInstanceReportValuesDump.RequestInBehandelingStartDate,
                    r.FlowInstanceReportValuesDump.ReceiveFormStartDate) / 24.0,

            OntvangFaseDagen = DbFunctions.DiffHours(
                    r.FlowInstanceReportValuesDump.ReceiveFormStartDate,
                    r.FlowInstanceReportValuesDump.ReceiveFormRealizedDate) / 24.0,

            BesluitFaseDagen = DbFunctions.DiffHours(
                    r.FlowInstanceReportValuesDump.DecisionFormStartDate,
                    r.FlowInstanceReportValuesDump.DecisionFormRealizedDate) / 24.0,

            OntvangBesluitFaseDagen = DbFunctions.DiffHours(
                    r.FlowInstanceReportValuesDump.ReceiveFormStartDate,
                    r.FlowInstanceReportValuesDump.DecisionFormRealizedDate) / 24.0,

            OverdrachtFaseDagen = DbFunctions.DiffHours(
                    r.FlowInstanceReportValuesDump.DecisionFormRealizedDate, 
                    r.RealizedDischargeDate) / 24.0,

            NazorgType = r.AfterCareTypeDossier,
            NazorgCategorie = r.AfterCareCategoryDossier,
            IsVoorrang = r.IsVoorrang,
            NazorgCategorieLumc = r.IsVoorrang ? "Voorrang" : (r.AfterCareCategoryDossier ?? "Geen"),
            OntvangFaseStart = r.FlowInstanceReportValuesDump.ReceiveFormStartDate,
            OntvangFaseEnd = r.FlowInstanceReportValuesDump.ReceiveFormRealizedDate,
            BesluitFaseStart = r.FlowInstanceReportValuesDump.DecisionFormStartDate,
            BesluitFaseEnd = r.FlowInstanceReportValuesDump.DecisionFormRealizedDate
        };

        public static Expression<Func<FlowInstanceReportValues, bool>> ByFlowDirectionAndOrganizationTypeIDs(FlowDirection flowDirection, int[] organizationTypeIDs) =>
           fi => fi.FlowInstanceOrganization.Any(org =>
                        activestatuses.Contains(org.InviteStatus) &&
                        (organizationTypeIDs.Any() == false || organizationTypeIDs.Contains(org.Organization.OrganizationTypeID)) &&
                        (flowDirection == FlowDirection.Any || org.FlowDirection == flowDirection)
                    );

        public static Expression<Func<FlowInstanceReportValues, bool>> ByDateRange(DateTime startDate, DateTime endDate) =>
         fi => fi.TransferCreatedDate > startDate && fi.TransferCreatedDate < endDate;

        public static Expression<Func<FlowInstanceReportValues, bool>> WithReceiver =>
         fi => fi.FlowInstanceOrganization.Any(d => activestatuses.Contains(d.InviteStatus) && d.InviteType == InviteType.Regular);

        public static Expression<Func<FlowInstanceReportValues, bool>> ByFlowDefinitionIDs(FlowDefinitionID[] flowDefinitionIDs) =>
         fi => flowDefinitionIDs.Contains(fi.FlowDefinitionID);

        public static Expression<Func<FlowInstanceReportValues, bool>> ByDossierStatus(DossierStatus dossierstatus) =>
        //Alleen nog afgesloten en actieve (voorlopig)
         fi => (dossierstatus == DossierStatus.Active && fi.ScopeType != ScopeType.Closed) ||
                (dossierstatus == DossierStatus.IsClosed && fi.ScopeType == ScopeType.Closed) ||
                (dossierstatus == DossierStatus.CanBeClosed && fi.ScopeType == ScopeType.Definitive);
        
        public static Expression<Func<FlowInstanceReportValues, bool>> ActiveInviteByRegionID(int regionID, FlowDirection flowDirection, int[] organizationTypeIDs) =>
            fi => fi.FlowInstanceOrganization.Any(inv => activestatuses.Contains(inv.InviteStatus) && inv.Organization.RegionID == regionID
                  && (organizationTypeIDs.Any() == false || organizationTypeIDs.Contains(inv.Organization.OrganizationTypeID))
                  && (flowDirection == FlowDirection.Any || inv.FlowDirection == flowDirection));
        public static Expression<Func<FlowInstanceReportValues, bool>> RefusedInviteByRegionID(int regionID, FlowDirection flowDirection, int[] organizationTypeIDs) =>
            fi => fi.FlowInstanceOrganization.Any(inv => refusedstatuses.Contains(inv.InviteStatus) && inv.Organization.RegionID == regionID
                  && (organizationTypeIDs.Any() == false || organizationTypeIDs.Contains(inv.Organization.OrganizationTypeID))
                  && (flowDirection == FlowDirection.Any || inv.FlowDirection == flowDirection));
        public static Expression<Func<FlowInstanceReportValues, bool>> ActiveOrRefusedInviteByRegionID(int regionID, FlowDirection flowDirection, int[] organizationTypeIDs) =>
            fi => fi.FlowInstanceOrganization.Any(inv => activeorrefusedstatuses.Contains(inv.InviteStatus) && inv.Organization.RegionID == regionID
                  && (organizationTypeIDs.Any() == false || organizationTypeIDs.Contains(inv.Organization.OrganizationTypeID))
                  && (flowDirection == FlowDirection.Any || inv.FlowDirection == flowDirection));
        public static Expression<Func<FlowInstanceReportValues, bool>> ActiveInviteByOrganizationIDs(int[] organizationIDs, FlowDirection flowDirection) =>
            fi => fi.FlowInstanceOrganization.Any(inv => activestatuses.Contains(inv.InviteStatus) && organizationIDs.Contains(inv.OrganizationID)
                  && (flowDirection == FlowDirection.Any || inv.FlowDirection == flowDirection));
        public static Expression<Func<FlowInstanceReportValues, bool>> RefusedInviteByOrganizationIDs(int[] organizationIDs, FlowDirection flowDirection) =>
            fi => fi.FlowInstanceOrganization.Any(inv => refusedstatuses.Contains(inv.InviteStatus) && organizationIDs.Contains(inv.OrganizationID)
                  && (flowDirection == FlowDirection.Any || inv.FlowDirection == flowDirection));
        public static Expression<Func<FlowInstanceReportValues, bool>> ActiveOrRefusedInviteByOrganizationIDs(int[] organizationIDs, FlowDirection flowDirection) =>
            fi => fi.FlowInstanceOrganization.Any(inv => activeorrefusedstatuses.Contains(inv.InviteStatus) && organizationIDs.Contains(inv.OrganizationID)
                  && (flowDirection == FlowDirection.Any || inv.FlowDirection == flowDirection));
        public static Expression<Func<FlowInstanceReportValues, bool>> ActiveInviteByLocationIDs(int[] locationIDs, FlowDirection flowDirection) =>
            fi => fi.FlowInstanceOrganization.Any(inv => activestatuses.Contains(inv.InviteStatus) && locationIDs.Contains(inv.LocationID)
                  && (flowDirection == FlowDirection.Any || inv.FlowDirection == flowDirection));
        public static Expression<Func<FlowInstanceReportValues, bool>> RefusedInviteByLocationIDs(int[] locationIDs, FlowDirection flowDirection) =>
            fi => fi.FlowInstanceOrganization.Any(inv => refusedstatuses.Contains(inv.InviteStatus) && locationIDs.Contains(inv.LocationID)
                  && (flowDirection == FlowDirection.Any || inv.FlowDirection == flowDirection));
        public static Expression<Func<FlowInstanceReportValues, bool>> ActiveOrRefusedInviteByLocationIDs(int[] locationIDs, FlowDirection flowDirection) =>
            fi => fi.FlowInstanceOrganization.Any(inv => activeorrefusedstatuses.Contains(inv.InviteStatus) && locationIDs.Contains(inv.LocationID)
                  && (flowDirection == FlowDirection.Any || inv.FlowDirection == flowDirection));
        public static Expression<Func<FlowInstanceReportValues, bool>> ActiveInviteByDepartmentIDs(int[] departmentIDs, FlowDirection flowDirection) =>
            fi => fi.FlowInstanceOrganization.Any(inv => activestatuses.Contains(inv.InviteStatus) && departmentIDs.Contains(inv.DepartmentID)
                  && (flowDirection == FlowDirection.Any || inv.FlowDirection == flowDirection));
        public static Expression<Func<FlowInstanceReportValues, bool>> RefusedInviteByDepartmentIDs(int[] departmentIDs, FlowDirection flowDirection) =>
            fi => fi.FlowInstanceOrganization.Any(inv => refusedstatuses.Contains(inv.InviteStatus) && departmentIDs.Contains(inv.DepartmentID)
                  && (flowDirection == FlowDirection.Any || inv.FlowDirection == flowDirection));
        public static Expression<Func<FlowInstanceReportValues, bool>> ActiveOrRefusedInviteByDepartmentIDs(int[] departmentIDs, FlowDirection flowDirection) =>
            fi => fi.FlowInstanceOrganization.Any(inv => activeorrefusedstatuses.Contains(inv.InviteStatus) && departmentIDs.Contains(inv.DepartmentID)
                  && (flowDirection == FlowDirection.Any || inv.FlowDirection == flowDirection));

        public static Expression<Func<FlowInstanceReportValues, bool>> ByTags(string[] tags) =>
            fi => fi.FlowInstanceTag.Any(t => tags.Contains(t.Tag));
    }
}
