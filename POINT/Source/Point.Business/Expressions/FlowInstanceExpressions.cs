using Point.Database.Models;
using Point.Database.Models.ViewModels;
using Point.Models.Enums;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace Point.Business.Expressions
{
    public static class FlowInstanceExpressions
    {
        private static InviteStatus[] activestatuses = new InviteStatus[] { InviteStatus.Active, InviteStatus.Acknowledged, InviteStatus.AcknowledgedPartially, InviteStatus.Realized };

        public static Expression<Func<FlowInstanceSearchViewModel, bool>> ByPhaseDefinitions(IEnumerable<int> phaseDefinitionIDs)
        {
            return fi => fi.FlowInstanceSearchValues.FlowInstance.PhaseInstances.Any(pi => !pi.Cancelled && pi.Status == (int)Status.Active && phaseDefinitionIDs.Contains(pi.PhaseDefinitionID));
        }

        public static Expression<Func<FlowInstanceSearchViewModel, bool>> ByFlowDefinitions(FlowDefinitionID[] flowDefinitionIDs)
        {
            return fi => flowDefinitionIDs.Contains(fi.FlowInstanceSearchValues.FlowDefinitionID);
        }

        public static Expression<Func<FlowInstanceSearchViewModel, bool>> ByTyperingNazorgCombined(string typeringnazorg)
        {
            return t => t.FlowInstanceSearchValues.TyperingNazorgCombined.Contains(typeringnazorg);
        }

        public static Expression<Func<FlowInstanceSearchViewModel, bool>> ByAcceptedByTP(int acceptedby)
        {
            return t => t.FlowInstanceSearchValues.AcceptedByID == acceptedby;
        }

        public static Expression<Func<FlowInstanceSearchViewModel, bool>> ByAcceptedByTelephoneNumber(string telephone)
        {
            return t => t.FlowInstanceSearchValues.AcceptedByTelephoneNumber.Contains(telephone);
        }

        public static Expression<Func<FlowInstanceSearchViewModel, bool>> ByOrganizationName(string organizationname)
        {
            return t => t.FlowInstanceSearchValues.OrganizationName.Contains(organizationname);
        }

        public static Expression<Func<FlowInstanceSearchViewModel, bool>> ByLocationName(string locationname)
        {
            return t => t.FlowInstanceSearchValues.LocationName.Contains(locationname);
        }

        public static Expression<Func<FlowInstanceSearchViewModel, bool>> ByBehandelaarSpecialisme(string specialisme)
        {
            return t => t.FlowInstanceSearchValues.BehandelaarSpecialisme == specialisme;
        }

        public static Expression<Func<FlowInstanceSearchViewModel, bool>> ByRequestFormZHVVTType(string requestformname)
        {
            return t => t.FlowInstanceSearchValues.RequestFormZHVVTType.Contains(requestformname);
        }

        public static Expression<Func<FlowInstanceSearchViewModel, bool>> ByRequestTransferPointIntakeDate(DateTime tpdate, SearchCritera.MatchType matchingtype)
        {
            return t =>
            matchingtype == SearchCritera.MatchType.equal && t.FlowInstanceSearchValues.RequestTransferPointIntakeDate.HasValue && DbFunctions.TruncateTime(t.FlowInstanceSearchValues.RequestTransferPointIntakeDate.Value) == tpdate
            ||
            matchingtype == SearchCritera.MatchType.lesser && t.FlowInstanceSearchValues.RequestTransferPointIntakeDate.HasValue && DbFunctions.TruncateTime(t.FlowInstanceSearchValues.RequestTransferPointIntakeDate.Value) < tpdate
            ||
            matchingtype == SearchCritera.MatchType.greater && t.FlowInstanceSearchValues.RequestTransferPointIntakeDate.HasValue && DbFunctions.TruncateTime(t.FlowInstanceSearchValues.RequestTransferPointIntakeDate.Value) > tpdate
            ;
        }

        public static Expression<Func<FlowInstanceSearchViewModel, bool>> ByDatumEindeBehandelingMedischSpecialist(DateTime date, SearchCritera.MatchType matchingtype)
        {
            return t =>
            matchingtype == SearchCritera.MatchType.equal && t.FlowInstanceSearchValues.DatumEindeBehandelingMedischSpecialist.HasValue && DbFunctions.TruncateTime(t.FlowInstanceSearchValues.DatumEindeBehandelingMedischSpecialist.Value) == date
            ||
            matchingtype == SearchCritera.MatchType.lesser && t.FlowInstanceSearchValues.DatumEindeBehandelingMedischSpecialist.HasValue && DbFunctions.TruncateTime(t.FlowInstanceSearchValues.DatumEindeBehandelingMedischSpecialist.Value) < date
            ||
            matchingtype == SearchCritera.MatchType.greater && t.FlowInstanceSearchValues.DatumEindeBehandelingMedischSpecialist.HasValue && DbFunctions.TruncateTime(t.FlowInstanceSearchValues.DatumEindeBehandelingMedischSpecialist.Value) > date
            ;
        }

        public static Expression<Func<FlowInstanceSearchViewModel, bool>> ByAcceptedDateTP(DateTime date, SearchCritera.MatchType matchingtype)
        {
            return t =>
            matchingtype == SearchCritera.MatchType.equal && t.FlowInstanceSearchValues.AcceptedDateTP.HasValue && DbFunctions.TruncateTime(t.FlowInstanceSearchValues.AcceptedDateTP.Value) == date
            ||
            matchingtype == SearchCritera.MatchType.lesser && t.FlowInstanceSearchValues.AcceptedDateTP.HasValue && DbFunctions.TruncateTime(t.FlowInstanceSearchValues.AcceptedDateTP.Value) < date
            ||
            matchingtype == SearchCritera.MatchType.greater && t.FlowInstanceSearchValues.AcceptedDateTP.HasValue && DbFunctions.TruncateTime(t.FlowInstanceSearchValues.AcceptedDateTP.Value) > date
            ;
        }
        
        public static Expression<Func<FlowInstanceSearchViewModel, bool>> ByTransferDateVVT(DateTime date, SearchCritera.MatchType matchingtype)
        {
            return t =>
            matchingtype == SearchCritera.MatchType.equal && t.FlowInstanceSearchValues.TransferDateVVT.HasValue && DbFunctions.TruncateTime(t.FlowInstanceSearchValues.TransferDateVVT.Value) == date
            ||
            matchingtype == SearchCritera.MatchType.lesser && t.FlowInstanceSearchValues.TransferDateVVT.HasValue && DbFunctions.TruncateTime(t.FlowInstanceSearchValues.TransferDateVVT.Value) < date
            ||
            matchingtype == SearchCritera.MatchType.greater && t.FlowInstanceSearchValues.TransferDateVVT.HasValue && DbFunctions.TruncateTime(t.FlowInstanceSearchValues.TransferDateVVT.Value) > date
            ;
        }

        public static Expression<Func<FlowInstanceSearchViewModel, bool>> ByDischargeProposedStartDate(DateTime date, SearchCritera.MatchType matchingtype)
        {
            return t =>
            matchingtype == SearchCritera.MatchType.equal && t.FlowInstanceSearchValues.DischargeProposedStartDate.HasValue && DbFunctions.TruncateTime(t.FlowInstanceSearchValues.DischargeProposedStartDate.Value) == date
            ||
            matchingtype == SearchCritera.MatchType.lesser && t.FlowInstanceSearchValues.DischargeProposedStartDate.HasValue && DbFunctions.TruncateTime(t.FlowInstanceSearchValues.DischargeProposedStartDate.Value) < date
            ||
            matchingtype == SearchCritera.MatchType.greater && t.FlowInstanceSearchValues.DischargeProposedStartDate.HasValue && DbFunctions.TruncateTime(t.FlowInstanceSearchValues.DischargeProposedStartDate.Value) > date
            ;
        }

        public static Expression<Func<FlowInstanceSearchViewModel, bool>> ByCareBeginDate(DateTime date, SearchCritera.MatchType matchingtype)
        {
            return t =>
            matchingtype == SearchCritera.MatchType.equal && t.FlowInstanceSearchValues.CareBeginDate.HasValue && DbFunctions.TruncateTime(t.FlowInstanceSearchValues.CareBeginDate.Value) == date
            ||
            matchingtype == SearchCritera.MatchType.lesser && t.FlowInstanceSearchValues.CareBeginDate.HasValue && DbFunctions.TruncateTime(t.FlowInstanceSearchValues.CareBeginDate.Value) < date
            ||
            matchingtype == SearchCritera.MatchType.greater && t.FlowInstanceSearchValues.CareBeginDate.HasValue && DbFunctions.TruncateTime(t.FlowInstanceSearchValues.CareBeginDate.Value) > date
            ;
        }

        public static Expression<Func<FlowInstanceSearchViewModel, bool>> ByHandling(DossierHandling handling, PointUserInfo pointuserinfo)
        {
            var employeeId = pointuserinfo.Employee.EmployeeID;
            var departmentIDs = pointuserinfo.EmployeeDepartmentIDs;
            var primaryorganizationtypeid = pointuserinfo.Organization.OrganizationTypeID;

            switch (handling)
            {
                case DossierHandling.Handling:
                    switch (primaryorganizationtypeid)
                    {
                        case (int)OrganizationTypeID.Hospital:
                            return fi => fi.FlowInstanceSearchValues.AcceptedByID.HasValue;
                        case (int)OrganizationTypeID.CIZ:
                            return fi => fi.FlowInstanceSearchValues.AcceptedByIIID.HasValue;
                        case (int)OrganizationTypeID.HealthCareProvider:
                            return fi => fi.FlowInstanceSearchValues.AcceptedByVVTID.HasValue;
                    }

                    return fi => false;

                case DossierHandling.NotHandling:
                    switch (primaryorganizationtypeid)
                    {
                        case (int)OrganizationTypeID.Hospital:
                            return fi => !fi.FlowInstanceSearchValues.AcceptedByID.HasValue;
                        case (int)OrganizationTypeID.CIZ:
                            return fi => !fi.FlowInstanceSearchValues.AcceptedByIIID.HasValue;
                        case (int)OrganizationTypeID.HealthCareProvider:
                            return fi => !fi.FlowInstanceSearchValues.AcceptedByVVTID.HasValue;
                    }

                    return fi => false;

                case DossierHandling.HandledByMe:
                    switch (primaryorganizationtypeid)
                    {
                        case (int)OrganizationTypeID.Hospital:
                            return fi => fi.FlowInstanceSearchValues.AcceptedByID == employeeId;
                        case (int)OrganizationTypeID.GRZ:
                        case (int)OrganizationTypeID.CIZ:
                            return fi => fi.FlowInstanceSearchValues.AcceptedByIIID == employeeId;
                        case (int)OrganizationTypeID.HealthCareProvider:
                            return fi => fi.FlowInstanceSearchValues.AcceptedByVVTID == employeeId;
                    }
                    return fi => false;

                case DossierHandling.OwnedByMyDepartment:
                    return fi => departmentIDs.Contains(fi.FlowInstanceSearchValues.FlowInstance.StartedByDepartmentID);
                case DossierHandling.OwnedByMe:
                    return fi => fi.FlowInstanceSearchValues.FlowInstance.StartedByEmployeeID == employeeId;

                case DossierHandling.CreatedByMe:
                    return fi => fi.FlowInstanceSearchValues.TransferCreatedByID == employeeId;
                case DossierHandling.CreatedByDepartment:
                    return fi => departmentIDs.Contains(fi.FlowInstanceSearchValues.FlowInstance.StartedByDepartmentID);

                default:
                    return t => true;
            }
        }

        public static Expression<Func<FlowInstanceSearchViewModel, bool>> ByStatus(DossierStatus status)
        {
            switch (status)
            {
                case DossierStatus.Active:
                    return fi => fi.FlowInstanceSearchValues.ScopeType != (int?)ScopeType.Closed;

                case DossierStatus.IsClosed:
                    return fi => fi.FlowInstanceSearchValues.ScopeType == (int?)ScopeType.Closed;

                case DossierStatus.Interrupted:
                    return fi => fi.FlowInstanceSearchValues.ScopeType == (int?)ScopeType.Interrupted;

                case DossierStatus.CanBeTransferred:
                    return fi => fi.FlowInstanceSearchValues.ScopeType == (int?)ScopeType.Accepted;

                case DossierStatus.CanPerhapsBeTransferred:
                    return fi => fi.FlowInstanceSearchValues.ScopeType == (int?)ScopeType.PartiallyAccepted;

                case DossierStatus.CannotBeTransferred:
                    return fi => fi.FlowInstanceSearchValues.ScopeType == (int?)ScopeType.NotAccepted;

                case DossierStatus.CanBeClosed:
                    return fi => fi.FlowInstanceSearchValues.ScopeType == (int?)ScopeType.Definitive;

                default:
                    return t => true;
            }
        }

        public static Expression<Func<FlowInstanceSearchViewModel, bool>> ByTransfer(int transferID)
        {
            return fi => fi.FlowInstanceSearchValues.TransferID == transferID;
        }

        public static Expression<Func<FlowInstanceSearchViewModel, bool>> ByTransfers(int[] transferIDs)
        {
            return fi => transferIDs.Contains(fi.FlowInstanceSearchValues.TransferID);
        }

        public static Expression<Func<FlowInstanceSearchViewModel, bool>> ByTransferCreatedDate(DateTime createdDate, SearchCritera.MatchType matchingtype)
        {
            return fi =>
            (matchingtype == SearchCritera.MatchType.equal && DbFunctions.TruncateTime(fi.FlowInstanceSearchValues.TransferCreatedDate) == createdDate)
            ||
            (matchingtype == SearchCritera.MatchType.lesser && DbFunctions.TruncateTime(fi.FlowInstanceSearchValues.TransferCreatedDate) < createdDate)
            ||
            (matchingtype == SearchCritera.MatchType.greater && DbFunctions.TruncateTime(fi.FlowInstanceSearchValues.TransferCreatedDate) > createdDate)
            ;
        }

        public static Expression<Func<FlowInstanceSearchViewModel, bool>> ByCopyOfTransfer(int transferID)
        {
            return fi => fi.FlowInstanceSearchValues.CopyOfTransferID != null && fi.FlowInstanceSearchValues.CopyOfTransferID == transferID;
        }

        public static Expression<Func<FlowInstance, bool>> ByFlowDefinitionName(string flowDefinitionName)
        {
            return fi => fi.FlowDefinition.Name.Contains(flowDefinitionName);
        }

        public static Expression<Func<FlowInstanceSearchViewModel, bool>> ByClientFullNameOrTransferID(string fullNameortransferid)
        {
            //This is to make it possible to search for anonymous clients wich have the transferid as clientname
            if (int.TryParse(fullNameortransferid, out var nameastransferid))
            {
                return fi => fi.FlowInstanceSearchValues.TransferID == nameastransferid;
            }

            return fi => fi.FlowInstanceSearchValues.ClientFullname.Contains(fullNameortransferid);
        }

        public static Expression<Func<FlowInstanceSearchViewModel, bool>> BySearchValuesClientCivilServiceNumber(string civilServiceNumber, bool exactMatch = false)
        {
            if (exactMatch)
            {
                return fi => fi.FlowInstanceSearchValues.ClientCivilServiceNumber == civilServiceNumber;
            }

            return fi => fi.FlowInstanceSearchValues.ClientCivilServiceNumber.Contains(civilServiceNumber);
        }

        public static Expression<Func<FlowInstanceSearchViewModel, bool>> BySearchValuesClientCivilServiceNumberOrPatientNumber(string civilServiceNumber, string patientNumber)
        {
            if (!string.IsNullOrEmpty(civilServiceNumber) && !string.IsNullOrEmpty(patientNumber))
            {
                return fi => fi.FlowInstanceSearchValues.ClientCivilServiceNumber == civilServiceNumber || fi.FlowInstanceSearchValues.PatientNumber == patientNumber;
            }
            else if (!string.IsNullOrEmpty(patientNumber))
            {
                return fi => fi.FlowInstanceSearchValues.PatientNumber == patientNumber;
            }
            else if (!string.IsNullOrEmpty(civilServiceNumber))
            {
                return fi => fi.FlowInstanceSearchValues.ClientCivilServiceNumber == civilServiceNumber;
            }
            else
            {
                return fi => true;
            }
        }

        public static Expression<Func<FlowInstance, bool>> ByClientCivilServiceNumber(string civilServiceNumber, bool exactMatch = false)
        {
            if (exactMatch)
            {
                return fi => fi.Transfer.Client.CivilServiceNumber == civilServiceNumber;
            }

            return fi => fi.Transfer.Client.CivilServiceNumber.Contains(civilServiceNumber);
        }

        public static Expression<Func<FlowInstanceSearchViewModel, bool>> ByClientGender(string gender)
        {
            return fi => fi.FlowInstanceSearchValues.ClientGender == gender;
        }

        public static Expression<Func<FlowInstanceSearchViewModel, bool>> ByClientBirthDate(DateTime birthData, SearchCritera.MatchType matchingtype)
        {
            return fi =>
            (matchingtype == SearchCritera.MatchType.equal && DbFunctions.TruncateTime(fi.FlowInstanceSearchValues.ClientBirthDate) == birthData)
            ||
            (matchingtype == SearchCritera.MatchType.lesser && DbFunctions.TruncateTime(fi.FlowInstanceSearchValues.ClientBirthDate) < birthData)
            ||
            (matchingtype == SearchCritera.MatchType.greater && DbFunctions.TruncateTime(fi.FlowInstanceSearchValues.ClientBirthDate) > birthData);
        }

        public static Expression<Func<FlowInstanceSearchViewModel, bool>> ByClientInsuranceNumber(string insuranceNumber)
        {
            return fi => fi.FlowInstanceSearchValues.FlowInstance.Transfer.Client.InsuranceNumber.Contains(insuranceNumber);
        }

        public static Expression<Func<FlowInstanceSearchViewModel, bool>> ByClientPatientNumber(string patientNumber)
        {
            return fi => fi.FlowInstanceSearchValues.PatientNumber.Contains(patientNumber);
        }

        public static Expression<Func<FlowInstanceSearchViewModel, bool>> BySearchValuesClientPatientNumber(string patientNumber)
        {
            return fi => fi.FlowInstanceSearchValues.PatientNumber.Contains(patientNumber);
        }

        public static Expression<Func<FlowInstanceSearchViewModel, bool>> ByDepartmentName(string departmentName)
        {
            return fi => fi.FlowInstanceSearchValues.DepartmentName.Contains(departmentName);
        }

        public static Expression<Func<FlowInstanceSearchViewModel, bool>> ByFlowInstances(IEnumerable<int> flowInstanceIDs)
        {
            return fi => flowInstanceIDs.Contains(fi.FlowInstanceSearchValues.FlowInstanceID);
        }

        public static Expression<Func<FlowInstanceSearchViewModel, bool>> ByUserRole(PointUserInfo pointUserInfo, FlowDirection flowdirection)
        {
            if (pointUserInfo.Employee.PointRole == Role.None)
                return fi => false;

            var employeeDepartmentIDs = pointUserInfo.EmployeeDepartmentIDs.ToArray();
            var employeeLocationIDS = pointUserInfo.EmployeeLocationIDs.ToArray();
            var employeeorganizationIDs = pointUserInfo.EmployeeOrganizationIDs.ToArray();
            var employeeRegionID = pointUserInfo.Region?.RegionID;

            switch (pointUserInfo.Employee.PointRole)
            {
                case Role.Global:
                    return fi => true;

                case Role.Region:
                    if(employeeRegionID == null)
                    {
                        throw new Exception($"Gebruiker {pointUserInfo.Employee.UserName} heeft geen regio");
                    }                    
                    switch (flowdirection)
                    {
                        case FlowDirection.Any:
                            //RB heeft in het geheel geen toegang wanneer de versturende organisatie zich buiten haar regio bevindt
                            return fi => (fi.FlowInstanceSearchValues.FlowInstance.StartedByOrganization.RegionID == employeeRegionID) &&
                            fi.FlowInstanceSearchValues.FlowInstanceOrganization.Any(fisvd => activestatuses.Contains(fisvd.InviteStatus) && fisvd.Organization.RegionID == employeeRegionID);
                            
                        case FlowDirection.Receiving:
                            //RB heeft in het geheel geen toegang wanneer de versturende organisatie zich buiten haar regio bevindt
                            return fi => (fi.FlowInstanceSearchValues.FlowInstance.StartedByOrganization.RegionID == employeeRegionID) &&
                            fi.FlowInstanceSearchValues.FlowInstanceOrganization.Any(fisvd => activestatuses.Contains(fisvd.InviteStatus) && fisvd.Organization.RegionID == employeeRegionID  && fisvd.FlowDirection == FlowDirection.Receiving);

                        case FlowDirection.Sending:
                            return fi => fi.FlowInstanceSearchValues.FlowInstanceOrganization.Any(fisvd => activestatuses.Contains(fisvd.InviteStatus) && fisvd.Organization.RegionID == employeeRegionID && fisvd.FlowDirection == FlowDirection.Sending);

                        default:
                            throw new ArgumentOutOfRangeException(nameof(flowdirection), flowdirection, null);
                    }

                case Role.Organization:
                    switch (flowdirection)
                    {
                        case FlowDirection.Any:
                            return fi => fi.FlowInstanceSearchValues.FlowInstanceOrganization.Any(fisvd => activestatuses.Contains(fisvd.InviteStatus) && employeeorganizationIDs.Contains(fisvd.OrganizationID));
                        case FlowDirection.Receiving:
                            return fi => fi.FlowInstanceSearchValues.FlowInstanceOrganization.Any(fisvd => activestatuses.Contains(fisvd.InviteStatus) && employeeorganizationIDs.Contains(fisvd.OrganizationID)  && fisvd.FlowDirection == FlowDirection.Receiving);
                        case FlowDirection.Sending:
                            return fi => fi.FlowInstanceSearchValues.FlowInstanceOrganization.Any(fisvd => activestatuses.Contains(fisvd.InviteStatus) && employeeorganizationIDs.Contains(fisvd.OrganizationID) && fisvd.FlowDirection == FlowDirection.Sending);
                        default:
                            throw new ArgumentOutOfRangeException(nameof(flowdirection), flowdirection, null);
                    }

                case Role.Location:
                    switch (flowdirection)
                    {
                        case FlowDirection.Any:
                            return fi => fi.FlowInstanceSearchValues.FlowInstanceOrganization.Any(fisvd => activestatuses.Contains(fisvd.InviteStatus) && employeeLocationIDS.Contains(fisvd.LocationID));
                        case FlowDirection.Receiving:
                            return fi => fi.FlowInstanceSearchValues.FlowInstanceOrganization.Any(fisvd => activestatuses.Contains(fisvd.InviteStatus) && employeeLocationIDS.Contains(fisvd.LocationID) && fisvd.FlowDirection == FlowDirection.Receiving);
                        case FlowDirection.Sending:
                            return fi => fi.FlowInstanceSearchValues.FlowInstanceOrganization.Any(fisvd => activestatuses.Contains(fisvd.InviteStatus) && employeeLocationIDS.Contains(fisvd.LocationID) && fisvd.FlowDirection == FlowDirection.Sending);
                        default:
                            throw new ArgumentOutOfRangeException(nameof(flowdirection), flowdirection, null);
                    }

                case Role.Department:
                    switch (flowdirection)
                    {
                        case FlowDirection.Any:
                            return fi => fi.FlowInstanceSearchValues.FlowInstanceOrganization.Any(fisvd => activestatuses.Contains(fisvd.InviteStatus) && employeeDepartmentIDs.Contains(fisvd.DepartmentID));
                        case FlowDirection.Receiving:
                            return fi => fi.FlowInstanceSearchValues.FlowInstanceOrganization.Any(fisvd => activestatuses.Contains(fisvd.InviteStatus) && employeeDepartmentIDs.Contains(fisvd.DepartmentID) && fisvd.FlowDirection == FlowDirection.Receiving);
                        case FlowDirection.Sending:
                            return fi => fi.FlowInstanceSearchValues.FlowInstanceOrganization.Any(fisvd => activestatuses.Contains(fisvd.InviteStatus) && employeeDepartmentIDs.Contains(fisvd.DepartmentID) && fisvd.FlowDirection == FlowDirection.Sending);
                        default:
                            throw new ArgumentOutOfRangeException(nameof(flowdirection), flowdirection, null);
                    }

                case Role.None:
                    return fi => false;

                default:
                    return fi => false;
            }
        }

        public static Expression<Func<FlowInstanceSearchViewModel, bool>> ByDepartment(int departmentID)
        {
            return fi => fi.FlowInstanceSearchValues.FlowInstanceOrganization.Any(fisvd => activestatuses.Contains(fisvd.InviteStatus) && fisvd.DepartmentID == departmentID);
        }

        public static Expression<Func<FlowInstanceSearchViewModel, bool>> ByDepartments(int[] departmentIDs)
        {
            return fi => fi.FlowInstanceSearchValues.FlowInstanceOrganization.Any(fisvd => activestatuses.Contains(fisvd.InviteStatus) && departmentIDs.Contains(fisvd.DepartmentID));
        }

        public static Expression<Func<FlowInstanceSearchViewModel, bool>> ByLocation(int locationID)
        {
            return fi => fi.FlowInstanceSearchValues.FlowInstanceOrganization.Any(it => activestatuses.Contains(it.InviteStatus) && it.LocationID == locationID);
        }

        public static Expression<Func<FlowInstanceSearchViewModel, bool>> ByOrganization(int organizationID)
        {
            return fi => fi.FlowInstanceSearchValues.FlowInstanceOrganization.Any(it => activestatuses.Contains(it.InviteStatus) && it.OrganizationID == organizationID);
        }

        public static Expression<Func<FlowInstanceSearchViewModel, bool>> ByFrequencyStatus(FrequencyDossierStatus status)
        {
            switch (status)
            {
                case FrequencyDossierStatus.Active:
                    return fi => fi.FlowInstanceDoorlopendDossierSearchValues != null &&
                        fi.FlowInstanceDoorlopendDossierSearchValues.FrequencyScopeType == (int?)ScopeType.FeedbackActiveInRange;

                case FrequencyDossierStatus.Open:
                    return fi => fi.FlowInstanceDoorlopendDossierSearchValues != null &&
                        fi.FlowInstanceDoorlopendDossierSearchValues.FrequencyScopeType == (int?)ScopeType.FeedbackActiveOutRange;

                case FrequencyDossierStatus.Close:
                    return fi => fi.FlowInstanceDoorlopendDossierSearchValues != null &&
                        fi.FlowInstanceDoorlopendDossierSearchValues.FrequencyScopeType == (int?)ScopeType.FeedbackClosed;

                default:
                    return fi => true;
            }
        }
    }
}