﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Security.Claims;
using System.Web;
using System.Web.Routing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Point.Infrastructure.Extensions;
using Point.MVC.Auth.Infrastructure.Extensions;
using Point.MVC.Auth;
using Point.Business.Logic;
using System.Linq;
using Point.Business.Helper;

namespace Point.UnitTests
{
    [TestClass]
    public class ConversionTests
    {
        [TestMethod]
        public void ConvertEnumToString()
        {
            var enumval = Models.Enums.Status.Active;
            var enumname = enumval.ConvertTo<string>();

            Assert.AreEqual("Active", enumname);
        }

        [TestMethod]
        public void ConvertNullIntTo0()
        {
            int? nullval = null;
            var intval = nullval.ConvertTo<int>();

            Assert.AreEqual(0, intval);
        }

        [TestMethod]
        public void ConvertNullEnumToDefault()
        {
            Models.Enums.Status? nullval = null;
            Models.Enums.Status val = nullval.ConvertTo<Models.Enums.Status>();

            Assert.AreEqual(Models.Enums.Status.NotActive, val);
        }

        [TestMethod]
        public void ConvertEnumByInt()
        {
            var activeint = 1;
            var enumstatus = activeint.ConvertTo<Models.Enums.Status>();

            Assert.AreEqual(Models.Enums.Status.Active, enumstatus);
        }

        [TestMethod]
        public void ConvertEnumByName()
        {
            var activestring = "Active";
            var enumstatus = activestring.ConvertTo<Models.Enums.Status>();

            Assert.AreEqual(Models.Enums.Status.Active, enumstatus);
        }

        [TestMethod]
        public void ConvertEmptyStringToNullableInt()
        {
            var value = "";
            var intvalue = value.ConvertTo<int?>();

            Assert.AreEqual(null, intvalue);
        }

        [TestMethod]
        public void ConvertDateTimeToString()
        {
            var datetime = DateTime.Now;
            var datetimestring = datetime.ConvertTo<string>();

            Assert.AreEqual(datetime.ToString("yyyy-MM-dd HH:mm:ss"), datetimestring);
        }

        [TestMethod]
        public void ConvertDateToString()
        {
            var date = DateTime.Now.Date;
            var datestring = date.ConvertTo<string>();

            Assert.AreEqual(date.ToString("yyyy-MM-dd"), datestring);
        }

        [TestMethod]
        public void RequestVariableNullInt()
        {
            var request = new Mock<HttpRequestBase>();
            var unvalidatedrb = new Mock<UnvalidatedRequestValuesBase>();
            var requestContext = new Mock<RequestContext>();

            unvalidatedrb.SetupGet(rb => rb.QueryString).Returns(new NameValueCollection() { });
            unvalidatedrb.SetupGet(rb => rb.Form).Returns(new NameValueCollection() { });
            requestContext.SetupGet(rc => rc.RouteData).Returns(new RouteData() { });

            request.SetupGet(g => g.Unvalidated).Returns(unvalidatedrb.Object);
            request.SetupGet(g => g.RequestContext).Returns(requestContext.Object);

            var testval = request.Object.GetRequestVariable<int?>("name");

            Assert.AreEqual(null, testval);
        }

        [TestMethod]
        public void RequestVariableInt()
        {
            var request = new Mock<HttpRequestBase>();
            var unvalidatedrb = new Mock<UnvalidatedRequestValuesBase>();
            var requestContext = new Mock<RequestContext>();

            unvalidatedrb.SetupGet(rb => rb.QueryString).Returns(new NameValueCollection() { { "name", "1" }, });
            unvalidatedrb.SetupGet(rb => rb.Form).Returns(new NameValueCollection() { });
            requestContext.SetupGet(rc => rc.RouteData).Returns(new RouteData() { });

            request.SetupGet(g => g.Unvalidated).Returns(unvalidatedrb.Object);
            request.SetupGet(g => g.RequestContext).Returns(requestContext.Object);

            var testval = request.Object.GetRequestVariable<int>("name");

            Assert.AreEqual(1, testval);
        }

        [TestMethod]
        public void RequestVariableIntDefaultValue()
        {
            var request = new Mock<HttpRequestBase>();
            var unvalidatedrb = new Mock<UnvalidatedRequestValuesBase>();
            var requestContext = new Mock<RequestContext>();

            unvalidatedrb.SetupGet(rb => rb.QueryString).Returns(new NameValueCollection() { });
            unvalidatedrb.SetupGet(rb => rb.Form).Returns(new NameValueCollection() { });
            requestContext.SetupGet(rc => rc.RouteData).Returns(new RouteData() { });

            request.SetupGet(g => g.Unvalidated).Returns(unvalidatedrb.Object);
            request.SetupGet(g => g.RequestContext).Returns(requestContext.Object);

            var testval = request.Object.GetRequestVariable("name", -1);

            Assert.AreEqual(-1, testval);
        }

        [TestMethod]
        public void RequestVariableBool()
        {
            var request = new Mock<HttpRequestBase>();
            var unvalidatedrb = new Mock<UnvalidatedRequestValuesBase>();
            var requestContext = new Mock<RequestContext>();

            unvalidatedrb.SetupGet(rb => rb.QueryString).Returns(new NameValueCollection() { { "name", "true" }, });
            unvalidatedrb.SetupGet(rb => rb.Form).Returns(new NameValueCollection() { });
            requestContext.SetupGet(rc => rc.RouteData).Returns(new RouteData() { });

            request.SetupGet(g => g.Unvalidated).Returns(unvalidatedrb.Object);
            request.SetupGet(g => g.RequestContext).Returns(requestContext.Object);

            var testval = request.Object.GetRequestVariable<bool>("name");

            Assert.AreEqual(true, testval);
        }


        [TestMethod]
        public void RouteDataNullInt()
        {
            var routedata = new RouteData();

            var testval = routedata.GetValue<int?>("name", null);

            Assert.AreEqual(null, testval);
        }

        [TestMethod]
        public void RouteDataInt()
        {
            var routedata = new RouteData();
            routedata.Values.Add("name", "1");

            var testval = routedata.GetValue<int>("name", -1);

            Assert.AreEqual(1, testval);
        }

        [TestMethod]
        public void RouteDataIntDefaultValue()
        {
            var routedata = new RouteData();

            var testval = routedata.GetValue<int>("name", -1);

            Assert.AreEqual(-1, testval);
        }

        [TestMethod]
        public void RouteDataBool()
        {
            var routedata = new RouteData();
            routedata.Values.Add("name", "true");

            var testval = routedata.GetValue<bool>("name", false);

            Assert.AreEqual(true, testval);
        }

        [TestMethod]
        public void ClaimsIdentityBool()
        {
            var claimid = new ClaimsIdentity();
            claimid.AddClaims(new List<Claim>() { new Claim(Constants.POINT_Claim_OrganizationID, "true", ClaimValueTypes.Boolean) });

            var value = claimid.GetValue(Constants.POINT_Claim_OrganizationID, false);
            Assert.AreEqual(true, value);
        }

        [TestMethod]
        public void ClaimsIdentityInt()
        {
            var claimid = new ClaimsIdentity();
            claimid.AddClaims(new List<Claim>() { new Claim(Constants.POINT_Claim_OrganizationID, "1", ClaimValueTypes.Integer ) });

            var value = claimid.GetValue(Constants.POINT_Claim_OrganizationID, -1);
            Assert.AreEqual(1, value);
        }

        [TestMethod]
        public void ClaimsIdentityDefaultValue()
        {
            var claimid = new ClaimsIdentity();
            claimid.AddClaims(new List<Claim>() {  });

            var value = claimid.GetValue<int>(Constants.POINT_Claim_OrganizationID, -1);
            Assert.AreEqual(-1, value);
        }

        [TestMethod]
        public void ClaimsIdentityNullInt()
        {
            var claimid = new ClaimsIdentity();
            claimid.AddClaims(new List<Claim>() { });

            var value = claimid.GetValue<int?>(Constants.POINT_Claim_OrganizationID, null);
            Assert.AreEqual(null, value);
        }

        [TestMethod]
        public void TestInvokeParameters()
        {
            var actionname = "GetOrganizationProjectByTransferID";
            var actionMethod = typeof(LookUpBL).GetMethods().FirstOrDefault(method => method.Name == actionname);
            var parameterinfos = new NameValueCollection() { { "currentvalue", "1234" }, { "transferid", "2345" } }; //string currentvalue, int transferid

            var invokeParameters = FlowFieldValueHelper.GetInvokeParameters(actionMethod, parameterinfos);
            var expected = new object[] { "1234", 2345 };

            Assert.AreEqual(expected.Length, invokeParameters.Length);
            Assert.AreEqual(expected[0], invokeParameters[0]);
            Assert.AreEqual(expected[1], invokeParameters[1]);
        }
    }
}
