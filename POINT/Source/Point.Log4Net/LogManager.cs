using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace Point.Log4Net
{
    public static class LogManager
    {
        public static ILogger GetLogger(Type type)
        {
            return new Log4NetWrapper(type);
        }

        [MethodImpl(MethodImplOptions.NoInlining)]
        public static ILogger GetLogger()
        {
            StackTrace stack = new StackTrace();
            StackFrame frame = stack.GetFrame(1);
            return new Log4NetWrapper(frame.GetMethod().DeclaringType);
        }
    }
}