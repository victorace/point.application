using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Reflection;
using log4net;
using log4net.Config;
using log4net.Repository.Hierarchy;
using Point.Log4Net.Enums;

[assembly: XmlConfigurator(Watch = true)]

namespace Point.Log4Net
{
    public class Log4NetWrapper : ILogger
    {
        private const string Log4NetDefaultConfig = "log4net.config";
        private static readonly string _log4NetResourceName = $"{Assembly.GetExecutingAssembly().GetName().Name}.{Log4NetDefaultConfig}";
        private readonly ILog _logger;
        private static bool _isConfigured;

        public bool IsConfigured => _isConfigured;
        public bool IsDebugEnabled => _logger != null && _logger.IsDebugEnabled;
        public bool IsErrorEnabled => _logger != null && _logger.IsErrorEnabled;
        public bool IsFatalEnabled => _logger != null && _logger.IsFatalEnabled;
        public bool IsInfoEnabled => _logger != null && _logger.IsInfoEnabled;
        public bool IsWarnEnabled => _logger != null && _logger.IsWarnEnabled;
        
        public Log4NetWrapper(Type type)
        {
            if (type == null)
            {
                throw new ArgumentNullException("type");
            }

            Name = type.ToString();

            ConfigureLog4Net();

            if (_logger == null)
            {
                _logger = log4net.LogManager.GetLogger(type);
            }
        }

        public Log4NetWrapper(ILog logger)
        {
            if (logger == null)
            {
                throw new ArgumentNullException("logger");
            }

            Name = logger.Logger?.Name ?? string.Empty;

            _logger = logger;
            ConfigureLog4Net();
        }

        public string Name { get; }

        private static bool ConfigureLog4Net()
        {
            _isConfigured = false;

            var rep = (Hierarchy)log4net.LogManager.GetRepository();

            if (rep.Configured)
            {
                return true;
            }

            if (rep.Root.Appenders.Count == 0)
            {
                rep.ResetConfiguration();

                // Load the Point.Log4Net default configuration
                rep = (Hierarchy)log4net.LogManager.GetRepository(Assembly.GetExecutingAssembly());
                var assembly = Assembly.GetExecutingAssembly();

                using (Stream stream = assembly.GetManifestResourceStream(_log4NetResourceName))
                {
                    XmlConfigurator.Configure(stream);
                }
            }

            if (rep.Root.Appenders.Count == 0)
            {
                return false;
            }

            _isConfigured = true;
            return true;
        }

        private static void SetThreadProperties(int? errorCode, Exception exception)
        {
            const string unknown = "Unknown";
            var fileName = unknown;
            var methodName = unknown;
            int? lineNumber = null;

            if (exception != null)
            {
                // Get stack trace for the exception with source file information
                var frame = new StackTrace(exception, true).GetFrame(0);

                if (frame != null)
                {
                    lineNumber = frame.GetFileLineNumber();
                    methodName = frame.GetMethod().Name;
                    fileName = Path.GetFileNameWithoutExtension(frame.GetFileName());
                }
            }

            if (errorCode.HasValue)
            {
                ThreadContext.Properties[FieldsDefaultAdoNetAppender.ErrorNumber] = errorCode.ToString();
            }

            ThreadContext.Properties[FieldsDefaultAdoNetAppender.ErrorCode] = errorCode;
            ThreadContext.Properties[FieldsDefaultAdoNetAppender.ClassName] = fileName;
            ThreadContext.Properties[FieldsDefaultAdoNetAppender.MethodName] = methodName;
            ThreadContext.Properties[FieldsDefaultAdoNetAppender.LineNumber] = lineNumber;
        }

        public int Debug(LogParameters parameters)
        {
            return LogMessage(parameters, LogType.Debug);
        }
        public int Debug(string message, Exception exception = null)
        {
            return Debug(new LogParameters { Message = message, Exception = exception });
        }
        public int Debug(Exception exception)
        {
            return Debug(exception.Message, exception);
        }
        public int DebugFormat(LogParameters parameters, params object[] args)
        {
            return LogMessage(parameters, LogType.DebugFormat, args);
        }
        public int Error(LogParameters parameters)
        {
            return LogMessage(parameters, LogType.Error);
        }
        public int Error(string message, Exception exception = null)
        {
            return Error(new LogParameters { Message = message, Exception = exception });
        }
        public int Error(Exception exception)
        {
            return Error(exception.Message, exception);
        }
        public int ErrorFormat(LogParameters parameters, params object[] args)
        {
            return LogMessage(parameters, LogType.ErrorFormat, args);
        }
        public int Fatal(LogParameters parameters)
        {
            return LogMessage(parameters, LogType.Fatal);
        }
        public int Fatal(string message, Exception exception = null)
        {
            return Fatal(new LogParameters { Message = message, Exception = exception });
        }
        public int Fatal(Exception exception)
        {
            return Fatal(exception.Message, exception);
        }
        public int FatalFormat(LogParameters parameters, params object[] args)
        {
            return LogMessage(parameters, LogType.FatalFormat, args);
        }
        public int Info(LogParameters parameters)
        {
            return LogMessage(parameters, LogType.Info);
        }
        public int Info(string message, Exception exception = null)
        {
            return Info(new LogParameters { Message = message, Exception = exception });
        }
        public int Info(Exception exception)
        {
            return Info(exception.Message, exception);
        }
        public int InfoFormat(LogParameters parameters, params object[] args)
        {
            return LogMessage(parameters, LogType.InfoFormat, args);
        }
        public int Warning(LogParameters parameters)
        {
            return LogMessage(parameters, LogType.Warning);
        }
        public int Warning(string message, Exception exception = null)
        {
            return Warning(new LogParameters { Message = message, Exception = exception });
        }
        public int Warning(Exception exception)
        {
            return Warning(exception.Message, exception);
        }
        public int WarningFormat(LogParameters parameters, params object[] args)
        {
            return LogMessage(parameters, LogType.WarningFormat, args);
        }

        private int LogMessage(LogParameters parameters, LogType logType, params object[] args)
        {
            if (_logger == null || (logType == LogType.Debug || logType == LogType.DebugFormat) && !_logger.IsDebugEnabled ||
                (logType == LogType.Error || logType == LogType.ErrorFormat) && !_logger.IsErrorEnabled ||
                (logType == LogType.Fatal || logType == LogType.FatalFormat) && !_logger.IsFatalEnabled ||
                (logType == LogType.Info || logType == LogType.InfoFormat) && !_logger.IsInfoEnabled ||
                (logType == LogType.Warning || logType == LogType.WarningFormat) && !_logger.IsWarnEnabled)
            {
                return -1;
            }

            parameters.LogType = logType;
            SetProperties(parameters);

            switch (parameters.LogType)
            {
                case LogType.Debug:
                    if (parameters.Exception != null)
                    {
                        _logger.Debug(parameters.Message, parameters.Exception);
                    }
                    else
                    {
                        _logger.Debug(parameters.Message);
                    }
                    break;
                case LogType.DebugFormat:
                    _logger.DebugFormat(CultureInfo.InvariantCulture, parameters.Message, args);
                    break;
                case LogType.Error:
                    if (parameters.Exception != null)
                    {
                        _logger.Error(parameters.Message, parameters.Exception);
                    }
                    else
                    {
                        _logger.Error(parameters.Message);
                    }
                    break;
                case LogType.ErrorFormat:
                    _logger.ErrorFormat(CultureInfo.InvariantCulture, parameters.Message, args);
                    break;
                case LogType.Fatal:
                    if (parameters.Exception != null)
                    {
                        _logger.Fatal(parameters.Message, parameters.Exception);
                    }
                    else
                    {
                        _logger.Fatal(parameters.Message);
                    }
                    break;
                case LogType.FatalFormat:
                    _logger.FatalFormat(CultureInfo.InvariantCulture, parameters.Message, args);
                    break;
                case LogType.Warning:
                    if (parameters.Exception != null)
                    {
                        _logger.Warn(parameters.Message, parameters.Exception);
                    }
                    else
                    {
                        _logger.Warn(parameters.Message);
                    }
                    break;
                case LogType.WarningFormat:
                    _logger.WarnFormat(CultureInfo.InvariantCulture, parameters.Message, args);
                    break;
                case LogType.InfoFormat:
                    _logger.InfoFormat(CultureInfo.InvariantCulture, parameters.Message, args);
                    break;
                default:
                    // INFO
                    if (parameters.Exception != null)
                    {
                        _logger.Info(parameters.Message, parameters.Exception);
                    }
                    else
                    {
                        _logger.Info(parameters.Message);
                    }
                    break;
            }

            return parameters.ErrorCode ?? -1;
        }
        private static void SetProperties(LogParameters parameters)
        {
            if (parameters == null)
            {
                return;
            }

            if (parameters.TransferID.HasValue)
            {
                ThreadContext.Properties[FieldsDefaultAdoNetAppender.TransferID] = parameters.TransferID.Value;
            }
            if (parameters.EmployeeID.HasValue)
            {
                ThreadContext.Properties[FieldsDefaultAdoNetAppender.EmployeeID] = parameters.EmployeeID.Value;
            }
            if (!string.IsNullOrEmpty(parameters.MachineName))
            {
                ThreadContext.Properties[FieldsDefaultAdoNetAppender.MachineName] = parameters.MachineName;
            }
            if (!string.IsNullOrEmpty(parameters.WindowsIdentity))
            {
                ThreadContext.Properties[FieldsDefaultAdoNetAppender.WindowsIdentity] = parameters.WindowsIdentity;
            }

            ThreadContext.Properties[FieldsDefaultAdoNetAppender.EmployeeUserName] = parameters.EmployeeUserName;
            ThreadContext.Properties[FieldsDefaultAdoNetAppender.Url] = parameters.Url;
            ThreadContext.Properties[FieldsDefaultAdoNetAppender.QueryString] = parameters.QueryString;
            ThreadContext.Properties[FieldsDefaultAdoNetAppender.UserAgent] = parameters.UserAgent;
            ThreadContext.Properties[FieldsDefaultAdoNetAppender.IsAjaxRequest] = parameters.IsAjaxRequest;
            ThreadContext.Properties[FieldsDefaultAdoNetAppender.Cookies] = parameters.Cookies;
            ThreadContext.Properties[FieldsDefaultAdoNetAppender.PostData] = parameters.PostData;

            if (parameters.Exception == null)
            {
                return;
            }

            var stackTrace = parameters.Exception.StackTrace;
            if (!string.IsNullOrEmpty(stackTrace))
            {
                ThreadContext.Properties[FieldsDefaultAdoNetAppender.Stacktrace] = stackTrace;
            }

            int? errorCode = null;

            if (parameters.ErrorCode.HasValue)
            {
                errorCode = parameters.ErrorCode;
            }
            else if (parameters.LogType == LogType.Error || parameters.LogType == LogType.Fatal)
            {
                errorCode = GetErrorCode();
                parameters.ErrorCode = errorCode;
            }

            SetThreadProperties(errorCode, parameters.Exception);
        }
        private static int GetErrorCode()
        {
            var now = DateTime.Now;
            var random = new Random(now.Millisecond).Next(10, 99);
            return Convert.ToInt32(random + now.ToString("HHmm"));
        }
        
    }
}
