﻿using System;
using Point.Log4Net.Enums;


namespace Point.Log4Net
{
    public class LogParameters
    {
        public LogType LogType { get; set; }
        public Exception Exception { get; set; }
        public string MachineName { get; set; } // Environment.MachineName
        public string Message { get; set; }
        public string WindowsIdentity { get; set; } // Environment.UserName
        public int? EmployeeID { get; set; }
        public string EmployeeUserName { get; set; }
        public int? ErrorCode { get; set; }

        // WEB
        public string Cookies { get; set; }
        public string PostData { get; set; }
        public int? TransferID { get; set; }
        public bool IsAjaxRequest { get; set; }
        public string UserAgent { get; set; }
        public string QueryString { get; set; }
        public string Url { get; set; }
    }
}
