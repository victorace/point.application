using System;

namespace Point.Log4Net
{
    public interface ILogger
    {
        string Name { get; }
        int Debug(LogParameters parameters);
        int Debug(string message, Exception exception = null);
        int Debug(Exception exception);
        int DebugFormat(LogParameters parameters, params object[] args);
        int Error(LogParameters parameters);
        int Error(string message, Exception exception = null);
        int Error(Exception exception);
        int ErrorFormat(LogParameters parameters, params object[] args);
        int Fatal(LogParameters parameters);
        int Fatal(string message, Exception exception = null);
        int Fatal(Exception exception);
        int FatalFormat(LogParameters parameters, params object[] args);
        int Info(LogParameters parameters);
        int Info(string message, Exception exception = null);
        int Info(Exception exception);
        int InfoFormat(LogParameters parameters, params object[] args);
        int Warning(LogParameters parameters);
        int Warning(string message, Exception exception = null);
        int Warning(Exception exception);
        int WarningFormat(LogParameters parameters, params object[] args);
        bool IsDebugEnabled { get; }
        bool IsErrorEnabled { get; }
        bool IsFatalEnabled { get; }
        bool IsInfoEnabled { get; }
        bool IsWarnEnabled { get; }
    }
}