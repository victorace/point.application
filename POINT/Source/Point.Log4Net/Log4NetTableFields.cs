﻿
namespace Point.Log4Net
{
    public static class FieldsDefaultAdoNetAppender
    {      
        public const string ClassName = "className";
        public const string Cookies = "cookies";
        public const string EmployeeID = "employeeID";
        public const string EmployeeUserName = "employeeUserName";
        public const string ErrorCode = "errorCode";
        public const string ErrorNumber = "errorNumber";
        public const string IsAjaxRequest = "isAjaxRequest";
        public const string MachineName = "machineName";
        public const string MethodName = "methodName";
        public const string LineNumber = "lineNumber";
        public const string PostData = "postData";
        public const string QueryString = "queryString";
        public const string Stacktrace = "stacktrace";
        public const string TransferID = "transferID";
        public const string Url = "url";
        public const string UserAgent = "userAgent";
        public const string WindowsIdentity = "windowsIdentity";
    }
}
