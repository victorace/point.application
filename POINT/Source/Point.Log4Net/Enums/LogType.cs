﻿
namespace Point.Log4Net.Enums
{
    public enum LogType
    {
        Info,
        InfoFormat,
        Debug,
        DebugFormat,
        Error,
        ErrorFormat,
        Fatal,
        FatalFormat,
        Warning,
        WarningFormat
    }
}
