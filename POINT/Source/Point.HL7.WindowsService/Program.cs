﻿using Point.Database.Context;
using Point.HL7.Processing.Context;
using Point.HL7.Processing.Helpers;
using System.Data.Entity.Infrastructure.Interception;
using System.ServiceProcess;

namespace Point.HL7.WindowsService
{
    static class Program
    {
        static void Main()
        {
            System.Data.Entity.Database.SetInitializer<HL7Context>(null);
            var useReadUncommited = ConfigHelper.GetAppSettingByName<bool>("UseReadUncommited");

            DbInterception.Add(new IsolationLevelInterceptor(useReadUncommited));

            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new PointHL7Service()
            };
            ServiceBase.Run(ServicesToRun);
        }
    }
}
