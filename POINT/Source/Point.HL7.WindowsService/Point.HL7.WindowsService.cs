﻿using Point.HL7.Processing;
using System.ServiceProcess;

namespace Point.HL7.WindowsService
{
    public partial class PointHL7Service : ServiceBase
    {
        public PointHL7Service()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            var hl7servicehost = new HL7ServiceHost();
            hl7servicehost.Start();
        }

        protected override void OnStop()
        {
        }
    }
}
