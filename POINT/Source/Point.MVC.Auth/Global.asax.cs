﻿using Point.Infrastructure;
using Point.Log4Net;
using Point.MVC.Auth.Context;
using System;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Point.MVC.Auth
{
    public class MvcApplication : HttpApplication
    {
        public static ILogger Logger = LogManager.GetLogger(typeof(MvcApplication));

        protected void Application_Error(object sender, EventArgs e)
        {
            var exception = Server.GetLastError();

            Logger.Error(exception);

            if (exception is HttpException)
            {
                int httpcode = ((HttpException)exception).GetHttpCode();
                if (httpcode == (int)HttpStatusCode.NotFound)
                {
                    // handle invalid urls (invalid control or action name)
                    HttpContext.Current.Response.RedirectToRoute(new
                    {
                        controller = "Error",
                        action = "NotFound"
                    });
                    HttpContext.Current.Response.End();
                }
                // https://github.com/Sustainsys/Saml2/issues/698
                else if (exception.Message.StartsWith("The controller for path '/Saml2/"))
                {
                    HttpContext.Current.Response.RedirectToRoute(new
                    {
                        controller = "Login",
                        action = "Index"
                    });
                    HttpContext.Current.Response.End();
                }
            }
            else if (exception is Sustainsys.Saml2.Exceptions.UnexpectedInResponseToException)
            {
                // https://github.com/Sustainsys/Saml2/issues/666
                HttpContext.Current.Response.RedirectToRoute(new
                {
                    controller = "Login",
                    action = "Index"
                });
                HttpContext.Current.Response.End();
            }
        }

        protected void Application_Start()
        {
            System.Data.Entity.Database.SetInitializer<AuthContext>(null);

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        protected void Session_Start(Object sender, EventArgs e)
        {
            // Dummy entry to fix session is always new see https://forums.asp.net/t/1520827.aspx
            // https://github.com/Sustainsys/Saml2/issues/698
            Session["RunSession"] = "1";
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            Utils.ClearHeaders(Response);
        }
    }
}
