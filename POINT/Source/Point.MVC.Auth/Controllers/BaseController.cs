﻿using Microsoft.Owin.Security;
using Point.Infrastructure.Extensions;
using Point.Log4Net;
using System.Configuration;
using System.IO;
using System.Net;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace Point.MVC.Auth.Controllers
{
    public class BaseController : Controller
    {
        protected IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        protected bool IsAuthenticated
        {
            get
            {
                if (User?.Identity != null)
                {
                    return User.Identity.IsAuthenticated;
                }
                return false;
            }
        }

        protected ClaimsIdentity ClaimsIdentity
        {
            get
            {
                if (IsAuthenticated)
                {
                    return HttpContext.User.Identity as ClaimsIdentity;
                }

                throw new System.Exception("No Identity found.");
            }
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            if (filterContext.ExceptionHandled)
            {
                return;
            }

            var forceerrorhandling = ConfigurationManager.AppSettings.GetValueOrDefault<bool>("ForceErrorHandling", false);
            if (!HttpContext.IsDebuggingEnabled || forceerrorhandling)
            {
                string postdata = null;
                if (filterContext.HttpContext?.Request?.InputStream != null)
                {
                    var streamreader = new StreamReader(filterContext.HttpContext.Request.InputStream);
                    postdata = streamreader.ReadToEnd();
                }

                string cookies = null;
                if (filterContext.HttpContext?.Request?.Cookies != null)
                {
                    foreach (var cookiekey in filterContext.HttpContext.Request.Cookies.AllKeys)
                    {
                        if (cookiekey == ".POINTAUTH" || cookiekey == ".POINTROLES" || cookiekey == ".AspNet.Cookies" || cookiekey == "ASP.NET_SessionId")
                        {
                            continue;
                        }

                        var httpCookie = filterContext.HttpContext.Request.Cookies[cookiekey];
                        if (httpCookie != null)
                        {
                            cookies += $"{cookiekey}={httpCookie.Value}&";
                        }
                    }
                }

                filterContext.HttpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                filterContext.ExceptionHandled = true;
                int errorNumber = MvcApplication.Logger.Error(new LogParameters {
                    Exception = filterContext?.Exception,
                    Message = filterContext?.Exception?.Message,
                    Url = Request?.Url?.ToString(),
                    QueryString = Request?.QueryString?.ToString(),
                    PostData = postdata,
                    Cookies = cookies
                });
                filterContext.Result = new RedirectResult($"~/error/errorpage?errorNumber={errorNumber}");
            }
        }
    }
}