﻿using Point.MVC.Auth.Infrastructure;
using Point.MVC.Auth.Infrastructure.Configuration;
using Point.MVC.Auth.Infrastructure.Exceptions;
using Point.MVC.Auth.Infrastructure.Extensions;
using Point.MVC.Auth.Infrastructure.SSO;
using Point.MVC.Auth.Logic;
using Point.MVC.Auth.Logic.API;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Point.MVC.Auth.Controllers
{
    public class LoginController : BaseController
    {
        [AllowAnonymous]
        public ActionResult Index(int? organizationID, string patExternalReference, string returnUrl)
        {
            var authenticationTypes = AuthenticationManager.GetAuthenticationTypes();
            var viewModel = AuthenticationTypeViewBL.Get(authenticationTypes);
            if (organizationID.HasValue)
            {
                var authenticationType = viewModel.FirstOrDefault(at => at.OrganizationID == organizationID);
                if (authenticationType != null)
                {
                    return RedirectToAction("ExternalLogin", new
                    {
                        organizationid = organizationID.Value,
                        authenticationtype = authenticationType.AuthenticationType,
                        patExternalReference,
                        returnUrl
                    });
                }
            }
            return View(viewModel);
        }

        // Unsolicited Responses
        //[Authorize]
        public ActionResult SSO()
        {
            if (IsAuthenticated)
            {
                var organizationID = ClaimsIdentity.GetOrganizationID();
                return RedirectToAction("ExternalLoginCallback", new
                {
                    organizationID,
                    unsolicitedresponse = true
                });
            }

            return RedirectToAction("Index");
        }

        [AllowAnonymous]
        public ActionResult ExternalLogin(int organizationID, string authenticationType, string patExternalReference, string returnUrl)
        {
            if (string.IsNullOrEmpty(authenticationType))
            {
                throw new ArgumentNullException(nameof(authenticationType));
            }

            return new ChallengeResult(authenticationType, Url.Action("ExternalLoginCallback", new { organizationID, patExternalReference, returnUrl }));
        }

        [Authorize]
        public async Task<ActionResult> ExternalLoginCallback(int organizationID, string patExternalReference, string returnUrl, bool unsolicitedresponse = false)
        {
            if (IsAuthenticated)
            {
                if (allowedInPoint())
                {
                    var externID = ClaimsIdentity.GetExternID();
                    if (await employeeExistsInPoint(organizationID, externID))
                    {
                        if (!string.IsNullOrEmpty(returnUrl) || allowedInPointWithoutPatientNumber(patExternalReference))
                        {
                            var sso = SSOHelper.GenerateSSOLoginUrl(organizationID, externID, patExternalReference, returnUrl);
                            return Redirect(sso.URL);
                        }

                        throw new LoginException($"Authentication not allowed when a patiëntnummer is not supplied for OrganizationID[{organizationID}].");
                    }

                    if (OrganizationBL.AllowAutoCreate(organizationID))
                    {
                        if (ClaimsIdentity.HasClaim(c => c.Type == Constants.POINT_Claim_AutoCreateCode && 
                            !string.IsNullOrEmpty(c.Value)))
                        {
                            return RedirectToAction("Create", "Account", new { patExternalReference, returnUrl, unsolicitedresponse });
                        }

                        throw new LoginException($"No valid AutoCreateCode claim found for Organizaiton: {organizationID}.");
                    }

                    throw new LoginException($"AutoCreateAccount not allowed for Organization: {organizationID}.");
                }

                throw new LoginException($"Authentication not allowed for Organization {organizationID}.");
            }

            return RedirectToAction("Index", new { patExternalReference });
        }

        [AllowAnonymous]
        public ActionResult SignOut()
        {
            // TODO: Shouldn't the SignOut trigger come from POINT? How to handle this? Do we know the AuthenticationType? - PP
            //if (HttpContext.User.Identity.IsAuthenticated)
            //{
            //    HttpContext.GetOwinContext().Authentication.SignOut(
            //        CookieAuthenticationDefaults.AuthenticationType, 
            //        OpenIdConnectAuthenticationDefaults.AuthenticationType);
            //}
            return RedirectToAction("Index");
        }

        private bool allowedInPoint()
        {
            var organizationid = ClaimsIdentity.GetOrganizationID();
            var providerelement = AuthConfigurationSectionHelper.ByOrganizationID(organizationid);
            var usepointroles = providerelement.Roles.OfType<RoleElement>()
                .Where(re => !string.IsNullOrEmpty(re.UsePoint))
                .Select(re => re.UsePoint);
            if (!usepointroles.Any())
            {
                return true;
            }

            var roles = ClaimsIdentity.FindAll(ClaimTypes.Role);
            return roles.Any(r => usepointroles.Contains(r.Value));
        }

        private bool allowedInPointWithoutPatientNumber(string patExternalReference)
        {
            var organizationid = ClaimsIdentity.GetOrganizationID();
            var providerelement = AuthConfigurationSectionHelper.ByOrganizationID(organizationid);
            var usepointwithoutexternid = providerelement.Roles.OfType<RoleElement>()
                .Where(re => !string.IsNullOrEmpty(re.UsePointWithoutExternID))
                .Select(re => re.UsePointWithoutExternID);
            if (!usepointwithoutexternid.Any())
            {
                return true;
            }

            var roles = ClaimsIdentity.FindAll(ClaimTypes.Role);
            return roles.Any(r => usepointwithoutexternid.Contains(r.Value));
        }

        private static async Task<bool> employeeExistsInPoint(int organizationID, string externID)
        {
            var formServiceClient = new FormServiceClient();
            return await formServiceClient.EmployeeExistsAsync(organizationID, externID);
        }
    }
}