﻿using Point.MVC.Auth.Models.ViewModels;
using System.Web.Mvc;

namespace Point.MVC.Auth.Controllers
{
    public class ErrorController : BaseController
    {
        public ActionResult ErrorPage(ErrorViewModel model)
        {
            return View(model);
        }

        public ActionResult NotFound()
        {
            return View();
        }

    }
}