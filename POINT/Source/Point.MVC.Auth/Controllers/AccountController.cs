﻿using Point.Infrastructure.Attributes;
using Point.Infrastructure.Extensions;
using Point.MVC.Auth.Infrastructure.Exceptions;
using Point.MVC.Auth.Infrastructure.Extensions;
using Point.MVC.Auth.Logic.API;
using Point.MVC.Auth.Models.API;
using Point.MVC.Auth.Models.ViewModels;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Point.MVC.Auth.Controllers
{
    public class AccountController : BaseController
    {
        [Authorize]
        [HttpGet]
        public ActionResult Create(string patExternalReference, string returnUrl, bool unsolicitedresponse)
        {
            var values = Enum.GetValues(typeof(Gender)).Cast<Gender>()
                .Select(e => new { Text = e.GetDescription(), Value = e.ToString("G") });
            ViewBag.GenderList = new SelectList(values, "Value", "Text", "o");

            var employeeAutoCreate = ClaimsIdentity.ToEmployeeAutoCreate();

            var accountCreateViewModel = new AccountCreateViewModel() { PatExternalReference = patExternalReference, ReturnUrl = returnUrl, UnsolicitedResponse = unsolicitedresponse };
            accountCreateViewModel.Merge(employeeAutoCreate);

            return View(accountCreateViewModel);
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateAntiFiddleInjection("OrganizationID")]
        public async Task<ActionResult> Create(AccountCreateViewModel accountCreateViewModel)
        {
            if (ModelState.IsValid)
            {
                var employeeAutoCreate = ClaimsIdentity.ToEmployeeAutoCreate();
                // This form has been made read only - so we take no further action against the AccountCreateViewModel
                employeeAutoCreate.Merge(accountCreateViewModel);

                var formServiceClient = new FormServiceClient();
                var employeeResponse = await formServiceClient.EmployeeAutoCreateAsync(employeeAutoCreate);
                if (employeeResponse.EmployeeID == 0)
                {
                    throw new AutoCreateException($"Failed to create Employee using AutoCreateCode[{employeeAutoCreate.AutoCreateCode}]. Check the table LogCreatUser for a more detailed error message.");
                }

                MvcApplication.Logger.Info($"ExternID[{employeeResponse.ExternID}] created with EmployeeID[{employeeResponse.EmployeeID}].");

                var departmentexternids = ClaimsIdentity.FindAll(c => c.Type == Constants.POINT_Claim_DepartmentExternID)
                    .Select(c => c.Value).ToList();
                if (departmentexternids.Any())
                {
                    var departmentids = await formServiceClient.EmployeeAddToDepartmentsAsync(new EmployeeDepartment()
                    {
                        EmployeeID = employeeResponse.EmployeeID,
                        OrganizationID = accountCreateViewModel.OrganizationID,
                        DepartmentExternIDs = departmentexternids
                    });

                    MvcApplication.Logger.Info($"ExternID[{employeeResponse.ExternID}] with EmployeeID[{employeeResponse.EmployeeID}] has been attached to the following DepartmentID[{string.Join(",", departmentids)}].");
                }

                return RedirectToAction("Success", new { accountCreateViewModel.PatExternalReference, accountCreateViewModel.ReturnUrl, accountCreateViewModel.UnsolicitedResponse });
            }

            throw new AutoCreateException($"ModelState is invalid. AccountCreateViewModel = {accountCreateViewModel.ToString()}");
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult Error()
        {
            return View();
        }

        [Authorize]
        [HttpGet]
        public ActionResult Success(string patExternalReference, string returnUrl, bool unsolicitedresponse)
        {
            var organizationID = ClaimsIdentity.GetOrganizationID();

            var accountCreateSuccessViewModel = new AccountCreateSuccessViewModel() {
                OrganizationID = organizationID,
                PatExternalReference = patExternalReference,
                ReturnUrl = returnUrl,
                UnsolicitedResponse = unsolicitedresponse
            };

            return View(accountCreateSuccessViewModel);
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateAntiFiddleInjection("OrganizationID")]
        public ActionResult Success(AccountCreateSuccessViewModel accountCreateSuccessViewModel)
        {
            return RedirectToAction("index", "login", new {
                accountCreateSuccessViewModel.OrganizationID,
                accountCreateSuccessViewModel.PatExternalReference,
                accountCreateSuccessViewModel.ReturnUrl
            });
        }
    }
}