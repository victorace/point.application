﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.Caching;

namespace Point.MVC.Auth.Infrastructure.Configuration
{
    public static class AuthConfigurationSectionHelper
    {
        // If you use MemoryCache.Default then your cache lives as long as your 
        // application pool lives. (Just to remind you that default application pool idle
        // time-out is 20 minutes which is less than 1 hour.)
        private static readonly MemoryCache _cache = MemoryCache.Default;

        public static ProviderElement ByOrganizationID(int organizationid)
        {
            var key = $"ProviderElement.{ organizationid}";
            if (!_cache.Contains(key))
            {
                var providerelement = GetProviders().FirstOrDefault(pe => pe.OrganizationID == organizationid);
                var cacheItemPolicy = new CacheItemPolicy()
                {
                    AbsoluteExpiration = DateTime.Now.AddHours(4)
                };
                _cache.Add(key, providerelement, cacheItemPolicy);
            }

            return _cache.Get(key) as ProviderElement;
        }

        public static IEnumerable<ProviderElement> GetProviders()
        {
            var key = "AllProviderElements";
            if (!_cache.Contains(key))
            {
                var authconfigurationsection = getConfiguration();
                var properties = authconfigurationsection.GetType().GetProperties();
                var providerelements = new List<ProviderElement>();
                foreach (var propertyinfo in properties)
                {
                    var isproviderelement = propertyinfo.PropertyType.GetInterfaces()
                        .Where(p => p.IsGenericType && p.GetGenericTypeDefinition() == typeof(IEnumerable<>))
                        .Any(p => p.GetGenericArguments()[0] == typeof(ProviderElement));
                    if (isproviderelement)
                    {
                        var providercollection = propertyinfo.GetValue(authconfigurationsection, null) as IEnumerable<ProviderElement>;
                        if (providercollection != null)
                        {
                            foreach (var provider in providercollection)
                            {
                                providerelements.Add(provider);
                            }
                        }
                    }
                }
                var cacheItemPolicy = new CacheItemPolicy()
                {
                    AbsoluteExpiration = DateTime.Now.AddHours(4)
                };
                _cache.Add(key, providerelements.ToList(), cacheItemPolicy);
            }

            return _cache.Get(key) as IEnumerable<ProviderElement>;
        }

        private static AuthConfigurationSection getConfiguration()
        {
            var authconfigurationsection = ConfigurationManager.GetSection("point.auth") as AuthConfigurationSection;
            if (authconfigurationsection == null)
            {
                throw new ConfigurationErrorsException("The Web.config is missing a 'point.auth' configuration element.");
            }
            return authconfigurationsection;
        }
    }
}