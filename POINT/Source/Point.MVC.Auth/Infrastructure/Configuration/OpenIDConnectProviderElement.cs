﻿using System.Configuration;

namespace Point.MVC.Auth.Infrastructure.Configuration
{
    public class OpenIDConnectProviderElement : ProviderElement
    {
        [ConfigurationProperty("clientid", IsRequired = true)]
        public ConfigurationTextElement<string> ClientId
        {
            get
            {
                return (ConfigurationTextElement<string>)this["clientid"];
            }
        }

        [ConfigurationProperty("clientsecret", IsRequired = true)]
        public ConfigurationTextElement<string> ClientSecret
        {
            get
            {
                return (ConfigurationTextElement<string>)this["clientsecret"];
            }
        }

        [ConfigurationProperty("authority", IsRequired = true)]
        public ConfigurationTextElement<string> Authority
        {
            get
            {
                return (ConfigurationTextElement<string>)this["authority"];
            }
        }

        [ConfigurationProperty("tokenaddress", IsRequired = true)]
        public ConfigurationTextElement<string> TokenAddress
        {
            get
            {
                return (ConfigurationTextElement<string>)this["tokenaddress"];
            }
        }

        [ConfigurationProperty("userinfoendpoint", IsRequired = true)]
        public ConfigurationTextElement<string> UserInfoEndpoint
        {
            get
            {
                return (ConfigurationTextElement<string>)this["userinfoendpoint"];
            }
        }

        [ConfigurationProperty("redirecturi", IsRequired = true)]
        public ConfigurationTextElement<string> RedirectUri
        {
            get
            {
                return (ConfigurationTextElement<string>)this["redirecturi"];
            }
        }

        [ConfigurationProperty("postlogoutredirecturi", IsRequired = true)]
        public ConfigurationTextElement<string> PostLogoutRedirectUri
        {
            get
            {
                return (ConfigurationTextElement<string>)this["postlogoutredirecturi"];
            }
        }

        [ConfigurationProperty("responsetype", IsRequired = true)]
        public ConfigurationTextElement<string> ResponseType
        {
            get
            {
                return (ConfigurationTextElement<string>)this["responsetype"];
            }
        }

        [ConfigurationProperty("scope", IsRequired = true)]
        public ConfigurationTextElement<string> Scope
        {
            get
            {
                return (ConfigurationTextElement<string>)this["scope"];
            }
        }
    }
}