﻿using System.Configuration;

namespace Point.MVC.Auth.Infrastructure.Configuration
{
    public class Saml2ProviderElement : ProviderElement
    {
        [ConfigurationProperty("entityid", IsRequired = true, IsKey = true)]
        public string EntityID
        {
            get
            {
                return (string)base["entityid"];
            }
        }

        [ConfigurationProperty("singlesignonserviceurl", IsRequired = true)]
        public ConfigurationTextElement<string> SingleSignOnServiceUrl
        {
            get
            {
                return (ConfigurationTextElement<string>)this["singlesignonserviceurl"];
            }
        }

        [ConfigurationProperty("singlelogoutserviceurl", IsRequired = true)]
        public ConfigurationTextElement<string> SingleLogoutServiceUrl
        {
            get
            {
                return (ConfigurationTextElement<string>)this["singlelogoutserviceurl"];
            }
        }

        [ConfigurationProperty("singlelogoutserviceresponseurl", IsRequired = true)]
        public ConfigurationTextElement<string> SingleLogoutServiceResponseUrl
        {
            get
            {
                return (ConfigurationTextElement<string>)this["singlelogoutserviceresponseurl"];
            }
        }

        [ConfigurationProperty("signingkey", IsRequired = true)]
        public ConfigurationTextElement<string> SigningKey
        {
            get
            {
                return (ConfigurationTextElement<string>)this["signingkey"];
            }
        }

        [ConfigurationProperty("serviceprovider", IsRequired = true)]
        public ServiceProviderElement ServiceProvider
        {
            get
            {
                return (ServiceProviderElement)base["serviceprovider"];
            }
        }
    }
}