﻿using System.Configuration;

namespace Point.MVC.Auth.Infrastructure.Configuration
{
    public class ServiceProviderElement : ConfigurationElement
    {
        [ConfigurationProperty("entityid", IsRequired = true)]
        public string EntityID
        {
            get
            {
                return (string)base["entityid"];
            }
        }

        [ConfigurationProperty("returnurl", IsRequired = true)]
        public ConfigurationTextElement<string> ReturnURL
        {
            get { return (ConfigurationTextElement<string>)this["returnurl"]; }
        }

        [ConfigurationProperty("servicecertificate")]
        public ConfigurationTextElement<string> ServiceCertificate
        {
            get
            {
                return (ConfigurationTextElement<string>)this["servicecertificate"];
            }
        }
    }
}