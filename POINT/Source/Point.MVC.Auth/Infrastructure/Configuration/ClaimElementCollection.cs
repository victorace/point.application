﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace Point.MVC.Auth.Infrastructure.Configuration
{
    [ConfigurationCollection(typeof(ClaimElement), AddItemName = "claim", CollectionType = ConfigurationElementCollectionType.BasicMapAlternate)]
    public class ClaimElementCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new ClaimElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            if (element == null)
            {
                throw new ArgumentNullException(nameof(element));
            }
            var claimelement = element as ClaimElement;
            return claimelement.DestinationType;
        }

        public new IEnumerator<ClaimElement> GetEnumerator()
        {
            return base.GetEnumerator().AsGeneric<ClaimElement>();
        }
    }
}