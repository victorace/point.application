﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace Point.MVC.Auth.Infrastructure.Configuration
{
    [ConfigurationCollection(typeof(RoleElement), AddItemName = "role", CollectionType = ConfigurationElementCollectionType.BasicMapAlternate)]
    public class RoleElementCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new RoleElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            if (element == null)
            {
                throw new ArgumentNullException(nameof(element));
            }
            return element as RoleElement;
        }

        public new IEnumerator<RoleElement> GetEnumerator()
        {
            return base.GetEnumerator().AsGeneric<RoleElement>();
        }
    }
}