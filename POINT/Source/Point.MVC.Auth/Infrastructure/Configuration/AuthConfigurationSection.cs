﻿using Point.Infrastructure.Extensions;
using System;
using System.Configuration;
using System.Linq;

namespace Point.MVC.Auth.Infrastructure.Configuration
{
    public class AuthConfigurationSection : ConfigurationSection
    {
        protected override void PostDeserialize()
        {
            var duplicates = OpenIDConnectProviders
                .Concat(Saml2Providers).GetDuplicates(pe => pe.OrganizationID);
            if (duplicates.Any())
            {
                throw new ArgumentException($"Duplicate OrganizationIDs [{string.Join(",", duplicates)}].");
            }

            base.PostDeserialize();
        }

        [ConfigurationProperty("openidconnectproviders")]
        public OpenIDConnectProviderElementCollection OpenIDConnectProviders
        {
            get
            {
                return (OpenIDConnectProviderElementCollection)base["openidconnectproviders"];
            }
        }

        [ConfigurationProperty("saml2providers")]
        public Saml2ProviderElementCollection Saml2Providers
        {
            get
            {
                return (Saml2ProviderElementCollection)base["saml2providers"];
            }
        }
    }
}