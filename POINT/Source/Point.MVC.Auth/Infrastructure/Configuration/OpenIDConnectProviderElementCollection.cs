﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace Point.MVC.Auth.Infrastructure.Configuration
{
    [ConfigurationCollection(typeof(OpenIDConnectProviderElement), AddItemName = "openidconnectprovider", CollectionType = ConfigurationElementCollectionType.BasicMapAlternate)]
    public class OpenIDConnectProviderElementCollection : ConfigurationElementCollection, IEnumerable<ProviderElement>
    {
        protected override object GetElementKey(ConfigurationElement element)
        {
            if (element == null)
            {
                throw new ArgumentNullException(nameof(element));
            }
            var openidconnectproviderelement = element as OpenIDConnectProviderElement;
            return openidconnectproviderelement.AuthenticationType;
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new OpenIDConnectProviderElement();
        }

        public new IEnumerator<ProviderElement> GetEnumerator()
        {
            return base.GetEnumerator().AsGeneric<OpenIDConnectProviderElement>();
        }
    }
}