﻿using System;
using System.Configuration;

namespace Point.MVC.Auth.Infrastructure.Configuration
{
    public class RoleElement : ConfigurationElement
    {
        protected override void PostDeserialize()
        {
            if (string.IsNullOrEmpty(UsePoint) && string.IsNullOrEmpty(UsePointWithoutExternID))
            {
                throw new ArgumentException($"A role element can not be empty. Please provide a 'UsePoint' or 'UsePointWithoutExternID' value.");
            }

            base.PostDeserialize();
        }

        [ConfigurationProperty("usepoint", DefaultValue = "UsePoint")]
        public string UsePoint
        {
            get
            {
                return (string)base["usepoint"];
            }
        }

        [ConfigurationProperty("usepointwithoutexternid", DefaultValue = "UsePointWithoutExternID")]
        public string UsePointWithoutExternID
        {
            get
            {
                return (string)base["usepointwithoutexternid"];
            }
        }
    }
}