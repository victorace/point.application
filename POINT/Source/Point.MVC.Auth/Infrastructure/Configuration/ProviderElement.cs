﻿using System.Configuration;

namespace Point.MVC.Auth.Infrastructure.Configuration
{
    public class ProviderElement : ConfigurationElement
    {
        [ConfigurationProperty("authenticationtype", IsRequired = true, IsKey = true)]
        public string AuthenticationType
        {
            get
            {
                return (string)base["authenticationtype"];
            }
        }

        [ConfigurationProperty("unsolicited", IsRequired = false)]
        public bool Unsolicited
        {
            get
            {
                return (bool)this["unsolicited"];
            }
        }

        [ConfigurationProperty("organizationid", IsRequired = true)]
        public int OrganizationID
        {
            get
            {
                return (int)this["organizationid"];
            }
        }

        [ConfigurationProperty("caption", IsRequired = true)]
        public string Caption
        {
            get
            {
                return (string)this["caption"];
            }
        }

        [ConfigurationProperty("minincomingsigningalgorithm")]
        public string MinIncomingSigningAlgorithm
        {
            get
            {
                return (string)this["minincomingsigningalgorithm"];
            }
        }

        [ConfigurationProperty("claims")]
        public ClaimElementCollection Claims
        {
            get
            {
                return (ClaimElementCollection)this["claims"];
            }
        }

        [ConfigurationProperty("roles")]
        public RoleElementCollection Roles
        {
            get
            {
                return (RoleElementCollection)this["roles"];
            }
        }
    }
}