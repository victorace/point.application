﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace Point.MVC.Auth.Infrastructure.Configuration
{
    [ConfigurationCollection(typeof(Saml2ProviderElement), AddItemName = "saml2provider", CollectionType = ConfigurationElementCollectionType.BasicMapAlternate)]
    public class Saml2ProviderElementCollection : ConfigurationElementCollection, IEnumerable<ProviderElement>
    {
        protected override object GetElementKey(ConfigurationElement element)
        {
            if (element == null)
            {
                throw new ArgumentNullException(nameof(element));
            }
            var saml2providerelement = element as Saml2ProviderElement;
            return saml2providerelement.AuthenticationType;
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new Saml2ProviderElement();
        }

        public new IEnumerator<ProviderElement> GetEnumerator()
        {
            return base.GetEnumerator().AsGeneric<Saml2ProviderElement>();
        }
    }
}