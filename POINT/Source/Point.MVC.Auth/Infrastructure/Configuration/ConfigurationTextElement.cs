﻿using System.Configuration;
using System.Xml;

namespace Point.MVC.Auth.Infrastructure.Configuration
{
    // https://www.codeproject.com/Articles/50117/Extend-ConfigurationElement-to-Deserialize-Text-El
    public class ConfigurationTextElement<T> : ConfigurationElement
    {
        protected override void DeserializeElement(XmlReader reader, bool serializeCollectionKey)
        {
            Value = (T)reader.ReadElementContentAs(typeof(T), null);
        }

        public T Value { get; private set; }
    }
}