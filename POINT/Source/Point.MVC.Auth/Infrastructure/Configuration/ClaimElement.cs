﻿using System;
using System.Configuration;

namespace Point.MVC.Auth.Infrastructure.Configuration
{
    public class ClaimElement : ConfigurationElement
    {
        protected override void PostDeserialize()
        {
            if (string.IsNullOrEmpty(SourceType) && string.IsNullOrEmpty(DefaultValue))
            {
                throw new ArgumentException($"The claim '{DestinationType}' has no default value.");
            }

            base.PostDeserialize();
        }

        [ConfigurationProperty("sourcetype")]
        public string SourceType
        {
            get
            {
                return (string)base["sourcetype"];
            }
        }

        [ConfigurationProperty("destinationtype", IsRequired = true, IsKey = true)]
        public string DestinationType
        {
            get
            {
                return (string)base["destinationtype"];
            }
        }

        [ConfigurationProperty("defaultvalue")]
        public string DefaultValue
        {
            get
            {
                return (string)base["defaultvalue"];
            }
        }
    }
}