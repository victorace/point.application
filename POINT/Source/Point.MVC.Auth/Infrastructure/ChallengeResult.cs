﻿using Microsoft.Owin.Security;
using System.Web;
using System.Web.Mvc;

namespace Point.MVC.Auth.Infrastructure
{
    internal class ChallengeResult : HttpUnauthorizedResult
    {
        public string AuthenticationType { get; set; }
        public string RedirectUri { get; set; }

        public ChallengeResult(string authenticationType, string redirectUri)
        {
            AuthenticationType = authenticationType;
            RedirectUri = redirectUri;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
            context.HttpContext.GetOwinContext().Authentication.Challenge(properties, AuthenticationType);
        }
    }
}