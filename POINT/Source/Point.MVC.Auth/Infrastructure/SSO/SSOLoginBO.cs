﻿using Flurl;
using System;
using System.Security.Cryptography;

namespace Point.MVC.Auth.Infrastructure.SSO
{
    public class SSOLoginBO
    {
        public string WebServer { get; set; }
        public string ExternalReference { get; set; }
        public string OrgExternalReference { get; set; }
        public string AutoCreateCode { get; set; }
        public DateTime Date { get; set; }
        public string DateString { get; set; }
        public string Sign { get; set; }
        public string EmplReference { get; set; }
        public string DepartmentReference { get; set; }
        public string PatExternalReference { get; set; }
        public string PatExternalReferenceBSN { get; set; }
        public string ClientIP { get; set; }
        public bool IsAuth { get; set; }

        //Note: only used for reading the parameters on it since almost all customers (still pass this when logging in). Not used for "redirecting" 
        public string ReturnUrl { get; set; }

        public string URL
        {
            get
            {
                var url = WebServer.AppendPathSegments(new string[] { "Login", "Index" }).ToString();
                SetQueryParameters(ref url);
                SetSearchValue(ref url);
                return url;
            }
        }

        public int OrganizationID
        {
            get
            {
                int organizationID;
                if (!int.TryParse(OrgExternalReference, out organizationID))
                {
                    organizationID = 0;
                }
                return organizationID;
            }
        }

        private static HashAlgorithm GetAlgorithmFromString(string algorithm)
        {
            HashAlgorithm hashAlgorithm = new SHA256Managed();
            if (String.IsNullOrEmpty(algorithm) || algorithm.Equals("SHA1", StringComparison.InvariantCultureIgnoreCase))
            {
                hashAlgorithm = new SHA1Managed();
            }
            return hashAlgorithm;
        }

        /// <summary>
        /// This is the value used in the FlowInstanceSearchValues lookup function.
        /// </summary>
        private void SetSearchValue(ref string uri)
        {
            if (uri == null)
            {
                throw new ArgumentNullException(nameof(uri));
            }
            if (!string.IsNullOrEmpty(PatExternalReference))
            {
                uri = uri.SetQueryParam("patexternalreference1", PatExternalReference);
            }
            if (!string.IsNullOrEmpty(PatExternalReferenceBSN))
            {
                uri = uri.SetQueryParam("patexternalreferencebsn1", PatExternalReferenceBSN);
            }
        }

        private void SetQueryParameters(ref string uri)
        {
            if (uri == null)
            {
                throw new ArgumentNullException(nameof(uri));
            }
            uri = uri.SetQueryParam("date", Date.ToString(Constants.SIGN_DATE_FORMAT))
                .SetQueryParam("sign", Sign)
                .SetQueryParam("emplreference1", "")
                .SetQueryParam("departmentReference1", "")
                .SetQueryParam("externalreference1", ExternalReference)
                .SetQueryParam("orgexternalreference1", OrgExternalReference)
                .SetQueryParam("returnUrl", Url.Encode(ReturnUrl))
                .SetQueryParam("IsAuth", true);
        }
    }
}