﻿using Point.Infrastructure.Exceptions;
using Point.MVC.Auth.Logic;
using System;
using System.Configuration;
using System.Security.Cryptography;
using System.Text;

namespace Point.MVC.Auth.Infrastructure.SSO
{
    public static class SSOHelper
    {
        public static SSOLoginBO GenerateSSOLoginUrl(int organizationID, string externID, string patExternalReference, string returnUrl)
        {
            if (string.IsNullOrEmpty(externID))
            {
                throw new ArgumentNullOrEmptyException(nameof(externID));
            }

            var organization = OrganizationBL.GetByID(organizationID);
            if (organization == null)
            {
                throw new Exception($"No Organization found with the OrganizationID '{organizationID}'.");
            }
            if (organization.Inactive)
            {
                throw new Exception($"OrganizationID '{organization.OrganizationID}' is disabled.");
            }
            if (organization.SSO == false)
            {
                throw new Exception($"OrganizationID '{organization.OrganizationID}' has SSO disabled.");
            }
            if (string.IsNullOrEmpty(organization.SSOKey))
            {
                throw new Exception($"OrganizationID '{organization.OrganizationID} has no SSOKey.");
            }

            return GenerateSSOLogin(organization.SSOHashType, organization.SSOKey, organization.OrganizationID, externID, DateTime.Now, patExternalReference, null, returnUrl);
        }
        private static string CalculatedSign(string algorithm, string ssoKey, int organizationID, string externID, DateTime timestamp)
        {
            if (string.IsNullOrEmpty(ssoKey))
            {
                throw new ArgumentNullOrEmptyException(nameof(ssoKey));
            }
            if (organizationID <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(organizationID));
            }
            if (string.IsNullOrEmpty(externID))
            {
                throw new ArgumentNullOrEmptyException(nameof(externID));
            }

            var hashAlgorithm = GetAlgorithmFromString(algorithm);
            byte[] hashBytes = Encoding.UTF8.GetBytes($"{ssoKey}_{organizationID}_{externID}_{timestamp.ToString(Constants.SIGN_DATE_FORMAT)}");
            byte[] computedHash = hashAlgorithm.ComputeHash(hashBytes);
            string calculatedSign = "";
            foreach (byte b in computedHash)
            {
                calculatedSign += b.ToString("X2");
            }
            return calculatedSign.ToLower();
        }

        private static SSOLoginBO GenerateSSOLogin(string algorithm, string ssoKey, int organizationID, string externID, DateTime timestamp, string patientNumber, string patientBSN, string returnUrl)
        {
            var webServer = ConfigurationManager.AppSettings["WebServer"] as string;
            if (string.IsNullOrEmpty(webServer))
            {
                throw new Exception("'WebServer' configuration is missing.");
            }

            var sign = CalculatedSign(algorithm, ssoKey, organizationID, externID, timestamp);
            var ssoLoginBO = new SSOLoginBO()
            {
                WebServer = webServer,
                OrgExternalReference = organizationID.ToString(),
                ExternalReference = externID,
                Date = timestamp,
                Sign = sign,
                PatExternalReference = patientNumber,
                PatExternalReferenceBSN = patientBSN,
                IsAuth = true,
                ReturnUrl = returnUrl
            };
            return ssoLoginBO;
        }

        private static HashAlgorithm GetAlgorithmFromString(string algorithm)
        {
            HashAlgorithm hashAlgorithm = new SHA256Managed();
            if (String.IsNullOrEmpty(algorithm) || algorithm.Equals("SHA1", StringComparison.InvariantCultureIgnoreCase))
            {
                hashAlgorithm = new SHA1Managed();
            }
            return hashAlgorithm;
        }
    }
}