﻿using System;
using Point.Log4Net;

namespace Point.MVC.Auth.Infrastructure.Logging
{
    public class SustainsysSaml2Log4netLoggerAdapter : Sustainsys.Saml2.ILoggerAdapter
    {
        public static ILogger _logger;

        public SustainsysSaml2Log4netLoggerAdapter(ILogger logger)
        {
            _logger = logger;
        }

        public void WriteError(string message, Exception ex)
        {
            _logger?.Error(message, ex);
        }

        public void WriteInformation(string message)
        {
            _logger?.Info(message);
        }

        public void WriteVerbose(string message)
        {
            _logger?.Debug(message);
        }
    }
}