﻿using System;

namespace Point.MVC.Auth.Infrastructure.Exceptions
{
    public class AutoCreateException : Exception
    {
        public AutoCreateException()
        {
        }

        public AutoCreateException(string message)
            : base(message)
        {
        }

        public AutoCreateException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}