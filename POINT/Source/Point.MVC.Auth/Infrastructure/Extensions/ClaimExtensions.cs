﻿using Point.MVC.Auth.Infrastructure.Configuration;
using Point.MVC.Auth.Models.API;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using Point.Log4Net;
using Point.Infrastructure.Extensions;

namespace Point.MVC.Auth.Infrastructure.Extensions
{
    public static class ClaimExtensions
    {
        public static ILogger Logger = LogManager.GetLogger(typeof(ClaimExtensions));

        public static void Map(this ClaimsIdentity claimsidentity, ProviderElement providerelement)
        {
            if (claimsidentity == null)
            {
                throw new ArgumentNullException(nameof(claimsidentity));
            }

            if (providerelement == null)
            {
                throw new ArgumentNullException(nameof(providerelement));
            }

            foreach (var claim in providerelement.Claims)
            {
                if (string.IsNullOrEmpty(claim.SourceType))
                {
                    claimsidentity.AddClaim(new Claim(claim.DestinationType, claim.DefaultValue));
                }
                else
                {
                    var sourceclaims = claimsidentity.FindAll(claim.SourceType);
                    if (!sourceclaims.Any())
                    {
                        claimsidentity.AddClaim(new Claim(claim.DestinationType, claim.DefaultValue));
                    }
                    else
                    {
                        foreach (var sourceclaim in sourceclaims)
                        {
                            if (claimsidentity.TryRemoveClaim(sourceclaim))
                            {
                                claimsidentity.AddClaim(new Claim(claim.DestinationType, sourceclaim.Value, sourceclaim.ValueType));
                            }
                            else
                            {
                                Logger.Error($"'{providerelement.AuthenticationType}' unable to remove claim '{sourceclaim.Type}'.");
                            }
                        }
                    }
                }
            }
        }

        public static void Map(this ClaimsIdentity claimsidentity, ProviderElement providerelement, JwtSecurityToken jwtsecuritytoken)
        {
            if (claimsidentity == null)
            {
                throw new ArgumentNullException(nameof(claimsidentity));
            }

            if (providerelement == null)
            {
                throw new ArgumentNullException(nameof(providerelement));
            }

            if (jwtsecuritytoken == null)
            {
                throw new ArgumentNullException(nameof(jwtsecuritytoken));
            }

            foreach (var claim in jwtsecuritytoken.Claims)
            {
                var sourceclaims = claimsidentity.FindAllByClaim(claim);

                if (!sourceclaims.Any())
                {
                    claimsidentity.AddClaim(claim);
                }
            }

            claimsidentity.Map(providerelement);
        }

        public static T GetValue<T>(this ClaimsIdentity claimsidentity, string match, T defaultvalue)
        {
            if (claimsidentity == null)
            {
                throw new ArgumentNullException(nameof(claimsidentity));
            }

            Type type = Nullable.GetUnderlyingType(typeof(T)) ?? typeof(T);
            if (claimsidentity != null && !string.IsNullOrEmpty(match))
            {
                try
                {
                    var claim = claimsidentity.FindFirst(match);
                    if (claim != null)
                    {
                        return claim.Value.ConvertTo<T>();
                    }
                }
                catch { /* return default value */ }
            }
            return defaultvalue == null ? default(T) : defaultvalue.ConvertTo<T>();
        }

        public static IEnumerable<Claim> FindAllByClaim(this ClaimsIdentity claimsidentity, Claim claim)
        {
            if (claimsidentity == null)
            {
                throw new ArgumentNullException(nameof(claimsidentity));
            }

            if (claim == null)
            {
                throw new ArgumentNullException(nameof(claim));
            }

            return claimsidentity.FindAll(c => c.Type == claim.Type && c.Value == claim.Value);
        }

        public static Claim FindFirstByClaim(this ClaimsIdentity claimsidentity, Claim claim)
        {
            if (claimsidentity == null)
            {
                throw new ArgumentNullException(nameof(claimsidentity));
            }

            if (claim == null)
            {
                throw new ArgumentNullException(nameof(claim));
            }

            return claimsidentity.FindFirst(c => c.Type == claim.Type && c.Value == claim.Value);
        }

        public static string GetExternID(this ClaimsIdentity claimsidentity)
        {
            if (claimsidentity == null)
            {
                throw new ArgumentNullException(nameof(claimsidentity));
            }

            var claim = claimsidentity.FindFirst(Constants.POINT_Claim_ExternID);
            if (claim == null)
            {
                throw new Exception($"No Claim found matching '{Constants.POINT_Claim_ExternID}'.");
            }
            if (string.IsNullOrEmpty(claim.Value))
            {
                throw new Exception($"Claim '{Constants.POINT_Claim_ExternID}' must not be null or empty.");
            }

            return claim.Value;
        }

        public static int GetOrganizationID(this ClaimsIdentity claimsidentity)
        {
            if (claimsidentity == null)
            {
                throw new ArgumentNullException(nameof(claimsidentity));
            }

            var claim = claimsidentity.FindFirst(Constants.POINT_Claim_OrganizationID);
            if (claim == null)
            {
                throw new Exception($"No Claim found matching '{Constants.POINT_Claim_OrganizationID}'.");
            }
            if (string.IsNullOrEmpty(claim.Value))
            {
                throw new Exception($"Claim '{Constants.POINT_Claim_OrganizationID}' must not be null or empty.");
            }

            return int.Parse(claim.Value);
        }

        public static EmployeeAutoCreate ToEmployeeAutoCreate(this ClaimsIdentity claimsidentity)
        {
            if (claimsidentity == null)
            {
                throw new ArgumentNullException(nameof(claimsidentity));
            }

            var employeeAutoCreate = new EmployeeAutoCreate()
            {
                OrganizationID = claimsidentity.GetValue(Constants.POINT_Claim_OrganizationID, -1),
                AutoCreateCode = claimsidentity.GetValue<string>(Constants.POINT_Claim_AutoCreateCode, null),
                ExternID = claimsidentity.GetValue<string>(Constants.POINT_Claim_ExternID, null),
                FirstName = claimsidentity.GetValue<string>(Constants.POINT_Claim_FirstName, null),
                MiddleName = claimsidentity.GetValue<string>(Constants.POINT_Claim_MiddleName, null),
                LastName = claimsidentity.GetValue<string>(Constants.POINT_Claim_LastName, null),
                Gender = claimsidentity.GetValue<string>(Constants.POINT_Claim_Gender, null),
                Function = claimsidentity.GetValue<string>(Constants.POINT_Claim_Function, null),
                PhoneNumber = claimsidentity.GetValue<string>(Constants.POINT_Claim_PhoneNumber, null),
                Email = claimsidentity.GetValue<string>(Constants.POINT_Claim_Email, null),
                BIG = claimsidentity.GetValue<string>(Constants.POINT_Claim_BIG, null),
            };

            return employeeAutoCreate;
        }

        public static void Log(this ClaimsIdentity claimsidentity, ProviderElement providerelement)
        {
            if (claimsidentity != null && Logger.IsDebugEnabled)
            {
                var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                try
                {
                    var claimlog = new
                    {
                        providerelement.AuthenticationType,
                        providerelement.OrganizationID,
                        Claims = from c in claimsidentity.Claims orderby c.Type
                                 select new
                                 {
                                     c.Type,
                                     c.ValueType,
                                     c.Value
                                 }
                    };

                    string message = serializer.Serialize(claimlog);
                    Logger.Debug(message);
                }
                catch (Exception e)
                {
                    Logger.Fatal(e);
                }
            }
        }
    }
}