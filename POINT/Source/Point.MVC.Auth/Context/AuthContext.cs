﻿using Point.MVC.Auth.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Point.MVC.Auth.Context
{
    public partial class AuthContext : DbContext
    {
        public AuthContext() : base("name=AuthContext")
        {
        }

        public virtual DbSet<Organization> Organization { get; set; }
        public virtual DbSet<Department> Department { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

        public void ModelCreatedFromDatabase(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Organization>()
                .HasMany(e => e.Departments)
                .WithRequired(e => e.Organization)
                .HasForeignKey(e => e.OrganizationID)
                .WillCascadeOnDelete(false);
        }
    }
}
