﻿using System.ComponentModel;

namespace Point.MVC.Auth
{
    public enum Gender //Using char in the database
    {
        [Description("Man")] m = 1,
        [Description("Vrouw")] v = 2,
        [Description("Onbekend/Anders")] o = 3,
    }
}