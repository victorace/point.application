﻿using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Owin;
using Point.Log4Net;
using Point.MVC.Auth.Infrastructure.Configuration;
using System;
using System.Linq;
using System.Security.Claims;
using System.Web.Helpers;

namespace Point.MVC.Auth
{
    public partial class Startup
    {
        public static ILogger Logger = LogManager.GetLogger(typeof(Startup));

        public void ConfigureAuth(IAppBuilder app)
        {
            // https://github.com/Sustainsys/Saml2/issues/698
            // https://coding.abel.nu/2014/11/catching-the-system-webowin-cookie-monster/
            // Still doesn't fix the problem!
            app.UseKentorOwinCookieSaver();

            app.SetDefaultSignInAsAuthenticationType(CookieAuthenticationDefaults.AuthenticationType);
            app.UseCookieAuthentication(new CookieAuthenticationOptions {
                LoginPath = new PathString("/Login/Index"),
                CookieName = Constants.COOKIE_NAME
            });

            var providerelements = AuthConfigurationSectionHelper.GetProviders();
            ConfigureOpenIdConnect(app, providerelements.OfType<OpenIDConnectProviderElement>());
            ConfigureSaml(app, providerelements.OfType<Saml2ProviderElement>());

            AntiForgeryConfig.UniqueClaimTypeIdentifier = Constants.POINT_Claim_ExternID;
        }

        private void addPOINTClaims(ClaimsIdentity claimsidentity, ProviderElement providerelement)
        {
            if (claimsidentity == null)
            {
                throw new ArgumentNullException(nameof(claimsidentity));
            }

            if (providerelement == null)
            {
                throw new ArgumentNullException(nameof(providerelement));
            }

            if (!claimsidentity.HasClaim(Constants.POINT_Claim_OrganizationID, providerelement.OrganizationID.ToString()))
            {
                claimsidentity.AddClaim(new Claim(Constants.POINT_Claim_OrganizationID, providerelement.OrganizationID.ToString(), Constants.ClaimType_Int));
            }
        }
    }
}