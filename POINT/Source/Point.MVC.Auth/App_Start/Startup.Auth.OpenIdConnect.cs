﻿using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Owin.Extensions;
using Microsoft.Owin.Security.OpenIdConnect;
using Owin;
using Point.MVC.Auth.Infrastructure.Configuration;
using Point.MVC.Auth.Infrastructure.Extensions;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;

namespace Point.MVC.Auth
{
    public partial class Startup
    {
        private void ConfigureOpenIdConnect(IAppBuilder app, IEnumerable<OpenIDConnectProviderElement> openidelementproviders)
        {
            foreach (var openidelementprovider in openidelementproviders)
            {
                try
                {
                    var openidprovider = CreateOpenIdConnectionAuthenticationOptions(openidelementprovider as OpenIDConnectProviderElement);
                    app.UseOpenIdConnectAuthentication(openidprovider).UseStageMarker(PipelineStage.PostAcquireState);
                }
                catch (Exception e)
                {
                    throw new Exception($"Failed to add ({openidelementprovider.AuthenticationType}) OpenIdConnect provider.", e);
                }
            }
        }

        private OpenIdConnectAuthenticationOptions CreateOpenIdConnectionAuthenticationOptions(OpenIDConnectProviderElement openidconnectprovider)
        {
            var openidconnectauthenticationoptions = new OpenIdConnectAuthenticationOptions
            {
                AuthenticationType = openidconnectprovider.AuthenticationType,
                Caption = openidconnectprovider.Caption,
                ClientId = openidconnectprovider.ClientId.Value,
                ClientSecret = openidconnectprovider.ClientSecret.Value,
                Authority = openidconnectprovider.Authority.Value,
                RedirectUri = openidconnectprovider.RedirectUri.Value,
                ResponseType = openidconnectprovider.ResponseType.Value,
                Scope = openidconnectprovider.Scope.Value,
                PostLogoutRedirectUri = openidconnectprovider.PostLogoutRedirectUri.Value,
                TokenValidationParameters = new TokenValidationParameters
                {
                    NameClaimType = "name",
                },
                SignInAsAuthenticationType = "Cookies",
                Notifications = new OpenIdConnectAuthenticationNotifications
                {
                    SecurityTokenValidated = n =>
                    {
                        var ticket = n.AuthenticationTicket;

                        addPOINTClaims(ticket.Identity, openidconnectprovider);

                        if (openidconnectprovider.ResponseType.Value != OpenIdConnectResponseType.Code)
                        {
                            ticket.Identity.Map(openidconnectprovider);

                            ticket.Identity.Log(openidconnectprovider);
                        }

                        return Task.CompletedTask;
                    },
                    AuthorizationCodeRedeemed = n =>
                    {
                        var ticket = n.AuthenticationTicket;

                        // store tokens for later use
                        var id_token = ticket.Properties.GetTokenValue(Constants.OpenIdConnect_Claim_IDToken);
                        var access_token = ticket.Properties.GetTokenValue(Constants.OpenIdConnect_Claim_AccessToken);
                        var refresh_token = ticket.Properties.GetTokenValue(Constants.OpenIdConnect_Claim_RefreshToken);

                        var claims = new List<Claim>();
                        claims.Add(new Claim(Constants.OpenIdConnect_Claim_IDToken, id_token));
                        claims.Add(new Claim(Constants.OpenIdConnect_Claim_AccessToken, access_token));

                        if (!string.IsNullOrEmpty(refresh_token))
                        {
                            claims.Add(new Claim(Constants.OpenIdConnect_Claim_RefreshToken, refresh_token));
                        }

                        // TODO: Is this necessary? - PP
                        var httpContext = HttpContext.Current;
                        Dictionary<string, string> oidc = new Dictionary<string, string>
                        {
                            {Constants.OpenIdConnect_Claim_IDToken, id_token},
                            {Constants.OpenIdConnect_Claim_AccessToken, access_token},
                            {Constants.OpenIdConnect_Claim_RefreshToken, refresh_token}
                        };

                        httpContext.Session.Add(Constants.OpenIdConnect_Session_Key, oidc);

                        ticket.Identity.AddClaims(claims);

                        ticket.Identity.Log(openidconnectprovider);

                        try
                        {
                            var jwtsecuritytoken = new JwtSecurityToken(oidc[Constants.OpenIdConnect_Claim_AccessToken]);
                            ticket.Identity.Map(openidconnectprovider, jwtsecuritytoken);
                        }
                        catch (Exception e)
                        {
                            Logger.Fatal(e);
                        }

                        ticket.Identity.Log(openidconnectprovider);

                        return Task.CompletedTask;
                    },
                    RedirectToIdentityProvider = n =>
                    {
                        // If signing out, add the id_token_hint
                        if (n.ProtocolMessage.RequestType == OpenIdConnectRequestType.Logout)
                        {
                            var idTokenClaim = n.OwinContext.Authentication.User.FindFirst(Constants.OpenIdConnect_Claim_IDToken);
                            if (idTokenClaim != null)
                            {
                                n.ProtocolMessage.IdTokenHint = idTokenClaim.Value;
                            }
                        }
                        return Task.CompletedTask;
                    }
                }
            };

            return openidconnectauthenticationoptions;
        }
    }
}