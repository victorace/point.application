﻿using Owin;
using Point.MVC.Auth.Infrastructure.Configuration;
using Point.MVC.Auth.Infrastructure.Extensions;
using Point.MVC.Auth.Infrastructure.Logging;
using Sustainsys.Saml2;
using Sustainsys.Saml2.Configuration;
using Sustainsys.Saml2.Owin;
using Sustainsys.Saml2.WebSso;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IdentityModel.Metadata;
using System.Security.Claims;
using System.Security.Cryptography.X509Certificates;
using System.Web.Hosting;

namespace Point.MVC.Auth
{
    public partial class Startup
    {
        private void ConfigureSaml(IAppBuilder app, IEnumerable<Saml2ProviderElement> saml2elementproviders)
        {
            foreach (var saml2elementprovider in saml2elementproviders)
            {
                try
                {
                    var saml2authenticationoptions = CreateSaml2AuthenticationOptions(saml2elementprovider);
                    app.UseSaml2Authentication(saml2authenticationoptions);
                }
                catch (Exception e)
                {
                    throw new Exception($"Failed to add ({saml2elementprovider.AuthenticationType}) Saml2 provider.", e);
                }
            }
        }

        private Saml2AuthenticationOptions CreateSaml2AuthenticationOptions(Saml2ProviderElement saml2provider)
        {
            var serviceprovider = saml2provider.ServiceProvider;
            var spoptions = CreateSPOptions(saml2provider);

            var saml2authenticationoptions = new Saml2AuthenticationOptions(false) { SPOptions = spoptions };
            saml2authenticationoptions.AuthenticationType = saml2provider.AuthenticationType;
            saml2authenticationoptions.Caption = saml2provider.Caption;

            saml2authenticationoptions.Notifications = new Saml2Notifications
            {
                AcsCommandResultCreated = (cr, r) =>
                {
                    if (r.MessageName == "SAMLResponse" && r.Status == Sustainsys.Saml2.Saml2P.Saml2StatusCode.Success)
                    {
                        var claimsidentity = cr.Principal.Identity as ClaimsIdentity;

                        addPOINTClaims(claimsidentity, saml2provider);

                        claimsidentity.Map(saml2provider);

                        claimsidentity.Log(saml2provider);
                    }
                }
            };

            var identityprovider = new IdentityProvider(new EntityId(saml2provider.EntityID), spoptions)
            {
                AllowUnsolicitedAuthnResponse = saml2provider.Unsolicited,
                Binding = Saml2BindingType.HttpRedirect,
                SingleSignOnServiceUrl = new Uri(saml2provider.SingleSignOnServiceUrl.Value),
                SingleLogoutServiceUrl = new Uri(saml2provider.SingleLogoutServiceUrl.Value)
            };

            if (!string.IsNullOrEmpty(saml2provider.SingleLogoutServiceResponseUrl.Value))
            {
                identityprovider.SingleLogoutServiceResponseUrl = new Uri(saml2provider.SingleLogoutServiceResponseUrl.Value);
            }

            string filename = HostingEnvironment.MapPath(saml2provider.SigningKey.Value);
            identityprovider.SigningKeys.AddConfiguredKey(new X509Certificate2(filename));

            saml2authenticationoptions.IdentityProviders.Add(identityprovider);

            return saml2authenticationoptions;
        }

        private SPOptions CreateSPOptions(Saml2ProviderElement saml2provider)
        {
            var spOptions = new SPOptions
            {
                // As far as I can tell, the Entity ID is required to be an absolute URI (but not necessarily a URL).
                // The entity id used in the<issuer> element is mentioned in the SAML Core spec in section 2.2.5.It references section 8.3.6:
                // https://www.oasis-open.org/committees/download.php/35711/sstc-saml-core-errata-2.0-wd-06-diff.pdf
                EntityId = new EntityId(saml2provider.ServiceProvider.EntityID),
                Logger = new SustainsysSaml2Log4netLoggerAdapter(Logger)
            };

            // http://www.w3.org/2000/09/xmldsig#rsa-sha1
            if (!string.IsNullOrEmpty(saml2provider.MinIncomingSigningAlgorithm))
            {
                spOptions.MinIncomingSigningAlgorithm = saml2provider.MinIncomingSigningAlgorithm;
            }

            spOptions.SystemIdentityModelIdentityConfiguration.AudienceRestriction =
                new System.IdentityModel.Tokens.AudienceRestriction(System.IdentityModel.Selectors.AudienceUriMode.Never);

            // ReturnURL https://authdev.verzorgdeoverdracht.nl/Saml2/<modulepath>/Acs
            spOptions.ModulePath = saml2provider.AuthenticationType;
            Uri baseUri = new Uri(saml2provider.ServiceProvider.ReturnURL.Value);
            if (saml2provider.Unsolicited)
            {
                spOptions.ReturnUrl = new Uri(baseUri, "Login/SSO");
            }
            else
            {
                spOptions.ReturnUrl = new Uri(baseUri, $"Saml2/{saml2provider.AuthenticationType}/Acs");
            }

            if (!string.IsNullOrEmpty(saml2provider.ServiceProvider.ServiceCertificate.Value))
            {
                // Without a ServiceCertificate AuthenticationManager.SignOut() fails
                // https://github.com/Sustainsys/Saml2/issues/494
                string filename = HostingEnvironment.MapPath(saml2provider.ServiceProvider.ServiceCertificate.Value);
                spOptions.ServiceCertificates.Add(new X509Certificate2(filename));
            }

            var dutch = CultureInfo.GetCultureInfo("nl-nl");
            var organization = new Organization();
            organization.Names.Add(new LocalizedName("Linkassist", dutch));
            organization.DisplayNames.Add(new LocalizedName("Linkassist", dutch));
            organization.Urls.Add(new LocalizedUri(new Uri("https://www.verzorgdeoverdracht.nl/"), dutch));
            spOptions.Organization = organization;

            var contactperson = new ContactPerson(ContactType.Support);
            contactperson.EmailAddresses.Add("support@linkassist.nl");
            contactperson.TelephoneNumbers.Add("0235574400");
            spOptions.Contacts.Add(contactperson);

            return spOptions;
        }
    }
}