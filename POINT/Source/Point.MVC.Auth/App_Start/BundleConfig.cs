﻿using System.Web.Optimization;

namespace Point.MVC.Auth
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/scripts/jquery").Include(
                "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/scripts/jqueryval").Include(
                "~/Scripts/jquery.validate*",
                "~/Scripts/jquery.validate.unobtrusive"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/scripts/modernizr").Include(
                "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/scripts/bootstrap").Include(
                "~/Scripts/bootstrap.js",
                "~/Scripts/respond.js",
                "~/Scripts/IE8Compat.js"));

            bundles.Add(new ScriptBundle("~/bundles/scripts/BootstrapIE8").Include(
                "~/Scripts/html5shiv.js",
                "~/Scripts/respond.js"
            ));

            bundles.Add(new StyleBundle("~/content/styles/css").Include(
                "~/Content/Bootstrap/bootstrap.css"));
        }
    }
}