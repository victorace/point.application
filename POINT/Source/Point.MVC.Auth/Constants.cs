﻿namespace Point.MVC.Auth
{
    public static class Constants
    {
        public const string COOKIE_NAME = "Point.Auth";

        public const string SIGN_DATE_FORMAT = "yyyyMMddHHmmss";

        public const string Role_UsePoint                   = "UsePoint";
        public const string ROLE_UsePointWithoutExternID    = "UsePointWithoutExternID";

        public const string POINT_Claim_Namespace = "http://verzorgdeoverdracht.nl/identity/claims/";

        public const string ClaimType_String = "http://www.w3.org/2001/XMLSchema#string";
        public const string ClaimType_Int = "http://www.w3.org/2001/XMLSchema#int";

        public const string POINT_Claim_OrganizationID = "http://verzorgdeoverdracht.nl/identity/claims/organizationID"; // int
        public const string POINT_Claim_ExternID = "http://verzorgdeoverdracht.nl/identity/claims/externid";
        public const string POINT_Claim_Username = "http://verzorgdeoverdracht.nl/identity/claims/username";
        public const string POINT_Claim_FirstName = "http://verzorgdeoverdracht.nl/identity/claims/firstname"; 
        public const string POINT_Claim_MiddleName = "http://verzorgdeoverdracht.nl/identity/claims/middlename"; 
        public const string POINT_Claim_LastName = "http://verzorgdeoverdracht.nl/identity/claims/lastname"; 
        public const string POINT_Claim_Gender = "http://verzorgdeoverdracht.nl/identity/claims/gender"; 
        public const string POINT_Claim_Function = "http://verzorgdeoverdracht.nl/identity/claims/function"; 
        public const string POINT_Claim_PhoneNumber = "http://verzorgdeoverdracht.nl/identity/claims/phonenumber";
        public const string POINT_Claim_Department = "http://verzorgdeoverdracht.nl/identity/claims/department";
        public const string POINT_Claim_Email = "http://verzorgdeoverdracht.nl/identity/claims/email"; 
        public const string POINT_Claim_BIG = "http://verzorgdeoverdracht.nl/identity/claims/big"; 
        public const string POINT_Claim_AutoCreateCode = "http://verzorgdeoverdracht.nl/identity/claims/autocreatecode";
        public const string POINT_Claim_Role = "http://verzorgdeoverdracht.nl/identity/claims/role";
        public const string POINT_Claim_DepartmentExternID = "http://verzorgdeoverdracht.nl/identity/claims/departmentexternid";
        public const string POINT_Claim_PatientNumber = "http://verzorgdeoverdracht.nl/identity/claims/patientnumber";

        // TODO: Remove namespace from the claims - PP
        // - azMaastricht has been contacted as they currently provide claims like this
        public const string Saml2_Claim_OrganizationID = "http://www.w3.org/2001/XMLSchema#int/organizationid";
        public const string Saml2_Claim_ExternID = "http://www.w3.org/2001/XMLSchema#string/externid";
        public const string Saml2_Claim_Gebruikersnaam = "http://www.w3.org/2001/XMLSchema#string/gebruikersnaam";
        public const string Saml2_Claim_FirstName = "http://www.w3.org/2001/XMLSchema#string/voornaam";
        public const string Saml2_Claim_MiddleName = "http://www.w3.org/2001/XMLSchema#string/tussenvoegsel";
        public const string Saml2_Claim_LastName = "http://www.w3.org/2001/XMLSchema#string/achternaam";
        public const string Saml2_Claim_Gender = "http://www.w3.org/2001/XMLSchema#string/geslacht";
        public const string Saml2_Claim_Function = "http://www.w3.org/2001/XMLSchema#string/functie";
        public const string Saml2_Claim_PhoneNumber = "http://www.w3.org/2001/XMLSchema#string/telefoonnummer";
        public const string Saml2_Claim_Department = "http://www.w3.org/2001/XMLSchema#string/afdeling";
        public const string Saml2_Claim_Email = "http://www.w3.org/2001/XMLSchema#string/email";
        public const string Saml2_Claim_BIG = "http://www.w3.org/2001/XMLSchema#string/big";
        public const string Saml2_Claim_AutoCreateCode = "http://www.w3.org/2001/XMLSchema#string/profiel";
        public const string Saml2_Claim_Role = "http://schemas.microsoft.com/ws/2008/06/identity/claims/role";

        public const string OpenIdConnect_Session_Key = "oidc";
        public const string OpenIdConnect_Claim_IDToken = "id_token";
        public const string OpenIdConnect_Claim_AccessToken = "access_token";
        public const string OpenIdConnect_Claim_RefreshToken = "refresh_token";
        public const string OpenIdConnect_Claim_PreferredUsername = "preferred_username";
        public const string OpenIdConnect_Claim_GivenName = "given_name";
        public const string OpenIdConnect_Claim_FamilyName = "family_name";
        public const string OpenIdConnect_Claim_Gender = "gender";
        public const string OpenIdConnect_Claim_PhoneNumber = "primary_phone";
        public const string OpenIdConnect_Claim_Function = "function";
        public const string OpenIdConnect_Claim_AutoCreateCode = "autocreate_code";
    }
}