﻿using Flurl;
using Flurl.Http;
using Point.Infrastructure.Extensions;
using Point.MVC.Auth.Models.API;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Threading.Tasks;

namespace Point.MVC.Auth.Logic.API
{
    public class FormServiceClient
    {
        private const string HEADER_APIKEY = "apikey";
        private string apiURL;
        private string apiKey;

        public FormServiceClient()
        {
            apiURL = ConfigurationManager.AppSettings.GetValueOrDefault<string>("apiurl", null);
            apiKey = ConfigurationManager.AppSettings.GetValueOrDefault<string>("apikey", null);

            if (string.IsNullOrEmpty(apiURL))
            {
                throw new ArgumentException("No API baseurl configuration found.");
            }

            if (string.IsNullOrEmpty(apiKey))
            {
                throw new ArgumentException("No API key configuration found.");
            }
        }

        public async Task<bool> EmployeeExistsAsync(int organizationID, string employeeExternID)
        {
            string url = apiURL.AppendPathSegments("employee", "exists").SetQueryParams(new { employeeExternID, organizationID });
            var employeeResponseYN = await url.WithHeader(HEADER_APIKEY, apiKey).GetJsonAsync<EmployeeResponseYN>();
            return employeeResponseYN.Result;
        }

        public async Task<EmployeeResponse> EmployeeAutoCreateAsync(EmployeeAutoCreate employeeAutoCreate)
        {
            string url = apiURL.AppendPathSegments("employee", "autocreate");
            var employeeResponse = await url.WithHeader(HEADER_APIKEY, apiKey)
                .PostJsonAsync(employeeAutoCreate)
                .ReceiveJson<EmployeeResponse>();
            return employeeResponse;
        }

        public async Task<IEnumerable<int>> EmployeeAddToDepartmentsAsync(EmployeeDepartment employeedepartment)
        {
            string url = apiURL.AppendPathSegments("employee", "addtodepartment");
            return await url.WithHeader(HEADER_APIKEY, apiKey)
                .PostJsonAsync(employeedepartment)
                .ReceiveJson<IEnumerable<int>>();
        }
    }
}