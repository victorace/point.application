﻿using Microsoft.Owin.Security;
using Point.MVC.Auth.Infrastructure.Configuration;
using Point.MVC.Auth.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Point.MVC.Auth.Logic
{
    public static class AuthenticationTypeViewBL
    {
        public static IEnumerable<AuthenticationTypeViewModel> Get(IEnumerable<AuthenticationDescription> authenticationdescriptions)
        {
            if (authenticationdescriptions == null)
            {
                throw new ArgumentNullException(nameof(authenticationdescriptions));
            }

            var authenticationtypes = new List<AuthenticationTypeViewModel>();

            var providerelements = AuthConfigurationSectionHelper.GetProviders();

            add(authenticationtypes, authenticationdescriptions, providerelements.OfType<OpenIDConnectProviderElement>());
            add(authenticationtypes, authenticationdescriptions, providerelements.OfType<Saml2ProviderElement>());

            return authenticationtypes;
        }

        private static void add(List<AuthenticationTypeViewModel> authenticationtypes, IEnumerable<AuthenticationDescription> authenticationdescriptions, IEnumerable<ProviderElement> providers)
        {
            if (authenticationtypes == null)
            {
                throw new ArgumentNullException(nameof(authenticationtypes));
            }

            if (authenticationdescriptions == null)
            {
                throw new ArgumentNullException(nameof(authenticationdescriptions));
            }

            if (providers == null)
            {
                throw new ArgumentNullException(nameof(providers));
            }

            foreach (var provider in providers.Where(p => p.Unsolicited == false))
            {
                if (authenticationdescriptions.Any(ad => ad.AuthenticationType == provider.AuthenticationType))
                {
                    authenticationtypes.Add(new AuthenticationTypeViewModel()
                    {
                        OrganizationID = provider.OrganizationID,
                        AuthenticationType = provider.AuthenticationType,
                        Caption = provider.Caption
                    });
                }
            }
        }
    }
}