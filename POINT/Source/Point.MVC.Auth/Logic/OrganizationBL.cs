﻿using Point.MVC.Auth.Context;
using Point.MVC.Auth.Models;
using System.Linq;

namespace Point.MVC.Auth.Logic
{
    public static class OrganizationBL
    {
        public static Organization GetByID(int organizationID)
        {
            Organization organization = null;
            using (var db = new AuthContext())
            {
                organization = db.Organization.Include("Departments").FirstOrDefault(o => o.OrganizationID == organizationID);
            }
            return organization;
        }

        public static bool AllowAutoCreate(int organizationid)
        {
            var organization = GetByID(organizationid);
            if (organization == null)
            {
                return false;
            }

            return organization.SSO && organization.CreateUserViaSSO;
        }
    }
}
