﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Point.MVC.Auth.Models
{
    public class Organization
    {
        public Organization()
        {
            Departments = new HashSet<Department>();
        }

        public int OrganizationID { get; set; }
        public bool SSO { get; set; }
        [StringLength(50)]
        public string SSOKey { get; set; }
        [StringLength(50)]
        public string SSOHashType { get; set; }
        public bool CreateUserViaSSO { get; set; }
        [StringLength(20)]
        public string AGB { get; set; }
        public bool Inactive { get; set; }

        public virtual ICollection<Department> Departments { get; set; }
    }
}