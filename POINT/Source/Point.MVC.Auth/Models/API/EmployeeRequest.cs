﻿namespace Point.MVC.Auth.Models.API
{
    public class EmployeeRequest
    {
        public int? OrganizationID { get; set; }
        public string OrganizationAGB { get; set; }
        public int? EmployeeID { get; set; }
        public string EmployeeExternID { get; set; }
        public string EmployeeBIG { get; set; }
    }
}