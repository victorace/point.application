﻿namespace Point.MVC.Auth.Models.API
{
    public class EmployeeAutoCreate
    {
        public int OrganizationID { get; set; }
        public string AutoCreateCode { get; set; }
        public string ExternID { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public string Function { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string BIG { get; set; }
    }
}