﻿namespace Point.MVC.Auth.Models.API
{
    public class EmployeeResponse
    {
        public int EmployeeID { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string ExternID { get; set; }
    }
}