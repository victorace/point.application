﻿using System.Collections.Generic;

namespace Point.MVC.Auth.Models.API
{
    public class EmployeeDepartment
    {
        public int EmployeeID { get; set; }
        public int OrganizationID { get; set; }
        public List<string> DepartmentExternIDs { get; set; }
    }
}