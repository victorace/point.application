﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Point.MVC.Auth.Models.ViewModels
{
    public class AccountCreateViewModel
    {
        public int OrganizationID { get; set; }
        [StringLength(50)]
        public string AutoCreateCode { get; set; }
        [StringLength(50)]
        public string ExternID { get; set; }
        [StringLength(255), DisplayName("Voornaam")]
        public string FirstName { get; set; }
        [StringLength(50), DisplayName("Tussenvoegsel")]
        public string MiddleName { get; set; }
        [StringLength(255), Required, DisplayName("Achternaam")]
        public string LastName { get; set; }
        [DisplayName("Geslacht")]
        public Gender Gender { get; set; } = Gender.o;
        [StringLength(255), DisplayName("Functie")]
        public string Function { get; set; }
        [StringLength(50), DisplayName("Telefoonnummer / Piepernummer")]
        public string PhoneNumber { get; set; }
        [StringLength(255), DisplayName("Emailadres")]
        public string Email { get; set; }
        [StringLength(50), DisplayName("BIG nummer")]
        public string BIG { get; set; }
        public string PatExternalReference { get; set; }
        public string ReturnUrl { get; set; }
        public bool UnsolicitedResponse { get; set; }

        public override string ToString()
        {
            var result = string.Empty;
            var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            try {
                result = serializer.Serialize(this);
            }
            catch (Exception e) {
                result = $"ToString failed: {e.ToString()}";
            }
            return result;
        }
    }
}