﻿namespace Point.MVC.Auth.Models.ViewModels
{
    public class AccountCreateSuccessViewModel
    {
        public int OrganizationID { get; set; }
        public string PatExternalReference { get; set; }
        public string ReturnUrl { get; set; }
        public bool UnsolicitedResponse { get; set; }
    }
}