﻿namespace Point.MVC.Auth.Models.ViewModels
{
    public class AuthenticationTypeViewModel
    {
        public int OrganizationID { get; set; }
        public string AuthenticationType { get; set; }
        public string Caption { get; set; }
    }
}