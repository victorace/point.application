﻿using System.ComponentModel.DataAnnotations;

namespace Point.MVC.Auth.Models
{
    public class Department
    {
        public int DepartmentID { get; set; }
        public int OrganizationID { get; set; }
        public virtual Organization Organization { get; set; }
        public int LocationID { get; set; }
        [StringLength(20)]
        public string AGB { get; set; }
        public bool Inactive { get; set; }
    }
}