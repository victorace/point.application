﻿using System;

namespace Point.Processing.Exceptions
{
    public class ExceptionLogException : Exception
    {
        public ExceptionLogException(Exception sourceexception) : base(sourceexception.Message, sourceexception) {}
    }
}
