﻿using System;

namespace Point.Processing.Exceptions
{
    public class InvalidMessageException : Exception
    {
        public InvalidMessageException(string message) : base(message) {}
    }
}
