﻿using System;

namespace Point.Processing.Exceptions
{
    public class DeadLetterMessageException : Exception
    {
        public DeadLetterMessageException(string message) : base(message) { }
    }
}
