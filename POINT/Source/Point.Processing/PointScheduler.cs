﻿using Point.Business.Logic;
using Point.Infrastructure.Helpers;
using Point.Log4Net;
using Point.Processing.Handlers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Timers;
using static Point.Processing.Handlers.ScheduledActions;

namespace Point.Processing
{
    public class PointScheduler
    {
        private static readonly ILogger Logger = LogManager.GetLogger(typeof(PointScheduler));
        private static readonly int intervalminutes = 5;
        private static readonly string ANY_MACHINE = "ANY_MACHINE";

        private List<Schedule> GetSchedules()
        {
            List<Schedule> schedules = new List<Schedule>();

            schedules.AddRange(new[] {
                new Schedule() { Name = "AutoCloseTransfers", Hour = 1, Minute = 15, MethodName = "AutoCloseTransfers" },
                new Schedule() { Name = "UpdatePlanbord", EveryHour = true, Minute = 0, MethodName = "UpdatePlanbord" },
                new Schedule() { Name = "Nedap CheckQueue", Hour = 10, Minute = 0, MethodName = "CheckNedapQueue" },
                new Schedule() { Name = "Nedap CheckQueue", Hour = 15, Minute = 0, MethodName = "CheckNedapQueue" },
                new Schedule() { Name = "Nedap CheckQueue", Hour = 19, Minute = 0, MethodName = "CheckNedapQueue" },
                new Schedule() { Name = "Clear Privacy", Hour = 1, Minute = 0, MethodName = "ClearPrivacyData" },
                new Schedule() { Name = "Send Feedback", Hour = 6, Minute = 0, MethodName = "SendDossierFeedback" },
                new Schedule() { Name = "Update Capacity", Hour = 1, Minute = 30, MethodName = "UpdateCapmanDaily" },
                new Schedule() { Name = "Update Iguana Channelstatus", EveryHour = true, Minute = 0, MethodName = "UpdateIguanaChannelStatus" },
                new Schedule() { Name = "Update Iguana Channelstatus", EveryHour = true, Minute = 15, MethodName = "UpdateIguanaChannelStatus" },
                new Schedule() { Name = "Update Iguana Channelstatus", EveryHour = true, Minute = 30, MethodName = "UpdateIguanaChannelStatus" },
                new Schedule() { Name = "Update Iguana Channelstatus", EveryHour = true, Minute = 45, MethodName = "UpdateIguanaChannelStatus" },
                new Schedule() { Name = "AutoLockEmployee", Hour = 2 , Minute = 0, MethodName = "AutoLockEmployee"},
                new Schedule() { Name = "AutoSignaleringEmail", Hour = 12, Minute = 0 , MethodName = "AutoSignaleringEmail" },
                new Schedule() { Name = "AutoSignaleringEmail", Hour = 16, Minute = 0 , MethodName = "AutoSignaleringEmail" }
            });

            return schedules;
        }

        public void Start()
        {
            var schedulerMachine = ConfigHelper.GetAppSettingByName<string>("SchedulerMachine");
            if (schedulerMachine != Environment.MachineName && schedulerMachine != ANY_MACHINE)
            {
                Logger.Info("The scheduler will (intentionally) not run on this machine (See App.Config SchedulerMachine)");
                return;
            }

            var timer = new Timer
            {
                Interval = 1000 * 60 * intervalminutes //Every x Minutes
            };

            timer.Elapsed += new ElapsedEventHandler(Timer_Elapsed);
            timer.Start();
        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            int hour = DateTime.Now.Hour;
            int minute = DateTime.Now.Minute;

            Logger.Debug($"Scheduletimer Tick: @{DateTime.Now.ToString("dd-MM HH:mm:ss")}.");

            var schedules = GetSchedules().Where(s => (minute >= s.Minute && minute < s.Minute + intervalminutes) && (s.EveryHour || s.Hour == hour)).ToList();
            foreach (var schedule in schedules)
            {
                ServiceBusBL.SendMessage(ScheduledActions.QueueName, schedule.Name, schedule);
            }
        }
    }
}
