﻿using Point.Business.Helper;
using Point.Business.Logic;
using Point.Communication;
using Point.Database.Context;
using Point.Database.Repository;
using Point.Infrastructure.Extensions;
using Point.Models.Enums;
using Point.ServiceBus.EventTypes;
using Point.ServiceBus.Models;
using System.Linq;
using Point.Database.Models;

namespace Point.Processing.Handlers
{
    public class AutoSignaleringHandler
    {
        public const string QueueName = "AutoSignalering";
        public void HandleAutoEmailSignalering(IMessageEvent message)
        {
            if (message.ObjectData is AutoEmailSignalering objectdata)
            {
                using (var uow = new UnitOfWork<PointContext>())
                {
                    var employees = uow.EmployeeRepository.Get(x => x.AutoEmailSignalering);
                    var primaryDepartmentIds = employees?.Select(x => x.DepartmentID)?.Distinct();
                    foreach (var primaryDepartmentId in primaryDepartmentIds)
                    {
                        var signals = uow.SignaleringRepository.Get(s =>
                                      (s.Status == SignaleringStatus.Open) &&
                                      s.CreatedByDepartmentID != primaryDepartmentId
                        );
                        if (signals.Any())
                        {
                            var samePrimaryDepartmentEmployees = employees?.Where(e => e.DepartmentID == primaryDepartmentId);
                            foreach (var emp in samePrimaryDepartmentEmployees)
                            {
                                var employeeSignal = SignaleringBL.GetSignalsByEmployee(uow, emp, signals);
                                if (employeeSignal.SenderSignalering.Any())
                                {
                                    Email.SendRazorMail(uow, null, "AutoSignaleringEmail", employeeSignal, emp.EmailAddress);

                                }
                            }
                        }
                    }
                }
            }
            message.Complete();
        }
    }
}
