﻿using Newtonsoft.Json;
using Point.Business.Logic;
using Point.Database.Context;
using Point.Database.Extensions;
using Point.Database.Models;
using Point.Database.Repository;
using Point.Infrastructure.Constants;
using Point.Infrastructure.Extensions;
using Point.Infrastructure.Helpers;
using Point.Log4Net;
using Point.Models.Enums;
using Point.ServiceBus.EventTypes;
using Point.ServiceBus.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Point.Communication;

namespace Point.Processing.Handlers
{
    public class ScheduledActions
    {
        private static readonly ILogger Logger = LogManager.GetLogger(typeof(ScheduledActions));

        public class Schedule : IPointServiceBusModel
        {
            public string Name { get; set; }
            public int Hour { get; set; }
            public int Minute { get; set; }
            public bool EveryHour { get; set; }
            public string MethodName { get; set; }
        }

        public static string QueueName = "Scheduler";

        public void HandleScheduledActions(IMessageEvent message)
        {
            var fullname = message.FullName;
            var objectdata = message.ObjectData as Schedule;
            Task.Factory.StartNew(() =>
            {
                try
                {
                    Type thisType = this.GetType();
                    MethodInfo theMethod = thisType.GetMethod(objectdata.MethodName);
                    theMethod.Invoke(this, null);
                }
                catch (Exception e)
                {
                    Logger.Fatal(fullname, e);
                }
            });

            message.Complete();
        }

        public void AutoCloseTransfers()
        {
            using (var uow = new UnitOfWork<PointContext>())
            {
                var result = FlowInstanceBL.AutoCloseTransfers(uow, new Dictionary<string, object>());
                Output(result);
            }
        }

        public void UpdatePlanbord()
        {
            using (var uow = new UnitOfWork<PointContext>())
            {
                var planbord = new Communication.Planbord()
                {
                    IgnoreCertificateError = true
                };
                var sendstatusdelegate = new Communication.Planbord.SendStatusDelegate(planbord.SendStatus);

                var flowdefinitionssending = FlowDefinitionBL.GetFlowDefinitionsByFlowType(uow, FlowTypeID.ZH_VVT).Select(fd => fd.FlowDefinitionID).ToArray();
                var activestatuses = new InviteStatus[] { InviteStatus.Active, InviteStatus.Acknowledged, InviteStatus.Realized };

                var planbordorganizations = Enumerable.Empty<VismoOrganization>();
                var vismoorganizations = ConfigHelper.GetAppSettingByName<string>("VismoOrganizations", "");
                if (!string.IsNullOrEmpty(vismoorganizations))
                {
                    planbordorganizations = JsonConvert.DeserializeObject<IEnumerable<VismoOrganization>>(vismoorganizations);
                    foreach (var planbororganization in planbordorganizations)
                    {
                        var searchquerybl = SearchQueryBL.GetSearchQuerySimple(uow);
                        searchquerybl.ApplyFlowDefinitionsFilter(flowdefinitionssending);
                        searchquerybl.ApplyStatusFilter(DossierStatus.Active);
                        searchquerybl.ApplyFilter(fis => fis.FlowInstanceSearchValues.FlowInstanceOrganization.Any(fisvd => activestatuses.Contains(fisvd.InviteStatus) && fisvd.OrganizationID == planbororganization.OrganizationID));

                        var searchresults = searchquerybl.Queryable.ToList();

                        IAsyncResult result = sendstatusdelegate.BeginInvoke(planbororganization.SystemCode, planbororganization.PrivateKey, searchresults, null, null);
                    }
                }

                Output($"Done invoking planbord.SendStatus @{DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss")} for {planbordorganizations.Count()} organizations.");
            }
        }

        public void Output(string message)
        {
            Logger.Info(message);
        }

        public void CheckNedapQueue()
        {
            using (var uow = new UnitOfWork<PointContext>())
            {
                CommunicationQueueBL.SetHandledByDays(uow, 7);

                var unhandledcommunicationqueues = CommunicationQueueBL.GetUnhandledOfTypes(uow, new[] { CommunicationLogType.NedapAttachment, CommunicationLogType.NedapForm, CommunicationLogType.NedapVO });
                if (unhandledcommunicationqueues == null || unhandledcommunicationqueues.Count() == 0)
                {
                    Output($"Done @{DateTime.Now.ToString("dd-MM HH:mm:ss")}. No unhandled CommunicationQueues.");
                }

                foreach (var unhandledcommunicationqueue in unhandledcommunicationqueues)
                {
                    switch (unhandledcommunicationqueue.MailType)
                    {
                        case CommunicationLogType.NedapAttachment:
                            ServiceBusBL.SendMessage(uow, "FlowInstance", "SendTransferAttachment",
                                new SendTransferAttachment()
                                {
                                    TimeStamp = DateTime.Now,
                                    UniqueID = DateTime.Now.Ticks,
                                    TransferID = unhandledcommunicationqueue.TransferID.ConvertTo<int>(),
                                    TransferAttachmentID = unhandledcommunicationqueue.TransferAttachmentID.ConvertTo<int>(),
                                    RecievingDepartmentID = unhandledcommunicationqueue.ReceivingDepartmentID,
                                    RecievingOrganizationID = unhandledcommunicationqueue.ReceivingOrganizationID.ConvertTo<int>(),
                                    SendingEmployeeID = unhandledcommunicationqueue.EmployeeID.ConvertTo<int>(),
                                    MailType = unhandledcommunicationqueue.MailType.ConvertTo<CommunicationLogType>()
                                });
                            break;
                        case CommunicationLogType.NedapForm:
                        case CommunicationLogType.NedapVO:
                            ServiceBusBL.SendMessage(uow, MessageQueues.PhaseQueue, "FormSetVersionNotification",
                                    new FormSetVersionNotification()
                                    {
                                        TimeStamp = DateTime.Now,
                                        UniqueID = DateTime.Now.Ticks,
                                        TransferID = unhandledcommunicationqueue.TransferID.ConvertTo<int>(),
                                        FormSetVersionID = unhandledcommunicationqueue.FormSetVersionID.ConvertTo<int>(),
                                        RecievingDepartmentID = unhandledcommunicationqueue.ReceivingDepartmentID,
                                        RecievingOrganizationID = unhandledcommunicationqueue.ReceivingOrganizationID.ConvertTo<int>(),
                                        SendingEmployeeID = unhandledcommunicationqueue.EmployeeID.ConvertTo<int>(),
                                        MailType = unhandledcommunicationqueue.MailType.ConvertTo<CommunicationLogType>(),
                                        IsVO = (unhandledcommunicationqueue.MailType == CommunicationLogType.NedapVO)
                                    });

                            break;
                    }
                }

                Output($"Done @{DateTime.Now.ToString("dd-MM HH:mm:ss")}. {unhandledcommunicationqueues.Count()} unhandled CommunicationQueues.");
            }
        }

        public void ClearPrivacyData()
        {
            using (var uow = new UnitOfWork<PointContext>())
            {
                var result = LogPrivacyBL.ClearData(uow);
                Output($"Done @{DateTime.Now.ToString("dd-MM HH:mm:ss")}" + Environment.NewLine + result);
            }
        }

        public string GetDossierUrl(int transferID, int phaseDefinitionID, int phaseInstanceID)
        {
            var webserver = ConfigHelper.GetAppSettingByName<string>("WebServer");
            var url = $"{webserver}/FlowMain/StartFlowForm?TransferID={transferID}&PhaseDefinitionID={phaseDefinitionID}&PhaseInstanceID={phaseInstanceID}";

            return url;
        }

        public void SendDossierFeedback()
        {
            using (var uow = new UnitOfWork<PointContext>())
            {
                var dateToday = DateTime.Now.Date;

                HashSet<int> sent = new HashSet<int>();
                HashSet<int> alreadysent = new HashSet<int>();
                HashSet<int> damaged = new HashSet<int>();

                var activefrequencies = FrequencyBL.GetByStatusNotExpired(uow, FrequencyStatus.Active);
                foreach (var frequency in activefrequencies)
                {
                    int frequencyid = frequency.FrequencyID;

                    if (ScheduleHistoryBL.Exists(uow, frequencyid, dateToday))
                    {
                        alreadysent.Add(frequencyid);
                        continue;
                    }

                    var flowinstance = FlowInstanceBL.GetByTransferID(uow, frequency.TransferID);
                    if (flowinstance == null)
                    {
                        damaged.Add(frequencyid);
                        continue;
                    }

                    var flowformtype = frequency.GetFlowFormType();
                    if (flowformtype == FlowFormType.NoScreen)
                    {
                        damaged.Add(frequencyid);
                        continue;
                    }

                    var formtype = FormTypeBL.GetByFormTypeID(uow, (int)flowformtype);
                    if (formtype == null)
                    {
                        damaged.Add(frequencyid);
                        continue;
                    }

                    var phaseinstance = PhaseInstanceBL
                        .GetAllByFlowInstanceIDAndFormTypeID(uow, flowinstance.FlowInstanceID, formtype.FormTypeID)
                        .OrderByDescending(it => it.PhaseInstanceID)
                        .FirstOrDefault();
                    if (phaseinstance == null)
                    {
                        damaged.Add(frequencyid);
                        continue;
                    }

                    var dates = frequency.GetPeriodDates();
                    foreach (var date in dates)
                    {
                        if (date.Date == dateToday.Date)
                        {
                            var organizationid = frequency.Employee?.DefaultDepartment?.Location?.OrganizationID ?? 0;
                            var usesauth = OrganizationSettingBL.GetValue<bool>(uow, organizationid, OrganizationSettingBL.UsesAuth);

                            ServiceBusBL.SendMessage(
                                uow,
                                "Notification",
                                "FrequencyNotification",
                                new FrequencyNotification()
                                {
                                    TimeStamp = DateTime.Now,
                                    UniqueID = DateTime.Now.Ticks,
                                    TransferID = frequency.TransferID,
                                    FrequencyID = frequency.FrequencyID,
                                    Timestamp = dateToday,
                                    Url = GetDossierUrl(frequency.TransferID, phaseinstance.PhaseDefinitionID, phaseinstance.PhaseInstanceID),
                                    RedirectToAuth = usesauth,
                                    AuthOrganizationID = usesauth ? organizationid : (int?)null
                                }
                            );
                            sent.Add(frequencyid);
                        }
                    }
                }

                Output($"Sent:{sent.Count},AlreadySent{alreadysent.Count},Damaged:{damaged.Count}");
            }
        }

        public void UpdateCapmanDaily()
        {
            var result = DepartmentBL.UpdateCapacityPerDay();

            Output(result);
        }

        public void UpdateIguanaChannelStatus()
        {
            using (var uow = new UnitOfWork<PointContext>())
            {
                ServiceBusBL.SendMessage(uow, "Iguana", "UpdateIguanaStatus", new UpdateIguanaStatus());
                Output("Updating Iguanastatus");
            }
        }

        public void AutoLockEmployee()
        {
            using (var uow = new UnitOfWork<PointContext>())
            {
                var message = EmployeeBL.AutoLockEmployee(uow);
                Output(message);
            }
        }

        public void AutoSignaleringEmail()
        {
            //using (var uow = new UnitOfWork<PointContext>())
            //{
            //    ServiceBusBL.SendMessage(uow, AutoSignaleringHandler.QueueName, "AutoEmailSignalering", new AutoEmailSignalering());
            //    Output("Sending signalering email");
            //}

            Output($"AutoSignaleringEmail Start {DateTime.Now.ToString()}.");

            using (var uow = new UnitOfWork<PointContext>())
            {
                IQueryable<Signalering> opensignals = null;

                var employees = uow.EmployeeRepository.Get(x => x.AutoEmailSignalering);
                var primaryDepartmentIds = employees?.Select(x => x.DepartmentID)?.Distinct();
                if (primaryDepartmentIds.Any())
                {
                    opensignals = uow.SignaleringRepository.Get(s => s.Status == SignaleringStatus.Open);
                }

                Output($"AutoSignaleringEmail OpenSignals #{opensignals?.Count()}.");

                foreach (var primaryDepartmentId in primaryDepartmentIds)
                {
                    var signals = opensignals.Where(s => s.CreatedByDepartmentID != primaryDepartmentId);
                    if (signals.Any())
                    {
                        Output($"AutoSignaleringEmail primaryDepartmentId #{primaryDepartmentId}");

                        var samePrimaryDepartmentEmployees = employees?.Where(e => e.DepartmentID == primaryDepartmentId);
                        foreach (var emp in samePrimaryDepartmentEmployees)
                        {
                            Output($"AutoSignaleringEmail samePrimaryDepartmentEmployees #{emp.EmployeeID}");

                            var employeeSignal = SignaleringBL.GetSignalsByEmployee(uow, emp, signals);
                            if (employeeSignal.SenderSignalering.Any())
                            {
                                Email.SendRazorMail(uow, null, "AutoSignaleringEmail", employeeSignal, emp.EmailAddress);
                            }
                        }
                    }
                }
            }

            Output($"AutoSignaleringEmail End {DateTime.Now.ToString()}.");
        }
    }
}
