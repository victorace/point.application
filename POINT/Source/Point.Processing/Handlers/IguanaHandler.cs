﻿using Point.Business.Logic;
using Point.Database.Context;
using Point.Database.Repository;
using Point.Infrastructure.Helpers;
using Point.Models.Iguana;
using Point.ServiceBus.EventTypes;
using Point.ServiceBus.Models;
using System;
using System.IO;
using System.Net;
using System.Xml.Serialization;

namespace Point.Processing.Handlers
{
    public class IguanaHandler
    {
        public const string QueueName = "Iguana";


        public void HandleUpdateIguanaStatus(IMessageEvent message)
        {
            if (message.ObjectData is UpdateIguanaStatus objectdata)
            {
                using (CookieAwareWebClient client = new CookieAwareWebClient())
                {
                    string result = "";
                    if (System.Diagnostics.Debugger.IsAttached)
                    {
                        result = Resources.IguanaTestResult;
                    }
                    else
                    {
                        string username = ConfigHelper.GetAppSettingByName("IguanaUsername", "");
                        string password = ConfigHelper.GetAppSettingByName("IguanaPassword", "");
                        string URI = ConfigHelper.GetAppSettingByName("WebServerIguana", "") + "/status";
                        var uri = new Uri(URI);

                        string data = string.Format("password={0}&username={1}", password, username);
                        result = client.UploadString(uri, data);
                    }

                    using (TextReader reader = new StringReader(result))
                    {
                        XmlSerializer xmlserializer = new XmlSerializer(typeof(IguanaStatus));
                        var iguanastatus = xmlserializer.Deserialize(reader) as IguanaStatus;
                        using (var uow = new UnitOfWork<PointContext>())
                        {
                            IguanaChannelBL.UpdateIguanaChannelsFromIguanaStatus(uow, iguanastatus);
                        }
                    }
                }
            }

            message.Complete();
        }

    }
}
