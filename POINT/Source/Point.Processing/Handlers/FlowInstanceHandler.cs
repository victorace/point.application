﻿using Point.Business.Helper;
using Point.Business.Logic;
using Point.Communication;
using Point.Database.Context;
using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Repository;
using Point.Infrastructure.Extensions;
using Point.ServiceBus.EventTypes;
using Point.ServiceBus.Models;

namespace Point.Processing.Handlers
{
    public class FlowInstanceHandler
    {
        public static string QueueName = "FlowInstance";


        public void HandleCloseMessage(IMessageEvent message)
        {
            var objectdata = message.ObjectData as CloseFlowInstance;

            if (objectdata != null && objectdata.FlowInstanceID > 0)
            {
                FlowInstanceSearchValuesBL.CalculateSearchValues(objectdata.FlowInstanceID);
            }

            message.Complete();
        }

        public void HandleReOpenMessage(IMessageEvent message)
        {
            var objectdata = message.ObjectData as ReopenFlowInstance;

            if (objectdata != null && objectdata.FlowInstanceID > 0)
            {
                FlowInstanceSearchValuesBL.CalculateSearchValues(objectdata.FlowInstanceID);
            }

            message.Complete();
        }



        public void HandleTransferAttachment(IMessageEvent messageEvent)
        {
            var objectdata = messageEvent.ObjectData as SendTransferAttachment;

            HandleAttachmentNedap(objectdata);

            messageEvent.Complete();
        }



        public void HandleAttachmentNedap(SendTransferAttachment objectdata)
        {
            if (objectdata.RecievingOrganizationID > 0 && objectdata.RecievingDepartmentID != null)
            {
                using (var uow = new UnitOfWork<PointContext>())
                {
                    var transferattachment = TransferAttachmentBL.GetByID(uow, objectdata.TransferAttachmentID);
                    if (transferattachment != null)
                    {
                        var receivingorganization = OrganizationBL.GetByOrganizationID(uow, objectdata.RecievingOrganizationID);
                        if (OrganizationHelper.IsValidNedapOrganization(uow, receivingorganization, transferattachment.AttachmentTypeID))
                        {
                            var receivingdepartment = DepartmentBL.GetByDepartmentID(uow, objectdata.RecievingDepartmentID.ConvertTo<int>());
                            SendAttachmentNedap(uow, objectdata.TransferID, objectdata.EmployeeID.ConvertTo<int>(), objectdata.TransferAttachmentID, receivingorganization, receivingdepartment);
                        }
                    }
                }
            }
        }

        public void SendAttachmentNedap(IUnitOfWork uow, int transferID, int employeeID, int transferAttachmentID, Organization recievingOrganization, Department recievingDepartment)
        {
            new Nedap().SendAttachment(uow, transferID, employeeID, transferAttachmentID, recievingOrganization, recievingDepartment);
        }


    }
}
