﻿using Point.ServiceBus.EventTypes;
using Point.ServiceBus.Models;

namespace Point.Processing.Handlers
{
    public class CheckHandler
    {
        public const string QueueName = "Check";


        public void HandleServiceBusCheck(IMessageEvent message)
        {
            var objectdata = message.ObjectData as ServiceBusCheck;
            if (objectdata != null)
            {
                message.Complete();
            }
        }

    }
}
