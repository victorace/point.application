﻿using Point.Business.BusinessObjects;
using Point.Business.Logic;
using Point.Database.Context;
using Point.Database.Repository;
using Point.ServiceBus.EventTypes;
using Point.ServiceBus.Models;

namespace Point.Processing.Handlers
{
    public class ReadModelHandler
    {
        public static string QueueNameReadModel = "ReadModel";
        public static string QueueNameReadModelBatch = "ReadModelBatch";
        public static string QueueNameOrganizationSearch = "OrganizationSearch";

        public void HandleUpdateOrganizationSearch(IMessageEvent message)
        {
            var objectdata = message.ObjectData as UpdateOrganizationSearch;
            if (objectdata != null)
            {
                using (var context = new UnitOfWork<PointSearchContext>())
                {
                    if (objectdata.OrganizationID.HasValue)
                    {
                        OrganizationUpdateBL.UpdateReadModelByOrganizationID(context, objectdata.OrganizationID.Value);
                    }
                    else if (objectdata.LocationID.HasValue)
                    {
                        OrganizationUpdateBL.UpdateReadModelByLocationID(context, objectdata.LocationID.Value);
                    }
                    else if (objectdata.DepartmentID.HasValue)
                    {
                        OrganizationUpdateBL.UpdateReadModelByDepartmentID(context, objectdata.DepartmentID.Value);
                    }
                    else
                    {
                        OrganizationUpdateBL.UpdateCompleteReadModel(context);
                    }
                }
            }

            message.Complete();
        }

        public void HandleUpdateReadModel(IMessageEvent message)
        {
            var objectdata = message.ObjectData as UpdateReadModel;
            if (objectdata?.FlowInstanceID > 0)
            {
                FlowInstanceSearchValuesBL.CalculateSearchValues(objectdata.FlowInstanceID);
            }

            message.Complete();
        }

        public void HandleUpdateReadModelBatch(IMessageEvent message)
        {
            var objectdata = message.ObjectData as UpdateReadModelBatch;
            FlowInstanceSearchValuesBL.CalculateSearchValues(objectdata.FlowInstanceIDs);
            message.Complete();
        }


    }
}
