﻿using Point.Business.Helper;
using Point.Business.Logic;
using Point.Communication;
using Point.Database.Context;
using Point.Database.Extensions;
using Point.Models.Enums;
using Point.Database.Repository;
using Point.ServiceBus.EventTypes;
using Point.ServiceBus.Models;

namespace Point.Processing.Handlers
{
    public class NotificationHandler
    {
        public const string QueueName = "Notification";

        public void HandleIMailServiceBusModel(IMessageEvent message)
        {
            var objectdata = message.ObjectData as IMailServiceBusModel;
            if (objectdata?.RedirectToAuth == true && objectdata.AuthOrganizationID.HasValue)
            {
                objectdata.Url += "&AuthOrganizationID=" + objectdata.AuthOrganizationID;
            }
        }

        public void HandleRelocateNotification(IMessageEvent message)
        {
            using (var uow = new UnitOfWork<PointContext>())
            {
                var relocateNotification = message.ObjectData as RelocateNotification;
                var emailViewModel = EmailBL.GetEmailRelocateDossierViewModel(uow, relocateNotification.TransferID, relocateNotification.EmployeeID ?? 0, relocateNotification.SourceDepartmentID, relocateNotification.DestinationDepartmentID, relocateNotification.RelocateReason);
                if (emailViewModel != null)
                {
                    if (!string.IsNullOrEmpty(emailViewModel.ReceivingEmailAdress))
                    {
                        Email.SendRazorMail(uow, relocateNotification.TransferID, "DossierIsRelocatedToYou", emailViewModel, emailViewModel.ReceivingEmailAdress);
                    }

                    if (!string.IsNullOrEmpty(emailViewModel.ReceivingTransferPointEmailAdress))
                    {
                        Email.SendRazorMail(uow, relocateNotification.TransferID, "DossierIsRelocatedToYou", emailViewModel, emailViewModel.ReceivingTransferPointEmailAdress);
                    }
                }

                message.Complete();
            }
        }

        public void HandleFrequencyNotification(IMessageEvent message)
        {
            var objectdata = message.ObjectData as FrequencyNotification;
            if (objectdata != null)
            {
                using (var context = new UnitOfWork<PointContext>())
                {
                    var frequency = FrequencyBL.GetByID(context, objectdata.FrequencyID);
                    if (frequency != null)
                    {
                        bool sendemail = frequency.FeedbackType == FrequencyFeedbackType.Both || frequency.FeedbackType == FrequencyFeedbackType.Email;
                        bool sendsignal = frequency.FeedbackType == FrequencyFeedbackType.Both || frequency.FeedbackType == FrequencyFeedbackType.Signalering;

                        if (sendemail)
                        {
                            var viewmodel = EmailBL.GetEmailDossierFeedback(context, objectdata.FrequencyID, objectdata.Timestamp, objectdata.Url);
                            if (viewmodel != null)
                            {
                                Email.SendRazorMail(context, objectdata.TransferID, "EmailDoorlopendDossierFeedback", viewmodel, viewmodel.RecievingEmailAdress);
                            }
                        }

                        if (sendsignal)
                        {
                            var flowinstance = FlowInstanceBL.GetByTransferID(context, frequency.TransferID);
                            var systememployee = PointUserInfoHelper.GetSystemEmployee(context);
                            var formtypeid = (int)frequency.GetFlowFormType();

                            SignaleringBL.HandleSignalByFlowDefinitionFormTypeAction(context, flowinstance, formtypeid, "DoorlopendDossierFeedBack", systememployee, "Terugkoppeling gewenst");
                        }
                    }
                }
            }

            message.Complete();
        }
    }
}
