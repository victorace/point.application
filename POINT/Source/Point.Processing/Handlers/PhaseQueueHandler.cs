﻿using Point.Business.Helper;
using Point.Business.Logic;
using Point.Communication;
using Point.Database.Context;
using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Repository;
using Point.Infrastructure.Extensions;
using Point.Infrastructure.Helpers;
using Point.Processing.Exceptions;
using Point.ServiceBus.EventTypes;
using Point.ServiceBus.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using Point.Infrastructure.Constants;

namespace Point.Processing.Handlers
{
    public class PhaseQueueHandler
    {
        public static string QueueName = "PhaseQueue";

        public void HandleEvaluateDecisionNotification(IMessageEvent message)
        {
            using (var context = new UnitOfWork<PointContext>())
            {
                var objectdata = message.ObjectData as EvaluateDecisionNotification;
                var organizationinvite = OrganizationInviteBL.GetByID(context, objectdata.OrganizationInviteID);
                var emailviewmodel = EmailBL.GetGenericEmail(context, organizationinvite.FlowInstance.TransferID, organizationinvite.DepartmentID, organizationinvite.CreatedByID, objectdata.InviteUrl);

                if(organizationinvite.InviteStatus == InviteStatus.Cancelled)
                {
                    //Heeft alternatieve datum geweigerd
                }

                if (organizationinvite.InviteStatus == InviteStatus.Acknowledged)
                {
                    //Heeft alternatieve datum geaccepteerd
                    Email.SendRazorMail(context, objectdata.TransferID, "EvaluateDecisionYes", emailviewmodel, emailviewmodel.ReceivingEmailAdress);
                }
            }

            message.Complete();
        }

        public void HandleInviteNotification(IMessageEvent message)
        {
            using (var context = new UnitOfWork<PointContext>())
            {
                var objectdata = message.ObjectData as InviteNotification;
                var organizationinvite = OrganizationInviteBL.GetByID(context, objectdata.OrganizationInviteID);

                switch (organizationinvite.FlowInstance.FlowDefinitionID)
                {
                    case FlowDefinitionID.VVT_ZH:
                        var viewmodelvvtzh = EmailBL.GetEmailVVTZH(context, objectdata.TransferID, organizationinvite.DepartmentID, objectdata.EmployeeID, objectdata.Url, EmailBL.EmailType.Email);
                        if (viewmodelvvtzh != null)
                        {
                            Email.SendRazorMail(context, objectdata.TransferID, "EmailVVTZH", viewmodelvvtzh, viewmodelvvtzh.RecievingEmailAdress);
                        }
                        message.Complete();
                        break;

                    case FlowDefinitionID.ZH_ZH:
                        var viewmodelzhzh = EmailBL.GetEmailZHZH(context, objectdata.TransferID, organizationinvite.DepartmentID, objectdata.EmployeeID, objectdata.Url, EmailBL.EmailType.Email);
                        if (viewmodelzhzh != null)
                        {
                            Email.SendRazorMail(context, objectdata.TransferID, "EmailZHZH", viewmodelzhzh, viewmodelzhzh.RecievingEmailAdress);
                        }
                        message.Complete();
                        break;

                    case FlowDefinitionID.RzTP:
                        var viewmodelrztp = EmailBL.GetEmailZHVVT(context, objectdata.TransferID, organizationinvite.DepartmentID, objectdata.EmployeeID, objectdata.Url, EmailBL.EmailType.Email);
                        if(viewmodelrztp != null)
                        {
                            Email.SendRazorMail(context, objectdata.TransferID, "EmailZHVVT-NOACK", viewmodelrztp, viewmodelrztp.RecievingEmailAdress);
                        }

                        message.Complete();
                        break;

                    case FlowDefinitionID.ZH_VVT:
                    case FlowDefinitionID.CVA:
                    
                        if (organizationinvite.RecievingPhaseDefinition.FlowHandling == FlowHandling.IndicatingCIZ)
                        {
                            var viewmodelciz = EmailBL.GetEmailZHCIS(context, objectdata.TransferID, organizationinvite.DepartmentID, objectdata.EmployeeID, objectdata.Url, EmailBL.EmailType.Email);
                            if (viewmodelciz != null)
                            {
                                Email.SendRazorMail(context, objectdata.TransferID, "EmailZHCIZ", viewmodelciz, viewmodelciz.RecievingEmailAdress);
                            }

                            message.Complete();
                            break;
                        }
                        else if (organizationinvite.RecievingPhaseDefinition.FlowHandling == FlowHandling.IndicatingGRZ)
                        {
                            var viewmodelgrz = EmailBL.GetGenericEmail(context, objectdata.TransferID, organizationinvite.DepartmentID, objectdata.EmployeeID, objectdata.Url);
                            if (viewmodelgrz != null)
                            {
                                Email.SendRazorMail(context, objectdata.TransferID, "EmailZHGRZ", viewmodelgrz, viewmodelgrz.ReceivingEmailAdress);
                            }

                            message.Complete();
                            break;
                        }
                        else if (organizationinvite.RecievingPhaseDefinition.FlowHandling == FlowHandling.Receive || organizationinvite.RecievingPhaseDefinition.FlowHandling == FlowHandling.ReceiveDecision)
                        {
                            var viewmodelzhvvt = EmailBL.GetEmailZHVVT(context, objectdata.TransferID, organizationinvite.DepartmentID, objectdata.EmployeeID, objectdata.Url, EmailBL.EmailType.Email);
                            if (viewmodelzhvvt != null)
                            {
                                var templatecode = "EmailZHVVT";
                                if (organizationinvite.InviteStatus == InviteStatus.Realized)
                                {
                                    templatecode = "EmailZHRealizedVVT";
                                }

                                Email.SendRazorMail(context, objectdata.TransferID, templatecode, viewmodelzhvvt, viewmodelzhvvt.RecievingEmailAdress);
                            }

                            message.Complete();
                            break;
                        }
                        else if (organizationinvite.RecievingPhaseDefinition.FlowHandling == FlowHandling.DoorlDossier)
                        {
                            var templatecode = "EmailZHVVT-NOACK";
                            var viewmodelzhvvt = EmailBL.GetEmailZHVVT(context, objectdata.TransferID, organizationinvite.DepartmentID, objectdata.EmployeeID, objectdata.Url, EmailBL.EmailType.Email);
                            if(viewmodelzhvvt != null)
                            {
                                Email.SendRazorMail(context, objectdata.TransferID, templatecode, viewmodelzhvvt, viewmodelzhvvt.RecievingEmailAdress);
                            }

                            message.Complete();
                            break;
                        }

                        throw new DeadLetterMessageException("InviteNotification wrong handling: " + organizationinvite.RecievingPhaseDefinition.FlowHandling.ToString());

                    case FlowDefinitionID.HA_VVT:
                        if (organizationinvite.Organization.OrganizationTypeID == (int)OrganizationTypeID.RegionaalCoordinatiepunt)
                        {
                            var viewmodelhato = EmailBL.GetEmailHATO(context, objectdata.TransferID, organizationinvite.DepartmentID, objectdata.EmployeeID, objectdata.Url, EmailBL.EmailType.Email);
                            if (viewmodelhato != null)
                            {
                                Email.SendRazorMail(context, objectdata.TransferID, "EmailHATO", viewmodelhato, viewmodelhato.RecievingEmailAdress);
                            }

                            message.Complete();
                            break;
                        }

                        var viewmodel = EmailBL.GetEmailHAVVT(context, objectdata.TransferID, organizationinvite.DepartmentID, objectdata.EmployeeID, objectdata.Url, EmailBL.EmailType.Email);
                        if (viewmodel != null)
                        {
                            Email.SendRazorMail(context, objectdata.TransferID, "EmailHAVVT", viewmodel, viewmodel.RecievingEmailAdress);
                        }

                        message.Complete();
                        break;

                    case FlowDefinitionID.VVT_VVT: // vreemd geen eigen template
                        var viewmodelvvtvvt = EmailBL.GetEmailVVTVVT(context, objectdata.TransferID, organizationinvite.DepartmentID, objectdata.EmployeeID, objectdata.Url, EmailBL.EmailType.Email);
                        if (viewmodelvvtvvt != null)
                        {
                            Email.SendRazorMail(context, objectdata.TransferID, "EmailVVTVVT", viewmodelvvtvvt, viewmodelvvtvvt.RecievingEmailAdress);
                        }

                        message.Complete();
                        break;

                    default:
                        throw new DeadLetterMessageException("InviteNotification: flow niet ondersteund. " + organizationinvite.FlowInstance.FlowDefinitionID.ToString());

                }
            }
        }

        public void HandleFormSetVersionNotification(IMessageEvent messageEvent)
        {
            var objectdata = messageEvent.ObjectData as FormSetVersionNotification;

            if (objectdata.IsVO)
            {
                switch (objectdata.MailType)
                {
                    case CommunicationLogType.NedapVO:
                        HandleVONedap(objectdata); break;
                    case CommunicationLogType.ZorgmailSendPDF:
                        HandleVOZorgmail(objectdata); break;
                    default:
                        HandleVONotification(objectdata);
                        sendThirdPartyVOMessages(objectdata);
                        break;
                }
            }
            else
            {
                HandleFormSetVersionNedap(objectdata);
            }

            messageEvent.Complete();
        }

        public void HandleSendMedvri(IMessageEvent message)
        {
            using (var context = new UnitOfWork<PointContext>())
            {
                var objectdata = message.ObjectData as SendMedvri;

                var organization = OrganizationBL.GetByOrganizationID(context, objectdata.SendingOrganizationID);
                var pointuserinfo = PointUserInfoHelper.GetPointUserInfoByEmployeeID(context, objectdata.SendingEmployeeID);
                if (MedVRIBL.OrganizationUsesORU(context, organization))
                {
                    new NoticeDoctor(objectdata.TransferID, objectdata.SendingEmployeeID).SendURO(context, pointuserinfo, organization.OrganizationID, objectdata.TemplateTypeID);
                }
                else
                {
                    var havvttypes = new List<TemplateTypeID>() { TemplateTypeID.ZorgMailHAVVTJa, TemplateTypeID.ZorgMailHAVVTNee, TemplateTypeID.ZorgMailHAVVTNeeEnNieuweVVT };

                    string zorgmailusername = "";
                    string zorgmailpassword = "";
                    var zorgmailinitusertype = Zorgmail.ZorgmailUserType.SendForConnectedOrganization;

                    if (objectdata.TemplateTypeID == TemplateTypeID.ZorgMailZHVVTJa)
                    {
                        zorgmailusername = organization?.ZorgmailUsername;
                        zorgmailpassword = organization?.ZorgmailPassword;
                        zorgmailinitusertype = Zorgmail.ZorgmailUserType.SendForConnectedOrganization;
                    }
                    else if (havvttypes.Contains(objectdata.TemplateTypeID))
                    {
                        zorgmailusername = ConfigHelper.GetAppSettingByName<string>("Zorgmail.Username");
                        zorgmailpassword = ConfigHelper.GetAppSettingByName<string>("Zorgmail.Password");
                        zorgmailinitusertype = Zorgmail.ZorgmailUserType.SendAsLinkassist;
                    }

                    if (string.IsNullOrWhiteSpace(zorgmailusername) || string.IsNullOrWhiteSpace(zorgmailpassword))
                    {
                        throw new ExceptionLogException(new Exception(string.Format("TransferID:{0} Empty ZorgmailUsername and/or ZorgmailPassword", objectdata.TransferID)));
                    }

                    var client = ClientBL.GetByClientID(context, objectdata.ClientID);
                    if (client == null || string.IsNullOrWhiteSpace(client.AddressGPForZorgmail))
                    {
                        throw new ExceptionLogException(new Exception(string.Format("Client:{0} Empty AddressGPForZorgmail", objectdata.ClientID)));
                    }

                    var recipient = new Recipient() { AccountId = client.AddressGPForZorgmail, Name = client.GeneralPractitionerName };

                    Email.SendMedvri(zorgmailinitusertype, objectdata.TransferID, objectdata.ClientID, 
                    objectdata.SendingOrganizationID, objectdata.SendingDepartmentID,
                    recipient, client.AddressGPForZorgmail,
                    zorgmailusername, zorgmailpassword,
                    objectdata.SendingEmployeeID, objectdata.ScreenID, DateTime.Now, pointuserinfo, objectdata.TemplateTypeID);
                }
                message.Complete();
            }
        }

        public void HandleCancelNotification(IMessageEvent message)
        {
            using (var context = new UnitOfWork<PointContext>())
            {
                var objectdata = message.ObjectData as CancelNotification;
                var invite = OrganizationInviteBL.GetByID(context, objectdata.OrganizationInviteID);               

                var department = DepartmentBL.GetByDepartmentID(context, invite.DepartmentID);
                if (department == null)
                {
                    throw new DeadLetterMessageException("HandleCancelNotification error RecievingDepartmentID = " + invite.DepartmentID);
                }   
                
                var transferId = invite.FlowInstance.TransferID;
                int? organizationTypeID = invite.Organization.OrganizationTypeID;

                if (organizationTypeID == (int)OrganizationTypeID.HealthCareProvider)
                {
                    var viewmodeltransfer = EmailBL.GetEmailZHVVT(context, transferId, invite.DepartmentID, objectdata.SendingEmployeeID, "", EmailBL.EmailType.Email, forceclientname: true,cancelReason : invite.HandlingRemark);
                    if (viewmodeltransfer != null)
                    {
                        Email.SendRazorMail(context, transferId, "EmailCancelZHVVT", viewmodeltransfer, viewmodeltransfer.RecievingEmailAdress);
                    }

                    message.Complete();
                    return;
                }

                if (organizationTypeID == (int)OrganizationTypeID.CIZ)
                {
                    //No notification for CIZ
                    message.Complete();
                    return;
                }

                if (organizationTypeID == (int)OrganizationTypeID.GRZ)
                {
                    //No notification for GRZ
                    message.Complete();
                    return;
                }

                if (organizationTypeID == (int)OrganizationTypeID.Hospital)
                {
                    //No notification for hospital (yet)
                    message.Complete();
                    return;
                }

                if (organizationTypeID == null)
                {
                    throw new InvalidMessageException("HandleCancelNotification OrganizationType: null");
                }

                throw new InvalidMessageException("HandleCancelNotification with unexpected OrganizationType: " + organizationTypeID) ;
            }
        }



        public void HandleVOZorgmail(FormSetVersionNotification objectdata)
        {
            if (objectdata.RecievingDepartmentID.ConvertTo<int>() > 0)
            {
                using (var uow = new UnitOfWork<PointContext>())
                {
                    var department = DepartmentBL.GetByDepartmentID(uow, objectdata.RecievingDepartmentID.ConvertTo<int>());
                    if (Zorgmail.IsValidZorgmailDepartment(department))
                    {
                        var filecontents = FormsetVersionPdfHelper.GetPDFData(objectdata.FormSetVersionID, objectdata.SendingEmployeeID);
                        if (filecontents != null)
                        {
                            int emailCount = CommunicationLogBL.GetByTransferIDAndMailType(uow, objectdata.TransferID, CommunicationLogType.ZorgmailSendPDF).Count();
                            string attachmentname = (emailCount > 0 ? "VerpleegkundigeOverdrachtGewijzigd" : "VerpleegkundigeOverdracht");
                            Email.SendPDF(objectdata.TransferID, department.ZorgmailTransferSend, department.FullName(), attachmentname, filecontents);
                        }
                    }
                }
            }

        }

        public void HandleVONedap(FormSetVersionNotification objectdata)
        {
            if (objectdata.RecievingOrganizationID > 0 && objectdata.RecievingDepartmentID != null)
            {
                using (var uow = new UnitOfWork<PointContext>())
                {
                    var formsetversion = FormSetVersionBL.GetByID(uow, objectdata.FormSetVersionID);
                    if (formsetversion != null)
                    {
                        var receivingorganization = OrganizationBL.GetByOrganizationID(uow, objectdata.RecievingOrganizationID);
                        if (OrganizationHelper.IsValidNedapOrganization(uow, receivingorganization, (FlowFormType)formsetversion.FormTypeID))
                        {
                            var receivingdepartment = DepartmentBL.GetByDepartmentID(uow, objectdata.RecievingDepartmentID.ConvertTo<int>());
                            SendEOverdrachtNedap(uow, objectdata.TransferID, objectdata.EmployeeID.ConvertTo<int>(), objectdata.FormSetVersionID, objectdata.IsAltered, receivingorganization, receivingdepartment);
                        }
                    }
                }
            }
        }

        public void HandleFormSetVersionNedap(FormSetVersionNotification objectdata)
        {
            if (objectdata.RecievingOrganizationID > 0 && objectdata.RecievingDepartmentID != null)
            {
                using (var uow = new UnitOfWork<PointContext>())
                {
                    var formsetversion = FormSetVersionBL.GetByID(uow, objectdata.FormSetVersionID);
                    if (formsetversion != null)
                    {
                        var receivingorganization = OrganizationBL.GetByOrganizationID(uow, objectdata.RecievingOrganizationID);
                        if (OrganizationHelper.IsValidNedapOrganization(uow, receivingorganization, (FlowFormType)formsetversion.FormTypeID))
                        {
                            var receivingdepartment = DepartmentBL.GetByDepartmentID(uow, objectdata.RecievingDepartmentID.ConvertTo<int>());
                            SendFormNedap(uow, objectdata.TransferID, objectdata.EmployeeID.ConvertTo<int>(), objectdata.FormSetVersionID, objectdata.IsAltered, receivingorganization, receivingdepartment);
                        }
                    }
                }
            }
        }


        public void HandleVONotification(FormSetVersionNotification objectdata)
        {
            using (var uow = new UnitOfWork<PointContext>())
            {
                var flowinstance = FlowInstanceBL.GetByTransferID(uow, objectdata.TransferID);

                switch ((FlowTypeID?)flowinstance?.FlowDefinition?.FlowTypeID)
                {
                    case FlowTypeID.VVT_ZH:
                        var viewmodeltransfervvtzh = EmailBL.GetEmailVVTZH(uow, objectdata.TransferID, objectdata.RecievingDepartmentID.GetValueOrDefault(0), objectdata.SendingEmployeeID, objectdata.Url, EmailBL.EmailType.TransferEmail);
                        if (viewmodeltransfervvtzh != null)
                        {
                            string razortemplatecode = (objectdata.MailType == CommunicationLogType.RequestToHospitalDepartment) ? "NOTIFYZHVO" : "NOTIFYZHTPVO";
                            Email.SendRazorMail(uow, objectdata.TransferID, razortemplatecode, viewmodeltransfervvtzh, viewmodeltransfervvtzh.RecievingEmailAdress);
                        }
                        break;
                    case FlowTypeID.ZH_VVT:
                        var viewmodeltransferzhvvt = EmailBL.GetEmailZHVVT(uow, objectdata.TransferID, objectdata.RecievingDepartmentID.GetValueOrDefault(), objectdata.SendingEmployeeID, objectdata.Url, EmailBL.EmailType.TransferEmail);
                        if (viewmodeltransferzhvvt != null)
                        {
                            string razortemplatecode = objectdata.IsAltered ? "NOTIFYZHVVTVOCHANGE" : "NOTIFYVVTVO";
                            Email.SendRazorMail(uow, objectdata.TransferID, razortemplatecode, viewmodeltransferzhvvt, viewmodeltransferzhvvt.RecievingEmailAdress);
                        }
                        break;
                    case FlowTypeID.ZH_ZH:
                        var viewmodeltransferzhzh = EmailBL.GetEmailZHZH(uow, objectdata.TransferID, objectdata.RecievingDepartmentID.GetValueOrDefault(), objectdata.SendingEmployeeID, objectdata.Url, EmailBL.EmailType.TransferEmail);
                        if (viewmodeltransferzhzh != null)
                        {
                            string razortemplatecode = objectdata.IsAltered ? "NOTIFYZHZHVOCHANGE" : "NOTIFYZHZHVO";
                            Email.SendRazorMail(uow, objectdata.TransferID, razortemplatecode, viewmodeltransferzhzh, viewmodeltransferzhzh.RecievingEmailAdress);
                        }
                        break;
                    case FlowTypeID.HA_VVT:
                        //Geen
                        break;
                    case FlowTypeID.VVT_VVT:
                        var viewmodeltransfervvtvvt = EmailBL.GetEmailVVTVVT(uow, objectdata.TransferID, objectdata.RecievingDepartmentID.GetValueOrDefault(), objectdata.SendingEmployeeID, objectdata.Url, EmailBL.EmailType.TransferEmail);
                        if (viewmodeltransfervvtvvt != null)
                        {
                            string razortemplatecode = objectdata.IsAltered ? "NOTIFYVVTVVTVOCHANGE" : "NOTIFYVVTVVTVO";
                            Email.SendRazorMail(uow, objectdata.TransferID, razortemplatecode, viewmodeltransfervvtvvt, viewmodeltransfervvtvvt.RecievingEmailAdress);
                        }
                        break;
                }
            }
        }



        private void sendThirdPartyVOMessages(FormSetVersionNotification formSetVersionNotification)
        {
            using (var uow = new UnitOfWork<PointContext>())
            {
                if (formSetVersionNotification.RecievingDepartmentID.ConvertTo<int>() > 0)
                {
                    var department = DepartmentBL.GetByDepartmentID(uow, formSetVersionNotification.RecievingDepartmentID.ConvertTo<int>());
                    if (Zorgmail.IsValidZorgmailDepartment(department))
                    {
                        ServiceBusBL.SendMessage(uow, MessageQueues.PhaseQueue, "FormSetVersionNotification",
                            new FormSetVersionNotification()
                            {
                                TimeStamp = DateTime.Now,
                                UniqueID = DateTime.Now.Ticks,
                                TransferID = formSetVersionNotification.TransferID,
                                FormSetVersionID = formSetVersionNotification.FormSetVersionID,
                                RecievingDepartmentID = formSetVersionNotification.RecievingDepartmentID,
                                RecievingOrganizationID = formSetVersionNotification.RecievingOrganizationID,
                                SendingEmployeeID = formSetVersionNotification.SendingEmployeeID,
                                MailType = CommunicationLogType.ZorgmailSendPDF,
                                IsVO = true
                            });
                    }
                }

                if (formSetVersionNotification.RecievingOrganizationID.ConvertTo<int>() > 0)
                {
                    var organization = OrganizationBL.GetByOrganizationID(uow, formSetVersionNotification.RecievingOrganizationID.ConvertTo<int>());
                    if (OrganizationHelper.IsValidNedapOrganization(organization))
                    {
                        ServiceBusBL.SendMessage(uow, MessageQueues.PhaseQueue, "FormSetVersionNotification",
                            new FormSetVersionNotification()
                            {
                                TimeStamp = DateTime.Now,
                                UniqueID = DateTime.Now.Ticks,
                                TransferID = formSetVersionNotification.TransferID,
                                FormSetVersionID = formSetVersionNotification.FormSetVersionID,
                                RecievingDepartmentID = formSetVersionNotification.RecievingDepartmentID,
                                RecievingOrganizationID = formSetVersionNotification.RecievingOrganizationID,
                                SendingEmployeeID = formSetVersionNotification.SendingEmployeeID,
                                MailType = CommunicationLogType.NedapVO,
                                IsVO = true,
                                IsAltered = formSetVersionNotification.IsAltered
                            });
                    }
                }
            }
        }




        public void SendEOverdrachtNedap(IUnitOfWork uow, int transferID, int employeeID, int formSetVersionID, bool isAltered, Organization recievingOrganization, Department recievingDepartment)
        {
            new Nedap().SendPDF(uow, transferID, employeeID, formSetVersionID, isAltered, recievingOrganization, recievingDepartment, true);
        }

        public void SendFormNedap(IUnitOfWork uow, int transferID, int employeeID, int formSetVersionID, bool isAltered, Organization recievingOrganization, Department recievingDepartment)
        {
            new Nedap().SendPDF(uow, transferID, employeeID, formSetVersionID, isAltered, recievingOrganization, recievingDepartment, false);
        }

        public void HandleExternalSupplierOrder(IMessageEvent message)
        {
            using (var context = new UnitOfWork<PointContext>())
            {
                if (message.ObjectData is SendExternalSupplierOrder objectData)
                {
                    new SupplierSendOrder().SendOrder(context, objectData);
                }
            }

            message.Complete();
        }



    }
}
