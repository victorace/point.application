﻿using Point.Business.Expressions;
using Point.Business.Helper;
using Point.Business.Logic;
using Point.Database.Context;
using Point.Database.Models;
using Point.Models.Enums;
using Point.Database.Repository;
using Point.ServiceBus.EventTypes;
using Point.ServiceBus.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using Point.Database.Models.ReportModels;
using Point.Communication.Infrastructure.Helpers;
using Point.Infrastructure.Helpers;

namespace Point.Processing.Handlers
{
    public class CSVDumpHandler
    {
        public static string QueueName = "CSVDump";

        public void HandleCSVDumpRequest(IMessageEvent message)
        {
            if (message.ObjectData is CSVDumpRequest objectdata)
            {
                using (var uow = new UnitOfWork<PointSearchContext>())
                {
                    var pointUserInfo = PointUserInfoHelper.GetPointUserInfoByEmployeeID(uow, objectdata.EmployeeID);

                    var query = FlowInstanceReportValuesBL.GetFilteredQuery(uow, objectdata.Filter, objectdata.FlowDirection, pointUserInfo);
                    var data = query.Select(FlowInstanceReportValuesExpressions.AllDataFromModel).ToList();
                    var csvfilename = string.Format("csvdump-{0}.csv", DateTime.Now.ToString("yyyyMMddHHmm"));
                    var zipfilename = string.Format("csvdump-{0}.zip", DateTime.Now.ToString("yyyyMMddHHmm"));

                    var csvdata = new CSVHelper().GenerateCSVZipFromEnumarable(data, typeof(DumpReportModel), csvfilename, pointUserInfo.Organization.OrganizationID, CSVTemplateTypeID.CSVDump);
                    TransferAttachmentBL.CreateZipTransferAttachment(objectdata.EmployeeID, csvdata, zipfilename, AttachmentTypeID.DumpReport);

                    string downloadLink = ConfigHelper.GetAppSettingByName<string>("WebServer") + "/DownloadReport/CSVDumpOverview";
                    AsyncReportMail.SendAsyncReportMail(objectdata.EmployeeID, downloadLink, "Point Dump aangevraagd", TemplateTypeID.DumpReportMail);
                }
            }

            message.Complete();
        }
    }
}
