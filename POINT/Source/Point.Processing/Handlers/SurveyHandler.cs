﻿using Point.Business.Logic;
using Point.Database.Context;
using Point.Database.Repository;
using Point.ServiceBus.EventTypes;
using Point.ServiceBus.Models;

namespace Point.Processing.Handlers
{
    public class SurveyHandler
    {
        public const string QueueName = "Survey";

        public void HandleSendSurveyInviteMessage(IMessageEvent message)
        {
            var objectdata = message.ObjectData as SendSurveyInvite;

            if (objectdata != null)
            {
                using (var context = new UnitOfWork<PointContext>())
                {
                    SurveyBL.SendSurveyInvite(context, objectdata.TransferID);
                }
            }

            message.Complete();
        }
    }
}