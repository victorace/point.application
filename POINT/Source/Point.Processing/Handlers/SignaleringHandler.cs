﻿using Point.Business.Helper;
using Point.Business.Logic;
using Point.Communication;
using Point.Database.Context;
using Point.Database.Repository;
using Point.Infrastructure.Extensions;
using Point.Models.Enums;
using Point.ServiceBus.EventTypes;
using Point.ServiceBus.Models;
using System.Linq;
using Point.Database.Models;

namespace Point.Processing.Handlers
{
    public class SignaleringHandler
    {
        public const string QueueName = "Signalering";

        public void HandleSignaleringMessage(IMessageEvent message)
        {
            var objectdata = message.ObjectData as SignaleringMessage;


            if (objectdata != null)
            {
                using (var context = new UnitOfWork<PointContext>())
                {
                    var flowinstance = FlowInstanceBL.GetByTransferID(context, objectdata.TransferID);
                    var pointuserinfo = PointUserInfoHelper.GetPointUserInfoByEmployeeID(context, objectdata.EmployeeID);
                    if (objectdata.SignaleringType == SignaleringType.FormSetVersion)
                    {
                        SignaleringBL.HandleSignalByFlowDefinitionFormTypeAction(context, flowinstance, objectdata.FormTypeID.ConvertTo<int>(), "Save", pointuserinfo, objectdata.Message);
                    }
                    else if (objectdata.SignaleringType == SignaleringType.GeneralAction)
                    {
                        SignaleringBL.HandleSignalByFlowDefinitionGeneralAction(context, flowinstance, objectdata.GeneralActionID.ConvertTo<int>(), pointuserinfo, objectdata.Message);
                    }
                }                
            }

            message.Complete();
        }
      
    }
}
