﻿using Point.Business.Logic;
using Point.Database.Context;
using Point.Database.Repository;
using Point.Processing.Handlers;
using Point.ServiceBus.EventTypes;
using Point.ServiceBus.Models;
using Point.ServiceBus.Readers;
using System;

namespace Point.Processing
{
    public class PointQueueProcessorList : QueueProcessorList
    {
        public PointQueueProcessorList()
        {
            System.Data.Entity.Database.SetInitializer<PointContext>(null);
            System.Data.Entity.Database.SetInitializer<PointSearchContext>(null);

            Add(new QueueReader<InviteNotification>(PhaseQueueHandler.QueueName));
            Add(new QueueReader<EvaluateDecisionNotification>(PhaseQueueHandler.QueueName));
            Add(new QueueReader<FormSetVersionNotification>(PhaseQueueHandler.QueueName));
            Add(new QueueReader<SendMedvri>(PhaseQueueHandler.QueueName));
            Add(new QueueReader<CancelNotification>(PhaseQueueHandler.QueueName));
            Add(new QueueReader<SendVegroOrder>(PhaseQueueHandler.QueueName));
            Add(new QueueReader<SendMediPointOrder>(PhaseQueueHandler.QueueName));
            Add(new QueueReader<CloseFlowInstance>(FlowInstanceHandler.QueueName));
            Add(new QueueReader<ReopenFlowInstance>(FlowInstanceHandler.QueueName));
            Add(new QueueReader<SendTransferAttachment>(FlowInstanceHandler.QueueName));
            Add(new QueueReader<UpdateReadModel>(ReadModelHandler.QueueNameReadModel));
            Add(new QueueReader<UpdateReadModelBatch>(ReadModelHandler.QueueNameReadModelBatch));
            Add(new QueueReader<UpdateOrganizationSearch>(ReadModelHandler.QueueNameOrganizationSearch));
            Add(new QueueReader<ScheduledActions.Schedule>(ScheduledActions.QueueName));
            Add(new QueueReader<CSVDumpRequest>(CSVDumpHandler.QueueName));
            Add(new QueueReader<FrequencyNotification>(NotificationHandler.QueueName));
            Add(new QueueReader<ServiceBusCheck>(CheckHandler.QueueName));
            Add(new QueueReader<SignaleringMessage>(SignaleringHandler.QueueName));
            Add(new QueueReader<SendSurveyInvite>(SurveyHandler.QueueName));
            Add(new QueueReader<UpdateIguanaStatus>(IguanaHandler.QueueName));
          //  Add(new QueueReader<AutoEmailSignalering>(AutoSignaleringHandler.QueueName));
            Add(new QueueReader<RelocateNotification>(NotificationHandler.QueueName));
        }

        public override void OnMessageRecieved(object sender, IMessageEvent e)
        {
            try
            {
                if (e.ObjectData is IMailServiceBusModel)
                    new NotificationHandler().HandleIMailServiceBusModel(e);

                if (e.ObjectData.GetType() == typeof(InviteNotification))
                    new PhaseQueueHandler().HandleInviteNotification(e);

                if (e.ObjectData.GetType() == typeof(EvaluateDecisionNotification))
                    new PhaseQueueHandler().HandleEvaluateDecisionNotification(e);

                else if (e.ObjectData.GetType() == typeof(FormSetVersionNotification))
                    new PhaseQueueHandler().HandleFormSetVersionNotification(e);

                else if (e.ObjectData.GetType() == typeof(SendMedvri))
                    new PhaseQueueHandler().HandleSendMedvri(e);

                else if (e.ObjectData.GetType() == typeof(CancelNotification))
                    new PhaseQueueHandler().HandleCancelNotification(e);

                else if (e.ObjectData.GetType() == typeof(RelocateNotification))
                    new NotificationHandler().HandleRelocateNotification(e);

                else if (e.ObjectData.GetType() == typeof(UpdateReadModel))
                    new ReadModelHandler().HandleUpdateReadModel(e);

                else if (e.ObjectData.GetType() == typeof(UpdateReadModelBatch))
                    new ReadModelHandler().HandleUpdateReadModelBatch(e);

                else if (e.ObjectData.GetType() == typeof(UpdateOrganizationSearch))
                    new ReadModelHandler().HandleUpdateOrganizationSearch(e);

                else if (e.ObjectData.GetType() == typeof(ScheduledActions.Schedule))
                    new ScheduledActions().HandleScheduledActions(e);

                else if (e.ObjectData.GetType() == typeof(CSVDumpRequest))
                    new CSVDumpHandler().HandleCSVDumpRequest(e);

                else if (e.ObjectData.GetType() == typeof(CloseFlowInstance))
                    new FlowInstanceHandler().HandleCloseMessage(e);

                else if (e.ObjectData.GetType() == typeof(ReopenFlowInstance))
                    new FlowInstanceHandler().HandleReOpenMessage(e);

                else if (e.ObjectData.GetType() == typeof(FrequencyNotification))
                    new NotificationHandler().HandleFrequencyNotification(e);

                else if (e.ObjectData.GetType() == typeof(ServiceBusCheck))
                    new CheckHandler().HandleServiceBusCheck(e);

                else if (e.ObjectData.GetType() == typeof(SendTransferAttachment))
                    new FlowInstanceHandler().HandleTransferAttachment(e);

                else if (e.ObjectData.GetType() == typeof(SendVegroOrder))
                    new PhaseQueueHandler().HandleExternalSupplierOrder(e);

                else if (e.ObjectData.GetType() == typeof(SendMediPointOrder))
                    new PhaseQueueHandler().HandleExternalSupplierOrder(e);

                else if (e.ObjectData.GetType() == typeof(SignaleringMessage))
                    new SignaleringHandler().HandleSignaleringMessage(e);

                else if (e.ObjectData.GetType() == typeof(SendSurveyInvite))
                    new SurveyHandler().HandleSendSurveyInviteMessage(e);

                else if (e.ObjectData.GetType() == typeof(UpdateIguanaStatus))
                    new IguanaHandler().HandleUpdateIguanaStatus(e);

                //else if (e.ObjectData.GetType() == typeof(AutoEmailSignalering))
                //    new AutoSignaleringHandler().HandleAutoEmailSignalering(e);

                HandleLoggingServiceBusModel(e);
                MessageLog(e);
            }
            catch (Exception exc)
            {
                using (var uow = new UnitOfWork<PointContext>())
                {
                    MessageError(uow, e, exc);
                }
            }
        }

        public void HandleLoggingServiceBusModel(IMessageEvent message)
        {
            var loggingservicebusmodel = message.ObjectData as ILoggingServiceBusModel;
            if (loggingservicebusmodel != null)
            {
                using (var uow = new UnitOfWork<PointContext>())
                {
                    ServiceBusLogBL.CompleteByUniqueID(uow, loggingservicebusmodel.UniqueID);
                }
            }
        }
    }
}