﻿using System;
using System.Diagnostics;
using Point.Infrastructure.Helpers;
using Point.Processing.Exceptions;
using Point.ServiceBus.EventTypes;
using Point.ServiceBus.Readers;
using Point.Database.Repository;
using Point.Infrastructure;
using Point.Infrastructure.Extensions;
using Point.Log4Net;

namespace Point.Processing
{
    public abstract class QueueProcessorList
    {
        private static readonly ILogger Logger = LogManager.GetLogger(typeof(QueueProcessorList));

        public abstract void OnMessageRecieved(object sender, IMessageEvent e);
        public bool FromConsole { get; set; } = false;

        public void Add(IQueueReader queuereader)
        {
            QueueReaderController.RegisterQueue(queuereader);
            queuereader.OnMessageRecieved += OnMessageRecieved;
        }

        public void Start()
        {
            AppDomain currentDomain = AppDomain.CurrentDomain;
            currentDomain.UnhandledException += currentDomain_UnhandledException;

            QueueReaderController.OnError += QueueReaderController_OnError;
            QueueReaderController.Start();
        }

        void currentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Exception exc = (Exception)e.ExceptionObject;

            AddLog($"Application error: (see eventlog)", ConsoleColor.Red);
            AddLog($"{exc.Message}");
            EventLogHelper.AddItem("Point", "ApplicationError", exc.Message + "\r\n" + exc.StackTrace, EventLogEntryType.Error);
        }

        void QueueReaderController_OnError(object sender, Exception e)
        {
            AddLog($"QueueReaderController error: (see eventlog)", ConsoleColor.Red);
            AddLog($"{e.Message}");
            EventLogHelper.AddItem("Point", "QueueReaderControllerError", e.Message + "\r\n" + e.StackTrace, EventLogEntryType.Error);
        }

        public async void Stop()
        {
            await QueueReaderController.Stop();
        }
        
        private void SendMessageBusException(IUnitOfWork uow, IMessageEvent e, Exception exc)
        {
            int? transferid = null;
            var dossierservicebusmodel = e.ObjectData as ServiceBus.Models.IDossierServiceBusModel;
            if(dossierservicebusmodel != null && dossierservicebusmodel.TransferID > 0)
            {
                transferid = dossierservicebusmodel.TransferID;
            }

            ExceptionHelper.SendMessageException(exc, transferid: transferid);
        }

        public void MessageError(IUnitOfWork uow, IMessageEvent e, Exception exc)
        {
            if(exc is ExceptionLogException || exc is DeadLetterMessageException)
            {
                e.DeadLetter(exc.Message, exc.StackTrace);
                SendMessageBusException(uow, e, exc);
                return;
            }

            if (exc is InvalidMessageException && e.IsOldMessage)
            {
                e.Invalid(exc.Message, exc.StackTrace);
                SendMessageBusException(uow, e, exc);
                return;
            }

            string errormessage = exc.Message + "\r\n" + exc.StackTrace;
            if (exc.InnerException != null)
            {
                errormessage += "\r\nInner exception:\r\n" + exc.InnerException.Message + "\r\n" + exc.InnerException.StackTrace;
            }

            SendMessageBusException(uow, e, exc);
            e.Error(errormessage);
        }

        public void MessageLog(IMessageEvent e)
        {
            var newprocessingtime = e.EndTime.Subtract(e.StartTime).TotalMilliseconds.ConvertTo<int>();
            AddLog($"Message completed @{DateTime.Now.ToString("HH:mm:ss")} - {e.FullName} - {newprocessingtime}ms", ConsoleColor.Green);
        }

        public void AddLog(string message, ConsoleColor color = ConsoleColor.Gray)
        {
            if (FromConsole)
            {
                Console.ForegroundColor = color;
                Console.WriteLine(message);
                Console.ForegroundColor = ConsoleColor.Gray;
            }

            Logger.Info(message);
        }
    }
}
