﻿using System;

namespace Point.Processing.Events
{
    public class HandleMessageEvent : EventArgs
    {
        public string Group { get; set; }
        public string Label { get; set; }
        public string Type { get; set; }
        public string ID { get; set; }
    }
}
