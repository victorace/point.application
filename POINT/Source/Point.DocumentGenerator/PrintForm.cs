﻿using System.Data;

namespace DocumentGenerator
{
    public class PrintForm
    {

        public string PrintFormID { get; set; }
        public int FormSetVersionID { get; set; }
        public string HTMLTemplate { get; set; }
        public DataSet FormData { get; set; }
        public string HeaderText { get; set; }
        public string HeaderImage { get; set; }
        public string FooterRight { get; set; }
        public string FooterLeft { get; set; }

        public PrintForm()
        {
        }

        public PrintForm(string formTypeID)
        {
            PrintFormID = formTypeID;
        }
    }
}
