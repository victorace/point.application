﻿namespace DocumentGenerator
{
    public class StyleComponent
    {
        private string _TagName = "";
        private string _Value = "";

        public string TagName
        {
            get
            {
                return _TagName;
            }
        }

        public string Value
        {
            get
            {
                return _Value;
            }
        }

        public StyleComponent(string tagName, string value)
        {
            _TagName = tagName;
            _Value = value;
        }
    }

}
