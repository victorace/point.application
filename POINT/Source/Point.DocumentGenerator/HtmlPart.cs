﻿namespace DocumentGenerator
{
    public class HtmlPart
    {
        private string _Part = "";

        public bool Skip = false;

        public string Part
        {
            get
            {
                return _Part;
            }
        }

        public bool IsOpenTag
        {
            get
            {
                if (_Part != "" && _Part.Length > 1)
                {
                    if ((_Part[0] == '<') && (_Part[1] != '/') && (_Part[_Part.Length - 1] == '>'))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }

        public bool IsCloseTag
        {
            get
            {
                if (_Part != "" && _Part.Length > 1)
                {
                    if ((_Part[0] == '<') && (_Part[1] == '/') && (_Part[_Part.Length - 1] == '>'))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }

        public bool IsSingleTag
        {
            get
            {
                if (_Part != "")
                {
                    if ((_Part.Length > 2) && (_Part[0] == '<') && (_Part[_Part.Length - 2] == '/') && (_Part[_Part.Length - 1] == '>'))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }

        public string TagName
        {
            get
            {
                if (IsOpenTag || IsCloseTag)
                {
                    string strippedStyle = _Part.Replace("<", "").Replace("/", "").Replace(">", "").Trim().ToLower();
                    string[] components = strippedStyle.Split(' ');
                    return components[0];
                }
                else
                {
                    return "";
                }
            }
        }

        public string Style
        {
            get
            {
                if (IsOpenTag)
                {
                    string strippedStyle = _Part.Replace("<", "").Replace("/", "").Replace(">", "").Trim().ToLower();

                    if (strippedStyle == "b")
                    {
                        return "bold";
                    }
                    else if (strippedStyle == "i")
                    {
                        return "italic";
                    }
                    else if (strippedStyle == "u")
                    {
                        return "underline";
                    }
                    else if ( ((TagName == "span") || (TagName == "th")) && (strippedStyle.Contains("style")) )
                    {
                        int startIndex = strippedStyle.IndexOf("style") + 7;
                        int endIndex = strippedStyle.IndexOf("\"", startIndex + 1);

                        if (endIndex == -1)
                        {
                            endIndex = strippedStyle.IndexOf("'", startIndex + 1);
                        }

                        string styleString = "";

                        for (int i = startIndex; i < endIndex; i++)
                        {
                            styleString += strippedStyle[i];
                        }

                        return styleString;
                    }
                    else
                    {
                        return "";
                    }
                }
                else
                {
                    return "";
                }
            }
        }

        public string Class
        {
            get
            {
                if (IsOpenTag)
                {
                    string strippedClass = _Part.Replace("<", "").Replace("/", "").Replace(">", "").Trim().ToLower();

                    if (((TagName == "table") || (TagName == "td")) && (strippedClass.Contains("class")))
                    {
                        int startIndex = strippedClass.IndexOf("class") + 7;
                        int endIndex = strippedClass.IndexOf("\"", startIndex + 1);

                        if (endIndex == -1)
                        {
                            endIndex = strippedClass.IndexOf("'", startIndex + 1);
                        }

                        string classString = "";

                        for (int i = startIndex; i < endIndex; i++)
                        {
                            classString += strippedClass[i];
                        }

                        return classString;
                    }
                    else
                    {
                        return "";
                    }
                }
                else
                {
                    return "";
                }
            }
        }

        public HtmlPart(string part)
        {
            _Part = part;
        }

    }
}
