﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Point.Models.Attributes
{
    [AttributeUsage(AttributeTargets.Constructor, AllowMultiple = true)]
    public class ImportAttribute : Attribute
    {
        //Point,LUMC.. etc
        public SourceType Type { get; set; }
        public string SourceFieldName { get; set; }
        public delegate void Handler(ImportAttribute attribute);

        public enum SourceType
        {
            Point = 0
        }

        public ImportAttribute(SourceType type, string sourceFieldName, Handler handler = null)
        {
            Type = type;
            SourceFieldName = sourceFieldName;
        }
    }
}
