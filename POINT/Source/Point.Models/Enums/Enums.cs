﻿using System.ComponentModel;

// ADDING AN ENUM? THEN PLEASE KEEP THE ORDER CORRECT (ALPHABETICALLY) - Thank you.
// ADDING AN ENUM? THEN PLEASE KEEP THE ORDER CORRECT (ALPHABETICALLY) - Thank you.
// ADDING AN ENUM? THEN PLEASE KEEP THE ORDER CORRECT (ALPHABETICALLY) - Thank you.
// ADDING AN ENUM? THEN PLEASE KEEP THE ORDER CORRECT (ALPHABETICALLY) - Thank you.

namespace Point.Models.Enums
{
    public enum ActionTypeID
    {
        Save = 1,
        Next = 2,
        Definitive = 3
    }

    public enum AfterCareCategoryID
    {
        Thuiszorg = 1,
        Zorginstelling = 2,
        Hospice = 3
    }

    public enum AftercareClassificationShowID
    {
        Old = 0,
        Request = 1,
        TransferPoint = 2,
        Base = 3,
        HealthCare = 4
    }

    public enum AftercareClassificationTypeID
    {
        Old = 0,
        Extramuraal = 1,
        Hospice = 2,
        Intramuraal = 3
    }

    public enum AfterCareTileGroupID
    {
    	WLZ = 1,
        ZVW = 2,
        Anders = 3,
        Overig = 4,
        Ziekenhuis = 5
    }

    public enum AidProductGroupID
    {
        // Vegro
        Vegro_Matrassen = 1,
        Vegro_Kussens = 2,
        Vegro_Bedden = 3,
        Vegro_BedToebehoren = 4,
        Vegro_DoucheToiletRolstoelen = 5,
        Vegro_Trippelstoelen = 6,
        Vegro_TransferLiften = 7,
        VegroPoint_Algemeen = 8,       // Point + Vegro
        Vegro_Huurartikelen = 9,
        // MediPoint
        MediPoint_Gipssteunen = 10,     //	GPSTN
        MediPoint_Drempelhulpen = 11,   //	DRPLH
        MediPoint_Draaischijven = 12,   //	DRISC
        MediPoint_StaLoopbeugels = 13,  //	SLBGH	Sta-/loopbeugels
        MediPoint_Transferplanken = 14, //	TRSFP
        MediPoint_RolGlijlakens = 15,   //	RGLKN	Rol-/glijlakens
        MediPoint_Tilliften = 16,       //	TLLFT
        MediPoint_Badplanken = 17,      //	BDPLK
        MediPoint_Douchekrukken = 18,   //	DCHKR
        MediPoint_Douchestoelen = 19,   //	DCHST
        MediPoint_DoucheToiletstoelen = 20, //	DTSTL	Douche-/toiletstoelen
        MediPoint_Ondersteken = 21,     //	ONSTK
        MediPoint_Toiletrekken = 22,    //	TOIRK
        MediPoint_Toiletstoelen = 23,   //	TOIST
        MediPoint_Toiletverhogers = 24, //	TOIVH
        MediPoint_Bedcarriers = 25,     //	BDCAR
        MediPoint_HoogLaagBedden = 26,  //	HLBED	Hoog-laag bedden
        MediPoint_Kinderbedden = 27,    //	KDBED
        MediPoint_Bedbeugels = 28,      //	BEDBG
        MediPoint_Bedleestafels = 29,   //	BEDLT
        MediPoint_Bedverhogers = 30,    //	BEDVH
        MediPoint_Bedverlengers = 31,   //	BEDVL
        MediPoint_Dekenbogen = 32,      //	DEKBG
        MediPoint_Papegaaien = 33,      //	PAPEG
        MediPoint_Rugsteunen = 34,      //	RUGST
        MediPoint_Windringen = 35,      //	WNDRI
        MediPoint_Trippelstoelen = 36,  //	TRPST
        MediPoint_Standaarden = 37,     //	STNDR
        MediPoint_ADMatrassen = 38,     //	ADMAT	AD-matrassen
        MediPoint_Tilbanden = 39,       //	TILBD
        MediPoint_Toiletbanden = 40,    //	TOIBD
        MediPoint_ADZitkussens = 41,	//	ADZKS	AD-zitkussens
        MediPoint_Rolstoelen = 42	    //	RLSTL	Rolstoelen
    }

    public enum AfterCareType
    {
        [Description("ELV Palliatief")]
        ELVPalliatief = 6,
        [Description("Hospice")]
        Hospice = 16,
        [Description("Palliatieve zorg thuiszorg")]
        PalliatieveZorgThuiszorg = 21,
        [Description("Palliatieve zorg intramuraal")]
        PalliatieveZorgIntramuraal = 35
    }

    public enum AlertLevel
    {
        Log = 0,
        Email = 1
    }

    public enum AttachmentSource
    {
        [Description("")]
        None = 0,
        [Description("MSVT")]
        FrequencyMSVT = 1,
        [Description("CVA")]
        FrequencyCVA = 2,
        [Description("Doorlopend dossier")]
        FrequencyGeneral = 3
    }

    public enum AttachmentTypeID
    {
        [Description("")] None = 0,
        [Description("Afspraken patient")] PatientAppointment = 1,
        [Description("Eerstelijn verblijf")] ShortTemporaryStay = 2,
        [Description("GRZ formulier")] GRZForm = 3,
        [Description("Huisarts brief")] GeneralPractitionerLetter = 4,
        [Description("Hulpmiddelen formulier")] ApparatusForm = 5,
        [Description("Indicatiestelling")] Indication = 6,
        [Description("Medicatielijst")] MedicationList = 7,
        [Description("Medische gegevens")] MedicalDetails = 8,
        [Description("MSVT Indicatie")] MSVTIndication = 9,
        [Description("Paramedische overdracht")] ParamedicalTransfer = 10,
        [Description("Recept")] Prescription = 11,
        [Description("Specialist brief (ontslagbrief)")] DismissionLetter = 12,
        [Description("Terminaal verklaring")] TerminalStatement = 13,
        [Description("Toestemming patient")] PatientConsent = 14,
        [Description("Uitvoeringsverzoek")] FullfillmentRequest = 15,
        [Description("Verpleegkundige Overdracht")] NurseTransfer = 16,
        [Description("Wond formulier")] InjuryForm = 17,
        [Description("Verpleegkundige Overdracht (als bijlage)")] ExternVODocument = 19,
        [Description("Dump Report")] DumpReport = 20,
        [Description("Machtigingsformulier")] AuthorizationForm = 21,
        [Description("GVP formulier")] GVpForm = 22,
        [Description("Log Read Report")] LogRead = 23,
        [Description("Overig")] Other = 18,        
    }

    public enum AuthorizationStatus
    {
        NoTransferID = 0,
        NotAllowed = 1,
        Allowed = 2
    }

    public enum CommunicationLogType
    {
        [Description("")]
        None = 0,
        [Description("PDF naar VVT via zorgmail")]
        ZorgmailSendPDF = 1,
        [Description("CDA naar VVT via zorgmail")]
        ZorgmailSendCDA = 2,
        [Description("Huisartsbericht via zorgmail")]
        ZorgmailMedvri = 3,
        [Description("Notificatie naar VVT")]
        ToRequestDesiredOrganization = 4,
        [Description("Notificatie naar CIZ")]
        ToCIZ = 5,
        [Description("Notificatie naar Ziekenhuis")]
        RequestToHospital = 6,
        [Description("PDF naar Ziekenhuis via zorgmail")]
        ZorgmailSendPDFToHospital = 7,
        [Description("CDA naar Ziekenhuis via zorgmail")]
        ZorgmailSendCDAToHospital = 8,
        [Description("Notificatie naar Ziekenhuis afdeling")]
        RequestToHospitalDepartment = 9,
        [Description("Doorlopend dossier terugkoppeling")]
        Frequency = 10,
        [Description("Foutmelding")]
        Error = 11,
        [Description("Notificatie VO")]
        NotifyVO = 12,
        [Description("Ingetrokken")]
        Canceled = 13,
        [Description("VO via Nedap")]
        NedapVO = 14,
        [Description("Bijlage via Nedap")]
        NedapAttachment = 15,
        [Description("Formulier via Nedap")]
        NedapForm = 16,
        [Description("Vegro Order")]
        VegroOrder = 17,
        [Description("Notificatie naar Gerealiseerde VVT")]
        ToRealizedOrganization = 18,
        [Description("Huisartsbericht via HL7-ORU")]
        ORUNoticeDoctor = 19,
        [Description("Notificatie naar GRZ")]
        ToGRZ = 20,
        [Description("MediPoint Order")]
        MediPointOrder = 21,
        [Description("Signalering reminder email")]
        SignaleringReminderEmail = 22,
        [Description("Notificatie verplaatsing dossier")]
        RelocationOfDossier = 23
    }

    public enum CSVTemplateTypeID
    {
        [Description("CSV Dump")] CSVDump = 1,
        [Description("Medewerker Export")] EmployeeExport = 2,
    }

    public enum DepartmentTypeID
    {
        Afdeling = 1,
        Transferpunt = 2,
        CentraalMeldpunt = 3 //VVT
    }

    public enum DossierHandling
    {
        [Description("Alle")] All = 0,

        [Description("Niet in behandeling")]
        // "Patiënten/dossiers nog niet in behandeling genomen" 
        NotHandling = 1,

        [Description("In behandeling")] // i.e. Phase 2 exists
        Handling = 2,

        [Description("In behandeling door mij")]
        // "dossier waar mijn naam staat in het veld In behandeling genomen door"
        HandledByMe = 3,

        [Description("Aangemaakt door mij")] // "dossier aangemaakt door mij
        CreatedByMe = 5,

        [Description("Aangemaakt door afdeling")] // "dossier aanvraagformulier aangemaakt door afdeling
        CreatedByDepartment = 6,

        [Description("Mijn dossier")]
        OwnedByMe = 7,

        [Description("Dossiers van mijn afdeling")]
        OwnedByMyDepartment = 8
    }

    public enum DossierStatus
    {
        [Description("Actueel en gesloten")] All = 0,
        [Description("Actueel")] Active = 1,
        [Description("Onderbroken")] Interrupted = 2,
        [Description("Kan overnemen")] CanBeTransferred = 3,
        [Description("Kan wellicht overnemen")] CanPerhapsBeTransferred = 8,
        [Description("Kan niet overnemen")] CannotBeTransferred = 4,
        [Description("Definitief / Af te sluiten")] CanBeClosed = 5,
        [Description("Afgesloten")] IsClosed = 6,
        [Description("Actief / Gesloten < 14 dagen")] ActiveOrClosedWithin14days = 7
    }

    public enum DossierPhaseType
    {
        [Description("Pre transfer")] PreTransfer = 0,
        [Description("Transfer")] Transfer = 1,
        [Description("Ontvangst / Besluit")] ReceiveAndDecision = 2,
        [Description("Overdracht")] Overdracht = 3
    }

    public enum AcceptanceState
    {
        Refused = 0,
        Acknowledged = 1,
        AcknowledgedPartially = 2
    }

    public enum DossierType
    {
        [Description("Alle")] All = 0
        // FUTURE CVA & MSVT etc.
    }

    public enum DumpEncodingType
    {
        [Description("Openen in Excel")]
        Excel = 0,
        [Description("Importeren in SQL")]
        SQL = 1
    }

    public enum DumpTransferExportHealthCareProviderType
    {
        [Description("Gerealiseerde-VVT")]
        Realized = 1,
        [Description("Gewenste-VVT")]
        Desired = 2
    }

    public enum DumpTransferExportType
    {
        [Description("Bestaande transfers")]
        Active = 1
    }

    public enum EOverdrachtType
    {
        Volwassenen = 0,
        Baby = 1,
        Kind = 2
    }

    /// <summary>
    ///     This enum represents the ID of table 'FlowDefinition' (is not the same as 'FlowType')
    /// </summary>
    public enum FlowDefinitionID
    {
        [Description("VVTZH")]
        VVT_ZH = 1,
        [Description("ZHVVT")]
        ZH_VVT = 2,
        [Description("RzTP")]
        RzTP = 3,
        [Description("CVA")]
        CVA = 4,
        [Description("ZHZH")]
        ZH_ZH = 5,
        [Description("HAVVT")]
        HA_VVT = 6,
        [Description("VVTVVT")]
        VVT_VVT = 7
    }

    public enum FlowDirection
    {
        [Description("Geen")] None = -1,
        [Description("Verzender")] Sending = 0,
        [Description("Ontvanger")] Receiving = 1,
        [Description("Beide")] Any = 2
    }

    public enum FlowFieldType
    {
        None = 0,
        String = 1,
        DateTime = 2,
        Time = 3,
        Number = 4,
        RadioButton = 5,
        Checkbox = 6,
        DropDownList = 7,
        IDText = 8,
        Date = 9,
        CheckboxList = 10,
        Hidden = 11,
        DropDownListMultiSelect = 12,
        Email = 13,
        Group = 100,
    }

    public enum FlowFieldDataSourceType
    {
        None,
        List,
        VerticalList,
        Entity,
        IDText
    }

    public enum Source
    {
        DEFAULT = 0,
        USER = 1,
        POINT_PREFILL = 10,
        WEBSERVICE = 20,
        HL7 = 30
    }

    public enum FlowHandling
    {
        Receive = 0,
        ReceiveDecision = 5,
        ReceiveEvaluateDecision = 6,
        IndicatingCIZ = 10,
        IndicatingCIZDecision = 15,
        IndicatingGRZ = 20,
        IndicatingGRZDecision = 25,
        RequestForm = 30,
        RequestInBehandeling = 40,
        OpstellenVO = 50,
        Send = 60,
        Forward = 65,
        Aanvullen = 70,
        MSVTUVV = 75,
        MSVTIND = 80,
        GRZ = 81,
        ELV = 82,
        CloseDossier = 100,
        DoorlDossier = 200
    }

    public enum FlowFormType
    {
        NoScreen = 1,
        SAMLRequest = 2,

        Overdracht_NEW = 24,
        MSVT_Uitvoeringsverzoek = 30,
        MSVT_Indicatiestelling = 31,
        ZorgPlanTransfer = 32,
        MSVT_Frequency = 33,

        GegevensPatient = 101,
        VerpleegkundigeOverdracht = 102,
        VersturenNaarZH = 103,
        InBehandelingTP = 104,
        DoorsturenNaarAfd = 105,
        AfsluitenDossier = 106,

        GegevensPatientZHVVT = 201,
        AanvraagFormulierZHVVT = 202,
        AanvraagFormulierHAVVT = 241,
        AanvullendeGegevensTPZHVVT = 203,
        RouterenDossierZHVVT = 204,
        TransferRouteHAVVT = 248,
        InBehandelingIIZHVVT = 205,
        WLZIndicatieZHVVT = 206,
        InBehandelingVVTZHVVT = 207,
        VastleggenBesluitVVTZHVVT = 208,
        VerpleegkundigeOverdrachtFlow = 209,
        EOverdracht30 = 250,
        EvaluateDecision = 251,

        // 210 used as differentiation for AanvraagFormulierZHVVT
        // 211 used as differentiation for AanvraagFormulierZHVVT
        // 212 used as differentiation for AanvraagFormulierZHVVT
        // 213 used as differentiation for AanvraagFormulierZHVVT
        RequestFormZHVVT_FormulierStandaard = 210,
        RequestFormZHVVT_FormulierRetourVPHTZ = 211,
        RequestFormZHVVT_FormulierPoli = 212,

        MSVTUitvoeringsverzoek = 214,
        AfsluitenDossierZHVVT = 215,
        GRZFormulier = 216,
        VersturenVO = 217,
        MSVTIndicatiestelling = 218,
        DoorlopendDossierZHVVT = 219, // is this used?
        GRZIndicatie = 220,
        RealizedCare = 221,
        ZorgAdvies = 222,
        CommunicationJournal = 223,
        CIZOverview = 224,
        Signature = 225,
        RouterenDossierRzTP = 226,
        AanvraagFormulierRzTP = 227,
        AfsluitenDossierRzTP = 228,
        InvullenCVARegistratieZH = 229,
        CVARouteSelection = 230,
        InvullenCVARegistratieVVT = 231,
        InvullenCVARegistratie3mnd = 232,
        // 233 used as differentiation for AanvraagFormulierZHVVT
        MSVTIndDoorldossier = 234,
        DoorlopendDossier = 235,
        AfsluitenDossierCVA = 236,
        // 237 Poli (CVA) AanvraagFormulierZHVVT
        AfwegingELV = 238,
        AanvraagFormulierZHZH = 239,
        PatientenBrief = 240,
        Hulpmiddelen = 242,
        RequestFormZHVVT_VVTVVT = 243,

        RequestFormHAVVT_Thuiszorg = 244,
        RequestFormHAVVT_Opname = 245,
        RequestFormHAVVT_Consultatie = 246,
        RequestFormHAVVT_Kort = 247,
        RequestFormHAVVT_TijdelijkeZorg = 253,

        GVp = 249,

        AfwegingELVHA = 254,
        AfsluitenDossierHA = 255,
        RealizedCareHA = 256,




        // Not active as FormType records
        Dashboard = 9000,
        NewFlowInstance = 9001,
        StartFlowInstance = 9002,
        InterruptTransfer = 9003,
        CopyTransfer = 9004,
        ReopenTransfer = 9005,
        ResumeTransfer = 9006,
        DeleteTransfer = 9007,
        PrintTransfer = 9008,
        Attachments = 9009,
        DocumentAndForms = 9010,
        Emails = 9011,

        RelocateDepartment = 9013,
        RelocateOrganization = 9014,
        Frequency = 9015,
        TakeOverData = 9016,

        AdminEmployee = 2000,
        AdminOrganization = 2001,
        AdminLocation = 2002,
        AdminDepartment = 2003,
        AdminBulkEmployee = 2004,
        AdminRegion = 2005,

        FormServiceAttachment = 3000,
        FormServiceFormFields = 3001
    }

    public enum FlowInstanceStatusTypeID
    {
        Onderbreken = 1,
        Hervatten = 2,
        Afsluiten = 3,
        Heropenen = 4
    }

    public enum FlowTypeID
    {
        VVT_ZH = 1,
        ZH_VVT = 2,
        ZH_ZH = 3,
        HA_VVT = 4,
        VVT_VVT = 5
    }

    public enum FrequencyDossierStatus
    {
        [Description("Alle")] All = 0,
        [Description("Actueel")] Active = 1,
        [Description("Open")] Open = 2,
        [Description("Gesloten")] Close = 3
    }

    public enum FrequencyStatus
    {
        [Description("Niet actief")] NotActive = 0,
        [Description("Actief")] Active = 1,
        [Description("Afgerond")] Done = 2
    }

    public enum FrequencyType
    {
        [Description("")]
        None = 0,
        [Description("MSVT")]
        MSVT = 1,
        [Description("CVA")]
        CVA = 2,
        [Description("Algemeen")]
        General = 3
    }

    public enum FrequencyFeedbackType
    {
        [Description("Geen notificatie")]
        None = 0,
        [Description("Email")]
        Email = 1,
        [Description("Point Signalering")]
        Signalering = 2,
        [Description("Beide")]
        Both = 3
    }

    public enum FunctionRoleLevelID
    {
        [Description("Geen")]
        None = 0,
        [Description("Afdeling")]
        Department = 1,
        [Description("Locatie")]
        Location = 2,
        [Description("Organisatie")]
        Organization = 3,
        [Description("Regionaal")]
        Region = 4,
        [Description("Globaal")]
        Global = 5
    }

    public enum FunctionRoleTypeID
    {
        [Description("Beheer artsen")]
        ManageDoctors = 21,
        [Description("Beheer organisatie(s)")]
        ManageOrganization = 22,
        [Description("Beheer medewerkers")]
        ManageEmployees = 23,
        [Description("Beheer favorieten")]
        ManageFavorites = 24,
        [Description("Beheer zoekscherm")]
        ManageSearch = 25,
        [Description("Beheer documenten")]
        ManageDocuments = 26,
        [Description("Beheer formulieren")]
        ManageForms = 27,
        [Description("Beheer capaciteit")]
        ManageCapacity = 28,
        [Description("Beheer servicepaginas")]
        ManageServicePage = 29,
        [Description("Beheer memo templates")]
        ManageTransferMemoTemplates = 30,
        [Description("Beheer regio(s)")]
        ManageRegion = 31,
        [Description("Beheer schermen")]
        ManageScreens = 32,
        [Description("Blokkering opheffen formulieren")]
        UnlockForms = 33,
        [Description("Raadplegen communicatie log")]
        ManageLogs = 100,
        [Description("Rapportage")]
        Reporting = 500,
        [Description("Te beheren dossier toegangsniveau")]
        MaxDossierLevelAdmin = 600,
        [Description("Beheer alles")]
        ManageAll = 999
    }

    public enum Gender //Using char in the database
    {
        [Description("Man")] m = 1,
        [Description("Vrouw")] v = 2,
        [Description("Onbekend/Anders")] o = 3,
    }

    /// <summary>
    ///     Note: The int's used over here don't match the id's in the database.
    /// </summary>
    public enum GeneralActionTypeName
    {
        [Description("Allen")] All = -1,
        [Description("Automatisch/Code-driven uit te voeren acties")] Automatic = 1,
        [Description("Handmatig uit te voeren acties via menu-optie")] Menu = 2,
        [Description("Fase-specifieke actie, handmatig uit te voeren via knop")] Phase = 3,
        [Description("Handmatig uit te voeren acties via de dossier-sectie in menu")] MenuProcess = 4,
        [Description("Handmatig uit te voeren acties via de aanvullend dossier-sectie in menu")] MenuTransfer = 5
    }

    public enum HashAlgorythm
    {
        SHA1,
        SHA256
    }

    public enum InterruptionType
    {
        [Description("Niet bekend")] Unknown = -1,
        [Description("Onderbroken")] Interrupted = 1,
        [Description("Hervatten")] Resumed = 2
    }

    public enum InviteStatus
    {
        [Description("Ingetrokken")]
        Cancelled = 0,
        [Description("In verzendrij")]
        Queued = 1,
        [Description("Aangeboden")]
        Active = 2,
        [Description("Bevestigd")]
        Acknowledged = 3,
        [Description("Afgewezen")]
        Refused = 4,
        [Description("Gerealiseerd")]
        Realized = 5,
        [Description("Deels bevestigd")]
        AcknowledgedPartially = 6,
    }

    public enum InviteType
    {
        None = -10,
        Regular = 0,
        Mediating = 5,
        IndicatingCIZ = 10,
        IndicatingGRZ = 15,
        DoorlDossier = 20
    }

    public enum MenuItemDestination
    {
        Menu = 0,
        FormTab = 1,
        Dashboard = 2
    }

    public enum MenuItemStatus
    {
        Invisible = 0,
        Disabled = 1,
        Visible = 2
    }

    public enum MenuItemType
    {
        None = 0,
        CreateNew = 1,
        CreateNewInActiveSubcategory = 2,
        SelectExisting = 3
    }

    public enum MFACodeType
    {
        Register = 1,
        Login = 2
    }

    public enum MFAAuthType
    {
        [Description("Via SMS")]
        SMS = 1,
        [Description("Google authenticator-app")]
        Google = 2
    }

    public enum OrganizationTypeID
    {
        [Description("Ziekenhuis")] Hospital = 1,
        [Description("VVT")] HealthCareProvider = 2,
        [Description("GGZ")] GGZ = 3,
        [Description("Klinische Revalidatie")] ClinicalRehabilitation = 4,
        [Description("Non-VVT")] NonHealthCareProvider = 5,
        [Description("CIZ")] CIZ = 6,
        [Description("Gemeente")] Municipality = 7,
        [Description("Revalidatie Centrum")] Rehabilitation = 8,
        [Description("Intensieve revalidatie verpleeghuis")] RehabilitationNursing = 9,
        [Description("Zorgkantoor")] CareOffice = 10,
        [Description("Beheer")] Beheer = 11,
        [Description("Huisarts")] GeneralPracticioner = 12,
        [Description("(Regionaal) coördinatiepunt")] RegionaalCoordinatiepunt = 13,
        [Description("Indicerende Instelling GRZ")] GRZ = 14
    }

    public enum Participation
    {
        [Description("Geen")]
        None = 0,
        [Description("Alleen Fax/Mail")]
        FaxMail = 1,
        [Description("Actief")]
        Active = 2,
        [Description("Actief maar Onzichtbaar")]
        ActiveButInvisibleInSearch = 3
        //it must be used only to make an active flow invisible. 
    }
    public enum PointTransferPhase
    {
        [Description("Alles")] None = -1,
        [Description("Aanmaken aanvraag patiënt")] Phase0 = 0,
        [Description("Versturen naar het transferpunt")] Phase1 = 1,
        [Description("Behandeling door Transferpunt")] Phase2 = 2,
        [Description("Versturen naar CIZ")] Phase3 = 3,
        [Description("Behandeling door CIZ")] Phase4 = 4,
        [Description("Uitslag CIZ")] Phase5 = 5,
        [Description("Ontslagvoorbereiding; Versturen naar Zorgaanbieder")] Phase6 = 6,
        [Description("Ontslagvoorbereiding; Behandeling door Zorgaanbieder")] Phase7 = 7,
        [Description("Ontslagvoorbereiding; Uitslag Zorgaanbieder")] Phase8 = 8,
        [Description("Ontslagvoorbereiding; Overdracht")] Phase9 = 9,
        [Description("Ontslagvoorbereiding; CVA Versturen naar Zorgaanbieder")] Phase91 = 91,
        [Description("Ontslagvoorbereiding; CVA Behandeling door Zorgaanbieder")] Phase92 = 92,
        [Description("Ontslagvoorbereiding; CVA Uitslag Zorgaanbieder")] Phase93 = 93,
        [Description("Afsluiten dossier")] Phase10 = 10,
        [Description("Afgesloten")] Close = 11,
        [Description("12")] ReOpen = 12
    }

    public enum ReceiveStateFilter
    {
        Active = 0,
        CancelledOrRefused = 10,
        CancelledOrRefusedAndActive = 20
    }


    public enum RegionForSearch
    {
        [Description("Eigen regio")] OwnRegion = 0,
        [Description("Landelijk")] AllRegions = 1
    }

    public enum RehabilitationCenterType
    {
        RehabilitationCenter = 1,
        IntensiveRehabilitationNursinghome = 2
    }

    public enum ReportDefinition
    {
        [Description("Afdeling overzicht")] DepartmentOverview = 33
    }

    public enum ReportPermissionType : int
    {
        None = 0,

        // RegionID, OrganizationID, LocationID, DepartmentID
        Source = 1,

        // [TP Gerealiseerde zorgaanbieder, LocationID], [TP Gerealiseerde zorgaanbieder, DepartmentID], [TP Van(gewenste zorgaanbieder), RegionID],
        // [TP Van(gewenste zorgaanbieder), OrganizationID], [TP Van(gewenste zorgaanbieder), LocationID], [TP Van(gewenste zorgaanbieder), DepartmentID],
        // [Ontvanger RegionID], [Ontvanger OrganizationID], [Ontvanger LocationID], [Ontvanger DepartmentID]
        Destination = 2
    }

    public enum ReportRoleID
    {
        Region = 1,
        Organization = 2,
        Location = 3,
        Department = 4
    }

    public enum Rights //Using char in the database
    {
        [Description("None")] None = 0,
        [Description("Read")] R = 1,
        [Description("Write")] W = 2,
        [Description("Execute")] X = 3
    }

    public enum Role
    {
        [Description("Geen")]
        None = 0,
        [Description("Afdeling")]
        Department = 10,
        [Description("Locatie")]
        Location = 20,
        [Description("Organisatie")]
        Organization = 30,
        [Description("Regionaal")]
        Region = 40,
        [Description("Globaal")]
        Global = 999
    }

    public enum ScopeType
    {
        [Description("Geen")] None = 0,
        [Description("Alle dossiers")] AllTransfers = 1,
        [Description("Patiënten nog niet in behandeling")] NotProcessing = 2,
        [Description("Patiënten in behandeling")] Handled = 3,
        [Description("Patiënten in behandeling door mij")] HandledByMe = 4,
        [Description("Patiënten in behandeling door<br />de afdeling")] HandledByDepartment = 5,
        [Description("Onderbroken")] Interrupted = 6,
        [Description("ZA kan WEL overnemen")] Accepted = 7,
        [Description("ZA kan NIET overnemen")] NotAccepted = 8,
        [Description("VO in de maak")] InTheMaking = 9,
        [Description("VO Definitief / Af te sluiten")] Definitive = 10,
        [Description("Alle dossiers behalve CVA")] WithoutCVA = 11,
        [Description("Alle dossiers behalve MSVT")] WithoutMSVT = 12,
        [Description("Alle dossiers behalve CVA en MSVT")] WithoutCVAandMSVT = 13,
        [Description("Actief en gesloten < 14d")] LessThan14Days = 14,
        [Description("Alleen CVA dossiers")] CVA = 15,
        [Description("Alleen MSVT dossiers")] MSVT = 16,
        [Description("Afgesloten")] Closed = 17,
        [Description("Doorlopend dossier actief binnen de periode")] FeedbackActiveInRange = 18,
        [Description("Doorlopend dossier actief buiten de periode")] FeedbackActiveOutRange = 19,
        [Description("Doorlopend dossier gesloten")] FeedbackClosed = 20,
        [Description("ZA kan Wellicht overnemen")] PartiallyAccepted = 21,
        [Description("Mijn dossiers")] OwnedByMe = 22,
        [Description("Dossiers van mijn afdeling")] OwnedByMyDepartment = 23,
        [Description("Dossiers aangemaakt door mij")] CreatedByMe = 24,
        [Description("Dossiers aangemaakt door mijn afdeling")] CreatedByDepartment = 25
    }

    public enum ScreenType
    {
        NoScreenType = 1,
        Persoon = 2,
        SysteemObject = 3,
        Organisatie = 4,
        Overig = 5,
        MedischeGegevens = 6
    }

    public enum SearchComparisonOperatorType
    {
        [Description("Bevat")] Like = 0,
        [Description("Is precies")] Equal = 1,
        [Description("Is niet")] NotEqual = 2,
        [Description("Bevat niet")] NotLike = 3,
        [Description("Vroeger dan")] LessThan = 4,
        [Description("Later dan")] GreaterThan = 5
    }

    public enum SearchDestination
    {
        Dashboard = 0,
        //ClientVVT = 1,
        //ClientFull = 2,
        CommunicationJournal = 3,
        AdditionalInfoRequestTP = 4,
        TransferAttachment = 5,
        ListFromFrequencySearch = 6,
        TransferRoute = 7,
        RequestFormZHVVT = 8,
        FeedbackTransferMemo = 9,
        FeedbackAttachment = 10,
        RequestFormRzTP = 11,
        TransferRouteRzTP = 12,
        AcceptAtHospital = 13,
        RequestFormHAVVT = 14,
        GRZForm = 15,
    }

    public enum SearchFieldOwningType
    {
        None = 0,
        Transfer = 1,
        Frequency = 2
    }

    public enum SearchLogicOperatorType
    {
        [Description("En")] And = 0,
        [Description("Of")] Or = 1
    }

    public enum SearchPropertyType
    {
        String = 0,
        Numeric = 1,
        Date = 2,
        Lookup = 3,
        LookupDate = 4,
        DateTime = 5
    }

    public enum SearchType
    {
        None = 0,
        Transfer = 1,
        Frequency = 2,
        Mobile = 3,
        PatientOverview = 4
    }

    public enum SendDestinationType
    {
        None = 0,
        Nedap = 1
    }

    public enum SignaleringType
    {
        FormSetVersion = 1,
        GeneralAction = 2
    }

    public enum SortOrder
    {
        asc = 0,
        desc = 1
    }

    public enum Status
    {
        [Description("Niet actief")] NotActive = 0,
        [Description("Actief")] Active = 1,
        [Description("Afgerond")] Done = 2
    }

    public enum SupplierID
    {
        Vegro = 1,
        Point = 2,
        MediPoint = 3
    }

    public enum TemplateTypeID
    {
        ZorgMailZHVVTJa = 1,
        PasswordResetMail = 2,
        PatientenBrief = 3,
        ZorgMailHAVVTJa = 4,
        ZorgMailHAVVTNee = 5,
        ZorgMailHAVVTNeeEnNieuweVVT = 6,
        UnblockUserMail = 7,
        DumpReportMail = 8,
        LogReadMail = 9,
        ErrorLogReadMail = 10
    }

    public enum TempletTypeID
    {
        [Description("")] None = 0,
        [Description("Verpleging")] Nursing = 1,
        [Description("Transferbureau")] Transferpoint = 2,
        [Description("VVT")] HealthCareProvider = 3,
        [Description("Algemeen")] General = 4
    }

    public enum TransferMemoTypeID
    {
        [Description("Zorgprofessional")]
        Zorgprofessional = 1,
        [Description("Patient")]
        Patient = 2,
        [Description("Transferpunt")]
        Transferpunt = 3,
        [Description("Aanvullende gegevens bevindingen medewerker TP")]
        AanvullendeGegevensBevindingenMedewerkerTransferpunt = 4,
        [Description("Aanvullende gegevens afspraken")]
        AanvullendeGegevensAfspraken = 5,
        [Description("Doorlopend dossier - MSVT")]
        FrequencyMSVT = 7,
        [Description("Intercollegiale communicatie")]
        IntercollegiateCommunications = 8,
        [Description("Doorlopend dossier - Algemeen")]
        FrequencyGeneral = 9,
        [Description("Doorlopend dossier - CVA")]
        FrequencyCVA = 10
    }

    public enum TrendReportFilterSource : int
    {
        Region = 0,
        Organization = 1,
        Location = 2,
        Department = 3
    }

    /// <summary>
    ///     1-9 are equal to the ones, used in Point.Business.Application\Logic\FormTypeBC.vb
    /// </summary>
    public enum TypeID
    {
        [Description("Aanvraag-formulier")] Aanvraag = 1,
        [Description("Overdracht-formulier")] Overdracht = 2,
        [Description("Extra informatie")] ExtraInformatie = 3,
        [Description("Overige")] Overige = 4,
        [Description("Formulier waar meerdere exemplaren van kunnen bestaan")] ExemplaarFormulier = 7,
        [Description("Voortgang transferproces-details")] TransferDetails = 8,
        [Description("GRZ Formulier en Uitslag")] GRZ = 9,
        [Description("Zorgplan")] Zorgplan = 10,

        [Description("Proces formulier")] FlowProcessForm = 100,
        [Description("Proces scherm")] FlowProcessScreen = 101,
        [Description("Dossier formulier")] FlowTransferForm = 102,
        [Description("Dossier scherm")] FlowTransferScreen = 103,
        [Description("Doorlopend formulier")] FlowDoorlopendForm = 104,

        [Description("Aanvraag-formulier (flow)")] FlowAanvraag = 201,
        [Description("Overdracht-formulier (flow)")] FlowOverdracht = 202,
        [Description("Inbehandeling-formulier (flow)")] FlowReceive = 203,
        [Description("Accepteer-formulier (flow)")] FlowAccept = 204,
        [Description("Evalueer-besluit-formulier (flow)")] FlowEvaluate = 205,
    }

    public enum TakeOverRelatedTransferStatus : int
    {
        DoNotTakeOver = -1
    }
}