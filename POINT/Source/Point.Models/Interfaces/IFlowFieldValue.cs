﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Point.Models.Interfaces
{
    public interface IFlowFieldValue
    {
        int FlowFieldID { get; set; }
        string Value { get; set; }
    }
}
