﻿using System.Xml.Serialization;
using System.Collections.Generic;
namespace Point.Models.Iguana
{
    [XmlRoot(ElementName = "Channel")]
    public class Channel
    {
        [XmlAttribute(AttributeName = "Name")]
        public string Name { get; set; }
        [XmlAttribute(AttributeName = "Guid")]
        public string Guid { get; set; }
        [XmlAttribute(AttributeName = "Status")]
        public string Status { get; set; }
        [XmlAttribute(AttributeName = "Source")]
        public string Source { get; set; }
        [XmlAttribute(AttributeName = "SourceStatus")]
        public string SourceStatus { get; set; }
        [XmlAttribute(AttributeName = "Destination")]
        public string Destination { get; set; }
        [XmlAttribute(AttributeName = "DestinationStatus")]
        public string DestinationStatus { get; set; }
        [XmlAttribute(AttributeName = "SuccessLogs")]
        public string SuccessLogs { get; set; }
        [XmlAttribute(AttributeName = "SourceErrorLogs")]
        public string SourceErrorLogs { get; set; }
        [XmlAttribute(AttributeName = "DestinationErrorLogs")]
        public string DestinationErrorLogs { get; set; }
        [XmlAttribute(AttributeName = "TotalErrorLogs")]
        public string TotalErrorLogs { get; set; }
        [XmlAttribute(AttributeName = "MessagesQueued")]
        public string MessagesQueued { get; set; }
        [XmlAttribute(AttributeName = "TotalProcessed")]
        public string TotalProcessed { get; set; }
        [XmlAttribute(AttributeName = "CurrentProcessed")]
        public string CurrentProcessed { get; set; }
        [XmlAttribute(AttributeName = "LastActivityTime")]
        public string LastActivityTime { get; set; }
    }

    [XmlRoot(ElementName = "IguanaStatus")]
    public class IguanaStatus
    {
        [XmlElement(ElementName = "Channel")]
        public List<Channel> Channel { get; set; }
        [XmlAttribute(AttributeName = "DateTime")]
        public string DateTime { get; set; }
        [XmlAttribute(AttributeName = "NumberOfChannels")]
        public string NumberOfChannels { get; set; }
        [XmlAttribute(AttributeName = "NumberOfRunningChannels")]
        public string NumberOfRunningChannels { get; set; }
        [XmlAttribute(AttributeName = "NumberOfLicensedChannels")]
        public string NumberOfLicensedChannels { get; set; }
        [XmlAttribute(AttributeName = "TotalLogs")]
        public string TotalLogs { get; set; }
        [XmlAttribute(AttributeName = "TotalSuccessLogs")]
        public string TotalSuccessLogs { get; set; }
        [XmlAttribute(AttributeName = "TotalSourceErrorLogs")]
        public string TotalSourceErrorLogs { get; set; }
        [XmlAttribute(AttributeName = "TotalDestinationErrorLogs")]
        public string TotalDestinationErrorLogs { get; set; }
        [XmlAttribute(AttributeName = "TotalErrors")]
        public string TotalErrors { get; set; }
        [XmlAttribute(AttributeName = "TotalServiceErrors")]
        public string TotalServiceErrors { get; set; }
    }

}
