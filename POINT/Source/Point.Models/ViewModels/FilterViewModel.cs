﻿using Point.Models.Enums;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Point.Models.ViewModels
{
    public class FilterViewModel
    {
        public FilterViewModel()
        {
            OrganizationTypeIDs = new int[] { };
            OrganizationIDs = new int[] { };
            LocationIDs = new int[] { };
            DepartmentIDs = new int[] { };
            OrganizationProjects = new string[] { };
            FlowDefinitionIDs = new FlowDefinitionID[] { };
        }

        [DisplayName("Gebruik excel")]
        public bool UseExcel { get; set; }

        [DisplayName("Gebruik CSV")]
        public bool UseCSV { get; set; }

        [DisplayName("Rapport")]
        [Required]
        public int ReportDefinitionID { get; set; }
        public string ReportActionName { get; set; } = "Index";

        [DisplayName("Flow(s)")]
        public FlowDefinitionID[] FlowDefinitionIDs { get; set; }

        [DisplayName("Dossier status")]
        public DossierStatus? DossierStatus { get; set; }

        [DisplayName("Regio")]
        public int? RegionID { get; set; }

        [DisplayName("Organisatie type(s)")]
        public int[] OrganizationTypeIDs { get; set; }

        [DisplayName("Organisatie(s)")]
        [Required(ErrorMessage = "Kies tenminste 1 organisatie")]
        public int[] OrganizationIDs { get; set; }
        [DisplayName("Locatie(s)")]
        public int[] LocationIDs { get; set; }
        [DisplayName("Afdeling(en)")]
        public int[] DepartmentIDs { get; set; }
        [DisplayName("Kenmerk(en)")]
        public string[] OrganizationProjects { get; set; }

        [DisplayName("Startdatum")]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime StartDate { get; set; }

        [DisplayName("Datum t/m")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime EndDate { get; set; }
    }
}
