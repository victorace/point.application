﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Point.Models.ViewModels
{
    public class FlowFieldValueModel
    {
        public int FlowFieldID;
        public string Value;
        public int? FlowFieldDataSourceID;
    }
}
