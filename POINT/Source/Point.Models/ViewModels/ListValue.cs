﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Point.Models.ViewModels
{
    public class ListValue
    {
        public string Value { get; set; }
        public string Text { get; set; }
    }
}
