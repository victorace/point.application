﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Point.Models.LDM.EOverdracht30.Enums
{
    public enum ImportSourceType
    {
        Point = 0
    }

    public enum OrganisatieType
    {
        [Description("Ziekenhuis")] V4,
        [Description("Universitair Medisch Centrum")] V5,
        [Description("Algemeen ziekenhuis")] V6,
        [Description("Zelfstandig behandelcentrum")] Z4,
        [Description("Diagnostisch centrum")] Z5,
        [Description("Echocentrum")] B2,
        [Description("Verplegings - of verzorgingsinstelling")] X3,
        [Description("Verpleeghuis")] R5,
        [Description("Verzorgingstehuis")] M1,
        [Description("Openbare apotheek")] J8,
        [Description("Zelfstandig opererende ziekenhuisapotheek")] K9,
        [Description("Huisartspraktijk (zelfstandig of groepspraktijk)")] Z3,
        [Description("Apotheekhoudende huisartspraktijk")] K3,
        [Description("Huisartsenpost (t.b.v. dienstwaarneming)")] N6,
        [Description("Laboratorium")] L1,
        [Description("Polikliniek (als onderdeel van een ziekenhuis)")] P4,
        [Description("Revalidatiecentrum")] R8,
        [Description("Preventieve gezondheidszorg")] P6,
        [Description("Geestelijke Gezondheidszorg")] G5,
        [Description("Verstandelijk gehandicaptenzorg")] G6,
        [Description("Thuiszorg")] T2,
        [Description("Jeugdgezondheidszorg")] JGZ,
        [Description("Verloskundigenpraktijk")] G3
    }

    public enum ZorgverlenersRol
    {
        [Description("Hoofdbehandelaar")] RESP,
        [Description("Verwijzer")] REF,
        [Description("Uitvoerder")] PRF,
        [Description("Tweede uitvoerder")] SPRF,
        [Description("Consulent")] CON,
        [Description("Behandelaar")] ATND,
        [Description("Anders")] OTH
    }

    public enum AanduidingBijNummer
    {
        [Description("Tegenover")] to,
        [Description("Bij")] By
    }

    public enum AdresSoort
    {
        [Description("Postadres")] PST,
        [Description("Officieel adres")] HP,
        [Description("Woon-/verblijfadres")] PHYS,
        [Description("Tijdelijk adres")] TMP,
        [Description("Werkadres")] WP,
        [Description("Vakantie adres")] HV
    }

    public enum TelecomType
    {
        [Description("Vast telefoonnummer")] LL,
        [Description("Fax")] FAX,
        [Description("Mobiel telefoonnummer")] MC,
        [Description("Pieper")] PG
    }

    public enum NummerSoort
    {
        [Description("Telefoonnummer thuis")] HP,
        [Description("Tijdelijk telefoonnummer")] TMP,
        [Description("Zakelijk telefoonnummer")] WP
    }

    public enum EmailSoort
    {
        [Description("Privé e-mailadres")] HP,
        [Description("Zakelijk e-mailadres")] WP
    }

    public enum IdentificatieSoort
    {
        [Description("Burgerservicenummer (BSN)")] BSN,
        [Description("Patientnummer")] PATIENTNR,
        [Description("Opnamenummer")] OPNAMENR
    }

    public enum Geslacht
    {
        [Description("Ongedifferentieerd")] OUN = 0,
        [Description("Man")] OM = 1,
        [Description("Vrouw")] OF = 2
    }

    public enum BurgerlijkeStaat
    {
        [Description("Gescheiden (huwelijk/geregistreerd partnerschap ontbonden)")] D = 0,
        [Description("Gehuwd")] M = 1,
        [Description("Nooit gehuwd geweest/geregistreerd partnerschap gehad")] S = 2,
        [Description("Geregistreerd partnerschap")] T = 3,
        [Description("Weduwe/weduwnaar")] W = 4
    }

    public enum GeverifieerdBij
    {
        [Description("Patiënt")] SNOMED_116154003,
        [Description("Ouder")] SNOMED_40683002,
        [Description("Voogd")] SNOMED_394619001,
        [Description("Gevolmachtigde")] SNOMED_8601000146109,
        [Description("Anders, namelijk")] OTH
    }

    public enum Behandeling
    {
        [Description("Opname op intensive care")] SNOMED_305351004,
        [Description("Cardiopulmonaire resuscitatie")] SNOMED_89666000,
        [Description("Kunstmatige beademing")] SNOMED_40617009,
        [Description("Toediening van een bloedproduct")] SNOMED_116762002,
        [Description("Overige behandelingen, namelijk")] OTH
    }

    public enum BehandelingToegestaan
    {
        [Description("Ja")] JA,
        [Description("Ja, maar met beperkingen")] JA_MAAR,
        [Description("Nee")] NEE
    }

    public enum WilsverklaringType
    {
        [Description("Niet reanimeren verklaring")] NR,
        [Description("Volmacht")] VOL,
        [Description("Behandelverbod")] VERB,
        [Description("Behandelverbod met aanvulling Voltooid Leven")] VERBVL,
        [Description("Mondelinge afspraak")] MON,
        [Description("Euthanasieverzoek")] EU,
        [Description("Euthanasieverzoek met aanvulling Dementie")] EUD,
        [Description("Levenswensverklaring")] LW,
        [Description("Verklaring donorschap")] DO,
        [Description("Anders")] OTH
    }

    public enum ProbleemLateraliteit
    {
        [Description("Links")] SNOMED_7771000,
        [Description("Rechts")] SNOMED_24028007,
        [Description("Rechts en links")] SNOMED_51440002
    }

    public enum ProbleemType
    {
        [Description("Diagnose")] SNOMED_282291009,
        [Description("Symptoom")] SNOMED_418799008,
        [Description("Klacht")] SNOMED_409586006,
        [Description("Functionele Beperking")] SNOMED_248536006,
        [Description("Complicatie")] SNOMED_116223007
    }

    public enum ProbleemStatus
    {
        [Description("Actueel")] SNOMED_55561003,
        [Description("Niet actueel")] SNOMED_73425007
    }

    public enum VerificatieStatus
    {
        [Description("Werk")] SNOMED_415684004,
        [Description("Differentiaal")] SNOMED_410590009,
        [Description("Bevestigd")] SNOMED_410605003,
        [Description("Uitgesloten")] SNOMED_410516002,
        [Description("Onbekend")] UNK
    }

    public enum Rol
    {
        [Description("Eerste relatie/contactpersoon")] COD472_1,
        [Description("Tweede relatie/contactpersoon")] COD472_2,
        [Description("Curator (juridisch)")] COD472_3,
        [Description("Financieel (gemachtigd)")] COD472_4,
        [Description("Financieel (toetsing)")] COD472_5,
        [Description("Leefeenheid")] COD472_6,
        [Description("Hulpverlener")] COD472_7,
        [Description("Anders")] COD472_9,
        [Description("Voogd")] COD472_11,
        [Description("Bewindvoerder")] COD472_14,
        [Description("Mentor")] COD472_15,
        [Description("Buur")] COD472_19,
        [Description("Vriend(in)/kennis")] COD472_20,
        [Description("Cliëntondersteuner")] COD472_21,
        [Description("Contactpersoon")] COD472_23,
        [Description("Wettelijke vertegenwoordiger")] COD472_24
    }

    public enum Relatie
    {
        [Description("Adoptievader")] ADOPTF,
        [Description("Adoptiemoeder")] ADOPTM,
        [Description("Tante")] AUNT,
        [Description("Broer")] BRO,
        [Description("Zwager")] BROINLAW,
        [Description("Neef/nicht, zoon/dochter van oom/tante")] COUSN,
        [Description("Dochter")] DAUC,
        [Description("Schoondochter")] DAUINLAW,
        [Description("Partner")] DOMPART,
        [Description("Vader")] FTH,
        [Description("Schoonvader")] FTHINLAW,
        [Description("Pleegdochter")] DAUFOST,
        [Description("Pleegvader")] FTHFOST,
        [Description("Pleegmoeder")] MTHFOST,
        [Description("Pleegzoon")] SONFOST,
        [Description("Kleindochter")] GRNDDAU,
        [Description("Opa")] GRFTH,
        [Description("Oma")] GRMTH,
        [Description("Kleinzoon")] GRNDSON,
        [Description("Overgrootvader")] GGRFTH,
        [Description("Overgrootmoeder")] GGRMTH,
        [Description("Echtgenoot")] HUSB,
        [Description("Moeder")] MTH,
        [Description("Schoonmoeder")] MTHINLAW,
        [Description("Neef, zoon van broer/zus")] NEPHEW,
        [Description("Nicht, dochter van broer/zus")] NIECE,
        [Description("Zuster")] SIS,
        [Description("Schoonzuster")] SISINLAW,
        [Description("Zoon")] SONC,
        [Description("Schoonzoon")] SONINLAW,
        [Description("Stiefvader")] STPFTH,
        [Description("Stiefmoeder")] STPMTH,
        [Description("Oom")] UNCLE,
        [Description("Echtgenote")] WIFE
    }

    public enum Verzekeringsoort
    {
        [Description("Basis")] B,
        [Description("Aanvullend verzekerd")] A,
        [Description("Basisverzekering vanuit AWBZ")] BZ,
        [Description("Hoofdverzekering")] H,
        [Description("Tandverzekering (los)")] T,
        [Description("Aanvullend + tand")] AT
    }

    public enum Gezinssamenstelling
    {
        [Description("Alleenwonend")] SNOMED_105529008 = 0,
        [Description("Samenwonend met partner")] SNOMED_408821002 = 1,
        [Description("Inwonende minderjarige kinderen")] SNOMED_8871000146101 = 2,
        [Description("Inwonende meerderjarige kinderen")] SNOMED_8861000146107 = 3,
        [Description("Woont samen met ouders")] SNOMED_224137008 = 4,
        [Description("Woont samen met familie")] SNOMED_160756002 = 5,
        [Description("Woont samen met vrienden")] SNOMED_224131009 = 6,
        [Description("Anders")] OTH = 7
    }

    public enum WoningType
    {
        [Description("Bovenwoning")] BOVENW = 0,
        [Description("Benedenwoning")] BENEDENW = 1,
        [Description("Eengezinswoning")] EENGEZW = 2,
        [Description("Appartement of flatwoning")] FLATW = 3,
        [Description("Aanleunwoning")] AANLW = 4,
        [Description("Woonboot")] WOONB = 5,
        [Description("Woonwagen")] WOONW = 6,
        [Description("Instelling AWBZ")] AWBZIN = 7,
        [Description("Dakloos")] DAKLOO = 8,
        [Description("Anders")] OTH = 9,
    }

    public enum CommunicatieTaal
    {
        [Description("Nederlands")] nld = 1,
        [Description("Turks")] tur = 2,
        [Description("Frans")] fra = 3,
        [Description("Duits")] ger = 4,
        [Description("Engels")] eng = 5,
        [Description("Bulgaars")] bul = 6,
        [Description("Deens")] dan = 7,
        [Description("Papiamento")] pap = 8,
        [Description("Grieks")] gre = 9,
        [Description("Chinees")] chi = 10,
        [Description("Maleis")] mei = 11,
        [Description("Javaans")] jav = 12,
        [Description("Arabisch")] ara = 13,
        [Description("Koerdisch")] kur = 14,
        [Description("Perzisch")] per = 15,
        [Description("Hebreews")] heb = 16,
        [Description("Italiaans")] ita = 17,
        [Description("Servisch")] scc = 18,
        [Description("Kroatisch")] scr = 19,
        [Description("Sloveens")] slv = 20,
        [Description("Macedonisch")] mac = 21,
        [Description("Berber")] ber = 22,
        [Description("Pools")] pol = 23,
        [Description("Portugees")] por = 24,
        [Description("Roemeens")] rum = 25,
        [Description("Spaans")] spa = 26,
        [Description("Surinaams")] srn = 27,
        [Description("Overig")] oth = 28
    }

    public enum Levensovertuiging
    {
        [Description("Geen geloof")] GeenGeloof,
        [Description("Atheïst")] SNOMED_160542002,
        [Description("Agnost")] SNOMED_160567004,
        [Description("Boeddhist")] SNOMED_309687009,
        [Description("Anglicaan")] SNOMED_271448006,
        [Description("Jehova's getuige")] SNOMED_80587008,
        [Description("Non -conformist")] SNOMED_276119007,
        [Description("Christen")] SNOMED_160549006,
        [Description("Protestant")] SNOMED_309885004,
        [Description("Katholiek")] SNOMED_160540005,
        [Description("Heilsoldaat")] SNOMED_160234004,
        [Description("Zevendedagsadventist")] SNOMED_428821008,
        [Description("Hindoe")] SNOMED_160545000,
        [Description("Jood")] SNOMED_160543007,
        [Description("Moslim")] SNOMED_309884000,
        [Description("Humanist")] Humanist,
        [Description("Mormoon")] Mormoon,
        [Description("Anders")] OTH
    }

    public enum SoortHulp
    {
        [Description("Professionele thuiszorg")] SNOMED_60689008,
        [Description("Mantelzorg")] SNOMED_386229000
    }

    public enum TaalvaardigheidBegrijpen
    {
        [Description("Uitstekend")] E,
        [Description("Goed")] G,
        [Description("Redelijk")] F,
        [Description("Slecht")] P
    }

    public enum TaalvaardigheidLezen
    {
        [Description("Uitstekend")] E,
        [Description("Goed")] G,
        [Description("Redelijk")] F,
        [Description("Slecht")] P
    }

    public enum TaalvaardigheidSpreken
    {
        [Description("Uitstekend")] E,
        [Description("Goed")] G,
        [Description("Redelijk")] F,
        [Description("Slecht")] P
    }
}
