﻿using Point.Models.LDM.EOverdracht30.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Point.Models.LDM.EOverdracht30
{
    public class AdministratieveGegevens
    {
        public DateTime? DatumOverplaatsing { get; set; }
        public Patient Patient { get; set; }
        public Betaler Zorgverzekeraar { get; set; }
        public Contactpersoon Contactpersoon { get; set; }
        public Zorgaanbieder SturendeZorgaanbieder { get; set; }
        public Zorgverlener SturendeZorgverlener { get; set; }
        public Zorgaanbieder OntvangendeZorgaanbieder { get; set; }
        public Zorgverlener OntvangendeZorgverlener { get; set; }
    }
}
