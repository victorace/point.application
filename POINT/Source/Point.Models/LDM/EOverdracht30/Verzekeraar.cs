﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Point.Models.LDM.EOverdracht30
{
    public class Verzekeraar
    {
        public List<Verzekering> Verzekering { get; set; }
        public string IdentificatieNummer { get; set; }
        public string OrganisatieNaam { get; set; }
        public string VerzekerdeNummer { get; set; }
    }
}
