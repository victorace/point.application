﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Point.Models.LDM.EOverdracht30
{
    public class Behandelrestricties
    {
        public List<BehandelAanwijzing> BehandelAanwijzing { get; set; } = new List<BehandelAanwijzing>();
        public List<Wilsverklaring> Wilsverklaring { get; set; } = new List<Wilsverklaring>();
    }
}
