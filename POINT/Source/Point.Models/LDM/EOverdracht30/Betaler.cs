﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Point.Models.LDM.EOverdracht30
{
    public class Betaler
    {
        public List<Adresgegevens> Adresgegevens { get; set; }
        public List<Contactgegevens> Contactgegevens { get; set; }
        public List<BetalerPersoon> BetalerPersoon { get; set; }
        public List<Verzekeraar> Verzekeraar { get; set; }
    }
}
