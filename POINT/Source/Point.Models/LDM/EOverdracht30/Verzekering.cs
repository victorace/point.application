﻿using Point.Models.LDM.EOverdracht30.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Point.Models.LDM.EOverdracht30
{
    public class Verzekering
    {
        public DateTime? BeginDatumTijd { get; set; }
        public DateTime? EindDatumTijd { get; set; }
        public Verzekeringsoort Verzekeringsoort { get; set; }
    }
}
