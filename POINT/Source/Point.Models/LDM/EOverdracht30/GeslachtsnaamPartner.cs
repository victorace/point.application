﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Point.Models.LDM.EOverdracht30
{
    public class GeslachtsnaamPartner
    {
        public string VoorvoegselsPartner { get; set; }
        public string AchternaamPartner { get; set; }
    }
}
