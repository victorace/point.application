﻿using Point.Models.LDM.EOverdracht30.Enums;

namespace Point.Models.LDM.EOverdracht30
{
    public class Taalvaardigheid
    {
        public string CommunicatieTaal { get; set; }
        public TaalvaardigheidBegrijpen? TaalvaardigheidBegrijpen { get; set; }
        public TaalvaardigheidSpreken? TaalvaardigheidSpreken { get; set; }
        public TaalvaardigheidLezen? TaalvaardigheidLezen { get; set; }
        public string Toelichting { get; set; }
    }
}