﻿using Point.Models.LDM.EOverdracht30.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Point.Models.LDM.EOverdracht30
{
    public class Identificatie
    {
        public string Identificatienummer { get; set; }
        public IdentificatieSoort IdentificatieSoort { get; set; }
    }
}
