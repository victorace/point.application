﻿using Point.Models.LDM.EOverdracht30.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Point.Models.LDM.EOverdracht30
{
    public class Adresgegevens
    {
        public string Straat { get; set; }
        public string Huisnummer { get; set; }
        public string Huisnummerletter { get; set; }
        public string Huisnummertoevoeging { get; set; }
        public AanduidingBijNummer? AanduidingBijNummer { get; set; }
        public string Postcode { get; set; }
        public string Woonplaats { get; set; }
        public string Gemeente { get; set; }
        public string Land { get; set; }
        public string AdditioneleInformatie { get; set; }
        public AdresSoort AdresSoort { get; set; }
    }
}
