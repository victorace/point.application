﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Point.Models.LDM.EOverdracht30
{
    public class Naamgegevens
    {
        public string Voornamen { get; set; }
        public string Initialen { get; set; }
        public string Roepnam { get; set; }
        public string Naamgebruik { get; set; }
        public Geslachtsnaam Geslachtsnaam { get; set; }
        public GeslachtsnaamPartner GeslachtsnaamPartner { get; set; }
    }
}
