﻿using Point.Models.LDM.EOverdracht30.Enums;

namespace Point.Models.LDM.EOverdracht30
{
    public class Gezinssituatie
    {
        public BurgerlijkeStaat? BurgerlijkeStaat { get; set; }
        public Gezinssamenstelling? Gezinssamenstelling { get; set; }
        public int? AantalKinderen { get; set; }
        public int? AantalKinderenInwonend { get; set; }
    }
}