﻿using Point.Models.LDM.EOverdracht30.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Point.Models.LDM.EOverdracht30
{
    public class Woonsituatie
    {
        public WoningType? WoningType { get; set; }
        public string Toelichting { get; set; }
    }
}
