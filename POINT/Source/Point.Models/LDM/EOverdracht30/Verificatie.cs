﻿using Point.Models.LDM.EOverdracht30.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Point.Models.LDM.EOverdracht30
{
    public class Verificatie
    {
        public bool Geverifieerd { get; set; }
        public List<GeverifieerdBij> GeverifieerdBij { get; set; } = new List<GeverifieerdBij>();
        public DateTime? VerificatieDatum { get; set; }
    }
}
