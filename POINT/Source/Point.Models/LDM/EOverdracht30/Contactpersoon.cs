﻿using Point.Models.LDM.EOverdracht30.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Point.Models.LDM.EOverdracht30
{
    public class Contactpersoon
    {
        public Naamgegevens Naamgegevens { get; set; }
        public Contactgegevens Contactgegevens { get; set; }
        public Adresgegevens Adresgegevens { get; set; }
        public List<Rol> Rol { get; set; } = new List<Rol>();
        public List<Relatie> Relatie { get; set; } = new List<Relatie>();
    }
}
