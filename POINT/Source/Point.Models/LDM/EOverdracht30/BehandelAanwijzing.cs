﻿using Point.Models.LDM.EOverdracht30.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Point.Models.LDM.EOverdracht30
{
    public class BehandelAanwijzing
    {
        public List<Verificatie> Verificatie { get; set; } = new List<Verificatie>();
        public Behandeling Behandeling { get; set; }
        public BehandelingToegestaan BehandelingToegestaan { get; set; }
        public string Beperkingen { get; set; }
        public DateTime? BeginDatum { get; set; }
        public DateTime? EindDatum { get; set; }
        public string Toelichting { get; set; }
    }
}
