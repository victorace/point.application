﻿using Point.Models.LDM.EOverdracht30.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Point.Models.LDM.EOverdracht30
{
    public class Wilsverklaring
    {
        public WilsverklaringType WilsverklaringType { get; set; }
        public DateTime WilsverklaringDatum { get; set; }
        public List<Probleem> Aandoening { get; set; } = new List<Probleem>();
        public Contactpersoon Vertegenwoordiger { get; set; }
        public byte[] WilsverklaringDocument { get; set; }
        public string Toelichting { get; set; }
    }
}
