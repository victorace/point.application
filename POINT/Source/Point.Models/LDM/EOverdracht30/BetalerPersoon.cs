﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Point.Models.LDM.EOverdracht30
{
    public class BetalerPersoon
    {
        public string BetalerNaam { get; set; }
        public List<Bankgegevens> Bankgegevens { get; set; } = new List<Bankgegevens>();
    }
}
