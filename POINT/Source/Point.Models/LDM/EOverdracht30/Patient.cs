﻿using Point.Models.LDM.EOverdracht30.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Point.Models.LDM.EOverdracht30
{
    public class Patient
    {
        public Naamgegevens Naamgegevens { get; set; } = new Naamgegevens(); //Required
        public List<Adresgegevens> Adresgegevens { get; set; } = new List<Adresgegevens>();
        public Contactgegevens Contactgegevens { get; set; } = new Contactgegevens();
        public List<Identificatie> Identificatienummer { get; set; } = new List<Identificatie>();
        public DateTime Geboortedatum { get; set; }
        public Geslacht Geslacht { get; set; }
        public bool? Meerling { get; set; }
        public bool? OverlijdensIndicator { get; set; }
        public DateTime? DatumOverlijden { get; set; }
        public BurgerlijkeStaat? BurgerlijkeStaat { get; set; }
    }
}
