﻿using Point.Models.LDM.EOverdracht30.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Point.Models.LDM.EOverdracht30
{
    public class Zorgverlener
    {
        public string ZorgverlenerIdentificatienummer { get; set; }
        public List<Naamgegevens> Naamgegevens { get; set; }
        public string Specialisme { get; set; }
        public Adresgegevens Adresgegevens { get; set; }
        public List<Contactgegevens> Contactgegevens { get; set; } = new List<Contactgegevens>();
        public Zorgaanbieder Zorgaanbieder { get; set; }
        public ZorgverlenersRol? ZorgverlenersRol { get; set; }
    }
}
