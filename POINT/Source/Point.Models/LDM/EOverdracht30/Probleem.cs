﻿using Point.Models.LDM.EOverdracht30.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Point.Models.LDM.EOverdracht30
{
    public class Probleem
    {
        public string ProbleemAnatomischeLocatie { get; set; }
        public ProbleemLateraliteit? ProbleemLateraliteit { get; set; }
        public ProbleemType? ProbleemType { get; set; }
        public string ProbleemNaam { get; set; }
        public DateTime? ProbleemBeginDatum { get; set; }
        public DateTime? ProbleemEindDatum { get; set; }
        public ProbleemStatus ProbleemStatus { get; set; }
        public VerificatieStatus? VerificatieStatus { get; set; }
        public string Toelichting { get; set; }
    }
}
