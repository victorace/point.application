﻿using Point.Models.LDM.EOverdracht30.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Point.Models.LDM.EOverdracht30
{
    public class EmailAdressen
    {
        public string EmailAdres { get; set; }
        public EmailSoort? EmailSoort { get; set; }
    }
}
