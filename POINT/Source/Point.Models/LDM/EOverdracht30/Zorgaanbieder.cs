﻿using Point.Models.LDM.EOverdracht30.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Point.Models.LDM.EOverdracht30
{
    public class Zorgaanbieder
    {
        public List<string> ZorgaanbiederIdentificatienummer { get; set; } = new List<string>();
        public string OrganisatieNaam { get; set; }
        public string OrganisatieLocatie { get; set; }
        public string AfdelingSpecialisme { get; set; }
        public Contactgegevens Contactgegevens { get; set; } = new Contactgegevens();
        public Adresgegevens Adresgegevens { get; set; } = new Adresgegevens();
        public OrganisatieType? OrganisatieType { get; set; }
    }
}
