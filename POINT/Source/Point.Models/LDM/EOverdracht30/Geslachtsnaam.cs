﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Point.Models.LDM.EOverdracht30
{
    public class Geslachtsnaam
    {
        public string Voorvoegsels { get; set; }
        public string Achternaam { get; set; }
    }
}
