﻿using Point.Models.LDM.EOverdracht30.Enums;

namespace Point.Models.LDM.EOverdracht30
{
    public class HulpVanAnderen
    {
        public SoortHulp SoortHulp { get; set; }
        public Hulpverlener Hulpverlener { get; set; }
    }
}