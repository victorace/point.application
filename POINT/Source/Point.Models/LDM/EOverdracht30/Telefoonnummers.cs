﻿using Point.Models.LDM.EOverdracht30.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Point.Models.LDM.EOverdracht30
{
    public class Telefoonnummers
    {
        public string Telefoonnummer { get; set; }
        public TelecomType TelecomType { get; set; }
        public NummerSoort? NummerSoort { get; set; }
    }
}
