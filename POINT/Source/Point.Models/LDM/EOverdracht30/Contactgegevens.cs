﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Point.Models.LDM.EOverdracht30
{
    public class Contactgegevens
    {
        public List<Telefoonnummers> Telefoonnummers { get; set; } = new List<Telefoonnummers>();
        public List<EmailAdressen> EmailAdressen { get; set; } = new List<EmailAdressen>();
    }
}
