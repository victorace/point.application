﻿using Point.Models.LDM.EOverdracht30.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Point.Models.LDM.EOverdracht30
{
    public class EOverdracht30
    {
        public AdministratieveGegevens Administratieve_Gegevens { get; set; } = new AdministratieveGegevens();
        public AlgemenePatientenContext AlgemenePatientenContext { get; set; } = new AlgemenePatientenContext();
        
        //public FinancieleInformatie Financiele_Informatie { get; set; } = new FinancieleInformatie(); //Required
        //public Behandelrestricties Behandelrestricties { get; set; } = new Behandelrestricties(); //Required
        //public Contactpersoon Contactpersonen { get; set; }
    }
}
