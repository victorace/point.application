﻿namespace Point.Models.LDM.EOverdracht30
{
    public class Hulpverlener
    {
        public Zorgverlener Zorgverlener { get; set; }
        public Contactpersoon Mantelzorger { get; set; }
        public Zorgaanbieder Zorgaanbieder { get; set; }
    }
}