﻿using Point.Models.LDM.EOverdracht30.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Point.Models.LDM.EOverdracht30
{
    public class AlgemenePatientenContext
    {
        public Gezinssituatie Gezinssituatie { get; set; }
        public Woonsituatie Woonomgeving { get; set; }
        public Levensovertuiging? Levensovertuiging { get; set; }
        public Wilsverklaring Wilsverklaring { get; set; }
        public ParticipatieInMaatschappij ParticipatieInMaatschappij { get; set; }
        public HulpVanAnderen HulpVanAnderen { get; set; }
        public Taalvaardigheid Taalvaardigheid { get; set; }
    }
}
