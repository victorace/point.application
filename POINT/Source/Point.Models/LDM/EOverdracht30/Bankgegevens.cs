﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Point.Models.LDM.EOverdracht30
{
    public class Bankgegevens
    {
        public string BankNaam { get; set; }
        public string Bankcode { get; set; }
        public string Rekeningnummer { get; set; }
    }
}
