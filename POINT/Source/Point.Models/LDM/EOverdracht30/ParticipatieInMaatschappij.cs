﻿namespace Point.Models.LDM.EOverdracht30
{
    public class ParticipatieInMaatschappij
    {
        public string SociaalNetwerk { get; set; }
        public string Vrijetijdsbesteding { get; set; }
        public string Arbeidssituatie { get; set; }
        public string Toelichting { get; set; }
    }
}