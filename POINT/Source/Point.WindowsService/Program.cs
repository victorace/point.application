﻿using System.ServiceProcess;

namespace Point.WindowsService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
            { 
                new PointService() 
            };
            ServiceBase.Run(ServicesToRun);
        }
    }
}
