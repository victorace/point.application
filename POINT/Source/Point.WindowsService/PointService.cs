﻿using Point.Processing;
using System.ServiceProcess;

namespace Point.WindowsService
{
    public partial class PointService : ServiceBase
    {
        public PointService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            var pointQueueProcessorList = new PointQueueProcessorList();
            pointQueueProcessorList.Start();

            var pointScheduler = new PointScheduler();
            pointScheduler.Start();
        }

        protected override void OnStop()
        {
            
        }
    }
}
