﻿namespace Point.WindowsService
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PointServiceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.PointServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // PointServiceProcessInstaller
            // 
            this.PointServiceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.NetworkService;
            this.PointServiceProcessInstaller.Password = null;
            this.PointServiceProcessInstaller.Username = null;
            // 
            // PointServiceInstaller
            // 
            this.PointServiceInstaller.DisplayName = "Point WindowsService";
            this.PointServiceInstaller.ServiceName = "Point WindowsService";
            this.PointServiceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.PointServiceProcessInstaller,
            this.PointServiceInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller PointServiceProcessInstaller;
        private System.ServiceProcess.ServiceInstaller PointServiceInstaller;
    }
}