﻿using Point.Database.Models;
using Point.HL7.Processing.Business;
using Point.HL7.Processing.Models;
using Point.HL7.Processing.ServiceContract;
using Point.Log4Net;
using System;
using System.Reflection;

namespace Point.HL7.Processing
{
    public class EPDConnector
    {
        private static readonly ILogger Logger = LogManager.GetLogger(typeof(EPDConnector));

        public void CheckEndPoints()
        {
            var endpoints = EndpointConfigurationBL.Get();
            foreach (var endpointconfiguration in endpoints)
            {
                var model = Type.GetType($"{HL7Service.EPDAssemblyModels}.{endpointconfiguration.Model}");

                try
                {
                    MethodInfo method = GetType().GetMethod(nameof(checkEndPoint), BindingFlags.NonPublic | BindingFlags.Instance);
                    MethodInfo generic = method.MakeGenericMethod(model);
                    generic.Invoke(this, new object[] { endpointconfiguration });
                }
                catch (Exception ex)
                {
                    Logger.Error($"Fatal error connecting to [{endpointconfiguration.Address}] for organizationID [{endpointconfiguration.OrganizationID}].", ex);
                }
            }
        }

        private void checkEndPoint<T>(EndpointConfiguration endpointconfiguration)
        {
            int organization = endpointconfiguration.OrganizationID;
            string endpointaddress = endpointconfiguration.Address;
            string methodname = endpointconfiguration.Method;
            string type = endpointconfiguration.FormType?.Name;

            try
            {
                if (string.IsNullOrWhiteSpace(endpointaddress) && string.IsNullOrWhiteSpace(methodname))
                {
                    return; // it's OK to skip both, if the specific type (i.e. EOverdracht, Aanvraag, ...) isn't used by the organization
                }

                if (string.IsNullOrWhiteSpace(endpointaddress) || string.IsNullOrWhiteSpace(methodname))
                {
                    Logger.Error($"One of the settings address or method is not specified for organizationID [{organization}] for {type}.");
                }
                else
                {
                    Logger.Info($"OrganizationID [{organization}]: [{endpointaddress}], [{methodname}].");

                    var epdtype = Type.GetType($"{HL7Service.EPDAssemblyMethods}.{methodname}");
                    if (epdtype == null || !typeof(IEPDEndpoint<T>).IsAssignableFrom(epdtype))
                    {
                        Logger.Error($"Incorrect method [{methodname}].");
                    }
                    else
                    {
                        Logger.Info($"Using method [{methodname}] for organizationID [{organization}].");
                    }

                    bool validendpoint = false;
                    try
                    {
                        var epdinstance = (IEPDEndpoint<T>)Activator.CreateInstance(epdtype, new object[] { endpointconfiguration });

                        // TODO: Check if dit nog steeds goed gaat met huidige implementatie van interface. 
                        // Niet bestaande en onbereikbare webadressen geven ook een 'goed' terug (??) <<<<<<
                        validendpoint = epdinstance.ValidConnection(); 
                    }
                    catch
                    {
                        validendpoint = false;
                    }

                    if (validendpoint)
                    {
                        Logger.Info($"Successfully connected to [{endpointaddress}] for organizationID [{organization}].");
                    }
                    else
                    {
                        Logger.Error($"Could not connect to [{endpointaddress}] for organizationID [{organization}].");
                    }
                }
            }
            catch(Exception ex)
            {
                Logger.Error($"Fatal error connecting to [{endpointaddress}] for organizationID [{organization}].", ex);
            }
        }
    }
}