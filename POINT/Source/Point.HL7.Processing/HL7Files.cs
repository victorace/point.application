﻿using Point.HL7.Processing.Business;
using Point.HL7.Processing.Logic;
using Point.Log4Net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Threading;

namespace Point.HL7.Processing
{
    public class HL7Files
    {
        private static readonly ILogger Logger = LogManager.GetLogger(typeof(HL7Files));

        public const string DirQuery = "QUERY";
        public const string DirReceived = "RECEIVED";
        public const string DirError = "ERROR";
        public const string DirDone = "DONE";
        public const string DirSend = "SEND";

        private static Dictionary<string, FileSystemWatcher> fileSystemWatchers;
        
        public static void CheckDirectories()
        {
            try
            {
                var basedir = ConfigurationManager.AppSettings["IguanaBaseDir"];

                string[] neededdirs = { DirQuery, DirReceived, DirError, DirDone, DirSend };

                foreach (var interfacecode in OrganizationSettingBL.GetAllInterfaceCodes())
                {
                    foreach (string neededdir in neededdirs)
                    {
                        string directory = Path.Combine(basedir, interfacecode, neededdir);
                        if (!Directory.Exists(directory))
                        {
                            Logger.Info("Created directory: " + directory);
                            Directory.CreateDirectory(directory);
                        }
                        foreach (FileInfo fileinfo in new DirectoryInfo(directory).GetFiles())
                        {
                            fileinfo.Delete();
                        }
                    }
                    Logger.Info($"All HL7-directories available for interfacecode [{interfacecode}]");
                }
            }
            catch (Exception exc)
            {
                Logger.Error(exc);
            }
            
        }

        public static void InitWatchers(MessageHandler messageHandler)
        {
            try
            {
                if (fileSystemWatchers != null)
                {
                    foreach (var filesystemwatcher in fileSystemWatchers)
                    {
                        filesystemwatcher.Value.EnableRaisingEvents = false;
                        filesystemwatcher.Value.Dispose();
                    }
                }
                fileSystemWatchers = new Dictionary<string, FileSystemWatcher>();


                var basedir = ConfigurationManager.AppSettings["IguanaBaseDir"];

                foreach (var interfacecode in OrganizationSettingBL.GetAllInterfaceCodes())
                {
                    string directory = Path.Combine(basedir, interfacecode, DirReceived);

                    Logger.Info("Started watching: " + directory);
                    FileSystemWatcher watcher = new FileSystemWatcher
                    {
                        Path = directory,
                        NotifyFilter = NotifyFilters.LastWrite | NotifyFilters.FileName,
                        Filter = "*.txt",
                        EnableRaisingEvents = true

                    };
                    watcher.Created += (sender, args) =>
                    {
                        Logger.Debug($"Watcher found created file {args.FullPath}");
                        handleFileFromWatcher(messageHandler, args.FullPath);
                    };
                    watcher.Renamed += (sender, args) =>
                    {
                        Logger.Debug($"Watcher found created file {args.FullPath}");
                        handleFileFromWatcher(messageHandler, args.FullPath);
                    };

                    fileSystemWatchers.Add(directory, watcher);
                }
            }
            catch (Exception exc)
            {
                Logger.Error(exc);
            }
        }

        private static void handleFileFromWatcher(MessageHandler messageHandler, string fullPath)
        {
            try
            {
                bool processed = false;
                if (getIdleFile(fullPath))
                {
                    HL7Processor hl7processor = new HL7Processor()
                    {
                        FileContent = File.ReadAllText(fullPath),
                        InterfaceCode = InterfaceCodeHelper.GetInterfaceCodeFromPath(fullPath, DirReceived)
                    };

                    processed = hl7processor.ProcessContents();
                    if (processed)
                    {
                        handleProcessedContent(messageHandler, hl7processor);

                        Logger.Debug($"Succesfully processed {Path.GetFileName(fullPath)}, PatientNumber: {hl7processor.HL7Client.PatientNumber}, CivilServiceNumber: {hl7processor.HL7Client.CivilServiceNumber}. MessageType: {hl7processor.HL7HeaderInfo.MessageType}");
                        Logger.Debug(hl7processor.HL7Client.ToStringEx());
                        Logger.Debug(hl7processor.HL7RequestForm.ToStringEx());
                    }
                }
                handleProcessedFile(fullPath, processed);
            }
            catch(Exception ex)
            {
                Logger.Error(ex);
            }
        }

        public static bool CreateQuery(int organizationID, int? locationID, string patientNumber)
        {
            try
            {
                HL7Creator hl7creator = new HL7Creator();
                if (hl7creator.CreateQryA19(organizationID, patientNumber))
                {
                    string interfacecode = getInterfaceCode(organizationID, locationID);
                    if (String.IsNullOrEmpty(interfacecode))
                    {
                        return false;
                    }
                    var filename = Path.Combine(ConfigurationManager.AppSettings["IguanaBaseDir"], interfacecode, DirQuery, DateTime.Now.ToString("yyyyMMdd-HHmmss") + ".txt");
                    File.WriteAllText(filename, hl7creator.MessageContent);
                    Logger.Debug($"Succesfully created Query-file {filename}");
                    return true;
                }
                return false;
            }
            catch(Exception ex)
            {
                Logger.Error(ex);
            }
            return false;
        }

        public static bool CreateORU(int organizationID, int? locationID, int transferID, string contents)
        {
            try
            {
                HL7Creator hl7creator = new HL7Creator();
                if (hl7creator.CreateORU(organizationID, transferID, contents))
                {
                    string interfacecode = getInterfaceCode(organizationID, locationID);
                    if (String.IsNullOrEmpty(interfacecode))
                    {
                        return false;
                    }
                    var filename = Path.Combine(ConfigurationManager.AppSettings["IguanaBaseDir"], interfacecode, DirSend, DateTime.Now.ToString("yyyyMMdd-HHmmss") + ".txt");
                    File.WriteAllText(filename, hl7creator.MessageContent);
                    Logger.Debug($"Succesfully created ORU-file {filename}");
                    return true;
                }
                return false;
            }
            catch(Exception ex)
            {
                Logger.Error(ex);
            }
            return false;
        }


        private static void handleProcessedContent(MessageHandler messageHandler, HL7Processor hl7processor)
        {
            try
            {
                if (hl7processor.HL7HeaderInfo.TriggerEvent == HL7MessageHelper.A19)
                {
                    messageHandler.Add(hl7processor.HL7Client.OrganizationID, hl7processor.HL7Client.LocationID, hl7processor.HL7Client.PatientNumber, hl7processor.HL7Client);

                    var flowfieldstoskip = OrganizationSettingBL.GetA19FlowFieldsToSkipByOrganizationID(hl7processor.HL7Client.OrganizationID);

                    FieldReceivedTempBL.SaveReceivedTemp(hl7processor.HL7HeaderInfo, hl7processor.HL7Client, hl7processor.HL7RequestForm, flowfieldstoskip);
                }
                else if (hl7processor.HL7HeaderInfo.TriggerEvent == HL7MessageHelper.A08)
                {
                    var cachea08 = new CacheA08();
                    if (!cachea08.HL7ClientWasSendBefore(hl7processor.InterfaceCode, hl7processor.HL7Client))
                    {
                        var clientpropertiesnotupdated = OrganizationSettingBL.GetA08ClientPropertiesNotUpdatedByOrganizationID(hl7processor.HL7Client.OrganizationID);
                        var clientpropertiestoskip = OrganizationSettingBL.GetA08ClientPropertiesToSkipByOrganizationID(hl7processor.HL7Client.OrganizationID);

                        ClientBL.UpdateClient(hl7processor.HL7Client, clientpropertiesnotupdated, clientpropertiestoskip);
                    }
                    if (!cachea08.HL7RequestFormWasSendBefore(hl7processor.InterfaceCode, hl7processor.HL7Client, hl7processor.HL7RequestForm))
                    {
                        var flowfieldsnotupdated = OrganizationSettingBL.GetA08FlowFieldsNotUpdatedByOrganizationID(hl7processor.HL7Client.OrganizationID);
                        var flowfieldstoskip = OrganizationSettingBL.GetA08FlowFieldsToSkipByOrganizationID(hl7processor.HL7Client.OrganizationID);

                        RequestFormBL.HandleHL7RequestForm(hl7processor.HL7HeaderInfo, hl7processor.HL7Client, hl7processor.HL7RequestForm, flowfieldsnotupdated, flowfieldstoskip);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
        }

        private static void handleProcessedFile(string filename, bool processed)
        {
            try
            {
                if (processed)
                {
                    File.Delete(filename);
                }
                else
                {
                    var newfilename = filename.Replace($"{Path.DirectorySeparatorChar}{DirReceived}{Path.DirectorySeparatorChar}", $"{Path.DirectorySeparatorChar}{DirError}{Path.DirectorySeparatorChar}");
                    File.Move(filename, newfilename);
                }
            }
            catch(Exception ex)
            {
                Logger.Error(ex);
            }
        }


        private static string getInterfaceCode(int organizationID, int? locationID)
        {
            try
            {
                string interfacecode = "";
                if (locationID.HasValue)
                {
                    interfacecode = OrganizationSettingBL.GetInterfaceCodeByLocationID(locationID.Value);
                }
                if (string.IsNullOrEmpty(interfacecode))
                {
                    interfacecode = OrganizationSettingBL.GetInterfaceCodeByOrganizationID(organizationID);
                }
                if (string.IsNullOrEmpty(interfacecode))
                {
                    Logger.Error($"No interfacecode specified for organizationID {organizationID} / locationID {locationID}");
                    return null;
                }
                return interfacecode;
            }
            catch (Exception exc)
            {
                Logger.Error(exc);
            }

            return "";
        }

        private static bool getIdleFile(string filename)
        {
            bool fileIdle = false;
            int maxAttempts = 10;
            int attempts = 0;

            while (!fileIdle && attempts < maxAttempts)
            {
                try
                {
                    using (File.Open(filename, FileMode.Open, FileAccess.Read))
                    {
                        fileIdle = true;
                    }
                }
                catch
                {
                    attempts++;
                    Thread.Sleep(50);
                }
            }
            return fileIdle;
        }
    }
}