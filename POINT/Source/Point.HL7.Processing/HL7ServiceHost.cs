﻿using Point.HL7.Processing.Logic;
using Point.HL7.Processing.ServiceContract;
using Point.Log4Net;
using System;
using System.Configuration;
using System.ServiceModel;
using System.ServiceModel.Description;

namespace Point.HL7.Processing
{
    public class HL7ServiceHost
    {
        private static readonly ILogger Logger = LogManager.GetLogger(typeof(HL7ServiceHost));

        public Uri BaseUri { get; private set; }
        public bool ServiceDebug { get; private set; }

        public void Start()
        {
            BaseUri = new Uri(ConfigurationManager.AppSettings["BaseUri"]);
            ServiceHost serviceHost = new ServiceHost(typeof(HL7Service), BaseUri); 

            serviceHost.AddServiceEndpoint(typeof(IHL7Service), new BasicHttpBinding(), "HL7Service");

            serviceHost.Description.Behaviors.Add(new ServiceMetadataBehavior()
            {
                HttpGetEnabled = true,
                HttpsGetEnabled = false,
            });

            if (ServiceDebug)
            {
                ServiceDebugBehavior serviceDebugBehaviour = serviceHost.Description.Behaviors.Find<ServiceDebugBehavior>();
                if (serviceDebugBehaviour == null)
                {
                    serviceHost.Description.Behaviors.Add(new ServiceDebugBehavior() { IncludeExceptionDetailInFaults = true });
                }
                else
                {
                    serviceDebugBehaviour.IncludeExceptionDetailInFaults = true;
                }

                Logger.Info("Enabled 'IncludeExceptionDetailInFaults' for the serviceHost.");
            }

            serviceHost.Open();

            Logger.Info($"Started listening at: {BaseUri}");

            HL7Files.CheckDirectories();
            HL7Files.InitWatchers(HL7Service.MessageHandler);

            var epdconnector = new EPDConnector();
            epdconnector.CheckEndPoints();
        }
    }
}