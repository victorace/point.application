﻿using Point.Database.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Point.HL7.Processing.Context
{
    public partial class HL7Context : DbContext
    {
        public HL7Context() : base("name=PointContext")
        {
            Configuration.AutoDetectChangesEnabled = false;
            Configuration.ProxyCreationEnabled = false;
        }

        public virtual DbSet<Client> Client { get; set; }

        public virtual DbSet<Department> Department { get; set; }
        public virtual DbSet<Employee> Employee { get; set; }
        public virtual DbSet<EndpointConfiguration> EndpointConfigurations { get; set; }
        public virtual DbSet<FieldReceivedTemp> FieldReceivedTemp { get; set; }
        public virtual DbSet<FlowInstance> FlowInstance { get; set; }
        public virtual DbSet<FlowInstanceSearchValues> FlowInstanceSearchValues { get; set; }
        public virtual DbSet<FlowFieldAttribute> FlowFieldAttribute { get; set; }
        public virtual DbSet<FlowFieldValue> FlowFieldValue { get; set; }
        public virtual DbSet<FormType> FormType { get; set; }
        public virtual DbSet<FormSetVersion> FormSetVersion { get; set; }
        public virtual DbSet<HealthInsurer> HealthInsurer { get; set; }
        public virtual DbSet<Location> Location { get; set; }
        public virtual DbSet<MessageReceivedTemp> MessageReceivedTemp { get; set; }
        public virtual DbSet<MutFlowFieldValue> MutFlowFieldValue { get; set; }
        public virtual DbSet<Organization> Organization { get; set; }
        public virtual DbSet<OrganizationSetting> OrganizationSetting { get; set; }
        public virtual DbSet<FieldReceivedValueMapping> FieldReceivedValueMapping { get; set; }
        public virtual DbSet<Transfer> Transfer { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            new Database.Context.PointContext().ModelCreatedFromDatabase(modelBuilder);
            new Database.Context.PointContext().ModelCreatedOverrideRequired(modelBuilder);
        }

    }
}
