﻿namespace Point.HL7.Processing.Models
{
    public partial class FieldReceivedValueMapping
    {
        public int FieldReceivedValueMappingID { get; set; }

        public string FieldName { get; set; }

        public string SourceValue { get; set; }

        public string TargetValue { get; set; }
    }
}
