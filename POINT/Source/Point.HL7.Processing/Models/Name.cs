﻿namespace Point.HL7.Processing.Models
{
    public class Name
    {
        public string Initials { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string FullName()
        {
            return $"{Initials} {FirstName} {LastName}".Trim();
        }
    }
}
