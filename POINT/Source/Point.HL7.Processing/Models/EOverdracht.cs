﻿using System;

namespace Point.HL7.Processing.Models
{
    public class EOverdracht
    {
        public string PatientNumber { get; set; }
        public string VisitNumber { get; set; }
        public string CivilServiceNumber { get; set; }
        public DateTime? BirthDate { get; set; }
        public string Gender { get; set; }
        //ClientID ???
        //TransferID ???
        public string ResultString { get; set; }
        public DateTime? TimeStamp { get; set; }

        public string AanspreekpersoonVoorClient { get; set; }
        public string AfsprakenClient { get; set; }
        public Dropdown AlgemeneMentaleFunctieBewustzijn { get; set; } = new Dropdown();
        public Dropdown AlgemeneMentaleFunctieIntellectueleFuncties { get; set; } = new Dropdown();
        public Dropdown AlgemeneMentaleFunctieOrientatiePersoon { get; set; } = new Dropdown();
        public Dropdown AlgemeneMentaleFunctieOrientatiePlaats { get; set; } = new Dropdown();
        public Dropdown AlgemeneMentaleFunctieOrientatieTijd { get; set; } = new Dropdown();
        public string AlgemeneMentaleFunctieToelichting { get; set; }
        public string AlgemeneMentaleFunctieVerpleegkundigeInterventies { get; set; }
        public string AllergieBronBewering { get; set; }
        public string AllergieSoortEnReactie { get; set; }
        public string AndereRelevanteMedischeAandoening { get; set; }
        public DateTime? BeginInZorgBehandeling { get; set; }
        public string BeginKlachten { get; set; }
        public string BehandelaarNaam { get; set; }
        public Radio BesprokenEuthenasie { get; set; } = new Radio();
        public string BesprokenEuthenasieToelichting { get; set; }
        public Radio BesprokenPalliatieveSedatie { get; set; } = new Radio();
        public string BesprokenPalliatieveSedatieToelichting { get; set; }
        public Radio BesprokenReanimatiebeleid { get; set; } = new Radio();
        public string BesprokenReanimatiebeleidToelichting { get; set; }
        public Radio BesprokenStoppenParenteraleVochttoediening { get; set; } = new Radio();
        public string BesprokenStoppenParenteraleVochttoedieningToelichting { get; set; }
        public Radio BesprokenStoppenSondeParenteraleVoeding { get; set; } = new Radio();
        public string BesprokenStoppenSondeParenteraleVoedingToelichting { get; set; }
        public Radio BesprokenToedieningAntibiotica { get; set; } = new Radio();
        public string BesprokenToedieningAntibioticaToelichting { get; set; }
        public Radio BesprokenToedieningBloedtransfusie { get; set; } = new Radio();
        public string BesprokenToedieningBloedtransfusieToelichting { get; set; }
        public Radio BesprokenUitvoerenDiagnostischOnderzoek { get; set; } = new Radio();
        public string BesprokenUitvoerenDiagnostischOnderzoekToelichting { get; set; }
        public Radio BesprokenUitzettenICDOfPacemaker { get; set; } = new Radio();
        public string BesprokenUitzettenICDOfPacemakerToelichting { get; set; }
        public Radio BesprokenWensZHopnameVsNietOpnemenZHOfSEH { get; set; } = new Radio();
        public string BesprokenWensZHopnameVsNietOpnemenZHOfSEHToelichting { get; set; }
        public string BloeddrukToelichting { get; set; }
        public Dropdown BurgerlijkeStaat { get; set; } = new Dropdown();
        public string CIZIndicatie { get; set; }
        public Dropdown CommunicatieBegrijpenNonVerbale { get; set; } = new Dropdown();
        public Dropdown CommunicatieBegrijpenVerbale { get; set; } = new Dropdown();
        public string CommunicatieSpreektNederlandse { get; set; }
        public string CommunicatieToelichting { get; set; }
        public string CommunicatieVerpleegkundigeInterventies { get; set; }
        public Dropdown CommunicatieZichKunnenUitenNonVerbale { get; set; } = new Dropdown();
        public Dropdown CommunicatieZichKunnenUitenVerbale { get; set; } = new Dropdown();
        public string Contactpersoonemail { get; set; }
        public string Contactpersoonnaam { get; set; }
        public string Contactpersoontelefoonnummer { get; set; }
        public Dropdown Contactpersoontype { get; set; } = new Dropdown();
        public DateTime? DatumBloeddruk { get; set; }
        public DateTime? DatumFrequentie { get; set; }
        public DateTime? DatumGewicht { get; set; }
        public DateTime? DatumLengte { get; set; }
        public DateTime? DatumMeting { get; set; }
        public DateTime? DatumTemperatuur { get; set; }
        public DateTime? DatumZuurstofgebruik { get; set; }
        public string Diastole { get; set; }
        public string DisciplineMedeBehandelaars { get; set; }
        public string Email { get; set; }
        public Dropdown EtenDrinkenDrinkenBereiden { get; set; } = new Dropdown();
        public Dropdown EtenDrinkenDrinkenNaarMondBrengen { get; set; } = new Dropdown();
        public Dropdown EtenDrinkenDrinkenVastpakken { get; set; } = new Dropdown();
        public Dropdown EtenDrinkenEatenEetgereiHanteren { get; set; } = new Dropdown();
        public Dropdown EtenDrinkenEatenNaarMondBrengen { get; set; } = new Dropdown();
        public Dropdown EtenDrinkenEatenSnijdenOpenen { get; set; } = new Dropdown();
        public string EtenDrinkenVerpleegkundigeInterventies { get; set; }
        public DateTime? Geboortedatum { get; set; }
        public Radio Geslacht { get; set; } = new Radio();
        public string GeslachtsNaam { get; set; }
        public string GeslachtsNaamPartner { get; set; }
        public string Gewicht { get; set; }
        public Radio HuidDecubitusAanwezig { get; set; } = new Radio();
        public string HuidDecubitusBehandelplan { get; set; }
        public Dropdown HuidDecubitusCategorie { get; set; } = new Dropdown();
        public string HuidDecubitusLocatie { get; set; }
        public string HuidDecubitusPreventie { get; set; }
        public Radio HuidHuidletselAanwezig { get; set; } = new Radio();
        public string HuidHuidletselBehandelplan { get; set; }
        public string HuidHuidletselFaseBeschrijving { get; set; }
        public string HuidHuidletselLocatie { get; set; }
        public string HuidHuidletselPreventie { get; set; }
        public Radio HuidOverigeAanwezig { get; set; } = new Radio();
        public string HuidOverigeBehandelplan { get; set; }
        public string HuidOverigeLocatie { get; set; }
        public string HuidOverigeSoort { get; set; }
        public Radio HuidWondAanwezig { get; set; } = new Radio();
        public string HuidWondBehandeling { get; set; }
        public string HuidWondLocatie { get; set; }
        public Dropdown HuidWondSoort { get; set; } = new Dropdown();
        public string Huisnummer { get; set; }
        public string Huisnummertoevoeging { get; set; }
        public string HulpVanAnderenMantelzorgAard { get; set; }
        public string HulpVanAnderenMantelzorgFrequentie { get; set; }
        public string HulpVanAnderenProfessioneleAard { get; set; }
        public string HulpVanAnderenProfessioneleFrequentie { get; set; }
        public string HulpVanAnderenToelichting { get; set; }
        public Radio InfectierisicoAanwezig { get; set; } = new Radio();
        public string InfectierisicoAanwezigToelichting { get; set; }
        public string KindAlgemeen { get; set; }
        public string KindBelevingGevolgenZorgsituatie { get; set; }
        public string KindBijzonderheden { get; set; }
        public int? KinderenAantalKinderenInwonend { get; set; }
        public Radio KinderenAanwezig { get; set; } = new Radio();
        public int? KinderenAanwezigAantal { get; set; }
        public string KinderenLeeftijd { get; set; }
        public string KindGedrag { get; set; }
        public string KindHobbySpelInteresse { get; set; }
        public string KindOntwikkeling { get; set; }
        public string KindRolMantelzorgFamilieAnderen { get; set; }
        public string KindSpecifiekeHandelingen { get; set; }
        public string Lengte { get; set; }
        public string LevensOvertuigingCultuur { get; set; }
        public Radio LevensOvertuigingGeregistreerdAlsDonor { get; set; } = new Radio();
        public string LevensOvertuigingReligie { get; set; }
        public string LevensOvertuigingVerpleegkundigeInterventies { get; set; }
        public Radio LevensOvertuigingWilsverklaringAanwezig { get; set; } = new Radio();
        public string LevensOvertuigingWilsverklaringWaarTeVinden { get; set; }
        public Radio LevensVerwachting { get; set; } = new Radio();
        public string Lichaamstemperatuur { get; set; }
        public Radio MedicatieGebruik { get; set; } = new Radio();
        public string MedicatieGebruikToediening { get; set; }
        public string MedischeSituatieRedenOpname { get; set; }
        public Radio MedischeZorgNaOntslag { get; set; } = new Radio();
        public string MeningClientOverplaatsing { get; set; }
        public Dropdown MobiliteitHoudingHandhavenLiggen { get; set; } = new Dropdown();
        public Dropdown MobiliteitHoudingHandhavenStaan { get; set; } = new Dropdown();
        public Dropdown MobiliteitHoudingHandhavenZitten { get; set; } = new Dropdown();
        public Dropdown MobiliteitHoudingVeranderenLiggen { get; set; } = new Dropdown();
        public Dropdown MobiliteitHoudingVeranderenStaan { get; set; } = new Dropdown();
        public Dropdown MobiliteitHoudingVeranderenZitten { get; set; } = new Dropdown();
        public Dropdown MobiliteitTransfersInLig { get; set; } = new Dropdown();
        public Dropdown MobiliteitTransfersInZit { get; set; } = new Dropdown();
        public Radio MobiliteitValrisicoAanwezig { get; set; } = new Radio();
        public string MobiliteitValrisicoToelichting { get; set; }
        public string MobiliteitVerpleegkundigeInterventies { get; set; }
        public string MobiliteitVoortbewegenHulpmiddelen { get; set; }
        public Dropdown MobiliteitVoortbewegenLopen { get; set; } = new Dropdown();
        public Radio MondVerzorgingOrthotheseAanwezig { get; set; } = new Radio();
        public Radio MondVerzorgingProtheseAanwezig { get; set; } = new Radio();
        public string MondVerzorgingSoortOndersteuningTandverzorging { get; set; }
        public string MondVerzorgingVerpleegkundigeInterventies { get; set; }
        public IDText NaamInstelling { get; set; } = new IDText();
        public Dropdown NaamVerzekeringmaatschappij { get; set; } = new Dropdown();
        public IDText OntvangendeContactpersoon { get; set; } = new IDText();
        public DateTime? OntvangendeDatumOverplaatsing { get; set; }
        public IDText OntvangendeLocatie { get; set; } = new IDText();
        public IDText OntvangendeNaam { get; set; } = new IDText();
        public Dropdown OntvangendeSoortOrganisatie { get; set; } = new Dropdown();
        public string OpZiekteGerichteBehandelingenAfspraken { get; set; }
        public Radio OpZiekteGerichteBehandelingenMogelijk { get; set; } = new Radio();
        public IDText OrganisatieAfdeling { get; set; } = new IDText();
        public string OrganisatieEmail { get; set; }
        public IDText OrganisatieLocatie { get; set; } = new IDText();
        public Dropdown OrganisatieNaamContactpersoon { get; set; } = new Dropdown();
        public Dropdown OrganisatieOpstellerOverdracht { get; set; } = new Dropdown();
        public string OrganisatieTelefoonnummer { get; set; }
        public string OverigAanvraagHulpmiddelen { get; set; }
        public string OverigEHealthVoorzieningen { get; set; }
        public string OverigeMeetwaarden { get; set; }
        public string OverigMeegegevenDocumentatie { get; set; }
        public string ParticipatieInMaatschappijParticipatieHobby { get; set; }
        public string ParticipatieInMaatschappijParticipatieSociaalNetwerk { get; set; }
        public string ParticipatieInMaatschappijParticipatieToelichting { get; set; }
        public string ParticipatieInMaatschappijParticipatieWerkOfBeroep { get; set; }
        public string ParticipatieInMaatschappijVerpleegkundigeInterventies { get; set; }
        public string PatientIdentificatie { get; set; }
        public Dropdown PijnBijBeweging { get; set; } = new Dropdown();
        public Dropdown PijnInRust { get; set; } = new Dropdown();
        public string PijnLocatie { get; set; }
        public Dropdown PijnMateVan { get; set; } = new Dropdown();
        public string PijnToelichting { get; set; }
        public string PijnVerpleegkundigeInterventies { get; set; }
        public string Plaatsnaam { get; set; }
        public string Polisnummer { get; set; }
        public string PolsFrequentie { get; set; }
        public string PolsToelichting { get; set; }
        public string Postbus { get; set; }
        public string Postcode { get; set; }
        public string Prognose { get; set; }
        public DateTime? PsychischFunctionerenDwangEnDrangAanvangEpisode { get; set; }
        public DateTime? PsychischFunctionerenDwangEnDrangEindeEpisode { get; set; }
        public Dropdown PsychischFunctionerenDwangEnDrangInstemming { get; set; } = new Dropdown();
        public Dropdown PsychischFunctionerenDwangEnDrangInterventie { get; set; } = new Dropdown();
        public Dropdown PsychischFunctionerenJuridischeStatus { get; set; } = new Dropdown();
        public string PsychischFunctionerenMiddelengebruik { get; set; }
        public string PsychischFunctionerenVerpleegkundigeInterventies { get; set; }
        public Radio PsychischFunctionerenVrijheidsbeperkendeInterventiesAanwezig { get; set; } = new Radio();
        public Radio PsychischFunctionerenVrijheidsbeperkendeInterventiesBesproken { get; set; } = new Radio();
        public string PsychischFunctionerenVrijheidsbeperkendeInterventiesSoort { get; set; }
        public Radio PsychischFunctionerenWilsbekwaamSprakeVan { get; set; } = new Radio();
        public string PsychischFunctionerenWilsbekwaamWilsonbekwaam { get; set; }
        public string RedenInZorg { get; set; }
        public string RedenOverdragenZorg { get; set; }
        public string SeksualiteitToelichting { get; set; }
        public string SeksualiteitVerpleegkundigeInterventies { get; set; }
        public string SeksualiteitVoortplanting { get; set; }
        public string SeksualiteitZwangerschapAantalWeken { get; set; }
        public string SeksualiteitZwangerschapToelichting { get; set; }
        public Radio SeksualiteitZwangerschapZwanger { get; set; } = new Radio();
        public Dropdown SlaapDoorslapen { get; set; } = new Dropdown();
        public Dropdown SlaapInslapen { get; set; } = new Dropdown();
        public Dropdown SlaapSlaapcyclus { get; set; } = new Dropdown();
        public string SlaapToelichting { get; set; }
        public string SlaapVerpleegkundigeInterventies { get; set; }
        public string SlechtNieuwsGesprekVerslag { get; set; }
        public Dropdown SoortOrganisatie { get; set; } = new Dropdown();
        public string SoortWoning { get; set; }
        public IDText SourceOrganization { get; set; } = new IDText();
        public Dropdown SpecifiekeMentaleFunctieAandacht { get; set; } = new Dropdown();
        public Dropdown SpecifiekeMentaleFunctieDenkenCoherentie { get; set; } = new Dropdown();
        public Dropdown SpecifiekeMentaleFunctieDenkenControle { get; set; } = new Dropdown();
        public Dropdown SpecifiekeMentaleFunctieDenkenInhoud { get; set; } = new Dropdown();
        public Dropdown SpecifiekeMentaleFunctieDenkenTempo { get; set; } = new Dropdown();
        public Dropdown SpecifiekeMentaleFunctieGeheugenKorteTermijn { get; set; } = new Dropdown();
        public Dropdown SpecifiekeMentaleFunctieGeheugenLangeTermijn { get; set; } = new Dropdown();
        public Dropdown SpecifiekeMentaleFunctieStemmingAdequaatheid { get; set; } = new Dropdown();
        public Dropdown SpecifiekeMentaleFunctieStemmingRegulering { get; set; } = new Dropdown();
        public string Straat { get; set; }
        public string Systole { get; set; }
        public string Telefoonnummer { get; set; }
        public string TemperatuurToelichting { get; set; }
        public DateTime? ToedieningDatumPlaatsingSonde { get; set; }
        public DateTime? ToedieningInfuusDatumPlaatsing { get; set; }
        public string ToedieningInfuusLocatie { get; set; }
        public Dropdown ToedieningInfuusSoort { get; set; } = new Dropdown();
        public string ToedieningInfuusVloeistof { get; set; }
        public string ToedieningInfuusVloeistofDosering { get; set; }
        public string ToedieningInfuusVloeistofHoeveelheid { get; set; }
        public string ToedieningSoortSonde { get; set; }
        public string ToedieningVerpleegkundigeInterventies { get; set; }
        public string ToedieningVloeistof { get; set; }
        public string ToelichtingBeloop { get; set; }
        public Dropdown ToiletgangDefecatie { get; set; } = new Dropdown();
        public Dropdown ToiletgangMenstruatie { get; set; } = new Dropdown();
        public Dropdown ToiletgangMictie { get; set; } = new Dropdown();
        public string ToiletgangVerpleegkundigeInterventies { get; set; }
        public Dropdown UitkledenBovenlichaamBeperking { get; set; } = new Dropdown();
        public Dropdown UitkledenOnderlichaamBeperking { get; set; } = new Dropdown();
        public string UitkledenVerpleegkundigeInterventies { get; set; }
        public DateTime? UitscheidingDatumLaatsteMenstruatie { get; set; }
        public Dropdown UitscheidingDefecatieContinentie { get; set; } = new Dropdown();
        public DateTime? UitscheidingDefecatieDatumLaatste { get; set; }
        public string UitscheidingDefecatieFrequentie { get; set; }
        public string UitscheidingIncontinentiemateriaal { get; set; }
        public Dropdown UitscheidingMictieContinentie { get; set; } = new Dropdown();
        public string UitscheidingMictieContinentieSoort { get; set; }
        public string UitscheidingStomaPlaats { get; set; }
        public DateTime? UitscheidingStomaSinds { get; set; }
        public string UitscheidingStomaSoort { get; set; }
        public string UitscheidingStomaToelichting { get; set; }
        public string UitscheidingToelichting { get; set; }
        public Dropdown UitscheidingUrineKatheterSoort { get; set; } = new Dropdown();
        public string UitscheidingUrineKatheterToelichting { get; set; }
        public DateTime? UitscheidingUrostomaSinds { get; set; }
        public string UitscheidingUrostomaToelichting { get; set; }
        public string UitscheidingVerpleegkundigeInterventies { get; set; }
        public Hidden ValuesRetrievedFromWebservice { get; set; } = new Hidden();
        public Radio VerwachtingAnders { get; set; } = new Radio();
        public string VerwachtingAndersToelichting { get; set; }
        public Radio VerwachtingAscites { get; set; } = new Radio();
        public string VerwachtingAscitesToelichting { get; set; }
        public Radio VerwachtingBenauwdheid { get; set; } = new Radio();
        public string VerwachtingBenauwdheidToelichting { get; set; }
        public Radio VerwachtingBloeding { get; set; } = new Radio();
        public string VerwachtingBloedingToelichting { get; set; }
        public Radio VerwachtingInsulten { get; set; } = new Radio();
        public string VerwachtingInsultenToelichting { get; set; }
        public Radio VerwachtingKoorts { get; set; } = new Radio();
        public string VerwachtingKoortsToelichting { get; set; }
        public string VerwachtingLaagHBTrombo { get; set; }
        public Radio VerwachtingMisselijkheid { get; set; } = new Radio();
        public string VerwachtingMisselijkheidToelichting { get; set; }
        public Radio VerwachtingObstipatie { get; set; } = new Radio();
        public string VerwachtingObstipatieToelichting { get; set; }
        public Radio VerwachtingPleuravocht { get; set; } = new Radio();
        public string VerwachtingPleuravochtToelichting { get; set; }
        public string VerwachtingToenameOverig { get; set; }
        public string VoedingConsistentie { get; set; }
        public Radio VoedingDieetAanwezig { get; set; } = new Radio();
        public string VoedingDieetSoort { get; set; }
        public Radio VoedingOndervoedingAanwezig { get; set; } = new Radio();
        public Radio VoedingOndervoedingAanwezigMeetmethode { get; set; } = new Radio();
        public Dropdown VoedingOndervoedingAanwezigScore { get; set; } = new Dropdown();
        public Radio VoedingSprake { get; set; } = new Radio();
        public string VoedingSprakeSoort { get; set; }
        public string VoedingVerpleegkundigeInterventies { get; set; }
        public string VOFormulierEmployeeName { get; set; }
        public string VOFormulierHandedTo { get; set; }
        public string VOFormulierHandedToBijlagen { get; set; }
        public DateTime? VOFormulierHandedToDateTime { get; set; }
        public Dropdown VOFormulierHandedToSoortRelatieClient { get; set; } = new Dropdown();
        public Hidden VOFormulierTransferAttachmentID { get; set; } = new Hidden();
        public string VOFormulierTransferFileName { get; set; }
        public Radio VOFormulierType { get; set; } = new Radio();
        public DateTime? VOFormulierUploadDate { get; set; }
        public string VoorkeursPlaatsSterven { get; set; }
        public string Voorletters { get; set; }
        public string Voornamen { get; set; }
        public Dropdown WassenBovenlichaamBeperking { get; set; } = new Dropdown();
        public Dropdown WassenGezichtBeperking { get; set; } = new Dropdown();
        public Dropdown WassenHaarverzorging { get; set; } = new Dropdown();
        public Dropdown WassenOnderlichaamBeperking { get; set; } = new Dropdown();
        public string WassenVerpleegkundigeInterventies { get; set; }
        public string WelkeAanpassingenNodig { get; set; }
        public Radio WensenPatientNaastenBehandelingEnZiekte { get; set; } = new Radio();
        public string WensenPatientNaastenBehandelingEnZiekteAfspraken { get; set; }
        public string WettelijkeVertegenwoordigerEmail { get; set; }
        public string WettelijkeVertegenwoordigerNaam { get; set; }
        public Dropdown WettelijkeVertegenwoordigerSoortRelatieClient { get; set; } = new Dropdown();
        public string WettelijkeVertegenwoordigerTelefoonnummer { get; set; }
        public string ZiekteBelevingOmgaanMetZiekteprocesNaasten { get; set; }
        public string ZiekteBelevingOmgaanMetZiekteprocesPatient { get; set; }
        public string ZiekteBelevingVerpleegkundigeInterventies { get; set; }
        public string ZiekteBelevingZiekteInzicht { get; set; }
        public Radio ZijnMedeBehandelaars { get; set; } = new Radio();
        public Dropdown ZintuigenHoren { get; set; } = new Dropdown();
        public Dropdown ZintuigenHorenHulpmiddelen { get; set; } = new Dropdown();
        public string ZintuigenOverig { get; set; }
        public string ZintuigenVerpleegkundigeInterventies { get; set; }
        public Dropdown ZintuigenZien { get; set; } = new Dropdown();
        public Dropdown ZintuigenZienHulpmiddelen { get; set; } = new Dropdown();
        public string Zorgvragen { get; set; }
        public Radio Zuurstofgebruik { get; set; } = new Radio();
        public string ZuurstofgebruikToelichting { get; set; }


    }
}
