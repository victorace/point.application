﻿using System;

namespace Point.HL7.Processing.Models
{
    public class HL7HeaderInfo
    {

        public string CharacterSet { get; set; }
        public string MessageHeaderControlID { get; set; }
        public string AcknowledgeControlID { get; set; }
        public DateTime DateTimeOfMessage { get; set; }
        public string MessageType { get; set; }
        public string TriggerEvent { get; set; }
        public string Version { get; set; }


        private string messagestructure = null;
        public string MessageStructure {
            get
            {
                if (String.IsNullOrEmpty(messagestructure))
                {
                    return $"{MessageType}_{TriggerEvent}";
                }
                else
                {
                    return messagestructure;
                }
            }

            set
            {
                messagestructure = value;
            }
        }



    }
}
