﻿using System;

namespace Point.HL7.Processing.Models
{
    public class HL7Client
    {
        public DateTime TimeStamp { get; set; }
        public string MessageID { get; set; }
        public int OrganizationID { get; set; }
        public int? LocationID { get; set; }
        public string InterfaceCode { get; set; }

        public string Error { get; set; }

        public string PatientNumber { get; set; }
        public string VisitNumber { get; set; }
        public string CivilServiceNumber { get; set; }

        public string Gender { get; set; }
        public string Salutation { get; set; }
        public string Initials { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string MaidenName { get; set; }
        public DateTime BirthDate { get; set; }

        
        public string StreetName { get; set; }
        public string Number { get; set; }

        public string NumberAddition { get; set; }
        public string PostalCode { get; set; }
        public string City { get; set; }
        public string Country { get; set; }

        public string PhoneNumberGeneral { get; set; }
        public string PhoneNumberMobile { get; set; }
        public string PhoneNumberWork { get; set; }


        public string ContactPersonName { get; set; }
        public string ContactPersonRelationType { get; set; }
        public string ContactPersonBirthDate { get; set; }
        public string ContactPersonPhoneNumberGeneral { get; set; }
        public string ContactPersonPhoneNumberMobile { get; set; }
        public string ContactPersonPhoneNumberWork { get; set; }

        public string HealthInsuranceCompanyUZOVICode { get; set; }

        public int HealthInsuranceCompanyID { get; set; }
        public string InsuranceNumber { get; set; }
        public string HealthCareProvider { get; set; }

        public string GeneralPractitionerName { get; set; }
        public string GeneralPractitionerPhoneNumber { get; set; }
        public string GeneralPractitionerZorgmail { get; set; }

        public string PharmacyName { get; set; }
        public string PharmacyPhoneNumber { get; set; }


        public bool EqualsEx(object obj)
        {
            var other = obj as HL7Client;
            if (other == null)
            {
                return false;
            }
            if (OrganizationID != other.OrganizationID ||
                LocationID != other.LocationID ||
                InterfaceCode != other.InterfaceCode ||
                PatientNumber != other.PatientNumber ||
                VisitNumber != other.VisitNumber ||
                CivilServiceNumber != other.CivilServiceNumber ||
                Gender != other.Gender ||
                Salutation != other.Salutation ||
                Initials != other.Initials ||
                FirstName != other.FirstName ||
                MiddleName != other.MiddleName ||
                LastName != other.LastName ||
                MaidenName != other.MaidenName ||
                BirthDate != other.BirthDate ||
                StreetName != other.StreetName ||
                Number != other.Number ||
                NumberAddition != other.NumberAddition ||
                PostalCode != other.PostalCode ||
                City != other.City ||
                Country != other.Country ||
                PhoneNumberGeneral != other.PhoneNumberGeneral ||
                PhoneNumberMobile != other.PhoneNumberMobile ||
                PhoneNumberWork != other.PhoneNumberWork ||
                ContactPersonName != other.ContactPersonName ||
                ContactPersonRelationType != other.ContactPersonRelationType ||
                ContactPersonBirthDate != other.ContactPersonBirthDate ||
                ContactPersonPhoneNumberGeneral != other.ContactPersonPhoneNumberGeneral ||
                ContactPersonPhoneNumberMobile != other.ContactPersonPhoneNumberMobile ||
                ContactPersonPhoneNumberWork != other.ContactPersonPhoneNumberWork ||
                HealthInsuranceCompanyUZOVICode != other.HealthInsuranceCompanyUZOVICode ||
                HealthInsuranceCompanyID != other.HealthInsuranceCompanyID ||
                InsuranceNumber != other.InsuranceNumber ||
                GeneralPractitionerName != other.GeneralPractitionerName ||
                GeneralPractitionerPhoneNumber != other.GeneralPractitionerPhoneNumber ||
                GeneralPractitionerZorgmail != other.GeneralPractitionerZorgmail ||
                HealthCareProvider != other.HealthCareProvider ||
                PharmacyName != other.PharmacyName ||
                PharmacyPhoneNumber != other.PharmacyPhoneNumber )
            {
                return false;
            }

            return true;
        }

        public string ToStringEx()
        {
            return $"TimeStamp: {TimeStamp}" + Environment.NewLine +
                    $"MessageID: {MessageID}" + Environment.NewLine +
                    $"OrganizationID: {OrganizationID}" + Environment.NewLine +
                    $"LocationID: {LocationID}" + Environment.NewLine +
                    $"InterfaceCode: {InterfaceCode}" + Environment.NewLine +
                    "-----" + Environment.NewLine +
                    $"Error: {Error}" + Environment.NewLine +
                    "-----" + Environment.NewLine +
                    $"PatientNumber: {PatientNumber}" + Environment.NewLine +
                    $"VisitNumber: {VisitNumber}" + Environment.NewLine +
                    $"CivilServiceNumber: {CivilServiceNumber}" + Environment.NewLine +
                    "-----" + Environment.NewLine +
                    $"Gender: {Gender}" + Environment.NewLine +
                    $"Salutation: {Salutation}" + Environment.NewLine +
                    $"Initials: {Initials}" + Environment.NewLine +
                    $"FirstName: {FirstName}" + Environment.NewLine +
                    $"MiddleName: {MiddleName}" + Environment.NewLine +
                    $"LastName: {LastName}" + Environment.NewLine +
                    $"MaidenName: {MaidenName}" + Environment.NewLine +
                    $"BirthDate: {BirthDate}" + Environment.NewLine +
                    "-----" + Environment.NewLine +
                    $"StreetName: {StreetName}" + Environment.NewLine +
                    $"Number: {Number}" + Environment.NewLine +
                    "-----" + Environment.NewLine +
                    $"NumberAddition: {NumberAddition}" + Environment.NewLine +
                    $"PostalCode: {PostalCode}" + Environment.NewLine +
                    $"City: {City}" + Environment.NewLine +
                    $"Country: {Country}" + Environment.NewLine +
                    "-----" + Environment.NewLine +
                    $"PhoneNumberGeneral: {PhoneNumberGeneral}" + Environment.NewLine +
                    $"PhoneNumberMobile: {PhoneNumberMobile}" + Environment.NewLine +
                    $"PhoneNumberWork: {PhoneNumberWork}" + Environment.NewLine +
                    "-----" + Environment.NewLine +
                    $"ContactPersonName: {ContactPersonName}" + Environment.NewLine +
                    $"ContactPersonRelationType: {ContactPersonRelationType}" + Environment.NewLine +
                    $"ContactPersonBirthDate: {ContactPersonBirthDate}" + Environment.NewLine +
                    $"ContactPersonPhoneNumberGeneral: {ContactPersonPhoneNumberGeneral}" + Environment.NewLine +
                    $"ContactPersonPhoneNumberMobile: {ContactPersonPhoneNumberMobile}" + Environment.NewLine +
                    $"ContactPersonPhoneNumberWork: {ContactPersonPhoneNumberWork}" + Environment.NewLine +
                    "-----" + Environment.NewLine +
                    $"HealthInsuranceCompanyUZOVICode: {HealthInsuranceCompanyUZOVICode}" + Environment.NewLine +
                    $"HealthInsuranceCompanyID: {HealthInsuranceCompanyID}" + Environment.NewLine +
                    $"InsuranceNumber: {InsuranceNumber}" + Environment.NewLine +
                    $"HealthCareProvider: {HealthCareProvider}" + Environment.NewLine +
                    "-----" + Environment.NewLine +
                    $"GeneralPractitionerName: {GeneralPractitionerName}" + Environment.NewLine +
                    $"GeneralPractitionerPhoneNumber: {GeneralPractitionerPhoneNumber}" + Environment.NewLine +
                    $"GeneralPractitionerZorgmail: {GeneralPractitionerZorgmail}" + Environment.NewLine +
                    "-----" + Environment.NewLine +
                    $"PharmacyName: {PharmacyName}" + Environment.NewLine +
                    $"PharmacyPhoneNumber: {PharmacyPhoneNumber}" + Environment.NewLine;
        }
    }

    
}
