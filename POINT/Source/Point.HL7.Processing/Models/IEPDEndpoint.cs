﻿using System.ServiceModel;
using System.ServiceModel.Channels;

namespace Point.HL7.Processing.Models
{
    public interface IEPDEndpoint<T>
    {
        Binding Binding { get; set; }
        EndpointAddress EndpointAddress { get; set; }
        bool ValidConnection();
        T Get(EPDRequest epdRequest);      
    }
}
