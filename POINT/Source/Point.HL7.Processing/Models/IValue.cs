﻿namespace Point.HL7.Processing.Models
{
    public interface IValue
    {
        string GetValue();
    }

    public class Dropdown : IValue
    {
        public string Value { get; set; }

        public string GetValue()
        {
            //return Value ?? "";
            return Value;
        }
    }

    public class Radio : IValue
    {
        public string Value { get; set; }
        public string GetValue()
        {
            //return Value ?? "";
            return Value;
        }

    }

    public class CheckBox : IValue
    {
        public string Value { get; set; }
        public string GetValue()
        {
            //return Value ?? "";
            return Value;
        }
    }

    public class IDText : IValue
    {
        public string Value { get; set; }
        public string GetValue()
        {
            //return Value ?? "";
            return Value;
        }

    }

    public class Hidden : IValue
    {
        public string Value { get; set; }
        public string GetValue()
        {
            //return Value ?? "";
            return Value;
        }
    }
}