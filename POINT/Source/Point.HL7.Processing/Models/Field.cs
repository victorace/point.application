﻿using Point.Database.Models;
using System;

namespace Point.HL7.Processing.Models
{
    public class Field
    {
        public string FieldName { get; set; }
        public string FieldValue { get; set; }
        public Type FieldType { get; set; }

        public string GetLabel()
        {
            string label = "";
            switch (FieldName)
            {
                case FlowFieldConstants.Name_BehandelaarNaam:
                    label = "Behandelend arts"; break;
                case FlowFieldConstants.Name_DatumEindeBehandelingMedischSpecialist:
                    label = "Gewenste ontslagdatum"; break;
                case FlowFieldConstants.Name_Kamernummer:
                    label = "Kamernummer"; break;
                case FlowFieldConstants.Name_MedischeSituatieDatumOpname:
                    label = "Datum opname"; break;
                case FlowFieldConstants.Name_MedischeSituatieRedenOpname:
                    label = "Diagnose/Reden opname/aanvraag"; break;
                case FlowFieldConstants.Name_SourceOrganizationDepartment:
                    label = "Versturende afdeling"; break;
                default:
                    label = FieldName; break;
            }
            return label;
        }

    }
}
