﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Point.HL7.Processing.Models
{
    public class OrgaLocaDep
    {
        public int OrganizationID { get; set; }
        public string OrganizationName { get; set; }
        public int LocationID { get; set; }
        public string LocationName { get; set; }

        public int DepartmentID { get; set; }
        public string DepartmentName { get; set; }
        public string DepartmentExternID { get; set; }
    }
}
