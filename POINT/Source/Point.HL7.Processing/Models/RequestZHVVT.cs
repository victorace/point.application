﻿using System;

namespace Point.HL7.Processing.Models
{
    public class RequestZHVVT
    {
        public string PatientNumber { get; set; }
        public string VisitNumber { get; set; }
        public string CivilServiceNumber { get; set; }
        public DateTime? BirthDate { get; set; }
        public string Gender { get; set; }
        //ClientID ???
        //TransferID ???
        public string ResultString { get; set; }
        public DateTime? TimeStamp { get; set; }

        public string AanvraagToelichtingRHD { get; set; }
        public Radio AanvraagVoor { get; set; } = new Radio();
        public string AanwezigheidContactpersoonBijTransfergesprekGewenst { get; set; }
        public Radio AanwezigheidContactpersoonBijTransfergesprekGewenstJaNee { get; set; } = new Radio();
        public string AfleveringAndersRHD { get; set; }
        public CheckBox AfleveringRHD { get; set; } = new CheckBox();
        public Dropdown AfterCare { get; set; } = new Dropdown();
        public Radio AfterCareCategory { get; set; } = new Radio();
        public string AfwijkendLeveringAdres { get; set; }
        public CheckBox BasiszorgEtenEnDrinken { get; set; } = new CheckBox();
        public string BasiszorgEtenEnDrinkenKerenPerDag { get; set; }
        public string BasiszorgEtenEnDrinkenKerenPerWeek { get; set; }
        public string BasiszorgEtenEnDrinkenToelichting { get; set; }
        public CheckBox BasiszorgMobiliteit { get; set; } = new CheckBox();
        public string BasiszorgMobiliteitKerenPerDag { get; set; }
        public string BasiszorgMobiliteitKerenPerWeek { get; set; }
        public string BasiszorgMobiliteitToelichting { get; set; }
        public CheckBox BasiszorgOverig { get; set; } = new CheckBox();
        public string BasiszorgOverigKerenPerDag { get; set; }
        public string BasiszorgOverigKerenPerWeek { get; set; }
        public string BasiszorgOverigToelichting { get; set; }
        public CheckBox BasiszorgPersoonlijkeVerzorging { get; set; } = new CheckBox();
        public string BasiszorgPersoonlijkeVerzorgingKerenPerDag { get; set; }
        public string BasiszorgPersoonlijkeVerzorgingKerenPerWeek { get; set; }
        public string BasiszorgPersoonlijkeVerzorgingToelichting { get; set; }
        public CheckBox BasiszorgToiletgang { get; set; } = new CheckBox();
        public string BasiszorgToiletgangKerenPerDag { get; set; }
        public string BasiszorgToiletgangKerenPerWeek { get; set; }
        public string BasiszorgToiletgangToelichting { get; set; }
        public CheckBox BasiszorgTransferBedStoel { get; set; } = new CheckBox();
        public string BasiszorgTransferBedStoelKerenPerDag { get; set; }
        public string BasiszorgTransferBedStoelKerenPerWeek { get; set; }
        public string BasiszorgTransferBedStoelToelichting { get; set; }
        public string BehandelaarAGB { get; set; }
        public string BehandelaarBIG { get; set; }
        public string BehandelaarNaam { get; set; }
        public string BehandelaarOpmerkingen { get; set; }
        public Dropdown BehandelaarSpecialisme { get; set; } = new Dropdown();
        public string BehandelaarTelefoonPieper { get; set; }
        public Dropdown BehandelendArtsID { get; set; } = new Dropdown();
        public string BehandelingRHD { get; set; }
        public DateTime? CareBeginDate { get; set; }
        public CheckBox CatheterVerzorging { get; set; } = new CheckBox();
        public CheckBox CatheterVerzorgingBlaascatheter { get; set; } = new CheckBox();
        public string CatheterVerzorgingBlaascatheterKerenPerDag { get; set; }
        public string CatheterVerzorgingBlaascatheterKerenPerWeek { get; set; }
        public string CatheterVerzorgingBlaascatheterToelichting { get; set; }
        public CheckBox CatheterVerzorgingSuprapubisCatheter { get; set; } = new CheckBox();
        public string CatheterVerzorgingSuprapubisCatheterKerenPerDag { get; set; }
        public string CatheterVerzorgingSuprapubisCatheterKerenPerWeek { get; set; }
        public string CatheterVerzorgingSuprapubisCatheterToelichting { get; set; }
        public string ContinuiteitsbezoekToelichting { get; set; }
        public CheckBox ControleLichaamsfunctiesAnders { get; set; } = new CheckBox();
        public string ControleLichaamsfunctiesAndersKerenPerDag { get; set; }
        public string ControleLichaamsfunctiesAndersKerenPerWeek { get; set; }
        public string ControleLichaamsfunctiesAndersToelichting { get; set; }
        public CheckBox ControleLichaamsfunctiesBloedsuikerControle { get; set; } = new CheckBox();
        public string ControleLichaamsfunctiesBloedsuikerControleKerenPerDag { get; set; }
        public string ControleLichaamsfunctiesBloedsuikerControleKerenPerWeek { get; set; }
        public string ControleLichaamsfunctiesBloedsuikerControleToelichting { get; set; }
        public CheckBox ControleLichaamsfunctiesZuurstoftoediening { get; set; } = new CheckBox();
        public string ControleLichaamsfunctiesZuurstoftoedieningKerenPerDag { get; set; }
        public string ControleLichaamsfunctiesZuurstoftoedieningKerenPerWeek { get; set; }
        public string ControleLichaamsfunctiesZuurstoftoedieningToelichting { get; set; }
        public string CreatedByDepartmentTelephoneNumber { get; set; }
        public DateTime? DatumEindeBehandelingInitieel { get; set; }
        public DateTime? DatumEindeBehandelingMedischSpecialist { get; set; }
        public string DatumEindeBehandelingMedischSpecialistToelichting { get; set; }
        public CheckBox DeMogelijkheidVanHulpmiddelen { get; set; } = new CheckBox();
        public string DiagnoseRHD { get; set; }
        public Dropdown DossierOwner { get; set; } = new Dropdown();
        public string DossierOwnerTelephoneNumber { get; set; }
        public CheckBox Drainverzorging { get; set; } = new CheckBox();
        public string DrainverzorgingKerenPerDag { get; set; }
        public string DrainverzorgingKerenPerWeek { get; set; }
        public string DrainverzorgingToelichting { get; set; }
        public DateTime? EindDatumRHD { get; set; }
        public Dropdown ExtramuraalTyperingNazorg { get; set; } = new Dropdown();
        public string FilledOutBy { get; set; }
        public Radio GebruikPalliatieveSectie { get; set; } = new Radio();
        public Radio GeenToestemmingPatientVan { get; set; } = new Radio();
        public string GeenToestemmingPatientVanReden { get; set; }
        public string GewensteFrequentie { get; set; }
        public DateTime? GewensteIngangsdatum { get; set; }
        public string GewensteZorg { get; set; }
        public string HervattenZorgToelichting { get; set; }
        public Radio HervattenZorgType { get; set; } = new Radio();
        public Dropdown HospiceTyperingNazorg { get; set; } = new Dropdown();
        public string HulpmiddelenAndersRHD { get; set; }
        public CheckBox HulpmiddelenRHD { get; set; } = new CheckBox();
        public CheckBox Infectie { get; set; } = new CheckBox();
        public Radio InfectieIsolatie { get; set; } = new Radio();
        public CheckBox InfectieIsolatieAnders { get; set; } = new CheckBox();
        public string InfectieIsolatieAndersTekst { get; set; }
        public CheckBox InfectieIsolatieInfectieAnders { get; set; } = new CheckBox();
        public string InfectieIsolatieInfectieAndersTekst { get; set; }
        public CheckBox InfectieIsolatieSoortInfectie { get; set; } = new CheckBox();
        public CheckBox InfectieIsolatieSoortIsolatie { get; set; } = new CheckBox();
        public string InfectieMRSA { get; set; }
        public string InfectieNoodzakelijke { get; set; }
        public CheckBox Infusietherapie { get; set; } = new CheckBox();
        public CheckBox InfusietherapieCentraalVeneuzeLijn { get; set; } = new CheckBox();
        public string InfusietherapieCentraalVeneuzeLijnKerenPerDag { get; set; }
        public string InfusietherapieCentraalVeneuzeLijnKerenPerWeek { get; set; }
        public string InfusietherapieCentraalVeneuzeLijnToelichting { get; set; }
        public CheckBox InfusietherapieParenteraleToedieningMedicatie { get; set; } = new CheckBox();
        public string InfusietherapieParenteraleToedieningMedicatieKerenPerDag { get; set; }
        public string InfusietherapieParenteraleToedieningMedicatieKerenPerWeek { get; set; }
        public string InfusietherapieParenteraleToedieningMedicatieToelichting { get; set; }
        public CheckBox InfusietherapieParenteraleVoeding { get; set; } = new CheckBox();
        public string InfusietherapieParenteraleVoedingKerenPerDag { get; set; }
        public string InfusietherapieParenteraleVoedingKerenPerWeek { get; set; }
        public string InfusietherapieParenteraleVoedingToelichting { get; set; }
        public CheckBox InfusietherapiePICC { get; set; } = new CheckBox();
        public string InfusietherapiePICCKerenPerDag { get; set; }
        public string InfusietherapiePICCKerenPerWeek { get; set; }
        public string InfusietherapiePICCToelichting { get; set; }
        public CheckBox IngeschakeldeOndersteunendeDienstenAnders { get; set; } = new CheckBox();
        public CheckBox IngeschakeldeOndersteunendeDienstenDietist { get; set; } = new CheckBox();
        public CheckBox IngeschakeldeOndersteunendeDienstenErgotherapeut { get; set; } = new CheckBox();
        public CheckBox IngeschakeldeOndersteunendeDienstenFysiotherapeut { get; set; } = new CheckBox();
        public CheckBox IngeschakeldeOndersteunendeDienstenGeriater { get; set; } = new CheckBox();
        public CheckBox IngeschakeldeOndersteunendeDienstenGespecialiseerdVerpleegkundige { get; set; } = new CheckBox();
        public CheckBox IngeschakeldeOndersteunendeDienstenLogopedist { get; set; } = new CheckBox();
        public CheckBox IngeschakeldeOndersteunendeDienstenMedischMaatschappelijkWerk { get; set; } = new CheckBox();
        public CheckBox IngeschakeldeOndersteunendeDienstenPedagogischMedewerker { get; set; } = new CheckBox();
        public CheckBox IngeschakeldeOndersteunendeDienstenPsychiater { get; set; } = new CheckBox();
        public CheckBox IngeschakeldeOndersteunendeDienstenRevalidatiearts { get; set; } = new CheckBox();
        public string IngeschakeldeOndersteunendeDienstenToelichting { get; set; }
        public CheckBox InschattingZorgverleningNaOntslagMetZorgNaarHuis { get; set; } = new CheckBox();
        public Radio InschattingZorgverleningNaOntslagNieuweAanvraag { get; set; } = new Radio();
        public CheckBox InschattingZorgverleningNaOntslagZorgInZorginstelling { get; set; } = new CheckBox();
        public Dropdown IntramuraalTyperingNazorg { get; set; } = new Dropdown();
        public string InventarisatieMedisch { get; set; }
        public string InventarisatieOntwikkeling { get; set; }
        public string InventarisatieOpmerkingAfdVPK { get; set; }
        public string InventarisatieSociaal { get; set; }
        public string InventarisatieVeiligheid { get; set; }
        public CheckBox KamerNoodzakelijk { get; set; } = new CheckBox();
        public string Kamernummer { get; set; }
        public Radio KwetsbareOudereJaNee { get; set; } = new Radio();
        public string KwetsbareOudereToelichting { get; set; }
        public string Mantelzorg { get; set; }
        public CheckBox MedicatieToediening { get; set; } = new CheckBox();
        public CheckBox MedicatieToedieningAnders { get; set; } = new CheckBox();
        public string MedicatieToedieningAndersKerenPerDag { get; set; }
        public string MedicatieToedieningAndersKerenPerWeek { get; set; }
        public string MedicatieToedieningAndersToelichting { get; set; }
        public CheckBox MedicatieToedieningDruppelen { get; set; } = new CheckBox();
        public string MedicatieToedieningDruppelenKerenPerDag { get; set; }
        public string MedicatieToedieningDruppelenKerenPerWeek { get; set; }
        public string MedicatieToedieningDruppelenToelichting { get; set; }
        public CheckBox MedicatieToedieningInhalatieVerneveling { get; set; } = new CheckBox();
        public string MedicatieToedieningInhalatieVernevelingKerenPerDag { get; set; }
        public string MedicatieToedieningInhalatieVernevelingKerenPerWeek { get; set; }
        public string MedicatieToedieningInhalatieVernevelingToelichting { get; set; }
        public CheckBox MedicatieToedieningPerInjectie { get; set; } = new CheckBox();
        public string MedicatieToedieningPerInjectieKerenPerDag { get; set; }
        public string MedicatieToedieningPerInjectieKerenPerWeek { get; set; }
        public string MedicatieToedieningPerInjectieToelichting { get; set; }
        public CheckBox MedicatieToedieningPerOs { get; set; } = new CheckBox();
        public string MedicatieToedieningPerOsKerenPerDag { get; set; }
        public string MedicatieToedieningPerOsKerenPerWeek { get; set; }
        public string MedicatieToedieningPerOsToelichting { get; set; }
        public CheckBox MedicatieToedieningZalven { get; set; } = new CheckBox();
        public string MedicatieToedieningZalvenKerenPerDag { get; set; }
        public string MedicatieToedieningZalvenKerenPerWeek { get; set; }
        public string MedicatieToedieningZalvenToelichting { get; set; }
        public CheckBox MedicatieToezichtOpInname { get; set; } = new CheckBox();
        public string MedicatieToezichtOpInnameKerenPerDag { get; set; }
        public string MedicatieToezichtOpInnameKerenPerWeek { get; set; }
        public string MedicatieToezichtOpInnameToelichting { get; set; }
        public string MedischeSituatieBehandeling { get; set; }
        public DateTime? MedischeSituatieDatumOpname { get; set; }
        public string MedischeSituatieRedenOpname { get; set; }
        public string MedischeSituatieVoorgeschiedenis { get; set; }
        public CheckBox MetZorgNaarHuisAanpassingVanBestaandeZorg { get; set; } = new CheckBox();
        public CheckBox MetZorgNaarHuisContinueringVanBestaandeZorg { get; set; } = new CheckBox();
        public CheckBox MetZorgNaarHuisNieuweAanvraagAlarmering { get; set; } = new CheckBox();
        public CheckBox MetZorgNaarHuisNieuweAanvraagAndersNl { get; set; } = new CheckBox();
        public string MetZorgNaarHuisNieuweAanvraagAndersNlText { get; set; }
        public CheckBox MetZorgNaarHuisNieuweAanvraagDagopvangDagbehandeling { get; set; } = new CheckBox();
        public CheckBox MetZorgNaarHuisNieuweAanvraagHulpBijHuishouding { get; set; } = new CheckBox();
        public CheckBox MetZorgNaarHuisNieuweAanvraagMaaltijdvoorziening { get; set; } = new CheckBox();
        public CheckBox MetZorgNaarHuisNieuweAanvraagPalliatieveTerminaleZorgThuis { get; set; } = new CheckBox();
        public CheckBox MetZorgNaarHuisNieuweAanvraagPersoonlijkeVerzorging { get; set; } = new CheckBox();
        public CheckBox MetZorgNaarHuisNieuweAanvraagVerpleging { get; set; } = new CheckBox();
        public CheckBox NierfunctievervangendeTherapie { get; set; } = new CheckBox();
        public string NierfunctievervangendeTherapieKerenPerDag { get; set; }
        public string NierfunctievervangendeTherapieKerenPerWeek { get; set; }
        public string NierfunctievervangendeTherapieToelichting { get; set; }
        public CheckBox NoodzakelijkTeRegelenHulpmiddelen { get; set; } = new CheckBox();
        public Radio NoodzakelijkTeRegelenHulpmiddelenAflevering { get; set; } = new Radio();
        public CheckBox NoodzakelijkTeRegelenHulpmiddelenAfleveringAnderAdres { get; set; } = new CheckBox();
        public string NoodzakelijkTeRegelenHulpmiddelenAfleveringAnderAdresText { get; set; }
        public CheckBox NoodzakelijkTeRegelenHulpmiddelenAfleveringThuis { get; set; } = new CheckBox();
        public CheckBox NoodzakelijkTeRegelenHulpmiddelenAfleveringZiekenhuis { get; set; } = new CheckBox();
        public CheckBox NoodzakelijkTeRegelenHulpmiddelenAlarmering { get; set; } = new CheckBox();
        public CheckBox NoodzakelijkTeRegelenHulpmiddelenAnders { get; set; } = new CheckBox();
        public string NoodzakelijkTeRegelenHulpmiddelenAndersText { get; set; }
        public CheckBox NoodzakelijkTeRegelenHulpmiddelenAntiDecubitusMatras { get; set; } = new CheckBox();
        public CheckBox NoodzakelijkTeRegelenHulpmiddelenDraaischijf { get; set; } = new CheckBox();
        public CheckBox NoodzakelijkTeRegelenHulpmiddelenHoogLaagbed { get; set; } = new CheckBox();
        public CheckBox NoodzakelijkTeRegelenHulpmiddelenKrukkenStok { get; set; } = new CheckBox();
        public CheckBox NoodzakelijkTeRegelenHulpmiddelenPostoel { get; set; } = new CheckBox();
        public CheckBox NoodzakelijkTeRegelenHulpmiddelenRollator { get; set; } = new CheckBox();
        public CheckBox NoodzakelijkTeRegelenHulpmiddelenRolstoel { get; set; } = new CheckBox();
        public CheckBox NoodzakelijkTeRegelenHulpmiddelenTillift { get; set; } = new CheckBox();
        public string NoodzakelijkTeRegelenHulpmiddelenToelichting { get; set; }
        public CheckBox Obesitas { get; set; } = new CheckBox();
        public string ObesitasBMI { get; set; }
        public string ObesitasGewicht { get; set; }
        public string ObesitasLengte { get; set; }
        public DateTime? OorspronkelijkeDatumEindeBehandelingMedischSpecialist { get; set; }
        public string OpmerkingenRHD { get; set; }
        public Radio OudersEnKindDiagnose { get; set; } = new Radio();
        public string OudersEnKindDiagnoseWelke { get; set; }
        public string OudersEnKindHandelingenBuitenHetZiekenhuis { get; set; }
        public string OudersEnKindHandelingenBuitenHetZiekenhuisVanaf { get; set; }
        public string OudersEnKindHandelingenBuitenHetZiekenhuisZelf { get; set; }
        public string OudersEnKindHulpmiddelen { get; set; }
        public Radio OudersEnKindMeerInformatie { get; set; } = new Radio();
        public string OudersEnKindMeerInformatieWaarover { get; set; }
        public string OudersEnKindOndersteuningPersoonlijkeVerzorging { get; set; }
        public string OudersEnKindOntwikkelingCognitieve { get; set; }
        public string OudersEnKindOntwikkelingGevolgen { get; set; }
        public string OudersEnKindOntwikkelingMotorische { get; set; }
        public string OudersEnKindOntwikkelingProfessioneleOndersteuning { get; set; }
        public string OudersEnKindOntwikkelingSociaalEmotionele { get; set; }
        public string OudersEnKindOntwikkelingSpraak { get; set; }
        public string OudersEnKindOntwikkelingUitgesprokenGedrag { get; set; }
        public string OudersEnKindSociaalDraagkracht { get; set; }
        public string OudersEnKindSociaalDraaglast { get; set; }
        public string OudersEnKindSociaalOndersteunen { get; set; }
        public string OudersEnKindSociaalSocialeNetwerk { get; set; }
        public string OudersEnKindSociaalWerkzaamheden { get; set; }
        public string OudersEnKindVeiligheidContactMetAndereBetrokkenInstanties { get; set; }
        public string OudersEnKindVeiligheidGezondheid { get; set; }
        public string OudersEnKindVeiligheidPratenOverDeSituatie { get; set; }
        public string OudersEnKindVeiligheidZorg { get; set; }
        public string OudersEnKindVerblijf { get; set; }
        public string OudersEnKindVerblijfHandelingenNodig { get; set; }
        public string OudersEnKindZorgEnOpvang { get; set; }
        public CheckBox OverigeVerrichtingen { get; set; } = new CheckBox();
        public string OverigeVerrichtingenKerenPerDag { get; set; }
        public string OverigeVerrichtingenKerenPerWeek { get; set; }
        public string OverigeVerrichtingenToelichting { get; set; }
        public Radio PatientOntvingZorgVoorOpnameJaNee { get; set; } = new Radio();
        public Dropdown PrognoseRHD { get; set; } = new Dropdown();
        public Dropdown PrognoseVerwachteOntwikkeling { get; set; } = new Dropdown();
        public Radio PsychischFunctionerenDenkWaarnemingsstroornissen { get; set; } = new Radio();
        public Radio PsychischFunctionerenDenkWaarnemingsstroornissen_Geen { get; set; } = new Radio();
        public Radio PsychischFunctionerenDenkWaarnemingsstroornissen_Matig { get; set; } = new Radio();
        public Radio PsychischFunctionerenDesorientatie { get; set; } = new Radio();
        public Radio PsychischFunctionerenDesorientatie_Geen { get; set; } = new Radio();
        public Radio PsychischFunctionerenDesorientatie_Matig { get; set; } = new Radio();
        public Radio PsychischFunctionerenGedragsstoornissen { get; set; } = new Radio();
        public Radio PsychischFunctionerenGedragsstoornissen_Geen { get; set; } = new Radio();
        public Radio PsychischFunctionerenGedragsstoornissen_Matig { get; set; } = new Radio();
        public Radio PsychischFunctionerenGeheugenverlies { get; set; } = new Radio();
        public Radio PsychischFunctionerenGeheugenverlies_Geen { get; set; } = new Radio();
        public Radio PsychischFunctionerenGeheugenverlies_Matig { get; set; } = new Radio();
        public string PsychischFunctionerenItemIngevuldToelichting { get; set; }
        public Radio PsychischFunctionerenJaNee { get; set; } = new Radio();
        public Radio PsychischFunctionerenStemmingsstoornissen { get; set; } = new Radio();
        public Radio PsychischFunctionerenStemmingsstoornissen_Geen { get; set; } = new Radio();
        public Radio PsychischFunctionerenStemmingsstoornissen_Matig { get; set; } = new Radio();
        public CheckBox PsychischFunctionerenZiekteInzicht_Nee { get; set; } = new CheckBox();
        public Radio PsychischFunctionerenZiekteInzichtJaNee { get; set; } = new Radio();
        public DateTime? RealizedDischargeDate { get; set; }
        public CheckBox RelevanteAspectenVoorZorgverleningAnders { get; set; } = new CheckBox();
        public string RelevanteAspectenVoorZorgverleningAndersText { get; set; }
        public CheckBox RelevanteAspectenVoorZorgverleningDwaalZwerfgedrag { get; set; } = new CheckBox();
        public CheckBox RelevanteAspectenVoorZorgverleningGehoorproblemen { get; set; } = new CheckBox();
        public CheckBox RelevanteAspectenVoorZorgverleningIncontinentie { get; set; } = new CheckBox();
        public Radio RelevanteAspectenVoorZorgverleningIncontinentieKeus { get; set; } = new Radio();
        public CheckBox RelevanteAspectenVoorZorgverleningSpraakproblemen { get; set; } = new CheckBox();
        public CheckBox RelevanteAspectenVoorZorgverleningVerwaarlozing { get; set; } = new CheckBox();
        public CheckBox RelevanteAspectenVoorZorgverleningVisusproblemen { get; set; } = new CheckBox();
        public CheckBox RelevanteAspectenVoorZorgverleningZwakOntbrekendMantelzorgnetwerk { get; set; } = new CheckBox();
        public Dropdown RequestFormZHVVTType { get; set; } = new Dropdown();
        public string RevalidatiedoelenToelichting { get; set; }
        public CheckBox Sondevoeding { get; set; } = new CheckBox();
        public string SondevoedingKerenPerDag { get; set; }
        public string SondevoedingKerenPerWeek { get; set; }
        public string SondevoedingToelichting { get; set; }
        public IDText SourceOrganization { get; set; } = new IDText();
        public IDText SourceOrganizationDepartment { get; set; } = new IDText();
        public IDText SourceOrganizationLocation { get; set; } = new IDText();
        public Dropdown SpecialismeHospital { get; set; } = new Dropdown();
        public DateTime? StartDatumRHD { get; set; }
        public CheckBox Stomaverzorging { get; set; } = new CheckBox();
        public string StomaverzorgingKerenPerDag { get; set; }
        public string StomaverzorgingKerenPerWeek { get; set; }
        public string StomaverzorgingToelichting { get; set; }
        public string ToelichtingRHD { get; set; }
        public CheckBox ToestemmingOuderPatientNietGegeven { get; set; } = new CheckBox();
        public Radio ToestemmingPatient { get; set; } = new Radio();
        public CheckBox ToestemmingPatientHandtekeningVastgelegd { get; set; } = new CheckBox();
        public Dropdown ToestemmingPatientIngevuldDoor { get; set; } = new Dropdown();
        public CheckBox ToestemmingPatientKonNietVerkregenWorden { get; set; } = new CheckBox();
        public CheckBox ToestemmingPatientNietGegeven { get; set; } = new CheckBox();
        public string ToestemmingPatientNietMogelijkReden { get; set; }
        public CheckBox ToestemmingPatientOuderHandtekeningGewenst { get; set; } = new CheckBox();
        public string ToestemmingPatientRedenGeenToestemming { get; set; }
        public Radio ToestemmingPatientVan { get; set; } = new Radio();
        public CheckBox ToestemmingPatientVanOuderOfDiensVertegenwoordiger { get; set; } = new CheckBox();
        public CheckBox ToestemmingPatientVanPatient { get; set; } = new CheckBox();
        public string ToestemmingPatientVanVertegenwoordigerAdres { get; set; }
        public string ToestemmingPatientVanVertegenwoordigerNaam { get; set; }
        public CheckBox TransfusiesBloed { get; set; } = new CheckBox();
        public string TransfusiesBloedKerenPerDag { get; set; }
        public string TransfusiesBloedKerenPerWeek { get; set; }
        public string TransfusiesBloedToelichting { get; set; }
        public Radio TyperingNazorg { get; set; } = new Radio();
        public CheckBox Verslaving { get; set; } = new CheckBox();
        public CheckBox VerslavingDrank { get; set; } = new CheckBox();
        public CheckBox VerslavingDrugs { get; set; } = new CheckBox();
        public CheckBox VerslavingMedicatie { get; set; } = new CheckBox();
        public string VoorgeschiedenisRHD { get; set; }
        public CheckBox Wondverzorging { get; set; } = new CheckBox();
        public string WondverzorgingKerenPerDag { get; set; }
        public string WondverzorgingKerenPerWeek { get; set; }
        public string WondverzorgingToelichting { get; set; }
        public Dropdown ZorginzetVoorDezeOpnameTypeZorginstelling { get; set; } = new Dropdown();
        public IDText ZorginzetVoorDezeOpnameTypeZorginstellingNaam { get; set; } = new IDText();
        public string ZorginzetVoorDezeOpnameTypeZorginstellingOf { get; set; }
        public string ZorginzetVoorDezeOpnameTypeZorginstellingToelichting { get; set; }
        public Radio ZorgInZorginstellingAanpassingVanBestaandeZorg { get; set; } = new Radio();
        public Radio ZorgInZorginstellingContinueringVanBestaandeZorg { get; set; } = new Radio();
        public Radio ZorgInZorginstellingHospicePalliatieveTerminaleZorgunit { get; set; } = new Radio();
        public Radio ZorgInZorginstellingNieuweAanvraag { get; set; } = new Radio();
        public Radio ZorgInZorginstellingPalliatieveTerminaleZorg { get; set; } = new Radio();
        public Radio ZorgInZorginstellingPermanentVerblijf { get; set; } = new Radio();
        public Radio ZorgInZorginstellingPermanentVerblijfAnders { get; set; } = new Radio();
        public string ZorgInZorginstellingPermanentVerblijfAndersText { get; set; }
        public Radio ZorgInZorginstellingPermanentVerblijfPG { get; set; } = new Radio();
        public Radio ZorgInZorginstellingPermanentVerblijfSomatiek { get; set; } = new Radio();
        public Radio ZorgInZorginstellingRevalidatieEnTerugkeerNaarHuis { get; set; } = new Radio();
        public Radio ZorgInZorginstellingScreenenWonen { get; set; } = new Radio();
        public Radio ZorgInZorginstellingScreeningprogramma { get; set; } = new Radio();
        public Radio ZorgInZorginstellingTijdelijkVerblijfRevalidatie { get; set; } = new Radio();
        public Radio ZorgInZorginstellingTijdelijkVerblijfRevalidatiePG { get; set; } = new Radio();
        public Radio ZorgInZorginstellingTijdelijkVerblijfRevalidatieSomatiek { get; set; } = new Radio();
        public Radio ZorgInZorginstellingVerpleeghuiszorg { get; set; } = new Radio();
        public Radio ZorgInZorginstellingVerzorgingshuisKortdurendeOpname { get; set; } = new Radio();
        public IDText ZorgInZorginstellingVoorkeurPatientstring { get; set; } = new IDText();
        public IDText ZorgInZorginstellingVoorkeurPatient2 { get; set; } = new IDText();
        public IDText ZorgInZorginstellingVoorkeurPatient3 { get; set; } = new IDText();
        public string ZorgInZorginstellingVoorkeurPatientOverig { get; set; }
        public string ZorgInZorginstellingVoorkeurPatientZorgInGemeenteText { get; set; }
        public Radio ZorgInZorginstellingZorghotelHerstellingsoord { get; set; } = new Radio();
        public string ZorgInZorginstellingZorghotelHerstellingsoordToelichting { get; set; }
        public Radio ZorgsoortBestaandeZorg { get; set; } = new Radio();
    }
}