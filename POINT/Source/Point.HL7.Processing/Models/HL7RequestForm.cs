﻿using Point.HL7.Processing.Extensions;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Point.HL7.Processing.Models
{
    public class HL7RequestForm
    {
        public List<Field> Fields { get; set; } = new List<Field>();

        public HL7RequestForm()
        {
        }

        public Field GetField(string fieldName)
        {
            return Fields.FirstOrDefault(f => f.FieldName.Equals(fieldName, StringComparison.InvariantCultureIgnoreCase));
        }

        public string GetFieldValue(string fieldName)
        {
            var field = GetField(fieldName);
            return GetFieldValue(field);
        }

        public string GetFieldValue(Field field)
        {
            return field?.FieldValue;
        }

        public DateTime? GetFieldValueDate(string fieldName)
        {
            var field = GetField(fieldName);
            return GetFieldValueDate(field);
        }

        public DateTime? GetFieldValueDate(Field field)
        {
            if (field != null && !field.FieldValue.IsNullOrEmptyHL7())
            {
                DateTime value;
                if (DateTime.TryParseExact(field.FieldValue, "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.AssumeLocal, out value))
                {
                    return value;
                }
            }
            return null;
        }

        public void SetField(string fieldName, DateTime? fieldValue)
        {
            string value = (fieldValue == null ? "" : fieldValue.Value.ToString("yyyy-MM-dd HH:mm:ss"));
            setField(fieldName, value, typeof(DateTime?));
        }

        public void SetField(string fieldName, string fieldValue)
        {
            setField(fieldName, fieldValue, typeof(string));
        }

        private void setField(string fieldName, string fieldValue, Type fieldType)
        {
            var field = GetField(fieldName);
            if (fieldValue.IsNullOrEmptyHL7())
            {
                fieldValue = "";
            }
            fieldValue = fieldValue.TrimHL7Empty();

            if (field != null)
            {
                field.FieldValue = fieldValue;
            }
            else
            {
                Fields.Add(new Field()
                {
                    FieldName = fieldName,
                    FieldValue = fieldValue,
                    FieldType = fieldType
                });
            }
        }

        public string ToStringEx()
        {
            string result = "";
            foreach(Field field in Fields)
            {
                result += $"{field.FieldName}: {field.FieldValue}" + Environment.NewLine;
            }
            return result;
        }

        public bool EqualsEx(object obj)
        {
            var other = obj as HL7RequestForm;
            if (other == null)
            {
                return false;
            }

            if (other.Fields.Count() != Fields.Count())
            {
                return false;
            }

            foreach(var field in Fields)
            {
                if (other.Fields.FirstOrDefault(f => f.FieldName == field.FieldName).FieldValue != field.FieldValue )
                {
                    return false;
                }
            }

            return true;
        }

    }

}
