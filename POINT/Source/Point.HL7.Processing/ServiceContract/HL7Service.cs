﻿using Point.Database.Models;
using Point.HL7.Processing.Business;
using Point.HL7.Processing.Extensions;
using Point.HL7.Processing.Logic;
using Point.HL7.Processing.Models;
using Point.Log4Net;
using Point.Models.Enums;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel;
using System.Threading;

namespace Point.HL7.Processing.ServiceContract
{
    [ServiceContract()]
    public interface IHL7Service
    {
        [OperationContract()]
        HL7Client GetPatient(int organizationID, int? locationID, string patientNumber);

        [OperationContract()]
        void CreateORU(int organizationID, int? locationID, int transferID, string contents);

        [OperationContract()]
        void UpdateDirectories();

        [OperationContract()]
        Dictionary<string, string> GetEOverdracht(int organizationID, EPDRequest epdRequest);

        [OperationContract()]
        Dictionary<string, string> GetRequestZHVVT(int organizationID, EPDRequest epdRequest);
    }

    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class HL7Service : IHL7Service
    {
        private static readonly ILogger Logger = LogManager.GetLogger(typeof(HL7Service));

        private static Dictionary<MessageHandleInfo, EventHandler<MessageHandlerEventArgs>> delegates = new Dictionary<MessageHandleInfo, EventHandler<MessageHandlerEventArgs>>();
        public static MessageHandler MessageHandler = new MessageHandler();

        public static string EPDAssemblyMethods = "Point.HL7.Processing.Business.Methods";
        public static string EPDAssemblyModels = "Point.HL7.Processing.Models";
        
        private ManualResetEventSlim manualResetEvent = new ManualResetEventSlim(false);

        public void UpdateDirectories()
        {
            OrganizationSettingBL.GetOrganizationSettings(false);
            HL7Files.CheckDirectories();
            HL7Files.InitWatchers(HL7Service.MessageHandler);

            var epdconnector = new EPDConnector();
            epdconnector.CheckEndPoints();
        }

        public void CreateORU(int organizationID, int? locationID, int transferID, string contents)
        {
            Logger.Info($"CreateORU() received: organizationID [{organizationID}], locationID [{locationID}], transferID [{transferID}]");

            HL7Files.CreateORU(organizationID, locationID, transferID, contents);
        }

        public HL7Client GetPatient(int organizationID, int? locationID, string patientNumber)
        {

            Logger.Info($"GetPatient() received: organizationID [{organizationID}], locationID [{locationID}], patientNumber [{patientNumber}]");

            if (!InterfaceCodeHelper.HasInterfaceCodeForLocation(locationID))
            {
                Logger.Info($"LocationID [{locationID}] has no InterfaceCode specified. Resetting locationID to null.");
                locationID = null;
            }

            if (!HL7Files.CreateQuery(organizationID, locationID, patientNumber))
            {
                return new HL7Client() { Error = $"Fout bij aanmaken QRY-bericht." };
            }

            registerDelegates(organizationID, locationID, patientNumber);

            int timeout = Int32.Parse(ConfigurationManager.AppSettings["ReceiveTimeout"]);
            manualResetEvent.Wait(timeout * 1000);

            var messagehandleinfo = MessageHandler.Get(organizationID, locationID, patientNumber);
            if (messagehandleinfo?.HL7Client != null)
            {
                return messagehandleinfo.HL7Client;
            }
            else
            {
                return new HL7Client() { Error = $"Geen gegevens ontvangen na {timeout} sec." };
            }
        }

        public Dictionary<string, string> GetEOverdracht(int organizationID, EPDRequest epdRequest)
        {
            Logger.Info($"GetEOverdracht() received: organizationID [{organizationID}], epdRequest [{epdRequest.ToString()}] ");

            EOverdracht eoverdracht = new EOverdracht();

            EndpointConfiguration endpointconfiguration = EndpointConfigurationBL.Get(organizationID)
                    .Where(ec => ec.FormTypeID == (int)FlowFormType.VerpleegkundigeOverdrachtFlow)
                    .FirstOrDefault();

            var epdendpointaddress = endpointconfiguration?.Address;
            var epdmethodname = endpointconfiguration?.Method;
            if (epdendpointaddress == null || epdmethodname == null)
            {                
                eoverdracht.ResultString = $"EPD address or method is not specified for organizationID {organizationID}";
                return new ModelHelper().ToDictionary(eoverdracht);
            }

            var epdtype = Type.GetType($"{EPDAssemblyMethods}.{epdmethodname}");
            if (epdtype == null || !typeof(IEPDEndpoint<EOverdracht>).IsAssignableFrom(epdtype))
            {
                eoverdracht.ResultString = $"Incorrect method [{epdmethodname}]";
                return new ModelHelper().ToDictionary(eoverdracht);
            } 

            try
            {
                // TODO: The fact that we use Activator.CreateInstance and pass arguments as an object array
                // totally defeats the point of generics and makes this code overly complex - PP
                var epdinstance = (IEPDEndpoint<EOverdracht>)Activator.CreateInstance(epdtype, new object[] { endpointconfiguration });
                eoverdracht = epdinstance.Get(epdRequest);
            }
            catch(Exception ex)
            {
                eoverdracht.ResultString = ex.Message;
            }

            if (!string.IsNullOrEmpty(eoverdracht.ResultString))
            {
                if (eoverdracht.ResultString.ToLower().Contains("geen overdracht gevonden"))
                {
                    Logger.Info($"GetEOverdracht() INFO: [{eoverdracht.ResultString}], epdRequest [{epdRequest.ToString()}] ");
                }
                else
                {
                    Logger.Error($"GetEOverdracht() ERROR: [{eoverdracht.ResultString}], epdRequest [{epdRequest.ToString()}] ");
                }
            }

            return new ModelHelper().ToDictionary(eoverdracht);
        }

        public Dictionary<string, string> GetRequestZHVVT(int organizationID, EPDRequest epdRequest)
        {
            Logger.Info($"GetRequestZHVVT() received: organizationID [{organizationID}], epdRequest [{epdRequest.ToString()}] ");

            RequestZHVVT requestzhvvt = new RequestZHVVT();

            EndpointConfiguration endpointconfiguration = EndpointConfigurationBL.Get(organizationID)
                    .Where(ec => ec.FormTypeID == (int)FlowFormType.AanvraagFormulierZHVVT)
                    .FirstOrDefault();

            var epdendpointaddress = endpointconfiguration?.Address;
            var epdmethodname = endpointconfiguration?.Method;
            if (epdendpointaddress == null || epdmethodname == null)
            {
                requestzhvvt.ResultString = $"EPD address or method is not specified for organizationID {organizationID}";
                return new ModelHelper().ToDictionary(requestzhvvt);
            }

            var epdtype = Type.GetType($"{EPDAssemblyMethods}.{epdmethodname}");
            if (epdtype == null || !typeof(IEPDEndpoint<RequestZHVVT>).IsAssignableFrom(epdtype))
            {
                requestzhvvt.ResultString = $"Incorrect method [{epdmethodname}]";
                return new ModelHelper().ToDictionary(requestzhvvt);
            }

            try
            {
                // TODO: The fact that we use Activator.CreateInstance and pass arguments as an object array
                // totally defeats the point of generics and makes this code overly complex - PP
                var epdinstance = (IEPDEndpoint<RequestZHVVT>)Activator.CreateInstance(epdtype, new object[] { endpointconfiguration });
                requestzhvvt = epdinstance.Get(epdRequest);
            }
            catch (Exception ex)
            {
                requestzhvvt.ResultString = ex.Message;
            }

            if (!string.IsNullOrEmpty(requestzhvvt.ResultString))
            {
                Logger.Error($"GetRequestZHVVT() ERROR: [{requestzhvvt.ResultString}], epdRequest [{epdRequest.ToString()}] ");
            }

            return new ModelHelper().ToDictionary(requestzhvvt);
        }

        private void registerDelegates(int organizationID, int? locationID, string patientNumber)
        {
            var messagehandleinfo = new MessageHandleInfo() { OrganizationID = organizationID, LocationID = locationID, PatientNumber = patientNumber };
            var handler = new EventHandler<MessageHandlerEventArgs>((sender, mhe) => Messagehandler_GotMessage(sender, mhe, messagehandleinfo));

            if (delegates.ContainsKey(messagehandleinfo))
            {
                removeDelegate(messagehandleinfo);
            }
            Logger.Info("Registered for: " + messagehandleinfo.ToString());
            delegates.Add(messagehandleinfo, handler);
            MessageHandler.GotMessage += handler;
        }

        private void removeDelegate(MessageHandleInfo messageHandleInfo)
        {
            var delegatematch = delegates.FirstOrDefault(d => d.Key.OrganizationID == messageHandleInfo.OrganizationID && d.Key.LocationID == messageHandleInfo.LocationID && d.Key.PatientNumber == messageHandleInfo.PatientNumber);

            if (delegatematch.Value != null)
            {
                delegates.Remove(delegatematch.Key);
                MessageHandler.GotMessage -= delegatematch.Value;
            }
        }

        private void Messagehandler_GotMessage(object sender, MessageHandlerEventArgs messageHandlerEventArgs, MessageHandleInfo messageHandleInfo)
        {
            Logger.Debug($"Messagehandler_GotMessage() with messageHandleInfo: {messageHandleInfo}, eventArgs: {messageHandlerEventArgs.MessageHandleInfo}");

            if (messageHandlerEventArgs.MessageHandleInfo.OrganizationID == messageHandleInfo.OrganizationID
                && messageHandlerEventArgs.MessageHandleInfo.LocationID == messageHandleInfo.LocationID
                && messageHandlerEventArgs.MessageHandleInfo.PatientNumber == messageHandleInfo.PatientNumber)
            {
                removeDelegate(messageHandleInfo);
                manualResetEvent.Set();
            }
        }
    }
}
