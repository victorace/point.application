﻿using System;
using System.Globalization;

namespace Point.HL7.Processing.Extensions
{
    public static class StringExtensions
    {
        public static DateTime? ToDateTime(this string source)
        {
            if (string.IsNullOrEmpty(source))
            {
                return null;
            }
            DateTime dt;
            if (!DateTime.TryParseExact(source, new[] { "dd-MM-yyyy HH:mm:ss", "yyyy-MM-dd HH:mm:ss.fff", "yyyy-MM-dd HH:mm:ss", "dd-MM-yyyy", "yyyy-MM-dd" }, CultureInfo.InvariantCulture, DateTimeStyles.None, out dt))
            {
                return null;
            }
            return dt;
        }

        public static string ToDate(this string source)
        {
            DateTime? dt = source.ToDateTime();
            return dt.ToDate();
        }

        public static string Base64Encode(this string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        
        public static string Base64Decode(this string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }

        public static string ElseIfEmpty(this string source, string alternative)
        {
            if (string.IsNullOrEmpty(source))
            {
                return alternative;
            }
            return source;
        }

    }
}
