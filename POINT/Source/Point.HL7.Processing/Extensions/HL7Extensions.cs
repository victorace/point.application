﻿using Point.HL7.Processing.Business;
using Point.HL7.Processing.Models;
using Point.Models.LDM.EOverdracht30.Enums;
using System;
using System.Linq;
using System.Reflection;

namespace Point.HL7.Processing.Extensions
{
    public static class HL7Extensions
    {
        public static string StripChars(this string s, string charsToStrip)
        {
            if (String.IsNullOrEmpty(s))
            {
                return s;
            }

            string retval = s;
            foreach(char c in charsToStrip.ToCharArray())
            {
                retval = retval.Replace(c.ToString(), "");
            }
            return retval;
        }

        public static string TrimChars(this string s, string charsToStrip)
        {
            if (String.IsNullOrEmpty(s))
            {
                return s;
            }

            string retval = s;
            foreach(char c in charsToStrip.ToCharArray())
            {
                retval = retval.Trim(c);
            }
            return retval;
        }

        public static string TrimHL7Empty(this string s)
        {
            return s.TrimChars(" \"");
        }

        public static string SubstringMax(this string s, int length)
        {
            if (String.IsNullOrEmpty(s))
            {
                return s;
            }

            return s.Substring(0, Math.Min(s.Length, length));
        }

        public static string HL7GenderToPointGender(this string hl7gender)
        {
            if (String.IsNullOrEmpty(hl7gender))
            {
                return ((int)Geslacht.OUN).ToString();
            }
            else if (hl7gender.Equals("M", StringComparison.InvariantCultureIgnoreCase) || hl7gender.Equals("Male", StringComparison.InvariantCultureIgnoreCase))
            {
                return ((int)Geslacht.OM).ToString();
            }
            else if (hl7gender.Equals("F", StringComparison.InvariantCultureIgnoreCase) || hl7gender.Equals("V", StringComparison.InvariantCultureIgnoreCase) || hl7gender.Equals("Vrouw", StringComparison.InvariantCultureIgnoreCase))
            {
                return ((int)Geslacht.OF).ToString();
            }
            else
            {
                return ((int)Geslacht.OUN).ToString();
            }
        }

        public static string PointGenderToHL7Gender(this Geslacht pointgender)
        {
            if (pointgender == Geslacht.OM)
            {
                return "M";
            }
            else if (pointgender == Geslacht.OF)
            {
                return "F";
            }
            else
            {
                return "U";
            }
        }

        public static string HL7RelationToPointRelationType(this string relation)
        {
            if (String.IsNullOrEmpty(relation))
            {
                return "";
            }
            else
            {
                return relation.Replace(" van", "");
            }
        }

        public static string HL7CountryToPointCountry(this string country)
        {
            if (country == null)
            {
                return "";
            }
            switch (country.ToUpper())
            {
                case "NL":
                case "NLD":
                    return "Nederland";
                case "BE":
                case "BEL":
                    return "Belgie";
                case "DU":
                case "DUI":
                    return "Duitsland";
            }

            return country;
        }

        public static bool IsNullOrEmptyHL7(this string s)
        {
            return String.IsNullOrEmpty(s) || s.Equals("\"\"") || s.Equals("\"\"\"\"");
        }

        public static string GetNonEmptyValue(params string[] values)
        {
            foreach(string value in values)
            {
                if (!value.IsNullOrEmptyHL7())
                {
                    return value;
                }
            }
            return "";
        }

        public static string ToHL7(this DateTime dt, bool includeMilli = false)
        {
            return ((DateTime?)dt).ToHL7(includeMilli);
        }

        public static string ToHL7(this DateTime? dt, bool includeMilli = false)
        {
            string format = "yyyyMMddHHmmss" + (includeMilli?"fff":"");
            if (dt == null)
            {
                return DateTime.Now.ToString(format);
            }
            else
            {
                return dt.Value.ToString(format);
            }
        }

        public static string ToHL7Short(this DateTime dt)
        {
            return ((DateTime?)dt).ToHL7Short();
        }

        public static string ToHL7Short(this DateTime? dt)
        {
            string format = "yyyyMMdd";
            if (dt == null)
            {
                return "";
            }
            else
            {
                return dt.Value.ToString(format);
            }
        }


        public static string GetInitials(string givenName, string secondAndFurther)
        {
            string initials = "";
            secondAndFurther = secondAndFurther.TrimChars(" ");

            if (!givenName.IsNullOrEmptyHL7())
            {
                if (givenName.IndexOf('.') >= 0 && (givenName.Count(n => n == '.') * 2 == givenName.Length) && secondAndFurther.IsNullOrEmptyHL7())
                {
                    initials += givenName;
                }
                else
                {
                    initials += givenName.Substring(0, 1);
                }
            }
            if (!secondAndFurther.IsNullOrEmptyHL7())
            {
                if (secondAndFurther.Contains(" "))
                {
                    if (secondAndFurther.Split(' ').All(sp => sp.Length >= 2)) // Alle voornamen zijn meegegeven
                    {
                        initials += String.Join("", secondAndFurther.Split(' ').Select(sp => sp[0]));
                    }
                    else
                    {
                        initials += secondAndFurther;
                    }
                }
                else if (secondAndFurther.Contains(".") && initials != "")
                {
                    initials += "." + secondAndFurther;
                }
                else
                {
                    initials += secondAndFurther;
                }
            }

            return initials.StripChars(" \"");
        }

        public static void CleanValues(this HL7Client hl7ClientBO)
        {
            foreach (PropertyInfo property in hl7ClientBO.GetType().GetProperties())
            {
                if (property.PropertyType == typeof(string))
                {
                    string value = property.GetValue(hl7ClientBO, null) as string;
                    property.SetValue(hl7ClientBO, value.StripChars("\"").TrimChars(" "));
                }

            }
        }

    }
}
