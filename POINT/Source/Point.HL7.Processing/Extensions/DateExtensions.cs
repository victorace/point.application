﻿using System;

namespace Point.HL7.Processing.Extensions
{
    public static class DateExtensions
    {
        private static string defaultformat = "yyyy-MM-dd HH:mm:ss.fff";
        public static bool EqualYMD(this DateTime? dt, DateTime? other)
        {
            if (!dt.HasValue && !other.HasValue)
            {
                return true;
            }

            if ((dt.HasValue && !other.HasValue) || (!dt.HasValue && other.HasValue))
            {
                return false;
            }

            if (dt.Value.Year == other.Value.Year &&
                dt.Value.Month == other.Value.Month &&
                dt.Value.Day == other.Value.Day)
            {
                return true;
            }

            return false;

        }

        public static string ToDate(this DateTime? dt, string format = null)
        {
            if (!dt.HasValue)
            {
                return "";
            }
            return dt.Value.ToDate(format??defaultformat);
        }

        public static string ToDate(this DateTime dt, string format = null)
        {
            return dt.Date.ToString(format??defaultformat);
        }
    }
}
