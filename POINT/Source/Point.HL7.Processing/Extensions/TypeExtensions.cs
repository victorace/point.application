﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Point.HL7.Processing.Extensions
{
    public static class TypeExtensions
    {
        public static void AddRange<T, S>(this Dictionary<T, S> source, Dictionary<T, S> collection)
        {
            if (collection != null)
            {

                foreach (var item in collection)
                {
                    if (!source.ContainsKey(item.Key))
                    {
                        source.Add(item.Key, item.Value);
                    }
                    else
                    {
                        source[item.Key] = item.Value;
                    }
                }
            }
        }

        public static TResult FirstVal<TSource, TResult>(this IEnumerable<TSource> source, Func<TSource, TResult> selector)
        {
            TResult fs = default(TResult);

            if (source != null && source.Any())
            {
                fs = source.Select(selector).FirstOrDefault();
                if (fs == null)
                {
                    fs = default(TResult);
                }
            }
            return fs;
        }

        public static bool NoData<T>(this IEnumerable<T> source)
        {
            return (source == null || !source.Any());
        }

        public static T To<T>(this object value)
        {
            Type t = typeof(T);

            if (t.IsGenericType && t.GetGenericTypeDefinition() == typeof(Nullable<>))
            {
                // Nullable type.

                if (value == null)
                {
                    // you may want to do something different here.
                    return default(T);
                }
                else
                {
                    // Get the type that was made nullable.
                    Type valueType = t.GetGenericArguments()[0];

                    // Convert to the value type.
                    object result = Convert.ChangeType(value, valueType);

                    // Cast the value type to the nullable type.
                    return (T)result;
                }
            }
            else
            {
                // Not nullable.
                if (value == null)
                    return default(T);
                else
                    return (T)Convert.ChangeType(value, typeof(T));
            }
        }

        public static string ViewProperties<T>(this T item)
        {
            string result = "";
            foreach (var prop in item.GetType().GetProperties())
            {
                result += $"{prop.Name}: {prop.GetValue(item, null)}" + Environment.NewLine;
            }
            return result;

        }

        public static object ChangeType(this object value, Type convertToType)
        {
            if (convertToType == null)
            {
                throw new ArgumentNullException(nameof(convertToType));
            }

            // return null if the value is null or DBNull
            if (value == null || value is DBNull)
            {
                return null;
            }

            // non-nullable types, which are not supported by Convert.ChangeType(),
            // unwrap the types to determine the underlying time
            if (convertToType.IsGenericType &&
                convertToType.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
            {
                convertToType = Nullable.GetUnderlyingType(convertToType);
            }

            // deal with conversion to enum types when input is a string
            if (convertToType.IsEnum && value is string)
            {
                return Enum.Parse(convertToType, value as string, true);
            }

            // deal with conversion to enum types when input is a integral primitive
            if (value != null && convertToType.IsEnum && value.GetType().IsPrimitive &&
                !(value is bool) && !(value is char) &&
                !(value is float) && !(value is double))
            {
                return Enum.ToObject(convertToType, value);
            }

            // use Convert.ChangeType() to do all other conversions
            return Convert.ChangeType(value, convertToType, CultureInfo.InvariantCulture);
        }
    }
}
