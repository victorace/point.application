﻿using Point.HL7.Processing.Extensions;
using Point.HL7.Processing.Logic;
using Point.HL7.Processing.Models;
using Point.Log4Net;
using System;
using System.Runtime.Caching;

namespace Point.HL7.Processing.Business
{
    class CacheA08
    {
        private static readonly ILogger Logger = LogManager.GetLogger(typeof(CacheA08));

        public bool HL7ClientWasSendBefore(string interfaceCode, HL7Client hl7Client)
        {
            if (hl7Client == null)
            {
                return true;
            }

            string cachekey = $"A08_HL7Client_{interfaceCode}_{hl7Client.PatientNumber.ElseIfEmpty(hl7Client.CivilServiceNumber)}";
            bool wassendbefore = false;

            if (!(MemoryCache.Default.Get(cachekey) is HL7Client result))
            {
                MemoryCache.Default.Add(cachekey, hl7Client, new DateTimeOffset(DateTime.Now.AddHours(48)));
            }
            else
            {
                if (!hl7Client.EqualsEx(result))
                {
                    MemoryCache.Default.Set(cachekey, hl7Client, new DateTimeOffset(DateTime.Now.AddHours(48)));
                }
                else
                {
                    wassendbefore = true;
                }
            }

            if (wassendbefore)
            {
                Logger.Debug($"A08 Client was sent before for PatientNumber: {hl7Client.PatientNumber}, InterfaceCode: {interfaceCode}");
            }

            return wassendbefore;
        }

        public bool HL7RequestFormWasSendBefore(string interfaceCode, HL7Client hl7Client, HL7RequestForm hl7RequestForm)
        {
            if (hl7RequestForm == null)
            {
                return true;
            }

            string cachekey = $"A08_HL7RequestForm_{interfaceCode}_{hl7Client.PatientNumber.ElseIfEmpty(hl7Client.CivilServiceNumber)}";
            bool wassendbefore = false;

            if (!(MemoryCache.Default.Get(cachekey) is HL7RequestForm result))
            {
                MemoryCache.Default.Add(cachekey, hl7RequestForm, new DateTimeOffset(DateTime.Now.AddHours(48)));
            }
            else
            {
                if (!hl7RequestForm.EqualsEx(result))
                {
                    MemoryCache.Default.Set(cachekey, hl7RequestForm, new DateTimeOffset(DateTime.Now.AddHours(48)));
                }
                else
                {
                    wassendbefore = true;
                }
            }

            if (wassendbefore)
            {
                Logger.Debug($"A08 RequestForm was sent before for PatientNumber: {hl7Client.PatientNumber}, InterfaceCode: {interfaceCode}");
            }

            return wassendbefore;
        }
    }
}
