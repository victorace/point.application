﻿using Point.Database.Extensions;
using Point.Database.Models;
using Point.HL7.Processing.Context;
using Point.HL7.Processing.Extensions;
using Point.HL7.Processing.Models;
using Point.Infrastructure.Extensions;
using Point.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Point.HL7.Processing.Business
{
    public class ClientBL
    {
        public static int[] GetTransferIDsFromSearchValues(int organizationID, string patientNumber)
        {
            var activestatuses = new InviteStatus[] { InviteStatus.Active, InviteStatus.Acknowledged, InviteStatus.Realized };
            int[] transferids = { };
            using (var context = new HL7Context())
            {
                transferids = context.FlowInstanceSearchValues.Where(fisv => fisv.PatientNumber.ToLower() == patientNumber.ToLower() &&
                                                                     fisv.ScopeType != (int)ScopeType.Closed &&
                                                                     fisv.FlowInstanceOrganization.Any(fisvd => activestatuses.Contains(fisvd.InviteStatus) && fisvd.OrganizationID == organizationID))
                                                              .Select(fisv => fisv.TransferID).ToArray();
                return transferids;
            }
        }

        public static Client GetClientFromTransferID(int transferID)
        {
            using (var context = new HL7Context())
            {
                return context.Client.FirstOrDefault(C => C.Transfer.Any(t => t.TransferID == transferID));
            }
        }

        public static async void UpdateClient(HL7Client hl7Client, List<string> clientpropertiesnotupdated, List<string> clientpropertiestoskip)
        {
            var timestamp = DateTime.Now;

            var employee = EmployeeBL.GetHL7Employee();
            foreach (var transferid in GetTransferIDsFromSearchValues(hl7Client.OrganizationID, hl7Client.PatientNumber))
            {
                var client = GetClientFromTransferID(transferid);
                if (client != null)
                {
                    using (var context = new HL7Context())
                    {
                        context.Client.Add(client);

                        bool modified = false;
                        bool clientmodified = false;

                        Merge(client, hl7Client, clientpropertiesnotupdated, clientpropertiestoskip, ref modified, ref clientmodified);

                        if (clientmodified || modified)
                        {
                            if (clientmodified)
                            {
                                client.ClientModifiedBy = employee.EmployeeID;
                                client.ClientModifiedDate = timestamp;
                                context.Entry(client).State = System.Data.Entity.EntityState.Modified;
                            }

                            if (modified)
                            {
                                var flowinstancesearchvalues = context.FlowInstanceSearchValues.FirstOrDefault(fisv => fisv.TransferID == transferid && fisv.PatientNumber.ToLower() == hl7Client.PatientNumber.ToLower() && fisv.ScopeType != (int)ScopeType.Closed);
                                if (flowinstancesearchvalues != null)
                                {
                                    flowinstancesearchvalues.ClientCivilServiceNumber = client.CivilServiceNumber;
                                    flowinstancesearchvalues.ClientBirthDate = client.BirthDate.Value;
                                    flowinstancesearchvalues.ClientGender = client.Gender.GetDescription();
                                    flowinstancesearchvalues.ClientFullname = client.FullName();
                                    context.Entry(flowinstancesearchvalues).State = System.Data.Entity.EntityState.Modified;
                                }
                            }

                            await context.SaveChangesAsync();
                        }
                    }
                }
            }
        }

        public static void Merge<TTarget>(TTarget target, object copyFrom, List<string> clientpropertiesnotupdated, 
            List<string> clientpropertiestoskip, ref bool modified, ref bool clientmodified) where TTarget : new()
        {
            var flags = BindingFlags.Instance | BindingFlags.Public;

            // TODO: Move to OrganizationSetting - PP
            clientpropertiesnotupdated.AddRange(new[] { "CivilServiceNumber", "PatientNumber", "VisitNumber" });
            var clientprops = new[] { "FirstName", "MiddleName", "LastName", "CivilServiceNumber", "BirthDate", "Gender"};

            var targetprops = typeof(TTarget).GetProperties(flags).ToDictionary(f => f.Name);
            foreach (PropertyInfo propinfo in copyFrom.GetType().GetProperties(flags))
            {
                if (clientpropertiesnotupdated.Contains(propinfo.Name) && (propinfo.GetValue(copyFrom) == null || propinfo.GetValue(copyFrom).ToString().Trim() == ""))
                {
                    continue;
                }

                if (clientpropertiestoskip.Contains(propinfo.Name))
                {
                    continue;
                }

                if (targetprops.ContainsKey(propinfo.Name) && propinfo.CanWrite)
                {
                    object value = propinfo.GetValue(copyFrom);

                    if (targetprops[propinfo.Name].GetValue(target) != null && value != null
                         && !targetprops[propinfo.Name].GetValue(target).Equals(value))
                    {
                        // TODO: This doesn't work as expected. Mapped values are always
                        // different (Gender for example). Giving us a false 
                        // positive for modified.
                        modified = true;
                        if (clientprops.Contains(propinfo.Name))
                        {
                            clientmodified = true;
                        }
                    }

                    var mappedValue = FieldReceivedValueMappingBL.MapValue(propinfo.Name, value?.ToString());
                    if (mappedValue != null)
                    {
                        value = mappedValue.TargetValue;
                    }
                    var typedvalue = value.ChangeType(targetprops[propinfo.Name].PropertyType);
                    targetprops[propinfo.Name].SetValue(target, typedvalue);
                }
            }
        }
    }
}