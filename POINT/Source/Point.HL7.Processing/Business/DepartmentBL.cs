﻿using Point.HL7.Processing.Context;
using Point.HL7.Processing.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Threading.Tasks;

namespace Point.HL7.Processing.Business
{
    public class DepartmentBL
    {
        //Get a list of extern departments, cache it for 1 / 24 hour

        //Change department
        public static IEnumerable<OrgaLocaDep> GetOrgaLocaDeps(bool useCache = true)
        {
            IEnumerable<OrgaLocaDep> orgalocadeps = null;
            string cachekey = "OrgaLocaDeps";

            if (useCache)
            {
                orgalocadeps = MemoryCache.Default.Get(cachekey) as IEnumerable<OrgaLocaDep>;
            }
            if (orgalocadeps == null)
            {
                using (var context = new HL7Context())
                {
                    orgalocadeps = (from dep in context.Department.Where(dep => !String.IsNullOrEmpty(dep.ExternID) && !dep.Inactive)
                                    join loc in context.Location.Where(loc => !loc.Inactive) on dep.LocationID equals loc.LocationID
                                    join orga in context.Organization.Where(orga => !orga.Inactive) on loc.OrganizationID equals orga.OrganizationID
                                    select new OrgaLocaDep
                                    {
                                        DepartmentExternID = dep.ExternID,
                                        DepartmentID = dep.DepartmentID,
                                        DepartmentName = dep.Name,
                                        LocationID = loc.LocationID,
                                        LocationName = loc.Name,
                                        OrganizationID = orga.OrganizationID,
                                        OrganizationName = orga.Name
                                    }).ToList();
                }
                if (orgalocadeps != null && orgalocadeps.Any())
                {
                    MemoryCache.Default.Set(cachekey, orgalocadeps, new DateTimeOffset(DateTime.Now.AddHours(24)));
                }
            }
            return orgalocadeps;
        }


        public static OrgaLocaDep GetByExternID(int organizationID, string externID)
        {
            return GetOrgaLocaDeps().FirstOrDefault(old => old.OrganizationID == organizationID && old.DepartmentExternID.Equals(externID, StringComparison.InvariantCultureIgnoreCase));
        }

        
    }
}
