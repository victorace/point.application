﻿using Point.Database.Models;
using Point.HL7.Processing.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;

namespace Point.HL7.Processing.Business
{
    // TODO: This whole namespace should be merged to Point.Business.Logic - PP
    public static class FieldReceivedValueMappingBL
    {
        public static IEnumerable<FieldReceivedValueMapping> GetFieldReceivedValueMapping(bool useCache = true)
        {
            IEnumerable<FieldReceivedValueMapping> fieldreceivedvaluemapping = null;
            string cachekey = "FieldReceivedValueMapping";

            if (useCache)
            {
                fieldreceivedvaluemapping = MemoryCache.Default.Get(cachekey) as IEnumerable<FieldReceivedValueMapping>;
            }

            if (fieldreceivedvaluemapping == null)
            {
                using (var context = new HL7Context())
                {
                    fieldreceivedvaluemapping = context.FieldReceivedValueMapping.ToList();
                }
                if (fieldreceivedvaluemapping.Any())
                {
                    MemoryCache.Default.Set(cachekey, fieldreceivedvaluemapping, new DateTimeOffset(DateTime.Now.AddHours(24)));
                }
            }
            return fieldreceivedvaluemapping;
        }

        public static FieldReceivedValueMapping MapValue(string fieldName, string sourceValue)
        {
            var mappings = GetFieldReceivedValueMapping();
            return mappings.FirstOrDefault(frmv =>
                frmv.FieldName.Equals(fieldName, System.StringComparison.InvariantCultureIgnoreCase) &&
                (frmv.SourceValue == sourceValue ||
                !string.IsNullOrEmpty(frmv.SourceValue) && frmv.SourceValue.Equals(sourceValue, System.StringComparison.InvariantCultureIgnoreCase)));
        }
    }
}
