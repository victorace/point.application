﻿using Point.Database.Models;
using Point.HL7.Processing.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;

namespace Point.HL7.Processing.Business
{
    public class OrganizationSettingBL
    {
        public static string InterfaceCode = "InterfaceCode";

        // Skip, no insert or updates
        public static string A19FlowFieldsToSkip = "A19FlowFieldsToSkip";
        public static string A08FlowFieldsToSkip = "A08FlowFieldsToSkip";
        public static string A08ClientPropertiesToSkip = "A08ClientPropertiesToSkip";

        // Not updated if an empty string or no value is received
        public static string A08FlowFieldsNotUpdated = "A08FlowFieldsNotUpdated";
        public static string A08ClientPropertiesNotUpdated = "A08ClientPropertiesNotUpdated";

        public static string SkipQRYAssigningAuthority = "SkipQRYAssigningAuthority";
        public static string InterfaceExtraFields = "InterfaceExtraFields"; //Is used by POINT to _not_ import the fields to AF

        public static IEnumerable<OrganizationSetting> GetOrganizationSettings(bool useCache = true)
        {
            IEnumerable<OrganizationSetting> organizationsettings = null;
            string cachekey = "OrganizationSettings";

            string[] settings = {
                InterfaceCode,
                A19FlowFieldsToSkip,
                A08FlowFieldsNotUpdated,
                A08FlowFieldsToSkip,
                A08ClientPropertiesNotUpdated,
                A08ClientPropertiesToSkip,
                SkipQRYAssigningAuthority
            };

            if (useCache)
            {
                organizationsettings = MemoryCache.Default.Get(cachekey) as IEnumerable<OrganizationSetting>;
            }

            if (organizationsettings == null)
            {
                using (var context = new HL7Context())
                {
                    organizationsettings = context.OrganizationSetting.Where(os => settings.Contains(os.Name)).ToList(); 
                }
                if (organizationsettings.Any())
                {
                    MemoryCache.Default.Set(cachekey, organizationsettings, new DateTimeOffset(DateTime.Now.AddHours(24)));
                }
            }
            return organizationsettings;
        }

        public static OrganizationSetting GetByOrganizationIDAndName(int organizationID, string name)
        {
            var organizationsetting = GetOrganizationSettings().FirstOrDefault(os => os.OrganizationID == organizationID && os.Name == name);
            if (organizationsetting == null)
            {
                organizationsetting = new OrganizationSetting();
            }

            return organizationsetting;
        }

        public static string GetValueByOrganizationIDAndName(int organizationID, string name)
        {
            return GetByOrganizationIDAndName(organizationID, name)?.Value;
        }

        public static OrganizationSetting GetOrganizationSettingByInterfaceCode(string interfaceCode)
        {
            var organizationsetting = GetOrganizationSettings().FirstOrDefault(os => os.Name == InterfaceCode && os.Value == interfaceCode);
            if (organizationsetting == null)
            {
                organizationsetting = new OrganizationSetting();
            }
            return organizationsetting;
        }

        public static int? GetLocationIDByInterfaceCode(string interfaceCode)
        {
            var organizationsetting = GetOrganizationSettings().FirstOrDefault(os => os.Name == InterfaceCode && os.Value == interfaceCode);
            if (organizationsetting == null)
            {
                return -1;
            }
            return organizationsetting.LocationID;
        }

        public static int GetOrganizationIDByInterfaceCode(string interfaceCode)
        {
            var organizationsetting = GetOrganizationSettings().FirstOrDefault(os => os.Name == InterfaceCode && os.Value == interfaceCode);
            if (organizationsetting == null)
            {
                return -1;
            }
            return organizationsetting.OrganizationID;
        }

        public static string GetInterfaceCodeByLocationID(int locationID)
        {
            var organizationsetting = GetOrganizationSettings().FirstOrDefault(os => os.Name == InterfaceCode && os.LocationID == locationID);
            if (organizationsetting == null)
            {
                return null;
            }
            return organizationsetting.Value;
        }

        public static string GetInterfaceCodeByOrganizationID(int organizationID)
        {
            var organizationsetting = GetOrganizationSettings().FirstOrDefault(os => os.Name == InterfaceCode && os.OrganizationID == organizationID);
            if (organizationsetting == null)
            {
                return null;
            }
            return organizationsetting.Value;
        }

        public static IEnumerable<string> GetAllInterfaceCodes()
        {
            return GetOrganizationSettings().Where(os => os.Name == InterfaceCode).Select(os => os.Value);
        }

        public static List<string> GetA08FlowFieldsNotUpdatedByOrganizationID(int organizationID)
        {
            var organizationsetting = GetOrganizationSettings().FirstOrDefault(os => os.Name == A08FlowFieldsNotUpdated && os.OrganizationID == organizationID);
            if (organizationsetting == null)
            {
                return new List<string>();
            }

            return organizationsetting.Value.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries).ToList();
        }

        public static List<string> GetA08ClientPropertiesNotUpdatedByOrganizationID(int organizationID)
        {
            var organizationsetting = GetOrganizationSettings().FirstOrDefault(os => os.Name == A08ClientPropertiesNotUpdated && os.OrganizationID == organizationID);
            if (organizationsetting == null)
            {
                return new List<string>();
            }

            return organizationsetting.Value.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries).ToList();
        }

        public static List<string> GetA19FlowFieldsToSkipByOrganizationID(int organizationID)
        {
            var organizationsetting = GetOrganizationSettings().FirstOrDefault(os => os.Name == A19FlowFieldsToSkip && os.OrganizationID == organizationID);
            if (organizationsetting == null)
            {
                return new List<string>();
            }

            return organizationsetting.Value.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries).ToList();
        }

        public static List<string> GetA08FlowFieldsToSkipByOrganizationID(int organizationID)
        {
            var organizationsetting = GetOrganizationSettings().FirstOrDefault(os => os.Name == A08FlowFieldsToSkip && os.OrganizationID == organizationID);
            if (organizationsetting == null)
            {
                return new List<string>();
            }

            return organizationsetting.Value.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries).ToList();
        }

        public static List<string> GetA08ClientPropertiesToSkipByOrganizationID(int organizationID)
        {
            var organizationsetting = GetOrganizationSettings().FirstOrDefault(os => os.Name == A08ClientPropertiesToSkip && os.OrganizationID == organizationID);
            if (organizationsetting == null)
            {
                return new List<string>();
            }

            return organizationsetting.Value.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries).ToList();
        }

        public static bool GetSkipQRYAssigningAuthority(int organizationID)
        {
            var organizationsetting = GetOrganizationSettings().FirstOrDefault(os => os.Name == SkipQRYAssigningAuthority && os.OrganizationID == organizationID);
            if (organizationsetting == null)
            {
                return false;
            }

            return string.Equals("True", organizationsetting?.Value, StringComparison.InvariantCultureIgnoreCase);
        }
    }
}