﻿using Point.Database.Models;
using Point.HL7.Processing.Context;
using System.Collections.Generic;
using System.Linq;

// TODO: this whole namespace should be merged to Point.Business.Logic - PP
namespace Point.HL7.Processing.Business
{
    public static class EndpointConfigurationBL
    {
        public static IEnumerable<EndpointConfiguration> Get()
        {
            var endpointconfigurations = Enumerable.Empty<EndpointConfiguration>();
            using (var context = new HL7Context())
            {
                endpointconfigurations = context.EndpointConfigurations
                    .Include("FormType")
                    .Where(ec => !ec.Inactive)
                    .ToList();
            }
            return endpointconfigurations;
        }

        public static IEnumerable<EndpointConfiguration> Get(int organizationid)
        {
            var endpointconfigurations = Enumerable.Empty<EndpointConfiguration>();
            using (var context = new HL7Context())
            {
                endpointconfigurations = context.EndpointConfigurations
                    .Where(ec => ec.OrganizationID == organizationid && !ec.Inactive)
                    .ToList();
            }
            return endpointconfigurations;
        }
    }
}
