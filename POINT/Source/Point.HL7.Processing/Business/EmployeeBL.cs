﻿using Point.Database.Models;
using Point.HL7.Processing.Context;
using Point.Log4Net;
using System;
using System.Configuration;
using System.Linq;
using System.Runtime.Caching;

namespace Point.HL7.Processing.Business
{
    class EmployeeBL
    {
        private static readonly ILogger Logger = LogManager.GetLogger(typeof(EmployeeBL));

        public static Employee GetHL7Employee(bool useCache = true)
        {
            string cachekey = $"HL7Employee";
            Employee employee = null;

            if (useCache)
            {
                employee = MemoryCache.Default.Get(cachekey) as Employee;
            }

            if (employee == null)
            {
                using (var context = new HL7Context())
                {
                    Int32.TryParse(ConfigurationManager.AppSettings["HL7EmployeeID"] ?? "-1", out int employeeid);
                    employee = context.Employee.FirstOrDefault(emp => emp.EmployeeID == employeeid);
                }
                if (employee != null)
                {
                    MemoryCache.Default.Set(cachekey, employee, new DateTimeOffset(DateTime.Now.AddHours(24)));
                }
                else
                {
                    Logger.Error($"No employee found for appsetting 'HL7EmployeeID': [{ConfigurationManager.AppSettings["HL7EmployeeID"]}]");
                }
            }
            return employee;
        }
    }
}
