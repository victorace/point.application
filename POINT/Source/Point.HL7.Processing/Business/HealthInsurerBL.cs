﻿using Point.Database.Models;
using Point.HL7.Processing.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;

namespace Point.HL7.Processing.Business
{
    public class HealthInsurerBL
    {
        public static IEnumerable<HealthInsurer> GetHealthInsurers(bool useCache = true)
        {
            IEnumerable<HealthInsurer> healthinsurers = null;
            string cachekey = "HealthInsurer";

            if (useCache)
            {
                healthinsurers = MemoryCache.Default.Get(cachekey) as IEnumerable<HealthInsurer>;
            }

            if (healthinsurers == null)
            {
                using (var context = new HL7Context())
                {
                    healthinsurers = context.HealthInsurer.ToList();
                }
                if (healthinsurers.Any())
                {
                    MemoryCache.Default.Set(cachekey, healthinsurers, new DateTimeOffset(DateTime.Now.AddHours(24)));
                }
            }
            return healthinsurers;
        }

        public static int GetHealthInsurerIDByUZOVI(string uzovi)
        {
            var healthinsurer = GetHealthInsurers().FirstOrDefault(hi => hi.UZOVI.ToString() == uzovi);

            if (healthinsurer == null)
            {
                healthinsurer = GetHealthInsurers().FirstOrDefault(hi => hi.UZOVI.ToString() == uzovi.TrimStart('0'));
            }
            if (healthinsurer == null)
            {
                return -1;
            }
            return healthinsurer.HealthInsurerID;
        }

        public static int GetUZOVIByHealthInsurerID(int healthInsurerID)
        {
            var healthinsurer = GetHealthInsurers().FirstOrDefault(hi => hi.HealthInsurerID == healthInsurerID);
            if (healthinsurer == null)
            {
                return -1;
            }
            return (int)healthinsurer.UZOVI;
        }

    }
}
