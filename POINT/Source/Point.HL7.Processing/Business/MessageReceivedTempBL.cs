﻿using Point.Database.Models;
using Point.HL7.Processing.Context;
using Point.HL7.Processing.Models;
using Point.Models.Enums;
using System;

namespace Point.HL7.Processing.Business
{
    public class MessageReceivedTempBL
    {
        public static MessageReceivedTemp Save(HL7HeaderInfo hl7HeaderInfo, HL7Client hl7Client)
        {
            var messagereceivedtemp = new MessageReceivedTemp()
            {
                GUID = Guid.NewGuid(),
                Hash = null,
                MessageID = hl7HeaderInfo.MessageHeaderControlID,
                OrganisationID = hl7Client.OrganizationID,
                PatientNumber = hl7Client.PatientNumber,
                VisitNumber = hl7Client.VisitNumber,
                SendDate = hl7HeaderInfo.DateTimeOfMessage,
                SenderUserName = "",
                Status = null,
                TimeStamp = DateTime.Now,
                Type = hl7HeaderInfo.TriggerEvent,
                Source = Source.HL7
            };

            using (var context = new HL7Context())
            {
                context.MessageReceivedTemp.Add(messagereceivedtemp);
                context.SaveChanges();
            }

            return messagereceivedtemp;
        }
    }
}
