﻿using Point.Database.Extensions;
using Point.Database.Models;
using Point.HL7.Processing.Context;
using Point.HL7.Processing.Extensions;
using Point.HL7.Processing.Models;
using Point.Log4Net;
using Point.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Threading.Tasks;

namespace Point.HL7.Processing.Business
{
    public class RequestFormBL
    {
        private static readonly ILogger Logger = LogManager.GetLogger(typeof(RequestFormBL));

        private static int[] flowfieldids = new[] {
            FlowFieldConstants.ID_BehandelaarNaam,
            FlowFieldConstants.ID_DatumEindeBehandelingMedischSpecialist,
            FlowFieldConstants.ID_Kamernummer,
            FlowFieldConstants.ID_MedischeSituatieDatumOpname,
            FlowFieldConstants.ID_MedischeSituatieRedenOpname,
            FlowFieldConstants.ID_SourceOrganizationDepartment,
            FlowFieldConstants.ID_SourceOrganizationLocation
        };

        private static int generalActionRelocateDepartment = 28;

        private static IEnumerable<FlowFieldAttribute> GetFlowFieldAttributes(bool useCache = true)
        {
            IEnumerable<FlowFieldAttribute> flowfieldattributes = null;
            string cachekey = "FlowFieldAttributes";

            if (useCache)
            {
                flowfieldattributes = MemoryCache.Default.Get(cachekey) as IEnumerable<FlowFieldAttribute>;
            }

            if (flowfieldattributes == null)
            {
                using (var context = new HL7Context())
                {
                    flowfieldattributes = context.FlowFieldAttribute.Where(ffa => flowfieldids.Contains(ffa.FlowFieldID) && ffa.FormType.PhaseDefinition.Any(pd => pd.FlowDefinitionID == FlowDefinitionID.ZH_VVT)).ToList(); 
                }
                if (flowfieldattributes.Any())
                {
                    MemoryCache.Default.Set(cachekey, flowfieldattributes, new DateTimeOffset(DateTime.Now.AddHours(24)));
                }
            }
            return flowfieldattributes;
        }


        public static void HandleHL7RequestForm(HL7HeaderInfo hl7HeaderInfo, HL7Client hl7Client, HL7RequestForm hl7RequestForm, List<string> flowfieldsnotupdated, List<string> flowfieldstoskip)
        {
            var transferids = ClientBL.GetTransferIDsFromSearchValues(hl7Client.OrganizationID, hl7Client.PatientNumber);
            var hl7employee = EmployeeBL.GetHL7Employee();

            if (transferids?.Length == 0)
            {
                return; // Not saving data to the temp-tables or AF-forms if the client doesn't exist in our system
            }

            bool didupdate = false;
            using (var context = new HL7Context())
            {
                foreach (var transferid in transferids)
                {
                    var client = ClientBL.GetClientFromTransferID(transferid);
                    if (string.IsNullOrEmpty(hl7Client.VisitNumber) || string.IsNullOrEmpty(client.VisitNumber) || hl7Client.VisitNumber == client.VisitNumber)
                    {
                        var formsetversion = context.FormSetVersion.FirstOrDefault(fsv => fsv.TransferID == transferid && fsv.FormType.TypeID == (int)TypeID.FlowAanvraag && fsv.Deleted == false);
                        if (formsetversion != null)
                        {
                            var flowfieldvalues = context.FlowFieldValue.Where(ffv => ffv.FormSetVersionID == formsetversion.FormSetVersionID && flowfieldids.Contains(ffv.FlowFieldID));
                            var flowinstance = context.FlowInstance.FirstOrDefault(fi => fi.TransferID == transferid && !fi.Deleted);

                            var merger = new Merger() {
                                Context = context,
                                OrganizationID = hl7Client.OrganizationID,
                                FlowFieldsNotUpdated = flowfieldsnotupdated,
                                FlowFieldsToSkip = flowfieldstoskip
                            };
                            merger.UpdateFlowFieldValues(hl7RequestForm, flowfieldvalues);

                            if (merger.HaveChanges())
                            {
                                foreach (var changedflowfieldvalue in merger.ChangedFlowFieldValues)
                                {
                                    foreach(var flowfieldattribute in GetFlowFieldAttributes().Where(ffa => ffa.FlowFieldID == changedflowfieldvalue.FlowFieldID && ffa.OriginCopyToAll && ffa.FormTypeID != (int)TypeID.FlowAanvraag))
                                    {
                                        var targetformsetversion = context.FormSetVersion.FirstOrDefault(fsv => fsv.TransferID == transferid && fsv.FormTypeID == flowfieldattribute.FormTypeID && fsv.Deleted == false);
                                        if (targetformsetversion != null)
                                        {
                                            var targetflowfieldvalue = context.FlowFieldValue.FirstOrDefault(ffv => ffv.FlowFieldID == changedflowfieldvalue.FlowFieldID && ffv.FormSetVersionID == targetformsetversion.FormSetVersionID);
                                            merger.CopyOriginValue(changedflowfieldvalue, targetflowfieldvalue);
                                        }
                                    }
                                }
                            }

                            if (merger.HaveChanges() || merger.HaveDepartmentChanges())
                            {
                                merger.UpdateFormSetVersion(formsetversion);
                                if (merger.HaveChanges())
                                {
                                    SendSignaleringFormType(transferid, formsetversion.FormTypeID, hl7employee.EmployeeID, merger.Changes);
                                }
                                if (merger.HaveDepartmentChanges())
                                {
                                    merger.UpdateFlowInstance(flowinstance);
                                    SendSignaleringGeneralAction(transferid, generalActionRelocateDepartment, hl7employee.EmployeeID, merger.DepartmentChanges);
                                }
                                context.SaveChanges();
                                SendUpdateReadmodel(flowinstance.FlowInstanceID);
                                didupdate = true;
                            }
                        }
                    }
                }
            }

            if (!didupdate)
            {
                FieldReceivedTempBL.SaveReceivedTemp(hl7HeaderInfo, hl7Client, hl7RequestForm, flowfieldstoskip);
            }
        }

        public static void SendSignaleringFormType(int transferID, int formTypeID, int employeeID, List<string> changes)
        {
            try
            {
                Task.Run(async () =>
                    await ServiceBus.Helpers.SendMessage("Signalering", "FlowDefinitionSignal",
                        new ServiceBus.Models.SignaleringMessage()
                        {
                            EmployeeID = employeeID,
                            FormTypeID = formTypeID,
                            Message = String.Join(Environment.NewLine, changes),
                            SignaleringType = SignaleringType.FormSetVersion,
                            TransferID = transferID
                        })
                   );
            }
            catch(Exception ex)
            {
                Logger.Error(ex.Message);
            }
        }

        public static void SendSignaleringGeneralAction(int transferID, int generalActionID, int employeeID, List<string> changes)
        {
            try
            {
                Task.Run(async () =>
                {
                    await ServiceBus.Helpers.SendMessage("Signalering", "GeneralActionSignal",
                        new ServiceBus.Models.SignaleringMessage()
                        {
                            EmployeeID = employeeID,
                            GeneralActionID = generalActionID,
                            Message = String.Join(Environment.NewLine, changes),
                            SignaleringType = SignaleringType.GeneralAction,
                            TransferID = transferID
                        });
                });
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
            }

        }

        public static void SendUpdateReadmodel(int flowInstanceID)
        {
            try
            {
                Task.Run(async () =>
                {
                    await ServiceBus.Helpers.SendMessage("ReadModel", $"UpdateFlowInstanceID{flowInstanceID}",
                        new ServiceBus.Models.UpdateReadModel()
                        {
                            FlowInstanceID = flowInstanceID
                        });
                });

            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
            }
        }

        protected class Merger
        {
            public int OrganizationID { private get; set; }
            public HL7Context Context { private get; set; }
            public List<string> FlowFieldsNotUpdated { get; set; } = new List<string>();
            public List<string> FlowFieldsToSkip { get; set; } = new List<string>();
            private Employee Employee { get; set; }
            private int? newDepartmentID { get; set; } = null;
            public List<string> Changes { get; private set; } = new List<string>();
            public List<FlowFieldValue> ChangedFlowFieldValues { get; private set; } = new List<FlowFieldValue>();
            public bool HaveChanges()
            {
                return (Changes != null && Changes.Count() >= 1);
            }
            public List<string> DepartmentChanges { get; private set; } = new List<string>();
            public bool HaveDepartmentChanges()
            {
                return (DepartmentChanges != null && DepartmentChanges.Count() >= 1);
            }

            public void UpdateFlowFieldValues(HL7RequestForm hL7RequestForm, IEnumerable<FlowFieldValue> flowFieldValues)
            {
                Employee = EmployeeBL.GetHL7Employee();
                compareValues(hL7RequestForm, FlowFieldConstants.Name_BehandelaarNaam, flowFieldValues, FlowFieldConstants.ID_BehandelaarNaam);
                compareValuesDate(hL7RequestForm, FlowFieldConstants.Name_DatumEindeBehandelingMedischSpecialist, flowFieldValues, FlowFieldConstants.ID_DatumEindeBehandelingMedischSpecialist);
                compareValues(hL7RequestForm, FlowFieldConstants.Name_Kamernummer, flowFieldValues, FlowFieldConstants.ID_Kamernummer);
                compareValuesDate(hL7RequestForm, FlowFieldConstants.Name_MedischeSituatieDatumOpname, flowFieldValues, FlowFieldConstants.ID_MedischeSituatieDatumOpname);
                compareValues(hL7RequestForm, FlowFieldConstants.Name_MedischeSituatieRedenOpname, flowFieldValues, FlowFieldConstants.ID_MedischeSituatieRedenOpname);

                compareDepartment(hL7RequestForm, flowFieldValues);
            }

            public void UpdateFormSetVersion(FormSetVersion formSetVersion)
            {
                formSetVersion.UpdatedByID = Employee?.EmployeeID;
                formSetVersion.UpdateDate = DateTime.Now;
                formSetVersion.UpdatedBy = Employee.FullName();
                Context.Entry(formSetVersion).State = System.Data.Entity.EntityState.Modified;
            }

            public void CopyOriginValue(FlowFieldValue originFlowFieldValue, FlowFieldValue targetFlowFieldValue)
            {
                if (originFlowFieldValue == null || targetFlowFieldValue == null)
                {
                    return;
                }
                fillFlowFieldValue(targetFlowFieldValue, originFlowFieldValue.Value);
            }

            private void compareValues(HL7RequestForm hL7RequestForm, string requestFormFieldName, IEnumerable<FlowFieldValue> flowFieldValues, int flowFieldID)
            {
                var hl7field = hL7RequestForm.GetField(requestFormFieldName);
                var flowfieldvalue = flowFieldValues.FirstOrDefault(ffv => ffv.FlowFieldID == flowFieldID);

                if (flowfieldvalue != null && hl7field != null && 
                    (string.IsNullOrEmpty(flowfieldvalue.Value) || !flowfieldvalue.Value.Equals(hl7field.FieldValue)))
                {
                    if (FlowFieldsNotUpdated.Contains(requestFormFieldName) && hl7field.FieldValue.IsNullOrEmptyHL7())
                    {
                        return;
                    }
                    if (FlowFieldsToSkip.Contains(requestFormFieldName))
                    {
                        return;
                    }

                    fillFlowFieldValue(flowfieldvalue, hl7field.FieldValue);
                    Changes.Add($"'{hl7field.GetLabel()}' is aangepast");
                    ChangedFlowFieldValues.Add(flowfieldvalue);
                }
            }

            private void compareValuesDate(HL7RequestForm hL7RequestForm, string requestFormFieldName, IEnumerable<FlowFieldValue> flowFieldValues, int flowFieldID)
            {
                var hl7field = hL7RequestForm.GetField(requestFormFieldName);
                var flowfieldvalue = flowFieldValues.FirstOrDefault(ffv => ffv.FlowFieldID == flowFieldID);

                if (flowfieldvalue != null && hl7field != null && 
                    (string.IsNullOrEmpty(flowfieldvalue.Value) || !flowfieldvalue.Value.ToDateTime().EqualYMD(hl7field.FieldValue.ToDateTime())))
                {
                    if (FlowFieldsNotUpdated.Contains(requestFormFieldName) && hl7field.FieldValue.IsNullOrEmptyHL7())
                    {
                        return;
                    }
                    if (FlowFieldsToSkip.Contains(requestFormFieldName))
                    {
                        return;
                    }

                    fillFlowFieldValue(flowfieldvalue, hl7field.FieldValue.ToDate());
                    Changes.Add($"'{hl7field.GetLabel()}' is aangepast");
                    ChangedFlowFieldValues.Add(flowfieldvalue);
                }
            }

            private void compareDepartment(HL7RequestForm hL7RequestForm, IEnumerable<FlowFieldValue> flowfieldValues) 
            {
                var hl7field = hL7RequestForm.GetField(FlowFieldConstants.Name_SourceOrganizationDepartment);
                var flowfieldvalue = flowfieldValues.FirstOrDefault(ffv => ffv.FlowFieldID == FlowFieldConstants.ID_SourceOrganizationDepartment);

                if (flowfieldvalue != null && hl7field != null && !hl7field.FieldValue.IsNullOrEmptyHL7())
                {
                    var orgalocadep = DepartmentBL.GetByExternID(OrganizationID, hl7field.FieldValue);
                    if (orgalocadep != null && orgalocadep.DepartmentID.ToString() != flowfieldvalue.Value)
                    {
                        newDepartmentID = orgalocadep.DepartmentID;
                        fillFlowFieldValue(flowfieldvalue, orgalocadep.DepartmentID.ToString());

                        var flowfieldvalueloc = flowfieldValues.FirstOrDefault(ffv => ffv.FlowFieldID == FlowFieldConstants.ID_SourceOrganizationLocation);
                        fillFlowFieldValue(flowfieldvalueloc, orgalocadep.LocationID.ToString());

                        DepartmentChanges.Add($"'{hl7field.GetLabel()}' is aangepast");
                    }
                }
            }

            private void fillFlowFieldValue(FlowFieldValue flowFieldValue, string value)
            {
                var mutflowfieldvalue = new MutFlowFieldValue()
                {
                    EmployeeID = flowFieldValue.EmployeeID,
                    FlowFieldID = flowFieldValue.FlowFieldID,
                    FlowFieldValueID = flowFieldValue.FlowFieldValueID,
                    FormSetVersionID = flowFieldValue.FormSetVersionID,
                    ScreenName = flowFieldValue.ScreenName,
                    Timestamp = flowFieldValue.Timestamp,
                    Value = flowFieldValue.Value,
                    Source = flowFieldValue.Source
                };
                Context.MutFlowFieldValue.Add(mutflowfieldvalue);

                var employee = EmployeeBL.GetHL7Employee();

                flowFieldValue.Value = value;
                flowFieldValue.EmployeeID = employee?.EmployeeID;
                flowFieldValue.Timestamp = DateTime.Now;
                flowFieldValue.Source = Source.HL7;
                Context.Entry(flowFieldValue).State = System.Data.Entity.EntityState.Modified;
            }

            public void UpdateFlowInstance(FlowInstance flowInstance)
            {
                if (newDepartmentID.HasValue)
                {
                    flowInstance.StartedByDepartmentID = newDepartmentID.Value;
                    Context.Entry(flowInstance).State = System.Data.Entity.EntityState.Modified;
                }
            }
        }
    }
}