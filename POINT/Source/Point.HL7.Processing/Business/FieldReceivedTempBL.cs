﻿using Point.Database.Models;
using Point.HL7.Processing.Context;
using Point.HL7.Processing.Models;
using System;
using System.Collections.Generic;

namespace Point.HL7.Processing.Business
{
    public class FieldReceivedTempBL
    {
        public static void SaveReceivedTemp(HL7HeaderInfo hl7HeaderInfo, HL7Client hl7Client, HL7RequestForm hl7RequestForm, List<string> flowfieldstoskip)
        {
            if (hl7HeaderInfo == null || hl7Client == null || hl7RequestForm == null)
            {
                return;
            }

            var messagereceivedtemp = MessageReceivedTempBL.Save(hl7HeaderInfo, hl7Client);
            save(hl7RequestForm, messagereceivedtemp, flowfieldstoskip);
        }

        private static void save(HL7RequestForm hl7RequestForm, MessageReceivedTemp messageReceivedTemp, List<string> flowfieldstoskip)
        {
            if (hl7RequestForm == null || hl7RequestForm.Fields.Count == 0 || messageReceivedTemp == null)
            {
                return;
            }

            var timestamp = DateTime.Now;

            using (var context = new HL7Context())
            {
                foreach (var field in hl7RequestForm.Fields)
                {
                    if (!flowfieldstoskip.Contains(field.FieldName) && !string.IsNullOrWhiteSpace(field.FieldValue))
                    {
                        var fieldreceivedtemp = new FieldReceivedTemp();
                        fieldreceivedtemp.FieldName = field.FieldName;
                        if (field.FieldType == typeof(DateTime) || field.FieldType == typeof(DateTime?))
                        {
                            fieldreceivedtemp.FieldValue = hl7RequestForm.GetFieldValueDate(field)?.ToString("yyyyMMddHHmmss");
                        }
                        else
                        {
                            fieldreceivedtemp.FieldValue = hl7RequestForm.GetFieldValue(field);
                        }
                        fieldreceivedtemp.MessageReceivedTempID = messageReceivedTemp.MessageReceivedTempID;
                        fieldreceivedtemp.Timestamp = timestamp;
                        fieldreceivedtemp.Status = null;

                        context.FieldReceivedTemp.Add(fieldreceivedtemp);
                    }
                }
                context.SaveChanges();
            }
        }
    }
}
