﻿using Point.Database.Models;
using Point.HL7.Processing.Extensions;
using Point.HL7.Processing.Logic.DataContracts;
using Point.HL7.Processing.Models;
using Point.Log4Net;
using System;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace Point.HL7.Processing.Business.Methods
{
    public class Z_POINT_RequestFormZHVVT : IEPDEndpoint<RequestZHVVT>
    {
        private static readonly ILogger Logger = LogManager.GetLogger(typeof(Z_POINT_RequestFormZHVVT));

        private string username;
        private string password;
        private bool skipcertificatecheck = false;

        public Z_POINT_RequestFormZHVVT(EndpointConfiguration endpointconfiguration)
        {
            if (endpointconfiguration == null)
            {
                throw new ArgumentNullException(nameof(endpointconfiguration));
            }

            EndpointAddress = new EndpointAddress(endpointconfiguration.Address);
            skipcertificatecheck = endpointconfiguration.SkipCertificateCheck;

            Binding = new WSHttpBinding();
            ((WSHttpBinding)Binding).Security.Mode = SecurityMode.Transport;
            ((WSHttpBinding)Binding).Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;

            username = endpointconfiguration.Username;
            password = endpointconfiguration.Password;
        }

        public EndpointAddress EndpointAddress { get; set; }

        public Binding Binding { get; set; }

        public bool ValidConnection()
        {
            bool validconnection = false;
            try
            {
                // 12/20/2018 - known sample dossier by Cerner
                var request = new EPDRequest()
                {
                    PatientNumber = "1000001100",
                    BirthDate = DateTime.Parse("1968-09-29"),
                    VisitNumber = "2000002600"
                };

                Get(request);

                validconnection = true;
            }
            catch (Exception e)
            {
                validconnection = false;
                Logger.Error(e);
            }

            return validconnection;
        }

        public RequestZHVVT Get(EPDRequest epdRequest)
        {
            if (skipcertificatecheck)
            {
                // During development for Cerner they server side certificate issues.
                // This made testing only really possible in ACCEPTATIE and/or PRODUCTIE.
                // This skip flag was added so that in the event of issues; we can quickly
                // disable the certificate checks without having to deploy a new codebase - PP
                System.Net.ServicePointManager.ServerCertificateValidationCallback =
                    delegate (object sender,
                        System.Security.Cryptography.X509Certificates.X509Certificate certificate,
                        System.Security.Cryptography.X509Certificates.X509Chain chain,
                        System.Net.Security.SslPolicyErrors sslPolicyErrors)
                    { return true; };
            }


            z_point_aanvrClient client = new z_point_aanvrClient(Binding, EndpointAddress);
            Z_POINT_AANVRAAG request = new Z_POINT_AANVRAAG()
            {
                I_FALNR = epdRequest.VisitNumber,
                I_GBDAT = epdRequest.BirthDate.Value.ToString("yyyy-MM-dd"),
                I_PATNR = epdRequest.PatientNumber
            };

            client.ClientCredentials.UserName.UserName = username;
            client.ClientCredentials.UserName.Password = password;

            var epd = client.Z_POINT_AANVRAAG(request);

            var requestzhvvt = new RequestZHVVT();

            requestzhvvt.PatientNumber = epd.E_AANVRAAG.PATNR;
            requestzhvvt.VisitNumber = request.I_FALNR; // The response has no VisitNumber field
            requestzhvvt.TimeStamp = DateTime.Now;
            requestzhvvt.ResultString = epd.E_ERROR;

            if (!string.IsNullOrEmpty(epd.E_ERROR))
            {
                return requestzhvvt;
            }

            fillRequestZHVVT(requestzhvvt, epd.E_AANVRAAG);

            return requestzhvvt;
        }

        private void fillRequestZHVVT(RequestZHVVT requestzhvvt, _SHN_POI_S_AANVRAAG aanvraag)
        {
            if (aanvraag == null)
            {
                return;
            }

            requestzhvvt.AanvraagVoor.Value = aanvraag.AANVRAAG_VOOR;
            requestzhvvt.MedischeSituatieRedenOpname = aanvraag.DIAGNOSE_REDEN_OPNAME_AANVRAAG;
            requestzhvvt.ObesitasGewicht = aanvraag.GEWICHT;
            requestzhvvt.ObesitasLengte = aanvraag.LENGTE;
            requestzhvvt.ObesitasBMI = aanvraag.BMI;
            requestzhvvt.BehandelaarNaam = aanvraag.BEHANDELEND_ARTS1;
            requestzhvvt.BehandelaarAGB = aanvraag.AGB_CODE;
            requestzhvvt.BehandelaarSpecialisme.Value = aanvraag.SPECIALISME;
            requestzhvvt.MedischeSituatieDatumOpname = aanvraag.DATUM_OPNAME.ToDateTime();
            requestzhvvt.DatumEindeBehandelingMedischSpecialist = aanvraag.GEWENSTE_ONTSLAGDATUM.ToDateTime();
        }
    }
}