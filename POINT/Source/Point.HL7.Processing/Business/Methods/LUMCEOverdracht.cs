﻿using Point.Database.Models;
using Point.HL7.Processing.Extensions;
using Point.HL7.Processing.Models;
using System;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace Point.HL7.Processing.Business.Methods
{
    public class LUMCEOverdracht : IEPDEndpoint<EOverdracht>
    {
        public LUMCEOverdracht(EndpointConfiguration endpointconfiguration)
        {
            EndpointAddress = new EndpointAddress(endpointconfiguration.Address);

            Binding = new BasicHttpsBinding(BasicHttpsSecurityMode.Transport);
        }

        public EndpointAddress EndpointAddress { get; set; }

        public Binding Binding { get; set; }

        public bool ValidConnection()
        {
            bool validconnection = false;
            try
            {
                IwsPointClient pointClient = new IwsPointClient(Binding, EndpointAddress);
                var epd = pointClient.getPatient("0", "01-01-1900", "V");
                validconnection = true;
            }
            catch
            {
                validconnection = false;
            }

            return validconnection;
        }

        public EOverdracht Get(EPDRequest epdRequest)
        {
            IwsPointClient pointClient = new IwsPointClient(Binding, EndpointAddress);

            //NOTE: We've changed their model! They're expecting a string, but their model states an int.
            var epd = pointClient.getPatient(epdRequest.PatientNumber, epdRequest.BirthDate.Value.ToString("dd-MM-yyyy"), epdRequest.Gender.ToUpper());

            var eoverdracht = new EOverdracht();

            eoverdracht.PatientNumber = epd.patientnummerField;
            eoverdracht.ResultString = epd.errorField;
            eoverdracht.TimeStamp = epd.timestampField.ToDateTime();
            eoverdracht.VisitNumber = epd.opnameNummerField;

            fillMedicalData(eoverdracht, epd.medischeGegevensField);
            fillReading(eoverdracht, epd.meetwaardenField);
            fillMobility(eoverdracht, epd.mobiliteitField);
            fillNeedsHelp(eoverdracht, epd.wassenField, epd.aanEnUitkledenField, epd.toiletgangField, epd.etenDrinkenField);
            fillSkin(eoverdracht, epd.huidField);
            fillPain(eoverdracht, epd.pijnField);
            fillCommunication(eoverdracht, epd.communicatieField);
            fillConviction(eoverdracht, epd.levensovertuigingField);
            fillFood(eoverdracht, epd.voedingField);
            fillSleep(eoverdracht, epd.slaapField);
            fillSecretion(eoverdracht, epd.uitscheidingField);
            fillSenses(eoverdracht, epd.zintuigenField);
            fillOralCare(eoverdracht, epd.mondverzorgingField);
            fillGeneralMentalFunctioning(eoverdracht, epd.algemeneMentaleFunctiesField);

            return eoverdracht;
        }

        private void fillMedicalData(EOverdracht eoverdracht, PointMedischeGegevens[] medischeGegevens)
        {
            if (medischeGegevens.NoData())
            {
                return;
            }

            eoverdracht.BehandelaarNaam = getName(medischeGegevens.FirstVal(f => f.hoofdbehandelaarField).FirstVal(f => f.persoonsnaamField).First());
            eoverdracht.MedischeSituatieRedenOpname = medischeGegevens.FirstVal(f => f.medischeDiagnoseField);
            eoverdracht.BeginKlachten = medischeGegevens.FirstVal(f => f.duurZiekteBehandelingField).FirstVal(f => f.beginKlachtenField);
            eoverdracht.BeginInZorgBehandeling = medischeGegevens.FirstVal(f => f.duurZiekteBehandelingField).FirstVal(f => f.beginZorgBehandelingField).ToDateTime();
            eoverdracht.ToelichtingBeloop = medischeGegevens.FirstVal(f => f.duurZiekteBehandelingField).FirstVal(f => f.toelichtingBeloopField);
            eoverdracht.Prognose = medischeGegevens.FirstVal(f => f.prognoseField);
            eoverdracht.AndereRelevanteMedischeAandoening = medischeGegevens.FirstVal(f => f.andereRelevanteMedischeAandoeningField);
            eoverdracht.AllergieSoortEnReactie = medischeGegevens.FirstVal(f => f.allergienField).FirstVal(f => f.soortEnReactieField);
            eoverdracht.InfectierisicoAanwezig.Value = medischeGegevens.FirstVal(f => f.infectieRisicoField).FirstVal(f => f.aanwezigField);
            eoverdracht.InfectierisicoAanwezigToelichting = medischeGegevens.FirstVal(f => f.infectieRisicoField).FirstVal(f => f.toelichtingField);
        }

        private void fillReading(EOverdracht eoverdracht, PointMeetwaarden[] meetwaarden)
        {
            if (meetwaarden.NoData())
            {
                return;
            }

            eoverdracht.Systole = meetwaarden.FirstVal(f => f.bloeddrukField).FirstVal(f => f.systoleField);
            eoverdracht.Diastole = meetwaarden.FirstVal(f => f.bloeddrukField).FirstVal(f => f.diastoleField);
            eoverdracht.DatumBloeddruk = meetwaarden.FirstVal(f => f.bloeddrukField).FirstVal(f => f.datumBloeddrukField).ToDateTime();
            eoverdracht.BloeddrukToelichting = meetwaarden.FirstVal(f => f.bloeddrukField).FirstVal(f => f.toelichtingField);
            eoverdracht.PolsFrequentie = meetwaarden.FirstVal(f => f.polsField).FirstVal(f => f.frequentieField);
            eoverdracht.DatumFrequentie = meetwaarden.FirstVal(f => f.polsField).FirstVal(f => f.datumFrequentieField).ToDateTime();
            eoverdracht.PolsToelichting = meetwaarden.FirstVal(f => f.polsField).FirstVal(f => f.toelichtingField);
            eoverdracht.Lichaamstemperatuur = meetwaarden.FirstVal(f => f.temperatuurField).FirstVal(f => f.lichaamsTemperatuurField);
            eoverdracht.DatumTemperatuur = meetwaarden.FirstVal(f => f.temperatuurField).FirstVal(f => f.datumTemperatuurField).ToDateTime();
            eoverdracht.TemperatuurToelichting = meetwaarden.FirstVal(f => f.temperatuurField).FirstVal(f => f.toelichtingField);
            eoverdracht.Zuurstofgebruik.Value = meetwaarden.FirstVal(f => f.ademhalingField).FirstVal(f => f.zuurstofgebruikField);
            eoverdracht.DatumZuurstofgebruik = meetwaarden.FirstVal(f => f.ademhalingField).FirstVal(f => f.datumZuurstofgebruikField).ToDateTime();
            eoverdracht.ZuurstofgebruikToelichting = meetwaarden.FirstVal(f => f.ademhalingField).FirstVal(f => f.toelichtingField);
            eoverdracht.Gewicht = meetwaarden.FirstVal(f => f.gewichtField).FirstVal(f => f.lichaamsGewichtField);
            eoverdracht.DatumGewicht = meetwaarden.FirstVal(f => f.gewichtField).FirstVal(f => f.datumGewichtField).ToDateTime();
            eoverdracht.Lengte = meetwaarden.FirstVal(f => f.lengteField).FirstVal(f => f.lichaamsLengteField);
            eoverdracht.DatumLengte = meetwaarden.FirstVal(f => f.lengteField).FirstVal(f => f.datumLengteField).ToDateTime();
            eoverdracht.OverigeMeetwaarden = meetwaarden.FirstVal(f => f.overigeMeetwaardenField);
        }

        private void fillMobility(EOverdracht eoverdracht, PointMobiliteit[] mobiliteit)
        {
            if (mobiliteit.NoData())
            {
                return;
            }

            eoverdracht.MobiliteitVoortbewegenHulpmiddelen = mobiliteit.FirstVal(f => f.voortbewegenField).FirstVal(f => f.hulpmiddelenField);
            eoverdracht.MobiliteitValrisicoAanwezig.Value = mobiliteit.FirstVal(f => f.valrisicoField).FirstVal(f => f.verhoogdValrisicoAanwezigField);
            eoverdracht.MobiliteitValrisicoToelichting = mobiliteit.FirstVal(f => f.valrisicoField).FirstVal(f => f.toelichtingField);
        }

        private void fillNeedsHelp(EOverdracht eoverdracht, PointWassen[] wassen, PointAanEnUitkleden[] aanEnUitkleden, PointToiletgang[] toiletgang, PointEtenDrinken[] etenDrinken)
        {
            eoverdracht.WassenVerpleegkundigeInterventies = wassen.FirstVal(f => f.verpleegkundigeInterventiesField);
            eoverdracht.UitkledenVerpleegkundigeInterventies = aanEnUitkleden.FirstVal(f => f.verpleegkundigeInterventiesField);
            eoverdracht.ToiletgangVerpleegkundigeInterventies = toiletgang.FirstVal(f => f.verpleegkundigeInterventiesField);
            eoverdracht.EtenDrinkenVerpleegkundigeInterventies = etenDrinken.FirstVal(f => f.verpleegkundigeInterventiesField);
        }

        private void fillSkin(EOverdracht eoverdracht, PointHuid[] huid)
        {
            if (huid.NoData())
            {
                return;
            }

            eoverdracht.HuidDecubitusAanwezig.Value = huid.FirstVal(f => f.decubitusField).FirstVal(f => f.aanwezigField);
            eoverdracht.HuidDecubitusBehandelplan = huid.FirstVal(f => f.decubitusField).FirstVal(f => f.behandelplanField);
            eoverdracht.HuidDecubitusCategorie.Value = huid.FirstVal(f => f.decubitusField).FirstVal(f => f.categorieField);
            eoverdracht.HuidDecubitusLocatie = huid.FirstVal(f => f.decubitusField).FirstVal(f => f.locatieField);
            eoverdracht.HuidDecubitusPreventie = huid.FirstVal(f => f.decubitusField).FirstVal(f => f.preventieField);

            eoverdracht.HuidHuidletselAanwezig.Value = huid.FirstVal(f => f.huidletselDoorIncontinentieField).FirstVal(f => f.aanwezigField);
            eoverdracht.HuidHuidletselBehandelplan = huid.FirstVal(f => f.huidletselDoorIncontinentieField).FirstVal(f => f.behandelplanField);
            eoverdracht.HuidHuidletselFaseBeschrijving = huid.FirstVal(f => f.huidletselDoorIncontinentieField).FirstVal(f => f.toelichtingField);
            eoverdracht.HuidHuidletselLocatie = huid.FirstVal(f => f.huidletselDoorIncontinentieField).FirstVal(f => f.locatieField);
            eoverdracht.HuidHuidletselPreventie = huid.FirstVal(f => f.huidletselDoorIncontinentieField).FirstVal(f => f.preventieField);

            eoverdracht.HuidOverigeAanwezig.Value = huid.FirstVal(f => f.overigeHuidaandoeningenField).FirstVal(f => f.aanwezigField);
            eoverdracht.HuidOverigeBehandelplan = huid.FirstVal(f => f.overigeHuidaandoeningenField).FirstVal(f => f.behandelplanField);
            eoverdracht.HuidOverigeLocatie = huid.FirstVal(f => f.overigeHuidaandoeningenField).FirstVal(f => f.locatieField);
            eoverdracht.HuidOverigeSoort = huid.FirstVal(f => f.overigeHuidaandoeningenField).FirstVal(f => f.soortField);

            eoverdracht.HuidWondAanwezig.Value = huid.FirstVal(f => f.wondenField).FirstVal(f => f.aanwezigField);
            eoverdracht.HuidWondBehandeling = huid.FirstVal(f => f.wondenField).FirstVal(f => f.wondField).FirstVal(f => f.behandelplanField);
            eoverdracht.HuidWondLocatie = huid.FirstVal(f => f.wondenField).FirstVal(f => f.wondField).FirstVal(f => f.locatieField);
            eoverdracht.HuidWondSoort.Value = huid.FirstVal(f => f.wondenField).FirstVal(f => f.wondField).FirstVal(f => f.soortField);
        }

        private void fillPain(EOverdracht eoverdracht, PointPijn[] pijn)
        {
            if (pijn.NoData())
            {
                return;
            }

            eoverdracht.PijnBijBeweging.Value = pijn.FirstVal(f => f.pijnBijBewegingField);
            eoverdracht.PijnInRust.Value = pijn.FirstVal(f => f.pijnInRustField);
            eoverdracht.PijnLocatie = pijn.FirstVal(f => f.locatiePijnField);
            eoverdracht.PijnMateVan.Value = pijn.FirstVal(f => f.mateVanPijnField);
            eoverdracht.PijnToelichting = pijn.FirstVal(f => f.toelichtingField);
            eoverdracht.PijnVerpleegkundigeInterventies = pijn.FirstVal(f => f.verpleegkundigeInterventiesField);
        }

        private void fillCommunication(EOverdracht eoverdracht, PointCommunicatie[] communicatie)
        {
            if (communicatie.NoData())
            {
                return;
            }

            eoverdracht.CommunicatieToelichting = communicatie.FirstVal(f => f.toelichtingField);
            eoverdracht.CommunicatieSpreektNederlandse = communicatie.FirstVal(f => f.spreektNederlandseTaalField);
        }

        private void fillConviction(EOverdracht eoverdracht, PointLevensovertuiging[] levensovertuiging)
        {
            if (levensovertuiging.NoData())
            {
                return;
            }

            eoverdracht.LevensOvertuigingReligie = levensovertuiging.FirstVal(f => f.religieField);
        }

        private void fillFood(EOverdracht eoverdracht, PointVoeding[] voeding)
        {
            if (voeding.NoData())
            {
                return;
            }

            var meetmethode = voeding.FirstVal(f => f.ondervoedingField).FirstVal(f => f.meetmehodeField);
            var scoreletter = "";
            if (meetmethode == "MUST")
            {
                scoreletter = "M";
            }
            else if (meetmethode == "SNAQ")
            {
                scoreletter = "S";
            }

            eoverdracht.VoedingOndervoedingAanwezig.Value = voeding.FirstVal(f => f.ondervoedingField).FirstVal(f => f.aanwezigField);
            eoverdracht.VoedingOndervoedingAanwezigMeetmethode.Value = (meetmethode == "MUST" ? "414648004" : meetmethode);
            eoverdracht.VoedingOndervoedingAanwezigScore.Value = String.Concat(scoreletter, voeding.FirstVal(f => f.ondervoedingField).FirstVal(f => f.scoreField));

            eoverdracht.VoedingSprake.Value = voeding.FirstVal(f => f.voedselIntolerantieField).FirstVal(f => f.aanwezigField);
            eoverdracht.VoedingSprakeSoort = voeding.FirstVal(f => f.voedselIntolerantieField).FirstVal(f => f.soortField);

            eoverdracht.VoedingDieetAanwezig.Value = voeding.FirstVal(f => f.dieetField).FirstVal(f => f.aanwezigField);
            eoverdracht.VoedingDieetSoort = voeding.FirstVal(f => f.dieetField).FirstVal(f => f.soortField);

            eoverdracht.VoedingConsistentie = voeding.FirstVal(f => f.consistentieField);

            eoverdracht.VoedingVerpleegkundigeInterventies = voeding.FirstVal(f => f.verpleegkundigeInterventiesField);
        }

        private void fillSleep(EOverdracht eoverdracht, PointSlaap[] slaap)
        {
            if (slaap.NoData())
            {
                return;
            }

            eoverdracht.SlaapInslapen.Value = slaap.FirstVal(f => f.slapenField).FirstVal(f => f.inslapenField);
            eoverdracht.SlaapDoorslapen.Value = slaap.FirstVal(f => f.slapenField).FirstVal(f => f.doorslapenField);
            eoverdracht.SlaapSlaapcyclus.Value = slaap.FirstVal(f => f.slapenField).FirstVal(f => f.slaapcyclusField);
            eoverdracht.SlaapToelichting = slaap.FirstVal(f => f.slapenField).FirstVal(f => f.toelichtingField);
            eoverdracht.SlaapVerpleegkundigeInterventies = slaap.FirstVal(f => f.verpleegkundigeInterventiesField);
        }

        private void fillSecretion(EOverdracht eoverdracht, PointUitscheiding[] uitscheiding)
        {
            if (uitscheiding.NoData())
            {
                return;
            }

            eoverdracht.UitscheidingMictieContinentieSoort = uitscheiding.FirstVal(f => f.mictieField);
            eoverdracht.UitscheidingStomaSoort = uitscheiding.FirstVal(f => f.stomaField);
            eoverdracht.UitscheidingUrostomaToelichting = uitscheiding.FirstVal(f => f.urostomaField);
            eoverdracht.UitscheidingUrineKatheterToelichting = uitscheiding.FirstVal(f => f.urineKatheterField);
            eoverdracht.UitscheidingDefecatieContinentie.Value = uitscheiding.FirstVal(f => f.defecatieField);
            eoverdracht.UitscheidingIncontinentiemateriaal = uitscheiding.FirstVal(f => f.incontinentieMateriaalField);
            eoverdracht.UitscheidingDatumLaatsteMenstruatie = uitscheiding.FirstVal(f => f.datumLaatsteMenstruatieField).ToDateTime();
            eoverdracht.UitscheidingVerpleegkundigeInterventies = uitscheiding.FirstVal(f => f.verpleegkundigeInterventiesField); //stomaField )
        }

        private void fillSenses(EOverdracht eoverdracht, PointZintuigen[] zintuigen)
        {
            if (zintuigen.NoData())
            {
                return;
            }

            eoverdracht.ZintuigenZien.Value = zintuigen.FirstVal(f => f.zienField);
            eoverdracht.ZintuigenZienHulpmiddelen.Value = zintuigen.FirstVal(f => f.hulpmiddelenBijZienField);
            eoverdracht.ZintuigenHoren.Value = zintuigen.FirstVal(f => f.horenField);
            eoverdracht.ZintuigenHorenHulpmiddelen.Value = zintuigen.FirstVal(f => f.hulpmiddelenBijHorenField);
            eoverdracht.ZintuigenOverig = zintuigen.FirstVal(f => f.bijzonderhedenField);
            eoverdracht.ZintuigenVerpleegkundigeInterventies = zintuigen.FirstVal(f => f.verpleegkundigeInterventiesField);
        }

        private void fillOralCare(EOverdracht eoverdracht, PointMondverzorging[] mondverzorging)
        {
            if (mondverzorging.NoData())
            {
                return;
            }

            eoverdracht.MondVerzorgingProtheseAanwezig.Value = mondverzorging.FirstVal(f => f.gebitsprotheseAanwezigField);
            eoverdracht.MondVerzorgingSoortOndersteuningTandverzorging = mondverzorging.FirstVal(f => f.soortOndersteuningField);
        }

        private void fillGeneralMentalFunctioning(EOverdracht eoverdracht, PointAlgemeneMentaleFuncties[] algemeneMentaleFuncties)
        {
            if (algemeneMentaleFuncties.NoData())
            {
                return;
            }

            eoverdracht.AlgemeneMentaleFunctieToelichting = algemeneMentaleFuncties.FirstVal(f => f.toelichtingField);
        }

        private string getName(Persoonsnaam persoonsnaam)
        {
            if (persoonsnaam == null)
            {
                return "";
            }
            Name name = new Name()
            {
                FirstName = persoonsnaam.voornamenField,
                Initials = persoonsnaam.voorlettersField,
                LastName = persoonsnaam.geslachtsnaamField
            };
            return name.FullName();
        }
    }
}