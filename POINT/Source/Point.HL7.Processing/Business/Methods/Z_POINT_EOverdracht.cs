﻿using Point.Database.Models;
using Point.HL7.Processing.Extensions;
using Point.HL7.Processing.Logic.DataContracts;
using Point.HL7.Processing.Models;
using Point.Log4Net;
using System;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace Point.HL7.Processing.Business.Methods
{
    public class Z_POINT_EOverdracht : IEPDEndpoint<EOverdracht>
    {
        private static readonly ILogger Logger = LogManager.GetLogger(typeof(Z_POINT_EOverdracht));

        private string username;
        private string password;
        private bool skipcertificatecheck = false;

        public Z_POINT_EOverdracht(EndpointConfiguration endpointconfiguration)
        {
            if (endpointconfiguration == null)
            {
                throw new ArgumentNullException(nameof(endpointconfiguration));
            }

            EndpointAddress = new EndpointAddress(endpointconfiguration.Address);
            skipcertificatecheck = endpointconfiguration.SkipCertificateCheck;

            Binding = new WSHttpBinding();
            ((WSHttpBinding)Binding).Security.Mode = SecurityMode.Transport;
            ((WSHttpBinding)Binding).Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;

            username = endpointconfiguration.Username;
            password = endpointconfiguration.Password;
        }

        public EndpointAddress EndpointAddress { get; set; }

        public Binding Binding { get; set; }

        public bool ValidConnection()
        {
            bool validconnection = false;
            try
            {
                // 12/20/2018 - known sample dossier by Cerner
                var request = new EPDRequest()
                {
                    PatientNumber = "1000001100",
                    BirthDate = DateTime.Parse("1968-09-29"),
                    VisitNumber = "2000002600"
                };

                Get(request);

                validconnection = true;
            }
            catch (Exception e)
            {
                validconnection = false;
                Logger.Error(e);
            }

            return validconnection;
        }

        public EOverdracht Get(EPDRequest epdRequest)
        {
            if (skipcertificatecheck)
            {
                // During development for Cerner they server side certificate issues.
                // This made testing only really possible in ACCEPTATIE and/or PRODUCTIE.
                // This skip flag was added so that in the event of issues; we can quickly
                // disable the certificate checks without having to deploy a new codebase - PP
                System.Net.ServicePointManager.ServerCertificateValidationCallback =
                    delegate (object sender,
                        System.Security.Cryptography.X509Certificates.X509Certificate certificate,
                        System.Security.Cryptography.X509Certificates.X509Chain chain,
                        System.Net.Security.SslPolicyErrors sslPolicyErrors)
                    { return true; };
            }

            Z_POINT_OVERDRClient client = new Z_POINT_OVERDRClient(Binding, EndpointAddress);
            Z_POINT_OVERDRACHT request = new Z_POINT_OVERDRACHT()
            {
                I_FALNR = epdRequest.VisitNumber,
                I_GBDAT = epdRequest.BirthDate.Value.ToString("yyyy-MM-dd"),
                I_PATNR = epdRequest.PatientNumber
            };

            client.ClientCredentials.UserName.UserName = username;
            client.ClientCredentials.UserName.Password = password;

            var epd = client.Z_POINT_OVERDRACHT(request);

            var eoverdracht = new EOverdracht();

            eoverdracht.PatientNumber = epd.E_OVERDRACHT.PERSOONSGEGEVENS.PATIENTGEGEVENS.PATIENTIDENTIFICATIE;
            eoverdracht.VisitNumber = request.I_FALNR; // The response has no VisitNumber field
            eoverdracht.TimeStamp = DateTime.Now;
            eoverdracht.ResultString = epd.E_ERROR;

            if (!string.IsNullOrEmpty(epd.E_ERROR))
            {
                return eoverdracht;
            }

            fillMedicalData(eoverdracht, epd?.E_OVERDRACHT?.MEDISCHE_GEGEVENS);
            fillCareSummary(eoverdracht, epd?.E_OVERDRACHT.SAMENVATTING_ZORG);
            fillReading(eoverdracht, epd?.E_OVERDRACHT?.MEETWAARDEN);
            fillMobility(eoverdracht, epd?.E_OVERDRACHT?.MOBILITEIT);
            fillNeedsHelp(eoverdracht, epd?.E_OVERDRACHT?.WASSEN, epd?.E_OVERDRACHT?.AAN_EN_UITKLEDEN, epd?.E_OVERDRACHT?.TOILETGANG, epd?.E_OVERDRACHT?.ETEN_DRINKEN);
            fillSkin(eoverdracht, epd?.E_OVERDRACHT?.HUID);
            fillPain(eoverdracht, epd?.E_OVERDRACHT?.PIJN);
            fillPsychologicalFunctioning(eoverdracht, epd?.E_OVERDRACHT?.INTERV_PSYCHISCH_FUNC);
            fillCommunication(eoverdracht, epd?.E_OVERDRACHT?.COMMUNICATIE);
            fillHelpFromOthers(eoverdracht, epd?.E_OVERDRACHT?.HULP_VAN_ANDEREN);
            fillSexuality(eoverdracht, epd?.E_OVERDRACHT?.SEKSUALITEIT_VOORTPL);
            fillCommunity(eoverdracht, epd?.E_OVERDRACHT?.PART_IN_MAATSCHAPPIJ);
            fillIllnessExperience(eoverdracht, epd?.E_OVERDRACHT?.ZIEKTEBELEVING);
            fillConviction(eoverdracht, epd?.E_OVERDRACHT?.LEVENSOVERTUIGING);
            fillFood(eoverdracht, epd?.E_OVERDRACHT?.VOEDING);
            fillAdministrationSystems(eoverdracht, epd?.E_OVERDRACHT?.TOEDIENING_SYSTEMEN);
            fillSleep(eoverdracht, epd?.E_OVERDRACHT?.SLAAP);
            fillSecretion(eoverdracht, epd?.E_OVERDRACHT?.UITSCHEIDING);
            fillSenses(eoverdracht, epd?.E_OVERDRACHT?.ZINTUIGEN);
            fillOralCare(eoverdracht, epd?.E_OVERDRACHT?.MONDVERZORGING);

            return eoverdracht;
        }

        private void fillMedicalData(EOverdracht eoverdracht, _SHN_POI_MEDISCHE_GEGEVENS medischeGegevens)
        {
            if (medischeGegevens ==  null)
            {
                return;
            }

            eoverdracht.BehandelaarNaam = getName(medischeGegevens?.HOOFDBEHANDELAAR?.PERSOONSNAAM);
            eoverdracht.MedischeSituatieRedenOpname = medischeGegevens?.MEDISCHE_DIAGNOSE;
            eoverdracht.BeginInZorgBehandeling = medischeGegevens?.DUUR_ZIEKTE_BEHANDELING?.BEGIN_IN_ZORG_BEHANDELING?.ToDateTime();
            eoverdracht.AllergieSoortEnReactie = medischeGegevens?.ALLERGIE?.SOORT_EN_REACTIE;
            eoverdracht.InfectierisicoAanwezig.Value = medischeGegevens?.INFECTIERISICO?.AANWEZIG;
            eoverdracht.InfectierisicoAanwezigToelichting = medischeGegevens?.INFECTIERISICO?.TOELICHTING;
        }

        private void fillCareSummary(EOverdracht eoverdracht, _SHN_POI_SAMENVATTING_ZORG samenvattingZorg)
        {
            if (samenvattingZorg == null)
            {
                return;
            }

            eoverdracht.RedenInZorg = samenvattingZorg?.SAMENVATTING_ZORGVRAAG.REDEN_IN_ZORG;
        }

        private void fillReading(EOverdracht eoverdracht, _SHN_POI_MEETWAARDEN meetwaarden)
        {
            if (meetwaarden == null)
            {
                return;
            }

            eoverdracht.Systole = meetwaarden?.BLOEDDRUK?.SYSTOLE;
            eoverdracht.Diastole = meetwaarden?.BLOEDDRUK?.DIASTOLE;
            eoverdracht.DatumBloeddruk = meetwaarden?.BLOEDDRUK?.DATUM_BLOEDDRUK?.ToDateTime();
            eoverdracht.BloeddrukToelichting = meetwaarden?.BLOEDDRUK?.TOELICHTING;
            eoverdracht.PolsFrequentie = meetwaarden?.POLS?.FREQUENTIE;
            eoverdracht.DatumFrequentie = meetwaarden?.POLS?.DATUM_FREQUENTIE?.ToDateTime();
            eoverdracht.PolsToelichting = meetwaarden?.POLS?.TOELICHTING;
            eoverdracht.Lichaamstemperatuur = meetwaarden?.TEMPERATUUR?.LICHAAMSTEMPERATUUR;
            eoverdracht.DatumTemperatuur = meetwaarden?.TEMPERATUUR?.DATUM_TEMPERATUUR?.ToDateTime();
            eoverdracht.TemperatuurToelichting = meetwaarden?.TEMPERATUUR?.TOELICHTING;
            eoverdracht.Zuurstofgebruik.Value = meetwaarden?.ADEMHALING?.ZUURSTOFGEBRUIK;
            eoverdracht.DatumZuurstofgebruik = meetwaarden?.ADEMHALING?.DATUM_ZUURSTOFGEBRUIK?.ToDateTime();
            eoverdracht.ZuurstofgebruikToelichting = meetwaarden?.ADEMHALING?.TOELICHTING;
            eoverdracht.Gewicht = meetwaarden?.GEWICHT?.LICHAAMSGEWICHT;
            eoverdracht.DatumGewicht = meetwaarden?.GEWICHT?.DATUM_GEWICHT?.ToDateTime();
            eoverdracht.Lengte = meetwaarden?.LENGTE?.LICHAAMSLENGTE;
            eoverdracht.DatumLengte = meetwaarden?.LENGTE?.DATUM_LENGTE?.ToDateTime();
            eoverdracht.OverigeMeetwaarden = meetwaarden?.OVERIGE_MEETWAARDEN?.MEETWAARDE;
            eoverdracht.DatumMeting = meetwaarden?.OVERIGE_MEETWAARDEN?.DATUM_METING.ToDateTime();
        }

        private void fillMobility(EOverdracht eoverdracht, _SHN_POI_MOBILITEIT mobiliteit)
        {
            if (mobiliteit == null)
            {
                return;
            }

            eoverdracht.MobiliteitVoortbewegenHulpmiddelen = mobiliteit?.VOORTBEWEGEN?.HULPMIDDELEN;
            eoverdracht.MobiliteitValrisicoAanwezig.Value = mobiliteit?.VALRISICO?.VERHOOGD_VALRISICO_AANWEZIG;
            eoverdracht.MobiliteitValrisicoToelichting = mobiliteit?.VALRISICO?.TOELICHTING;
        }

        private void fillNeedsHelp(EOverdracht eoverdracht, _SHN_POI_WASSEN wassen, _SHN_POI_AAN_EN_UITKLEDEN aanEnUitkleden, _SHN_POI_TOILETGANG toiletgang, _SHN_POI_ETEN_DRINKEN etenDrinken)
        {
            eoverdracht.ToiletgangVerpleegkundigeInterventies = toiletgang?.VERPLEEGKUNDIGE_INTERVENTIES;
            eoverdracht.EtenDrinkenVerpleegkundigeInterventies = etenDrinken?.VERPLEEGKUNDIGE_INTERVENTIES;
        }

        private void fillSkin(EOverdracht eoverdracht, _SHN_POI_HUID huid)
        {
            if (huid == null)
            {
                return;
            }

            eoverdracht.HuidDecubitusAanwezig.Value = huid?.DECUBITUS?.AANWEZIG;
            eoverdracht.HuidDecubitusCategorie.Value = huid?.DECUBITUS?.CATEGORIE;
            eoverdracht.HuidDecubitusLocatie = huid?.DECUBITUS?.LOCATIE;

            eoverdracht.HuidHuidletselAanwezig.Value = huid?.HUIDLETSEL_DOOR_INCONTINENTIE?.AANWEZIG;
            eoverdracht.HuidHuidletselLocatie = huid?.HUIDLETSEL_DOOR_INCONTINENTIE?.LOCATIE;

            eoverdracht.HuidOverigeAanwezig.Value = huid?.OVERIGE_HUIDAANDOENINGEN?.AANWEZIG;
            eoverdracht.HuidOverigeBehandelplan = huid?.OVERIGE_HUIDAANDOENINGEN?.BEHANDELPLAN;
            eoverdracht.HuidOverigeLocatie = huid?.OVERIGE_HUIDAANDOENINGEN?.LOCATIE;
            eoverdracht.HuidOverigeSoort = huid?.OVERIGE_HUIDAANDOENINGEN?.SOORT_AANDOENING;

            eoverdracht.HuidWondAanwezig.Value = huid?.WOND?.AANWEZIG;
            eoverdracht.HuidWondBehandeling = huid?.WOND?.BEHANDELING;
            eoverdracht.HuidWondLocatie = huid?.WOND?.LOCATIE;
            eoverdracht.HuidWondSoort.Value = huid?.WOND?.SOORT_WOND;
        }

        private void fillPain(EOverdracht eoverdracht, _SHN_POI_PIJN pijn)
        {
            if (pijn == null)
            {
                return;
            }

            eoverdracht.PijnMateVan.Value = pijn?.MATE_VAN_PIJN;
        }

        private void fillPsychologicalFunctioning(EOverdracht eoverdracht, _SHN_POI_INTERV_PSYCHISCH_FUNC psychischfunctioneren)
        {
            if (psychischfunctioneren == null)
            {
                return;
            }

            eoverdracht.PsychischFunctionerenVrijheidsbeperkendeInterventiesAanwezig.Value = psychischfunctioneren?.VRIJHEIDSBEP_INTERVENTIES?.AANWEZIG;
            eoverdracht.PsychischFunctionerenVrijheidsbeperkendeInterventiesSoort = psychischfunctioneren?.VRIJHEIDSBEP_INTERVENTIES?.SOORT_INTERVENTIE;
        }

        private void fillCommunication(EOverdracht eoverdracht, _SHN_POI_COMMUNICATIE communicatie)
        {
            if (communicatie == null)
            {
                return;
            }

            eoverdracht.CommunicatieSpreektNederlandse = communicatie?.SPREEKT_NEDERLANDSE_TAAL;
        }

        private void fillHelpFromOthers(EOverdracht eoverdracht, _SHN_POI_HULP_VAN_ANDEREN hulpvananderen)
        {
            if (hulpvananderen == null)
            {
                return;
            }

            eoverdracht.HulpVanAnderenMantelzorgAard = hulpvananderen?.MANTELZORG?.AARD;
            eoverdracht.HulpVanAnderenProfessioneleAard = hulpvananderen?.PROFESSIONELE_HULP?.AARD;
            eoverdracht.HulpVanAnderenProfessioneleFrequentie = hulpvananderen?.PROFESSIONELE_HULP?.FREQUENTIE;
            eoverdracht.HulpVanAnderenToelichting = hulpvananderen?.TOELICHTING;
        }

        private void fillSexuality(EOverdracht eoverdracht, _SHN_POI_SEKSUALITEIT_VOORTPL seksualiteitenvoortplanting)
        {
            if (seksualiteitenvoortplanting == null)
            {
                return;
            }

            eoverdracht.SeksualiteitToelichting = seksualiteitenvoortplanting?.TOELICHTING_SEKSUALITEIT;
            eoverdracht.SeksualiteitZwangerschapZwanger.Value = seksualiteitenvoortplanting?.ZWANGERSCHAP?.ZWANGER;
            eoverdracht.SeksualiteitZwangerschapAantalWeken = seksualiteitenvoortplanting?.ZWANGERSCHAP?.AANTAL_WEKEN;
            eoverdracht.SeksualiteitZwangerschapToelichting = seksualiteitenvoortplanting?.ZWANGERSCHAP?.TOELICHTING;
        }

        private void fillCommunity(EOverdracht eoverdracht, _SHN_POI_PART_IN_MAATSCHAPPIJ participatieinmaatschappij)
        {
            if (participatieinmaatschappij == null)
            {
                return;
            }

            eoverdracht.ParticipatieInMaatschappijParticipatieWerkOfBeroep = participatieinmaatschappij?.PARTICIPATIE?.WERK_BEROEP;
            eoverdracht.ParticipatieInMaatschappijParticipatieHobby = participatieinmaatschappij?.PARTICIPATIE?.HOBBY;
            eoverdracht.ParticipatieInMaatschappijParticipatieSociaalNetwerk = participatieinmaatschappij?.PARTICIPATIE?.SOCIAAL_NETWERK;
        }

        private void fillIllnessExperience(EOverdracht eoverdracht, _SHN_POI_ZIEKTEBELEVING ziektebeleving)
        {
            if (ziektebeleving == null)
            {
                return;
            }

            eoverdracht.ZiekteBelevingOmgaanMetZiekteprocesPatient = ziektebeleving?.OMGAAN_MET_ZIEKTEPROCES?.PATIENT;
            eoverdracht.ZiekteBelevingOmgaanMetZiekteprocesNaasten = ziektebeleving?.OMGAAN_MET_ZIEKTEPROCES?.NAASTEN;
        }

        private void fillConviction(EOverdracht eoverdracht, _SHN_POI_LEVENSOVERTUIGING levensovertuiging)
        {
            if (levensovertuiging == null)
            {
                return;
            }

            eoverdracht.LevensOvertuigingReligie = levensovertuiging?.RELIGIE;
        }

        private void fillFood(EOverdracht eoverdracht, _SHN_POI_VOEDING voeding)
        {
            if (voeding == null)
            {
                return;
            }

            var meetmethode = voeding?.ONDERVOEDING?.MEETMETHODE;
            var scoreletter = "";
            if (meetmethode == "MUST")
            {
                scoreletter = "M";
            }
            else if (meetmethode == "SNAQ")
            {
                scoreletter = "S";
            }

            eoverdracht.VoedingOndervoedingAanwezig.Value = voeding?.ONDERVOEDING?.AANWEZIG;
            eoverdracht.VoedingOndervoedingAanwezigMeetmethode.Value = (meetmethode == "MUST" ? "414648004" : meetmethode);
            eoverdracht.VoedingOndervoedingAanwezigScore.Value = String.Concat(scoreletter, voeding?.ONDERVOEDING?.SCORE);

            eoverdracht.VoedingSprake.Value = voeding?.VOEDSEL_INTOLERANTIE?.SPRAKE_VAN;
            eoverdracht.VoedingSprakeSoort = voeding?.VOEDSEL_INTOLERANTIE?.SOORT;

            eoverdracht.VoedingDieetAanwezig.Value = voeding?.DIEET?.AANWEZIG;
            eoverdracht.VoedingDieetSoort = voeding?.DIEET?.SOORT_DIEET;

            eoverdracht.VoedingConsistentie = voeding?.CONSISTENTIE;
        }

        private void fillAdministrationSystems(EOverdracht eoverdracht, _SHN_POI_TOEDIENING_SYSTEMEN toedieningssystemen)
        {
            if (toedieningssystemen == null)
            {
                return;
            }

            eoverdracht.ToedieningSoortSonde = toedieningssystemen?.SONDE?.SOORT_SONDE;
            eoverdracht.ToedieningDatumPlaatsingSonde = toedieningssystemen?.SONDE?.DATUM_PLAATSING_SONDE?.ToDateTime();

            eoverdracht.ToedieningInfuusSoort.Value = toedieningssystemen?.INFUUS?.SOORT;
            eoverdracht.ToedieningInfuusLocatie = toedieningssystemen?.INFUUS?.LOCATIE_INFUUS;
            eoverdracht.ToedieningInfuusDatumPlaatsing = toedieningssystemen?.INFUUS?.DATUM_PLAATSING_INFUUS.ToDateTime();
        }

        private void fillSleep(EOverdracht eoverdracht, _SHN_POI_SLAAP slaap)
        {
            if (slaap == null)
            {
                return;
            }

            eoverdracht.SlaapToelichting = slaap?.SLAPEN?.TOELICHTING;
        }

        private void fillSecretion(EOverdracht eoverdracht, _SHN_POI_UITSCHEIDING uitscheiding)
        {
            if (uitscheiding == null)
            {
                return;
            }

            eoverdracht.UitscheidingStomaSoort = uitscheiding?.STOMA.SOORT_STOMA;
            eoverdracht.UitscheidingStomaToelichting = uitscheiding?.STOMA?.TOELICHTING;

            eoverdracht.UitscheidingUrineKatheterToelichting = uitscheiding?.URINE_KATHETER?.TOELICHTING;
            eoverdracht.UitscheidingUrineKatheterSoort.Value = uitscheiding?.URINE_KATHETER?.SOORT_KATHETER;

            eoverdracht.UitscheidingIncontinentiemateriaal = uitscheiding?.INCONTINENTIEMATERIAAL;
        }

        private void fillSenses(EOverdracht eoverdracht, _SHN_POI_ZINTUIGEN zintuigen)
        {
            if (zintuigen == null)
            {
                return;
            }

            eoverdracht.ZintuigenZienHulpmiddelen.Value = zintuigen?.HULPMIDDELEN_BIJ_ZIEN;
            eoverdracht.ZintuigenHorenHulpmiddelen.Value = zintuigen?.HULPMIDDELEN_BIJ_GEHOOR;
            eoverdracht.ZintuigenOverig = zintuigen?.OVERIG;
        }

        private void fillOralCare(EOverdracht eoverdracht, _SHN_POI_MONDVERZORGING mondverzorging)
        {
            if (mondverzorging == null)
            {
                return;
            }

            eoverdracht.MondVerzorgingProtheseAanwezig.Value = mondverzorging?.PROTHESE_AANWEZIG;
        }

        private string getName(_SHN_POI_PERSOONSNAAM persoonsnaam)
        {
            if (persoonsnaam == null)
            {
                return "";
            }
            Name name = new Name()
            {
                Initials = persoonsnaam.VOORLETTERS,
                LastName = persoonsnaam.GESLACHTSNAAM
            };
            return name.FullName();
        }
    }
}