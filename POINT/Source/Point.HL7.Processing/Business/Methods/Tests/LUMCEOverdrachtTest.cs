﻿using Point.HL7.Processing.Models;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace Point.HL7.Processing.Business.Methods.Tests
{
    public class LUMCEOverdrachtTest : IEPDEndpoint<EOverdracht>
    {
        public EndpointAddress EndpointAddress { get; set; }

        public Binding Binding { get; set; }

        public bool ValidConnection()
        {
            return true;
        }

        public EOverdracht Get(EPDRequest epdRequest)
        {
            var eoverdracht = new EOverdracht
            {
                PatientNumber = "987654"
            };

            return eoverdracht;
        }
    }
}