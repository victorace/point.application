﻿using Point.Database.Models;
using Point.HL7.Processing.Models;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace Point.HL7.Processing.Business.Methods.Tests
{
    public class Z_POINT_EOverdrachtTest : IEPDEndpoint<EOverdracht>
    {
        public Z_POINT_EOverdrachtTest(EndpointConfiguration endpointconfiguration)
        {
            EndpointAddress = new EndpointAddress(endpointconfiguration.Address);
        }

        public EndpointAddress EndpointAddress { get; set; }

        public Binding Binding { get; set; }

        public bool ValidConnection()
        {
            return true;
        }

        public EOverdracht Get(EPDRequest epdRequest)
        {
            var eoverdracht = new EOverdracht
            {
                PatientNumber = "987654"
            };

            return eoverdracht;
        }
    }
}