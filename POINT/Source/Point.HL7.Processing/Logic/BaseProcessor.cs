﻿using NHapi.Base.Model;
using Point.HL7.Processing.Business;
using Point.HL7.Processing.Extensions;
using Point.HL7.Processing.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Point.HL7.Processing.Logic
{
    public static class BaseProcessor
    {

        public static void MessageExToClientBO(MessageEx messageEx, HL7Client hl7Client, HL7RequestForm hl7RequestForm)
        {
            if (messageEx.Version == HL7MessageHelper.V24)
            {
                NHapi.Model.V24.Segment.PID pid = null;
                NHapi.Model.V24.Segment.PV1 pv1 = null;
                NHapi.Model.V24.Segment.PV2 pv2 = null;
                NHapi.Model.V24.Segment.IN1 in1 = null;
                IEnumerable<NHapi.Model.V24.Segment.ROL> rols = null;
                IEnumerable<NHapi.Model.V24.Segment.NK1> nk1s = null;

                if (messageEx.HL7HeaderInfo.MessageStructure == HL7MessageHelper.ADT_A01 || messageEx.HL7HeaderInfo.MessageStructure == HL7MessageHelper.ADT_A08)
                {
                    var typedhl7 = (NHapi.Model.V24.Message.ADT_A01)messageEx.Message;
                    pid = typedhl7.PID;
                    pv1 = typedhl7.PV1;
                    pv2 = typedhl7.PV2;
                    if (typedhl7.INSURANCERepetitionsUsed > 0 && typedhl7.INSURANCEs.FirstOrDefault() != null)
                    {
                        in1 = typedhl7.INSURANCEs.FirstOrDefault().IN1;
                    }
                    rols = typedhl7.ROLs;
                    nk1s = typedhl7.NK1s;
                }
                else if (messageEx.HL7HeaderInfo.MessageStructure == HL7MessageHelper.ADR_A19)
                {
                    var typedmessage = (NHapi.Model.V24.Message.ADR_A19)messageEx.Message;
                    if (typedmessage.MSA.AcknowledgementCode.Value == "AE" || typedmessage.MSA.AcknowledgementCode.Value == "AR")
                    {
                        hl7Client.Error = typedmessage.MSA.TextMessage.Value; 
                        return;
                    }
                    var typedhl7 = typedmessage.GetQUERY_RESPONSE();
                    pid = typedhl7.PID;
                    pv1 = typedhl7.PV1;
                    pv2 = typedhl7.PV2;
                    if (typedhl7.INSURANCERepetitionsUsed > 0 && typedhl7.INSURANCEs.FirstOrDefault() != null)
                    {
                        in1 = typedhl7.INSURANCEs.FirstOrDefault()?.IN1;
                    }
                    rols = typedhl7.ROLs;
                    nk1s = typedhl7.NK1s;
                }
                else
                {
                    return;
                }


                var structuretobo = new V24.StructureToBO() { HL7Client = hl7Client, HL7RequestForm = hl7RequestForm };
                structuretobo.FillPatient(pid);
                structuretobo.FillVisit(pv1);
                structuretobo.FillVisit(pv2);
                structuretobo.FillInsurance(in1);
                structuretobo.FillGeneralPractioner(rols);
                structuretobo.FillPharmacy(rols);
                structuretobo.FillNextOfKin(nk1s);
            }
            

            hl7Client.CleanValues();
        }

        public static void HeaderInfoToClientBO(HL7HeaderInfo hl7HeaderInfo, HL7Client hl7Client)
        {
            hl7Client.TimeStamp = DateTime.Now;
            hl7Client.MessageID = hl7HeaderInfo.MessageHeaderControlID;
        }

        public static void InterfaceCodeToClientBO(string interfaceCode, HL7Client hl7Client)
        {
            var organizationsetting = OrganizationSettingBL.GetOrganizationSettingByInterfaceCode(interfaceCode);
            hl7Client.InterfaceCode = interfaceCode;
            hl7Client.OrganizationID = organizationsetting.OrganizationID;
            hl7Client.LocationID = organizationsetting.LocationID;
        }


        public static HL7HeaderInfo GetHL7HeaderInfo(this IMessage message)
        {
            IStructure mshstructure = message.GetStructure(HL7MessageHelper.MSH);

            if (message.Version == HL7MessageHelper.V24) // Only version supported and used at the moment (okt. 2017)
            {
                var structuretobo = new V24.StructureToBO();
                structuretobo.FillHL7HeaderInfo((NHapi.Model.V24.Segment.MSH)mshstructure);

                if (structuretobo.HL7HeaderInfo.MessageStructure == HL7MessageHelper.ADR_A19)
                {
                    IStructure msastructure = message.GetStructure(HL7MessageHelper.MSA);
                    structuretobo.FillHL7HeaderInfo((NHapi.Model.V24.Segment.MSA)msastructure);
                }

                return structuretobo.HL7HeaderInfo;
                
            }

            return new HL7HeaderInfo();
        }



    }
}
