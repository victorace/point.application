﻿using NHapi.Base.Model;
using NHapi.Base.Parser;
using Point.HL7.Processing.Business;
using Point.HL7.Processing.Logic;
using Point.HL7.Processing.Models;
using Point.Log4Net;
using System;

namespace Point.HL7.Processing.Logic
{

    public class MessageEx
    {
        public bool IsParsed { get; set; }
        public IMessage Message { get; set; }
        public HL7HeaderInfo HL7HeaderInfo { get; set; }
        public string Version { get; set; }

    }

    public class PipeParserEx : PipeParser, IDisposable
    {
        private static readonly ILogger Logger = LogManager.GetLogger(typeof(PipeParserEx));
        protected MessageEx messageex = new MessageEx();

        public string FileContents { get; set; }

        public PipeParserEx() : base()
        {
        }

        public MessageEx Parse()
        {
            messageex.IsParsed = false;
            if (String.IsNullOrEmpty(FileContents))
            {
                return null;
            }

            initialParse();

            if (!messageex.IsParsed)
            {
                retryBadlyFormattedContents();
            }

            if (messageex.IsParsed && isGenericMessage())
            {
                retryGenericMessage();
            }
            
            if (messageex.IsParsed)
            {
                messageex.Version = messageex.Message.Version;
                return messageex;
            }
            else
            {
                return null;
            }

        }

        private void initialParse()
        {
            try
            {
                FileContents = HL7MessageHelper.RemoveUnknownSegments(FileContents, new[] { "ZCO" });
                messageex.Message = base.Parse(FileContents);
                messageex.HL7HeaderInfo = messageex.Message.GetHL7HeaderInfo();
                messageex.IsParsed = true;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
        }

        private void retryBadlyFormattedContents()
        {
            if (HL7MessageHelper.HasMultipleMSH(FileContents))
            {
                FileContents = HL7MessageHelper.StripMultipleMSH(FileContents);
            }
            if (HL7MessageHelper.HasBrokenLines(FileContents))
            {
                FileContents = HL7MessageHelper.FixBrokenLinesHL7(FileContents);
            }
            try
            {
                messageex.Message = base.Parse(FileContents);
                messageex.IsParsed = true;
            }
            catch
            {
                // handle/log the exception (?)
            }
        }

        private bool isGenericMessage()
        {
            return (messageex.Message.GetType().BaseType == typeof(GenericMessage));
        }

        private void retryGenericMessage()
        {
            // One known issue: Albert Schweitzer sending ADT_A08-structure with version 2.4, which doesn't exist. They mean ADT_A01, but Chipsoft didn't support that.
            if (messageex.HL7HeaderInfo.Version == HL7MessageHelper.V24 && messageex.HL7HeaderInfo.MessageStructure == HL7MessageHelper.ADT_A08)
            {
                FileContents = HL7MessageHelper.FixMessageStructure(FileContents, HL7MessageHelper.ADT_A08, HL7MessageHelper.ADT_A01);
            }
            try
            {
                messageex.Message = base.Parse(FileContents);
                messageex.HL7HeaderInfo = messageex.Message.GetHL7HeaderInfo();
                messageex.IsParsed = true;
            }
            catch(Exception ex)
            {
                Logger.Error(ex);
            }
        }

        bool disposed;
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    FileContents = null;
                }
            }
            disposed = true;
        }
    }
}
