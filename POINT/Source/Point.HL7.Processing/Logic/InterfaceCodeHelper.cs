﻿using Point.HL7.Processing.Business;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Point.HL7.Processing.Logic
{
    public static class InterfaceCodeHelper
    {

        public static string GetInterfaceCodeFromPath(string fullPath, string subDir)
        {
            string interfacecode = null;
            string fulldirectoryname = Path.GetDirectoryName(fullPath);
            string[] directoryparts = fulldirectoryname.Split(Path.DirectorySeparatorChar);
            if (String.IsNullOrEmpty(subDir))
            {
                interfacecode = directoryparts.ElementAtOrDefault(directoryparts.Length - 1);
            }
            else if (directoryparts.Last().Equals(subDir))
            {
                interfacecode = directoryparts.ElementAtOrDefault(directoryparts.Length - 2);
            }

            return interfacecode;
        }

        public static int GetOrganizationIDFromPath(string fullPath, string subDir)
        {
            string interfacecode = GetInterfaceCodeFromPath(fullPath, subDir);
            if (String.IsNullOrEmpty(interfacecode))
            {
                return -1;
            }

            return OrganizationSettingBL.GetOrganizationIDByInterfaceCode(interfacecode);
        }

        public static int? GetLocationIDFromPath(string fullPath, string subDir)
        {
            string interfacecode = GetInterfaceCodeFromPath(fullPath, subDir);
            if (String.IsNullOrEmpty(interfacecode))
            {
                return null;
            }

            return OrganizationSettingBL.GetLocationIDByInterfaceCode(interfacecode);
        }

        public static bool HasInterfaceCodeForLocation(int? locationID)
        {
            if (!locationID.HasValue)
            {
                return false;
            }

            var organizationsetting = OrganizationSettingBL.GetInterfaceCodeByLocationID(locationID.Value);

            return (organizationsetting != null && organizationsetting.Any());
        }


    }
}
