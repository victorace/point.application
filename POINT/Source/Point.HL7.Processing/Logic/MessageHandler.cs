﻿using Point.HL7.Processing.Business;
using Point.HL7.Processing.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Point.HL7.Processing.Logic
{
    public class MessageHandlerEventArgs : EventArgs
    {
        public MessageHandleInfo MessageHandleInfo { get; private set; }

        public MessageHandlerEventArgs(MessageHandleInfo messageHandleInfo)
        {
            MessageHandleInfo = messageHandleInfo;
        }
    }

    public class MessageHandleInfo
    {
        public int OrganizationID { get; set; }
        public int? LocationID { get; set; }
        public string PatientNumber { get; set; }
        public HL7Client HL7Client { get; set; }

        public override string ToString()
        {
            return $"OrganizationID: {OrganizationID}, LocationID: {LocationID}, PatientNumber: {PatientNumber}";
        }
    }

    

    public class MessageHandler
    {
        public event EventHandler<MessageHandlerEventArgs> GotMessage;

        private List<MessageHandleInfo> messages = new List<MessageHandleInfo>();

        public void Add(int organizationID, int? locationID, string patientNumber, HL7Client hl7Client)
        {
            var messagehandleinfo = new MessageHandleInfo() { OrganizationID = organizationID, LocationID = locationID, PatientNumber = patientNumber, HL7Client = hl7Client };
            messages.Add(messagehandleinfo);
            GotMessage?.Invoke(this, new MessageHandlerEventArgs(messagehandleinfo));
        }

        public MessageHandleInfo Get(int organizationID, int? locationID, string patientNumber)
        {
            var message = messages.FirstOrDefault(m => m.OrganizationID == organizationID && m.LocationID == locationID && m.PatientNumber == patientNumber);
            if (message != null)
            {
                messages.Remove(message);
                return message;
            }
            return null;

        }

    }
}
