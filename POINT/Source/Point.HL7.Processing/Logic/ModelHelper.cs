﻿using Point.HL7.Processing.Extensions;
using Point.HL7.Processing.Models;
using System.Collections.Generic;

namespace Point.HL7.Processing.Logic
{
    public class ModelHelper
    {
        public Dictionary<string, string> ToDictionary<T>(T source)
        {
            var dictionary = new Dictionary<string, string>();

            foreach (var property in source.GetType().GetProperties())
            {
                var value = property.GetValue(source);
                if (value == null)
                {
                    continue;
                }

                if (typeof(IValue).IsAssignableFrom(property.PropertyType))
                {
                    dictionary.Add(property.Name, ((IValue)value).GetValue());
                }
                else if (property.PropertyType.IsClass && property.PropertyType.Assembly != typeof(object).Assembly) 
                {
                    dictionary.AddRange(ToDictionary(value));
                }
                else
                {
                    dictionary.Add(property.Name, value.To<string>());
                }
            }

            return dictionary;
        }
    }
}