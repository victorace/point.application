﻿using NHapi.Base.Parser;
using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace Point.HL7.Processing.Logic
{
    public class HL7MessageHelper
    {
        public const string MSH = "MSH";
        public const string MSA = "MSA";
        public const string EVN = "EVN";
        public const char FieldDelimiter = '|';
        public const string EncodingChars = @"^~\&";
        public const string V23 = "2.3";
        public const string V24 = "2.4";
        public const string ADT_A01 = "ADT_A01";
        public const string ADT_A08 = "ADT_A08";
        public const string ADR_A19 = "ADR_A19";
        public const string QRY = "QRY";
        public const string ORU = "ORU";
        public const string A08 = "A08";
        public const string A19 = "A19";
        public const string R01 = "R01";
        public const string UzoviUnknown = "106";
        public const string QueryProcess = "P";
        public const string QueryFormat = "R";
        public const string QueryPriority = "I";
        public const string QueryUnits = "RD";
        public const string QuerySubject = "DEM";

        public static bool HasMultipleMSH(string fileContents)
        {
            return (Regex.Matches(fileContents, MSH).Count >= 2);
        }

        public static string StripMultipleMSH(string fileContents)
        {
            int secondindex = fileContents.IndexOf(MSH, fileContents.IndexOf(MSH) + MSH.Length);
            if (secondindex > 0)
            {
                return fileContents.Substring(0, secondindex);
            }
            else
            {
                return fileContents;
            }
        }

        public static bool HasBrokenLines(string fileContents)
        {
            // .GetEncoding checks every line, if it starts with segment-name (3 letters) followed by |
            // if it can't get this encoding it most likely contains lines belonging to the previous line but "broken" by a 'cr' or 'lf'
            return (new PipeParser().GetEncoding(fileContents) == null);
        }

        public static string FixBrokenLinesHL7(string fileContents)
        {

            var newfilecontents = "";
            foreach (var line in fileContents.Split(new string[] { Environment.NewLine, "\n", }, StringSplitOptions.RemoveEmptyEntries))
            {
                if (line[3] == FieldDelimiter)
                {
                    newfilecontents += Environment.NewLine;
                }
                newfilecontents += line;
            }
            return newfilecontents.TrimStart(Environment.NewLine.ToCharArray());

        }

        public static string FixMessageStructure(string fileContents, string oldStructure, string newStructure)
        {
            var newfilecontents = "";
            foreach (var line in fileContents.Split(new string[] { Environment.NewLine, "\r" }, StringSplitOptions.RemoveEmptyEntries))
            {
                if (line.Substring(0, 3) == MSH)
                {
                    newfilecontents += line.Replace(oldStructure, newStructure) + Environment.NewLine;
                }
                else
                {
                    newfilecontents += line + Environment.NewLine;
                }
            }
            return newfilecontents.TrimStart(Environment.NewLine.ToCharArray());
        }

        public static string RemoveUnknownSegments(string fileContents, string[] unknownSegments)
        {
            var newfilecontents = "";
            foreach (var line in fileContents.Split(new string[] { Environment.NewLine, "\r" }, StringSplitOptions.RemoveEmptyEntries))
            {
                if (!unknownSegments.Contains(line.Substring(0, 3)))
                {
                    newfilecontents += line + Environment.NewLine;
                }
            }
            return newfilecontents.TrimStart(Environment.NewLine.ToCharArray());
        }

        public static string SetEnvironmentNewLine(string fileContents)
        {
            var newfilecontents = "";
            foreach (var line in fileContents.Split(new string[] { Environment.NewLine, "\r" }, StringSplitOptions.RemoveEmptyEntries))
            {
                newfilecontents += line + Environment.NewLine;
            }
            return newfilecontents;
        }

        public static bool IsValidZorgmail(string zorgmail)
        {
            if (zorgmail == null)
            {
                return false;
            }
            if (Regex.IsMatch(zorgmail, @"^\d{9,10}$"))
            {
                return true;
            }
            if (zorgmail.IndexOf("@lms.lifeline", StringComparison.InvariantCultureIgnoreCase) > 0)
            {
                return true;
            }
            return false;
        }

        public static bool IsValidPhoneNumber(string phonenumber)
        {
            if (phonenumber == null)
            {
                return false;
            }
            if (Regex.IsMatch(phonenumber, @"^[\d\ \-\(\)\+]{10,20}$"))
            {
                return true;
            }
            return false;

        }






    }
}
