﻿using NHapi.Base.Model;
using NHapi.Model.V24.Datatype;
using NHapi.Model.V24.Segment;
using Point.Database.Models;
using Point.HL7.Processing.Extensions;
using System;
using System.Collections.Generic;

namespace Point.HL7.Processing.Logic.V24
{
    public class CreateORU
    {
        public DateTime CreateDateTime { get; set; }

        //For now there's only one type of document sent. Discussed with UMCU to use these:
        public const string ORUServiceIdentifierID = "10";
        public const string ORUServiceIdentifierName = "huisartsbrief";

        public void FillHL7HeaderInfo(MSH msh, string interfaceCode)
        {
            msh.FieldSeparator.Value = HL7MessageHelper.FieldDelimiter.ToString();
            msh.EncodingCharacters.Value = HL7MessageHelper.EncodingChars;
            msh.SendingApplication.NamespaceID.Value = "POINT";
            msh.ReceivingApplication.NamespaceID.Value = "CS-document";
            msh.ReceivingFacility.NamespaceID.Value = interfaceCode;
            msh.DateTimeOfMessage.TimeOfAnEvent.Value = CreateDateTime.ToHL7(false);
            msh.MessageType.MessageType.Value = HL7MessageHelper.ORU;
            msh.MessageType.TriggerEvent.Value = HL7MessageHelper.R01;
            msh.MessageControlID.Value = CreateDateTime.ToHL7(true);
            msh.ProcessingID.ProcessingID.Value = HL7MessageHelper.QueryProcess;
            
            msh.VersionID.VersionID.Value = HL7MessageHelper.V24;
            //msh.AcceptAcknowledgmentType.Value      // not sending this one for 'original mode'. 'enhanced mode' = AL
            //msh.ApplicationAcknowledgmentType.Value // not sending this one for 'original mode'. 'enhanced mode' = NE
        }

        public void FillPatientIdentification(PID pid, Client client)
        {
            pid.PatientID.ID.Value = client.CivilServiceNumber; // client.PatientNumber;
            CX cx = pid.GetPatientIdentifierList(0);
            cx.ID.Value = client.PatientNumber;
            fillPatientName(pid.GetPatientName(0), client);
            pid.DateTimeOfBirth.TimeOfAnEvent.Value = client.BirthDate.ToHL7Short();
            pid.AdministrativeSex.Value = client.Gender.PointGenderToHL7Gender();
            fillPatientAddress(pid.GetPatientAddress(0), client);
        }

        public void FillObservationRequest(OBR obr, Client client, int transferID)
        {
            obr.PlacerOrderNumber.EntityIdentifier.Value = client.VisitNumber.ElseIfEmpty(transferID.ToString());
            obr.FillerOrderNumber.EntityIdentifier.Value = transferID.ToString();
            obr.UniversalServiceIdentifier.Identifier.Value = ORUServiceIdentifierID;
            obr.UniversalServiceIdentifier.Text.Value = ORUServiceIdentifierName;
            obr.ObservationDateTime.TimeOfAnEvent.Value = CreateDateTime.ToHL7();

            //16: can be left empty, since we don't have the exact HIX-codes and -names
            //28: can be left empty, since we don't have the exact HIX-codes and -names
            //32: can be left empty, since we don't have the exact HIX-codes and -names
        }

        public FT GetAndFillFormattedText(IMessage message, string contents)
        {
            var ft = new FT(message)
            {
                Value = contents.Base64Encode()
            };
            return ft;
        }

        public void FillObservationResult(OBX obx, FT ft)
        {
            obx.ValueType.Value = "FT";
            obx.ObservationIdentifier.Identifier.Value = ORUServiceIdentifierID;
            obx.ObservationIdentifier.Text.Value = ORUServiceIdentifierName;

            var observationvalue = obx.GetObservationValue(0);
            observationvalue.Data = ft;

            obx.ObservationResultStatus.Value = "F";

            obx.DateTimeOfTheObservation.TimeOfAnEvent.Value = CreateDateTime.ToHL7();
        }

        private void fillPatientName(XPN xpn, Client client)
        {
            xpn.GivenName.Value = client.FirstName;
            xpn.PrefixEgDR.Value = client.Salutation;
            xpn.SecondAndFurtherGivenNamesOrInitialsThereof.Value = client.Initials;
            xpn.FamilyName.Surname.Value = client.LastName;
        }

        private void fillPatientAddress(XAD xad, Client client)
        {
            xad.City.Value = client.City;
            xad.StreetAddress.StreetOrMailingAddress.Value = $"{client.StreetName} {client.Number}{client.HuisnummerToevoeging}".Trim();
            xad.ZipOrPostalCode.Value = client.PostalCode;
        }

        private Dictionary<Type, int> segmentCounter = new Dictionary<Type, int>();

        public T SetID<T>(T abstractSegment)
        {
            var counter = 1;
            if (segmentCounter.ContainsKey(abstractSegment.GetType()))
            {
                counter = segmentCounter[abstractSegment.GetType()] + 1;
            }
            else
            {
                segmentCounter.Add(abstractSegment.GetType(), counter);
            }

            TypeSwitch.Do(abstractSegment,
                TypeSwitch.Case<PID>(item => item.SetIDPID.Value = counter.To<string>()),
                TypeSwitch.Case<OBR>(item => item.SetIDOBR.Value = counter.To<string>()),
                TypeSwitch.Case<OBX>(item => item.SetIDOBX.Value = counter.To<string>()));

            return abstractSegment;
        }
    }
}
