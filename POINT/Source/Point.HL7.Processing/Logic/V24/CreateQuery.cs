﻿using NHapi.Model.V24.Datatype;
using NHapi.Model.V24.Segment;
using Point.HL7.Processing.Extensions;
using System;

namespace Point.HL7.Processing.Logic.V24
{
    public class CreateQuery
    {
        public DateTime QueryDateTime { get; set; }

        public string PatientNumber { get; set; }

        public bool SkipQRYAssigningAuthority { get; set; } = false;

        public void FillHL7HeaderInfo(MSH msh)
        {
            msh.FieldSeparator.Value = HL7MessageHelper.FieldDelimiter.ToString();
            msh.EncodingCharacters.Value = HL7MessageHelper.EncodingChars;
            msh.SendingApplication.NamespaceID.Value = "POINT"; 
            msh.DateTimeOfMessage.TimeOfAnEvent.Value = QueryDateTime.ToHL7(false);
            msh.MessageType.MessageType.Value = HL7MessageHelper.QRY;
            msh.MessageType.TriggerEvent.Value = HL7MessageHelper.A19;
            msh.MessageControlID.Value = QueryDateTime.ToHL7(true);
            msh.ProcessingID.ProcessingID.Value = HL7MessageHelper.QueryProcess;
            msh.VersionID.VersionID.Value = HL7MessageHelper.V24;
        }

        public void FillQueryDefinition(QRD qrd)
        {
            qrd.QueryDateTime.TimeOfAnEvent.Value = QueryDateTime.ToHL7(false);
            qrd.QueryFormatCode.Value = HL7MessageHelper.QueryFormat;
            qrd.QueryPriority.Value = HL7MessageHelper.QueryPriority;
            qrd.QueryID.Value = QueryDateTime.ToHL7(false);
            qrd.QuantityLimitedRequest.Quantity.Value = "1";
            qrd.QuantityLimitedRequest.Units.Text.Value = HL7MessageHelper.QueryUnits;

            XCN xcn = qrd.GetWhoSubjectFilter(0);
            xcn.IDNumber.Value = PatientNumber;

            if (!SkipQRYAssigningAuthority)
            {
                xcn.AssigningAuthority.NamespaceID.Value = "LOCAL";
            }

            CE ce = qrd.GetWhatSubjectFilter(0);
            ce.Identifier.Value = HL7MessageHelper.QuerySubject;
        }
    }
}
