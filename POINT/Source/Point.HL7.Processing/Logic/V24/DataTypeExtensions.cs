﻿using System;
using NHapi.Model.V24.Datatype;
using System.Linq;
using Point.HL7.Processing.Extensions;

namespace Point.HL7.Processing.Logic.V24
{
    public static class DataTypeExtensions
    {
        public static DateTime? GetDateTime(this TS ts, DateTime? defaultDate = null)
        {
            if (ts == null || ts.TimeOfAnEvent == null || ts.TimeOfAnEvent.Value.IsNullOrEmptyHL7())
            {
                return defaultDate;
            }
            else
            {
                return ts.TimeOfAnEvent.GetAsDate();
            }
        }

        public static string GetFullName(this XCN xcn)
        {
            string fullname = "";

            string surname = xcn.FamilyName.Surname.Value;
            if (surname.IsNullOrEmptyHL7())
            {
                surname = String.Concat(xcn.FamilyName.OwnSurname.Value, " ", xcn.FamilyName.OwnSurnamePrefix.Value).TrimChars(" ");
            }
            fullname = String.Concat(xcn.GetInitials(), " ", surname).TrimChars(" ");

            return fullname;
        }

        public static string GetInitials(this XPN xpn)
        {
            return HL7Extensions.GetInitials(xpn.GivenName.Value, xpn.SecondAndFurtherGivenNamesOrInitialsThereof.Value);
        }

        public static string GetInitials(this XCN xcn)
        {
            return HL7Extensions.GetInitials(xcn.GivenName.Value, xcn.SecondAndFurtherGivenNamesOrInitialsThereof.Value);
        }


    }
}
