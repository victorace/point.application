﻿using NHapi.Model.V24.Datatype;
using NHapi.Model.V24.Segment;
using Point.Database.Models;
using Point.HL7.Processing.Business;
using Point.HL7.Processing.Extensions;
using Point.HL7.Processing.Models;
using Point.Log4Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Point.HL7.Processing.Logic.V24
{
    public class StructureToBO
    {
        private static readonly ILogger Logger = LogManager.GetLogger(typeof(StructureToBO));
        public HL7HeaderInfo HL7HeaderInfo { get;  private set; }
        public HL7Client HL7Client { get;  set; }
        public HL7RequestForm HL7RequestForm { get;  set; }

        public StructureToBO()
        {
            HL7Client = new HL7Client();
            HL7HeaderInfo = new HL7HeaderInfo();
            HL7RequestForm = new HL7RequestForm();
        }

        public bool FillHL7HeaderInfo(MSH msh)
        {
            try
            {
                HL7HeaderInfo.MessageStructure = msh.MessageType.MessageStructure.Value;
                HL7HeaderInfo.MessageHeaderControlID = msh.MessageControlID.Value;
                HL7HeaderInfo.MessageType = msh.MessageType.MessageType.Value;
                HL7HeaderInfo.TriggerEvent = msh.MessageType.TriggerEvent.Value;
                HL7HeaderInfo.Version = msh.VersionID.VersionID.Value;
                if (msh.CharacterSetRepetitionsUsed > 0)
                {
                    HL7HeaderInfo.CharacterSet = msh.GetCharacterSet(0).Value;
                }
                HL7HeaderInfo.DateTimeOfMessage = (DateTime)msh.DateTimeOfMessage.GetDateTime(DateTime.Now);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return false;
            }
            return true;
        }

        public bool FillHL7HeaderInfo(MSA msa)
        {
            try
            {
                HL7HeaderInfo.AcknowledgeControlID = msa.MessageControlID.Value;
            }
            catch
            {
                return false;
            }
            return true;
        }

        // Also see: http://hl7-definition.caristix.com:9010/
        public void FillPatient(PID pid)
        {
            var cxpi = pid.GetPatientIdentifierList().FirstOrDefault(rep => rep.IdentifierTypeCode.Value == "PI");
            if (cxpi == null)
            {
                cxpi = pid.GetPatientIdentifierList(0); // fallback on the old "rule"
            }
            if (cxpi != null)
            {
                HL7Client.PatientNumber = cxpi.ID.Value;
            }

            var cxcivil = pid.GetPatientIdentifierList().FirstOrDefault(rep => rep.IdentifierTypeCode.Value == "NNNLD");
            if (cxcivil != null)
            {
                HL7Client.CivilServiceNumber = cxcivil.ID.Value;
            }

            HL7Client.Gender = pid.AdministrativeSex.Value.HL7GenderToPointGender();
            fillPatientName(pid, HL7Client);
            HL7Client.BirthDate = (DateTime)pid.DateTimeOfBirth.GetDateTime(DateTime.MinValue);

            var xad = pid.GetPatientAddress().FirstOrDefault(rep => rep.AddressType.Value == "H");
            if (xad == null)
            {
                xad = pid.GetPatientAddress().FirstOrDefault(rep => rep.AddressType.Value == "M");
            }

            fillPatientAddress(xad);
            fillPatientPhone(pid);


        }

        public void FillNextOfKin(IEnumerable<NK1> nk1s)
        {
            if (nk1s == null || nk1s.Count() == 0)
            {
                return;
            }

            var nk1 = nk1s.FirstOrDefault();
            var xpn = nk1.GetName(0);
            if (xpn != null)
            {
                HL7Client.ContactPersonName = xpn.FamilyName.Surname.Value;
            }
            var xtn = nk1.GetPhoneNumber(0);
            if (xtn != null)
            {
                HL7Client.ContactPersonPhoneNumberGeneral = xtn.Get9999999X99999CAnyText.Value;
            }
            var ce = nk1.Relationship;
            if (ce != null)
            {
                // TODO: This needs to be converted ZIB - PP
                HL7Client.ContactPersonRelationType = ce.Text.Value.HL7RelationToPointRelationType();
            }

            if (String.IsNullOrEmpty(HL7Client.ContactPersonName))
            {
                HL7Client.ContactPersonName = "nog niet bekend";
            }
            if (String.IsNullOrEmpty(HL7Client.ContactPersonPhoneNumberGeneral))
            {
                HL7Client.ContactPersonPhoneNumberGeneral = "nog niet bekend";
            }
        }

        public void FillVisit(PV1 pv1)
        {
            if (pv1 == null)
            {
                return;
            }

            HL7Client.VisitNumber = pv1.VisitNumber.ID.Value;

            var attendingdoctorperson = pv1.GetAttendingDoctor().FirstOrDefault();
            if (attendingdoctorperson != null)
            {
                HL7RequestForm.SetField(FlowFieldConstants.Name_BehandelaarNaam, attendingdoctorperson.GetFullName());
            }
            HL7RequestForm.SetField(FlowFieldConstants.Name_Kamernummer, pv1.AssignedPatientLocation.Room.Value);
            HL7RequestForm.SetField(FlowFieldConstants.Name_MedischeSituatieDatumOpname, pv1.AdmitDateTime.GetDateTime());

            var assignedpatientlocation = pv1.AssignedPatientLocation;
            if (assignedpatientlocation != null)
            {
                HL7RequestForm.SetField(FlowFieldConstants.Name_SourceOrganizationDepartment, assignedpatientlocation.PointOfCare.Value);
            }
        }

        public void FillVisit(PV2 pv2)
        {
            if (pv2 == null)
            {
                return;
            }
            HL7RequestForm.SetField(FlowFieldConstants.Name_MedischeSituatieRedenOpname, pv2.AdmitReason.Text.Value);
            HL7RequestForm.SetField(FlowFieldConstants.Name_DatumEindeBehandelingMedischSpecialist, pv2.ExpectedDischargeDateTime.GetDateTime());

        }

        // TODO: UZOVI is an incorrect value according to the HL7 standard. It appears
        // that many of the POINT hospitals use this as a assigning authority though.
        public static readonly List<string> assigningauthorities = new List<string>() { "UZOVI", "VEKTIS" };
        public void FillInsurance(IN1 in1)
        {
            HL7Client.HealthInsuranceCompanyUZOVICode = HL7MessageHelper.UzoviUnknown;

            if (in1 == null)
            {
                return;
            }

            HL7Client.InsuranceNumber = in1.PolicyNumber.Value;

            var cxuzovi = in1.GetInsuranceCompanyID().FirstOrDefault(cx => assigningauthorities.Contains(cx?.AssigningAuthority?.NamespaceID?.Value?.ToUpper()));
            HL7Client.HealthInsuranceCompanyUZOVICode = cxuzovi?.ID?.Value?.TrimHL7Empty();
            if (HL7Client.HealthInsuranceCompanyUZOVICode.IsNullOrEmptyHL7() || HL7Client.HealthInsuranceCompanyUZOVICode == "0")
            {
                cxuzovi = in1.GetInsuranceCompanyID().FirstOrDefault();
                HL7Client.HealthInsuranceCompanyUZOVICode = cxuzovi?.ID?.Value?.TrimHL7Empty();
            }
            if (HL7Client.HealthInsuranceCompanyUZOVICode.IsNullOrEmptyHL7() || HL7Client.HealthInsuranceCompanyUZOVICode == "0")
            {
                HL7Client.HealthInsuranceCompanyUZOVICode = HL7MessageHelper.UzoviUnknown;
            }

            HL7Client.HealthInsuranceCompanyID = HealthInsurerBL.GetHealthInsurerIDByUZOVI(HL7Client.HealthInsuranceCompanyUZOVICode);

            var xon = in1.GetInsuranceCompanyName().FirstOrDefault();
            HL7Client.HealthCareProvider = xon?.OrganizationName?.Value;
        }

        private string getGPPhone(XTN xtn)
        {
            string gpphone = "";
            if (!xtn.Get9999999X99999CAnyText.Value.IsNullOrEmptyHL7())
            {
                gpphone = xtn.Get9999999X99999CAnyText.Value;
            }
            else if (!xtn.PhoneNumber.Value.IsNullOrEmptyHL7())
            {
                gpphone = xtn.PhoneNumber.Value;
            }
            return gpphone;
        }

        public void FillGeneralPractioner(IEnumerable<ROL> rols)
        {
            var generalpractitionerrol = rols.FirstOrDefault(rol => rol.RoleROL.Identifier.Value == "PP" && rol.GetProviderType().Any(ce => ce.Identifier.Value == "01")); 
            if (generalpractitionerrol == null)
            {
                generalpractitionerrol = rols.FirstOrDefault(rol => rol.RoleROL.Identifier.Value == "PP" && rol.GetRolePerson().Any(xcn => xcn.SourceTable.Value == "H"));
            }
            if (generalpractitionerrol == null)
            {
                generalpractitionerrol = rols.FirstOrDefault(rol => rol.RoleROL.Identifier.Value == "PP");
            }

            if (generalpractitionerrol == null)
            {
                return;
            }

            var generalpractitionerperson = generalpractitionerrol.GetRolePerson().FirstOrDefault(xcn => xcn.SourceTable.Value == "H");
            if (generalpractitionerperson == null)
            {
                generalpractitionerperson = generalpractitionerrol.GetRolePerson().FirstOrDefault(xcn => xcn.AssigningAuthority.NamespaceID.Value == "VEKTIS");
            }
            if (generalpractitionerperson == null)
            {
                generalpractitionerperson = generalpractitionerrol.GetRolePerson().FirstOrDefault(xcn => xcn.AssigningAuthority.NamespaceID.Value == "LOCAL");
            }
            if (generalpractitionerperson != null)
            {
                HL7Client.GeneralPractitionerName = generalpractitionerperson.GetFullName();
            }

            var generalpractitionerxtn = generalpractitionerrol.GetPhone().FirstOrDefault(xtn => xtn.TelecommunicationEquipmentType.Value == "PH");
            if (generalpractitionerxtn == null || getGPPhone(generalpractitionerxtn).IsNullOrEmptyHL7())
            {
                generalpractitionerxtn = generalpractitionerrol.GetPhone().FirstOrDefault(xtn => xtn.TelecommunicationEquipmentType.Value == "CP");
            }
            if (generalpractitionerxtn == null || getGPPhone(generalpractitionerxtn).IsNullOrEmptyHL7())
            {
                generalpractitionerxtn = generalpractitionerrol.GetPhone().FirstOrDefault(xtn => xtn.TelecommunicationUseCode.Value == "PRN");
            }
            if (generalpractitionerxtn != null && !getGPPhone(generalpractitionerxtn).IsNullOrEmptyHL7())
            {
                HL7Client.GeneralPractitionerPhoneNumber = getGPPhone(generalpractitionerxtn);
            }
            else
            {
                generalpractitionerxtn = generalpractitionerrol.GetPhone().FirstOrDefault(); // GHZ
                if (generalpractitionerxtn != null && HL7MessageHelper.IsValidPhoneNumber(getGPPhone(generalpractitionerxtn)))
                {
                    HL7Client.GeneralPractitionerPhoneNumber = getGPPhone(generalpractitionerxtn);
                }
            }

            //Multiple ways of getting the zorgmail-adres, best first
            if (HL7Client.GeneralPractitionerZorgmail.IsNullOrEmptyHL7())
            {
                var zorgmailxtn = generalpractitionerrol.GetPhone().FirstOrDefault(xtn => xtn.TelecommunicationEquipmentType.Value == "X.400");
                if (zorgmailxtn != null)
                {
                    HL7Client.GeneralPractitionerZorgmail = zorgmailxtn.EmailAddress.Value;
                }
            }
            if (HL7Client.GeneralPractitionerZorgmail.IsNullOrEmptyHL7())
            {
                var generalpractionerzorgmail = generalpractitionerrol.GetRolePerson().FirstOrDefault(xcn => xcn.IdentifierTypeCode.Value == "ZORGMAIL"); //AMC (e.a.?)
                if (generalpractionerzorgmail != null)
                {
                    HL7Client.GeneralPractitionerZorgmail = generalpractionerzorgmail.IDNumber.Value;
                }
            }
            if (HL7Client.GeneralPractitionerZorgmail.IsNullOrEmptyHL7())
            {
                var workxtn = generalpractitionerrol.GetPhone().FirstOrDefault(xtn => xtn.TelecommunicationUseCode.Value == "WPN"); //Gelre (e.a.?)
                if (workxtn == null)
                {
                    workxtn = generalpractitionerrol.GetPhone().FirstOrDefault();
                }
                if (workxtn != null && HL7MessageHelper.IsValidZorgmail(workxtn.EmailAddress.Value))
                {
                    HL7Client.GeneralPractitionerZorgmail = workxtn.EmailAddress.Value;
                }
            }
        }

        public void FillPharmacy(IEnumerable<ROL> rols)
        {

            var pharmacyrole = rols.FirstOrDefault(rol => rol.RoleROL.Identifier.Value == "FHCP" && rol.GetProviderType().Any(ce => ce.Identifier.Value == "02"));
            if (pharmacyrole == null)
            {
                pharmacyrole = rols.FirstOrDefault(rol => rol.RoleROL.Identifier.Value == "PP" && rol.GetProviderType().Any(ce => ce.Identifier.Value == "02")); // && rol.GetRolePerson().Any(xcn => xcn.AssigningAuthority.NamespaceID.Value == "VEKTIS" && xcn.SourceTable.Value == "02"));
            }

            if (pharmacyrole != null)
            {
                var pharmacyperson = pharmacyrole.GetRolePerson().FirstOrDefault(xcn => xcn.AssigningAuthority.NamespaceID.Value == "LOCAL");
                if (pharmacyperson != null)
                {
                    HL7Client.PharmacyName = String.Concat(HL7Extensions.GetNonEmptyValue(new[] { pharmacyperson.FamilyName.OwnSurname.Value, pharmacyperson.FamilyName.Surname.Value }));

                    var pharmacyxtn = pharmacyrole.GetPhone().FirstOrDefault(xtn => xtn.TelecommunicationEquipmentType.Value == "PH");
                    if (pharmacyxtn == null || pharmacyxtn.Get9999999X99999CAnyText.Value.IsNullOrEmptyHL7())
                    {
                        pharmacyxtn = pharmacyrole.GetPhone(0);
                    }
                    if (pharmacyxtn != null)
                    {
                        HL7Client.PharmacyPhoneNumber = pharmacyxtn.Get9999999X99999CAnyText.Value;
                        if (HL7Client.PharmacyPhoneNumber.IsNullOrEmptyHL7() && !pharmacyxtn.PhoneNumber.Value.IsNullOrEmptyHL7())
                        {
                            HL7Client.PharmacyPhoneNumber = pharmacyxtn.PhoneNumber.Value;
                        }
                    }
                }
            }

        }

        private static void fillPatientName(PID pid, HL7Client hl7Client)
        {
            if (pid == null)
            {
                return;
            }

            XPN[] xpns = pid.GetPatientName();
            if (xpns == null || xpns.Count() == 0)
            {
                return;
            }

            var xpn_l = xpns.FirstOrDefault(xpn => xpn.NameTypeCode.Value == "L");
            if (xpn_l == null)
            {
                xpn_l = xpns.FirstOrDefault();
            }

            var xpn_n = xpns.FirstOrDefault(xpn => xpn.NameTypeCode.Value == "N");
            var xpn_b = xpns.FirstOrDefault(xpn => xpn.NameTypeCode.Value == "B");
            if (xpn_n != null && !xpn_n.GivenName.Value.IsNullOrEmptyHL7())
            {
                hl7Client.FirstName = xpn_n.GivenName.Value;
            }
            else if (xpn_b != null && !xpn_b.GivenName.Value.IsNullOrEmptyHL7())
            {
                hl7Client.FirstName = xpn_b.GivenName.Value;
            }
            else
            {
                if (xpn_l.GivenName.Value?.IndexOf('.') == -1)
                {
                    hl7Client.FirstName = xpn_l.GivenName.Value;
                }
                if (hl7Client.FirstName?.Length == 1)
                {
                    hl7Client.FirstName = "";
                }
            }

            if (hl7Client.FirstName?.Length == 1)
            {
                var xpn_first = xpns.First();
                if (xpn_first != null && xpn_first.GivenName.Value?.Length > 1)
                {
                    hl7Client.FirstName = xpn_first.GivenName.Value;
                }
            }


            hl7Client.Salutation = xpn_l.PrefixEgDR.Value;

            hl7Client.Initials = xpn_l.GetInitials();

            hl7Client.MiddleName = null;
            hl7Client.LastName = xpn_l.FamilyName.Surname.Value;
            if (!pid.AdministrativeSex.Value.IsNullOrEmptyHL7() &&
                pid.AdministrativeSex.Value.Equals("m", StringComparison.InvariantCultureIgnoreCase))
            {
                hl7Client.MaidenName = null;
            }
            else
            {
                hl7Client.MaidenName = xpn_l.FamilyName.OwnSurname.Value;
                hl7Client.MiddleName = xpn_l.FamilyName.OwnSurnamePrefix.Value;
            }
        }

        private void fillPatientAddress(XAD xad)
        {
            if (xad == null)
            {
                return;
            }
            HL7Client.StreetName = xad.StreetAddress.StreetName.Value;
            HL7Client.Number = xad.StreetAddress.DwellingNumber.Value;
            if (HL7Client.StreetName.IsNullOrEmptyHL7() && HL7Client.Number.IsNullOrEmptyHL7())
            {
                HL7Client.StreetName = xad.StreetAddress.StreetOrMailingAddress.Value;
            }
            HL7Client.NumberAddition = xad.OtherDesignation.Value;
            HL7Client.PostalCode = xad.ZipOrPostalCode.Value.StripChars(" \"");
            HL7Client.City = xad.City.Value;
            HL7Client.Country = xad.Country.Value.HL7CountryToPointCountry();

            if (!HL7Client.StreetName.IsNullOrEmptyHL7() && HL7Client.Number.IsNullOrEmptyHL7())
            {
                Match match = new Regex(@"(\d+$)").Match(HL7Client.StreetName);
                if (match.Success)
                {
                    HL7Client.Number = match.Groups[1].Value;
                    HL7Client.StreetName = HL7Client.StreetName.Substring(0, HL7Client.StreetName.LastIndexOf(match.Groups[1].Value));
                }
            }

        }

        private void fillPatientPhone(PID pid)
        {
            string phonenumbergeneral = "";
            string phonenumbermobile = "";
            string phonenumberwork = "";

            // Get the phonenumber based on the "best" matching HL7-rule
            var xtns = pid.GetPhoneNumberHome();
            if (xtns != null && xtns.Count() > 0)
            {
                XTN xtngeneral = xtns.FirstOrDefault(xtn => xtn.TelecommunicationUseCode.Value == "PRN" && xtn.TelecommunicationEquipmentType.Value == "PH");
                if (xtngeneral == null)
                {
                    xtngeneral = xtns.FirstOrDefault(xtn => xtn.TelecommunicationUseCode.Value == "PRN" && xtn.TelecommunicationEquipmentType.Value != "CP");
                }
                if (xtngeneral != null)
                {
                    phonenumbergeneral = xtngeneral.Get9999999X99999CAnyText.Value.TrimHL7Empty();
                    if (phonenumbergeneral.IsNullOrEmptyHL7() && !xtngeneral.PhoneNumber.Value.IsNullOrEmptyHL7())
                    {
                        phonenumbergeneral = xtngeneral.PhoneNumber.Value;
                    }
                }

                XTN xtnmobile = xtns.FirstOrDefault(xtn => xtn.TelecommunicationUseCode.Value == "PRN" && xtn.TelecommunicationEquipmentType.Value == "CP");
                if (xtnmobile == null || xtnmobile.Get9999999X99999CAnyText.Value.IsNullOrEmptyHL7())
                {
                    xtnmobile = xtns.FirstOrDefault(xtn => xtn.TelecommunicationUseCode.Value == "ORN");
                }
                if (xtnmobile != null)
                {
                    phonenumbermobile = xtnmobile.Get9999999X99999CAnyText.Value.TrimHL7Empty();
                    if (phonenumbermobile.IsNullOrEmptyHL7() && !xtnmobile.PhoneNumber.Value.IsNullOrEmptyHL7())
                    {
                        phonenumbermobile = xtnmobile.PhoneNumber.Value;
                    }

                }
            }

            xtns = pid.GetPhoneNumberBusiness();
            if (xtns != null && xtns.Count() > 0)
            {
                XTN xtnbusiness = xtns.FirstOrDefault(xtn => xtn.TelecommunicationUseCode.Value == "WPN" && xtn.TelecommunicationEquipmentType.Value == "PH");
                if (xtnbusiness == null)
                {
                    xtnbusiness = xtns.FirstOrDefault(xtn => new[] { "PRN", "ORN", "WPN" }.Contains(xtn.TelecommunicationUseCode.Value));
                }
                if (xtnbusiness != null)
                {
                    phonenumberwork = xtnbusiness.Get9999999X99999CAnyText.Value.TrimHL7Empty();
                }
            }


            //Finally check if our first (mandatory) field isn't empty while others are filled
            if (phonenumbergeneral.IsNullOrEmptyHL7() && (!phonenumbermobile.IsNullOrEmptyHL7() || !phonenumberwork.IsNullOrEmptyHL7()))
            {
                if (!phonenumbermobile.IsNullOrEmptyHL7())
                {
                    phonenumbergeneral = phonenumbermobile;
                    phonenumbermobile = "";
                }
                else if (!phonenumberwork.IsNullOrEmptyHL7())
                {
                    phonenumbergeneral = phonenumberwork;
                    phonenumberwork = "";
                }
            }

            if (phonenumbergeneral.IsNullOrEmptyHL7() && pid.PhoneNumberHomeRepetitionsUsed > 0)
            {
                if (HL7MessageHelper.IsValidPhoneNumber(pid.GetPhoneNumberHome(0).Get9999999X99999CAnyText.Value))
                {
                    phonenumbergeneral = pid.GetPhoneNumberHome(0).Get9999999X99999CAnyText.Value;
                }
            }

            HL7Client.PhoneNumberGeneral = phonenumbergeneral.TrimChars("- ");
            HL7Client.PhoneNumberMobile = phonenumbermobile.TrimChars("- ");
            HL7Client.PhoneNumberWork = phonenumberwork.TrimChars("- ");
        }
    }
}
