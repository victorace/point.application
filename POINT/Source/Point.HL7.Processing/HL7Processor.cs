﻿using Point.HL7.Processing.Business;
using Point.HL7.Processing.Extensions;
using Point.HL7.Processing.Logic;
using Point.HL7.Processing.Models;
using Point.Log4Net;
using System;

namespace Point.HL7.Processing
{
    public class HL7Processor
    {
        private static readonly ILogger Logger = LogManager.GetLogger(typeof(HL7Processor));

        public string FileContent { get; set; }
        public string InterfaceCode { get; set; }

        public HL7Client HL7Client { get; private set; }
        public HL7HeaderInfo HL7HeaderInfo { get; private set; }
        public HL7RequestForm HL7RequestForm { get; private set; }

        public HL7Processor()
        {
            HL7Client = new HL7Client();
            HL7HeaderInfo = new HL7HeaderInfo();
            HL7RequestForm = new HL7RequestForm();
        }

        public bool ProcessContents()
        {
            try
            {
                if (string.IsNullOrEmpty(FileContent))
                {
                    return false;
                }

                using (PipeParserEx pipeparserex = new PipeParserEx() { FileContents = FileContent })
                {
                    var messageex = pipeparserex.Parse();
                    if (messageex == null || !messageex.IsParsed)
                    {
                        return false;
                    }

                    HL7HeaderInfo = messageex.HL7HeaderInfo;
                    BaseProcessor.HeaderInfoToClientBO(messageex.HL7HeaderInfo, HL7Client);
                    BaseProcessor.InterfaceCodeToClientBO(InterfaceCode, HL7Client);
                    BaseProcessor.MessageExToClientBO(messageex, HL7Client, HL7RequestForm);
                }

                return true;
            }
            catch (Exception exc)
            {
                Logger.Error(exc);
            }

            return false;
        }
    }
}
