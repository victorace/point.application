﻿using Point.HL7.Processing.Business;
using Point.HL7.Processing.Extensions;
using Point.HL7.Processing.Logic;
using Point.Log4Net;
using System;


namespace Point.HL7.Processing
{
    public class HL7Creator
    {
        private static readonly ILogger Logger = LogManager.GetLogger(typeof(HL7Creator));

        public string MessageContent { get; private set; }

        public bool CreateQryA19(int organizationID, string patientNumber)
        {
            try
            {

                string now = DateTime.Now.ToHL7(true);
                NHapi.Base.Parser.PipeParser pipeparser = new NHapi.Base.Parser.PipeParser();
                NHapi.Model.V24.Message.QRY_A19 qrya19 = new NHapi.Model.V24.Message.QRY_A19();

                var createquery = new Logic.V24.CreateQuery()
                {
                    QueryDateTime = DateTime.Now,
                    PatientNumber = patientNumber,
                    SkipQRYAssigningAuthority = OrganizationSettingBL.GetSkipQRYAssigningAuthority(organizationID)
                };
                createquery.FillHL7HeaderInfo(qrya19.MSH);
                createquery.FillQueryDefinition(qrya19.QRD);

                MessageContent = HL7MessageHelper.SetEnvironmentNewLine(pipeparser.Encode(qrya19));

                return true;
            }
            catch(Exception ex)
            {
                Logger.Error(ex);
            }
            return false;
        }


        public bool CreateORU(int organizationID, int transferID, string contents)
        {
            try
            {
                var client = ClientBL.GetClientFromTransferID(transferID);
                if (client == null)
                {
                    return false;
                }

                NHapi.Base.Parser.PipeParser pipeparser = new NHapi.Base.Parser.PipeParser();
                NHapi.Model.V24.Message.ORU_R01 orur01 = new NHapi.Model.V24.Message.ORU_R01();

                var interfaceCode = OrganizationSettingBL.GetInterfaceCodeByOrganizationID(organizationID);

                var createoru = new Logic.V24.CreateORU()
                {
                    CreateDateTime = DateTime.Now
                };
                createoru.FillHL7HeaderInfo(orur01.MSH, interfaceCode);

                var patientresult = orur01.AddPATIENT_RESULT();
                var pid = createoru.SetID(patientresult.PATIENT.PID);
                createoru.FillPatientIdentification(pid, client);

                var orderobservation = patientresult.AddORDER_OBSERVATION();
                var obr = createoru.SetID(orderobservation.OBR);
                createoru.FillObservationRequest(obr, client, transferID);

                var observation = orderobservation.AddOBSERVATION();
                var obx = createoru.SetID(observation.OBX);
                var ft = createoru.GetAndFillFormattedText(orur01, contents);
                createoru.FillObservationResult(obx, ft);

                MessageContent = HL7MessageHelper.SetEnvironmentNewLine(pipeparser.Encode(orur01));

                return true;
            }
            catch(Exception ex)
            {
                Logger.Error(ex);
            }
            return false;
        }
    }
}
