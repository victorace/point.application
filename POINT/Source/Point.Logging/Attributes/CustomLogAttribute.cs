﻿using System;

namespace Point.Log.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class CustomLogAttribute : Attribute
    {
        public string CustomLogValue;
    }
}
