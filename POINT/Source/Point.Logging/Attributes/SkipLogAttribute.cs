﻿using System;

namespace Point.Log.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class SkipLogAttribute : Attribute
    {
    }
}
