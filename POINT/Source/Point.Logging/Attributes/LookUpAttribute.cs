﻿using System;

namespace Point.Log.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class LookUpAttribute : Attribute
    {
        private string lookupurl;
        private string parametername;

        public LookUpAttribute(string lookupurl, string parametername)
        {
            this.lookupurl = lookupurl;
            this.parametername = parametername;
        }

        public virtual string LookUpUrl
        {
            get { return lookupurl; }
        }

        public virtual string ParameterName
        {
            get { return parametername; }
        }
    }
}
