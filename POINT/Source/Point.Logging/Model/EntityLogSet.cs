﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Point.Log.Model
{
    [Table("EntityLogSet")]
    public class EntityLogSet
    {
        public Guid EntityLogSetID { get; set; }
        public int EmployeeID { get; set; }
        public int ScreenID { get; set; }
        public DateTime TimeStamp { get; set; }

        public virtual ICollection<EntityLog> EntityLog { get; set; }
    }
}
