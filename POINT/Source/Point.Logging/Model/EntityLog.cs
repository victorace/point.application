﻿using Point.Log.Enums;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Point.Log.Model
{
    [Table("EntityLog")]
    public class EntityLog
    {
        public int EntityLogID { get; set; }
        public Guid EntityLogSetID { get; set; }
        public ChangeType ChangeType { get; set; }
        public string TableName { get; set; }
        public string PrimaryKeys { get; set; }
        public string FieldName { get; set; }
        public string OldValue { get; set; }
        public string NewValue { get; set; }

        public virtual EntityLogSet EntityLogSet { get; set; }
    }
}
