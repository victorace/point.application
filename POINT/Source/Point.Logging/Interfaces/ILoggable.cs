﻿using System;

namespace Point.Log.Interfaces
{
    public interface ILoggable
    {
        DateTime? ModifiedTimeStamp { get; set; }
    }
}
