﻿using Point.Log.Model;
using System.Data.Entity;

namespace Point.Log.Context
{
    public class PointLogContext : DbContext
    {
        public PointLogContext() : base("name=PointLogContext")
        {
        }

        public virtual DbSet<EntityLog> EntityLog { get; set; }
        public virtual DbSet<EntityLogSet> EntityLogSet { get; set; }
    }
}
