﻿using Point.Log.Attributes;
using Point.Log.Context;
using Point.Log.Enums;
using Point.Log.Interfaces;
using Point.Log.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;

namespace Point.Log
{
    public class LogBL : IDisposable
    {
        bool disposed = false;
        private EntityLogSet EntityLogSet { get; set; }
        private PointLogContext PointLogContext { get; set; }

        public bool IsHidden(Type tabletype, string propertyname)
        {
            var propertyinfo = tabletype.GetProperty(propertyname);
            if (propertyinfo != null)
            {
                var attributes = (SkipLogAttribute[])propertyinfo.GetCustomAttributes(typeof(SkipLogAttribute), false);
                if (attributes.Length > 0)
                {
                    return true;
                }
            }

            return false;
        }

        public string GetCustomLog(Type tabletype, string propertyname)
        {
            var propertyinfo = tabletype.GetProperty(propertyname);
            if (propertyinfo != null)
            {
                var attributes = (CustomLogAttribute[])propertyinfo.GetCustomAttributes(typeof(CustomLogAttribute), false);
                if (attributes.Length > 0)
                {
                    return attributes.FirstOrDefault().CustomLogValue;
                }
            }

            return null;
        }

        public LogBL(int screenid, int employeeid)
        {
            PointLogContext = new PointLogContext();

            EntityLogSet = new EntityLogSet() {
                EntityLogSetID = Guid.NewGuid(),
                EmployeeID = employeeid,
                ScreenID = screenid,
                TimeStamp = DateTime.Now
            };

            PointLogContext.EntityLogSet.Add(EntityLogSet);
        }

        public static List<EntityLog> GetLogByEntityLogSetID(Guid entitylogsetid)
        {
            using (var context = new PointLogContext())
            {
                return context.EntityLog.Where(it => it.EntityLogSetID == entitylogsetid).ToList();
            }
        }

        public static List<EntityLog> GetLogByTableNameAndPrimaryKey(string tablename, string primarykey)
        {
            using (var context = new PointLogContext())
            {
                return context.EntityLog.Include("EntityLogSet").Where(el => el.EntityLogSet.EntityLog.Any(ell => ell.TableName == tablename && ell.PrimaryKeys == primarykey)).ToList();
            }
        }

        public static List<EntityLogSet> GetEntityLogSetsByTableNameAndPrimaryKey(string tablename, string primarykey)
        {
            using (var context = new PointLogContext())
            {
                return context.EntityLogSet.Where(set => set.EntityLog.Any(log => log.TableName == tablename && log.PrimaryKeys == primarykey)).ToList();
            }
        }


        object[] GetPrimaryKeyValues(DbEntityEntry entry, ObjectContext objectContext)
        {
            var objectStateEntry = objectContext.ObjectStateManager.GetObjectStateEntry(entry.Entity);
            if(objectStateEntry.EntityKey.EntityKeyValues == null)
            {
                return null;
            }

            return objectStateEntry.EntityKey.EntityKeyValues.Select(x => x.Value).ToArray();
        }

        public void LogDBChanges(List<DbEntityEntry> changes, ObjectContext objectContext, ChangeType changetype)
        {
            foreach (var change in changes)
            {
                if (change.Entity is ILoggable loggable)
                {
                    var tabletype = ObjectContext.GetObjectType(change.Entity.GetType()); //solution for proxyname problem
                    var entityName = tabletype.Name; 
                    var primarykeys = GetPrimaryKeyValues(change, objectContext);
                    var primarykeystring = primarykeys == null ? null : String.Join(",", primarykeys);

                    var propertynames = change.State != System.Data.Entity.EntityState.Deleted ?
                                        change.CurrentValues.PropertyNames.ToList() :
                                        change.OriginalValues.PropertyNames.ToList();

                    foreach (string propertyname in propertynames)
                    {
                        var ishidden = IsHidden(tabletype, propertyname);
                        if (ishidden) continue;

                        var currentVal = change.State != System.Data.Entity.EntityState.Deleted ? change.CurrentValues[propertyname]?.ToString() : null;
                        var originalVal = change.State != System.Data.Entity.EntityState.Added ? change.OriginalValues[propertyname]?.ToString() : null;

                        if (currentVal != originalVal || changetype == ChangeType.Added)
                        {
                            writeDbChangeLog(changetype, entityName, String.Join(",", primarykeys), propertyname, originalVal, currentVal, GetCustomLog(tabletype, propertyname));
                        }
                    }
                }
            }

            PointLogContext.SaveChanges();
        }

        private void writeDbChangeLog(ChangeType changeType, string tableName, string primaryKeys, string fieldName, string oldValue, string newValue, string customLog)
        {
            var entitylog = new EntityLog
            {
                EntityLogSetID = EntityLogSet.EntityLogSetID,
                ChangeType = changeType,
                TableName = tableName,
                PrimaryKeys = primaryKeys,
                FieldName = fieldName,
                OldValue = customLog ?? oldValue,
                NewValue = customLog ?? newValue
            };

            PointLogContext.EntityLog.Add(entitylog);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // Protected implementation of Dispose pattern.
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                PointLogContext.Dispose();
            }

            disposed = true;
        }
    }
}
