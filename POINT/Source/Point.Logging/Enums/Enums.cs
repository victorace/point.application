﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Point.Log.Enums
{
    public enum ChangeType
    {
        Added = 0,
        Modified = 1,
        Deleted = 2
    }

}
