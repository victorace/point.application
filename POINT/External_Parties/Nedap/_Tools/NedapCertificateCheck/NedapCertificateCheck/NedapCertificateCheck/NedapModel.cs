﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NedapCertificateCheck
{
    public class NedapModel
    {
        public string CertificateName { get; set; }
        public string PrivateKey { get; set; }
        public string MedewerkerNummer { get; set; }

    }
}
