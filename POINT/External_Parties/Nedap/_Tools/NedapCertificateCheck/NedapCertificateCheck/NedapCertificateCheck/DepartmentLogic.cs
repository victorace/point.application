﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NedapCertificateCheck
{
    public class DepartmentLogic
    {
        private const string query = "SELECT DISTINCT NedapCertificate, NedapCertificatePrivateKey, NedapMedewerkerNummer FROM Department WHERE NedapCertificate IS NOT NULL";
        public IEnumerable<NedapModel> GetAllNedapCertificates()
        {
            var nedapDepartments = new List<NedapModel>();

            var connString = ConfigurationManager.ConnectionStrings["PointDB"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();
                using (SqlCommand sqlCommand = new SqlCommand(query, conn))
                {

                    var reader = sqlCommand.ExecuteReader();
                    
                    while(reader.Read())
                    {
                        nedapDepartments.Add(new NedapModel()
                        {
                            CertificateName = reader["NedapCertificate"].ToString(),
                            PrivateKey = reader["NedapCertificatePrivateKey"].ToString(),
                            MedewerkerNummer = reader["NedapMedewerkerNummer"].ToString()
                        });
                    }

                    reader.Close();
                }
                conn.Close();
            }

            return nedapDepartments;
        }
    }
}
