﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace NedapCertificateCheck
{
    class Program
    {
        static void Main(string[] args)
        {
            if (Boolean.Parse(ConfigurationManager.AppSettings["CheckManually"]))
            {
                checkManually();
            }
            else
            {
                checkAutomatically();
            }
        }

        private static void checkManually()
        {
            foreach(var file in new DirectoryInfo(ConfigurationManager.AppSettings["NedapCertificateDirectory"]).GetFiles("*.pfx"))
            {
                Console.WriteLine("Found certificate: " + file.Name);
                Console.Write("Enter password: ");
                string password = Console.ReadLine();

                try
                {
                    var certificate = getCertificate(file.Name, password);

                    int totalDays = certificate.NotAfter.Subtract(DateTime.Now).Days;
                    Console.WriteLine("Valid till: {0}, expires in {1} days.", certificate.NotAfter.Date.ToString("dd-MM-yyyy"), totalDays);
                    if (totalDays <= 14)
                    {
                        Console.WriteLine("!!! certificate will soon expire !!!");
                    }
                }
                catch(Exception ex)
                {
                    Console.WriteLine("Exception: " + ex.Message);
                }
                Console.WriteLine();
            }
            Console.WriteLine("Done! Press any key to exit.");
            Console.ReadKey();

        }

        private static void checkAutomatically()
        {
            var dep = new DepartmentLogic();

            var certs = dep.GetAllNedapCertificates();
            var invalidCerts = new List<string>();

            var intervals = ConfigurationManager.AppSettings["intervals"].Split(',').Select(i => Int32.Parse(i));
            var today = DateTime.Now.Date;

            foreach (var cert in certs)
            {
                var c = getCertificate(cert.CertificateName, cert.PrivateKey);
                if (c != null)
                {
                    var invalidDate = c.NotAfter.Date;
                    foreach (var i in intervals)
                    {
                        if (invalidDate == today.AddDays(i))
                        {
                            invalidCerts.Add(cert.CertificateName + ": " + c.NotAfter.ToString("dd MMM yyyy"));
                            break;
                        }
                    }
                }
            }

            if (invalidCerts.Count > 0)
            {
                var mail = new MailLogic();
                mail.SendNotification(invalidCerts);
            }
        }

        private static X509Certificate2 getCertificate(string certificateName, string password)
        {
            string certificateDirectory = ConfigurationManager.AppSettings["NedapCertificateDirectory"];
            string certificatePath = Path.Combine(certificateDirectory, certificateName);
            if (!System.IO.File.Exists(certificatePath))
            {
                //errormessage = String.Format("Certificate '{0}' not found in '{1}'", certificateName, certificateDirectory);
                return null;
            }
            return new X509Certificate2(certificatePath, password, X509KeyStorageFlags.PersistKeySet);
        }
    }
}
