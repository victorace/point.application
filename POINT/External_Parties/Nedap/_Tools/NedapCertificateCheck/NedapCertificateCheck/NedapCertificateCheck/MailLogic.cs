﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace NedapCertificateCheck
{
    public class MailLogic
    {
        public void SendNotification(List<string> certificates)
        {
            var serverAddress = ConfigurationManager.AppSettings["SmtpServer"];

            using (var smtp = new SmtpClient(serverAddress))
            {
                var msg = new MailMessage();
                msg.From = new MailAddress(ConfigurationManager.AppSettings["fromAddressPoint"]);
                msg.To.Add(ConfigurationManager.AppSettings["notificationsTo"]);
                msg.Subject = ConfigurationManager.AppSettings["subjectLine"];
                
                var sb = new StringBuilder();
                foreach (var cert in certificates)
                {
                    sb.AppendLine(cert);
                }

                msg.Body = sb.ToString();
                
                smtp.Send(msg);
            }
        }
    }
}