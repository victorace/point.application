﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleNedap.Models
{
    public static class Deserialize
    {
        public static Employee Employee(string employee)
        {
            if (String.IsNullOrEmpty(employee))
            {
                return null;
            }

            return JsonConvert.DeserializeObject<Employee>(employee);
        }

        public static Client DeSerializeClient(string client)
        {
            if (String.IsNullOrEmpty(client))
            {
                return null;
            }

            return JsonConvert.DeserializeObject<Client>(client);
        }

        public static Document Document(string document)
        {
            if (String.IsNullOrEmpty(document))
            {
                return null;
            }

            return JsonConvert.DeserializeObject<Document>(document);
        }

        public static List<Document> Documents(string documents)
        {
            if (String.IsNullOrEmpty(documents))
            {
                return new List<Document>();
            }

            return JsonConvert.DeserializeObject<ListDocumentList>(documents)
                .Documents.ToList();
        }
    }
}
