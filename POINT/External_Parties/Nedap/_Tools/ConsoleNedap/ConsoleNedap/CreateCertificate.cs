﻿using System.IO;

namespace ConsoleNedap
{
    public class CreateCertificate : BaseClass
    {
        protected string WindowsDir = @"c:\\windows\\system32\\";

        public void CreateCSR()
        {


            var cmd = new System.Diagnostics.Process();

            cmd.StartInfo.UseShellExecute = false;
            cmd.StartInfo.RedirectStandardOutput = true;
            cmd.StartInfo.FileName = Path.Combine(WindowsDir, "certreq.exe");

            var currentDir = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            var contents = File.ReadAllText(Path.Combine(currentDir, "CreateRequest.inf"));
            contents = contents.Replace("**CN**", CertificateName);
            var requestfile = Path.Combine(currentDir, "request.inf");
            File.WriteAllText(requestfile, contents);

            cmd.StartInfo.Arguments = $@"-new -f ""{requestfile}"" {CertificateName}.txt"; 
            cmd.Start();
            var output = cmd.StandardOutput.ReadToEnd();
            cmd.WaitForExit();

            AddToLogInfo($"Created csr: {CertificateName}.txt");
            AddToLog($"Output: {output.Trim("\r\n".ToCharArray())}");

            File.Delete(requestfile);
        }

    }
}
