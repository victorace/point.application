﻿using ConsoleNedap.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;

namespace ConsoleNedap
{
    public class Process : BaseClass 
    {
        public Employee Employee { get; set; }
        public Client Client { get; set; }
        public Document Document { get; set; }
        public List<Document> NedapDocumentsData { get; set; }

        public Process()
        {
            Employee = new Employee();
            Client = new Client();
            NedapDocumentsData = new List<Document>();
        }

        public void TestPing(string environmentLetter)
        {
            var url = Path.Combine(GetEnvironment(environmentLetter), "ping").Replace("\\", "/");
            doRequest(url);
        }

        public void Accessrights()
        {
            var url = Path.Combine(BaseUrl, "f", "accessrights").Replace("\\", "/");
            doRequest(url);
        }

        public void AdminVersion()
        {
            var url = Path.Combine(BaseUrl, "f", "admin", "version").Replace("\\", "/");
            doRequest(url);
        }

        public void ClientsID(string id)
        {
            var url = Path.Combine(BaseUrl, "f", "clients", id).Replace("\\", "/");
            string client = doRequest(url);
            Client = Deserialize.DeSerializeClient(client);
        }

        public void ClientsByIdentificationNo(string id)
        {
            var url = Path.Combine(BaseUrl, "f", "clients", "by_identification_no", id).Replace("\\", "/");
            doRequest(url);
        }


        public void ClientsBSN(string id, string interfacer = "f")
        {
            var url = Path.Combine(BaseUrl, interfacer, "clients", id, "bsn").Replace("\\", "/");
            doRequest(url);
        }

        public void ClientsByBSN(string id, string interfacer = "f")
        {
            var url = Path.Combine(BaseUrl, interfacer, "clients", "by_bsn", id).Replace("\\", "/");
            doRequest(url);
        }

        public void ClientsMainAddress(string id)
        {
            var url = Path.Combine(BaseUrl, "f", "clients", id, "main_address").Replace("\\", "/");
            doRequest(url);

        }


        public void DossierActions(string id)
        {
            var url = Path.Combine(BaseUrl, "f", "dossier", "actions", id).Replace("\\", "/");
            doRequest(url);
        }


        public void DocumentsByClientEmployeeID(string clientID, string employeeID)
        {
            var url = Path.Combine(BaseUrl, "t", "documents", "by_client", clientID).Replace("\\","/");
            url += "?employee_id=" + employeeID;
            var documents = doRequest(url);
            NedapDocumentsData = Deserialize.Documents(documents);
        }

        public void DocumentsByClientDocID(string clientID, string documentID)
        {
            var url = Path.Combine(BaseUrl, "t", "documents", "by_client", clientID).Replace("\\", "/");
            url += "?doc_id=" + documentID; // Not completely working from nedap-side, getting them all back ...
            var documents = doRequest(url);
            NedapDocumentsData = Deserialize.Documents(documents);
            Document = NedapDocumentsData.FirstOrDefault(ndd => ndd.Id == Int32.Parse(documentID));
        }

        public void Employees(string employeeID)
        {
            var url = Path.Combine(BaseUrl, "f", "employees", employeeID).Replace("\\", "/");
            string employee = doRequest(url);
            Employee = Deserialize.Employee(employee);
            
        }

        public void EmployeesByIdentificationNo(string id)
        {
            var url = Path.Combine(BaseUrl, "t", "employees", "by_identification_no", id).Replace("\\", "/");
            doRequest(url);
        }

        public void TestEmployee()
        {
            var url = BaseUrl + "/f/employees/search?query=lastname eq 'test'";
            doRequest(url);
        }


        private string doRequest(string url)
        {
            var response = "";
            try 
            {
                var request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "GET";
                request.Accept = "application/json";

                var cert = new X509Certificate2(CertificateFile, Password, X509KeyStorageFlags.PersistKeySet);
                request.ClientCertificates.Add(cert);
                
                AddToLog("Starting request to: ");
                AddToLogInfo(url);

                var webresponse = request.GetResponse();

                var streamreader = new StreamReader(webresponse.GetResponseStream());
                response = streamreader.ReadToEnd();

                AddToLog("Response:");
                AddToLog(response);
            }
            catch (WebException wex)
            {
                var httpwebresponse = (HttpWebResponse)wex.Response;
                AddToLogError($"WebException: statuscode [{httpwebresponse.StatusCode}], message [{wex.Message}]");
            }
            catch (Exception ex)
            {
                AddToLogError($"Exception: {ex.Message}");
            }
            AddToLog();

            return response;
        }
    }


}
