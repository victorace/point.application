﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Net.Http.Headers;
using System.Configuration;
using ConsoleNedap.Models;
using System.Xml.Linq;

namespace ConsoleNedap
{
    public class ProcessDocument : BaseClass
    {

        private Client nedapClientData = new Client();
        private Employee nedapEmployeeData = new Employee();
        public Document Document = null;
        private X509Certificate2 x509certificate = null;
        private string errormessage = "";

        public string ErrorMessage { get { return errormessage; } }

        public ProcessDocument(Client nedapClientData, Employee nedapEmployeeData)
        {
            this.nedapClientData = nedapClientData;
            this.nedapEmployeeData = nedapEmployeeData;
            x509certificate = new X509Certificate2(CertificateFile, Password, X509KeyStorageFlags.PersistKeySet);
        }

        public bool PostDocument()
        {
            string genericfilename = $"POINT_Testdocument_{DateTime.Now.ToString("yyyyMMddHHmm")}.pdf"; // POINT gets this from GenericFile.FileName

            string filename = System.IO.Path.GetFileName(genericfilename)?.Replace(System.IO.Path.GetInvalidFileNameChars().ToString() + Path.GetInvalidPathChars().ToString(), "_");
            string description = $"Bijlage '{filename}'{getDescriptionSuffix()}";

            byte[] filecontents = System.IO.File.ReadAllBytes(System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Test.pdf"));

            return PostDocument(description, filename, filecontents);
        }

        public bool PostDocument(string description, string filename, byte[] fileContents)
        {
            WebRequestHandler handler = new WebRequestHandler();
            handler.ClientCertificates.Add(x509certificate);

            try
            {
                using (var httpclient = new HttpClient(handler))
                {
                    using (var content = new MultipartFormDataContent())
                    {
                        var url = getFullUrl("t", "documents", null);
                        var requesturi = String.Concat("?client_id=", nedapClientData.Id);

                        var request = (HttpWebRequest)WebRequest.Create(url);

                        httpclient.BaseAddress = new Uri(url);

                        XDocument xdoc = new XDocument(
                                             new XDeclaration("1.0", "utf-8", "yes"),
                                             new XElement("document",
                                                 new XElement("clientObjectId", nedapClientData.Id),
                                                 new XElement("employeeObjectId", nedapEmployeeData.Id),
                                                 new XElement("description", description),
                                                 new XElement("rightSelection", "All"),
                                                 new XElement("status", "1"),
                                                 new XElement("fileName", filename)
                                             )
                                         );

                        var fileContentXML = new StringContent(xdoc.Declaration.ToString() + Environment.NewLine + xdoc.ToString());
                        fileContentXML.Headers.ContentDisposition = new ContentDispositionHeaderValue("metadata") { DispositionType = "form-data", Name = "metadata" };
                        fileContentXML.Headers.ContentType = new MediaTypeHeaderValue("application/xml");

                        var fileContentPDF = new ByteArrayContent(fileContents);
                        fileContentPDF.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment") { FileName = filename, DispositionType = "form-data", Name = "attachment" };
                        fileContentPDF.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");

                        content.Add(fileContentXML);
                        content.Add(fileContentPDF);

                        var response = httpclient.PostAsync(requesturi, content).Result;
                        var result = response.Content.ReadAsStringAsync().Result; // deserialize <<<<<
                        Document = Deserialize.Document(result);

                        //saveHandledToLog(uow, result, filename, (url + requesturi));

                    }
                }
            }
            catch (WebException wex)
            {
                var httpwebresponse = (HttpWebResponse)wex.Response;
                errormessage = $"WebException in Nedap.postDocument(). Statuscode: [{httpwebresponse.StatusCode}]. Message: [{wex.Message}]";
                return false;
            }
            catch (Exception ex)
            {
                errormessage = $"Exception in Nedap.postDocument(). Exception: [{ex.Message}]";
                return false;
            }

            return true;
        }
        
        public void DownloadDocument(string clientID, string documentID)
        {
            var url = System.IO.Path.Combine(BaseUrl, "t", "documents", "download").Replace("\\", "/");
            url += String.Concat("?client_id=", clientID, "&doc_id=", documentID);
            doRequestBinary(url, @"c:\temp\test.pdf"); //<<< retrieve filename from the other API-call
        }


        private void doRequestBinary(string url, string filename)
        {
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "GET";
                request.Accept = "application/json";

                var cert = new X509Certificate2(CertificateFile, Password, X509KeyStorageFlags.PersistKeySet);
                request.ClientCertificates.Add(cert);

                AddToLog("Starting request to: " + url);

                var webresponse = request.GetResponse();

                int bytesRead;
                byte[] buffer = new byte[1024];

                using (var memorystream = new System.IO.MemoryStream())
                {
                    using (var binaryreader = new System.IO.BinaryReader(webresponse.GetResponseStream()))
                    {
                        while ((bytesRead = binaryreader.Read(buffer, 0, buffer.Length)) > 0)
                        {
                            memorystream.Write(buffer, 0, bytesRead);
                        }
                    }
                    System.IO.File.WriteAllBytes(filename, memorystream.ToArray());
                    memorystream.Close();
                    AddToLog("Written file " + filename);
                }

            }
            catch (Exception ex)
            {
                AddToLog("Exception: " + ex.Message);
            }
            AddToLog();
        }

        private string getDescriptionSuffix()
        {
            return $" verzonden vanuit POINT op {DateTime.Now.ToString("dd-MM-yyyy")} om {DateTime.Now.ToString("HH:mm")}";
        }

    }
}
