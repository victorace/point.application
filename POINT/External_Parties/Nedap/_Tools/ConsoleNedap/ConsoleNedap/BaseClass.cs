﻿using System;
using System.Configuration;
using System.IO;
using System.Security.Cryptography.X509Certificates;

namespace ConsoleNedap
{
    public class BaseClass
    {
        public void AddToLog()
        {
            addToLog("");
        }

        public void AddToLog(string message)
        {
            Console.ForegroundColor = ConsoleColor.Gray;
            addToLog(message);
        }

        public void AddToLogInfo(string message)
        {
            Console.ForegroundColor = ConsoleColor.White;
            addToLog(message);
        }

        public void AddToLogError(string message)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            addToLog(message);
        }

        private void addToLog(string message)
        {
            Console.WriteLine(message);
        }

        public string[] CustomerData
        {
            get
            {
                string customerdata = ConfigurationManager.AppSettings["CustomerData"] as string;
                if (String.IsNullOrEmpty(customerdata))
                {
                    throw new Exception("AppSettings 'CustomerData' not set");
                }
                return customerdata.Split(new[] { ',' });
            }

        }

        public string BaseUrl
        {
            get
            {
                string baseurl = "https://api-development.ons.io";
                if (!String.IsNullOrEmpty(CustomerData[0]))
                {
                    baseurl = GetEnvironment(CustomerData[0]);
                }
                return baseurl;
            }
        }

        public string GetEnvironment(string environmentLetter)
        {
            string environment = "";
            switch (environmentLetter)
            {
                case "P":
                    environment = "https://api.ons.io"; break;
                case "A":
                    environment = "https://api-staging.ons.io"; break;
                case "T":
                default:
                    environment = "https://api-development.ons.io"; break;
            }
            return environment;
        }

        public string CertificateFile
        {
            get
            {
                string certificatedirectory = ConfigurationManager.AppSettings["CertificateDirectory"] as string;
                if (String.IsNullOrEmpty(certificatedirectory))
                {
                    throw new Exception("AppSettings 'CertificateDirectory' not set");
                }
                if (!certificatedirectory.EndsWith(@"\"))
                {
                    certificatedirectory += @"\";
                }

                string certificatefile = $"{certificatedirectory}{CertificateName}.pfx";
                if (!File.Exists(certificatefile))
                {
                    throw new Exception($"Certificate '{certificatefile}' cannot be found!");
                }
                return certificatefile;
            }
        }

        public string CertificateName
        {
            get
            {
                return $"POINT-{CustomerData[1]}-TXXVMAP0001{CustomerData[0]}";
            }
        }

        public string Password
        {
            get
            {
                string password = CustomerData[2];

                if (String.IsNullOrWhiteSpace(password))
                {
                    throw new Exception($"Password not set in AppSetting 'CustomerData'");
                }

                return password;
            }
        }

        public string TestDataEmployee
        {
            get
            {
                string testdataemployee = ConfigurationManager.AppSettings["TestDataEmployee"] as string;

                if (String.IsNullOrWhiteSpace(testdataemployee))
                {
                    throw new Exception("AppSettings 'TestDataEmployee' not set");
                }

                return testdataemployee;
            }
        }

        public string TestDataBSN
        {
            get
            {
                string testdatabsn = ConfigurationManager.AppSettings["TestDataBSN"] as string;

                if (String.IsNullOrWhiteSpace(testdatabsn))
                {
                    throw new Exception("AppSettings 'TestDataBSN' not set");
                }

                return testdatabsn;
            }
        }

        public bool TestDocument
        {
            get
            {
                string testdocument = ConfigurationManager.AppSettings["TestDocument"] as string;

                if (String.IsNullOrWhiteSpace(testdocument))
                {
                    return false;
                }

                return Boolean.Parse(testdocument);

            }
        }

        public X509Certificate2 Certificate()
        {
            var cert = new X509Certificate2(CertificateFile, Password, X509KeyStorageFlags.PersistKeySet);
            return cert;
        }

        protected string getFullUrl(string action, int id)
        {
            return getFullUrl("f", action, id);
        }
        protected string getFullUrl(string action, string id)
        {
            return getFullUrl("f", action, id);
        }
        protected string getFullUrl(string apitype, string action, int id)
        {
            return getFullUrl(apitype, action, id.ToString());
        }
        protected string getFullUrl(string apitype, string action, string id)
        {
            return System.IO.Path.Combine(BaseUrl, apitype, action, id ?? "").Replace("\\", "/").TrimEnd('/');
        }

    }
}
