﻿using ConsoleNedap.Models;
using System;

namespace ConsoleNedap
{
    class Program
    {

        // Zie https://development.ioservice.net/ voor test-data
        // Login staat in RDM Techxx\Techxx Healthcare\Non Azure 
        //
        // API documentatie: https://api-development.ons.io/docs/swagger/?mode=ext

        static BaseClass baseclass = new BaseClass();
        static Process process = new Process();


        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                performBasicTests();
                if (baseclass.TestDocument)
                {
                    performDocumentTest(process.Client, process.Employee);
                }
            }
            else
            {
                if (args[0].Equals("Ping", StringComparison.InvariantCultureIgnoreCase))
                {
                    performPings();
                }
                else if (args[0].Equals("Create", StringComparison.InvariantCultureIgnoreCase))
                {
                    new CreateCertificate().CreateCSR();
                }
            }


            baseclass.AddToLogInfo($"Done @{DateTime.Now.ToString("HH:mm:ss")}");
            Console.ReadKey();



        }

        static private void performPings()
        {
            baseclass.AddToLogInfo("Not working anymore (as of 15-08-2017)");

            baseclass.AddToLog("Pinging to TEST");
            process.TestPing("T");
            baseclass.AddToLog("Pinging to ACC");
            process.TestPing("A");
            baseclass.AddToLog("Pinging to PROD");
            process.TestPing("P");

        }

        static private void performBasicTests()
        {

            baseclass.AddToLog("Using certificate: " + Environment.NewLine + baseclass.CertificateFile);
            baseclass.AddToLogInfo("Expiring: " + baseclass.Certificate().NotAfter.ToString("dd-MM-yyyy"));
            baseclass.AddToLogInfo("CustomerCode: " + baseclass.CustomerData[1]);
            baseclass.AddToLogInfo("Environment: " + baseclass.CustomerData[0] + " = " + baseclass.BaseUrl);
            baseclass.AddToLog();

            process.Employees(baseclass.TestDataEmployee);

            baseclass.AddToLogInfo("NedapEmployee: " + baseclass.TestDataEmployee);
            if (process.Employee == null)
            {
                baseclass.AddToLogError("No employee found with number: " + baseclass.TestDataEmployee);
                return;
            }
            foreach (var prop in process.Employee.GetType().GetProperties())
            {
                baseclass.AddToLog(String.Format("{0}: {1}", prop.Name, prop.GetValue(process.Employee, null)));
            }
            baseclass.AddToLog();


            process.ClientsID(baseclass.TestDataBSN);

            baseclass.AddToLogInfo("NedapClient:" + baseclass.TestDataBSN);
            if (process.Client == null)
            {
                baseclass.AddToLogError("No client found with BSN: " + baseclass.TestDataBSN);
                return;
            }

            foreach (var prop in process.Client.GetType().GetProperties())
            {
                baseclass.AddToLog(String.Format("{0}: {1}", prop.Name, prop.GetValue(process.Client, null)));
            }
            baseclass.AddToLog();


            baseclass.AddToLogInfo("NedapDocuments:");
            if (process.Client.Id > 0 && process.Employee.Id > 0)
            {
                process.DocumentsByClientEmployeeID(process.Client.Id.ToString(), process.Employee.Id.ToString());
                foreach (var nedapdocument in process.NedapDocumentsData)
                {
                    foreach (var prop in nedapdocument.GetType().GetProperties())
                    {
                        baseclass.AddToLog(String.Format("{0}: {1}", prop.Name, prop.GetValue(nedapdocument, null)));
                    }
                    baseclass.AddToLog("-----");
                }
            }
            baseclass.AddToLog();
        }

        static private void performDocumentTest(Client nedapClientData, Employee nedapEmployeeData)
        {
            ProcessDocument processdocument = new ProcessDocument(nedapClientData, nedapEmployeeData);
            if (!processdocument.PostDocument())
            {
                baseclass.AddToLogError("Error: " + processdocument.ErrorMessage);
            }
            else
            {
                baseclass.AddToLog($"Uploaded document.");
                baseclass.AddToLogInfo($"DocumentID: {processdocument.Document.Id}, Description: {processdocument.Document.Description}");
                baseclass.AddToLog();
                process.DocumentsByClientDocID(nedapClientData.Id.ToString(), processdocument.Document.Id.ToString());
                var nedapdocument = process.Document;
                foreach (var prop in nedapdocument.GetType().GetProperties())
                {
                    baseclass.AddToLog(String.Format("{0}: {1}", prop.Name, prop.GetValue(nedapdocument, null)));
                }
            }

        }
    }
}
