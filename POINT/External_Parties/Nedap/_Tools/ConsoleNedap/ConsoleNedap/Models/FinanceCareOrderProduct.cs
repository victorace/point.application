// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace ConsoleNedap.Models
{
    using Newtonsoft.Json;
    using System.Linq;

    public partial class FinanceCareOrderProduct
    {
        /// <summary>
        /// Initializes a new instance of the FinanceCareOrderProduct class.
        /// </summary>
        public FinanceCareOrderProduct()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the FinanceCareOrderProduct class.
        /// </summary>
        /// <param name="quantity">Deprecated since: 14-03-2019</param>
        /// <param name="quantityUnit">Deprecated since: 14-03-2019</param>
        public FinanceCareOrderProduct(System.DateTime? beginDate = default(System.DateTime?), long? careOrderId = default(long?), System.DateTime? clippedBeginDate = default(System.DateTime?), System.DateTime? clippedEndDate = default(System.DateTime?), string comments = default(string), long? debtorId = default(long?), System.DateTime? endDate = default(System.DateTime?), long? financeTypeId = default(long?), string functietype = default(string), long? id = default(long?), string instellingsCode = default(string), string magnitudeFrequency = default(string), int? magnitudeQuantityValue = default(int?), int? magnitudeUnitQuantity = default(int?), string magnitudeUnitType = default(string), long? productId = default(long?), int? quantity = default(int?), string quantityUnit = default(string), string userIdentifier = default(string))
        {
            BeginDate = beginDate;
            CareOrderId = careOrderId;
            ClippedBeginDate = clippedBeginDate;
            ClippedEndDate = clippedEndDate;
            Comments = comments;
            DebtorId = debtorId;
            EndDate = endDate;
            FinanceTypeId = financeTypeId;
            Functietype = functietype;
            Id = id;
            InstellingsCode = instellingsCode;
            MagnitudeFrequency = magnitudeFrequency;
            MagnitudeQuantityValue = magnitudeQuantityValue;
            MagnitudeUnitQuantity = magnitudeUnitQuantity;
            MagnitudeUnitType = magnitudeUnitType;
            ProductId = productId;
            Quantity = quantity;
            QuantityUnit = quantityUnit;
            UserIdentifier = userIdentifier;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "beginDate")]
        public System.DateTime? BeginDate { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "careOrderId")]
        public long? CareOrderId { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "clippedBeginDate")]
        public System.DateTime? ClippedBeginDate { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "clippedEndDate")]
        public System.DateTime? ClippedEndDate { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "comments")]
        public string Comments { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "debtorId")]
        public long? DebtorId { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "endDate")]
        public System.DateTime? EndDate { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "financeTypeId")]
        public long? FinanceTypeId { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "functietype")]
        public string Functietype { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "id")]
        public long? Id { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "instellingsCode")]
        public string InstellingsCode { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "magnitudeFrequency")]
        public string MagnitudeFrequency { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "magnitudeQuantityValue")]
        public int? MagnitudeQuantityValue { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "magnitudeUnitQuantity")]
        public int? MagnitudeUnitQuantity { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "magnitudeUnitType")]
        public string MagnitudeUnitType { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "productId")]
        public long? ProductId { get; set; }

        /// <summary>
        /// Gets or sets deprecated since: 14-03-2019
        /// </summary>
        [JsonProperty(PropertyName = "quantity")]
        public int? Quantity { get; set; }

        /// <summary>
        /// Gets or sets deprecated since: 14-03-2019
        /// </summary>
        [JsonProperty(PropertyName = "quantityUnit")]
        public string QuantityUnit { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "userIdentifier")]
        public string UserIdentifier { get; set; }

    }
}
