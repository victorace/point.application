// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace ConsoleNedap.Models
{
    using Newtonsoft.Json;
    using System.Linq;

    public partial class PortalEmployeePortalProfile
    {
        /// <summary>
        /// Initializes a new instance of the PortalEmployeePortalProfile
        /// class.
        /// </summary>
        public PortalEmployeePortalProfile()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the PortalEmployeePortalProfile
        /// class.
        /// </summary>
        /// <param name="carePlanEnabled">Deprecated since: 14-03-2019</param>
        public PortalEmployeePortalProfile(bool? carePlanEnabled = default(bool?), long? employeeId = default(long?), string employeePortalUrl = default(string), string externalWebsiteName = default(string), bool? facebookEnabled = default(bool?), bool? rosterEnabled = default(bool?), bool? selfAvailabilityEnabled = default(bool?), bool? selfRosteringEnabled = default(bool?))
        {
            CarePlanEnabled = carePlanEnabled;
            EmployeeId = employeeId;
            EmployeePortalUrl = employeePortalUrl;
            ExternalWebsiteName = externalWebsiteName;
            FacebookEnabled = facebookEnabled;
            RosterEnabled = rosterEnabled;
            SelfAvailabilityEnabled = selfAvailabilityEnabled;
            SelfRosteringEnabled = selfRosteringEnabled;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// Gets or sets deprecated since: 14-03-2019
        /// </summary>
        [JsonProperty(PropertyName = "carePlanEnabled")]
        public bool? CarePlanEnabled { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "employeeId")]
        public long? EmployeeId { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "employeePortalUrl")]
        public string EmployeePortalUrl { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "externalWebsiteName")]
        public string ExternalWebsiteName { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "facebookEnabled")]
        public bool? FacebookEnabled { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "rosterEnabled")]
        public bool? RosterEnabled { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "selfAvailabilityEnabled")]
        public bool? SelfAvailabilityEnabled { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "selfRosteringEnabled")]
        public bool? SelfRosteringEnabled { get; set; }

    }
}
