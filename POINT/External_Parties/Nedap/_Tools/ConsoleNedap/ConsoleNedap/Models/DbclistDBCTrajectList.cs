// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace ConsoleNedap.Models
{
    using Newtonsoft.Json;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    public partial class DbclistDBCTrajectList
    {
        /// <summary>
        /// Initializes a new instance of the DbclistDBCTrajectList class.
        /// </summary>
        public DbclistDBCTrajectList()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the DbclistDBCTrajectList class.
        /// </summary>
        public DbclistDBCTrajectList(IList<DbcDBCTraject> dbcTrajects = default(IList<DbcDBCTraject>))
        {
            DbcTrajects = dbcTrajects;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "dbcTrajects")]
        public IList<DbcDBCTraject> DbcTrajects { get; set; }

    }
}
