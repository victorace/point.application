// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace ConsoleNedap.Models
{
    using Microsoft.Rest;
    using Newtonsoft.Json;
    using System.Linq;

    public partial class CarepathCarePathTemplate
    {
        /// <summary>
        /// Initializes a new instance of the CarepathCarePathTemplate class.
        /// </summary>
        public CarepathCarePathTemplate()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the CarepathCarePathTemplate class.
        /// </summary>
        /// <param name="name">The name of this template</param>
        /// <param name="uuid">The UUID of this template</param>
        public CarepathCarePathTemplate(string name, string uuid)
        {
            Name = name;
            Uuid = uuid;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// Gets or sets the name of this template
        /// </summary>
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the UUID of this template
        /// </summary>
        [JsonProperty(PropertyName = "uuid")]
        public string Uuid { get; set; }

        /// <summary>
        /// Validate the object.
        /// </summary>
        /// <exception cref="ValidationException">
        /// Thrown if validation fails
        /// </exception>
        public virtual void Validate()
        {
            if (Name == null)
            {
                throw new ValidationException(ValidationRules.CannotBeNull, "Name");
            }
            if (Uuid == null)
            {
                throw new ValidationException(ValidationRules.CannotBeNull, "Uuid");
            }
        }
    }
}
