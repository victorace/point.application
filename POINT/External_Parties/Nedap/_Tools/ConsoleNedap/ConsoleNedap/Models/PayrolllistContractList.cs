// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace ConsoleNedap.Models
{
    using Newtonsoft.Json;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    public partial class PayrolllistContractList
    {
        /// <summary>
        /// Initializes a new instance of the PayrolllistContractList class.
        /// </summary>
        public PayrolllistContractList()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the PayrolllistContractList class.
        /// </summary>
        public PayrolllistContractList(IList<PayrollContract> contracts = default(IList<PayrollContract>))
        {
            Contracts = contracts;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "contracts")]
        public IList<PayrollContract> Contracts { get; set; }

    }
}
