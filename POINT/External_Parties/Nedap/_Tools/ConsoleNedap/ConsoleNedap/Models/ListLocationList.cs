// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace ConsoleNedap.Models
{
    using Newtonsoft.Json;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    public partial class ListLocationList
    {
        /// <summary>
        /// Initializes a new instance of the ListLocationList class.
        /// </summary>
        public ListLocationList()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the ListLocationList class.
        /// </summary>
        public ListLocationList(IList<Location> locations = default(IList<Location>))
        {
            Locations = locations;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "locations")]
        public IList<Location> Locations { get; set; }

    }
}
