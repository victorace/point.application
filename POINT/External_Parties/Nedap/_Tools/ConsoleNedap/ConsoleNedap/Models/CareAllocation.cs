// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace ConsoleNedap.Models
{
    using Microsoft.Rest;
    using Microsoft.Rest.Serialization;
    using Newtonsoft.Json;
    using System.Linq;

    public partial class CareAllocation
    {
        /// <summary>
        /// Initializes a new instance of the CareAllocation class.
        /// </summary>
        public CareAllocation()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the CareAllocation class.
        /// </summary>
        /// <param name="destination">&lt;pre&gt;
        /// Possible values:
        /// NVT(0, "N.v.t"),
        /// EIGEN_OMGEVING("Eigen omgeving"),
        /// EIGEN_OMGEVING_DAGBEHANDELING("Eigen omgeving met dagbehandeling"),
        /// INSTELLING_AWBZ("Instelling (AWBZ) of ziekenhuis"),
        /// INSTELLING_NIET_AWBZ("Instelling (niet-AWBZ)");
        /// &lt;/pre&gt;</param>
        /// <param name="reason">&lt;pre&gt;
        /// Possible values:
        /// IN_CARE("N.v.t."),
        /// DISMISS("Ontslag (intramuraal)"),
        /// DEATH("Overlijden"),
        /// TEMP_GONE("Tijdelijke afwezigheid"),
        /// END_OF_CARE("Beëindiging zorg"),
        /// TRANSFER("Overplaatsing"),
        /// END_OF_CARE_FUNCTION_END("Einde zorg (functies intrekken)"),
        /// END_OF_CARE_FUNCTION_OPEN("Einde zorg (functies openlaten)"),
        /// TRANSFER_NO_MAZ("Overplaatsing (er is geen Maz)");
        /// &lt;/pre&gt;</param>
        public CareAllocation(System.DateTime? beginDate = default(System.DateTime?), long? clientId = default(long?), string comments = default(string), string destination = default(string), System.DateTime? endDate = default(System.DateTime?), long? id = default(long?), string reason = default(string))
        {
            BeginDate = beginDate;
            ClientId = clientId;
            Comments = comments;
            Destination = destination;
            EndDate = endDate;
            Id = id;
            Reason = reason;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// </summary>
        [JsonConverter(typeof(DateJsonConverter))]
        [JsonProperty(PropertyName = "beginDate")]
        public System.DateTime? BeginDate { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "clientId")]
        public long? ClientId { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "comments")]
        public string Comments { get; set; }

        /// <summary>
        /// Gets or sets &amp;lt;pre&amp;gt;
        /// Possible values:
        /// NVT(0, "N.v.t"),
        /// EIGEN_OMGEVING("Eigen omgeving"),
        /// EIGEN_OMGEVING_DAGBEHANDELING("Eigen omgeving met dagbehandeling"),
        /// INSTELLING_AWBZ("Instelling (AWBZ) of ziekenhuis"),
        /// INSTELLING_NIET_AWBZ("Instelling (niet-AWBZ)");
        /// &amp;lt;/pre&amp;gt;
        /// </summary>
        [JsonProperty(PropertyName = "destination")]
        public string Destination { get; set; }

        /// <summary>
        /// </summary>
        [JsonConverter(typeof(DateJsonConverter))]
        [JsonProperty(PropertyName = "endDate")]
        public System.DateTime? EndDate { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "id")]
        public long? Id { get; set; }

        /// <summary>
        /// Gets or sets &amp;lt;pre&amp;gt;
        /// Possible values:
        /// IN_CARE("N.v.t."),
        /// DISMISS("Ontslag (intramuraal)"),
        /// DEATH("Overlijden"),
        /// TEMP_GONE("Tijdelijke afwezigheid"),
        /// END_OF_CARE("Beëindiging zorg"),
        /// TRANSFER("Overplaatsing"),
        /// END_OF_CARE_FUNCTION_END("Einde zorg (functies intrekken)"),
        /// END_OF_CARE_FUNCTION_OPEN("Einde zorg (functies openlaten)"),
        /// TRANSFER_NO_MAZ("Overplaatsing (er is geen Maz)");
        /// &amp;lt;/pre&amp;gt;
        /// </summary>
        [JsonProperty(PropertyName = "reason")]
        public string Reason { get; set; }

    }
}
