// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace ConsoleNedap.Models
{
    using Newtonsoft.Json;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    public partial class AgendaAgendaClientInvitation
    {
        /// <summary>
        /// Initializes a new instance of the AgendaAgendaClientInvitation
        /// class.
        /// </summary>
        public AgendaAgendaClientInvitation()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the AgendaAgendaClientInvitation
        /// class.
        /// </summary>
        public AgendaAgendaClientInvitation(bool? accepted = default(bool?), long? careOrderId = default(long?), long? clientId = default(long?), long? employeeId = default(long?), long? hourTypeId = default(long?), long? locationId = default(long?), IList<int?> surchargeIds = default(IList<int?>), long? teamId = default(long?))
        {
            Accepted = accepted;
            CareOrderId = careOrderId;
            ClientId = clientId;
            EmployeeId = employeeId;
            HourTypeId = hourTypeId;
            LocationId = locationId;
            SurchargeIds = surchargeIds;
            TeamId = teamId;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "accepted")]
        public bool? Accepted { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "careOrderId")]
        public long? CareOrderId { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "clientId")]
        public long? ClientId { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "employeeId")]
        public long? EmployeeId { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "hourTypeId")]
        public long? HourTypeId { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "locationId")]
        public long? LocationId { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "surchargeIds")]
        public IList<int?> SurchargeIds { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "teamId")]
        public long? TeamId { get; set; }

    }
}
