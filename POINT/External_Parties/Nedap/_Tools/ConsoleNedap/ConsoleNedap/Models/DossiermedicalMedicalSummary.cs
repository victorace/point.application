// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace ConsoleNedap.Models
{
    using Newtonsoft.Json;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    public partial class DossiermedicalMedicalSummary
    {
        /// <summary>
        /// Initializes a new instance of the DossiermedicalMedicalSummary
        /// class.
        /// </summary>
        public DossiermedicalMedicalSummary()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the DossiermedicalMedicalSummary
        /// class.
        /// </summary>
        /// <param name="adverseReactions">A list of adverse reactions for this
        /// client</param>
        /// <param name="allergies">A list of allergies for this client</param>
        /// <param name="clientId">The ID of the client the medical summary
        /// belongs to</param>
        /// <param name="episodes">A list of episodes for this client</param>
        /// <param name="incompetences">A list of incompetences for this
        /// client</param>
        /// <param name="legalStatus">The current legal status of the
        /// client</param>
        /// <param name="medicalPolicy">The current medical policy of the
        /// client</param>
        /// <param name="problems">A list of medical problems for this
        /// client</param>
        /// <param name="resuscitationDecision">The current resuscitation
        /// decision for the client
        /// &lt;pre&gt;
        /// Possible values:
        /// 0 = NO
        /// 1 = YES
        /// 2 = UNKNOWN
        /// &lt;/pre&gt;. Possible values include: 'NO', 'YES',
        /// 'UNKNOWN'</param>
        /// <param name="simplifiedAdverseReactions">A list of simplified
        /// propensities to adverse reactions (suspicions) for this
        /// client</param>
        public DossiermedicalMedicalSummary(IList<DossiermedicalPropensityToAdverseReaction> adverseReactions = default(IList<DossiermedicalPropensityToAdverseReaction>), IList<DossiermedicalPropensityToAdverseReaction> allergies = default(IList<DossiermedicalPropensityToAdverseReaction>), long? clientId = default(long?), IList<DossierepisodesEpisode> episodes = default(IList<DossierepisodesEpisode>), IList<DossiermedicalinvoluntaryCareIncompetence> incompetences = default(IList<DossiermedicalinvoluntaryCareIncompetence>), DossiermedicalinvoluntaryCareLegalStatus legalStatus = default(DossiermedicalinvoluntaryCareLegalStatus), DossiermedicaladvanceDirectivesContext medicalPolicy = default(DossiermedicaladvanceDirectivesContext), IList<DossiermedicalProblem> problems = default(IList<DossiermedicalProblem>), string resuscitationDecision = default(string), IList<DossiermedicalsimplifiedPropensityToAdverseReaction> simplifiedAdverseReactions = default(IList<DossiermedicalsimplifiedPropensityToAdverseReaction>))
        {
            AdverseReactions = adverseReactions;
            Allergies = allergies;
            ClientId = clientId;
            Episodes = episodes;
            Incompetences = incompetences;
            LegalStatus = legalStatus;
            MedicalPolicy = medicalPolicy;
            Problems = problems;
            ResuscitationDecision = resuscitationDecision;
            SimplifiedAdverseReactions = simplifiedAdverseReactions;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// Gets or sets a list of adverse reactions for this client
        /// </summary>
        [JsonProperty(PropertyName = "adverseReactions")]
        public IList<DossiermedicalPropensityToAdverseReaction> AdverseReactions { get; set; }

        /// <summary>
        /// Gets or sets a list of allergies for this client
        /// </summary>
        [JsonProperty(PropertyName = "allergies")]
        public IList<DossiermedicalPropensityToAdverseReaction> Allergies { get; set; }

        /// <summary>
        /// Gets or sets the ID of the client the medical summary belongs to
        /// </summary>
        [JsonProperty(PropertyName = "clientId")]
        public long? ClientId { get; set; }

        /// <summary>
        /// Gets or sets a list of episodes for this client
        /// </summary>
        [JsonProperty(PropertyName = "episodes")]
        public IList<DossierepisodesEpisode> Episodes { get; set; }

        /// <summary>
        /// Gets or sets a list of incompetences for this client
        /// </summary>
        [JsonProperty(PropertyName = "incompetences")]
        public IList<DossiermedicalinvoluntaryCareIncompetence> Incompetences { get; set; }

        /// <summary>
        /// Gets or sets the current legal status of the client
        /// </summary>
        [JsonProperty(PropertyName = "legalStatus")]
        public DossiermedicalinvoluntaryCareLegalStatus LegalStatus { get; set; }

        /// <summary>
        /// Gets or sets the current medical policy of the client
        /// </summary>
        [JsonProperty(PropertyName = "medicalPolicy")]
        public DossiermedicaladvanceDirectivesContext MedicalPolicy { get; set; }

        /// <summary>
        /// Gets or sets a list of medical problems for this client
        /// </summary>
        [JsonProperty(PropertyName = "problems")]
        public IList<DossiermedicalProblem> Problems { get; set; }

        /// <summary>
        /// Gets or sets the current resuscitation decision for the client
        /// &amp;lt;pre&amp;gt;
        /// Possible values:
        /// 0 = NO
        /// 1 = YES
        /// 2 = UNKNOWN
        /// &amp;lt;/pre&amp;gt;. Possible values include: 'NO', 'YES',
        /// 'UNKNOWN'
        /// </summary>
        [JsonProperty(PropertyName = "resuscitationDecision")]
        public string ResuscitationDecision { get; set; }

        /// <summary>
        /// Gets or sets a list of simplified propensities to adverse reactions
        /// (suspicions) for this client
        /// </summary>
        [JsonProperty(PropertyName = "simplifiedAdverseReactions")]
        public IList<DossiermedicalsimplifiedPropensityToAdverseReaction> SimplifiedAdverseReactions { get; set; }

    }
}
