// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace ConsoleNedap.Models
{
    using Newtonsoft.Json;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    public partial class DossierepisodesSubGoal
    {
        /// <summary>
        /// Initializes a new instance of the DossierepisodesSubGoal class.
        /// </summary>
        public DossierepisodesSubGoal()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the DossierepisodesSubGoal class.
        /// </summary>
        /// <param name="actions">Actions on this sub goal</param>
        /// <param name="archived">Archived status of this sub goal</param>
        /// <param name="episodeId">Object id of the parent episode of this sub
        /// goal</param>
        /// <param name="id">Object id of this sub goal</param>
        /// <param name="title">Title of this sub goal</param>
        /// <param name="updatedAt">Dated Base extension.</param>
        public DossierepisodesSubGoal(IList<DossierepisodesAction> actions = default(IList<DossierepisodesAction>), bool? archived = default(bool?), System.DateTime? createdAt = default(System.DateTime?), string createdBy = default(string), long? episodeId = default(long?), long? id = default(long?), string title = default(string), System.DateTime? updatedAt = default(System.DateTime?))
        {
            Actions = actions;
            Archived = archived;
            CreatedAt = createdAt;
            CreatedBy = createdBy;
            EpisodeId = episodeId;
            Id = id;
            Title = title;
            UpdatedAt = updatedAt;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// Gets or sets actions on this sub goal
        /// </summary>
        [JsonProperty(PropertyName = "actions")]
        public IList<DossierepisodesAction> Actions { get; set; }

        /// <summary>
        /// Gets or sets archived status of this sub goal
        /// </summary>
        [JsonProperty(PropertyName = "archived")]
        public bool? Archived { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "createdAt")]
        public System.DateTime? CreatedAt { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "createdBy")]
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets object id of the parent episode of this sub goal
        /// </summary>
        [JsonProperty(PropertyName = "episodeId")]
        public long? EpisodeId { get; set; }

        /// <summary>
        /// Gets or sets object id of this sub goal
        /// </summary>
        [JsonProperty(PropertyName = "id")]
        public long? Id { get; set; }

        /// <summary>
        /// Gets or sets title of this sub goal
        /// </summary>
        [JsonProperty(PropertyName = "title")]
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets dated Base extension.
        /// </summary>
        [JsonProperty(PropertyName = "updatedAt")]
        public System.DateTime? UpdatedAt { get; set; }

    }
}
