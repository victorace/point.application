// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace ConsoleNedap.Models
{
    using Newtonsoft.Json;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    public partial class ListLocationAssignmentResidenceList
    {
        /// <summary>
        /// Initializes a new instance of the
        /// ListLocationAssignmentResidenceList class.
        /// </summary>
        public ListLocationAssignmentResidenceList()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the
        /// ListLocationAssignmentResidenceList class.
        /// </summary>
        public ListLocationAssignmentResidenceList(IList<LocationAssignmentResidence> locationAssignmentResidences = default(IList<LocationAssignmentResidence>))
        {
            LocationAssignmentResidences = locationAssignmentResidences;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "locationAssignmentResidences")]
        public IList<LocationAssignmentResidence> LocationAssignmentResidences { get; set; }

    }
}
