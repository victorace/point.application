// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace ConsoleNedap.Models
{
    using Newtonsoft.Json;
    using System.Linq;

    public partial class Location
    {
        /// <summary>
        /// Initializes a new instance of the Location class.
        /// </summary>
        public Location()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the Location class.
        /// </summary>
        /// <param name="agbCode">Deprecated since: 14-03-2019 - nationally
        /// issued unique identifying code for organisations and persons that
        /// have a registration in the agb register</param>
        /// <param name="ggzAllowed">Indication whether this location can be
        /// used as a mental healthcare residence location</param>
        /// <param name="materializedPath">automatically calculated
        /// field</param>
        /// <param name="medicineAllowed">Indication whether this location can
        /// be used as a medication location</param>
        /// <param name="parentObjectId">location objectid of the parent where
        /// this location belongs to</param>
        /// <param name="wzaCode">unique identifying code. Used by
        /// municipalities</param>
        public Location(long? addressObjectId = default(long?), string agbCode = default(string), long? agbcodeId = default(long?), System.DateTime? beginDate = default(System.DateTime?), int? capacity = default(int?), string costCenter = default(string), System.DateTime? createdAt = default(System.DateTime?), System.DateTime? endDate = default(System.DateTime?), bool? ggzAllowed = default(bool?), int? icon = default(int?), long? id = default(long?), string identificationNo = default(string), bool? intramuralLocation = default(bool?), string locationType = default(string), string materializedPath = default(string), bool? medicineAllowed = default(bool?), string name = default(string), long? parentObjectId = default(long?), string roomType = default(string), System.DateTime? updatedAt = default(System.DateTime?), string wzaCode = default(string))
        {
            AddressObjectId = addressObjectId;
            AgbCode = agbCode;
            AgbcodeId = agbcodeId;
            BeginDate = beginDate;
            Capacity = capacity;
            CostCenter = costCenter;
            CreatedAt = createdAt;
            EndDate = endDate;
            GgzAllowed = ggzAllowed;
            Icon = icon;
            Id = id;
            IdentificationNo = identificationNo;
            IntramuralLocation = intramuralLocation;
            LocationType = locationType;
            MaterializedPath = materializedPath;
            MedicineAllowed = medicineAllowed;
            Name = name;
            ParentObjectId = parentObjectId;
            RoomType = roomType;
            UpdatedAt = updatedAt;
            WzaCode = wzaCode;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "addressObjectId")]
        public long? AddressObjectId { get; set; }

        /// <summary>
        /// Gets or sets deprecated since: 14-03-2019 - nationally issued
        /// unique identifying code for organisations and persons that have a
        /// registration in the agb register
        /// </summary>
        [JsonProperty(PropertyName = "agbCode")]
        public string AgbCode { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "agbcodeId")]
        public long? AgbcodeId { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "beginDate")]
        public System.DateTime? BeginDate { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "capacity")]
        public int? Capacity { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "costCenter")]
        public string CostCenter { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "createdAt")]
        public System.DateTime? CreatedAt { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "endDate")]
        public System.DateTime? EndDate { get; set; }

        /// <summary>
        /// Gets or sets indication whether this location can be used as a
        /// mental healthcare residence location
        /// </summary>
        [JsonProperty(PropertyName = "ggzAllowed")]
        public bool? GgzAllowed { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "icon")]
        public int? Icon { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "id")]
        public long? Id { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "identificationNo")]
        public string IdentificationNo { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "intramuralLocation")]
        public bool? IntramuralLocation { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "locationType")]
        public string LocationType { get; set; }

        /// <summary>
        /// Gets or sets automatically calculated field
        /// </summary>
        [JsonProperty(PropertyName = "materializedPath")]
        public string MaterializedPath { get; set; }

        /// <summary>
        /// Gets or sets indication whether this location can be used as a
        /// medication location
        /// </summary>
        [JsonProperty(PropertyName = "medicineAllowed")]
        public bool? MedicineAllowed { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets location objectid of the parent where this location
        /// belongs to
        /// </summary>
        [JsonProperty(PropertyName = "parentObjectId")]
        public long? ParentObjectId { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "roomType")]
        public string RoomType { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "updatedAt")]
        public System.DateTime? UpdatedAt { get; set; }

        /// <summary>
        /// Gets or sets unique identifying code. Used by municipalities
        /// </summary>
        [JsonProperty(PropertyName = "wzaCode")]
        public string WzaCode { get; set; }

    }
}
