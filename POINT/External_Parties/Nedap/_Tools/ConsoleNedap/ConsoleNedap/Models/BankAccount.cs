// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace ConsoleNedap.Models
{
    using Newtonsoft.Json;
    using System.Linq;

    public partial class BankAccount
    {
        /// <summary>
        /// Initializes a new instance of the BankAccount class.
        /// </summary>
        public BankAccount()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the BankAccount class.
        /// </summary>
        public BankAccount(string cityOfOwner = default(string), long? id = default(long?), string nameOfOwner = default(string), string number = default(string))
        {
            CityOfOwner = cityOfOwner;
            Id = id;
            NameOfOwner = nameOfOwner;
            Number = number;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "cityOfOwner")]
        public string CityOfOwner { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "id")]
        public long? Id { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "nameOfOwner")]
        public string NameOfOwner { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "number")]
        public string Number { get; set; }

    }
}
