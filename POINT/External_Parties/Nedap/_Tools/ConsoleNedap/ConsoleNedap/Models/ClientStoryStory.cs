// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace ConsoleNedap.Models
{
    using Newtonsoft.Json;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    public partial class ClientStoryStory
    {
        /// <summary>
        /// Initializes a new instance of the ClientStoryStory class.
        /// </summary>
        public ClientStoryStory()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the ClientStoryStory class.
        /// </summary>
        /// <param name="accessLevel">The access level of the story, can be
        /// NONE, READ_ONLY, or READ_WRITE. Possible values include: 'NONE',
        /// 'READ_ONLY', 'READ_WRITE'</param>
        /// <param name="categories">A list of categories in this client
        /// story</param>
        /// <param name="updatedAt">When this story was last updated</param>
        /// <param name="updatedBy">Who the story was last updated by</param>
        /// <param name="updatedSource">The deployment that last updated this
        /// story</param>
        public ClientStoryStory(string accessLevel = default(string), IList<ClientStoryCategory> categories = default(IList<ClientStoryCategory>), long? id = default(long?), System.DateTime? updatedAt = default(System.DateTime?), long? updatedBy = default(long?), string updatedSource = default(string))
        {
            AccessLevel = accessLevel;
            Categories = categories;
            Id = id;
            UpdatedAt = updatedAt;
            UpdatedBy = updatedBy;
            UpdatedSource = updatedSource;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// Gets or sets the access level of the story, can be NONE, READ_ONLY,
        /// or READ_WRITE. Possible values include: 'NONE', 'READ_ONLY',
        /// 'READ_WRITE'
        /// </summary>
        [JsonProperty(PropertyName = "accessLevel")]
        public string AccessLevel { get; set; }

        /// <summary>
        /// Gets or sets a list of categories in this client story
        /// </summary>
        [JsonProperty(PropertyName = "categories")]
        public IList<ClientStoryCategory> Categories { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "id")]
        public long? Id { get; set; }

        /// <summary>
        /// Gets or sets when this story was last updated
        /// </summary>
        [JsonProperty(PropertyName = "updatedAt")]
        public System.DateTime? UpdatedAt { get; set; }

        /// <summary>
        /// Gets or sets who the story was last updated by
        /// </summary>
        [JsonProperty(PropertyName = "updatedBy")]
        public long? UpdatedBy { get; set; }

        /// <summary>
        /// Gets or sets the deployment that last updated this story
        /// </summary>
        [JsonProperty(PropertyName = "updatedSource")]
        public string UpdatedSource { get; set; }

    }
}
