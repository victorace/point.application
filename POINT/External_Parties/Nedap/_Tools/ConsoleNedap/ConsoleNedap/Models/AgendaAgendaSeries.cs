// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace ConsoleNedap.Models
{
    using Microsoft.Rest;
    using Microsoft.Rest.Serialization;
    using Newtonsoft.Json;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    public partial class AgendaAgendaSeries
    {
        /// <summary>
        /// Initializes a new instance of the AgendaAgendaSeries class.
        /// </summary>
        public AgendaAgendaSeries()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the AgendaAgendaSeries class.
        /// </summary>
        /// <param name="recurrenceType">Possible values include: 'SINGLE',
        /// 'DAILY', 'WEEKLY', 'MONTHLY_ON_DAY', 'MONTHLY_ON_WEEK',
        /// 'YEARLY'</param>
        public AgendaAgendaSeries(IList<AgendaAgendaClientInvitation> agendaClientInvitations = default(IList<AgendaAgendaClientInvitation>), IList<AgendaAgendaInvitation> agendaInvitations = default(IList<AgendaAgendaInvitation>), string comment = default(string), long? creatorObjectId = default(long?), string customLocation = default(string), System.DateTime? cycleBase = default(System.DateTime?), int? cycleInterval = default(int?), int? duration = default(int?), string endTime = default(string), bool? friday = default(bool?), long? hourTypeId = default(long?), long? id = default(long?), IList<AgendaLabel> labels = default(IList<AgendaLabel>), bool? monday = default(bool?), string name = default(string), string recurrenceType = default(string), bool? saturday = default(bool?), string startTime = default(string), bool? sunday = default(bool?), bool? thursday = default(bool?), long? timelineId = default(long?), bool? tuesday = default(bool?), string uuid = default(string), System.DateTime? validFrom = default(System.DateTime?), System.DateTime? validTo = default(System.DateTime?), bool? wednesday = default(bool?))
        {
            AgendaClientInvitations = agendaClientInvitations;
            AgendaInvitations = agendaInvitations;
            Comment = comment;
            CreatorObjectId = creatorObjectId;
            CustomLocation = customLocation;
            CycleBase = cycleBase;
            CycleInterval = cycleInterval;
            Duration = duration;
            EndTime = endTime;
            Friday = friday;
            HourTypeId = hourTypeId;
            Id = id;
            Labels = labels;
            Monday = monday;
            Name = name;
            RecurrenceType = recurrenceType;
            Saturday = saturday;
            StartTime = startTime;
            Sunday = sunday;
            Thursday = thursday;
            TimelineId = timelineId;
            Tuesday = tuesday;
            Uuid = uuid;
            ValidFrom = validFrom;
            ValidTo = validTo;
            Wednesday = wednesday;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "agendaClientInvitations")]
        public IList<AgendaAgendaClientInvitation> AgendaClientInvitations { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "agendaInvitations")]
        public IList<AgendaAgendaInvitation> AgendaInvitations { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "comment")]
        public string Comment { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "creatorObjectId")]
        public long? CreatorObjectId { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "customLocation")]
        public string CustomLocation { get; set; }

        /// <summary>
        /// </summary>
        [JsonConverter(typeof(DateJsonConverter))]
        [JsonProperty(PropertyName = "cycleBase")]
        public System.DateTime? CycleBase { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "cycleInterval")]
        public int? CycleInterval { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "duration")]
        public int? Duration { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "endTime")]
        public string EndTime { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "friday")]
        public bool? Friday { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "hourTypeId")]
        public long? HourTypeId { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "id")]
        public long? Id { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "labels")]
        public IList<AgendaLabel> Labels { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "monday")]
        public bool? Monday { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets possible values include: 'SINGLE', 'DAILY', 'WEEKLY',
        /// 'MONTHLY_ON_DAY', 'MONTHLY_ON_WEEK', 'YEARLY'
        /// </summary>
        [JsonProperty(PropertyName = "recurrenceType")]
        public string RecurrenceType { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "saturday")]
        public bool? Saturday { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "startTime")]
        public string StartTime { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "sunday")]
        public bool? Sunday { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "thursday")]
        public bool? Thursday { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "timelineId")]
        public long? TimelineId { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "tuesday")]
        public bool? Tuesday { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "uuid")]
        public string Uuid { get; set; }

        /// <summary>
        /// </summary>
        [JsonConverter(typeof(DateJsonConverter))]
        [JsonProperty(PropertyName = "validFrom")]
        public System.DateTime? ValidFrom { get; set; }

        /// <summary>
        /// </summary>
        [JsonConverter(typeof(DateJsonConverter))]
        [JsonProperty(PropertyName = "validTo")]
        public System.DateTime? ValidTo { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "wednesday")]
        public bool? Wednesday { get; set; }

    }
}
