// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace ConsoleNedap.Models
{
    using Newtonsoft.Json;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    public partial class ListUserList
    {
        /// <summary>
        /// Initializes a new instance of the ListUserList class.
        /// </summary>
        public ListUserList()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the ListUserList class.
        /// </summary>
        public ListUserList(IList<User> users = default(IList<User>))
        {
            Users = users;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "users")]
        public IList<User> Users { get; set; }

    }
}
