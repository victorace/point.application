// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace ConsoleNedap.Models
{
    using Newtonsoft.Json;
    using System.Linq;

    public partial class NexusRelationCategory
    {
        /// <summary>
        /// Initializes a new instance of the NexusRelationCategory class.
        /// </summary>
        public NexusRelationCategory()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the NexusRelationCategory class.
        /// </summary>
        /// <param name="updatedAt">Dated Base extension.</param>
        public NexusRelationCategory(System.DateTime? createdAt = default(System.DateTime?), string createdBy = default(string), long? id = default(long?), string name = default(string), string type = default(string), System.DateTime? updatedAt = default(System.DateTime?))
        {
            CreatedAt = createdAt;
            CreatedBy = createdBy;
            Id = id;
            Name = name;
            Type = type;
            UpdatedAt = updatedAt;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "createdAt")]
        public System.DateTime? CreatedAt { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "createdBy")]
        public string CreatedBy { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "id")]
        public long? Id { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets dated Base extension.
        /// </summary>
        [JsonProperty(PropertyName = "updatedAt")]
        public System.DateTime? UpdatedAt { get; set; }

    }
}
