// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace ConsoleNedap.Models
{
    using Newtonsoft.Json;
    using System.Linq;

    public partial class DossieromahaInterventionTarget
    {
        /// <summary>
        /// Initializes a new instance of the DossieromahaInterventionTarget
        /// class.
        /// </summary>
        public DossieromahaInterventionTarget()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the DossieromahaInterventionTarget
        /// class.
        /// </summary>
        /// <param name="name">(nl) Naam van dit actievlak</param>
        public DossieromahaInterventionTarget(string classificationId = default(string), string name = default(string))
        {
            ClassificationId = classificationId;
            Name = name;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "classificationId")]
        public string ClassificationId { get; set; }

        /// <summary>
        /// Gets or sets (nl) Naam van dit actievlak
        /// </summary>
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

    }
}
