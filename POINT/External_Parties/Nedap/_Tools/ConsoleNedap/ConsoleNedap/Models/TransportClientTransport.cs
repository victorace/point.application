// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace ConsoleNedap.Models
{
    using Microsoft.Rest;
    using Microsoft.Rest.Serialization;
    using Newtonsoft.Json;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    public partial class TransportClientTransport
    {
        /// <summary>
        /// Initializes a new instance of the TransportClientTransport class.
        /// </summary>
        public TransportClientTransport()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the TransportClientTransport class.
        /// </summary>
        /// <param name="clientId">Id of {@link
        /// com.nedap.healthcare.domain.Client} for which these details are
        /// valid</param>
        /// <param name="date">Date for which the transport details for the
        /// given client is retrieved</param>
        /// <param name="hasWeelchairAssignment">Has this client a wheelchair
        /// assigned at the given date</param>
        /// <param name="hourTypes">Set of all {@link HourType} instances that
        /// can be used for this client</param>
        /// <param name="individualTransport">Is individual transport needed
        /// for this client at the given date</param>
        /// <param name="wlz">Receives this client WLZ care at the given
        /// date</param>
        public TransportClientTransport(long clientId, System.DateTime date, bool hasWeelchairAssignment, IList<HourType> hourTypes, bool individualTransport, bool wlz, System.DateTime? createdAt = default(System.DateTime?), long? id = default(long?), System.DateTime? updatedAt = default(System.DateTime?))
        {
            ClientId = clientId;
            CreatedAt = createdAt;
            Date = date;
            HasWeelchairAssignment = hasWeelchairAssignment;
            HourTypes = hourTypes;
            Id = id;
            IndividualTransport = individualTransport;
            UpdatedAt = updatedAt;
            Wlz = wlz;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// Gets or sets id of {@link com.nedap.healthcare.domain.Client} for
        /// which these details are valid
        /// </summary>
        [JsonProperty(PropertyName = "clientId")]
        public long ClientId { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "createdAt")]
        public System.DateTime? CreatedAt { get; set; }

        /// <summary>
        /// Gets or sets date for which the transport details for the given
        /// client is retrieved
        /// </summary>
        [JsonConverter(typeof(DateJsonConverter))]
        [JsonProperty(PropertyName = "date")]
        public System.DateTime Date { get; set; }

        /// <summary>
        /// Gets or sets has this client a wheelchair assigned at the given
        /// date
        /// </summary>
        [JsonProperty(PropertyName = "hasWeelchairAssignment")]
        public bool HasWeelchairAssignment { get; set; }

        /// <summary>
        /// Gets or sets set of all {@link HourType} instances that can be used
        /// for this client
        /// </summary>
        [JsonProperty(PropertyName = "hourTypes")]
        public IList<HourType> HourTypes { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "id")]
        public long? Id { get; set; }

        /// <summary>
        /// Gets or sets is individual transport needed for this client at the
        /// given date
        /// </summary>
        [JsonProperty(PropertyName = "individualTransport")]
        public bool IndividualTransport { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "updatedAt")]
        public System.DateTime? UpdatedAt { get; set; }

        /// <summary>
        /// Gets or sets receives this client WLZ care at the given date
        /// </summary>
        [JsonProperty(PropertyName = "wlz")]
        public bool Wlz { get; set; }

        /// <summary>
        /// Validate the object.
        /// </summary>
        /// <exception cref="ValidationException">
        /// Thrown if validation fails
        /// </exception>
        public virtual void Validate()
        {
            if (HourTypes == null)
            {
                throw new ValidationException(ValidationRules.CannotBeNull, "HourTypes");
            }
            if (HourTypes != null)
            {
                if (HourTypes.Count != System.Linq.Enumerable.Count(System.Linq.Enumerable.Distinct(HourTypes)))
                {
                    throw new ValidationException(ValidationRules.UniqueItems, "HourTypes");
                }
            }
        }
    }
}
