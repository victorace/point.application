// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace ConsoleNedap.Models
{
    using Microsoft.Rest;
    using Newtonsoft.Json;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    public partial class CarepathModuleTemplate
    {
        /// <summary>
        /// Initializes a new instance of the CarepathModuleTemplate class.
        /// </summary>
        public CarepathModuleTemplate()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the CarepathModuleTemplate class.
        /// </summary>
        /// <param name="uuid">The UUID of this module. May be used to
        /// reference an existing module if this module template
        /// is already part of another care path template</param>
        /// <param name="duration">Duration in days</param>
        /// <param name="offset">The offset in days</param>
        /// <param name="precededBy">UUID of care path module which precedes
        /// this module.</param>
        /// <param name="templateRoot">This signifies that this module template
        /// may also be used without being embedded
        /// in a care path template.</param>
        public CarepathModuleTemplate(string uuid, long? duration = default(long?), IList<CarepathIntentTemplate> intents = default(IList<CarepathIntentTemplate>), string name = default(string), long? offset = default(long?), string precededBy = default(string), bool? templateRoot = default(bool?))
        {
            Duration = duration;
            Intents = intents;
            Name = name;
            Offset = offset;
            PrecededBy = precededBy;
            TemplateRoot = templateRoot;
            Uuid = uuid;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// Gets or sets duration in days
        /// </summary>
        [JsonProperty(PropertyName = "duration")]
        public long? Duration { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "intents")]
        public IList<CarepathIntentTemplate> Intents { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the offset in days
        /// </summary>
        [JsonProperty(PropertyName = "offset")]
        public long? Offset { get; set; }

        /// <summary>
        /// Gets or sets UUID of care path module which precedes this module.
        /// </summary>
        [JsonProperty(PropertyName = "precededBy")]
        public string PrecededBy { get; set; }

        /// <summary>
        /// Gets or sets this signifies that this module template may also be
        /// used without being embedded
        /// in a care path template.
        /// </summary>
        [JsonProperty(PropertyName = "templateRoot")]
        public bool? TemplateRoot { get; set; }

        /// <summary>
        /// Gets or sets the UUID of this module. May be used to reference an
        /// existing module if this module template
        /// is already part of another care path template
        /// </summary>
        [JsonProperty(PropertyName = "uuid")]
        public string Uuid { get; set; }

        /// <summary>
        /// Validate the object.
        /// </summary>
        /// <exception cref="ValidationException">
        /// Thrown if validation fails
        /// </exception>
        public virtual void Validate()
        {
            if (Uuid == null)
            {
                throw new ValidationException(ValidationRules.CannotBeNull, "Uuid");
            }
            if (Intents != null)
            {
                foreach (var element in Intents)
                {
                    if (element != null)
                    {
                        element.Validate();
                    }
                }
            }
        }
    }
}
