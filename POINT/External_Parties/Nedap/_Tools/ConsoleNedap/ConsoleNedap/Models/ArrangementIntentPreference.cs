// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace ConsoleNedap.Models
{
    using Microsoft.Rest;
    using Newtonsoft.Json;
    using System.Linq;

    public partial class ArrangementIntentPreference
    {
        /// <summary>
        /// Initializes a new instance of the ArrangementIntentPreference
        /// class.
        /// </summary>
        public ArrangementIntentPreference()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the ArrangementIntentPreference
        /// class.
        /// </summary>
        /// <param name="intent">Intent of the preference</param>
        /// <param name="timeSlot">TimeSlot of the preference</param>
        public ArrangementIntentPreference(ArrangementIntent intent, ArrangementTimeSlot timeSlot)
        {
            Intent = intent;
            TimeSlot = timeSlot;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// Gets or sets intent of the preference
        /// </summary>
        [JsonProperty(PropertyName = "intent")]
        public ArrangementIntent Intent { get; set; }

        /// <summary>
        /// Gets or sets timeSlot of the preference
        /// </summary>
        [JsonProperty(PropertyName = "timeSlot")]
        public ArrangementTimeSlot TimeSlot { get; set; }

        /// <summary>
        /// Validate the object.
        /// </summary>
        /// <exception cref="ValidationException">
        /// Thrown if validation fails
        /// </exception>
        public virtual void Validate()
        {
            if (Intent == null)
            {
                throw new ValidationException(ValidationRules.CannotBeNull, "Intent");
            }
            if (TimeSlot == null)
            {
                throw new ValidationException(ValidationRules.CannotBeNull, "TimeSlot");
            }
            if (Intent != null)
            {
                Intent.Validate();
            }
            if (TimeSlot != null)
            {
                TimeSlot.Validate();
            }
        }
    }
}
