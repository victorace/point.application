// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace ConsoleNedap.Models
{
    using Newtonsoft.Json;
    using System.Linq;

    public partial class MovesPlannedAddress
    {
        /// <summary>
        /// Initializes a new instance of the MovesPlannedAddress class.
        /// </summary>
        public MovesPlannedAddress()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the MovesPlannedAddress class.
        /// </summary>
        public MovesPlannedAddress(string city = default(string), double? latitude = default(double?), double? longitude = default(double?), string number = default(string), string numberPostfix = default(string), string street = default(string), string zipcode = default(string))
        {
            City = city;
            Latitude = latitude;
            Longitude = longitude;
            Number = number;
            NumberPostfix = numberPostfix;
            Street = street;
            Zipcode = zipcode;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "city")]
        public string City { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "latitude")]
        public double? Latitude { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "longitude")]
        public double? Longitude { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "number")]
        public string Number { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "numberPostfix")]
        public string NumberPostfix { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "street")]
        public string Street { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "zipcode")]
        public string Zipcode { get; set; }

    }
}
