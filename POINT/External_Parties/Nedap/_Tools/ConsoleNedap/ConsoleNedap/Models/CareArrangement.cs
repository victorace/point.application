// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace ConsoleNedap.Models
{
    using Newtonsoft.Json;
    using System.Linq;

    public partial class CareArrangement
    {
        /// <summary>
        /// Initializes a new instance of the CareArrangement class.
        /// </summary>
        public CareArrangement()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the CareArrangement class.
        /// </summary>
        /// <param name="activityObjectId">Id of an activityObject, also known
        /// as hour_type</param>
        public CareArrangement(long? activityObjectId = default(long?), string afternoons = default(string), System.DateTime? beginDate = default(System.DateTime?), int? category = default(int?), long? clientObjectId = default(long?), System.DateTime? endDate = default(System.DateTime?), string evenings = default(string), long? id = default(long?), string mornings = default(string), string nights = default(string), int? normAfternoon = default(int?), int? normEvening = default(int?), int? normMorning = default(int?), int? normNight = default(int?), long? teamObjectId = default(long?))
        {
            ActivityObjectId = activityObjectId;
            Afternoons = afternoons;
            BeginDate = beginDate;
            Category = category;
            ClientObjectId = clientObjectId;
            EndDate = endDate;
            Evenings = evenings;
            Id = id;
            Mornings = mornings;
            Nights = nights;
            NormAfternoon = normAfternoon;
            NormEvening = normEvening;
            NormMorning = normMorning;
            NormNight = normNight;
            TeamObjectId = teamObjectId;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// Gets or sets id of an activityObject, also known as hour_type
        /// </summary>
        [JsonProperty(PropertyName = "activityObjectId")]
        public long? ActivityObjectId { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "afternoons")]
        public string Afternoons { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "beginDate")]
        public System.DateTime? BeginDate { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "category")]
        public int? Category { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "clientObjectId")]
        public long? ClientObjectId { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "endDate")]
        public System.DateTime? EndDate { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "evenings")]
        public string Evenings { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "id")]
        public long? Id { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "mornings")]
        public string Mornings { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "nights")]
        public string Nights { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "normAfternoon")]
        public int? NormAfternoon { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "normEvening")]
        public int? NormEvening { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "normMorning")]
        public int? NormMorning { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "normNight")]
        public int? NormNight { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "teamObjectId")]
        public long? TeamObjectId { get; set; }

    }
}
