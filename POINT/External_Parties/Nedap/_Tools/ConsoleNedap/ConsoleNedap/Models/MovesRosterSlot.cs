// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace ConsoleNedap.Models
{
    using Microsoft.Rest;
    using Microsoft.Rest.Serialization;
    using Newtonsoft.Json;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    public partial class MovesRosterSlot
    {
        /// <summary>
        /// Initializes a new instance of the MovesRosterSlot class.
        /// </summary>
        public MovesRosterSlot()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the MovesRosterSlot class.
        /// </summary>
        /// <param name="externalExpertiseProfileId">Id of the related
        /// expertise profile in OA</param>
        /// <param name="externalTeamId">Id of the related team in OA</param>
        public MovesRosterSlot(System.DateTime? beginTime = default(System.DateTime?), System.DateTime? date = default(System.DateTime?), System.DateTime? endTime = default(System.DateTime?), long? externalExpertiseProfileId = default(long?), long? externalTeamId = default(long?), IList<MovesFlexSignup> flexSignups = default(IList<MovesFlexSignup>), string id = default(string), string note = default(string), IList<MovesShiftAssignment> shiftAssignments = default(IList<MovesShiftAssignment>), string shiftId = default(string))
        {
            BeginTime = beginTime;
            Date = date;
            EndTime = endTime;
            ExternalExpertiseProfileId = externalExpertiseProfileId;
            ExternalTeamId = externalTeamId;
            FlexSignups = flexSignups;
            Id = id;
            Note = note;
            ShiftAssignments = shiftAssignments;
            ShiftId = shiftId;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "beginTime")]
        public System.DateTime? BeginTime { get; set; }

        /// <summary>
        /// </summary>
        [JsonConverter(typeof(DateJsonConverter))]
        [JsonProperty(PropertyName = "date")]
        public System.DateTime? Date { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "endTime")]
        public System.DateTime? EndTime { get; set; }

        /// <summary>
        /// Gets or sets id of the related expertise profile in OA
        /// </summary>
        [JsonProperty(PropertyName = "externalExpertiseProfileId")]
        public long? ExternalExpertiseProfileId { get; set; }

        /// <summary>
        /// Gets or sets id of the related team in OA
        /// </summary>
        [JsonProperty(PropertyName = "externalTeamId")]
        public long? ExternalTeamId { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "flexSignups")]
        public IList<MovesFlexSignup> FlexSignups { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "note")]
        public string Note { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "shiftAssignments")]
        public IList<MovesShiftAssignment> ShiftAssignments { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "shiftId")]
        public string ShiftId { get; set; }

    }
}
