// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace ConsoleNedap.Models
{
    using Microsoft.Rest;
    using Microsoft.Rest.Serialization;
    using Newtonsoft.Json;
    using System.Linq;

    public partial class ViewModelClientSearchResultView
    {
        /// <summary>
        /// Initializes a new instance of the ViewModelClientSearchResultView
        /// class.
        /// </summary>
        public ViewModelClientSearchResultView()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the ViewModelClientSearchResultView
        /// class.
        /// </summary>
        /// <param name="addressString">This Client's address; e.g.
        /// "Parallelweg 2, 7141 DC, Groenlo"</param>
        /// <param name="dateOfBirth">Date of birth, fomatted
        /// yyyy-mm-dd</param>
        /// <param name="gender">This Client's gender; can be male, female or
        /// unknown</param>
        /// <param name="id">the id of this object</param>
        /// <param name="identificationNo">The client number for this Client;
        /// e.g. "234567"</param>
        /// <param name="name">The full name of this client; e.g. "Petrus
        /// Johannes Paulusma"</param>
        /// <param name="secretClient">Boolean for celebrities</param>
        public ViewModelClientSearchResultView(string addressString = default(string), System.DateTime? dateOfBirth = default(System.DateTime?), string gender = default(string), long? id = default(long?), string identificationNo = default(string), string name = default(string), bool? secretClient = default(bool?))
        {
            AddressString = addressString;
            DateOfBirth = dateOfBirth;
            Gender = gender;
            Id = id;
            IdentificationNo = identificationNo;
            Name = name;
            SecretClient = secretClient;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// Gets or sets this Client's address; e.g. "Parallelweg 2, 7141 DC,
        /// Groenlo"
        /// </summary>
        [JsonProperty(PropertyName = "addressString")]
        public string AddressString { get; set; }

        /// <summary>
        /// Gets or sets date of birth, fomatted yyyy-mm-dd
        /// </summary>
        [JsonConverter(typeof(DateJsonConverter))]
        [JsonProperty(PropertyName = "dateOfBirth")]
        public System.DateTime? DateOfBirth { get; set; }

        /// <summary>
        /// Gets or sets this Client's gender; can be male, female or unknown
        /// </summary>
        [JsonProperty(PropertyName = "gender")]
        public string Gender { get; set; }

        /// <summary>
        /// Gets or sets the id of this object
        /// </summary>
        [JsonProperty(PropertyName = "id")]
        public long? Id { get; set; }

        /// <summary>
        /// Gets or sets the client number for this Client; e.g. "234567"
        /// </summary>
        [JsonProperty(PropertyName = "identificationNo")]
        public string IdentificationNo { get; set; }

        /// <summary>
        /// Gets or sets the full name of this client; e.g. "Petrus Johannes
        /// Paulusma"
        /// </summary>
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets boolean for celebrities
        /// </summary>
        [JsonProperty(PropertyName = "secretClient")]
        public bool? SecretClient { get; set; }

    }
}
