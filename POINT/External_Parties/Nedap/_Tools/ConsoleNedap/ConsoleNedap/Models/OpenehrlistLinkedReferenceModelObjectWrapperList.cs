// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace ConsoleNedap.Models
{
    using Newtonsoft.Json;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    public partial class OpenehrlistLinkedReferenceModelObjectWrapperList
    {
        /// <summary>
        /// Initializes a new instance of the
        /// OpenehrlistLinkedReferenceModelObjectWrapperList class.
        /// </summary>
        public OpenehrlistLinkedReferenceModelObjectWrapperList()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the
        /// OpenehrlistLinkedReferenceModelObjectWrapperList class.
        /// </summary>
        public OpenehrlistLinkedReferenceModelObjectWrapperList(IList<OpenehrLinkedReferenceModelObjectWrapper> linkedReferenceModelObjectWrappers = default(IList<OpenehrLinkedReferenceModelObjectWrapper>))
        {
            LinkedReferenceModelObjectWrappers = linkedReferenceModelObjectWrappers;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "linkedReferenceModelObjectWrappers")]
        public IList<OpenehrLinkedReferenceModelObjectWrapper> LinkedReferenceModelObjectWrappers { get; set; }

    }
}
