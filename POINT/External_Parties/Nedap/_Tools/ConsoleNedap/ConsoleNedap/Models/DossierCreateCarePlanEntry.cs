// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace ConsoleNedap.Models
{
    using Newtonsoft.Json;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    public partial class DossierCreateCarePlanEntry
    {
        /// <summary>
        /// Initializes a new instance of the DossierCreateCarePlanEntry class.
        /// </summary>
        public DossierCreateCarePlanEntry()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the DossierCreateCarePlanEntry class.
        /// </summary>
        public DossierCreateCarePlanEntry(IList<DossierActionEntry> actionEntries = default(IList<DossierActionEntry>), DossierCarePlanEntry carePlanEntry = default(DossierCarePlanEntry), DossierDemandEntry demandEntry = default(DossierDemandEntry), DossierGoalEntry goalEntry = default(DossierGoalEntry))
        {
            ActionEntries = actionEntries;
            CarePlanEntry = carePlanEntry;
            DemandEntry = demandEntry;
            GoalEntry = goalEntry;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "actionEntries")]
        public IList<DossierActionEntry> ActionEntries { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "carePlanEntry")]
        public DossierCarePlanEntry CarePlanEntry { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "demandEntry")]
        public DossierDemandEntry DemandEntry { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "goalEntry")]
        public DossierGoalEntry GoalEntry { get; set; }

    }
}
