// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace ConsoleNedap.Models
{
    using Newtonsoft.Json;
    using System.Linq;

    public partial class PresenceLogProduct
    {
        /// <summary>
        /// Initializes a new instance of the PresenceLogProduct class.
        /// </summary>
        public PresenceLogProduct()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the PresenceLogProduct class.
        /// </summary>
        public PresenceLogProduct(string activityDirectOrIndirect = default(string), long? activityObjectId = default(long?), string description = default(string), long? duration = default(long?), long? quantity = default(long?))
        {
            ActivityDirectOrIndirect = activityDirectOrIndirect;
            ActivityObjectId = activityObjectId;
            Description = description;
            Duration = duration;
            Quantity = quantity;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "activityDirectOrIndirect")]
        public string ActivityDirectOrIndirect { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "activityObjectId")]
        public long? ActivityObjectId { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "duration")]
        public long? Duration { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "quantity")]
        public long? Quantity { get; set; }

    }
}
