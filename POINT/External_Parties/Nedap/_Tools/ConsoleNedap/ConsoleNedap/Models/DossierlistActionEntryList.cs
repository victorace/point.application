// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace ConsoleNedap.Models
{
    using Newtonsoft.Json;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    public partial class DossierlistActionEntryList
    {
        /// <summary>
        /// Initializes a new instance of the DossierlistActionEntryList class.
        /// </summary>
        public DossierlistActionEntryList()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the DossierlistActionEntryList class.
        /// </summary>
        public DossierlistActionEntryList(IList<DossierActionEntry> actionEntries = default(IList<DossierActionEntry>))
        {
            ActionEntries = actionEntries;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "actionEntries")]
        public IList<DossierActionEntry> ActionEntries { get; set; }

    }
}
