// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace ConsoleNedap.Models
{
    using Newtonsoft.Json;
    using System.Linq;

    public partial class NexusOrganisationCategory
    {
        /// <summary>
        /// Initializes a new instance of the NexusOrganisationCategory class.
        /// </summary>
        public NexusOrganisationCategory()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the NexusOrganisationCategory class.
        /// </summary>
        /// <param name="updatedAt">Dated Base extension.</param>
        public NexusOrganisationCategory(bool? active = default(bool?), bool? azr = default(bool?), System.DateTime? createdAt = default(System.DateTime?), string createdBy = default(string), long? id = default(long?), string name = default(string), long? reference = default(long?), System.DateTime? updatedAt = default(System.DateTime?), long? vektisCode = default(long?))
        {
            Active = active;
            Azr = azr;
            CreatedAt = createdAt;
            CreatedBy = createdBy;
            Id = id;
            Name = name;
            Reference = reference;
            UpdatedAt = updatedAt;
            VektisCode = vektisCode;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "active")]
        public bool? Active { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "azr")]
        public bool? Azr { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "createdAt")]
        public System.DateTime? CreatedAt { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "createdBy")]
        public string CreatedBy { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "id")]
        public long? Id { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "reference")]
        public long? Reference { get; set; }

        /// <summary>
        /// Gets or sets dated Base extension.
        /// </summary>
        [JsonProperty(PropertyName = "updatedAt")]
        public System.DateTime? UpdatedAt { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "vektisCode")]
        public long? VektisCode { get; set; }

    }
}
