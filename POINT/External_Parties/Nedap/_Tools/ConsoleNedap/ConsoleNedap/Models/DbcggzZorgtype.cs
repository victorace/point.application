// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace ConsoleNedap.Models
{
    using Microsoft.Rest;
    using Microsoft.Rest.Serialization;
    using Newtonsoft.Json;
    using System.Linq;

    public partial class DbcggzZorgtype
    {
        /// <summary>
        /// Initializes a new instance of the DbcggzZorgtype class.
        /// </summary>
        public DbcggzZorgtype()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the DbcggzZorgtype class.
        /// </summary>
        /// <param name="brancheIndicatie">Possible values:
        /// BEIDE(0)
        /// GGZ(1)
        /// FZ(2)</param>
        /// <param name="selecteerbaar">NIET_SELECTEERBAAR(0)
        /// SELECTEERBAAR_DIEPSTE_NIVEAU(1)
        /// SELECTEERBAAR_DIEPER_NIVEAU_MOGELIJK(2)</param>
        public DbcggzZorgtype(System.DateTime? begindatum = default(System.DateTime?), string brancheIndicatie = default(string), string code = default(string), System.DateTime? einddatum = default(System.DateTime?), string element = default(string), string groepcode = default(string), int? hierarchieniveau = default(int?), long? id = default(long?), string omschrijving = default(string), string prestatiecodedeel = default(string), string selecteerbaar = default(string), int? sorteervolgorde = default(int?))
        {
            Begindatum = begindatum;
            BrancheIndicatie = brancheIndicatie;
            Code = code;
            Einddatum = einddatum;
            Element = element;
            Groepcode = groepcode;
            Hierarchieniveau = hierarchieniveau;
            Id = id;
            Omschrijving = omschrijving;
            Prestatiecodedeel = prestatiecodedeel;
            Selecteerbaar = selecteerbaar;
            Sorteervolgorde = sorteervolgorde;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// </summary>
        [JsonConverter(typeof(DateJsonConverter))]
        [JsonProperty(PropertyName = "begindatum")]
        public System.DateTime? Begindatum { get; set; }

        /// <summary>
        /// Gets or sets possible values:
        /// BEIDE(0)
        /// GGZ(1)
        /// FZ(2)
        /// </summary>
        [JsonProperty(PropertyName = "brancheIndicatie")]
        public string BrancheIndicatie { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "code")]
        public string Code { get; set; }

        /// <summary>
        /// </summary>
        [JsonConverter(typeof(DateJsonConverter))]
        [JsonProperty(PropertyName = "einddatum")]
        public System.DateTime? Einddatum { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "element")]
        public string Element { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "groepcode")]
        public string Groepcode { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "hierarchieniveau")]
        public int? Hierarchieniveau { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "id")]
        public long? Id { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "omschrijving")]
        public string Omschrijving { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "prestatiecodedeel")]
        public string Prestatiecodedeel { get; set; }

        /// <summary>
        /// Gets or sets NIET_SELECTEERBAAR(0)
        /// SELECTEERBAAR_DIEPSTE_NIVEAU(1)
        /// SELECTEERBAAR_DIEPER_NIVEAU_MOGELIJK(2)
        /// </summary>
        [JsonProperty(PropertyName = "selecteerbaar")]
        public string Selecteerbaar { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "sorteervolgorde")]
        public int? Sorteervolgorde { get; set; }

    }
}
