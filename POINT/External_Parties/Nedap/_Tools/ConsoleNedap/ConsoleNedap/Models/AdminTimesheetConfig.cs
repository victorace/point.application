// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace ConsoleNedap.Models
{
    using Newtonsoft.Json;
    using System.Linq;

    public partial class AdminTimesheetConfig
    {
        /// <summary>
        /// Initializes a new instance of the AdminTimesheetConfig class.
        /// </summary>
        public AdminTimesheetConfig()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the AdminTimesheetConfig class.
        /// </summary>
        public AdminTimesheetConfig(bool? agreeTableEnabled = default(bool?), bool? detailEnabled = default(bool?), long? id = default(long?), string indirectHoursList = default(string), bool? indirectHoursTableEnabled = default(bool?), bool? periodOverviewTableEnabled = default(bool?), string planningText = default(string), bool? workedHoursTableEnabled = default(bool?))
        {
            AgreeTableEnabled = agreeTableEnabled;
            DetailEnabled = detailEnabled;
            Id = id;
            IndirectHoursList = indirectHoursList;
            IndirectHoursTableEnabled = indirectHoursTableEnabled;
            PeriodOverviewTableEnabled = periodOverviewTableEnabled;
            PlanningText = planningText;
            WorkedHoursTableEnabled = workedHoursTableEnabled;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "agreeTableEnabled")]
        public bool? AgreeTableEnabled { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "detailEnabled")]
        public bool? DetailEnabled { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "id")]
        public long? Id { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "indirectHoursList")]
        public string IndirectHoursList { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "indirectHoursTableEnabled")]
        public bool? IndirectHoursTableEnabled { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "periodOverviewTableEnabled")]
        public bool? PeriodOverviewTableEnabled { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "planningText")]
        public string PlanningText { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "workedHoursTableEnabled")]
        public bool? WorkedHoursTableEnabled { get; set; }

    }
}
