// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace ConsoleNedap.Models
{
    using Newtonsoft.Json;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    public partial class MilolistDeviceList
    {
        /// <summary>
        /// Initializes a new instance of the MilolistDeviceList class.
        /// </summary>
        public MilolistDeviceList()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the MilolistDeviceList class.
        /// </summary>
        public MilolistDeviceList(IList<MiloDevice> devices = default(IList<MiloDevice>))
        {
            Devices = devices;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "devices")]
        public IList<MiloDevice> Devices { get; set; }

    }
}
