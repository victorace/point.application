// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace ConsoleNedap.Models
{
    using Newtonsoft.Json;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    public partial class PayrolllistWorkHoursBalanceList
    {
        /// <summary>
        /// Initializes a new instance of the PayrolllistWorkHoursBalanceList
        /// class.
        /// </summary>
        public PayrolllistWorkHoursBalanceList()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the PayrolllistWorkHoursBalanceList
        /// class.
        /// </summary>
        public PayrolllistWorkHoursBalanceList(object workHourBalances = default(object), IList<PayrollWorkHoursBalance> workHoursBalances = default(IList<PayrollWorkHoursBalance>))
        {
            WorkHourBalances = workHourBalances;
            WorkHoursBalances = workHoursBalances;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "workHourBalances")]
        public object WorkHourBalances { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "workHoursBalances")]
        public IList<PayrollWorkHoursBalance> WorkHoursBalances { get; set; }

    }
}
