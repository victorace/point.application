// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace ConsoleNedap.Models
{
    using Newtonsoft.Json;
    using System.Linq;

    public partial class UserStatus
    {
        /// <summary>
        /// Initializes a new instance of the UserStatus class.
        /// </summary>
        public UserStatus()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the UserStatus class.
        /// </summary>
        /// <param name="loginDescription">describes why the user can (not) log
        /// in</param>
        /// <param name="type">describes the state as enum name
        /// - APPLICATION_LOCK_DOWN
        /// - CONTRACT_REQUIRED_USER_NEEDS_NO_CONTRACT
        /// - CONTRACT_REQUIRED_USER_HAS_ACTIVE_CONTRACT
        /// - CONTRACT_REQUIRED_USER_HAS_ACTIVE_CONTRACT_IN_NEAR_FUTURE
        /// - CONTRACT_REQUIRED_USER_HAS_NO_ACTIVE_CONTRACT_IN_NEAR_FUTURE
        /// - CONTRACT_REQUIRED_USER_HAS_NO_CONTRACTS
        /// - CONTRACT_NOT_REQUIRED_USER_HAS_CONTRACT_WITH_NO_ENDDATE
        /// - CONTRACT_NOT_REQUIRED_USER_HAS_NO_CONTRACTS
        /// - CONTRACT_NOT_REQUIRED_USER_HAS_ACTIVE_CONTRACT
        /// - CONTRACT_NOT_REQUIRED_USER_HAS_NO_ACTIVE_CONTRACT</param>
        /// <param name="userLoginAllowed">true if the user is allowed to log
        /// in</param>
        public UserStatus(string loginDescription = default(string), string type = default(string), bool? userLoginAllowed = default(bool?))
        {
            LoginDescription = loginDescription;
            Type = type;
            UserLoginAllowed = userLoginAllowed;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// Gets or sets describes why the user can (not) log in
        /// </summary>
        [JsonProperty(PropertyName = "loginDescription")]
        public string LoginDescription { get; set; }

        /// <summary>
        /// Gets or sets describes the state as enum name
        /// - APPLICATION_LOCK_DOWN
        /// - CONTRACT_REQUIRED_USER_NEEDS_NO_CONTRACT
        /// - CONTRACT_REQUIRED_USER_HAS_ACTIVE_CONTRACT
        /// - CONTRACT_REQUIRED_USER_HAS_ACTIVE_CONTRACT_IN_NEAR_FUTURE
        /// - CONTRACT_REQUIRED_USER_HAS_NO_ACTIVE_CONTRACT_IN_NEAR_FUTURE
        /// - CONTRACT_REQUIRED_USER_HAS_NO_CONTRACTS
        /// - CONTRACT_NOT_REQUIRED_USER_HAS_CONTRACT_WITH_NO_ENDDATE
        /// - CONTRACT_NOT_REQUIRED_USER_HAS_NO_CONTRACTS
        /// - CONTRACT_NOT_REQUIRED_USER_HAS_ACTIVE_CONTRACT
        /// - CONTRACT_NOT_REQUIRED_USER_HAS_NO_ACTIVE_CONTRACT
        /// </summary>
        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets true if the user is allowed to log in
        /// </summary>
        [JsonProperty(PropertyName = "userLoginAllowed")]
        public bool? UserLoginAllowed { get; set; }

    }
}
