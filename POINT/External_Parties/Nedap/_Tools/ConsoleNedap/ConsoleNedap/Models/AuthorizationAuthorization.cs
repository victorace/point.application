// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace ConsoleNedap.Models
{
    using Newtonsoft.Json;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    public partial class AuthorizationAuthorization
    {
        /// <summary>
        /// Initializes a new instance of the AuthorizationAuthorization class.
        /// </summary>
        public AuthorizationAuthorization()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the AuthorizationAuthorization class.
        /// </summary>
        /// <param name="accessrights">Deprecated since: 08-08-2019 - The set
        /// of access rights (actions) which users with this authorization can
        /// perform.</param>
        /// <param name="application">Deprecated since: 08-08-2019 - The
        /// application for which this authorization is valid</param>
        /// <param name="id">Deprecated since: 08-08-2019</param>
        /// <param name="name">Deprecated since: 08-08-2019 - The human
        /// readable name of this authorization.</param>
        public AuthorizationAuthorization(IList<AccessRight> accessrights = default(IList<AccessRight>), string application = default(string), string id = default(string), string name = default(string))
        {
            Accessrights = accessrights;
            Application = application;
            Id = id;
            Name = name;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// Gets or sets deprecated since: 08-08-2019 - The set of access
        /// rights (actions) which users with this authorization can perform.
        /// </summary>
        [JsonProperty(PropertyName = "accessrights")]
        public IList<AccessRight> Accessrights { get; set; }

        /// <summary>
        /// Gets or sets deprecated since: 08-08-2019 - The application for
        /// which this authorization is valid
        /// </summary>
        [JsonProperty(PropertyName = "application")]
        public string Application { get; set; }

        /// <summary>
        /// Gets or sets deprecated since: 08-08-2019
        /// </summary>
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets deprecated since: 08-08-2019 - The human readable name
        /// of this authorization.
        /// </summary>
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

    }
}
