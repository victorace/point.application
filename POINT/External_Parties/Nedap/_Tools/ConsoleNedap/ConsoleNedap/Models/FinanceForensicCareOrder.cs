// <auto-generated>
// Code generated by Microsoft (R) AutoRest Code Generator.
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.
// </auto-generated>

namespace ConsoleNedap.Models
{
    using Microsoft.Rest;
    using Microsoft.Rest.Serialization;
    using Newtonsoft.Json;
    using System.Linq;

    public partial class FinanceForensicCareOrder
    {
        /// <summary>
        /// Initializes a new instance of the FinanceForensicCareOrder class.
        /// </summary>
        public FinanceForensicCareOrder()
        {
            CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the FinanceForensicCareOrder class.
        /// </summary>
        /// <param name="forensischeZorgtitel">Codelijst
        /// https://www.vektis.nl/streams/standaardisatie/codelijsten/COD706-MVJ</param>
        /// <param name="plaatsingsbesluitnummer">9 digit number</param>
        /// <param name="strafrechtketennummer">7 or 8 digits</param>
        public FinanceForensicCareOrder(System.DateTime? beginDate = default(System.DateTime?), long? careOrderObjectId = default(long?), long? clientObjectId = default(long?), System.DateTime? endDate = default(System.DateTime?), string forensischeZorgtitel = default(string), long? id = default(long?), string plaatsingsbesluitnummer = default(string), string strafrechtketennummer = default(string))
        {
            BeginDate = beginDate;
            CareOrderObjectId = careOrderObjectId;
            ClientObjectId = clientObjectId;
            EndDate = endDate;
            ForensischeZorgtitel = forensischeZorgtitel;
            Id = id;
            Plaatsingsbesluitnummer = plaatsingsbesluitnummer;
            Strafrechtketennummer = strafrechtketennummer;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// </summary>
        [JsonConverter(typeof(DateJsonConverter))]
        [JsonProperty(PropertyName = "beginDate")]
        public System.DateTime? BeginDate { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "careOrderObjectId")]
        public long? CareOrderObjectId { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "clientObjectId")]
        public long? ClientObjectId { get; set; }

        /// <summary>
        /// </summary>
        [JsonConverter(typeof(DateJsonConverter))]
        [JsonProperty(PropertyName = "endDate")]
        public System.DateTime? EndDate { get; set; }

        /// <summary>
        /// Gets or sets codelijst
        /// https://www.vektis.nl/streams/standaardisatie/codelijsten/COD706-MVJ
        /// </summary>
        [JsonProperty(PropertyName = "forensischeZorgtitel")]
        public string ForensischeZorgtitel { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "id")]
        public long? Id { get; set; }

        /// <summary>
        /// Gets or sets 9 digit number
        /// </summary>
        [JsonProperty(PropertyName = "plaatsingsbesluitnummer")]
        public string Plaatsingsbesluitnummer { get; set; }

        /// <summary>
        /// Gets or sets 7 or 8 digits
        /// </summary>
        [JsonProperty(PropertyName = "strafrechtketennummer")]
        public string Strafrechtketennummer { get; set; }

    }
}
