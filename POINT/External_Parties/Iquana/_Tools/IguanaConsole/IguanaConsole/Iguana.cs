﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Linq;

namespace IguanaConsole
{

    // More info: http://help.interfaceware.com/kb/1477

    public class Iguana
    {

        public List<String> MessageList = new List<string>();

        private void addMessage(string message)
        {
            MessageList.Add(String.Format("{0:dd-MM HH:mm:ss} - {1}", DateTime.Now, message));
            Console.WriteLine(message);
        }

        public void ApiQuery()
        {
            DateTime starttime = DateTime.Now;
            DateTime lastcheck = new Util().GetLastCheck();

            foreach (string filter in new Util().GetAppSettingList("Filter"))
            {

                NameValueCollection querystring = HttpUtility.ParseQueryString(string.Empty);

                querystring["after"] = lastcheck.ToString("yyyy/MM/dd HH:mm:ss", CultureInfo.InvariantCulture);
                querystring["filter"] = filter;
                querystring["type"] = ConfigurationManager.AppSettings["FilterType"];

                string response = doRequest("api_query", querystring.ToString());

                if (String.IsNullOrEmpty(response))
                {
                    addMessage(String.Format("No response for filter {0}", filter));
                    continue;
                }

                var xdoc = XDocument.Parse(response);

                foreach (var message in xdoc.Root.Elements("message"))
                {
                    string type = message.Attribute("type").Value;
                    string sourcename = message.Attribute("source_name").Value;
                    string timestamp = message.Attribute("time_stamp").Value;
                    string data = message.Attribute("data").Value;
                    data = data.TrimEnd(Environment.NewLine.ToCharArray());

                    addMessage(String.Format("{0} occured for channel [{1}] at {2}\n{3}", type, sourcename, timestamp, data, Environment.NewLine));
                }
            }

            new Util().SetLastCheck(starttime);

        }

        public void GetServerConfig()
        {

            StreamWriter streamwriter = File.CreateText(new Util().GetConfigExportFile());

            streamwriter.WriteLine("channelname;channelsource;host;port;vmdpath;datasource;datausername;reconnections;interval");

            string response = doRequest("get_server_config");
            var xdoc = XDocument.Parse(response);

            foreach(var channel in xdoc.Root.Element("channel_config").Elements("channel"))
            {
                string channelname = channel.Attribute("name").Value;

                foreach (var element in channel.Elements())
                {
                    string channelsource = element.Name.LocalName;
                    string host = getAttributeValue(element, "remote_host");
                    string port = getAttributeValue(element, new[] { "remote_port", "port" });
                    string vmdpath = getAttributeValue(element, new[] { "ack_vmd_path", "vmdfile" });
                    string datasource = getAttributeValue(element, "datasource");
                    string datausername = getAttributeValue(element, "username");
                    string reconnections = getAttributeValue(element, "maximum_database_reconnections");
                    string interval = getAttributeValue(element, new[] { "database_reconnection_interval", "poll_time" });

                    streamwriter.WriteLine(String.Format("{0};{1};{2};{3};\"{4}\";{5};{6};{7};{8}", channelname, channelsource, host, port, vmdpath, datasource, datausername, reconnections, interval));
                }


            }
            streamwriter.Close();
        }

        private string getAttributeValue(XElement element, string attributeName)
        {
            return getAttributeValue(element, new[] { attributeName });
        }

        private string getAttributeValue(XElement element, string[] attributeNames)
        {
            foreach (string attributename in attributeNames)
            {
                var attribute = element.Attribute(attributename);
                if ( attribute != null && attribute.Value != null)
                {
                    return attribute.Value;
                }
            }
            return "";
        }

        public void Status()
        {
            string response = doRequest("status");

            if(String.IsNullOrEmpty(response))
            {
                addMessage(String.Format("No or empty response from iguana-server!"));
                return;
            }

            var xdoc = XDocument.Parse(response);

            var channels = xdoc.Root.Elements("Channel");

            foreach (var channel in channels)
            {
                if (channel.Attribute("Status").Value == "error")
                {
                    addMessage(String.Format("Channel [{0}] has currently status 'error'.", channel.Attribute("Name").Value));
                    restartChannel(channel.Attribute("Name").Value, channel.Attribute("Guid").Value);
                }
            }
        }

        private void restartChannel(string name, string guid, int attempt = 1)
        {
            addMessage(String.Format("Trying to restart channel [{0}], attempt #{1}", name, attempt));

            NameValueCollection querystring = HttpUtility.ParseQueryString(string.Empty);
            querystring["action"] = "start";
            querystring["guid"] = guid;
            doRequest("status", querystring.ToString()); 
            System.Threading.Thread.Sleep(5000); // wait 5 seconds to give Iguana some time

            string currentstatus = getStatus(guid);


            if (currentstatus == "on")
            {
                addMessage(String.Format("Succesfully restarted channel [{0}]", name));
                return;
            }
            if (currentstatus == "..." || currentstatus == "initializing")
            {
                System.Threading.Thread.Sleep(5000); // wait another 5 seconds
                if (getStatus(guid) == "on")
                {
                    addMessage(String.Format("Succesfully restarted channel [{0}] after waiting for initializing", name));
                    return;
                }
            }

            if (attempt < 3)
            {
                restartChannel(name, guid, ++attempt);
            }
            else
            {
                addMessage("========================================================");
                addMessage(String.Format("Could not automatically restart channel [{0}], please check!", name));
                addMessage("========================================================");
            }


        }

        private string getStatus(string guid)
        {
            string response = doRequest("status");
            var xdoc = XDocument.Parse(response);
            var channels = xdoc.Root.Elements("Channel");
            var currentchannel = channels.FirstOrDefault(ch => ch.Attribute("Guid").Value == guid);
            var currentstatus = currentchannel.Attribute("Status").Value;
            return currentstatus;
        }


        private string doRequest(string action, string filter = null)
        {
            var iguanaurl = ConfigurationManager.AppSettings["IguanaUrl"].ToString();
            var iguanaport = Int32.Parse(ConfigurationManager.AppSettings["IguanaPort"].ToString());

            var uribuilder = new UriBuilder(iguanaurl);
            uribuilder.Port = iguanaport;
            uribuilder.Path = action;
            if (!String.IsNullOrEmpty(filter))
            {
                uribuilder.Query = filter;
            }

            var uri = uribuilder.ToString();


            var webrequest = WebRequest.Create(uri);

            new Util().SetBasicAuthorization(webrequest);
            webrequest.Method = "GET";

            string response = "";
            try
            {
                var webresponse = (HttpWebResponse)webrequest.GetResponse();
                if (webresponse.StatusCode != HttpStatusCode.OK)
                {
                    addMessage(String.Format("Got HttpStatusCode {0} on request [{1}]", webresponse.StatusCode, uri));
                }
                else
                {
                    using (var responsestream = webresponse.GetResponseStream())
                    {
                        using (var streamreader = new StreamReader(responsestream))
                        {
                            response = streamreader.ReadToEnd();
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                addMessage(String.Format("EXCEPTION: '{0}' on request [{1}]", ex.Message, uri));
            }

            return response;

        }


    }
}
