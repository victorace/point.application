﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace IguanaConsole
{
    public class Util
    {

        private string getLastCheckFile()
        {
            string currentDir = System.Reflection.Assembly.GetExecutingAssembly().Location;

            return Path.Combine(Path.GetDirectoryName(currentDir), "LastCheck.txt");
        }

        public string GetConfigExportFile()
        {
            string currentDir = System.Reflection.Assembly.GetExecutingAssembly().Location;

            return Path.Combine(Path.GetDirectoryName(currentDir), "ConfigExport.csv");
        }

        public DateTime GetLastCheck()
        {
            if (!File.Exists(getLastCheckFile()))
            {
                return DateTime.Now.AddDays(-2);
            }
            string contents = File.ReadAllText(getLastCheckFile());

            return DateTime.ParseExact(contents, "dd-MM-yyyy HH:mm:ss", CultureInfo.InvariantCulture);
        }

        public void SetLastCheck(DateTime timestamp)
        {
            File.WriteAllText(getLastCheckFile(), timestamp.ToString("dd-MM-yyyy HH:mm:ss", CultureInfo.InvariantCulture));
        }


        public List<string> GetAppSettingList(string keyName)
        {
            var keys = ConfigurationManager.AppSettings.Keys;
            return keys.Cast<object>()
                       .Where(key => key.ToString().ToLower()
                       .StartsWith(keyName.ToLower()))
                       .Select(key => ConfigurationManager.AppSettings.Get(key.ToString())).ToList();
        }


        public void SetBasicAuthorization(WebRequest webrequest)
        {
            string username = ConfigurationManager.AppSettings["IguanaUserName"];
            string password = ConfigurationManager.AppSettings["IguanaPassword"];
            string encoded = Convert.ToBase64String(Encoding.UTF8.GetBytes(String.Format("{0}:{1}", username, password))); 
            webrequest.Headers.Add("Authorization", "Basic " + encoded);
        }

    }
}
