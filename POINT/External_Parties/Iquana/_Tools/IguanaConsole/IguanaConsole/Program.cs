﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.IO;
using System.Configuration;

namespace IguanaConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            
            if (args == null || args.Length == 0 || args[0] == "check")
            {
                doCheck();
            }
            else if (args[0] == "config")
            {
                doConfig();
            }

            if (bool.Parse(ConfigurationManager.AppSettings["CloseWhenDone"]) == false)
            {
                Console.WriteLine("Done. Press a key.");
                Console.ReadKey();
            }




        }

        private static void doCheck()
        {
            var iguana = new Iguana();
            iguana.Status();
            iguana.ApiQuery();

            if (iguana.MessageList.Count > 0)
            {
                Mail mail = new Mail();
                mail.Body = String.Join(Environment.NewLine, iguana.MessageList).Replace(Environment.NewLine, "<br />");
                mail.SendMail();
            }

        }

        private static void doConfig()
        {
            var iguana = new Iguana();
            iguana.GetServerConfig();
        }


    }
}
