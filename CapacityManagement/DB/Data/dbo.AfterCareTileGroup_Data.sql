SET IDENTITY_INSERT [dbo].[AfterCareTileGroup] ON
INSERT INTO [dbo].[AfterCareTileGroup] ([AfterCareTileGroupID], [Name], [SortOrder], [CssClass]) VALUES (1, N'WLZ', 1, N'capacity-wlz')
INSERT INTO [dbo].[AfterCareTileGroup] ([AfterCareTileGroupID], [Name], [SortOrder], [CssClass]) VALUES (2, N'ZVW', 2, N'capacity-zvw')
INSERT INTO [dbo].[AfterCareTileGroup] ([AfterCareTileGroupID], [Name], [SortOrder], [CssClass]) VALUES (3, N'Anders', 3, N'capacity-anders')
INSERT INTO [dbo].[AfterCareTileGroup] ([AfterCareTileGroupID], [Name], [SortOrder], [CssClass]) VALUES (4, N'Overig', 4, N'capacity-overig')
SET IDENTITY_INSERT [dbo].[AfterCareTileGroup] OFF
