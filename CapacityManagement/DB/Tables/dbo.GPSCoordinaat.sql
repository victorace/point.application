CREATE TABLE [dbo].[GPSCoordinaat]
(
[GPSCoordinaatID] [int] NOT NULL IDENTITY(1, 1),
[Postcode] [char] (6) COLLATE Latin1_General_CI_AS NOT NULL,
[Latitude] [float] NULL,
[Longitude] [float] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[GPSCoordinaat] ADD CONSTRAINT [PK_GPSCoordinaat] PRIMARY KEY CLUSTERED  ([GPSCoordinaatID]) ON [PRIMARY]
GO
