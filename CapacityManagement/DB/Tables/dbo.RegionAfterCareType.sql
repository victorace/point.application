CREATE TABLE [dbo].[RegionAfterCareType]
(
[RegionAfterCareTypeID] [int] NOT NULL IDENTITY(1, 1),
[RegionID] [int] NOT NULL,
[AfterCareTypeID] [int] NOT NULL
) ON [PRIMARY]
GO
