CREATE TABLE [dbo].[AfterCareType]
(
[AfterCareTypeID] [int] NOT NULL,
[AfterCareTypeName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AfterCareCategoryID] [int] NULL,
[AfterCareGroupID] [int] NULL,
[Description] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[IsActive] [bit] NOT NULL CONSTRAINT [DF_AfterCareType_IsActive] DEFAULT ((1))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AfterCareType] ADD CONSTRAINT [PK_AfterCareType] PRIMARY KEY CLUSTERED  ([AfterCareTypeID]) ON [PRIMARY]
GO
