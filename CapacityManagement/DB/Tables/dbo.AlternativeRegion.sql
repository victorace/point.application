CREATE TABLE [dbo].[AlternativeRegion]
(
[AlternativeRegionID] [int] NOT NULL IDENTITY(1, 1),
[DepartmentCapacityID] [int] NOT NULL,
[RegionID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AlternativeRegion] ADD CONSTRAINT [PK_AlternativeRegion] PRIMARY KEY CLUSTERED  ([AlternativeRegionID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AlternativeRegion] ADD CONSTRAINT [FK_AlternativeRegion_DepartmentCapacity] FOREIGN KEY ([DepartmentCapacityID]) REFERENCES [dbo].[DepartmentCapacity] ([DepartmentCapacityID])
GO
ALTER TABLE [dbo].[AlternativeRegion] ADD CONSTRAINT [FK_AlternativeRegion_Region] FOREIGN KEY ([RegionID]) REFERENCES [dbo].[Region] ([RegionID])
GO
