CREATE TABLE [dbo].[LogRead]
(
[LogReadID] [int] NOT NULL IDENTITY(1, 1),
[Timestamp] [datetime] NULL CONSTRAINT [DF_LogRead_Timestamp] DEFAULT (getdate()),
[Url] [varchar] (2048) COLLATE Latin1_General_CI_AS NULL,
[UrlReferrer] [varchar] (2048) COLLATE Latin1_General_CI_AS NULL,
[Querystring] [varchar] (2048) COLLATE Latin1_General_CI_AS NULL,
[PostData] [varchar] (2048) COLLATE Latin1_General_CI_AS NULL,
[Cookies] [varchar] (2048) COLLATE Latin1_General_CI_AS NULL,
[UserAgent] [varchar] (500) COLLATE Latin1_General_CI_AS NULL,
[IsAjaxRequest] [varchar] (50) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
