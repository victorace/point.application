CREATE TABLE [dbo].[Region]
(
[RegionID] [int] NOT NULL,
[Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CapacityBeds] [bit] NOT NULL CONSTRAINT [DF_Region_CapacityBeds] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Region] ADD CONSTRAINT [PK_Region] PRIMARY KEY CLUSTERED  ([RegionID]) ON [PRIMARY]
GO
