CREATE TABLE [dbo].[AfterCareTileGroup]
(
[AfterCareTileGroupID] [int] NOT NULL,
[Name] [nvarchar] (20) COLLATE Latin1_General_CI_AS NOT NULL,
[SortOrder] [int] NOT NULL,
[CssClass] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL CONSTRAINT [DF_AfterCareTileGroup_CssClass] DEFAULT ('')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AfterCareTileGroup] ADD CONSTRAINT [PK_AfterCareTileGroup] PRIMARY KEY CLUSTERED  ([AfterCareTileGroupID]) ON [PRIMARY]
GO
