CREATE TABLE [dbo].[DepartmentCapacity]
(
[DepartmentCapacityID] [int] NOT NULL IDENTITY(1, 1),
[AfterCareTypeID] [int] NULL,
[AfterCareTypeName] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[AfterCareGroupID] [int] NULL,
[RegionID] [int] NOT NULL,
[RegionName] [varchar] (255) COLLATE Latin1_General_CI_AS NOT NULL,
[OrganizationID] [int] NOT NULL,
[OrganizationName] [varchar] (255) COLLATE Latin1_General_CI_AS NOT NULL,
[LocationID] [int] NOT NULL,
[LocationName] [varchar] (255) COLLATE Latin1_General_CI_AS NOT NULL,
[LocationStreet] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[LocationNumber] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[LocationPostalCode] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[LocationCity] [varchar] (512) COLLATE Latin1_General_CI_AS NULL,
[LocationEmailAddress] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[DepartmentID] [int] NOT NULL,
[DepartmentName] [varchar] (255) COLLATE Latin1_General_CI_AS NOT NULL,
[DepartmentPhoneNumber] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[DepartmentFaxNumber] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[DepartmentEmailAddress] [varchar] (1024) COLLATE Latin1_General_CI_AS NULL,
[IsActive] [bit] NOT NULL CONSTRAINT [DF_DepartmentCapacity_IsActive] DEFAULT ((0)),
[AdjustmentCapacityDate] [datetime] NULL,
[Capacity1] [int] NULL,
[Capacity2] [int] NULL,
[Capacity3] [int] NULL,
[Capacity4] [int] NULL,
[Information] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[ModifiedBy] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[ModifiedOn] [datetime] NULL,
[CapacityFunctionality] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DepartmentCapacity] ADD CONSTRAINT [PK_DepartmentCapacity] PRIMARY KEY CLUSTERED  ([DepartmentCapacityID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DepartmentCapacity] ADD CONSTRAINT [FK_Department_AfterCareGroup] FOREIGN KEY ([AfterCareGroupID]) REFERENCES [dbo].[AfterCareGroup] ([AfterCareGroupID])
GO
