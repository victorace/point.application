﻿using System.Collections.Generic;

namespace Point.CapacityManagement.Web.ViewModels
{
    public class AfterCareGroupViewModel
    {
        public int AfterCareGroupID { get; set; }
        public string AfterCareGroupName { get; set; }
        public List<AfterCareTypeViewModel> AfterCareTypes { get; set; }
        public int SortOrder { get; set; }
        public int Capacity { get; set; }       
        public string ActiveClass { get; set; }
        public override string ToString()
        {
            return $"{AfterCareGroupID} {AfterCareGroupName} n={Capacity}";
        }
    }
}