﻿using System.Collections.Generic;

namespace Point.CapacityManagement.Web.ViewModels
{
    public class AfterCareFinancingViewModel
    {
        public int GroupID { get; set; }
        public string Name { get; set; }
        public string CssClass { get; set; }
        public List<AfterCareGroupViewModel> AfterCareGroups { get; set; }
        public int SortOrder { get; set; }
        public string ActiveClass { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}