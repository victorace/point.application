﻿using System;
using System.Collections.Generic;
using Point.CapacityManagement.Business.Logic;
using Point.CapacityManagement.Business.Models;
using Point.CapacityManagement.Web.Controllers.Shared;

namespace Point.CapacityManagement.Web.ViewModels
{
    public class CapacityPageViewModel
    {
        public virtual CapacityViewType ViewType { get; set; }
        public virtual string Title { get; set; }
        public List<IdAndName> RegionList { get; set; }
        //public int RegionIds { get; set; }
        public List<int> RegionIds { get; set; } 
        public CapacityPageViewModel(CapacityViewModelMapper mapper, List<int> regionIds, int idForEmptyListItem)
        {
            try
            {
                RegionIds = regionIds;
                var regions = mapper.GetRegionList();
                RegionList = SelectListHelper.AddEmptyItem(regions, idForEmptyListItem, "Alle regio's");
            }
            catch (Exception ex) 
            {
                LoggingBL.Error("Could not retrieve regions", ex);
            }
        }

        public string MenuActiveClass(CapacityViewType view)
        {
            return view == ViewType ? "menu-active" : "";
        }
    }

    public enum CapacityViewType
    {
        Tiles,
        List,
        Map
    }
}
