﻿using System;
using System.Collections.Generic;
using Point.CapacityManagement.Business.Models;
using Point.CapacityManagement.Web.Controllers.Shared;

namespace Point.CapacityManagement.Web.ViewModels
{
    public class CapacityListViewModel : CapacityPageViewModel
    {
        public PagingViewModel Paging { get; set; }
        public override CapacityViewType ViewType => CapacityViewType.List;
        public override string Title => "Lijstweergave";

        public string AfterCareGroupIdAndTypeId { get; set; }
        public List<ValueAndName> AfterCareGroupAndTypeList { get; set; }

        public int AfterCareGroupID { get; set; }

        public int AfterCareTypeID { get; set; }

        public string NameSearch1 { get; set; }
        public string NameSearch2 { get; set; }
        public string CitySubstring { get; set; }

        public string PostalCodeSubstring { get; set; }

        public int PostalCodeRadius { get; set; }

        public List<IdAndName> PostalCodeRadiusList { get; set; }
        public bool WithCapacityOnly { get; set; }

        public string RegionsFromTile { get; set; }

        public List<CapacityViewModel> CapacityList { get; set; }

        public CapacityListViewModel(CapacityViewModelMapper mapper, List<int> regionIds, int idForEmptyListItem)
            : base(mapper, regionIds, idForEmptyListItem)
        {
            // Nothing
        }

        /// <summary>
        /// If today is 20-Nov-2017,
        /// the outputs will be: 20-11, 21-11, 22-11, 23-11
        /// </summary>
        public void GetDayTitles(out string day1, out string day2, out string day3, out string day4)
        {
            var now = DateTime.Now;

            day1 = now.ToString("dd-MM");
            day2 = now.AddDays(1).ToString("dd-MM");
            day3 = now.AddDays(2).ToString("dd-MM");
            day4 = now.AddDays(3).ToString("dd-MM");
        }

        /// <summary>
        /// If today is Wed,
        /// the output will be: "wo: {0}, do: {1}, vr: {2}, za: {3}"
        /// </summary>
        /// <returns></returns>
        public string Get4DaysOfTheWeekFormatString()
        {
            var days = new[] {"zo", "ma", "di", "wo", "do", "vr", "za", "zo", "ma", "di", "wo" };

            var day = (int) DateTime.Now.DayOfWeek;

            return $"{days[day++]}: {{0}}, {days[day++]}: {{1}}, {days[day++]}: {{2}}, {days[day]}: {{3}}";
        }
    }
}
