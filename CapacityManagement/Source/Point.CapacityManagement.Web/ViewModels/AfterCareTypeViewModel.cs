﻿using System.Collections.Generic;
using Point.CapacityManagement.Database.Models;

namespace Point.CapacityManagement.Web.ViewModels
{
    public class AfterCareTypeViewModel
    {
        public int AfterCareTypeID { get; set; }
        public string AfterCareTypeName { get; set; }
        public int AfterCareTypeCapacity { get; set; }              
        public string ActiveClass { get; set; }
        public List<RegionAfterCareType> RegionAfterCareTypes { get; set; }
        public override string ToString()
        {
            return $"{AfterCareTypeID} {AfterCareTypeName} n={AfterCareTypeCapacity}";
        }
    }
}