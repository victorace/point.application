﻿using System.Collections.Generic;
using Point.CapacityManagement.Web.Controllers.Shared;

namespace Point.CapacityManagement.Web.ViewModels
{
    public class CapacityTilesViewModel : CapacityPageViewModel
    {
        public override CapacityViewType ViewType => CapacityViewType.Tiles;
        public override string Title => "Tegelweergave";

        public List<AfterCareFinancingViewModel> FinancingList { get; set; }

        public CapacityTilesViewModel(CapacityViewModelMapper mapper, List<int> regionIds, int idForEmptyListItem) : base(mapper, regionIds, idForEmptyListItem) { }
    }
}
