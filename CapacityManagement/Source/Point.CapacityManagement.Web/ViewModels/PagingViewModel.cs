﻿
namespace Point.CapacityManagement.Web.ViewModels
{
    public class PagingViewModel
    {
        public static int PageSize = 30;
        public int PageIndex { get; set; }
        public int TotalCount { get; set; }

        public PagingViewModel(int pageIndex, int totalCount)
        {
            PageIndex = pageIndex;
            TotalCount = totalCount;
        }

        public int GetStartIndex()
        {
            return (PageIndex - 1) * PageSize + 1;
        }

        public int GetEndIndex()
        {
            var endIndex = PageIndex * PageSize;
            return endIndex < TotalCount ? endIndex : TotalCount;
        }
    }
}
