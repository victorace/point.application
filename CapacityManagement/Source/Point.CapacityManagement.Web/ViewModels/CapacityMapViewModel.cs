﻿using System.Collections.Generic;
using Point.CapacityManagement.Web.Controllers.Shared;
using Point.CapacityManagement.Business.Models;

namespace Point.CapacityManagement.Web.ViewModels
{
    public class CapacityMapViewModel : CapacityPageViewModel
    {
        public override CapacityViewType ViewType => CapacityViewType.Map;
        public override string Title => "Mapweergave";

        public string AfterCareGroupIdAndTypeId { get; set; }
        public List<ValueAndName> AfterCareGroupAndTypeList { get; set; }

        public List<DepartmentCapacityGPSViewModel> DepartmentCapacityGPSList { get; set; } 

        public string AfterCareCategoryIDs { get; set; }

        public CapacityMapViewModel(CapacityViewModelMapper mapper,List<int> regionIds, int idForEmptyListItem)
            : base(mapper, regionIds, idForEmptyListItem)
        {
            // Nothing
        }
    }
}
