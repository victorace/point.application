﻿using System;

namespace Point.CapacityManagement.Web.ViewModels
{
    public class CapacityViewModel
    {
        public int AfterCareGroupID { get; set; } // For dropdownlist, and not for the results table

        // FOR CAPACITY TABLE:
        public string OrganizationName { get; set; }
        public string LocationName { get; set; }
        public string DepartmentName { get; set; }
        public string LocationCity { get; set; }

        public int? Capacity1 { get; set; }
        public int? Capacity2 { get; set; }
        public int? Capacity3 { get; set; }
        public int? Capacity4 { get; set; }

        public string Information { get; set; }
        public DateTime? ModifiedOn { get; set; }

        // FOR MODAL BOX:
        public string LocationStreet { get; set; }
        public string LocationNumber { get; set; }
        public string LocationPostalCode { get; set; }
        public string LocationEmailAddress { get; set; }

        public string DepartmentPhoneNumber { get; set; }
        public string DepartmentFaxNumber { get; set; }
        public string DepartmentEmailAddress { get; set; }

        // CALCULATED:
        public string LocationAddress => $"{LocationStreet} {LocationNumber}, {LocationPostalCode} {LocationCity}"
            .Trim(',', ' ');

        public string Capacities(string format)
        {
            return string.Format(format, Capacity1 ?? 0, Capacity2 ?? 0, Capacity3 ?? 0, Capacity4 ?? 0);
        }

        public override string ToString() // useful during/for debugging
        {
            return $"group={AfterCareGroupID} " +
                   $"OLDC={OrganizationName}/{LocationName}/{DepartmentName}/{LocationCity} " +
                   $"cap={Capacity1}/{Capacity2}/{Capacity3}/{Capacity4}";
        }
    }
}
