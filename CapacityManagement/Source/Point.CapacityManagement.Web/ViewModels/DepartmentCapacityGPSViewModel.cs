﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Point.CapacityManagement.Web.ViewModels
{
    public class DepartmentCapacityGPSViewModel
    {
        public string OrganizationName { get; set; }
        public string LocationName { get; set; }
        public List<string> DepartmentNames { get; set; }
        public List<int?> DepartmentCapacities { get; set; }
        public int? TotalCapacity { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }

        public DepartmentCapacityGPSViewModel()
        {
            DepartmentNames = new List<string>();
            DepartmentCapacities = new List<int?>();
        }

        public string ToMapsString()
        {
            //Newtonsoft.Json.Encod
            return JsonConvert.SerializeObject(this, Formatting.Indented);

           // return $"{{ lat: {Latitude.ToString().Replace(",", ".")}, lon: {Longitude.ToString().Replace(",", ".")}, title: '{String.Join("\r\n", DepartmentNames)}' }}"; //< text >{ lat: @marker.GPSCoordinaat.Latitude.ToString().Replace(",", "."), lng: @marker.GPSCoordinaat.Longitude.ToString().Replace(",", "."), title: '@marker.DepartmentCapacity.LocationName @marker.DepartmentCapacity.DepartmentName', capacity: '@marker.DepartmentCapacity.Capacity1' },</ text >
        }
    }
}