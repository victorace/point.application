﻿using System;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.Routing;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http.Controllers;


namespace Point.CapacityManagement.Web.Attributes
{
    public static class AntiFiddleInjection
    {
        public static string DigestKey = "Digest";
        public static string CreateDigest(object value)
        {
            if (value == null)
            {
                return "";
            }
            byte[] protectedbytes = MachineKey.Protect(Encoding.UTF8.GetBytes(Convert.ToString(value)), "Encryption");
            return protectedbytes.ByteArrayToHexString();
        }

        public static void AddDigest(this RouteValueDictionary routeValues, object value)
        {
            if (routeValues.ContainsKey(DigestKey))
            {
                routeValues.Remove(DigestKey);
            }
            routeValues.Add(DigestKey, CreateDigest(value));

        }

        public static MvcHtmlString AntiFiddleToken(this HtmlHelper html, object value)
        {
            return generateHiddenFormField(value);
        }

        public static MvcHtmlString AntiFiddleTokenFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression)
        {
            object modelValue = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData).Model;

            return generateHiddenFormField(modelValue);
        }

        private static MvcHtmlString generateHiddenFormField(object value)
        {
            TagBuilder tagbuilder = new TagBuilder("input");
            tagbuilder.Attributes["type"] = "hidden";
            tagbuilder.Attributes["name"] = DigestKey;
            tagbuilder.Attributes["value"] = CreateDigest(value);
            return new MvcHtmlString(tagbuilder.ToString(TagRenderMode.SelfClosing));
        }

        public static void AddDigest(this RouteValueDictionary routeValues, string routeValueKey)
        {
            if (routeValues.ContainsKey(routeValueKey))
            {
                AddDigest(routeValues, routeValues[routeValueKey]);
            }
        }

        public static string GetDigestValue(this HttpContextBase httpContextBase)
        {
            string value = ValidateAntiFiddleInjectionAttribute.GetParam(httpContextBase.Request, DigestKey);
            return value;
        }

        public static string GetPlainDigest(string digest)
        {
            byte[] plainbytes = MachineKey.Unprotect(digest.HexStringToByteArray(), "Encryption");
            string plaindigest = Encoding.UTF8.GetString(plainbytes);
            return plaindigest;
        }

        public static bool CheckDigest(object value, string digest)
        {
            if (String.IsNullOrWhiteSpace(digest) || value == null)
            {
                return false;
            }

            return GetPlainDigest(digest) == Convert.ToString(value);
        }

        /// <summary>
        /// Fastest methods to convert byte[] to string and vice versa
        /// http://stackoverflow.com/questions/623104/byte-to-hex-string/3974535#3974535
        /// </summary>

        public static string ByteArrayToHexString(this byte[] bytes)
        {
            char[] c = new char[bytes.Length * 2];

            byte b;

            for (int bx = 0, cx = 0; bx < bytes.Length; ++bx, ++cx)
            {
                b = ((byte)(bytes[bx] >> 4));
                c[cx] = (char)(b > 9 ? b + 0x37 + 0x20 : b + 0x30);

                b = ((byte)(bytes[bx] & 0x0F));
                c[++cx] = (char)(b > 9 ? b + 0x37 + 0x20 : b + 0x30);
            }

            return new string(c);
        }

        public static byte[] HexStringToByteArray(this string s)
        {
            if (String.IsNullOrWhiteSpace(s) || s.Length % 2 != 0)
                return new byte[0];

            byte[] buffer = new byte[s.Length / 2];
            char c;
            for (int bx = 0, sx = 0; bx < buffer.Length; ++bx, ++sx)
            {
                // Convert first half of byte
                c = s[sx];
                buffer[bx] = (byte)((c > '9' ? (c > 'Z' ? (c - 'a' + 10) : (c - 'A' + 10)) : (c - '0')) << 4);

                // Convert second half of byte
                c = s[++sx];
                buffer[bx] |= (byte)(c > '9' ? (c > 'Z' ? (c - 'a' + 10) : (c - 'A' + 10)) : (c - '0'));
            }

            return buffer;
        }
    }

    public class ValidateAntiFiddleInjectionAttribute : System.Web.Http.Filters.ActionFilterAttribute
    {
        private string propertyName { get; set; }
        private bool nullAllowed { get; set; }

        public ValidateAntiFiddleInjectionAttribute(string propertyName, bool nullAllowed = false)
        {
            this.propertyName = propertyName;
            this.nullAllowed = nullAllowed;
        }

        public override void OnActionExecuting(HttpActionContext filterContext)
        {
            if (String.IsNullOrWhiteSpace(propertyName))
            {
                throw new Exception("Digest.propertyName value must be a non empty string.");
            }
            var paramValue = GetParam(filterContext.Request, propertyName);
            var digestValue = GetParam(filterContext.Request, "Digest");

            if (string.IsNullOrEmpty(paramValue.Trim()))
            {
                throw new Exception(String.Format("Digest.PropertyName [{0}] must be present and contain a value.", propertyName));
            }
            if (string.IsNullOrWhiteSpace(digestValue))
            {
                throw new Exception(String.Format("Digest must be present and contain a value."));
            }
            if (!AntiFiddleInjection.CheckDigest(paramValue, digestValue))
            {
                throw new Exception(String.Format("Digest check failed. digestvalue [{0}], propertyvalue [{1}], plaindigest [{2}]", digestValue, paramValue, AntiFiddleInjection.GetPlainDigest(paramValue)));
            }
            base.OnActionExecuting(filterContext);
        }

        public static string GetParam(HttpRequestMessage httpRequest, string paramName)
        {
            var result = "";
            IEnumerable<string> keyValues;
            if (httpRequest.Headers.TryGetValues(paramName, out keyValues))
            {
                result = keyValues.FirstOrDefault();
            };
            return result;
        }

        public static string GetParam(HttpRequestBase httpRequest, string keyName)
        {
            var querystringkeys = httpRequest.Unvalidated.QueryString.AllKeys;
            IDictionary<string, string> querystringvalues = querystringkeys.ToDictionary(k => k.ToLowerInvariant(), k => httpRequest.Unvalidated.QueryString[k]);
            var formkeys = httpRequest.Unvalidated.Form.AllKeys.ToArray();
            IDictionary<string, string> formvalues = formkeys.ToDictionary(k => k.ToLowerInvariant(), k => httpRequest.Unvalidated.Form[k]);

            var allvalues =
                querystringvalues.Concat(formvalues.Where(it => querystringvalues.All(qsv => qsv.Key != it.Key)))
                    .ToDictionary(pair => pair.Key, pair => pair.Value);

            if (allvalues.ContainsKey(keyName.ToLowerInvariant()))
            {
                return allvalues[keyName.ToLowerInvariant()];
            }

            allvalues = httpRequest.RequestContext.RouteData.Values.ToDictionary(v => v.Key.ToLowerInvariant(), v => v.Value.ToString());

            return allvalues.ContainsKey(keyName.ToLowerInvariant())
                ? allvalues[keyName.ToLowerInvariant()]
                : null;
        }
    }
}