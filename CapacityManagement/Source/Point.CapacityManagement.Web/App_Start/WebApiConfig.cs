﻿using Point.CapacityManagement.Web.Controllers.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace Point.CapacityManagement.Web
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();

            config.MessageHandlers.Add(new RequireHttpsHandler());


            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

        }
    }
}
