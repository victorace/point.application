﻿var timeoutgap;
$(document).ready(function() {
    // ----------------------------------------------
    // SUBMIT ON CHANGE: 

    $(".submit-on-change").change(function () {
        $(".static").hide();
        $(".spinner").show();

        if (typeof window.idForEmptyListItem !== "undefined") {
            if (this.id === "AfterCareGroupID") {
                $("#AfterCareTypeID").val(window.idForEmptyListItem); // todo: get "0" value from razor
            } else if (this.id === "AfterCareTypeID") {
                $("#AfterCareGroupID").val(window.idForEmptyListItem);
            }
        }

        $("#pageIndex").val(1);

        $("form").submit(); 
    });

    // ----------------------------------------------
    // PAGING:
    function changePageAndSubmit(increment) {
        var pageIndex = $("#pageIndex");
        pageIndex.val(parseInt(pageIndex.val()) + increment);
        $("form").submit();
    }

    $("#previousPage.enabled").click(function() {
        changePageAndSubmit(-1);
    });
    $("#nextPage.enabled").click(function() {
        changePageAndSubmit(1);
    });

    // ----------------------------------------------
    // CLICK ON CAPACITY ROW --> SHOW MODAL:
    function assignAnchorTag(modal, tr, id, tag, prefix) {
        var value = tr.data(id);

        if (value === "") {
            modal.find("#" + id + "Div").hide();
        } else {
            modal.find("#" + id)
                .attr("href", value === "" ? "" : tag + value)
                .text(value === "" ? "" : prefix + value);
            modal.find("#" + id + "Div").show();
        }
    }

    $("table#capacityListTable tbody tr").click(function() {
        var modal = $("#capacityInfoModal");
        var tr = $(this);
        var departmentFaxNumber = tr.data("departmentFaxNumber");

        modal.find("#organizationName").text(tr.data("organizationName"));
        modal.find("#locationName").text(tr.data("locationName"));
        modal.find("#departmentName").text(tr.data("departmentName"));
        modal.find("#locationAddress").text(tr.data("locationAddress"));
        modal.find("#departmentFaxNumber").text(departmentFaxNumber === "" ? "" : "fax:" + departmentFaxNumber);

        assignAnchorTag(modal, tr, "locationEmailAddress", "mailto:", "email: ");
        assignAnchorTag(modal, tr, "departmentPhoneNumber", "tel:", "tel: ");
        assignAnchorTag(modal, tr, "departmentEmailAddress", "mailto:", "email: ");

        modal.find("#capacities").text(tr.data("capacities"));

        modal.modal("show");
    });
});
