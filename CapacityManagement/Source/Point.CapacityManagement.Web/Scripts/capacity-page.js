﻿var regionhaschanges = false;

function multiselect_initialise() {
    $('#RegionIds').multiselect({
        includeSelectAllOption: true,
        selectAllText: "Alles/Niets Selecteren",
        allSelectedText: "Alles geselecteerd",
        nonSelectedText: "Selecteer regio(s)",
        disableIfEmpty: true,
        maxHeight: 300,
        buttonWidth: '100%',
        numberDisplayed: 1,
        nSelectedText: 'geselecteerd',
        onDropdownHide: function () {
            if (regionhaschanges) {
                $("form").submit();
            }
        }
    });

    $('#RegionIds').change(function () {
        regionhaschanges = true;
    });

}

$(function () {
    multiselect_initialise();

    $(".show-info").click(function() {

        var url = "AfterCareType/Descriptions/" + $(this).data("aftercaretilegroupid");

        $("#afterCareTypeDescriptionsModal").find(".modal-body").load(url, function () {
            $("#afterCareTypeDescriptionsModal").modal("show");
        });
    });

    $(".show-info-category").click(function () {
        var url = "AfterCareType/DescriptionsByCategory/?"
        $.each($(this).data("aftercarecategoryids"), function (ix, el) {
            url += "ids=" + el + "&";
        });

        $("#afterCareTypeDescriptionsModal").find(".modal-body").load(url, function () {
            $("#afterCareTypeDescriptionsModal").modal("show");
        });

    })
});
