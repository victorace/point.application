﻿function openAfterCareTypes(currentItem) {

    var aftercaretypes = $(".aftercaretypes[data-aftercaregroupid='" + $(currentItem).data("aftercaregroupid") + "']");
    var isvisible = aftercaretypes.is(":visible");
    $(".aftercaretypes").slideUp(300);
    $(".capacity-expand").children("span").removeClass("glyphicon-chevron-up").addClass("glyphicon-chevron-down");
    $(".capacity-count").removeClass("inactive");
    if (!isvisible) {
        aftercaretypes.slideToggle(300);
        $(currentItem).children("span").toggleClass("glyphicon-chevron-down").toggleClass("glyphicon-chevron-up");
    }

    $(".capacity-groupname-header").removeClass("active").addClass("normal");

    var currentheader = $(currentItem).parent(".capacity-groupname-header");

    if (!isvisible) {
        currentheader.parents(".row").find(".capacity-groupname-header").removeClass("normal").addClass("inactive");
        currentheader.parents(".row").find(".capacity-count").addClass("inactive");
        currentheader.removeClass("inactive").addClass("active");
        currentheader.find(".capacity-count").removeClass("inactive").addClass("active");
    }

    $(".capacity-aftercare-header.DeActiveInRegion").removeClass("active");       
}

$(function () {
    $(".capacity-expand").click(function (event) {
        event.preventDefault();
        openAfterCareTypes(this);
    });
});