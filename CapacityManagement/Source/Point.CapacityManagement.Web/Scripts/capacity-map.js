﻿function initMap() {

    var myLatlng = new google.maps.LatLng(51.99, 5.12); 
    var myOptions = {
        zoom: 8,
        center: myLatlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    var map = new google.maps.Map(document.getElementById("map"), myOptions);


    var markers = [];
    locations.map(function (loc, ix) {
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(loc.Latitude, loc.Longitude),
            label: loc.TotalCapacity.toString(),
            title: loc.LocationName,
            totalcapacity: loc.TotalCapacity
        });

        var infoWindow = new google.maps.InfoWindow({ maxWidth: 400 });
        var html = "<div>";
        html += "<h4>" + loc.OrganizationName + " " + loc.LocationName + ": " + loc.TotalCapacity + "</h4>";
        loc.DepartmentNames.map(function (departmentname, ix) {
            html += "&bull; " + departmentname + ": " + loc.DepartmentCapacities[ix] + "<br />";
        });
        html += "</div>";
        bindInfoWindow(marker, map, infoWindow, html);
        markers.push(marker);

    });

    var markerCluster = new MarkerClusterer(map, markers, { imagePath: '/images/markerclusterer/m' });

    markerCluster.setCalculator(function (markers, numStyles) {
        var index = 0;
        var total = 0;

        markers.map(function (marker, ix) {
            total += parseInt(marker.totalcapacity, 10);
        });

        index = Math.min(index, numStyles);

        return { text: total, index: index };

    });

}

function bindInfoWindow(marker, map, infoWindow, html) {
    google.maps.event.addListener(marker, 'click', function () {
        infoWindow.setContent(html);
        infoWindow.open(map, marker);
    });
} 


$(function () {
    $(".submit-on-change").change(function () {

        $(".static").hide();
        $(".spinner").show();

        $("#pageIndex").val(1);

        $("form").submit();

    });
});
