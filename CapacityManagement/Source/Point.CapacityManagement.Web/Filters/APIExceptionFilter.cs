﻿using Point.CapacityManagement.Business.Helpers;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Web.Http.Filters;

namespace Point.CapacityManagement.Web.Filters
{
    public class APIExceptionFilter : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            HttpStatusCode status = HttpStatusCode.InternalServerError;
            var message = string.Empty;
            var exceptionType = actionExecutedContext.Exception.GetType();
            if (exceptionType == typeof(UnauthorizedAccessException))
            {
                message = "Access to the Web API is not authorized.";
                status = HttpStatusCode.Unauthorized;
            }
            else
            {
                var httpRequestMessage = actionExecutedContext.Request;
                var url = httpRequestMessage.RequestUri?.AbsoluteUri;
                var queryString = httpRequestMessage.RequestUri?.Query;

                var errornr = ExceptionHelper.SendMessageException(actionExecutedContext.Exception, url, queryString);

                message = "ErrorNumber:" + errornr + "\r\n" + actionExecutedContext.Exception.Message;
            }

            actionExecutedContext.Response = new HttpResponseMessage()
            {
                Content = new StringContent(message, System.Text.Encoding.UTF8, "text/plain"),
                StatusCode = status
            };

            base.OnException(actionExecutedContext);
        }
    }
}