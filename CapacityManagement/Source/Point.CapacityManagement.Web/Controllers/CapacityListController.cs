﻿using System.Threading.Tasks;
using System.Web.Mvc;
using Point.CapacityManagement.Web.Controllers.Shared;
using Point.CapacityManagement.Web.ViewModels;
using Point.CapacityManagement.Business.Logic;
using Point.CapacityManagement.Business.Models;
using System.Collections.Generic;

namespace Point.CapacityManagement.Web.Controllers
{
    public class CapacityListController : CapacityDisplayController
    {
        public async Task<ActionResult> Index(
            
            int pageIndex = 1,
            List<int> regionIds = null,           
            string nameSearch1 = null,
            string nameSearch2 = null,
            string afterCareGroupIdAndTypeId = IdForEmptyListItemString,
            int afterCareGroupID = IdForEmptyListItem,
            int afterCareTypeID = IdForEmptyListItem,
            string citySubstring = null,
            string postalCodeSubstring = null,
            int postalCodeRadius = IdForEmptyListItem,
            bool withCapacityOnly = true,
            string regionsFromTile = null)
        {
            // List<int> regionIds = null;
            if (regionIds == null)
            {
                if (regionIds == null) regionIds = new List<int>();

                if (!string.IsNullOrEmpty(regionsFromTile))
                {
                    var ids = regionsFromTile.Split('&');
                    foreach (string s in ids)
                    {

                        int outnum;
                        if (System.Int32.TryParse(s, out outnum))
                        {
                            regionIds.Add(outnum);
                        }
                    }
                    regionsFromTile = null;
                }
            }


            if (afterCareGroupIdAndTypeId != "")
            {
                Mapper.ParseAfterCareGroupIdAndTypeId(afterCareGroupIdAndTypeId, IdForEmptyListItem, IdForEmptyListItemString, out afterCareGroupID, out afterCareTypeID);
            }

            // NOTE: The mapper shall receive nullables for id's (and not the values filled/shown in the views)
            var page = await Mapper.GetCapacityListPage(
                new CapacitySearchParameters(
                    pageIndex,
                    PagingViewModel.PageSize,
                   // regionId.NullIfEmptyListItem(),
                    regionIds,
                    nameSearch1,
                    nameSearch2,
                    afterCareGroupID.NullIfEmptyListItem(),
                    afterCareTypeID.NullIfEmptyListItem(),
                    citySubstring,
                    postalCodeSubstring,
                    postalCodeRadius.NullIfEmptyListItem(),
                    withCapacityOnly,
                    regionsFromTile));

            var capacityList = page.List;

            if (afterCareTypeID != IdForEmptyListItem)
            {
                var result = await Mapper.GetAfterCareGroupIDForAfterCareType(afterCareTypeID);

                if (result == null)
                {
                    LoggingBL.Error($"No AfterCareGroup found for AfterCareType {afterCareTypeID}");
                }

                afterCareGroupID = result ?? IdForEmptyListItem;
            }

            var groupsAndTypes = await Mapper.GetAfterCareGroupsAndTypes(regionIds);
            var postalcoderadius = Mapper.GetPostalCodeRadius();

            var model = new CapacityListViewModel(Mapper, regionIds, IdForEmptyListItem)
            {
                // Inputs:
                Paging = new PagingViewModel(pageIndex, page.TotalCount),
                NameSearch1 = nameSearch1,
                NameSearch2 = nameSearch2,
                AfterCareGroupIdAndTypeId = $"{afterCareGroupID};{afterCareTypeID}",
                AfterCareGroupID = afterCareGroupID,
                AfterCareTypeID = afterCareTypeID,
                CitySubstring = citySubstring,
                WithCapacityOnly = withCapacityOnly,              
                // Dropdownlists:
                AfterCareGroupAndTypeList = SelectListHelper.AddEmptyItem(groupsAndTypes, IdForEmptyListItemString),
                PostalCodeRadiusList = postalcoderadius,
                PostalCodeSubstring = postalCodeSubstring,
                PostalCodeRadius = postalCodeRadius,
                RegionsFromTile = regionsFromTile,
                // Capacity data:
                CapacityList = capacityList
            };

            
            return View("CapacityList", model);
        }
    }
}
