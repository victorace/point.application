﻿using Point.CapacityManagement.Web.ViewModels;
using System.Web.Mvc;

namespace Point.CapacityManagement.Web.Controllers
{
    public class ErrorController : BaseController
    {
        public ActionResult ErrorPage(ErrorViewModel model)
        {
            return View(model);
        }
    }
}