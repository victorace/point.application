﻿using Point.CapacityManagement.Business.Helpers;
using Point.CapacityManagement.Business.Logic;
using Point.CapacityManagement.Database.Context;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace Point.CapacityManagement.Web.Controllers
{
    public class BaseApiController : ApiController
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            logReadAction(controllerContext);

            base.Initialize(controllerContext);
        }

        private void logReadAction(HttpControllerContext controllerContext)
        {
            var logReadActions = ConfigHelper.GetAppSettingByName<bool>("LogReadActions");
            if (controllerContext != null && logReadActions)
            {
                if (controllerContext.Request != null)
                {
                    using (var context = new CapacityManagementContext())
                    {
                        var logRead = new LogReadBL(context);
                        var url = controllerContext.Request.RequestUri.AbsoluteUri;
                        var queryString = controllerContext.Request.RequestUri.Query;

                        logRead.Insert(url, null, queryString, null, null, null, null);
                    }
                }
            }
        }
    }
}