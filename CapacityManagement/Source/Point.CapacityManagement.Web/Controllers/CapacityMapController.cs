﻿using Point.CapacityManagement.Business.Models;
using Point.CapacityManagement.Database.Context;
using Point.CapacityManagement.Web.Controllers.Shared;
using Point.CapacityManagement.Web.ViewModels;
using Point.CapacityManagement.Database.Models.Enums;
using System.Threading.Tasks;
using System.Web.Mvc;
using Point.CapacityManagement.Business.Extensions;
using System.Collections.Generic;

namespace Point.CapacityManagement.Web.Controllers
{
    public class CapacityMapController : CapacityDisplayController
    {
        public async Task<ActionResult> Index(List<int> regionIds = null , string afterCareGroupIdAndTypeId = IdForEmptyListItemString, int afterCareGroupID = IdForEmptyListItem, int afterCareTypeID = IdForEmptyListItem)
        {
            int[] aftercarecategoryids = new[] { AfterCareCategoryID.Zorginstelling.To<int>(), AfterCareCategoryID.Hospice.To<int>() };
            var aftercaretypeids = await Mapper.GetAfterCareTypeIDsByCategoryIDs(aftercarecategoryids);

            if (regionIds == null) { regionIds = new List<int>(); }
            var groupsAndTypes = await Mapper.GetAfterCareGroupsAndTypes(regionIds, aftercaretypeids);

            if (afterCareGroupIdAndTypeId != IdForEmptyListItemString)
            {
                Mapper.ParseAfterCareGroupIdAndTypeId(afterCareGroupIdAndTypeId, IdForEmptyListItem, IdForEmptyListItemString, out afterCareGroupID, out afterCareTypeID);
            }

            if (afterCareTypeID != IdForEmptyListItem)
            {
                var result = await Mapper.GetAfterCareGroupIDForAfterCareType(afterCareTypeID);

                afterCareGroupID = result ?? IdForEmptyListItem;
            }

            var context = new CapacityManagementContext();

            var searchparameters = new CapacitySearchParameters()
            {
                RegionIds = regionIds,
                AfterCareGroupID = afterCareGroupID.NullIfEmptyListItem(),
                AfterCareTypeID = afterCareTypeID.NullIfEmptyListItem(),
                AfterCareTypeIDs = aftercaretypeids
            };

            var model = new CapacityMapViewModel(Mapper, regionIds , IdForEmptyListItem)
            {
                AfterCareGroupAndTypeList = SelectListHelper.AddEmptyItem(groupsAndTypes, IdForEmptyListItemString),
                AfterCareGroupIdAndTypeId = $"{afterCareGroupID};{afterCareTypeID}",
                AfterCareCategoryIDs = string.Join(",", aftercarecategoryids),
                DepartmentCapacityGPSList = await Mapper.GetMarkers(searchparameters),
                RegionIds = regionIds,
                ViewType = CapacityViewType.Map
            };

            context.Dispose();

            return View("CapacityMap", model);
        }
    }
}
