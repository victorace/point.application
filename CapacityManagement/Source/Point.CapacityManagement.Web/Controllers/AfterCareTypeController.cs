﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Point.CapacityManagement.Web.Controllers
{
    public class AfterCareTypeController : CapacityDisplayController
    {
        public async Task<ActionResult> Descriptions(int id) // aftercaretilegroupid
        {
            var model = await Mapper.GetAfterCareTypeDescriptions(id);

            return View("AfterCareTypeDescriptionsBody", model);
        }

        public async Task<ActionResult> DescriptionsByCategory(int[] ids) 
        {
            var model = await Mapper.GetAfterCareTypeDescriptionsByCategoryIDs(ids); //aftercarecategoryid

            return View("AfterCareTypeDescriptionsBody", model);
        }


        public ActionResult Modal()
        {
            return View("AfterCareTypeDescriptionsModal");
        }
    }
}