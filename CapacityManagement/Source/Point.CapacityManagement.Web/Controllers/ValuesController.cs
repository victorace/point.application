﻿using Point.CapacityManagement.Business.Logic;
using Point.CapacityManagement.Business.Models;
using Point.CapacityManagement.Web.Attributes;
using Point.CapacityManagement.Web.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Point.CapacityManagement.Web.Controllers
{
    [APIExceptionFilter]
    public class ValuesController : BaseApiController
    {
        [ValidateAntiFiddleInjection("DigestValue")]
        public HttpResponseMessage UpdateCapacity([FromBody] DepartmentCapacityUpdateList value)
        {
            var errorList = new List<string>();

            foreach (var capacity in value.Capacities)
            {
                try
                {
                    DepartmentCapacitySyncBL.Upsert(capacity);

                }
                catch (Exception ex)
                {
                    errorList.Add($"DepartmentID: {capacity.DepartmentID}, " +
                                  $"ErrorMessage: {ex.Message} {ex.InnerException?.Message} ");
                }
            }

            if (errorList.Any())
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent(string.Join(",", errorList))
                };
            }

            return new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent("Succeeded")
            };
        }

        public HttpResponseMessage UpdateRegion([FromBody] RegionCapacityUpdate value)
        {
            try
            {
                DepartmentCapacitySyncBL.UpdateRegionFromPoint(value);
            }
            catch (Exception ex)
            {

                return new HttpResponseMessage(HttpStatusCode.NotAcceptable)
                {
                    Content = new StringContent($"ErrorMessage: {ex.Message} {ex.InnerException?.Message}")
                };
            }
            return new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent("Succeeded")
            };
        }

        [ValidateAntiFiddleInjection("OrganizationID")]
        public HttpResponseMessage UpdateOrganization([FromBody] OrganizationCapacityUpdate value)
        {
            try
            {
                DepartmentCapacitySyncBL.UpdateOrganizationFromPoint(value);
            }
            catch (Exception ex)
            {

                return new HttpResponseMessage(HttpStatusCode.NotAcceptable)
                {
                    Content = new StringContent($"ErrorMessage: {ex.Message} {ex.InnerException?.Message}")
                };
            }
            return new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent("Succeeded")
            };            
        }

        [ValidateAntiFiddleInjection("LocationID")]
        public HttpResponseMessage UpdateLocation([FromBody] LocationCapacityUpdate value)
        {
            try
            {
                DepartmentCapacitySyncBL.UpdateLocationFromPoint(value);
            }
            catch (Exception ex)
            {

                return new HttpResponseMessage(HttpStatusCode.NotAcceptable)
                {
                    Content = new StringContent($"ErrorMessage: {ex.Message} {ex.InnerException?.Message}")
                };
            }
            return new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent("Succeeded")
            };
        }

        [ValidateAntiFiddleInjection("DepartmentID")]
        public HttpResponseMessage UpdateDepartment([FromBody] DepartmentCapacityUpdate value)
        {
            try
            {
                DepartmentCapacitySyncBL.Upsert(value);
            }
            catch (Exception ex)
            {

                return new HttpResponseMessage(HttpStatusCode.NotAcceptable)
                {
                    Content = new StringContent($"ErrorMessage: {ex.Message} {ex.InnerException?.Message}")
                };
            }
            return new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent("Succeeded")
            };
        }
    }
}
