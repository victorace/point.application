﻿using System.Web.Mvc;
using Point.CapacityManagement.Database.Context;
using Point.CapacityManagement.Web.Controllers.Shared;
using Point.CapacityManagement.Business.Helpers;

namespace Point.CapacityManagement.Web.Controllers
{
    /// <summary>
    /// Controllers for displaying the capacity data (as opposed to updating it).
    /// </summary>
    public class CapacityDisplayController : BaseController
    {
        public const int IdForEmptyListItem = 0;
        public const string IdForEmptyListItemString = "";

        protected CapacityViewModelMapper Mapper;

        // Note: Do not dispose of the context
        // http://stephenwalther.com/archive/2008/08/20/asp-net-mvc-tip-34-dispose-of-your-datacontext-or-don-t
        public CapacityDisplayController()
        {
            var context = new CapacityManagementContext();
            Mapper = new CapacityViewModelMapper(context);
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            RefererHelper.SetReferer(filterContext.HttpContext.Request.Params);

            base.OnActionExecuting(filterContext);
        }

    }

    public static class Extensions
    {
        public static int? NullIfEmptyListItem(this int id)
        {
            return id == CapacityDisplayController.IdForEmptyListItem ? (int?)null : id;
        }
    }
}
