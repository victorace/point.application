﻿using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Point.CapacityManagement.Web.Controllers.Shared
{
    public class RequireHttpsHandler : DelegatingHandler
    {
        private readonly int _httpsPort;

        public RequireHttpsHandler() : this(443) { }

        public RequireHttpsHandler(int httpsPort)
        {
            _httpsPort = httpsPort;
        }

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            if (request.RequestUri.Scheme == Uri.UriSchemeHttps)
            {
                return base.SendAsync(request, cancellationToken);
            }

            var response = CreateResponse(request);
            var taskcompletionsource = new TaskCompletionSource<HttpResponseMessage>();
            taskcompletionsource.SetResult(response);
            return taskcompletionsource.Task;
        }

        private HttpResponseMessage CreateResponse(HttpRequestMessage request)
        {
            HttpResponseMessage response;
            var uri = new UriBuilder(request.RequestUri)
            {
                Scheme = Uri.UriSchemeHttps,
                Port = _httpsPort
            };
            const string body = "HTTPS is required";
            if (request.Method.Equals(HttpMethod.Get) || request.Method.Equals(HttpMethod.Head))
            {
                response = request.CreateResponse(HttpStatusCode.Found);
                response.Headers.Location = uri.Uri;
                if (request.Method.Equals(HttpMethod.Get))
                {
                    response.Content = new StringContent(body, Encoding.UTF8, "text/html");
                }
            }
            else
            {
                response = request.CreateResponse(HttpStatusCode.NotFound);
                response.Content = new StringContent(body, Encoding.UTF8, "text/html");
            }

            return response;
        }
    }

}

