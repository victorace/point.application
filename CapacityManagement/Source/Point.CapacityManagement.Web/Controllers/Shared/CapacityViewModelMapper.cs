﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Point.CapacityManagement.Web.ViewModels;
using Point.CapacityManagement.Business.Logic;
using Point.CapacityManagement.Business.Models;
using Point.CapacityManagement.Database.Models;
using Point.CapacityManagement.Database.Context;
using Point.CapacityManagement.Database.Models.Constants;
using Point.CapacityManagement.Database.Models.Enums;


namespace Point.CapacityManagement.Web.Controllers.Shared
{
    public class CapacityViewModelMapper
    {
        // HIERARCHY: AfterCareTileGroup > AfterCareGroup > AfterCareType
        private readonly AfterCareTileGroupBL _afterCareTileGroupBL;

        private readonly AfterCareGroupBL _afterCareGroupBL;
        private readonly AfterCareTypeBL _afterCareTypeBL;
        private readonly DepartmentCapacityViewBL _departmentCapacityViewBL;
        private readonly RegionBL _regionBL;
        private readonly RegionAfterCareTypeBL _regionAfterCareTypeBL;

        public class Page
        {
            public int TotalCount { get; set; }
            public List<CapacityViewModel> List { get; set; }
        }

        public CapacityViewModelMapper(CapacityManagementContext context)
        {
            _afterCareTileGroupBL = new AfterCareTileGroupBL(context);
            _afterCareGroupBL = new AfterCareGroupBL(context);
            _afterCareTypeBL = new AfterCareTypeBL(context);
            _departmentCapacityViewBL = new DepartmentCapacityViewBL(context);
            _regionBL = new RegionBL(context);
            _regionAfterCareTypeBL = new RegionAfterCareTypeBL(context);
        }

        // NOTE: The mapper shall receive nullables for id's (and not the values filled/shown in the views)
        public async Task<Page> GetCapacityListPage(CapacitySearchParameters parameters)
        {
            var capacityList = GetCapacityUnpaged(parameters);

            var totalCount = capacityList.Count();

            var orderedList = capacityList
                .OrderBy(x => x.OrganizationName)
                .ThenBy(x => x.LocationName)
                .ThenBy(x => x.DepartmentName)
                .ThenBy(x => x.LocationCity);

            var page = Pager<CapacityViewModel>.GetPage(parameters.PageIndex, parameters.PageSize, orderedList);

            var actualData = await page.ToListAsync();

            return new Page
            {
                TotalCount = totalCount,
                List = actualData
            };
        }

        private IQueryable<CapacityViewModel> GetCapacityUnpaged(CapacitySearchParameters capacitySearchParameters)
        {
            return _departmentCapacityViewBL
                .GetCapacityList(capacitySearchParameters)
                .Select(capacity => new CapacityViewModel
                {
                    AfterCareGroupID = capacity.AfterCareGroupID ?? 0, // For dropdownlist, not for the results table

                    OrganizationName = capacity.OrganizationName,
                    LocationName = capacity.LocationName,
                    DepartmentName = capacity.DepartmentName,
                    LocationCity = capacity.LocationCity,
                    Capacity1 = capacity.Capacity1,
                    Capacity2 = capacity.Capacity2,
                    Capacity3 = capacity.Capacity3,
                    Capacity4 = capacity.Capacity4,
                    Information = capacity.Information,
                    ModifiedOn = capacity.ModifiedOn,

                    DepartmentPhoneNumber = capacity.DepartmentPhoneNumber,
                    DepartmentFaxNumber = capacity.DepartmentFaxNumber,
                    DepartmentEmailAddress = capacity.DepartmentEmailAddress,

                    LocationStreet = capacity.LocationStreet,
                    LocationNumber = capacity.LocationNumber,
                    LocationPostalCode = capacity.LocationPostalCode,
                    LocationEmailAddress = capacity.LocationEmailAddress
                });
        }

        public List<IdAndName> GetRegionList()
        {
            return _regionBL.GetAll().ToList();
        }

        public async Task<List<AfterCareFinancingViewModel>> GetAfterCareFinancingViewModel(List<int> regionIds)
        {
            var tileGroups = await _afterCareTileGroupBL.GetAll();

            var list = new List<AfterCareFinancingViewModel>();
            foreach (var tileGroup in tileGroups)
            {
                var afterCareGroups = await GetAfterCareGroups(regionIds, tileGroup.AfterCareTileGroupID, PageType.Tile);
                var parentActivation = afterCareGroups.Any(group => group.ActiveClass == Constants.ActiveInRegion);



                list.Add(new AfterCareFinancingViewModel
                {
                    GroupID = tileGroup.AfterCareTileGroupID,
                    Name = tileGroup.Name,
                    CssClass = tileGroup.CssClass,
                    SortOrder = tileGroup.SortOrder,
                    AfterCareGroups = afterCareGroups,
                    ActiveClass = parentActivation ? Constants.ActiveInRegion : Constants.DeActiveInRegion
                });
            }

            

            return list;
        }

        public async Task<List<AfterCareType>> GetAfterCareTypeDescriptions(int afterCareTileGroupID)
        {
            var list = _afterCareGroupBL.GetAll().Where(acg => afterCareTileGroupID == -1 || acg.AfterCareTileGroupID == afterCareTileGroupID).Select(acg => acg.AfterCareGroupID).ToList();
            return await _afterCareTypeBL.GetByAfterCareGroupIDs(list).ToListAsync();
        }

        public async Task<List<AfterCareType>> GetAfterCareTypeDescriptionsByCategoryIDs(int[] categoryIDs)
        {
            return await _afterCareTypeBL.GetByAfterCareCategoryIDs(categoryIDs).ToListAsync();
        }

        public async Task<List<int>> GetAfterCareTypeIDsByCategoryIDs(int[] categoryIDs)
        {
            return await _afterCareTypeBL.GetByAfterCareCategoryIDs(categoryIDs).Select(act => act.AfterCareTypeID).ToListAsync();
        }

        public void ParseAfterCareGroupIdAndTypeId(
            string afterCareGroupIdAndTypeId,
            int idForEmptyListItem,
            string idForEmptyListItemString,
            out int afterCareGroupID,
            out int afterCareTypeID)
        {

            afterCareGroupID = idForEmptyListItem;
            afterCareTypeID = idForEmptyListItem;

            var regex = new Regex(@"\d+;\d+", RegexOptions.Compiled);

            if (afterCareGroupIdAndTypeId != idForEmptyListItemString
                && regex.IsMatch(afterCareGroupIdAndTypeId))
            {
                var split = afterCareGroupIdAndTypeId.Split(';');
                if (split.Length == 2)
                {
                    int.TryParse(split[0], out afterCareGroupID);
                    int.TryParse(split[1], out afterCareTypeID);
                }
            }
        }

        public async Task<List<ValueAndName>> GetAfterCareGroupsAndTypes(List<int> regionIds, IEnumerable<int> aftercareTypeIDs = null)
        {
            var midconnector = "├─";
            var endconnector = "└─";

            var groups =  await GetAfterCareGroups(regionIds);
            
            if (aftercareTypeIDs != null)
            {
                groups = groups.Where(acg => acg.AfterCareTypes.Any(act => aftercareTypeIDs.Contains(act.AfterCareTypeID))).ToList();
            }


            var definedTypesInRegions = _regionAfterCareTypeBL.GetAll();

            var list = new List<ValueAndName>();
            foreach (var group in groups.OrderBy(gr => gr.AfterCareGroupName))
            {
                list.Add(new ValueAndName(group.AfterCareGroupName, $"{group.AfterCareGroupID};0"));

                if (group.AfterCareTypes.Count > 1)
                {
                    if (regionIds == null || regionIds.Contains(0))
                    {
                        list.AddRange(group.AfterCareTypes.Where(act => definedTypesInRegions.Any(reg => reg.AfterCareTypeID == act.AfterCareTypeID)).OrderBy(act => act.AfterCareTypeName)
                      .Select(type => new ValueAndName($"{midconnector} {type.AfterCareTypeName}", $"{group.AfterCareGroupID};{type.AfterCareTypeID}")));                        
                    }
                    else
                    {
                        list.AddRange(group.AfterCareTypes.Where(act => act.ActiveClass == Constants.ActiveInRegion).OrderBy(act => act.AfterCareTypeName)
                          .Select(type => new ValueAndName($"{midconnector} {type.AfterCareTypeName}", $"{group.AfterCareGroupID};{type.AfterCareTypeID}")));
                    }
                    list.Last().Value = list.Last().Value.Replace(midconnector, endconnector);
                }
            }

            return list;
        }

        private async Task<List<AfterCareGroupViewModel>> GetAfterCareGroups(List<int> regionIds, int? afterCareTileGroupID = null, PageType? pageType = PageType.Others)
        {
            var groups = await _afterCareGroupBL.GetAll(afterCareTileGroupID).ToListAsync();

            var list = new List<AfterCareGroupViewModel>();
            foreach (var group in groups)
            {
                var afterCareGroupViewModel = await GetAfterCareGroupViewModel(regionIds, group);
                list.Add(afterCareGroupViewModel);
            }

            if (pageType == PageType.Others)
            {
                //if (regionIds == null || regionIds.Contains(0))
                //{
                //    list = list.Where(lst => _regionAfterCareTypeBL.GetAll().Where(rg => rg.AfterCareType.AfterCareGroupID == lst.AfterCareGroupID).Any()).ToList();
                //}
                //else
                //{
                    list = list.Where(lst => lst.ActiveClass == Constants.ActiveInRegion).ToList();
                //}

            }

            return list;
        }
      
        private async Task<AfterCareGroupViewModel> GetAfterCareGroupViewModel(List<int> regionIds, AfterCareGroup group)
        {
            var afterCareTypesWithCapacity = await _departmentCapacityViewBL
                .GetAfterCareTypesByAfterCareGroupID(regionIds, group.AfterCareGroupID)
                .Select(x => new AfterCareTypeViewModel
                {
                    AfterCareTypeID = x.AfterCareTypeID,
                    AfterCareTypeName = x.AfterCareTypeName,
                    AfterCareTypeCapacity = x.AfterCareTypeCapacity,
                    ActiveClass = Constants.ActiveInRegion
                })
                .ToListAsync();

            var definedTypesInRegions = _regionAfterCareTypeBL.GetAll()?.Where(x => x.AfterCareType.AfterCareGroupID == group.AfterCareGroupID );


            var afterCareTypes = await _afterCareTypeBL
                .GetByAfterCareGroupID(group.AfterCareGroupID)
                .ToListAsync();

            

            foreach (var afterCareType in afterCareTypes)
            {                
                if (!afterCareTypesWithCapacity.Any(x => x.AfterCareTypeID == afterCareType.AfterCareTypeID))
                {
                    var emptyAfterCareType = new AfterCareTypeViewModel
                    {
                        AfterCareTypeID = afterCareType.AfterCareTypeID,
                        AfterCareTypeName = afterCareType.AfterCareTypeName,
                        AfterCareTypeCapacity = 0
                    };
                    //if (definedTypesInRegions.Any(x => (regionIds == null || regionIds.Contains(0) || regionIds.Contains(x.RegionID) ) && x.AfterCareTypeID == afterCareType.AfterCareTypeID))
                    //{
                    //    emptyAfterCareType.ActiveClass = Constants.ActiveInRegion;                        
                    //}
                    if (definedTypesInRegions.Any(x => (regionIds.Contains(x.RegionID)) && x.AfterCareTypeID == afterCareType.AfterCareTypeID))
                    {
                        emptyAfterCareType.ActiveClass = Constants.ActiveInRegion;
                    }
                    else
                    {
                        emptyAfterCareType.ActiveClass = Constants.DeActiveInRegion;                        
                    }
                    afterCareTypesWithCapacity.Add(emptyAfterCareType);
                }
            }
           
            var parentActivation = afterCareTypesWithCapacity.Any(type => type.ActiveClass == Constants.ActiveInRegion);

            return new AfterCareGroupViewModel
            {
                AfterCareGroupName = group.Name,
                SortOrder = group.SortOrder,
                AfterCareGroupID = group.AfterCareGroupID,
                Capacity = await GetSum(
                    _departmentCapacityViewBL.GetTotalCapacityByAfterCareGroupID(regionIds, group.AfterCareGroupID)),
                AfterCareTypes = afterCareTypesWithCapacity,
                //AfterCareGroup is active if there is minimum 1 active afterCareType in this region, So =>                
                ActiveClass = parentActivation ? Constants.ActiveInRegion : Constants.DeActiveInRegion
            };
        }

        public async Task<int?> GetAfterCareGroupIDForAfterCareType(int afterCareTypeID)
        {
            var type = await _afterCareTypeBL.GetAll().FirstOrDefaultAsync(x => x.AfterCareTypeID == afterCareTypeID);

            return type?.AfterCareGroupID;
        }

        private static async Task<int> GetSum(IQueryable<int> sums)
        {
            return await sums.AnyAsync() ? sums.Sum() : 0;
        }

        public List<IdAndName> GetPostalCodeRadius()
        {
            return new[] { 1, 3, 5, 10, 15, 20 }
                .Select(radius => new IdAndName
                {
                    Name = $"< {radius} km",
                    Id = radius
                }).ToList();
        }

        public async Task<List<DepartmentCapacityGPSViewModel>> GetMarkers(CapacitySearchParameters capacitySearchParameters)
        {
            var departmentcapacitygpslist = await _departmentCapacityViewBL.GetMarkers(capacitySearchParameters).ToListAsync();
            var departmentcapacitygpsvms = new List<DepartmentCapacityGPSViewModel>();

            foreach (var grouped in departmentcapacitygpslist.GroupBy(dcgps => dcgps.DepartmentCapacity.LocationPostalCode))
            {
                var departmentcapacitygpsvm = new DepartmentCapacityGPSViewModel();
                foreach (var departmentcapacitygps in departmentcapacitygpslist
                    .Where(dcgps => dcgps.DepartmentCapacity.LocationPostalCode == grouped.Key)
                    .OrderBy(dcgps => dcgps.DepartmentCapacity.DepartmentName))
                {
                    if (!departmentcapacitygpsvm.DepartmentNames.Any())
                    {
                        departmentcapacitygpsvm.Latitude = departmentcapacitygps.GPSCoordinaat.Latitude;
                        departmentcapacitygpsvm.Longitude = departmentcapacitygps.GPSCoordinaat.Longitude;
                        departmentcapacitygpsvm.LocationName = departmentcapacitygps.DepartmentCapacity.LocationName;
                        departmentcapacitygpsvm.OrganizationName =
                            departmentcapacitygps.DepartmentCapacity.OrganizationName;

                    }
                    departmentcapacitygpsvm.DepartmentCapacities.Add(departmentcapacitygps.DepartmentCapacity
                        .Capacity1);
                    departmentcapacitygpsvm.DepartmentNames.Add(departmentcapacitygps.DepartmentCapacity
                        .DepartmentName);
                }
                departmentcapacitygpsvm.TotalCapacity = departmentcapacitygpsvm.DepartmentCapacities.Sum(dc => dc);
                departmentcapacitygpsvms.Add(departmentcapacitygpsvm);
            }

            return departmentcapacitygpsvms;
        }

    }
}
