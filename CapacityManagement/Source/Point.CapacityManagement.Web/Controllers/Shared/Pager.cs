﻿using System.Linq;

namespace Point.CapacityManagement.Web.Controllers.Shared
{
    public static class Pager<T>
    {
        public static IQueryable<T> GetPage(int pageIndex, int pageSize, IQueryable<T> input)
        {
            if (pageIndex < 1)
            {
                pageIndex = 1;
            }

            if (pageSize < 10)
            {
                pageSize = 10;
            }

            return input.Skip((pageIndex - 1) * pageSize).Take(pageSize);
        }
    }
}
