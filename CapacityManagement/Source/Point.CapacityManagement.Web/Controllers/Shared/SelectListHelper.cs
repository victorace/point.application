﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Point.CapacityManagement.Business.Models;

namespace Point.CapacityManagement.Web.Controllers.Shared
{
    public static class SelectListHelper
    {
        /// <summary>
        /// Returns a select list.
        /// Prepends an extra "Empty" item if an id is given for that.
        /// </summary>
        public static List<SelectListItem> GetSelectList(
            IEnumerable<IdAndName> list,
            int selectedId,
            int? idForEmpty = null,
            string valueForEmpty = "")
        {
            var selectList = list.Select(x => new SelectListItem
            {
                Text = x.Name,
                Value = x.Id.ToString(),
                Selected = x.Id == selectedId
            }).ToList();

            if (idForEmpty != null)
            {
                selectList.Insert(0, new SelectListItem
                {
                    Text = valueForEmpty,
                    Value = idForEmpty.Value.ToString(),
                    Selected = idForEmpty == selectedId
                });
            }

            return selectList;
        }

        /// <summary>
        /// Returns the input list, after prepending an extra "Empty" item.
        /// </summary>
        public static List<IdAndName> AddEmptyItem(
            IEnumerable<IdAndName> list,
            int idForEmpty,
            string valueForEmpty = "")
        {
            var selectList = list.Select(x => new IdAndName
            {
                Name = x.Name,
                Id = x.Id
            }).ToList();


            return selectList;
        }

        /// <summary>
        /// Returns the input list, after prepending an extra "Empty" item.
        /// </summary>
        public static List<ValueAndName> AddEmptyItem(IEnumerable<ValueAndName> list, string idForEmptyListItemString)
        {
            var selectList = list.Select(x => new ValueAndName(x.Name, x.Value)).ToList();

            selectList.Insert(0, new ValueAndName("", idForEmptyListItemString));

            return selectList;
        }
    }
}
