﻿using Point.CapacityManagement.Web.ViewModels;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Collections.Generic;

namespace Point.CapacityManagement.Web.Controllers
{
    public class CapacityTilesController : CapacityDisplayController
    {
        public async Task<ActionResult> Index(List<int> regionIds)
        {
            if (regionIds == null)
            {
                regionIds = new List<int>();
            }
            var model = new CapacityTilesViewModel(Mapper, regionIds, IdForEmptyListItem)
            {
                FinancingList = await Mapper.GetAfterCareFinancingViewModel(regionIds)
            };

            return View("CapacityTiles", model);
        }
    }
}
