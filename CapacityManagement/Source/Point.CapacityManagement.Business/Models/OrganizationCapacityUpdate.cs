﻿using System;
namespace Point.CapacityManagement.Business.Models
{
    public class OrganizationCapacityUpdate
    {
        public int OrganizationID { get; set; }
        public string OrganizationName { get; set; }        
        public int RegionID { get; set; }
        public string RegionName { get; set; }
        public bool IsActive { get; set; }
        public DateTime? ModifiedOn { get; set; }
    }
}
