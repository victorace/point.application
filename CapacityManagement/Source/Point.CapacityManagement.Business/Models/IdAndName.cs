﻿namespace Point.CapacityManagement.Business.Models
{
    public class IdAndName
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
