﻿using System;
using System.Collections.Generic;

namespace Point.CapacityManagement.Business.Models
{
    public class LocationCapacityUpdate
    {
        public int LocationID { get; set; }
        public string LocationName { get; set; }
        public string LocationStreet { get; set; }
        public string LocationNumber { get; set; }
        public string LocationPostalCode { get; set; }
        public string LocationCity { get; set; }
        public string LocationEmailAddress { get; set; }
        public bool IsActive { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public List<int> AlternativeRegionIDs { get; set; } = new List<int>();
    }
}
