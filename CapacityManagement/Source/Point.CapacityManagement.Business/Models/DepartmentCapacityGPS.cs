﻿using Point.CapacityManagement.Database.Models;

namespace Point.CapacityManagement.Business.Models
{
    public class DepartmentCapacityGPS
    {
        public DepartmentCapacity  DepartmentCapacity { get; set; }
        public GPSCoordinaat GPSCoordinaat { get; set; }
    }
}
