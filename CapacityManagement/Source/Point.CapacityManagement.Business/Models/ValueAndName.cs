﻿namespace Point.CapacityManagement.Business.Models
{
    public class ValueAndName
    {
        public string Value { get; set; }
        public string Name { get; set; }

        public ValueAndName(string value, string name)
        {
            Value = value;
            Name = name;
        }
    }
}
