﻿namespace Point.CapacityManagement.Business.Models
{
    public class RegionCapacityUpdate
    {
        public int RegionID { get; set; }
        public string Name { get; set; }
        public bool CapacityBeds { get; set; }
    }
}
