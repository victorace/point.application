﻿namespace Point.CapacityManagement.Business.Models
{
    public class AfterCareTypeWithCapacity
    {
        public int AfterCareTypeID { get; set; }
        public string AfterCareTypeName { get; set; }
        public int AfterCareGroupID { get; set; }
        public int AfterCareTypeCapacity { get; set; }
    }
}
