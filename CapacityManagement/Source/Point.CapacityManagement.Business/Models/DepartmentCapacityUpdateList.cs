﻿using System.Collections.Generic;

namespace Point.CapacityManagement.Business.Models
{
    public class DepartmentCapacityUpdateList
    {
        public int DigestValue { get; set; }
        public List<DepartmentCapacityUpdate> Capacities { get; set; }
    }
}
