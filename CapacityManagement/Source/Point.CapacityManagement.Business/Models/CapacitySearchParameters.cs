﻿using System.Collections.Generic;

namespace Point.CapacityManagement.Business.Models
{
    // NOTE: The mapper shall receive nullables for id's (and not the values filled/shown in the views)
    public class CapacitySearchParameters
    {
        // Paging
        public int PageIndex { get; set; }
        public int PageSize { get; set; }

        // Actual search
        public List<int> RegionIds { get; set; }
       // public int? RegionId { get; set; }
        public string NameSearch1 { get; set; }
        public string NameSearch2 { get; set; }
        public int? AfterCareGroupID { get; set; }
        public int? AfterCareTypeID { get; set; }
        public IEnumerable<int> AfterCareTypeIDs { get; set; }
        public string CitySubstring { get; set; }
        public string PostalCodeSubstring { get; set; }
        public int? PostalCodeRadius { get; set; }
        public bool WithCapacityOnly { get; set; }

        public string RegionsFromTile { get; set; }
        public CapacitySearchParameters() { }

        public CapacitySearchParameters(
            int pageIndex, int pageSize, List<int> regionIds, string nameSearch1, string nameSearch2,
            int? afterCareGroupID, int? afterCareTypeID, string citySubstring,
            string postalCodeSubstring, int? postalCodeRadius, bool withCapacityOnly, string regionsFromTile )
        {
            PageIndex = pageIndex;
            PageSize = pageSize;
            RegionIds = regionIds;
            NameSearch1 = nameSearch1;
            NameSearch2 = nameSearch2;
            AfterCareGroupID = afterCareGroupID;
            AfterCareTypeID = afterCareTypeID;
            CitySubstring = citySubstring;
            PostalCodeSubstring = postalCodeSubstring;
            PostalCodeRadius = postalCodeRadius;
            WithCapacityOnly = withCapacityOnly;
            RegionsFromTile = regionsFromTile;
        }
    }
}
