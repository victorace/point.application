﻿using System;

namespace Point.CapacityManagement.Business.Extensions
{
    public static class TypeExtensions
    {
        //http://stackoverflow.com/questions/9772686/how-to-write-a-generic-conversion-method-which-would-support-converting-to-and-f
        public static T To<T>(this object value)
        {
            var t = typeof(T);

            // Not nullable.
            if (!t.IsGenericType || t.GetGenericTypeDefinition() != typeof(Nullable<>))
            {
                return value == null ? default(T) : (T) Convert.ChangeType(value, typeof(T));
            }

            // Nullable type.
            if (value == null)
            {
                // you may want to do something different here.
                return default(T);
            }

            // Get the type that was made nullable.
            var valueType = t.GetGenericArguments()[0];

            // Convert to the value type.
            var result = Convert.ChangeType(value, valueType);

            // Cast the value type to the nullable type.
            return (T) result;
        }
    }
}
