﻿using System;
using System.Configuration;

namespace Point.CapacityManagement.Business.Helpers
{
    public static class ConfigHelper
    {
        public static T GetAppSettingByName<T>(string name, T defaultvalue = default(T))
        {
            var stringValue = ConfigurationManager.AppSettings.Get(name);
            if (String.IsNullOrEmpty(stringValue))
            {
                return defaultvalue;
            }

            return (T)Convert.ChangeType(stringValue, typeof(T));
        }
    }
}
