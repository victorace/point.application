﻿using Point.CapacityManagement.Business.Logic;
using System;

namespace Point.CapacityManagement.Business.Helpers
{
    public class ExceptionHelper
    {
        public static int GetErrorNumber(DateTime datetime)
        {
            var random = new Random(DateTime.Now.Millisecond).Next(10, 99);
            return Convert.ToInt32(random.ToString() + datetime.ToString("HHmm"));
        }

        public static int SendMessageException(Exception exc, string url = null, string queryString = null, string postData = null, string cookies = null, string userAgent = null, bool? isAjaxRequest = null)
        {
            var errorNumber = GetErrorNumber(DateTime.Now);
            LoggingBL.LogException(errorNumber, exc, url, queryString, postData, cookies, userAgent, isAjaxRequest);
            return errorNumber;
        }
    }
}
