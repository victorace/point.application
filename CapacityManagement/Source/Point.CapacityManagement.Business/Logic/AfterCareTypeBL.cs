﻿using Point.CapacityManagement.Database.Context;
using Point.CapacityManagement.Database.Models;
using System.Collections.Generic;
using System.Linq;

// Return only IQueryable from *BL.cs
namespace Point.CapacityManagement.Business.Logic
{
    public class AfterCareTypeBL
    {
        private readonly IQueryable<AfterCareType> _afterCareTypes;

        public AfterCareTypeBL(CapacityManagementContext context)
        {
            _afterCareTypes = context.AfterCareType.Where(x => x.IsActive);
        }

        public IQueryable<AfterCareType> GetAll()
        {
            return _afterCareTypes;
        }

        public IQueryable<AfterCareType> GetByAfterCareGroupID(int afterCareGroupID)
        {
            return _afterCareTypes.Where(act => act.AfterCareGroupID == afterCareGroupID);
        }

        public IQueryable<AfterCareType> GetByAfterCareGroupIDs(List<int> afterCareGroupIDs)
        {
            return _afterCareTypes.Where(act => afterCareGroupIDs.Contains((int)act.AfterCareGroupID));
        }

        public IQueryable<AfterCareType> GetByAfterCareCategoryID(int afterCareCategoryID)
        {
            return GetByAfterCareCategoryIDs(new[] { afterCareCategoryID });
        }

        public IQueryable<AfterCareType> GetByAfterCareCategoryIDs(int[] afterCareCategoryIDs)
        {
            return _afterCareTypes.Where(act => act.AfterCareGroupID != null && afterCareCategoryIDs.Contains((int)act.AfterCareCategoryID));
        }
    }
}
