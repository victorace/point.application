﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Point.CapacityManagement.Database.Context;
using Point.CapacityManagement.Database.Models;

// Return only IQueryable from *BL.cs
namespace Point.CapacityManagement.Business.Logic
{
    public class AfterCareTileGroupBL
    {
        private readonly IQueryable<AfterCareTileGroup> _afterCareTileGroups;

        public AfterCareTileGroupBL(CapacityManagementContext context)
        {
            _afterCareTileGroups = context.AfterCareTileGroup;
        }

        public async Task<List<AfterCareTileGroup>> GetAll()
        {
            return await _afterCareTileGroups.ToListAsync();
        }
    }
}
