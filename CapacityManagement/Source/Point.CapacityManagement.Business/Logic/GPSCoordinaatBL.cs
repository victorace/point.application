﻿using Point.CapacityManagement.Database.Context;
using Point.CapacityManagement.Database.Models;
using System.Linq;

namespace Point.CapacityManagement.Business.Logic
{
    public class GPSCoordinaatBL
    {
        private readonly IQueryable<GPSCoordinaat> _gpsCoordinaten;

        public GPSCoordinaatBL(CapacityManagementContext context)
        {
            _gpsCoordinaten = context.GPSCoordinaat;
        }
        
        public GPSCoordinaat GetStartCoordinate(string postalCode)
        {
            var startcoordinate = _gpsCoordinaten.FirstOrDefault(it => it.Postcode.StartsWith(postalCode));
            if (startcoordinate == null && postalCode.Length == 6)
            {
                startcoordinate = _gpsCoordinaten.FirstOrDefault(it => it.Postcode.StartsWith(postalCode.Substring(5)));
            }
            if (startcoordinate == null && postalCode.Length == 5)
            {
                startcoordinate = _gpsCoordinaten.FirstOrDefault(it => it.Postcode.StartsWith(postalCode.Substring(4)));
            }
            if (startcoordinate == null)
            {
                startcoordinate = new GPSCoordinaat() { Latitude = 0, Longitude = 0, Postcode = "" };
            }
            return startcoordinate;
        }
    }
}
