﻿using Point.CapacityManagement.Business.Models;
using Point.CapacityManagement.Database.Context;
using Point.CapacityManagement.Database.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Point.CapacityManagement.Business.Logic
{
    public class DepartmentCapacitySyncBL
    {

        public static void Upsert(DepartmentCapacityUpdate departmentCapacity)
        {            
            using (var context = new CapacityManagementContext())
            {
                var capDepartment = GetByPointDepartmentID(context, departmentCapacity.DepartmentID);

                if (capDepartment != null && capDepartment.DepartmentCapacityID >0) //Update
                {
                    capDepartment = toDbModel(capDepartment, departmentCapacity);                    
                    context.Entry(capDepartment).State = EntityState.Modified;
                }
                else
                {
                    capDepartment = new DepartmentCapacity();
                    capDepartment = toDbModel(capDepartment, departmentCapacity);
                    context.Entry(capDepartment).State = EntityState.Added;
                }

                upsertAlternativeRegions(context, capDepartment, departmentCapacity.AlternativeRegionIDs);

                context.SaveChanges();
            }
        }

        public static void UpdateRegionFromPoint(RegionCapacityUpdate pointRegion)
        {
            using (var context = new CapacityManagementContext())
            {
                var regionBL = new RegionBL(context);

                var region = regionBL.GetByID(pointRegion.RegionID);
                if (region != null && region.RegionID > 0)
                {
                    region.Name = pointRegion.Name;
                    region.CapacityBeds = pointRegion.CapacityBeds;
                    context.Entry(region).State = EntityState.Modified;
                }
                context.SaveChanges();
            }
        }

        public static void UpdateOrganizationFromPoint(OrganizationCapacityUpdate pointOrganization)
        {
            using (var context = new CapacityManagementContext())
            {
                var organizationDeps = GetByOrganizationId(context, pointOrganization.OrganizationID ).ToList();
                                
                foreach (var CapDep in organizationDeps)
                {
                    CapDep.OrganizationName = pointOrganization.OrganizationName;
                    CapDep.RegionID = pointOrganization.RegionID;
                    CapDep.RegionName = pointOrganization.RegionName;
                    CapDep.IsActive = pointOrganization.IsActive;
                    CapDep.ModifiedOn = pointOrganization.ModifiedOn;

                    context.Entry(CapDep).State = EntityState.Modified;
                }
                context.SaveChanges();
            }
        }

        public static void UpdateLocationFromPoint(LocationCapacityUpdate pointLocation)
        {
            using (var context = new CapacityManagementContext())
            {
                var departmentcapacities = GetByLocationId(context, pointLocation.LocationID).ToList();

                foreach (var departmentcapacity in departmentcapacities)
                {
                    departmentcapacity.LocationCity = pointLocation.LocationCity;
                    departmentcapacity.LocationEmailAddress = pointLocation.LocationEmailAddress;
                    departmentcapacity.LocationName = pointLocation.LocationName;
                    departmentcapacity.LocationNumber = pointLocation.LocationNumber;
                    departmentcapacity.LocationPostalCode = pointLocation.LocationPostalCode;
                    departmentcapacity.LocationStreet = pointLocation.LocationStreet;
                    departmentcapacity.IsActive = pointLocation.IsActive;
                    departmentcapacity.ModifiedOn = pointLocation.ModifiedOn;
                    context.Entry(departmentcapacity).State = EntityState.Modified;

                    upsertAlternativeRegions(context, departmentcapacity, pointLocation.AlternativeRegionIDs);

                    context.SaveChanges();
                }
            }
        }

        private static void upsertAlternativeRegions(CapacityManagementContext context, DepartmentCapacity dep, List<int> regionids)
        {
            // remove AlternativeRegions no longer connected
            dep.AlternativeRegions.Where(ar => !regionids.Any(plar => plar == ar.RegionID))
                .ToList().ForEach(ar => context.Entry(ar).State = EntityState.Deleted);

            // add additional AlternativeRegions
            regionids.Where(plar => !dep.AlternativeRegions.Any(ar => ar.RegionID == plar))
                .ToList().ForEach(ar => context.Entry(new AlternativeRegion()
                {
                    DepartmentCapacityID = dep.DepartmentCapacityID,
                    RegionID = ar
                }).State = EntityState.Added);
        }

        private static DepartmentCapacity GetByPointDepartmentID(CapacityManagementContext context, int pointDepartmentId)
        {
            return context.DepartmentCapacity.FirstOrDefault(dc => dc.DepartmentID == pointDepartmentId);
        }

        private static IEnumerable<DepartmentCapacity> GetByOrganizationId(CapacityManagementContext context, int organizationId)
        {
            return context.DepartmentCapacity.Where(dep => dep.OrganizationID == organizationId);
        }

        private static IEnumerable<DepartmentCapacity> GetByLocationId(CapacityManagementContext context, int locationId)
        {
            return context.DepartmentCapacity.Where(dep => dep.LocationID == locationId);
        }

        private static DepartmentCapacity toDbModel(DepartmentCapacity dbModel, DepartmentCapacityUpdate viewModel)
        {
            dbModel.AfterCareTypeID = viewModel.AfterCareTypeID;
            dbModel.AfterCareTypeName = viewModel.AfterCareTypeName ?? "";
            dbModel.AfterCareGroupID = viewModel.AfterCareGroupID;
            dbModel.RegionID = viewModel.RegionID;
            dbModel.RegionName = viewModel.RegionName ?? "";
            dbModel.OrganizationID = viewModel.OrganizationID;
            dbModel.OrganizationName = viewModel.OrganizationName ?? "";
            dbModel.LocationID = viewModel.LocationID;
            dbModel.LocationName = viewModel.LocationName;
            dbModel.LocationStreet = viewModel.LocationStreet;
            dbModel.LocationNumber = viewModel.LocationNumber;
            dbModel.LocationPostalCode = viewModel.LocationPostalCode;
            dbModel.LocationCity = viewModel.LocationCity;
            dbModel.LocationEmailAddress = viewModel.LocationEmailAddress;
            dbModel.DepartmentID = viewModel.DepartmentID;
            dbModel.DepartmentName = viewModel.DepartmentName;
            dbModel.DepartmentPhoneNumber = viewModel.DepartmentPhoneNumber;
            dbModel.DepartmentFaxNumber = viewModel.DepartmentFaxNumber;
            dbModel.DepartmentEmailAddress = viewModel.DepartmentEmailAddress;
            dbModel.IsActive = viewModel.IsActive;
            dbModel.AdjustmentCapacityDate = viewModel.AdjustmentCapacityDate;
            dbModel.Capacity1 = viewModel.Capacity1;
            dbModel.Capacity2 = viewModel.Capacity2;
            dbModel.Capacity3 = viewModel.Capacity3;
            dbModel.Capacity4 = viewModel.Capacity4;
            dbModel.Information = viewModel.Information;
            dbModel.ModifiedBy = viewModel.ModifiedBy;
            dbModel.ModifiedOn = viewModel.ModifiedOn;
            dbModel.CapacityFunctionality = viewModel.CapacityFunctionality;

            dbModel.AlternativeRegions = viewModel.AlternativeRegionIDs
                .Select(ar => new AlternativeRegion()
                {
                    DepartmentCapacityID = dbModel.DepartmentCapacityID,
                    RegionID = ar
                }).ToList();

            return dbModel;
        }
    }
}
