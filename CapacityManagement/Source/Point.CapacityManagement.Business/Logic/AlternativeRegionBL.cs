﻿using Point.CapacityManagement.Database.Context;
using Point.CapacityManagement.Database.Models;
using System.Collections.Generic;
using System.Linq;

// Return only IQueryable from *BL.cs
namespace Point.CapacityManagement.Business.Logic
{
    public class AlternativeRegionBL
    {
        private readonly IQueryable<AlternativeRegion> _locationRegions;

        // Constructor
        public AlternativeRegionBL(CapacityManagementContext context)
        {
            _locationRegions = context.LocationRegion;
        }

        public IQueryable<int> GetLocationIDsByRegionID(int regionid)
        {
            return _locationRegions.Where(lr => lr.RegionID == regionid).Select(x => x.DepartmentCapacityID);
        }

        public IQueryable<AlternativeRegion> GetByDepartmentCapacityID(int departmentCapacityID)
        {
            return _locationRegions.Where(lr => lr.DepartmentCapacityID == departmentCapacityID);
        }

        public IQueryable<AlternativeRegion> GetByDepartmentCapacityID(IEnumerable<int> departmentCapacityIDs)
        {
            return _locationRegions.Where(lr => departmentCapacityIDs.Contains(lr.DepartmentCapacityID));
        }
    }
}
