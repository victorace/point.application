﻿using log4net;
using System;

namespace Point.CapacityManagement.Business.Logic
{
    public static class LoggingBL
    {
        private static ILog Log { get; set; }

        static LoggingBL()
        {
            Log = LogManager.GetLogger(typeof(LoggingBL));
        }

        public static void LogException(int errorNumber, Exception exc, string url = null, string queryString = null, string postData = null, string cookies = null, string userAgent = null, bool? isAjaxRequest = null)
        {
            MDC.Set("url", url);
            MDC.Set("queryString", queryString);
            MDC.Set("postData", postData);
            MDC.Set("cookies", cookies);
            MDC.Set("userAgent", userAgent);
            MDC.Set("isAjaxRequest", isAjaxRequest.HasValue ? isAjaxRequest.Value.ToString() : "false");
            MDC.Set("machineName", Environment.MachineName);
            MDC.Set("windowsIdentity", Environment.UserName);
            MDC.Set("stacktrace", exc?.StackTrace);
            MDC.Set("errorNumber", errorNumber.ToString());

            Log.Error(exc?.Message, exc);
        }

        public static void Error(object msg)
        {
            Log.Error(msg);
        }

        public static void Error(object msg, Exception ex)
        {
            Log.Error(msg, ex);
        }

        public static void Error(Exception ex)
        {
            Log.Error(ex.Message, ex);
        }

        public static void Fatal(object msg)
        {
            Log.Fatal(msg);
        }

        public static void Fatal(object msg, Exception ex)
        {
            Log.Fatal(msg, ex);
        }

        public static void Fatal(Exception ex)
        {
            Log.Fatal(ex.Message, ex);
        }

        public static void Info(object msg)
        {
            Log.Info(msg);
        }

        public static void Debug(object msg)
        {
            Log.Debug(msg);
        }
    }
}
