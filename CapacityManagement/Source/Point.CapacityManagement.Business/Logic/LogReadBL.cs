﻿using Point.CapacityManagement.Database.Context;
using Point.CapacityManagement.Database.Models;
using System;

namespace Point.CapacityManagement.Business.Logic
{
    public class LogReadBL
    {
        private readonly CapacityManagementContext _context;

        public LogReadBL(CapacityManagementContext context)
        {
            _context = context;
        }

        public void Insert(string url, string urlReferrer, string queryString, string postData, string cookies, string userAgent, bool? isAjaxRequest)
        {
            var logRead = new LogRead
            {
                TimeStamp = DateTime.Now,
                Url = url,
                UrlReferrer = urlReferrer,
                QueryString = queryString,
                PostData = postData,
                Cookies = cookies,
                UserAgent = userAgent,
                IsAjaxRequest = isAjaxRequest
            };

            _context.LogRead.Add(logRead);
            _context.SaveChanges();
        }
    }
}