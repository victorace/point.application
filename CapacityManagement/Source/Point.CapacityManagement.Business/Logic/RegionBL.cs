﻿using Point.CapacityManagement.Database.Context;
using Point.CapacityManagement.Database.Models;
using System.Linq;
using Point.CapacityManagement.Business.Models;

// Return only IQueryable from *BL.cs
namespace Point.CapacityManagement.Business.Logic
{
    public class RegionBL
    {
        private readonly IQueryable<Region> _regions;

        // Constructor
        public RegionBL(CapacityManagementContext context)
        {
            _regions = context.Region;
        }

        public Region GetByID(int regionid)
        {
            return _regions.Where(r => r.RegionID == regionid).FirstOrDefault();
        }

        public IQueryable<IdAndName> GetAll()
        {
            return _regions
                .Where(x => x.CapacityBeds)
                .OrderBy(x => x.Name)
                .Select(x => new IdAndName
                {
                    Id = x.RegionID,
                    Name = x.Name
                });
        }
    }
}
