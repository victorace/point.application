﻿using Point.CapacityManagement.Database.Context;
using Point.CapacityManagement.Database.Models;
using System.Linq;

// Return only IQueryable from *BL.cs
namespace Point.CapacityManagement.Business.Logic
{
    public class AfterCareGroupBL
    {
        private readonly IQueryable<AfterCareGroup> _afterCareGroups;

        public AfterCareGroupBL(CapacityManagementContext context)
        {
            _afterCareGroups = context.AfterCareGroup;
        }

        public IQueryable<AfterCareGroup> GetAll(int? afterCareTileGroupID = null)
        {
            var groups = _afterCareGroups;

            if (afterCareTileGroupID != null)
            {
                groups = groups.Where(x => x.AfterCareTileGroupID == afterCareTileGroupID);
            }
           
            return groups.OrderBy(x => x.SortOrder);
        }
    }
}
