﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Point.CapacityManagement.Database.Context;
using Point.CapacityManagement.Database.Models;

namespace Point.CapacityManagement.Business.Logic
{
    public class RegionAfterCareTypeBL
    {
        private readonly IQueryable<RegionAfterCareType> _regionAfterCareType;

        public RegionAfterCareTypeBL(CapacityManagementContext context)
        {
            _regionAfterCareType = context.RegionAfterCareType.Where(x => x.AfterCareType.IsActive);
        }

        public List<RegionAfterCareType> GetAll(int? regionId = null)
        {       

            var regiontypes = _regionAfterCareType;

            if (regionId != null)
            {
                regiontypes = regiontypes.Where(x => x.RegionID == regionId);
            }

            return regiontypes.ToList();

        }
    }
}
