﻿using Point.CapacityManagement.Business.Models;
using Point.CapacityManagement.Database.Context;
using Point.CapacityManagement.Database.Models;
using System;
using System.Linq;
using System.Collections.Generic;

// Return only IQueryable from *BL.cs
namespace Point.CapacityManagement.Business.Logic
{
    public class DepartmentCapacityViewBL
    {
        private readonly IQueryable<DepartmentCapacity> _departmentCapacities;
        private readonly IQueryable<GPSCoordinaat> _gpsCoordinaten;
        private readonly GPSCoordinaatBL _gpsCoordinaatBL;
        private readonly IQueryable<RegionAfterCareType> _regionAfterCareType;
        private readonly IQueryable<AfterCareType> _afterCareType;


        public DepartmentCapacityViewBL(CapacityManagementContext context)
        {
            _afterCareType = context.AfterCareType?.Where(x => x.IsActive);
            //ONLY: active departments with "Toon op capaciteitscherm == true" must be shown (sum). + zorgtype must be active too.
            _departmentCapacities = context.DepartmentCapacity?.Where(x => x.IsActive && (x.CapacityFunctionality ?? false) &&
                                    _afterCareType.Any(act => act.AfterCareTypeID == x.AfterCareTypeID)); 
            _gpsCoordinaten = context.GPSCoordinaat;
            _gpsCoordinaatBL = new GPSCoordinaatBL(context);
            _regionAfterCareType = context.RegionAfterCareType?.Where(x => x.AfterCareType.IsActive);            
        }

        public IQueryable<DepartmentCapacity> GetCapacityList(CapacitySearchParameters capacitySearchParameters)
        {
            var capacities = GetAllByRegionAndAfterCareGroupID(capacitySearchParameters.RegionIds, capacitySearchParameters.AfterCareGroupID);

            if (capacitySearchParameters.AfterCareTypeID != null)
            {
                capacities = capacities?.Where(cap => cap.AfterCareTypeID == capacitySearchParameters.AfterCareTypeID.Value);
            }

            // Add all words in the name search strings, one by one:
            var nameSearchArray = $"{capacitySearchParameters.NameSearch1} {capacitySearchParameters.NameSearch2}".Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (var nameSearch in nameSearchArray)
            {
                if (!string.IsNullOrWhiteSpace(nameSearch))
                {
                    capacities = capacities?.Where(cap =>
                        cap.OrganizationName.Contains(nameSearch) ||
                        cap.LocationName.Contains(nameSearch) ||
                        cap.DepartmentName.Contains(nameSearch));
                }
            }
            if (!string.IsNullOrWhiteSpace(capacitySearchParameters.CitySubstring))
            {
                capacities = capacities?.Where(cap => cap.LocationCity.Contains(capacitySearchParameters.CitySubstring));
            }

            if (capacitySearchParameters.WithCapacityOnly)
            {
                capacities = capacities?.Where(x => (x.Capacity1 ?? 0) > 0  || (x.Capacity2 ?? 0) >0 || (x.Capacity3 ?? 0) > 0 || (x.Capacity4 ?? 0) > 0);
            }

            if (!string.IsNullOrWhiteSpace(capacitySearchParameters.PostalCodeSubstring))
            {

                var startcoordinates = _gpsCoordinaatBL.GetStartCoordinate(capacitySearchParameters.PostalCodeSubstring);

                double gpsrange = (double)1 / 111 * (capacitySearchParameters.PostalCodeRadius ?? 5);
                double maxlongitude = startcoordinates.Longitude.GetValueOrDefault(0) + gpsrange;
                double minlongitude = startcoordinates.Longitude.GetValueOrDefault(0) - gpsrange;
                double maxlatitude = startcoordinates.Latitude.GetValueOrDefault(0) + gpsrange;
                double minlatitude = startcoordinates.Latitude.GetValueOrDefault(0) - gpsrange;

                capacities = capacities?.Join(_gpsCoordinaten, cap => cap.LocationPostalCode, coord => coord.Postcode, (cap, coord) => new { cap, coord })
                    .Where(com => com.coord.Longitude > minlongitude && com.coord.Longitude < maxlongitude && com.coord.Latitude > minlatitude && com.coord.Latitude < maxlatitude)
                    .Select(com => com.cap);
            }

            return capacities;
        }

        public IQueryable<int> GetTotalCapacityByAfterCareGroupID(List<int> regionIds, int afterCareGroupID)
        {
           return GetAllByRegionAndAfterCareGroupID(regionIds, afterCareGroupID)?.Select(x => (x.Capacity1 ?? 0));
        }

        public IQueryable<AfterCareTypeWithCapacity> GetAfterCareTypesByAfterCareGroupID(List<int> regionIds, int afterCareGroupID)
        {
            return GetAllByRegionAndAfterCareGroupID(regionIds, afterCareGroupID)?
                .GroupBy(x => new { x.AfterCareTypeID, x.AfterCareTypeName })
                .Select(y => new AfterCareTypeWithCapacity
                {
                    AfterCareTypeID = y.Key.AfterCareTypeID ?? 0,
                    AfterCareTypeName = y.Key.AfterCareTypeName,
                    AfterCareTypeCapacity = y.Sum(z => (z.Capacity1 ?? 0))
                });
        }

        public IQueryable<DepartmentCapacityGPS> GetMarkers(CapacitySearchParameters capacitySearchParameters)
        {
            var capacities = GetAllByRegionAndAfterCareGroupID(capacitySearchParameters.RegionIds, capacitySearchParameters.AfterCareGroupID);

            if (capacitySearchParameters.AfterCareTypeID != null)
            {
                capacities = capacities?.Where(cap => cap.AfterCareTypeID == capacitySearchParameters.AfterCareTypeID.Value);
            }

            if (capacitySearchParameters.AfterCareTypeIDs != null)
            {
                capacities = capacities?.Where(cap => capacitySearchParameters.AfterCareTypeIDs.Contains(cap.AfterCareTypeID.Value));
            }

            return capacities?.Join(_gpsCoordinaten, cap => cap.LocationPostalCode, coord => coord.Postcode, (cap, coord) => new { cap, coord })
                   .Select(com => new DepartmentCapacityGPS() { DepartmentCapacity = com.cap, GPSCoordinaat = com.coord });

        }
        /// <summary>
        /// select department capacities.
        /// </summary>
        /// <param name="regionId"></param>
        /// <param name="afterCareGroupID"></param>
        /// <returns>Department capacities based on regions and after care types defined in RegionAfterCareType table.</returns>
        private IQueryable<DepartmentCapacity> GetAllByRegionAndAfterCareGroupID(List<int> regionIds, int? afterCareGroupID)
        {
                return _departmentCapacities.Where(dc =>
                (_regionAfterCareType.Where(reg => dc.Region.CapacityBeds && reg.RegionID == dc.RegionID && reg.AfterCareTypeID == dc.AfterCareTypeID && reg.AfterCareType.AfterCareGroupID == dc.AfterCareGroupID).Any() ||
                dc.AlternativeRegions.Where(ar => ar.Region.CapacityBeds && _regionAfterCareType.Where(reg => ar.RegionID == reg.RegionID && dc.AfterCareTypeID == reg.AfterCareTypeID && dc.AfterCareGroupID == reg.AfterCareType.AfterCareGroupID).Any()).Any()) &&
                (regionIds.Contains(dc.RegionID) || regionIds.Where(selectedreg => dc.AlternativeRegions.Select(ar => ar.RegionID).Contains(selectedreg)).Any()) &&
                (afterCareGroupID == null || dc.AfterCareGroupID == afterCareGroupID)
                && (dc.Region.CapacityBeds || dc.AlternativeRegions.Where(ar => ar.Region.CapacityBeds).Any())                
                );
        }
    }
}
