﻿using Point.CapacityManagement.Database.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Point.CapacityManagement.Database.Context
{
    public class CapacityManagementContext : DbContext
    {
        public CapacityManagementContext() : base("name=PointCapacityManagementContext")
        {
            Configuration.AutoDetectChangesEnabled = false;
        }

        public virtual DbSet<DepartmentCapacity> DepartmentCapacity { get; set; }
        public virtual DbSet<AfterCareGroup> AfterCareGroup { get; set; }
        public virtual DbSet<AfterCareTileGroup> AfterCareTileGroup { get; set; }
        public virtual DbSet<AfterCareType> AfterCareType { get; set; }
        public virtual DbSet<GPSCoordinaat> GPSCoordinaat { get; set; }
        public virtual DbSet<AlternativeRegion> LocationRegion { get; set; }
        public virtual DbSet<LogRead> LogRead { get; set; }
        public virtual DbSet<Region> Region { get; set; }
        public virtual DbSet<RegionAfterCareType> RegionAfterCareType { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}
