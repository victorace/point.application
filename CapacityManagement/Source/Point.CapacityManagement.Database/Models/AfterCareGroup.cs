﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Point.CapacityManagement.Database.Models
{
    public class AfterCareGroup
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AfterCareGroupID { get; set; }

        [StringLength(255), Required]
        public string Name { get; set; }

        [Required]
        public int SortOrder { get; set; }

        public int? AfterCareTileGroupID { get; set; }
        public virtual AfterCareTileGroup AfterCareTileGroup { get; set; }
    }
}
