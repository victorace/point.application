﻿using System.Collections.Generic;

namespace Point.CapacityManagement.Database.Models
{
    public class AfterCareTileGroup
    {
        public int AfterCareTileGroupID { get; set; }
        public string Name { get; set; }
        public string CssClass { get; set; }
        public int SortOrder { get; set; }

        public virtual ICollection<AfterCareGroup> AfterCareGroup { get; set; }
    }
}
