﻿namespace Point.CapacityManagement.Database.Models
{
    public class GPSCoordinaat
    {
        public int GPSCoordinaatID { get; set; }
        public string Postcode { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
    }
}
