﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Point.CapacityManagement.Database.Models
{
    public class AlternativeRegion
    {
        public int AlternativeRegionID { get; set; }
        public int DepartmentCapacityID { get; set; }
        public virtual DepartmentCapacity DepartmentCapacity { get; set; }
        public int RegionID { get; set; }
        public virtual Region Region { get; set; }
    }
}
