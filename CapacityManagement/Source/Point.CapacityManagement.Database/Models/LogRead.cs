﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Point.CapacityManagement.Database.Models
{
    public class LogRead
    {
        public int LogReadID { get; set; }
        public DateTime? TimeStamp { get; set; }
        [StringLength(2048)]
        public string Url { get; set; }
        public string UrlReferrer { get; set; }
        [StringLength(2048)]
        public string QueryString { get; set; }
        public string PostData { get; set; }
        [StringLength(2048)]
        public string Cookies { get; set; }
        [StringLength(500)]
        public string UserAgent { get; set; }
        public bool? IsAjaxRequest { get; set; }
    }
}
