﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Point.CapacityManagement.Database.Models.Constants
{
    public static class Constants
    {
        public const string ActiveInRegion = "ActiveInRegion";
        public const string DeActiveInRegion = "DeActiveInRegion";
    }
}
