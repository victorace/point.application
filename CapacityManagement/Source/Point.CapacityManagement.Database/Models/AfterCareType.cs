﻿namespace Point.CapacityManagement.Database.Models
{
    public class AfterCareType
    {
        public int AfterCareTypeID { get; set; }
        public string AfterCareTypeName { get; set; }
        public int? AfterCareCategoryID { get; set; } 
        public int? AfterCareGroupID { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }
}
