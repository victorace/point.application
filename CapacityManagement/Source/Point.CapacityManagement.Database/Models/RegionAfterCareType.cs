﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Point.CapacityManagement.Database.Models
{
    public class RegionAfterCareType
    {
        public int RegionAfterCareTypeID { get; set; }
        public int RegionID { get; set; }
        public int AfterCareTypeID { get; set; }        
        public virtual AfterCareType AfterCareType { get; set; }
        public virtual Region Region { get; set; }
    }
}
