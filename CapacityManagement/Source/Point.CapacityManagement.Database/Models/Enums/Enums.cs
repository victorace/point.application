﻿
namespace Point.CapacityManagement.Database.Models.Enums
{
    public enum AfterCareCategoryID
    {
        Thuiszorg = 1,
        Zorginstelling = 2,
        Hospice = 3
    }

    public enum PageType
    {
        Tile = 1,
        Others = 0
    }

}
