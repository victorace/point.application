﻿using Point.Planbord.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using Point.Planbord.Infrastructure.Extensions;

namespace Point.Planbord.Database.Extensions
{
    public static class CodeExtensions
    {

        public static string GetDiagnose(this IEnumerable<Code> codes)
        {
            DiagnosisDisplayCodeType diagnosisDisplayCodeType = DiagnosisDisplayCodeType.ICD10;
            UserSettings us = SettingExtensions.GetUsersSettingsFromSession();
            if (us != null)
                diagnosisDisplayCodeType = us.GetDiagnoseDisplayCodeType();
            return codes.GetValue(CodeMainType.Diagnose, diagnosisDisplayCodeType.ToString());
        }

        public static IEnumerable<Code> GetDiagnoseCode(this IEnumerable<Code> codes)
        {
            DiagnosisDisplayCodeType diagnosisDisplayCodeType = DiagnosisDisplayCodeType.ICD10;
            UserSettings us = SettingExtensions.GetUsersSettingsFromSession();
            if (us != null)
                diagnosisDisplayCodeType = us.GetDiagnoseDisplayCodeType();

            return codes.GetValues(CodeMainType.Diagnose, diagnosisDisplayCodeType.ToString());

        }

        public static string DischargeAcceptedAnswer(this IEnumerable<Code> codes)
        {
            string value = codes.GetValue(CodeMainType.PointStatus, "OntslagGeaccepteerd");
            if (!String.IsNullOrWhiteSpace(value))
                return "Patiënt kan overgenomen/in zorg genomen worden: " + value.ToUpper();
            else
                return "";
        }

        public static bool DischargeAcceptedAnswerYes(this IEnumerable<Code> codes)
        {
            string value = codes.GetValue(CodeMainType.PointStatus, "OntslagGeaccepteerd");
            return (value.ToUpper() == "JA");
        }

        public static string DischargeText(this IEnumerable<Code> codes)
        {
            //@item.Codes.GetValue(CodeMainType.PointStatus, "ActieveFase")
            string value = codes.GetValue(CodeMainType.PointStatus, "ActieveFase");
            if (value.ToUpper() == "TO") 
                value = "";

            return value;
        }

        public static string DischargeIcon(this IEnumerable<Code> codes)
        {
            string value = codes.GetValue(CodeMainType.PointStatus, "ActieveFase");
            if (value.ToUpper() == "TO")
                return "iconDotGray";

            if (DischargeAcceptedAnswerYes(codes))
                return "iconDotGreen";

            return "";
        }

        public static string PointPhase(this IEnumerable<Code> codes)
        {
            return codes.GetValue(CodeMainType.PointStatus, "ActieveFaseBeschrijving");
        }


        public static string BedIcon(this IEnumerable<Code> codes)
        {
            return codes.GetValue(CodeMainType.Bed);
        }

        public static int RestrictionCount(this IEnumerable<Code> codes)
        {
            if (codes == null || codes.Count() == 0) return 0;

            return codes.Count(c => c.CodeMainType == CodeMainType.Behandelingsbeperking && c.CodeType != "Code");
        }

        public static string RestrictionCode(this IEnumerable<Code> codes, int? patientID)
        {
            var value = codes.GetValue(CodeMainType.Behandelingsbeperking, "Code");
            if (String.IsNullOrEmpty(value)) value = (patientID > 0 ? "?" : "");

            return value;
        }

        public static bool VitalMissedMeasurement(this IEnumerable<Code> codes)
        {
            bool missedmeasurement = false;
            foreach (Code code in codes.Where(c => c.CodeMainType == CodeMainType.VitaleSignalen))
            {
                if (code.VitalMissedMeasurement())
                {
                    missedmeasurement = true;
                    break;
                }
            }
            return missedmeasurement;
        }

        public static bool VitalMissedMeasurement(this Code code)
        {
            double minutes = code.GetCodeCycleInMinutes();
            bool missedmeasurement = false;
            if (code.LastMeasuring.HasValue)
            {
                if (code.LastMeasuring.Value.AddMinutes(minutes) < DateTime.Now)
                    missedmeasurement = true;
            }
            return missedmeasurement;
        }

        public static IsolationCode GetIsolationCode(this Code code)
        {
            if (code == null || code.CodeMainType != CodeMainType.Isolatie) return new IsolationCode();

            return IsolationCode.GetList().Where(ic => ic.SystemCode == code.CodeValue).FirstOrNew();
        }

        public static bool QScoreMissedMeasurement(this IEnumerable<Code> codes)
        {
            bool missedmeasurement = false;
            foreach (Code code in codes.Where(c => c.CodeMainType == CodeMainType.QScore && c.CodeType != "Pijnscore"))
            {
                if (code.QScoreMissedMeasurement())
                {
                    missedmeasurement = true;
                    break;
                }
            }
            return missedmeasurement;
        }

        public static bool QScoreMissedMeasurement(this Code code)
        {
            bool missedmeasurement = false;
            if (string.IsNullOrEmpty(code.CodeValue))
            {
                if (code.LastMeasuring.HasValue)
                {
                    double hours = DateTime.Now.Subtract(code.LastMeasuring.Value).TotalHours;
                    if (hours > 24)
                        missedmeasurement = true;
                }

            }
            return missedmeasurement;
        }

        public static bool QScorePijnscore(this IEnumerable<Code> codes)
        {
            int score = -1;
            Code code = codes.Where(c => c.CodeMainType == CodeMainType.QScore && c.CodeType == "Pijnscore").FirstOrDefault();
            if (code != null)
            {
                Int32.TryParse(code.GetValue(), out score);
            }

            return score >= 4; 
        }

        public static int GetCount(this IEnumerable<Code> codes, CodeMainType codeMainType)
        {
            if (codes == null || codes.Count() == 0) return 0;

            return codes.Count(c => c.CodeMainType == codeMainType);
        }

        public static Code GetCode(this IEnumerable<Code> codes, CodeMainType codeMainType, string codeType = "")
        {
            if (codes == null || codes.Count() == 0) return default(Code);

            Code code = codes.FirstOrDefault(c => c.CodeMainType == codeMainType && (codeType == "" || c.CodeType == codeType));
            if (code == null) return default(Code);

            return code;
        }

        public static IEnumerable<Code> GetValues(this IEnumerable<Code> codes, CodeMainType codeMainType, string codeType)
        {
            return codes.Where(c => c.CodeMainType == codeMainType && c.CodeType == codeType);
        }


        public static IEnumerable<Code> GetValues(this IEnumerable<Code> codes, CodeMainType codeMainType)
        {
            return codes.Where(c => c.CodeMainType == codeMainType);
        }

        public static string GetValue(this IEnumerable<Code> codes, CodeMainType codeMainType, string codeType)
        {
            if (codes == null || codes.Count() == 0) return "";

            Code code = codes.FirstOrDefault(c => c.CodeMainType == codeMainType && c.CodeType == codeType);
            if (code == null) return "";

            return code.GetValue();
        }

        public static string GetValue(this IEnumerable<Code> codes, CodeMainType codeMainType)
        {
            if (codes == null || codes.Count() == 0) return "";

            Code code = codes.FirstOrDefault(c => c.CodeMainType == codeMainType);
            if (code == null) return "";

            return code.GetValue();
        }

        public static string GetValue(this IEnumerable<Code> codes, string codeType)
        {
            if (codes == null || codes.Count() == 0) return "";

            Code code = codes.FirstOrDefault(c => c.CodeType.Equals(codeType, StringComparison.InvariantCultureIgnoreCase));
            if (code == null) return "";

            return code.GetValue();
        }

        public static string GetValue(this Code code)
        {
            if (code == null) return "";

            return code.CodeValue ?? "";
        }

        public static string GetCodeTypeDescription(this Code code)
        {
            return code.CodeType.GetDescription() ?? "";
        }

        public static Double GetCodeCycleInMinutes(this Code code)
        {
            double minutes = 0;
            UserSettings us = SettingExtensions.GetUsersSettingsFromSession();
            if (us != null)
            {
                SettingsCodeValues codeValues = us.GetCodeCycles();
                if (codeValues != null)
                {
                    try
                    {
                        SettingsCodeCycleValue codeCycle = codeValues.Codes.Where(c => c.CodeType == code.CodeType).FirstOrDefault();
                        if (codeCycle != null && !String.IsNullOrWhiteSpace(codeCycle.Value))
                            minutes = Convert.ToDouble(codeCycle.Value);
                        else if (codeCycle == null)
                            minutes = Convert.ToDouble(codeValues.Default);
                    }
                    catch
                    {
                        minutes = 0;
                    }
                }
            }

            return minutes;
        }

        public static string GetEwsDateTime(this IEnumerable<Code> codes)
        {
            string ewsDateTime = "";
            
            var ewsDate = codes.GetValue(CodeMainType.EwsScore, "Datum");
            var ewsTime = codes.GetValue(CodeMainType.EwsScore, "Tijd");

            if (!String.IsNullOrWhiteSpace(ewsDate) && !String.IsNullOrWhiteSpace(ewsTime))
            {
                ewsDateTime = String.Concat(ewsDate, "T", ewsTime);
            }
            return ewsDateTime;
        }

        public static string GetEwsAction(this IEnumerable<Code> codes)
        {
            string ewsAction = codes.GetValue(CodeMainType.EwsScore, "Actie");
            return ewsAction;
        }

    }
}