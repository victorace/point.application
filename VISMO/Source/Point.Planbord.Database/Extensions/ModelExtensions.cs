﻿using Point.Planbord.Database.Models;
using Point.Planbord.Infrastructure;
using Point.Planbord.Infrastructure.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Point.Planbord.Database.Extensions
{
    public static class ModelExtensions
    {
        // Place some model-specific extensions here ...

        // Entity Code is in CodeExtensions.cs

        public static string FullName<T>(this T person) where T : Person, new()
        {

            if (person == null) person = new T();

            string name = "";

            if (person.LastName != null)
                name += person.LastName.UppercaseFirst() + ", ";
            if (person.FirstName != null)
                name += person.FirstName.UppercaseFirst() + " ";
            if (person.MiddleName != null)
                name += person.MiddleName.ToLower();

            return name.Trim(new[] { ',', ' ' });
        }

        public static string FullNameInitials<T>(this T person) where T : Person, new()
        {
            if (person == null) person = new T();

            string name = "";

            if (person.LastName != null)
                name += person.LastName.UppercaseFirst() + ", ";
            if (person.Initials != null)
                name += person.Initials.ToUpper() + " ";
            if (person.MiddleName != null)
                name += person.MiddleName.ToLower();

            return name.Trim(new[] { ',', ' ' });
        }

        public static string FullNameInitialsPatient(this Patient patient)
        {
            if (patient == null) return "";

            string name = FullNameInitials(patient);
            if (String.IsNullOrEmpty(name)) name = patient.FullName;

            return name;
        }

        public static string SafeBirthDate(this Patient patient)
        {
            string birthDate = "";

            if (patient.BirthDate.HasValue)
            {
                birthDate = patient.BirthDate.Value.ToString("dd-MM-yyyy");
            }

            return birthDate;
        }

        public static string FirstMiddleLastName<T>(this T person) where T : Person, new()
        {
            if (person == null) person = new T();

            string name = String.Format("{0} {1} {2}", person.FirstName, person.MiddleName, person.LastName).ReplaceDoubleToSingle(' ');

            //if ( person.FirstName != null)
            return name.Trim(new[] { ',', ' ' });

        }

        public static string Description(this Bed bed)
        {
            if (bed == null) bed = new Bed();

            string description = "";

            if (bed.RoomNumber == null && bed.BedNumber == null)
            {
                description = bed.SystemCode ?? "";
            }
            else
            {

                if (bed.Department != null)
                    description += bed.Department.ShortCode + ".";
                if (bed.RoomNumber != null)
                    description += bed.RoomNumber + ".";
                if (bed.BedNumber != null)
                    description += bed.BedNumber;
            }
            return description.Trim('.');

        }

        public static string GenderSymbol(this Patient patient)
        {
            string symbol = "";
            if ( patient == null || patient.PatientID <= 0 ) return symbol;

            switch (patient.Gender)
            {
                case Gender.Female:
                    symbol = "&#9792;";
                    break;
                case Gender.Male:
                    symbol = "&#9794;";
                    break;
                case Gender.Unknown:
                default:
                    symbol = "";
                    break;
            }

            return symbol;
        }

        public static string GenderText(this Patient patient)
        {
            if (patient == null || patient.PatientID <= 0)
                return "";
            else
                return patient.Gender.GetDescription();
        }

        public static bool ToBeDischarged(this BedForPatient bedforpatient)
        {
            if (bedforpatient == null) return false;

            return (bedforpatient.BedPatientStatus == BedPatientStatus.Ontslagen || bedforpatient.BedPatientStatus == BedPatientStatus.OntslagGepland);
        }

        public static Nurse NurseForRole(this IEnumerable<ShiftDetail> shiftdetails, ShiftDetailRole shiftdetailrole)
        {
            if (shiftdetails == null || shiftdetails.Count() == 0) return new Nurse();

            Nurse nurse = shiftdetails.FirstOrNew(sd => sd.ShiftDetailRole == shiftdetailrole).Nurse;

            if (nurse == null) nurse = new Nurse();

            return nurse;
        }

        public static ShiftDetail ShiftDetailForRole(this IEnumerable<ShiftDetail> shiftdetails, ShiftDetailRole shiftdetailrole)
        {
            if (shiftdetails == null || shiftdetails.Count() == 0) return new ShiftDetail();

            return shiftdetails.FirstOrNew(sd => sd.ShiftDetailRole == shiftdetailrole);
        }

        public static string DropdownDisplay(this ShiftDetail shiftdetail)
        {
            if (shiftdetail == null) return "";

            if (shiftdetail.Nurse == null) shiftdetail.Nurse = new Nurse();

            string detaildisplay = shiftdetail.Nurse.FirstMiddleLastName();
            if (shiftdetail.Pager != null && shiftdetail.PagerID > 0) detaildisplay += " " + shiftdetail.Pager.PagerNumber + "";
            if (shiftdetail.Pause > 0) detaildisplay += " (" + shiftdetail.Pause + ")";
            //if (shiftdetail.Visit > 0) detaildisplay += " V:" + shiftdetail.Visit;
            return detaildisplay; 
        }

        public static string NurseDisplay(this ShiftDetail shiftdetail)
        {
            if (shiftdetail == null) return "";

            if (shiftdetail.Nurse == null) return "";

            return shiftdetail.Nurse.FirstMiddleLastName();
        }

        public static string PagerPauseVisitWorkloadDisplay(this ShiftDetail shiftdetail)
        {
            if (shiftdetail == null) return "";

            if (shiftdetail.Pager == null) shiftdetail.Pager = new Pager();

            int workload = (int)shiftdetail.Workload;

            string detaildisplay = String.Format("{0}||{1}||{2}||{3}", 
                shiftdetail.Pager.PagerNumber, 
                (shiftdetail.Pause > 0 ? shiftdetail.Pause.ToString() : ""), 
                (shiftdetail.Visit > 0 ? shiftdetail.Visit.ToString() : ""),
                (workload > 0 ? workload.ToString() : ""));

            return detaildisplay;
        }

        public static string RoleName(this EmployeeUserViewModel instance)
        {
            string roleName = "";

            if (instance == null)
                return roleName;

            switch (instance.Role)
            {
                case Role.PlanbordAdministrator :
                    roleName = "Planbord Administrator";
                    break;
                case Role.OrganizationAdministrator:
                    roleName = "Organization Administrator";
                    break;
                case Role.PersonalAccount:
                    roleName = "Personal Account";
                    break;
                case Role.DepartmentAccount:
                    roleName = "Department Account";
                    break;
                case Role.CapacityAccount:
                    roleName = "Capacity Account";
                    break;
                default :
                    roleName = "";
                    break;
            }

            return roleName;
            
        }

        public static void SetParentId(this Setting instance, int parentId, PlanbordSettingsParentType planbordSettingsParentType)
        {
            if (instance == null)
                return;

            switch (planbordSettingsParentType)
            {
                case PlanbordSettingsParentType.Organization :
                    instance.OrganizationID = parentId; 
                    break;

                case PlanbordSettingsParentType.Location :
                    instance.LocationID = parentId;
                    break;

                case PlanbordSettingsParentType.Department :
                    instance.DepartmentID = parentId;
                    break;
            }
        }

        public static string EmptyShortCode(this Specialism specialism)
        {
            if (specialism == null || String.IsNullOrEmpty(specialism.ShortCode)) return " ";

            return specialism.ShortCode;
        }

        public static bool HaveRowAlert(this PlanbordViewModel planbordviewmodel)
        {
            if (planbordviewmodel == null || planbordviewmodel.BedForPatient == null) return false;

            if (!planbordviewmodel.BedForPatient.EndTime.HasValue) return false;

            return (planbordviewmodel.BedForPatient.EndTime < DateTime.Now);
        }


        public static int GetCapacityTotal(this IEnumerable<CapacityOverviewViewModel> capacityList)
        {
            
            return capacityList.Sum(cl => cl.BedTotals.Capacity);
        }

        public static int GetExpectedTotal(this IEnumerable<CapacityOverviewViewModel> capacityList)
        {
            return capacityList.Sum(cl => cl.BedTotals.ExpectedToday);
        }

        public static int GetBeddenBezetTotal(this IEnumerable<CapacityOverviewViewModel> capacityList)
        {
            return capacityList.Sum(cl => cl.BedTotals.Occupied);
        }

        //public static int GetCapacity(this IEnumerable<CapacityOverviewViewModel> capacityList)
        //{
        //    int total = 0;




        //    foreach (var capacity in capacityList)
        //        total += capacity.BedTotals.Capacity;
        //        //total += capacity.BedTotals.Free;
        //    return total;
        //}

        //public static int GetExpected(this IEnumerable<CapacityOverviewViewModel> capacityOverviewViewModel, bool excludeFuturePatients = true)
        //{
        //    int newPatients = 0;
        //    int futurePatients = 0;
        //    foreach (var capacity in capacityOverviewViewModel)
        //    {
        //        newPatients += capacity.BedTotals.ExpectedToday;
        //        if (excludeFuturePatients)
        //            futurePatients += capacity.BedTotals.ExpectedTomorrow;
        //    }
        //    int total = newPatients - futurePatients;
        //    if (total < 0) total = 0;
        //    return total;
        //}

        public static bool IsBlok(this BedPatientStatus bedpatientstatus)
        {
            return (bedpatientstatus >= BedPatientStatus.BlokBAU && bedpatientstatus <= BedPatientStatus.BlokZOM);
        }

        public static bool IsBlokReservering(this BedPatientStatus bedpatientstatus)
        {
            return (bedpatientstatus == BedPatientStatus.BlokRES ||
                    bedpatientstatus == BedPatientStatus.BlokICU ||
                    bedpatientstatus == BedPatientStatus.BlokEHH);
        }

        public static bool IsBlokBlokkade(this BedPatientStatus bedpatientstatus)
        {
            return (bedpatientstatus == BedPatientStatus.BlokDESI ||
                    bedpatientstatus == BedPatientStatus.BlokISO ||
                    bedpatientstatus == BedPatientStatus.BlokTERM ||
                    bedpatientstatus == BedPatientStatus.BlokOVR);
        }

        public static bool IsBlokGesloten(this BedPatientStatus bedpatientstatus)
        {
            return (bedpatientstatus == BedPatientStatus.BlokCAP ||
                    bedpatientstatus == BedPatientStatus.BlokZOM ||
                    bedpatientstatus == BedPatientStatus.BlokBAU);
        }

        public static bool HasHeaderMessage(this SettingHeaderMessageMessage settingHeaderMessageMessage)
        {
            return (!settingHeaderMessageMessage.EndDate.HasValue || (settingHeaderMessageMessage.EndDate.HasValue && DateTime.Now <= settingHeaderMessageMessage.EndDate.Value));
        }
    }
}