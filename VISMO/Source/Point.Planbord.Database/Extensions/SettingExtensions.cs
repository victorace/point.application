﻿using Point.Planbord.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Point.Planbord.Database.Extensions
{
    public static class SettingExtensions
    {
        // Place the setting-specific extensions here ... Can be based on another model.

        //Settings should be available from memory/cache for the logged in user/role !!!


        //http://themergency.com/calculate-text-color-based-on-background-color-brightness/
        public static string GetContrastingForeground(string colorCode, string lightClass, string darkClass)
        {
            var c = System.Drawing.ColorTranslator.FromHtml(colorCode);
            var b = Math.Sqrt(c.R * c.R * .241 + c.G * c.G * .691 + c.B * c.B * .068);
            return b < 130 ? lightClass : darkClass;
        }

        public static UserSettings GetUsersSettingsFromSession()
        {
            if (HttpContext.Current != null && HttpContext.Current.Session != null && HttpContext.Current.Session[Infrastructure.SessionKey.Settings.ToString()] != null)
                return (UserSettings)HttpContext.Current.Session[Infrastructure.SessionKey.Settings.ToString()];
            return null;
        }

        public const string SpecialismStyleColorDefault = "#3BB6F0";

        public static string SpecialismStyle(this Specialism specialism)
        {

            string color = "";

            if (specialism == null || PlanbordColumn.Appointments.DisplayType() == (int)ColumnDisplayType.Invisible || String.IsNullOrEmpty(specialism.SystemCode))
            {
                color = "Transparent";// SpecialismStyleColorDefault;
            }
            else
            {

                UserSettings us =  GetUsersSettingsFromSession();
                if (us != null)
                {
                    IList<SettingsSpecialismValues> specialisms = us.GetSpecialismColors();

                    SettingsSpecialismValues specialismValues = specialisms.Where(s => s.SystemCode == specialism.SystemCode).FirstOrDefault();
                    if (specialismValues != null)
                        color = specialismValues.Color;
                }
            }

            if (String.IsNullOrEmpty(color))
                color = SpecialismStyleColorDefault;

            if (color.EndsWith(";")) color.TrimEnd(';');
            return String.Format("background-color:#{0};color:#{1}", color.Trim('#'), GetContrastingForeground(color, "#FFF", "#000").Trim('#'));
        }

        public static string Hospitalization(this BedForPatient bedforpatient, bool sortable = false)
        {
            if (bedforpatient == null || !bedforpatient.StartTime.HasValue) return "";

            if (bedforpatient.BedPatientStatus.IsBlok()) return "";

            var hospitalization = "";

            UserSettings us = GetUsersSettingsFromSession();
            HospitalizationDisplayCodeType hospitalizationdisplaycodetype = HospitalizationDisplayCodeType.Dag;
            if (us != null)
                hospitalizationdisplaycodetype = us.GetHospitalizationDisplayCodeType();

            switch ( hospitalizationdisplaycodetype )
            {
                case HospitalizationDisplayCodeType.Dag:
                    var daydiff = (new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59, 999)
                                   - (bedforpatient.StartTime ?? DateTime.Now));
                    hospitalization = String.Format(sortable?"{0}":"{0} d.", Math.Ceiling(daydiff.TotalDays)); 
                    break;
                case HospitalizationDisplayCodeType.Uur:
                    var hourdiff = (DateTime.Now - (bedforpatient.StartTime ?? DateTime.Now));
                    hospitalization = String.Format(sortable?"{0}":"{0} u.", Math.Ceiling(hourdiff.TotalHours));
                    break;
            }

            return hospitalization;
        }

        public static string ShiftStyle(this IEnumerable<Shift> shifts, int shiftID)
        {
            //Since shiftID's don't have to start with 1, it's matched from the whole collection.
            //Shift is kinda hardcoded stuff for the moment.

            string shiftstyle = "";

            int i = 0;
            foreach (var shift in shifts)
            {
                if (shift.ShiftID == shiftID) break;
                i++;
            }
            switch (i)
            {
                default:
                case 0:
                    shiftstyle = "shiftDD"; break;
                case 1:
                    shiftstyle = "shiftLD"; break;
                case 2:
                    shiftstyle = "shiftND"; break;
            }

            return shiftstyle;
        }

        public static int DisplayType(this PlanbordColumn planbordColumn)
        {
            ColumnDisplayType columndisplaytype = GetUsersSettingsFromSession().GetColumnDisplayType(planbordColumn);

            return (int)columndisplaytype;
        }
    }
}