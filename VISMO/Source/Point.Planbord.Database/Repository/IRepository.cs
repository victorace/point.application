﻿using Point.Planbord.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Point.Planbord.Database.Repository
{
    public interface IRepository<T> : IDisposable  where T : class, IIdentified,  new()
    {
        IEnumerable<T> Get(Expression<Func<T, bool>> where = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, string includeProperties = "");
        IEnumerable<T> GetAll();
        T GetByID(object id);
        void Insert(T entity);
        void Delete(object id);
        void Delete(T entityToDelete);
        void Update(T entityToUpdate);
    }

    public interface IIdentified
    {
        int EntityID { get; set; }
    }

    public interface IInActive 
    {
        bool InActive { get; set; }
    }

    public interface ILoginHistoryRepository : IRepository<LoginHistory> { }
    public interface IEmployeeRepository : IRepository<Employee> { }
    public interface IPatientRepository : IRepository<Patient> { }
    public interface INurseRepository : IRepository<Nurse> { }
    public interface IBedRepository : IRepository<Bed> { }
    public interface IBedForPatientRepository : IRepository<BedForPatient> { }
    public interface IShiftRepository : IRepository<Shift> { }
    public interface IShiftDetailRepository : IRepository<ShiftDetail> { }
    public interface IOrganizationRepository : IRepository<Organization> { }
    public interface ILocationRepository : IRepository<Location> { }
    public interface IDepartmentRepository : IRepository<Department> { }
    public interface ISpecialismRepository : IRepository<Specialism> { }
    public interface IPagerRepository : IRepository<Pager> { }
    public interface ICycleRepository : IRepository<Cycle> { }
    public interface ICodeRepository : IRepository<Code> { }
    public interface ICodeHistoryRepository : IRepository<CodeHistory> { }
    public interface ISettingRepository : IRepository<Setting> { }
    public interface ILoggingRepository : IRepository<Logging> { }
    public interface IIconRepository : IRepository<Icon> { }
}