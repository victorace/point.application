﻿using Point.Planbord.Database.Models;
using System.Data.Entity;

namespace Point.Planbord.Database.Repository
{
    // Move to their own class when extending it with new methods
    public class LoginHistoryRepository : GenericRepository<LoginHistory>, ILoginHistoryRepository
    {
        public LoginHistoryRepository(DbContext context) : base(context) { }
    }
    public class EmployeeRepository : GenericRepository<Employee>, IEmployeeRepository
    {
        public EmployeeRepository(DbContext context) : base(context) { }
    }
    public class PatientRepository : GenericRepository<Patient>, IPatientRepository
    {
        public PatientRepository(DbContext context) : base(context) { }
    }
    public class NurseRepository : GenericRepository<Nurse>, INurseRepository
    {
        public NurseRepository(DbContext context) : base(context) { }
    }
    public class BedRepository : GenericRepository<Bed>, IBedRepository
    {
        public BedRepository(DbContext context) : base(context) { }
    }
    public class BedForPatientRepository : GenericRepository<BedForPatient>, IBedForPatientRepository
    {
        public BedForPatientRepository(DbContext context) : base(context) { }
    }
    //public class ShiftForBedRepository : GenericRepository<ShiftForBed>, IShiftForBedRepository
    //{
    //    public ShiftForBedRepository(DbContext context) : base(context) { }
    //}
    public class ShiftRepository : GenericRepository<Shift>, IShiftRepository
    {
        public ShiftRepository(DbContext context) : base(context) { }
    }
    public class ShiftDetailRepository : GenericRepository<ShiftDetail>, IShiftDetailRepository
    {
        public ShiftDetailRepository(DbContext context) : base(context) { }
    }
    public class OrganizationRepository : GenericRepository<Organization>, IOrganizationRepository
    {
        public OrganizationRepository(DbContext context) : base(context) { }
    }
    public class LocationRepository : GenericRepository<Location>, ILocationRepository
    {
        public LocationRepository(DbContext context) : base(context) { }
    }
    public class DepartmentRepository : GenericRepository<Department>, IDepartmentRepository
    {
        public DepartmentRepository(DbContext context) : base(context) { }
    }
    public class SpecialismRepository : GenericRepository<Specialism>, ISpecialismRepository
    {
        public SpecialismRepository(DbContext context) : base(context) { }
    }
    public class PagerRepository : GenericRepository<Pager>, IPagerRepository
    {
        public PagerRepository(DbContext context) : base(context) { }
    }
    public class CycleRepository : GenericRepository<Cycle>, ICycleRepository
    {
        public CycleRepository(DbContext context) : base(context) { }
    }
    public class CodeRepository : GenericRepository<Code>, ICodeRepository
    {
        public CodeRepository(DbContext context) : base(context) { }
    }
    public class CodeHistoryRepository : GenericRepository<CodeHistory>, ICodeHistoryRepository
    {
        public CodeHistoryRepository(DbContext context) : base(context) { }
    }
    public class SettingRepository : GenericRepository<Setting>, ISettingRepository
    {
        public SettingRepository(DbContext context) : base(context) { }
    }
    public class LoggingRepository : GenericRepository<Logging>, ILoggingRepository
    {
        public LoggingRepository(DbContext context) : base(context) { }
    }
    public class IconRepository : GenericRepository<Icon>, IIconRepository
    {
        public IconRepository(DbContext context) : base(context) { }
    }
}