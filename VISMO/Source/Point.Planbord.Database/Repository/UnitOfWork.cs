﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using Point.Planbord.Database.Models;
using Point.Planbord.Database.Context;
using System.Data.Entity;


namespace Point.Planbord.Database.Repository
{

    public interface IUnitOfWork
    {
        IEmployeeRepository EmployeeRepository { get; }
        IPatientRepository PatientRepository { get; }
        INurseRepository NurseRepository { get; }
        IBedRepository BedRepository { get; }
        IBedForPatientRepository BedForPatientRepository { get; }
        IShiftRepository ShiftRepository { get; }
        IShiftDetailRepository ShiftDetailRepository { get; }
        IOrganizationRepository OrganizationRepository { get; }
        ILocationRepository LocationRepository { get; }
        IDepartmentRepository DepartmentRepository { get; }
        ISpecialismRepository SpecialismRepository { get; }
        IPagerRepository PagerRepository { get; }
        ICycleRepository CycleRepository { get; }
        ICodeRepository CodeRepository { get; }
        ICodeHistoryRepository CodeHistoryRepository { get; }
        ISettingRepository SettingRepository { get; }
        ILoggingRepository LoggingRepository { get; }
        IIconRepository IconRepository { get; }

    //   DbContext DbContext { get; }

        void Save();
    }

    public class UnitOfWork<TDbContext> : IUnitOfWork,  IDisposable where TDbContext : DbContext, new()
    {
        public UnitOfWork()
        {
            DbContext = new TDbContext();
        }

        public TDbContext DbContext { get; private set; }

        

        #region --- REPOSITORIES-IMPLEMENTATION ---


        private ILoginHistoryRepository loginhistoryRepository;
        public ILoginHistoryRepository LoginHistoryRepository
        {
            get
            {
                if (this.loginhistoryRepository == null)
                {
                    this.loginhistoryRepository = new LoginHistoryRepository(DbContext);
                }
                return this.loginhistoryRepository;
            }
        }


        private IEmployeeRepository employeeRepository;
        public IEmployeeRepository EmployeeRepository
        {
            get
            {
                if (this.employeeRepository == null)
                {
                    this.employeeRepository = new EmployeeRepository(DbContext);
                }
                return this.employeeRepository;
            }
        }


        private IPatientRepository patientRepository;
        public IPatientRepository PatientRepository
        {
            get
            {
                if (this.patientRepository == null)
                {
                    this.patientRepository = new PatientRepository(DbContext);
                }
                return this.patientRepository;
            }
        }


        private INurseRepository nurseRepository;
        public INurseRepository NurseRepository
        {
            get
            {
                if (this.nurseRepository == null)
                {
                    this.nurseRepository = new NurseRepository(DbContext);
                }
                return this.nurseRepository;
            }
        }


        private IBedRepository bedRepository;
        public IBedRepository BedRepository
        {
            get
            {
                if (this.bedRepository == null)
                {
                    this.bedRepository = new BedRepository(DbContext);
                }
                return this.bedRepository;
            }
        }


        private IBedForPatientRepository bedforpatientRepository;
        public IBedForPatientRepository BedForPatientRepository
        {
            get
            {
                if (this.bedforpatientRepository == null)
                {
                    this.bedforpatientRepository = new BedForPatientRepository(DbContext);
                }
                return this.bedforpatientRepository;
            }
        }


        //private IShiftForBedRepository shiftforbedRepository;
        //public IShiftForBedRepository ShiftForBedRepository
        //{
        //    get
        //    {
        //        if (this.shiftforbedRepository == null)
        //        {
        //            this.shiftforbedRepository = new ShiftForBedRepository(DbContext);
        //        }
        //        return this.shiftforbedRepository;
        //    }
        //}


        private IShiftRepository shiftRepository;
        public IShiftRepository ShiftRepository
        {
            get
            {
                if (this.shiftRepository == null)
                {
                    this.shiftRepository = new ShiftRepository(DbContext);
                }
                return this.shiftRepository;
            }
        }


        private IShiftDetailRepository shiftdetailRepository;
        public IShiftDetailRepository ShiftDetailRepository
        {
            get
            {
                if (this.shiftdetailRepository == null)
                {
                    this.shiftdetailRepository = new ShiftDetailRepository(DbContext);
                }
                return this.shiftdetailRepository;
            }
        }


        private IOrganizationRepository organizationRepository;
        public IOrganizationRepository OrganizationRepository
        {
            get
            {
                if (this.organizationRepository == null)
                {
                    this.organizationRepository = new OrganizationRepository(DbContext);
                }
                return this.organizationRepository;
            }
        }


        private ILocationRepository locationRepository;
        public ILocationRepository LocationRepository
        {
            get
            {
                if (this.locationRepository == null)
                {
                    this.locationRepository = new LocationRepository(DbContext);
                }
                return this.locationRepository;
            }
        }


        private IDepartmentRepository departmentRepository;
        public IDepartmentRepository DepartmentRepository
        {
            get
            {
                if (this.departmentRepository == null)
                {
                    this.departmentRepository = new DepartmentRepository(DbContext);
                }
                return this.departmentRepository;
            }
        }


        private ISpecialismRepository specialismRepository;
        public ISpecialismRepository SpecialismRepository
        {
            get
            {
                if (this.specialismRepository == null)
                {
                    this.specialismRepository = new SpecialismRepository(DbContext);
                }
                return this.specialismRepository;
            }
        }


        private IPagerRepository pagerRepository;
        public IPagerRepository PagerRepository
        {
            get
            {
                if (this.pagerRepository == null)
                {
                    this.pagerRepository = new PagerRepository(DbContext);
                }
                return this.pagerRepository;
            }
        }


        private ICycleRepository cycleRepository;
        public ICycleRepository CycleRepository
        {
            get
            {
                if (this.cycleRepository == null)
                {
                    this.cycleRepository = new CycleRepository(DbContext);
                }
                return this.cycleRepository;
            }
        }


        private ICodeRepository codeRepository;
        public ICodeRepository CodeRepository
        {
            get
            {
                if (this.codeRepository == null)
                {
                    this.codeRepository = new CodeRepository(DbContext);
                }
                return this.codeRepository;
            }
        }

        private ICodeHistoryRepository codeHistoryRepository;
        public ICodeHistoryRepository CodeHistoryRepository
        {
            get
            {
                if (this.codeHistoryRepository == null)
                {
                    this.codeHistoryRepository = new CodeHistoryRepository(DbContext);
                }
                return this.codeHistoryRepository;
            }
        }


        private ISettingRepository settingRepository;
        public ISettingRepository SettingRepository
        {
            get
            {
                if (this.settingRepository == null)
                {
                    this.settingRepository = new SettingRepository(DbContext);
                }
                return this.settingRepository;
            }
        }

        private ILoggingRepository loggingRepository;
        public ILoggingRepository LoggingRepository
        {
            get
            {
                if (this.loggingRepository == null)
                {
                    this.loggingRepository = new LoggingRepository(DbContext);
                }
                return this.loggingRepository;
            }
        }

        private IIconRepository iconRepository;
        public IIconRepository IconRepository
        {
            get
            {
                if (this.iconRepository == null)
                {
                    this.iconRepository = new IconRepository(DbContext);
                }
                return this.iconRepository;
            }
        }

        
        //private ITestRepository testRepository;
        //public ITestRepository TestRepository
        //{
        //    get
        //    {
        //        if (this.testRepository == null)
        //        {
        //            this.testRepository = new TestRepository(DbContext);//DbContext);
        //        }
        //        return this.testRepository;
        //    }
        //}





        #endregion

        public void Save()
        {
            DbContext.SaveChanges();
        }


        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    DbContext.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }




}
