﻿using Newtonsoft.Json;
using Point.Planbord.Database.Models;
using Point.Planbord.Infrastructure.Extensions;
using System.Collections.Generic;
using System.Linq;

namespace Point.Planbord.Database
{
    public class UserSettings
    {
        private readonly IList<Setting> settings;

        public UserSettings(IList<Setting> settings)
        {
            this.settings = settings;
        }

        public int GetAutomaticCloseSeconds()
        {
            var setting = settings.Where(s => s.SettingMainType == SettingMainType.AutomaticClose).FirstOrDefault();
            return new SettingsAutomaticClose()
            {
                Values = JsonConvert.DeserializeObject<AutomaticCloseValues>(setting.SettingValue)
            }.Values.Seconds;
        }

        public int GetAutomaticRefreshSeconds()
        {
            var setting = settings.Where(s => s.SettingMainType == SettingMainType.AutomaticRefresh).FirstOrDefault();
            return new SettingsAutomaticRefresh()
            {
                Values = JsonConvert.DeserializeObject<AutomaticRefreshValues>(setting.SettingValue)
            }.Values.Seconds;
        }

        public DiagnosisDisplayCodeType GetDiagnoseDisplayCodeType()
        {
            var setting = settings.Where(s => s.SettingMainType == SettingMainType.DiagnosisCode).FirstOrDefault();
            return new SettingsDiagnosisCode()
            {
                Values = JsonConvert.DeserializeObject<DiagnosisCodeValues>(setting.SettingValue)
            }.Values.DisplayCodeType;
        }

        public HospitalizationDisplayCodeType GetHospitalizationDisplayCodeType()
        {
            var setting = settings.Where(s => s.SettingMainType == SettingMainType.HospitalizationCode).FirstOrDefault();
            return new SettingsHospitalization()
            {
                Values = JsonConvert.DeserializeObject<HospitalizationValues>(setting.SettingValue)
            }.Values.HospitalizationDisplayCodeType; 
        }

        public ColumnDisplayType GetColumnDisplayType(PlanbordColumn column)
        {
            var setting = settings.Where(s => s.SettingMainType == SettingMainType.ColumnDisplayType).FirstOrDefault();
            SettingColumnDisplayType columnSettings = new SettingColumnDisplayType()
            {
                Values = JsonConvert.DeserializeObject<IList<SettingColumnDisplayTypeValues>>(setting.SettingValue)
            };
            return columnSettings.Values.Where(c => c.Column == column).Select(c => c.DisplayType).FirstOrNew<ColumnDisplayType>();
        }

        public IList<SettingColumnDisplayTypeValues> GetColumnDisplayTypes()
        {
            var setting = settings.Where(s => s.SettingMainType == SettingMainType.ColumnDisplayType).FirstOrDefault();
            SettingColumnDisplayType columnSettings = new SettingColumnDisplayType()
            {
                Values = JsonConvert.DeserializeObject<IList<SettingColumnDisplayTypeValues>>(setting.SettingValue)
            };
            return columnSettings.Values;
        }

        public IList<SettingsIconSetIcons> GetDetailIcons()
        {
            var setting = settings.Where(s => s.SettingMainType == SettingMainType.Icons).FirstOrDefault();
            SettingsIconSet iconSettings = new SettingsIconSet()
            {
                Values = JsonConvert.DeserializeObject<SettingsIconSetValues>(setting.SettingValue)
            };
            return iconSettings.Values.Icons;
        }

        public SettingsCodeValues GetCodeCycles()
        {
            var setting = settings.Where(s => s.SettingMainType == SettingMainType.Codes).FirstOrDefault();
            SettingsCode codeSettings = new SettingsCode()
            {
                Values = JsonConvert.DeserializeObject<SettingsCodeValues>(setting.SettingValue)
            };
            return codeSettings.Values;
        }

        public IList<SettingsSpecialismValues> GetSpecialismColors()
        {
            var setting = settings.Where(s => s.SettingMainType == SettingMainType.SpecialismColor).FirstOrDefault();
            SettingsSpecialism specialismSettings = new SettingsSpecialism()
            {
                Values = JsonConvert.DeserializeObject<IList<SettingsSpecialismValues>>(setting.SettingValue)
            };
            return specialismSettings.Values;
        }

        public SettingsHeaderMessage GetSettingsHeaderMessage()
        {
            var setting = settings.Where(s => s.SettingMainType == SettingMainType.HeaderMessage).FirstOrDefault();
            SettingsHeaderMessage headerMessageSettings = new SettingsHeaderMessage()
            {
                Id = setting.SettingID,
                SettingMainType = setting.SettingMainType,
                Values = JsonConvert.DeserializeObject<SettingsHeaderMessageValues>(setting.SettingValue)
            };
            return headerMessageSettings;
        }
    }
}