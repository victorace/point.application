// <auto-generated />
namespace Point.Planbord.WebApplication.Migrations
{
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    public sealed partial class v1_2_0_1_BedForPatientSpecialism : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(v1_2_0_1_BedForPatientSpecialism));
        
        string IMigrationMetadata.Id
        {
            get { return "201412020923473_v1_2_0_1_BedForPatientSpecialism"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
