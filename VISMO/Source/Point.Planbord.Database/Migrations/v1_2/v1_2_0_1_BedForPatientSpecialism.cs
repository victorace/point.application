namespace Point.Planbord.WebApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class v1_2_0_1_BedForPatientSpecialism : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Bed", "Identifier", c => c.String(maxLength: 255));
            AddColumn("dbo.BedForPatient", "SpecialismID", c => c.Int());
            AddForeignKey("dbo.BedForPatient", "SpecialismID", "dbo.Specialism", "SpecialismID");
            CreateIndex("dbo.BedForPatient", "SpecialismID");
        }
        
        public override void Down()
        {
            DropIndex("dbo.BedForPatient", new[] { "SpecialismID" });
            DropForeignKey("dbo.BedForPatient", "SpecialismID", "dbo.Specialism");
            DropColumn("dbo.BedForPatient", "SpecialismID");
            DropColumn("dbo.Bed", "Identifier");
        }
    }
}
