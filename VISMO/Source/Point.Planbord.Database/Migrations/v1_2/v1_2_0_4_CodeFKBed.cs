namespace Point.Planbord.WebApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class v1_2_0_4_CodeFKBed : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Code", "BedID", c => c.Int());
            AddColumn("dbo.CodeHistory", "BedID", c => c.Int());
            AddForeignKey("dbo.Code", "BedID", "dbo.Bed", "BedID");
            AddForeignKey("dbo.CodeHistory", "BedID", "dbo.Bed", "BedID");
            CreateIndex("dbo.Code", "BedID");
            CreateIndex("dbo.CodeHistory", "BedID");
        }
        
        public override void Down()
        {
            DropIndex("dbo.CodeHistory", new[] { "BedID" });
            DropIndex("dbo.Code", new[] { "BedID" });
            DropForeignKey("dbo.CodeHistory", "BedID", "dbo.Bed");
            DropForeignKey("dbo.Code", "BedID", "dbo.Bed");
            DropColumn("dbo.CodeHistory", "BedID");
            DropColumn("dbo.Code", "BedID");
        }
    }
}
