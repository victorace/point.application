namespace Point.Planbord.WebApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class v1_2_0_2_CapacityAccountRole : DbMigration
    {
        public override void Up()
        {
            string sql = @"
set nocount on

declare @roles table ( Name varchar(100), ID uniqueidentifier )

-- Set the identifiers:
declare @applicationId uniqueidentifier = '{16a5dadf-8bd8-4423-ab03-8e4f8ae8afeb}'
declare @applicationName nvarchar(100) = 'Planbord'

insert into @roles values ( 'CapacityAccount' , '{FD3111EC-D04E-45B9-8355-C97C949BE27B}')

if not exists ( select 1 from dbo.Applications where ApplicationName = @applicationName )
begin
    insert into dbo.Applications 
           ( ApplicationId, ApplicationName, Description )
    values ( @applicationId, @applicationName, @applicationName )
end

insert into Roles 
select ID, @applicationId, Name, Name
  from @roles 
 where Name not in ( select RoleName 
                         from Roles
                        where ApplicationId = @applicationId )";

            Sql(sql);
        }
        
        public override void Down()
        {
            // a rollback isn't really necessary
        }
    }
}
