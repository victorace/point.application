namespace Point.Planbord.WebApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class v1_2_0_3_Icon : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Icon",
                c => new
                    {
                        IconID = c.Int(nullable: false, identity: true),
                        OrganizationID = c.Int(nullable: false),
                        LargeFileName = c.String(maxLength: 255),
                        SmallFileName = c.String(maxLength: 255),
                    })
                .PrimaryKey(t => t.IconID)
                .ForeignKey("dbo.Organization", t => t.OrganizationID, cascadeDelete: true)
                .Index(t => t.OrganizationID);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Icon", new[] { "OrganizationID" });
            DropForeignKey("dbo.Icon", "OrganizationID", "dbo.Organization");
            DropTable("dbo.Icon");
        }
    }
}
