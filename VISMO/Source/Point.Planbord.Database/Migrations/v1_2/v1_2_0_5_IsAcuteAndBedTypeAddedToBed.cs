namespace Point.Planbord.WebApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class v1_2_0_5_IsAcuteAndBedTypeAddedToBed : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Bed", "IsAcute", c => c.Boolean(nullable: false));
            AddColumn("dbo.Bed", "BedType", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Bed", "BedType");
            DropColumn("dbo.Bed", "IsAcute");
        }
    }
}
