namespace Point.Planbord.WebApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class v1_2_0_6_SettingValueHospitalization : DbMigration
    {
        public override void Up()
        {
            string sql = @"

if (not exists (select 1 from Setting where OrganizationID is null and LocationID is null and DepartmentID is null and SettingMainType = 9) )
    insert into Setting ([OrganizationID],[LocationID],[DepartmentID],[SettingMainType],[SettingType],[SettingValue])
           values (NULL,NULL,NULL,9,NULL,'{""HospitalizationDisplayCodeType"":1}')
";
            Sql(sql);

        }
        
        public override void Down()
        {
        }
    }
}
