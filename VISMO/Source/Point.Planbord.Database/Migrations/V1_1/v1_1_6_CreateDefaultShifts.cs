namespace Point.Planbord.WebApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class v1_1_6_CreateDefaultShifts : DbMigration
    {
        public override void Up()
        {
            string createShifts = @"
DECLARE @Description VARCHAR(255) = 'Ochtenddienst (06:00-12:00)'
DECLARE @WeekDay INT = -1
DECLARE @StartTime INT = 360
DECLARE @Duration INT = 360

IF NOT EXISTS(SELECT * FROM Shift WHERE 
	Description = @Description AND WeekDay = @WeekDay AND StartTime = @StartTime AND Duration = @Duration)
BEGIN

	INSERT INTO Shift (Description, WeekDay, StartTime, Duration)
		VALUES (@Description, @WeekDay, @StartTime, @Duration)

END

SET @Description = 'Middagdienst (09:00-18:00)'
SET @WeekDay = -1
SET @StartTime = 720
SET @Duration = 540
IF NOT EXISTS(SELECT * FROM Shift WHERE 
	Description = @Description AND WeekDay = @WeekDay AND StartTime = @StartTime AND Duration = @Duration)
BEGIN

	INSERT INTO Shift (Description, WeekDay, StartTime, Duration)
		VALUES (@Description, @WeekDay, @StartTime, @Duration)

END

SET @Description = 'Nachtdienst (18:00-06:00)'
SET @WeekDay = -1
SET @StartTime = 1080
SET @Duration = 720
IF NOT EXISTS(SELECT * FROM Shift WHERE 
	Description = @Description AND WeekDay = @WeekDay AND StartTime = @StartTime AND Duration = @Duration)
BEGIN

	INSERT INTO Shift (Description, WeekDay, StartTime, Duration)
		VALUES (@Description, @WeekDay, @StartTime, @Duration)

END";

            Sql(createShifts);
        }
        
        public override void Down()
        {
        }
    }
}
