namespace Point.Planbord.WebApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class v1_1_4_Workload : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ShiftDetail", "Workload", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ShiftDetail", "Workload");
        }
    }
}
