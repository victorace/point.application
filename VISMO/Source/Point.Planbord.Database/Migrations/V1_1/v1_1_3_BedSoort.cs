namespace Point.Planbord.WebApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class v1_1_3_BedSoort : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Bed", "BedSoort", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Bed", "BedSoort");
        }
    }
}
