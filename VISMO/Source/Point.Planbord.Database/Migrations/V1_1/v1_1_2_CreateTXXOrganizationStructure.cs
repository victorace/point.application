namespace Point.Planbord.WebApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class v1_1_2_CreateTXXOrganizationStructure : DbMigration
    {
        public override void Up()
        {
            string addOrganizationStructure = @"
set nocount on

IF NOT EXISTS(SELECT 1 FROM Organization where SystemCode = 'TXX')
BEGIN
    insert into Organization values ('TXX','TXX','Techxx','',0)
END

IF NOT EXISTS(SELECT 1 FROM Location where SystemCode = 'TXX')
BEGIN
    insert into Location 
    select OrganizationID, 'TXX','TXX','Techxx','','','','',0,'',0
    from Organization where SystemCode = 'TXX'
END

IF NOT EXISTS(SELECT 1 FROM Department where SystemCode = 'TXX')
BEGIN
    insert into Department 
    select LocationID, 'TXX','TXX','Techxx','','',0
    from Location where SystemCode = 'TXX'
END

IF NOT EXISTS(SELECT 1 FROM Employee where UserID = (select UserID from Users where UserName = 'TechxxAdmin') )
BEGIN
    insert into Employee 
    select (select UserID from Users where UserName = 'TechxxAdmin'), DepartmentID, '0','','','','TechxxAdmin','',0,0
    from Department where SystemCode = 'TXX'
END

";
            Sql(addOrganizationStructure);

        }
        
        public override void Down()
        {
            string deleteOrganizationStructure = @"

delete from Employee where UserId = (select UserID from Users where UserName = 'TechxxAdmin')

delete from Department where SystemCode = 'TXX'

delete from Location where SystemCode = 'TXX'

delete from Organization where SystemCode = 'TXX'

";
            Sql(deleteOrganizationStructure);

        }
    }
}
