namespace Point.Planbord.WebApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class v1_1_7_ShiftDepartmentSelectConfirmation : DbMigration
    {
        public override void Up()
        {
            string modifyShiftDescriptions = @"
UPDATE Shift SET Description = 'Dagdienst' WHERE Description = 'Ochtenddienst (06:00-12:00)'
UPDATE Shift SET Description = 'Latedienst' WHERE Description = 'Middagdienst (09:00-18:00)'
UPDATE Shift SET Description = 'Nachtdienst' WHERE Description = 'Nachtdienst (18:00-06:00)'";

            Sql(modifyShiftDescriptions);
        }
        
        public override void Down()
        {
        }
    }
}
