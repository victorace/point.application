// <auto-generated />
namespace Point.Planbord.WebApplication.Migrations
{
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    public sealed partial class v1_1_0_FullNameAndLogging : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(v1_1_0_FullNameAndLogging));
        
        string IMigrationMetadata.Id
        {
            get { return "201310300954026_v1_1_0_FullNameAndLogging"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
