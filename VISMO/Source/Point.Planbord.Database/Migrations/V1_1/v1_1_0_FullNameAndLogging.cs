namespace Point.Planbord.WebApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class v1_1_0_FullNameAndLogging : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Logging",
                c => new
                {
                    LoggingID = c.Int(nullable: false, identity: true),
                    LoggingDateTime = c.DateTime(nullable: false),
                    Severity = c.Int(nullable: false),
                    Description = c.String(),
                    ApplicationName = c.String(maxLength: 255),
                    ReferenceID = c.Int(),
                    ReferenceName = c.String(maxLength: 100),
                } )
                .PrimaryKey(t => t.LoggingID);

            //AddColumn("dbo.Patient", "FullName", c => c.String(maxLength: 255));
            string addColumn = @"

set nocount on

BEGIN TRANSACTION

CREATE TABLE dbo.Tmp_Patient
	(
	PatientID int NOT NULL IDENTITY (1, 1),
	PatientNumber nvarchar(50) NULL,
	CivilServiceNumber nvarchar(15) NULL,
	BirthDate datetime NULL,
	FullName varchar(255) NULL,
	Initials nvarchar(MAX) NULL,
	FirstName nvarchar(MAX) NULL,
	MiddleName nvarchar(MAX) NULL,
	LastName nvarchar(MAX) NULL,
	MaidenName nvarchar(MAX) NULL,
	Gender int NOT NULL,
	InActive bit NOT NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]

ALTER TABLE dbo.Tmp_Patient SET (LOCK_ESCALATION = TABLE)

SET IDENTITY_INSERT dbo.Tmp_Patient ON

IF EXISTS(SELECT * FROM dbo.Patient)
	 EXEC('INSERT INTO dbo.Tmp_Patient (PatientID, PatientNumber, CivilServiceNumber, BirthDate, Initials, FirstName, MiddleName, LastName, MaidenName, Gender, InActive)
		SELECT PatientID, PatientNumber, CivilServiceNumber, BirthDate, Initials, FirstName, MiddleName, LastName, MaidenName, Gender, InActive FROM dbo.Patient WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_Patient OFF

ALTER TABLE dbo.BedForPatient
	DROP CONSTRAINT [FK_dbo.BedForPatient_dbo.Patient_PatientID]

ALTER TABLE dbo.Code
	DROP CONSTRAINT [FK_dbo.Code_dbo.Patient_PatientID]

ALTER TABLE dbo.CodeHistory
	DROP CONSTRAINT [FK_dbo.CodeHistory_dbo.Patient_PatientID]

DROP TABLE dbo.Patient

EXECUTE sp_rename N'dbo.Tmp_Patient', N'Patient', 'OBJECT' 

ALTER TABLE dbo.Patient ADD CONSTRAINT
	[PK_dbo.Patient] PRIMARY KEY CLUSTERED 
	(
	PatientID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

COMMIT

BEGIN TRANSACTION

ALTER TABLE dbo.CodeHistory ADD CONSTRAINT
	[FK_dbo.CodeHistory_dbo.Patient_PatientID] FOREIGN KEY
	(
	PatientID
	) REFERENCES dbo.Patient
	(
	PatientID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
ALTER TABLE dbo.CodeHistory SET (LOCK_ESCALATION = TABLE)

COMMIT

BEGIN TRANSACTION

ALTER TABLE dbo.Code ADD CONSTRAINT
	[FK_dbo.Code_dbo.Patient_PatientID] FOREIGN KEY
	(
	PatientID
	) REFERENCES dbo.Patient
	(
	PatientID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
ALTER TABLE dbo.Code SET (LOCK_ESCALATION = TABLE)

COMMIT

BEGIN TRANSACTION

ALTER TABLE dbo.BedForPatient ADD CONSTRAINT
	[FK_dbo.BedForPatient_dbo.Patient_PatientID] FOREIGN KEY
	(
	PatientID
	) REFERENCES dbo.Patient
	(
	PatientID
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
	
ALTER TABLE dbo.BedForPatient SET (LOCK_ESCALATION = TABLE)

COMMIT";
            Sql(addColumn);
        }
        

        public override void Down()
        {
            DropColumn("dbo.Patient", "FullName");
            DropTable("dbo.Logging");
        }
    }
}
