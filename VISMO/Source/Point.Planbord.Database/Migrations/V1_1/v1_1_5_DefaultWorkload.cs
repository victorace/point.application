namespace Point.Planbord.WebApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class v1_1_5_DefaultWorkload : DbMigration
    {
        public override void Up()
        {
            string dropDefaultWorkload = @"
IF EXISTS(SELECT * FROM sys.columns WHERE Name = 'Workload' AND Object_ID = object_id('dbo.ShiftDetail'))
BEGIN
	UPDATE ShiftDetail SET Workload = 1 WHERE Workload = 0
END

DECLARE @con nvarchar(128)
SELECT @con = name FROM sys.default_constraints WHERE parent_object_id = object_id('dbo.ShiftDetail') AND col_name(parent_object_id, parent_column_id) = 'Workload';
IF @con IS NOT NULL
BEGIN
    EXECUTE('ALTER TABLE [dbo].[ShiftDetail] DROP CONSTRAINT ' + @con)
END";

            Sql(dropDefaultWorkload);
            
            AlterColumn("dbo.ShiftDetail", "Workload", c => c.Int(nullable: false, defaultValue: 1));
        }
        
        public override void Down()
        {
            string dropDefaultWorkload = @"
DECLARE @con nvarchar(128)
SELECT @con = name FROM sys.default_constraints WHERE parent_object_id = object_id('dbo.ShiftDetail') AND col_name(parent_object_id, parent_column_id) = 'Workload';
IF @con IS NOT NULL
BEGIN
    EXECUTE('ALTER TABLE [dbo].[ShiftDetail] DROP CONSTRAINT ' + @con)
END";

            Sql(dropDefaultWorkload);
            AlterColumn("dbo.ShiftDetail", "Workload", c => c.Int(nullable: false, defaultValue: 0));
        }
    }
}
