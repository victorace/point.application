namespace Point.Planbord.WebApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class v1_1_9_TMOAndTMBDetailIcons : DbMigration
    {
        public override void Up()
        {
            string addTMOAndTMBDetailIcons = @"
UPDATE Setting SET SettingValue = '{""DetailIconSet"": 1, ""Icons"": [{""Description"": ""Rooming in"", ""DetailIconType"": 0, ""Name"": ""Rooming in"", ""Visible"": false }, {""Description"": ""Thuiszorg is geregeld"", ""DetailIconType"": 1, ""Name"": ""Thuiszorg"", ""Visible"": true }, {""Description"": ""Apotheek is bij patient langs geweest"", ""DetailIconType"": 2, ""Name"": ""Apotheek"", ""Visible"": true }, {""Description"": ""Dietiste is bij patient langs geweest"", ""DetailIconType"": 3, ""Name"": ""Dietiste"", ""Visible"": true }, {""Description"": ""Hartrevalidatie"", ""DetailIconType"": 4, ""Name"": ""Hartrevalidatie"", ""Visible"": false }, {""Description"": ""Verwardheid/Delier"", ""DetailIconType"": 5, ""Name"": ""Verwardheid"", ""Visible"": true }, {""Description"": ""Patient is met ontslag, het bed moet gereinigd worden"", ""DetailIconType"": 6, ""Name"": ""Reinigen"", ""Visible"": true }, {""Description"": ""Medium risk"", ""DetailIconType"": 7, ""Name"": ""Medium risk"", ""Visible"": true }, {""Description"": ""Inleiding"", ""DetailIconType"": 8, ""Name"": ""Inleiding"", ""Visible"": true }, {""Description"": ""Diabetes"", ""DetailIconType"": 9, ""Name"": ""Diabetes"", ""Visible"": true }, {""Description"": ""Fysiotherapie"", ""DetailIconType"": 10, ""Name"": ""Fysiotherapie"", ""Visible"": true }, {""Description"": ""Hartvalenverpleegkundige"", ""DetailIconType"": 11, ""Name"": ""Hartvalenvpk"", ""Visible"": true }, {""Description"": ""Telemetrie"", ""DetailIconType"": 12, ""Name"": ""Telemetrie"", ""Visible"": true }, {""Description"": ""Poliklinisch"", ""DetailIconType"": 13, ""Name"": ""Poliklinisch"", ""Visible"": true }, {""Description"": ""Opname"", ""DetailIconType"": 14, ""Name"": ""Opname"", ""Visible"": false }, {""Description"": ""Dagbehandeling"", ""DetailIconType"": 15, ""Name"": ""Dagbeh."", ""Visible"": false }, {""Description"": ""Kinderarts"", ""DetailIconType"": 16, ""Name"": ""Kinderarts"", ""Visible"": false }, {""Description"": ""Neonatologie"", ""DetailIconType"": 17, ""Name"": ""Neonatologie"", ""Visible"": false }, {""Description"": ""Gezonde moeder"", ""DetailIconType"": 18, ""Name"": ""Gez.moeder"", ""Visible"": false }, {""Description"": ""Kraamvrouw"", ""DetailIconType"": 19, ""Name"": ""Kraamvrouw"", ""Visible"": false }, {""Description"": ""Nuchter"", ""DetailIconType"": 20, ""Name"": ""Nuchter"", ""Visible"": false }, {""Description"": ""Pedagogisch medewerker"", ""DetailIconType"": 21, ""Name"": ""Pedagogie"", ""Visible"": false }, {""Description"": ""Zuurstof"", ""DetailIconType"": 22, ""Name"": ""Zuurstof"", ""Visible"": false }, {""Description"": ""Volledige ADL zorg nodig"", ""DetailIconType"": 23, ""Name"": ""ADL zorg"", ""Visible"": false }, {""Description"": ""Hulp nodig bij ADL"", ""DetailIconType"": 24, ""Name"": ""ADL hulp"", ""Visible"": false }, {""Description"": ""Valrisico"", ""DetailIconType"": 25, ""Name"": ""Valrisico"", ""Visible"": false }, {""Description"": ""Telemetrie Onbewaakt"", ""DetailIconType"": 26, ""Name"": ""Telem. Onb."", ""Visible"": false }, {""Description"": ""Telemetrie Bewaakt"", ""DetailIconType"": 27, ""Name"": ""Telem. Bew."", ""Visible"": false } ] }'
WHERE SettingMainType = 4 AND OrganizationID IS NULL AND LocationID IS NULL AND DepartmentID IS NULL";

            Sql(addTMOAndTMBDetailIcons);
        }
        
        public override void Down()
        {
        }
    }
}
