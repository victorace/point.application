namespace Point.Planbord.WebApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class v1_1_1_BedForPatientOptionalPatient : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.BedForPatient", "PatientID", "dbo.Patient");
            DropIndex("dbo.BedForPatient", new[] { "PatientID" });
            AlterColumn("dbo.BedForPatient", "PatientID", c => c.Int());
            AddForeignKey("dbo.BedForPatient", "PatientID", "dbo.Patient", "PatientID");
            CreateIndex("dbo.BedForPatient", "PatientID");
        }
        
        public override void Down()
        {
            DropIndex("dbo.BedForPatient", new[] { "PatientID" });
            DropForeignKey("dbo.BedForPatient", "PatientID", "dbo.Patient");
            AlterColumn("dbo.BedForPatient", "PatientID", c => c.Int(nullable: false));
            CreateIndex("dbo.BedForPatient", "PatientID");
            AddForeignKey("dbo.BedForPatient", "PatientID", "dbo.Patient", "PatientID", cascadeDelete: true);
        }
    }
}
