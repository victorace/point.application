namespace Point.Planbord.WebApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class v1_0_8_ShiftDetailRecreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ShiftDetail",
                c => new
                    {
                        ShiftDetailID = c.Int(nullable: false, identity: true),
                        BedID = c.Int(nullable: false),
                        NurseID = c.Int(),
                        ShiftID = c.Int(nullable: false),
                        PagerID = c.Int(),
                        Pause = c.Int(),
                        Visit = c.Int(),
                        ShiftDetailRole = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ShiftDetailID)
                .ForeignKey("dbo.Bed", t => t.BedID, cascadeDelete: true)
                .ForeignKey("dbo.Nurse", t => t.NurseID)
                .ForeignKey("dbo.Shift", t => t.ShiftID, cascadeDelete: true)
                .ForeignKey("dbo.Pager", t => t.PagerID)
                .Index(t => t.BedID)
                .Index(t => t.NurseID)
                .Index(t => t.ShiftID)
                .Index(t => t.PagerID);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.ShiftDetail", new[] { "PagerID" });
            DropIndex("dbo.ShiftDetail", new[] { "ShiftID" });
            DropIndex("dbo.ShiftDetail", new[] { "NurseID" });
            DropIndex("dbo.ShiftDetail", new[] { "BedID" });
            DropForeignKey("dbo.ShiftDetail", "PagerID", "dbo.Pager");
            DropForeignKey("dbo.ShiftDetail", "ShiftID", "dbo.Shift");
            DropForeignKey("dbo.ShiftDetail", "NurseID", "dbo.Nurse");
            DropForeignKey("dbo.ShiftDetail", "BedID", "dbo.Bed");
            DropTable("dbo.ShiftDetail");
        }
    }
}
