namespace Point.Planbord.WebApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class v1_0_7_ShiftForBedDrop : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ShiftForBed", "BedID", "dbo.Bed");
            DropForeignKey("dbo.ShiftForBed", "ShiftDetailID", "dbo.ShiftDetail");
            DropForeignKey("dbo.ShiftDetail", "ShiftID", "dbo.Shift");
            DropForeignKey("dbo.ShiftDetail", "NurseID", "dbo.Nurse");
            DropForeignKey("dbo.ShiftDetail", "PagerID", "dbo.Pager");
            DropIndex("dbo.ShiftForBed", new[] { "BedID" });
            DropIndex("dbo.ShiftForBed", new[] { "ShiftDetailID" });
            DropIndex("dbo.ShiftDetail", new[] { "ShiftID" });
            DropIndex("dbo.ShiftDetail", new[] { "NurseID" });
            DropIndex("dbo.ShiftDetail", new[] { "PagerID" });
            DropTable("dbo.ShiftForBed");
            DropTable("dbo.ShiftDetail");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ShiftDetail",
                c => new
                    {
                        ShiftDetailID = c.Int(nullable: false, identity: true),
                        ShiftID = c.Int(nullable: false),
                        NurseID = c.Int(),
                        PagerID = c.Int(),
                        Pause = c.Int(),
                        Visit = c.Int(),
                        ShiftDetailRole = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ShiftDetailID);
            
            CreateTable(
                "dbo.ShiftForBed",
                c => new
                    {
                        ShiftForBedID = c.Int(nullable: false, identity: true),
                        BedID = c.Int(nullable: false),
                        ShiftDetailID = c.Int(),
                        ShiftOrder = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ShiftForBedID);
            
            CreateIndex("dbo.ShiftDetail", "PagerID");
            CreateIndex("dbo.ShiftDetail", "NurseID");
            CreateIndex("dbo.ShiftDetail", "ShiftID");
            CreateIndex("dbo.ShiftForBed", "ShiftDetailID");
            CreateIndex("dbo.ShiftForBed", "BedID");
            AddForeignKey("dbo.ShiftDetail", "PagerID", "dbo.Pager", "PagerID");
            AddForeignKey("dbo.ShiftDetail", "NurseID", "dbo.Nurse", "NurseID");
            AddForeignKey("dbo.ShiftDetail", "ShiftID", "dbo.Shift", "ShiftID", cascadeDelete: true);
            AddForeignKey("dbo.ShiftForBed", "ShiftDetailID", "dbo.ShiftDetail", "ShiftDetailID");
            AddForeignKey("dbo.ShiftForBed", "BedID", "dbo.Bed", "BedID", cascadeDelete: true);
        }
    }
}
