namespace Point.Planbord.WebApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class v1_0_5_SettingsRecreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Setting",
                c => new
                    {
                        SettingID = c.Int(nullable: false, identity: true),
                        OrganizationID = c.Int(),
                        LocationID = c.Int(),
                        DepartmentID = c.Int(),
                        SettingMainType = c.Int(nullable: false),
                        SettingType = c.String(maxLength: 255),
                        SettingValue = c.String(),
                    })
                .PrimaryKey(t => t.SettingID)
                .ForeignKey("dbo.Organization", t => t.OrganizationID)
                .ForeignKey("dbo.Location", t => t.LocationID)
                .ForeignKey("dbo.Department", t => t.DepartmentID)
                .Index(t => t.OrganizationID)
                .Index(t => t.LocationID)
                .Index(t => t.DepartmentID);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Setting", new[] { "DepartmentID" });
            DropIndex("dbo.Setting", new[] { "LocationID" });
            DropIndex("dbo.Setting", new[] { "OrganizationID" });
            DropForeignKey("dbo.Setting", "DepartmentID", "dbo.Department");
            DropForeignKey("dbo.Setting", "LocationID", "dbo.Location");
            DropForeignKey("dbo.Setting", "OrganizationID", "dbo.Organization");
            DropTable("dbo.Setting");
        }
    }
}
