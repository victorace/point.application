// <auto-generated />
namespace Point.Planbord.WebApplication.Migrations
{
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    public sealed partial class v1_0_7_ShiftForBedDrop : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(v1_0_7_ShiftForBedDrop));
        
        string IMigrationMetadata.Id
        {
            get { return "201310200821156_v1_0_7_ShiftForBedDrop"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
