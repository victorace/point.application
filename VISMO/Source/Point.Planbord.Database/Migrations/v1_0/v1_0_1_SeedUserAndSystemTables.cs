namespace Point.Planbord.WebApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class v1_0_1_SeedUserAndSystemTables : DbMigration
    {

        
        public override void Up()
        {
            string addUsersAndRoles = @"
set nocount on

declare @roles table ( Name varchar(100), ID uniqueidentifier )

-- Set the identifiers:
declare @applicationId uniqueidentifier = '{16a5dadf-8bd8-4423-ab03-8e4f8ae8afeb}'
declare @applicationName nvarchar(100) = 'Planbord'


insert into @roles values ( 'PlanbordAdministrator', '{CF5F629A-0AEF-2205-A46A-E9DD0862CD64}')
insert into @roles values ( 'OrganizationAdministrator' , '{50C2C2AC-2110-4823-A06C-E5E3DA33B57E}')
insert into @roles values ( 'PersonalAccount', '{6E4F148D-86AB-47F8-84C4-FD3C7A1B61C7}')
insert into @roles values ( 'DepartmentAccount' , '{AAADC115-E10A-4EB6-A0E5-BE496B18B8C2}')




if not exists ( select 1 from dbo.Applications where ApplicationName = @applicationName )
begin
    insert into dbo.Applications 
           ( ApplicationId, ApplicationName, Description )
    values ( @applicationId, @applicationName, @applicationName )
end

insert into Roles 
select ID, @applicationId, Name, Name
  from @roles 
 where Name not in ( select RoleName 
                         from Roles
                        where ApplicationId = @applicationId )


declare @username nvarchar(100) 
declare @rolename nvarchar(100)
declare @pass nvarchar(256) 
declare @salt nvarchar(256) 
declare @userid uniqueidentifier

set @username = 'TechxxAdmin'
set @rolename = 'PlanbordAdministrator'
set @pass = 'zj/soZpP7R+8RonjyUIkH+d8bPs=' -- Techxx!123
set @salt = 'aRe7wGMI7vmoe8ROx7k4gA=='
set @userid = NEWID()

if not exists ( select 1 from dbo.Users where UserName = @username and ApplicationID = @applicationId )
begin
    declare @now datetime = getutcdate() -- get the same timestamp in the several records  
    declare @mindate datetime = convert(datetime,'1754-01-01') -- membership uses 1754 

    insert into dbo.Users
           ( UserId, ApplicationId, UserName, IsAnonymous, LastActivityDate )
    values ( @userid, @applicationId, @username, 0, GETDATE())

    insert into dbo.UsersInRoles
           ( UserId, RoleId )
    values ( @userid, (select RoleId from Roles where RoleName = @rolename ) )
    
    insert into dbo.Memberships
           ( UserId,
             ApplicationId,
             Password,
             PasswordFormat,
             PasswordSalt,
             Email,
             PasswordQuestion,
             PasswordAnswer,
             IsApproved,
             IsLockedOut,
             CreateDate,
             LastLoginDate,
             LastPasswordChangedDate,
             LastLockoutDate,
             FailedPasswordAttemptCount,
             FailedPasswordAttemptWindowStart,
             FailedPasswordAnswerAttemptCount,
             FailedPasswordAnswerAttemptWindowsStart,
             Comment )
    values ( @userid,
             @applicationId,
             @pass,
             1,
             @salt,
             null,
             null,
             null,
             1,
             0,
             @now,
             @now,
             @now,
             @mindate,
             0,
             @mindate,
             0,
             @mindate,
             null)
                   
end";
            Sql(addUsersAndRoles);
        }
        
        public override void Down()
        {

            string deleteUsersAndRoles = @"
set nocount on

declare @username nvarchar(100)

set @username = 'TechxxAdmin';

delete from dbo.UsersInRoles where UserID in ( select UserID from dbo.Users where UserName = @username )

delete from dbo.Memberships where UserID in ( select UserID from dbo.Users where UserName = @username )

delete from dbo.Users where UserName = @username

delete from UsersInRoles where RoleID in ( select RoleID from dbo.Roles where RoleName in ( 'PlanbordAdministrator', 'OrganizationAdministrator', 'PersonalAccount', 'DepartmentAccount' ) )

delete from dbo.Roles where RoleName in ( 'PlanbordAdministrator', 'OrganizationAdministrator', 'PersonalAccount', 'DepartmentAccount' ) )

";

            Sql(deleteUsersAndRoles);

        }
    }
}
