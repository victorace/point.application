namespace Point.Planbord.WebApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class v1_0_2_MainModelSet : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.User", newName: "Users");
            CreateTable(
                "dbo.Employee",
                c => new
                    {
                        EmployeeID = c.Int(nullable: false, identity: true),
                        UserId = c.Guid(),
                        DepartmentID = c.Int(nullable: false),
                        EmployeeNumber = c.String(maxLength: 50),
                        Initials = c.String(),
                        FirstName = c.String(),
                        MiddleName = c.String(),
                        LastName = c.String(),
                        MaidenName = c.String(),
                        Gender = c.Int(nullable: false),
                        InActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.EmployeeID)
                .ForeignKey("dbo.Users", t => t.UserId)
                .ForeignKey("dbo.Department", t => t.DepartmentID, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.DepartmentID);
            
            CreateTable(
                "dbo.Department",
                c => new
                    {
                        DepartmentID = c.Int(nullable: false, identity: true),
                        LocationID = c.Int(nullable: false),
                        SystemCode = c.String(maxLength: 50),
                        ShortCode = c.String(maxLength: 25),
                        Name = c.String(maxLength: 1024),
                        PhoneNumber = c.String(maxLength: 255),
                        EmailAddress = c.String(maxLength: 1024),
                        InActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.DepartmentID)
                .ForeignKey("dbo.Location", t => t.LocationID, cascadeDelete: true)
                .Index(t => t.LocationID);
            
            CreateTable(
                "dbo.Location",
                c => new
                    {
                        LocationID = c.Int(nullable: false, identity: true),
                        OrganizationID = c.Int(nullable: false),
                        SystemCode = c.String(maxLength: 50),
                        ShortCode = c.String(maxLength: 25),
                        Name = c.String(maxLength: 1024),
                        Street = c.String(maxLength: 1024),
                        HouseNumber = c.String(maxLength: 255),
                        ZipCode = c.String(maxLength: 25),
                        City = c.String(maxLength: 255),
                        IPCheck = c.Boolean(nullable: false),
                        IPAddress = c.String(maxLength: 1024),
                        InActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.LocationID)
                .ForeignKey("dbo.Organization", t => t.OrganizationID, cascadeDelete: true)
                .Index(t => t.OrganizationID);
            
            CreateTable(
                "dbo.Organization",
                c => new
                    {
                        OrganizationID = c.Int(nullable: false, identity: true),
                        SystemCode = c.String(maxLength: 50),
                        ShortCode = c.String(maxLength: 25),
                        Name = c.String(),
                        PrivateKey = c.String(),
                        InActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.OrganizationID);
            
            CreateTable(
                "dbo.Patient",
                c => new
                    {
                        PatientID = c.Int(nullable: false, identity: true),
                        PatientNumber = c.String(maxLength: 50),
                        CivilServiceNumber = c.String(maxLength: 15),
                        BirthDate = c.DateTime(),
                        Initials = c.String(),
                        FirstName = c.String(),
                        MiddleName = c.String(),
                        LastName = c.String(),
                        MaidenName = c.String(),
                        Gender = c.Int(nullable: false),
                        InActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.PatientID);
            
            CreateTable(
                "dbo.Nurse",
                c => new
                    {
                        NurseID = c.Int(nullable: false, identity: true),
                        DepartmentID = c.Int(nullable: false),
                        Description = c.String(),
                        NurseRole = c.Int(nullable: false),
                        Initials = c.String(),
                        FirstName = c.String(),
                        MiddleName = c.String(),
                        LastName = c.String(),
                        MaidenName = c.String(),
                        Gender = c.Int(nullable: false),
                        InActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.NurseID)
                .ForeignKey("dbo.Department", t => t.DepartmentID, cascadeDelete: true)
                .Index(t => t.DepartmentID);
            
            CreateTable(
                "dbo.Bed",
                c => new
                    {
                        BedID = c.Int(nullable: false, identity: true),
                        DepartmentID = c.Int(nullable: false),
                        DepartmentOverruleID = c.Int(),
                        SpecialismID = c.Int(),
                        SpecialismOverruleID = c.Int(),
                        SystemCode = c.String(maxLength: 50),
                        RoomNumber = c.String(maxLength: 25),
                        BedNumber = c.String(maxLength: 10),
                    })
                .PrimaryKey(t => t.BedID)
                .ForeignKey("dbo.Department", t => t.DepartmentID, cascadeDelete: true)
                .ForeignKey("dbo.Department", t => t.DepartmentOverruleID)
                .ForeignKey("dbo.Specialism", t => t.SpecialismID)
                .ForeignKey("dbo.Specialism", t => t.SpecialismOverruleID)
                .Index(t => t.DepartmentID)
                .Index(t => t.DepartmentOverruleID)
                .Index(t => t.SpecialismID)
                .Index(t => t.SpecialismOverruleID);
            
            CreateTable(
                "dbo.Specialism",
                c => new
                    {
                        SpecialismID = c.Int(nullable: false, identity: true),
                        SystemCode = c.String(maxLength: 50),
                        ShortCode = c.String(maxLength: 25),
                        Name = c.String(maxLength: 1024),
                    })
                .PrimaryKey(t => t.SpecialismID);
            
            CreateTable(
                "dbo.BedForPatient",
                c => new
                    {
                        BedForPatientID = c.Int(nullable: false, identity: true),
                        BedID = c.Int(nullable: false),
                        PatientID = c.Int(nullable: false),
                        BedPatientStatus = c.Int(nullable: false),
                        StartTime = c.DateTime(),
                        EndTime = c.DateTime(),
                    })
                .PrimaryKey(t => t.BedForPatientID)
                .ForeignKey("dbo.Bed", t => t.BedID, cascadeDelete: true)
                .ForeignKey("dbo.Patient", t => t.PatientID, cascadeDelete: true)
                .Index(t => t.BedID)
                .Index(t => t.PatientID);
            
            CreateTable(
                "dbo.ShiftForBed",
                c => new
                    {
                        ShiftForBedID = c.Int(nullable: false, identity: true),
                        BedID = c.Int(nullable: false),
                        ShiftDetailID = c.Int(),
                        ShiftOrder = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ShiftForBedID)
                .ForeignKey("dbo.Bed", t => t.BedID, cascadeDelete: true)
                .ForeignKey("dbo.ShiftDetail", t => t.ShiftDetailID)
                .Index(t => t.BedID)
                .Index(t => t.ShiftDetailID);
            
            CreateTable(
                "dbo.ShiftDetail",
                c => new
                    {
                        ShiftDetailID = c.Int(nullable: false, identity: true),
                        ShiftID = c.Int(nullable: false),
                        NurseID = c.Int(),
                        PagerID = c.Int(),
                        Pause = c.Int(),
                        Visit = c.Int(),
                    })
                .PrimaryKey(t => t.ShiftDetailID)
                .ForeignKey("dbo.Shift", t => t.ShiftID, cascadeDelete: true)
                .ForeignKey("dbo.Nurse", t => t.NurseID)
                .ForeignKey("dbo.Pager", t => t.PagerID)
                .Index(t => t.ShiftID)
                .Index(t => t.NurseID)
                .Index(t => t.PagerID);
            
            CreateTable(
                "dbo.Shift",
                c => new
                    {
                        ShiftID = c.Int(nullable: false, identity: true),
                        Description = c.String(maxLength: 255),
                        Display = c.String(maxLength: 1024),
                        WeekDay = c.Int(nullable: false),
                        StartTime = c.Int(nullable: false),
                        Duration = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ShiftID);
            
            CreateTable(
                "dbo.Pager",
                c => new
                    {
                        PagerID = c.Int(nullable: false, identity: true),
                        DepartmentID = c.Int(nullable: false),
                        PagerNumber = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.PagerID)
                .ForeignKey("dbo.Department", t => t.DepartmentID, cascadeDelete: true)
                .Index(t => t.DepartmentID);
            
            CreateTable(
                "dbo.Cycle",
                c => new
                    {
                        CycleID = c.Int(nullable: false, identity: true),
                        OrganizationID = c.Int(nullable: false),
                        DepartmentID = c.Int(),
                        Frequency = c.Int(nullable: false),
                        Delay = c.Int(nullable: false),
                        Code_CodeID = c.Int(),
                    })
                .PrimaryKey(t => t.CycleID)
                .ForeignKey("dbo.Organization", t => t.OrganizationID, cascadeDelete: true)
                .ForeignKey("dbo.Department", t => t.DepartmentID)
                .ForeignKey("dbo.Code", t => t.Code_CodeID)
                .Index(t => t.OrganizationID)
                .Index(t => t.DepartmentID)
                .Index(t => t.Code_CodeID);
            
            CreateTable(
                "dbo.Code",
                c => new
                    {
                        CodeID = c.Int(nullable: false, identity: true),
                        CodeMainType = c.Int(nullable: false),
                        CodeType = c.String(),
                        CodeValue = c.String(),
                        Unit = c.String(),
                        LastMeasuring = c.DateTime(),
                        CodeStatus = c.Int(nullable: false),
                        TimeStamp = c.DateTime(),
                        PatientID = c.Int(),
                    })
                .PrimaryKey(t => t.CodeID)
                .ForeignKey("dbo.Patient", t => t.PatientID)
                .Index(t => t.PatientID);
            
            CreateTable(
                "dbo.CodeHistory",
                c => new
                    {
                        CodeHistoryID = c.Int(nullable: false, identity: true),
                        CodeMainType = c.Int(nullable: false),
                        CodeType = c.String(),
                        CodeValue = c.String(),
                        Unit = c.String(),
                        LastMeasuring = c.DateTime(),
                        CodeStatus = c.Int(nullable: false),
                        TimeStamp = c.DateTime(),
                        PatientID = c.Int(),
                    })
                .PrimaryKey(t => t.CodeHistoryID)
                .ForeignKey("dbo.Patient", t => t.PatientID)
                .Index(t => t.PatientID);
            
            CreateTable(
                "dbo.Setting",
                c => new
                    {
                        SettingID = c.Int(nullable: false, identity: true),
                        OrganizationID = c.Int(nullable: false),
                        DepartmentID = c.Int(),
                        SettingType = c.Int(nullable: false),
                        SettingValue = c.String(),
                    })
                .PrimaryKey(t => t.SettingID)
                .ForeignKey("dbo.Organization", t => t.OrganizationID, cascadeDelete: true)
                .ForeignKey("dbo.Department", t => t.DepartmentID)
                .Index(t => t.OrganizationID)
                .Index(t => t.DepartmentID);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Setting", new[] { "DepartmentID" });
            DropIndex("dbo.Setting", new[] { "OrganizationID" });
            DropIndex("dbo.CodeHistory", new[] { "PatientID" });
            DropIndex("dbo.Code", new[] { "PatientID" });
            DropIndex("dbo.Cycle", new[] { "Code_CodeID" });
            DropIndex("dbo.Cycle", new[] { "DepartmentID" });
            DropIndex("dbo.Cycle", new[] { "OrganizationID" });
            DropIndex("dbo.Pager", new[] { "DepartmentID" });
            DropIndex("dbo.ShiftDetail", new[] { "PagerID" });
            DropIndex("dbo.ShiftDetail", new[] { "NurseID" });
            DropIndex("dbo.ShiftDetail", new[] { "ShiftID" });
            DropIndex("dbo.ShiftForBed", new[] { "ShiftDetailID" });
            DropIndex("dbo.ShiftForBed", new[] { "BedID" });
            DropIndex("dbo.BedForPatient", new[] { "PatientID" });
            DropIndex("dbo.BedForPatient", new[] { "BedID" });
            DropIndex("dbo.Bed", new[] { "SpecialismOverruleID" });
            DropIndex("dbo.Bed", new[] { "SpecialismID" });
            DropIndex("dbo.Bed", new[] { "DepartmentOverruleID" });
            DropIndex("dbo.Bed", new[] { "DepartmentID" });
            DropIndex("dbo.Nurse", new[] { "DepartmentID" });
            DropIndex("dbo.Location", new[] { "OrganizationID" });
            DropIndex("dbo.Department", new[] { "LocationID" });
            DropIndex("dbo.Employee", new[] { "DepartmentID" });
            DropIndex("dbo.Employee", new[] { "UserId" });
            DropForeignKey("dbo.Setting", "DepartmentID", "dbo.Department");
            DropForeignKey("dbo.Setting", "OrganizationID", "dbo.Organization");
            DropForeignKey("dbo.CodeHistory", "PatientID", "dbo.Patient");
            DropForeignKey("dbo.Code", "PatientID", "dbo.Patient");
            DropForeignKey("dbo.Cycle", "Code_CodeID", "dbo.Code");
            DropForeignKey("dbo.Cycle", "DepartmentID", "dbo.Department");
            DropForeignKey("dbo.Cycle", "OrganizationID", "dbo.Organization");
            DropForeignKey("dbo.Pager", "DepartmentID", "dbo.Department");
            DropForeignKey("dbo.ShiftDetail", "PagerID", "dbo.Pager");
            DropForeignKey("dbo.ShiftDetail", "NurseID", "dbo.Nurse");
            DropForeignKey("dbo.ShiftDetail", "ShiftID", "dbo.Shift");
            DropForeignKey("dbo.ShiftForBed", "ShiftDetailID", "dbo.ShiftDetail");
            DropForeignKey("dbo.ShiftForBed", "BedID", "dbo.Bed");
            DropForeignKey("dbo.BedForPatient", "PatientID", "dbo.Patient");
            DropForeignKey("dbo.BedForPatient", "BedID", "dbo.Bed");
            DropForeignKey("dbo.Bed", "SpecialismOverruleID", "dbo.Specialism");
            DropForeignKey("dbo.Bed", "SpecialismID", "dbo.Specialism");
            DropForeignKey("dbo.Bed", "DepartmentOverruleID", "dbo.Department");
            DropForeignKey("dbo.Bed", "DepartmentID", "dbo.Department");
            DropForeignKey("dbo.Nurse", "DepartmentID", "dbo.Department");
            DropForeignKey("dbo.Location", "OrganizationID", "dbo.Organization");
            DropForeignKey("dbo.Department", "LocationID", "dbo.Location");
            DropForeignKey("dbo.Employee", "DepartmentID", "dbo.Department");
            DropForeignKey("dbo.Employee", "UserId", "dbo.Users");
            DropTable("dbo.Setting");
            DropTable("dbo.CodeHistory");
            DropTable("dbo.Code");
            DropTable("dbo.Cycle");
            DropTable("dbo.Pager");
            DropTable("dbo.Shift");
            DropTable("dbo.ShiftDetail");
            DropTable("dbo.ShiftForBed");
            DropTable("dbo.BedForPatient");
            DropTable("dbo.Specialism");
            DropTable("dbo.Bed");
            DropTable("dbo.Nurse");
            DropTable("dbo.Patient");
            DropTable("dbo.Organization");
            DropTable("dbo.Location");
            DropTable("dbo.Department");
            DropTable("dbo.Employee");
            RenameTable(name: "dbo.Users", newName: "User");
        }
    }
}
