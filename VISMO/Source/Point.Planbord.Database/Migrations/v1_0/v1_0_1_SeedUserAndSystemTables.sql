﻿
set nocount on

declare @roles table ( Name varchar(100), ID uniqueidentifier )

-- Set the identifiers:
declare @applicationId uniqueidentifier = '{16a5dadf-8bd8-4423-ab03-8e4f8ae8afeb}'
declare @applicationName nvarchar(100) = 'Planbord'


insert into @roles values ( 'PlanbordAdministrator', '{CF5F629A-0AEF-2205-A46A-E9DD0862CD64}')
insert into @roles values ( 'OrganizationAdministrator' , '{50C2C2AC-2110-4823-A06C-E5E3DA33B57E}')
insert into @roles values ( 'PersonalAccount', '{6E4F148D-86AB-47F8-84C4-FD3C7A1B61C7}')
insert into @roles values ( 'DepartmentAccount' , '{AAADC115-E10A-4EB6-A0E5-BE496B18B8C2}')




if not exists ( select 1 from dbo.Applications where ApplicationName = @applicationName )
begin
    insert into dbo.Applications 
           ( ApplicationId, ApplicationName, Description )
    values ( @applicationId, @applicationName, @applicationName )
end

insert into Roles 
select ID, @applicationId, Name, Name
  from @roles 
 where Name not in ( select RoleName 
                         from Roles
                        where ApplicationId = @applicationId )


declare @username nvarchar(100) 
declare @rolename nvarchar(100)
declare @pass nvarchar(256) 
declare @salt nvarchar(256) 
declare @userid uniqueidentifier

set @username = 'TechxxAdmin'
set @rolename = 'PlanbordAdministrator'
set @pass = 'zj/soZpP7R+8RonjyUIkH+d8bPs=' -- Techxx!123
set @salt = 'aRe7wGMI7vmoe8ROx7k4gA=='
set @userid = NEWID()

if not exists ( select 1 from dbo.Users where UserName = @username and ApplicationID = @applicationId )
begin
    declare @now datetime = getutcdate() -- get the same timestamp in the several records  
    declare @mindate datetime = convert(datetime,'1754-01-01') -- membership uses 1754 

    insert into dbo.Users
           ( UserId, ApplicationId, UserName, IsAnonymous, LastActivityDate )
    values ( @userid, @applicationId, @username, 0, GETDATE())

    insert into dbo.UsersInRoles
           ( UserId, RoleId )
    values ( @userid, (select RoleId from Roles where RoleName = @rolename ) )
    
    insert into dbo.Memberships
           ( UserId,
             ApplicationId,
             Password,
             PasswordFormat,
             PasswordSalt,
             Email,
             PasswordQuestion,
             PasswordAnswer,
             IsApproved,
             IsLockedOut,
             CreateDate,
             LastLoginDate,
             LastPasswordChangedDate,
             LastLockoutDate,
             FailedPasswordAttemptCount,
             FailedPasswordAttemptWindowStart,
             FailedPasswordAnswerAttemptCount,
             FailedPasswordAnswerAttemptWindowsStart,
             Comment )
    values ( @userid,
             @applicationId,
             @pass,
             1,
             @salt,
             null,
             null,
             null,
             1,
             0,
             @now,
             @now,
             @now,
             @mindate,
             0,
             @mindate,
             0,
             @mindate,
             null)
                   
end
INSERT INTO [Version] ([VersionNumber], [MigrationId], [Remark], [LastUpdate]) VALUES ('1.0.1', '201309161751425_v1_0_1_SeedUserAndSystemTables', '', getdate()) 
INSERT INTO [__MigrationHistory] ([MigrationId], [Model], [ProductVersion]) VALUES ('201309161751425_v1_0_1_SeedUserAndSystemTables', 0x1F8B0800000000000400E55DDB721BB9117D4F55FE81C5A724554BCAF27ACB7151BB254BD64615CB524C7BF7D10571206ACAC319EE0C4696BE2D0FF9A4FC4280B9E23EB8911A3A2F2E7100341ADD8D46372EC7FFFDF77F16BF3C6E92C903CC8B384B4FA62F6647D3094C575914A7EB936989EE7E783DFDE5E73FFF69F12EDA3C4E7E6BEBBD24F570CBB43899DE23B47D339F17AB7BB801C56C13AFF2ACC8EED06C956DE620CAE6C747477F9FBF78318798C414D39A4C161FCB14C51B58FDC03FCFB27405B7A804C95516C1A468BEE392654575F2016C60B1052B7832BDC9E214CD6E1290DE667934FB1DDE9E6EB749BC02087336C394107C44D3C9691203CCDC122677D3C9F6C7379F0BB8447996AE975B5C13249F9EB61097DF81A480CD50DE6C7F341DCDD13119CD1CA46986AA8E9DA431EDC68947FA0E4B043D11B6AAD19E4C1B61D39570B57FC227E603FE7493675B98A3A78FF08E6D7A793E9DCCD9E673BE7DD75A6C4A7839995EA6E8E5F174F2A14C12709BC04E6458A64B94E5F05798C21C2018DD0084608E8DE33282D55884CEE55D7D2837B7306FBBC34AC2A6379D5CC48F307A0FD335BAEFBABC028FED9757D8FE3EA7313654DC06E525A439AC7FEB3BBF8AD779A5B97EA4865D1FBF7AE5D9F7476C1AF957CB6EF19F9EDDBE0705FABC8DB0AEDAAECFF1DF9FF03C94A89726B598F7C6A935D9F7D93A4EFF1117D82E9E5CEC966EEF62BC7CFB5D5B70D51FF62C39F969A9CF1747BE265CF5DEAB50AD521362641497514BE5D7328EECADBAC892B276867B960461FE748D7566DDF3F18F21947079B36FF7F53E5B81E439BAC5A3BD0071023B53799B650904E920A50FE0215E571E57A2BCE9E4234CAAC2E23EDED6CBF68C9ECD5FEA5A1779B6F99825DC54AF0ABF2CB3325F917990A96A7C02F91A2247DF5631E0E0D3DA8965EBCBF41352E2A545125460E449C9D1C769EDCDA8E3CBE234CDD2A74D56161A8333224596BFD3158A1FB05ECFED17419D095382965A72657D4CA5DE90F932C18E850A32331E9A60C5654AA8154AF69A0AC23CE3CBBADE69F6980A2DFFA6EC5D4112FF1176A4CCF5C5F5941438945610D894D7B2E515FFBCC3CE4FCA6853A6E0522C1558945491F167ECB1688373705C9CF7B0F55FE19C0F45C9C1071DBFD485EB460C9CC36295C75B87D0E6F8D54FB62BAE7E1AFBBB17D9FC95FA1FFBF92BE74E98790A4E75F586A7B3CF08D49E9194E8B996D710F85554F39ADFBC0CFE6FA3931B5014DFB23CB28DFE8F5FFB7A86B6E78B2CDF003494709AD15A82C43A8FF11FC9BB0D8EEA77EFDD1463FE57098BFDB8570503A769F1CD7A134A2F7693EE7178BBDDE6D983369D320C94714AF81546D725F2257596431C12BB85C89298BBDBA60842AD55D8D93D48D7300AC8E5EA6B56A220F4EA04B9332D84E0668BCEB232F5F41052BABFC769947D5B2290A3D08C57736267ECD3D4EB4114614671966D36F67B41A143B590C90C9F12EA539E50596B90C06D90F5A1CCD6381CEA432C974088B4760984DA76A3088408332E2992D6F40F2D4532DDE920FFA8763AAA7F743B1DD53FB679C6D08473CE36F84936909438EF7F36A30F9768049D8A01721297D96CBF0A58EDB7C9B603A51B7236D9AEFFE4D0B1C5CC1E276363B6C20E2CAF6D7F567716821EF25A75FF1B484A5877FBDC5CBC8D53903F756949F3CB84098740BE3EE1764C0CECE7B2F5AE2F3F71341BC32653E7B428B2555C71A5D816FA2239B97A974613A380B3361B5934889555262826AB0BFE75323D9ACD5E08F21CEAA75B62FB7E6A11B2B445C278A6C39C1CD283E40CEB03E5204E91E816E274156F4162C203D7D86E119B775DF125E7700B53729DC044DE7E3C745D713E6F48568B39654496B6A53E63D0A85E1ABA985BDADF6CCC4CBA29DBF7C5C459BCD1F112BE4ECF6102119C906344720DEE0C142B10896E04CFD56817F62A198C89C9684F72FCAC57A2CA201CEDC19685284C6553EA908CF55A6D746C61AEEAD3D52187F8DCB6A9E27C4F6E54A593D17B5021C836310D36E20E6A756CA6DB93A67653466D7B34FF26CA972797CEB6476BC6AFFB3DD89E627F416526439B0D26C6A233C3A1A3D30359A5F5C3D8EFFAAC57D921ADCC4616AABE3BA55F3E879CE341DBA36A00FBB544956A0EC5062579BDCA6274497E6F33EC96965DF2ACBB3CB697CC59CDC09EE23DB58C471FF189D798556AD6DC69EEB5CCBCB1B0F06BEAEBD0032624B54D272B52B2B0272352CA776436545B3979CA865BC0BCF548CD93B7F6891BAF68D2660911FBB8AA984EFA7DC346C9EDC332D156581294B8622823C49AE200B5E6FAA240A436B981C6D48A20A3C1ACC703A4B82D0CF9C8C42DA801AA5DE025A74707C82672EACE30A5E2EA52BE0152B4D394F3C52D4A1C3DCA3C95F26BF7BCA9BADABB0EFCEC31DD89EEC626D7A0302B4D779E29BA8D85F251192B05170931E669202865E86BBFAD1A506CB2989822CF4E516F218A67A4A2E0F43B78667B78BCFEBB99A7118D72D76E07C6241ECAEAE520EE2999ED2AF9CA81D947A288B18ED15B1CAA2B12A2504C363B6CB63B0CC764B3C3B1DBD93324207D966D96670F19BC5966BD3B39C84E6745490CE57AA6D91E350E61E9D5C84593DDEDC0A1485E558A1219C8590CB3168A7B3E96D488439DA87848A33D3EEF82EAAE6C31AF31269A0F8BB9028C627105B65B7299A26FD97C992C6B648AB31F96F630109B9AC67C5548D0203A6EBB9EB050C01A72A5E4CA6B042FE2BC20F797C12D20371BCEA28D50CD3085687BE3320951656D34D936207F3713CF04A5632685B8A0457B81474BEEF256038794FE952D27043304242057E35A9C6549B94985CFBC490ED26A812B24F4DA22739A0C1E054D912930A7D7624CD0A4DA6FE65468C8089A12FD5DA4B69873EA135266C174849D50D6148D0C95F331E1CD550D6F51753064B3FAE64A157098168C1AB4781983548913ADAF0B0B44FB224B9AFD5D2B81665F644EB3DD75A189A97662D45468680A7642F4DFED786AD02678B69ACF963223380E82B4C8471B3A0D1E044BA7F968C94F0BF020F0D4168C66CAD76140F8992E0B790C66B8BCD96E6D9B3B2CA1890D9CA3E839131D838B4F60F01B68624C81DD82C48238F0CB125B3A1A4B65129BF006ABC9E30CEC56DB7A9F86273CF55750B53543E6790A4D9329188DB148B6EAC29B8CFEF9B891DD0C93383CDFD73F26A7C9F55FED29B58FC365F4DA327BAAF5337119CDBAC49C62F3F49B26D57CB2E7AA7FC82DE3AC2FB5A7DCBED096D16DCBACD6A5EEE135B72C75DF6DA8518FAF59725481393DFA05364D8EFE6EB766528FB0F905932AB2A3297D8ACD539756B2E59D7A9A2D724F159AD3D53DD1A6BBD0D5F3EC8D79B83DD82753DBB967C98B6E4DCF92DA417A665F7B1B32C036B2984BED6B706622B51F47B3E433670AE1177BEA00DA7E99D73556A6DCCD455726DD565C7E5553D9C5024FB81083CBFEEB771B5532E77ABBC999A50F848D6C4CDB7AC7BB4356A6FA4CCAE30F94C2EB4FF3E6D6488303ED77AB43EED12D1324B245F634D997B432D26C0DC71EDA77B1CA0EDA0A2EDBF78AE88C29DCB3AD0B47727C95AEF7EE688E3B825B34C761C3A0F1C2F9585D653AC1027B88237236B67C2A709C31231566CB3F92B324AE0284B6C21548E33B9CBD7CCABEC2F4647A7C74F49A03997700809F1745941C040A7C4C643188966DF95C5B0A009F3E807C750F720148D61BDF5D4AB94273F7816F6FA9FE65031EFFEA0FC94EFE46DF3D24FB4ECC498AC62E557A85381E006C5DA62C7BB0F5328DFF28615C0DFB2E2633C117783DD4A0A9232D2D6182A2EE059B1E68D273A8E8C1A80AA0E7B731D253391058719DE9B963577953359AC4AC225DD1C4394D7A218987F6DDEA2770CF06E01CD26006355C4137FB039129D6FD9F02CDE0F1C0F08E7436F3F0BCF2458460A90641DFAD628B00D8BBC1D864A075DD8C51CD318F9C1B9A3E0B8C6B221337DC5B173F2CC1BC752123E2DD1A3A72F9CA20E0DD7A51D3E0DD7A7229E0DD3AD31BC6BB759993A668B7A1D856A2DD06607E08EBD6790C1CD6ED8E973AD541C03E215647BAC8F5E707E69A186940A3DC8A1F29D26710DD39933B344C4BFF708EDD4F1FD805F3C6AC0C4899C5A1C4746FAB0F6E941558939E39A00F7C40FD98C7E535BFFCF18D07EC803370C97340018CEDF9BF07200EF3E4EF19006B2C8D3190E1E82FBF5A6E8C589991B1C98E01D2C60E18D648813688AFFE68B2A3F72A06576A47E6599C005DC7E365BC6CECE0BD8F93B98DC1135902118EC7DE681C1747FCC303B331EDC5C1715A971E16C47E89DB597CD4E1F8B842BA8E7E41D4DF091CD95AA88751F1F102DF9D0D597A85B0A0BBE307DABD31078FF40EB3D96D253FC4C9D1FB93A13BAACFEA51B8EB8CDD0E3D0FCDC16BB2B921C95DF8D3E0F6D597174FA6D16D86155EEF313980FA3D0D42FAC93AB206FD5362FEC9A8DBA2010E8101CAFAF0400B94894CAC22EB94F95F92073AA51712A13BBA50D69114B64B8732380432A852930B0AA16C405CB9ACBBA68A38ACFD201CD156DCBFF3175D8A6A1B956AA99D3DCF8EF3259958C2FB70DDB08D041502513114E6A4BD66550E41FE1E7A0743DF0D98A4AFE6F72D961DE200FA8A42F4DEFC23BA30F33E303AA6DD3490AC1FC233AEB0C30C007E69AD996718B22475F0452CB4D7AE74C9963CF2321FAE05FE9EF8A807C7E4654A0E55EB5FE7B088D73D8905A699C215138D77752ED3BBAC4D0A388EDA2AFCC31188408443F5D31CC577608570F10A1645F5FF2656A7C8E442DF2D8C2ED3EB126D4B84878CDD5BC208832417BAFE2B904196E7C57575C5A3083104CC664CCEA1AFD3B7659C441DDF17927368050992B5344F33882E1179A2B17EEA287DC85243428DF8BA64EB13DC6C134CACB84E97E001AA791B96212BB1C5790CD639D8140D8DBE3DFE89CD2FDA3CFEFC3F3D0E866FF1940000, '5.0.0.net45')
