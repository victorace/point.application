namespace Point.Planbord.WebApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class v1_0_6_CodeChanges : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Code", "SystemCycle", c => c.String());
            AddColumn("dbo.CodeHistory", "SystemCycle", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.CodeHistory", "SystemCycle");
            DropColumn("dbo.Code", "SystemCycle");
        }
    }
}
