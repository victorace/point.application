namespace Point.Planbord.WebApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class v1_0_3_ShiftChanges : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ShiftDetail", "ShiftDetailRole", c => c.Int(nullable: false));
            DropColumn("dbo.Shift", "Display");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Shift", "Display", c => c.String(maxLength: 1024));
            DropColumn("dbo.ShiftDetail", "ShiftDetailRole");
        }
    }
}
