namespace Point.Planbord.WebApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class v1_0_9_SeedSetting : DbMigration
    {
        public override void Up()
        {

            string addSettings = @"
set nocount on

IF NOT EXISTS(SELECT Setting.SettingID FROM Setting WHERE Setting.SettingMainType = 0 AND Setting.OrganizationID IS NULL AND Setting.LocationID IS NULL AND Setting.DepartmentID IS NULL)
BEGIN
	INSERT INTO Setting 
	(OrganizationID, LocationID, DepartmentID, SettingMainType, SettingValue)
	VALUES(NULL, NULL, NULL, 0, '[]');
END

IF NOT EXISTS(SELECT Setting.SettingID FROM Setting WHERE Setting.SettingMainType = 1 AND Setting.OrganizationID IS NULL AND Setting.LocationID IS NULL AND Setting.DepartmentID IS NULL)
BEGIN
	INSERT INTO Setting 
	(OrganizationID, LocationID, DepartmentID, SettingMainType, SettingValue)
	VALUES(NULL, NULL, NULL, 1, '[{""Column"":1,""DisplayType"":1},{""Column"":2,""DisplayType"":1},{""Column"":3,""DisplayType"":1},{""Column"":4,""DisplayType"":1},{""Column"":5,""DisplayType"":1},{""Column"":6,""DisplayType"":1},{""Column"":7,""DisplayType"":1},{""Column"":8,""DisplayType"":1},{""Column"":9,""DisplayType"":1},{""Column"":10,""DisplayType"":1},{""Column"":11,""DisplayType"":1},{""Column"":12,""DisplayType"":1},{""Column"":13,""DisplayType"":1},{""Column"":14,""DisplayType"":1},{""Column"":15,""DisplayType"":1},{""Column"":16,""DisplayType"":1},{""Column"":17,""DisplayType"":1},{""Column"":18,""DisplayType"":1}]');
END

IF NOT EXISTS(SELECT Setting.SettingID FROM Setting WHERE Setting.SettingMainType = 2 AND Setting.OrganizationID IS NULL AND Setting.LocationID IS NULL AND Setting.DepartmentID IS NULL)
BEGIN
	INSERT INTO Setting 
	(OrganizationID, LocationID, DepartmentID, SettingMainType, SettingValue)
	VALUES(NULL, NULL, NULL, 2, '{""MessageLevel"":2,""Messages"":[{""MessageLevel"":0,""Text"":""NORMALE SITUATIE"",""Color"":""FFFFFF""},{""MessageLevel"":1,""Text"":""DIJKBEWAKING"",""Color"":""FFFF80"" },{""MessageLevel"":2,""Text"":""EXTERNE OPNAME"",""Color"":""FFFF00""},{""MessageLevel"":3,""Text"":""DREIGENDE OPNAMESTOP"",""Color"":""FFBF80""},{""MessageLevel"":4,""Text"":""DIRECTE OPNAMESTOP"",""Color"":""FF4040""},{""MessageLevel"":5,""Text"":""vrije ingave"",""Color"":""FFBF80""}]}');
END

IF NOT EXISTS(SELECT Setting.SettingID FROM Setting WHERE Setting.SettingMainType = 3 AND Setting.OrganizationID IS NULL AND Setting.LocationID IS NULL AND Setting.DepartmentID IS NULL)
BEGIN
	INSERT INTO Setting 
	(OrganizationID, LocationID, DepartmentID, SettingMainType, SettingValue)
	VALUES(NULL, NULL, NULL, 3, '{""Pin"":""""}');
END

IF NOT EXISTS(SELECT Setting.SettingID FROM Setting WHERE Setting.SettingMainType = 4 AND Setting.OrganizationID IS NULL AND Setting.LocationID IS NULL AND Setting.DepartmentID IS NULL)
BEGIN
	INSERT INTO Setting 
	(OrganizationID, LocationID, DepartmentID, SettingMainType, SettingValue)
	VALUES(NULL, NULL, NULL, 4, '{""DetailIconSet"":1,""Icons"":[{""DetailIconType"":0,""Visible"":false,""Name"":""Rooming in"",""Description"":""Rooming in""},{""DetailIconType"":1,""Visible"":true,""Name"":""Thuiszorg"",""Description"":""Thuiszorg is geregeld""},{""DetailIconType"":2,""Visible"":true,""Name"":""Apotheek"",""Description"":""Apotheek is bij patient langs geweest""},{""DetailIconType"":3,""Visible"":true,""Name"":""Dietiste"",""Description"":""Dietiste is bij patient langs geweest""},{""DetailIconType"":4,""Visible"":false,""Name"":""Hartrevalidatie"",""Description"":""Hartrevalidatie""},{""DetailIconType"":5,""Visible"":true,""Name"":""Verwardheid"",""Description"":""Verwardheid/Delier""},{""DetailIconType"":6,""Visible"":true,""Name"":""Reinigen"",""Description"":""Patient is met ontslag, het bed moet gereinigd worden""},{""DetailIconType"":7,""Visible"":true,""Name"":""Medium risk"",""Description"":""Medium risk""},{""DetailIconType"":8,""Visible"":true,""Name"":""Inleiding"",""Description"":""Inleiding""},{""DetailIconType"":9,""Visible"":true,""Name"":""Diabetes"",""Description"":""Diabetes""},{""DetailIconType"":10,""Visible"":true,""Name"":""Fysiotherapie"",""Description"":""Fysiotherapie""},{""DetailIconType"":11,""Visible"":true,""Name"":""Hartvalenvpk"",""Description"":""Hartvalenverpleegkundige""},{""DetailIconType"":12,""Visible"":true,""Name"":""Tlemetrie"",""Description"":""Telemetrie""},{""DetailIconType"":13,""Visible"":true,""Name"":""Poliklinisch"",""Description"":""Poliklinisch""},{""DetailIconType"":14,""Visible"":false,""Name"":""Opname"",""Description"":""Opname""},{""DetailIconType"":15,""Visible"":false,""Name"":""Dagbeh."",""Description"":""Dagbehandeling""},{""DetailIconType"":16,""Visible"":false,""Name"":""Kinderarts"",""Description"":""Kinderarts""},{""DetailIconType"":17,""Visible"":false,""Name"":""Neonatologie"",""Description"":""Neonatologie""},{""DetailIconType"":18,""Visible"":false,""Name"":""Gez.moeder"",""Description"":""Gezonde moeder""},{""DetailIconType"":19,""Visible"":false,""Name"":""Kraamvrouw"",""Description"":""Kraamvrouw""},{""DetailIconType"":20,""Visible"":false,""Name"":""Nuchter"",""Description"":""Nuchter""},{""DetailIconType"":21,""Visible"":false,""Name"":""Pedagogie"",""Description"":""Pedagogisch medewerker""},{""DetailIconType"":22,""Visible"":false,""Name"":""Zuurstof"",""Description"":""Zuurstof""},{""DetailIconType"":23,""Visible"":false,""Name"":""ADL zorg"",""Description"":""Volledige ADL zorg nodig""},{""DetailIconType"":24,""Visible"":false,""Name"":""ADL hulp"",""Description"":""Hulp nodig bij ADL""},{""DetailIconType"":25,""Visible"":false,""Name"":""Valrisico"",""Description"":""Valrisico""}]}');
END

IF NOT EXISTS(SELECT Setting.SettingID FROM Setting WHERE Setting.SettingMainType = 5 AND Setting.OrganizationID IS NULL AND Setting.LocationID IS NULL AND Setting.DepartmentID IS NULL)
BEGIN
	INSERT INTO Setting 
	(OrganizationID, LocationID, DepartmentID, SettingMainType, SettingValue)
	VALUES(NULL, NULL, NULL, 5, '{""Default"":""60"",""Codes"":[]}');
END

IF NOT EXISTS(SELECT Setting.SettingID FROM Setting WHERE Setting.SettingMainType = 6 AND Setting.OrganizationID IS NULL AND Setting.LocationID IS NULL AND Setting.DepartmentID IS NULL)
BEGIN
	INSERT INTO Setting 
	(OrganizationID, LocationID, DepartmentID, SettingMainType, SettingValue)
	VALUES(NULL, NULL, NULL, 6, '{""Seconds"":10}');
END

IF NOT EXISTS(SELECT Setting.SettingID FROM Setting WHERE Setting.SettingMainType = 7 AND Setting.OrganizationID IS NULL AND Setting.LocationID IS NULL AND Setting.DepartmentID IS NULL)
BEGIN
	INSERT INTO Setting 
	(OrganizationID, LocationID, DepartmentID, SettingMainType, SettingValue)
	VALUES(NULL, NULL, NULL, 7, '{""Seconds"":300}');
END

IF NOT EXISTS(SELECT Setting.SettingID FROM Setting WHERE Setting.SettingMainType = 8 AND Setting.OrganizationID IS NULL AND Setting.LocationID IS NULL AND Setting.DepartmentID IS NULL)
BEGIN
	INSERT INTO Setting 
	(OrganizationID, LocationID, DepartmentID, SettingMainType, SettingValue)
	VALUES(NULL, NULL, NULL, 8, '{""DisplayCodeType"":1}');
END

";
            Sql(addSettings);

        }
        
        public override void Down()
        {
            string deleteSettings = @"
set nocount on

delete from dbo.Settings where OrganizationID is null and LocationID is null and DepartmentID is null

";

            Sql(deleteSettings);


        }
    }
}
