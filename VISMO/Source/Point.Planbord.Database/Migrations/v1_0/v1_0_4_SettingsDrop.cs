namespace Point.Planbord.WebApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class v1_0_4_SettingsDrop : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Setting", "OrganizationID", "dbo.Organization");
            DropForeignKey("dbo.Setting", "DepartmentID", "dbo.Department");
            DropIndex("dbo.Setting", new[] { "OrganizationID" });
            DropIndex("dbo.Setting", new[] { "DepartmentID" });
            DropTable("dbo.Setting");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Setting",
                c => new
                    {
                        SettingID = c.Int(nullable: false, identity: true),
                        OrganizationID = c.Int(nullable: false),
                        DepartmentID = c.Int(),
                        SettingType = c.Int(nullable: false),
                        SettingValue = c.String(),
                    })
                .PrimaryKey(t => t.SettingID);
            
            CreateIndex("dbo.Setting", "DepartmentID");
            CreateIndex("dbo.Setting", "OrganizationID");
            AddForeignKey("dbo.Setting", "DepartmentID", "dbo.Department", "DepartmentID");
            AddForeignKey("dbo.Setting", "OrganizationID", "dbo.Organization", "OrganizationID", cascadeDelete: true);
        }
    }
}
