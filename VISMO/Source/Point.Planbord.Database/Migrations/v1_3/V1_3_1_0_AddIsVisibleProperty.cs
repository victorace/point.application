namespace Point.Planbord.WebApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class V1_3_1_0_AddIsVisibleProperty : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Bed", "DepartmentID", "dbo.Department");
            DropIndex("dbo.Bed", new[] { "DepartmentID" });
            AddColumn("dbo.Department", "IsVisible", c => c.Boolean(nullable: false));
            AlterColumn("dbo.Bed", "DepartmentID", c => c.Int());
            AddForeignKey("dbo.Bed", "DepartmentID", "dbo.Department", "DepartmentID");
            CreateIndex("dbo.Bed", "DepartmentID");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Bed", new[] { "DepartmentID" });
            DropForeignKey("dbo.Bed", "DepartmentID", "dbo.Department");
            AlterColumn("dbo.Bed", "DepartmentID", c => c.Int(nullable: false));
            DropColumn("dbo.Department", "IsVisible");
            CreateIndex("dbo.Bed", "DepartmentID");
            AddForeignKey("dbo.Bed", "DepartmentID", "dbo.Department", "DepartmentID", cascadeDelete: true);
        }
    }
}
