namespace Point.Planbord.WebApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class v1_3_2_0_Indexes : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.BedForPatient", "BedPatientStatus");
            CreateIndex("dbo.Bed", "SystemCode");
            CreateIndex("dbo.Patient", "PatientNumber");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Patient", new[] { "PatientNumber" });
            DropIndex("dbo.Bed", new[] { "SystemCode" });
            DropIndex("dbo.BedForPatient", new[] { "BedPatientStatus" });
        }
    }
}
