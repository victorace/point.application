// <auto-generated />
namespace Point.Planbord.WebApplication.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class v1_3_2_0_Indexes : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(v1_3_2_0_Indexes));
        
        string IMigrationMetadata.Id
        {
            get { return "201608151145422_v1_3_2_0_Indexes"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
