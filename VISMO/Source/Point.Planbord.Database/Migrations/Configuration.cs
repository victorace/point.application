namespace Point.Planbord.WebApplication.Migrations
{
    using System;
    using System.Data.Entity.Core.Common.CommandTrees;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Model;
    using System.Data.Entity.SqlServer;
    using System.Linq;
    using System.Text.RegularExpressions;



    internal sealed class Configuration : DbMigrationsConfiguration<Point.Planbord.Database.Context.PlanbordContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;

            SetSqlGenerator("System.Data.SqlClient", new CustomSqlServerMigrationSqlGenerator());
        }

        protected override void Seed(Point.Planbord.Database.Context.PlanbordContext context)
        {
           
        }
    }


    

    public class CustomSqlServerMigrationSqlGenerator : SqlServerMigrationSqlGenerator
    {

        protected override void Generate(RenameTableOperation renameTableOperation)
        {
            if (renameTableOperation.Name == "dbo.User" || renameTableOperation.Name == "dbo.Users") return;

            base.Generate(renameTableOperation);
        }

        protected override void Generate(CreateTableOperation createTableOperation)
        {
            foreach (var c in createTableOperation.Columns)
            {
                if (c.Name == "TimeStamp")
                    c.DefaultValueSql = "getdate()";
            }

            base.Generate(createTableOperation);
        }

        protected override void Generate(AddColumnOperation addColumnOperation)
        {

            if (addColumnOperation.Column.Name == "TimeStamp" )
                addColumnOperation.Column.DefaultValueSql = "getdate()";

            base.Generate(addColumnOperation);
        }

        protected override void Generate(HistoryOperation historyOperation)
        {
            insertVersion(historyOperation);

            base.Generate(historyOperation);
        }

        private void insertVersion(HistoryOperation historyOperation)
        {

            string version = "### ENTER VERSION ###";
            string migrationid = "";
            string remark = "";

            try
            {


                DbModificationCommandTree commandtree = historyOperation.CommandTrees.First();

                if (commandtree.CommandTreeKind == DbCommandTreeKind.Insert)
                {
                    DbInsertCommandTree insertcommandtree = commandtree as DbInsertCommandTree;


                    foreach (DbSetClause dbsetclause in insertcommandtree.SetClauses)
                    {
                        if (((DbPropertyExpression)dbsetclause.Property).Property.Name.Equals("MigrationId", StringComparison.InvariantCultureIgnoreCase))
                        {
                            migrationid = ((DbConstantExpression)dbsetclause.Value).Value.ToString();
                            break;
                        }
                    }

                    if (!String.IsNullOrWhiteSpace(migrationid))
                    {
                        Match m = Regex.Match(migrationid, @"v([\d_]*)_(.*)");
                        if (m.Success)
                        {
                            version = m.Groups[1].Value.Replace("_", ".").Trim('.');
                            remark = m.Groups[2].Value;
                        }

                    }
                }

            }
            catch (Exception ex)
            {
                Statement("-- Something went wrong when trying to create version-statement");
                Statement("-- " + ex.Message);
            }

            Statement(String.Format("INSERT INTO [Version] ([VersionNumber], [MigrationId], [Remark], [LastUpdate]) VALUES ('{0}', '{1}', '{2}', getdate()) ", version, migrationid, remark));
        }

    }


}
