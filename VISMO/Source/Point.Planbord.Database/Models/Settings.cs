﻿using Point.Planbord.Database.Repository;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Point.Planbord.Database.Models
{
    public enum SettingMainType : int
    {
        [Description("SpecialismeKleur")]
        SpecialismColor = 0,
        [Description("Zichtbaarheid kolommen")]
        ColumnDisplayType = 1,
        [Description("Boodschap")]
        HeaderMessage = 2,
        [Description("Pin")]
        Pin = 3,
        [Description("IconenSet")]
        Icons = 4,
        [Description("Codes")]
        Codes = 5,
        [Description("Automatisch sluiten")]
        AutomaticClose = 6,
        [Description("Automatisch refresh")]
        AutomaticRefresh = 7,
        [Description("Diagnose Code")]
        DiagnosisCode = 8,
        [Description("Ligduur")]
        HospitalizationCode = 9
    }

    public class Setting : BaseEntity, IIdentified
    {
        public int SettingID { get; set; }

        public int? OrganizationID { get; set; }
        [ForeignKey("OrganizationID")]
        public virtual Organization Organization { get; set; }
        public int? LocationID { get; set; }
        [ForeignKey("LocationID")]
        public virtual Location Location { get; set; }
        public int? DepartmentID { get; set; }
        [ForeignKey("DepartmentID")]
        public virtual Department Department { get; set; }

        public SettingMainType SettingMainType { get; set; }
        [StringLength(255)]
        public string SettingType { get; set; }
        public string SettingValue { get; set; }

        [NotMapped]
        public int EntityID
        {
            get { return SettingID; }
            set { SettingID = value; }
        }
    }
}