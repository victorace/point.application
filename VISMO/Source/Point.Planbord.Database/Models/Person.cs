﻿using Point.Planbord.Database.Repository;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Providers.Entities;

namespace Point.Planbord.Database.Models
{

    public enum Gender : int
    {
        [Description("Man")]
        Male = 0,
        [Description("Vrouw")]
        Female = 1,
        [Description("Onbekend")]
        Unknown = 2
    }

    public class Person : BaseEntity, IInActive
    {
        [DisplayName("Initialen")]
        public string Initials { get; set; }
        [DisplayName("Voornaam")]
        public string FirstName { get; set; }
        [DisplayName("Tussenvoegsel")]
        public string MiddleName { get; set; }
        [DisplayName("Achternaam")]
        public string LastName { get; set; }
        [DisplayName("Meisjesnaam")]
        public string MaidenName { get; set; }
        [DisplayName("Geslacht")]
        public Gender Gender { get; set; }

        [DefaultValue(false), DisplayName("Verwijderd")]
        public bool InActive { get; set; }


    }

    public class Employee : Person, IIdentified
    {
        public int EmployeeID { get; set; }

        public Guid? UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual User User { get; set; }
        public int DepartmentID { get; set; }
        [ForeignKey("DepartmentID")]
        public virtual Department Department { get; set; }
        [StringLength(50)]
        public string EmployeeNumber { get; set; }


        [NotMapped]
        public int EntityID
        {
            get { return EmployeeID; }
            set { EmployeeID = value; }
        }
    }

    public class Patient : Person, IIdentified
    {
        public int PatientID { get; set; }
        [StringLength(50)]
        [Index("IX_PatientNumber")]
        public string PatientNumber { get; set; }
        [StringLength(15)]
        public string CivilServiceNumber { get; set; }
        [DisplayName("Geboortedatum")]
        public DateTime? BirthDate { get; set; }
        [StringLength(255)]
        public string FullName { get; set; }

        [NotMapped]
        public int EntityID
        {
            get { return PatientID; }
            set { PatientID = value; }
        }

    }

    public enum NurseRole : int
    {
        [Description("Verpleegkundige")]
        Nurse,
        [Description("Flex medewerker")]
        Flex,
        [Description("Stagiair")]
        Intern
    }

    public class Nurse : Person, IIdentified
    {
        public int NurseID { get; set; }
        [DisplayName("Afdeling")]
        public int DepartmentID { get; set; }
        [ForeignKey("DepartmentID")]
        public virtual Department Department { get; set; }

        [DisplayName("Verpleegkundige")]
        public string Description { get; set; } // Overrule for first/middle/last-name for flex and stagaiar
        [DisplayName("Rol")]
        [DefaultValue(NurseRole.Nurse)]
        public NurseRole NurseRole { get; set; }

        [NotMapped]
        public int EntityID
        {
            get { return NurseID; }
            set { NurseID = value; }
        }
    }
}