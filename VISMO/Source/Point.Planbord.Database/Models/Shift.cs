﻿using Point.Planbord.Database.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Point.Planbord.Database.Models
{
    public enum ShiftDetailRole : int
    {
        [Description("Niet gedefinieerd")]
        Undefined = 0, // Shouldn't be used
        [Description("Verpleegkundige 1")]
        FirstNurse = 1,
        [Description("Verpleegkundige 2")]
        SecondNurse = 2,
        [Description("Stagiair")]
        Intern = 3
    }

    public enum WorkloadType : int
    {
        [Description("Geen")]
        Undefined = 0, // Shouldn't be used
        [Description("Laag")]
        Low = 1,
        [Description("Middel")]
        Middle = 2,
        [Description("Hoog")]
        High = 3
    }

    public class Shift : BaseEntity, IIdentified
    {
        public int ShiftID { get; set; }
        [StringLength(255)]
        public string Description { get; set; }
        /// <summary>
        /// Daynumber of the week, -1 = all days
        /// </summary>
        public int WeekDay { get; set; } 
        /// <summary>
        /// In minutes since 0:00
        /// </summary>
        public int StartTime { get; set; }
        /// <summary>
        /// Duration of the shift in minutes. 
        /// F.e. shift StartTime = 540 (09:00), Duration = 360, then endtime = 15:00
        ///      shift StartTime = 780 (13:00), Duration = 1080, then endtime = 07:00 next day
        /// </summary>
        public int Duration { get; set; }

        [NotMapped]
        public int EntityID
        {
            get { return ShiftID; }
            set { ShiftID = value; }
        }
    }

    public class ShiftDetail : BaseEntity, IIdentified
    {
        public int ShiftDetailID { get; set; }

        public int BedID { get; set; }
        [ForeignKey("BedID")]
        public Bed Bed {get; set;}

        public int? NurseID { get; set; }
        [ForeignKey("NurseID")]
        public Nurse Nurse { get; set; }

        public int ShiftID { get; set; }
        [ForeignKey("ShiftID")]
        public Shift Shift { get; set; }

        public int? PagerID { get; set; }
        [ForeignKey("PagerID")]
        public Pager Pager { get; set; }

        public int? Pause { get; set; }
        public int? Visit { get; set; }

        [DefaultValue(WorkloadType.Low)]
        public WorkloadType Workload { get; set; }

        [DefaultValue(ShiftDetailRole.Undefined)]
        public ShiftDetailRole ShiftDetailRole { get; set; }

        [NotMapped]
        public int EntityID
        {
            get { return ShiftDetailID; }
            set { ShiftDetailID = value; }
        }

    }
}