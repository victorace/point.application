﻿//using Point.Planbord.Infrastructure;
using Point.Planbord.Database.Models;
using Point.Planbord.Database.Repository;
using Point.Planbord.Infrastructure;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Point.Planbord.Database.Models
{
    public class EmployeeUserBaseViewModel : IInActive
    {
        // [Employee]
        public int EmployeeID { get; set; }

        public Guid? UserId { get; set; }

        [DisplayName("Afdeling")]
        [Required(ErrorMessage = "Dit is een verplicht veld.")]
        public int DepartmentID { get; set; }

        [DisplayName("Afdeling")]
        public string DepartmentName { get; set; }

        [DisplayName("Werknemernummer")]
        public string EmployeeNumber { get; set; }

        [DisplayName("Initialen")]
        public string Initials { get; set; }

        [DisplayName("Voornaam")]
        public string FirstName { get; set; }

        [DisplayName("Tussenvoegsel")]
        public string MiddleName { get; set; }

        [DisplayName("Achternaam")]
        [Required(ErrorMessage = "Dit is een verplicht veld.")]
        public string LastName { get; set; }

        [DisplayName("Meisjesnaam")]
        public string MaidenName { get; set; }

        [DisplayName("Geslacht")]
        public Gender Gender { get; set; }

        [DisplayName("Verwijderd")]
        public bool InActive { get; set; }

        // [Memberships]
        [DisplayName("Gebruikersnaam")]
        [Required(ErrorMessage = "Dit is een verplicht veld.")]
        [StringLength(100)]
        public string UserName { get; set; }

        [DisplayName("E-mail")]
        [Required(ErrorMessage = "Dit is een verplicht veld.")]
        public string Email { get; set; }

        [DisplayName("Geblokkeerd")]
        public bool IsLockedOut { get; set; }

        [DisplayName("Laatste inlog")]
        public DateTime? LastLoginDate { get; set; }

        [DisplayName("Locatie")]
        [Required(ErrorMessage = "Dit is een verplicht veld.")]
        public int LocationID { get; set; }

        [DisplayName("Locatie")]
        public string LocationName { get; set; }

        [DisplayName("Organisatie")]
        [Required(ErrorMessage = "Dit is een verplicht veld.")]
        public int OrganizationID { get; set; }

        [DisplayName("Rol(len)")]
        [Required(ErrorMessage = "Dit is een verplicht veld.")]
        public Role Role { get; set; }

        public string[] RoleList { get; set; }

        [DisplayName("Rol(len)")]
        [Required(ErrorMessage = "Dit is een verplicht veld.")]
        public string[] SelectedRoles { get; set; }
    }

    public class EmployeeUserEditViewModel : EmployeeUserBaseViewModel
    {
        [DisplayName("Wachtwoord")]
        [RegularExpression(@"^(?=^[^\s]{8,}$)(?=.*[A-Za-z])(?=.*\d)(?=.*\W)[\dA-Za-z\W]{8,}$",
                           ErrorMessage = "Wachtwoord moet bestaan uit min. 8 karakters, waarvan min. 1 cijfer en min. 1 speciaal teken")]
        public string Password { get; set; }

        [DisplayName("Bevestiging")]
        [Compare("Password", ErrorMessage = "Wachtwoorden komen niet overeen")]
        public string ConfirmPassword { get; set; }
    }
    public class EmployeeUserViewModel : EmployeeUserBaseViewModel
    {
        [DisplayName("Wachtwoord")]
        [RegularExpression(@"^(?=^[^\s]{8,}$)(?=.*[A-Za-z])(?=.*\d)(?=.*\W)[\dA-Za-z\W]{8,}$",
                           ErrorMessage = "Wachtwoord moet bestaan uit min. 8 karakters, waarvan min. 1 cijfer en min. 1 speciaal teken")]
        [Required(ErrorMessage = "Dit is een verplicht veld.")]
        public string Password { get; set; }

        [DisplayName("Bevestiging")]
        [Compare("Password", ErrorMessage = "Wachtwoorden komen niet overeen")]
        public string ConfirmPassword { get; set; }
    }
}