﻿using System.Collections.Generic;
using System.ComponentModel;

namespace Point.Planbord.Database.Models
{
    public enum PlanbordColumn : int
    {
        [Description("Achternaam")]
        Achternaam = 1,

        [Description("Bed")]
        Bed = 2,

        [Description("Specialisme")]
        Spm = 3,

        [Description("Opname")]
        StartDate = 4,

        [Description("Ontslag")]
        EndDate = 5,

        [Description("Diagnose")]
        Diagnosis = 6,

        [Description("Code")]
        Code = 7,

        [Description("Vitale")]
        Vitale = 8,

        [Description("Isolatie")]
        Isolation = 9,

        [Description("Q-Score")]
        QScore = 10,

        [Description("Bijzonderheden")]
        Details = 11,

        [Description("Afspraken")]
        Appointments = 12,

        [Description("Mobiliteit")]
        Mobility = 13,

        [Description("Nazorg")]
        Discharged = 14,

        [Description("Verpleegkundige 1")]
        Nurse1 = 15,

        [Description("Verpleegkundige 2")]
        Nurse2 = 16,

        [Description("Stagiaire(s)")]
        Intern = 17,

        [Description("Verpleegkundige toewijzen")]
        NurseDetails = 18,

        [Description("Ligduur")]
        Hospitalization = 19,
        
        [Description("Ordening/Opdrachten")]
        OrdeningOpdrachten = 20,

        [Description("AOA Patient")]
        AOAPatient = 21,

        [Description("APO IN")]
        ApoInPatient = 22,

        [Description("APO EXIT")]
        ApoExitPatient = 23,

        [Description("EWS SCORE")]
        EwsScorePatient = 24
    }

    public class PlanbordViewModel
    {
        public Bed Bed { get; set; }
        public BedForPatient BedForPatient { get; set; }
        public Patient Patient { get; set; }
        public IEnumerable<ShiftDetail> ShiftDetails { get; set; }
        public IEnumerable<Code> Codes { get; set; }
        public IEnumerable<Code> BedCodes { get; set; }
    }

}