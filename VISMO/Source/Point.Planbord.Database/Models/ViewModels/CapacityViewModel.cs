﻿using System.Collections.Generic;

namespace Point.Planbord.Database.Models
{
    public class CapacityOverview
    {
        public int DepartmentID { get; set; }
        public string DepartmentName { get; set; }
        public int SpecialismID { get; set; }
        public string SpecialismName { get; set; }
        public bool IsAcute { get; set; }
        public List<Bed> PhysicalBeds { get; set; }
        public List<Bed> ProductionBeds { get; set; }
        public List<Bed> CombinationBeds { get; set; }
        public List<Bed> PatientWithoutBeds { get; set; }
        public List<Bed> OccupiedBeds { get; set; }

        public CapacityOverview()
        {
            PhysicalBeds = new List<Bed> { };
            ProductionBeds = new List<Bed> { };
            CombinationBeds = new List<Bed> { };
            PatientWithoutBeds = new List<Bed> { };
            OccupiedBeds = new List<Bed> { };
        }
    }

    public class CapacityOverviewViewModel
    {
        public int DepartmentID { get; set; }
        public string DepartmentName { get; set; }
        public int? SpecialismID { get; set; }
        public Specialism Specialism { get; set; }
        public bool IsAcute { get; set; }
        public SettingHeaderMessageMessage SettingHeaderMessage { get; set; }
        public BedTotals BedTotals { get; set; }
        //public List<Bed> ProductionBeds { get; set; }
        public List<Bed> PhysicalBeds { get; set; }
        //public List<Bed> CombinationBeds { get; set; }
        public List<Bed> PatientWithoutBeds { get; set; }
        public List<Bed> OccupiedBeds { get; set; }
    }

    public class BedTotals 
    {
        public int Production { get; set; }
        public int Physical { get; set; }
        public int Lend { get; set; }
        public int Borrow { get; set; }
        public int Free { get; set; }
        public int DischargeToday { get; set; }
        public int Occupied { get; set; }
        public int ExpectedToday { get; set; }
        public int Capacity { get; set; }
        public int Reserved { get; set; }
        public int Blocked { get; set; }
        public int Closed { get; set; }
        public int DischargeTomorrow { get; set; }
        public int ExpectedTomorrow { get; set; }
        public int Eenpersoonskamers { get; set; }
        public int Sluiskamers { get; set; }
        public int OccupiedDay { get; set; }
        public int OccupiedClinical { get; set; }
    }
}
