﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Point.Planbord.Database.Models.ViewModels
{
    public class DepartmentHeaderMessageViewModel
    {
        public int DepartmentID { get; set; }
        [DisplayName("Systeemcode")]
        public string SystemCode { get; set; }
        [DisplayName("Afdeling")]
        public string DepartmentName { get; set; }
        [DisplayName("Locatie")]
        public string LocationName { get; set; }
        public string Color { get; set; }
        [DisplayName("Boodschap")]
        public string HeaderMessage { get; set; }
        [DisplayName("Einddatum")]
        //[DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}")]
        public DateTime? EndDate { get; set; }
    }

    public class SettingsHeaderMessageEx : SettingsHeaderMessage
    {
        public int DepartmentID { get; set; }
    }
}
