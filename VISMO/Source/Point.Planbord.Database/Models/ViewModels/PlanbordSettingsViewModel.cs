﻿using Point.Planbord.Database.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Point.Planbord.Database.Models
{
    public enum PlanbordSettingsParentType : int
    {
        Organization = 1,
        Location = 2,
        Department = 3
    }

    public enum ColumnDisplayType : int
    {
        Visible = 1,
        Invisible = 2,
        Ellipsis = 3
    }

    public enum DiagnosisDisplayCodeType : int
    {
        ICD10 = 1,
        manual = 2
    }

    public enum HospitalizationDisplayCodeType : int
    {
        Dag = 1,
        Uur = 2
    }

    public enum MessageLevel : int
    {
        Level0 = 0, // normale situatie,
        Level1 = 1, // dijkbewaking
        Level2 = 2, // externe opname
        Level3 = 3, // dreigende opnamestop of een 
        Level4 = 4, // directe opnamestop
        Level5 = 5  // mogelijk
    }

    public class PlanbordSettingsViewModel
    {
        public int ParentId { get; set; }

        public PlanbordSettingsParentType ParentType { get; set; }

        public SettingColumnDisplayType ColumnDisplayType { get; set; }

        public SettingsSpecialism Specialism { get; set; }

        public SettingsHeaderMessage HeaderMessage { get; set; }

        public SettingsPin Pin { get; set; }

        public SettingsIconSet DetailIconSet { get; set; }

        public SettingsCode Code { get; set; }

        public SettingsDiagnosisCode DiagnosisCode { get; set; }

        public SettingsAutomaticClose AutomaticClose { get; set; }

        public SettingsAutomaticRefresh AutomaticRefresh { get; set; }

        public SettingsHospitalization Hospitalization { get; set; }


    }

    public interface ISetting<T> where T : class
    {
        int Id { get; set; }
        SettingMainType SettingMainType { get; set; }
        bool Checked { get; set; }
    }

    public class SettingsSpecialism : ISetting<SettingsSpecialismValues>
    {
        public int Id { get; set; }
        public SettingMainType SettingMainType { get; set; }
        public bool Checked { get; set; }
        public IList<SettingsSpecialismValues> Values { get; set; }
    }
         
    public class SettingsSpecialismValues
    {
        [DisplayName("SpecialismId")]
        public int Id { get; set; }

        [DisplayName("SystemCode")]
        public string SystemCode { get; set; }

        [DisplayName("Kleur")]
        [Required]
        public string Color { get; set; }
    }

    public class SettingColumnDisplayType : ISetting<SettingColumnDisplayTypeValues>
    {
        public int Id { get; set; }
        public SettingMainType SettingMainType { get; set; }
        public bool Checked { get; set; }
        public IList<SettingColumnDisplayTypeValues> Values { get; set; }
    }

    public class SettingColumnDisplayTypeValues
    {
        [DisplayName("Kolom")]
        public PlanbordColumn Column { get; set;}

        [DisplayName("Zichtbaar")]
        public ColumnDisplayType DisplayType { get; set; }
    }

    public class SettingsHeaderMessage : ISetting<SettingsHeaderMessageValues>
    {
        public int Id { get; set; }
        public SettingMainType SettingMainType { get; set; }
        public bool Checked { get; set; }
        public SettingsHeaderMessageValues Values { get; set; }
    }

    public class SettingsHeaderMessageValues
    {
        [Required]
        public MessageLevel MessageLevel { get; set; }
        public IList<SettingHeaderMessageMessage> Messages { get; set; }
    }

    public class SettingHeaderMessageMessage
    {
        public MessageLevel MessageLevel { get; set; }
        public string Text { get; set; }
        public string Color { get; set; }
        public DateTime? EndDate { get; set; }
    }

    public class SettingsPin : ISetting<SettingsPinValues>
    {
        public int Id { get; set; }
        public SettingMainType SettingMainType { get; set; }
        public bool Checked { get; set; }
        public SettingsPinValues Values { get; set; }
    }

    public class SettingsPinValues
    {
        [Range(1000, 9999)]
        public string Pin { get; set; }
    }

    public class SettingsIconSet : ISetting<SettingsIconSetValues>
    {
        public int Id { get; set; }
        public SettingMainType SettingMainType { get; set; }
        public bool Checked { get; set; }
        public SettingsIconSetValues Values { get; set; }
    }

    public class SettingsIconSetValues
    {
        public IList<SettingsIconSetIcons> Icons { get; set; }
    }

    public class SettingsIconSetIcons
    {
        [Required]
        public int DetailIconType { get; set; }
        public bool Visible { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        [DefaultValue(false)]
        public bool IsDeleted { get; set; }
    }

    public class SettingsCode : ISetting<SettingsCodeValues>
    {
        public int Id { get; set; }
        public SettingMainType SettingMainType { get; set; }
        public bool Checked { get; set; }
        public SettingsCodeValues Values { get; set; }
    }

    public class SettingsCodeValues
    {
        [Required]
        public string Default { get; set; }
        public IList<SettingsCodeCycleValue> Codes { get; set; }
    }

    public class SettingsCodeCycleValue
    {
        public string CodeType { get; set; }
        public string SystemCycle { get; set; }
        [Range(0, 9999)]
        public string Value { get; set; }
    }

    public class SettingsDiagnosisCode : ISetting<DiagnosisCodeValues>
    {
        public int Id { get; set; }
        public SettingMainType SettingMainType { get; set; }
        public bool Checked { get; set; }
        public DiagnosisCodeValues Values { get; set; }
    }

    public class DiagnosisCodeValues
    {
        [Required]
        public DiagnosisDisplayCodeType DisplayCodeType { get; set; }
    }

    public class SettingsAutomaticClose : ISetting<AutomaticCloseValues>
    {
        public int Id { get; set; }
        public SettingMainType SettingMainType { get; set; }
        public bool Checked { get; set; }
        public AutomaticCloseValues Values { get; set; }
    }

    public class AutomaticCloseValues
    {
        [Required]
        [Range(0, 9999)]
        public int Seconds { get; set; }
    }

    public class SettingsAutomaticRefresh : ISetting<AutomaticRefreshValues>
    {
        public int Id { get; set; }
        public SettingMainType SettingMainType { get; set; }
        public bool Checked { get; set; }
        public AutomaticRefreshValues Values { get; set; }
    }

    public class AutomaticRefreshValues
    {
        [Required]
        [Range(0, 9999)]
        public int Seconds { get; set; }
    }

    public class SettingsHospitalization : ISetting<HospitalizationValues>
    {
        public int Id { get; set; }
        public SettingMainType SettingMainType { get; set; }
        public bool Checked { get; set; }
        public HospitalizationValues Values { get; set; }
    }

    public class HospitalizationValues
    {
        [Required]
        public HospitalizationDisplayCodeType HospitalizationDisplayCodeType { get; set; }
    }


}