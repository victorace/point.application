﻿using Point.Planbord.Database.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Providers.Entities;
using System.Web.Script.Serialization;

namespace Point.Planbord.Database.Models
{
    public class Organization : BaseEntity, IIdentified, IInActive
    {
        [DisplayName("Organisatie")]
        public int OrganizationID { get; set; }
        /// <summary>
        /// Used for mapping with SAP/External system
        /// </summary>
        [DisplayName("Systeemcode")]
        [StringLength(50)]
        public string SystemCode { get; set; }
        /// <summary>
        /// Used for display purposes
        /// </summary>
        [StringLength(25), DisplayName("Code")]
        public string ShortCode { get; set; }
        [DisplayName("Naam")]
        public string Name { get; set; }
        /// <summary>
        /// Contains a hash or other key to authenticate the organization in (web)communication
        /// </summary>
        [DisplayName("Privé-sleutel")]
        public string PrivateKey { get; set; }
        [DefaultValue(false), DisplayName("Verwijderd")]
        public bool InActive { get; set; }

//        [ScriptIgnore]
//        public virtual ICollection<Location> Locations { get; set; }

        [NotMapped]
        public int EntityID
        {
            get { return OrganizationID; }
            set { OrganizationID = value; }
        }
    }

    public class Location : BaseEntity, IIdentified, IInActive
    {
        public int LocationID { get; set; }
        [DisplayName("Organisatie")]
        public int OrganizationID { get; set; }
        [ForeignKey("OrganizationID")]
        [ScriptIgnore]
        [DisplayName("Organisatie")]
        public virtual Organization Organization { get; set; }

        /// <summary>
        /// Used for mapping with SAP/External system
        /// </summary>
        [StringLength(50)]
        [DisplayName("Systeemcode")]
        public string SystemCode { get; set; }
        /// <summary>
        /// Used for display purposes
        /// </summary>
        [StringLength(25), DisplayName("Code")]
        public string ShortCode { get; set; }
        [StringLength(1024), DisplayName("Naam")]
        public string Name { get; set; }
        [StringLength(1024), DisplayName("Straat")]
        public string Street { get; set; }
        [StringLength(255), DisplayName("Huisnummer")]
        public string HouseNumber { get; set; }
        [StringLength(25), DisplayName("Postcode")]
        public string ZipCode { get; set; }
        [StringLength(255), DisplayName("Plaats")]
        public string City { get; set; }
        [DefaultValue(false), DisplayName("IP controleren")]
        public bool IPCheck { get; set; }
        [StringLength(1024), DisplayName("IP adres")]
        public string IPAddress { get; set; }
        [DefaultValue(false), DisplayName("Verwijderd")]
        public bool InActive { get; set; }

//        [ScriptIgnore]
//        public virtual ICollection<Department> Departments { get; set; }


        [NotMapped]
        public int EntityID
        {
            get { return LocationID; }
            set { LocationID = value; }
        }
    }

    public class Department : BaseEntity, IIdentified, IInActive
    {
        [DisplayName("Afdeling")]
        public int DepartmentID { get; set; }
        [DisplayName("Locatie")]
        public int LocationID { get; set; }
        [ForeignKey("LocationID")]
        [ScriptIgnore]
        [DisplayName("Locatie")]
        public virtual Location Location { get; set; }
        [DisplayName("Zichtbaar")]
        [DefaultValue(true)]
        public bool IsVisible { get; set; }

        /// <summary>
        /// Used for mapping with SAP/External system
        /// </summary>
        [StringLength(50)]
        [DisplayName("Systeemcode")]
        public string SystemCode { get; set; }
        /// <summary>
        /// Used for 'Bed'-display on planbord
        /// </summary>
        [StringLength(25), DisplayName("Code")]
        public string ShortCode { get; set; }
        [StringLength(1024), DisplayName("Naam")]
        public string Name { get; set; }
        [StringLength(255), DisplayName("Telefoonnummer")]
        public string PhoneNumber { get; set; }
        [StringLength(1024), DisplayName("E-mail")]
        public string EmailAddress { get; set; }
        [DefaultValue(false), DisplayName("Verwijderd")]
        public bool InActive { get; set; }


        [NotMapped]
        public int EntityID
        {
            get { return DepartmentID; }
            set { DepartmentID = value; }
        }

    }
}