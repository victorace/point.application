﻿using Point.Planbord.Database.Repository;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Providers.Entities;

namespace Point.Planbord.Database.Models
{
    public enum BedPatientStatus : int
    {
        [Description("Geen")]
        Geen = 0,
        [Description("Opname gepland")]
        OpnameGepland = 1,
        [Description("Ontslag gepland")]
        OntslagGepland = 2,
        [Description("Opgenomen")]
        Opgenomen = 3,
        [Description("Ontslagen")]
        Ontslagen = 4,
        [Description("Blokkade: Bouwkundige maatregel")]
        BlokBAU = 10,
        [Description("Blokkade: Capaciteit probleem")]
        BlokCAP = 11,
        [Description("Blokkade: Desinfectie")]
        BlokDESI = 12,
        [Description("Blokkade: EHH bed (CCU)")]
        BlokEHH = 13,
        [Description("Blokkade: ICU bed")]
        BlokICU = 14,
        [Description("Blokkade: Beschermde isolatie")]
        BlokISO = 15,
        [Description("Blokkade: Overig")]
        BlokOVR = 16,
        [Description("Blokkade: Opname voor 9u")]
        BlokRES = 17,
        [Description("Blokkade: Terminale patient")]
        BlokTERM = 18,
        [Description("Blokkade: Zomersluiting")]
        BlokZOM = 19
    }

    public enum BedSoort : int
    {
        [Description("Onbekend")]
        Onbekend = 0,
        [Description("Beddenlocatie voor normale bedden")]
        Soort01 = 1,
        [Description("Beddenlocatie voor speciale bedden")]
        Soort02 = 2,
        [Description("Beddenlocatie voor spoedbedden")]
        Soort03 = 3,
        [Description("Kamer")]
        Soort11 = 11,
        [Description("Gang met mogelijkheid om bedden te plaatsen")]
        Soort12 = 12,
        [Description("Gang zonder mogelijkheid om bedden te plaatsten")]
        Soort13 = 13,
        [Description("Overige afdelingskamers of ruimtes in een afdeling")]
        Soort14 = 14,
        [Description("Behandelkamer in de polikliniek")]
        Soort15 = 15
    }

    public enum BedType : int
    {
        [Description("")]
        None = 0,
        [Description("Eenpersoonskamer")]
        Eenpersoonskamer = 1,
        [Description("Sluiskamer")]
        Sluiskamer = 2
    }

    public class Bed : BaseEntity, IIdentified
    {
        public int BedID { get; set; }

        public int? DepartmentID { get; set; }
        [ForeignKey("DepartmentID")]
        public virtual Department Department { get; set; }
        public int? DepartmentOverruleID { get; set; }
        [ForeignKey("DepartmentOverruleID")]
        [DisplayName("In gebruik door afdeling")]
        public virtual Department DepartmentOverrule { get; set; }
        public int? SpecialismID { get; set; }
        [ForeignKey("SpecialismID")]
        [DisplayName("Specialisme")]
        public virtual Specialism Specialism { get; set; }
        public int? SpecialismOverruleID { get; set; }
        [ForeignKey("SpecialismOverruleID")]
        [DisplayName("In gebruik door specialisme")]
        public virtual Specialism SpecialismOverrule { get; set; }

        /// <summary>
        /// Used for mapping with SAP/External system
        /// </summary>
        [DisplayName("Systeemcode")]
        [StringLength(50)]
        [Index("IX_SystemCode")]
        public string SystemCode { get; set; }


        [StringLength(25)]
        public string RoomNumber { get; set; }
        [StringLength(10)]
        public string BedNumber { get; set; }
        [DefaultValue(BedSoort.Onbekend)]
        public BedSoort BedSoort { get; set; }

        [DisplayName("Identifier")]
        [StringLength(255)]
        public string Identifier { get; set; }

        [DisplayName("Acuut")]
        public bool IsAcute { get; set; }

        [DisplayName("Bed type")]
        [DefaultValue(BedType.None)]
        public BedType BedType { get; set; }

        [NotMapped]
        public int EntityID
        {
            get { return BedID; }
            set { BedID = value; }
        }
    }

    public class BedForPatient : BaseEntity, IIdentified
    {
        public int BedForPatientID { get; set; }
        public int BedID { get; set; }
        [ForeignKey("BedID")]
        public virtual Bed Bed { get; set; }
        public int? PatientID { get; set; }
        [ForeignKey("PatientID")]
        public virtual Patient Patient { get; set; }
        [DefaultValue(BedPatientStatus.Geen)]
        [Index("IX_BedPatientStatus")]
        public BedPatientStatus BedPatientStatus { get; set; }
        /// <summary>
        /// Full datetime, when is the patient assigned to the bed
        /// </summary>
        [DisplayName("Opname")]
        public DateTime? StartTime { get; set; }
        /// <summary>
        /// Full datetime, when is the patient discharged from the bed
        /// </summary>
        [DisplayName("Ontslag")]
        public DateTime? EndTime { get; set; }

        public int? SpecialismID { get; set; }
        [ForeignKey("SpecialismID")]
        public virtual Specialism Specialism { get; set; }

        [NotMapped]
        public int EntityID
        {
            get { return BedForPatientID; }
            set { BedForPatientID = value; }
        }
    }



}