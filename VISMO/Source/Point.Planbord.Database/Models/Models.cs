﻿using Point.Planbord.Database.Repository;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Providers.Entities;

namespace Point.Planbord.Database.Models
{

    public abstract class BaseEntity
    {
        //Kinda lonely out here. 
        //Well maybe some code in the near future...
    }

    #region --- SYSTEM ---
    public class Version : BaseEntity
    {
        public int VersionID { get; set; }
        [StringLength(50)]
        public string VersionNumber { get; set; }
        [StringLength(255)]
        public string MigrationID { get; set; }
        public string Remark { get; set; }
        public DateTime LastUpdate { get; set;}
    }

    public enum Severity : int
    {
        NotSpecified = 0,
        Error = 1,
        Warning = 2,
        Info = 3,
        Verbose = 4
    }

    public class Logging : BaseEntity, IIdentified
    {
        public int LoggingID { get; set; }
        [DisplayName("Datum/Tijd")]
        public DateTime LoggingDateTime { get; set; }
        [DisplayName("Status")]
        public Severity Severity { get; set; }
        [DisplayName("Melding")]
        public string Description { get; set; }
        [StringLength(255)]
        [DisplayName("Applicatie")]
        public string ApplicationName { get; set; }
        [DisplayName("ReferentieID")]
        public int? ReferenceID { get; set; }
        [StringLength(100)]
        [DisplayName("Referentie")]
        public string ReferenceName { get; set; }

        [NotMapped]
        public int EntityID
        {
            get { return LoggingID; }
            set { LoggingID = value; }
        }
    }

    public class LoginHistory : BaseEntity, IIdentified 
    {
        public int LoginHistoryID { get; set; }
        public Guid? UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual User User { get; set; }

        [StringLength(100)]
        public string LoginUserName { get; set; }
        public DateTime? LoginDateTime { get; set; }
        [StringLength(100)]
        public string Resolution { get; set; }
        [StringLength(1024)]
        public string UserAgent { get; set; }
        [StringLength(50)]
        public string LoginIP { get; set; }
        [StringLength(50)]
        public string LocalIP { get; set; }
        public Nullable<bool> LoginFailed { get; set; }


        [NotMapped]
        public int EntityID
        {
            get { return LoginHistoryID; }
            set { LoginHistoryID = value; }
        }
    }
    #endregion




    public class Specialism : BaseEntity, IIdentified
    {
        public int SpecialismID { get; set; }

        //public int DepartmentID { get; set; }
        //[ForeignKey("DepartmentID")]
        //public virtual Department Department { get; set; }
        
        /// <summary>
        /// Used for mapping with SAP/External system
        /// </summary>
        [StringLength(50)]
        [DisplayName("Systeemcode")]
        public string SystemCode { get; set; }
        /// <summary>
        /// Used for display purposes
        /// </summary>
        [StringLength(25), DisplayName("Spm")]
        public string ShortCode { get; set; }
        [StringLength(1024), DisplayName("Naam")]
        public string Name { get; set; }

        [NotMapped]
        public int EntityID
        {
            get { return SpecialismID; }
            set { SpecialismID = value; }

        }
    }



    public class Pager : BaseEntity, IIdentified
    {
        public int PagerID { get; set; }

        public int DepartmentID { get; set; }
        [ForeignKey("DepartmentID")]
        public virtual Department Department { get; set; }

        [StringLength(50)]
        public string PagerNumber { get; set; }


        [NotMapped]
        public int EntityID
        {
            get { return PagerID; }
            set { PagerID = value; }
        }
    }

    public class Cycle : BaseEntity, IIdentified
    {
        public int CycleID { get; set; }

        // if both are null, it's our default ???
        public int OrganizationID { get; set; }
        [ForeignKey("OrganizationID")]
        public virtual Organization Organization { get; set; }
        public int? DepartmentID { get; set; }
        [ForeignKey("DepartmentID")]
        public virtual Department Department { get; set; }

        /// <summary>
        /// In minutes. F.e. 1440 is once per day, 480 is 3x pe day
        /// </summary>
        public int Frequency { get; set; }
        /// <summary>
        /// In minutes
        /// </summary>
        public int Delay { get; set; }
        public Code Code { get; set; } 


        [NotMapped]
        public int EntityID
        {
            get { return CycleID; }
            set { CycleID = value; }
        }
    }


}