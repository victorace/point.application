﻿using Newtonsoft.Json;
using Point.Planbord.Database.Repository;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Script.Serialization;

namespace Point.Planbord.Database.Models
{

    public enum CodeMainType : int
    {
        [Description("Diagnose")]
        Diagnose = 0, 
        [Description("Behandelingsbeperking")]
        Behandelingsbeperking = 1,
        [Description("Vitale signalen")]
        VitaleSignalen = 2,
        [Description("Isolatie")]
        Isolatie = 3,
        [Description("Q-Score")]
        QScore = 4,
        [Description("Bijzonderheden")]
        Bijzonderheden = 5,
        [Description("Afspraken")]
        Afspraken = 6,
        [Description("Mobiliteit")]
        Mobiliteit = 7,
        [Description("PointStatus")]
        PointStatus = 8,
        [Description("Bed")]
        Bed = 9,
        [Description("SAPSignalering")]
        SAPSignalering = 10,
        [Description("ApoIn")]
        ApoIn = 11,
        [Description("ApoExit")]
        ApoExit = 12,
        [Description("EwsScore")]
        EwsScore = 13
    }


    public enum CodeStatus : int
    {
        Pending = 0,
        Done = 1,
        PlanbordOnly = 2
    }

    public class CodeBase :BaseEntity
    {
        public CodeMainType CodeMainType { get; set; }
        // Pols, Temp., Saturation, etc. 
        public string CodeType { get; set; } 
        // 37, 180/100, Normaal, A, B, etc. 0100100100101 used for "icon"-fields
        public string CodeValue { get; set; }
        // O2, kg, cm, etc. Optional
        public string Unit { get; set; }
        public DateTime? LastMeasuring { get; set; }
        // Cycle/Frequency of this code. 
        public string SystemCycle { get; set; }
        [DefaultValue(CodeStatus.Done)]
        public CodeStatus CodeStatus { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime? TimeStamp { get; set; }

        public int? PatientID { get; set; }
        [ForeignKey("PatientID")]
        public virtual Patient Patient { get; set; }

        public int? BedID { get; set; }
        [ForeignKey("BedID")]
        public virtual Bed Bed { get; set; }

    }

    public class Code : CodeBase, IIdentified
    {
        public int CodeID { get; set; }

        [NotMapped]
        public int EntityID
        {
            get { return CodeID; }
            set { CodeID = value; }
        }

    }

    public class CodeHistory : CodeBase, IIdentified
    {
        public int CodeHistoryID { get; set; }

        [NotMapped]
        public int EntityID
        {
            get { return CodeHistoryID; }
            set { CodeHistoryID = value; }
        }
    }


    //public class CodesLookup
    //{
    //    public IEnumerable<IsolationCodes> Zzzz()
    //    {
    //        return new Enumerable<IsolationCodes>();
    //    }
    //}

    public class IsolationCode
    {
        public string SystemCode { get; set; }
        public string SystemDescription { get; set; }
        public string Color { get; set; }

        public static IEnumerable<IsolationCode> GetList()
        {
            List<IsolationCode> lst = new List<IsolationCode>();

            lst.Add(new IsolationCode() { SystemCode = "1", SystemDescription = "Strikte isolatie", Color = "Red" });
            lst.Add(new IsolationCode() { SystemCode = "2", SystemDescription = "Contactplus isolatie", Color = "Orange" });
            lst.Add(new IsolationCode() { SystemCode = "3", SystemDescription = "Contact isolatie", Color = "Green" });
            lst.Add(new IsolationCode() { SystemCode = "4", SystemDescription = "Aerogene isolatie", Color = "Pink" });
            lst.Add(new IsolationCode() { SystemCode = "5", SystemDescription = "Druppel isolatie", Color = "Yellow" });
            lst.Add(new IsolationCode() { SystemCode = "6", SystemDescription = "Beschermde isolatie", Color = "Blue" });

            return lst;
        }

        public static string GetListJson()
        {

            return JsonConvert.SerializeObject(GetList());
        }

    }

}