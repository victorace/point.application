﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
//using System.Web.Mvc;
using System.Web.Security;

namespace Point.Planbord.Database.Models
{



    public class LoginModel
    {
        [Required(ErrorMessage="Vul een gebruikersnaam in!")]
        [Display(Name = "Gebruikersnaam")]
        public string UserName { get; set; }

        [Required(ErrorMessage="Vul een wachtwoord in!")]
        [DataType(DataType.Password)]
        [Display(Name = "Wachtwoord")]
        public string Password { get; set; }

        [Display(Name = "Onthoud me?")]
        public bool RememberMe { get; set; }
    }
}
