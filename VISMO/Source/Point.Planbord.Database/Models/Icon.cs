﻿using Point.Planbord.Database.Repository;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Point.Planbord.Database.Models
{
    public class Icon : BaseEntity, IIdentified
    {
        public int IconID { get; set; }

        public int OrganizationID { get; set; }
        [ForeignKey("OrganizationID")]
        public virtual Organization Organization { get; set; }

        [StringLength(255)]
        public string LargeFileName { get; set; }
        [StringLength(255)]
        public string SmallFileName { get; set; }

        [NotMapped]
        public int EntityID
        {
            get { return IconID; }
            set { IconID = value; }
        }
    }
}
