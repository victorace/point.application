﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;
using M = Point.Planbord.Database.Models;
using Point.Planbord.Database.Models;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.Migrations;


namespace Point.Planbord.Database.Context
{

    public class PlanbordImportContext : PlanbordContext 
    {
        public PlanbordImportContext()
        {
            base.Configuration.AutoDetectChangesEnabled = false;
        }
    }

    public class PlanbordContext : DbContext
    {

        public PlanbordContext() : base("Name=PlanbordContext") {  }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            //Keeping this in plural, since dbo.User is a database-user-object.
            modelBuilder.Entity<System.Web.Providers.Entities.User>().ToTable("Users", "dbo");
        }

        public DbSet<M.Version> Versions { get; set; }
        
        public DbSet<LoginHistory> LoginHistories { get; set; }

        public DbSet<Employee> Employees { get; set; }
        public DbSet<Patient> Patients { get; set; }
        public DbSet<Nurse> Nurses { get; set; }
        public DbSet<Bed> Beds {get; set;}
        public DbSet<BedForPatient> BedForPatients { get; set; }

        public DbSet<Shift> Shifts { get; set; }
        public DbSet<ShiftDetail> ShiftDetails { get; set; } 

        public DbSet<Organization> Organizations { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Specialism> Specialisms { get; set; }

        public DbSet<Pager> Pagers { get; set; }
        public DbSet<Cycle> Cycles { get; set; }
        public DbSet<Code> Codes { get; set; }
        public DbSet<CodeHistory> CodeHistories { get; set; }
        public DbSet<Setting> Settings { get; set; }
        public DbSet<Icon> Icons { get; set; }
    }

    public class LoggingContext : DbContext
    {
        public LoggingContext() : base("Name=PlanbordContext") { }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

        public DbSet<Logging> Loggings { get; set; }
        
    }
}