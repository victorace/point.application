﻿using Point.Planbord.Database.Context;
using Point.Planbord.Database.Repository;
using Point.Planbord.Infrastructure;
using System.Linq;
using System.Web.Security;

namespace Point.Planbord.Database
{
    public class EmployeeUserHelper
    {
        //private readonly MembershipUser membershipUser;
        //private readonly UnitOfWork<PlanbordContext> uow = new UnitOfWork<PlanbordContext>();
        //private ISessionWrapper session;

        //public EmployeeUserHelper(ISessionWrapper session)
        //{
        //    this.session = session;
        //    membershipUser = Membership.GetUser();
        //}

        //public EmployeeUserHelper(ISessionWrapper session, string username)
        //{
        //    this.session = session;
        //    membershipUser = Membership.GetUser(username);
        //}

        //public EmployeeUserHelper(ISessionWrapper session, object key)
        //{
        //    this.session = session;
        //    membershipUser = Membership.GetUser(key);
        //}

        //public Employee GetEmployee()
        //{
        //    if (membershipUser != null)
        //    {
        //        var employee = (from e in uow.EmployeeRepository.GetAll()
        //                        join m in Membership.GetAllUsers().Cast<MembershipUser>() on membershipUser.ProviderUserKey equals m.ProviderUserKey
        //                        select e).FirstOrNew();
        //        return employee;
        //    }

        //    return new Employee();
        //}

        public static bool IsDepartmentOfUser(EmployeeUserSessionObject sessionObject, int departmentID)
        {
            if ( sessionObject == null ) return false;
            if ( departmentID < 0) return false;



            if ( Roles.IsUserInRole(Role.PlanbordAdministrator.ToString()))
            {
                return true;
            }
            else if ( Roles.IsUserInRole(Role.OrganizationAdministrator.ToString()))
            {
                int i = 0;
                using ( UnitOfWork<PlanbordContext> unitofwork = new UnitOfWork<PlanbordContext>() )
                {
                    i = unitofwork.DepartmentRepository.Get(d=>d.Location.Organization.OrganizationID==sessionObject.OrganizationID).Count();
                }
                return i > 0;
            }
            else if ( Roles.IsUserInRole(Role.PersonalAccount.ToString()) || Roles.IsUserInRole(Role.DepartmentAccount.ToString()))
            {
                return sessionObject.DepartmentID == departmentID;
            }
            else {
                return false;
            }
        }
    }

    public class EmployeeUserSessionObject
    {
        public int EmployeeID { get; set; }
        public int OrganizationID { get; set; }
        public int LocationID { get; set; }
        public int DepartmentID { get; set; }
        public string DepartmentName { get; set; }
        public UserSettings UserSettings { get; set; }
        public int CurrentShiftID { get; set; }
        public int CurrentDepartmentID { get; set; }
        public string CurrentDepartmentName { get; set; }
    }
}