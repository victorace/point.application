﻿DECLARE @previousversion VARCHAR(10) = '#.#.#' 

DECLARE @currentversion VARCHAR(10)

SELECT TOP 1 @currentversion = versionnumber 
  FROM version
 ORDER BY lastupdate desc

IF ( @previousversion <> @currentversion )
    PRINT 'Verkeerde huidige versie: ' + @currentversion + ', versie moet zijn: ' +  @previousversion + ' !!!'
ELSE
    PRINT 'Huidige versie: ' +  @previousversion + ' is correct.'