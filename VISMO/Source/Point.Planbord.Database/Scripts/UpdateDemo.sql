﻿

declare @demodate as char(8) = '20150127' --< Enter the date when the demo takes place
declare @demohour as int = 10             --< Enter the hour (24h) on which the demo takes place
declare @demodelay as int = 2             --< Enter the delay (in hours!) for the 'missed measurement' (can be found in Setting.SettingMainType = 5, which is in minutes)
                                          --  set both to a higher value if demo takes all day


update BedForPatient  
   set StartTime = dateadd(d,(datediff(d,StartTime,@demodate))-3+(PatientID*BedID)%4,starttime), --Set to current date keeping the time, minus 3 + some "random" modulo days 
       EndTime = dateadd(d,(datediff(d,EndTime,@demodate))+(PatientID*BedID)%20,endtime) --same for endtime, current date + some "random"
  from BedForPatient 
 where StartTime is not null


update Code 
   set LastMeasuring =  DATEADD(d, datediff(d,LastMeasuring,@demodate),LastMeasuring) -- Only update the 'date'-part, leave the 'time'-part in tact.
  from Code
 where LastMeasuring is not null


-- Vitale signalen: --
update Code 
       set LastMeasuring = dateadd(hour, -datepart(hour, LastMeasuring), LastMeasuring) -- Reset to 00:.. 
  from Code
 where LastMeasuring is not null
   and CodeMainType = 2

update Code 
       set LastMeasuring = dateadd(hour, (@demohour), LastMeasuring) -- Set to the demo-hour
  from Code
 where LastMeasuring is not null
   and CodeMainType = 2

update Code 
       set LastMeasuring = dateadd(hour, -@demodelay, LastMeasuring) -- Set some back in time, to have a missed measuring
  from Code
 where LastMeasuring is not null
   and CodeMainType = 2 and CodeID % 40 = 0 

-- Q-Score: --
update Code 
   set LastMeasuring = ( select top 1 dateadd(hour, 2, StartTime) from BedForPatient where c.PatientID = BedForPatient.PatientID ) -- Set two hours after 'opnamedatum-tijd'
  from Code C 
 where CodeMainType = 4

-- !!! Fix this one, as the 24hours check is adjusted (as discussed 26-05-14)
update Code 
   set CodeValue = ' ',
       LastMeasuring = dateadd(hour,24,LastMeasuring) -- Set some forward in time, to simulate missed (within 24 hours) 
  from Code 
 where CodeMainType = 4
   and CodeType <> 'Pijnscore'
   and CodeID % 20 = 0 
 