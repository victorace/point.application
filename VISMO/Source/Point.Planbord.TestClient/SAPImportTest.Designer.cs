﻿namespace Point.Planbord.TestClient
{
    partial class SAPImportTest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SAPImportTest));
            this.txbLog = new System.Windows.Forms.TextBox();
            this.btnXML = new System.Windows.Forms.Button();
            this.txbXMLFile = new System.Windows.Forms.TextBox();
            this.txbXML = new System.Windows.Forms.TextBox();
            this.btnSendXML = new System.Windows.Forms.Button();
            this.txbOrgaSystemCode = new System.Windows.Forms.TextBox();
            this.txbLocaSystemCode = new System.Windows.Forms.TextBox();
            this.txbPrivateKey = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnSetHeader = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txbLog
            // 
            this.txbLog.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txbLog.Location = new System.Drawing.Point(8, 470);
            this.txbLog.Multiline = true;
            this.txbLog.Name = "txbLog";
            this.txbLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txbLog.Size = new System.Drawing.Size(990, 119);
            this.txbLog.TabIndex = 0;
            // 
            // btnXML
            // 
            this.btnXML.Location = new System.Drawing.Point(264, 6);
            this.btnXML.Name = "btnXML";
            this.btnXML.Size = new System.Drawing.Size(82, 23);
            this.btnXML.TabIndex = 5;
            this.btnXML.TabStop = false;
            this.btnXML.Text = "Load XML";
            this.btnXML.UseVisualStyleBackColor = true;
            this.btnXML.Click += new System.EventHandler(this.btnXML_Click);
            // 
            // txbXMLFile
            // 
            this.txbXMLFile.Location = new System.Drawing.Point(8, 7);
            this.txbXMLFile.Name = "txbXMLFile";
            this.txbXMLFile.Size = new System.Drawing.Size(250, 20);
            this.txbXMLFile.TabIndex = 6;
            // 
            // txbXML
            // 
            this.txbXML.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txbXML.Location = new System.Drawing.Point(8, 34);
            this.txbXML.MaxLength = 3276700;
            this.txbXML.Multiline = true;
            this.txbXML.Name = "txbXML";
            this.txbXML.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txbXML.Size = new System.Drawing.Size(990, 405);
            this.txbXML.TabIndex = 10;
            // 
            // btnSendXML
            // 
            this.btnSendXML.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSendXML.Location = new System.Drawing.Point(525, 443);
            this.btnSendXML.Name = "btnSendXML";
            this.btnSendXML.Size = new System.Drawing.Size(474, 23);
            this.btnSendXML.TabIndex = 11;
            this.btnSendXML.TabStop = false;
            this.btnSendXML.Text = "Send XML to: *WebAddress*";
            this.btnSendXML.UseVisualStyleBackColor = true;
            this.btnSendXML.Click += new System.EventHandler(this.btnSendXML_Click);
            // 
            // txbOrgaSystemCode
            // 
            this.txbOrgaSystemCode.Location = new System.Drawing.Point(490, 7);
            this.txbOrgaSystemCode.Name = "txbOrgaSystemCode";
            this.txbOrgaSystemCode.Size = new System.Drawing.Size(54, 20);
            this.txbOrgaSystemCode.TabIndex = 12;
            // 
            // txbLocaSystemCode
            // 
            this.txbLocaSystemCode.Location = new System.Drawing.Point(669, 7);
            this.txbLocaSystemCode.Name = "txbLocaSystemCode";
            this.txbLocaSystemCode.Size = new System.Drawing.Size(60, 20);
            this.txbLocaSystemCode.TabIndex = 13;
            // 
            // txbPrivateKey
            // 
            this.txbPrivateKey.Location = new System.Drawing.Point(799, 7);
            this.txbPrivateKey.Name = "txbPrivateKey";
            this.txbPrivateKey.Size = new System.Drawing.Size(100, 20);
            this.txbPrivateKey.TabIndex = 14;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(353, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(131, 13);
            this.label1.TabIndex = 15;
            this.label1.Text = "Organization_SystemCode";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(550, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(113, 13);
            this.label2.TabIndex = 16;
            this.label2.Text = "Location_SystemCode";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(735, 11);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 17;
            this.label3.Text = "PrivateKey";
            // 
            // btnSetHeader
            // 
            this.btnSetHeader.Location = new System.Drawing.Point(917, 6);
            this.btnSetHeader.Name = "btnSetHeader";
            this.btnSetHeader.Size = new System.Drawing.Size(82, 23);
            this.btnSetHeader.TabIndex = 18;
            this.btnSetHeader.TabStop = false;
            this.btnSetHeader.Text = "Set Header";
            this.btnSetHeader.UseVisualStyleBackColor = true;
            this.btnSetHeader.Click += new System.EventHandler(this.btnSetHeader_Click);
            // 
            // SAPImportTest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1004, 598);
            this.Controls.Add(this.btnSetHeader);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txbPrivateKey);
            this.Controls.Add(this.txbLocaSystemCode);
            this.Controls.Add(this.txbOrgaSystemCode);
            this.Controls.Add(this.btnSendXML);
            this.Controls.Add(this.txbXML);
            this.Controls.Add(this.txbXMLFile);
            this.Controls.Add(this.btnXML);
            this.Controls.Add(this.txbLog);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SAPImportTest";
            this.Text = "SAP Import Test";
            this.Load += new System.EventHandler(this.SAPImportTest_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txbLog;
        private System.Windows.Forms.Button btnXML;
        private System.Windows.Forms.TextBox txbXMLFile;
        private System.Windows.Forms.TextBox txbXML;
        private System.Windows.Forms.Button btnSendXML;
        private System.Windows.Forms.TextBox txbOrgaSystemCode;
        private System.Windows.Forms.TextBox txbLocaSystemCode;
        private System.Windows.Forms.TextBox txbPrivateKey;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnSetHeader;
    }
}

