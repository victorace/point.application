﻿using System;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Configuration;
using System.Xml.Linq;
using System.Linq;
using System.Text.RegularExpressions;

namespace Point.Planbord.TestClient
{

    public partial class SAPImportTest : Form
    {
        public SAPImportTest()
        {
            InitializeComponent();
         
        }


        private void SAPImportTest_Load(object sender, EventArgs e)
        {
            try
            {
                txbXMLFile.Text = ConfigurationManager.AppSettings["XMLTestFile"].ToString();
                txbOrgaSystemCode.Text = ConfigurationManager.AppSettings["OrgaSystemCode"].ToString();
                txbLocaSystemCode.Text = ConfigurationManager.AppSettings["LocaSystemCode"].ToString();
                txbPrivateKey.Text = ConfigurationManager.AppSettings["PrivateKey"].ToString();
                btnSendXML.Text = btnSendXML.Text.Replace("*WebAddress*", ConfigurationManager.AppSettings["WebAddress"].ToString());
            }
            catch
            {
                addToLog("Check the .config-file. Not all keys are present or valid.");
            }
        }

        private void addToLog(string message)
        {
            txbLog.AppendText(String.Format("{0:HH:mm:ss} - {1}{2}", DateTime.Now, message, Environment.NewLine));
        }

        private void btnXML_Click(object sender, EventArgs e)
        {
            try
            {
                txbXML.Clear();

                XDocument xdoc = XDocument.Load(txbXMLFile.Text);

                txbXML.Text = xdoc.ToString();

            }
            catch(Exception ex)
            {
                addToLog(ex.Message);
            }
        }

        private void btnSendXML_Click(object sender, EventArgs e)
        {
            using (WebClient wc = new WebClient())
            {
                wc.Encoding = System.Text.Encoding.UTF8;
                wc.Headers.Add("Content-Type", "text/xml; charset=utf-8");


                wc.Headers.Add("SOAPAction", "http://point.planbord.nl/ImportDetails");

                string webaddress = ConfigurationManager.AppSettings["WebAddress"].ToString();

                addToLog(String.Format("Calling '{0}'", webaddress));

                string result = "";
                try
                {
                    result = wc.UploadString(webaddress, txbXML.Text);
                    XNamespace ns = "";
                    XDocument xdoc = XDocument.Parse(result);

                    Match match = new Regex(@"xmlns=""(.*?)""").Match(xdoc.ToString());
                    if (match.Success)
                    {
                        ns = match.Groups[1].Value;
                    }

                    var importdetailsresponse = xdoc.Descendants(ns + "ImportDetailsResponse").FirstOrDefault();
                    var importdetailsresult = importdetailsresponse.Element(ns + "ImportDetailsResult");


                    addToLog(importdetailsresult.Value);
                }
                catch (WebException ex)
                {
                    addToLog(ex.Message);
                    if (ex.Response != null)
                    {
                        addToLog(String.Format("Status Code: {0}", ((HttpWebResponse)ex.Response).StatusCode));
                        addToLog(String.Format("Status Description: {0}", ((HttpWebResponse)ex.Response).StatusDescription));
                        addToLog(String.Format("Response Body: {0}", new StreamReader(ex.Response.GetResponseStream()).ReadToEnd()));
                    }
                }
            }
        }
        

        private byte[] GetHash(string inputString)
        {
            HashAlgorithm algorithm = SHA1.Create();
            return algorithm.ComputeHash(Encoding.UTF8.GetBytes(inputString));
        }

        private string GetHashString(string inputString)
        {
            StringBuilder sb = new StringBuilder();
            foreach (byte b in GetHash(inputString))
                sb.Append(b.ToString("X2"));
            return sb.ToString();
        }

        private void btnSetHeader_Click(object sender, EventArgs e)
        {
            XNamespace ns = "";

            XDocument xdoc = XDocument.Parse(txbXML.Text);

            Match match = new Regex(@"xmlns=""(.*?)""").Match(xdoc.ToString());
            if (match.Success)
            {
                ns = match.Groups[1].Value;
            }

            var header = xdoc.Descendants(ns + "SAPHeader").FirstOrDefault();

            var datetimesent = DateTime.Now;

            header.Element(ns + "Organization_SystemCode").Value = txbOrgaSystemCode.Text;
            header.Element(ns + "Location_SystemCode").Value = txbLocaSystemCode.Text;
            header.Element(ns + "IdentificationHash").Value = WebService.Security.GetHashString(txbOrgaSystemCode.Text, txbPrivateKey.Text, datetimesent);
            header.Element(ns + "DateTimeSent").Value = datetimesent.ToString("o");

            txbXML.Text = xdoc.ToString();
            
        }

        
    }
    
}
