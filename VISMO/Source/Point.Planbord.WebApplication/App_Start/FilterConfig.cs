﻿using System.Web.Mvc;

namespace Point.Planbord.WebApplication
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleAndLogErrorAttribute());
        }

        public class HandleAndLogErrorAttribute : HandleErrorAttribute
        {
            public override void OnException(ExceptionContext filterContext)
            {
                // Log the filterContext.Exception details
                base.OnException(filterContext);
            }
        }
    }
}