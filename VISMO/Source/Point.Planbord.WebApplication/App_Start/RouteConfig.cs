﻿using Point.Planbord.Database.Models;
using System.Web.Mvc;
using System.Web.Routing;

namespace Point.Planbord.WebApplication
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Settings",
                url: "Settings/{parentType}/{parentId}",
                defaults: new { controller = "Settings", action = "Index", parentType = PlanbordSettingsParentType.Organization, parentId = 0 }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Planbord", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}