﻿using Point.Planbord.Database.Context;
using Point.Planbord.Database.Models;
using Point.Planbord.Database.Repository;
using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Point.Planbord.WebApplication
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            // Handling the changes ourselves:
            System.Data.Entity.Database.SetInitializer<Point.Planbord.Database.Context.PlanbordContext>(null);
            System.Data.Entity.Database.SetInitializer<Point.Planbord.Database.Context.LoggingContext>(null);

            AreaRegistration.RegisterAllAreas();

            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception exception = Server.GetLastError();
            try
            {
                new Point.Planbord.WebApplication.Controllers.LoggingController().Log(Server.GetLastError());

                if ( !Request.IsLocal )
                    Server.ClearError();
            }
            catch { /* ignore */ }
        }

        protected void Application_BeginRequest()
        {
            if (!HttpContext.Current.Request.IsSecureConnection)
            {
                Response.Redirect("https://" + Request.ServerVariables["HTTP_HOST"]
                                             + HttpContext.Current.Request.RawUrl);
            }
        }
    }
}
