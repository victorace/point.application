﻿$(document).ready(function () {
    $('input#LastName').attr('data-val-required', 'Achternaam is verplicht');
    $('input#LastName').attr('data-val', 'true');
    $('form#NurseForm').removeData('validator').removeData('unobtrusiveValidation');
    $.validator.unobtrusive.parse('form#NurseForm');
});