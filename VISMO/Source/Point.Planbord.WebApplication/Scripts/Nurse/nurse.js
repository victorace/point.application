﻿$(document).ready(function () {

    $(["CreateNurse", "EditNurse"]).each(function (ix, el) {
        $('body').append($('<div/>').attr('id', 'dialog' + el).css('display', 'none'));
    });

    $("#dialogEditNurse").dialog({
        height: 450,
        buttons: [deleteButton, closeButton, saveButton]
    });

    $("#dialogCreateNurse").dialog({
        height: 450,
        buttons: [closeButton, saveButton]
    });

    BindDialog(".EditNurse", "#dialogEditNurse", 440);
    BindDialog(".CreateNurse", "#dialogCreateNurse", 440);

    $('#dataTable').dataTable(administrationDataTableOptions);
});