﻿$.extend($.fn.dataTableExt.oSort, { "num-html-pre": function (a) { var x = String(a).replace(/<[\s\S]*?>/g, ""); return parseFloat(x); }, "num-html-asc": function (a, b) { return ((a < b) ? -1 : ((a > b) ? 1 : 0)); }, "num-html-desc": function (a, b) { return ((a < b) ? 1 : ((a > b) ? -1 : 0)); } });

$(document).ready(function () {

    var opt = {
        "sScrollY": ($(window).height() - $('#mainwrap').offset().top - 38) + "px", // Math.min(832, ($(window).height() - $('#mainwrap').offset().top - 38)) + "px",
        "sDom": "t",
        "bAutoWidth": false,
        "iDisplayLength": 50,
        "oLanguage": { "sEmptyTable": "Geen data beschikbaar momenteel !" },
        "aoColumnDefs": [{ "aTargets": [1], "iDataSort": 2 },
                         { "aTargets": [6], "iDataSort": 7 },
                         { "aTargets": [8], "iDataSort": 9 },
                         { "aTargets": [10], "iDataSort": 11 }
        ], "aaSorting": [[4, 'asc']]



    };

    // Set column-widths according to their spec'd in the th
    $('#planbordTable tr th').each(function (ix, el) {

        var visWidth = $(this).width() - parseInt($(this).css("padding-left"), 10) - parseInt($(this).css("padding-right"), 10);

        if (visWidth < 0) visWidth = 0;
        opt.aoColumnDefs.push({ "sWidth": visWidth + "px", "aTargets": [ix] });

        if ($(this).width() < 0) {
            opt.aoColumnDefs.push({ "bVisible": false, "aTargets": [ix] });
        }

        if ($.trim($(this)[0].innerText).length <= 0 || $(this).hasClass("sorting_disabled")) {
            opt.aoColumnDefs.push({ "bSortable": false, "aTargets": [ix] });
        }

        
    });


    var expr = new RegExp('>[ \t\r\n\v\f]*<', 'g');
    var tbhtml = $('#planbordTable').html();
    //$('#planbordTable').html(tbhtml.replace(expr, '><'));


    var oTable = $('#planbordTable').dataTable(opt);

    $(window).bind("resize", function (e) {
        var availHeight = ($(window).height() - $('#mainwrap').offset().top );
        $('#mainwrap').css("height", availHeight + "px").css("overflow", "none");
        $('.dataTables_scrollBody').css("height", availHeight-38 + "px").css("overflow", "none");
    }).resize();


});