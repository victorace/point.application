﻿$(document).ready(function () {

    var startColumn = 3;
    var totalArray = new Array();
    var totalTR = $("TR[id^='group-id']");
                
    var colspan = parseInt(totalTR.find("TD:first").attr("colspan"),10);

    totalTR.find("TD:first").addClass("left").css("text-indent","10px").removeAttr("colspan").attr("colspan", startColumn) ;

    for (var i = startColumn; i < colspan ; i++) {
        totalTR.append($("<td />").addClass("group"));
        totalArray[i] = 0;
    }



    $("#planbordTable TR").each(function (ix, el) {
        if (ix <= 1) return true; //continue with the next 

        $(this).find("td").each(function (ixtd, eltd) {
            if (ixtd < startColumn) return true;
            totalArray[ixtd] += parseInt($(this).text(), 10);
            if ($(this).text() == "0" && $(this).attr("data-keepzero") != 'true') 
                $(this).text('');
        });
                    
    });
                
    for (var i = startColumn; i < colspan-1 ; i++) {
        $(totalTR.find("TD")[i-startColumn+1]).text(totalArray[i]);
    }


    LoadCommonDialogs();


    $("#dialogFulltext").dialog({
        height: 280,
        buttons: [closeButton],
        open: function (type, data) {
            var opener = $(this).data('opener');

            var parentTR = $(opener).parent("tr");

            var openedDialog = $("#dialogFulltext");
            openedDialog.find("#dialogFulltextPatient").text(parentTR.attr("data-departmentname"));
            openedDialog.find("#dialogFulltextBed").text(parentTR.attr("data-specialism"));
            openedDialog.find("#dialogFulltextSubheader").text($(opener).attr('data-title'));
            openedDialog.find("#dialogFulltextSubheader2").html($(opener).attr('data-fulltext'));

            var parent = $(this);
            $('.ui-widget-overlay').bind('click', function () { parent.dialog('close'); });
        },
        close: function (type, data) {
            //   $(this).empty();
        }
    });

    HandleSumColumns();

                
                
});


function HandleSumColumns() {
    $([".showProductie"]).each(function (ix, el) {

        $(el).each(function () {
            BindFulltextClick($(this));
        });

    });
}

