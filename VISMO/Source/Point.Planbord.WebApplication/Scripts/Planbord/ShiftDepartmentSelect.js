﻿function showShiftResetButton(shiftID) {
    $(".ShiftAction").css("visibility", "hidden");
    $("#ShiftReset_" + shiftID).css("visibility", "visible");
    $("#ShiftDelete_" + shiftID).css("visibility", "visible");
}

function changeShiftDepartment(shiftID, departmentID) {
    $.post(ShiftDepartmentSelectURL,
        { shiftId: shiftID, departmentId: departmentID },
        function (data) {
            location.replace(ShiftDepartmentReloadURL);
        }
    );
}

$("#dialogShiftDepartmentConfirm").dialog({
    height: 300,
    buttons: [closeButton,
        {
            "text": "Ja",
            "class": "buttonOk",
            "click": function () {

                $('.overlay').show();

                var opener = $(this).data('opener');

                var shiftID = $("input[name=Shift]:checked").val();
                var departmentID = $("input[name=Department]:checked").val();

                $.post($(opener).attr("data-url"),
                      { shiftID: shiftID, departmentId: departmentID },
                      function (data) { }
                );

                $("#dialogShiftDepartment").dialog("close");
                $("#dialogShiftDepartmentConfirm").dialog("close");

                changeShiftDepartment(shiftID, departmentID);
            }
        }
    ],
    open: function (type, data) {
        var opener = $(this).data('opener');
        var openedDialog = $("#dialogShiftDepartmentConfirm");
        openedDialog.find("#dialogConfirmHeader").text("Let op!");
        openedDialog.find("#dialogConfirmSubheader2").html($(opener).attr('data-confirm'));
        var parent = $(this);
        $('.ui-widget-overlay').bind('click', function () { parent.dialog('close'); });




    },
    close: function (type, data) {
    }
});


$(document).ready(function () {

    BindDialog(".ShiftAction", "#dialogShiftDepartmentConfirm", 440);

    $('input:radio[name="Shift"]').change(function (ev) {
        showShiftResetButton($(this).val());
    });

    showShiftResetButton(CurrentShiftID);

});