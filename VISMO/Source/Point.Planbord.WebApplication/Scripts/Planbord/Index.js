﻿var columnDataInitialized = false;
function initColumnData() {
    $('#planbordTable tr th').each(function (ix, el) {
        var visWidth = $(this).width(); 
        if (visWidth < 0) visWidth = 0;
        columnWidths.push(visWidth);

        columnNames.push($(this).attr("title"));

        columnDisplayTypes.push(parseInt($(this).attr("data-displaytype") || 1, 10));

        columnDataInitialized = true;
    });
}
//Store the columnwidth (defined in the header) for future use
var columnWidths = new Array();
function getColumnWidth(colIndex) {
    if (!columnDataInitialized) initColumnData();
    return columnWidths[colIndex];
}
//Store the columnnames (defined in the header) for future use
var columnNames = new Array()
function getColumnName(colIndex) {
    if (!columnDataInitialized) initColumnData();
    return columnNames[colIndex];
}
//Store the displaytype (defined in the header) for future use
//Visible = 1, Invisible = 2, Ellipsis = 3
var columnDisplayTypes = new Array()
function getColumnDisplayType(colIndex) {
    if (!columnDataInitialized) initColumnData();
    return columnDisplayTypes[colIndex];
}


function HandleVisibility(columnDisplayType, el, fnHandleVisibility) {
    

    switch (columnDisplayType) { 
        case 1: //Visible
        default:
            if (fnHandleVisibility != null)
                fnHandleVisibility; //call the passed function to handle the specific column
            break;
        case 2: //Invisible
            TextToInvisible(el); 
            break;
        case 3: //Ellipsis
            TextToEllipsis(el[0]);
            break;
    }
}



$(document).ready(function () {

    //Add the div's for the several dialogs
    $(["dialogBed", "dialogRestriction", "dialogVital", "dialogIsolation", "dialogQScore", "dialogDetails", "dialogAppointment", "dialogMobility", "dialogShiftDetail", "dialogSecurity", "dialogEwsScore"]).each(function (ix, el) {
        $('body').append($('<div/>').attr('id', el).css('display', 'none'));
    });

    // Initialize the 'empty' dialogs
    $(["Restriction", "Vital", "Isolation", "QScore", "EwsScore"]).each(function (ix, el) {
        $("#dialog" + el).dialog();
    });

    // Initialize the dialogs with some more functionality
    $("#dialogAppointment").dialog({
        height: 225,
        buttons: [
            closeButton,
            {
                "text": "Opslaan",
                "class": "buttonSave",
                "click": function () {
                    var opener = $(this).data('opener');
                    $.post($(opener).attr('data-url'),
                            $("#CodeForm").serialize(),
                            function (data) {
                                $("#dialogAppointment").dialog("close");
                                showText(opener, data.Code.CodeValue);
                            });
                }
            }
        ]
    });


    $("#dialogDetails").dialog({
        height: 405,
        buttons: [
            closeButton,
            {
                "text": "Opslaan",
                "class": "buttonSave",
                "click": function () {
                    var opener = $(this).data('opener');
                    $.post($(opener).attr('data-url'),
                            $("#CodeForm").serialize(),
                            function (data) {
                                $("#dialogDetails").dialog("close");
                                showIconDetails(opener, data.Code.CodeValue);
                            });
                }
            }
        ]
    });

    $("#dialogMobility").dialog({
        height: 205,
        buttons: [
            closeButton,
            {
                "text": "Opslaan",
                "class": "buttonSave",
                "click": function () {
                    var opener = $(this).data('opener');
                    $.post($(opener).attr('data-url'),
                            $("#CodeForm").serialize(),
                            function (data) {
                                $("#dialogMobility").dialog("close");
                                showIconMobility(opener, data.Code.CodeValue);
                            });
                }
            }
        ]
    });

    $("#dialogBed").dialog({
        height: 265,
        buttons: [
            closeButton,
            {
                "text": "Opslaan",
                "class": "buttonSave",
                "click": function () {
                    var opener = $(this).data('opener');

                    $.post($(opener).attr('data-url'),
                            $("#CodeForm").serialize(),
                            function (data) {
                                $("#dialogBed").dialog("close");
                                if ($(opener).hasClass('showBed'))
                                    opener = $(opener).closest('tr').find('.showBedIcon');
                                showIconBed(opener, data.Code.CodeValue, $(opener).attr('data-bedpatientstatus'));
                            });
                }
            }
        ]
    });



    $("#dialogShiftDetail").dialog({
        height: 630,
        buttons: [
             closeButton,
             {
                 "text": "Opslaan",
                 "class": "buttonSave",
                 "click": function () {
                     var opener = $(this).data('opener');
                     $.post($(opener).attr('data-url'),
                             $("#ShiftDetailForm").serialize(),
                             function (data) {
                                 $("#dialogShiftDetail").dialog("close");
                                 handleShiftDetail(opener, data.NewShiftDetails);
                             });
                 }
             }
        ]
    })

    $("#dialogFulltext").dialog({
        height: 220,
        buttons: [closeButton],
        open: function (type, data) {

            if (!PinCheck($(this))) return false;

            var opener = $(this).data('opener');
            var parentTR = $(opener).parent("tr");
            var openedDialog = $("#dialogFulltext");
            openedDialog.find("#dialogFulltextPatient").text(parentTR.attr("data-patient"));
            openedDialog.find("#dialogFulltextBed").text(parentTR.attr("data-bed"));
            openedDialog.find("#dialogFulltextSubheader").text(getColumnName(parentTR.children().index($(opener))));
            openedDialog.find("#dialogFulltextSubheader2").html("");

            var fulltext = $(opener).attr('data-fulltext');
            if (typeof fulltext != "undefined" && fulltext != "") {
                openedDialog.find("#dialogFulltextSubheader2").html(fulltext);
            }
            else {
                var icons = $(opener).attr('data-icons');
                if (typeof icons != "undefined" && icons != "") { // Only wanna know if it's an "icon"-column
                    openedDialog.find("#dialogFulltextSubheader2").html($(opener).html());
                }
            }

            var parent = $(this);
            $('.ui-widget-overlay').bind('click', function () { parent.dialog('close'); });

        }
    });


    $("#dialogPatientOverview").dialog({
        height: 720,
        buttons: [closeButton],
        open: function (type, data) {

            if (!PinCheck($(this))) return false;

            var skipColumns = ["Stagiaire"];
            var iconColumns = ["Bed Icoon", "APO IN", "APO EXIT", "AOA Patient", "Bijzonderheden", "Mobiliteit"]

            var opener = $(this).data('opener');
            var row = $(opener).parent("tr");
            var openedDialog = $("#dialogPatientOverview");

            var patientDetails = openedDialog.find("#patientDetails");
            patientDetails.empty();
            openedDialog.find("#dialogFulltextPatient").text(row.attr("data-patient"));
            openedDialog.find("#dialogFulltextBed").text(row.attr("data-bed"));


            $.each(row.children(), function (ix, el) {
                var columnName = getColumnName(ix);

                var url = $(el).data("url"); 
                var fulltext = $(el).data("fulltext"); 

                if (typeof columnName != "undefined" && columnName != "" && $.inArray(columnName, skipColumns) == -1) {


                    var divleft = $("<div />").addClass("leftColumn").html(columnName);
                    var divright = $("<div />").addClass("rightColumn").html("...");

                    if ($.inArray(columnName, iconColumns) > -1) {
                        divright.html($(el).html());
                    }
                    else if (typeof fulltext != "undefined" && fulltext != "") {
                        divright.html(fulltext);
                    }
                    else if (typeof url != "undefined" && url != "") {
                        $.ajax({
                            url: url,
                            type: 'GET',
                            async: true,
                            dataType: "text",
                            success: function (data) {
                                var htmldata = $("<html />").html(data);
                                var contents = "";
                                if (contents.length == 0)
                                {
                                    contents = htmldata.find(".dialogDetails");
                                    if (contents.length > 0) {
                                        divright.html(contents.html());
                                    }   
                                }
                                if (contents.length == 0) {
                                    var contents = htmldata.find("#fieldHolder");
                                    if (contents.length > 0) {
                                        divright.html(contents.html());
                                    }
                                }
                            },
                            error: function (data) {
                                divright.html("&nbsp;");
                            }
                        });
                    }
                    else {
                        divright.html("&nbsp;");
                    }
                    patientDetails.append($("<div />").addClass("parentColumns").append(divleft).append(divright));
                }


            });

            var parent = $(this);
            $('.ui-widget-overlay').bind('click', function () { parent.dialog('close') });

        }
        

    });

    LoadCommonDialogs();

    // Bind the dialog to their opener (table-cell)
    BindDialog(".showBed", "#dialogBed", 400, true);
    BindDialog(".showBedIcon", "#dialogBed", 400, true);
    BindDialog(".showRestriction", "#dialogRestriction", 350, true);
    BindDialog(".showVital", "#dialogVital", 430, true);
    BindDialog(".showIsolation", "#dialogIsolation", 430, true);
    BindDialog(".showQScore", "#dialogQScore",430, true);
    BindDialog(".showDetails", "#dialogDetails", 394);
    BindDialog(".showAppointment", "#dialogAppointment");
    BindDialog(".showMobility", "#dialogMobility", (4 + (mobilityIconsCount * 78)));
    BindDialog(".showFirstNurse", "#dialogShiftDetail", 376);
    BindDialog(".showSecondNurse", "#dialogShiftDetail", 376);
    BindDialog(".showIntern", "#dialogShiftDetail", 376);
    BindDialog(".showEwsScore", "#dialogEwsScore", 430, true);

    // Checkbox column
    $('.showMultiNurse').click(function () {
        var values = $.map($("input[type='checkbox'][id='chbBedID']:checked"), function (el, ix) { return "id=" + $(el).val(); }).join('&');

        if (values == "") {
            $(this).attr('data-alert', 'Maak eerst een selectie<br />van één of meerdere bedden.');
            $("#dialogAlert").data('opener', this)
                             .dialog("option", "width", 330)
                             .dialog('open');
            return;
        }

        values += "&shiftID=" + $(this).attr("data-shiftid");
        $(this).attr('data-values', values);
        $("#dialogShiftDetail").data('opener', this)
                                   .dialog('option', {
                                       close: function (type, data) {
                                           if ((DoPinCheck == '1' && pinValid == true) || DoPinCheck == '0') {
                                               $("input[type='checkbox'][id='chbBedID']").prop('checked', false);
                                           }
                                       }
                                   })
                                   .dialog("option", "width", 375)
                                   .dialog('open');

    });



    // Fill up the columns
    HandleTextColumns();
    HandleIconColumns();
    HandleNurseColumns(); 

    // Extend the clickable area of the checkboxes
    $("input[type='checkbox'][id='chbBedID']").each(function () {
        var chb = $(this);
        chb.click(function (e) { e.stopPropagation(); });
        $(this).closest("td").click(function (e) {
            chb.prop('checked', !chb.prop('checked'));
        }).css("cursor", "pointer");
    });

    $(document).bind("contextmenu", function (e) {
        if (location.hostname != "localhost") {
            alert('VISMO\n\nVisuele Interactieve Stuurinformatie\nvoor Multidisciplinaire Ondersteuning\n\n\u00A9 2013-' + new Date().getFullYear() + ' TwoBuild');
            return false;
        }
    });

});

function HandleNurseColumns() {

    $([".showFirstNurse", ".showSecondNurse", ".showIntern"]).each(function (ix, el) {
        var parentTR = $(el).first().parent("tr");
        var columnIndex = parentTR.children().index($(el));
        var columnWidth = getColumnWidth(columnIndex);
        var columnDisplayType = getColumnDisplayType(columnIndex);

        if (el != ".showIntern")
            columnWidth = columnWidth - 80;

        $(el).each(function () {
            switch (columnDisplayType) { //Visible = 1, Invisible = 2, Ellipsis = 3
                case 1:
                default:
                    $(this).empty();
                    TextToColumnWidth(columnWidth, $(this)[0]); // using the plain JS-object
                    showPagerPauseVisit($(this), $(this).attr("data-pagerpausevisit"));
                    if ($(this).css("color") != "rgb(255, 0, 0)" && $(this).css("color") != "red")
                        $(this).addClass(ShiftClass);
                    break;
                case 2:
                    TextToInvisible($(this));
                    break;
                case 3:
                    TextToEllipsis($(this)[0]);
                    break;
            }
        });
    });
}

function HandleIconColumns() {

    $([".showBedIcon", ".showAOAIcon", ".showFaseIcon", ".showApoInIcon", ".showApoExitIcon", ".showEwsScore", ".showRestriction", ".showVital", ".showIsolation", ".showQScore", ".showDetails", ".showMobility"]).each(function (ix, el) {
            var columnIndex = $(el).first().parent("tr").children().index($(el));
        var columnIndex = $(el).first().parent("tr").children().index($(el));
        var columnDisplayType = getColumnDisplayType(columnIndex);

        $(el).each(function () {
            var dataicons = $(this).attr('data-icons');
            var fnHandleVisibility = null;
            switch (el) {
                case ".showBedIcon":
                    fnHandleVisibility = showIconBed($(this), dataicons, $(this).attr('data-bedpatientstatus')); break;
                case ".showRestriction":
                    fnHandleVisibility = showIconRestriction($(this), dataicons); break;
                case ".showVital":
                    fnHandleVisibility = showIconVital($(this), dataicons); break;
                case ".showIsolation":
                    fnHandleVisibility = showIconIsolation($(this), dataicons); break;
                case ".showQScore":
                    fnHandleVisibility = showIconQScore($(this), dataicons); break;
                case ".showDetails":
                    fnHandleVisibility = showIconDetails($(this), dataicons); break;
                case ".showMobility":
                    fnHandleVisibility = showIconMobility($(this), dataicons); break;
                case ".showAOAIcon":
                    fnHandleVisibility = showIconAOA($(this), dataicons); break;
                case ".showFaseIcon":
                    fnHandleVisibility = showIconFase($(this), dataicons, $(this).attr('data-flowdefinition')); break;
                case ".showApoInIcon":
                    fnHandleVisibility = showIconApoIn($(this), dataicons); break;
                case ".showApoExitIcon":
                    fnHandleVisibility = showIconApoExit($(this), dataicons); break;
                case ".showEwsScore":
                    fnHandleVisibility = showIconEwsScore($(this), dataicons, $(this).attr('data-ewsdate'), $(this).attr('data-ewsaction')); break;
            }
            HandleVisibility(columnDisplayType, $(this), fnHandleVisibility);

        });
    });
}

function HandleTextColumns() { 
    $([".showPatientOverview", ".showBed", ".showSpecialism", ".showStartTime", ".showEndTime", ".showHospitalization", ".showDiagnose", ".showAppointment", ".showDischarge", ".showApoInIcon", ".showApoExitIcon", ".showAOAIcon"]).each(function (ix, el) {
        var columnIndex = $(el).first().parent("tr").children().index($(el));
        var columnWidth = getColumnWidth(columnIndex);
        var columnDisplayType = getColumnDisplayType(columnIndex);

        $(el).each(function () {
            var fnHandleVisibility = null;
            switch (el) {
                case ".showDiagnose":
                case ".showAppointment":
               
                    fnHandleVisibility = TextToColumnWidth(columnWidth, $(this)[0]); break;
                    //case ".showPatientName":
                case ".showPatientOverview":
                case ".showSpecialism":
                case ".showStartTime":
                case ".showHospitalization":
                case ".showEndTime":
                case ".showDischarge":
               
                    break; //leave 'm
            }

            HandleVisibility(columnDisplayType, $(this), fnHandleVisibility);

            if ($(this).text != "") {
                if (el == ".showAppointment" || el==".showBed" ) {
                    //leave it
                }
                else if (el== ".showDischarge" ) {
                    BindFulltextClick($(this), 600);
                }
                else if (el == ".showPatientOverview") {
                    BindPatientOverviewClick($(this));
                }
                else {
                    BindFulltextClick($(this));
                }
            }
        });

    });
}



