﻿$(function () {

    $.keyboard.keyaction.sec1 = function (base) {
        base.$el.closest("#dialogSecurity").parent().find(".buttonSave").click();
        base.close(true);
        return false;
    }

    $.keyboard.keyaction.sec2 = function (base) {
        base.$preview.val('');
        return false;
    }

    keyboardField.keyboard({
        usePreview: false,
        autoAccept: true,
        acceptValid: true,
        initialFocus: true,
        preventPaste: true,
        openOn: 'click',
        stickyShift: false,
        lockInput: true,
        maxLength: 4,
        layout: 'custom',
        customLayout: {
            'default': [
            '1 2 3',
            '4 5 6',
            '7 8 9 ',
            '{sec2} 0 {sec1}'
            ]
        },
        display: {
            'sec1': '\u2714:Ok',
            'sec2': '\u21a4:Annuleren' // \u2716 X
        },
        position: {
            at2: 'center bottom+10',
            collision: 'flipfit flipfit'
        }
    });

    keyboardField.click();





});