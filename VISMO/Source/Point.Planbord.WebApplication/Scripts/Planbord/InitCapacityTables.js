﻿$(document).ready(function () {

    var opt = {
        "sScrollY": ($(window).height() - $('#mainwrap').offset().top - 38) + "px", 
        "sDom": "t",
        "bAutoWidth": false,
        "iDisplayLength": 50,
        "oLanguage": { "sEmptyTable": "Geen data beschikbaar momenteel !" },
        "aoColumnDefs": [],
        "aaSorting": [[0, 'asc']]
    };

    // Set column-widths according to their spec'd in the th
    $('#planbordTable tr th').each(function (ix, el) {

        var visWidth = $(this).width() - parseInt($(this).css("padding-left"), 10) - parseInt($(this).css("padding-right"), 10);
        if (visWidth < 0) visWidth = 0;
        opt.aoColumnDefs.push({ "sWidth": visWidth + "px", "aTargets": [ix] });

        if ($(this).width() < 0) {
            opt.aoColumnDefs.push({ "bVisible": false, "aTargets": [ix] });
        }

        if ($.trim($(this)[0].innerText).length <= 0 || $(this).hasClass("sorting_disabled")) {
            opt.aoColumnDefs.push({ "bSortable": false, "aTargets": [ix] });
        }

        
    });


    var oTable = $('#planbordTable').dataTable(opt).rowGrouping({ iGroupingColumnIndex: 0, bHideGroupingColumn: true, bSetGroupingClassOnTR: false, bExpandableGrouping: true });

    /*$('#planbordTable tr th').each(function (ix, el) {

        if ($(this).attr("data-displaytype") == "2") {
            oTable.fnSetColumnVis(ix, false);
        }
    });*/

    //$('#mainwrap').css("height", ($(window).height() - $('#mainwrap').offset().top) + "px").css("overflow", "none");

    $(window).bind("resize", function (e) {
        //alert('x');
        //window.location.href = window.location.href;
        var availHeight = ($(window).height() - $('#mainwrap').offset().top );
        $('#mainwrap').css("height", availHeight + "px").css("overflow", "none");
        $('.dataTables_scrollBody').css("height", availHeight-38 + "px").css("overflow", "none");
    }).resize();


});