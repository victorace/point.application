﻿
function handleNurseInfo(opener, nurse) {

    var id = opener.prop("id")[opener.prop("id").length - 1];

    if (nurse == null || nurse.NurseID == null || nurse.NurseID <= 0) {
        $("#details" + id).hide();
    }
    else {
        $("#details" + id).show();
        $("#nurseID" + id).find('option[value="' + nurse.NurseID + '"]').text(nurse.Description);
        $("#pager" + id).html("Tel:&nbsp;" + (nurse.Pager != null ? (nurse.Pager.PagerNumber || "-") : "-"));
        $("#pause" + id).html("P" + (nurse.Pause || "-"));
        $("#visit" + id).html("V" + (nurse.Visit || "-"));
        $("#workload" + id).html("W" + (nurse.Workload || "-"));
        $("#details" + id).attr("data-id", nurse.NurseID);
    }

};

function addNurseToDropdown(opener) {
    var id = opener.prop("id")[opener.prop("id").length - 1];

    $.post(opener.attr('data-url'),
        { description: opener.val() },
        function (data) {
            opener.val('');
            $("#nurseID" + id).append(
                    $('<option></option>').val(data.NurseID).html(data.Description).prop('selected', true)
            ).change();
        }
    );
}


$("#nurseID1, #nurseID2").change(function (ev) {
    var opener = $(this);
    if (opener.val() == "") { handleNurseInfo(opener, null); };
    $.post(opener.attr('data-url'),
        { id: opener.val() },
        function (data) {
            handleNurseInfo(opener, data);
        }
    );
}).change();

$("#nurse1, #nurse2").change(function (ev) { addNurseToDropdown($(this)); });

$('body').append($('<div/>').attr('id', 'dialogEditNurse').css('display', 'none'));


$("#dialogEditNurse").dialog({
    height: 330,
    buttons: [
        {
            "text": "Verwijder",
            "class": "buttonTrash",
            "click": function () {
                var opener = $(this).data('opener');
                $.post($(opener).attr('data-trashurl'),
                            { id: $(opener).attr('data-id') },
                            function (data) {
                                $("#dialogEditNurse").dialog("close");
                                $("#dialogNurseForPatient").dialog("close").dialog("open");
                            });
            }
        },
        closeButton,
        {
            "text": "Opslaan",
            "class": "buttonSave",
            "click": function () {
                var opener = $(this).data('opener');
                $.post($(opener).attr('data-url'),
                        $("#NurseForm").serialize(),
                        function (data) {
                            $("#dialogEditNurse").dialog("close");
                            handleNurseInfo($(opener), data);
                        });
            }
        }
    ]
});


BindDialog("#details1, #details2", "#dialogEditNurse", 310);
