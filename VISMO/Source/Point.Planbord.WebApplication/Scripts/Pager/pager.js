﻿$(document).ready(function () {

    $(["CreatePager", "EditPager"]).each(function (ix, el) {
        $('body').append($('<div/>').attr('id', 'dialog' + el).css('display', 'none'));
    });

    $("#dialogEditPager").dialog({
        height: 240,
        buttons: [deleteButton, closeButton, saveButton]
    });

    $("#dialogCreatePager").dialog({
        height: 240,
        buttons: [closeButton, saveButton]
    });

    BindDialog(".EditPager", "#dialogEditPager");
    BindDialog(".CreatePager", "#dialogCreatePager");

    $('#dataTable').dataTable(administrationDataTableOptions);
});