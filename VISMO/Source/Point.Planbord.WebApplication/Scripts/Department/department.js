﻿$(document).ready(function () {

    $(["CreateDepartment", "EditDepartment"]).each(function (ix, el) {
        $('body').append($('<div/>').attr('id', 'dialog' + el).css('display', 'none'));
    });

    $("#dialogEditDepartment").dialog({
        height: 412,
        buttons: [deleteButton, closeButton, saveButton]
    });

    $("#dialogCreateDepartment").dialog({
        height: 412,
        buttons: [closeButton, saveButton]
    });

    BindDialog(".EditDepartment", "#dialogEditDepartment", 440);
    BindDialog(".CreateDepartment", "#dialogCreateDepartment", 440);

    $('#dataTable').dataTable(administrationDataTableOptions);
});