﻿
var deleteButton = {
    "text": "Verwijder",
    "class": "buttonTrash",
    "click": function () {
        var owner = $(this);
        var opener = owner.data('opener');
        $.post($(opener).attr('data-trashurl'),
                    { id: $(opener).attr('data-id') },
                    function (data) {
                        owner.dialog("close");
                        location.reload();
                    });
    }
};

// closeButton defined in scripts/Shared/_LayOut_Planbord.js

var saveButton = {
    "text": "Opslaan",
    "class": "buttonSave",
    "click": function () {
        var owner = $(this);
        var opener = owner.data('opener');
        var entity = $(opener).attr('data-entity');
        if ($("#" + entity + "Form").validate().form()) {
            $.post($(opener).attr('data-url'),
                    $("#" + entity + "Form").serialize(),
                     function (data) {
                         owner.dialog("close");
                         location.reload();
                     });
        }
    }
};

var administrationDataTableOptions = {
    "bStateSave": true,
    "sDom": "tip",
    "bAutoWidth": false,
    "iDisplayLength": 50,
    "oLanguage": {
        "sProcessing": "Een moment geduld a.u.b.",
        "sLengthMenu": "_MENU_ resultaten weergeven",
        "sZeroRecords": "Geen resultaten om weer te geven",
        "sInfo": "_START_ tot _END_ van _TOTAL_ resultaten",
        "sInfoEmpty": "Geen resultaten om weer te geven",
        "sInfoFiltered": " (gefilterd uit _MAX_ resultaten)",
        "sInfoPostFix": "",
        "sSearch": "Zoeken:",
        "sEmptyTable": "Geen resultaten om weer te geven",
        "sInfoThousands": ".",
        "sLoadingRecords": "Een moment geduld a.u.b. - bezig met laden...",
        "oPaginate": { "sFirst": "Eerste", "sLast": "Laatste", "sNext": "Volgende", "sPrevious": "Vorige" }
    }
};

