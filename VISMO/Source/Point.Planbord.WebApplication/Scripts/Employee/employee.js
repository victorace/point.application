﻿var asInitVals = new Array();
$(document).ready(function () {
    $(["CreateEmployee", "EditEmployee", "AccessEmployee"]).each(function (ix, el) {
        $('body').append($('<div/>').attr('id', 'dialog' + el).css('display', 'none'));
    });

    $("#dialogEditEmployee").dialog({
        height: 660,
        buttons: [deleteButton, closeButton, saveButton]
    });

    $("#dialogCreateEmployee").dialog({
        height: 660,
        buttons: [closeButton, saveButton]
    });

    $("#dialogAccessEmployee").dialog({
        height: 200,
        buttons: [closeButton, saveButton]
    });

    BindDialog(".CreateEmployee", "#dialogCreateEmployee", 440);
    BindDialog(".EditEmployee", "#dialogEditEmployee", 440);
    BindDialog(".AccessEmployee", "#dialogAccessEmployee", 440);

    var dataTableOptions = {
        "bStateSave": true,
        "sDom": "ftip",
        "bAutoWidth": false,
        "iDisplayLength": 50,
        "oLanguage": {
            "sProcessing": "Een moment geduld a.u.b.",
            "sLengthMenu": "_MENU_ resultaten weergeven",
            "sZeroRecords": "Geen resultaten om weer te geven",
            "sInfo": "_START_ tot _END_ van _TOTAL_ resultaten",
            "sInfoEmpty": "Geen resultaten om weer te geven",
            "sInfoFiltered": " (gefilterd uit _MAX_ resultaten)",
            "sInfoPostFix": "",
            "sSearch": "Zoeken:",
            "sEmptyTable": "Geen resultaten om weer te geven",
            "sInfoThousands": ".",
            "sLoadingRecords": "Een moment geduld a.u.b. - bezig met laden...",
            "oPaginate": { "sFirst": "Eerste", "sLast": "Laatste", "sNext": "Volgende", "sPrevious": "Vorige" }
        }
    };

   $('#dataTable').dataTable(dataTableOptions);
});