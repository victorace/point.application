﻿$(document).ready(function () {

    if ($('input[type=hidden][name=IsLockedOut]').val() === "True") {
        $('#dialogEditEmployee').parent().find('.ui-button:contains(' + saveButton.text + ')').hide();
        $('#dialogEditEmployee').parent().find('.ui-button:contains(' + deleteButton.text + ')').hide();
        $('#dialogEditEmployee :input').prop("disabled", "disabled");
    }

    $('#OrganizationID').on('change', function (e) {
        $.getJSON($('#LocationsFromOrganizationURL').attr('value'), { 'id': $(this).val() }, function (param) {
            $('#DepartmentID').empty();
            var locations = $('#LocationID').empty();
            $.each(param, function (index, param) {
                locations.append($('<option/>').attr('value', param.locationID).text(param.locationName));
            });
        }).done(function () {
            if ($('#hLocationID').val() !== '0')
                $('#LocationID').val($('#hLocationID').val()).trigger('change');
            else
                $('#LocationID option:first').trigger('change').attr('selected', 'selected');
        });
    });

    $('#LocationID').on('change', function (e) {
        $.getJSON($('#DepartmentsFromLocationURL').attr('value'), { 'id': $(this).val() }, function (param) {
            var departments = $('#DepartmentID').empty();
            $.each(param, function (index, param) {
                departments.append($('<option/>').attr('value', param.departmentID).text(param.departmentName));
            });
        }).done(function () {
            if ($('#hDepartmentID').val() !== '0')
                $('#DepartmentID').val($('#hDepartmentID').val());
            else
                $('#DepartmentID option:first').trigger('change').attr('selected', 'selected');
        });
    });

    if ($('#hOrganizationID').val() !== '0')
        $('#OrganizationID').val($('#hOrganizationID').val()).trigger('change');
    else
        $('#OrganizationID option:first').trigger('change').attr('selected', 'selected');
});