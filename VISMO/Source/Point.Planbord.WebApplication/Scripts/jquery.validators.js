﻿$(document).ready(function () {
    $.validator.addMethod('date',
            function (value, element, params) {
                if (this.optional(element)) {
                    return true;
                }
                return (Date.parseExact(value, "dd-MM-yyyy HH:mm") !== null);
            });
});