﻿$(document).ready(function () {

    if (!$.ui) { showErrorMessage("jQuery UI NOT loaded!"); return; }

    var errorMessages = [];
    function isFormValid()
    {
        errorMessages = [];

        var validElements = 0;
        $("input:file").each(function (i, e) {

            if (errorMessages.length) {
                return;
            }

            var filename = $(e).val();
            if (!filename) {
                return;
            }

            var validExtensions = ["jpg", "jpeg", "gif", "png"];
            var fileNameExt = filename.substr(filename.lastIndexOf(".") + 1);
            if ($.inArray(fileNameExt.toLowerCase(), validExtensions) == -1) {
                errorMessages.push("Een of meer iconen heeft een ongeldig bestandstype of formaat.");
                return;
            }

            validElements++;

        });

        if (!errorMessages.length && checkDuplicates()) {
            errorMessages.push("Er is twee keer hetzelfde bestand geselecteerd.");
        }

        if (!errorMessages.length && validElements !== 2) {
            errorMessages.push("U moet twee iconen selecteren.");
        }

        if (errorMessages.length) {
            errorMessages.unshift("Er is een fout opgetreden: ");
            showErrorMessage(errorMessages);
            return false;
        }

        return true;
    }

    function checkDuplicates()
    {
        var hasDuplicates = false;
        var lastFilename = '';
        $("input:file").each(function (i, e) {
            var filename = $(e).val();
            if (filename && filename == lastFilename) {
                hasDuplicates = true;
                return;
            }
            lastFilename = filename;
        });

        return hasDuplicates;
    }

    var formInsertOptions = {
        url: formRequestUrl,
        type: 'post',
        dataType: 'json',
        beforeSubmit: function () {
            if (!isFormValid()) {
                return false;
            }
        },
        success: function (data, status, xhr) {
            $("#dialog-insert").dialog("close");
            if (data.ErrorMessage) {
                showErrorMessage(data.ErrorMessage);
            }
            else {
                showSuccessMessage("Succesvol opgeslagen.");
            }
        }
    };


    $("#btnInsertAttachment").on("click", function () {
        $("#iconUploadForm").ajaxSubmit(formInsertOptions);
    });

    $("#btnOpslaan").on("click", function () {
        var submitdata = $("#iconUploadForm").serialize();
        var formIconProperties = {
            url: '/Icon/SaveIconProperties',
            type: 'POST',
            dataType: 'json',
            data: submitdata,
            success: function (data, status, xhr) {
                if (data.ErrorMessage) {
                    showErrorMessage(data.ErrorMessage);
                }
                else {
                    showSuccessMessage("Succesvol opgeslagen.");
                }
            }
        };
        $("#iconUploadForm").ajaxSubmit(formIconProperties);
    });
});