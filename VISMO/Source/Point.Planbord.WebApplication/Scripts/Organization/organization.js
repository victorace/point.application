﻿$(document).ready(function () {

    $(["CreateOrganization", "EditOrganization"]).each(function (ix, el) {
        $('body').append($('<div/>').attr('id', 'dialog' + el).css('display', 'none'));
    });

    $("#dialogEditOrganization").dialog({
        height: 276,
        buttons: [deleteButton, closeButton, saveButton]
    });

    $("#dialogCreateOrganization").dialog({
        height: 276,
        buttons: [closeButton, saveButton]
    });

    BindDialog(".EditOrganization", "#dialogEditOrganization", 444);
    BindDialog(".CreateOrganization", "#dialogCreateOrganization", 444);

    $('#dataTable').dataTable(administrationDataTableOptions);
});