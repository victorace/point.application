﻿$(document).ready(function () {

    $(formID).validate();

    $('#mainwrap').css("height", ($(window).height() - $('#mainwrap').offset().top) + "px").css("overflow", "none");

    $('.colorpicker').each(function () {
        $.minicolors.defaults.defaultValue = 'ffffff';
        $(this).minicolors();
    });

    $('table .dataTable').dataTable({
        "bStateSave": true,
        "sDom": "t",
        "bAutoWidth": false,
        "bSort": false,
        "iDisplayLength": -1,
        "oLanguage": { "sEmptyTable": "Geen data beschikbaar momenteel !" }
    });

    function replaceImageSet(url, set) {
        var parts = url.split('/');
        if (parts.length === 1 || !set)
            return url;
        var pos = jQuery.inArray("images", parts);
        if (pos === -1)
            return url;

        // images/BordIcons/Set1
        // we have found images now we need to move past
        // BordIcons and replace the new SetX with the new set
        pos += 2;
        parts[pos] = set;

        return parts.join("/");
    }

    $('#detailIconSet').on('change', function (e) {
        var set = $("#detailIconSet option:selected").text();
        $("img.detailIconType").each(function () {
            $(this).attr('src', replaceImageSet($(this).attr('src'), set));
        });
    });

    $(".numeric").keydown(function(event) {
        // Allow: backspace, delete, tab, escape and enter
        if ( $.inArray(event.keyCode,[46,8,9,27,13]) !== -1 ||
            // Allow: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) || 
            // Allow: home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        }
        else {
            // Ensure that it is a number and stop the keypress
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                event.preventDefault(); 
            }   
        }
    });
});