﻿$(document).ready(function () {

    $(["CreateSpecialism", "EditSpecialism"]).each(function (ix, el) {
        $('body').append($('<div/>').attr('id', 'dialog' + el).css('display', 'none'));
    });

    $("#dialogEditSpecialism").dialog({
        height: 236,
        buttons: [closeButton, saveButton]
    });

    $("#dialogCreateSpecialism").dialog({
        height: 236,
        buttons: [closeButton, saveButton]
    });

    BindDialog(".EditSpecialism", "#dialogEditSpecialism");
    BindDialog(".CreateSpecialism", "#dialogCreateSpecialism");

    $('#dataTable').dataTable(administrationDataTableOptions);
});