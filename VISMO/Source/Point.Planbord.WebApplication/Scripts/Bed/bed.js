﻿$(document).ready(function () {

    var dataTableOptions = {
        "bStateSave": true,
        "sDom": "ti",
        "bAutoWidth": false,
        "iDisplayLength": -1,
        "oLanguage": {
            "sProcessing": "Een moment geduld a.u.b.",
            "sLengthMenu": "_MENU_ resultaten weergeven",
            "sZeroRecords": "Geen resultaten om weer te geven",
            "sInfo": "_START_ tot _END_ van _TOTAL_ resultaten",
            "sInfoEmpty": "Geen resultaten om weer te geven",
            "sInfoFiltered": " (gefilterd uit _MAX_ resultaten)",
            "sInfoPostFix": "",
            "sSearch": "Zoeken:",
            "sEmptyTable": "Geen resultaten om weer te geven",
            "sInfoThousands": ".",
            "sLoadingRecords": "Een moment geduld a.u.b. - bezig met laden...",
            "oPaginate": { "sFirst": "Eerste", "sLast": "Laatste", "sNext": "Volgende", "sPrevious": "Vorige" }
        }
    };

    var oTable = $('#bedDataTable').dataTable(dataTableOptions);

});