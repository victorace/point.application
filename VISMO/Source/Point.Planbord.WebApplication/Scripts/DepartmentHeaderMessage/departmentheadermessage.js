﻿$(document).ready(function () {

    $(["EditDepartmentHeaderMessage"]).each(function (ix, el) {
        $('body').append($('<div/>').attr('id', 'dialog' + el).css('display', 'none'));
    });

    $("#dialogEditDepartmentHeaderMessage").dialog({
        height: 360,
        buttons: [closeButton, saveButton]
    });

    BindDialog(".EditDepartmentHeaderMessage", "#dialogEditDepartmentHeaderMessage", 600);

    $('#dataTable').dataTable(administrationDataTableOptions);
});