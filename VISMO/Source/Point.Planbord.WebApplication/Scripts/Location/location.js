﻿$(document).ready(function () {

    $(["CreateLocation", "EditLocation"]).each(function (ix, el) {
        $('body').append($('<div/>').attr('id', 'dialog' + el).css('display', 'none'));
    });

    $("#dialogEditLocation").dialog({
        height: 598,
        buttons: [deleteButton, closeButton, saveButton]
    });

    $("#dialogCreateLocation").dialog({
        height: 598,
        buttons: [closeButton, saveButton]
    });

    BindDialog(".EditLocation", "#dialogEditLocation", 440);
    BindDialog(".CreateLocation", "#dialogCreateLocation", 440);

    $('#dataTable').dataTable(administrationDataTableOptions);
});