﻿$(function () {

    $("#UserName, #Password").keyboard({
        usePreview: false,
        autoAccept: true,
        initialFocus: false,
        preventPaste: true,
        openOn: 'click',
        stickyShift: false,
        lockInput: false, //(location.hostname == "localhost" ? false : true), 
        layout: 'custom', //Custom to "remove" the cancel-button since it makes no sense in this app. (user has to save/cancel afterwards anyway)
        position: {
            of: $(".planbordLogin"),
            at2: 'center bottom',
            collision: 'flipfit flipfit'
        },
        css : {
            input          : ''
        },
        customLayout: {
            'default': [
                '` 1 2 3 4 5 6 7 8 9 0 - = {b}',
                '{tab} q w e r t y u i o p [ ] \\',
                '{lock} a s d f g h j k l ; \' {enter}',
                '{shift} z x c v b n m , . / {shift}',
                '{clear!!} {space} {a}'
            ],
            'shift': [
                '~ ! @ # $ % ^ & * ( ) _ + {b}',
            '{tab} Q W E R T Y U I O P { } |',
            '{lock} A S D F G H J K L : " {enter}',
            '{shift} Z X C V B N M < > ? {shift}',
            '{space} {a}'
            ]
        },
        display: {
            'b': '\u21e0Bck:Backspace',
            'tab': '\u21b9Tab:Tab',
            'lock': 'Caps',
            'clear': '\u2421:Wis'
        }

    });


});