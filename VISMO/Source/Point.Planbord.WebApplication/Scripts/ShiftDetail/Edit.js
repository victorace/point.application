﻿$(document).ready(function () {

    $('body').append($('<div/>').attr('id', 'dialogEditNurse').css('display', 'none'));

    $("#dialogEditNurse").dialog({
        height: 230,
        buttons: [
            closeButton,
            {
                "text": "Opslaan",
                "class": "buttonSave",
                "click": function () {
                    var opener = $(this).data('opener');
                    $.post($(opener).attr('data-url'),
                            $("#NurseForm").serialize(),
                            function (data) {
                                addNurseToDropdown($(opener), data);
                                $("#dialogEditNurse").dialog("close");
                            });
                }
            }
        ]
    });

    BindDialog("#addFlex1, #addFlex2, #addIntern1, #addIntern2", "#dialogEditNurse", 326);

    function addNurseToDropdown(opener, data) {
        var dropdown = $("#" + $(opener).attr("data-dropdownid"));
        if (dropdown != null) {
            dropdown.append($('<option/>').val(data.Nurse.NurseID).text(data.FullName).prop('selected', true))
                    .change();
        }
    }

    function getNurseInfo(opener) {
        $.post($(opener).attr('data-url'),
                { id: opener.val(), shiftid: shiftID },
                function (data) {
                    handleNurseInfo(opener, data);
                }
            );
    }

    function handleNurseInfo(opener, data) {

        var seq = opener.attr("data-sequence");

        $('input[id="Pause' + seq + '"]').prop('checked', false);
        $('input[id="Pause' + seq + '"][value="' + data.ShiftDetail.Pause + '"]').prop('checked', true);

        $('#Pager' + seq + 'ID').val(data.ShiftDetail.PagerID);

        $('input[id="Workload' + seq + '"]').prop('checked', false);
        $('input[id="Workload' + seq + '"][value="' + data.ShiftDetail.Workload + '"]').prop('checked', true);
    }

    $("#Nurse1ID, #Nurse2ID").change(function (ev) {
        var opener = $(this);
        $('#Flex' + opener.attr("data-sequence") + 'ID option:first-child').prop('selected', true);
        getNurseInfo(opener);
    });

    $("#Flex1ID, #Flex2ID").change(function (ev) {
        var opener = $(this);
        $('#Nurse' + opener.attr("data-sequence") + 'ID option:first-child').prop('selected', true);
        getNurseInfo(opener);
    });

    $('input[id="Visit1"]').click(function (ev) {
        $('input[id="Visit2"]').prop('checked', false);
    });

    $('input[id="Visit2"]').click(function (ev) {
        $('input[id="Visit1"]').prop('checked', false);
    });

    $("#clearVisit1, #clearVisit2").click(function (ev) {
        $('input[id="Visit1"], input[id="Visit2"]').prop('checked', false);
    });


    $("#Nurse1ID, #Flex1ID, #Nurse2ID, #Flex2ID").each(function (ix, el) {
        if ($(this).prop("selectedIndex") > 0)
            getNurseInfo($(this));
    });



});