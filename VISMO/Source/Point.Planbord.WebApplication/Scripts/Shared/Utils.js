﻿// Implement a "psuedo" console
window.console = window.console || (function () {
    var c = {}; c.log = c.warn = c.debug = c.info = c.error = c.time = c.dir = c.profile = c.clear = c.exception = c.trace = c.assert = function () { };
    return c;
})();

// Some error-logging
function ajaxError(xheader, err) {
    console.log("--------------");
    console.log("readyState:\n" + xheader.readyState);
    console.log("status:\n" + xheader.status);
    console.log("responseText:\n" + xheader.responseText);
    console.log("--------------");
}

// Shorten string to specified max length and and append ... at the end. 
// It will not cut in the middle of a word but at the minimum amount of 
// words under the limit.
function trimEx(message, maxLength) {
    var m = message;
    if (message && !isNaN(maxLength)) {
        var maxLen = maxLength;
        if ((!m || 0 === m.length) == false) {
            //string is too long, lets trim it and append ...
            if (m.length > maxLen) {
                var lastSpace = m.lastIndexOf(' ');
                //there is no space in the word
                if (lastSpace === -1) {
                    m = m.slice(0, maxLen - 3);
                    m += '...';
                }
                else if (lastSpace > -1) {
                    m = m.slice(0, maxLen - 3);
                    var t = m.lastIndexOf(' ');
                    m = m.slice(0, t);
                    m += '...';
                }
            }
        }
    }
    return m;
}