﻿
function showIconBed(opener, details, bedpatientstatus) {

    var icons = null;

    if (details.indexOf('1') > -1) {
        icons = $.map($.trim(details).split(''), function (n, i) {
            return (n == 1 ? $("<div/>").addClass("planbordIcon").addClass(bedIcons[i][0]) : '');
        });
    }
    else {
        bedpatientstatus = bedpatientstatus || 0;
        if (bedpatientstatus >= 10 && bedpatientstatus <= 19) // BlokBAU = 10 ... BlokZOM = 19
            icons = $("<div/>").addClass("planbordIcon").addClass("iconBedBlocked");
    }

    AddIconsToOpener(opener, details, icons);
}

function showIconRestriction(opener, details) {
    var icons = $.map($.trim(details).split(''), function (n, i) {
        var iconClass = "";
        switch (n) {
            case "A":
                break;
            case "B":
                iconClass = "iconCodeB"; break;
            case "C":
                iconClass = "iconCodeC"; break;
            case "?":
                iconClass = "iconCodeQuestion"; break
            default:
                break;
        }
        return (iconClass == "") ? "" : $("<div/>").addClass("planbordIcon").addClass(iconClass);
    });
    AddIconsToOpener(opener, details, icons);
}


function showIconVital(opener, details) {
    var icons = $.map($.trim(details).split(''), function (n, i) {
        var iconClass = "";
        switch (n) {
            case "!":
                iconClass = "iconExclamation"; break
        }
        return $("<div/>").addClass("planbordIcon").addClass(iconClass);
    });
    AddIconsToOpener(opener, details, icons);
}

function showIconIsolation(opener, details) {
    var icons = $.map([details], function (n, i) {
        var iconClass = "iconDot" + n;
        return $("<div/>").addClass("planbordIcon").addClass(iconClass);
    });
    AddIconsToOpener(opener, details, icons);
}

function showIconQScore(opener, details) {
    var icons = $.map(details.split(''), function (n, i) {
        var iconClass = "";
        switch (n) {
            case "R":
                iconClass = "iconDotRed"; break;
            case "!":
                iconClass = (details.length > 1 ? "iconExclamationLeft" : "iconExclamation"); break
        }
        return $("<div/>").addClass("planbordIcon").addClass(iconClass);
    });
    AddIconsToOpener(opener, details, icons);
}

function showIconDetails(opener, details) {
    var icons = $.map($.trim(details).split(''), function (n, i) {
        if (n == 1) { // only continue for icons set to "on" by the user
            var detailIcon = $.grep(detailIcons, function(el, ix) { return el.DetailIconType == i; });
            if (detailIcon.length !== 0 && detailIcon[0].Visible && detailIcon[0].IsDeleted == false) {
                return $("<div/>").addClass("planbordIcon").addClass("detailIcon" + detailIcon[0].DetailIconType);
            }
        }
        return null;
    });

    var icons5 = icons.slice(0, 5); // max 5 icons displayed 
    AddIconsToOpener(opener, details, icons.slice(0, 5));
}


function showIconMobility(opener, details) {
    var icons = $.map($.trim(details).split(''), function (n, i) {
        return (n == 1 ? $("<div/>").addClass("planbordIcon").addClass(mobilityIcons[i][0]) : '');
    });
    AddIconsToOpener(opener, details, icons);
}

function showIconAOA(opener, details) {
    var icons = "";
    if (details == "Ja") {
        var iconClass = "iconAOA";
        icons = $("<div/>").addClass("planbordIcon").addClass(iconClass);
    }
    AddIconsToOpener(opener, details, icons);
}

function showIconFase(opener, details, flowdefinition) {
    var iconClass = "";

    switch (flowdefinition)
    {
        case "1": // VVT-ZH
            break;
        case "2": // ZH-VVT
            var icons = { 1: "iconFaseAF", 2: "iconFaseInBeh", 3: "iconFaseAanvul", 6: "iconFaseInBeh", 7: "iconFaseAanvul", 8: "iconFaseVOGroen" };
            if (typeof icons[details] != "undefined")  {
                iconClass = icons[details];
            }
            break;
        case "3": // RzTP
            var icons = { 1: "iconFaseAF", 2: "iconFaseInBeh" };
            if (typeof icons[details] != "undefined") {
                iconClass = icons[details];
            }
            break;
        case "4": // CVA
            var icons = { 3: "iconFaseAF", 4: "iconFaseInBeh", 5: "iconFaseAanvul", 8: "iconFaseInBeh", 9: "iconFaseAanvul", 10: "iconFaseVOGroen" };
            if (typeof icons[details] != "undefined") {
                iconClass = icons[details];
            }
            break;
        case "5": // ZH-ZH
            break;
    }
    if (details == "-1")
    {
        iconClass = "iconFaseTO";
    }

    icons = $("<div/>").addClass("planbordIcon").addClass(iconClass);

    AddIconsToOpener(opener, details, icons);
}

function showIconApoIn(opener, details) {
    var icons = $.map($.trim(details).split(''), function (n, i) {
        var iconClass = "";
        switch (n) {
            case "X":
                iconClass = "iconDotGreen"; break
        }
        return $("<div/>").addClass("planbordIcon").addClass(iconClass);
    });
    AddIconsToOpener(opener, details, icons);
}

function showIconApoExit(opener, details) {
    var icons = $.map($.trim(details).split(''), function (n, i) {
        var iconClass = "";
        switch (n) {
            case "X":
                iconClass = "iconDotGreen"; break
        }
        return $("<div/>").addClass("planbordIcon").addClass(iconClass);
    });
    AddIconsToOpener(opener, details, icons);
}

function showIconEwsScore(opener, details, ewsdatetime, ewsaction) {
    if (ewsdatetime != null) {
        var dtEws = new Date(ewsdatetime).getTime();
        var dtNow = new Date().getTime();
        var diffHours = Math.abs(dtNow - dtEws) / 3600000;
    }
    
    if (ewsdatetime == null || ewsdatetime == "" || diffHours > 24) {
        iconClass = "iconCodeQuestion";
        var icons = $("<div/>").addClass("planbordIcon").addClass(iconClass);
        AddIconsToOpener(opener, details, icons);
    }
    else {
        var score = parseInt(details, 10);
        var ewsclass = "";
        if (score < 3) {
            ewsclass = "ewsblack";
        }
        else if (score >= 3) {
            if (diffHours > 1) {
                ewsclass = "ewsred";
            }
            else if (diffHours <= 1) {
                ewsclass = "ewsgreen";
            }
        }
        if (ewsaction.toLowerCase() == "geen") {
            ewsclass = "ewsblack";
        }

        if (ewsclass != "") {
            var scoretext = $("<div/>").addClass("ewsscore " + ewsclass).text(score);
            AddIconsToOpener(opener, '', scoretext);
        }
    }
}

function showIconDischarge(opener, details) {
    var icons = $.map($.trim(details).split(''), function (n, i) {
        var iconClass = "";
        switch (n) {
            case "V":
                iconClass = "iconCheck"; break
        }
        return $("<div/>").addClass("planbordIcon").addClass(iconClass);
    });
    AddIconsToOpener(opener, details, icons);
}

function showPagerPauseVisit(opener, details) {

    if (details == null || details == "") {
        return;
    }

    var vals = details.split('||');
    var pagerpausevisit = "";

    if (vals[0] != "") {
        pagerpausevisit += vals[0] + " ";
    }
    if (vals[1] != "") {
        pagerpausevisit += "<div class='vpkBackgroundCircle'>" + vals[1] + "</div> ";
    }
    if (vals[2] != "") {
        pagerpausevisit += "<div class='vpkBackgroundRectangle'>" + vals[2] + "</div> ";
    }

    if (vals[3] != "") {
        var workloadClass = "";
        switch (vals[3]) {
            case "1":
                workloadClass = "iconWorkloadLow";
                break;
            case "2":
                workloadClass = "iconWorkloadMedium";
                break;
            case "3":
                workloadClass = "iconWorkloadHigh";
                break;
        }
        if (workloadClass) {
            var workload = "<div class='iconWorkload " + workloadClass + "'/>";
            workload = $.trim(workload);
            $(opener).prepend($("<span/>").html(workload + "&nbsp;"));
        }
    }

    pagerpausevisit = $.trim(pagerpausevisit);
    if (pagerpausevisit != "") {
        $(opener).append($("<span/>").html(" " + pagerpausevisit));
    }
}



function showText(opener, details) {
    $(opener).empty();
    $(opener).text(details==null?"":details);
    var visibleColumnWidth = getColumnWidth($(opener).parent("tr").children().index($(opener)));
    TextToColumnWidth(visibleColumnWidth, $(opener)[0]);
}





function handleShiftDetail(opener, newshiftdetails) { //data = data.NewShiftDetails;
    $(newshiftdetails).each(function (ix, newshiftdetail) {

        var bedid = newshiftdetail.BedID;
        $('.showFirstNurse[data-bedid="' + bedid + '"]').attr('data-fulltext', newshiftdetail.FirstNurse).attr('data-pagerpausevisit', newshiftdetail.FirstPagerPauseVisit);
        $('.showSecondNurse[data-bedid="' + bedid + '"]').attr('data-fulltext', newshiftdetail.SecondNurse).attr('data-pagerpausevisit', newshiftdetail.SecondPagerPauseVisit);
        $('.showIntern[data-bedid="' + bedid + '"]').attr('data-fulltext', newshiftdetail.Intern);

    });
    HandleNurseColumns();

}

