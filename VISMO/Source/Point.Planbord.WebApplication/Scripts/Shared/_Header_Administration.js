﻿var DoPinCheck = '0';

function currentTime() {
    var d = new Date(); var hours = d.getHours(), minutes = d.getMinutes();
    return (hours < 10 ? "0" : "") + hours + ":" + (minutes < 10 ? "0" : "") + minutes;
};

setInterval(function () {
    $('#currentTime').text(currentTime());
}, 1000);

$(document).ready(function () {

    $(["dialogSecurity"]).each(function (ix, el) {
        $('body').append($('<div/>').attr('id', el).css('display', 'none'));
    });

    $(".header-admin").show();
    $(".header-home").show();
    $(".header-logoff").show();

    LoadCommonDialogs();

    BindDialog(".header-logoff", "#dialogConfirm", 380);
});