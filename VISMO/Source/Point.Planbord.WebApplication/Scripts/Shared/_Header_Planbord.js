﻿
var lastPageRefresh = new Date();

setInterval(function () {
    $('#currentTime').text(formatTime(new Date(), false));
}, 1000);

function formatTime(dt, showSeconds) {
    showSeconds = false;
    var hours = dt.getHours(), minutes = dt.getMinutes(), seconds = dt.getSeconds();
    return (hours < 10 ? "0" : "") + hours + ":" 
         + (minutes < 10 ? "0" : "") + minutes 
         + ( showSeconds ? ":" + (seconds < 10 ? "0" : "") + seconds : "" );
}

function pageRefresh(showOverlay) {
    if (showOverlay)
        $('.overlay').show();
    location.replace(urlCurrentAction);
}

// Auto-refresh of the page:
if (RefreshTimer > 0 && RefreshTimer < 30) RefreshTimer = 30 // min. 30 seconds for each refresh

var idleTime = 0;
var idleTimeout = RefreshTimer * 1000; // seconds to be idle, before a refresh
var idleTimerInterval = 15 * 1000; // 15 sec. interval in which the idleTime will tick, to check if it should refresh
var idleInterval = null; 

idleTimerIncrement = function () {
    idleTime = idleTime + idleTimerInterval;
    if (idleTime >= idleTimeout) { //    and no popup is open ...

        if ($(".ui-widget-overlay.ui-front:visible").length > 0) {
            //popup open
            idleTimeout = idleTimeout + idleTimerInterval;
            setPageRefreshTime();
        }
        else {
            idleTime = 0;
            pageRefresh(false);
        }
    }
}

// Give the timer some extra time if the user is working on the screen
idleTimerReset = function () {

    if ((idleTimeout - idleTime) <= idleTimerInterval) {
        idleTimeout = idleTimeout + idleTimerInterval;
    }
    setPageRefreshTime();
}

function setPageRefreshTime() {
    $("#header-refresh-current").text(formatTime(lastPageRefresh));
    var nextPageRefresh = new Date(lastPageRefresh.toString());
    nextPageRefresh.setMilliseconds(lastPageRefresh.getMilliseconds() + idleTimeout);
    $("#header-refresh-next").text(formatTime(nextPageRefresh));
}

$(document).ready(function () {

    $('div.header-home a').click(function (e) {
        e.preventDefault();
        if (currentAction != "Index") {
            $('.overlay').show();
            location.replace(urlIndex);
        }
    });

    setPageRefreshTime();

    if (idleTimeout > 0) {
        // set the timer for checking the idle time
        var idleInterval = setInterval("idleTimerIncrement()", idleTimerInterval);
        // reset the idle time, when there's some action of the user, only reset on the last interval.
        $(this).mousemove(function (e) { idleTimerReset(); });
        $(this).keypress(function (e) { idleTimerReset(); });
    }




    $("#dialogExpected").dialog({
        height: 330
    });

    $("#dialogCapacity").dialog({
        height: 330,
        buttons: [
                closeButton,
                {
                    "text": "Bezettingsoverzicht",
                    "class": "buttonOverview",
                    "click": function() {
                        $('.overlay').show();
                        location.assign(urlCapacityOverview);
                    }
                }
                ]
    });

    $("#dialogShiftDepartment").dialog({
        height: 522,
        buttons: [
                closeButton,
                {
                    "text": "Opslaan",
                    "class": "buttonSave",
                    "click": function () {
                        $('.overlay').show();
                        var opener = $(this).data('opener');
                        $.post($(opener).attr('data-url'),
                            { shiftId: $('input[name=Shift]:checked').val(), departmentId: $('input[name=Department]:checked').val() },
                            function () { location.replace(urlIndex); });
                        $("#dialogShiftDepartment").dialog("close");
                    }
                }
        ]
    });

    $.getJSON(urlTotals,
                function (data) {
                    $(".header-verwacht .accent").text(data.ExpectedTotal);
                    $(".header-capaciteit .accent").text(data.CapacityTotal);
                    $(".header-bezettebedden .accent").text(data.OccupiedTotal);
                });

    function showHideMenuIcon(el) {
        var fadeouttime = (DoPinCheck == '1' && pinValid == false) ? 60000 : 10000;
        el.fadeToggle(500).animate({ opacity: 1.0 }, fadeouttime).fadeToggle(500);
    }


    if (currentAction == "Index" || currentAction == "CapacityOverview") {

        $('div.header-home div.headerIcon, .iconHome').addClass('shiftDepartmentDialog');

        $("#showHideMenuIcons").click(function () {
            showHideMenuIcon($(".header-admin"));
            showHideMenuIcon($(".header-home"));
            showHideMenuIcon($(".header-logoff"));
        });
    }
    else {
        $(".header-admin, .header-home, .header-logoff").show();

        $('div.header-home div.headerIcon, .iconHome').removeClass('shiftDepartmentDialog')
    }


    BindDialog(".header-verwacht", "#dialogExpected", 600);
    BindDialog(".header-capaciteit", "#dialogCapacity", 532);
    BindDialog(".shiftDepartmentDialog", "#dialogShiftDepartment", 460);


    BindDialog(".header-logoff", "#dialogConfirm", 380);

});