﻿var showErrorMessage = function (messages) {

    var errorMessage = [];
    if (messages.constructor !== Array) {
        errorMessage.push(messages);
    } else {
        errorMessage = messages
    }

    $("#error #errormessage").html(errorMessage.join("<br/>"));
    $("#error").show().delay(6000).fadeOut();
}

var errorMessageIsShowing = function () {
    return $("#error").is(":visible")
}

var showSuccessMessage = function (message) {
    $("#success #successmessage").html(message);
    $("#success").show().delay(4000).fadeOut();
}

var closeButton = {
    "text": "Sluit",
    "class": "buttonClose",
    "click": function () {
        $(this).dialog('close');
        return false;
    }
};

var confirmButton = {
    "text": "OK",
    "class": "buttonOk",
    "click": function () {
        $(this).dialog('close');
        var opener = $(this).data('opener');
        if ($(opener).attr('data-url'))
            $.post($(opener).attr("data-url"));
        else if ($(opener).attr('data-url-replace'))
            location.replace($(opener).attr('data-url-replace'));
    }
}


var pinValid = false;

function PinCheck(el) {
    if (DoPinCheck == '0' || pinValid == true)
        return true;

    var opener = $(el).data('opener');

    if ($(opener).attr('data-nopincheck') == 'true')
        return true;

    
    showDialogSecurity(el);
    return false;
}

// Set the default options for the dialog used on this page
$.extend($.ui.dialog.prototype.options, {
    height: 320,
    autoOpen: false,
    resizable: false,
    modal: true,
    buttons: [closeButton],
    open: function (type, data) {

        if (!PinCheck($(this))) return false; //Include this, when 'open' is overruled. All columns are protected now (if PIN is set)

        var opener = $(this).data('opener');
        var parent = $(this);
        $(this).html('<div class="dialogLoader"></div>');
        $('.ui-widget-overlay').bind('click', function () { parent.dialog('close'); });
        if ($(opener).attr('data-values'))
            $(this).load($(opener).attr('data-url'), $(opener).attr('data-values'));
        else if ($(opener).attr('data-id'))
            $(this).load($(opener).attr('data-url') + '/' + $(opener).attr('data-id'));
        else
            $(this).load($(opener).attr('data-url'));

    },
    close: function (type, data) {
        var opener = $(this).data('opener');
        HighlightRow(opener);
        $(this).parent().find('button.ui-button[style*="display: none"]').css('display', 'inline');
        $(this).empty();
    }
});

function showDialogSecurity(el) {
    $("#dialogSecurity")
            .data('opener', $(el).data('opener'))
            .dialog("option", "position", { my: "center top", at: "center top", of: el })
            .dialog("option", "width", 204)
            .dialog("open");
    $(el).dialog('close');
    return false;
}


var autoCloseDialogTimeout = null;
function AutoCloseDialog(dialogID, waitSeconds) {
    if (waitSeconds <= 0) return;

    clearTimeout(autoCloseDialogTimeout);

    autoCloseDialogTimeout = setTimeout(function () {
        $(dialogID).dialog("close");
    }, waitSeconds * 1000);
}

function BindDialog(openerSelector, dialogID, width, doClose) {

    $(openerSelector).click(function (e) {
        e.preventDefault();
        $(dialogID).data('opener', this)
                   .dialog("option", "position", { my: "center top", at: "center bottom", of: e.target })
                   .dialog("option", "width", (width || 430))
                   .dialog("open");

        if (doClose === true)
            AutoCloseDialog(dialogID, AutoCloseTimer||0);

    }).css("cursor", "pointer");

    $(openerSelector + " .noclick").click(function (e) {
        e.stopPropagation();
    });
}

function BindFulltextClick(el, width) {
    $(el).click(function (e) {
        var opener = $(this);
        $("#dialogFulltext")
            .data('opener', this)
            .dialog("option", "position", { my: "center top", at: "center bottom", of: e.target })
            .dialog("option", "width", width||430)
            .dialog("option", "close", function () { HighlightRow(opener); })
            .dialog("open");

        AutoCloseDialog("#dialogFulltext", AutoCloseTimer || 0);

    }).css("cursor", "pointer");
}

function BindPatientOverviewClick(el, width) {
    $(el).click(function (e) {
        var opener = $(this);
        $("#dialogPatientOverview")
            .data('opener', this)
            .dialog("option", "position", { my: "center top", at: "center bottom", of: e.target })
            .dialog("option", "width", width || 630)
            .dialog("option", "close", function () { HighlightRow(opener); })
            .dialog("open");

        AutoCloseDialog("#dialogPatientOverview", 0);

    }).css("cursor", "pointer");
}

function RemoveClick(opener, attrName) {
    if (opener.attr(attrName) == "") {
        opener.off('click');
        opener.css("cursor", "default");
    }
}

function AddIconsToOpener(opener, details, icons) {
    $(opener).empty();
    if ( icons||"" != "" )
        $(opener).append(icons);
    if ( details||"" != "" )
        $(opener).append($("<div/>").addClass("hiddenIconSort").text(details));
}

function addEllipsisText(opener) {
    $(opener).append($("<span/>").addClass("ellipsis").text("..."));
}

function addEllipsisIcon(opener) {
    $(opener).append($("<div/>").addClass("planbordIcon iconEllipsis"));
}

function HighlightRow(opener) {
    $(opener).parent("tr").effect("highlight", {color:"#00bdf4"}, 1000);
}

//just to avoid 'string ...' with spaces
String.prototype.trim = function () { return this.replace(/^\s\s*/, '').replace(/\s\s*$/, ''); };

function getTextVisualLength(str) {
    var ruler = document.createElement('span');
    ruler.style.visibility = 'hidden';
    ruler.style['white-space'] = 'nowrap';
    document.body.appendChild(ruler);
    ruler.innerHTML = str;
    var l = ruler.offsetWidth;
    document.body.removeChild(ruler);
    return l;
};

function TextToColumnWidth(width, element) {
    var text = element.getAttribute("data-fulltext") || (element.innerHTML || "");
    if (getTextVisualLength(text) > width) {
        do {
            text = text.substr(0, text.length - 1).trim();
        } while (getTextVisualLength(text) > width);
        element.innerText = text;
        addEllipsisText(element);
    }
    else {
        element.innerText = text;
    }
};

function TextToInvisible(element) {
    $(element).text("");
}

function TextToEllipsis(element) {
    var text = element.getAttribute("data-fulltext") || (element.innerHTML || "");

    element.innerText = "";
    if (text != "") {
        addEllipsisIcon($(element));
    }
}

function MarkRequiredFields() {
    $('input[type=text]:visible,select:visible').each(function () {
        var req = $(this).attr('data-val-required');
        if (undefined != req) {
            $(this).after($("<span />").addClass("input-required").text(" *"));
        }
    });
}

function LoadCommonDialogs() {

    $("#dialogAlert").dialog({
        height: 220,
        buttons: [closeButton],
        open: function (type, data) {
            var opener = $(this).data('opener');

            var openedDialog = $("#dialogAlert");
            openedDialog.find("#dialogAlertHeader").text("Let op!");
            openedDialog.find("#dialogAlertSubheader2").html($(opener).attr('data-alert'));

            var parent = $(this);
            $('.ui-widget-overlay').bind('click', function () { parent.dialog('close'); });
        },
        close: function (type, data) {
        }
    });


    $("#dialogConfirm").dialog({
        height: 270,
        buttons: [closeButton, confirmButton],
        open: function (type, data) {

            if (!PinCheck($(this))) return false;

            var opener = $(this).data('opener');

            var openedDialog = $("#dialogConfirm");
            openedDialog.find("#dialogConfirmHeader").text("Let op!");
            openedDialog.find("#dialogConfirmSubheader2").html($(opener).attr('data-confirm'));

            var parent = $(this);
            $('.ui-widget-overlay').bind('click', function () { parent.dialog('close'); });
        },
        close: function (type, data) {
        }
    });

    $("#dialogSecurity").dialog({
        height: 342,
        open: function (type, data) {
            var parent = $(this);
            $('.ui-widget-overlay').bind('click', function () { parent.dialog('close'); });
            $(this).empty();
            $(this).load(urlSecurity);

        },
        close: function (type, data) {
            var opener = $(this).data('opener');
            $(this).empty();
            if (pinValid == true) opener.click();
        },
        buttons: [
             closeButton,
             {
                 "text": "Opslaan",
                 "class": "buttonSave",
                 "click": function () {
                     var opener = $(this).data('opener');

                     $.post(urlSecurity,
                             $("#SecurityForm").serialize(),
                             function (data) {
                                 pinValid = (data == "OK");
                                 if (pinValid == true) {
                                     setTimeout(function () { pinValid = false; }, 60 * 1000);
                                 }
                                 $("#dialogSecurity").dialog("close");
                             });
                 }
             }
        ]
    });




}