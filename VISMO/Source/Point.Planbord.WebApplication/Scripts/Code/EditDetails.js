﻿$(document).ready(function () {

    var visibleIcons = 0;
    var icons = $.map(detailIcons, function (element, index) {
        if (element.Visible) {
            visibleIcons++;
        }

        return $("<div/>")
            .addClass("planbordIcon_xl")
            .addClass("detailIcon_xl" + index)
            .addClass("planbordIconUp")
            .css("display", (element.Visible && visibleIcons <= 15) ? "" : "none")
            .append($("<span/>")
            .text((element.Name ? element.Name : ""))
            .addClass("label"));
    });

    iconHolder.append(icons);

    var iconsSelected = 0;
    iconHolder.find("div").each(function (index, element) {
        $(this).click(function () {
            if (clickable) {
                handleLargeButton(index, element);
            }
        });

        var selected = false;
        if (valueHolder.val()[index] === "1" && iconsSelected <= 5) {
            if ($(this).is(":visible")) {
                iconsSelected++;
                selected = true;
            }
        }

        if (selected) {
            $(this).removeClass("planbordIconUp").addClass("planbordIconDown");
        } else {
            $(this).addClass("planbordIconUp");
            valueHolder.val(replaceAt(valueHolder.val(), index, "0"));
        }
    });
});

function handleLargeButton(index, element) {

    $("#divAlert").hide();

    if ($(element).hasClass("planbordIconDown")) {
        $(element).removeClass("planbordIconDown").addClass("planbordIconUp");
        valueHolder.val(replaceAt(valueHolder.val(), index, "0"));
    }
    else {
        var iconsSelected = 0;
        iconHolder.find("div").each(function (ix, el) {
            if ($(this).hasClass("planbordIconDown") && $(this).is(":visible")) {
                iconsSelected++;
            }
        });
        if (iconsSelected >= 5) {
            $("#divAlert").show();
            return;
        }

        $(element).removeClass("planbordIconUp").addClass("planbordIconDown");
        valueHolder.val(replaceAt(valueHolder.val(), index, "1"));
    }
}

function replaceAt(s, n, t) {
    return s.substring(0, n) + t + s.substring(n + 1);
}
