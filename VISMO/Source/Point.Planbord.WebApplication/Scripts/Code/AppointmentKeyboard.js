﻿
$(function () {

    $.keyboard.keyaction.c1 = function (base) {
        base.insertText('Fysiotherapie');
    };
    $.keyboard.keyaction.c2 = function (base) {
        base.insertText('Controle van ');
    };
    $.keyboard.keyaction.c3 = function (base) {
        base.insertText('Geen bezoek');
    };


    keyboardField.keyboard({
        usePreview: false,
        autoAccept: true,
        initialFocus: false,
        preventPaste: true,
        openOn: 'click',
        stickyShift: false,
        lockInput: true,
        layout: 'custom', //Custom to "remove" the cancel-button since it makes no sense in this app. (user has to save/cancel afterwards anyway)
        position: {
            at2: 'center bottom+55',
            collision: 'flipfit flipfit'
        },
        customLayout: {
            'default': [
                '` 1 2 3 4 5 6 7 8 9 0 - = : {b}',
                '{tab} q w e r t y u i o p [ ] \\',
                '{lock} a s d f g h j k l ; \' {enter}',
                '{shift} z x c v b n m , . / {shift}',
                '{c1!!} {c2!!} {c3!!} {space} {a}'
            ],
            'shift': [
                '~ ! @ # $ % ^ & * ( ) _ + : {b}',
            '{tab} Q W E R T Y U I O P { } |',
            '{lock} A S D F G H J K L : " {enter}',
            '{shift} Z X C V B N M < > ? {shift}',
            '{c1!!} {c2!!} {c3!!} {space} {a}'
            ]
        },
        display: {
            'b': '\u21e0Bck:Backspace',
            'tab': '\u21b9Tab:Tab',
            'lock': 'Caps',
            'c1': 'F:Fysiotherapie',
            'c2': 'C:Controle van',
            'c3': 'G:Geen bezoek'
        }

    });


});
