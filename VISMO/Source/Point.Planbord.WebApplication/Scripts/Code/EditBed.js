﻿$(document).ready(function () {

    var icons = $.map(bedIcons, function (el, ix) {
        return $("<div/>")
            .addClass("planbordIcon_xl")
            .addClass(el[0] + '_xl')
            .css("display", (el[3] ? "" : "none"))
            .append($("<span/>")
            .text(el[1])
            .addClass("label"));
    });
    iconHolder.append(icons);

    iconHolder.find("div").each(function (ix, el) {
        $(this).click(function () { handleLargeButton(ix, el); });
        if (valueHolder.val()[ix] == "1")
        { $(this).addClass("planbordIconDown"); }
        else
        { $(this).addClass("planbordIconUp"); }
    });

});

function handleLargeButton(index, element) {

    var elementWasDownBefore = $(element).hasClass("planbordIconDown");

    iconHolder.find("div").each(function (ix, el) {
        $(el).removeClass("planbordIconDown").addClass("planbordIconUp");
        valueHolder.val(replaceAt(valueHolder.val(), ix, 0));
    });

    if (elementWasDownBefore) {
        $(element).removeClass("planbordIconDown").addClass("planbordIconUp");
        valueHolder.val(replaceAt(valueHolder.val(), index, 0));

    }
    else {
        $(element).removeClass("planbordIconUp").addClass("planbordIconDown");
        valueHolder.val(replaceAt(valueHolder.val(), index, 1));
    }
}

function replaceAt(s, n, t) {
    return s.substring(0, n) + t + s.substring(n + 1);
}
