﻿using Point.Planbord.Database.Context;
using Point.Planbord.Infrastructure;
using Point.Planbord.Database.Repository;
using System.Web.Mvc;
using System.Linq;
using Point.Planbord.Database.Models;
using System;

namespace Point.Planbord.WebApplication.Controllers
{
    public class AdministrationController : BaseController
    {
        private readonly UnitOfWork<PlanbordContext> uow = new UnitOfWork<PlanbordContext>();

        [AuthorizeRoles(Role.PlanbordAdministrator, Role.OrganizationAdministrator, Role.PersonalAccount, Role.LogistiekAccount)]
        public ActionResult Index()
        {
            return View();
        }

        [AuthorizeRoles(Role.PlanbordAdministrator, Role.OrganizationAdministrator)]
        public ActionResult ListSoap(int rows = 50)
        {
            var soaps = (from logging in (new LoggingContext()).Loggings 
                         where logging.ReferenceName == "SOAP" 
                         orderby logging.LoggingDateTime descending 
                         select logging).Take(rows);

            return View(soaps);

        }

        [AuthorizeRoles(Role.PlanbordAdministrator, Role.OrganizationAdministrator)]
        public ActionResult ViewSoap(int id)
        {

            string xml = (from logging in (new LoggingContext()).Loggings
                          where logging.LoggingID == id
                          select logging.Description).FirstOrDefault();

            return this.Content(xml, "text/xml");
        }
    }
}
