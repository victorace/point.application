﻿using Point.Planbord.Database.Context;
using Point.Planbord.Infrastructure;
using Point.Planbord.Database.Repository;
using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using Point.Planbord.Database;

namespace Point.Planbord.WebApplication.Controllers
{
    [OutputCache(Duration = 0, NoStore = true)]
    public class BaseController : Controller
    {
        private readonly UnitOfWork<PlanbordContext> uow = new UnitOfWork<PlanbordContext>();
        
        public ISessionWrapper SessionWrapper { get; set; }

        public BaseController()
        {
            SessionWrapper = new HttpContextSessionWrapper();

            ViewBag.CurrentDepartment = SessionWrapper.GetFromSession<string>(SessionKey.DepartmentName, "");
        }

        public void SaveEmployeeUserInSession(string username)
        {
            MembershipUser membershipUser = Membership.GetUser(username);

            EmployeeUserSessionObject employeeUserSessionObject = null;
            if (!Roles.IsUserInRole(username, Role.PlanbordAdministrator.ToString()))
            {
                employeeUserSessionObject = (from e in uow.EmployeeRepository.Get(emp=>emp.UserId == (Guid)membershipUser.ProviderUserKey)
                 select new EmployeeUserSessionObject
                 {
                     EmployeeID = e.EmployeeID,
                     OrganizationID = e.Department.Location.OrganizationID,
                     LocationID = e.Department.LocationID,
                     DepartmentID = e.DepartmentID,
                     DepartmentName = e.Department.Name
                 }).FirstOrDefault();
            }

            SessionWrapper.SetInSession(SessionKey.EmployeeID, (employeeUserSessionObject == null ? -1 : employeeUserSessionObject.EmployeeID));
            SessionWrapper.SetInSession(SessionKey.OrganizationID, (employeeUserSessionObject == null ? -1 : employeeUserSessionObject.OrganizationID));
            SessionWrapper.SetInSession(SessionKey.LocationID, (employeeUserSessionObject == null ? -1 : employeeUserSessionObject.LocationID));
            SessionWrapper.SetInSession(SessionKey.DepartmentID, (employeeUserSessionObject == null ? -1 : employeeUserSessionObject.DepartmentID));
            SessionWrapper.SetInSession(SessionKey.DepartmentName, (employeeUserSessionObject == null ? "" : employeeUserSessionObject.DepartmentName));

            int currentDepartmentID = -1;
            string currentDepartmentName = "";
            if (Roles.IsUserInRole(username, Role.PlanbordAdministrator.ToString()))
            {
                var department = uow.DepartmentRepository.GetAll().FirstOrDefault();
                if (department != null) {
                    currentDepartmentID = department.DepartmentID;
                    currentDepartmentName = department.Name;
                }
            }
            else if (Roles.IsUserInRole(username, Role.OrganizationAdministrator.ToString()) && employeeUserSessionObject != null)
            {
                var department = uow.DepartmentRepository.Get(d => d.Location.OrganizationID == employeeUserSessionObject.OrganizationID).FirstOrDefault();
                if (department != null) {
                    currentDepartmentID = department.DepartmentID;
                    currentDepartmentName = department.Name;
                }
            }
            else if (employeeUserSessionObject != null)
            {
                currentDepartmentID = employeeUserSessionObject.DepartmentID;
                currentDepartmentName = employeeUserSessionObject.DepartmentName;
            }
            
            SessionWrapper.SetInSession(SessionKey.CurrentDepartmentID, currentDepartmentID);
            SessionWrapper.SetInSession(SessionKey.CurrentDepartmentName, currentDepartmentName);

            var shift = uow.ShiftRepository.GetAll().FirstOrDefault();
            SessionWrapper.SetInSession(SessionKey.ShiftID, (shift == null ? -1 : shift.ShiftID));

            var settingsController = new SettingsController();
            var us = new UserSettings(settingsController.GetUserSettings(currentDepartmentID));
            SessionWrapper.SetInSession(SessionKey.Settings, us);

            SessionWrapper.SetInSession(SessionKey.ValidSession, true);
        }

        public EmployeeUserSessionObject GetSessionObject()
        {
            if (!SessionWrapper.GetFromSession<bool>(SessionKey.ValidSession, false))
                SaveEmployeeUserInSession(Membership.GetUser().UserName);

            var employeeUserSessionObject = new EmployeeUserSessionObject()
            {
                EmployeeID = SessionWrapper.GetFromSession<int>(SessionKey.EmployeeID, -1),
                OrganizationID = SessionWrapper.GetFromSession<int>(SessionKey.OrganizationID, -1),
                LocationID = SessionWrapper.GetFromSession<int>(SessionKey.LocationID, -1),
                DepartmentID = SessionWrapper.GetFromSession<int>(SessionKey.DepartmentID, -1),
                DepartmentName = SessionWrapper.GetFromSession<string>(SessionKey.DepartmentName, ""),
                UserSettings = SessionWrapper.GetFromSession<UserSettings>(SessionKey.Settings, null),
                CurrentShiftID = SessionWrapper.GetFromSession<int>(SessionKey.ShiftID, -1),
                CurrentDepartmentID = SessionWrapper.GetFromSession<int>(SessionKey.CurrentDepartmentID, -1),
                CurrentDepartmentName = SessionWrapper.GetFromSession<string>(SessionKey.CurrentDepartmentName, "")
            };

            return employeeUserSessionObject;
        }
    }
}
