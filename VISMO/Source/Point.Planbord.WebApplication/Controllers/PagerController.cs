﻿using Point.Planbord.Database.Context;
using Point.Planbord.Infrastructure;
using Point.Planbord.Database.Models;
using Point.Planbord.Database.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using Point.Planbord.Database;

namespace Point.Planbord.WebApplication.Controllers
{
    [AuthorizeRoles(Role.PlanbordAdministrator, Role.OrganizationAdministrator, Role.PersonalAccount)]
    public class PagerController : BaseController
    {
        private readonly UnitOfWork<PlanbordContext> uow = new UnitOfWork<PlanbordContext>();

        public ActionResult Index()
        {
            return View(GetPagers());
        }

        public ActionResult Edit(int id = 0)
        {
            var controller = new DepartmentController();
            ViewBag.Departments = new SelectList(controller.GetDepartments(), "DepartmentID", "Name");

            var pager = uow.PagerRepository.GetByID(id);
            if (pager == null)
                pager = new Pager();

            setAdministrationDialogHeader(id, pager);

            return View(pager);
        }

        private void setAdministrationDialogHeader(int id, Pager pager)
        {
            ViewBag.AdministrationDialogHeader = pager.PagerNumber;
            ViewBag.AdministrationDialogSubHeader = "Pieper " + (id == 0 ? "aanmaken" : "bewerken");
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult Edit(Pager pager)
        {
            var controller = new DepartmentController();
            ViewBag.Departments = new SelectList(controller.GetDepartments(), "DepartmentID", "Name");

            if (ModelState.IsValid)
            {
                if (pager.PagerID != 0)
                    uow.PagerRepository.Update(pager);
                else
                    uow.PagerRepository.Insert(pager);
                uow.Save();
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            if (id != 0)
            {
                uow.PagerRepository.Delete(id);
                uow.Save();
            }
            return RedirectToAction("Index");
        }

        private IEnumerable<Pager> GetPagers()
        {
            if (Roles.IsUserInRole(Role.PlanbordAdministrator.ToString()))
            {
                return uow.PagerRepository.GetAll();
            }
            else
            {
                var user = Membership.GetUser();
                var userId = (Guid)user.ProviderUserKey;
                var employee = uow.EmployeeRepository.Get(e => e.UserId == userId).FirstOrDefault();
                if (employee == null)
                    return Enumerable.Empty<Pager>();

                if (Roles.IsUserInRole(Role.OrganizationAdministrator.ToString()))
                {
                    return uow.PagerRepository.Get(p => p.Department.Location.OrganizationID == employee.Department.Location.OrganizationID);
                }
                else if (Roles.IsUserInRole(Role.PersonalAccount.ToString()))
                {
                    return uow.PagerRepository.Get(p => p.Department.DepartmentID == employee.DepartmentID);
                }
            }

            return Enumerable.Empty<Pager>();
        }

        public IEnumerable<Pager> GetPagersForDepartment(int departmentID)
        {
            if (!EmployeeUserHelper.IsDepartmentOfUser(GetSessionObject(), departmentID)) return Enumerable.Empty<Pager>();

            return uow.PagerRepository.Get(p => p.DepartmentID == departmentID);
        }
    }
}
