﻿using Point.Planbord.Database.Context;
using Point.Planbord.Infrastructure;
using Point.Planbord.Database.Models;
using Point.Planbord.Database.Repository;
using Point.Planbord.Infrastructure.Extensions;
using Point.Planbord.Database.Extensions;
using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using System.Collections.Generic;
using Point.Planbord.Database;

namespace Point.Planbord.WebApplication.Controllers
{
    [Authorize]
    public class NurseController : BaseController
    {
        private readonly UnitOfWork<PlanbordContext> uow = new UnitOfWork<PlanbordContext>();

        [AuthorizeRoles(Role.PlanbordAdministrator, Role.OrganizationAdministrator, Role.PersonalAccount)]
        public ActionResult Index()
        {
            return View(getNurses());
        }

        public ActionResult Edit(int id = 0)
        {
            ViewBag.Departments = new SelectList(new DepartmentController().GetDepartments(), "DepartmentID", "Name");

            var nurse = uow.NurseRepository.GetByID(id);
            if (nurse == null)
                nurse = new Nurse();

            setAdministrationDialogHeader(id, nurse);

            return View(nurse);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult Edit(Nurse nurse)
        {
            ViewBag.Departments = new SelectList(new DepartmentController().GetDepartments(), "DepartmentID", "Name");

            

            if (ModelState.IsValid)
            {
                if (nurse.NurseID != 0)
                    uow.NurseRepository.Update(nurse);
                else
                    uow.NurseRepository.Insert(nurse);
                uow.Save();
            }
            return RedirectToAction("Index");
        }

        [Authorize]
        public ActionResult EditSimple(int id, int departmentid, NurseRole nurserole) // id = NurseID
        {
            var nurse = uow.NurseRepository.GetByID(id);
            if (nurse == null) 
                nurse = new Nurse();
            
            nurse.DepartmentID = departmentid;
            nurse.NurseRole = nurserole;

            ViewBag.AdministrationDialogHeader = "";
            switch (nurserole)
            {
                default:
                case NurseRole.Nurse:
                    ViewBag.AdministrationDialogSubHeader = "Verpleegkundige aanmaken"; break;
                case NurseRole.Flex:
                    ViewBag.AdministrationDialogSubHeader = "Flex-medewerker aanmaken"; break;
                case NurseRole.Intern:
                    ViewBag.AdministrationDialogSubHeader = "Stagiair(e) aanmaken"; break;
            }

            return View(nurse);
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            if (id != 0)
            {
                uow.NurseRepository.Delete(id);
                uow.Save();
            }
            return RedirectToAction("Index", "Nurse");
        }

        [HttpPost, ValidateAntiForgeryToken]
        [Authorize]
        public JsonResult EditSimple(Nurse nurse)
        {
            if (EmployeeUserHelper.IsDepartmentOfUser(GetSessionObject(), nurse.DepartmentID))
            {
                if (ModelState.IsValid && nurse.FullName() != String.Empty)
                {
                    if (nurse.NurseID != 0)
                        uow.NurseRepository.Update(nurse);
                    else
                        uow.NurseRepository.Insert(nurse);
                    uow.Save();
                }

                return Json(new { Nurse = nurse, FullName = nurse.FullName() });
            }
            return Json("");
        }


        private void setAdministrationDialogHeader(int id, Nurse nurse)
        {
            ViewBag.AdministrationDialogHeader = nurse.FullName();
            ViewBag.AdministrationDialogSubHeader = "Verpleegkundige " + (id <= 0 ? "aanmaken" : "bewerken");
        }



        private IEnumerable<Nurse> getNurses()
        {
            if (Roles.IsUserInRole(Role.PlanbordAdministrator.ToString()))
            {
                return uow.NurseRepository.GetAll();
            }
            else
            {
                var user = Membership.GetUser();
                var userId = (Guid)user.ProviderUserKey;
                var employee = uow.EmployeeRepository.Get(e => e.UserId == userId).FirstOrDefault();
                if (employee == null)
                    return Enumerable.Empty<Nurse>();

                if (Roles.IsUserInRole(Role.OrganizationAdministrator.ToString()))
                {
                    return uow.NurseRepository.Get(n => n.Department.Location.OrganizationID == employee.Department.Location.OrganizationID && !n.InActive);
                }
                else if (Roles.IsUserInRole(Role.PersonalAccount.ToString()))
                {
                    return uow.NurseRepository.Get(n => n.Department.DepartmentID == employee.DepartmentID && !n.InActive);
                }
            }

            return Enumerable.Empty<Nurse>();
        } 

        public IEnumerable<Nurse> GetNursesForDepartment(int departmentID)
        {
            if (!EmployeeUserHelper.IsDepartmentOfUser(GetSessionObject(), departmentID)) return Enumerable.Empty<Nurse>();

            return uow.NurseRepository.Get(n => n.DepartmentID == departmentID && !n.InActive);
        }

    }
}
