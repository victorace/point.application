﻿using Point.Planbord.Database.Context;
using Point.Planbord.Database.Models;
using Point.Planbord.Database.Repository;
using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Point.Planbord.WebApplication.Controllers
{
    public class LoginHistoryController : Controller
    {
        private readonly UnitOfWork<PlanbordContext> uow = new UnitOfWork<PlanbordContext>();

        public void Create(string username, MembershipUser user, DateTime when, HttpContext httpContext, string screenWidth, string screenHeight, bool loginFailed)
        {
            var loginIP = httpContext.Request.UserHostAddress;
            var localIP = httpContext.Request.ServerVariables["LOCAL_ADDR"];
            var resolution = String.Format("{0}x{1}", screenWidth, screenHeight);
            var userAgent = httpContext.Request.UserAgent;

            var loginHistory = new LoginHistory()
            {
                LoginUserName = username,
                LoginDateTime = when,
                Resolution = resolution,
                UserAgent = userAgent,
                LocalIP = localIP,
                LoginIP = loginIP,
                LoginFailed = loginFailed
            };

            if (user != null)
            {
                loginHistory.UserId = new Guid(user.ProviderUserKey.ToString());
            }

            uow.LoginHistoryRepository.Insert(loginHistory);
            uow.Save();
        }
    }
}
