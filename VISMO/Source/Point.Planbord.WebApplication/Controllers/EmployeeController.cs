﻿using Point.Planbord.Database.Context;
using Point.Planbord.Infrastructure;
using Point.Planbord.Infrastructure.Extensions;
using Point.Planbord.Database.Extensions;
using Point.Planbord.Database.Models;
using Point.Planbord.Database.Repository;
using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using System.Collections.Generic;

namespace Point.Planbord.WebApplication.Controllers
{
    [AuthorizeRoles(Role.PlanbordAdministrator, Role.OrganizationAdministrator)]
    public class EmployeeController : BaseController
    {
        private readonly UnitOfWork<PlanbordContext> uow = new UnitOfWork<PlanbordContext>();

        public ActionResult Index()
        {
            IEnumerable<Employee> employees = getEmployees();

            var employeeUsers = (from e in employees
                                 join m in Membership.GetAllUsers().Cast<MembershipUser>() on e.UserId equals m.ProviderUserKey
                                 select new EmployeeUserViewModel
                                 {
                                     UserId = e.UserId,
                                     EmployeeID = e.EmployeeID,
                                     EmployeeNumber = e.EmployeeNumber,
                                     DepartmentID = e.DepartmentID,
                                     DepartmentName = e.Department.Name,
                                     Initials = e.Initials,
                                     FirstName = e.FirstName,
                                     MiddleName = e.MiddleName,
                                     LastName = e.LastName,
                                     MaidenName = e.MaidenName,
                                     Gender = e.Gender,
                                     InActive = e.InActive,
                                     UserName = m.UserName,
                                     LocationID = e.Department.LocationID,
                                     LocationName = e.Department.Location.Name,
                                     OrganizationID = e.Department.Location.OrganizationID,
                                     Email = m.Email,
                                     Role = (Role)Enum.Parse(typeof(Role), Roles.GetRolesForUser(m.UserName)[0]),
                                     RoleList = Roles.GetAllRoles(),
                                     IsLockedOut = m.IsLockedOut,
                                     LastLoginDate = m.LastLoginDate,
                                 }).ToList();
            return View(employeeUsers);
        }

        private IEnumerable<Employee> getEmployees()
        {
            IEnumerable<Employee> employees = null;
            if (Roles.IsUserInRole(Role.PlanbordAdministrator.ToString()))
            {
                employees = uow.EmployeeRepository.Get(e => !e.InActive);
            }
            else
            {
                var organizationId = GetSessionObject().OrganizationID;
                employees = uow.EmployeeRepository.Get(e => e.Department.Location.OrganizationID == organizationId && !e.InActive);
            }
            return employees;
        }

        public ActionResult Edit(int id = 0)
        {
            ViewBag.Organizations = new SelectList(new OrganizationController().GetOrganizations(), "OrganizationID", "Name");

            IEnumerable<Employee> employees = getEmployees();

            var employeeUser = (from e in employees
                                join m in Membership.GetAllUsers().Cast<MembershipUser>() on e.UserId equals m.ProviderUserKey
                                where e.EmployeeID == id
                                select new EmployeeUserEditViewModel
                                 {
                                     UserId = e.UserId,
                                     EmployeeID = e.EmployeeID,
                                     EmployeeNumber = e.EmployeeNumber,
                                     DepartmentID = e.DepartmentID,
                                     Initials = e.Initials,
                                     FirstName = e.FirstName,
                                     MiddleName = e.MiddleName,
                                     LastName = e.LastName,
                                     MaidenName = e.MaidenName,
                                     Gender = e.Gender,
                                     InActive = e.InActive,
                                     UserName = m.UserName,
                                     LocationID = e.Department.LocationID,
                                     OrganizationID = e.Department.Location.OrganizationID,
                                     Email = m.Email,
                                     Role = (Role)Enum.Parse(typeof(Role), Roles.GetRolesForUser(m.UserName)[0]),
                                     SelectedRoles = Roles.GetRolesForUser(m.UserName),
                                    RoleList = Roles.GetAllRoles(),
                                    IsLockedOut = m.IsLockedOut
                                 }).FirstOrDefault();

            setAdministrationDialogHeader(id, employeeUser);

            return View(employeeUser);
        }

        [HttpPost]
        public ActionResult Edit(EmployeeUserEditViewModel employeeUser)
        {
            ViewBag.Organizations = new SelectList(new OrganizationController().GetOrganizations(), "OrganizationID", "Name");

            if (ModelState.IsValid)
            {
                MembershipUser user = Membership.GetUser(employeeUser.UserName);
                if (user != null)
                {
                    string[] roles = Roles.GetRolesForUser(employeeUser.UserName);
                    if (roles.Length > 0)
                        Roles.RemoveUserFromRoles(employeeUser.UserName, roles);
                   
                    foreach (var item in employeeUser.SelectedRoles)
                    {
                        Roles.AddUserToRole(employeeUser.UserName, item);
                    }
                    user.Email = employeeUser.Email;
                    Membership.UpdateUser(user);

                    Employee employee = new Employee();
                    employee.Merge(employeeUser);
                    employee.UserId = (Guid)user.ProviderUserKey;
                    uow.EmployeeRepository.Update(employee);
                }

                uow.Save();

                if (!String.IsNullOrWhiteSpace(employeeUser.Password) && !String.IsNullOrWhiteSpace(employeeUser.ConfirmPassword) && employeeUser.Password == employeeUser.ConfirmPassword)
                {
                    string resetPassword = user.ResetPassword();
                    user.ChangePassword(resetPassword, employeeUser.Password);
                }
            }
            employeeUser.RoleList = Roles.GetAllRoles();
            return View(employeeUser);
        }

        public ActionResult Access(int id = 0)
        {
            IEnumerable<Employee> employees = getEmployees();
            var employeeUser = (from e in employees
                                join m in Membership.GetAllUsers().Cast<MembershipUser>() on e.UserId equals m.ProviderUserKey
                                where e.EmployeeID == id
                                select new EmployeeUserEditViewModel
                                {
                                    UserId = e.UserId,
                                    EmployeeID = e.EmployeeID,
                                    EmployeeNumber = e.EmployeeNumber,
                                    DepartmentID = e.DepartmentID,
                                    Initials = e.Initials,
                                    FirstName = e.FirstName,
                                    MiddleName = e.MiddleName,
                                    LastName = e.LastName,
                                    MaidenName = e.MaidenName,
                                    Gender = e.Gender,
                                    InActive = e.InActive,
                                    UserName = m.UserName,
                                    LocationID = e.Department.LocationID,
                                    OrganizationID = e.Department.Location.OrganizationID,
                                    Email = m.Email,
                                    Role = (Role)Enum.Parse(typeof(Role), Roles.GetRolesForUser(m.UserName)[0]),
                                    IsLockedOut = m.IsLockedOut
                                }).FirstOrDefault();

            setAdministrationDialogHeader(id, employeeUser);

            return View(employeeUser);
        }

        [HttpPost]
        public ActionResult Access(EmployeeUserEditViewModel employeeUser)
        {
            IEnumerable<Employee> employees = getEmployees();
            var userName = (from e in employees
                            join m in Membership.GetAllUsers().Cast<MembershipUser>() on e.UserId equals m.ProviderUserKey
                            where e.EmployeeID == employeeUser.EmployeeID
                            select m.UserName).FirstOrDefault();

            if (!string.IsNullOrWhiteSpace(userName))
            {
                MembershipUser user = Membership.GetUser(userName);
                if (user != null)
                {
                    if (!employeeUser.IsLockedOut)
                        user.UnlockUser();
                    else
                    {
                        if (0 < Membership.MaxInvalidPasswordAttempts && Membership.MaxInvalidPasswordAttempts < 100)
                        {
                            for (int i = 0; i <= Membership.MaxInvalidPasswordAttempts; i++)
                            {
                                Membership.ValidateUser(userName, "why_Does_THEMembership*PROVIDER*nothaveAlockFUNCTIONa_l_r_e_a_d_y!");
                            }
                        }
                    }
                }
            }

            return View(employeeUser);
        }

        public ActionResult Create()
        {
            ViewBag.Organizations = new SelectList(new OrganizationController().GetOrganizations(), "OrganizationID", "Name");
            var employeeUser = new EmployeeUserViewModel();
            employeeUser.RoleList = Roles.GetAllRoles();

            setAdministrationDialogHeader(0, employeeUser);

            return View(employeeUser);
        }

        private void setAdministrationDialogHeader(int id, EmployeeUserBaseViewModel employee)
        {
            ViewBag.AdministrationDialogHeader = employee.UserName + (employee.IsLockedOut ? " (geblokkeerd)" : "");
            ViewBag.AdministrationDialogSubHeader = "Medewerker " + (id == 0 ? "aanmaken" : "bewerken");
        }

        [HttpPost]
        public ActionResult Create(EmployeeUserViewModel employeeUser)
        {
            ViewBag.Organizations = new SelectList(new OrganizationController().GetOrganizations(), "OrganizationID", "Name");

            if (ModelState.IsValid)
            {
                MembershipUser user = Membership.CreateUser(employeeUser.UserName, employeeUser.Password, employeeUser.Email);
                if (user != null)
                {
                    foreach (var item in employeeUser.SelectedRoles)
                    {
                        Roles.AddUserToRole(employeeUser.UserName, item);
                    }
                    employeeUser.RoleList = Roles.GetAllRoles();
                    Employee employee = new Employee();
                    employee.Merge(employeeUser);
                    uow.EmployeeRepository.Insert(employee);
                    employee.UserId = (Guid)user.ProviderUserKey;
                }

                uow.Save();

                return View(employeeUser);
            }
            employeeUser.RoleList = Roles.GetAllRoles();
            return PartialView("Create");
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            if (id != 0)
            {
                uow.EmployeeRepository.Delete(id);
                uow.Save();
            }
            return RedirectToAction("Index", "Employee");
        }
    }
}
