﻿using Point.Planbord.Infrastructure;
using Point.Planbord.Database.Models;
using System;
using System.Web.Mvc;
using System.Web.Security;
using Point.Planbord.Database.Repository;
using Point.Planbord.Database.Context;

namespace Point.Planbord.WebApplication.Controllers
{

    [Authorize]
    public class AccountController : BaseController
    {
        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }

        //[AllowAnonymous]
        public ActionResult CreateUser(string user, string pass)
        {
            if (!String.IsNullOrEmpty(user) && Membership.GetUser(user) == null)
            {
                Membership.CreateUser(user, pass);
            }
            
            return RedirectToAction("Login", "Account");
        }

        //[AllowAnonymous]
        public ActionResult DeleteUser(string user)
        {
            if (!String.IsNullOrEmpty(user) && Membership.GetUser(user) != null)
            {
                Membership.DeleteUser(user);
            }

            return RedirectToAction("Login", "Account");
        }

        //
        // POST: /Account/Login
        [AllowAnonymous]
        [HttpPost]
        public ActionResult Login(LoginModel model, string returnUrl, string screenWidth, string screenHeight)
        {
            if (ModelState.IsValid)
            {

                var user = Membership.GetUser(model.UserName);
                var loginDateTime = DateTime.Now;
                var loginHistoryController = new LoginHistoryController();

                if (Membership.ValidateUser(model.UserName, model.Password))
                {
                    SaveEmployeeUserInSession(model.UserName);

                    if (checkIP())
                    {

                        FormsAuthentication.SetAuthCookie(model.UserName, model.RememberMe);

                        loginHistoryController.Create(model.UserName, user, loginDateTime, System.Web.HttpContext.Current, screenWidth, screenHeight, false);

                        if (Roles.IsUserInRole(model.UserName, Role.CapacityAccount.ToString()))
                            return RedirectToAction("CapacityOverview", "Planbord");
                        else
                        {
                            if (Url.IsLocalUrl(returnUrl))
                            {
                                return Redirect(returnUrl);
                            }
                            else
                            {
                                if (Roles.IsUserInRole(model.UserName, Role.PlanbordAdministrator.ToString()) || Roles.IsUserInRole(model.UserName, Role.OrganizationAdministrator.ToString()))
                                    return RedirectToAction("Index", "Administration");
                                else
                                    return RedirectToAction("Index", "Planbord");
                            }
                        }
                    }
                    else
                    {
                        loginHistoryController.Create(model.UserName, user, loginDateTime, System.Web.HttpContext.Current, screenWidth, screenHeight, true);

                        string s = GetSessionObject().LocationID.ToString();

                        ModelState.AddModelError("", "U kunt zich alleen aanmelden vanuit uw werklocatie.");
                    }
                }
                else
                {
                    loginHistoryController.Create(model.UserName, user, loginDateTime, System.Web.HttpContext.Current, screenWidth, screenHeight, true);
                    if (user != null && user.IsLockedOut)
                    {
                        ModelState.AddModelError("", "Uw account is uitgeschakeld doordat het maximum aantal incorrecte aanmeldpogingen is bereikt of doordat de beheerder uw account heeft vergrendeld. Neem contact op met uw beheerder om uw account vrij te geven.");
                    }
                    else
                    {
                        ModelState.AddModelError("", "Gebruikersnaam of wachtwoord is incorrect.");
                    }
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        private bool checkIP()
        {
            if (User.IsInRole(Role.PlanbordAdministrator.ToString())) return true;

            var location = new UnitOfWork<PlanbordContext>().LocationRepository.GetByID(GetSessionObject().LocationID);
            if (location == null || String.IsNullOrEmpty(location.IPAddress) || !location.IPCheck) return true;

            bool ipcorrect = false;
            string currentIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if ( String.IsNullOrEmpty(currentIP) ) currentIP = HttpContext.Request.UserHostAddress;

            foreach (string ip in location.IPAddress.Split(';'))
            {
                if (ip == currentIP) 
                    ipcorrect = true;
            }

            return ipcorrect;

        }

        //
        // GET: /Account/LogOff
        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            SessionWrapper.SetInSession(SessionKey.ValidSession, false);
            return RedirectToAction("Index", "Planbord");
        }

        #region Status Codes
        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "Gebruikersnaam bestaat al. Kies een andere gebruikersnaam";

                case MembershipCreateStatus.DuplicateEmail:
                    return "Een gebruikersnaam voor dit e-mailadres bestaat al. Kies een ander e-mailadres.";

                case MembershipCreateStatus.InvalidPassword:
                    return "Het wachtwoord voldoet niet aan de vereisten (min. 8 karakters, min. 1 cijfer, min 1 leesteken)";

                case MembershipCreateStatus.InvalidEmail:
                    return "Het e-mailadres is ongeldig. Corrigeer deze en probeer het opnieuw.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "Het antwoord is ongeldig. Corrigeer deze en probeer het opnieuw.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "De vraag is ongeldig. Corrigeer deze en probeer het opnieuw.";

                case MembershipCreateStatus.InvalidUserName:
                    return "De gebruikersnaam is ongeldig. Corrigeer deze en probeer het opnieuw.";

                case MembershipCreateStatus.ProviderError:
                    return "Er ging iets mis. Controleer de invoer en probeer het opnieuw.";

                case MembershipCreateStatus.UserRejected:
                    return "Er ging iets mis. Controleer de invoer en probeer het opnieuw.";

                default:
                    return "Er ging iets mis. Controleer de invoer en probeer het opnieuw.";
            }
        }
        #endregion
    }
}
