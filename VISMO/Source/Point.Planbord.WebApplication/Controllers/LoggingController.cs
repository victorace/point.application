﻿using Point.Planbord.Database.Context;
using Point.Planbord.Database.Models;
using Point.Planbord.Database.Repository;
using Point.Planbord.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;
using System.Web.Security;

namespace Point.Planbord.WebApplication.Controllers
{
   
    public class LoggingController : BaseController
    {
        private readonly UnitOfWork<LoggingContext> uow = new UnitOfWork<LoggingContext>();

        [AuthorizeRoles(Role.PlanbordAdministrator)]
        public ActionResult Index()
        {
            var result = Enumerable.Empty<Logging>();
            if (Roles.IsUserInRole(Role.PlanbordAdministrator.ToString()))
            {
                result =  uow.LoggingRepository.Get(l => l.Severity != Severity.Verbose).OrderByDescending(l => l.LoggingDateTime).Take(250);
            }
            return View(result);
        }
        
        [AuthorizeRoles(Role.PlanbordAdministrator)]
        public ActionResult Detail(int id) // id = LoggingID
        {
            var result = uow.LoggingRepository.GetByID(id);
            return View(result);
        }

        [NonAction]
        public void Log(Exception exception)
        {
            string description = exception.Message + Environment.NewLine;
            if (exception.InnerException != null) description += exception.InnerException.ToString() + Environment.NewLine;
            if (exception.StackTrace != null) description += exception.StackTrace + Environment.NewLine;

            Log(Severity.Error, description, null, "");
        }

        [NonAction]
        public void Log(Severity severity, string description, int? referenceID, string referenceName)
        {
            try
            {
                using (UnitOfWork<LoggingContext> u = new UnitOfWork<LoggingContext>())
                {
                    u.LoggingRepository.Insert(new Logging()
                    {
                        LoggingDateTime = DateTime.Now,
                        Severity = severity,
                        Description = description,
                        ApplicationName = ((AssemblyTitleAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyTitleAttribute), false)[0]).Title,
                        ReferenceID = (referenceID.HasValue ? referenceID.Value : (int?)null),
                        ReferenceName = referenceName
                    });
                    u.Save();
                }
            }
            catch(Exception ex)
            { throw ex; }
        }

    }
}
