﻿using Point.Planbord.Database.Context;
using Point.Planbord.Infrastructure;
using Point.Planbord.Database.Models;
using Point.Planbord.Database.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;

namespace Point.Planbord.WebApplication.Controllers
{
    [AuthorizeRoles(Role.PlanbordAdministrator, Role.OrganizationAdministrator)]
    public class DepartmentController : BaseController
    {
        private readonly UnitOfWork<PlanbordContext> uow = new UnitOfWork<PlanbordContext>();

        public ActionResult Index()
        {
            return View(GetDepartments());
        }

        public ActionResult Edit(int id = 0)
        {
            var controller = new LocationController();
            ViewBag.Locations = new SelectList(controller.GetLocations(), "LocationID", "Name");

            var department = uow.DepartmentRepository.GetByID(id);
            if (department == null)
                department = new Department();

            setAdministrationDialogHeader(id, department);

            return View(department);
        }

        private void setAdministrationDialogHeader(int id, Department department)
        {
            ViewBag.AdministrationDialogHeader = department.Name;
            ViewBag.AdministrationDialogSubHeader = "Afdeling " + (id == 0 ? "aanmaken" : "bewerken");

        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult Edit(Department department)
        {
            var controller = new LocationController();
            ViewBag.Locations = new SelectList(controller.GetLocations(), "LocationID", "Name");

            if (ModelState.IsValid)
            {
                if (department.DepartmentID != 0)
                    uow.DepartmentRepository.Update(department);
                else
                    uow.DepartmentRepository.Insert(department);
                uow.Save();
            }
            return View(department);
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            if (id != 0)
            {
                uow.DepartmentRepository.Delete(id);
                uow.Save();
            }
            return RedirectToAction("Index", "Department");
        }

        public IEnumerable<Department> GetDepartments()
        {
            if (Roles.IsUserInRole(Role.PlanbordAdministrator.ToString()))
            {
                return uow.DepartmentRepository.Get(d => !d.InActive);
            }
            else
            {
                var user = Membership.GetUser();
                var userId = (Guid)user.ProviderUserKey;
                var employee = uow.EmployeeRepository.Get(e => e.UserId == userId).FirstOrDefault();
                if (employee == null)
                    return Enumerable.Empty<Department>();

                if (Roles.IsUserInRole(Role.PersonalAccount.ToString()))
                    return uow.DepartmentRepository.Get(d => d.DepartmentID == employee.Department.DepartmentID && !d.InActive);
                else
                    return uow.DepartmentRepository.Get(d => d.Location.OrganizationID == employee.Department.Location.OrganizationID && !d.InActive);
            }
        }
    }
}
