﻿using Point.Planbord.Database.Context;
using Point.Planbord.Database.Models;
using Point.Planbord.Database.Repository;
using Point.Planbord.Database;
using Point.Planbord.Database.Extensions;
using Point.Planbord.Infrastructure;
using Point.Planbord.Infrastructure.Extensions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Security;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace Point.Planbord.WebApplication.Controllers
{
    [AuthorizeRoles(Role.PlanbordAdministrator, Role.OrganizationAdministrator)]
    public class IconController : BaseController
    {
        private enum IconType
        {
            Large = 0,
            Small = 1
        }

        private readonly UnitOfWork<PlanbordContext> uow = new UnitOfWork<PlanbordContext>();
        
        public ActionResult Upload(int organizationID)
        {
            ViewBag.OrganizationID = organizationID;
            var organization = uow.OrganizationRepository.GetByID(organizationID);
            IList<Setting> settings = new List<Setting>();
            settings.AddRange(getSettings(s=>s.OrganizationID==organizationID));
            if (settings == null || settings.Count() == 0)
            {
                settings.AddRange(getSettings(s => s.OrganizationID == null && s.LocationID == null && s.DepartmentID == null));
            }

            PlanbordSettingsViewModel planbordSettingsViewModel = new PlanbordSettingsViewModel();
            loadSettingsIconSet(settings, planbordSettingsViewModel);
            planbordSettingsViewModel.ParentId = organizationID;

            mergeDetailIconTypeIntoSettingsDetailIconSet(planbordSettingsViewModel.DetailIconSet, getIcons(organization));

            return View(planbordSettingsViewModel);
        }

        [HttpPost]
        public ActionResult Upload(IEnumerable<HttpPostedFileBase> files, int organizationID = 0)
        {
            string errorMessage = "";

            Organization organization = uow.OrganizationRepository.GetByID(organizationID);
            if (files == null)
                errorMessage = "U moet twee iconen selecteren.";

            if (String.IsNullOrEmpty(errorMessage) && files.Count() != 2)
                errorMessage = "U moet twee iconen selecteren.";

            var path = Path.Combine(Server.MapPath("~/Content/images/BordIcons"), organization.SystemCode);
            if (String.IsNullOrEmpty(errorMessage) && !Directory.Exists(path))
                errorMessage = string.Format("De directory bestaat niet: {0}.", path);

            var duplicates = getDuplicatesIcons(files, organizationID);
            if (String.IsNullOrEmpty(errorMessage) && duplicates.Count() > 0)
                errorMessage = string.Format("Deze iconen bestaan al: ({0}).", string.Join(", ", duplicates));

            if (String.IsNullOrEmpty(errorMessage) && !writeFilesToFileSystem(files, path))
                errorMessage = "Opslaan mislukt.";

            Database.Models.Icon icon = createIcon(files, path, organizationID);
            if (String.IsNullOrEmpty(errorMessage) && icon == null)
                errorMessage = string.Format("Een of meer iconen heeft een ongeldig bestandstype of formaat.");

            if (String.IsNullOrEmpty(errorMessage))
            {
                try
                {
                    uow.IconRepository.Insert(icon);
                    uow.Save();
                }
                catch
                {
                    errorMessage = "Opslaan mislukt.";
                }
            }

            //// IE9 JSON Data “do you want to open or save this file” - http://stackoverflow.com/a/15911008
            string contenttype = Request.AcceptTypes.Contains("application/json") ? "application/json" : "text/plain";

            //// NOTE System.Net.HttpStatusCode.OK is always returned in order to keep IE9 from removing our json object due
            //// to a Bad Request
            Response.StatusCode = (errorMessage == "" ? System.Net.HttpStatusCode.OK : System.Net.HttpStatusCode.OK).To<int>();

            if (errorMessage == "")
                return Json(new { Url = Url.Action("Upload", "Icon", new { OrganizationID = organizationID }, Request.Url.Scheme) }, contenttype);

            else
                return Json(new { ErrorMessage = errorMessage }, contenttype, JsonRequestBehavior.DenyGet); 
        }

        [HttpPost]
        public ActionResult SaveIconProperties(PlanbordSettingsViewModel model, int organizationID)
        {
            Setting setting = getSettings(organizationID, PlanbordSettingsParentType.Organization);
            if(setting == null)
            {  
                //do nothing
            }
           
            else
            { 
                updateSettings(setting, model);
            }
            return View(model);
        }

        private void updateSettings(Setting setting, PlanbordSettingsViewModel planbordSettingsViewModel)
        {
            if (setting != null)
            {
                setting.SettingValue = JsonConvert.SerializeObject(planbordSettingsViewModel.DetailIconSet.Values);
                uow.SettingRepository.Update(setting);
                uow.Save();
            }
        }

        private IEnumerable<string> getDuplicatesIcons(IEnumerable<HttpPostedFileBase> files, int organizationID)
        {
            var icons = uow.IconRepository.Get(i => i.OrganizationID == organizationID);
            var matches = icons.Where(i => files.Any(f => Path.GetFileName(f.FileName) == i.LargeFileName)).Select(i => i.LargeFileName).ToList();
            matches.AddRange(icons.Where(i => files.Any(f => Path.GetFileName(f.FileName) == i.SmallFileName)).Select(i => i.SmallFileName).ToList());
            return matches.Distinct();
        }

        private bool writeFilesToFileSystem(IEnumerable<HttpPostedFileBase> files, string path)
        {
            foreach (HttpPostedFileBase file in files)
            {
                if (file.ContentLength > 0)
                {
                    file.SaveAs(Path.Combine(path, Path.GetFileName(file.FileName)));
                }
            }

            return true;
        }

        private Database.Models.Icon createIcon(IEnumerable<HttpPostedFileBase> files, string path, int organizationID)
        {
            string largeFileName = "";
            string smallFileName = "";

            foreach (var file in files)
            {
                var filename = Path.GetFileName(file.FileName);
                var img = System.Drawing.Image.FromFile(Path.Combine(path, filename));
                if (img.Width == 64 && img.Height == 64)
                    largeFileName = filename;
                else if (img.Width == 20 && img.Height == 20)
                    smallFileName = filename;
                img.Dispose();
            }

            if (string.IsNullOrWhiteSpace(largeFileName) || string.IsNullOrWhiteSpace(smallFileName))
                return null;

            return new Database.Models.Icon()
            {
                OrganizationID = organizationID,
                LargeFileName = largeFileName,
                SmallFileName = smallFileName
            };
        }

        private bool hasAccess(PlanbordSettingsParentType parentType, int parentId)
        {
            EmployeeUserSessionObject employeeUserSessionObject = GetSessionObject();

            if (Roles.IsUserInRole(Role.PlanbordAdministrator.ToString()))
                return true;

            if (parentType == PlanbordSettingsParentType.Organization)
            {
                if (uow.OrganizationRepository.GetByID(parentId) == null)
                    return false;

                if (GetSessionObject().OrganizationID != parentId)
                    return false;
            }
            else if (parentType == PlanbordSettingsParentType.Location)
            {
                Location location = uow.LocationRepository.GetByID(parentId);
                if (location == null)
                    return false;

                if (Roles.IsUserInRole(Role.OrganizationAdministrator.ToString()) && location.OrganizationID != employeeUserSessionObject.OrganizationID)
                    return false;
            }
            else if (parentType == PlanbordSettingsParentType.Department)
            {
                if (uow.DepartmentRepository.GetByID(parentId) == null)
                    return false;

                Department department = uow.DepartmentRepository.GetByID(parentId);
                if (Roles.IsUserInRole(Role.OrganizationAdministrator.ToString()) && department.Location.OrganizationID != employeeUserSessionObject.OrganizationID)
                    return false;

                if (!EmployeeUserHelper.IsDepartmentOfUser(employeeUserSessionObject, parentId))
                    return false;
            }

            return true;
        }
        private Organization getOrganization(PlanbordSettingsParentType parentType, int parentId)
        {
            int organizationID = -1;
            if (parentType == PlanbordSettingsParentType.Organization)
                organizationID = parentId;
            else if (parentType == PlanbordSettingsParentType.Location)
            {
                var location = uow.LocationRepository.GetByID(parentId);
                organizationID = location.OrganizationID;
            }
            else if (parentType == PlanbordSettingsParentType.Department)
            {
                var department = uow.DepartmentRepository.GetByID(parentId);
                var location = uow.LocationRepository.GetByID(department.LocationID);
                organizationID = location.OrganizationID;
            }
            var organization = uow.OrganizationRepository.GetByID(organizationID);
            return organization;
        }

        private void loadSettingsIconSet(IList<Setting> settings, PlanbordSettingsViewModel planbordSettingsViewModel)
        {
            Setting setting = settings.Where(s => s.SettingMainType == SettingMainType.Icons).FirstOrDefault();

            bool isParentSetting = isSettingFromParent(setting, planbordSettingsViewModel.ParentType, planbordSettingsViewModel.ParentId);

            planbordSettingsViewModel.DetailIconSet = new SettingsIconSet()
            {
                Id = setting.EntityID,
                SettingMainType = SettingMainType.Icons,
                Values = JsonConvert.DeserializeObject<SettingsIconSetValues>(setting.SettingValue, new JsonSerializerSettings { DefaultValueHandling = DefaultValueHandling.Populate }),
                Checked = isParentSetting
            };
        }
        private bool isSettingFromParent(Setting setting, PlanbordSettingsParentType parentType, int parentId)
        {
            int? id = null;
            if (parentType == PlanbordSettingsParentType.Department)
                id = setting.DepartmentID;
            else if (parentType == PlanbordSettingsParentType.Location)
                id = setting.LocationID;
            else if (parentType == PlanbordSettingsParentType.Organization)
                id = setting.OrganizationID;
            return (id.HasValue && id.Value == parentId);
        }

        private IList<Setting> getDefaultSettings()
        {
            return getSettings(s => !s.OrganizationID.HasValue && !s.LocationID.HasValue && !s.DepartmentID.HasValue);
        }

        private IList<Setting> getSettings(Expression<Func<Setting, bool>> predicate)
        {
            return uow.SettingRepository.Get(predicate).ToList<Setting>();
        }

        private void mergeDetailIconTypeIntoSettingsDetailIconSet(SettingsIconSet settingsIconSet, IList<Icon> icons)
        {
            int count = icons.Count();
            for (int index = 0; index < count; index++)
            {
                if (settingsIconSet.Values.Icons.Where(i => i.DetailIconType == index).Count() == 0)
                {
                    settingsIconSet.Values.Icons.Add(new SettingsIconSetIcons
                    {
                        DetailIconType = index,
                        Name = "",
                        Description = "",
                        Visible = false,
                        IsDeleted = false
                    });
                }
            }
        }

        private IList<Icon> getIcons(Organization organization)
        {
            return uow.IconRepository.Get(i => i.OrganizationID == organization.OrganizationID).ToList();
        }

        private Setting getSettings(int id, PlanbordSettingsParentType parentType, List<SettingMainType> settingMainTypes = null)
        {
            IList<Setting> settings = new List<Setting>();
            if (parentType == PlanbordSettingsParentType.Organization)
                settings = getSettings(s => s.OrganizationID == id && !s.DepartmentID.HasValue && !s.LocationID.HasValue);
                
                foreach(Setting setting in settings)
                {
                if (setting.SettingMainType == SettingMainType.Icons)
                     return setting;
                }
            return null;
                //settings = settings.Where(s => settingMainTypes.Contains(SettingMainType.Icons)).ToList();
        }
    }
}