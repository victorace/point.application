﻿using Point.Planbord.Database;
using Point.Planbord.Database.Context;
using Point.Planbord.Database.Extensions;
using Point.Planbord.Database.Models;
using Point.Planbord.Database.Repository;
using Point.Planbord.Infrastructure;
using Point.Planbord.Infrastructure.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Point.Planbord.WebApplication.Controllers
{
    [Authorize]
    public class PlanbordController : BaseController
    {
        private readonly UnitOfWork<PlanbordImportContext> uow = new UnitOfWork<PlanbordImportContext>();

        int departmentID = -1;
        int shiftID = -1;

        private void initVars()
        {
            departmentID = GetSessionObject().CurrentDepartmentID;
            shiftID = GetSessionObject().CurrentShiftID;
        }

        [AuthorizeRoles(Role.PlanbordAdministrator, Role.OrganizationAdministrator, Role.PersonalAccount, Role.DepartmentAccount, Role.LogistiekAccount)]
        public ActionResult Index()
        {
            initVars();

            var usersettings = GetSessionObject().UserSettings;

            ViewBag.ShiftID = shiftID;
            ViewBag.CurrentDepartmentName = "VISMO - " + GetSessionObject().CurrentDepartmentName;

            int refreshTimer = usersettings.GetAutomaticRefreshSeconds();
            ViewBag.RefreshTimer = refreshTimer;

            ViewBag.AutoCloseTimer = usersettings.GetAutomaticCloseSeconds();
            ViewBag.DoPinCheck = (String.IsNullOrEmpty(SettingsController.GetPinFromDatabase(GetSessionObject().CurrentDepartmentID)) ? 0 : 1);

            var shifts = uow.ShiftRepository.GetAll();
            ViewBag.ShiftClass = shifts.ShiftStyle(shiftID);

            var planbordData = (from bed in uow.BedRepository.Get(b => (b.DepartmentOverruleID ?? b.DepartmentID) == departmentID && b.SystemCode != null)
                                join bedforpatient in uow.BedForPatientRepository.Get(bfp => bfp.BedPatientStatus > BedPatientStatus.OpnameGepland) on bed.BedID equals bedforpatient.BedID into bedforpatientJoin
                                from bedforpatient in bedforpatientJoin.DefaultIfEmpty()

                                select new
                                {
                                    Bed = bed,
                                    BedForPatient = bedforpatient ?? new BedForPatient(),
                                } into bedforpatientGroup

                                select new PlanbordViewModel
                                {
                                    Bed = bedforpatientGroup.Bed,
                                    BedForPatient = bedforpatientGroup.BedForPatient,
                                    Patient = (from patient in uow.PatientRepository.Get(p => p.PatientID == bedforpatientGroup.BedForPatient.PatientID)
                                               select patient).FirstOrNew(),
                                    ShiftDetails = (from shiftdetail in uow.ShiftDetailRepository.Get(sd => sd.BedID == bedforpatientGroup.Bed.BedID && sd.ShiftID == shiftID, null, "Nurse, Pager")
                                                    select shiftdetail),
                                    Codes = (from code in uow.CodeRepository.Get(c => (c.CodeStatus == CodeStatus.Done || c.CodeStatus == CodeStatus.PlanbordOnly) && c.PatientID == bedforpatientGroup.BedForPatient.PatientID)
                                             select code),
                                    BedCodes = (from code in uow.CodeRepository.Get(c => c.BedID == bedforpatientGroup.Bed.BedID && c.CodeStatus == CodeStatus.PlanbordOnly)
                                                select code)
                                }
                               );

            cachePlanbordViewModel(planbordData, refreshTimer);

            return View(planbordData.OrderBy(r => r.Bed.GetDescription()));
        }

        private void cachePlanbordViewModel(IEnumerable<PlanbordViewModel> planbordData, int refreshSeconds)
        {
            long ticks = DateTime.Now.Ticks;
            ViewBag.Ticks = ticks;

            DateTime expires = DateTime.Now.AddSeconds(refreshSeconds);
            expires.AddMinutes(15);

            string cachestring = string.Format("Department_{0}_Ticks", departmentID);
            HttpRuntime.Cache.Insert(cachestring, ticks, null, expires, System.Web.Caching.Cache.NoSlidingExpiration);

            cachestring = string.Format("Department_{0}_Codes", departmentID);
            HttpRuntime.Cache.Insert(cachestring, planbordData, null, expires, System.Web.Caching.Cache.NoSlidingExpiration);
        }

        [AuthorizeRoles(Role.PlanbordAdministrator, Role.OrganizationAdministrator, Role.PersonalAccount, Role.DepartmentAccount)]
        public ActionResult ShiftDepartmentSelect(int id = 0)
        {
            ViewBag.CurrentShiftId = GetSessionObject().CurrentShiftID;
            ViewBag.Shifts = uow.ShiftRepository.GetAll().ToList();
            ViewBag.CurrentDepartmentId = GetSessionObject().CurrentDepartmentID;
            ViewBag.Departments = new OrganizationController().GetOrganizationDepartments().OrderBy(d => d.Name);
            return View();
        }

        [HttpPost]
        [AuthorizeRoles(Role.PlanbordAdministrator, Role.OrganizationAdministrator, Role.PersonalAccount, Role.DepartmentAccount)]
        public JsonResult ShiftDepartmentSelect(int shiftId, int departmentId)
        {
            if (EmployeeUserHelper.IsDepartmentOfUser(GetSessionObject(), departmentId))
            {
                SessionWrapper.SetInSession(SessionKey.ShiftID, shiftId);

                if (!User.IsInRole(Role.DepartmentAccount.ToString()))
                    SessionWrapper.SetInSession(SessionKey.CurrentDepartmentID, departmentId);

                var departmentName = "";
                var department = uow.DepartmentRepository.GetByID(departmentId);
                if (department != null)
                    departmentName = department.Name;

                SessionWrapper.SetInSession(SessionKey.CurrentDepartmentName, departmentName);
                ViewBag.CurrentDepartmentName = departmentName;
            }
            return Json("");
        }

        public ActionResult Security()
        {

            return View();
        }

        [HttpPost, ValidateAntiForgeryToken]
        public JsonResult Security(int SecurityCode = -1)
        {
            var pin = SettingsController.GetPinFromDatabase(GetSessionObject().CurrentDepartmentID);

            var result = (pin == SecurityCode.ToString() ? "OK" : "NOK");

            return Json(result);
        }





        [AuthorizeRoles(Role.PlanbordAdministrator, Role.OrganizationAdministrator, Role.PersonalAccount, Role.DepartmentAccount)]
        public ActionResult Expected()
        {
            initVars();

            int parentLocation = uow.DepartmentRepository.GetByID(departmentID).LocationID;
            var capacityList = getCapacityListSummed(parentLocation, departmentID);
            ViewBag.Total = capacityList.GetExpectedTotal();

            var tomorrow = DateTime.Now.AddDays(1);
            DateTime tomorrowEOB =  new DateTime(tomorrow.Year, tomorrow.Month, tomorrow.Day, 23, 59, 59, 999);

            List<Bed> allBeds = capacityList.SelectMany(cl=>cl.PhysicalBeds).ToList<Bed>();
            allBeds.AddRange(capacityList.SelectMany(cl=>cl.PatientWithoutBeds)); 

            var result = ( from bed in allBeds
                           join bedforpatient in uow.BedForPatientRepository.Get(bfp => bfp.BedPatientStatus == BedPatientStatus.OpnameGepland && bfp.StartTime <= tomorrowEOB, null, "Patient")
                             on bed.BedID equals bedforpatient.BedID
                           select new PlanbordViewModel()
                           {
                               Bed = bed,
                               BedForPatient = bedforpatient,
                               Patient = bedforpatient.Patient,
                               Codes = (from code in uow.CodeRepository.Get(c => c.CodeStatus == CodeStatus.Done && c.PatientID == bedforpatient.PatientID && c.CodeMainType == CodeMainType.Diagnose) select code).GetDiagnoseCode()
                           }
                         );

            return View(result.OrderBy(r=>r.BedForPatient.StartTime));
        }

        [AuthorizeRoles(Role.PlanbordAdministrator, Role.OrganizationAdministrator, Role.PersonalAccount, Role.DepartmentAccount)]
        public ActionResult Capacity()
        {
            //var today = DateTime.Now;

            initVars();

            int parentLocation = uow.DepartmentRepository.GetByID(departmentID).LocationID;
            var capacityList = getCapacityListSummed(parentLocation, departmentID);
            ViewBag.Total = capacityList.GetCapacityTotal();

            List<Bed> allBeds = capacityList.SelectMany(cl => cl.PhysicalBeds).ToList<Bed>();

            var result = (from bed in allBeds
                          join bedforpatient in uow.BedForPatientRepository.Get(bfp => bfp.BedPatientStatus != BedPatientStatus.Opgenomen && bfp.BedPatientStatus != BedPatientStatus.OntslagGepland)
                            on bed.BedID equals bedforpatient.BedID
                          select new PlanbordViewModel()
                          {
                              Bed = bed,
                              BedForPatient = bedforpatient
                          }
                         );

            return View(result.OrderBy(r => r.Bed.SystemCode));


        }
        public JsonResult Totals()
        {
            initVars();
            int parentlocation = uow.DepartmentRepository.GetByID(departmentID).LocationID;
            var capacityList = getCapacityListSummed(parentlocation, departmentID);

            int expected = capacityList.GetExpectedTotal();
            int capacity = capacityList.GetCapacityTotal();
            int occupied = capacityList.GetBeddenBezetTotal();

            return Json(new { ExpectedTotal = expected, CapacityTotal = capacity, OccupiedTotal = occupied }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CapacityOverview()
        {
            initVars();
            var usersettings = GetSessionObject().UserSettings;
            ViewBag.RefreshTimer = (10 * 60).ToString();
            ViewBag.AutoCloseTimer = usersettings.GetAutomaticCloseSeconds();
            ViewBag.CurrentDepartmentName = "VISMO - Bezettingsoverzicht";// - " + GetSessionObject().CurrentDepartmentName; 

            ViewBag.DoPinCheck = (String.IsNullOrEmpty(SettingsController.GetPinFromDatabase(GetSessionObject().CurrentDepartmentID)) ? 0 : 1);

            int parentlocation = uow.DepartmentRepository.GetByID(departmentID).LocationID;
            var location = uow.LocationRepository.GetByID(parentlocation);

            ViewBag.LocationName = "Locatie: " + location.Name;




            return View(getCapacityListSummed(parentlocation));

        }

        private IEnumerable<CapacityOverviewViewModel> getCapacityListSummed(int locationid, int departmentid = -1)
        {
            var departmentList = (from department in uow.DepartmentRepository.Get(d => d.LocationID == locationid).Where(d=>d.IsVisible==true) // Counting everything off this location, filtering for department at the end
                                  select department.DepartmentID);
            var bedList = from bed in uow.BedRepository.Get(b => departmentList.Contains((int)b.DepartmentID))// && b.SystemCode != null)
                          join depphys in uow.DepartmentRepository.GetAll() on bed.DepartmentID equals depphys.DepartmentID
                            into departmentPhysicalSub
                          from departmentPhysical in departmentPhysicalSub.DefaultIfEmpty()
                          join depprod in uow.DepartmentRepository.GetAll() on bed.DepartmentOverruleID equals depprod.DepartmentID
                            into departmentProductionSub
                          from departmentProduction in departmentProductionSub.DefaultIfEmpty()
                          select new
                          {
                              Bed = bed,
                              BedID = bed.BedID,
                              SystemCode = bed.SystemCode,
                              DepartmentPhysical = departmentPhysical,
                              DepartmentProduction = departmentProduction,
                              Specialism = (from specialism in uow.SpecialismRepository.Get(sp => sp.SpecialismID == (bed.SpecialismOverruleID)) select specialism).FirstOrNew()
                          };

            List<CapacityOverview> capacityOverviewList = new List<CapacityOverview>();
            foreach (var bl in bedList)
            {
                CapacityOverview capacityOverview = null;

                capacityOverview = capacityOverviewList.Where(col => col.DepartmentID == bl.DepartmentPhysical.DepartmentID && col.SpecialismID == bl.Specialism.SpecialismID && col.IsAcute == bl.Bed.IsAcute).FirstOrDefault();
                if (capacityOverview == null)
                {
                    capacityOverview = new CapacityOverview()
                    {
                        DepartmentID = bl.DepartmentPhysical.DepartmentID,
                        SpecialismID = bl.Specialism.SpecialismID,
                        IsAcute = bl.Bed.IsAcute
                    };
                    capacityOverviewList.Add(capacityOverview);
                }

                if (bl.Bed.SystemCode != null)
                {
                    capacityOverview.PhysicalBeds.Add(bl.Bed);

                    if (bl.Bed.DepartmentOverruleID == null || bl.Bed.DepartmentOverruleID == bl.Bed.DepartmentID)
                        capacityOverview.CombinationBeds.Add(bl.Bed); // physical bed is in production on same department
                }
                if (bl.Bed.SystemCode == null && capacityOverview.SpecialismID <= 0) // Add patient without a bed to the "global" bed-pool
                {
                    capacityOverview.PatientWithoutBeds.Add(bl.Bed);
                }


                if (bl.Bed.DepartmentOverruleID != null)
                {
                    capacityOverview = capacityOverviewList.Where(col => col.DepartmentID == bl.DepartmentProduction.DepartmentID && col.DepartmentID == bl.Specialism.SpecialismID && col.IsAcute == bl.Bed.IsAcute).FirstOrDefault();
                    if (capacityOverview == null)
                    {
                        capacityOverview = new CapacityOverview()
                        {
                            DepartmentID = bl.DepartmentProduction.DepartmentID,
                            SpecialismID = bl.Specialism.SpecialismID,
                        };
                        capacityOverviewList.Add(capacityOverview);
                    }
                    capacityOverview.ProductionBeds.Add(bl.Bed); // physical bed is in production on other department
                    capacityOverview.CombinationBeds.Add(bl.Bed);
                }
            }

            var groupCapacityOverviewList = from capacityOverview in capacityOverviewList
                                            group capacityOverview by new
                                            {
                                                capacityOverview.DepartmentID,
                                                capacityOverview.SpecialismID,
                                                capacityOverview.IsAcute
                                            };



            var capacityListSummed = new List<CapacityOverviewViewModel>();
            foreach (var capacityOverview in groupCapacityOverviewList)
            {
                List<Bed> productionbeds = capacityOverview.SelectMany(co => co.ProductionBeds).ToList();
                List<Bed> physicalbeds = capacityOverview.SelectMany(co => co.PhysicalBeds).ToList();
                List<Bed> combinationbeds = capacityOverview.SelectMany(co => co.CombinationBeds).ToList();
                List<Bed> patientwithoutbeds = capacityOverview.SelectMany(co => co.PatientWithoutBeds).ToList();

                int physicalCount = physicalbeds.Count();

                var bedIDs = (from bed in combinationbeds select bed.BedID).ToList();
                var bedForPatient = uow.BedForPatientRepository.Get(bfp => bedIDs.Contains(bfp.BedID)).ToList();
                //remove the double beds(meerdere patienten op 1 bed)
                bedForPatient = bedForPatient.GroupBy(b => b.BedID,(key,group)=>group.First()).ToList();
               
                var bedIDsWithout = (from bed in patientwithoutbeds select bed.BedID).ToList();
                var bedForPatientWithout = uow.BedForPatientRepository.Get(bfp => bedIDsWithout.Contains(bfp.BedID)).ToList();
                var bedForPatientWithoutCount = bedForPatientWithout.Where(bfp => bfp.BedPatientStatus == BedPatientStatus.Opgenomen).Count();

                var lendCount = physicalbeds.Where(b => b.DepartmentID == capacityOverview.Key.DepartmentID && b.DepartmentOverrule != null).Count();
                var borrowCount = productionbeds.Where(b => b.DepartmentOverruleID == capacityOverview.Key.DepartmentID).Count();
                var dischargeCount = bedForPatient.Where(bfp => bfp.BedPatientStatus == BedPatientStatus.OntslagGepland && bfp.EndTime.IsToday()).Count();
                var occupiedCount = bedForPatient.Where(bfp => bfp.BedPatientStatus == BedPatientStatus.Opgenomen || bfp.BedPatientStatus == BedPatientStatus.OntslagGepland).Count();
                var expectedCount = bedForPatient.Where(bfp => bfp.BedPatientStatus == BedPatientStatus.OpnameGepland && bfp.StartTime.IsToday()).Count()
                    + bedForPatientWithout.Where(bfp => bfp.BedPatientStatus == BedPatientStatus.OpnameGepland && bfp.StartTime.IsToday()).Count()
                    + bedForPatientWithout.Where(bfp => bfp.BedPatientStatus == BedPatientStatus.Opgenomen && bfp.StartTime.IsToday()).Count();
                var reservedCount = bedForPatient.Where(bfp => bfp.BedPatientStatus.IsBlokReservering()).Count();
                var blockedCount = bedForPatient.Where(bfp => bfp.BedPatientStatus.IsBlokBlokkade()).Count();
                var closedCount = bedForPatient.Where(bfp => bfp.BedPatientStatus.IsBlokGesloten()).Count();
                var dischargeTomorrowCount = bedForPatient.Where(bfp => bfp.BedPatientStatus == BedPatientStatus.OntslagGepland && bfp.EndTime.IsTomorrow()).Count();
                var expectedTomorrowCount = bedForPatient.Where(bfp => bfp.BedPatientStatus == BedPatientStatus.OpnameGepland && bfp.StartTime.IsTomorrow()).Count()
                    + bedForPatientWithout.Where(bfp => bfp.BedPatientStatus == BedPatientStatus.OpnameGepland && bfp.StartTime.IsTomorrow()).Count();
                var occupiedDayCount = bedForPatient.Where(bfp => (bfp.BedPatientStatus == BedPatientStatus.Opgenomen || bfp.BedPatientStatus == BedPatientStatus.OntslagGepland) && bfp.StartTime.IsSameDay(bfp.EndTime)).Count();
                var occupiedClinicalCount = bedForPatient.Where(bfp => (bfp.BedPatientStatus == BedPatientStatus.Opgenomen || bfp.BedPatientStatus == BedPatientStatus.OntslagGepland) && !bfp.StartTime.IsSameDay(bfp.EndTime)).Count();

                bedIDs = (from b in physicalbeds.Where(pb => pb.BedType == BedType.Eenpersoonskamer) select b.BedID).ToList();
                var eenpersoonskamerCount = bedIDs.Count();
                var eenpersoonskamerOccupiedCount = bedForPatient.Where(bfp => bfp.BedPatientStatus == BedPatientStatus.Opgenomen && bedIDs.Contains(bfp.BedID)).Count();

                bedIDs = (from b in physicalbeds.Where(pb => pb.BedType == BedType.Sluiskamer) select b.BedID).ToList();
                var sluiskamerCount = bedIDs.Count();
                var sluiskamerOccupiedCount = bedForPatient.Where(bfp => bfp.BedPatientStatus == BedPatientStatus.Opgenomen && bedIDs.Contains(bfp.BedID)).Count();


                capacityListSummed.Add(new CapacityOverviewViewModel()
                {
                    DepartmentID = capacityOverview.Key.DepartmentID,
                    DepartmentName = uow.DepartmentRepository.GetByID(capacityOverview.Key.DepartmentID).Name,
                    IsAcute = capacityOverview.Key.IsAcute,
                    SpecialismID = capacityOverview.Key.SpecialismID,
                    Specialism = uow.SpecialismRepository.Get(sp => sp.SpecialismID == capacityOverview.Key.SpecialismID).FirstOrNew(),
                    SettingHeaderMessage = SettingsController.GetHeaderMessageFromDatabase(capacityOverview.Key.DepartmentID),
                    PhysicalBeds = physicalbeds,
                    PatientWithoutBeds = patientwithoutbeds,
                    BedTotals = new BedTotals()
                    {
                        Lend = lendCount,
                        Borrow = borrowCount,
                        Production = (physicalCount + borrowCount - lendCount - closedCount),
                        Physical = physicalCount,
                        Free = (physicalCount + borrowCount - occupiedCount - lendCount - reservedCount - blockedCount - closedCount),
                        DischargeToday = dischargeCount,
                        Occupied = occupiedCount,
                        ExpectedToday = expectedCount,
                        Reserved = reservedCount,
                        Blocked = blockedCount,
                        Closed = closedCount,
                        DischargeTomorrow = dischargeTomorrowCount,
                        ExpectedTomorrow = expectedTomorrowCount,
                        Capacity = (physicalCount + borrowCount - lendCount - occupiedCount - reservedCount - blockedCount - closedCount - expectedCount - bedForPatientWithoutCount),
                        Eenpersoonskamers = (eenpersoonskamerCount - eenpersoonskamerOccupiedCount),
                        Sluiskamers = (sluiskamerCount - sluiskamerOccupiedCount),
                        OccupiedDay = occupiedDayCount,
                        OccupiedClinical = occupiedClinicalCount
                    }
                });


            }

            return capacityListSummed.Where(cls=>cls.DepartmentID==departmentid || departmentid==-1).OrderBy(c => c.DepartmentName).ThenBy(c => c.Specialism.Name).ThenBy(c => c.IsAcute).AsEnumerable<CapacityOverviewViewModel>();
        }
    }
}
