﻿using Point.Planbord.Database.Context;
using Point.Planbord.Infrastructure;
using Point.Planbord.Database.Models;
using Point.Planbord.Database.Repository;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;

namespace Point.Planbord.WebApplication.Controllers
{
    [AuthorizeRoles(Role.PlanbordAdministrator, Role.OrganizationAdministrator)]
    public class SpecialismController : BaseController
    {
        private readonly UnitOfWork<PlanbordContext> uow = new UnitOfWork<PlanbordContext>();

        public ActionResult Index()
        {
            var result = uow.SpecialismRepository.GetAll();

            return View(result.OrderBy(s=>s.Name));
        }

        public ActionResult Edit(int id = 0)
        {
            var specialism = uow.SpecialismRepository.GetByID(id);
            if (specialism == null)
                specialism = new Specialism();

            setAdministrationDialogHeader(id, specialism);

            return View(specialism);
        }

        private void setAdministrationDialogHeader(int id, Specialism specialism)
        {
            ViewBag.AdministrationDialogHeader = specialism.Name;
            ViewBag.AdministrationDialogSubHeader = "Specialisme " + (id == 0 ? "aanmaken" : "bewerken");
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult Edit(Specialism specialism)
        {
            if (ModelState.IsValid)
            {
                if (specialism.SpecialismID != 0)
                    uow.SpecialismRepository.Update(specialism);
                else
                    uow.SpecialismRepository.Insert(specialism);
                uow.Save();
            }
            return View(specialism);
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            if (id != 0)
            {
                uow.SpecialismRepository.Delete(id);
                uow.Save();
            }
            return RedirectToAction("Index", "Specialism");
        }

        public IEnumerable<Specialism> GetSpecialisms()
        {
            return uow.SpecialismRepository.GetAll();  //Might need to specify this per organization
        }


    }
}
