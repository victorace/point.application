﻿using Newtonsoft.Json;
using Point.Planbord.Database.Context;
using Point.Planbord.Database.Models;
using Point.Planbord.Database.Repository;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace Point.Planbord.WebApplication.Controllers
{
    public class ScriptController : BaseController
    {
        private readonly UnitOfWork<PlanbordContext> uow = new UnitOfWork<PlanbordContext>();

        [Authorize]
        public string IconDefinitions()
        {
            return DetailIcons() + Environment.NewLine + 
                   MobilityIcons() + Environment.NewLine + 
                   BedIcons();
        }

        [Authorize]
        public ActionResult IconCSS(int id = -1) //organizationID
        {
            StringBuilder css = new StringBuilder();

            var organizationID = id;

            if (organizationID == -1)
            {
                var department = uow.DepartmentRepository.GetByID(GetSessionObject().CurrentDepartmentID);
                var location = uow.LocationRepository.GetByID(department.LocationID);
                organizationID = location.OrganizationID;
            }
            var organization = uow.OrganizationRepository.GetByID(organizationID);
            var icons = uow.IconRepository.Get(i => i.OrganizationID == organizationID).ToList();

            int index = 0;
            foreach (var icon in icons)
            {
                foreach ( var isLarge in new[] {false, true})
                {
                    var url = new Uri(Request.Url, Url.Content(String.Format("~/Content/images/BordIcons/{0}/{1}", organization.SystemCode, (isLarge?icon.LargeFileName:icon.SmallFileName))));
                    css.Append(String.Format(".detailIcon{0}{1} {{ background-image:url('{2}') }} ", (isLarge ? "_xl" : ""), index, url));
                }
                index++;
            }

            return Content(css.ToString(), "text/css");
        }

        [Authorize]
        public string DetailIcons()
        {
            IList<SettingsIconSetIcons> icons = new List<SettingsIconSetIcons>();

            var detailIcons = GetSessionObject().UserSettings.GetDetailIcons().OrderBy(i => i.DetailIconType).ToList();
            foreach (var icon in detailIcons)
            {
                icons.Add(new SettingsIconSetIcons()
                {
                    DetailIconType = icon.DetailIconType,
                    Description = icon.Description,
                    Name = icon.Name,
                    Visible = icon.Visible
                });
            }

            return "var detailIcons = " + JsonConvert.SerializeObject(icons) +";";
        }

        [Authorize]
        public string MobilityIcons()
        {
            string s = @"var mobilityIconsCount = "+ MobilityIconsCount() + @";
                         var mobilityIcons = [
                            [""iconBed"", ""Bedvervoer"", ""Patient moet met bed vervoerd worden naar onderzoeken"", true],
                            [""iconWalk"", ""Lopen"", ""Patient kan zelfstandig lopen"", true],
                            [""iconWheelchair"", ""Rolstoel"", ""Patient mag met rolstoel vervoerd worden naar onderzoeken"", true],
                            [""iconTillift"", ""Tillift"", ""Patient moet met tillift vervoerd worden"", true],
                            [""iconRollator"", ""Rollator"", ""Patient gebruikt rollator"", true],
                            [""iconKrukken"", ""Krukken"", ""Patient gebruikt krukken"", true]
                        ];";
            return s;
        }

        [Authorize]
        public string BedIcons()
        {
            //04-12-14 Only one icon for now, but keeping same logic as mobility and there might be some more in the future, though only 1 will be displayed now.
            string s = @"var bedIcons = [
                            [""iconBedClean"", ""Reinigen"", ""Bed dient gereinigd te worden"", true]
                        ];";
            return s;
        }


        public bool PlanbordColumnsClickable()
        {
            bool planbordcolumnsclickable = true;
            if (ConfigurationManager.AppSettings["PlanbordColumnsClickable"] != null)
            {
                planbordcolumnsclickable = Boolean.Parse(ConfigurationManager.AppSettings["PlanbordColumnsClickable"].ToString());
            }
            return planbordcolumnsclickable;
        }

        public int MobilityIconsCount()
        {
            int mobilityiconscount = 3;
            if (ConfigurationManager.AppSettings["MobilityIconsCount"] != null)
            {
                mobilityiconscount = Int32.Parse(ConfigurationManager.AppSettings["MobilityIconsCount"].ToString());
            }
            return mobilityiconscount;
        }

        public int BedIconsCount()
        {
            //04-12-14 Only one icon for now, but keeping same logic as mobility and there might be some more in the future, though only 1 will be displayed now.
            int bediconscount = 1;
            if (ConfigurationManager.AppSettings["BedIconsCount"] != null)
            {
                bediconscount = Int32.Parse(ConfigurationManager.AppSettings["BedIconsCount"].ToString());
            }
            return bediconscount;
        }

    }
}
