﻿using Point.Planbord.Database.Context;
using Point.Planbord.Infrastructure;
using Point.Planbord.Database.Models;
using Point.Planbord.Database.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;

namespace Point.Planbord.WebApplication.Controllers
{
    [AuthorizeRoles(Role.PlanbordAdministrator, Role.OrganizationAdministrator)]
    public class OrganizationController : BaseController
    {
        private readonly UnitOfWork<PlanbordContext> uow = new UnitOfWork<PlanbordContext>();

        public ActionResult Index()
        {
            return View(GetOrganizations());
        }

        public ActionResult Edit(int id = 0)
        {
            var organization = uow.OrganizationRepository.GetByID(id);
            if (organization == null)
            {
                organization = new Organization();
            }
            setAdministrationDialogHeader(id, organization);
            return View(organization);
        }

        private void setAdministrationDialogHeader(int id, Organization organization)
        {
            ViewBag.AdministrationDialogHeader = organization.Name;
            ViewBag.AdministrationDialogSubHeader = "Organisatie " + (id == 0 ? "aanmaken" : "bewerken");
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult Edit(Organization organization)
        {
            if (ModelState.IsValid)
            {
                if (organization.OrganizationID != 0)
                {
                    uow.OrganizationRepository.Update(organization);
                    uow.Save();
                }
                else
                {
                    if (User.IsInRole(Role.PlanbordAdministrator.ToString()))
                    {
                        uow.OrganizationRepository.Insert(organization);
                        uow.Save();
                    }

                }
                
            }
            return View(organization);
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            if (id != 0)
            {
                uow.OrganizationRepository.Delete(id);
                uow.Save();
            }
            return RedirectToAction("Index", "Organization");
        }

        public IEnumerable<Organization> GetOrganizations()
        {
            if (Roles.IsUserInRole(Role.PlanbordAdministrator.ToString()))
            {
                return uow.OrganizationRepository.Get(o => !o.InActive);
            }
            else
            {
                var organizationId = GetSessionObject().OrganizationID;
                return uow.OrganizationRepository.Get(o => o.OrganizationID == organizationId && !o.InActive);
            }
        }

        public ActionResult LocationsFromOrganization(int id = 0) //organizationid
        {
            var locations = uow.LocationRepository.Get(l=>l.OrganizationID==id && !l.InActive).Select(l => new
            {
                locationID = l.LocationID,
                locationName = l.Name
            });
            return Json(locations, JsonRequestBehavior.AllowGet);
        }

        public IEnumerable<Department> GetOrganizationDepartments()
        {
            if (Roles.IsUserInRole(Role.PlanbordAdministrator.ToString()))
            {
                return uow.DepartmentRepository.Get(d => !d.InActive).ToList();
            }
            else
            {
                if (Roles.IsUserInRole(Role.OrganizationAdministrator.ToString()))
                {
                    var organizationId =  GetSessionObject().OrganizationID;
                    return uow.DepartmentRepository.Get(d => d.Location.OrganizationID == organizationId && !d.InActive);
                }
                else
                {
                    var departmentId = GetSessionObject().DepartmentID;
                    return uow.DepartmentRepository.Get(d => d.DepartmentID == departmentId && !d.InActive);
                }
            }
        }
    }
}