﻿using Newtonsoft.Json;
using Point.Planbord.Database;
using Point.Planbord.Database.Context;
using Point.Planbord.Database.Extensions;
using Point.Planbord.Database.Models;
using Point.Planbord.Database.Models.ViewModels;
using Point.Planbord.Database.Repository;
using Point.Planbord.Infrastructure;
using Point.Planbord.Infrastructure.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Security;

namespace Point.Planbord.WebApplication.Controllers
{
    [Authorize]
    public class SettingsController : BaseController
    {
        private readonly List<SettingMainType> settingTypes = new List<SettingMainType>() {  
            SettingMainType.SpecialismColor,
            SettingMainType.ColumnDisplayType,
            SettingMainType.HeaderMessage,
            SettingMainType.Pin,
            SettingMainType.Icons,
            SettingMainType.Codes,
            SettingMainType.AutomaticClose,
            SettingMainType.AutomaticRefresh,
            SettingMainType.DiagnosisCode,
            SettingMainType.HospitalizationCode 
        };

        private readonly UnitOfWork<PlanbordContext> uow = new UnitOfWork<PlanbordContext>();

        public const string SpecialismStyleColorDefault = "#3BB6F0";

        [AuthorizeRoles(Role.PlanbordAdministrator, Role.OrganizationAdministrator, Role.DepartmentAccount)]
        public ActionResult Index(PlanbordSettingsParentType parentType, int parentId)
        {
            if (!hasAccess(parentType, parentId))
                return RedirectToAction("Index", parentType.ToString());

            var organization = getOrganization(parentType, parentId);

            IList<Setting> settings = buildSettingList(parentType, parentId);

            PlanbordSettingsViewModel planbordSettingsViewModel = new PlanbordSettingsViewModel()
            {
                ParentId = parentId,
                ParentType = parentType
            };

            // SettingsSpecialism
            loadSettingsSpecialism(settings, planbordSettingsViewModel);
            mergeDatabaseSpecialismIntoSettingsSpecialism(planbordSettingsViewModel.Specialism);

            // SettingColumnDisplayType
            loadSettingColumnDisplayType(settings, planbordSettingsViewModel);
            mergePlanbordColumnIntoSettingColumnDisplayType(planbordSettingsViewModel.ColumnDisplayType);

            // SettingsHeaderMessageValues
            loadSettingsHeaderMessage(settings, planbordSettingsViewModel);

            // SettingsPin
            loadSettingsPin(settings, planbordSettingsViewModel);

            // SettingsIconSet
            loadSettingsIconSet(settings, planbordSettingsViewModel);
            
            mergeDetailIconTypeIntoSettingsDetailIconSet(planbordSettingsViewModel.DetailIconSet, getIcons(organization));

            // AutomaticClose
            loadSettingsAutomaticClose(settings, planbordSettingsViewModel);

            // AutomaticRefresh
            loadSettingsAutomaticRefresh(settings, planbordSettingsViewModel);

            // DiagnosisCode
            loadSettingsDiagnosisCode(settings, planbordSettingsViewModel);

            // Ligduur
            loadSettingsHospitalization(settings, planbordSettingsViewModel);

            // SettingsCode
            loadSettingsCode(settings, planbordSettingsViewModel);
            mergeDatabaseCodesIntoSettingsCode(planbordSettingsViewModel.Code);
            // sort by codetype text, not value
            planbordSettingsViewModel.Code.Values.Codes = planbordSettingsViewModel.Code.Values.Codes.OrderBy(c => c.CodeType.GetDescription()).ToList();

            string selectedEntity = "";
            try
            {
                switch (parentType)
                {
                    case PlanbordSettingsParentType.Organization:
                        selectedEntity = String.Format("Organisatie '{0}'", uow.OrganizationRepository.GetByID(parentId).Name);
                        break;
                    case PlanbordSettingsParentType.Location:
                        selectedEntity = String.Format("Locatie '{0}'", uow.LocationRepository.GetByID(parentId).Name);
                        break;
                    case PlanbordSettingsParentType.Department:
                        selectedEntity = String.Format("Afdeling '{0}'", uow.DepartmentRepository.GetByID(parentId).Name);
                        break;

                }
            }
            catch (Exception ex)
            {
                new LoggingController().Log(ex);
            }

            ViewBag.SelectedEntity = selectedEntity;
            ViewBag.OrganizationID = organization.OrganizationID;

            return View(planbordSettingsViewModel);
        }

        private Organization getOrganization(PlanbordSettingsParentType parentType, int parentId)
        {
            int organizationID = -1;
            if (parentType == PlanbordSettingsParentType.Organization)
                organizationID = parentId;
            else if (parentType == PlanbordSettingsParentType.Location)
            {
                var location = uow.LocationRepository.GetByID(parentId);
                organizationID = location.OrganizationID;
            }
            else if (parentType == PlanbordSettingsParentType.Department)
            {
                var department = uow.DepartmentRepository.GetByID(parentId);
                var location = uow.LocationRepository.GetByID(department.LocationID);
                organizationID = location.OrganizationID;
            }
            var organization = uow.OrganizationRepository.GetByID(organizationID);
            return organization;
        }

        [HttpParamAction]
        [HttpPost]
        public ActionResult Cancel()
        {
            return RedirectToAction("Index", ControllerContext.RouteData.Values["parentType"].ToString());
        }

        [HttpParamAction]
        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult Save(PlanbordSettingsViewModel planbordSettingsViewModel)
        {
            if (ModelState.IsValid)
            {
                deleteSettings(planbordSettingsViewModel);

                IList<Setting> settings = getSettings(planbordSettingsViewModel.ParentId, planbordSettingsViewModel.ParentType);
                if (settings.IsNullOrEmpty())
                    createAndSaveSettings(planbordSettingsViewModel);
                else
                    updateSettings(settings, planbordSettingsViewModel);
            }
            return RedirectToAction("Index", planbordSettingsViewModel.ParentType.ToString());
        }

        [HttpParamAction]
        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult Delete(PlanbordSettingsViewModel planbordSettingsViewModel)
        {
            deleteSettings(planbordSettingsViewModel);
            return RedirectToAction("Index", planbordSettingsViewModel.ParentType.ToString());
        }

        [ChildActionOnly]
        [OutputCache(Duration = 1)]
        public ActionResult HeaderMessage(int departmentId = -1)
        {
            if (departmentId <= 0)
                departmentId = GetSessionObject().CurrentDepartmentID;

            SettingHeaderMessageMessage messages = GetHeaderMessageFromDatabase(departmentId);

            if (messages != null && messages.MessageLevel == MessageLevel.Level0)
            {
                return Content("");
            }
            else
            {
                if (!messages.EndDate.HasValue || (messages.EndDate.HasValue && DateTime.Now <= messages.EndDate.Value))
                {
                    ViewBag.HeaderMessage = messages.Text;
                    ViewBag.HeaderMessageLevel = messages.MessageLevel;
                    string color = messages.Color;
                    if (!color.StartsWith("#"))
                        color = String.Format("#{0}", color);
                    ViewBag.HeaderMessageColor = color;
                    return PartialView("HeaderMessage");
                }
                else
                    return Content("");
            }
        }

        public static string GetPinFromDatabase(int departmentId)
        {
            var controller = new SettingsController();
            IList<Setting> settings = controller.GetUserSettings(departmentId);
            var setting = settings.Where(s => s.SettingMainType == SettingMainType.Pin).FirstOrDefault();
            SettingsPin pin = new SettingsPin()
            {
                Values = JsonConvert.DeserializeObject<SettingsPinValues>(setting.SettingValue)
            };
            return pin.Values.Pin;
        }

        public static SettingHeaderMessageMessage GetHeaderMessageFromDatabase(int departmentId)
        {
            var controller = new SettingsController();
            IList<Setting> settings = controller.GetUserSettings(departmentId);
            var setting = settings.Where(s => s.SettingMainType == SettingMainType.HeaderMessage).FirstOrDefault();
            SettingsHeaderMessage headerMessage = new SettingsHeaderMessage()
            {
                Values = JsonConvert.DeserializeObject<SettingsHeaderMessageValues>(setting.SettingValue)
            };
            return headerMessage.Values.Messages.Where(hm => hm.MessageLevel == headerMessage.Values.MessageLevel).FirstOrDefault();
        }

        public IList<Setting> GetUserSettings(int departmentId)
        {
            return buildSettingList(PlanbordSettingsParentType.Department, departmentId);
        }

        private IList<Setting> buildSettingList(PlanbordSettingsParentType parentType, int parentId)
        {
            IList<Setting> settings = getSettings(parentId, parentType);
            List<SettingMainType> missing = settingTypes.Except(settings.Select(s => s.SettingMainType)).ToList();
            if (!missing.IsNullOrEmpty())
            {
                bool searchedAllLevels = false;
                do
                {
                    if (parentType == PlanbordSettingsParentType.Department)
                    {
                        var department = uow.DepartmentRepository.GetByID(parentId);
                        settings.AddRange(getSettings(department.LocationID, PlanbordSettingsParentType.Location, missing));
                        parentType = PlanbordSettingsParentType.Location;
                        parentId = department.Location.LocationID;
                    }
                    else if (parentType == PlanbordSettingsParentType.Location)
                    {
                        var location = uow.LocationRepository.GetByID(parentId);
                        settings.AddRange(getSettings(location.OrganizationID, PlanbordSettingsParentType.Organization, missing));
                        parentType = PlanbordSettingsParentType.Organization;
                        parentId = location.Organization.OrganizationID;
                    }
                    else
                    {
                        // pType == PlanbordSettingsParentType.Organisation
                        settings.AddRange(getDefaultSettings());
                        searchedAllLevels = true;
                    }

                    missing = settingTypes.Except(settings.Select(s => s.SettingMainType)).ToList();

                } while (!searchedAllLevels);
            }
            if (!missing.IsNullOrEmpty())
                throw new ApplicationException("No valid settings. Please contact Techxx.");

            return settings;
        }

        private List<SettingMainType> getMissingSettingsList(IList<Setting> settings)
        {
            return settingTypes.Except(settings.Select(s => s.SettingMainType)).ToList();
        }

        private void createAndSaveSettings(PlanbordSettingsViewModel planbordSettingsViewModel)
        {
            Setting setting = null;

            if (planbordSettingsViewModel.Specialism.Checked)
            {
                setting = new Setting();
                setting.SetParentId(planbordSettingsViewModel.ParentId, planbordSettingsViewModel.ParentType);
                setting.SettingMainType = planbordSettingsViewModel.Specialism.SettingMainType;
                setting.SettingValue = JsonConvert.SerializeObject(planbordSettingsViewModel.Specialism.Values);
                uow.SettingRepository.Insert(setting);
            }

            if (planbordSettingsViewModel.ColumnDisplayType.Checked)
            {
                setting = new Setting();
                setting.SetParentId(planbordSettingsViewModel.ParentId, planbordSettingsViewModel.ParentType);
                setting.SettingMainType = planbordSettingsViewModel.ColumnDisplayType.SettingMainType;
                setting.SettingValue = JsonConvert.SerializeObject(planbordSettingsViewModel.ColumnDisplayType.Values);
                uow.SettingRepository.Insert(setting);
            }

            if (planbordSettingsViewModel.HeaderMessage.Checked)
            {
                setting = new Setting();
                setting.SetParentId(planbordSettingsViewModel.ParentId, planbordSettingsViewModel.ParentType);
                setting.SettingMainType = planbordSettingsViewModel.HeaderMessage.SettingMainType;
                setting.SettingValue = JsonConvert.SerializeObject(planbordSettingsViewModel.HeaderMessage.Values);
                uow.SettingRepository.Insert(setting);
            }

            if (planbordSettingsViewModel.Pin.Checked)
            {
                setting = new Setting();
                setting.SetParentId(planbordSettingsViewModel.ParentId, planbordSettingsViewModel.ParentType);
                setting.SettingMainType = planbordSettingsViewModel.Pin.SettingMainType;
                setting.SettingValue = JsonConvert.SerializeObject(planbordSettingsViewModel.Pin.Values);
                uow.SettingRepository.Insert(setting);
            }

            if (planbordSettingsViewModel.DetailIconSet.Checked)
            {
                setting = new Setting();
                setting.SetParentId(planbordSettingsViewModel.ParentId, planbordSettingsViewModel.ParentType);
                setting.SettingMainType = planbordSettingsViewModel.DetailIconSet.SettingMainType;
                setting.SettingValue = JsonConvert.SerializeObject(planbordSettingsViewModel.DetailIconSet.Values);
                uow.SettingRepository.Insert(setting);
            }

            if (planbordSettingsViewModel.Code.Checked)
            {
                setting = new Setting();
                setting.SetParentId(planbordSettingsViewModel.ParentId, planbordSettingsViewModel.ParentType);
                setting.SettingMainType = planbordSettingsViewModel.Code.SettingMainType;
                setting.SettingValue = JsonConvert.SerializeObject(planbordSettingsViewModel.Code.Values);
                uow.SettingRepository.Insert(setting);
            }

            if (planbordSettingsViewModel.AutomaticClose.Checked)
            {
                setting = new Setting();
                setting.SetParentId(planbordSettingsViewModel.ParentId, planbordSettingsViewModel.ParentType);
                setting.SettingMainType = planbordSettingsViewModel.AutomaticClose.SettingMainType;
                setting.SettingValue = JsonConvert.SerializeObject(planbordSettingsViewModel.AutomaticClose.Values);
                uow.SettingRepository.Insert(setting);
            }

            if (planbordSettingsViewModel.AutomaticRefresh.Checked)
            {
                setting = new Setting();
                setting.SetParentId(planbordSettingsViewModel.ParentId, planbordSettingsViewModel.ParentType);
                setting.SettingMainType = planbordSettingsViewModel.AutomaticRefresh.SettingMainType;
                setting.SettingValue = JsonConvert.SerializeObject(planbordSettingsViewModel.AutomaticRefresh.Values);
                uow.SettingRepository.Insert(setting);
            }

            if (planbordSettingsViewModel.DiagnosisCode.Checked)
            {
                setting = new Setting();
                setting.SetParentId(planbordSettingsViewModel.ParentId, planbordSettingsViewModel.ParentType);
                setting.SettingMainType = planbordSettingsViewModel.DiagnosisCode.SettingMainType;
                setting.SettingValue = JsonConvert.SerializeObject(planbordSettingsViewModel.DiagnosisCode.Values);
                uow.SettingRepository.Insert(setting);
            }

            if (planbordSettingsViewModel.Hospitalization.Checked)
            {
                setting = new Setting();
                setting.SetParentId(planbordSettingsViewModel.ParentId, planbordSettingsViewModel.ParentType);
                setting.SettingMainType = planbordSettingsViewModel.Hospitalization.SettingMainType;
                setting.SettingValue = JsonConvert.SerializeObject(planbordSettingsViewModel.Hospitalization.Values);
                uow.SettingRepository.Insert(setting);
            }

            uow.Save();
        }

        private void deleteSettings(PlanbordSettingsViewModel planbordSettingsViewModel)
        {
            foreach (Setting setting in getSettings(planbordSettingsViewModel.ParentId, planbordSettingsViewModel.ParentType))
                uow.SettingRepository.Delete(setting);
            uow.Save();


        }

        private void updateSettings(IList<Setting> settings, PlanbordSettingsViewModel planbordSettingsViewModel)
        {
            Setting setting = null;

            if (planbordSettingsViewModel.Specialism.Checked)
            {
                setting = settings.Where(s => s.EntityID == planbordSettingsViewModel.Specialism.Id && s.SettingMainType == planbordSettingsViewModel.Specialism.SettingMainType).FirstOrDefault();
                if (setting != null)
                {
                    setting.SettingValue = JsonConvert.SerializeObject(planbordSettingsViewModel.Specialism.Values);
                    uow.SettingRepository.Update(setting);
                }
            }

            if (planbordSettingsViewModel.ColumnDisplayType.Checked)
            {
                setting = settings.Where(s => s.EntityID == planbordSettingsViewModel.ColumnDisplayType.Id && s.SettingMainType == planbordSettingsViewModel.ColumnDisplayType.SettingMainType).FirstOrDefault();
                if (setting != null)
                {
                    setting.SettingValue = JsonConvert.SerializeObject(planbordSettingsViewModel.ColumnDisplayType.Values);
                    uow.SettingRepository.Update(setting);
                }
            }

            if (planbordSettingsViewModel.HeaderMessage.Checked)
            {
                setting = settings.Where(s => s.EntityID == planbordSettingsViewModel.HeaderMessage.Id && s.SettingMainType == planbordSettingsViewModel.HeaderMessage.SettingMainType).FirstOrDefault();
                if (setting != null)
                {
                    setting.SettingValue = JsonConvert.SerializeObject(planbordSettingsViewModel.HeaderMessage.Values);
                    uow.SettingRepository.Update(setting);
                }
            }

            if (planbordSettingsViewModel.Pin.Checked)
            {
                setting = settings.Where(s => s.EntityID == planbordSettingsViewModel.Pin.Id && s.SettingMainType == planbordSettingsViewModel.Pin.SettingMainType).FirstOrDefault();
                if (setting != null)
                {
                    setting.SettingValue = JsonConvert.SerializeObject(planbordSettingsViewModel.Pin.Values);
                    uow.SettingRepository.Update(setting);
                }
            }

            if (planbordSettingsViewModel.DetailIconSet.Checked)
            {
                setting = settings.Where(s => s.EntityID == planbordSettingsViewModel.DetailIconSet.Id && s.SettingMainType == planbordSettingsViewModel.DetailIconSet.SettingMainType).FirstOrDefault();
                if (setting != null)
                {
                    setting.SettingValue = JsonConvert.SerializeObject(planbordSettingsViewModel.DetailIconSet.Values);
                    uow.SettingRepository.Update(setting);
                }
            }

            if (planbordSettingsViewModel.Code.Checked)
            {
                setting = settings.Where(s => s.EntityID == planbordSettingsViewModel.Code.Id && s.SettingMainType == planbordSettingsViewModel.Code.SettingMainType).FirstOrDefault();
                if (setting != null)
                {
                    setting.SettingValue = JsonConvert.SerializeObject(planbordSettingsViewModel.Code.Values);
                    uow.SettingRepository.Update(setting);
                }
            }

            if (planbordSettingsViewModel.AutomaticClose.Checked)
            {
                setting = settings.Where(s => s.EntityID == planbordSettingsViewModel.AutomaticClose.Id && s.SettingMainType == planbordSettingsViewModel.AutomaticClose.SettingMainType).FirstOrDefault();
                if (setting != null)
                {
                    setting.SettingValue = JsonConvert.SerializeObject(planbordSettingsViewModel.AutomaticClose.Values);
                    uow.SettingRepository.Update(setting);
                }
            }

            if (planbordSettingsViewModel.AutomaticRefresh.Checked)
            {
                setting = settings.Where(s => s.EntityID == planbordSettingsViewModel.AutomaticRefresh.Id && s.SettingMainType == planbordSettingsViewModel.AutomaticRefresh.SettingMainType).FirstOrDefault();
                if (setting != null)
                {
                    setting.SettingValue = JsonConvert.SerializeObject(planbordSettingsViewModel.AutomaticRefresh.Values);
                    uow.SettingRepository.Update(setting);
                }
            }

            if (planbordSettingsViewModel.DiagnosisCode.Checked)
            {
                setting = settings.Where(s => s.EntityID == planbordSettingsViewModel.DiagnosisCode.Id && s.SettingMainType == planbordSettingsViewModel.DiagnosisCode.SettingMainType).FirstOrDefault();
                if (setting != null)
                {
                    setting.SettingValue = JsonConvert.SerializeObject(planbordSettingsViewModel.DiagnosisCode.Values);
                    uow.SettingRepository.Update(setting);
                }
            }

            if (planbordSettingsViewModel.Hospitalization.Checked)
            {
                setting = settings.Where(s => s.EntityID == planbordSettingsViewModel.Hospitalization.Id && s.SettingMainType == planbordSettingsViewModel.Hospitalization.SettingMainType).FirstOrDefault();
                {
                    setting.SettingValue = JsonConvert.SerializeObject(planbordSettingsViewModel.Hospitalization.Values);
                    uow.SettingRepository.Update(setting);
                }
            }            

            uow.Save();

            createMissingUpdateSettings(settings, planbordSettingsViewModel);
        }

        private void createMissingUpdateSettings(IList<Setting> settings, PlanbordSettingsViewModel planbordSettingsViewModel)
        {
            bool saveRequired = false;

            foreach (var settingMainType in getMissingSettingsList(settings))
            {
                string value = "";
                switch (settingMainType)
                {
                    case SettingMainType.SpecialismColor:
                        if (planbordSettingsViewModel.Specialism.Checked)
                            value = JsonConvert.SerializeObject(planbordSettingsViewModel.Specialism.Values);
                        break;

                    case SettingMainType.ColumnDisplayType:
                        if (planbordSettingsViewModel.ColumnDisplayType.Checked)
                            value = JsonConvert.SerializeObject(planbordSettingsViewModel.ColumnDisplayType.Values);
                        break;

                    case SettingMainType.HeaderMessage:
                        if (planbordSettingsViewModel.HeaderMessage.Checked)
                            value = JsonConvert.SerializeObject(planbordSettingsViewModel.HeaderMessage.Values);
                        break;

                    case SettingMainType.Pin:
                        if (planbordSettingsViewModel.Pin.Checked)
                            value = JsonConvert.SerializeObject(planbordSettingsViewModel.Pin.Values);
                        break;

                    case SettingMainType.Icons:
                        if (planbordSettingsViewModel.DetailIconSet.Checked)
                            value = JsonConvert.SerializeObject(planbordSettingsViewModel.DetailIconSet.Values);
                        break;

                    case SettingMainType.Codes:
                        if (planbordSettingsViewModel.Code.Checked)
                            value = JsonConvert.SerializeObject(planbordSettingsViewModel.Code.Values);
                        break;

                    case SettingMainType.AutomaticClose:
                        if (planbordSettingsViewModel.AutomaticClose.Checked)
                            value = JsonConvert.SerializeObject(planbordSettingsViewModel.AutomaticClose.Values);
                        break;

                    case SettingMainType.AutomaticRefresh:
                        if (planbordSettingsViewModel.AutomaticRefresh.Checked)
                            value = JsonConvert.SerializeObject(planbordSettingsViewModel.AutomaticRefresh.Values);
                        break;

                    case SettingMainType.DiagnosisCode:
                        if (planbordSettingsViewModel.DiagnosisCode.Checked)
                            value = JsonConvert.SerializeObject(planbordSettingsViewModel.DiagnosisCode.Values);
                        break;

                    case SettingMainType.HospitalizationCode:
                        if (planbordSettingsViewModel.Hospitalization.Checked)
                            value = JsonConvert.SerializeObject(planbordSettingsViewModel.Hospitalization.Values);
                        break;
                }

                if (!String.IsNullOrWhiteSpace(value))
                {
                    Setting setting = new Setting()
                    {
                        SettingMainType = settingMainType,
                        SettingValue = value
                    };

                    if (planbordSettingsViewModel.ParentType == PlanbordSettingsParentType.Department)
                        setting.DepartmentID = planbordSettingsViewModel.ParentId;
                    else if (planbordSettingsViewModel.ParentType == PlanbordSettingsParentType.Location)
                        setting.LocationID = planbordSettingsViewModel.ParentId;
                    else if (planbordSettingsViewModel.ParentType == PlanbordSettingsParentType.Organization)
                        setting.OrganizationID = planbordSettingsViewModel.ParentId;

                    uow.SettingRepository.Insert(setting);

                    saveRequired = true;
                }

            }

            if (saveRequired)
                uow.Save();
        }

        private IList<Setting> getSettings(int id, PlanbordSettingsParentType parentType, List<SettingMainType> settingMainTypes = null)
        {
            IList<Setting> settings = new List<Setting>();
            if (parentType == PlanbordSettingsParentType.Organization)
                settings = getSettings(s => s.OrganizationID == id && !s.DepartmentID.HasValue && !s.LocationID.HasValue);
            else if (parentType == PlanbordSettingsParentType.Location)
                settings = getSettings(s => s.LocationID == id && !s.OrganizationID.HasValue && !s.DepartmentID.HasValue);
            else if (parentType == PlanbordSettingsParentType.Department)
                settings = getSettings(s => s.DepartmentID == id && !s.OrganizationID.HasValue && !s.LocationID.HasValue);

            if (!settingMainTypes.IsNullOrEmpty())
                settings = settings.Where(s => settingMainTypes.Contains(s.SettingMainType)).ToList();

            return settings;
        }

        private IList<Setting> getDefaultSettings()
        {
            return getSettings(s => !s.OrganizationID.HasValue && !s.LocationID.HasValue && !s.DepartmentID.HasValue);
        }

        private IList<Setting> getSettings(Expression<Func<Setting, bool>> predicate)
        {
            return uow.SettingRepository.Get(predicate).ToList<Setting>();
        }

        private bool isSettingFromParent(Setting setting, PlanbordSettingsParentType parentType, int parentId)
        {
            int? id = null;
            if (parentType == PlanbordSettingsParentType.Department)
                id = setting.DepartmentID;
            else if (parentType == PlanbordSettingsParentType.Location)
                id = setting.LocationID;
            else if (parentType == PlanbordSettingsParentType.Organization)
                id = setting.OrganizationID;
            return (id.HasValue && id.Value == parentId);
        }

        private void loadSettingColumnDisplayType(IList<Setting> settings, PlanbordSettingsViewModel planbordSettingsViewModel)
        {
            Setting setting = settings.Where(s => s.SettingMainType == SettingMainType.ColumnDisplayType).FirstOrDefault();

            bool isParentSetting = isSettingFromParent(setting, planbordSettingsViewModel.ParentType, planbordSettingsViewModel.ParentId);

            planbordSettingsViewModel.ColumnDisplayType = new SettingColumnDisplayType()
            {
                Id = setting.EntityID,
                SettingMainType = SettingMainType.ColumnDisplayType,
                Values = JsonConvert.DeserializeObject<IList<SettingColumnDisplayTypeValues>>(setting.SettingValue),
                Checked = isParentSetting
            };
        }

        private void loadSettingsSpecialism(IList<Setting> settings, PlanbordSettingsViewModel planbordSettingsViewModel)
        {
            Setting setting = settings.Where(s => s.SettingMainType == SettingMainType.SpecialismColor).FirstOrDefault();

            bool isParentSetting = isSettingFromParent(setting, planbordSettingsViewModel.ParentType, planbordSettingsViewModel.ParentId);

            planbordSettingsViewModel.Specialism = new SettingsSpecialism()
            {
                Id = setting.EntityID,
                SettingMainType = SettingMainType.SpecialismColor,
                Values = JsonConvert.DeserializeObject<IList<SettingsSpecialismValues>>(setting.SettingValue),
                Checked = isParentSetting
            };
        }

        private void loadSettingsCode(IList<Setting> settings, PlanbordSettingsViewModel planbordSettingsViewModel)
        {
            Setting setting = settings.Where(s => s.SettingMainType == SettingMainType.Codes).FirstOrDefault();

            bool isParentSetting = isSettingFromParent(setting, planbordSettingsViewModel.ParentType, planbordSettingsViewModel.ParentId);

            planbordSettingsViewModel.Code = new SettingsCode()
            {
                Id = setting.EntityID,
                SettingMainType = SettingMainType.Codes,
                Values = JsonConvert.DeserializeObject<SettingsCodeValues>(setting.SettingValue),
                Checked = isParentSetting
            };
        }

        private void loadSettingsDiagnosisCode(IList<Setting> settings, PlanbordSettingsViewModel planbordSettingsViewModel)
        {
            Setting setting = settings.Where(s => s.SettingMainType == SettingMainType.DiagnosisCode).FirstOrDefault();

            bool isParentSetting = isSettingFromParent(setting, planbordSettingsViewModel.ParentType, planbordSettingsViewModel.ParentId);

            planbordSettingsViewModel.DiagnosisCode = new SettingsDiagnosisCode()
            {
                Id = setting.EntityID,
                SettingMainType = SettingMainType.DiagnosisCode,
                Values = JsonConvert.DeserializeObject<DiagnosisCodeValues>(setting.SettingValue),
                Checked = isParentSetting
            };
        }

        private void loadSettingsHospitalization(IList<Setting> settings, PlanbordSettingsViewModel planbordSettingsViewModel)
        {
            Setting setting = settings.Where(s => s.SettingMainType == SettingMainType.HospitalizationCode).FirstOrDefault();

            bool isParentSetting = isSettingFromParent(setting, planbordSettingsViewModel.ParentType, planbordSettingsViewModel.ParentId);

            planbordSettingsViewModel.Hospitalization = new SettingsHospitalization()
            {
                Id = setting.EntityID,
                SettingMainType = SettingMainType.HospitalizationCode,
                Values = JsonConvert.DeserializeObject<HospitalizationValues>(setting.SettingValue),
                Checked = isParentSetting
            };
        }

        private void loadSettingsAutomaticRefresh(IList<Setting> settings, PlanbordSettingsViewModel planbordSettingsViewModel)
        {
            Setting setting = settings.Where(s => s.SettingMainType == SettingMainType.AutomaticRefresh).FirstOrDefault();

            bool isParentSetting = isSettingFromParent(setting, planbordSettingsViewModel.ParentType, planbordSettingsViewModel.ParentId);

            planbordSettingsViewModel.AutomaticRefresh = new SettingsAutomaticRefresh()
            {
                Id = setting.EntityID,
                SettingMainType = SettingMainType.AutomaticRefresh,
                Values = JsonConvert.DeserializeObject<AutomaticRefreshValues>(setting.SettingValue),
                Checked = isParentSetting
            };
        }

        private void loadSettingsAutomaticClose(IList<Setting> settings, PlanbordSettingsViewModel planbordSettingsViewModel)
        {
            Setting setting = settings.Where(s => s.SettingMainType == SettingMainType.AutomaticClose).FirstOrDefault();

            bool isParentSetting = isSettingFromParent(setting, planbordSettingsViewModel.ParentType, planbordSettingsViewModel.ParentId);

            planbordSettingsViewModel.AutomaticClose = new SettingsAutomaticClose()
            {
                Id = setting.EntityID,
                SettingMainType = SettingMainType.AutomaticClose,
                Values = JsonConvert.DeserializeObject<AutomaticCloseValues>(setting.SettingValue),
                Checked = isParentSetting
            };
        }

        private void loadSettingsIconSet(IList<Setting> settings, PlanbordSettingsViewModel planbordSettingsViewModel)
        {
            Setting setting = settings.Where(s => s.SettingMainType == SettingMainType.Icons).FirstOrDefault();

            bool isParentSetting = isSettingFromParent(setting, planbordSettingsViewModel.ParentType, planbordSettingsViewModel.ParentId);

            planbordSettingsViewModel.DetailIconSet = new SettingsIconSet()
            {
                Id = setting.EntityID,
                SettingMainType = SettingMainType.Icons,
                Values = JsonConvert.DeserializeObject<SettingsIconSetValues>(setting.SettingValue, new JsonSerializerSettings { DefaultValueHandling = DefaultValueHandling.Populate }),
                Checked = isParentSetting
            };
        }

        private void loadSettingsPin(IList<Setting> settings, PlanbordSettingsViewModel planbordSettingsViewModel)
        {
            Setting setting = settings.Where(s => s.SettingMainType == SettingMainType.Pin).FirstOrDefault();

            bool isParentSetting = isSettingFromParent(setting, planbordSettingsViewModel.ParentType, planbordSettingsViewModel.ParentId);

            planbordSettingsViewModel.Pin = new SettingsPin()
            {
                Id = setting.EntityID,
                SettingMainType = SettingMainType.Pin,
                Values = JsonConvert.DeserializeObject<SettingsPinValues>(setting.SettingValue),
                Checked = isParentSetting
            };
        }

        private void loadSettingsHeaderMessage(IList<Setting> settings, PlanbordSettingsViewModel planbordSettingsViewModel)
        {
            Setting setting = settings.Where(s => s.SettingMainType == SettingMainType.HeaderMessage).FirstOrDefault();

            bool isParentSetting = isSettingFromParent(setting, planbordSettingsViewModel.ParentType, planbordSettingsViewModel.ParentId);

            planbordSettingsViewModel.HeaderMessage = new SettingsHeaderMessage()
            {
                Id = setting.EntityID,
                SettingMainType = SettingMainType.HeaderMessage,
                Values = JsonConvert.DeserializeObject<SettingsHeaderMessageValues>(setting.SettingValue),
                Checked = isParentSetting
            };
        }

        private void mergeDatabaseSpecialismIntoSettingsSpecialism(SettingsSpecialism settingsSpecialism)
        {
            foreach (Specialism specialism in getSpecialism())
            {
                if (settingsSpecialism.Values.Where(s => s.SystemCode == specialism.SystemCode).Count() == 0)
                {
                    settingsSpecialism.Values.Add(new SettingsSpecialismValues
                    {
                        Id = specialism.SpecialismID,
                        SystemCode = specialism.SystemCode,
                        Color = SpecialismStyleColorDefault
                    });
                }
            }
            settingsSpecialism.Values = settingsSpecialism.Values.OrderBy(s => s.SystemCode).ToList<SettingsSpecialismValues>();

        }

        private void mergeDatabaseCodesIntoSettingsCode(SettingsCode settingCode)
        {
            if (settingCode.Values == null)
                settingCode.Values = new SettingsCodeValues();
            if (settingCode.Values.Codes == null)
                settingCode.Values.Codes = new List<SettingsCodeCycleValue>();

            foreach (Code code in getSystemCycleCodes())
            {
                try
                {
                    SettingsCodeCycleValue settingsCodeCycleValue = settingCode.Values.Codes
                        .Where(c => c.CodeType == code.CodeType && c.SystemCycle == code.SystemCycle).FirstOrDefault();
                    if (settingsCodeCycleValue == null)
                    {
                        settingCode.Values.Codes.Add(new
                            SettingsCodeCycleValue()
                        {
                            CodeType = code.CodeType,
                            SystemCycle = code.SystemCycle
                        });
                    }
                }
                catch (Exception ex)
                {
                    new LoggingController().Log(ex);
                }
            }
        }

        private void mergeDetailIconTypeIntoSettingsDetailIconSet(SettingsIconSet settingsIconSet, IList<Icon> icons)
        {
            int count = icons.Count();
            for (int index = 0; index < count; index++)
            {
                if (settingsIconSet.Values.Icons.Where(i => i.DetailIconType == index).Count() == 0)
                {
                    settingsIconSet.Values.Icons.Add(new SettingsIconSetIcons
                    {
                        DetailIconType = index,
                        Name = "",
                        Description = "",
                        Visible = false,
                        IsDeleted = false
                    });
                }
            }
        }

        private void mergePlanbordColumnIntoSettingColumnDisplayType(SettingColumnDisplayType settingColumnDisplayType)
        {
            foreach (PlanbordColumn planbordColumn in Enum.GetValues(typeof(PlanbordColumn)))
            {
                if (settingColumnDisplayType.Values.Where(c => c.Column == planbordColumn).Count() == 0)
                {
                    settingColumnDisplayType.Values.Add(new SettingColumnDisplayTypeValues
                    {
                        Column = planbordColumn,
                        DisplayType = ColumnDisplayType.Invisible
                    });
                }
            }
        }

        private IList<Code> getSystemCycleCodes()
        {
            var codesVitaleSignalen = uow.CodeRepository.GetAll().Where(c => c.CodeMainType == CodeMainType.VitaleSignalen).ToList();
            var cyclesVitaleSignalen = codesVitaleSignalen.GroupBy(item => new
            {
                CodeType = item.CodeType,
                SystemCycle = item.SystemCycle,
            }).Select(group => group.First());

            var codesQScore = uow.CodeRepository.GetAll().Where(c => c.CodeMainType == CodeMainType.QScore).ToList();
            var cyclesQScore = codesQScore.GroupBy(item => new
            {
                CodeType = item.CodeType,
                SystemCycle = item.SystemCycle,
            }).Select(group => group.First());

            return cyclesVitaleSignalen.Concat(cyclesQScore).ToList();
        }

        private IList<Specialism> getSpecialism()
        {
            return uow.SpecialismRepository.GetAll().ToList();
        }

        private bool hasAccess(PlanbordSettingsParentType parentType, int parentId)
        {
            EmployeeUserSessionObject employeeUserSessionObject = GetSessionObject();

            if (Roles.IsUserInRole(Role.PlanbordAdministrator.ToString()))
                return true;

            if (parentType == PlanbordSettingsParentType.Organization)
            {
                if (uow.OrganizationRepository.GetByID(parentId) == null)
                    return false;

                if (GetSessionObject().OrganizationID != parentId)
                    return false;
            }
            else if (parentType == PlanbordSettingsParentType.Location)
            {
                Location location = uow.LocationRepository.GetByID(parentId);
                if (location == null)
                    return false;

                if (Roles.IsUserInRole(Role.OrganizationAdministrator.ToString()) && location.OrganizationID != employeeUserSessionObject.OrganizationID)
                    return false;
            }
            else if (parentType == PlanbordSettingsParentType.Department)
            {
                if (uow.DepartmentRepository.GetByID(parentId) == null)
                    return false;

                Department department = uow.DepartmentRepository.GetByID(parentId);
                if (Roles.IsUserInRole(Role.OrganizationAdministrator.ToString()) && department.Location.OrganizationID != employeeUserSessionObject.OrganizationID)
                    return false;

                if (!EmployeeUserHelper.IsDepartmentOfUser(employeeUserSessionObject, parentId))
                    return false;
            }

            return true;
        }

        private IList<Icon> getIcons(Organization organization)
        {
            return uow.IconRepository.Get(i => i.OrganizationID == organization.OrganizationID).ToList();
        }
    }
}



