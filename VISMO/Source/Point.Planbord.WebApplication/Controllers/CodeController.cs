﻿using Newtonsoft.Json;
using Point.Planbord.Database.Context;
using Point.Planbord.Database.Extensions;
using Point.Planbord.Database.Models;
using Point.Planbord.Database.Repository;
using Point.Planbord.Infrastructure;
using Point.Planbord.Infrastructure.Extensions;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Point.Planbord.WebApplication.Controllers
{
    [Authorize]
    public class CodeController : BaseController
    {
        private readonly UnitOfWork<PlanbordContext> uow = new UnitOfWork<PlanbordContext>();

        int departmentID = -1;

        public CodeController()
        {
            departmentID = GetSessionObject().CurrentDepartmentID;
        }

        private void fillViewBag(int patientID)
        {
            fillViewBag(patientID, 0);
        }

        private void fillViewBag(int patientID, long ticks)
        {
            BedForPatient bedForPatient = null;

            if (ticks > 0)
            {
                string cachestring = string.Format("Department_{0}_Ticks", departmentID);
                long cachedTicks = HttpRuntime.Cache[cachestring] != null ? (long)HttpRuntime.Cache[cachestring] : -1;
                if (cachedTicks == ticks)
                {
                    cachestring = string.Format("Department_{0}_Codes", departmentID);
                    IEnumerable<PlanbordViewModel> planbordViewModels = HttpRuntime.Cache[cachestring] as IEnumerable<PlanbordViewModel>;
                    if (planbordViewModels != null)
                        planbordViewModels.Where(pbvm => pbvm.BedForPatient.PatientID == patientID).FirstOrDefault();
                }
            }

            if (bedForPatient == null)
                bedForPatient = uow.BedForPatientRepository.Get(b => b.PatientID == patientID).FirstOrNew();

            ViewBag.PlanbordDialogHeaderLeft = bedForPatient.Patient.FullNameInitialsPatient();
            ViewBag.PlanbordDialogHeaderRight = bedForPatient.Bed.Description();
        }

        private void fillViewBagBed(int bedID)
        {
            fillViewBagBed(bedID, 0);
        }

        private void fillViewBagBed(int bedID, long ticks)
        {
            BedForPatient bedForPatient = null;

            if (ticks > 0)
            {
                string cachestring = string.Format("Department_{0}_Ticks", departmentID);
                long cachedTicks = HttpRuntime.Cache[cachestring] != null ? (long)HttpRuntime.Cache[cachestring] : -1;
                if (cachedTicks == ticks)
                {
                    cachestring = string.Format("Department_{0}_Codes", departmentID);
                    IEnumerable<PlanbordViewModel> planbordViewModels = HttpRuntime.Cache[cachestring] as IEnumerable<PlanbordViewModel>;
                    if (planbordViewModels != null)
                        planbordViewModels.Where(pbvm => pbvm.Bed.BedID == bedID).FirstOrDefault();
                }
            }

            if (bedForPatient == null)
                bedForPatient = uow.BedForPatientRepository.Get(b => b.BedID == bedID).FirstOrNew();

            ViewBag.PlanbordDialogHeaderLeft = (bedForPatient.Patient == null ? "" : bedForPatient.Patient.FullNameInitialsPatient());
            ViewBag.PlanbordDialogHeaderRight = bedForPatient.Bed.Description();
            if (bedForPatient.BedPatientStatus.IsBlokBlokkade() || bedForPatient.BedPatientStatus.IsBlokGesloten() || bedForPatient.BedPatientStatus.IsBlokReservering())
                ViewBag.BlockReason = bedForPatient.BedPatientStatus.GetDescription();
            else
                ViewBag.BlockReason = "";
        }



        //Check if patient belongs to the department of the logged in user.
        private bool checkUserDepartment(int patientID)
        {
            if ( User.IsInRole(Role.PlanbordAdministrator.ToString()) ) return true;

            int departmentID = GetSessionObject().CurrentDepartmentID;
            var bedforpatients = uow.BedForPatientRepository.Get(bfp => bfp.PatientID == patientID && ( bfp.Bed.DepartmentID == departmentID || bfp.Bed.DepartmentOverruleID == departmentID ) );
            if ( bedforpatients == null || bedforpatients.Count() == 0 ) return false;

            return true;
        }

        private bool checkUserBed(int bedID)
        {
            if (User.IsInRole(Role.PlanbordAdministrator.ToString())) return true;

            int departmentID = GetSessionObject().CurrentDepartmentID;
            var beds = uow.BedRepository.Get(b => b.DepartmentID == departmentID && b.BedID == bedID);
            if (beds == null || beds.Count() == 0) return false;

            return true;
        }

        private IEnumerable<Code> getCodes(CodeMainType codemaintype, int patientID)
        {
            if (checkUserDepartment(patientID))
            {
                fillViewBag(patientID);
                return uow.CodeRepository.Get(c => c.CodeMainType == codemaintype
                                              && c.PatientID == patientID
                                              && c.CodeStatus != CodeStatus.Pending);
            }
            else
            {
                return Enumerable.Empty<Code>();
            }
        }

        private IEnumerable<Code> getCachedCodes(CodeMainType codemaintype, int patientID, long ticks)
        {
            IEnumerable<Code> codes = Enumerable.Empty<Code>();

            if (checkUserDepartment(patientID))
            {
                fillViewBag(patientID, ticks);

                string cachestring = string.Format("Department_{0}_Ticks", departmentID);
                long cachedTicks = HttpRuntime.Cache[cachestring] != null ? (long)HttpRuntime.Cache[cachestring] : -1;
                if (cachedTicks == ticks)
                {
                    cachestring = string.Format("Department_{0}_Codes", departmentID);
                    IEnumerable<PlanbordViewModel> planbordViewModels = HttpRuntime.Cache[cachestring] as IEnumerable<PlanbordViewModel>;
                    if (planbordViewModels != null)
                    {
                        var planbordViewModel = planbordViewModels.Where(pbvm => pbvm.Patient.PatientID == patientID).FirstOrDefault();
                        if (planbordViewModel != null)
                            codes = planbordViewModel.Codes.Where(c => c.CodeMainType == codemaintype && c.PatientID == patientID && c.CodeStatus != CodeStatus.Pending);
                    }
                }
                else
                    codes = uow.CodeRepository.Get(c => c.CodeMainType == codemaintype && c.PatientID == patientID && c.CodeStatus != CodeStatus.Pending);
            }

            return codes;
        }

        private IEnumerable<Code> getCachedCodesBed(CodeMainType codemaintype, int bedID, long ticks)
        {
            if (checkUserBed(bedID))
            {
                fillViewBagBed(bedID, ticks);

                // NOTE we don't actually use cache here as the values are planbord only
                return uow.CodeRepository.Get(c => c.CodeMainType == codemaintype
                                              && c.BedID == bedID
                                              && c.CodeStatus != CodeStatus.Pending);
            }
            else
            {
                return Enumerable.Empty<Code>();
            }
        }

        public ActionResult Restriction(int id, long ticks) //id = PatientID
        {
            ViewBag.PlanbordDialogSubHeader = "Code behandelingsbeperking";
            return View(getCachedCodes(CodeMainType.Behandelingsbeperking, id, ticks));
        }

        public ActionResult Vital(int id, long ticks) //id = PatientID
        {
            ViewBag.PlanbordDialogSubHeader = "Vitale signalen";
            return View(getCachedCodes(CodeMainType.VitaleSignalen, id, ticks));
        }

        public ActionResult Isolation(int id, long ticks) //id = PatientID
        {
            ViewBag.PlanbordDialogSubHeader = "Isolatie";
            return View(getCachedCodes(CodeMainType.Isolatie, id, ticks));
        }

        public ActionResult QScore(int id, long ticks) //id = PatientID
        {
            ViewBag.PlanbordDialogSubHeader = "Q-Score";
            return View(getCachedCodes(CodeMainType.QScore, id, ticks));
        }

        public ActionResult EwsScore(int id, long ticks) //id = PatientID
        {
            ViewBag.PlanbordDialogSubHeader = "EWS Score";
            return View(getCachedCodes(CodeMainType.EwsScore, id, ticks));
        }

        public ActionResult EditDetails(int id = 0) //id = PatientID
        {
            var detailIconCount = GetSessionObject().UserSettings.GetDetailIcons().OrderBy(i => i.DetailIconType).Count();
            ViewBag.PlanbordDialogSubHeader = "Bijzonderheden";
            Code code = getCodes(CodeMainType.Bijzonderheden, id).FirstOrNew();
            if (String.IsNullOrEmpty(code.CodeValue)) code.CodeValue = new String('0', detailIconCount);
            ViewBag.Clickable = new ScriptController().PlanbordColumnsClickable();
            return View(code);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public JsonResult EditDetails(int id, Code code) //id = PatientID
        {

            if (ModelState.IsValid && checkUserDepartment(id))
            {
                Code uowCode = saveCodeValue(id, null, code, CodeMainType.Bijzonderheden);
                return Json(new { Code = uowCode }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { Code = code }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult EditAppointment(int id = 0) //id = PatientID
        {
            ViewBag.PlanbordDialogSubHeader = "Afspraken";
            Code code = getCodes(CodeMainType.Afspraken, id).FirstOrNew();
            ViewBag.Clickable = new ScriptController().PlanbordColumnsClickable();
            return View(code);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public JsonResult EditAppointment(int id, Code code) //id = PatientID
        {

            if (ModelState.IsValid && checkUserDepartment(id))
            {
                Code uowCode = saveCodeValue(id, null,  code, CodeMainType.Afspraken);
                return Json(new { Code = uowCode }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { Code = code }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult EditMobility(int id = 0) //id = PatientID
        {
            ViewBag.PlanbordDialogSubHeader = "Mobiliteit";
            Code code = getCodes(CodeMainType.Mobiliteit, id).FirstOrNew();
            if (String.IsNullOrEmpty(code.CodeValue)) code.CodeValue = new String('0', new ScriptController().MobilityIconsCount());
            ViewBag.Clickable = new ScriptController().PlanbordColumnsClickable();
            return View(code);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public JsonResult EditMobility(int id, Code code) //id = PatientID
        {

            if (ModelState.IsValid && checkUserDepartment(id))
            {
                Code uowCode = saveCodeValue(id, null, code, CodeMainType.Mobiliteit);
                return Json(new { Code = uowCode }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { Code = code }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult EditBed(int id, long ticks) //id = BedID
        {
            ViewBag.PlanbordDialogSubHeader = "Bed";
            Code code = getCachedCodesBed(CodeMainType.Bed, id, ticks).FirstOrNew();
            if (String.IsNullOrEmpty(code.CodeValue)) code.CodeValue = new String('0', new ScriptController().BedIconsCount());
            return View(code);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public JsonNetResult EditBed(int id, Code code) //id = BedID
        {

            var settings = new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore };

            if (ModelState.IsValid && checkUserBed(id))
            {
                Code uowCode = saveCodeValue(null, id, code, CodeMainType.Bed);
                return new JsonNetResult() { Data = new { Code = uowCode }, JsonRequestBehavior = JsonRequestBehavior.AllowGet, SerializerSettings = settings };
            }
            return new JsonNetResult() { Data = new { Code = code }, JsonRequestBehavior = JsonRequestBehavior.AllowGet, SerializerSettings = settings };

        }


        private Code saveCodeValue(int? patientID, int? bedID, Code code, CodeMainType codemaintype) 
        {
            Code uowCode;

            if (code.EntityID <= 0)
            {
                uowCode = new Code() { CodeMainType = codemaintype, CodeStatus = CodeStatus.PlanbordOnly, PatientID = patientID, BedID = bedID, TimeStamp = DateTime.Now };
                uow.CodeRepository.Insert(uowCode);
            }
            else
            {
                uowCode = uow.CodeRepository.GetByID(code.EntityID);
                uow.CodeRepository.Update(uowCode);
            }
            uowCode.CodeValue = code.CodeValue; 

            uow.Save();

            return uowCode;
        }

        protected override void Dispose(bool disposing)
        {
            uow.Dispose();
            base.Dispose(disposing);
        }
    
    }
}