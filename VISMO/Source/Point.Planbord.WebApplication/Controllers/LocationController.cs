﻿using Point.Planbord.Database.Context;
using Point.Planbord.Infrastructure;
using Point.Planbord.Database.Models;
using Point.Planbord.Database.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;

namespace Point.Planbord.WebApplication.Controllers
{
    [AuthorizeRoles(Role.PlanbordAdministrator, Role.OrganizationAdministrator)]
    public class LocationController : BaseController
    {
        private readonly UnitOfWork<PlanbordContext> uow = new UnitOfWork<PlanbordContext>();

        public ActionResult Index()
        {
            return View(GetLocations());
        }

        public ActionResult Edit(int id = 0)
        {
            var controller = new OrganizationController();
            ViewBag.Organizations = new SelectList(controller.GetOrganizations(), "OrganizationID", "Name");

            var location = uow.LocationRepository.GetByID(id);
            if (location == null)
                location = new Location();

            setAdministrationDialogHeader(id, location);

            return View(location);
        }

        private void setAdministrationDialogHeader(int id, Location location)
        {
            ViewBag.AdministrationDialogHeader = location.Name;
            ViewBag.AdministrationDialogSubHeader = "Locatie " + (id == 0 ? "aanmaken" : "bewerken");
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult Edit(Location location)
        {
            var controller = new OrganizationController();
            ViewBag.Organizations = new SelectList(controller.GetOrganizations(), "OrganizationID", "Name");

            if (ModelState.IsValid)
            {
                if (location.LocationID != 0)
                    uow.LocationRepository.Update(location);
                else
                    uow.LocationRepository.Insert(location);
                uow.Save();
            }
            return View(location);
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            if (id != 0)
            {
                uow.LocationRepository.Delete(id);
                uow.Save();
            }
            return RedirectToAction("Index", "Location");
        }

        public IEnumerable<Location> GetLocations()
        {
            if (Roles.IsUserInRole(Role.PlanbordAdministrator.ToString()))
            {
                return uow.LocationRepository.Get(l => !l.InActive);
            }
            else
            {
                var userId = (Guid)Membership.GetUser().ProviderUserKey;
                var employee = uow.EmployeeRepository.Get(e => e.UserId == userId).FirstOrDefault();
                if (employee == null)
                    return Enumerable.Empty<Location>();
                return uow.LocationRepository.Get(l => l.OrganizationID == employee.Department.Location.OrganizationID && !l.InActive);
            }
        }

        public ActionResult DepartmentsFromLocation(int id = 0) //locationid
        {
            var departments = uow.DepartmentRepository.Get(d=>d.LocationID == id && !d.InActive).Select(d => new
            {
                departmentID = d.DepartmentID,
                departmentName = d.Name
            });
            return Json(departments, JsonRequestBehavior.AllowGet);
        }
    }
}
