﻿using Point.Planbord.Database.Context;
using Point.Planbord.Infrastructure;
using Point.Planbord.Database.Models;
using Point.Planbord.Database.Repository;
using System;
using System.Linq;
using System.Web.Mvc;
using Point.Planbord.Infrastructure.Extensions;
using Point.Planbord.Database.Extensions;
using System.Collections.Generic;
using Point.Planbord.Database;

namespace Point.Planbord.WebApplication.Controllers
{
    [Authorize]
    public class ShiftDetailController : BaseController
    {
        private readonly UnitOfWork<PlanbordContext> uow = new UnitOfWork<PlanbordContext>();

        private void fillViewBag(int bedID)
        {
            if (bedID > 0)
            {
                var bedforpatient = uow.BedForPatientRepository.Get(b => b.BedID == bedID, null, "Bed, Patient").FirstOrNew();

                if (bedforpatient.Patient == null) bedforpatient.Patient = new Patient();
                if (bedforpatient.Bed == null) bedforpatient.Bed = uow.BedRepository.Get(b => b.BedID == bedID).FirstOrNew();

                ViewBag.PlanbordDialogHeaderLeft = bedforpatient.Patient.FullNameInitials();
                ViewBag.PlanbordDialogHeaderRight = bedforpatient.Bed.Description();
            }
            else
            {
                ViewBag.PlanbordDialogHeaderLeft = "";
                ViewBag.PlanbordDialogHeaderRight = "Meerdere bedden geselecteerd";

            }
            @ViewBag.PlanbordDialogSubHeader = "Verpleegkundige toewijzen";

        }

        private bool checkUserBedDepartment(int bedID)
        {
            var departmentID = GetSessionObject().CurrentDepartmentID;

            if (User.IsInRole(Role.PlanbordAdministrator.ToString())) return true;

            if (!EmployeeUserHelper.IsDepartmentOfUser(GetSessionObject(), departmentID)) return false;

            var beds = uow.BedRepository.Get(b => b.BedID == bedID && (b.DepartmentOverruleID??b.DepartmentID) == departmentID);

            if (beds == null || beds.Count() == 0) return false;

            return true;
        }

        private IEnumerable<ShiftDetail> getShiftDetailsForBed(int bedID, int shiftID)
        {
            if (checkUserBedDepartment(bedID))
            {
                fillViewBag(bedID);
                return uow.ShiftDetailRepository.Get(sd => sd.BedID == bedID && sd.ShiftID == shiftID, null, "Nurse, Pager");

            }
            else
            {
                return Enumerable.Empty<ShiftDetail>();
            }
        }
       
        private IEnumerable<ShiftDetail> getShiftDepartmentDetailsForNurses(int shiftID, int departmentID)
        {
            return uow.ShiftDetailRepository.Get(sd => sd.Nurse.DepartmentID == departmentID & sd.ShiftID == shiftID, null, "Nurse, Pager");
        }

        private SelectList getNurseListForRole(IEnumerable<Nurse> nurses, IEnumerable<ShiftDetail> shiftdetails, NurseRole nurseRole, int? selectedNurseID = -1)
        {
            List<SelectListItem> nurselist = new List<SelectListItem>();
            nurselist.Add(new SelectListItem() { Text = "", Value = "-1" });
            foreach (var nurse in nurses.Where(n => n.NurseRole == nurseRole).OrderBy(n=>n.LastName))
            {
                var shiftdetail = shiftdetails.FirstOrDefault(sd => sd.NurseID == nurse.NurseID);
                string displaytext = (shiftdetail == null ? nurse.FullName() : shiftdetail.DropdownDisplay());

                nurselist.Add(new SelectListItem() { Text = displaytext, Value = nurse.NurseID.ToString() });
            }
            return new SelectList(nurselist, "Value", "Text", selectedNurseID);

        }

        private SelectList getPagerList(IEnumerable<Pager> pagers, int? selectedPagerID = -1)
        {
            List<SelectListItem> pagerList = new List<SelectListItem>();
            pagerList.Add(new SelectListItem() { Text = "", Value = "-1" });
            foreach (var pager in pagers)
            {
                pagerList.Add(new SelectListItem() { Text = pager.PagerNumber, Value = pager.PagerID.ToString() });
            }
            return new SelectList(pagerList, "Value", "Text", selectedPagerID);
        }

        private void setUniqueVisit(int[] bedIDs, int shiftID)
        {
            int visit1 = 0;
            int visit2 = 0;

            var visits1forbed = uow.ShiftDetailRepository.Get(sd => sd.ShiftID == shiftID && sd.ShiftDetailRole == ShiftDetailRole.FirstNurse && bedIDs.Contains(sd.BedID))
                                                         .Select(sd => sd.Visit ?? 0).Distinct();
            var visits2forbed = uow.ShiftDetailRepository.Get(sd => sd.ShiftID == shiftID && sd.ShiftDetailRole == ShiftDetailRole.SecondNurse && bedIDs.Contains(sd.BedID))
                                                         .Select(sd => sd.Visit ?? 0).Distinct();

            if (visits1forbed.Count() + visits2forbed.Count() == 1) //One of them is filled and unique for the selected bed(s)
            {
                if (visits1forbed.Count() == 1) visit1 = Int32.Parse(visits1forbed.First().ToString());
                if (visits2forbed.Count() == 1) visit2 = Int32.Parse(visits2forbed.First().ToString());
            }
            if (visits1forbed.Count() == 1 && visits2forbed.Count() == 1) //Both of them have 1 value for the selected bed(s)
            {
                if (Int32.Parse(visits1forbed.First().ToString()) > 0 && Int32.Parse(visits2forbed.First().ToString()) > 0)
                {
                    // Different visits assigned, leaving them blank
                }
                else
                {
                    if (Int32.Parse(visits1forbed.First().ToString()) > 0) visit1 = Int32.Parse(visits1forbed.First().ToString());
                    if (Int32.Parse(visits2forbed.First().ToString()) > 0) visit2 = Int32.Parse(visits2forbed.First().ToString());
                }
            }

            ViewBag.Visit1 = visit1;
            ViewBag.Visit2 = visit2;
        }

        private void setWorkload(int[] bedIDs, int shiftID)
        {
            WorkloadType workload1 = WorkloadType.Low;
            WorkloadType workload2 = WorkloadType.Low;

            var workload1forbed = uow.ShiftDetailRepository.Get(sd => sd.ShiftID == shiftID && sd.ShiftDetailRole == ShiftDetailRole.FirstNurse && bedIDs.Contains(sd.BedID))
                                                         .Select(sd => sd.Workload).Distinct();
            var workload2forbed = uow.ShiftDetailRepository.Get(sd => sd.ShiftID == shiftID && sd.ShiftDetailRole == ShiftDetailRole.SecondNurse && bedIDs.Contains(sd.BedID))
                                                         .Select(sd => sd.Workload).Distinct();

            if (workload1forbed.Count() == 1)
            {
                workload1 = workload1forbed.First();
            }
            if (workload2forbed.Count() == 1)
            {
                workload2 = workload2forbed.First();
            }

            ViewBag.Workload1 = (int)workload1;
            ViewBag.Workload2 = (int)workload2;
        }

        public void Reset(int shiftID, int departmentID)
        {
            if (EmployeeUserHelper.IsDepartmentOfUser(GetSessionObject(), departmentID))
            {
                var bedsInDepartment = uow.BedRepository.Get(bed => bed.DepartmentID == departmentID);
                var shiftDetails = getShiftDepartmentDetailsForNurses(shiftID, departmentID).Where(shiftDetail => bedsInDepartment.Contains(shiftDetail.Bed));
                foreach (ShiftDetail shiftDetail in shiftDetails)
                {
                    shiftDetail.PagerID = null;
                    shiftDetail.Pause = 0;
                    shiftDetail.Visit = 0;
                    shiftDetail.Workload = WorkloadType.Low;
                    uow.ShiftDetailRepository.Update(shiftDetail);
                }
                uow.Save();
            }
        }

        public void Delete(int shiftID, int departmentID)
        {
            if (EmployeeUserHelper.IsDepartmentOfUser(GetSessionObject(), departmentID))
            {
                var bedsInDepartment = uow.BedRepository.Get(bed => bed.DepartmentID == departmentID);
                var shiftDetails = getShiftDepartmentDetailsForNurses(shiftID, departmentID).Where(shiftDetail => bedsInDepartment.Contains(shiftDetail.Bed));
                foreach (ShiftDetail shiftDetail in shiftDetails)
                {
                    uow.ShiftDetailRepository.Delete(shiftDetail);
                }
                uow.Save();
            }
        }

        public JsonResult GetNurseInfo(int id = 0, int shiftid = 0) //id = NurseID
        {
            var shiftdetail = uow.ShiftDetailRepository.Get(sd => sd.NurseID == id && sd.ShiftID == shiftid).FirstOrDefault();
            if (shiftdetail == null)
            {
                shiftdetail = new ShiftDetail()
                {
                    Workload = WorkloadType.Low
                };
            }
            
            return Json(new { ShiftDetail = shiftdetail });
            
        }

        public ActionResult Edit(int[] id, int shiftID = 0) //id = BedID
        {
            var departmentID = GetSessionObject().CurrentDepartmentID;

            var shiftdetails = getShiftDetailsForBed( id.Count() == 1 ? id[0] : -1, shiftID);

            var shiftdetailsfornurses = getShiftDepartmentDetailsForNurses(shiftID, departmentID);

            var nurses = new NurseController().GetNursesForDepartment(departmentID);
            var pagers = new PagerController().GetPagersForDepartment(departmentID);

            ViewBag.DepartmentID = departmentID;
            ViewBag.ShiftID = shiftID;
            ViewBag.BedIDs = id; 

            ViewBag.Nurse1List = getNurseListForRole(nurses, shiftdetailsfornurses, NurseRole.Nurse, shiftdetails.FirstOrNew(sd => sd.ShiftDetailRole == ShiftDetailRole.FirstNurse).NurseID);
            ViewBag.Flex1List = getNurseListForRole(nurses, shiftdetailsfornurses, NurseRole.Flex, shiftdetails.FirstOrNew(sd => sd.ShiftDetailRole == ShiftDetailRole.FirstNurse).NurseID);
            ViewBag.Pager1List = getPagerList(pagers,-1);

            ViewBag.Nurse2List = getNurseListForRole(nurses, shiftdetailsfornurses, NurseRole.Nurse, shiftdetails.FirstOrNew(sd => sd.ShiftDetailRole == ShiftDetailRole.SecondNurse).NurseID);
            ViewBag.Flex2List = getNurseListForRole(nurses, shiftdetailsfornurses, NurseRole.Flex, shiftdetails.FirstOrNew(sd => sd.ShiftDetailRole == ShiftDetailRole.SecondNurse).NurseID);
            ViewBag.Pager2List = getPagerList(pagers, -1);

            var internshiftdetails = shiftdetails.Where(sd => sd.ShiftDetailRole == ShiftDetailRole.Intern);
            ViewBag.Intern1List = getNurseListForRole(nurses, shiftdetailsfornurses, NurseRole.Intern, (internshiftdetails.Count() >= 1 ? internshiftdetails.ElementAt(0).NurseID : -1));
            ViewBag.Intern2List = getNurseListForRole(nurses, shiftdetailsfornurses, NurseRole.Intern, (internshiftdetails.Count() >= 2 ? internshiftdetails.ElementAt(1).NurseID : -1));

            setUniqueVisit(id, shiftID);
            setWorkload(id, shiftID);
            
            return View(shiftdetails);
        }

        [HttpPost]
        public JsonResult Edit(string BedIDs, int DepartmentID, int ShiftID, int? Nurse1ID = -1, int? Flex1ID = -1, int? Pause1 = 0, int? Pager1ID = -1, int? Visit1 = 0, int? Workload1 = 0, 
            int? Nurse2ID = -1, int? Flex2ID = -1, int? Pause2 = 0, int? Pager2ID = -1, int? Intern1ID = -1, int? Intern2ID = -1, int? Visit2 = 0, int? Workload2 = 0)
        {

            if (EmployeeUserHelper.IsDepartmentOfUser(GetSessionObject(), DepartmentID))
            {

                var ids = BedIDs.ToIntList();

                foreach (var shiftdetail in uow.ShiftDetailRepository.Get(sd => sd.ShiftID == ShiftID && ids.Contains(sd.BedID))) // Delete what we've got already.
                {
                    uow.ShiftDetailRepository.Delete(shiftdetail);
                }

                foreach (var bedid in ids)
                {

                    // First Nurse:
                    if (Nurse1ID > 0 || Flex1ID > 0)
                    {
                        uow.ShiftDetailRepository.Insert(new ShiftDetail()
                            {
                                BedID = bedid,
                                NurseID = (Nurse1ID > 0 ? Nurse1ID : Flex1ID),
                                ShiftDetailRole = ShiftDetailRole.FirstNurse,
                                ShiftID = ShiftID,
                                Visit = Visit1,
                                Workload = (WorkloadType)Workload1
                            });
                    }
                    // Second Nurse:
                    if (Nurse2ID > 0 || Flex2ID > 0)
                    {
                        uow.ShiftDetailRepository.Insert(new ShiftDetail()
                            {
                                BedID = bedid,
                                NurseID = (Nurse2ID > 0 ? Nurse2ID : Flex2ID),
                                ShiftDetailRole = ShiftDetailRole.SecondNurse,
                                ShiftID = ShiftID,
                                Visit = Visit2,
                                Workload = (WorkloadType)Workload2
                            });
                    }
                    // Intern(s):
                    if (Intern1ID > 0)
                    {
                        uow.ShiftDetailRepository.Insert(new ShiftDetail()
                            {
                                BedID = bedid,
                                NurseID = Intern1ID,
                                ShiftDetailRole = ShiftDetailRole.Intern,
                                ShiftID = ShiftID
                            });
                    }
                    if (Intern2ID > 0)
                    {
                        uow.ShiftDetailRepository.Insert(new ShiftDetail()
                            {
                                BedID = bedid,
                                NurseID = Intern2ID,
                                ShiftDetailRole = ShiftDetailRole.Intern,
                                ShiftID = ShiftID
                            });
                    }


                }
                uow.Save();

                if (Nurse1ID > 0)
                {
                    foreach (var shiftdetail in uow.ShiftDetailRepository.Get(sd => sd.ShiftID == ShiftID && sd.NurseID == Nurse1ID))
                    {
                        shiftdetail.PagerID = null;
                        if (Pager1ID > 0)
                            shiftdetail.PagerID = Pager1ID;
                        shiftdetail.Pause = 0;
                        if (Pause1 != null && Pause1 > 0)
                            shiftdetail.Pause = Pause1;
                        shiftdetail.Workload = WorkloadType.Undefined;
                        if ((WorkloadType)Workload1 > WorkloadType.Undefined)
                            shiftdetail.Workload = (WorkloadType)Workload1;
                        uow.ShiftDetailRepository.Update(shiftdetail);
                    }
                }
                if (Flex1ID > 0)
                {
                    foreach (var shiftdetail in uow.ShiftDetailRepository.Get(sd => sd.ShiftID == ShiftID && sd.NurseID == Flex1ID))
                    {
                        shiftdetail.PagerID = null;
                        if (Pager1ID > 0)
                            shiftdetail.PagerID = Pager1ID;
                        shiftdetail.Pause = 0;
                        if (Pause1 != null && Pause1 > 0)
                            shiftdetail.Pause = Pause1;
                        shiftdetail.Workload = WorkloadType.Undefined;
                        if ((WorkloadType)Workload1 > WorkloadType.Undefined)
                            shiftdetail.Workload = (WorkloadType)Workload1;
                        uow.ShiftDetailRepository.Update(shiftdetail);
                    }
                }
                if (Nurse2ID > 0)
                {
                    foreach (var shiftdetail in uow.ShiftDetailRepository.Get(sd => sd.ShiftID == ShiftID && sd.NurseID == Nurse2ID))
                    {
                        shiftdetail.PagerID = null;
                        if (Pager2ID > 0)
                            shiftdetail.PagerID = Pager2ID;
                        shiftdetail.Pause = 0;
                        if (Pause2 != null && Pause2 > 0)
                            shiftdetail.Pause = Pause2;
                        shiftdetail.Workload = WorkloadType.Undefined;
                        if ((WorkloadType)Workload2 > WorkloadType.Undefined)
                            shiftdetail.Workload = (WorkloadType)Workload2;
                        uow.ShiftDetailRepository.Update(shiftdetail);
                    }
                }
                if (Flex2ID > 0)
                {
                    foreach (var shiftdetail in uow.ShiftDetailRepository.Get(sd => sd.ShiftID == ShiftID && sd.NurseID == Flex2ID))
                    {
                        shiftdetail.PagerID = null;
                        if (Pager2ID > 0)
                            shiftdetail.PagerID = Pager2ID;
                        shiftdetail.Pause = 0;
                        if (Pause2 != null && Pause2 > 0)
                            shiftdetail.Pause = Pause2;
                        shiftdetail.Workload = WorkloadType.Undefined;
                        if ((WorkloadType)Workload2 > WorkloadType.Undefined)
                            shiftdetail.Workload = (WorkloadType)Workload2;
                        uow.ShiftDetailRepository.Update(shiftdetail);
                    }
                }
                uow.Save();
            }

            var newShiftDetails = ( from item 
                                      in ( from bed 
                                             in uow.BedRepository.Get(b => (b.DepartmentOverruleID??b.DepartmentID) == DepartmentID)
                                         select new
                                                {
                                                    BedID = bed.BedID,
                                                    ShiftDetails = (from shiftdetail in uow.ShiftDetailRepository.Get(sd => sd.BedID == bed.BedID && sd.ShiftID == ShiftID, null, "Nurse, Pager") select shiftdetail)
                                                }
                                         )
                                   select new
                                   {
                                       BedID = item.BedID,
                                       FirstNurse = item.ShiftDetails.ShiftDetailForRole(ShiftDetailRole.FirstNurse).NurseDisplay(),
                                       FirstPagerPauseVisit = item.ShiftDetails.ShiftDetailForRole(ShiftDetailRole.FirstNurse).PagerPauseVisitWorkloadDisplay(),
                                       SecondNurse = item.ShiftDetails.ShiftDetailForRole(ShiftDetailRole.SecondNurse).NurseDisplay(),
                                       SecondPagerPauseVisit = item.ShiftDetails.ShiftDetailForRole(ShiftDetailRole.SecondNurse).PagerPauseVisitWorkloadDisplay(),
                                       Intern = item.ShiftDetails.ShiftDetailForRole(ShiftDetailRole.Intern).NurseDisplay(),

                                   }
                                );



            return Json(new { NewShiftDetails = newShiftDetails }); 
        }




    }
}