﻿using Newtonsoft.Json;
using Point.Planbord.Database;
using Point.Planbord.Database.Context;
using Point.Planbord.Database.Models;
using Point.Planbord.Database.Models.ViewModels;
using Point.Planbord.Database.Repository;
using Point.Planbord.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;

namespace Point.Planbord.WebApplication.Controllers
{
    [AuthorizeRoles(Role.PlanbordAdministrator, Role.OrganizationAdministrator, Role.LogistiekAccount)]
    public class DepartmentHeaderMessageController : BaseController
    {
        private readonly UnitOfWork<PlanbordContext> uow = new UnitOfWork<PlanbordContext>();

        public ActionResult Index()
        {
            return View(getDepartmentHeaderMessages());
        }

        private void setAdministrationDialogHeader(int id, Department department)
        {
            string header = "";
            string subHeader = "";

            if (department != null)
            {
                if (!string.IsNullOrWhiteSpace(department.SystemCode) && !string.IsNullOrWhiteSpace(department.Name))
                    header = string.Format("{0} - {1}", department.SystemCode, department.Name);
                else if (!string.IsNullOrWhiteSpace(department.SystemCode))
                    header = department.SystemCode;
                else if (!string.IsNullOrWhiteSpace(department.Name))
                    header = department.Name;

                if (department.Location != null && !string.IsNullOrWhiteSpace(department.Location.Name))
                    subHeader = department.Location.Name;
            }

            ViewBag.AdministrationDialogHeader = header;
            ViewBag.AdministrationDialogSubHeader = subHeader;
        }

        public ActionResult Edit(int id = 0)
        {
            ViewBag.DepartmentID = id;

            var department = uow.DepartmentRepository.GetByID(id);
            if (department != null)
            {
                setAdministrationDialogHeader(id, department);

                SettingsController settingsController = new SettingsController();
                var settings = settingsController.GetUserSettings(id);
                var us = new UserSettings(settings);
                var settingsHeaderMessage = us.GetSettingsHeaderMessage();
                SettingsHeaderMessageEx settingsHeaderMessageEx = new SettingsHeaderMessageEx()
                {
                    DepartmentID = id,
                    Id = settingsHeaderMessage.Id,
                    SettingMainType = settingsHeaderMessage.SettingMainType,
                    Checked = settingsHeaderMessage.Checked,
                    Values = settingsHeaderMessage.Values
                };
                return View(settingsHeaderMessageEx);
            }

            return View();
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult Edit(SettingsHeaderMessageEx settingsHeaderMessageEx)
        {
            if (ModelState.IsValid)
            {
                var setting = uow.SettingRepository.Get(s => s.DepartmentID == settingsHeaderMessageEx.DepartmentID && s.SettingMainType == SettingMainType.HeaderMessage).FirstOrDefault();
                if (setting == null)
                {
                    setting = new Setting()
                    {
                        DepartmentID = settingsHeaderMessageEx.DepartmentID,
                        SettingMainType = SettingMainType.HeaderMessage,
                        SettingValue = JsonConvert.SerializeObject(settingsHeaderMessageEx.Values)
                    };
                    uow.SettingRepository.Insert(setting);
                }
                else
                    setting.SettingValue = JsonConvert.SerializeObject(settingsHeaderMessageEx.Values);

                uow.Save();

            }

            return RedirectToAction("Index");
        }

        private IEnumerable<DepartmentHeaderMessageViewModel> getDepartmentHeaderMessages()
        {
            var departments = Enumerable.Empty<Department>();

            if (Roles.IsUserInRole(Role.PlanbordAdministrator.ToString()))
            {
                departments = uow.DepartmentRepository.Get(d => !d.InActive);
            }
            else
            {
                var user = Membership.GetUser();
                var userId = (Guid)user.ProviderUserKey;
                var employee = uow.EmployeeRepository.Get(e => e.UserId == userId).FirstOrDefault();
                if (employee == null)
                    return Enumerable.Empty<DepartmentHeaderMessageViewModel>();

                if (Roles.IsUserInRole(Role.PersonalAccount.ToString()))
                    departments = uow.DepartmentRepository.Get(d => d.DepartmentID == employee.Department.DepartmentID && !d.InActive);
                else
                    departments = uow.DepartmentRepository.Get(d => d.Location.OrganizationID == employee.Department.Location.OrganizationID && !d.InActive);
            }

            var deparmentHeaderMessages = new List<DepartmentHeaderMessageViewModel>();
            foreach (var department in departments.OrderBy(d => d.SystemCode).ThenBy(d => d.Name).ThenBy(d => d.Location.Name))
            {
                var settingHeaderMessageMessage = SettingsController.GetHeaderMessageFromDatabase(department.DepartmentID);
                deparmentHeaderMessages.Add(new DepartmentHeaderMessageViewModel()
                {
                    DepartmentID = department.DepartmentID,
                    SystemCode = department.SystemCode,
                    DepartmentName = department.Name,
                    LocationName = department.Location.Name,
                    Color = settingHeaderMessageMessage != null ? "#" + settingHeaderMessageMessage.Color : "",
                    HeaderMessage = settingHeaderMessageMessage != null ? settingHeaderMessageMessage.Text : "",
                    EndDate = settingHeaderMessageMessage != null ? settingHeaderMessageMessage.EndDate : null
                });
            }

            return deparmentHeaderMessages;
        }
    }
}
