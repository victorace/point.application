﻿using Point.Planbord.Database;
using Point.Planbord.Database.Context;
using Point.Planbord.Database.Models;
using Point.Planbord.Database.Repository;
using Point.Planbord.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Point.Planbord.WebApplication.Controllers
{
    [Authorize]
    public class BedController : BaseController
    {
        private readonly UnitOfWork<PlanbordContext> uow = new UnitOfWork<PlanbordContext>();

        public ActionResult OverruleDepartment(int departmentId)
        {
            ViewBag.Departments = new SelectList(new DepartmentController().GetDepartments()
                .Where(d => d.DepartmentID != departmentId)
                .OrderBy(d=>d.Name), "DepartmentID", "Name");

            var spec = from specialism in new SpecialismController().GetSpecialisms()
                       select new { Name = specialism.Name + " (" + specialism.ShortCode + ")", SpecialismID = specialism.SpecialismID };
            ViewBag.Specialism = new SelectList(spec, "SpecialismID", "Name");
            
            ViewBag.SelectedEntity = String.Format("Afdeling '{0}'", uow.DepartmentRepository.GetByID(departmentId).Name);

            return View(getBedsForDepartment(departmentId).OrderBy(b => b.RoomNumber).ThenBy(b=>b.SystemCode).ToList());
        }

        [HttpParamAction]
        [HttpPost]
        public ActionResult SaveDepartment(IList<Bed> beds)
        {
            if (ModelState.IsValid)
            {
                foreach (Bed bed in beds)
                {
                    var b = uow.BedRepository.GetByID(bed.BedID);
                    if (b != null)
                    {
                        b.DepartmentOverruleID = bed.DepartmentOverruleID;
                        b.SpecialismOverruleID = bed.SpecialismOverruleID;
                        b.IsAcute = bed.IsAcute;
                        b.BedType = bed.BedType;
                    }
                }

                uow.Save();
            }
            return RedirectToAction("Index", "Department");
        }

        [HttpParamAction]
        [HttpPost]
        public ActionResult CancelDepartment()
        {
            return RedirectToAction("Index", "Department");
        }

        private IEnumerable<Bed> getBedsForDepartment(int departmentId)
        {
            if (!EmployeeUserHelper.IsDepartmentOfUser(GetSessionObject(), departmentId)) return Enumerable.Empty<Bed>();

            return uow.BedRepository.Get(b => b.DepartmentID == departmentId && b.SystemCode != null);
        }




        public ActionResult OverruleSpecialism(int specialismId) //  -1 = without, -2 = all
        {
            var spec = from specialism in new SpecialismController().GetSpecialisms()
                       select new { Name = specialism.Name + " (" + specialism.ShortCode + ")", SpecialismID = specialism.SpecialismID };

            ViewBag.Specialisms = new SelectList(spec, "SpecialismID", "Name");

            var specialismName = "";
            if ( specialismId == -2 )
                specialismName = "Alle bedden";
            else if ( specialismId == -1 )
                specialismName = "Geen specialisme bekend";
            else
                specialismName = String.Format("Specialisme '{0}'",uow.SpecialismRepository.GetByID(specialismId).Name);

            ViewBag.SelectedEntity =  specialismName;
            return View(getBedForSpecialism(specialismId).OrderBy(b => b.SystemCode).ToList());
        }

        [HttpParamAction]
        [HttpPost]
        public ActionResult SaveSpecialism(IList<Bed> beds)
        {
            if (ModelState.IsValid)
            {
                foreach (Bed bed in beds)
                {
                    var b = uow.BedRepository.GetByID(bed.BedID);
                    if (b != null)
                    {
                        b.SpecialismOverruleID = bed.SpecialismOverruleID;
                    }
                }

                uow.Save();
            }
            return RedirectToAction("Index", "Specialism");
        }

        [HttpParamAction]
        [HttpPost]
        public ActionResult CancelSpecialism()
        {
            return RedirectToAction("Index", "Specialism");
        }
     




        private IEnumerable<Bed> getBedForSpecialism(int specialismId)
        {
            if (specialismId == -2)
                return uow.BedRepository.GetAll();
            else if (specialismId == -1)
                return uow.BedRepository.Get(b => b.SpecialismID == null);
            else // if (specialismId > 0)
                return uow.BedRepository.Get(b => b.SpecialismID == specialismId);

        }
    }
}
