﻿using System;
using System.Web;

namespace Point.Planbord.Infrastructure
{
    public enum SessionKey
    {
        EmployeeID,
        OrganizationID,
        LocationID,
        DepartmentID,
        DepartmentName,
        Settings,
        ShiftID,
        CurrentDepartmentID,
        CurrentDepartmentName,
        ValidSession
    }

    public interface ISessionWrapper : IDisposable
    {
        T GetFromSession<T>(SessionKey key, T defaultValue);
        void SetInSession(SessionKey key, object value);
    }

    public class HttpContextSessionWrapper : ISessionWrapper
    {
        public T GetFromSession<T>(SessionKey key, T defaultValue)
        {
            if (HttpContext.Current.Session == null || HttpContext.Current.Session != null && HttpContext.Current.Session[key.ToString()] == null)
            {
                if (defaultValue != null)
                    return defaultValue;
                else
                    return default(T);
            }
            return (T)HttpContext.Current.Session[key.ToString()];
        }

        public void SetInSession(SessionKey key, object value)
        {
            HttpContext.Current.Session[key.ToString()] = value;
        }

        bool _disposed;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~HttpContextSessionWrapper()
        {
            Dispose(false);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                // free other managed objects that implement
                // IDisposable only
            }

            // release any unmanaged objects
            // set the object references to null
            try
            {
                if ( HttpContext.Current != null && HttpContext.Current.Session != null)
                    HttpContext.Current.Session[SessionKey.ValidSession.ToString()] = false;
            }
            catch
            {
                // silent
            }

            _disposed = true;
        }
    }
}