﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Web;

namespace Point.Planbord.Infrastructure
{
    public static class ReflectionHelper
    {
        public static string PropertyName<TProperty>(Expression<Func<TProperty>> property)
        {
            var lambda = (LambdaExpression)property;

            MemberExpression memberExpression;
            if (lambda.Body is UnaryExpression)
            {
                var unaryExpression = (UnaryExpression)lambda.Body;
                memberExpression = (MemberExpression)unaryExpression.Operand;
            }
            else
            {
                memberExpression = (MemberExpression)lambda.Body;
            }

            return memberExpression.Member.Name;
        }

        public static bool ImplementsInterface(this Type type, Type ifaceType)
        {
            Type[] intf = type.GetInterfaces();
            for (int i = 0; i < intf.Length; i++)
            {
                if (intf[i] == ifaceType)
                {
                    return true;
                }
            }
            return false;
        }
    }

    public static class Merger
    {
        public static TTarget Merge<TTarget>(this TTarget orig, object copyFrom) where TTarget : new()
        {
            if (orig == null || object.Equals(orig, default(TTarget)))
                orig = new TTarget();

            var flags = BindingFlags.Instance | BindingFlags.Public; 
            
            var targetDicF = typeof(TTarget).GetFields(flags).ToDictionary(f => f.Name);
            foreach (FieldInfo f in copyFrom.GetType().GetFields(flags).Concat(copyFrom.GetType().BaseType.GetFields(flags)))
            {
                if (targetDicF.ContainsKey(f.Name))
                    targetDicF[f.Name].SetValue(orig, f.GetValue(copyFrom));
            }

            var targetDicP = typeof(TTarget).GetProperties(flags).ToDictionary(f => f.Name);
            foreach (PropertyInfo p in copyFrom.GetType().GetProperties(flags))
            {
                if (targetDicP.ContainsKey(p.Name))
                    targetDicP[p.Name].SetValue(orig, p.GetValue(copyFrom));
            }

            return orig;
        }
    }
}