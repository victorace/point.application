﻿using System.Collections.Generic;

namespace Point.Planbord.Infrastructure.Extensions
{
    public static class CollectionExtensions
    {
        public static void AddRange<T>(this ICollection<T> instance, IEnumerable<T> enumerable)
        {
            foreach (var cur in enumerable)
                instance.Add(cur);
        }
    }
}
