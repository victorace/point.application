﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using Point.Planbord.Database.Models;
using System.Linq.Expressions;
using System.Reflection;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace Point.Planbord.Infrastructure.Extensions
{
    public static class DatetimeExtensions
    {
        public static string ToStringEx(this DateTime? dt, string format)
        {
            if (dt == null || !dt.HasValue)
                return "";

            return dt.Value.ToString(format);
        }

        public static string ToPlanbordDisplay(this DateTime? dt)
        {
            if (dt.IsOutOfScope()) return "";
            return dt.ToStringEx("ddd dd-MM HH:mm");
        }

        public static string ToSortable(this DateTime? dt)
        {
            if (dt == null || !dt.HasValue )
                dt = new DateTime(1799,1,1,0,0,0);

            return dt.Value.ToString("yyyyMMddHHmmss");
        }

        public static string ToPointDateTime(this DateTime? dateTime)
        {
            if (dateTime.HasValue)
                return dateTime.Value.ToString("dd-MM-yyyy HH:mm");
            else
                return "";
        }

        public static bool IsToday(this DateTime? dt)
        {
            if (dt == null || !dt.HasValue)
                return false;

            var dtnow = DateTime.Now;
            var dtcomp = dt.GetValueOrDefault();
            return (dtnow.Year == dtcomp.Year && dtnow.Month == dtcomp.Month && dtnow.Day == dtcomp.Day);
        }

        public static bool IsTomorrow(this DateTime? dt)
        {
            if (dt == null || !dt.HasValue)
                return false;

            var dtnow = DateTime.Now.AddDays(1);
            var dtcomp = dt.GetValueOrDefault();
            return (dtnow.Year == dtcomp.Year && dtnow.Month == dtcomp.Month && dtnow.Day == dtcomp.Day);

        }

        public static bool IsYesterday(this DateTime? dt)
        {
            if (dt == null || !dt.HasValue)
                return false;

            var dtnow = DateTime.Now.AddDays(-1);
            var dtcomp = dt.GetValueOrDefault();
            return (dtnow.Year == dtcomp.Year && dtnow.Month == dtcomp.Month && dtnow.Day == dtcomp.Day);

        }

        public static bool IsSameDay(this DateTime? dt, DateTime? compareDate)
        {
            if (dt == null || !dt.HasValue)
                return false;

            if (compareDate == null || !compareDate.HasValue)
                return false;

            var dt1 = dt.GetValueOrDefault();
            var dt2 = compareDate.GetValueOrDefault();

            return (dt1.Day == dt2.Day && dt1.Month == dt2.Month && dt1.Year == dt2.Year);
        }

        public static bool IsOutOfScope(this DateTime? dt)
        {
            if (dt == null || !dt.HasValue) return true;

            var dtscope = dt.GetValueOrDefault();
            return (dtscope.Year < 1900 || dtscope.Year > 2100);
        }


    }
}