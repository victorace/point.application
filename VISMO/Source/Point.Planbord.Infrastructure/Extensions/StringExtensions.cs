﻿using System;
using System.Collections.Generic;

namespace Point.Planbord.Infrastructure.Extensions
{
    public static class StringExtensions
    {

        public static string UppercaseFirst(this string s)
        {
            if (String.IsNullOrEmpty(s)) return "";

            char[] a = s.ToCharArray();
            a[0] = char.ToUpper(a[0]);
            return new string(a);
        }

        public static string ToTitleCase(this string s)
        {
            if (String.IsNullOrEmpty(s)) return "";

            return System.Globalization.CultureInfo.InvariantCulture.TextInfo.ToTitleCase(s);
        }

        public static string NullToEmpty(this string s)
        {
            if (String.IsNullOrEmpty(s)) return "";

            return s;
        }

        public static string NullIfEmpty(this string s)
        {
            if (String.IsNullOrWhiteSpace(s))
            {
                return null;
            }
            return s;
        }

        public static string ReplaceDoubleToSingle(this string s, char c, bool recursive = false)
        {
            s = s.Replace("".PadLeft(2, c), c.ToString());
            if (recursive && s.IndexOf("".PadLeft(2, c)) != -1)
                s = s.ReplaceDoubleToSingle(c, recursive);

            return s;
        }

        public static string BooltoJaNee(this bool b)
        {
            return b.ToString().BoolToJaNee();
        }

        public static string BoolToJaNee(this string s)
        {
            if (String.IsNullOrEmpty(s)) return "";

            if (s == "1" || s.ToUpper() == "Y" || s.ToUpper() == "J") return "Ja";
            if (s == "0" || s.ToUpper() == "N") return "Nee";

            return s;
        }

        public static string ReverseString(this string s)
        {
            if (String.IsNullOrEmpty(s)) return "";

            char[] arr = s.ToCharArray();
            Array.Reverse(arr);
            return new string(arr);
        }

        public static IEnumerable<int> ToIntList(this string s)
        {
            if (String.IsNullOrEmpty(s))
                yield break;

            foreach (var num in s.Split(','))
            {
                int i;
                if (int.TryParse(num, out i))
                    yield return i;
            }
        }

        public static string AddEllipsis(this string s, int length)
        {
            if (String.IsNullOrEmpty(s)) return "";
            if (s.Length <= length) return s;
            return s.Substring(0, length - 1) + "...";
        }

        /// <summary>
        /// Fastest methods to convert byte[] to string and vice versa
        /// http://stackoverflow.com/questions/623104/byte-to-hex-string/3974535#3974535
        /// </summary>

        public static string ByteArrayToHexString(this byte[] bytes)
        {
            char[] c = new char[bytes.Length * 2];

            byte b;

            for (int bx = 0, cx = 0; bx < bytes.Length; ++bx, ++cx)
            {
                b = ((byte)(bytes[bx] >> 4));
                c[cx] = (char)(b > 9 ? b + 0x37 + 0x20 : b + 0x30);

                b = ((byte)(bytes[bx] & 0x0F));
                c[++cx] = (char)(b > 9 ? b + 0x37 + 0x20 : b + 0x30);
            }

            return new string(c);
        }

        public static byte[] HexStringToByteArray(this string str)
        {
            if (str.Length == 0 || str.Length % 2 != 0)
                return new byte[0];

            byte[] buffer = new byte[str.Length / 2];
            char c;
            for (int bx = 0, sx = 0; bx < buffer.Length; ++bx, ++sx)
            {
                // Convert first half of byte
                c = str[sx];
                buffer[bx] = (byte)((c > '9' ? (c > 'Z' ? (c - 'a' + 10) : (c - 'A' + 10)) : (c - '0')) << 4);

                // Convert second half of byte
                c = str[++sx];
                buffer[bx] |= (byte)(c > '9' ? (c > 'Z' ? (c - 'a' + 10) : (c - 'A' + 10)) : (c - '0'));
            }

            return buffer;
        }

    }
}