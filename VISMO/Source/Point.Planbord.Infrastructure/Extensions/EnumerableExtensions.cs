﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;

namespace Point.Planbord.Infrastructure.Extensions
{
    public static class EnumerableExtensions
    {
        public static bool IsNullOrEmpty<T>(this IEnumerable<T> list)
        {
            if (list == null)
                return true;
            return (list.Count() == 0);
        }

        public static T PickRandomOne<T>(this IEnumerable<T> list)
        {
            System.Security.Cryptography.RNGCryptoServiceProvider generator = new RNGCryptoServiceProvider();
            byte[] data = new byte[4];
            generator.GetBytes(data);

            Random rnd = new Random(BitConverter.ToInt32(data, 0));


            T picked = default(T);
            int cnt = 0;
            foreach (T item in list)
            {
                if (rnd.Next(++cnt) == 0)
                {
                    picked = item;
                }
            }
            return picked;
        }

    }
}