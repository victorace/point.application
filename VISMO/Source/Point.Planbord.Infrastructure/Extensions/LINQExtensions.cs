﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Web;

namespace Point.Planbord.Infrastructure.Extensions
{
    public static class LINQExtensions
    {

        public static T FirstOrNew<T>(this IEnumerable<T> e, Func<T, bool> where = null) where T : new()
        {
            var r = ( where == null ? e.FirstOrDefault<T>() : e.FirstOrDefault<T>(where) );
            return (r == null) ? new T() : r;
        }

        public static T LastOrNew<T>(this IEnumerable<T> e, Func<T, bool> where = null) where T : new()
        {
            var r = (where == null ? e.LastOrDefault<T>() : e.LastOrDefault<T>(where));
            return (r == null) ? new T() : r;
        }

    }
}