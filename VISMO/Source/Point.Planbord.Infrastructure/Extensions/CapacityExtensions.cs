﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using Point.Planbord.Database.Models;
using System.Linq.Expressions;
using System.Reflection;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace Point.Planbord.Infrastructure.Extensions
{
    public static class CapacityExtensions
    {


            public static int GetSum(this List<SumTotal> sumtotals, SumType sumtype)
            {
                if (sumtotals == null || sumtotals.Count() == 0) return 0;

                return GetSumList(sumtotals, sumtype).Count();
            }

            public static IList<int> GetSumList(this List<SumTotal> sumtotals, SumType sumtype)
            {
                IList<SumTotal> sumlist = new List<SumTotal>();

                if ((sumtype & SumType.VrijeBedden) == SumType.VrijeBedden)
                    sumlist.AddRange(sumtotals.Where(st => st.VrijeBedden > 0));

                if ((sumtype & SumType.VerwachteOntslagen) == SumType.VerwachteOntslagen)
                    sumlist.AddRange(sumtotals.Where(st => st.VerwachteOntslagen > 0));

                if ((sumtype & SumType.BezetteBedden) == SumType.BezetteBedden)
                    sumlist.AddRange(sumtotals.Where(st => st.BezetteBedden > 0));

                if ((sumtype & SumType.NieuwePatienten) == SumType.NieuwePatienten)
                    sumlist.AddRange(sumtotals.Where(st => st.NieuwePatienten > 0));

                if ((sumtype & SumType.GereserveerdeBedden) == SumType.GereserveerdeBedden)
                    sumlist.AddRange(sumtotals.Where(st => st.GereserveerdeBedden > 0));

                if ((sumtype & SumType.GeblokkeerdeBedden) == SumType.GeblokkeerdeBedden)
                    sumlist.AddRange(sumtotals.Where(st => st.GeblokkeerdeBedden > 0));

                if ((sumtype & SumType.GeslotenBedden) == SumType.GeslotenBedden)
                    sumlist.AddRange(sumtotals.Where(st => st.GeslotenBedden > 0));

                if ((sumtype & SumType.NieuwePatientenMorgen) == SumType.NieuwePatientenMorgen)
                    sumlist.AddRange(sumtotals.Where(st => st.NieuwePatientenMorgen > 0));

                if ((sumtype & SumType.VerwachteOntslagenMorgen) == SumType.VerwachteOntslagenMorgen)
                    sumlist.AddRange(sumtotals.Where(st => st.VerwachteOntslagenMorgen > 0));

                if ((sumtype & SumType.OrigineelBedden) == SumType.OrigineelBedden)
                    sumlist.AddRange(sumtotals.Where(st => st.OrigineelBed > 0));

                if ((sumtype & SumType.UitgeleendBedden) == SumType.UitgeleendBedden)
                    sumlist.AddRange(sumtotals.Where(st => st.UitgeleendBed > 0));

                if ((sumtype & SumType.GeleendeBedden) == SumType.GeleendeBedden)
                    sumlist.AddRange(sumtotals.Where(st => st.GeleendBed > 0));

                return sumlist.Select(st => st.BedID).ToList<int>();
            }
        }

        [Flags]
        public enum SumType
        {
            VrijeBedden = 1,
            VerwachteOntslagen = 2,
            BezetteBedden = 4,
            NieuwePatienten = 8,
            GereserveerdeBedden = 16,
            GeblokkeerdeBedden = 32,
            GeslotenBedden = 64,
            NieuwePatientenMorgen = 128,
            VerwachteOntslagenMorgen = 256,
            OrigineelBedden = 512,
            UitgeleendBedden = 1024,
            GeleendeBedden = 2048
        }

        public class SumTotal
        {
            [Description("Vrije bedden")] // 1
            public int VrijeBedden { get; set; }
            [Description("Verwachte ontslagen")] // 2
            public int VerwachteOntslagen { get; set; }
            [Description("Bezette bedden")] // 3
            public int BezetteBedden { get; set; }
            [Description("Nieuwe patienten")] // 4
            public int NieuwePatienten { get; set; }
            [Description("Gereserveerde bedden")] // 5
            public int GereserveerdeBedden { get; set; }
            [Description("Geblokkeerde bedden")] // 6
            public int GeblokkeerdeBedden { get; set; }
            [Description("Gesloten bedden")] // 8
            public int GeslotenBedden { get; set; }
            [Description("Nieuwe patienten morgen")] // 10
            public int NieuwePatientenMorgen { get; set; }
            [Description("Verwachte ontslagen morgen")] // 11
            public int VerwachteOntslagenMorgen { get; set; }

            public int OrigineelBed { get; set; }
            public int UitgeleendBed { get; set; }
            public int GeleendBed { get; set; }

            public int BedID { get; set; }

            public SumTotal()
            {
                VrijeBedden = VerwachteOntslagen = BezetteBedden = NieuwePatienten = GereserveerdeBedden = GeblokkeerdeBedden = GeslotenBedden = VerwachteOntslagenMorgen = NieuwePatientenMorgen = 0;
            }
        }


    
}