﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;

namespace Point.Planbord.Infrastructure
{
    public class Randomizer
    {


        public static string GetRandom(string[] list, int empties = 0)
        {
            int rnd = GetRandomInt(0, list.Length + empties);
            if (rnd >= list.Length)
                return "";
            else
                return list[rnd];
        }

        public static string GetRandomOnce(ref List<string> list)
        {
            int i = GetRandomInt(0, list.Count - 1);
            string r = list[i];
            list.RemoveAt(i);
            return r;
        }

        public static int GetRandomInt(int min, int max)
        {
            System.Security.Cryptography.RNGCryptoServiceProvider generator = new RNGCryptoServiceProvider();
            byte[] data = new byte[4];
            generator.GetBytes(data);

            return new Random(BitConverter.ToInt32(data, 0)).Next(min, max);
        }

        public static DateTime GetRandomDateTime(int days = 0)
        {
            DateTime dt = DateTime.Now.AddDays(days);
            dt = dt.AddHours(-dt.Hour).AddHours(GetRandomInt(7, 18));
            dt = dt.AddMinutes(-dt.Minute).AddMinutes(Int32.Parse(GetRandom(new[] { "0", "15", "30", "45" })));
            return dt;

        }

        public static string GetRandomBitString(int maxOnes, int stringLength, int percentageEmpty)
        {
            System.Text.StringBuilder bits = new System.Text.StringBuilder("".PadLeft(stringLength, '0'));

            if (GetRandomInt(0, 100) > percentageEmpty && maxOnes > 0)
            {
                for (int i = 0; i < maxOnes; i++)
                {
                    bits[GetRandomInt(0, stringLength)] = GetRandomInt(0, 2).ToString()[0];
                }
            }
            return bits.ToString();
        }


    }
}