﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using Point.Planbord.Infrastructure.Extensions;
using System.ComponentModel;

namespace Point.Planbord.Infrastructure
{
    public class Authorization
    {
    }


    public enum Role
    {
        [Description("PlanbordAdministrator")]
        PlanbordAdministrator,
        [Description("OrganizationAdministrator")]
        OrganizationAdministrator,
        [Description("PersonalAccount")]
        PersonalAccount,
        [Description("DepartmentAccount")]
        DepartmentAccount,
        [Description("CapacityAccount")]
        CapacityAccount,
        [Description("LogistiekAccount")]
        LogistiekAccount
    }


    public class AuthorizeRoles : AuthorizeAttribute
    {

        private Role[] roles;

        public AuthorizeRoles(params Role[] roles)
        {
            this.roles = roles;
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            //httpContext.User.Identity.IsAuthenticated checked in base ????

            if (!base.AuthorizeCore(httpContext)) return false;

            IPrincipal user = httpContext.User;

            foreach (var role in roles)
            {
                if (user.IsInRole(role.GetDescription())) return true;
            }

            return false;


        }

        //protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        //{
        //    base.HandleUnauthorizedRequest(filterContext);
        //}

    }

}