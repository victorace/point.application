﻿using Point.Planbord.Database.Context;
using Point.Planbord.Database.Models;
using Point.Planbord.Database.Repository;
using System;
using System.Linq;

namespace Point.Planbord.WebService
{
    public class Controllers
    {

        public Location GetFirstLocation(UnitOfWork<PlanbordImportContext> uow, Organization organization)
        {
            Location location = null;
            try
            {
                if (organization == null)
                {
                    return null;
                }

                var locations = uow.LocationRepository.Get(l => l.OrganizationID == organization.OrganizationID);
                if (locations == null || locations.Count() == 0)
                {
                    var description = String.Format("'{0}' - '{1}' doesn't have any Locations.", organization.Name, organization.SystemCode);
                    Logger.LogError(description, null, "Organization");
                    return null;
                }
                location = locations.First();
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.ToString());
                return null;
            }

            return location;
        }

        public Organization GetOrganization(UnitOfWork<PlanbordImportContext> uow, string systemCode)
        {
            // try and find an organizationid for this request
            Organization organization = null;
            try
            {
                organization = uow.OrganizationRepository.Get(o => o.SystemCode == systemCode).FirstOrDefault();
                if (organization == null)
                {
                    Logger.LogError(String.Format("No Organization found with the code '{0}'.", systemCode), null, "Organization");
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.ToString());
                return null;
            }

            return organization;
        }


    }
}