﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using PointModels = Point.Planbord.Database.Models;

namespace Point.Planbord.WebService
{
    public static class Security
    {


        public static bool Authenticate(IHashHeader HashHeader, PointModels.Organization organization)
        {
            string hash = GetHashString(String.Format("{0}_{1}_{2}",
                                        HashHeader.Organization_SystemCode,
                                        organization.PrivateKey,
                                        HashHeader.DateTimeSent.ToString("yyyyMMddHHmm")));
            return (hash == HashHeader.IdentificationHash);
        }

        private static byte[] getHash(string inputString)
        {
            HashAlgorithm algorithm = SHA1.Create();
            return algorithm.ComputeHash(Encoding.UTF8.GetBytes(inputString));
        }


        public static string GetHashString(string organizationSystemCode, string privateKey, DateTime dateTimeSent)
        {
            return GetHashString(String.Format("{0}_{1}_{2}",
                                organizationSystemCode,
                                privateKey,
                                dateTimeSent.ToString("yyyyMMddHHmm")));
        }
        public static string GetHashString(string inputString)
        {
            StringBuilder sb = new StringBuilder();
            foreach (byte b in getHash(inputString))
            {
                sb.Append(b.ToString("X2"));
            }
            return sb.ToString();
        }

    }
}