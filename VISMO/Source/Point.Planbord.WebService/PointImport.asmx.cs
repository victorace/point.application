﻿using Point.Planbord.Database.Context;
using Point.Planbord.Database.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using PointModels = Point.Planbord.Database.Models;

namespace Point.Planbord.WebService
{
    [WebService(Namespace = "http://point.planbord.nl/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    public class ImportPoint : System.Web.Services.WebService
    {
        private UnitOfWork<PlanbordImportContext> uow;

        private List<PointModels.Patient> handledPatients = new List<PointModels.Patient>();

        private IEnumerable<PointModels.Patient> allPatients;

        [WebMethod]
        public string ImportDetails(PointHeader PointHeader, List<PointDetail> PointDetails)
        {
            Logger.LogInformation("ImportDetails POINT started at: " + DateTime.Now.ToString("HH:mm:ss.fff"), null, "");

            uow = new UnitOfWork<PlanbordImportContext>();

            try 
            { 
                Logger.LogSOAP(HttpContext.Current.Request); 
            }
            catch (Exception ex) 
            { 
                Logger.LogError(ex.ToString()); 
            }

            var organization = new Controllers().GetOrganization(uow, PointHeader.Organization_SystemCode);
            if (organization == null)
            {
                return String.Format("(ERROR) {0}", String.Format("No Organization found with the code '{0}'.", PointHeader.Organization_SystemCode));
            }

            allPatients = uow.BedForPatientRepository.Get(bfp => bfp.Bed.Department.Location.OrganizationID == organization.OrganizationID && bfp.Patient != null, null, "Patient").Select(bfp => bfp.Patient).ToList<PointModels.Patient>();

            foreach (PointDetail pointdetail in PointDetails)
            {
                PointModels.Patient patient = getPatient(pointdetail);
                if (patient != null)
                {
                    updatePointStatus(pointdetail, patient);
                    handledPatients.Add(patient);
                }
            }

            cleanupNonHandledPatients();

            uow.Dispose();

            Logger.LogInformation("ImportDetails POINT ended at: " + DateTime.Now.ToString("HH:mm:ss.fff"), null, "");

            return String.Format("(OK) Data received. {0} details for '{1}'", PointDetails.Count(), PointHeader.Organization_SystemCode);
        }



        private PointModels.Patient getPatient(PointDetail pointDetail)
        {
            PointModels.Patient pointmodelpatient = null;

            try
            {
                if (pointDetail.Patient != null)
                {

                    if (String.IsNullOrWhiteSpace(pointDetail.Patient.PatientNumber) && String.IsNullOrWhiteSpace(pointDetail.Patient.CivilServiceNumber))
                    {
                        Code code = new Code();
                        if (pointDetail.Codes != null)
                        {
                            code = pointDetail.Codes.FirstOrDefault(c => c.CodeType == "TransferID");
                        }
                        Logger.LogError(String.Format("No valid Patient CivilServiceNumber or PatientNumber data provided. TransferID: {0}", code.CodeValue), null, "Patient");
                        return null;
                    }

                    pointmodelpatient = allPatients.FirstOrDefault(p => p.PatientNumber == pointDetail.Patient.PatientNumber || p.CivilServiceNumber == pointDetail.Patient.CivilServiceNumber);
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.ToString(), null, "Patient");
            }

            return pointmodelpatient;
        }

        private void updatePointStatus(PointDetail pointDetail, PointModels.Patient patient)
        {
            var codes = uow.CodeRepository.Get(c => c.CodeMainType == PointModels.CodeMainType.PointStatus && c.PatientID == patient.PatientID);

            foreach (var code in codes)
            {
                uow.CodeRepository.Delete(code);
            }
            uow.Save();

            foreach (var code in pointDetail.Codes)
            {
                uow.CodeRepository.Insert(new PointModels.Code()
                                            {
                                                CodeMainType = PointModels.CodeMainType.PointStatus,
                                                CodeStatus = PointModels.CodeStatus.PlanbordOnly,
                                                CodeType = code.CodeType,
                                                CodeValue = code.CodeValue,
                                                PatientID = patient.PatientID                        
                                            }
                );
            }
            uow.Save();
        }

        private void cleanupNonHandledPatients()
        {
            var handledPatientIDs = handledPatients.Select(hp => hp.PatientID);

            var codesOfNonHandledPatients = (from code in uow.CodeRepository.Get(c => c.CodeMainType == PointModels.CodeMainType.PointStatus
                                                 && !handledPatientIDs.Contains((int)c.PatientID))
                                             select code);

            foreach (var code in codesOfNonHandledPatients)
            {
                uow.CodeRepository.Delete(code);
            }
            uow.Save();
        }

    }
}
