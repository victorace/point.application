﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Point.Planbord.Database.Models;

namespace Point.Planbord.WebService
{

    public interface IHashHeader
    {
        string Organization_SystemCode { get; set; }
        string IdentificationHash { get; set; }
        DateTime DateTimeSent { get; set; }
    }

    [Serializable]
    public class SAPHeader: IHashHeader
    {
        public string Organization_SystemCode { get; set; }
        public string Location_SystemCode { get; set; }
        public string IdentificationHash { get; set; }
        public DateTime DateTimeSent { get; set; }

        public override string ToString()
        {
            return String.Format("Organization_SystemCode: [{0}], Location_SystemCode: [{1}], IdentificationHash: [{2}], DateTimeSent : [{3:yyyyMMddHHmm}]",
                                 Organization_SystemCode, Location_SystemCode, IdentificationHash, DateTimeSent.ToString("yyyyMMddHHmm"));
        }
    }

    [Serializable]
    public class SAPDetail
    {
        public Bed Bed { get; set; }
        public BedPatientStatus BedPatientStatus { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public Patient Patient { get; set; }
        public List<Code> Codes { get; set; }

    }

    [Serializable]
    public class PointHeader : IHashHeader 
    {
        public string Organization_SystemCode { get; set; }
        public string IdentificationHash { get; set; }
        public DateTime DateTimeSent { get; set; }

        public override string ToString()
        {
            return String.Format("Organization_SystemCode: [{0}], IdentificationHash: [{1}], DateTimeSent: [{2}]",
                (String.IsNullOrWhiteSpace(Organization_SystemCode) ? "" : Organization_SystemCode),
                (String.IsNullOrWhiteSpace(IdentificationHash) ? "" : IdentificationHash),
                (DateTimeSent == null ? "" : DateTimeSent.ToString("yyyyMMddHHmm")));
        }


    }

    [Serializable]
    public class PointDetail
    {
        public Patient Patient { get; set; }
        public List<Code> Codes { get; set; }
    }


    [Serializable]
    public class Bed
    {
        public string SystemCode { get; set; }
        public string Department_SystemCode { get; set; }
        public string Department_Description { get; set; }
        public string Specialism_SystemCode { get; set; }
        public string Specialism_Description { get; set; }
        public string RoomNumber { get; set; }
        public string BedNumber { get; set; }
        public BedSoort BedSoort { get; set; }
    }

    [Serializable]
    public class Patient
    {
        public string Initials { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string MaidenName { get; set; }
        public string FullName { get; set; }
        public Gender Gender { get; set; }
        public string PatientNumber { get; set; }
        public string CivilServiceNumber { get; set; }
        public DateTime? BirthDate { get; set; }
    }

    [Serializable]
    public class Code
    {
        public CodeMainType CodeMainType { get; set; }
        public string CodeType { get; set; }
        public string CodeValue { get; set; }
        public DateTime? LastMeasuring { get; set; }
        public string SystemCycle { get; set; }
    }
}