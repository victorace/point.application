﻿using System;

namespace Point.Planbord.WebService
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            System.Data.Entity.Database.SetInitializer<Database.Context.PlanbordImportContext>(null);
            System.Data.Entity.Database.SetInitializer<Database.Context.LoggingContext>(null);
        }
        
    }
}