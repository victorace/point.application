﻿using Point.Planbord.Database.Context;
using Point.Planbord.Database.Repository;
using Point.Planbord.Infrastructure.Extensions;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;
using PointModels = Point.Planbord.Database.Models;

namespace Point.Planbord.WebService
{
    [WebService(Namespace = "http://point.planbord.nl/", Name = "Import")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    public class ImportSAP : System.Web.Services.WebService
    {
        private readonly UnitOfWork<PlanbordImportContext> uow = new UnitOfWork<PlanbordImportContext>();
        private List<int> departmentIds;
        private List<PointModels.Code> insertedCodes;
        private DateTime dayAfterTomorrow = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day).AddDays(2); //Get the start 0:00:00 of the day after tomorrow

        private List<PointModels.Specialism> specialismsLookup = new List<PointModels.Specialism>();
        private List<PointModels.Department> departmentsLookup = new List<PointModels.Department>();

        private static String getIP()
        {
            string ip = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (String.IsNullOrEmpty(ip))
            {
                ip = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            }

            return ip;
        }


        [WebMethod]
        public string ImportDetail(SAPHeader SAPHeader, SAPDetail SAPDetail)
        {
            return "Dummy method, only for the auto-generated parameter info";
        }

        [WebMethod]
        public string ImportDetails(SAPHeader SAPHeader, List<SAPDetail> SAPDetails)
        {
            Logger.LogInformation(String.Format("ImportDetails SAP started at: {0} from IP: {1}", DateTime.Now.ToString("HH:mm:ss.fff"), getIP()), null, "");

            try 
            { 
                Logger.LogSOAP(HttpContext.Current.Request);
            }
            catch (Exception ex) 
            { 
                Logger.LogError(ex.ToString());
            }
            

            var organization = new Controllers().GetOrganization(uow, SAPHeader.Organization_SystemCode);
            if (organization == null)
            {
                return String.Format("(ERROR) {0}", String.Format("No Organization found with the code '{0}'.", SAPHeader.Organization_SystemCode));
            }

            if (!Security.Authenticate(SAPHeader, organization))
            {
                var description = String.Format("Authentication failed - {0}.", SAPHeader.ToString());
                Logger.LogError(description);
                return "(ERROR) Authentication failed.";
            }

            PointModels.Location location = null;
            if (String.IsNullOrWhiteSpace(SAPHeader.Location_SystemCode))
            {
                location = new Controllers().GetFirstLocation(uow, organization);
            }
            else
            {
                location = uow.LocationRepository.Get(l => l.SystemCode == SAPHeader.Location_SystemCode).FirstOrDefault();
            }

            if (location == null)
            {
                return String.Format("(ERROR) '{0}' - '{1}' doesn't have any Locations or specified Location_SystemCode '{2}' cannot be found.", organization.Name, organization.SystemCode);
            }
            int locationId = location.LocationID;
            int organizationId = organization.OrganizationID;
            departmentIds = new List<int>();
            insertedCodes = new List<PointModels.Code>();

            initLookupLists();

            foreach (SAPDetail sapDetail in SAPDetails.Where(sd => sd.StartTime == null || sd.StartTime < dayAfterTomorrow).OrderBy(sd => !String.IsNullOrWhiteSpace(sd.Bed.SystemCode)).ThenByDescending(sd => sd.StartTime)) 
            {
                PointModels.Department department = getDepartment(organizationId, locationId, sapDetail);
                if (!departmentIds.Any(d => d == department.DepartmentID))
                {
                    departmentIds.Add(department.DepartmentID);
                }

                PointModels.Bed bed = getBed(organizationId, locationId, department, sapDetail);
                PointModels.Patient patient = getOrInsertPatient(sapDetail);
                processBedForPatient(bed, patient, sapDetail);
                processCodes(patient, sapDetail);
            }

            clearBeds(locationId);

            refreshCodes();
            processMissingPatients(locationId, SAPHeader, SAPDetails);

            removeBedsNotInUseFromPlanbord(SAPDetails, location);

            Logger.LogInformation("ImportDetails SAP ended at: " + DateTime.Now.ToString("HH:mm:ss.fff"), null, "");

            return "(OK) Data received. 1 header, " + SAPDetails.Count().ToString() + " details.";
        }

        private void removeBedsNotInUseFromPlanbord(List<SAPDetail> sapDetails, PointModels.Location location)
        {
            var bedsFromSAP = sapDetails.Select(sd => sd.Bed.SystemCode);
            var bedsFromDBForLocation = uow.BedRepository.Get(b => b.Department.LocationID == location.LocationID && b.SystemCode != null).ToList();
            var bedIDs = new List<int>();

            foreach (var bed in bedsFromDBForLocation)
            {
                if (!bedsFromSAP.Any(b=>b == bed.SystemCode))
                {
                    bedIDs.Add(bed.BedID);
                }
            }

            if (bedIDs.Count == 0)
            {
                return;
            }
            try
            {
                string sql = String.Format(@"update bed set DepartmentID = null where BedID in ({0})", String.Join(",", bedIDs));
                uow.DbContext.Database.ExecuteSqlCommand(sql);
            }
            catch (Exception ex)
            {
                Logger.LogError(String.Format("{0} {1}", "Error in removeBedsNotInUseFromPlanbord()", ex.ToString(), null, "Bed"));
            }
        }

        private void clearBeds(int locationID)
        {
            var departmentIDs = (from d in uow.DepartmentRepository.Get(d => d.LocationID == locationID) select d.DepartmentID).ToList();
            if (departmentIDs == null || departmentIDs.Count == 0)
            {
                return;
            }
            try
            { 
                string sql = String.Format(@"delete from Bed 
                                              where ((Bed.SystemCode is null )) 
                                                and BedID not in ( select BedID from BedForPatient )
                                                and Bed.DepartmentID in ( {0} )", String.Join(",", departmentIDs));
                uow.DbContext.Database.ExecuteSqlCommand(sql);
            }
            catch(Exception ex)
            {
                Logger.LogError(String.Format("{0} {1}", "Error in clearBeds()", ex.ToString(), null, "Bed"));
            }
        }

        private void refreshCodes()
        {
            var beds = (from b in uow.BedRepository.Get(bed => departmentIds.Contains((int)bed.DepartmentID)) select b.BedID).Distinct();

            var patients = (from pfb in uow.BedForPatientRepository.Get(patient => beds.Contains(patient.BedID)) select pfb.PatientID).Distinct();

            // 1. delete codes for patients attached to a bed (in the locations, departments, beds)
            var doneCodeIds = (from code in uow.CodeRepository.Get(c =>
                c.CodeStatus == PointModels.CodeStatus.Done && patients.Contains(c.PatientID))
                               select code.CodeID).ToList<int>();
            foreach (int codeId in doneCodeIds)
            {
                uow.CodeRepository.Delete(codeId);
            }
            uow.Save();

            // 2. delete codes for patients not attached to a bed with a TimeStamp < (now - 1 hour))
            DateTime oneHourEarlier = DateTime.Now.AddHours(-1);
            doneCodeIds = (from code in uow.CodeRepository.Get(c => c.CodeStatus == PointModels.CodeStatus.Done &&
                    (!patients.Contains(c.PatientID) && c.TimeStamp < oneHourEarlier))
                               select code.CodeID).ToList<int>();
            foreach (int codeId in doneCodeIds)
            {
                uow.CodeRepository.Delete(codeId);
            }
            uow.Save();

            // 3. delete all codes that have no PatientID
            doneCodeIds = (from code in uow.CodeRepository.Get(c => c.CodeStatus == PointModels.CodeStatus.Done &&
                               c.PatientID == null)
                               select code.CodeID).ToList<int>();
            foreach (int codeId in doneCodeIds)
            {
                uow.CodeRepository.Delete(codeId);
            }
            uow.Save();

            foreach (var code in insertedCodes)
            {
                code.CodeStatus = PointModels.CodeStatus.Done;
                uow.CodeRepository.Update(code);
            }
            uow.Save();
        }

        private void processMissingPatients(int locationId, SAPHeader SAPHeader, List<SAPDetail> SAPDetails)
        {
            List<PointModels.Patient> patients = uow.PatientRepository.GetAll().ToList();
            List<PointModels.Patient> patientsSAP = new List<PointModels.Patient>();
            foreach (SAPDetail sapDetail in SAPDetails.Where(sd => sd.StartTime == null || sd.StartTime < dayAfterTomorrow))
            {
                if (sapDetail.Patient != null)
                {
                    if (!String.IsNullOrWhiteSpace(sapDetail.Patient.PatientNumber))
                        patientsSAP.AddRange(patients.Where(p => p.PatientNumber.Equals(sapDetail.Patient.PatientNumber)).ToList());
                    else if (!String.IsNullOrWhiteSpace(sapDetail.Patient.CivilServiceNumber))
                        patientsSAP.AddRange(patients.Where(p => p.CivilServiceNumber.Equals(sapDetail.Patient.CivilServiceNumber)).ToList());
                }
            }

            var bedForPatientWithPatient = uow.BedForPatientRepository.Get(bfp => bfp.Bed.Department.LocationID == locationId && bfp.PatientID.HasValue);
            var missingpatients = bedForPatientWithPatient.Where(bfp => !patientsSAP.Any(p => p.PatientID == bfp.PatientID));
            foreach (var bedForPatient in missingpatients)
            {
                uow.BedForPatientRepository.Delete(bedForPatient);
            }
            if (missingpatients.Count() > 0)
                uow.Save();
        }

        private void processCodes(PointModels.Patient patient, SAPDetail sapDetail)
        {
            if (sapDetail.Codes != null)
            {
                foreach (Code c in sapDetail.Codes)
                {
                    try
                    {
                        PointModels.Code code = new PointModels.Code()
                        {
                            CodeMainType = c.CodeMainType,
                            CodeType = (String.IsNullOrWhiteSpace(c.CodeType) ? null : c.CodeType), 
                            CodeValue = (String.IsNullOrWhiteSpace(c.CodeValue) ? null : c.CodeValue),
                            LastMeasuring = c.LastMeasuring,
                            Unit = null,
                            SystemCycle = (String.IsNullOrWhiteSpace(c.SystemCycle) ? null : c.SystemCycle),
                            CodeStatus = PointModels.CodeStatus.Pending,
                            Patient = patient,
                            Bed = null
                        };
                        uow.CodeRepository.Insert(code);
                        
                        insertedCodes.Add(code);
                    }
                    catch (Exception ex)
                    {
                        Logger.LogError(String.Format("{0} {1}", patient.PatientNumber, ex.ToString(), null, "Code"));
                    }
                }

                uow.Save();
            }
        }

        private bool deleteBedForPatient(PointModels.BedForPatient bedforpatient, PointModels.Bed bed)
        {
            uow.BedForPatientRepository.Delete(bedforpatient);
            Logger.LogVerbose(String.Format("BedForPatient-record for BedID - {0}, SystemCode - {1}. BedForPatientID - {2} Deleting.", bed.BedID, bed.SystemCode, bedforpatient.BedForPatientID), null, "BedForPatient");
            return true;
        }

        private Point.Planbord.Database.Models.Specialism getSpecialism(SAPDetail sapDetail)
        {
            Point.Planbord.Database.Models.Specialism specialism = null;
            if (!String.IsNullOrWhiteSpace(sapDetail.Bed.Specialism_SystemCode))
            {
                specialism = specialismsLookup.FirstOrDefault(s => s.SystemCode == sapDetail.Bed.Specialism_SystemCode);
                if (specialism == null)
                {
                    specialism = uow.SpecialismRepository.Get(s => s.SystemCode == sapDetail.Bed.Specialism_SystemCode).FirstOrDefault();
                }
                if (specialism == null)
                {
                    specialism = new PointModels.Specialism()
                    {
                        SystemCode = (String.IsNullOrWhiteSpace(sapDetail.Bed.Specialism_SystemCode) ? null : sapDetail.Bed.Specialism_SystemCode),
                        Name = (String.IsNullOrWhiteSpace(sapDetail.Bed.Specialism_Description) ? null : sapDetail.Bed.Specialism_Description),
                        ShortCode = (String.IsNullOrWhiteSpace(sapDetail.Bed.Specialism_SystemCode) ? null : sapDetail.Bed.Specialism_SystemCode),
                    };
                    if (String.IsNullOrEmpty(specialism.Name))
                    {
                        specialism.Name = specialism.SystemCode;
                    }
                    uow.SpecialismRepository.Insert(specialism);
                    uow.Save();

                    Logger.LogVerbose(String.Format("Create Specialism - {0}.", sapDetail.Bed.Specialism_SystemCode), null, "Specialism");
                }
            }
            return specialism;
        }

        private void processBedForPatient(PointModels.Bed bed, PointModels.Patient patient, SAPDetail sapDetail)
        {
            try
            {
                bool haveBedToDelete = false;
                if (bed.SystemCode != null) //dealing with a real bed, delete existing assignments to this bed or done for this patient (removing future assignment also, since it's sorted descending on StartTime).
                {
                    //delete all records for this bed with status 'Opgenomen'
                    var count = uow.BedForPatientRepository.Get(bfp => bfp.BedID == bed.BedID && bfp.BedPatientStatus == PointModels.BedPatientStatus.Opgenomen).Count();
                    if(count >=2)
                    { 
                        foreach (PointModels.BedForPatient bedforpatient in uow.BedForPatientRepository.Get(bfp => bfp.BedID == bed.BedID && bfp.BedPatientStatus == PointModels.BedPatientStatus.Opgenomen ) )
                        haveBedToDelete = deleteBedForPatient(bedforpatient, bed);
                    }
                    else if(count==1)
                    {
                        PointModels.BedForPatient existingBedForPatient = uow.BedForPatientRepository.Get(bfp => bfp.BedID == bed.BedID && bfp.BedPatientStatus == PointModels.BedPatientStatus.Opgenomen).FirstOrDefault();
                        if ((existingBedForPatient.StartTime == sapDetail.StartTime && existingBedForPatient.EndTime == sapDetail.EndTime) == false)
                        {
                            haveBedToDelete = deleteBedForPatient(existingBedForPatient, bed);
                        }
                    }

                    //delete all records for this bed with blokkade status 
                    foreach (PointModels.BedForPatient bedforpatient in uow.BedForPatientRepository.Get(bfp => bfp.BedID == bed.BedID && (bfp.BedPatientStatus == PointModels.BedPatientStatus.Geen || bfp.BedPatientStatus >= PointModels.BedPatientStatus.BlokBAU)))
                    {
                        haveBedToDelete = deleteBedForPatient(bedforpatient, bed);
                    }

                    //delete all (previous) records for this patient. If a patient occurs more than once in the SAP-import only his first dated record will remain (since SAPDetails are sorted descending on StartTime)
                    if (patient != null)
                    {
                        foreach (PointModels.BedForPatient bedforpatient in uow.BedForPatientRepository.Get(bfp => bfp.PatientID == patient.PatientID))
                            haveBedToDelete = deleteBedForPatient(bedforpatient, bed);
                    }
                }
                else  // bed.SystemCode == null
                {
                    // Delete all (future) assignments to this bed for this patient. 
                    // Multiple patients can be assigned to a bed without a systemcode, only one (future) assignment for a patient will remain in the database
                    // Details are sorted descending on StartTime, so the most recent/upcoming will remain in the end
                    foreach (PointModels.BedForPatient bedforpatient in uow.BedForPatientRepository.Get(bfp => bfp.PatientID == patient.PatientID))
                        haveBedToDelete = deleteBedForPatient(bedforpatient, bed);
                }
                if (haveBedToDelete) uow.Save();

                Point.Planbord.Database.Models.Specialism specialism = getSpecialism(sapDetail);

                uow.BedForPatientRepository.Insert(new PointModels.BedForPatient()
                    {
                        Bed = bed,
                        Patient = patient,
                        BedPatientStatus = sapDetail.BedPatientStatus,
                        StartTime = sapDetail.StartTime,
                        EndTime = sapDetail.EndTime,
                        SpecialismID = (specialism == null ? null : (int?)specialism.SpecialismID)
                    });
                Logger.LogVerbose(String.Format("BedForPatient-record for BedID - {0}, SystemCode - {1}. Creating.", bed.BedID, bed.SystemCode), null, "BedForPatient");
                uow.Save();
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.ToString(), null, "BedForPatient");
            }
        }

        private PointModels.Patient getOrInsertPatient(SAPDetail sapDetail)
        {
            if (sapDetail.Patient == null)
            {
                return null;
            }

            PointModels.Patient patient = null;
            Patient sapPatient = sapDetail.Patient;

            try
            {

                if (String.IsNullOrWhiteSpace(sapPatient.PatientNumber) && String.IsNullOrWhiteSpace(sapPatient.CivilServiceNumber))
                {
                    Logger.LogError("No valid Patient CivilServiceNumber or PatientNumber data provided.", null, "Patient");
                    return null;
                }

                if (!String.IsNullOrWhiteSpace(sapPatient.PatientNumber))
                {
                    patient = uow.PatientRepository.Get(p => p.PatientNumber.Equals(sapPatient.PatientNumber)).FirstOrDefault();
                }
                if (patient == null && !String.IsNullOrWhiteSpace(sapPatient.CivilServiceNumber))
                {
                    patient = uow.PatientRepository.Get(p => p.CivilServiceNumber.Equals(sapPatient.CivilServiceNumber)).FirstOrDefault();
                }
                if (patient == null)
                {
                    patient = new PointModels.Patient();
                    fillPatient(patient, sapPatient);
                    uow.PatientRepository.Insert(patient);
                    uow.Save();

                    Logger.LogVerbose(String.Format("Created Patient - PatientNumber '{0}', CivilServiceNumber '{1}'.",patient.PatientNumber, patient.CivilServiceNumber), null, "Patient");
                }
                else
                {
                    if (patient.BirthDate != sapPatient.BirthDate ||
                        patient.FirstName != sapPatient.FirstName ||
                        patient.FullName != sapPatient.FullName ||
                        patient.Gender != sapPatient.Gender ||
                        patient.Initials != sapPatient.Initials ||
                        patient.LastName != sapPatient.LastName ||
                        patient.MaidenName != sapPatient.MaidenName ||
                        patient.MiddleName != sapPatient.MiddleName)
                    {
                        fillPatient(patient, sapPatient);
                        uow.PatientRepository.Update(patient);
                        uow.Save();
                        Logger.LogVerbose(String.Format("Updated Patient - PatientNumber '{0}', CivilServiceNumber '{1}'.", patient.PatientNumber, patient.CivilServiceNumber), null, "Patient");
                    }

                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.ToString(), null, "Patient");
            }

            return patient;
        }

        private void fillPatient(PointModels.Patient patient, Patient sapPatient)
        {
            patient.BirthDate = sapPatient.BirthDate;
            patient.CivilServiceNumber = sapPatient.CivilServiceNumber.NullIfEmpty();
            patient.FirstName = sapPatient.FirstName.NullIfEmpty();
            patient.FullName = sapPatient.FullName.NullIfEmpty();
            patient.Gender = sapPatient.Gender;
            patient.Initials = sapPatient.Initials.NullIfEmpty();
            patient.LastName = sapPatient.LastName.NullIfEmpty();
            patient.MaidenName = sapPatient.MaidenName.NullIfEmpty();
            patient.MiddleName = sapPatient.MiddleName.NullIfEmpty();
            patient.PatientNumber = sapPatient.PatientNumber.NullIfEmpty();
        }

        private PointModels.Bed getBed(int organizationId, int locationId, PointModels.Department department, SAPDetail sapDetail)
        {
            PointModels.Bed bed = null;

            try
            {
                bed = uow.BedRepository.Get(b => b.SystemCode == sapDetail.Bed.SystemCode).FirstOrDefault();

                if (bed != null && bed.DepartmentID != department.DepartmentID)
                {
                    bed.Department = department; //update the entity internally
                    bed.DepartmentID = department.DepartmentID;
                    string sql = String.Format(@"update bed set DepartmentID = {0} where BedID = {1}", bed.DepartmentID, bed.BedID);
                    uow.DbContext.Database.ExecuteSqlCommand(sql); //no other (quick) way found to deal with this and AutoDetectChangesEnabled=false
                }

                string identifier = getBedIdentifier(sapDetail);
                if (bed == null && String.IsNullOrWhiteSpace(sapDetail.Bed.SystemCode)) //no real bed provided, most likely future planned intake
                {
                    bed = uow.BedRepository.Get(b => b.DepartmentID == department.DepartmentID && 
                                                b.SystemCode == null &&
                                                b.Identifier == identifier).FirstOrDefault();
                }

                if (bed == null)
                {
                    bed = new PointModels.Bed()
                    {
                        SystemCode = (String.IsNullOrWhiteSpace(sapDetail.Bed.SystemCode) ? null : sapDetail.Bed.SystemCode),
                        RoomNumber = (String.IsNullOrWhiteSpace(sapDetail.Bed.RoomNumber) ? null : sapDetail.Bed.RoomNumber),
                        BedNumber = (String.IsNullOrWhiteSpace(sapDetail.Bed.BedNumber) ? null : sapDetail.Bed.BedNumber),
                        Department = department,
                        BedSoort = sapDetail.Bed.BedSoort,
                        Identifier = (String.IsNullOrWhiteSpace(sapDetail.Bed.SystemCode) ? identifier : null)
                    };

                    uow.BedRepository.Insert(bed);
                    uow.Save();

                    Logger.LogVerbose(String.Format("Create Bed - {0}.", bed.GetDescription()), null, "Bed");
                }
                else
                {
                    if (sapDetail.BedPatientStatus == PointModels.BedPatientStatus.Opgenomen ||
                        sapDetail.BedPatientStatus == PointModels.BedPatientStatus.OntslagGepland &&
                        bed.BedSoort != sapDetail.Bed.BedSoort)
                    {
                        SqlParameter bedSoortParam = new SqlParameter() { ParameterName = "@bedSoort", Value = sapDetail.Bed.BedSoort };
                        uow.DbContext.Database.ExecuteSqlCommand("update Bed set BedSoort = @bedSoort where BedID = @bedID",
                            bedSoortParam,
                            new SqlParameter() { ParameterName = "@bedID", Value = bed.BedID });
                    }

                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.ToString(), null, "Bed");
            }

            return bed;
        }

        private string getBedIdentifier(SAPDetail sapDetail)
        {
            List<string> identifiers = new List<string>(4);
            identifiers.Add(sapDetail.Patient==null?"":sapDetail.Patient.PatientNumber);
            identifiers.Add(sapDetail.Bed.Department_SystemCode);
            identifiers.Add(sapDetail.Bed.Specialism_SystemCode);
            identifiers.Add(sapDetail.Bed.BedSoort.ToString());
            return String.Join("_", identifiers);
        }

        private PointModels.Department getDepartment(int organizationId, int locationId, SAPDetail sapDetail)
        {
            PointModels.Department department = null;
            try
            {
                department = departmentsLookup.FirstOrDefault(d => d.SystemCode == sapDetail.Bed.Department_SystemCode);
                if (department == null)
                {
                    department = uow.DepartmentRepository.Get(d => d.SystemCode == sapDetail.Bed.Department_SystemCode).FirstOrDefault();
                }

                if (department == null)
                {
                    department = new PointModels.Department()
                    {
                        SystemCode = (String.IsNullOrWhiteSpace(sapDetail.Bed.Department_SystemCode) ? null : sapDetail.Bed.Department_SystemCode),
                        Name = (String.IsNullOrWhiteSpace(sapDetail.Bed.Department_Description) ? null : sapDetail.Bed.Department_Description),
                        LocationID = locationId
                    };
                    if (String.IsNullOrEmpty(department.Name))
                    {
                        department.Name = department.SystemCode;
                    }
                    uow.DepartmentRepository.Insert(department);
                    uow.Save();

                    Logger.LogVerbose(String.Format("Created Department - {0}, SystemCode {1}.", department.Name, department.SystemCode),null, "Department");
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.ToString(), null, "Department");
            }
            return department;
        }

        private void initLookupLists()
        {
            // Place the current data of some tables in a local list. 
            // If not found, the lookup will be done the "old" way (via the repository) and will be added as before. Next run it will be in the list.

            specialismsLookup = uow.SpecialismRepository.GetAll().ToList();
            departmentsLookup = uow.DepartmentRepository.GetAll().ToList();

        }

    }
}