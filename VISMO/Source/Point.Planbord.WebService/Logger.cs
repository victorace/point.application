﻿using Point.Planbord.Database.Context;
using Point.Planbord.Database.Repository;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Xml;
using PointModels = Point.Planbord.Database.Models;

namespace Point.Planbord.WebService
{
    public static class Logger
    {

        public static void LogNotSpecified(string description, int? entityId, string entityName)
        {
            Log(PointModels.Severity.NotSpecified, description, entityId, entityName);
        }

        public static void LogError(string description)
        {
            Log(PointModels.Severity.Error, description, null, null);
        }

        public static void LogError(string description, int? entityId, string entityName)
        {
            Log(PointModels.Severity.Error, description, entityId, entityName);
        }

        public static void LogWarning(string description, int? entityId, string entityName)
        {
            Log(PointModels.Severity.Warning, description, entityId, entityName);
        }

        public static void LogInformation(string description, int? entityId, string entityName)
        {
            if (System.Configuration.ConfigurationManager.AppSettings["LogInformation"] == "true")
            {
                Log(PointModels.Severity.Info, description, entityId, entityName);
            }
        }

        public static void LogVerbose(string description, int? entityId, string entityName)
        {
            if (System.Configuration.ConfigurationManager.AppSettings["LogVerbose"] == "true")
            {
                Log(PointModels.Severity.Verbose, description, entityId, entityName);
            }
        }

        public static void LogSOAP(HttpRequest httpRequest)
        {

            if (System.Configuration.ConfigurationManager.AppSettings["LogSOAP"] == "true")
            {

                if (httpRequest != null)
                {
                    try
                    {
                        XmlDocument xmlSoapRequest = new XmlDocument();
                        Stream receiveStream = httpRequest.InputStream;
                        // Move to begining of input stream and read
                        receiveStream.Position = 0;
                        using (StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8))
                        {
                            xmlSoapRequest.Load(readStream);
                        }
                        LogInformation(xmlSoapRequest.OuterXml, null, "SOAP");
                    }
                    catch (Exception ex)
                    {
                        LogError(ex.ToString(), null, "SOAP");
                    }
                }
            }
        }

        public static void Log(PointModels.Severity severity, string description, int? entityId, string entityName)
        {
            using (UnitOfWork<LoggingContext> u = new UnitOfWork<LoggingContext>())
            {
                u.LoggingRepository.Insert(new PointModels.Logging
                {
                    LoggingDateTime = DateTime.Now,
                    Severity = severity,
                    Description = description,
                    ApplicationName = ((AssemblyTitleAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyTitleAttribute), false)[0]).Title,
                    ReferenceID = (entityId.HasValue ? entityId.Value : (int?)null),
                    ReferenceName = entityName
                });
                u.Save();
            }
        }
    }
}