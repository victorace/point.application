﻿DECLARE @currentversion VARCHAR(14) 
set @currentversion = '1.0.3' 

INSERT INTO [dbo].[version] 
       ( versionnumber, lastupdate )
VALUES ( @currentversion, getdate() )

Print 'Done'
