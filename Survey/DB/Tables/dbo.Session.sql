CREATE TABLE [dbo].[Session]
(
[SessionID] [int] NOT NULL IDENTITY(1, 1),
[CampaignID] [uniqueidentifier] NOT NULL,
[TransferID] [int] NOT NULL,
[Status] [int] NOT NULL CONSTRAINT [DF_Survey_Status] DEFAULT ((0)),
[Timestamp] [datetime] NOT NULL,
[DepartmentID] [int] NOT NULL,
[EmailAddress] [nvarchar] (512) COLLATE Latin1_General_CI_AS NOT NULL,
[Organization] [varchar] (max) COLLATE Latin1_General_CI_AS NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Session] ADD CONSTRAINT [PK_Session] PRIMARY KEY CLUSTERED  ([SessionID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Session] ADD CONSTRAINT [UIX_Session_CampaignID_TransferID] UNIQUE NONCLUSTERED  ([CampaignID], [TransferID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Session] ADD CONSTRAINT [FK_Session_Campaign] FOREIGN KEY ([CampaignID]) REFERENCES [dbo].[Campaign] ([CampaignID])
GO
