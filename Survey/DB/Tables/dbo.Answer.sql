CREATE TABLE [dbo].[Answer]
(
[AnswerID] [int] NOT NULL IDENTITY(1, 1),
[SessionID] [int] NOT NULL,
[Name] [nvarchar] (max) COLLATE Latin1_General_CI_AS NOT NULL,
[Value] [nvarchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Timestamp] [datetime] NOT NULL,
[DepartmentID] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Answer] ADD CONSTRAINT [PK_Answer] PRIMARY KEY CLUSTERED  ([AnswerID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Answer] ADD CONSTRAINT [FK_Answer_Session] FOREIGN KEY ([SessionID]) REFERENCES [dbo].[Session] ([SessionID])
GO
