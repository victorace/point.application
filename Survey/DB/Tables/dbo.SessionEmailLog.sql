CREATE TABLE [dbo].[SessionEmailLog]
(
[SessionEmailLogID] [int] NOT NULL IDENTITY(1, 1),
[SessionID] [int] NOT NULL,
[Timestamp] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SessionEmailLog] ADD CONSTRAINT [PK_SessionEmailLog] PRIMARY KEY CLUSTERED  ([SessionEmailLogID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SessionEmailLog] ADD CONSTRAINT [FK_SessionEmailLog_Session] FOREIGN KEY ([SessionID]) REFERENCES [dbo].[Session] ([SessionID])
GO
