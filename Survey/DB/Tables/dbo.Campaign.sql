CREATE TABLE [dbo].[Campaign]
(
[CampaignID] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Campaign_CampaignID] DEFAULT (newid()),
[Name] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[Active] [bit] NULL CONSTRAINT [DF_Campaign_Isactive] DEFAULT ((1)),
[StartDate] [date] NOT NULL,
[EndDate] [date] NOT NULL,
[Identifier] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[EmailSubject] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[EmailBody] [nvarchar] (max) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Campaign] ADD CONSTRAINT [PK_Campaign] PRIMARY KEY CLUSTERED  ([CampaignID]) ON [PRIMARY]
GO
