CREATE TABLE [dbo].[Version]
(
[VersionNumber] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[LastUpdate] [datetime] NULL
) ON [PRIMARY]
GO
