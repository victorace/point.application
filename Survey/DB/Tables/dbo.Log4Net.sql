CREATE TABLE [dbo].[Log4Net]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Date] [datetime] NOT NULL,
[Thread] [varchar] (255) COLLATE Latin1_General_CI_AS NOT NULL,
[Level] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[Logger] [varchar] (255) COLLATE Latin1_General_CI_AS NOT NULL,
[Message] [varchar] (4000) COLLATE Latin1_General_CI_AS NOT NULL,
[Exception] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[Url] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[QueryString] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[PostData] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[UserAgent] [nvarchar] (max) COLLATE Latin1_General_CI_AS NULL,
[MachineName] [varchar] (4000) COLLATE Latin1_General_CI_AS NULL,
[WindowsIdentity] [varchar] (4000) COLLATE Latin1_General_CI_AS NULL,
[Stacktrace] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[ErrorNumber] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[DepartmentID] [int] NULL,
[CampaignID] [uniqueidentifier] NULL,
[TransferID] [int] NULL,
[Email] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Organization] [varchar] (max) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
