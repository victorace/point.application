INSERT INTO [dbo].[Campaign] ([CampaignID], [Name], [Active], [StartDate], [EndDate], [Identifier], [EmailSubject], [EmailBody]) VALUES ('24aab001-6f05-4b06-8741-ce90b72feafe', N'Verbetering zorgoverdacht', 1, '2018-01-01', '2018-12-31', N'amsmetenavvo', N'Uitnodiging deelname enquête t.b.v. verbeteren zorgoverdacht patiënt', N'Geachte heer/mevrouw,
<br /><br />
U bent betrokken bij de zorgoverdracht van een patiënt van ons ziekenhuis (<Org-Loc>) naar uw zorginstelling. Voor deze transfer is, zoals u weet, een POINT transferdossier gebruikt.
<br /><br />
Om de kwaliteit van de zorgaanmelding en de verpleegkundige overdracht bij de transfer van patiënten te verbeteren vragen wij u medewerking door deel te nemen aan een kleine enquête.
<br /><br />
Wilt u deelnemen? Klik dan op de onderstaande link.
<br />
<enquête>
<br /><br />
Het beantwoorden van de enquête vragen neemt slechts een aantal minuten van uw tijd in beslag.
<br /><br />
Met vriendelijke groet,
<br /><br />
<Org-Loc>')
