﻿using Point.MVC.Survey.Infrastructure;
using System.Web.Mvc;

namespace Point.MVC.Survey.Controllers
{
    public class NotFoundController : Controller
    {
        // GET: NotFound
        public ActionResult Index()
        {
            ViewBag.PageTitle = Constants.PAGE_TITLE;
            return View();
        }
    }
}