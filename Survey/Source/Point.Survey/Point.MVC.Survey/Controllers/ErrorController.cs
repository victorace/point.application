﻿using Point.MVC.Survey.Infrastructure;
using Point.MVC.Survey.Models.ViewModels;
using System.Web.Mvc;

namespace Point.MVC.Survey.Controllers
{
    public class ErrorController : Controller
    {
        public ActionResult Index(ErrorViewModel model)
        {
            ViewBag.PageTitle = Constants.PAGE_TITLE;
            return View(model);
        }
    }
}