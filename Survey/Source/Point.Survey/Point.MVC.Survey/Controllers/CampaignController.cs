﻿using Point.MVC.Survey.Infrastructure;
using Point.MVC.Survey.Infrastructure.Attributes;
using Point.MVC.Survey.Infrastructure.Extensions;
using Point.MVC.Survey.Infrastructure.JsonResults;
using Point.MVC.Survey.Infrastructure.Logging;
using Point.MVC.Survey.Logic;
using Point.MVC.Survey.Models.Campaigns;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Web.Mvc;
using System.Web.Routing;

namespace Point.MVC.Survey.Controllers
{
    public class CampaignController : Controller
    {
        public Guid CampaignID { get; set; }
        public int TransferID { get; set; }
        public int DepartmentID { get; set; }
        public string Digest { get; set; }

        [HttpGet, ValidSession, Digest(Values = new string[] {
            Constants.URL_PARAM_CAMPAIGNID,
            Constants.URL_PARAM_TRANSFERID,
            Constants.URL_PARAM_DEPARTMENTID })]
        public ActionResult Index()
        {
            var campaign = CampaignBL.Get(CampaignID);
            var session = SessionBL.Get(CampaignID, TransferID);
            if (session?.Status == Status.Created)
            {
                SessionBL.Active(CampaignID, TransferID, DepartmentID, DateTime.Now);
            }

            return RedirectToAction(campaign.Identifier, new RouteValueDictionary(new Dictionary<string, object>()
            {
                { Constants.URL_PARAM_CAMPAIGNID, campaign.CampaignID },
                { Constants.URL_PARAM_TRANSFERID, TransferID },
                { Constants.URL_PARAM_DEPARTMENTID, DepartmentID },
                { Constants.URL_PARAM_DIGEST, Digest }
            }));
        }

        [HttpPost, Digest(Values = new string[] {
            Constants.URL_PARAM_CAMPAIGNID,
            Constants.URL_PARAM_TRANSFERID,
            Constants.URL_PARAM_DEPARTMENTID,
            Constants.URL_PARAM_EMAIL,
            Constants.URL_PARAM_ORGANIZATION })]
        public JsonResult Start(string e, string o)
        {
            var email = e;
            if (string.IsNullOrEmpty(email))
            {
                Logger.Error("Email is invalid.", DepartmentID, CampaignID, TransferID, email, o);
                return new BadRequest("Email is invalid.");
            }

            var organization = o;
            if (string.IsNullOrEmpty(organization))
            {
                Logger.Error("Organization is invalid.", DepartmentID, CampaignID, TransferID, email, organization);
                return new BadRequest("Organization is invalid.");
            }

            var session = SessionBL.Get(CampaignID, TransferID);
            if (session == null)
            {
                session = SessionBL.Invite(CampaignID, TransferID, DepartmentID, DateTime.Now, email, organization);
                if (session == null)
                {
                    Logger.Error("Session is invalid.", DepartmentID, CampaignID, TransferID, email, organization);
                    return new BadRequest("Session is invalid.");
                }
            } 

            return new JsonResult()
            {
                Data = new
                {
                    status = Constants.STATUS_OK,
                    session
                },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [HttpGet, Digest(Values = new string[] {
            Constants.URL_PARAM_CAMPAIGNID,
            Constants.URL_PARAM_TRANSFERID,
            Constants.URL_PARAM_DEPARTMENTID })]
        public ActionResult Done()
        {
            var session = SessionBL.Get(CampaignID, TransferID);
            ViewBag.Status = session?.Status ?? Status.Done;
            return View();
        }

        [HttpPost, ValidateAntiForgeryToken, ValidSession, Digest(Values = new string[] {
            Constants.URL_PARAM_CAMPAIGNID,
            Constants.URL_PARAM_TRANSFERID,
            Constants.URL_PARAM_DEPARTMENTID })]
        public JsonResult Dismiss()
        {
            SessionBL.Dismiss(CampaignID, TransferID, DepartmentID, DateTime.Now);

            return new JsonResult()
            {
                Data = new
                {
                    status = Constants.STATUS_OK,
                    url = Url.Action(Constants.CAMPAIGN_DONE,
                        new RouteValueDictionary(new Dictionary<string, object>()
                        {
                            { Constants.URL_PARAM_CAMPAIGNID, CampaignID },
                            { Constants.URL_PARAM_TRANSFERID, TransferID },
                            { Constants.URL_PARAM_DEPARTMENTID, DepartmentID },
                            { Constants.URL_PARAM_DIGEST, Digest }
                        }))
                }
            };
        }

        [HttpGet, ValidSession, Digest(Values = new string[] {
            Constants.URL_PARAM_CAMPAIGNID,
            Constants.URL_PARAM_TRANSFERID,
            Constants.URL_PARAM_DEPARTMENTID })]
        public ActionResult amsmetenavvo()
        {
            var session = SessionBL.Get(CampaignID, TransferID);
            var model = CampaignViewBL.Get(session);
            return View((amsmetenavvo)model);
        }

        [HttpPost, ValidateAntiForgeryToken, ValidSession, Digest(Values = new string[] {
            Constants.URL_PARAM_CAMPAIGNID,
            Constants.URL_PARAM_TRANSFERID,
            Constants.URL_PARAM_DEPARTMENTID })]
        public JsonResult amsmetenavvo(amsmetenavvo model)
        {
            AnswerBL.Save(model);

            SessionBL.Done(model.CampaignID, model.TransferID, model.DepartmentID, DateTime.Now);

            return new JsonResult()
            {
                Data = new
                {
                    status = Constants.STATUS_OK,
                    url = Url.Action(Constants.CAMPAIGN_DONE,
                        new RouteValueDictionary(new Dictionary<string, object>()
                        {
                            { Constants.URL_PARAM_CAMPAIGNID, model.CampaignID },
                            { Constants.URL_PARAM_TRANSFERID, model.TransferID },
                            { Constants.URL_PARAM_DEPARTMENTID, model.DepartmentID },
                            { Constants.URL_PARAM_DIGEST, Digest }
                        }))
                }
            };
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            CampaignID = System.Web.HttpContext.Current.Request.GetRequestVariable<Guid>(Constants.URL_PARAM_CAMPAIGNID, default(Guid));
            TransferID = System.Web.HttpContext.Current.Request.GetRequestVariable<int>(Constants.URL_PARAM_TRANSFERID, 0);
            DepartmentID = System.Web.HttpContext.Current.Request.GetRequestVariable<int>(Constants.URL_PARAM_DEPARTMENTID, 0);
            Digest = System.Web.HttpContext.Current.Request.GetRequestVariable<string>(Constants.URL_PARAM_DIGEST, "");

            if (CampaignID == Guid.Empty)
            {
                throw new ArgumentException(nameof(CampaignID));
            }
            if (TransferID <= 0)
            {
                throw new ArgumentException(nameof(TransferID));
            }
            if (DepartmentID <= 0)
            {
                throw new ArgumentException(nameof(DepartmentID));
            }

            if (filterContext.HttpContext.Request.HttpMethod == HttpMethod.Post.Method)
            {
                // Update model with parameters from URL, this way we don't need to create
                // hidden input elements to hold important information required in the model.
                foreach (var key in filterContext.ActionParameters.Keys)
                {
                    if (filterContext.ActionParameters[key] is ICampaign model)
                    {
                        model.CampaignID = CampaignID;
                        model.TransferID = TransferID;
                        model.DepartmentID = DepartmentID;
                        break;
                    }
                }
            }

            ViewBag.PageTitle = $"{Constants.PAGE_TITLE} - {CampaignBL.Get(CampaignID)?.Name}";
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            if (filterContext.ExceptionHandled)
            {
                return;
            }

            var forceerrorhandling = ConfigurationManager.AppSettings.GetValueOrDefault<bool>(Constants.CONFIG_FORCEERRORHANDLING, false);
            if (!HttpContext.IsDebuggingEnabled || forceerrorhandling)
            {
                int errornummber = logerror(filterContext);
                if (filterContext.HttpContext.Request.IsAjaxRequest())
                {
                    filterContext.Result = new InternalServerError(errornummber);
                }
                else
                {
                    filterContext.Result = new RedirectResult($"~/error?errorNumber={errornummber}");
                }
            }
        }

        private int logerror(ExceptionContext filterContext)
        {
            string postdata = null;
            if (filterContext.HttpContext?.Request?.InputStream != null)
            {
                var streamreader = new StreamReader(filterContext.HttpContext.Request.InputStream);
                postdata = streamreader.ReadToEnd();
            }

            filterContext.HttpContext.Response.StatusCode = HttpStatusCode.BadRequest.To<int>();
            filterContext.ExceptionHandled = true;

            var email = System.Web.HttpContext.Current.Request.GetRequestVariable<string>(Constants.URL_PARAM_EMAIL, null);
            var organization = System.Web.HttpContext.Current.Request.GetRequestVariable<string>(Constants.URL_PARAM_ORGANIZATION, null);
            return Logger.Error(filterContext.Exception, Request?.Url, postdata, Request?.UserAgent,
                DepartmentID, CampaignID, TransferID, email, organization);
        }
    }
}