﻿namespace Point.MVC.Survey.Infrastructure
{
    public enum Status
    {
        None = 0,
        Created = 1,
        Active = 2,
        Dismissed = 3,
        Done = 4
    }
}