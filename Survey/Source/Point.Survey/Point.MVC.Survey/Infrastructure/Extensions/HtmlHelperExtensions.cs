﻿using System.Configuration;
using System.Web.Mvc;
using System.Web.Routing;

namespace Point.MVC.Survey.Infrastructure.Extensions
{
    public static class HtmlHelperExtensions
    {
        public static MvcHtmlString DossierPatientLink(this HtmlHelper htmlhelper, int transferid, string title = "Dossier patient")
        {
            var dossierpatienturl = ConfigurationManager.AppSettings.GetValueOrDefault<string>(Constants.CONFIG_DOSSIERPATIENTURL, "#");

            TagBuilder tagbuilder = new TagBuilder("a");
            tagbuilder.GenerateId("dossierpatient");
            var routevaluedict = new RouteValueDictionary();
            routevaluedict.Add("TransferID", transferid);
            tagbuilder.Attributes["name"] = "dossierpatient";
            tagbuilder.Attributes["href"] = $"{dossierpatienturl.TrimEnd('/')}/FlowMain/Dashboard?TransferID={transferid}";
            tagbuilder.Attributes["target"] = "_blank";
            tagbuilder.InnerHtml = title;

            return MvcHtmlString.Create(tagbuilder.ToString(TagRenderMode.Normal));
        }
    }
}