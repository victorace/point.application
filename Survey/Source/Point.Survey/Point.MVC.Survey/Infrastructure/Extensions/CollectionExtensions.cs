﻿using System;
using System.Collections.Specialized;
using System.Linq;

namespace Point.MVC.Survey.Infrastructure.Extensions
{
    public static class CollectionExtensions
    {
        public static T GetValueOrDefault<T>(this NameValueCollection collection, string key, object defaultValue)
        {
            if (collection.Get(key) != null || collection.Keys.Cast<string>().Contains(key, StringComparer.InvariantCulture))
            {
                try
                {
                    return (T)Convert.ChangeType(collection.Get(key), typeof(T));
                }
                catch { /* return default value */ }
            }

            return (T)Convert.ChangeType(defaultValue, typeof(T));
        }
    }
}