﻿using System;
using System.Text;
using System.Text.RegularExpressions;

namespace Point.MVC.Survey.Infrastructure.Extensions
{
    public static class StringExtensions
    {
        /// <summary>
        /// Fastest methods to convert byte[] to string and vice versa
        /// http://stackoverflow.com/questions/623104/byte-to-hex-string/3974535#3974535
        /// </summary>

        public static string ByteArrayToHexString(this byte[] bytes)
        {
            char[] c = new char[bytes.Length * 2];

            byte b;

            for (int bx = 0, cx = 0; bx < bytes.Length; ++bx, ++cx)
            {
                b = ((byte)(bytes[bx] >> 4));
                c[cx] = (char)(b > 9 ? b + 0x37 + 0x20 : b + 0x30);

                b = ((byte)(bytes[bx] & 0x0F));
                c[++cx] = (char)(b > 9 ? b + 0x37 + 0x20 : b + 0x30);
            }

            return new string(c);
        }

        public static byte[] HexStringToByteArray(this string s)
        {
            if (String.IsNullOrWhiteSpace(s) || s.Length % 2 != 0)
                return new byte[0];

            byte[] buffer = new byte[s.Length / 2];
            char c;
            for (int bx = 0, sx = 0; bx < buffer.Length; ++bx, ++sx)
            {
                // Convert first half of byte
                c = s[sx];
                buffer[bx] = (byte)((c > '9' ? (c > 'Z' ? (c - 'a' + 10) : (c - 'A' + 10)) : (c - '0')) << 4);

                // Convert second half of byte
                c = s[++sx];
                buffer[bx] |= (byte)(c > '9' ? (c > 'Z' ? (c - 'a' + 10) : (c - 'A' + 10)) : (c - '0'));
            }

            return buffer;
        }

        public static bool IsBase64String(this string s)
        {
            if (String.IsNullOrEmpty(s))
            {
                return false;
            }

            s = s.Trim();
            return (s.Length % 4 == 0) && Regex.IsMatch(s, @"^[a-zA-Z0-9\+/]*={0,3}$", RegexOptions.None);
        }

        public static string Base64Decode(this string s)
        {
            if (!IsBase64String(s))
            {
                return s;
            }
            return Encoding.UTF8.GetString(Convert.FromBase64String(s));
        }
    }
}
