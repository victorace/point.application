﻿using Point.MVC.Survey.Infrastructure.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Point.MVC.Survey.Infrastructure.Extensions
{
    public static class HttpRequestBaseExtensions
    {
        public static string GetParam(this HttpRequestBase httpRequest, string keyName)
        {
            var querystringkeys = httpRequest.Unvalidated.QueryString.AllKeys;
            IDictionary<string, string> querystringvalues = querystringkeys.ToDictionary(k => k.ToLowerInvariant(), k => httpRequest.Unvalidated.QueryString[k]);
            var formkeys = httpRequest.Unvalidated.Form.AllKeys.ToArray();
            IDictionary<string, string> formvalues = formkeys.ToDictionary(k => k.ToLowerInvariant(), k => httpRequest.Unvalidated.Form[k]);

            var allvalues =
                querystringvalues.Concat(formvalues.Where(it => querystringvalues.All(qsv => qsv.Key != it.Key)))
                    .ToDictionary(pair => pair.Key, pair => pair.Value);

            if (allvalues.ContainsKey(keyName.ToLowerInvariant()))
            {
                return allvalues[keyName.ToLowerInvariant()];
            }

            allvalues = httpRequest.RequestContext.RouteData.Values.ToDictionary(v => v.Key.ToLowerInvariant(), v => v.Value.ToString());

            return allvalues.ContainsKey(keyName.ToLowerInvariant())
                ? allvalues[keyName.ToLowerInvariant()]
                : null;
        }

        public static T GetRequestVariable<T>(this HttpRequestBase httpRequestBase, string name, object defaultvalue)
        {
            T result;

            var value = httpRequestBase?.GetParam(name);

            var t = Nullable.GetUnderlyingType(typeof(T)) ?? typeof(T);

            try
            {
                if (t == typeof(Guid) || t == typeof(Guid?))
                {
                    if (!string.IsNullOrWhiteSpace(value))
                    {
                        result = (T)Convert.ChangeType(new Guid(value), t);
                    }
                    else
                    {
                        result = (defaultvalue == null  ? default(T) : (T)Convert.ChangeType(new Guid(defaultvalue.ToString()), t));
                    }
                }
                else
                {
                    var isnullable = t.IsGenericType && t.GetGenericTypeDefinition() == typeof(Nullable<>);
                    if (!string.IsNullOrWhiteSpace(value))
                    {
                        result = (T)Convert.ChangeType(value, t);
                    }
                    else
                    {
                        result = ((defaultvalue == null && !isnullable) ? default(T) : (T)Convert.ChangeType((T)defaultvalue, t));
                    }
                }
            }
            catch (Exception e)
            {
                result = default(T);
                value = (string.IsNullOrWhiteSpace(value) ? "null" : value);
                defaultvalue = (defaultvalue ?? "null");
                Logger.Error($"GetRequestVariable - Name:'{name}', Value:'{value}', DefaultValue:{defaultvalue}', Type:'{typeof(T).FullName}', Result:{result?.ToString()}", e);
            }
            return result;
        }

        public static T GetRequestVariable<T>(this HttpRequest httpRequest, string name, object defaultValue)
        {
            var httpRequestBase = new HttpRequestWrapper(httpRequest);
            return httpRequestBase.GetRequestVariable<T>(name, defaultValue);
        }
    }
}