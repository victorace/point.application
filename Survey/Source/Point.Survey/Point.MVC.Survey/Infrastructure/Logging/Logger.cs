﻿using log4net;
using System;

namespace Point.MVC.Survey.Infrastructure.Logging
{
    public static class Logger
    {
        private static ILog Log { get; set; }

        static Logger()
        {
            Log = LogManager.GetLogger(typeof(Logger));
        }

        public static int GetErrorNumber(DateTime datetime)
        {
            var random = new Random(DateTime.Now.Millisecond).Next(10, 99);
            return Convert.ToInt32(random.ToString() + datetime.ToString("HHmm"));
        }

        public static int Error(Exception exc, Uri uri, string postdata, string useragent, int departmentid, Guid campaignid, int transferid, string email, string organization)
        {
            int errorNumber = GetErrorNumber(DateTime.Now);
            ThreadContext.Properties["url"] = uri?.AbsolutePath;
            ThreadContext.Properties["queryString"] = uri?.Query;
            ThreadContext.Properties["postData"] = postdata;
            ThreadContext.Properties["userAgent"] = useragent;
            ThreadContext.Properties["machineName"] = Environment.MachineName;
            ThreadContext.Properties["windowsIdentity"] = Environment.UserName;
            ThreadContext.Properties["stacktrace"] = exc.StackTrace;
            ThreadContext.Properties["errorNumber"] = errorNumber;
            ThreadContext.Properties["departmentID"] = departmentid;
            ThreadContext.Properties["campaignID"] = campaignid;
            ThreadContext.Properties["transferid"] = transferid;
            ThreadContext.Properties["email"] = email;
            ThreadContext.Properties["organization"] = organization;
            Log.Error(exc.Message, exc);
            return errorNumber;
        }

        public static int Error(string message, int departmentid, Guid campaignid, int transferid, string email, string organization)
        {
            int errorNumber = GetErrorNumber(DateTime.Now);
            ThreadContext.Properties["machineName"] = Environment.MachineName;
            ThreadContext.Properties["windowsIdentity"] = Environment.UserName;
            ThreadContext.Properties["departmentID"] = departmentid;
            ThreadContext.Properties["campaignID"] = campaignid;
            ThreadContext.Properties["transferid"] = transferid;
            ThreadContext.Properties["email"] = email;
            ThreadContext.Properties["organization"] = organization;
            Log.Error(message);
            return errorNumber;
        }

        public static void Error(object msg)
        {
            Log.Error(msg);
        }

        public static void Error(object msg, Exception ex)
        {
            Log.Error(msg, ex);
        }

        public static void Error(Exception ex)
        {
            Log.Error(ex.Message, ex);
        }

        public static void Fatal(object msg)
        {
            Log.Fatal(msg);
        }

        public static void Fatal(object msg, Exception ex)
        {
            Log.Fatal(msg, ex);
        }

        public static void Fatal(Exception ex)
        {
            Log.Fatal(ex.Message, ex);
        }

        public static void Info(object msg)
        {
            Log.Info(msg);
        }

        public static void Info(int departmentid, Guid campaignid, int transferid, string message)
        {
            ThreadContext.Properties["machineName"] = Environment.MachineName;
            ThreadContext.Properties["windowsIdentity"] = Environment.UserName;
            ThreadContext.Properties["departmentID"] = departmentid;
            ThreadContext.Properties["campaignID"] = campaignid;
            ThreadContext.Properties["transferid"] = transferid;
            Log.Info(message);
        }

        public static void Debug(object msg)
        {
            Log.Debug(msg);
        }
    }
}
