﻿namespace Point.MVC.Survey.Infrastructure
{
    public static class Constants
    {
        public const string PAGE_TITLE = "Enquête";

        public const string STATUS_OK = "OK";

        public const string URL_PARAM_CAMPAIGNID = "c";
        public const string URL_PARAM_TRANSFERID = "t";
        public const string URL_PARAM_DEPARTMENTID = "d";
        public const string URL_PARAM_EMAIL = "e";
        public const string URL_PARAM_ORGANIZATION = "o";
        public const string URL_PARAM_DIGEST = "h";

        public const string CONFIG_FORCEERRORHANDLING = "ForceErrorHandling";
        public const string CONFIG_WEBSERVER = "WebServer";
        public const string CONFIG_DOSSIERPATIENTURL = "DossierPatientURL";
        public const string CONFIG_SPARKPOSTMAIL_PORT = "sparkpostmail.port";
        public const string CONFIG_SPARKPOSTMAIL_HOST = "sparkpostmail.host";
        public const string CONFIG_SPARKPOSTMAIL_USERNAME = "sparkpostmail.username";
        public const string CONFIG_SPARKPOSTMAIL_PASSWORD = "sparkpostmail.password";
        public const string CONFIG_SPARKPOSTMAIL_FROM = "sparkpostmail.from";
        public const string CONFIG_SPARKPOSTMAIL_FROM_DISPLAYNAME = "sparkpostmail.from.displayname";

        public const string NAMESPACE_CAMPAIGNS = "Point.MVC.Survey.Models.Campaigns";

        public const string CAMPAIGN_CONTROLLER = "Campaign";
        public const string CAMPAIGN_DONE = "Done";
    }
}