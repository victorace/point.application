﻿using System;

namespace Point.MVC.Survey.Infrastructure.Exceptions
{
    public class CampaignException : Exception
    {
        public CampaignException() { }
        public CampaignException(string message) : base(message) { }
        public CampaignException(string message, Exception inner) : base(message, inner) { }
    }
}