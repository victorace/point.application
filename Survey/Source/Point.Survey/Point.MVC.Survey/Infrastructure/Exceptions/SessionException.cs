﻿using System;

namespace Point.MVC.Survey.Infrastructure.Exceptions
{
    public class SessionException : Exception
    {
        public SessionException() { }
        public SessionException(string message) : base(message) { }
        public SessionException(string message, Exception inner) : base(message, inner) { }
    }
}