﻿using System;

namespace Point.MVC.Survey.Infrastructure.Exceptions
{
    public class DigestException : Exception
    {
        public DigestException() { }
        public DigestException(string message) : base(message) { }
        public DigestException(string message, Exception inner) : base(message, inner) { }
    }
}