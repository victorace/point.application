﻿using Point.MVC.Survey.Infrastructure.Exceptions;
using Point.MVC.Survey.Infrastructure.Helpers;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;

namespace Point.MVC.Survey.Infrastructure.Attributes
{
    public class DigestAttribute : ActionFilterAttribute
    {
        public string[] Values { get; set; }

        private bool debug;

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            debug = filterContext.HttpContext.IsDebuggingEnabled;

            List<string> todigest = new List<string>(Values.Length);
            foreach (var key in Values)
            {
                todigest.Add(HttpContext.Current.Request.Params.Get(key));
            }

            var digest = HttpContext.Current.Request.Params.Get(Constants.URL_PARAM_DIGEST);
            var value = string.Join("|", todigest);
            if (!DigestHelper.CheckDigest(value, digest))
            {
                ThrowException(String.Format("Digest check failed: digestvalue [{0}], propertyvalue [{1}], plaindigest [{2}]", digest, value, DigestHelper.GetPlainDigest(digest)));
            }

            base.OnActionExecuting(filterContext);
        }

        protected void ThrowException(string message)
        {
            if (!debug)
            {
                message = "Digest check failed."; //Don't provide any more info than needed.
            }

            throw new DigestException(message);
        }
    }
}