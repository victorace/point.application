﻿using Point.MVC.Survey.Controllers;
using Point.MVC.Survey.Infrastructure.Exceptions;
using Point.MVC.Survey.Logic;
using System.Web.Mvc;
using System.Web.Routing;

namespace Point.MVC.Survey.Infrastructure.Attributes
{
    public class ValidSessionAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (filterContext.Controller is CampaignController controller)
            {
                var campaign = CampaignBL.Get(controller.CampaignID);
                var session = SessionBL.Get(campaign.CampaignID, controller.TransferID);
                if (session == null)
                {
                    throw new SessionException("No session found.");
                }
                if (session.Status == Status.Dismissed || session.Status == Status.Done)
                {
                    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary {
                        { "Controller", Constants.CAMPAIGN_CONTROLLER },
                        { "Action", Constants.CAMPAIGN_DONE },
                        { Constants.URL_PARAM_CAMPAIGNID, controller.CampaignID },
                        { Constants.URL_PARAM_TRANSFERID, controller.TransferID },
                        { Constants.URL_PARAM_DEPARTMENTID, controller.DepartmentID },
                        { Constants.URL_PARAM_DIGEST, controller.Digest },
                    });
                }
            }

            base.OnActionExecuting(filterContext);
        }
    }
}
