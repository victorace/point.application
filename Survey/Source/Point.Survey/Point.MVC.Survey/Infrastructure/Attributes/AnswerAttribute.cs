﻿using System;

namespace Point.MVC.Survey.Infrastructure.Attributes
{
    public class AnswerAttribute : Attribute { }
}