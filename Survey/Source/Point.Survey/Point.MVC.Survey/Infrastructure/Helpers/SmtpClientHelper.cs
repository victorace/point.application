﻿using Point.MVC.Survey.Infrastructure.Extensions;
using Point.MVC.Survey.Infrastructure.Logging;
using System;
using System.Configuration;
using System.Net.Mail;

namespace Point.MVC.Survey.Infrastructure.Helpers
{
    public static class SmtpClientHelper
    {
        public static bool Send(Guid campaignid, int transferid, int departmentid, string emailaddress, string subject, string body)
        {
            if (string.IsNullOrWhiteSpace(emailaddress))
            {
                Logger.Info(departmentid, campaignid, transferid, $"Invalid email address.");
                return false;
            }

            if (string.IsNullOrWhiteSpace(body))
            {
                Logger.Info(departmentid, campaignid, transferid, $"Invalid body.");
                return false;
            }

            var sparkpostmail_port = ConfigurationManager.AppSettings.GetValueOrDefault<int>(Constants.CONFIG_SPARKPOSTMAIL_PORT, 0);
            var sparkpostmail_host = ConfigurationManager.AppSettings.GetValueOrDefault<string>(Constants.CONFIG_SPARKPOSTMAIL_HOST, null);
            if (sparkpostmail_port <= 0 || string.IsNullOrWhiteSpace(sparkpostmail_host))
            {
                Logger.Info(departmentid, campaignid, transferid, "Invalid SparkPost configuration.");
                return false;
            }

            var sparkpostmail_username = ConfigurationManager.AppSettings.GetValueOrDefault<string>(Constants.CONFIG_SPARKPOSTMAIL_USERNAME, null);
            var sparkpostmail_password = ConfigurationManager.AppSettings.GetValueOrDefault<string>(Constants.CONFIG_SPARKPOSTMAIL_PASSWORD, null);
            if (string.IsNullOrWhiteSpace(sparkpostmail_username) || string.IsNullOrWhiteSpace(sparkpostmail_password))
            {
                Logger.Info(departmentid, campaignid, transferid, "Invalid SparkPost credentials.");
                return false;
            }

            try
            {
                SmtpClient smtpclient = new SmtpClient
                {
                    Port = sparkpostmail_port,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Host = sparkpostmail_host,
                    EnableSsl = true,
                    Credentials = new System.Net.NetworkCredential(sparkpostmail_username, sparkpostmail_password)
                };

                var fromaddress = ConfigurationManager.AppSettings.GetValueOrDefault<string>(Constants.CONFIG_SPARKPOSTMAIL_FROM, "geen-antwoord-point@verzorgdeoverdracht.nl");
                var mailmessage = new MailMessage()
                {
                    From = new MailAddress(fromaddress),
                    Subject = string.IsNullOrWhiteSpace(subject) ? "Uitnodiging deelname enquête tbv verbeteren zorgoverdacht patiënt" : subject,
                    IsBodyHtml = true,
                    Body = body
                };

                string[] toparts = emailaddress.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                if (toparts.Length == 0)
                {
                    Logger.Info(departmentid, campaignid, transferid, $"Invalid email address.");
                    return false;
                }

                foreach (string email in toparts)
                {
                    mailmessage.To.Add(email);
                }

                smtpclient.Send(mailmessage);
            }
            catch (Exception e)
            {
                Logger.Error(e);
                return false;
            }

            return true;
        }
    }
}