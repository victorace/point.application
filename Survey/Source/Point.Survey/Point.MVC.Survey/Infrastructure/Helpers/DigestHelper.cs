﻿using Point.MVC.Survey.Infrastructure.Extensions;
using Point.MVC.Survey.Infrastructure.Logging;
using System;
using System.Text;
using System.Web.Security;

namespace Point.MVC.Survey.Infrastructure.Helpers
{
    public static class DigestHelper
    {
        public static string CreateDigest(object value)
        {
            if (value == null)
            {
                return "";
            }
            byte[] protectedbytes = MachineKey.Protect(Encoding.UTF8.GetBytes(Convert.ToString(value)), "Encryption");
            return protectedbytes.ByteArrayToHexString();
        }

        public static string GetPlainDigest(string digest)
        {
            string plaindigest = string.Empty;
            try
            {
                byte[] plainbytes = MachineKey.Unprotect(digest.HexStringToByteArray(), "Encryption");
                plaindigest = Encoding.UTF8.GetString(plainbytes);
            }
            catch (Exception e)
            {
                Logger.Error(e);
            }
            return plaindigest;
        }

        public static bool CheckDigest(object value, string digest)
        {
            if (String.IsNullOrWhiteSpace(digest) || value == null)
            {
                return false;
            }

            return GetPlainDigest(digest) == Convert.ToString(value);
        }
    }
}