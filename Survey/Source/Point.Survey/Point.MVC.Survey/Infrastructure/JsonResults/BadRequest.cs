﻿using Point.MVC.Survey.Infrastructure.Extensions;
using System.Net;
using System.Web.Mvc;

namespace Point.MVC.Survey.Infrastructure.JsonResults
{
    public class BadRequest : JsonResult
    {
        public BadRequest()
        {
        }

        public BadRequest(string message)
        {
            Data = message;
        }

        public BadRequest(object data)
        {
            Data = data;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            context.RequestContext.HttpContext.Response.StatusCode = HttpStatusCode.BadRequest.To<int>();
            // HACK is this a good idea?
            JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            base.ExecuteResult(context);
        }
    }
}