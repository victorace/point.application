﻿using Point.MVC.Survey.Infrastructure.Extensions;
using System.Net;
using System.Web.Mvc;

namespace Point.MVC.Survey.Infrastructure.JsonResults
{
    public class InternalServerError : JsonResult
    {
        public InternalServerError()
        {
        }

        public InternalServerError(string message)
        {
            Data = message;
        }

        public InternalServerError(object data)
        {
            Data = data;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            context.RequestContext.HttpContext.Response.StatusCode = HttpStatusCode.InternalServerError.To<int>();
            // HACK is this a good idea?
            JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            base.ExecuteResult(context);
        }
    }
}