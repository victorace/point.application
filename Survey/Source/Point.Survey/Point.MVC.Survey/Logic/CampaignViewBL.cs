﻿using Point.MVC.Survey.Infrastructure;
using Point.MVC.Survey.Infrastructure.Attributes;
using Point.MVC.Survey.Infrastructure.Exceptions;
using Point.MVC.Survey.Models;
using Point.MVC.Survey.Models.Campaigns;
using System;
using System.Linq;

namespace Point.MVC.Survey.Logic
{
    public static class CampaignViewBL
    {
        public static ICampaign Get(Session session)
        {
            if (session == null)
            {
                throw new ArgumentNullException(nameof(session));
            }

            var identifier = session?.Campaign?.Identifier;
            if (string.IsNullOrWhiteSpace(identifier))
            {
                throw new CampaignException("No valid identifier.");
            }

            var campaign = (ICampaign)Activator.CreateInstance(Type.GetType($"{Constants.NAMESPACE_CAMPAIGNS}.{identifier}"));

            campaign.CampaignID = session.CampaignID;
            campaign.TransferID = session.TransferID;
            campaign.DepartmentID = session.DepartmentID;
            campaign.Status = session.Status;

            var answers = AnswerBL.Get(session.SessionID, session.TransferID);
            foreach (var answer in answers)
            {
                var propertyinfo = campaign.GetType().GetProperties().FirstOrDefault(p => string.Equals(p.Name, answer.Name, StringComparison.InvariantCultureIgnoreCase));
                if (propertyinfo != null)
                {
                    object[] attribute = propertyinfo.GetCustomAttributes(typeof(AnswerAttribute), true);
                    if (attribute.Length > 0)
                    {
                        propertyinfo.SetValue(campaign, Convert.ChangeType(answer.Value, propertyinfo.PropertyType));
                    }
                }
            }

            return campaign;
        }
    }
}