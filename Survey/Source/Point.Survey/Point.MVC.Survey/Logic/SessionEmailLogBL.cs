﻿using Point.MVC.Survey.Context;
using Point.MVC.Survey.Models;
using System;
using System.Data.Entity;

namespace Point.MVC.Survey.Logic
{
    public static class SessionEmailLogBL
    {
        public static void Add(int sessionid, DateTime timestamp)
        {
            using (var db = new SurveyContext())
            {
                var sessionemaillog = new SessionEmailLog()
                {
                    SessionID = sessionid,
                    Timestamp = timestamp
                };

                db.Entry(sessionemaillog).State = EntityState.Added;

                db.SaveChanges();
            }
        }
    }
}