﻿using Point.MVC.Survey.Context;
using Point.MVC.Survey.Infrastructure.Attributes;
using Point.MVC.Survey.Models;
using Point.MVC.Survey.Models.Campaigns;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Point.MVC.Survey.Logic
{
    public static class AnswerBL
    {
        public static void Save(ICampaign model)
        {
            var session = SessionBL.Get(model.CampaignID, model.TransferID);
            if (session.Status == Infrastructure.Status.Active)
            {
                var answers = Get(session.SessionID, session.TransferID);

                using (var db = new SurveyContext())
                {
                    foreach (var propertyinfo in model.GetType().GetProperties())
                    {
                        object[] attribute = propertyinfo.GetCustomAttributes(typeof(AnswerAttribute), true);
                        if (attribute.Length > 0)
                        {
                            var answer = answers.FirstOrDefault(a => string.Equals(a.Name, propertyinfo.Name, StringComparison.InvariantCultureIgnoreCase));
                            if (answer == null)
                            {
                                answer = new Answer()
                                {
                                    SessionID = session.SessionID,
                                    Name = propertyinfo.Name
                                };
                            }

                            answer.Value = propertyinfo.GetValue(model)?.ToString();
                            answer.Timestamp = DateTime.Now;
                            answer.DepartmentID = model.DepartmentID;

                            db.Entry(answer).State = answer.AnswerID <= 0 ? EntityState.Added : EntityState.Modified;
                        }
                    }

                    db.SaveChanges();
                }
            }
        }

        public static List<Answer> Get(int sessionid, int transferid)
        {
            var answers = new List<Answer>();

            using (var db = new SurveyContext())
            {
                answers = db.Answer.Include(nameof(Session)).Where(a => a.SessionID == sessionid && a.Session.TransferID == transferid).ToList();
            }

            return answers;
        }
    }
}