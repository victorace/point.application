﻿using Point.MVC.Survey.Context;
using Point.MVC.Survey.Infrastructure.Exceptions;
using Point.MVC.Survey.Models;
using System;
using System.Linq;

namespace Point.MVC.Survey.Logic
{
    public static class CampaignBL
    {
        public static Campaign Get(Guid campaignid)
        {
            DateTime now = DateTime.Now;
            var campaign = new Campaign();
            using (var db = new SurveyContext())
            {
                campaign = db.Campaign.FirstOrDefault(c => c.CampaignID == campaignid);

                if (campaign == null)
                {
                    throw new CampaignException($"No Campaign found.");
                }
                else if (campaign.Active == false || (now < campaign.StartDate || now > campaign.EndDate))
                {
                    throw new CampaignException($"Campaign is no longer available.");
                }
                else if (string.IsNullOrWhiteSpace(campaign.Identifier))
                {
                    throw new CampaignException($"Campaign has invalid Identifier.");
                }
            }
            return campaign;
        }
    }
}