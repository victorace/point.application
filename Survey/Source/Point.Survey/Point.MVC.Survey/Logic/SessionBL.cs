﻿using Point.MVC.Survey.Context;
using Point.MVC.Survey.Infrastructure;
using Point.MVC.Survey.Infrastructure.Extensions;
using Point.MVC.Survey.Infrastructure.Helpers;
using Point.MVC.Survey.Models;
using System;
using System.Configuration;
using System.Data.Entity;
using System.Linq;

namespace Point.MVC.Survey.Logic
{
    public static class SessionBL
    {
        public static Session Invite(Guid campaignid, int transferid, int departmentid, DateTime timestamp, string email, string organization)
        {
            var campaign = CampaignBL.Get(campaignid);
            if (campaign != null)
            {
                if (string.IsNullOrEmpty(organization))
                {
                    organization = "onbekend";
                }

                var webserver = ConfigurationManager.AppSettings.GetValueOrDefault<string>(Constants.CONFIG_WEBSERVER, string.Empty);
                var digest = DigestHelper.CreateDigest($"{campaignid}|{transferid}|{departmentid}");

                // simple find/replace for the email. supporting 2 external variables <enquête> and <Org-Loc>
                string url = $"{webserver.TrimEnd('/')}/Campaign?{Constants.URL_PARAM_CAMPAIGNID}={campaignid}&{Constants.URL_PARAM_TRANSFERID}={transferid}&{Constants.URL_PARAM_DEPARTMENTID}={departmentid}&{Constants.URL_PARAM_DIGEST}={digest}";
                var body = campaign?.EmailBody.Replace("<enquête>", $@"<a href='{url}'>Ga naar enquête</a>");
                body = body.Replace("<Org-Loc>", organization);

                var mailsent = SmtpClientHelper.Send(campaignid, transferid, departmentid, email, campaign.EmailSubject, body);
                if (mailsent)
                {
                    var session = Start(campaignid, transferid, departmentid, timestamp, email, organization);
                    if (session != null)
                    {
                        SessionEmailLogBL.Add(session.SessionID, timestamp);
                        return session;
                    }
                }
            }

            return null;
        }

        public static Session Get(Guid campaignid, int transferid)
        {
            var campaign = CampaignBL.Get(campaignid);

            var session = new Session();

            using (var db = new SurveyContext())
            {
                session = db.Session.Include("Campaign")
                    .FirstOrDefault(s => s.CampaignID == campaign.CampaignID && s.TransferID == transferid);
            }

            return session;
        }

        public static Session Start(Guid campaignid, int transferid, int departmentid, DateTime timestamp, string email, string organization)
        {
            var campaign = CampaignBL.Get(campaignid);

            using (var db = new SurveyContext())
            {
                var session = new Session()
                {
                    CampaignID = campaign.CampaignID,
                    TransferID = transferid,
                    Status = Status.Created,
                    Timestamp = timestamp,
                    DepartmentID = departmentid,
                    EmailAddress = email,
                    Organization = organization
                };

                db.Entry(session).State = System.Data.Entity.EntityState.Added;

                db.SaveChanges();
            }

            return Get(campaignid, transferid);
        }

        public static Session Dismiss(Guid campaignid, int transferid, int departmentid, DateTime timestamp)
        {
            return SetStatus(campaignid, transferid, departmentid, timestamp, Status.Dismissed);
        }

        public static Session Done(Guid campaignid, int transferid, int departmentid, DateTime timestamp)
        {
            return SetStatus(campaignid, transferid, departmentid, timestamp, Status.Done);
        }

        public static Session Active(Guid campaignid, int transferid, int departmentid, DateTime timestamp)
        {
            return SetStatus(campaignid, transferid, departmentid, timestamp, Status.Active);
        }

        public static Session SetStatus(Guid campaignid, int transferid, int departmentid, DateTime timestamp, Status status)
        {
            var campaign = CampaignBL.Get(campaignid);
            var session = Get(campaign.CampaignID, transferid);
            using (var db = new SurveyContext())
            {
                session.DepartmentID = departmentid;
                session.Timestamp = timestamp;
                session.Status = status;

                db.Entry(session).State = EntityState.Modified;

                db.SaveChanges();
            }

            return session;
        }
    }
}