﻿using Point.MVC.Survey.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Point.MVC.Survey.Context
{
    public partial class SurveyContext : DbContext
    {
        public SurveyContext() : base("name=SurveyContext")
        {
        }

        public virtual DbSet<Answer> Answer { get; set; }
        public virtual DbSet<Campaign> Campaign { get; set; }
        public virtual DbSet<Session> Session { get; set; }
        public virtual DbSet<SessionEmailLog> SessionEmailLog { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}
