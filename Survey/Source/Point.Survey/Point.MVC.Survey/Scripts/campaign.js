﻿function ajaxpost(formid, requesturl) {
    var form = $(formid);
    $.ajax({
        type: "POST",
        cache: false,
        url: requesturl,
        data: form.serialize(),
        async: true,
        success: function (data) {
            if (data !== undefined && data.url !== undefined) {
                location.replace(data.url);
            } else {
                location.reload();
            }
        },
        error: function (xhr) {
            var msg;
            try {
                var response = JSON.parse(xhr.responseText);
                msg = response.ErrorMessage;
            }
            catch (ex) {
                msg = xhr.statusText;
            }
        }
    });
}

function clearcontainer(container) {
    $(container).find(":input").each(function () {
        switch (this.type) {
            case "password":
            case "text":
            case "textarea":
            case "file":
            case "select-one":
            case "select-multiple":
            case "date":
            case "number":
            case "tel":
            case "email":
                $(this).val("");
                break;
            case "checkbox":
            case "radio":
                $(this).removeProp("checked");
                break;
        }
    });
}

$(function () {
    $("#start").click(function () {
        $("#information").hide();
        $("#survey").show();
    });

    $("#dismiss").click(function () {
        ajaxpost($("form"), $(this).data("url"));
    });

    $("#done").click(function () {
        ajaxpost($("form"), $(this).data("url"));
    });
});