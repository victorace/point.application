﻿$(function () {
    $('input:radio[data-children!=""]').change(function () {
        var container = $("#" + $(this).data("children"));
        var val = $(this).val();
        $(container).toggle(val === "false");
        if (val === "true") {
            clearcontainer(container);
        }
    });

    $('input:radio:checked[data-children!=""]').each(function (i, e) {
        var container = $("#" + $(e).data("children"));
        container.toggle($(e).val() === "false");
    });
});