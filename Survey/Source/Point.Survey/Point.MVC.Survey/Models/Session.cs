﻿using Point.MVC.Survey.Infrastructure;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Point.MVC.Survey.Models
{
    public class Session
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SessionID { get; set; }
        [Required]
        public Guid CampaignID { get; set; }
        public virtual Campaign Campaign { get; set; }
        [Required]
        public int TransferID { get; set; }
        [Required]
        public Status Status { get; set; }
        [Required]
        public DateTime Timestamp { get; set; }
        [Required]
        public int DepartmentID { get; set; }
        [StringLength(512),Required]
        public string EmailAddress { get; set; }
        [Required]
        public string Organization { get; set; }
    }
}