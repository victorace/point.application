﻿using Point.MVC.Survey.Infrastructure;
using System;

namespace Point.MVC.Survey.Models.Campaigns
{
    public interface ICampaign
    {
        Guid CampaignID { get; set; }
        int TransferID { get; set; }
        int DepartmentID { get; set; }
        Status Status { get; set; }
    }
}