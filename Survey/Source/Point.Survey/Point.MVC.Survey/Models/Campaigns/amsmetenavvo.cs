﻿using Point.MVC.Survey.Infrastructure;
using Point.MVC.Survey.Infrastructure.Attributes;
using System;
using System.ComponentModel;

namespace Point.MVC.Survey.Models.Campaigns
{
    public class amsmetenavvo : ICampaign
    {
        public Guid CampaignID { get; set; }
        public int TransferID { get; set; }
        public int DepartmentID { get; set; }
        public Status Status { get; set; }
        public string Identifier { get; set; }

        // Aanvraag

        [DisplayName("Is het dossier/de aanmelding volledig en juist?")]
        [Answer]
        public bool AanmeldingVolledig { get; set; } = true;

        [DisplayName("Ontbrekende informatie")]
        [Answer]
        public bool AanmeldingOntbrekendeInformatie { get; set; }

        [DisplayName("Onjuiste informatie over indicatie")]
        [Answer]

        public bool AanmeldingOnjuisteInformatie { get; set; }

        [DisplayName("Tegenstrijdige informatie in het dossier")]
        [Answer]
        public bool AanmeldingTegenstrijdigeInformatie { get; set; }

        [DisplayName("Ziekenhuis heeft termijn voor uitsluitsel geven niet afgewacht en is gaan bellen")]
        [Answer]
        public bool AanmeldingZKHIsGaanBellen { get; set; }

        [DisplayName("Overig")]
        [Answer]
        public string AanmeldingOverig { get; set; }

        // Overdracht

        [DisplayName("Is de overdracht volledig en juist?")]
        [Answer]
        public bool OverdrachtVolledig { get; set; } = true;

        [DisplayName("De volgende informatie ontbreekt:")]
        public bool OverdrachtInformatieOntbreekt { get; set; }

        [DisplayName("Actueel Medicatie Overzicht (AMO)")]
        [Answer]
        public bool OverdrachtInformatieOntbreektAMO { get; set; }

        [DisplayName("Uitvoeringsverzoek")]
        [Answer]
        public bool OverdrachtInformatieOntbreektUitvoeringsverzoek { get; set; }

        [DisplayName("Specifieke voorschriften op gebied van revalidatie en/of nazorg")]
        [Answer]
        public bool OverdrachtInformatieOntbreektRevalidatieOfNazorg { get; set; }

        [DisplayName("Tegenstrijdige informatie in de overdracht of met de aanvraag")]
        [Answer]
        public bool OverdrachtTegenstrijdigeInformatie { get; set; }

        [DisplayName("Recept is niet naar apotheek gestuurd")]
        [Answer]
        public bool OverdrachtReceptNietGestuurd { get; set; }

        [DisplayName("Materialen ontbreken of zijn niet besteld")]
        [Answer]
        public bool OverdrachtMaterialenOntbreken { get; set; }

        [DisplayName("Thuiszorg is niet op de hoogte gebracht bij uitstel ontslag")]
        [Answer]
        public bool OverdrachtThuiszorgNietOpDeHoogte { get; set; }

        [DisplayName("Overig")]
        [Answer]
        public string OverdrachtOverig { get; set; }
    }
}