﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Point.MVC.Survey.Models
{
    public class Campaign
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid CampaignID {get;set;}
        [StringLength(50), Required]
        public string Name { get; set; }
        [Required]
        public bool Active { get; set; }
        [Required]
        public DateTime StartDate { get; set; }
        [Required]
        public DateTime EndDate { get; set; }
        [StringLength(50), Required]
        public string Identifier { get; set; }
        [StringLength(255), Required]
        public string EmailSubject { get; set; }
        [Required]
        public string EmailBody { get; set; }
    }
}