﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Point.MVC.Survey.Models
{
    public class Answer
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AnswerID { get; set; }
        public int SessionID { get; set; }
        public virtual Session Session { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public DateTime Timestamp { get; set; }
        public int DepartmentID { get; set; }
    }
}