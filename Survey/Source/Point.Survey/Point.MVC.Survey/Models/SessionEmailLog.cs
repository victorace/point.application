﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Point.MVC.Survey.Models
{
    public class SessionEmailLog
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SessionEmailLogID { get; set; }
        [Required]
        public int SessionID { get; set; }
        public virtual Session Session { get; set; }
        [Required]
        public DateTime Timestamp { get; set; }
    }
}