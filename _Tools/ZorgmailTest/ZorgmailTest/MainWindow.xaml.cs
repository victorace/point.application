﻿using ENovation.Lifeline.MEDVRI_2;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Xml;
using System.Xml.Serialization;
using INL = ZorgmailTest.ZorgMailSimpleWebserviceInline;
using MTOM = ZorgmailTest.ZorgMailSimpleWebserviceMtom;

namespace ZorgmailTest
{
    public partial class MainWindow : Window
    {

        private string username = "500063492";
        private string password = "J0Zc9D7B";
        private int accountId = 500063492;

        public MainWindow()
        {
            InitializeComponent();
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
        }

        void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            try
            {
                addToLog("Application Error:");
                addToLog(((Exception)e.ExceptionObject).Message);
            }
            catch {  /* ignore */ }
        }

        private INL.MailServiceClient INL_MailServiceClient()
        {
            INL.MailServiceClient msc = new INL.MailServiceClient("MailServiceINL");

            msc.ClientCredentials.UserName.UserName = username;
            msc.ClientCredentials.UserName.Password = password;

            return msc;
        }

        private MTOM.MailServiceClient MTOM_MailServiceClient()
        {
            MTOM.MailServiceClient msc = new MTOM.MailServiceClient("MailServiceMTOM");

            msc.ClientCredentials.UserName.UserName = username;
            msc.ClientCredentials.UserName.Password = password;

            return msc;
        }
        

        private void btnCount_Click(object sender, RoutedEventArgs e)
        {
            INL.MailServiceClient msc = INL_MailServiceClient();

            INL.AccountIdRequest air = new INL.AccountIdRequest();
            air.accountId = accountId.ToString();
            INL.StatResponse sr = msc.Stat(air);

            addToLog("Messagecount: " + sr.count.ToString());

            msc.Close();
        }

        private void btnSend_Click(object sender, RoutedEventArgs e)
        {

            OpenFileDialog dialog = new OpenFileDialog();

            dialog.Filter = "xml files (*.xml)|*.xml|All files (*.*)|*.*" ;
            dialog.FilterIndex = 1 ;
            dialog.RestoreDirectory = true ;


            if (dialog.ShowDialog() != true)  return;

            string timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");

            string filename = dialog.FileName;
            addToLog("Using file: " + filename);

            INL.SendMessage sm = new INL.SendMessage();
            sm.messageId = "<" + messageId(timestamp, 129880) + ">"; // "<20071211133733.000@mydomain.nl>";
            sm.sender = "500063492@lms.lifeline.nl";
            sm.recipient = txbRecipient.Text + "@lms.lifeline.nl";
            sm.subject = "Testpersoon";
            sm.contentType = "application/edifact";
            sm.content = System.IO.File.ReadAllBytes(filename);

            INL.MailServiceClient msc = INL_MailServiceClient();
            msc.Send(sm);
            msc.Close();

            addToLog("Done (?) sending: " + filename);

        }

        private void btnSendPDF_Click(object sender, RoutedEventArgs e)
        {

            string timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");

            string filename = createPdfFilename(timestamp, 129880);

            INL.SendMessage sm = new INL.SendMessage();
            sm.messageId = "<" + messageId(timestamp, 129880) + ">"; // "<20071211133733.000@mydomain.nl>";
            sm.sender = "500063492@lms.lifeline.nl";
            sm.recipient = txbRecipient.Text + "@lms.lifeline.nl";
            sm.subject = filename;
            sm.contentType = "application/pdf;name=\"" + filename.Replace(" ","+") + "\";";

            OpenFileDialog dialog = new OpenFileDialog();

            dialog.Filter = "pdf files (*.pdf)|*.pdf|All files (*.*)|*.*";
            dialog.FilterIndex = 1;
            dialog.RestoreDirectory = true;

            if (dialog.ShowDialog() != true)  return;

            string sendfile = dialog.FileName;

            byte[] testcontent = File.ReadAllBytes(sendfile);

            sm.content = testcontent; // NO CODING 18-12-12, was: new ASCIIEncoding().GetBytes(Convert.ToBase64String(testcontent));
                        
            INL.MailServiceClient msc = INL_MailServiceClient();
            msc.Send(sm);
            msc.Close();

            addToLog("Done (?) sending: " + sendfile);
            addToLog("File was send with name '" +filename + "'");
            btnCount_Click(null, null);
        }


        private void btnList_Click(object sender, RoutedEventArgs e)
        {
            INL.MailServiceClient msc = INL_MailServiceClient();

            INL.AccountIdRequest air = new INL.AccountIdRequest();
            air.accountId = accountId.ToString();

            List<INL.Message> messagelist = new List<ZorgMailSimpleWebserviceInline.Message>();
            foreach(string s in msc.List(air))
            {
                INL.UniqueIdRequest uir = new INL.UniqueIdRequest();
                uir.uniqueId = s;

                INL.Message m = msc.Retrieve(uir);
                messagelist.Add(m);
            }

            addToLog("");
            addToLog(String.Format("Retrieved {0} messages.", messagelist.Count()));

            foreach(var message in messagelist.OrderByDescending(m=>m.receiveTimestamp))
            {
                foreach (var prop in message.GetType().GetProperties())
                {
                    addToLog(String.Format("{0}: {1} ", prop.Name, prop.GetValue(message, null)));
                }
                addToLog("");
            }

            msc.Close();

        }

        private void btnRetrieve_Click(object sender, RoutedEventArgs e)
        {
            if (!checkTxbRetrieve()) return;

            INL.MailServiceClient msc = INL_MailServiceClient();

            INL.UniqueIdRequest uir = new INL.UniqueIdRequest();
            uir.uniqueId = txbRetrieve.Text.Trim();

            INL.Message m = msc.Retrieve(uir);

            foreach (var prop in m.GetType().GetProperties())
            {
                addToLog(String.Format("{0}: {1}", prop.Name, prop.GetValue(m, null)));
            }

            addToLog("DocItemCount:" + Environment.NewLine + m.documentItemCount);
            string tmp = new ASCIIEncoding().GetString(m.content);
            addToLog("Content:" + Environment.NewLine + tmp);

            msc.Close();
        }

        private void btnGetDocument_Click(object sender, RoutedEventArgs e)
        {
            if (!checkTxbRetrieve()) return;
 
            INL.MailServiceClient msc = INL_MailServiceClient();

            INL.UniqueIdRequest uir = new INL.UniqueIdRequest();
            uir.uniqueId = txbRetrieve.Text.Trim();

            INL.Message m = msc.Retrieve(uir);

            byte[] content = m.content; //NO CODING 18-12-12, was: Convert.FromBase64String(new ASCIIEncoding().GetString(m.content));
            string filename = m.headerSubject;


            SaveFileDialog dialog = new SaveFileDialog();

            dialog.Filter = "pdf files (*.pdf)|*.pdf|txt files (*.txt)|*.txt|All files (*.*)|*.*";
            dialog.FilterIndex = 1;
            dialog.RestoreDirectory = true;

            if (dialog.ShowDialog() != true) return;

            string savefile = dialog.FileName;

            FileStream fs = new FileStream(savefile,FileMode.Create, FileAccess.Write);
            fs.Write(content,0, content.Length);
            fs.Close();

            addToLog("Done writing: " + savefile);
            addToLog("Original filename: " + filename);

            msc.Close();
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            if (!checkTxbRetrieve()) return;

            INL.MailServiceClient msc = INL_MailServiceClient();

            INL.UniqueIdRequest uir = new INL.UniqueIdRequest();
            uir.uniqueId = txbRetrieve.Text.Trim();

            msc.Delete(uir);

            msc.Close();

        }

        private void btnClearInbox_Click(object sender, RoutedEventArgs e)
        {
            INL.MailServiceClient msc = INL_MailServiceClient();

            INL.AccountIdRequest air = new INL.AccountIdRequest();
            air.accountId = accountId.ToString();

            foreach (string s in msc.List(air))
            {
                INL_MailServiceClient().Delete(new INL.UniqueIdRequest() { uniqueId = s });
                addToLog("Deleted message ID: " + s);
            }
            msc.Close();

        }

        private void btnClearLog_Click(object sender, RoutedEventArgs e)
        {
            txbLog.Clear();
        }




        private bool checkTxbRetrieve()
        {
            if (String.IsNullOrWhiteSpace(txbRetrieve.Text))
            {
                addToLog("!!! Enter a messageid in the appropriate textbox (the one on the left) !!!");
                return false;
            }
            return true;
        }


        private void btnTest_Click(object sender, RoutedEventArgs e)
        {
            MEDVRI_2 mv = new MEDVRI_2();
            mv.envelope.envelopeedisyntax = ENovation.Lifeline.MEDVRI_2.envelopeEnvelopeedisyntax.EDIFACT;

            mv.afzender.afzenderadres.adresstraatnaam.Value  = "siriusdreef";
            mv.afzender.afzenderadres.adreshuisnummer.Value = "1";
            mv.afzender.afzenderadres.adresland.Value = "nederland";


            addToLog(mv.Serialize().FilterTags(new string[] { "EDI-%" }).RemoveEmptyNodes().InnerXml);


            string testMV = mv.Serialize().FilterTags(new string[] { "EDI-%" }).RemoveEmptyNodes().InnerXml;

            MEDVRI_2 mv2 = new MEDVRI_2();
            MEDVRI_2.Deserialize(testMV, out mv2);

            addToLog(mv2.afzender.afzenderadres.adreshuisnummer.Value);

            return;
        }



        private void btnDTD_Click(object sender, RoutedEventArgs e)
        {
            CheckDTD checkdtd = new CheckDTD();
            checkdtd.Show();
        }

        private void addToLog(string message)
        {
            txbLog.AppendText(message + Environment.NewLine);
        }



        static public string Base64Encode(string toEncode)
        {
            byte[] toEncodeAsBytes = System.Text.ASCIIEncoding.UTF8.GetBytes(toEncode);
            return System.Convert.ToBase64String(toEncodeAsBytes);
        }

        static public string Base64Decode(string toDecode)
        {
            byte[] encodedDataAsBytes = System.Convert.FromBase64String(toDecode);
            return System.Text.ASCIIEncoding.UTF8.GetString(encodedDataAsBytes);
        }


        static public string messageId(string timestamp, int transferId)
        {
            //Our convention is:
            //<timestamp yyyyMMddHHmmss>_<transferid>@point.nl

            return timestamp + "-" + transferId.ToString() + "@point.nl";
        }

        static public string createPdfFilename(string timestamp, int transferId)
        {
             
            //Convention b01 / b02 (???) is:
            //Filename = <convention>_<sender-name>_<recipient-name>_<recipient-customerno>_<pat-name>_<pat-birthdate>_<pat-gender>_<pat-bsn>_<doc-id>_<unique-id>.<extension>
            
            return "B02_POINT_UNIT4-Test_500060457_VAN DER TECHXX_05-12-1957_M_182937281_OverdrachtVerkort_" + timestamp + "-" + transferId.ToString() + ".pdf";
        }

        private void txbRetrieve_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ((TextBox)sender).Clear();
        }











    }



}

