﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.296
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ZorgmailTest.MailServiceInlineMock {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="http://www.enovation.nl/ems/mailwebservice", ConfigurationName="MailServiceInlineMock.MailServiceBinding")]
    public interface MailServiceBinding {
        
        // CODEGEN: Generating message contract since the operation SendRfc822 is neither RPC nor document wrapped.
        [System.ServiceModel.OperationContractAttribute(IsOneWay=true, Action="")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        void SendRfc822(ZorgmailTest.MailServiceInlineMock.SendRfc822 request);
        
        // CODEGEN: Generating message contract since the operation List is neither RPC nor document wrapped.
        [System.ServiceModel.OperationContractAttribute(Action="", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        ZorgmailTest.MailServiceInlineMock.ListResponse List(ZorgmailTest.MailServiceInlineMock.ListRequest request);
        
        // CODEGEN: Generating message contract since the operation Retrieve is neither RPC nor document wrapped.
        [System.ServiceModel.OperationContractAttribute(Action="", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        ZorgmailTest.MailServiceInlineMock.RetrieveResponse Retrieve(ZorgmailTest.MailServiceInlineMock.RetrieveRequest request);
        
        // CODEGEN: Generating message contract since the operation RetrieveRfc822 is neither RPC nor document wrapped.
        [System.ServiceModel.OperationContractAttribute(Action="", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        ZorgmailTest.MailServiceInlineMock.RetrieveRfc822Response RetrieveRfc822(ZorgmailTest.MailServiceInlineMock.RetrieveRfc822Request request);
        
        // CODEGEN: Generating message contract since the operation Delete is neither RPC nor document wrapped.
        [System.ServiceModel.OperationContractAttribute(IsOneWay=true, Action="")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        void Delete(ZorgmailTest.MailServiceInlineMock.Delete request);
        
        // CODEGEN: Generating message contract since the operation Send is neither RPC nor document wrapped.
        [System.ServiceModel.OperationContractAttribute(IsOneWay=true, Action="")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        void Send(ZorgmailTest.MailServiceInlineMock.Send request);
        
        // CODEGEN: Generating message contract since the operation Stat is neither RPC nor document wrapped.
        [System.ServiceModel.OperationContractAttribute(Action="", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        ZorgmailTest.MailServiceInlineMock.StatResponse1 Stat(ZorgmailTest.MailServiceInlineMock.StatRequest request);
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.enovation.nl/ems/mailwebservice")]
    public partial class SendRfc822Message : object, System.ComponentModel.INotifyPropertyChanged {
        
        private string senderField;
        
        private string recipientField;
        
        private byte[] contentField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public string sender {
            get {
                return this.senderField;
            }
            set {
                this.senderField = value;
                this.RaisePropertyChanged("sender");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=1)]
        public string recipient {
            get {
                return this.recipientField;
            }
            set {
                this.recipientField = value;
                this.RaisePropertyChanged("recipient");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType="base64Binary", Order=2)]
        public byte[] content {
            get {
                return this.contentField;
            }
            set {
                this.contentField = value;
                this.RaisePropertyChanged("content");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.enovation.nl/ems/mailwebservice")]
    public partial class StatResponse : object, System.ComponentModel.INotifyPropertyChanged {
        
        private int countField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public int count {
            get {
                return this.countField;
            }
            set {
                this.countField = value;
                this.RaisePropertyChanged("count");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.enovation.nl/ems/mailwebservice")]
    public partial class SendMessage : object, System.ComponentModel.INotifyPropertyChanged {
        
        private string messageIdField;
        
        private string senderField;
        
        private string recipientField;
        
        private string subjectField;
        
        private string contentTypeField;
        
        private byte[] contentField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public string messageId {
            get {
                return this.messageIdField;
            }
            set {
                this.messageIdField = value;
                this.RaisePropertyChanged("messageId");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=1)]
        public string sender {
            get {
                return this.senderField;
            }
            set {
                this.senderField = value;
                this.RaisePropertyChanged("sender");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=2)]
        public string recipient {
            get {
                return this.recipientField;
            }
            set {
                this.recipientField = value;
                this.RaisePropertyChanged("recipient");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=3)]
        public string subject {
            get {
                return this.subjectField;
            }
            set {
                this.subjectField = value;
                this.RaisePropertyChanged("subject");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=4)]
        public string contentType {
            get {
                return this.contentTypeField;
            }
            set {
                this.contentTypeField = value;
                this.RaisePropertyChanged("contentType");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType="base64Binary", Order=5)]
        public byte[] content {
            get {
                return this.contentField;
            }
            set {
                this.contentField = value;
                this.RaisePropertyChanged("content");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.enovation.nl/ems/mailwebservice")]
    public partial class Message : object, System.ComponentModel.INotifyPropertyChanged {
        
        private string messageIdField;
        
        private string sourceAccountIdField;
        
        private string sourceAddressField;
        
        private System.DateTime receiveTimestampField;
        
        private string headerMessageIdField;
        
        private string headerSubjectField;
        
        private string headerFromField;
        
        private string headerToField;
        
        private string documentFormatField;
        
        private string documentTypeField;
        
        private int documentCountField;
        
        private int documentItemCountField;
        
        private string documentIdField;
        
        private System.Nullable<System.DateTime> documentTimestampField;
        
        private string documentSenderField;
        
        private string documentRecipientField;
        
        private string documentSubjectField;
        
        private string documentTypeVersionField;
        
        private string documentSubTypeField;
        
        private string contentTypeField;
        
        private byte[] contentField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public string messageId {
            get {
                return this.messageIdField;
            }
            set {
                this.messageIdField = value;
                this.RaisePropertyChanged("messageId");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=1)]
        public string sourceAccountId {
            get {
                return this.sourceAccountIdField;
            }
            set {
                this.sourceAccountIdField = value;
                this.RaisePropertyChanged("sourceAccountId");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true, Order=2)]
        public string sourceAddress {
            get {
                return this.sourceAddressField;
            }
            set {
                this.sourceAddressField = value;
                this.RaisePropertyChanged("sourceAddress");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=3)]
        public System.DateTime receiveTimestamp {
            get {
                return this.receiveTimestampField;
            }
            set {
                this.receiveTimestampField = value;
                this.RaisePropertyChanged("receiveTimestamp");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=4)]
        public string headerMessageId {
            get {
                return this.headerMessageIdField;
            }
            set {
                this.headerMessageIdField = value;
                this.RaisePropertyChanged("headerMessageId");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true, Order=5)]
        public string headerSubject {
            get {
                return this.headerSubjectField;
            }
            set {
                this.headerSubjectField = value;
                this.RaisePropertyChanged("headerSubject");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true, Order=6)]
        public string headerFrom {
            get {
                return this.headerFromField;
            }
            set {
                this.headerFromField = value;
                this.RaisePropertyChanged("headerFrom");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true, Order=7)]
        public string headerTo {
            get {
                return this.headerToField;
            }
            set {
                this.headerToField = value;
                this.RaisePropertyChanged("headerTo");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=8)]
        public string documentFormat {
            get {
                return this.documentFormatField;
            }
            set {
                this.documentFormatField = value;
                this.RaisePropertyChanged("documentFormat");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=9)]
        public string documentType {
            get {
                return this.documentTypeField;
            }
            set {
                this.documentTypeField = value;
                this.RaisePropertyChanged("documentType");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=10)]
        public int documentCount {
            get {
                return this.documentCountField;
            }
            set {
                this.documentCountField = value;
                this.RaisePropertyChanged("documentCount");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=11)]
        public int documentItemCount {
            get {
                return this.documentItemCountField;
            }
            set {
                this.documentItemCountField = value;
                this.RaisePropertyChanged("documentItemCount");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true, Order=12)]
        public string documentId {
            get {
                return this.documentIdField;
            }
            set {
                this.documentIdField = value;
                this.RaisePropertyChanged("documentId");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true, Order=13)]
        public System.Nullable<System.DateTime> documentTimestamp {
            get {
                return this.documentTimestampField;
            }
            set {
                this.documentTimestampField = value;
                this.RaisePropertyChanged("documentTimestamp");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true, Order=14)]
        public string documentSender {
            get {
                return this.documentSenderField;
            }
            set {
                this.documentSenderField = value;
                this.RaisePropertyChanged("documentSender");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true, Order=15)]
        public string documentRecipient {
            get {
                return this.documentRecipientField;
            }
            set {
                this.documentRecipientField = value;
                this.RaisePropertyChanged("documentRecipient");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true, Order=16)]
        public string documentSubject {
            get {
                return this.documentSubjectField;
            }
            set {
                this.documentSubjectField = value;
                this.RaisePropertyChanged("documentSubject");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true, Order=17)]
        public string documentTypeVersion {
            get {
                return this.documentTypeVersionField;
            }
            set {
                this.documentTypeVersionField = value;
                this.RaisePropertyChanged("documentTypeVersion");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true, Order=18)]
        public string documentSubType {
            get {
                return this.documentSubTypeField;
            }
            set {
                this.documentSubTypeField = value;
                this.RaisePropertyChanged("documentSubType");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=19)]
        public string contentType {
            get {
                return this.contentTypeField;
            }
            set {
                this.contentTypeField = value;
                this.RaisePropertyChanged("contentType");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType="base64Binary", Order=20)]
        public byte[] content {
            get {
                return this.contentField;
            }
            set {
                this.contentField = value;
                this.RaisePropertyChanged("content");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.enovation.nl/ems/mailwebservice")]
    public partial class UniqueIdRequest : object, System.ComponentModel.INotifyPropertyChanged {
        
        private string uniqueIdField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public string uniqueId {
            get {
                return this.uniqueIdField;
            }
            set {
                this.uniqueIdField = value;
                this.RaisePropertyChanged("uniqueId");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.enovation.nl/ems/mailwebservice")]
    public partial class AccountIdRequest : object, System.ComponentModel.INotifyPropertyChanged {
        
        private string accountIdField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public string accountId {
            get {
                return this.accountIdField;
            }
            set {
                this.accountIdField = value;
                this.RaisePropertyChanged("accountId");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class SendRfc822 {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://www.enovation.nl/ems/mailwebservice", Order=0)]
        public ZorgmailTest.MailServiceInlineMock.SendRfc822Message SendRfc822Request;
        
        public SendRfc822() {
        }
        
        public SendRfc822(ZorgmailTest.MailServiceInlineMock.SendRfc822Message SendRfc822Request) {
            this.SendRfc822Request = SendRfc822Request;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class ListRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="ListRequest", Namespace="http://www.enovation.nl/ems/mailwebservice", Order=0)]
        public ZorgmailTest.MailServiceInlineMock.AccountIdRequest ListRequest1;
        
        public ListRequest() {
        }
        
        public ListRequest(ZorgmailTest.MailServiceInlineMock.AccountIdRequest ListRequest1) {
            this.ListRequest1 = ListRequest1;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class ListResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="ListResponse", Namespace="http://www.enovation.nl/ems/mailwebservice", Order=0)]
        [System.Xml.Serialization.XmlArrayItemAttribute("uniqueIds", IsNullable=false)]
        public string[] ListResponse1;
        
        public ListResponse() {
        }
        
        public ListResponse(string[] ListResponse1) {
            this.ListResponse1 = ListResponse1;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class RetrieveRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="RetrieveRequest", Namespace="http://www.enovation.nl/ems/mailwebservice", Order=0)]
        public ZorgmailTest.MailServiceInlineMock.UniqueIdRequest RetrieveRequest1;
        
        public RetrieveRequest() {
        }
        
        public RetrieveRequest(ZorgmailTest.MailServiceInlineMock.UniqueIdRequest RetrieveRequest1) {
            this.RetrieveRequest1 = RetrieveRequest1;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class RetrieveResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="RetrieveResponse", Namespace="http://www.enovation.nl/ems/mailwebservice", Order=0)]
        public ZorgmailTest.MailServiceInlineMock.Message RetrieveResponse1;
        
        public RetrieveResponse() {
        }
        
        public RetrieveResponse(ZorgmailTest.MailServiceInlineMock.Message RetrieveResponse1) {
            this.RetrieveResponse1 = RetrieveResponse1;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class RetrieveRfc822Request {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="RetrieveRfc822Request", Namespace="http://www.enovation.nl/ems/mailwebservice", Order=0)]
        public ZorgmailTest.MailServiceInlineMock.UniqueIdRequest RetrieveRfc822Request1;
        
        public RetrieveRfc822Request() {
        }
        
        public RetrieveRfc822Request(ZorgmailTest.MailServiceInlineMock.UniqueIdRequest RetrieveRfc822Request1) {
            this.RetrieveRfc822Request1 = RetrieveRfc822Request1;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class RetrieveRfc822Response {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="RetrieveRfc822Response", Namespace="http://www.enovation.nl/ems/mailwebservice", Order=0)]
        public ZorgmailTest.MailServiceInlineMock.Message RetrieveRfc822Response1;
        
        public RetrieveRfc822Response() {
        }
        
        public RetrieveRfc822Response(ZorgmailTest.MailServiceInlineMock.Message RetrieveRfc822Response1) {
            this.RetrieveRfc822Response1 = RetrieveRfc822Response1;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class Delete {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://www.enovation.nl/ems/mailwebservice", Order=0)]
        public ZorgmailTest.MailServiceInlineMock.UniqueIdRequest DeleteRequest;
        
        public Delete() {
        }
        
        public Delete(ZorgmailTest.MailServiceInlineMock.UniqueIdRequest DeleteRequest) {
            this.DeleteRequest = DeleteRequest;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class Send {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://www.enovation.nl/ems/mailwebservice", Order=0)]
        public ZorgmailTest.MailServiceInlineMock.SendMessage SendRequest;
        
        public Send() {
        }
        
        public Send(ZorgmailTest.MailServiceInlineMock.SendMessage SendRequest) {
            this.SendRequest = SendRequest;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class StatRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="StatRequest", Namespace="http://www.enovation.nl/ems/mailwebservice", Order=0)]
        public ZorgmailTest.MailServiceInlineMock.AccountIdRequest StatRequest1;
        
        public StatRequest() {
        }
        
        public StatRequest(ZorgmailTest.MailServiceInlineMock.AccountIdRequest StatRequest1) {
            this.StatRequest1 = StatRequest1;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class StatResponse1 {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://www.enovation.nl/ems/mailwebservice", Order=0)]
        public ZorgmailTest.MailServiceInlineMock.StatResponse StatResponse;
        
        public StatResponse1() {
        }
        
        public StatResponse1(ZorgmailTest.MailServiceInlineMock.StatResponse StatResponse) {
            this.StatResponse = StatResponse;
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface MailServiceBindingChannel : ZorgmailTest.MailServiceInlineMock.MailServiceBinding, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class MailServiceBindingClient : System.ServiceModel.ClientBase<ZorgmailTest.MailServiceInlineMock.MailServiceBinding>, ZorgmailTest.MailServiceInlineMock.MailServiceBinding {
        
        public MailServiceBindingClient() {
        }
        
        public MailServiceBindingClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public MailServiceBindingClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public MailServiceBindingClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public MailServiceBindingClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        void ZorgmailTest.MailServiceInlineMock.MailServiceBinding.SendRfc822(ZorgmailTest.MailServiceInlineMock.SendRfc822 request) {
            base.Channel.SendRfc822(request);
        }
        
        public void SendRfc822(ZorgmailTest.MailServiceInlineMock.SendRfc822Message SendRfc822Request) {
            ZorgmailTest.MailServiceInlineMock.SendRfc822 inValue = new ZorgmailTest.MailServiceInlineMock.SendRfc822();
            inValue.SendRfc822Request = SendRfc822Request;
            ((ZorgmailTest.MailServiceInlineMock.MailServiceBinding)(this)).SendRfc822(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        ZorgmailTest.MailServiceInlineMock.ListResponse ZorgmailTest.MailServiceInlineMock.MailServiceBinding.List(ZorgmailTest.MailServiceInlineMock.ListRequest request) {
            return base.Channel.List(request);
        }
        
        public string[] List(ZorgmailTest.MailServiceInlineMock.AccountIdRequest ListRequest1) {
            ZorgmailTest.MailServiceInlineMock.ListRequest inValue = new ZorgmailTest.MailServiceInlineMock.ListRequest();
            inValue.ListRequest1 = ListRequest1;
            ZorgmailTest.MailServiceInlineMock.ListResponse retVal = ((ZorgmailTest.MailServiceInlineMock.MailServiceBinding)(this)).List(inValue);
            return retVal.ListResponse1;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        ZorgmailTest.MailServiceInlineMock.RetrieveResponse ZorgmailTest.MailServiceInlineMock.MailServiceBinding.Retrieve(ZorgmailTest.MailServiceInlineMock.RetrieveRequest request) {
            return base.Channel.Retrieve(request);
        }
        
        public ZorgmailTest.MailServiceInlineMock.Message Retrieve(ZorgmailTest.MailServiceInlineMock.UniqueIdRequest RetrieveRequest1) {
            ZorgmailTest.MailServiceInlineMock.RetrieveRequest inValue = new ZorgmailTest.MailServiceInlineMock.RetrieveRequest();
            inValue.RetrieveRequest1 = RetrieveRequest1;
            ZorgmailTest.MailServiceInlineMock.RetrieveResponse retVal = ((ZorgmailTest.MailServiceInlineMock.MailServiceBinding)(this)).Retrieve(inValue);
            return retVal.RetrieveResponse1;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        ZorgmailTest.MailServiceInlineMock.RetrieveRfc822Response ZorgmailTest.MailServiceInlineMock.MailServiceBinding.RetrieveRfc822(ZorgmailTest.MailServiceInlineMock.RetrieveRfc822Request request) {
            return base.Channel.RetrieveRfc822(request);
        }
        
        public ZorgmailTest.MailServiceInlineMock.Message RetrieveRfc822(ZorgmailTest.MailServiceInlineMock.UniqueIdRequest RetrieveRfc822Request1) {
            ZorgmailTest.MailServiceInlineMock.RetrieveRfc822Request inValue = new ZorgmailTest.MailServiceInlineMock.RetrieveRfc822Request();
            inValue.RetrieveRfc822Request1 = RetrieveRfc822Request1;
            ZorgmailTest.MailServiceInlineMock.RetrieveRfc822Response retVal = ((ZorgmailTest.MailServiceInlineMock.MailServiceBinding)(this)).RetrieveRfc822(inValue);
            return retVal.RetrieveRfc822Response1;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        void ZorgmailTest.MailServiceInlineMock.MailServiceBinding.Delete(ZorgmailTest.MailServiceInlineMock.Delete request) {
            base.Channel.Delete(request);
        }
        
        public void Delete(ZorgmailTest.MailServiceInlineMock.UniqueIdRequest DeleteRequest) {
            ZorgmailTest.MailServiceInlineMock.Delete inValue = new ZorgmailTest.MailServiceInlineMock.Delete();
            inValue.DeleteRequest = DeleteRequest;
            ((ZorgmailTest.MailServiceInlineMock.MailServiceBinding)(this)).Delete(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        void ZorgmailTest.MailServiceInlineMock.MailServiceBinding.Send(ZorgmailTest.MailServiceInlineMock.Send request) {
            base.Channel.Send(request);
        }
        
        public void Send(ZorgmailTest.MailServiceInlineMock.SendMessage SendRequest) {
            ZorgmailTest.MailServiceInlineMock.Send inValue = new ZorgmailTest.MailServiceInlineMock.Send();
            inValue.SendRequest = SendRequest;
            ((ZorgmailTest.MailServiceInlineMock.MailServiceBinding)(this)).Send(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        ZorgmailTest.MailServiceInlineMock.StatResponse1 ZorgmailTest.MailServiceInlineMock.MailServiceBinding.Stat(ZorgmailTest.MailServiceInlineMock.StatRequest request) {
            return base.Channel.Stat(request);
        }
        
        public ZorgmailTest.MailServiceInlineMock.StatResponse Stat(ZorgmailTest.MailServiceInlineMock.AccountIdRequest StatRequest1) {
            ZorgmailTest.MailServiceInlineMock.StatRequest inValue = new ZorgmailTest.MailServiceInlineMock.StatRequest();
            inValue.StatRequest1 = StatRequest1;
            ZorgmailTest.MailServiceInlineMock.StatResponse1 retVal = ((ZorgmailTest.MailServiceInlineMock.MailServiceBinding)(this)).Stat(inValue);
            return retVal.StatResponse;
        }
    }
}
