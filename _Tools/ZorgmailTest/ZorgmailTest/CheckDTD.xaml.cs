﻿using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Xml;
using System.Xml.Schema;

namespace ZorgmailTest
{
    /// <summary>
    /// Interaction logic for CheckDTD.xaml
    /// </summary>
    public partial class CheckDTD : Window
    {

        static string xmlFile = @"c:\Users\Pascal\Documents\_Zorgmail\Test\out1.xml"; 
        static string dtdFile = @"c:\Users\Pascal\Documents\_Zorgmail\Medspe_1 voorbeeldbericht\MEDSPE_1.dtd"; 
        static string dtdFile2 = "file:///" + dtdFile.Replace(@"\", @"/");


        public CheckDTD()
        {
            InitializeComponent();
        }


        private void btnCheck_Click2(object sender, RoutedEventArgs e)
        {
            string xml;
            string dtd;

            using (StreamReader sr = new StreamReader(dtdFile))
            {
                dtd = sr.ReadToEnd();
            }

            using(StreamReader sr = new StreamReader(xmlFile))
            {
                xml = sr.ReadToEnd();
            }
            //Manipulate the doctype, before putting it into a "real" xml
            xml = Regex.Replace(xml, @"<!DOCTYPE[^>]*>", "<!DOCTYPE MEDSPE_1 [" + dtd + "]>");

            MemoryStream mems = new MemoryStream(new ASCIIEncoding().GetBytes(xml));
            //mems.Position = 0;

            XmlReaderSettings sett = new XmlReaderSettings();
            sett.DtdProcessing = DtdProcessing.Parse;
            sett.ValidationType = ValidationType.DTD;
            sett.ValidationEventHandler += (a, ex) => { addToLog(ex.Message);};

            //XmlReader validator = XmlReader.Create(xmlFileB, sett);
            XmlReader validator = XmlReader.Create(mems, sett);


            while (validator.Read()) ;

            validator.Close();
            addToLog("done");
        }

        private void btnCheck_ClickOld2(object sender, RoutedEventArgs e)
        {

            var r = new XmlTextReader(xmlFile); //new StringReader(xmlFile));
            var settings = new XmlReaderSettings();
            var sb = new StringBuilder();

            //settings.ProhibitDtd = false;
            settings.DtdProcessing = DtdProcessing.Parse;
            settings.ValidationType = ValidationType.DTD;
            settings.ValidationEventHandler += (a, ex) =>
            {
                addToLog(ex.Message);
                //ret = false;
            };

            XmlReader validator = XmlReader.Create(r, settings);
            

            while (validator.Read())
            {
            }
            validator.Close();

            addToLog("done");
        }

        private void btnCheck_Click(object sender, RoutedEventArgs e)
        {
            XmlReaderSettings sett = new XmlReaderSettings();
            sett.IgnoreProcessingInstructions = true;
            sett.DtdProcessing = DtdProcessing.Parse;
            sett.NameTable = new NameTable();


            sett.ValidationType = ValidationType.DTD;
            sett.ValidationEventHandler += new ValidationEventHandler(sett_ValidationEventHandler);
            //sett.ValidationFlags |= XmlSchemaValidationFlags.ReportValidationWarnings;

            //NameTable nt = new NameTable();
            //XmlNamespaceManager xnm = new XmlNamespaceManager(nt);

            XmlParserContext cntx = new XmlParserContext(sett.NameTable, new XmlNamespaceManager(sett.NameTable), "MEDSPE_1", "", dtdFile, "", "", "", XmlSpace.Default);
            //XmlParserContext cntx = new XmlParserContext(null, null, "MEDSPE_1", null, dtdFile2, "", "", "", XmlSpace.Default);

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(xmlFile);
            XmlDocumentType docType = xmlDoc.CreateDocumentType("MEDSPE_1", null, dtdFile2, null);
            //XmlDocumentType docTypeOrg = xmlDoc.DocumentType;
            //xmlDoc.DocumentType.RemoveAll();
            xmlDoc.InsertAfter(docType, xmlDoc.FirstChild);

            StringReader xmlString = new StringReader(xmlDoc.InnerXml);
            XmlReader xmlreader = XmlReader.Create(xmlString, sett); //, cntx);
                       
            try
            {
                while (xmlreader.Read()) { }
            }
            catch (XmlException xmlex)
            {
                addToLog(xmlex.Message);
            }
            catch (Exception ex)
            {
                addToLog(ex.Message);
            }

            xmlreader.Close();

            addToLog("done");




        }

        void sett_ValidationEventHandler(object sender, ValidationEventArgs e)
        {
            addToLog("(" + e.Exception.LineNumber + "," + e.Exception.LinePosition + ")");
            addToLog(e.Message);
        }

        void v_ValidationEventHandler(object sender, ValidationEventArgs e)
        {
            addToLog("(" + e.Exception.LineNumber + "," + e.Exception.LinePosition + ")");
            addToLog(e.Message);
        }

        private void addToLog(string message)
        {
            txbLog.AppendText(message + Environment.NewLine);
        }

    }


    public class MyResolver : System.Xml.XmlUrlResolver 
    {
        public string NewDTD = "MEDSPE_1.dtd";
        public override Uri ResolveUri(Uri baseUri, string relativeUri)
        {
            XmlUrlResolver tempRes = new XmlUrlResolver();
            if (relativeUri.EndsWith("dtd"))
                return new Uri(NewDTD);
            else
                return tempRes.ResolveUri(baseUri, relativeUri);

        }
    }

    /*
    Public Class MyResolver

    Inherits System.Xml.XmlUrlResolver

    Public NewDTD As String = "newdtd.dtd"

    Public Overrides Function ResolveUri(ByVal baseUri As Uri, ByVal relativeUri As String) As System.Uri

        Dim TempRes As New XmlUrlResolver

        If relativeUri.EndsWith("dtd") Then
            Return New Uri(NewDTD)
        Else
            Return TempRes.ResolveUri(baseUri, relativeUri)
        End If
    End Function

End Class*/

}
