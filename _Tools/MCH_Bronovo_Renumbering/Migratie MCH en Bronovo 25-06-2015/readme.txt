Migratie MCH en Bronovo 25-06-2015 productie.

Copy Client table.

Let op: Kopieer geproduceerde 'log' vanuit de messages-tab van SQL server naar een tekstbestand 
    en sla deze op. Dit bestand goed bewaren voor eventuele onderzoek.

1. Script 'Create copy Client.sql'

2. Bestanden voor omnumerring van MCH en Bronovo krijgen.

3. Controleren dat Headers goed zijn. Als deze niet goed zijn dan Headers aanpassen. 
   Correcte headers staan in 'Header for MAPPING_PATIENT_PATIENT_all.txt'.

4. Importeren naar Point

5. Uitvoeren script 'Create indexes.sql'

6. Uitvoeren 'Script renumbering Bronovo.sql'.
   
   Volgroder van uitvoeren staat in script zelf.


7. Uitvoeren 'Script renumbering MCH.sql'.

   Volgroder van uitvoeren staat in script zelf.   


8. Uitvoeren 'Script merge two hospitalst.sql'


	