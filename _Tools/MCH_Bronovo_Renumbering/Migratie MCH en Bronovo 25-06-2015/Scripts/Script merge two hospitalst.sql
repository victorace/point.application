-- Script voor samenvoegen van twee organisaties type ziekenhuis.
-- Een van twee organisaties blijft en wordt hoofdorganisatie (@organizationid).
-- Tweede organisatie wordt toegevoed aan de hoofdorganisatie (@organizationnamemerged):
-- OrganizationID van de tweede organisatie wordt gewijzigd naar OrganizationID
-- van de hoofd organisatie.

-- OrganizationID van hoofd organisatie
declare @organizationid as int
-- Naam van hoofd organisatie
declare @organizationname varchar(255)
-- Naam van organisatie die wordt samengevoegd
declare @organizationnamemerged varchar(255)
declare @locationid int
declare @locationname varchar(255)

set @organizationname = 'Medisch Centrum Haaglanden'
set @organizationnamemerged = 'Ziekenhuis Bronovo'

Print 'Aanpassing van organisatie nummer voor locaties'

if (select COUNT(*) from Organization 
where OrganizationTypeID = 1 and Organization.Name like ( '%' + @organizationname + '%') and Organization.Inactive = 0) <> 1
begin
	print 'FOUT: aantal organisaties type ziekenhuis met naam ' + ( '%' + @organizationname + '%')+ @organizationname + ' <> 1'
	return
end

select @organizationid = OrganizationID from Organization 
	where OrganizationTypeID = 1 and Organization.Name like @organizationname and Organization.Inactive = 0

declare locationCursor cursor for
	select Location.LocationID,
		Location.Name
		from Location join Organization on Organization.OrganizationID = Location.OrganizationID
		where OrganizationTypeID = 1 and Organization.Name like ( '%' + @organizationnamemerged + '%') and Organization.Inactive = 0

open locationCursor

fetch next from locationCursor into
    @locationid,
    @locationname    

while @@FETCH_STATUS = 0

begin
	update Location 
	set Location.OrganizationID = @organizationid
	where Location.LocationID = @locationid
	
	Print 'LocationID ' + str(@locationid) + ' met naam ' + @locationname + ' is gekoppeld aan organisatie ' + @organizationname + ' met OrganizationID ' + str(@organizationid)
	
	fetch next from locationCursor into
    @locationid,
    @locationname    
end

close locationCursor
deallocate locationCursor

Print 'Done!'
