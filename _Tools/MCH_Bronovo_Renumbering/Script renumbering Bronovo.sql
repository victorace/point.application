-- Script voor omnummeren van patient nummers voor Bronovo
----------------------------------
--
-- Voorbereidingen op text bestand met data voor import
-- Toevoegen velden voor header: BRONOVO_OLD;BRONOVO_NEW;BRONOVO_BSN;BRONOVO_DATE_OF_BIRTH;BRONOVO_SEX;MCH_OLD;MCH_NEW;MCH_BSN;MCH_DATE_OF_BIRTH;MCH_SEX;PREFERENCE_NUMBER
----------------------------------
--
-- Importeren:
-- 
-- 1: Importeer .txt bestand met import data naar tabel 'MAPPING_PATIENT_PATIENT_all' (indien dit bestand is nog niet geimporteerd). In .txt bestand staat data 
--    voor Bronovo en MCH.
-- 2: Pas de headers eventueel aan
-- 3: Draai script Script renumbering Bronovo.sql
-- 4: Kopieer het nu geproduceerde 'log' vanuit de messages-tab naar een tekstbestand en sla deze op
--
----------------------------------

disable trigger Client.Client_InsertUpdate on Client
go

declare @organizationname varchar(255)
declare @clientid int
declare @patientnumber as varchar(50)
declare @preferencenumber as varchar(50)

set @organizationname = 'Ziekenhuis Bronovo' 
set @preferencenumber = ''

declare clientCursor cursor for
	select Client.ClientID, 
		Client.PatientNumber from Client
		join Transfer on Client.ClientID = Transfer.ClientID 
		join Department on Transfer.DepartmentID = Department.DepartmentID
		join Location on Department.LocationID = Location.LocationID
		join Organization on Organization.OrganizationID = Location.OrganizationID
		where OrganizationTypeID = 1 and Organization.Name like ( '%' + @organizationname + '%') and Organization.Inactive = 0

open clientCursor

Print 'Patienten omnummering ' + @organizationname

fetch next from clientCursor into
    @clientid,
    @patientnumber

while @@FETCH_STATUS = 0

begin
	set @preferencenumber = ''
	select @preferencenumber = PREFERENCE_NUMBER
	from MAPPING_PATIENT_PATIENT_all
	where BRONOVO_OLD = @patientnumber

	if exists (select 1 from MAPPING_PATIENT_PATIENT_all
		where BRONOVO_OLD = @patientnumber)
		begin

			update Client set Client.PatientNumber = @preferencenumber
			where Client.ClientID = @clientid and Client.PatientNumber = @patientnumber

			Print 'Patientnumber old ' + @patientnumber + ' ' + 'Patientnumber new ' + @preferencenumber + ' ClientID ' + str(@clientID)
		end	
	
	fetch next from clientCursor into
		@clientid,
		@patientnumber
end

close clientCursor
deallocate clientCursor

Print 'Done!'
go

enable trigger Client.Client_InsertUpdate on Client

