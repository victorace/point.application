﻿namespace Point.FormService.Client.Models
{
    public class ComboboxItem
    {
        public string Text { get; set; }
        public int Value { get; set; }

        public ComboboxItem(string text, int value)
        {
            this.Text = text;
            this.Value = value;
        }
    }
}
