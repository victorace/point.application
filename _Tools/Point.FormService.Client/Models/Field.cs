﻿namespace Point.FormService.Client.Models
{
    public class Field
    {
        public string FieldValue { get; set; }
        public string FieldName { get; set; }

        public Field() { }

        public Field(string name, string value)
        {
            FieldValue = value;
            FieldName = name;
        }
    }
}
