﻿using System;
using System.ComponentModel;
using System.Reflection;

namespace Point.FormService.Client.Extensions
{
    public static class StringExtensions
    {
        /// <summary>
        /// Get substring of specified number of characters on the right.
        /// </summary>
        public static string Right(this string value, int length)
        {
            if (String.IsNullOrEmpty(value))
            {
                return string.Empty;
            }

            return value.Length <= length ? value : value.Substring(value.Length - length);
        }

        public static string UrlCombine(this string root, params string[] parts)
        {
            foreach (var part in parts)
            {
                if (root.Length > 0 && part.Length > 0)
                {
                    if (!root.EndsWith("/"))
                    {
                        root += '/';
                    }
                }
                root += part.Trim('/');
            }

            return root;
        }
    }

    public static class EnumExtensions
    {
        public static string GetDescription<TEnum>(this TEnum value)
        {
            if (value == null)
            {
                return "";
            }

            FieldInfo fi = value.GetType().GetField(value.ToString());

            if (fi != null)
            {
                DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

                if (attributes.Length > 0)
                {
                    return attributes[0].Description;
                }
            }

            return value.ToString();
        }
    }
}