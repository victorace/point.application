﻿using System.Threading;
using System.Windows.Forms;

namespace Point.FormService.Client
{
    // Run multiple winform windows simultaneously
    // https://stackoverflow.com/a/15301089

    public class MultiFormContext : ApplicationContext
    {
        private int openForms;
        public MultiFormContext(params Form[] forms)
        {
            openForms = forms.Length;

            foreach (var form in forms)
            {
                form.FormClosed += (s, args) =>
                {
                    //When we have closed the last of the "starting" forms, 
                    //end the program.
                    if (Interlocked.Decrement(ref openForms) == 0)
                    {
                        ExitThread();
                    }
                };

                form.Show();
            }
        }
    }
}