﻿using Point.FormService.Client.Extensions;
using System;
using System.Security.Cryptography;
using System.Text;

namespace Point.FormService.Client
{
    public static class HashAuthentication
    {
        public static string Create(Guid guid, int organisationid, string senderusername, DateTime senddate, string patientnumber, string hashprefix, string hashAlgorithm)
        {
            //hash: <PREFIX>_<GUID>_<OrganisationID>_<SenderUserName>_<SendDate>_<PatientNumber>
            byte[] hashBytes = Encoding.UTF8.GetBytes($"{hashprefix}_{guid}_{organisationid}_{senderusername}_{senddate.ToString("yyyyMMdd")}_{patientnumber}");

            var hashManaged = GetAlgorithmFromString(hashAlgorithm);
            byte[] computedHash = hashManaged.ComputeHash(hashBytes);

            string hashedString = string.Empty;
            foreach (byte b_loopVariable in computedHash)
            {
                byte b = b_loopVariable;
                hashedString += StringExtensions.Right("0" + b.ToString("X").ToLower(), 2);
            }

            hashedString = hashedString.ToLower();
            return hashedString;
        }

        private static HashAlgorithm GetAlgorithmFromString(string algorithm)
        {
            HashAlgorithm hashAlgorithm = new SHA256Managed();
            if (String.IsNullOrEmpty(algorithm) || algorithm.Equals("SHA1", StringComparison.InvariantCultureIgnoreCase))
            {
                hashAlgorithm = new SHA1Managed();
            }
            return hashAlgorithm;
        }
    }
}