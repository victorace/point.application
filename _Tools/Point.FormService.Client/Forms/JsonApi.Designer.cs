﻿namespace Point.FormService.Client.Forms
{
    partial class JsonApi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(JsonApi));
            this.pnlhash = new System.Windows.Forms.Panel();
            this.lblapimethodinfo = new System.Windows.Forms.Label();
            this.cbapimethods = new System.Windows.Forms.ComboBox();
            this.chbaddapi = new System.Windows.Forms.CheckBox();
            this.lblapimethod = new System.Windows.Forms.Label();
            this.cbpointsites = new System.Windows.Forms.ComboBox();
            this.lblpointsite = new System.Windows.Forms.Label();
            this.txbapikey = new System.Windows.Forms.TextBox();
            this.lblapikey = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblRequestInfo = new System.Windows.Forms.Label();
            this.btnreload = new System.Windows.Forms.Button();
            this.btnok = new System.Windows.Forms.Button();
            this.txbrequest = new System.Windows.Forms.TextBox();
            this.btnbrowse = new System.Windows.Forms.Button();
            this.txbbestandnaam = new System.Windows.Forms.TextBox();
            this.lblinputbestand = new System.Windows.Forms.Label();
            this.lblrequest = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblresponsetime = new System.Windows.Forms.Label();
            this.txbresponsestatus = new System.Windows.Forms.TextBox();
            this.txbresponsefull = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.ofd = new System.Windows.Forms.OpenFileDialog();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.pnlhash.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlhash
            // 
            this.pnlhash.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlhash.Controls.Add(this.lblapimethodinfo);
            this.pnlhash.Controls.Add(this.cbapimethods);
            this.pnlhash.Controls.Add(this.chbaddapi);
            this.pnlhash.Controls.Add(this.lblapimethod);
            this.pnlhash.Controls.Add(this.cbpointsites);
            this.pnlhash.Controls.Add(this.lblpointsite);
            this.pnlhash.Controls.Add(this.txbapikey);
            this.pnlhash.Controls.Add(this.lblapikey);
            this.pnlhash.Location = new System.Drawing.Point(12, 9);
            this.pnlhash.Name = "pnlhash";
            this.pnlhash.Size = new System.Drawing.Size(524, 110);
            this.pnlhash.TabIndex = 136;
            // 
            // lblapimethodinfo
            // 
            this.lblapimethodinfo.AutoSize = true;
            this.lblapimethodinfo.Location = new System.Drawing.Point(77, 89);
            this.lblapimethodinfo.Name = "lblapimethodinfo";
            this.lblapimethodinfo.Size = new System.Drawing.Size(35, 13);
            this.lblapimethodinfo.TabIndex = 149;
            this.lblapimethodinfo.Text = "label1";
            // 
            // cbapimethods
            // 
            this.cbapimethods.FormattingEnabled = true;
            this.cbapimethods.Location = new System.Drawing.Point(80, 65);
            this.cbapimethods.Name = "cbapimethods";
            this.cbapimethods.Size = new System.Drawing.Size(436, 21);
            this.cbapimethods.TabIndex = 148;
            // 
            // chbaddapi
            // 
            this.chbaddapi.AutoSize = true;
            this.chbaddapi.Location = new System.Drawing.Point(462, 14);
            this.chbaddapi.Name = "chbaddapi";
            this.chbaddapi.Size = new System.Drawing.Size(54, 17);
            this.chbaddapi.TabIndex = 146;
            this.chbaddapi.Text = "+ /api";
            this.chbaddapi.UseVisualStyleBackColor = true;
            // 
            // lblapimethod
            // 
            this.lblapimethod.AutoSize = true;
            this.lblapimethod.Location = new System.Drawing.Point(15, 68);
            this.lblapimethod.Name = "lblapimethod";
            this.lblapimethod.Size = new System.Drawing.Size(63, 13);
            this.lblapimethod.TabIndex = 145;
            this.lblapimethod.Text = "Api method:";
            // 
            // cbpointsites
            // 
            this.cbpointsites.FormattingEnabled = true;
            this.cbpointsites.Location = new System.Drawing.Point(80, 11);
            this.cbpointsites.Name = "cbpointsites";
            this.cbpointsites.Size = new System.Drawing.Size(376, 21);
            this.cbpointsites.TabIndex = 1;
            // 
            // lblpointsite
            // 
            this.lblpointsite.AutoSize = true;
            this.lblpointsite.Location = new System.Drawing.Point(51, 14);
            this.lblpointsite.Name = "lblpointsite";
            this.lblpointsite.Size = new System.Drawing.Size(27, 17);
            this.lblpointsite.TabIndex = 142;
            this.lblpointsite.Text = "Site:";
            this.lblpointsite.UseCompatibleTextRendering = true;
            // 
            // txbapikey
            // 
            this.txbapikey.Location = new System.Drawing.Point(80, 38);
            this.txbapikey.Name = "txbapikey";
            this.txbapikey.Size = new System.Drawing.Size(440, 20);
            this.txbapikey.TabIndex = 2;
            // 
            // lblapikey
            // 
            this.lblapikey.AutoEllipsis = true;
            this.lblapikey.AutoSize = true;
            this.lblapikey.Location = new System.Drawing.Point(33, 41);
            this.lblapikey.Name = "lblapikey";
            this.lblapikey.Size = new System.Drawing.Size(45, 13);
            this.lblapikey.TabIndex = 113;
            this.lblapikey.Text = "Api key:";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.lblRequestInfo);
            this.panel1.Controls.Add(this.btnreload);
            this.panel1.Controls.Add(this.btnok);
            this.panel1.Controls.Add(this.txbrequest);
            this.panel1.Controls.Add(this.btnbrowse);
            this.panel1.Controls.Add(this.txbbestandnaam);
            this.panel1.Controls.Add(this.lblinputbestand);
            this.panel1.Controls.Add(this.lblrequest);
            this.panel1.Location = new System.Drawing.Point(12, 121);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(524, 300);
            this.panel1.TabIndex = 146;
            // 
            // lblRequestInfo
            // 
            this.lblRequestInfo.AutoSize = true;
            this.lblRequestInfo.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.lblRequestInfo.Location = new System.Drawing.Point(38, 57);
            this.lblRequestInfo.Name = "lblRequestInfo";
            this.lblRequestInfo.Size = new System.Drawing.Size(36, 13);
            this.lblRequestInfo.TabIndex = 155;
            this.lblRequestInfo.Text = "[ info ]";
            this.toolTip1.SetToolTip(this.lblRequestInfo, resources.GetString("lblRequestInfo.ToolTip"));
            // 
            // btnreload
            // 
            this.btnreload.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnreload.Location = new System.Drawing.Point(493, 10);
            this.btnreload.Name = "btnreload";
            this.btnreload.Size = new System.Drawing.Size(27, 23);
            this.btnreload.TabIndex = 154;
            this.btnreload.Text = "(@)";
            this.btnreload.UseVisualStyleBackColor = true;
            this.btnreload.Click += new System.EventHandler(this.btnreload_Click);
            // 
            // btnok
            // 
            this.btnok.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnok.Location = new System.Drawing.Point(80, 264);
            this.btnok.Name = "btnok";
            this.btnok.Size = new System.Drawing.Size(440, 23);
            this.btnok.TabIndex = 153;
            this.btnok.Text = "Verstuur";
            this.btnok.UseVisualStyleBackColor = true;
            this.btnok.Click += new System.EventHandler(this.btnok_Click);
            // 
            // txbrequest
            // 
            this.txbrequest.AcceptsReturn = true;
            this.txbrequest.AcceptsTab = true;
            this.txbrequest.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txbrequest.Location = new System.Drawing.Point(80, 39);
            this.txbrequest.Multiline = true;
            this.txbrequest.Name = "txbrequest";
            this.txbrequest.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txbrequest.Size = new System.Drawing.Size(440, 219);
            this.txbrequest.TabIndex = 152;
            this.txbrequest.TabStop = false;
            // 
            // btnbrowse
            // 
            this.btnbrowse.Location = new System.Drawing.Point(467, 10);
            this.btnbrowse.Name = "btnbrowse";
            this.btnbrowse.Size = new System.Drawing.Size(25, 23);
            this.btnbrowse.TabIndex = 150;
            this.btnbrowse.Text = "...";
            this.btnbrowse.UseVisualStyleBackColor = true;
            this.btnbrowse.Click += new System.EventHandler(this.btnbrowse_Click);
            // 
            // txbbestandnaam
            // 
            this.txbbestandnaam.Location = new System.Drawing.Point(80, 12);
            this.txbbestandnaam.Name = "txbbestandnaam";
            this.txbbestandnaam.Size = new System.Drawing.Size(381, 20);
            this.txbbestandnaam.TabIndex = 149;
            // 
            // lblinputbestand
            // 
            this.lblinputbestand.AutoSize = true;
            this.lblinputbestand.Location = new System.Drawing.Point(6, 15);
            this.lblinputbestand.Name = "lblinputbestand";
            this.lblinputbestand.Size = new System.Drawing.Size(72, 13);
            this.lblinputbestand.TabIndex = 151;
            this.lblinputbestand.Text = "Inputbestand:";
            // 
            // lblrequest
            // 
            this.lblrequest.AutoSize = true;
            this.lblrequest.Location = new System.Drawing.Point(28, 40);
            this.lblrequest.Name = "lblrequest";
            this.lblrequest.Size = new System.Drawing.Size(50, 13);
            this.lblrequest.TabIndex = 145;
            this.lblrequest.Text = "Request:";
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.lblresponsetime);
            this.panel2.Controls.Add(this.txbresponsestatus);
            this.panel2.Controls.Add(this.txbresponsefull);
            this.panel2.Controls.Add(this.label34);
            this.panel2.Location = new System.Drawing.Point(12, 423);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(524, 219);
            this.panel2.TabIndex = 147;
            // 
            // lblresponsetime
            // 
            this.lblresponsetime.AutoSize = true;
            this.lblresponsetime.Location = new System.Drawing.Point(25, 37);
            this.lblresponsetime.Name = "lblresponsetime";
            this.lblresponsetime.Size = new System.Drawing.Size(0, 13);
            this.lblresponsetime.TabIndex = 147;
            // 
            // txbresponsestatus
            // 
            this.txbresponsestatus.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txbresponsestatus.Location = new System.Drawing.Point(80, 7);
            this.txbresponsestatus.Name = "txbresponsestatus";
            this.txbresponsestatus.Size = new System.Drawing.Size(440, 20);
            this.txbresponsestatus.TabIndex = 146;
            // 
            // txbresponsefull
            // 
            this.txbresponsefull.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txbresponsefull.Location = new System.Drawing.Point(80, 33);
            this.txbresponsefull.Multiline = true;
            this.txbresponsefull.Name = "txbresponsefull";
            this.txbresponsefull.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txbresponsefull.Size = new System.Drawing.Size(440, 186);
            this.txbresponsefull.TabIndex = 145;
            this.txbresponsefull.TabStop = false;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(20, 10);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(58, 13);
            this.label34.TabIndex = 144;
            this.label34.Text = "Response:";
            // 
            // ofd
            // 
            this.ofd.FileName = "ofd";
            this.ofd.Filter = "Json|*.json|Text|*.txt|All files|*.*";
            // 
            // JsonApi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(547, 647);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pnlhash);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "JsonApi";
            this.Text = "JsonApi";
            this.pnlhash.ResumeLayout(false);
            this.pnlhash.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlhash;
        private System.Windows.Forms.ComboBox cbpointsites;
        private System.Windows.Forms.Label lblpointsite;
        private System.Windows.Forms.TextBox txbapikey;
        private System.Windows.Forms.Label lblapikey;
        private System.Windows.Forms.Label lblapimethod;
        private System.Windows.Forms.Panel panel1;
        internal System.Windows.Forms.TextBox txbrequest;
        private System.Windows.Forms.Button btnbrowse;
        private System.Windows.Forms.TextBox txbbestandnaam;
        internal System.Windows.Forms.Label lblinputbestand;
        internal System.Windows.Forms.Label lblrequest;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox txbresponsestatus;
        internal System.Windows.Forms.TextBox txbresponsefull;
        internal System.Windows.Forms.Label label34;
        private System.Windows.Forms.CheckBox chbaddapi;
        private System.Windows.Forms.Button btnok;
        private System.Windows.Forms.OpenFileDialog ofd;
        private System.Windows.Forms.Button btnreload;
        internal System.Windows.Forms.Label lblresponsetime;
        private System.Windows.Forms.Label lblRequestInfo;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ComboBox cbapimethods;
        private System.Windows.Forms.Label lblapimethodinfo;
    }
}