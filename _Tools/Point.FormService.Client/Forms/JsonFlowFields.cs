﻿using System;
using System.IO;
using System.Windows.Forms;

namespace Point.FormService.Client.Forms
{
    public partial class JsonFlowFields : Form
    {
        private bool modified = false;

        public JsonFlowFields(string filename) : this()
        {
            load(filename);
        }

        public JsonFlowFields()
        {
            InitializeComponent();
        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (open_file_dialog.ShowDialog() == DialogResult.OK)
            {
                load(open_file_dialog.FileName);
            }
        }

        private void load(string filename)
        {
            try
            {
                using (var streamreader = new StreamReader(filename))
                {
                    json_flowfields.Text = streamreader.ReadToEnd();
                }

                status_strip.Items["status_filename"].Text = filename;
                status_strip.Refresh();

                modified = false;
                saveToolStripMenuItem.Enabled = true;
                saveasToolStripMenuItem.Enabled = true;

                ((SendPatient)Owner).json_flowfields_filename = filename;
            }
            catch { /* silent */ }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var filename = status_strip.Items["status_filename"].Text;
            saveas(filename);
        }

        private void saveasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var filename = status_strip.Items["status_filename"].Text;

            save_file_dialog.FileName = Path.GetFileName(filename);

            if (save_file_dialog.ShowDialog() == DialogResult.OK)
            {
                saveas(save_file_dialog.FileName);
            }
        }

        private void saveas(string filename)
        {
            if (!string.IsNullOrWhiteSpace(filename))
            {
                try
                {
                    json_flowfields.SaveFile(filename, RichTextBoxStreamType.PlainText);

                    modified = false;
                }
                catch { /* silent */ }

                status_strip.Items["status_filename"].Text = filename;
                status_strip.Refresh();

                ((SendPatient)Owner).json_flowfields_filename = filename;
            }
        }

        private void json_flowfields_TextChanged(object sender, EventArgs e)
        {
            var filename = status_strip.Items["status_filename"].Text;
            if (!string.IsNullOrEmpty(filename))
                saveToolStripMenuItem.Enabled = true;
            saveasToolStripMenuItem.Enabled = true;
            modified = true;
        }

        private void JsonFlowFields_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (modified)
            {
                bool newfile = false;
                var filename = status_strip.Items["status_filename"].Text;
                if (string.IsNullOrEmpty(filename))
                {
                    newfile = true;
                    filename = "Untitled";
                }
                var message = $"Do you want to save changes to {filename}?";
                var result = MessageBox.Show(message, "JSON FlowFields editor", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    if (newfile)
                    {
                        save_file_dialog.FileName = filename;
                        if (save_file_dialog.ShowDialog() == DialogResult.OK)
                        {
                            saveas(save_file_dialog.FileName);
                        }
                        else
                        {
                            e.Cancel = true;
                        }
                    }
                    else
                    {
                        saveas(filename);
                    }
                }
            }
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            status_strip.Items["status_filename"].Text = string.Empty;
            status_strip.Refresh();
            modified = false;
            json_flowfields.Clear();
            json_flowfields.ClearUndo();
            ((SendPatient)Owner).json_flowfields_filename = string.Empty;
        }
    }
}
