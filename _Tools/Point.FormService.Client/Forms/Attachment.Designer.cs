﻿namespace Point.FormService.Client
{
    partial class Attachment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Attachment));
            this.lblpatientnummer = new System.Windows.Forms.Label();
            this.lblsenderusernaam = new System.Windows.Forms.Label();
            this.lblorganizationid = new System.Windows.Forms.Label();
            this.lblsenddate = new System.Windows.Forms.Label();
            this.txtHashPrefix = new System.Windows.Forms.TextBox();
            this.btnCreateHash = new System.Windows.Forms.Button();
            this.txtHash = new System.Windows.Forms.TextBox();
            this.lblhashprefix = new System.Windows.Forms.Label();
            this.txtPatientNumber = new System.Windows.Forms.TextBox();
            this.txtSenderUserName = new System.Windows.Forms.TextBox();
            this.txtOrganisationID = new System.Windows.Forms.TextBox();
            this.txtGUID = new System.Windows.Forms.TextBox();
            this.txtSendDate = new System.Windows.Forms.TextBox();
            this.pnlhash = new System.Windows.Forms.Panel();
            this.cbpointsites = new System.Windows.Forms.ComboBox();
            this.lblpointsite = new System.Windows.Forms.Label();
            this.lblguid = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtdescription = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cbattachmenttype = new System.Windows.Forms.ComboBox();
            this.btnbrowse = new System.Windows.Forms.Button();
            this.txtbestandnaam = new System.Windows.Forms.TextBox();
            this.txtgeboortedatum = new System.Windows.Forms.TextBox();
            this.txtCivilServiceNumber = new System.Windows.Forms.TextBox();
            this.txtopnamenummer = new System.Windows.Forms.TextBox();
            this.lblbestandnaam = new System.Windows.Forms.Label();
            this.lblopnamenummer = new System.Windows.Forms.Label();
            this.lblgeboortedatum = new System.Windows.Forms.Label();
            this.lblCivilServiceNumber = new System.Windows.Forms.Label();
            this.lblattachmenttype = new System.Windows.Forms.Label();
            this.ofd = new System.Windows.Forms.OpenFileDialog();
            this.btnok = new System.Windows.Forms.Button();
            this.lblResponseTime = new System.Windows.Forms.Label();
            this.txbResponse = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.btncancel = new System.Windows.Forms.Button();
            this.label40 = new System.Windows.Forms.Label();
            this.rdbSHA256 = new System.Windows.Forms.RadioButton();
            this.rdbSHA1 = new System.Windows.Forms.RadioButton();
            this.pnlhash.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblpatientnummer
            // 
            this.lblpatientnummer.AutoSize = true;
            this.lblpatientnummer.Location = new System.Drawing.Point(20, 139);
            this.lblpatientnummer.Name = "lblpatientnummer";
            this.lblpatientnummer.Size = new System.Drawing.Size(83, 13);
            this.lblpatientnummer.TabIndex = 116;
            this.lblpatientnummer.Text = "Patient nummer:";
            // 
            // lblsenderusernaam
            // 
            this.lblsenderusernaam.AutoSize = true;
            this.lblsenderusernaam.Location = new System.Drawing.Point(9, 109);
            this.lblsenderusernaam.Name = "lblsenderusernaam";
            this.lblsenderusernaam.Size = new System.Drawing.Size(94, 13);
            this.lblsenderusernaam.TabIndex = 114;
            this.lblsenderusernaam.Text = "SenderUserName:";
            // 
            // lblorganizationid
            // 
            this.lblorganizationid.AutoSize = true;
            this.lblorganizationid.Location = new System.Drawing.Point(23, 75);
            this.lblorganizationid.Name = "lblorganizationid";
            this.lblorganizationid.Size = new System.Drawing.Size(80, 13);
            this.lblorganizationid.TabIndex = 113;
            this.lblorganizationid.Text = "OrganisationID:";
            // 
            // lblsenddate
            // 
            this.lblsenddate.AutoSize = true;
            this.lblsenddate.Location = new System.Drawing.Point(241, 109);
            this.lblsenddate.Name = "lblsenddate";
            this.lblsenddate.Size = new System.Drawing.Size(58, 13);
            this.lblsenddate.TabIndex = 115;
            this.lblsenddate.Text = "SendDate:";
            // 
            // txtHashPrefix
            // 
            this.txtHashPrefix.Location = new System.Drawing.Point(305, 139);
            this.txtHashPrefix.Name = "txtHashPrefix";
            this.txtHashPrefix.Size = new System.Drawing.Size(117, 20);
            this.txtHashPrefix.TabIndex = 5;
            this.txtHashPrefix.Text = "**HashPrefix**";
            // 
            // btnCreateHash
            // 
            this.btnCreateHash.Location = new System.Drawing.Point(428, 138);
            this.btnCreateHash.Name = "btnCreateHash";
            this.btnCreateHash.Size = new System.Drawing.Size(79, 23);
            this.btnCreateHash.TabIndex = 6;
            this.btnCreateHash.Text = "Generate";
            this.btnCreateHash.UseVisualStyleBackColor = true;
            this.btnCreateHash.Click += new System.EventHandler(this.btnCreateHash_Click);
            // 
            // txtHash
            // 
            this.txtHash.Enabled = false;
            this.txtHash.Location = new System.Drawing.Point(109, 168);
            this.txtHash.Name = "txtHash";
            this.txtHash.ReadOnly = true;
            this.txtHash.Size = new System.Drawing.Size(399, 20);
            this.txtHash.TabIndex = 127;
            this.txtHash.TabStop = false;
            // 
            // lblhashprefix
            // 
            this.lblhashprefix.AutoSize = true;
            this.lblhashprefix.Location = new System.Drawing.Point(264, 142);
            this.lblhashprefix.Name = "lblhashprefix";
            this.lblhashprefix.Size = new System.Drawing.Size(35, 13);
            this.lblhashprefix.TabIndex = 126;
            this.lblhashprefix.Text = "Hash:";
            // 
            // txtPatientNumber
            // 
            this.txtPatientNumber.Location = new System.Drawing.Point(109, 139);
            this.txtPatientNumber.Name = "txtPatientNumber";
            this.txtPatientNumber.Size = new System.Drawing.Size(126, 20);
            this.txtPatientNumber.TabIndex = 4;
            this.txtPatientNumber.Text = "0001";
            // 
            // txtSenderUserName
            // 
            this.txtSenderUserName.Location = new System.Drawing.Point(109, 106);
            this.txtSenderUserName.Name = "txtSenderUserName";
            this.txtSenderUserName.Size = new System.Drawing.Size(126, 20);
            this.txtSenderUserName.TabIndex = 3;
            this.txtSenderUserName.Text = "Test Sender";
            // 
            // txtOrganisationID
            // 
            this.txtOrganisationID.Location = new System.Drawing.Point(109, 72);
            this.txtOrganisationID.Name = "txtOrganisationID";
            this.txtOrganisationID.Size = new System.Drawing.Size(126, 20);
            this.txtOrganisationID.TabIndex = 2;
            this.txtOrganisationID.Text = "76";
            // 
            // txtGUID
            // 
            this.txtGUID.Enabled = false;
            this.txtGUID.Location = new System.Drawing.Point(108, 40);
            this.txtGUID.Name = "txtGUID";
            this.txtGUID.ReadOnly = true;
            this.txtGUID.Size = new System.Drawing.Size(399, 20);
            this.txtGUID.TabIndex = 130;
            this.txtGUID.TabStop = false;
            // 
            // txtSendDate
            // 
            this.txtSendDate.Location = new System.Drawing.Point(305, 106);
            this.txtSendDate.Name = "txtSendDate";
            this.txtSendDate.ReadOnly = true;
            this.txtSendDate.Size = new System.Drawing.Size(203, 20);
            this.txtSendDate.TabIndex = 133;
            this.txtSendDate.TabStop = false;
            // 
            // pnlhash
            // 
            this.pnlhash.Controls.Add(this.label40);
            this.pnlhash.Controls.Add(this.rdbSHA256);
            this.pnlhash.Controls.Add(this.rdbSHA1);
            this.pnlhash.Controls.Add(this.cbpointsites);
            this.pnlhash.Controls.Add(this.lblpointsite);
            this.pnlhash.Controls.Add(this.lblguid);
            this.pnlhash.Controls.Add(this.txtPatientNumber);
            this.pnlhash.Controls.Add(this.txtHash);
            this.pnlhash.Controls.Add(this.lblhashprefix);
            this.pnlhash.Controls.Add(this.btnCreateHash);
            this.pnlhash.Controls.Add(this.txtSenderUserName);
            this.pnlhash.Controls.Add(this.lblpatientnummer);
            this.pnlhash.Controls.Add(this.txtGUID);
            this.pnlhash.Controls.Add(this.txtHashPrefix);
            this.pnlhash.Controls.Add(this.lblsenddate);
            this.pnlhash.Controls.Add(this.lblsenderusernaam);
            this.pnlhash.Controls.Add(this.txtOrganisationID);
            this.pnlhash.Controls.Add(this.lblorganizationid);
            this.pnlhash.Controls.Add(this.txtSendDate);
            this.pnlhash.Location = new System.Drawing.Point(12, 14);
            this.pnlhash.Name = "pnlhash";
            this.pnlhash.Size = new System.Drawing.Size(524, 202);
            this.pnlhash.TabIndex = 135;
            // 
            // cbpointsites
            // 
            this.cbpointsites.FormattingEnabled = true;
            this.cbpointsites.Location = new System.Drawing.Point(108, 11);
            this.cbpointsites.Name = "cbpointsites";
            this.cbpointsites.Size = new System.Drawing.Size(399, 21);
            this.cbpointsites.TabIndex = 1;
            // 
            // lblpointsite
            // 
            this.lblpointsite.AutoSize = true;
            this.lblpointsite.Location = new System.Drawing.Point(75, 14);
            this.lblpointsite.Name = "lblpointsite";
            this.lblpointsite.Size = new System.Drawing.Size(27, 17);
            this.lblpointsite.TabIndex = 142;
            this.lblpointsite.Text = "Site:";
            this.lblpointsite.UseCompatibleTextRendering = true;
            // 
            // lblguid
            // 
            this.lblguid.AutoSize = true;
            this.lblguid.Location = new System.Drawing.Point(66, 43);
            this.lblguid.Name = "lblguid";
            this.lblguid.Size = new System.Drawing.Size(37, 13);
            this.lblguid.TabIndex = 136;
            this.lblguid.Text = "GUID:";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtdescription);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.cbattachmenttype);
            this.panel1.Controls.Add(this.btnbrowse);
            this.panel1.Controls.Add(this.txtbestandnaam);
            this.panel1.Controls.Add(this.txtgeboortedatum);
            this.panel1.Controls.Add(this.txtCivilServiceNumber);
            this.panel1.Controls.Add(this.txtopnamenummer);
            this.panel1.Controls.Add(this.lblbestandnaam);
            this.panel1.Controls.Add(this.lblopnamenummer);
            this.panel1.Controls.Add(this.lblgeboortedatum);
            this.panel1.Controls.Add(this.lblCivilServiceNumber);
            this.panel1.Controls.Add(this.lblattachmenttype);
            this.panel1.Location = new System.Drawing.Point(12, 222);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(524, 213);
            this.panel1.TabIndex = 136;
            // 
            // txtdescription
            // 
            this.txtdescription.Location = new System.Drawing.Point(109, 143);
            this.txtdescription.Multiline = true;
            this.txtdescription.Name = "txtdescription";
            this.txtdescription.Size = new System.Drawing.Size(399, 56);
            this.txtdescription.TabIndex = 13;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(39, 143);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 147;
            this.label1.Text = "Description:";
            // 
            // cbattachmenttype
            // 
            this.cbattachmenttype.FormattingEnabled = true;
            this.cbattachmenttype.Location = new System.Drawing.Point(109, 77);
            this.cbattachmenttype.Name = "cbattachmenttype";
            this.cbattachmenttype.Size = new System.Drawing.Size(361, 21);
            this.cbattachmenttype.TabIndex = 10;
            // 
            // btnbrowse
            // 
            this.btnbrowse.Location = new System.Drawing.Point(476, 108);
            this.btnbrowse.Name = "btnbrowse";
            this.btnbrowse.Size = new System.Drawing.Size(31, 23);
            this.btnbrowse.TabIndex = 12;
            this.btnbrowse.Text = "...";
            this.btnbrowse.UseVisualStyleBackColor = true;
            this.btnbrowse.Click += new System.EventHandler(this.btnbrowse_Click);
            // 
            // txtbestandnaam
            // 
            this.txtbestandnaam.Location = new System.Drawing.Point(109, 109);
            this.txtbestandnaam.Name = "txtbestandnaam";
            this.txtbestandnaam.Size = new System.Drawing.Size(361, 20);
            this.txtbestandnaam.TabIndex = 11;
            // 
            // txtgeboortedatum
            // 
            this.txtgeboortedatum.Location = new System.Drawing.Point(109, 45);
            this.txtgeboortedatum.Name = "txtgeboortedatum";
            this.txtgeboortedatum.Size = new System.Drawing.Size(181, 20);
            this.txtgeboortedatum.TabIndex = 9;
            // 
            // txtCivilServiceNumber
            // 
            this.txtCivilServiceNumber.Location = new System.Drawing.Point(417, 14);
            this.txtCivilServiceNumber.MaxLength = 9;
            this.txtCivilServiceNumber.Name = "txtCivilServiceNumber";
            this.txtCivilServiceNumber.Size = new System.Drawing.Size(91, 20);
            this.txtCivilServiceNumber.TabIndex = 8;
            this.txtCivilServiceNumber.Text = "111222333";
            // 
            // txtopnamenummer
            // 
            this.txtopnamenummer.Location = new System.Drawing.Point(108, 14);
            this.txtopnamenummer.Name = "txtopnamenummer";
            this.txtopnamenummer.Size = new System.Drawing.Size(182, 20);
            this.txtopnamenummer.TabIndex = 6;
            // 
            // lblbestandnaam
            // 
            this.lblbestandnaam.AutoSize = true;
            this.lblbestandnaam.Location = new System.Drawing.Point(20, 109);
            this.lblbestandnaam.Name = "lblbestandnaam";
            this.lblbestandnaam.Size = new System.Drawing.Size(80, 13);
            this.lblbestandnaam.TabIndex = 140;
            this.lblbestandnaam.Text = "Bestandsnaam:";
            // 
            // lblopnamenummer
            // 
            this.lblopnamenummer.AutoSize = true;
            this.lblopnamenummer.Location = new System.Drawing.Point(12, 17);
            this.lblopnamenummer.Name = "lblopnamenummer";
            this.lblopnamenummer.Size = new System.Drawing.Size(90, 13);
            this.lblopnamenummer.TabIndex = 137;
            this.lblopnamenummer.Text = "Opname nummer:";
            // 
            // lblgeboortedatum
            // 
            this.lblgeboortedatum.AutoSize = true;
            this.lblgeboortedatum.Location = new System.Drawing.Point(20, 50);
            this.lblgeboortedatum.Name = "lblgeboortedatum";
            this.lblgeboortedatum.Size = new System.Drawing.Size(83, 13);
            this.lblgeboortedatum.TabIndex = 138;
            this.lblgeboortedatum.Text = "Geboortedatum:";
            // 
            // lblCivilServiceNumber
            // 
            this.lblCivilServiceNumber.AutoSize = true;
            this.lblCivilServiceNumber.Location = new System.Drawing.Point(302, 17);
            this.lblCivilServiceNumber.Name = "lblCivilServiceNumber";
            this.lblCivilServiceNumber.Size = new System.Drawing.Size(102, 13);
            this.lblCivilServiceNumber.TabIndex = 137;
            this.lblCivilServiceNumber.Text = "CivilServiceNumber:";
            // 
            // lblattachmenttype
            // 
            this.lblattachmenttype.AutoSize = true;
            this.lblattachmenttype.Location = new System.Drawing.Point(18, 80);
            this.lblattachmenttype.Name = "lblattachmenttype";
            this.lblattachmenttype.Size = new System.Drawing.Size(84, 13);
            this.lblattachmenttype.TabIndex = 139;
            this.lblattachmenttype.Text = "Type document:";
            // 
            // ofd
            // 
            this.ofd.FileName = "ofd";
            this.ofd.Filter = "All files|*.*";
            // 
            // btnok
            // 
            this.btnok.Location = new System.Drawing.Point(461, 530);
            this.btnok.Name = "btnok";
            this.btnok.Size = new System.Drawing.Size(75, 23);
            this.btnok.TabIndex = 14;
            this.btnok.Text = "OK";
            this.btnok.UseVisualStyleBackColor = true;
            this.btnok.Click += new System.EventHandler(this.btnok_Click);
            // 
            // lblResponseTime
            // 
            this.lblResponseTime.AutoSize = true;
            this.lblResponseTime.Location = new System.Drawing.Point(57, 475);
            this.lblResponseTime.Name = "lblResponseTime";
            this.lblResponseTime.Size = new System.Drawing.Size(0, 13);
            this.lblResponseTime.TabIndex = 140;
            // 
            // txbResponse
            // 
            this.txbResponse.Location = new System.Drawing.Point(121, 445);
            this.txbResponse.Multiline = true;
            this.txbResponse.Name = "txbResponse";
            this.txbResponse.Size = new System.Drawing.Size(399, 66);
            this.txbResponse.TabIndex = 139;
            this.txbResponse.TabStop = false;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(57, 448);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(58, 13);
            this.label34.TabIndex = 138;
            this.label34.Text = "Response:";
            // 
            // btncancel
            // 
            this.btncancel.Location = new System.Drawing.Point(380, 530);
            this.btncancel.Name = "btncancel";
            this.btncancel.Size = new System.Drawing.Size(75, 23);
            this.btncancel.TabIndex = 15;
            this.btncancel.Text = "Cancel";
            this.btncancel.UseVisualStyleBackColor = true;
            this.btncancel.Click += new System.EventHandler(this.btncancel_Click);
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(244, 75);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(58, 13);
            this.label40.TabIndex = 145;
            this.label40.Text = "Hash type:";
            // 
            // rdbSHA256
            // 
            this.rdbSHA256.AutoSize = true;
            this.rdbSHA256.Location = new System.Drawing.Point(441, 72);
            this.rdbSHA256.Name = "rdbSHA256";
            this.rdbSHA256.Size = new System.Drawing.Size(65, 17);
            this.rdbSHA256.TabIndex = 144;
            this.rdbSHA256.TabStop = true;
            this.rdbSHA256.Text = "SHA256";
            this.rdbSHA256.UseVisualStyleBackColor = true;
            // 
            // rdbSHA1
            // 
            this.rdbSHA1.AutoSize = true;
            this.rdbSHA1.Location = new System.Drawing.Point(307, 72);
            this.rdbSHA1.Name = "rdbSHA1";
            this.rdbSHA1.Size = new System.Drawing.Size(53, 17);
            this.rdbSHA1.TabIndex = 143;
            this.rdbSHA1.TabStop = true;
            this.rdbSHA1.Text = "SHA1";
            this.rdbSHA1.UseVisualStyleBackColor = true;
            // 
            // Attachment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(552, 564);
            this.Controls.Add(this.btncancel);
            this.Controls.Add(this.lblResponseTime);
            this.Controls.Add(this.txbResponse);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.btnok);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pnlhash);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Attachment";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Attachment";
            this.pnlhash.ResumeLayout(false);
            this.pnlhash.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        internal System.Windows.Forms.Label lblpatientnummer;
        private System.Windows.Forms.Label lblsenderusernaam;
        private System.Windows.Forms.Label lblorganizationid;
        private System.Windows.Forms.Label lblsenddate;
        internal System.Windows.Forms.TextBox txtHashPrefix;
        private System.Windows.Forms.Button btnCreateHash;
        private System.Windows.Forms.TextBox txtHash;
        private System.Windows.Forms.Label lblhashprefix;
        internal System.Windows.Forms.TextBox txtPatientNumber;
        private System.Windows.Forms.TextBox txtSenderUserName;
        private System.Windows.Forms.TextBox txtOrganisationID;
        private System.Windows.Forms.TextBox txtGUID;
        private System.Windows.Forms.TextBox txtSendDate;
        private System.Windows.Forms.Panel pnlhash;
        private System.Windows.Forms.Label lblguid;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtgeboortedatum;
        private System.Windows.Forms.TextBox txtCivilServiceNumber;
        private System.Windows.Forms.TextBox txtopnamenummer;
        internal System.Windows.Forms.Label lblbestandnaam;
        private System.Windows.Forms.Label lblopnamenummer;
        private System.Windows.Forms.Label lblgeboortedatum;
        private System.Windows.Forms.Label lblCivilServiceNumber;
        private System.Windows.Forms.Label lblattachmenttype;
        private System.Windows.Forms.TextBox txtbestandnaam;
        private System.Windows.Forms.OpenFileDialog ofd;
        private System.Windows.Forms.Button btnbrowse;
        private System.Windows.Forms.Button btnok;
        private System.Windows.Forms.ComboBox cbattachmenttype;
        internal System.Windows.Forms.Label lblResponseTime;
        internal System.Windows.Forms.TextBox txbResponse;
        internal System.Windows.Forms.Label label34;
        private System.Windows.Forms.ComboBox cbpointsites;
        private System.Windows.Forms.Label lblpointsite;
        private System.Windows.Forms.TextBox txtdescription;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btncancel;
        internal System.Windows.Forms.Label label40;
        private System.Windows.Forms.RadioButton rdbSHA256;
        private System.Windows.Forms.RadioButton rdbSHA1;
    }
}