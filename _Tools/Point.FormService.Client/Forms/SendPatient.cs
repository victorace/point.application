﻿using Point.FormService.Client.Constants;
using Point.FormService.Client.Forms;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace Point.FormService.Client
{
    public partial class SendPatient : Form
    {
        private class FlowField
        {
            public string Name { get; set; }
            public string Value { get; set; }

        }
        private class Root
        {
            public List<FlowField> FlowFields { get; set; }
        }

        public string json_flowfields_filename = string.Empty;

        public SendPatient()
        {
            InitializeComponent();

            if (!string.IsNullOrEmpty(Environment.UserName))
                txtSenderUserName.Text = Environment.UserName; 

            randomdates();
            getdefaulthash();
            getdefaultorganizationid();
            pointsites();
        }

        private void randomdates()
        {
            var birthdate = Utils.RandomDateInRange(new DateTime(1910, 1, 1), DateTime.Now);
            txtBirthDate.Text = birthdate.ToString("dd-MM-yyyy");

            var contactpersonbirthdate = Utils.RandomDateInRange(new DateTime(1910, 1, 1), DateTime.Now);
            txtContactPersonBirthDate.Text = contactpersonbirthdate.ToString("dd-MM-yyyy");
        }

        private void getdefaulthash()
        {
            txtHashPrefix.Text = Config.GetAppSetting("HashPrefix");
        }

        private void getdefaultorganizationid()
        {
            txtOrganisationID.Text = Config.GetAppSetting("OrganizationID");
        }

        private void pointsites()
        {
            cbpointsites.DataSource = Config.GetAppSettingList("PointSite");
            int selected = 1;
            Int32.TryParse(Config.GetAppSetting("DefaultPointSite"), out selected);
            cbpointsites.SelectedIndex = (selected - 1);
        }

        private formfieldsservice.Field serviceField(string fieldname, string fieldvalue)
        {
            formfieldsservice.Field field = new formfieldsservice.Field();
            field.FieldName = fieldname;
            field.FieldValue = fieldvalue;
            return field;
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            txbResponse.Text = "";
            lblResponseTime.Text = "";

            var formfieldssoapclient = new formfieldsservice.FormFieldsSoapClient();
            if (cbpointsites.SelectedValue != null && !string.IsNullOrWhiteSpace(cbpointsites.SelectedValue.ToString()))
            {
                var uri = Utils.BuildPointUrl(cbpointsites.SelectedValue.ToString(), formfieldssoapclient.Endpoint.Address);
                formfieldssoapclient.Endpoint.Address = new System.ServiceModel.EndpointAddress(uri);
            }

            var guid = System.Guid.NewGuid();
            var organizationid = Convert.ToInt32(txtOrganisationID.Text);
            var senderusername = txtSenderUserName.Text;
            var senddate = DateTime.Now;
            var patientnumber = txtPatientNumber.Text;
            var hashAlgorithmType = rdbSHA256.Checked ? HashType.SHA256 : HashType.SHA1;
            var hash = HashAuthentication.Create(guid, organizationid, senderusername, senddate, patientnumber, txtHashPrefix.Text, hashAlgorithmType);

            //var guid = System.Guid.NewGuid();
            //organizationid = 11557;
            //senderusername = "docrmst";
            //senddate = DateTime.Parse("2019-02-27T15:26:44");
            //patientnumber = "00073019";
            //var prefix = "qk@Xt4qXS.H";
            //var hash = HashAuthentication.Create(guid, organizationid, senderusername, senddate, patientnumber, prefix);
            var patient = new formfieldsservice.Patient();

            patient.CivilServiceNumber = txtCivilServiceNumber.Text;
            patient.Gender = txtGender.Text;
            patient.Salutation = txtSalutation.Text;
            patient.Initials = txtInitials.Text;
            patient.FirstName = txtFirstName.Text;
            patient.MiddleName = txtMiddelName.Text;
            patient.LastName = txtLastName.Text;
            patient.MaidenName = txtMaidenName.Text;
            patient.BirthDate = Utils.ParseDate(txtBirthDate.Text);
            patient.StreetName = txtStreetName.Text;
            patient.Number = txtNumber.Text;
            patient.PostalCode = txtPostalCode.Text;
            patient.City = txtCity.Text;
            patient.PhoneNumberGeneral = txtPhoneNumberGeneral.Text;
            patient.PhoneNumberMobile = txtPhoneNumberMobile.Text;
            patient.PhoneNumberWork = txtPhoneNumberWork.Text;
            patient.ContactPersonName = txtContactPersonName.Text;
            patient.ContactPersonRelationType = txtContactPersonRelationType.Text;
            patient.ContactPersonBirthDate = Utils.ParseDate(txtContactPersonBirthDate.Text);
            patient.ContactPersonPhoneNumberGeneral = txtContactPersonPhoneNumberGeneral.Text;
            patient.ContactPersonPhoneNumberMobile = txtContactPersonPhoneNumberMobile.Text;
            patient.ContactPersonPhoneNumberWork = txtContactPersonPhoneNumberWork.Text;
            patient.HealthInsuranceCompanyUZOVICode = Convert.ToInt32(txtHealthInsuranceCompanyUZOVICode.Text);
            patient.InsuranceNumber = txtInsuranceNumber.Text;
            patient.GeneralPractitionerName = txtGeneralPractitionerName.Text;
            patient.GeneralPractitionerPhoneNumber = txtGeneralPractitionerPhoneNumber.Text;
            patient.PharmacyName = txtPharmacyName.Text;
            patient.PharmacyPhoneNumber = txtPharmacyPhoneNumber.Text;
            patient.CivilClass = txtCivilClass.Text;
            patient.CompositionHousekeeping = txtCompositionHousekeeping.Text;
            if (txtChildrenInHousekeeping.Text != "")
            {
                patient.ChildrenInHousekeeping = Convert.ToInt32(txtChildrenInHousekeeping.Text);
            }            
            patient.HousingType = txtHousingType.Text;
            patient.AddressGPForZorgmail = txtAddressGPForZorgmail.Text;
            patient.VisitNumber = txtVisitNumber.Text;

            var response = "";
            try
            {
                if (cbCreate.Checked)
                {
                    response = formfieldssoapclient.AddAndCreateTransferData(guid, organizationid, senderusername, senddate, patientnumber, hash, patient);
                }
                else
                {
                    response = formfieldssoapclient.AddTransferData(guid, organizationid, senderusername, senddate, patientnumber, hash, patient);
                }
            }
            catch (Exception ex)
            {
                response = ex.Message;
            }

            txtGUID.Text = guid.ToString();
            txtHash.Text = hash;
            txtSendDate.Text = senddate.ToString("dd-MM-yyyy HH:mm:ss");
            txbResponse.Text = response;
            lblResponseTime.Text = DateTime.Now.ToString("HH:mm:ss");
        }

        private void btnSendFormulier_Click(object sender, EventArgs e)
        {
            txbResponse.Text = "";
            lblResponseTime.Text = "";

            var formfieldssoapclient = new formfieldsservice.FormFieldsSoapClient();
            if (cbpointsites.SelectedValue != null && !string.IsNullOrWhiteSpace(cbpointsites.SelectedValue.ToString()))
            {
                var uri = Utils.BuildPointUrl(cbpointsites.SelectedValue.ToString(), formfieldssoapclient.Endpoint.Address);
                formfieldssoapclient.Endpoint.Address = new System.ServiceModel.EndpointAddress(uri);
            }

            var guid = System.Guid.NewGuid();

            var organizationid = Convert.ToInt32(txtOrganisationID.Text);

            txtGUID.Text = guid.ToString();

            var senderusername = txtSenderUserName.Text;

            var senddate = DateTime.Now;

            txtSendDate.Text = senddate.ToString("dd-MM-yyyy HH:mm:ss");

            var patientnumber = txtPatientNumber.Text;
            var visitnumber = txtVisitNumber.Text;
            var hashAlgorithmType = rdbSHA256.Checked ? HashType.SHA256 : HashType.SHA1;
            var hash = HashAuthentication.Create(guid, organizationid, senderusername, senddate, patientnumber, txtHashPrefix.Text, hashAlgorithmType);
            txtHash.Text = hash;

            var fields = new List<formfieldsservice.Field>();

            if (!string.IsNullOrEmpty(json_flowfields_filename))
            {
                try
                {
                    using (var streamreader = new StreamReader(json_flowfields_filename))
                    {
                        var json_flowfields = streamreader.ReadToEnd();
                        var data = Newtonsoft.Json.JsonConvert.DeserializeObject<Root>(json_flowfields);
                        foreach (var flowfield in data.FlowFields)
                            fields.Add(serviceField(flowfield.Name, flowfield.Value));
                    }
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }

            if (!fields.Any())
            {
                if (MessageBox.Show(this, 
                    $"No external FlowField data loaded. Using a very limited internal set instead.{Environment.NewLine}{Environment.NewLine}Do you wish to continue?", 
                    "Verstuur aanvraag formulier",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    return; 

                foreach (var field in new SampleData().Sample())
                    fields.Add(serviceField(field.FieldName, field.FieldValue));
            }

            var response = "";
            try
            {
                response = formfieldssoapclient.PointFormFields(guid, organizationid, senderusername, senddate, patientnumber, visitnumber, hash, fields.ToArray<formfieldsservice.Field>());
            }
            catch (Exception ex)
            {
                response = ex.Message;
            }

            txbResponse.Text = response;
            lblResponseTime.Text = DateTime.Now.ToString("HH:mm:ss");
        }

        private void btnCreateHash_Click(object sender, EventArgs e)
        {
            var guid = System.Guid.NewGuid();

            txtGUID.Text = guid.ToString();

            var senddate = DateTime.Now;

            txtSendDate.Text = senddate.ToString("dd-MM-yyyy HH:mm:ss");

            var hashAlgorithmType = rdbSHA256.Checked ? HashType.SHA256 : HashType.SHA1;

            txtHash.Text = HashAuthentication.Create(guid, Int32.Parse(txtOrganisationID.Text), txtSenderUserName.Text, senddate, txtPatientNumber.Text, txtHashPrefix.Text, hashAlgorithmType);
        }

        private void btnFlowFields_Click(object sender, EventArgs e)
        {
            using (var jsonFlowFields = new JsonFlowFields(json_flowfields_filename))
                jsonFlowFields.ShowDialog(this);
        }
    }
}
