﻿namespace Point.FormService.Client
{
    partial class SendPatient
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SendPatient));
            this.txtGUID = new System.Windows.Forms.TextBox();
            this.OrganisationID = new System.Windows.Forms.Label();
            this.txtOrganisationID = new System.Windows.Forms.TextBox();
            this.txtSenderUserName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtSendDate = new System.Windows.Forms.TextBox();
            this.txtHash = new System.Windows.Forms.TextBox();
            this.txtCivilServiceNumber = new System.Windows.Forms.TextBox();
            this.txtGender = new System.Windows.Forms.TextBox();
            this.txtSalutation = new System.Windows.Forms.TextBox();
            this.txtFirstName = new System.Windows.Forms.TextBox();
            this.txtMiddelName = new System.Windows.Forms.TextBox();
            this.txtLastName = new System.Windows.Forms.TextBox();
            this.txtMaidenName = new System.Windows.Forms.TextBox();
            this.txtBirthDate = new System.Windows.Forms.TextBox();
            this.txtStreetName = new System.Windows.Forms.TextBox();
            this.txtNumber = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtPostalCode = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txtCity = new System.Windows.Forms.TextBox();
            this.txtPhoneNumberGeneral = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.txtPhoneNumberMobile = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.txtPhoneNumberWork = new System.Windows.Forms.TextBox();
            this.txtContactPersonName = new System.Windows.Forms.TextBox();
            this.txtContactPersonRelationType = new System.Windows.Forms.TextBox();
            this.txtContactPersonBirthDate = new System.Windows.Forms.TextBox();
            this.txtContactPersonPhoneNumberGeneral = new System.Windows.Forms.TextBox();
            this.txtContactPersonPhoneNumberMobile = new System.Windows.Forms.TextBox();
            this.txtContactPersonPhoneNumberWork = new System.Windows.Forms.TextBox();
            this.txtHealthInsuranceCompanyUZOVICode = new System.Windows.Forms.TextBox();
            this.txtInsuranceNumber = new System.Windows.Forms.TextBox();
            this.txtGeneralPractitionerName = new System.Windows.Forms.TextBox();
            this.txtGeneralPractitionerPhoneNumber = new System.Windows.Forms.TextBox();
            this.txtCivilClass = new System.Windows.Forms.TextBox();
            this.txtCompositionHousekeeping = new System.Windows.Forms.TextBox();
            this.txtChildrenInHousekeeping = new System.Windows.Forms.TextBox();
            this.Label38 = new System.Windows.Forms.Label();
            this.btnSend = new System.Windows.Forms.Button();
            this.txtHousingType = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.txtPatientNumber = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.txtInitials = new System.Windows.Forms.TextBox();
            this.btnSendFormulier = new System.Windows.Forms.Button();
            this.label34 = new System.Windows.Forms.Label();
            this.txbResponse = new System.Windows.Forms.TextBox();
            this.lblResponseTime = new System.Windows.Forms.Label();
            this.txtAddressGPForZorgmail = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.btnCreateHash = new System.Windows.Forms.Button();
            this.txtHashPrefix = new System.Windows.Forms.TextBox();
            this.txtVisitNumber = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.cbpointsites = new System.Windows.Forms.ComboBox();
            this.lblpointsite = new System.Windows.Forms.Label();
            this.txtPharmacyName = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.txtPharmacyPhoneNumber = new System.Windows.Forms.TextBox();
            this.lblPharmacyPhoneNumber = new System.Windows.Forms.Label();
            this.cbCreate = new System.Windows.Forms.CheckBox();
            this.rdbSHA1 = new System.Windows.Forms.RadioButton();
            this.rdbSHA256 = new System.Windows.Forms.RadioButton();
            this.label40 = new System.Windows.Forms.Label();
            this.btnFlowFields = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtGUID
            // 
            this.txtGUID.Location = new System.Drawing.Point(193, 39);
            this.txtGUID.Margin = new System.Windows.Forms.Padding(4);
            this.txtGUID.Name = "txtGUID";
            this.txtGUID.ReadOnly = true;
            this.txtGUID.Size = new System.Drawing.Size(393, 22);
            this.txtGUID.TabIndex = 1;
            this.txtGUID.TabStop = false;
            // 
            // OrganisationID
            // 
            this.OrganisationID.AutoSize = true;
            this.OrganisationID.Location = new System.Drawing.Point(8, 75);
            this.OrganisationID.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.OrganisationID.Name = "OrganisationID";
            this.OrganisationID.Size = new System.Drawing.Size(102, 17);
            this.OrganisationID.TabIndex = 2;
            this.OrganisationID.Text = "OrganisationID";
            // 
            // txtOrganisationID
            // 
            this.txtOrganisationID.Location = new System.Drawing.Point(193, 71);
            this.txtOrganisationID.Margin = new System.Windows.Forms.Padding(4);
            this.txtOrganisationID.Name = "txtOrganisationID";
            this.txtOrganisationID.Size = new System.Drawing.Size(393, 22);
            this.txtOrganisationID.TabIndex = 2;
            this.txtOrganisationID.Text = "76";
            // 
            // txtSenderUserName
            // 
            this.txtSenderUserName.Location = new System.Drawing.Point(193, 103);
            this.txtSenderUserName.Margin = new System.Windows.Forms.Padding(4);
            this.txtSenderUserName.Name = "txtSenderUserName";
            this.txtSenderUserName.Size = new System.Drawing.Size(393, 22);
            this.txtSenderUserName.TabIndex = 3;
            this.txtSenderUserName.Text = "Test Sender";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 107);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(121, 17);
            this.label2.TabIndex = 6;
            this.label2.Text = "SenderUserName";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 139);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 17);
            this.label3.TabIndex = 7;
            this.label3.Text = "SendDate";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 241);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 17);
            this.label4.TabIndex = 8;
            this.label4.Text = "Hash";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 368);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(130, 17);
            this.label5.TabIndex = 9;
            this.label5.Text = "CivilServiceNumber";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 400);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 17);
            this.label6.TabIndex = 10;
            this.label6.Text = "Gender";
            // 
            // txtSendDate
            // 
            this.txtSendDate.Location = new System.Drawing.Point(193, 135);
            this.txtSendDate.Margin = new System.Windows.Forms.Padding(4);
            this.txtSendDate.Name = "txtSendDate";
            this.txtSendDate.ReadOnly = true;
            this.txtSendDate.Size = new System.Drawing.Size(393, 22);
            this.txtSendDate.TabIndex = 4;
            // 
            // txtHash
            // 
            this.txtHash.Enabled = false;
            this.txtHash.Location = new System.Drawing.Point(193, 271);
            this.txtHash.Margin = new System.Windows.Forms.Padding(4);
            this.txtHash.Name = "txtHash";
            this.txtHash.ReadOnly = true;
            this.txtHash.Size = new System.Drawing.Size(393, 22);
            this.txtHash.TabIndex = 8;
            this.txtHash.TabStop = false;
            // 
            // txtCivilServiceNumber
            // 
            this.txtCivilServiceNumber.Location = new System.Drawing.Point(193, 364);
            this.txtCivilServiceNumber.Margin = new System.Windows.Forms.Padding(4);
            this.txtCivilServiceNumber.MaxLength = 9;
            this.txtCivilServiceNumber.Name = "txtCivilServiceNumber";
            this.txtCivilServiceNumber.Size = new System.Drawing.Size(393, 22);
            this.txtCivilServiceNumber.TabIndex = 10;
            this.txtCivilServiceNumber.Text = "111222333";
            // 
            // txtGender
            // 
            this.txtGender.Location = new System.Drawing.Point(193, 396);
            this.txtGender.Margin = new System.Windows.Forms.Padding(4);
            this.txtGender.Name = "txtGender";
            this.txtGender.Size = new System.Drawing.Size(393, 22);
            this.txtGender.TabIndex = 11;
            this.txtGender.Text = "m";
            // 
            // txtSalutation
            // 
            this.txtSalutation.Location = new System.Drawing.Point(193, 428);
            this.txtSalutation.Margin = new System.Windows.Forms.Padding(4);
            this.txtSalutation.Name = "txtSalutation";
            this.txtSalutation.Size = new System.Drawing.Size(393, 22);
            this.txtSalutation.TabIndex = 12;
            this.txtSalutation.Text = "de heer";
            // 
            // txtFirstName
            // 
            this.txtFirstName.Location = new System.Drawing.Point(193, 492);
            this.txtFirstName.Margin = new System.Windows.Forms.Padding(4);
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.Size = new System.Drawing.Size(393, 22);
            this.txtFirstName.TabIndex = 14;
            this.txtFirstName.Text = "FirstName";
            // 
            // txtMiddelName
            // 
            this.txtMiddelName.Location = new System.Drawing.Point(193, 524);
            this.txtMiddelName.Margin = new System.Windows.Forms.Padding(4);
            this.txtMiddelName.Name = "txtMiddelName";
            this.txtMiddelName.Size = new System.Drawing.Size(393, 22);
            this.txtMiddelName.TabIndex = 15;
            this.txtMiddelName.Text = "MiddleName";
            // 
            // txtLastName
            // 
            this.txtLastName.Location = new System.Drawing.Point(193, 556);
            this.txtLastName.Margin = new System.Windows.Forms.Padding(4);
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.Size = new System.Drawing.Size(393, 22);
            this.txtLastName.TabIndex = 16;
            this.txtLastName.Text = "LastName";
            // 
            // txtMaidenName
            // 
            this.txtMaidenName.Location = new System.Drawing.Point(193, 588);
            this.txtMaidenName.Margin = new System.Windows.Forms.Padding(4);
            this.txtMaidenName.Name = "txtMaidenName";
            this.txtMaidenName.Size = new System.Drawing.Size(393, 22);
            this.txtMaidenName.TabIndex = 17;
            this.txtMaidenName.Text = "MaidenName";
            // 
            // txtBirthDate
            // 
            this.txtBirthDate.Location = new System.Drawing.Point(193, 620);
            this.txtBirthDate.Margin = new System.Windows.Forms.Padding(4);
            this.txtBirthDate.Name = "txtBirthDate";
            this.txtBirthDate.Size = new System.Drawing.Size(393, 22);
            this.txtBirthDate.TabIndex = 18;
            this.txtBirthDate.Text = "BirthDate";
            // 
            // txtStreetName
            // 
            this.txtStreetName.Location = new System.Drawing.Point(193, 820);
            this.txtStreetName.Margin = new System.Windows.Forms.Padding(4);
            this.txtStreetName.Name = "txtStreetName";
            this.txtStreetName.Size = new System.Drawing.Size(393, 22);
            this.txtStreetName.TabIndex = 23;
            this.txtStreetName.Text = "StreetName";
            // 
            // txtNumber
            // 
            this.txtNumber.Location = new System.Drawing.Point(193, 852);
            this.txtNumber.Margin = new System.Windows.Forms.Padding(4);
            this.txtNumber.Name = "txtNumber";
            this.txtNumber.Size = new System.Drawing.Size(393, 22);
            this.txtNumber.TabIndex = 24;
            this.txtNumber.Text = "456";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 432);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(71, 17);
            this.label7.TabIndex = 23;
            this.label7.Text = "Salutation";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(8, 496);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(72, 17);
            this.label8.TabIndex = 24;
            this.label8.Text = "FirstName";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(8, 528);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(86, 17);
            this.label9.TabIndex = 25;
            this.label9.Text = "MiddleName";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(8, 560);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(72, 17);
            this.label10.TabIndex = 26;
            this.label10.Text = "LastName";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(8, 592);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(91, 17);
            this.label11.TabIndex = 27;
            this.label11.Text = "MaidenName";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(8, 624);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(67, 17);
            this.label12.TabIndex = 28;
            this.label12.Text = "BirthDate";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(8, 823);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(83, 17);
            this.label13.TabIndex = 29;
            this.label13.Text = "StreetName";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(8, 855);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(58, 17);
            this.label14.TabIndex = 30;
            this.label14.Text = "Number";
            // 
            // txtPostalCode
            // 
            this.txtPostalCode.Location = new System.Drawing.Point(193, 884);
            this.txtPostalCode.Margin = new System.Windows.Forms.Padding(4);
            this.txtPostalCode.Name = "txtPostalCode";
            this.txtPostalCode.Size = new System.Drawing.Size(393, 22);
            this.txtPostalCode.TabIndex = 25;
            this.txtPostalCode.Text = "1122DD";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 887);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 17);
            this.label1.TabIndex = 32;
            this.label1.Text = "PostalCode";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(8, 919);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(31, 17);
            this.label15.TabIndex = 33;
            this.label15.Text = "City";
            // 
            // txtCity
            // 
            this.txtCity.Location = new System.Drawing.Point(193, 916);
            this.txtCity.Margin = new System.Windows.Forms.Padding(4);
            this.txtCity.Name = "txtCity";
            this.txtCity.Size = new System.Drawing.Size(393, 22);
            this.txtCity.TabIndex = 26;
            this.txtCity.Text = "City";
            // 
            // txtPhoneNumberGeneral
            // 
            this.txtPhoneNumberGeneral.Location = new System.Drawing.Point(872, 6);
            this.txtPhoneNumberGeneral.Margin = new System.Windows.Forms.Padding(4);
            this.txtPhoneNumberGeneral.Name = "txtPhoneNumberGeneral";
            this.txtPhoneNumberGeneral.Size = new System.Drawing.Size(393, 22);
            this.txtPhoneNumberGeneral.TabIndex = 27;
            this.txtPhoneNumberGeneral.Text = "020 123 44 55";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(612, 10);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(150, 17);
            this.label16.TabIndex = 36;
            this.label16.Text = "PhoneNumberGeneral";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(612, 42);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(140, 17);
            this.label17.TabIndex = 37;
            this.label17.Text = "PhoneNumberMobile";
            // 
            // txtPhoneNumberMobile
            // 
            this.txtPhoneNumberMobile.Location = new System.Drawing.Point(872, 38);
            this.txtPhoneNumberMobile.Margin = new System.Windows.Forms.Padding(4);
            this.txtPhoneNumberMobile.Name = "txtPhoneNumberMobile";
            this.txtPhoneNumberMobile.Size = new System.Drawing.Size(393, 22);
            this.txtPhoneNumberMobile.TabIndex = 28;
            this.txtPhoneNumberMobile.Text = "064 123 44 55";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(612, 139);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(138, 17);
            this.label18.TabIndex = 40;
            this.label18.Text = "ContactPersonName";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(612, 74);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(132, 17);
            this.label19.TabIndex = 42;
            this.label19.Text = "PhoneNumberWork";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(612, 171);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(185, 17);
            this.label20.TabIndex = 44;
            this.label20.Text = "ContactPersonRelationType";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(612, 203);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(160, 17);
            this.label21.TabIndex = 45;
            this.label21.Text = "ContactPersonBirthDate";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(612, 235);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(243, 17);
            this.label22.TabIndex = 47;
            this.label22.Text = "ContactPersonPhoneNumberGeneral";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(612, 267);
            this.label23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(233, 17);
            this.label23.TabIndex = 49;
            this.label23.Text = "ContactPersonPhoneNumberMobile";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(612, 299);
            this.label24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(225, 17);
            this.label24.TabIndex = 51;
            this.label24.Text = "ContactPersonPhoneNumberWork";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(612, 383);
            this.label25.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(245, 17);
            this.label25.TabIndex = 53;
            this.label25.Text = "HealthInsuranceCompanyUZOVICode";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(612, 415);
            this.label26.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(120, 17);
            this.label26.TabIndex = 55;
            this.label26.Text = "InsuranceNumber";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(612, 447);
            this.label27.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(168, 17);
            this.label27.TabIndex = 57;
            this.label27.Text = "GeneralPractitionerName";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(612, 479);
            this.label28.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(222, 17);
            this.label28.TabIndex = 59;
            this.label28.Text = "GeneralPractitionerPhoneNumber";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(8, 677);
            this.label29.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(67, 17);
            this.label29.TabIndex = 61;
            this.label29.Text = "CivilClass";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(8, 708);
            this.label30.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(176, 17);
            this.label30.TabIndex = 63;
            this.label30.Text = "CompositionHousekeeping";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(8, 773);
            this.label31.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(162, 17);
            this.label31.TabIndex = 65;
            this.label31.Text = "ChildrenInHousekeeping";
            // 
            // txtPhoneNumberWork
            // 
            this.txtPhoneNumberWork.Location = new System.Drawing.Point(872, 70);
            this.txtPhoneNumberWork.Margin = new System.Windows.Forms.Padding(4);
            this.txtPhoneNumberWork.Name = "txtPhoneNumberWork";
            this.txtPhoneNumberWork.Size = new System.Drawing.Size(393, 22);
            this.txtPhoneNumberWork.TabIndex = 29;
            this.txtPhoneNumberWork.Text = "030 456 77 88";
            // 
            // txtContactPersonName
            // 
            this.txtContactPersonName.Location = new System.Drawing.Point(872, 135);
            this.txtContactPersonName.Margin = new System.Windows.Forms.Padding(4);
            this.txtContactPersonName.Name = "txtContactPersonName";
            this.txtContactPersonName.Size = new System.Drawing.Size(393, 22);
            this.txtContactPersonName.TabIndex = 30;
            this.txtContactPersonName.Text = "Contact Persoon Name";
            // 
            // txtContactPersonRelationType
            // 
            this.txtContactPersonRelationType.Location = new System.Drawing.Point(872, 167);
            this.txtContactPersonRelationType.Margin = new System.Windows.Forms.Padding(4);
            this.txtContactPersonRelationType.Name = "txtContactPersonRelationType";
            this.txtContactPersonRelationType.Size = new System.Drawing.Size(393, 22);
            this.txtContactPersonRelationType.TabIndex = 31;
            this.txtContactPersonRelationType.Text = "Contact Persoon Relation Type";
            // 
            // txtContactPersonBirthDate
            // 
            this.txtContactPersonBirthDate.Location = new System.Drawing.Point(872, 199);
            this.txtContactPersonBirthDate.Margin = new System.Windows.Forms.Padding(4);
            this.txtContactPersonBirthDate.Name = "txtContactPersonBirthDate";
            this.txtContactPersonBirthDate.Size = new System.Drawing.Size(393, 22);
            this.txtContactPersonBirthDate.TabIndex = 32;
            this.txtContactPersonBirthDate.Text = "19700501";
            // 
            // txtContactPersonPhoneNumberGeneral
            // 
            this.txtContactPersonPhoneNumberGeneral.Location = new System.Drawing.Point(872, 231);
            this.txtContactPersonPhoneNumberGeneral.Margin = new System.Windows.Forms.Padding(4);
            this.txtContactPersonPhoneNumberGeneral.Name = "txtContactPersonPhoneNumberGeneral";
            this.txtContactPersonPhoneNumberGeneral.Size = new System.Drawing.Size(393, 22);
            this.txtContactPersonPhoneNumberGeneral.TabIndex = 33;
            this.txtContactPersonPhoneNumberGeneral.Text = "123 44 55 4";
            // 
            // txtContactPersonPhoneNumberMobile
            // 
            this.txtContactPersonPhoneNumberMobile.Location = new System.Drawing.Point(872, 263);
            this.txtContactPersonPhoneNumberMobile.Margin = new System.Windows.Forms.Padding(4);
            this.txtContactPersonPhoneNumberMobile.Name = "txtContactPersonPhoneNumberMobile";
            this.txtContactPersonPhoneNumberMobile.Size = new System.Drawing.Size(393, 22);
            this.txtContactPersonPhoneNumberMobile.TabIndex = 34;
            this.txtContactPersonPhoneNumberMobile.Text = "061 123 44 55";
            // 
            // txtContactPersonPhoneNumberWork
            // 
            this.txtContactPersonPhoneNumberWork.Location = new System.Drawing.Point(872, 295);
            this.txtContactPersonPhoneNumberWork.Margin = new System.Windows.Forms.Padding(4);
            this.txtContactPersonPhoneNumberWork.Name = "txtContactPersonPhoneNumberWork";
            this.txtContactPersonPhoneNumberWork.Size = new System.Drawing.Size(393, 22);
            this.txtContactPersonPhoneNumberWork.TabIndex = 35;
            this.txtContactPersonPhoneNumberWork.Text = "040 123 44 55";
            // 
            // txtHealthInsuranceCompanyUZOVICode
            // 
            this.txtHealthInsuranceCompanyUZOVICode.Location = new System.Drawing.Point(872, 379);
            this.txtHealthInsuranceCompanyUZOVICode.Margin = new System.Windows.Forms.Padding(4);
            this.txtHealthInsuranceCompanyUZOVICode.Name = "txtHealthInsuranceCompanyUZOVICode";
            this.txtHealthInsuranceCompanyUZOVICode.Size = new System.Drawing.Size(393, 22);
            this.txtHealthInsuranceCompanyUZOVICode.TabIndex = 36;
            this.txtHealthInsuranceCompanyUZOVICode.Text = "304";
            // 
            // txtInsuranceNumber
            // 
            this.txtInsuranceNumber.Location = new System.Drawing.Point(872, 411);
            this.txtInsuranceNumber.Margin = new System.Windows.Forms.Padding(4);
            this.txtInsuranceNumber.Name = "txtInsuranceNumber";
            this.txtInsuranceNumber.Size = new System.Drawing.Size(393, 22);
            this.txtInsuranceNumber.TabIndex = 37;
            this.txtInsuranceNumber.Text = "456789";
            // 
            // txtGeneralPractitionerName
            // 
            this.txtGeneralPractitionerName.Location = new System.Drawing.Point(872, 443);
            this.txtGeneralPractitionerName.Margin = new System.Windows.Forms.Padding(4);
            this.txtGeneralPractitionerName.Name = "txtGeneralPractitionerName";
            this.txtGeneralPractitionerName.Size = new System.Drawing.Size(393, 22);
            this.txtGeneralPractitionerName.TabIndex = 38;
            this.txtGeneralPractitionerName.Text = "Arts";
            // 
            // txtGeneralPractitionerPhoneNumber
            // 
            this.txtGeneralPractitionerPhoneNumber.Location = new System.Drawing.Point(872, 475);
            this.txtGeneralPractitionerPhoneNumber.Margin = new System.Windows.Forms.Padding(4);
            this.txtGeneralPractitionerPhoneNumber.Name = "txtGeneralPractitionerPhoneNumber";
            this.txtGeneralPractitionerPhoneNumber.Size = new System.Drawing.Size(393, 22);
            this.txtGeneralPractitionerPhoneNumber.TabIndex = 39;
            this.txtGeneralPractitionerPhoneNumber.Text = "050 777 88 99";
            // 
            // txtCivilClass
            // 
            this.txtCivilClass.Location = new System.Drawing.Point(193, 673);
            this.txtCivilClass.Margin = new System.Windows.Forms.Padding(4);
            this.txtCivilClass.Name = "txtCivilClass";
            this.txtCivilClass.Size = new System.Drawing.Size(393, 22);
            this.txtCivilClass.TabIndex = 19;
            this.txtCivilClass.Text = "1";
            // 
            // txtCompositionHousekeeping
            // 
            this.txtCompositionHousekeeping.Location = new System.Drawing.Point(193, 705);
            this.txtCompositionHousekeeping.Margin = new System.Windows.Forms.Padding(4);
            this.txtCompositionHousekeeping.Name = "txtCompositionHousekeeping";
            this.txtCompositionHousekeeping.Size = new System.Drawing.Size(393, 22);
            this.txtCompositionHousekeeping.TabIndex = 20;
            this.txtCompositionHousekeeping.Tag = "";
            this.txtCompositionHousekeeping.Text = "3";
            // 
            // txtChildrenInHousekeeping
            // 
            this.txtChildrenInHousekeeping.Location = new System.Drawing.Point(193, 769);
            this.txtChildrenInHousekeeping.Margin = new System.Windows.Forms.Padding(4);
            this.txtChildrenInHousekeeping.Name = "txtChildrenInHousekeeping";
            this.txtChildrenInHousekeeping.Size = new System.Drawing.Size(393, 22);
            this.txtChildrenInHousekeeping.TabIndex = 22;
            this.txtChildrenInHousekeeping.Text = "1";
            // 
            // Label38
            // 
            this.Label38.AutoSize = true;
            this.Label38.Location = new System.Drawing.Point(8, 741);
            this.Label38.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label38.Name = "Label38";
            this.Label38.Size = new System.Drawing.Size(92, 17);
            this.Label38.TabIndex = 101;
            this.Label38.Text = "HousingType";
            // 
            // btnSend
            // 
            this.btnSend.Location = new System.Drawing.Point(612, 671);
            this.btnSend.Margin = new System.Windows.Forms.Padding(4);
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(260, 28);
            this.btnSend.TabIndex = 43;
            this.btnSend.Text = "Verstuur patient data";
            this.btnSend.UseVisualStyleBackColor = true;
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // txtHousingType
            // 
            this.txtHousingType.Location = new System.Drawing.Point(193, 738);
            this.txtHousingType.Margin = new System.Windows.Forms.Padding(4);
            this.txtHousingType.Name = "txtHousingType";
            this.txtHousingType.Size = new System.Drawing.Size(393, 22);
            this.txtHousingType.TabIndex = 21;
            this.txtHousingType.Text = "2";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(8, 171);
            this.label32.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(102, 17);
            this.label32.TabIndex = 112;
            this.label32.Text = "PatientNumber";
            // 
            // txtPatientNumber
            // 
            this.txtPatientNumber.Location = new System.Drawing.Point(193, 167);
            this.txtPatientNumber.Margin = new System.Windows.Forms.Padding(4);
            this.txtPatientNumber.Name = "txtPatientNumber";
            this.txtPatientNumber.Size = new System.Drawing.Size(393, 22);
            this.txtPatientNumber.TabIndex = 5;
            this.txtPatientNumber.Text = "0001";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(8, 464);
            this.label33.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(47, 17);
            this.label33.TabIndex = 114;
            this.label33.Text = "Initials";
            // 
            // txtInitials
            // 
            this.txtInitials.Location = new System.Drawing.Point(193, 460);
            this.txtInitials.Margin = new System.Windows.Forms.Padding(4);
            this.txtInitials.Name = "txtInitials";
            this.txtInitials.Size = new System.Drawing.Size(393, 22);
            this.txtInitials.TabIndex = 13;
            this.txtInitials.Text = "Initials";
            // 
            // btnSendFormulier
            // 
            this.btnSendFormulier.Location = new System.Drawing.Point(612, 702);
            this.btnSendFormulier.Margin = new System.Windows.Forms.Padding(4);
            this.btnSendFormulier.Name = "btnSendFormulier";
            this.btnSendFormulier.Size = new System.Drawing.Size(260, 28);
            this.btnSendFormulier.TabIndex = 44;
            this.btnSendFormulier.Text = "Verstuur aanvraag formulier";
            this.btnSendFormulier.UseVisualStyleBackColor = true;
            this.btnSendFormulier.Click += new System.EventHandler(this.btnSendFormulier_Click);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(608, 737);
            this.label34.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(72, 17);
            this.label34.TabIndex = 117;
            this.label34.Text = "Response";
            // 
            // txbResponse
            // 
            this.txbResponse.Location = new System.Drawing.Point(612, 757);
            this.txbResponse.Margin = new System.Windows.Forms.Padding(4);
            this.txbResponse.Multiline = true;
            this.txbResponse.Name = "txbResponse";
            this.txbResponse.Size = new System.Drawing.Size(653, 182);
            this.txbResponse.TabIndex = 46;
            this.txbResponse.TabStop = false;
            // 
            // lblResponseTime
            // 
            this.lblResponseTime.AutoSize = true;
            this.lblResponseTime.Location = new System.Drawing.Point(703, 737);
            this.lblResponseTime.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblResponseTime.Name = "lblResponseTime";
            this.lblResponseTime.Size = new System.Drawing.Size(0, 17);
            this.lblResponseTime.TabIndex = 119;
            // 
            // txtAddressGPForZorgmail
            // 
            this.txtAddressGPForZorgmail.Location = new System.Drawing.Point(872, 507);
            this.txtAddressGPForZorgmail.Margin = new System.Windows.Forms.Padding(4);
            this.txtAddressGPForZorgmail.Name = "txtAddressGPForZorgmail";
            this.txtAddressGPForZorgmail.Size = new System.Drawing.Size(393, 22);
            this.txtAddressGPForZorgmail.TabIndex = 40;
            this.txtAddressGPForZorgmail.Text = "85314520";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(612, 511);
            this.label35.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(156, 17);
            this.label35.TabIndex = 120;
            this.label35.Text = "AddressGPForZorgmail";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(8, 43);
            this.label36.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(38, 20);
            this.label36.TabIndex = 123;
            this.label36.Text = "GUID";
            this.label36.UseCompatibleTextRendering = true;
            // 
            // btnCreateHash
            // 
            this.btnCreateHash.Location = new System.Drawing.Point(492, 238);
            this.btnCreateHash.Margin = new System.Windows.Forms.Padding(4);
            this.btnCreateHash.Name = "btnCreateHash";
            this.btnCreateHash.Size = new System.Drawing.Size(96, 27);
            this.btnCreateHash.TabIndex = 7;
            this.btnCreateHash.Text = "↓ Create ↓";
            this.btnCreateHash.UseVisualStyleBackColor = true;
            this.btnCreateHash.Click += new System.EventHandler(this.btnCreateHash_Click);
            // 
            // txtHashPrefix
            // 
            this.txtHashPrefix.Location = new System.Drawing.Point(193, 239);
            this.txtHashPrefix.Margin = new System.Windows.Forms.Padding(4);
            this.txtHashPrefix.Name = "txtHashPrefix";
            this.txtHashPrefix.Size = new System.Drawing.Size(301, 22);
            this.txtHashPrefix.TabIndex = 6;
            this.txtHashPrefix.Text = "**HashPrefix**";
            // 
            // txtVisitNumber
            // 
            this.txtVisitNumber.Location = new System.Drawing.Point(193, 332);
            this.txtVisitNumber.Margin = new System.Windows.Forms.Padding(4);
            this.txtVisitNumber.Name = "txtVisitNumber";
            this.txtVisitNumber.Size = new System.Drawing.Size(393, 22);
            this.txtVisitNumber.TabIndex = 9;
            this.txtVisitNumber.Text = "146987321";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(8, 336);
            this.label37.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(84, 17);
            this.label37.TabIndex = 126;
            this.label37.Text = "VisitNumber";
            // 
            // cbpointsites
            // 
            this.cbpointsites.FormattingEnabled = true;
            this.cbpointsites.Location = new System.Drawing.Point(193, 6);
            this.cbpointsites.Margin = new System.Windows.Forms.Padding(4);
            this.cbpointsites.Name = "cbpointsites";
            this.cbpointsites.Size = new System.Drawing.Size(393, 24);
            this.cbpointsites.TabIndex = 0;
            // 
            // lblpointsite
            // 
            this.lblpointsite.AutoSize = true;
            this.lblpointsite.Location = new System.Drawing.Point(8, 10);
            this.lblpointsite.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblpointsite.Name = "lblpointsite";
            this.lblpointsite.Size = new System.Drawing.Size(28, 20);
            this.lblpointsite.TabIndex = 130;
            this.lblpointsite.Text = "Site";
            this.lblpointsite.UseCompatibleTextRendering = true;
            // 
            // txtPharmacyName
            // 
            this.txtPharmacyName.Location = new System.Drawing.Point(872, 539);
            this.txtPharmacyName.Margin = new System.Windows.Forms.Padding(4);
            this.txtPharmacyName.Name = "txtPharmacyName";
            this.txtPharmacyName.Size = new System.Drawing.Size(393, 22);
            this.txtPharmacyName.TabIndex = 41;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(612, 543);
            this.label39.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(108, 17);
            this.label39.TabIndex = 131;
            this.label39.Text = "PharmacyName";
            // 
            // txtPharmacyPhoneNumber
            // 
            this.txtPharmacyPhoneNumber.Location = new System.Drawing.Point(872, 571);
            this.txtPharmacyPhoneNumber.Margin = new System.Windows.Forms.Padding(4);
            this.txtPharmacyPhoneNumber.Name = "txtPharmacyPhoneNumber";
            this.txtPharmacyPhoneNumber.Size = new System.Drawing.Size(393, 22);
            this.txtPharmacyPhoneNumber.TabIndex = 42;
            // 
            // lblPharmacyPhoneNumber
            // 
            this.lblPharmacyPhoneNumber.AutoSize = true;
            this.lblPharmacyPhoneNumber.Location = new System.Drawing.Point(612, 575);
            this.lblPharmacyPhoneNumber.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPharmacyPhoneNumber.Name = "lblPharmacyPhoneNumber";
            this.lblPharmacyPhoneNumber.Size = new System.Drawing.Size(162, 17);
            this.lblPharmacyPhoneNumber.TabIndex = 133;
            this.lblPharmacyPhoneNumber.Text = "PharmacyPhoneNumber";
            // 
            // cbCreate
            // 
            this.cbCreate.AutoSize = true;
            this.cbCreate.Location = new System.Drawing.Point(880, 676);
            this.cbCreate.Margin = new System.Windows.Forms.Padding(4);
            this.cbCreate.Name = "cbCreate";
            this.cbCreate.Size = new System.Drawing.Size(133, 21);
            this.cbCreate.TabIndex = 45;
            this.cbCreate.Text = "+ creëer dossier";
            this.cbCreate.UseVisualStyleBackColor = true;
            // 
            // rdbSHA1
            // 
            this.rdbSHA1.AutoSize = true;
            this.rdbSHA1.Checked = true;
            this.rdbSHA1.Location = new System.Drawing.Point(193, 203);
            this.rdbSHA1.Margin = new System.Windows.Forms.Padding(4);
            this.rdbSHA1.Name = "rdbSHA1";
            this.rdbSHA1.Size = new System.Drawing.Size(65, 21);
            this.rdbSHA1.TabIndex = 134;
            this.rdbSHA1.TabStop = true;
            this.rdbSHA1.Text = "SHA1";
            this.rdbSHA1.UseVisualStyleBackColor = true;
            // 
            // rdbSHA256
            // 
            this.rdbSHA256.AutoSize = true;
            this.rdbSHA256.Location = new System.Drawing.Point(289, 203);
            this.rdbSHA256.Margin = new System.Windows.Forms.Padding(4);
            this.rdbSHA256.Name = "rdbSHA256";
            this.rdbSHA256.Size = new System.Drawing.Size(81, 21);
            this.rdbSHA256.TabIndex = 135;
            this.rdbSHA256.Text = "SHA256";
            this.rdbSHA256.UseVisualStyleBackColor = true;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(8, 208);
            this.label40.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(72, 17);
            this.label40.TabIndex = 136;
            this.label40.Text = "Hash type";
            // 
            // btnFlowFields
            // 
            this.btnFlowFields.Location = new System.Drawing.Point(880, 702);
            this.btnFlowFields.Name = "btnFlowFields";
            this.btnFlowFields.Size = new System.Drawing.Size(160, 28);
            this.btnFlowFields.TabIndex = 137;
            this.btnFlowFields.Text = "FlowFields";
            this.btnFlowFields.UseVisualStyleBackColor = true;
            this.btnFlowFields.Click += new System.EventHandler(this.btnFlowFields_Click);
            // 
            // SendPatient
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1291, 943);
            this.Controls.Add(this.btnFlowFields);
            this.Controls.Add(this.label40);
            this.Controls.Add(this.rdbSHA256);
            this.Controls.Add(this.rdbSHA1);
            this.Controls.Add(this.cbCreate);
            this.Controls.Add(this.txtPharmacyPhoneNumber);
            this.Controls.Add(this.lblPharmacyPhoneNumber);
            this.Controls.Add(this.txtPharmacyName);
            this.Controls.Add(this.label39);
            this.Controls.Add(this.lblpointsite);
            this.Controls.Add(this.cbpointsites);
            this.Controls.Add(this.txtVisitNumber);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.txtHashPrefix);
            this.Controls.Add(this.btnCreateHash);
            this.Controls.Add(this.label36);
            this.Controls.Add(this.txtAddressGPForZorgmail);
            this.Controls.Add(this.label35);
            this.Controls.Add(this.lblResponseTime);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.btnSendFormulier);
            this.Controls.Add(this.txtInitials);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.txtPatientNumber);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.btnSend);
            this.Controls.Add(this.txtHousingType);
            this.Controls.Add(this.Label38);
            this.Controls.Add(this.txtChildrenInHousekeeping);
            this.Controls.Add(this.txtCompositionHousekeeping);
            this.Controls.Add(this.txtCivilClass);
            this.Controls.Add(this.txtGeneralPractitionerPhoneNumber);
            this.Controls.Add(this.txtGeneralPractitionerName);
            this.Controls.Add(this.txtInsuranceNumber);
            this.Controls.Add(this.txtHealthInsuranceCompanyUZOVICode);
            this.Controls.Add(this.txtContactPersonPhoneNumberWork);
            this.Controls.Add(this.txtContactPersonPhoneNumberMobile);
            this.Controls.Add(this.txtContactPersonPhoneNumberGeneral);
            this.Controls.Add(this.txtContactPersonBirthDate);
            this.Controls.Add(this.txtContactPersonRelationType);
            this.Controls.Add(this.txtContactPersonName);
            this.Controls.Add(this.txtPhoneNumberWork);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtSenderUserName);
            this.Controls.Add(this.txtOrganisationID);
            this.Controls.Add(this.OrganisationID);
            this.Controls.Add(this.txtGUID);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.txtPhoneNumberMobile);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.txtPhoneNumberGeneral);
            this.Controls.Add(this.txtCity);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtPostalCode);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtNumber);
            this.Controls.Add(this.txtStreetName);
            this.Controls.Add(this.txtBirthDate);
            this.Controls.Add(this.txtMaidenName);
            this.Controls.Add(this.txtLastName);
            this.Controls.Add(this.txtMiddelName);
            this.Controls.Add(this.txtFirstName);
            this.Controls.Add(this.txtSalutation);
            this.Controls.Add(this.txtGender);
            this.Controls.Add(this.txtCivilServiceNumber);
            this.Controls.Add(this.txtHash);
            this.Controls.Add(this.txtSendDate);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txbResponse);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "SendPatient";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SendPatient";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtGUID;
        private System.Windows.Forms.Label OrganisationID;
        private System.Windows.Forms.TextBox txtOrganisationID;
        private System.Windows.Forms.TextBox txtSenderUserName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtSendDate;
        private System.Windows.Forms.TextBox txtHash;
        private System.Windows.Forms.TextBox txtCivilServiceNumber;
        private System.Windows.Forms.TextBox txtGender;
        private System.Windows.Forms.TextBox txtSalutation;
        private System.Windows.Forms.TextBox txtFirstName;
        private System.Windows.Forms.TextBox txtMiddelName;
        private System.Windows.Forms.TextBox txtLastName;
        private System.Windows.Forms.TextBox txtMaidenName;
        private System.Windows.Forms.TextBox txtBirthDate;
        private System.Windows.Forms.TextBox txtStreetName;
        private System.Windows.Forms.TextBox txtNumber;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtPostalCode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtCity;
        private System.Windows.Forms.TextBox txtPhoneNumberGeneral;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtPhoneNumberMobile;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox txtPhoneNumberWork;
        private System.Windows.Forms.TextBox txtContactPersonName;
        private System.Windows.Forms.TextBox txtContactPersonRelationType;
        private System.Windows.Forms.TextBox txtContactPersonBirthDate;
        private System.Windows.Forms.TextBox txtContactPersonPhoneNumberGeneral;
        private System.Windows.Forms.TextBox txtContactPersonPhoneNumberMobile;
        private System.Windows.Forms.TextBox txtContactPersonPhoneNumberWork;
        private System.Windows.Forms.TextBox txtHealthInsuranceCompanyUZOVICode;
        private System.Windows.Forms.TextBox txtInsuranceNumber;
        private System.Windows.Forms.TextBox txtGeneralPractitionerName;
        private System.Windows.Forms.TextBox txtGeneralPractitionerPhoneNumber;
        private System.Windows.Forms.TextBox txtCivilClass;
        private System.Windows.Forms.TextBox txtCompositionHousekeeping;
        private System.Windows.Forms.TextBox txtChildrenInHousekeeping;
        internal System.Windows.Forms.Label Label38;
        private System.Windows.Forms.Button btnSend;
        internal System.Windows.Forms.TextBox txtHousingType;
        internal System.Windows.Forms.Label label32;
        internal System.Windows.Forms.TextBox txtPatientNumber;
        internal System.Windows.Forms.Label label33;
        internal System.Windows.Forms.TextBox txtInitials;
        private System.Windows.Forms.Button btnSendFormulier;
        internal System.Windows.Forms.Label label34;
        internal System.Windows.Forms.TextBox txbResponse;
        internal System.Windows.Forms.Label lblResponseTime;
        internal System.Windows.Forms.TextBox txtAddressGPForZorgmail;
        internal System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Button btnCreateHash;
        internal System.Windows.Forms.TextBox txtHashPrefix;
        internal System.Windows.Forms.TextBox txtVisitNumber;
        internal System.Windows.Forms.Label label37;
        private System.Windows.Forms.ComboBox cbpointsites;
        private System.Windows.Forms.Label lblpointsite;
        internal System.Windows.Forms.TextBox txtPharmacyName;
        internal System.Windows.Forms.Label label39;
        internal System.Windows.Forms.TextBox txtPharmacyPhoneNumber;
        internal System.Windows.Forms.Label lblPharmacyPhoneNumber;
        private System.Windows.Forms.CheckBox cbCreate;
        private System.Windows.Forms.RadioButton rdbSHA1;
        private System.Windows.Forms.RadioButton rdbSHA256;
        internal System.Windows.Forms.Label label40;
        private System.Windows.Forms.Button btnFlowFields;
    }
}

