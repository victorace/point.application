﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Windows.Forms;
using System.Linq;

namespace Point.FormService.Client.Forms
{

    public partial class JsonApi : Form
    {

        private class ApiMethod
        {
            public int ID { get; set; }
            public string Name { get; set; }
            public string RequestMethod { get; set; }
            public string InputTypes { get; set; }

            public ApiMethod() { }

            public ApiMethod(int id, string name, string requestMethod, string inputTypes)
            {
                ID = id;
                Name = name;
                RequestMethod = requestMethod;
                InputTypes = inputTypes;
            }
        }

        private List<ApiMethod> GetApiMethods()
        {
            var apimethods = new List<ApiMethod>();
            int id = 0;
            apimethods.AddRange(new[] {
                new ApiMethod(id++, "Capacity/CapacityDetails/", "GET", "CapacityRequest"),
                new ApiMethod(id++, "Employee/Exists/", "GET", "EmployeeRequest"),
                new ApiMethod(id++, "Employee/EmployeeInfo/", "GET", "EmployeeRequest"),
                new ApiMethod(id++, "Employee/AutoCreate/", "GET", "EmployeeAutoCreate"),
                new ApiMethod(id++, "Flow/NewFlow", "POST", "NewFlowRequest"),
                new ApiMethod(id++, "Lookup/Regions", "GET", "geen"),
                new ApiMethod(id++, "Lookup/AfterCareTypes", "GET", "geen"),
                new ApiMethod(id++, "Lookup/DepartmentInfo", "GET", "DepartmentID"),
                new ApiMethod(id++, "Lookup/LocationInfo", "GET", "LocationID"),
                new ApiMethod(id++, "Lookup/OrganizationInfo", "GET", "OrganizationID"),
                new ApiMethod(id++, "Lookup/VerwijzingTypes", "GET", "geen"),
                new ApiMethod(id++, "Organization/RegionalCoordinators", "GET", "organizationRequest"),
                new ApiMethod(id++, "Transfer/Status", "GET", "TransferRequest")
            });
            return apimethods;
        }

        private ApiMethod GetSelectedApiMethod()
        {
            ApiMethod apimethod = null;
            if (cbapimethods.SelectedIndex>=0)
            {
                apimethod = GetApiMethods().FirstOrDefault(am => am.ID == (int)cbapimethods.SelectedValue);
            }
            if (apimethod == null)
            {
                apimethod = new ApiMethod();
            }
            return apimethod;
            
        }

        public JsonApi()
        {
            InitializeComponent();

            cbpointsites.DataSource = Config.GetAppSettingList("PointSite");
            int.TryParse(Config.GetAppSetting("DefaultPointSite"), out int selected);
            cbpointsites.SelectedIndex = (selected - 1);

            cbapimethods.DataSource = GetApiMethods().Select(am => new { Key = am.ID, Text = am.Name }).OrderBy(a => a.Text).ToList();
            cbapimethods.DisplayMember = "Text";
            cbapimethods.ValueMember = "Key";

            cbapimethods.SelectedIndexChanged += new EventHandler(cbapimethods_SelectedIndexChanged);
            cbapimethods_SelectedIndexChanged(null, null);

            txbapikey.Text = Config.GetAppSetting("ApiKey");
        }


        private void btnok_Click(object sender, EventArgs e)
        {
            if (cbpointsites.SelectedIndex < 0 || String.IsNullOrEmpty(cbpointsites.SelectedValue.ToString()))
            {
                txbresponsefull.Text = "Selecteer een site";
                return;
            }

            txbresponsestatus.Text = "";
            txbresponsefull.Text = "";
            lblresponsetime.Text = "";

            var apimethod = GetSelectedApiMethod();

            string url = cbpointsites.SelectedValue.ToString();
            if (url.EndsWith("/"))
            {
                url = url.TrimEnd(new[] { '/' });
            }

            sendRequest(url, (chbaddapi.Checked?"api/":"") + apimethod.Name, apimethod.RequestMethod.ToUpper());
        }

        private void btnbrowse_Click(object sender, EventArgs e)
        {
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                txbbestandnaam.Text = ofd.FileName;
                readInputFile();
            }

        }

        private void readInputFile()
        {
            if (!File.Exists(txbbestandnaam.Text))
            {
                return;
            }

            string content = File.ReadAllText(txbbestandnaam.Text);

            if (validJson(content))
            {
                object jsonobject = JsonConvert.DeserializeObject(content);
                txbrequest.Text = JsonConvert.SerializeObject(jsonobject, Formatting.Indented);
                return;
            }

            if (content.Contains("="))
            {
                string[] splitted = content.Split(new[] { "&", Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
                string tmpcontent = "";
                foreach(string split in splitted)
                {
                    tmpcontent += split + Environment.NewLine;
                }
                txbrequest.Text = tmpcontent;
            }

        }

        private bool validJson(string content)
        {
            if (string.IsNullOrEmpty(content))
            {
                return false;
            }
            bool validjson = false;
            object jsonobject = null;
            try
            {
                jsonobject = JsonConvert.DeserializeObject(content);
                validjson = true;
            }
            catch
            {
                validjson = false;
            }
            return validjson;
        }

        private void btnreload_Click(object sender, EventArgs e)
        {
            readInputFile();
        }

        private void sendRequest(string baseurl, string apiMethod, string requestMethod)
        {
            txbresponsestatus.Text = $"Sending to: [{baseurl}/{apiMethod}]";

            using (HttpClient httpclient = new HttpClient())
            {
                try
                {
                    HttpResponseMessage response = null;

                    httpclient.BaseAddress = new Uri(baseurl);
                    httpclient.DefaultRequestHeaders.Add("apiKey", txbapikey.Text);

                    if (requestMethod == WebRequestMethods.Http.Post)
                    {
                        var contenttype = validJson(txbrequest.Text) ? "application/json" : "text/plain";
                        var body = new StringContent(txbrequest.Text, Encoding.UTF8, contenttype);
                        response = httpclient.PostAsync($"{apiMethod}", body).Result; 
                    }
                    else if (requestMethod == WebRequestMethods.Http.Get)
                    {
                        var querystring = String.Join("&", txbrequest.Text.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries));
                        response = httpclient.GetAsync($"{apiMethod}?{querystring}").Result;
                    }
                    txbresponsestatus.Text = $"[{(int)response.StatusCode}]: {response.ReasonPhrase}";
                    var result = response.Content.ReadAsStringAsync().Result;

                    if (validJson(result))
                    {
                        object jsonobject = JsonConvert.DeserializeObject(result);
                        txbresponsefull.Text = JsonConvert.SerializeObject(jsonobject, Formatting.Indented);
                    }
                    else
                    {
                        txbresponsefull.Text = result;
                    }
                }
                catch (WebException wex)
                {
                    var httpwebresponse = (HttpWebResponse)wex.Response;
                    
                    txbresponsestatus.Text = $"[{(int)httpwebresponse.StatusCode}]: {httpwebresponse.StatusDescription}";
                    txbresponsefull.Text = $"Called [{baseurl}/{apiMethod}]\r\nWEBEXCEPTION:\r\n{wex.Message}";
                }
                catch (Exception ex)
                {
                    txbresponsefull.Text = $"Called [{baseurl}/{apiMethod}]\r\nEXCEPTION:\r\n{ex.Message}";
                }
            }
            lblresponsetime.Text = DateTime.Now.ToString("HH:mm:ss");

        }

        private void cbapimethods_SelectedIndexChanged(object sender, EventArgs e)
        {
            var apimethod = GetSelectedApiMethod();
            lblapimethodinfo.Text = $"[{apimethod.RequestMethod}] Input: {apimethod.InputTypes}";
        }
    }
}
