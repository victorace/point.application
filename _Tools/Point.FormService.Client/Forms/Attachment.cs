﻿using Point.FormService.Client.attachmentservice;
using Point.FormService.Client.Models;
using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Point.FormService.Client.Constants;

namespace Point.FormService.Client
{
    public partial class Attachment : Form
    {
        private string filepath;
        private string filename;

        public Attachment()
        {
            InitializeComponent();

            if (!string.IsNullOrEmpty(Environment.UserName))
                txtSenderUserName.Text = Environment.UserName; 

            randomdates();
            attachmenttypes();
            getdefaultorganizationid();
            getdefaulthash();
            pointsites();
        }

        private void randomdates()
        {
            var birthdate = Utils.RandomDateInRange(new DateTime(1910, 1, 1), DateTime.Now);
            txtgeboortedatum.Text = birthdate.ToString("dd-MM-yyyy");
        }

        private void getdefaulthash()
        {
            txtHashPrefix.Text = Config.GetAppSetting("HashPrefix");
        }

        private void getdefaultorganizationid()
        {
            txtOrganisationID.Text = Config.GetAppSetting("OrganizationID");
        }

        private void pointsites()
        {
            cbpointsites.DataSource = Config.GetAppSettingList("PointSite");
            int selected = 1;
            Int32.TryParse(Config.GetAppSetting("DefaultPointSite"), out selected);
            cbpointsites.SelectedIndex = (selected - 1);
        }

        private void attachmenttypes()
        {
            cbattachmenttype.DisplayMember = "Text";
            cbattachmenttype.ValueMember = "Value";
            cbattachmenttype.DataSource = Enum.GetValues(typeof(AttachmentTypeID))
                .Cast<Enum>()
                .Where(e => (AttachmentTypeID)e > AttachmentTypeID.None)
                .Select(e => new ComboboxItem(e.ToString(), Convert.ToInt32(e)))
                .ToList();

            // default 
            cbattachmenttype.SelectedIndex = cbattachmenttype.FindStringExact("Other");
        }

        private void btnCreateHash_Click(object sender, EventArgs e)
        {
            var guid = System.Guid.NewGuid();
            txtGUID.Text = guid.ToString();
            var senddate = DateTime.Now;
            txtSendDate.Text = senddate.ToString("dd-MM-yyyy HH:mm:ss");
            var hashAlgorithmType = rdbSHA256.Checked ? HashType.SHA256 : HashType.SHA1;
            txtHash.Text = HashAuthentication.Create(guid, Int32.Parse(txtOrganisationID.Text), txtSenderUserName.Text, senddate, txtPatientNumber.Text, txtHashPrefix.Text, hashAlgorithmType);
        }

        private void btnbrowse_Click(object sender, EventArgs e)
        {
            filepath = string.Empty;
            filename = string.Empty;

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                filepath = ofd.FileName;
                filename = ofd.SafeFileName;
                txtbestandnaam.Text = filename;
            }
        }

        private void btnok_Click(object sender, EventArgs e)
        {
            var attachmentsoapclient = new attachmentservice.AttachmentSoapClient();
            if (cbpointsites.SelectedValue != null && !string.IsNullOrWhiteSpace(cbpointsites.SelectedValue.ToString()))
            {
                var uri = Utils.BuildPointUrl(cbpointsites.SelectedValue.ToString(), attachmentsoapclient.Endpoint.Address);
                attachmentsoapclient.Endpoint.Address = new System.ServiceModel.EndpointAddress(uri);
            }

            var guid = System.Guid.NewGuid();
            var organizationid = Convert.ToInt32(txtOrganisationID.Text);
            var senderusername = txtSenderUserName.Text;
            var senddate = DateTime.Now;
            var patientnumber = txtPatientNumber.Text;
            var opnamenummer = txtopnamenummer.Text;
            var civilservicenumber = txtCivilServiceNumber.Text;
            var birthdate = Utils.ParseDate(txtgeboortedatum.Text);
            if (!birthdate.HasValue)
            {
                birthdate = DateTime.Now;
            }
            var hashprefix = txtHashPrefix.Text;
            var hashAlgorithmType = rdbSHA256.Checked ? HashType.SHA256 : HashType.SHA1;
            var hash = HashAuthentication.Create(guid, organizationid, senderusername, senddate, patientnumber, hashprefix, hashAlgorithmType);

            var response = "";
            try
            {
                byte[] filedata = File.ReadAllBytes(filepath);
                string base64encodedfile = Convert.ToBase64String(filedata);

                ComboboxItem cbitem = cbattachmenttype.SelectedItem as ComboboxItem;
                AttachmentTypeID attachmentypeid = (AttachmentTypeID)cbitem.Value;

                var attachmentreceivedtempbo = new attachmentservice.AttachmentReceivedTempBO()
                {
                    Name = filename,
                    Description = txtdescription.Text,
                    AttachmentTypeID = attachmentypeid,
                    Base64EncodedData = base64encodedfile
                };

                response = attachmentsoapclient.AddAttachment(guid, organizationid, senderusername, senddate, 
                    patientnumber, opnamenummer, civilservicenumber, birthdate.Value, attachmentreceivedtempbo, hash);
            }
            catch (Exception ex)
            {
                response = ex.Message;
            }

            txtGUID.Text = guid.ToString();
            txtHash.Text = hash;
            txtSendDate.Text = senddate.ToString("dd-MM-yyyy HH:mm:ss");
            txbResponse.Text = response;
            lblResponseTime.Text = DateTime.Now.ToString("HH:mm:ss");
        }

        private void btncancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
