﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

namespace Point.FormService.Client
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void addButtonForms()
        {
            btnform.Visible = false;

            int top = btnform.Top;
            int left = btnform.Left;
            int width = btnform.Width;
            int height = btnform.Height;
            Font font = btnform.Font;

            foreach (Type t in Assembly.GetExecutingAssembly().GetTypes())
            {
                if (t.BaseType == typeof(Form))
                {
                    var emptyconstructor = t.GetConstructor(Type.EmptyTypes);
                    if (emptyconstructor != null)
                    {
                        var form = (Form)emptyconstructor.Invoke(new object[] { });
                        if (form.Tag == null)
                        {
                            Button button = new Button()
                            {
                                Text = form.Name,
                                Top = top,
                                Left = left,
                                Width = width,
                                Height = height,
                                Font = font,
                                Tag = emptyconstructor,

                            };

                            button.Click += Button_Click;

                            this.Controls.Add(button);

                            top += button.Height + 2;
                        }
                    }
                }
            }
        }

        private void resizeform()
        {
            IEnumerable<Button> controls = Controls.Cast<Button>();
            if (controls.Any())
            {
                Control control = controls.First();
                if (control != null)
                {
                    int btntop = control.Top;
                    int btnheight = control.Height + 2;
                    int count = controls.Where(c => c.Visible && c.Tag != null).ToList().Count;

                    this.ClientSize = new Size(this.ClientSize.Width, btntop + btnheight * count + 10);
                }
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            addButtonForms();

            resizeform();
        }

        private void Button_Click(object sender, EventArgs e)
        {
            object tag = ((Button)sender).Tag;
            if (tag != null)
            {
                var constructor = tag as ConstructorInfo;
                if (constructor != null)
                {
                    Form form = null;

                    var openforms = Application.OpenForms;
                    foreach (var openform in openforms)
                    {
                        if (openform.GetType() == constructor.DeclaringType)
                        {
                            form = openform as Form;
                            break;
                        }
                    }

                    if (form == null)
                    {
                        form = (Form)constructor.Invoke(new object[] { });
                    }

                    if (form != null)
                    {
                        form.Show();
                    }
                }
            }
        }
    }
}
