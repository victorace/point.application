﻿using Point.FormService.Client.Extensions;
using System;
using System.ServiceModel;

namespace Point.FormService.Client
{
    public static class Utils
    {
        private static Random random = new Random();

        public static DateTime RandomDateInRange(DateTime start, DateTime end)
        {
            int range = Math.Abs((end - start).Days);
            return start.AddDays(random.Next(range));
        }

        public static DateTime? ParseDate(string value)
        {
            var dateformats = new string[] { "dd/MM/yyyy", "dd/MM/yyyy", "dd-MM-yyyy", "dd-MM-yyyy", "ddMMyyyy" };

            DateTime birthdate;
            if (DateTime.TryParseExact(value, dateformats, null, System.Globalization.DateTimeStyles.None, out birthdate))
            {
                return birthdate;
            }

            return null;
        }

        public static string BuildPointUrl(string pointsite, EndpointAddress endpointaddress)
        {
            if (endpointaddress == null)
            {
                throw new ArgumentNullException("endpointaddress");
            }

            if (string.IsNullOrWhiteSpace(pointsite))
            {
                return endpointaddress.ToString();
            }

            var currenturi = new Uri(endpointaddress.ToString());
            var pointurl = new Uri(pointsite).ToString();
            var endpoint = new Uri(pointurl.UrlCombine(currenturi.AbsolutePath)).ToString();
            return endpoint;
        }
    }
}
