﻿using System.ComponentModel;

namespace Point.FormService.Client.Enums
{
    public enum AttachmentTypeID
    {
        [Description("")] None = 0,
        [Description("Afspraken patient")] PatientAppointment = 1,
        [Description("Eerstelijn verblijf")] ShortTemporaryStay = 2,
        [Description("GRZ formulier")] GRZForm = 3,
        [Description("Huisarts brief")] GeneralPractitionerLetter = 4,
        [Description("Hulpmiddelen formulier")] ApparatusForm = 5,
        [Description("Indicatiestelling")] Indication = 6,
        [Description("Medicatielijst")] MedicationList = 7,
        [Description("Medische gegevens")] MedicalDetails = 8,
        [Description("MSVT Indicatie")] MSVTIndication = 9,
        [Description("Paramedische overdracht")] ParamedicalTransfer = 10,
        [Description("Recept")] Prescription = 11,
        [Description("Specialist brief (ontslagbrief)")] DismissionLetter = 12,
        [Description("Terminaal verklaring")] TerminalStatement = 13,
        [Description("Toestemming patient")] PatientConsent = 14,
        [Description("Uitvoeringsverzoek")] FullfillmentRequest = 15,
        [Description("Verpleegkundige Overdracht")] NurseTransfer = 16,
        [Description("Wond formulier")] InjuryForm = 17,
        [Description("Verpleegkundige Overdracht (als bijlage)")] ExternVODocument = 19,
        [Description("Overig")] Other = 18,
    }
}
