﻿using System;
using System.Windows.Forms;

namespace Point.FormService.Client
{
    static class Program
    {
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MultiFormContext(new MainForm()));
        }
    }
}