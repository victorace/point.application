﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace Point.FormService.Client
{
    public static class Config
    {
        public static List<string> GetAppSettingList(string name)
        {
            var keys = ConfigurationManager.AppSettings.Keys;
            return keys.Cast<object>()
                       .Where(key => key.ToString().ToLower()
                       .StartsWith(name.ToLower()))
                       .Select(key => ConfigurationManager.AppSettings.Get(key.ToString())).ToList();
        }

        public static string GetAppSetting(string key)
        {
            var value = "";

            try
            {
                value = ConfigurationManager.AppSettings.Get(key);
            }
            catch
            {
                value = "[" + key + "] not found !!!";
            }

            return value;
        }
    }
}
