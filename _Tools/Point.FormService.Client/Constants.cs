﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Point.FormService.Client.Constants
{
    public static class HashType
    {
        public const string SHA1 = "SHA1";
        public const string SHA256 = "SHA256";
    }
}
