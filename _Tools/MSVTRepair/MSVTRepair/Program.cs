﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace MSVTRepair
{
    public class Program
    {
        public static PointDataClassesDataContext DBContext;

        public class ActionHealthInsurerHistory
        {
            public int? ActionHealthInsurerID { get; set; }
            public int? LogActionHealthInsurerID { get; set; }
            public int? MutActionHealthInsurerID { get; set; }
            public string Action { get; set; }
            public int Version { get; set; }

            public int? ActionCodeHealthInsurerID { get; set; }
            public int TransferId { get; set; }
            public int FormSetVersionId { get; set; }
            public int? Amount { get; set; }
            public string Unit { get; set; }
            public int? ExpectedTime { get; set; }
            public string Definition { get; set; }
            public DateTime? TimeStamp { get; set; }
            public int? EmployeeID { get; set; }
            public int? ScreenID { get; set; }
        }

        private static List<ActionHealthInsurerHistory> getActionHealthInsurerHistory(int transferId)
        {
            return (from L in DBContext.LogActionHealthInsurers
                    join M in DBContext.MutActionHealthInsurers on new { LogActionHealthInsurerID = L.LogActionHealthInsurerID } equals new { LogActionHealthInsurerID = Convert.ToInt32(M.LogActionHealthInsurerID) }
                    where M.TransferID == transferId
                    orderby L.ActionHealthInsurerID, M.FormSetVersionID, M.TimeStamp
                    select new ActionHealthInsurerHistory
                    {
                        TransferId = M.TransferID.Value,
                        ActionHealthInsurerID = M.ActionHealthInsurerID,
                        LogActionHealthInsurerID = L.LogActionHealthInsurerID,
                        MutActionHealthInsurerID = M.MutActionHealthInsurerID,
                        FormSetVersionId = M.FormSetVersionID.Value,
                        Action = L.Action,
                        Version = L.Version.Value,
                        ActionCodeHealthInsurerID = M.ActionCodeHealthInsurerID,
                        Amount = M.Amount,
                        Unit = M.Unit,
                        ExpectedTime = M.ExpectedTime,
                        Definition = M.Definition,
                        TimeStamp = M.TimeStamp,
                        EmployeeID = M.EmployeeID,
                        ScreenID = M.ScreenID
                    }).ToList();
        }

        private static List<int> getAffectedTransferIds()
        {
            return (from fsv in DBContext.FormSetVersions
                    group fsv by fsv.TransferID into grp
                    where grp.Count() > 1
                    orderby grp.Count() descending
                    select grp.Key).ToList();
        }

        private static List<int> getMissingFormSetVersionIds(int transferId)
        {
            return (from FormSetVersions in DBContext.FormSetVersions
                    join ActionHealthInsurers in DBContext.ActionHealthInsurers on
                    new { FormSetVersionID = FormSetVersions.FormSetVersionID } equals new { FormSetVersionID = Convert.ToInt32(ActionHealthInsurers.FormSetVersionID) } into ActionHealthInsurers_join
                    from ActionHealthInsurers in ActionHealthInsurers_join.DefaultIfEmpty()
                    where ActionHealthInsurers.FormSetVersionID == null && FormSetVersions.TransferID == transferId
                    select FormSetVersions.FormSetVersionID).Distinct().ToList();
        }

        private static string getFormSetVersionDescription(int formSetVersionId)
        {
            return (from FormSetVersions in DBContext.FormSetVersions
                    where FormSetVersions.FormSetVersionID == formSetVersionId
                    select FormSetVersions.Description).FirstOrDefault();
        }

        static void Main(string[] args)
        {
            string filenameWithPath = Path.Combine(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location), "mvstrepair_records.csv");
            if (args.Count() == 1)
            {
                filenameWithPath = args[0];
            }

            DBContext = new PointDataClassesDataContext();

            StringBuilder csv = new StringBuilder();
            csv.AppendLine("ActionHealthInsurerID;ActionCodeHealthInsurerID;TransferID;FormSetVersionID;Amount;Unit;ExpectedTime;Definition;TimeStamp;EmployeeID;ScreenID;;FormSet-Description");

            StringBuilder sql = new StringBuilder();

            int recordsCreated = 0;
            foreach (int transferId in getAffectedTransferIds().OrderBy(t=>t))
            {
                var missingFormSetVersionIds = getMissingFormSetVersionIds(transferId);
                
                List<List<ActionHealthInsurerHistory>> actionHealthInsurerIdMutations = new List<List<ActionHealthInsurerHistory>>();

                List<ActionHealthInsurerHistory> mutations = getActionHealthInsurerHistory(transferId);
                foreach (int id in mutations.Select(x => x.ActionHealthInsurerID.Value).Distinct().ToList())
                {
                    actionHealthInsurerIdMutations.Add(mutations.Where(x => x.ActionHealthInsurerID.Value == id).ToList());
                }
                
                foreach (List<ActionHealthInsurerHistory> actionHealthInsurerHistories in actionHealthInsurerIdMutations)
                {
                    ActionHealthInsurerHistory previousActionHealthInsurerHistory = actionHealthInsurerHistories.First();
                    int formSetVersionId = previousActionHealthInsurerHistory.FormSetVersionId;
                    foreach (ActionHealthInsurerHistory actionHealthInsurerHistory in actionHealthInsurerHistories)
                    {
                        if (formSetVersionId != actionHealthInsurerHistory.FormSetVersionId)
                        {
                            if (missingFormSetVersionIds.Contains(formSetVersionId))
                            {
                                csv.AppendLine(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};;{11}",
                                            previousActionHealthInsurerHistory.ActionHealthInsurerID,
                                            previousActionHealthInsurerHistory.ActionCodeHealthInsurerID.HasValue ? previousActionHealthInsurerHistory.ActionCodeHealthInsurerID.Value.ToString() : "",
                                            previousActionHealthInsurerHistory.TransferId,
                                            previousActionHealthInsurerHistory.FormSetVersionId,
                                            previousActionHealthInsurerHistory.Amount.HasValue ? previousActionHealthInsurerHistory.Amount.Value.ToString() : "",
                                            previousActionHealthInsurerHistory.Unit,
                                            previousActionHealthInsurerHistory.ExpectedTime.HasValue ? previousActionHealthInsurerHistory.ExpectedTime.Value.ToString() : "",
                                            previousActionHealthInsurerHistory.Definition,
                                            previousActionHealthInsurerHistory.TimeStamp.HasValue ? previousActionHealthInsurerHistory.TimeStamp.Value.ToString("yyyy-MM-dd HH:mm:ss") : "",
                                            previousActionHealthInsurerHistory.EmployeeID.HasValue ? previousActionHealthInsurerHistory.EmployeeID.Value.ToString() : "",
                                            previousActionHealthInsurerHistory.ScreenID.HasValue ? previousActionHealthInsurerHistory.ScreenID.Value.ToString() : "",
                                            getFormSetVersionDescription(previousActionHealthInsurerHistory.FormSetVersionId)
                                    ));

                                sql.AppendLine(string.Format(@"INSERT INTO [ActionHealthInsurer] ([ActionCodeHealthInsurerID], [TransferID], [FormSetVersionID], [Amount], [Unit], [ExpectedTime], [Definition], [TimeStamp], [EmployeeID], [ScreenID]) 
                                        VALUES ({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9})",
                                        previousActionHealthInsurerHistory.ActionCodeHealthInsurerID.HasValue ? previousActionHealthInsurerHistory.ActionCodeHealthInsurerID.Value.ToString() : "NULL",
                                        previousActionHealthInsurerHistory.TransferId,
                                        previousActionHealthInsurerHistory.FormSetVersionId,
                                        previousActionHealthInsurerHistory.Amount.HasValue ? previousActionHealthInsurerHistory.Amount.Value.ToString() : "NULL",
                                        !string.IsNullOrWhiteSpace(previousActionHealthInsurerHistory.Unit) ? string.Format("'{0}'", previousActionHealthInsurerHistory.Unit) : "NULL",
                                        previousActionHealthInsurerHistory.ExpectedTime.HasValue ? previousActionHealthInsurerHistory.ExpectedTime.Value.ToString() : "NULL",
                                        !string.IsNullOrWhiteSpace(previousActionHealthInsurerHistory.Definition) ? string.Format("'{0}'", previousActionHealthInsurerHistory.Definition) : "NULL",
                                        previousActionHealthInsurerHistory.TimeStamp.HasValue ? string.Format("'{0}'", previousActionHealthInsurerHistory.TimeStamp.Value.ToString("yyyy-MM-dd HH:mm:ss")) : "NULL",
                                        previousActionHealthInsurerHistory.EmployeeID.HasValue ? previousActionHealthInsurerHistory.EmployeeID.Value.ToString() : "NULL",
                                        previousActionHealthInsurerHistory.ScreenID.HasValue ? previousActionHealthInsurerHistory.ScreenID.Value.ToString() : "NULL"
                                    ));

                                recordsCreated++;
                            }
                            formSetVersionId = actionHealthInsurerHistory.FormSetVersionId;
                        }
                        previousActionHealthInsurerHistory = actionHealthInsurerHistory;
                    }
                }
            }

            try
            {
                System.Console.WriteLine("Found '{0}' records.", recordsCreated);
                using (StreamWriter sw = new StreamWriter(filenameWithPath, false))
                {
                    System.Console.WriteLine("Creating '{0}'.", filenameWithPath);
                    sw.Write(csv.ToString());
                }

                filenameWithPath = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(filenameWithPath), string.Format("{0}.sql", System.IO.Path.GetFileNameWithoutExtension(filenameWithPath)));
                using (StreamWriter sw = new StreamWriter(filenameWithPath, false))
                {
                    System.Console.WriteLine("Creating '{0}'.", filenameWithPath);
                    sw.Write(sql.ToString());
                }
            }

            catch (Exception e)
            {
                System.Console.WriteLine(string.Format("ERROR whilst trying to save file. StackTrace: {0}", e.ToString()));
            }

            System.Console.WriteLine("Press any key to continue...");
            System.Console.ReadKey();
            System.Environment.Exit(0);
        }
    }
}