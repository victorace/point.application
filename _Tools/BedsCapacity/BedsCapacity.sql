-- Dump bedden capaciteit
-- Wordt hanmatig uitgevoerd

DECLARE @DateBegin DATETIME
DECLARE @DateEnd DATETIME

DECLARE @Days INT

SET @DateBegin = '20131001' -- Start datum voor dump
SET @Days = 1000            -- Aantal dagen

SELECT @DateEnd = dateadd(day, @Days, @DateBegin) -- End datum (berekent op basis van start datum en een aantal dagen)

select 
       vw_OrgaLocaDepCareInactive_Indexed.RegionName as 'Regio',      
       vw_OrgaLocaDepCareInactive_Indexed.OrganizationCareName as 'Organisatie',      
       vw_OrgaLocaDepCareInactive_Indexed.LocationCareName as 'Locatie',
	   vw_OrgaLocaDepCareInactive_Indexed.DepartmentCareName as 'Afdeling',
	   vw_OrgaLocaDepCareInactive_Indexed.City as 'Plaats',
       DepartmentCapacityID 'Nummer', 
       DepartmentCapacity.DepartmentID as 'Afdeling nummer', 
       DepartmentCapacity.CapacityDepartment as 'Capaciteit', 
       convert(date, DepartmentCapacity.CapacityDepartmentDate) as 'Datum',
       DepartmentCapacity.AdjustmentCapacityDate as 'Datum en tijd aanpassing bedden capaciteit'
from  DepartmentCapacity left outer join vw_OrgaLocaDepCareInactive_Indexed on vw_OrgaLocaDepCareInactive_Indexed.DepartmentID = DepartmentCapacity.DepartmentID
where DepartmentCapacity.CapacityDepartmentDate >= @DateBegin and DepartmentCapacity.CapacityDepartmentDate <= @DateEnd
order by DepartmentCapacity.DepartmentID, DepartmentCapacity.CapacityDepartmentDate