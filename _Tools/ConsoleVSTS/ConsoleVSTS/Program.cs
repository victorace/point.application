﻿using Microsoft.TeamFoundation.Client;
using Microsoft.TeamFoundation.VersionControl.Client;
using Microsoft.TeamFoundation.WorkItemTracking.Client;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ConsoleVSTS
{
    class Program
    {
        static void Main(string[] args)
        {
            var process = new Process();

            //process.DoQueryWrongBranch();
            process.DoQueryReleaseNotes();

            Console.WriteLine("Done?");
            Console.ReadKey();

        }
    }

    public class Process
    {

        StreamWriter logfile;
        string logfilename = @"c:\temp\consolereleasenotes.txt";
        string currentiteration = @"T2 2018";
        string branch = @"$/POINT.Application/Development/2018T2";

        VersionControlServer versionControlServer;
        WorkItemStore workItemStore;

        private void Log(string message)
        {
            logfile.WriteLine(message);
            Console.WriteLine(message);
        }


        private List<Changeset> GetChangesets()
        {
            int from = 1;

            var fromVersion = new ChangesetVersionSpec(from);
            var toVersion = VersionSpec.Latest;

            var tfs = TfsTeamProjectCollectionFactory.GetTeamProjectCollection(new Uri("https://linkassist.visualstudio.com"));

            versionControlServer = tfs.GetService<VersionControlServer>();
            workItemStore = tfs.GetService<WorkItemStore>();

            var history = versionControlServer.QueryHistory(branch, VersionSpec.Latest, 0, RecursionType.Full, null, fromVersion, toVersion, int.MaxValue, false, false, false);

            var changesets = history.OfType<Changeset>().ToList();

            return changesets;
        }

        public void DoQueryWrongBranch()
        {
            logfile = File.CreateText(logfilename);
            logfile.AutoFlush = true;


            foreach (var changeset in GetChangesets())
            {
                Log($"{changeset.ChangesetId}: {changeset.Comment} [{changeset.Owner}]");
                foreach(var workitem in changeset.WorkItems)
                {
                    if (GetWorkItemType(workitem) == "Task")
                    {
                        foreach(WorkItemLink link in workitem.WorkItemLinks)
                        {
                            var linkworkitem = workItemStore.GetWorkItem(link.TargetId);
                            WorkItemInfo(linkworkitem);
                        }
                    }
                    WorkItemInfo(workitem);
                }
            }

            logfile.Flush();
            logfile.Close();
        }

        private List<int> doneids = new List<int>();

        public void DoQueryReleaseNotes()
        {
            logfile = File.CreateText(logfilename);
            logfile.AutoFlush = true;


            foreach (var changeset in GetChangesets())
            {
                foreach (var workitem in changeset.WorkItems)
                {
                    if (GetWorkItemType(workitem) == "Task")
                    {
                        foreach (WorkItemLink link in workitem.WorkItemLinks)
                        {
                            var linkworkitem = workItemStore.GetWorkItem(link.TargetId);
                            WorkItemDetailsSmall(linkworkitem);
                        }
                    }
                    WorkItemDetailsSmall(workitem);
                }
            }

            logfile.Flush();
            logfile.Close();

        }

        private void WorkItemInfo(WorkItem workItem)
        {
            var workitemtype = GetWorkItemType(workItem);

            int indent = ((workitemtype == "Bug" || workitemtype == "PBI") ? 1 : 2);
            var warning = (workitemtype == "Bug" || workitemtype == "PBI") && !workItem.IterationPath.Contains(currentiteration);

            Log($"{new String(' ', indent*5)}{workitemtype} {workItem.Id}: {workItem.Title}");

            Log($"{new String(' ', indent*5)}{workItem.IterationPath.Replace(@"POINT.Application\3.Releases in Development\", "")}{(warning?"***":"")}");

        }

        private string GetWorkItemType(WorkItem workItem)
        {
            var workitemtype = workItem.Fields.OfType<Field>().Where(f => f.Name == "Work Item Type").FirstOrDefault();
            var workitemtypevalue = workitemtype?.Value as string;

            if (workitemtypevalue == "Product Backlog Item")
            {
                workitemtypevalue = "PBI";
            }

            return workitemtypevalue;
        }


        private void WorkItemDetailsSmall(WorkItem workItem)
        {
            if (doneids.Contains(workItem.Id))
            {
                return;
            }

            doneids.Add(workItem.Id);

            var workitemtype = GetWorkItemType(workItem);

            Log($"{workItem.Id} ({workitemtype})");

        }

        private void WorkItemDetails(WorkItem workItem)
        {
            if (doneids.Contains(workItem.Id))
            {
                return;
            }

            doneids.Add(workItem.Id);

            var workitemtype = GetWorkItemType(workItem);

            if (workitemtype != "Bug" && workitemtype != "PBI")
            {
                return;
            }

            Log($"ID: {workItem.Id}");
            Log($"Type: {workitemtype}");
            Log($"Title: {workItem.Title}");
            if (workitemtype == "Bug")
            {
                GetFieldInfo(workItem.Fields, "Repro Steps");
            }
            if (workitemtype == "PBI")
            {
                Log($"Description: {workItem.Description}");
            }

            GetFieldInfo(workItem.Fields, "Acceptance Criteria");
            GetFieldInfo(workItem.Fields, "Tags");
            GetFieldInfo(workItem.Fields, "Created Date");
            GetFieldInfo(workItem.Fields, "Created By");
            GetFieldInfo(workItem.Fields, "Assigned To");
            GetFieldInfo(workItem.Fields, "Board Column");
            GetFieldInfo(workItem.Fields, "State");

            Log("");
            Log("---------------------------------------------");
            Log("");

        }

        private void GetFieldInfo(FieldCollection fields, string name)
        {
            var field = fields.OfType<Field>().FirstOrDefault(f => f.Name == name);
            var fieldvalue = "";
            if (field != null)
            {
                if (name.EndsWith("Date"))
                {
                    fieldvalue = DateTime.Parse(field.Value.ToString()).ToString("dd-MM-yyyy HH:mm");
                }
                else
                {
                    fieldvalue = field.Value as string;
                }
            }

            Log($"{name}: {fieldvalue}");
        }
            
    }

    
}
