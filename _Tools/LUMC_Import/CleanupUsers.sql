-- Clears the users created when testing the import.
-- Set the empid to the lowest number of an employeeid which is just created.

declare @empid int
set @empid = 999999 -- <<< --- Lowest EmployeeID just created. -- <<< -------

declare @uids table ( userid uniqueidentifier )

insert into @uids
select UserId from Employee 
where EmployeeID >= @empid

delete from dbo.aspnet_Membership
 where UserId in ( select userid from @uids )
   
delete from dbo.aspnet_UsersInRoles
 where UserId in ( select userid from @uids )

delete from Employee 
 where employeeid >= @empid

delete from dbo.aspnet_Users 
 where UserId in ( select userid from @uids )






