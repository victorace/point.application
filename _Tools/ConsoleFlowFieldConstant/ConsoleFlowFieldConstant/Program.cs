﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleFlowFieldConstant
{
    class Program
    {
        static void Main(string[] args)
        {

            string connectionString = ConfigurationManager.ConnectionStrings["DBToImport"].ConnectionString;
            string tab = "    ";
            

            using (SqlConnection sqlconnection = new SqlConnection(connectionString))
            {
                using (SqlCommand sqlcommand = new SqlCommand($"select * from FlowField order by FlowFieldID", sqlconnection))
                {
                    sqlconnection.Open();

                    var reader = sqlcommand.ExecuteReader();

                    var file = File.CreateText(Path.Combine(ConfigurationManager.AppSettings["WorkingDir"], "FlowFieldConstants.cs"));
                    file.AutoFlush = true;

                    file.WriteLine($"namespace Point.Database.Models");
                    file.WriteLine($"{{");
                    file.WriteLine($"{tab}public static class FlowFieldConstants");
                    file.WriteLine($"{tab}{{");
                    file.WriteLine($"{tab}{tab}public const string Value_Ja = \"Ja\";");
                    file.WriteLine($"{tab}{tab}public const string Value_Nee = \"Nee\";");
                    file.WriteLine($"{tab}{tab}public const string Value_True = \"true\";");
                    file.WriteLine($"{tab}{tab}public const string Value_False = \"false\";");
                    file.WriteLine($"");
                    file.WriteLine($"{tab}{tab}public const string Value_BeloopOpname_Patientisontslagen = \"1\";");
                    file.WriteLine($"{tab}{tab}public const string Value_BeloopOpname_Patientisoverleden = \"2\";");
                    file.WriteLine($"{tab}{tab}public const string Value_Ontslagbestemming_Huis = \"1\";");
                    file.WriteLine($"{tab}{tab}public const string Value_Ontslagbestemming_Revalidatiecentrum = \"2\";");
                    file.WriteLine($"{tab}{tab}public const string Value_Ontslagbestemming_GRZCVAverpleeghuis = \"3\";");
                    file.WriteLine($"{tab}{tab}public const string Value_Ontslagbestemming_Verpleeghuis = \"4\";");
                    file.WriteLine($"{tab}{tab}public const string Value_Ontslagbestemming_Anderziekenhuis = \"5\";");
                    file.WriteLine($"{tab}{tab}public const string Value_Ontslagbestemming_Buitenland = \"6\";");
                    file.WriteLine($"{tab}{tab}public const string Value_Ontslagbestemming_Overig = \"7\";");
                    file.WriteLine($"{tab}{tab}public const string Value_Ontslagbestemming_Dossiersluiten = \"8\";");
                    file.WriteLine($"{tab}{tab}public const string Value_OntslagbestemmingThuis_Thuismetzorg = \"2\";");
                    file.WriteLine($"{tab}{tab}public const string Value_OntslagbestemmingThuis_Thuismetzorgenbehandeling = \"4\";");
                    file.WriteLine($"");



                    while (reader.Read())
                    {
                        var flowfieldid = reader["FlowFieldID"].ToString();
                        var flowfieldname = reader["Name"].ToString();
                        var flowfieldlabel = reader["Label"].ToString().Replace(Environment.NewLine, " ");
                        var flowfieldtype = reader["Type"].ToString();
                        Console.WriteLine($"{flowfieldid} {flowfieldname}");
                        file.WriteLine($"");
                        file.WriteLine($"{tab}{tab}// '{flowfieldlabel}' ({flowfieldtype}: {getType(flowfieldtype)})");
                        file.WriteLine($"{tab}{tab}public const int ID_{flowfieldname} = {flowfieldid};");
                        file.WriteLine($"{tab}{tab}public const string Name_{flowfieldname} = \"{flowfieldname}\";");

                    }


                    file.WriteLine($"{tab}}}");
                    file.WriteLine($"}}");
                }
            }

            Console.WriteLine("Done?");
            Console.ReadKey();

        }

        private static string getType(string flowfieldtype)
        {
            switch (flowfieldtype)
            {
                case "0":
                    return "None";
                case "1":
                    return "String";
                case "2":
                    return "DateTime";
                case "3":
                    return "Time";
                case "4":
                    return "Number";
                case "5":
                    return "RadioButton";
                case "6":
                    return "Checkbox";
                case "7":
                    return "DropDownList";
                case "8":
                    return "IDText";
                case "9":
                    return "Date";
                case "10":
                    return "CheckboxList";
                case "11":
                    return "Hidden";
                case "100":
                    return "Group";

            }

            return "???";
        }
    }
}
