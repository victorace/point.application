-- Script om AllOrganizationDepartmentsLinked aanzetten bij 
-- alle gebruikers van @organizationid met de rol @defaultrole.
-- Deze script wordt eenmalig gebruikt voor initiele 
-- aanzetten van AllOrganizationDepartmentsLinked
-- @organizationid moet aangepast worden.

declare @defaultrole varchar(50)

declare @organizationid as integer
declare @departmenttypeid as integer
declare @roleid uniqueidentifier

-- comment out voor uitvoeren.
--set @organizationid = 18 -- LUMC in PointLUMC db
set @defaultrole = 'HospitalDepartmentEmployee'
set @departmenttypeid = 1

select @roleID = RoleId
  from aspnet_roles
 where RoleName = @defaultrole

if @roleID is null
begin
    print 'FOUT: Rol "' + @defaultrole + '" niet gevonden!'
    return
end


update Employee
set AllOrganizationDepartmentsLinked = 1
from Employee
	INNER JOIN Department ON Employee.DepartmentID = Department.DepartmentID
	INNER JOIN Location ON Department.LocationID = Location.LocationID
	INNER JOIN Organization ON Location.OrganizationID = Organization.OrganizationID
	LEFT OUTER JOIN aspnet_Users ON Employee.UserId = aspnet_Users.UserId
	LEFT OUTER JOIN aspnet_UsersInRoles ON aspnet_Users.UserId = aspnet_UsersInRoles.UserId	
where Organization.OrganizationID = @organizationid and
	Employee.Inactive = 0 and
	isnull(Employee.AllOrganizationDepartmentsLinked, 0) <> 1 and
	aspnet_UsersInRoles.RoleId = @roleID and
	Department.Inactive = 0 and
	Department.DepartmentTypeID = @departmenttypeid and
	Location.Inactive = 0 and
	Organization.Inactive = 0
	