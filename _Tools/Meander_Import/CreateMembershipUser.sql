----------------------------------
--
-- Voorbereidingen op excelsheet
-- - Kolomnamen (deze dienen verplicht in de sheet aanwezig te zijn)
--   Afdelingsnaam, Voornaam, Tussenvoegsel, Achternaam, Meisjesnaam, Geslacht, Telnr, ExternID, Functie, Emailadres, Inlognaam,
--   KoppelenAlleAfdelingen
-- - Sheetnaam: PointImport
--
--
-- Voorbereiding script
-- Controleren/bepalen of [ChangePasswordByEmployee] moet aangepast worden (1 of 0). 0 bij SSO
-- DefaultPassword
-- LocationName
--
-- Importeren:
-- 
-- 1: Verwijder de tijdelijke tabel:
--    DROP TABLE [PointImport$]
-- 2: Importeer van de excelfile de sheet 'PointImport$' naar tabel '[dbo].[PointImport$]'
-- 3: Dubbelcheck de 5 variabelen of dit de juiste organisatie aangeeft
-- 4: Draai dit script "CreateMembershipUser.sql"
-- 5: Kopieer het nu geproduceerde 'log' vanuit de messages-tab naar een tekstbestand en sla deze op
--
----------------------------------

declare @organizationname varchar(255)
declare @locationname varchar(255)
declare @defaultpassword varchar(50)
declare @usernameprefix varchar(10)
declare @defaultrole varchar(50)
declare @changepasswordbyemployee bit

-- AVL (NKI) 
set @organizationname = 'Antoni van Leeuwenhoek'
set @locationname = 'Locatie 1 AVL'
set @defaultpassword = '!NkiAvl#1702.'
set @usernameprefix = ''
set @defaultrole = 'HospitalDepartmentEmployee'
set @changepasswordbyemployee = 0

----------------------------------
set nocount on

declare @organizationid int
declare @locationid int
declare @departmentid int

declare @line int
declare @cont bit

declare @salt varchar(50)
declare @saltbase64 varchar(50)
declare @saltcount int
declare @saltchars varchar(255)
declare @password varchar(50)
declare @passwordbase64 varchar(50) 
declare @base64output varchar(255)
declare @base64bits varbinary(3)
declare @base64pos int

declare @roleID uniqueidentifier

declare @department varchar(255)
declare @firstname varchar(50)
declare @middlename varchar(50)
declare @lastname varchar(255)
declare @maidenname varchar(255)
declare @gender char(1)
declare @phonenumber varchar(255)
declare @externid varchar(50)
declare @position varchar(255)
declare @emailaddress varchar(255)
declare @username varchar(255)
declare @allorganizationdepartmentslinked bit
declare @alldepartments char(1)
declare @pointalleafdelingen bit
declare @modifiedbyemployeeid int
declare @lastnamecreator varchar(255)
declare @screenid int
declare @timestamp datetime

set @screenid = 1
set @lastnamecreator = 'DataMigratieZorgaanbieders'

select @modifiedbyemployeeid = EmployeeID
  from Employee
  where LastName = @lastnamecreator

if ISNULL(@modifiedbyemployeeid,0) = 0 
begin
    print 'FOUT: Gebruiker "' + @modifiedbyemployeeid + '" niet gevonden!'
    return
end

select @organizationid = OrganizationID
  from Organization 
 where Name = @organizationname
 
if ISNULL(@organizationid,0) = 0 
begin
    print 'FOUT: Organisatie "' + @organizationname + '" niet gevonden!'
    return
end
--print 'INFO: OrganizationID = ' + cast(@organizationid as varchar(10))

select @locationid = LocationID
  from Location 
 where Name = @locationname 
   and OrganizationID = @organizationid 
 
if ISNULL(@locationid,0) = 0 
begin
    print 'FOUT: Locatie "' + @locationname + '" niet gevonden!'
    return
end
--print 'INFO: LocationID = ' + cast(@locationid as varchar(10))

select @roleID = RoleId 
  from aspnet_roles
 where RoleName = @defaultrole

if @roleID is null
begin
    print 'FOUT: Rol "' + @defaultrole + '" niet gevonden!'
    return
end
--print 'INFO: RoleID = ' + cast(@defaultrole as varchar(50))


declare accCursor cursor for
    select Afdelingsnaam, 
           Voornaam, 
           Tussenvoegsel, 
           Achternaam,
           Meisjesnaam, 
           Geslacht, 
           Telnr, 
           ExternID,
           Functie,
           Emailadres,
           Inlognaam,
   		   case KoppelenAlleAfdelingen when '1' then 1 when 'J' then 1 else 0 end
      from [PointImport$]

open accCursor

fetch next from accCursor into
    @department,
    @firstname,
    @middlename,
    @lastname,
    @maidenname,
    @gender,
    @phonenumber,
    @externid,
    @position,
    @emailaddress,
    @username,
    @pointalleafdelingen

set @line = 2
while @@FETCH_STATUS = 0
begin
    ------------------------------------------------------------------------------	
	set @cont = 1 -- Continue-boolean
	
	set @departmentid = 0
	select @departmentid = DepartmentID 
	  from Department
	 where Department.Name = @department
	   and Department.Inactive = 0
	   and DepartmentID in ( select DepartmentID 
	                           from Department
	                          where LocationID = @locationid )

    if isnull(@departmentid,0) = 0 
	begin
	    print 'FOUT' + char(9) + 'REGEL ' + cast(@line as varchar) + char(9) + 'Afdeling "' + @department + '" niet gevonden.'
	    print char(9) + char(9) + 'ZIS: ' + @externid + ', Achternaam: ' + @lastname
	    set @cont = 0
	end
	
	-- Users with empty ExternID will be imported
 	if exists ( select 1 from Employee where isnull(ExternID,'') = @externid and len(ltrim(rtrim(isnull(ExternID,'')))) > 0 and DepartmentID in ( select DepartmentID from Department where LocationID = @locationid ) )
	begin
	    print 'FOUT' + char(9) + 'REGEL ' + cast(@line as varchar) + char(9) + 'Medewerker met ZIS-ID "' + isnull(@externid,'') + '" bestaat al.' 
	    print char(9) + char(9) + 'ZIS: ' + isnull(@externid,'') + ', Achternaam: ' + @lastname
	    set @cont = 0
	end
	 
	 
	if exists ( select 1 from aspnet_users where LoweredUserName = (@usernameprefix + @username) )
	begin
	    print 'FOUT' + char(9) + 'REGEL ' + cast(@line as varchar) + char(9) + 'Gebruiker met username "' + (@usernameprefix + @username) + '" bestaat al.' 
	    print char(9) + char(9) + 'ZIS: ' + @externid + ', achternaam: ' + @lastname
	    set @cont = 0
	end

    ----- All ok, let's continue
    if @cont = 1
    begin
	
        ----- Create SALT as (random) string
        set @saltcount = 0
        set @saltchars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'
        set @salt = ''
        
        while @saltcount < 16
        begin
            set @salt = @salt + SUBSTRING(@saltchars,CAST(ABS(CHECKSUM(NEWID()))*RAND(@saltcount) as int)%LEN(@saltchars)+1,1)
            set @saltcount = @saltcount + 1
        end

        ----- Base64 the SALT
        set @base64pos = 1
        set @base64output = ''
        while @base64pos <= len(@salt) 
        begin
            set @base64bits = convert(varbinary(3), substring(@salt, @base64pos, 3))
            set @base64output = @base64output + substring('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/', substring(@base64bits, 1, 1) / 4 + 1, 1)
            set @base64output = @base64output + substring('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/', substring(@base64bits, 1, 1) % 4 * 16 + substring(@base64bits, 2, 1) / 16 + 1, 1)
            set @base64output = @base64output + substring('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/', substring(@base64bits, 2, 1) % 16 * 4 + substring(@base64bits, 3, 1) / 64 + 1, 1)
            set @base64output = @base64output + substring('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/', substring(@base64bits, 3, 1) % 64 + 1, 1)
            set @base64pos = @base64pos + 3
        end
        select @saltbase64 = (left(@base64output, len(@base64output) - 3 + len(@base64bits)) + replicate('=', 3 - len(@base64bits)))

        ----- Create PASSWORD as (random) string, in this case using the default password
        set @password = @defaultpassword 

        ----- Encode and Base64 the PASSWORD with the SALT
        declare @combi varbinary(512)
        set @combi = hashbytes('sha1',cast('' as  xml).value('xs:base64Binary(sql:variable(''@saltbase64''))','varbinary(256)') + convert(varbinary(256),N''+@password))
        set @passwordbase64 = cast('' as xml).value('xs:base64Binary(xs:hexBinary(sql:variable(''@combi'')))','varchar(64)')

        declare @UserID uniqueidentifier 
        declare @Now varchar(50)

        set @UserID = null
        set @username = (@usernameprefix + @username)
        set @Now = convert(varchar, getdate(),120)

		set @allorganizationdepartmentslinked = isnull(@pointalleafdelingen,0) 

        exec dbo.aspnet_Membership_CreateUser
              @ApplicationName = N'POINT', 
              @UserName = @username, 
              @Password = @passwordbase64,
              @PasswordSalt = @saltbase64, 
              @Email = NULL,
              @PasswordQuestion = NULL,
              @PasswordAnswer = NULL,
              @IsApproved = 1, 
              @UniqueEmail = 0, 
              @PasswordFormat = 1,   
              @CurrentTimeUtc = @Now, 
              @UserId = @UserID OUTPUT 

        --print 'INFO' + char(9) + 'REGEL ' + cast(@line as varchar) + char(9) + 'User aangemaakt met UserID ' + cast(@UserID as varchar(50))

        exec dbo.aspnet_UsersInRoles_AddUsersToRoles
              @ApplicationName = N'POINT', 
              @UserNames = @username, 
              @RoleNames = @defaultrole,
              @CurrentTimeUtc= @Now


        -- ROLLBACK TRAN
        declare @employeeId int
        set @employeeId = null
        set @gender = LOWER(@gender)
       
	INSERT INTO [Employee]
		(	[DepartmentID]
           ,[FirstName]
           ,[MiddleName]
           ,[LastName]
           ,[Gender]
           ,[Position]
           ,[PhoneNumber]
           ,[EmailAddress]
		   ,[ExternID]
		   ,[ChangePasswordByEmployee]
		   ,[BIG]
		   ,[ViewRights]
		   ,[AllOrganizationDepartmentsLinked]
		   ,[TimeStamp]
		   ,[ModifiedByEmployeeID]
		   ,[ScreenID]
		   )
     VALUES
		(	@DepartmentID,
			@FirstName,
			@MiddleName,
			@LastName + (case when isnull(@MaidenName,'') = '' then '' else '-' + @MaidenName end),
			@Gender,
			@Position,
			@PhoneNumber,
			@EmailAddress,
			@ExternID,
			@changepasswordbyemployee,
			NULL,
			0,
			@allorganizationdepartmentslinked,
			@Now,
			@modifiedbyemployeeid,
			@screenid				
			)

	SET @employeeID = SCOPE_IDENTITY()

        print 'INFO' + char(9) + 'REGEL ' + cast(@line as varchar) + char(9) + 'Medewerker aangemaakt met Gebruikersnaam ' + @username
        print char(9) + char(9) + 'ZIS: ' + @externid + ', Achternaam: ' + @lastname

		update Employee
		set	UserID		= @UserID,
			[TimeStamp]	= @Now,
			 @ModifiedByEmployeeID = @modifiedbyemployeeid,
			 @ScreenID = @screenid			
		where
			[EmployeeID] = @EmployeeID 
			
    end	 -- if @cont = 1
	
    set @line = @line + 1	
    print ''
    ------------------------------------------------------------------------------	
fetch next from accCursor into
    @department,
    @firstname,
    @middlename,
    @lastname,
    @maidenname,
    @gender,
    @phonenumber,
    @externid,
    @position,
    @emailaddress,
    @username,
    @pointalleafdelingen
    
end

close accCursor
deallocate accCursor