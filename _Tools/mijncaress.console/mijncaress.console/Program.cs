﻿using Hl7.Fhir.Model;
using Hl7.Fhir.Serialization;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;

namespace mijncaress.console
{
    public static class Extensions
    {
        public static bool TryParseJson<T>(this string obj, out T result)
        {
            try
            {
                JsonSerializerSettings settings = new JsonSerializerSettings();
                settings.MissingMemberHandling = MissingMemberHandling.Error;

                result = JsonConvert.DeserializeObject<T>(obj, settings);
                return true;
            }
            catch (Exception)
            {
                result = default(T);
                return false;
            }
        }
    }
     
    public class mijnCaressErrorResponse
    {
        public string ErrorMessage { get; set; }
        public string ReturnCode { get; set; }
    }

    public class Program
    {
        public static void Main(string[] args)
        {
            //201802011428
            //201802011427
            //var url = "https://cocreatieconnect.mijncaress.nl/restservices?path=/fhir/Patient&example=1";
            //var url = "https://cocreatieconnect.mijncaress.nl/fhir/Bundle";

            var get_request = create_get_request("https://cocreatieconnect.mijncaress.nl/fhir/Patient?identifier=201802011428",
                "helpdeskpoint", "H@ll0Po1nt", "Point2CoCreatie.pfx", "H@ll0Po1nt");
            get_pink_patient(get_request);

            var post_request = create_post_request("https://cocreatieconnect.mijncaress.nl/fhir/Bundle",
                "helpdeskpoint", "H@ll0Po1nt", "Point2CoCreatie.pfx", "H@ll0Po1nt");
            post_pink_patient(post_request);

            Console.WriteLine();
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }

        public static WebRequest create_get_request(string url, string username, string password, string certificate_path, string certificate_password)
        {
            HttpWebRequest request = null;
            try
            {

                request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "GET";
                request.Accept = "application/json";

                if (!string.IsNullOrEmpty(certificate_path))
                {
                    var cert = new X509Certificate2("Point2CoCreatie.pfx", "H@ll0Po1nt", X509KeyStorageFlags.PersistKeySet);
                    request.ClientCertificates.Add(cert);
                }

                if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password))
                {
                    //var username = "helpdeskpoint";
                    //var password = "H@ll0Po1nt";
                    var encoded = System.Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(username + ":" + password));
                    request.Headers.Add("Authorization", "Basic " + encoded);
                }
            }
            catch (CryptographicException ce)
            {
                Console.WriteLine(ce.ToString());
            }

            return request;
        }

        public static WebRequest create_post_request(string url, string username, string password, string certificate_path, string certificate_password)
        {
            HttpWebRequest request = null;
            try
            {
                request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "POST";
                request.Accept = "application/json";

                if (!string.IsNullOrEmpty(certificate_path))
                {
                    var cert = new X509Certificate2("Point2CoCreatie.pfx", "H@ll0Po1nt", X509KeyStorageFlags.PersistKeySet);
                    request.ClientCertificates.Add(cert);
                }

                //if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password))
                //{
                //    //var username = "helpdeskpoint";
                //    //var password = "H@ll0Po1nt";
                //    var encoded = System.Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(username + ":" + password));
                //    request.Headers.Add("Authorization", "Basic " + encoded);
                //}

            }
            catch (CryptographicException ce)
            {
                Console.WriteLine(ce.ToString());
            }

            return request;
        }

        public static void get_pink_patient(WebRequest webrequest)
        {
            if (webrequest == null)
            {
                Console.WriteLine($"{nameof(webrequest)} is invalid.");
            }

            try
            {
                var web_response = webrequest.GetResponse();
                var status = ((HttpWebResponse)web_response).StatusCode;

                using (var stream_reader = new StreamReader(web_response.GetResponseStream()))
                {
                    var result = stream_reader.ReadToEnd();

                    if (Extensions.TryParseJson<mijnCaressErrorResponse>(result, out mijnCaressErrorResponse errorresponse))
                    {
                        Console.WriteLine(errorresponse.ErrorMessage);
                    }
                    else
                    {
                        var parsersettings = new ParserSettings()
                        {
                            AcceptUnknownMembers = true
                        };
                        var parser = new FhirJsonParser(parsersettings);
                        var document = parser.Parse<Patient>(result);

                        Console.WriteLine($"Gender: {document?.Gender.Value}");
                        foreach (var name in document.Name)
                        {
                            Console.WriteLine($"Family: {name.Family}");
                            Console.WriteLine($"Given: {string.Join(" ", name.Given)}");
                        }
                        Console.WriteLine($"Birthdate: {document.BirthDate}");
                        Console.WriteLine(document.MaritalStatus.Text);

                        Console.WriteLine(result);
                    }
                }
            }
            catch (CryptographicException ce)
            {
                Console.WriteLine(ce.ToString());
            }
            catch (WebException we)
            {
                var httpwebresponse = (HttpWebResponse)we.Response;
                Console.WriteLine(we.ToString());
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        public static void post_pink_patient(WebRequest webrequest)
        {
            var test = create_fhir_hl7_patient();
            Console.WriteLine(test);

            using (var stream_writer = new StreamWriter(webrequest.GetRequestStream()))
            {
                string json = create_fhir_hl7_patient();
                stream_writer.Write(json);
                stream_writer.Flush();
                stream_writer.Close();
            }

            var web_response = (HttpWebResponse)webrequest.GetResponse();
            var status = ((HttpWebResponse)web_response).StatusCode;

            using (var stream_reader = new StreamReader(web_response.GetResponseStream()))
            {
                var result = stream_reader.ReadToEnd();

                if (Extensions.TryParseJson<mijnCaressErrorResponse>(result, out mijnCaressErrorResponse errorresponse))
                {
                    Console.WriteLine(errorresponse.ErrorMessage);
                }
                else
                {
                    Console.WriteLine(result);
                }
            }
        }

        public static string create_fhir_hl7_patient()
        {
            // patient
            var urn_patient = new Uri($"urn:uuid:{Guid.NewGuid().ToString()}");
            var patient = new Patient();
            var id = new Identifier()
            {
                Use = Identifier.IdentifierUse.Official,
                System = "http://fhir.nl/fhir/NamingSystem/bsn",
                Value = "118137426"
            };
            patient.Identifier.Add(id);

            patient.Meta = new Meta();
            patient.Meta.ProfileElement.Add(new Canonical("http://zorgdomein.nl/fhir/StructureDefinition/zd-patient"));

            var name = new HumanName().WithGiven("Phillip").AndFamily("Partridge");
            name.Prefix = new string[] { "Mr." };
            name.Use = HumanName.NameUse.Official;
            patient.Name.Add(name);

            patient.Gender = AdministrativeGender.Male;
            patient.BirthDate = "1975-01-01";
            patient.Deceased = new FhirBoolean(false);

            var birthplace = new Extension()
            {
                Url = "http://hl7.org/fhir/StructureDefinition/birthPlace",
                Value = new Address() { City = "Hoofddorp" }
            };
            patient.Extension.Add(birthplace);

            var birthtime = new Extension("http://hl7.org/fhir/StructureDefinition/patient-birthTime",
                                           new FhirDateTime(1975, 1, 1, 4, 4));
            patient.BirthDateElement.Extension.Add(birthtime);

            var address = new Address()
            {
                Line = new string[] { "Kruisweg 823 C,2132 NG, Hoofddorp" },
                City = "Hoofddorp",
                State = "NH",
                PostalCode = "2132 NG",
                Country = "NL"
            };
            patient.Address.Add(address);

            // bundle
            var urn_bundle = new Uri($"urn:uuid:{Guid.NewGuid().ToString()}");
            var bundle = new Bundle()
            {
                Type = Bundle.BundleType.Document,
                Identifier = new Identifier("http://zorgdomein.nl/fhir/identifier/local", urn_bundle.ToString()),
            };

            var composition = new Composition();
            composition.Subject = new ResourceReference()
            {
                Reference = urn_patient.ToString(),
                Display = patient.Name.ToString()
            };
            composition.Date = DateTime.Now.ToString();

            var afspraakbericht = "Afspraakbericht";
            var composition_type = new CodeableConcept();
            var composition_class = new CodeableConcept();
            var afspraakbericht_coding = new Coding() { Display = afspraakbericht };
            composition.Status = CompositionStatus.Final;
            composition_type.Coding.Add(afspraakbericht_coding);
            composition_class.Coding.Add(afspraakbericht_coding);
            //composition.Title = "Afspraakbericht - ZP10002466"

            bundle.AddResourceEntry(patient, urn_patient.ToString());

            return bundle.ToJson();
        }
    }
}
