-- ID's zijn gelijk op A en P (03-10-2016)

SELECT  *
FROM    Organization
WHERE   OrganizationTypeID = 1
        AND Name LIKE '%Alrijne zorggroep Leiden%'; --> 6387

SELECT  *
FROM    Location
WHERE   OrganizationID = 6387--> 'Locatie Leiden' = 6409

SELECT  *
FROM    Organization
WHERE   OrganizationTypeID = 1
        AND Name LIKE '%Alrijne Zorggroep Leiderdorp & Alphen%'; --> 6386

UPDATE  Location
SET     OrganizationID = 6386
WHERE   LocationID = 6409

PRINT 'Done.';