-- ID's zijn gelijk op A en P (06-10-2016)

SELECT  *
FROM    Organization
WHERE   OrganizationTypeID = 1
        AND Name LIKE '%Alrijne zorggroep Leiden%'; --> 6387
SELECT  *
FROM    Organization
WHERE   OrganizationTypeID = 1
        AND Name LIKE '%Alrijne Zorggroep Leiderdorp & Alphen%'; --> 6386

SELECT  DoctorID
FROM    Doctor
WHERE   OrganizationID = 6387;
PRINT 'DoctorIDs currently connected to Alrijne zorggroep Leiden.';

UPDATE  Doctor
SET     OrganizationID = 6386
WHERE   OrganizationID = 6387;

PRINT 'Done.';