DISABLE TRIGGER Client.Client_InsertUpdate ON Client;
GO

IF OBJECT_ID(N'tempdb..#AlrijneLeiden') IS NOT NULL
    DROP TABLE #AlrijneLeiden;

CREATE TABLE #AlrijneLeiden
    (
      OLD VARCHAR(50) ,
      NEW VARCHAR(50)
    );
GO

CREATE INDEX i1 ON #AlrijneLeiden(OLD) INCLUDE (NEW)
GO

PRINT 'Bulk insert d:\deployments\import db\Omnummer bestand Leiden.csv';

SET NOCOUNT ON;

BULK
INSERT #AlrijneLeiden
FROM 'd:\deployments\import db\Omnummer bestand Leiden.csv'
WITH
(
FIRSTROW = 2,
FIELDTERMINATOR = ';',
ROWTERMINATOR = '\n'
);
GO

SET NOCOUNT ON;

DECLARE @organizationname VARCHAR(255) = 'Alrijne zorggroep Leiden';
DECLARE @clientid INT;
DECLARE @patientnumber AS VARCHAR(50);
DECLARE @OLD VARCHAR(50);
DECLARE @NEW VARCHAR(50);
DECLARE @DESTINATION VARCHAR(50);

DECLARE clientCursor CURSOR
FOR
    SELECT  Client.ClientID ,
            Client.PatientNumber
    FROM    Client WITH ( NOLOCK )
            JOIN Transfer WITH ( NOLOCK ) ON Client.ClientID = Transfer.ClientID
            JOIN Department WITH ( NOLOCK ) ON Transfer.DepartmentID = Department.DepartmentID
            JOIN Location WITH ( NOLOCK ) ON Department.LocationID = Location.LocationID
            JOIN Organization WITH ( NOLOCK ) ON Organization.OrganizationID = Location.OrganizationID
    WHERE   Organization.Name = @organizationname
            AND Organization.Inactive = 0;

OPEN clientCursor;

PRINT 'Patienten omnummering ' + @organizationname;

FETCH NEXT FROM clientCursor INTO @clientid, @patientnumber;

WHILE @@FETCH_STATUS = 0
    BEGIN

        SET @DESTINATION = '';
        SELECT  @DESTINATION = NEW
        FROM    #AlrijneLeiden
        WHERE   OLD = @patientnumber;

        IF EXISTS ( SELECT  1
                    FROM    #AlrijneLeiden
                    WHERE   OLD = @patientnumber)
            BEGIN

                UPDATE  Client
                SET     Client.PatientNumber = @DESTINATION
                WHERE   Client.ClientID = @clientid
                        AND Client.PatientNumber = @patientnumber;

                PRINT 'Patientnumber old ' + @patientnumber + ' '
                    + 'Patientnumber new ' + @DESTINATION + ' ClientID '
                    + STR(@clientid);
            END;
    
        FETCH NEXT FROM clientCursor INTO @clientid, @patientnumber;
    END;

CLOSE clientCursor;
DEALLOCATE clientCursor;

PRINT 'Done!';
GO

ENABLE TRIGGER Client.Client_InsertUpdate ON Client;

SET NOCOUNT OFF;