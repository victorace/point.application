PBIs / Tasks connected to this are (13829, 13830, 13831)

Steps

*Deliver <filename>.csv to ProAct, so that it can be placed on the server*

*Update 2_Alrijne_Leiden.sql and 3_Alrijne_Leiderdorp.sql (with the correct path for the <filename>.csv file)*

SQL:

1_Create backups.sql
2_Alrijne_Leiden.sql
3_Alrijne_Leiderdorp.sql
4_Alrijne_Leiderdorp_ZIS.sql
5_Location Alrijne zorggroep Leiden To Alrijne Zorggroep Leiderdorp & Alphen.sql
6_Doctors_Alrijne zorggroep Leiden To Alrijne Zorggroep Leiderdorp & Alphen.sql