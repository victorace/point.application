DECLARE @tablename AS VARCHAR(50);
DECLARE @command AS VARCHAR(255);

PRINT 'Creating backups.';

SET @tablename = CONCAT('Client', FORMAT(GETDATE(), 'yyyyMMddHHmmss', 'nl-NL'));

IF NOT ( EXISTS ( SELECT    *
                  FROM      sysobjects
                  WHERE     xtype = 'U'
                            AND name = @tablename ) )
    BEGIN

        SET @command = 'SELECT * INTO ' + @tablename + ' FROM dbo.Client;';
        EXEC(@command);

        PRINT 'Client backup made to ' + @tablename;

    END;

SET @tablename = CONCAT('Employee',
                        FORMAT(GETDATE(), 'yyyyMMddHHmmss', 'nl-NL'));

IF NOT ( EXISTS ( SELECT    *
                  FROM      sysobjects
                  WHERE     xtype = 'U'
                            AND name = @tablename ) )
    BEGIN

        SET @command = 'SELECT * INTO ' + @tablename + ' FROM dbo.Employee;';
        EXEC(@command);

        PRINT 'Employee backup made to ' + @tablename;

    END;

SET @tablename = CONCAT('Doctor',
                        FORMAT(GETDATE(), 'yyyyMMddHHmmss', 'nl-NL'));

IF NOT ( EXISTS ( SELECT    *
                  FROM      sysobjects
                  WHERE     xtype = 'U'
                            AND name = @tablename ) )
    BEGIN

        SET @command = 'SELECT * INTO ' + @tablename + ' FROM dbo.Doctor;';
        EXEC(@command);

        PRINT 'Doctor backup made to ' + @tablename;

    END;

SET @tablename = CONCAT('Location',
                        FORMAT(GETDATE(), 'yyyyMMddHHmmss', 'nl-NL'));

IF NOT ( EXISTS ( SELECT    *
                  FROM      sysobjects
                  WHERE     xtype = 'U'
                            AND name = @tablename ) )
    BEGIN

        SET @command = 'SELECT * INTO ' + @tablename + ' FROM dbo.Location;';
        EXEC(@command);

        PRINT 'Location backup made to ' + @tablename;

    END;


PRINT 'Done.';