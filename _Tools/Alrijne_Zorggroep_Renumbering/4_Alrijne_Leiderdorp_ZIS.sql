IF OBJECT_ID(N'tempdb..#AlrijneLeiderdorpZIS', N'U') IS NOT NULL
    DROP TABLE #AlrijneLeiderdorpZIS;

CREATE TABLE #AlrijneLeiderdorpZIS
    (
      OLD VARCHAR(50) ,
      NEW VARCHAR(50)
    );
GO

CREATE INDEX i1 ON #AlrijneLeiderdorpZIS(OLD) INCLUDE (NEW)
GO

INSERT  INTO #AlrijneLeiderdorpZIS
        ( OLD, NEW )
VALUES  ( 'DCOENR', '22' ),
        ( 'IVELZ2', '789' ),
        ( 'GUnen', '1930' ),
        ( 'pstijn', '4770' ),
        ( 'JKodde', '11210' ),
        ( 'MBEELE', '12370' ),
        ( 'AHoogk', '26680' ),
        ( 'TVerha', '33170' ),
        ( 'SPeeks', '34080' ),
        ( 'LFriss', '40110' ),
        ( 'MWarme', '49880' ),
        ( 'mbodij', '50230' ),
        ( 'JLeeu8', '67910' ),
        ( 'NVLIET', '69260' ),
        ( 'inoest', '70800' ),
        ( 'FSchot', '75760' ),
        ( 'VSchel', '75810' ),
        ( 'IDittm', '75830' ),
        ( 'SMomme', '76850' ),
        ( 'EApeld', '76910' ),
        ( 'LBerkh', '78420' ),
        ( 'LHART2', '79420' ),
        ( 'EBouwm', '87670' ),
        ( 'DBRAAM', '100370' ),
        ( 'fberg', '101620' ),
        ( 'avliet', '102190' ),
        ( 'LVerdo', '103340' ),
        ( 'Lpiet4', '104040' ),
        ( 'EHorst', '104740' ),
        ( 'IKeize', '104760' ),
        ( 'GMulde', '104780' ),
        ( 'VWIT', '105570' ),
        ( 'MHerto', '106560' ),
        ( 'AElst', '106570' ),
        ( 'JPOST', '107150' ),
        ( 'CMEER6', '107790' ),
        ( 'IGROEN', '108740' ),
        ( 'LKENNI', '108920' ),
        ( 'TSTEIJ', '109060' ),
        ( 'ezon', '109170' ),
        ( 'CEGMON', '109420' ),
        ( 'DHORST', '109900' ),
        ( 'DBoume', '110190' ),
        ( 'ILOS', '111410' ),
        ( 'EKarac', '111710' ),
        ( 'EHAM', '111730' ),
        ( 'LEijk', '112210' ),
        ( 'MNoor3', '112240' ),
        ( 'MJANMA', '112360' ),
        ( 'GBORKO', '112760' ),
        ( 'SSlof', '113010' ),
        ( 'SWIT', '113660' ),
        ( 'JSTUTT', '114960' ),
        ( 'EKOPPE', '115040' ),
        ( 'LLelie', '115200' ),
        ( 'LBEERL', '117180' ),
        ( 'SSTIEN', '117530' ),
        ( 'SLee', '182900' ),
        ( 'RNoord', '185500' ),
        ( 'MLEK', '202750' ),
        ( 'SHUMER', '205390' ),
        ( 'SPLOEG', '205580' ),
        ( 'EBOSMA', '205980' ),
        ( 'LTESKE', '207460' ),
        ( 'WVERBO', '207630' ),
        ( 'MStrui', '226210' ),
        ( 'KObbem', '226290' ),
        ( 'YVerbe', '240140' ),
        ( 'MRoerd', '242030' ),
        ( 'rsluis', '251655' ),
        ( 'SJans2', '251670' ),
        ( 'CVrie1', '409600' ),
        ( 'BBIJL', '421600' ),
        ( 'CLAMER', '471900' ),
        ( 'KCOMBE', '480280' ),
        ( 'MLEEU3', '506500' ),
        ( 'WSERLI', '640500' ),
        ( 'APREMC', '670900' ),
        ( 'ADEKKE', '689700' ),
        ( 'LERFTE', '825500' ),
        ( 'JVINCO', '889300' ),
        ( 'DZAALB', '982400' ),
        ( 'JROMIJ', '2402' ),
        ( 'agroo2', '2920' ),
        ( 'mmeye2', '9430' ),
        ( 'cwit2', '9890' ),
        ( 'kvries', '14460' ),
        ( 'jsiera', '14480' ),
        ( 'mscha4', '19750' ),
        ( 'pevers', '24260' ),
        ( 'ebruin', '30200' ),
        ( 'bvooys', '34380' ),
        ( 'mparle', '40570' ),
        ( 'jrhijn', '47180' ),
        ( 'mdroo2', '52980' ),
        ( 'jdroo2', '61340' ),
        ( 'agroo3', '62920' ),
        ( 'lkuij2', '69900' ),
        ( 'ekatwi', '70740' ),
        ( 'JHOOG3', '78860' ),
        ( 'FGroo2', '84290' ),
        ( 'LELSEN', '93630' ),
        ( 'AUitho', '96420' ),
        ( 'MMeer2', '97510' ),
        ( 'JOlger', '103360' ),
        ( 'MHoek', '103440' ),
        ( 'EKUROW', '107060' ),
        ( 'AZEEGE', '109290' ),
        ( 'WBAAIJ', '110710' ),
        ( 'DHOEF', '110790' ),
        ( 'MASPER', '110920' ),
        ( 'LDELFT', '112790' ),
        ( 'FSNOEP', '112930' ),
        ( 'JNIEU2', '114420' ),
        ( 'IOTTEN', '114600' ),
        ( 'Lkuive', '114620' ),
        ( 'RBAKKE', '119360' ),
        ( 'JKUIJP', '120770' ),
        ( 'CDROP', '122070' ),
        ( 'ADONK', '190900' ),
        ( 'CCOLLA', '201660' ),
        ( 'BNIJS', '204300' ),
        ( 'KSCHOE', '204820' ),
        ( 'EWILTS', '205170' ),
        ( 'TLEEUW', '206730' ),
        ( 'DSchuu', '226365' ),
        ( 'Lkruk', '251050' ),
        ( 'Jdelfo', '251205' ),
        ( 'VJEGAN', '251400' ),
        ( 'CHANEN', '303500' ),
        ( 'CPLAS', '312600' ),
        ( 'HIREEU', '383300' ),
        ( 'RKOOIS', '462200' ),
        ( 'mruite', '606100' ),
        ( 'MSNOEK', '611400' ),
        ( 'RPOULA', '669000' ),
        ( 'TVEN', '782000' ),
        ( 'DWASSE', '927800' ),
        ( 'KGROMM', '931800' ),
        ( 'ADEGEL', '301' ),
        ( 'MHAVEN', '1005' ),
        ( 'APloe2', '1580' ),
        ( 'JGUIJT', '2500' ),
        ( 'mbregm', '2830' ),
        ( 'MHARBE', '4110' ),
        ( 'NKEIJ2', '4328' ),
        ( 'ELoo', '6900' ),
        ( 'cgraaf', '14100' ),
        ( 'rnoort', '26770' ),
        ( 'jbeele', '29850' ),
        ( 'cdorp', '32620' ),
        ( 'jhilge', '35960' ),
        ( 'HBOER', '54480' ),
        ( 'tgroen', '55630' ),
        ( 'mkwakk', '59050' ),
        ( 'fverku', '61160' ),
        ( 'cherwa', '61680' ),
        ( 'mveld2', '66530' ),
        ( 'VImtho', '66670' ),
        ( 'alinde', '72350' ),
        ( 'mbloem', '73700' ),
        ( 'iuitho', '75700' ),
        ( 'GDuijv', '76250' ),
        ( 'SBruin', '77180' ),
        ( 'MHend2', '80030' ),
        ( 'YBoer2', '81300' ),
        ( 'SDEKKE', '81820' ),
        ( 'stijst', '83340' ),
        ( 'Cstolw', '83820' ),
        ( 'AMaagd', '85550' ),
        ( 'TBeele', '90240' ),
        ( 'HDonge', '90330' ),
        ( 'lmolen', '90820' ),
        ( 'mgrisn', '91400' ),
        ( 'twout', '93220' ),
        ( 'IHaas2', '93750' ),
        ( 'MPost', '94390' ),
        ( 'TWekke', '96750' ),
        ( 'EBleke', '103450' ),
        ( 'MTromp', '103480' ),
        ( 'bcats', '104250' ),
        ( 'ileeu2', '104490' ),
        ( 'EOSKAM', '104980' ),
        ( 'RMEIJE', '105050' ),
        ( 'MSIP', '106830' ),
        ( 'NDalem', '106910' ),
        ( 'csteen', '106950' ),
        ( 'agils', '107010' ),
        ( 'PWibbe', '107300' ),
        ( 'RZwart', '107610' ),
        ( 'AMOL', '107810' ),
        ( 'LRUTGR', '107960' ),
        ( 'WLOGT', '108040' ),
        ( 'RWEENE', '108560' ),
        ( 'ENIGTE', '108640' ),
        ( 'LHAAST', '108780' ),
        ( 'DBLEEK', '108850' ),
        ( 'LGRISN', '108860' ),
        ( 'MManne', '109160' ),
        ( 'LLARSE', '110090' ),
        ( 'RKAMPE', '110730' ),
        ( 'JAalbe', '111370' ),
        ( 'MHoexu', '111460' ),
        ( 'EDUNKI', '111640' ),
        ( 'GWELS', '112020' ),
        ( 'GRidde', '112200' ),
        ( 'ARIJSB', '112750' ),
        ( 'EVijve', '112870' ),
        ( 'RVRIJL', '112910' ),
        ( 'AMOOI3', '113020' ),
        ( 'SWIT', '113660' ),
        ( 'CRoos', '113700' ),
        ( 'IKAMER', '114000' ),
        ( 'KOYEN', '114830' ),
        ( 'Jslee', '115840' ),
        ( 'VWEENE', '116800' ),
        ( 'TWEENI', '116870' ),
        ( 'IBernt', '117020' ),
        ( 'EDonse', '117260' ),
        ( 'GHORSM', '117370' ),
        ( 'SSTIEN', '117530' ),
        ( 'CBRAAK', '118800' ),
        ( 'abramm', '118900' ),
        ( 'BVLIET', '198800' ),
        ( 'mdijk2', '201740' ),
        ( 'ATURK', '202060' ),
        ( 'WKROON', '203420' ),
        ( 'CREEUW', '204160' ),
        ( 'EVERHO', '204380' ),
        ( 'MMOLLE', '204530' ),
        ( 'eeeden', '205740' ),
        ( 'avos', '206240' ),
        ( 'JBerg', '206640' ),
        ( 'GVERHE', '206670' ),
        ( 'DHendr', '206680' ),
        ( 'SZWAAK', '206860' ),
        ( 'LVis', '206970' ),
        ( 'LRijn', '240380' ),
        ( 'CFrans', '245600' ),
        ( 'ebeek2', '251415' ),
        ( 'nlauwe', '251420' ),
        ( 'lturke', '251425' ),
        ( 'Ldrabb', '251430' ),
        ( 'MSMAN', '261500' ),
        ( 'ARASEN', '294500' ),
        ( 'MNEIJS', '309800' ),
        ( 'LES2', '315100' ),
        ( 'NHOF', '348400' ),
        ( 'AJEU', '399900' ),
        ( 'hbeuse', '442700' ),
        ( 'hkoele', '459400' ),
        ( 'umougi', '490800' ),
        ( 'ELOOF', '528200' ),
        ( 'EVERWO', '611100' ),
        ( 'ROord', '623100' ),
        ( 'HLABOT', '635800' ),
        ( 'SROLIN', '702300' ),
        ( 'aversc', '734700' ),
        ( 'asmits', '774600' ),
        ( 'LSteen', '797500' ),
        ( 'mveen', '856400' ),
        ( 'WWerff', '939500' ),
        ( 'HSpier', '620' ),
        ( 'awasse', '993' ),
        ( 'ATrom2', '2600' ),
        ( 'MMULDE', '5870' ),
        ( 'npolde', '7080' ),
        ( 'MElder', '9470' ),
        ( 'GIters', '11050' ),
        ( 'EBooga', '13210' ),
        ( 'SLans', '13430' ),
        ( 'SJonge', '15550' ),
        ( 'JVerdo', '24440' ),
        ( 'LZUIDM', '25610' ),
        ( 'NCarpe', '46460' ),
        ( 'tverdo', '54420' ),
        ( 'EVreug', '63960' ),
        ( 'dbent', '68040' ),
        ( 'PSCHOU', '78800' ),
        ( 'SKADE', '79740' ),
        ( 'ARijn3', '84210' ),
        ( 'ESMOOR', '84410' ),
        ( 'LVleug', '84440' ),
        ( 'CPlug', '90120' ),
        ( 'sstrat', '90740' ),
        ( 'MVLIET', '90810' ),
        ( 'atamer', '93790' ),
        ( 'dwess2', '95280' ),
        ( 'mtille', '95690' ),
        ( 'GOERS', '97500' ),
        ( 'LPelte', '102470' ),
        ( 'JHOOGE', '103270' ),
        ( 'MHeilk', '103430' ),
        ( 'GBRUIN', '107410' ),
        ( 'PLOOIJ', '108200' ),
        ( 'BMOOR', '109640' ),
        ( 'LJONG3', '109650' ),
        ( 'NHAAS1', '110300' ),
        ( 'SPEN', '112900' ),
        ( 'Iroden', '113560' ),
        ( 'DBLOEM', '115510' ),
        ( 'EZWAAR', '116440' ),
        ( 'MLINDE', '119430' ),
        ( 'KNOTME', '119490' ),
        ( 'MBROWN', '133700' ),
        ( 'GBIJL', '153000' ),
        ( 'PCoene', '160000' ),
        ( 'JAMMER', '204520' ),
        ( 'EOEVER', '206630' ),
        ( 'TWUBBE', '220200' ),
        ( 'MOOSTR', '241770' ),
        ( 'aduijv', '250875' ),
        ( 'shenze', '251490' ),
        ( 'WPEELE', '278200' ),
        ( 'PGROOT', '285200' ),
        ( 'JHARG', '305300' ),
        ( 'ANELEM', '343800' ),
        ( 'fpitte', '654000' ),
        ( 'MPOELW', '660000' ),
        ( 'PBERGM', '698500' ),
        ( 'MVERBI', '926600' ),
        ( 'YWIT', '966800' ),
        ( 'IBRAND', '972600' ),
        ( 'IZORGE', '991000' ),
        ( 'MHORST', '14020' ),
        ( 'CBONTJ', '95500' ),
        ( 'ETIEM', '100450' ),
        ( 'KBURGM', '105990' ),
        ( 'HWEERH', '106880' ),
        ( 'MZONNE', '107210' ),
        ( 'AROOYE', '109410' ),
        ( 'NVELDE', '109980' ),
        ( 'ABERKE', '116640' ),
        ( 'HLOON', '141100' ),
        ( 'BJONG2', '145900' ),
        ( 'JPANJE', '204800' ),
        ( 'YHENZE', '205370' ),
        ( 'EWENT', '205990' ),
        ( 'ATURKE', '206330' ),
        ( 'SWIERI', '206690' ),
        ( 'JDIJK3', '212300' ),
        ( 'HSCHOO', '226010' ),
        ( 'AKLINK', '283100' ),
        ( 'JBRUIN', '292500' ),
        ( 'JHOEK2', '344200' ),
        ( 'MKROFT', '420200' ),
        ( 'MVEURI', '443700' ),
        ( 'MPost2', '459800' ),
        ( 'AWELBO', '481000' ),
        ( 'EFRANS', '512000' ),
        ( 'PRUS', '513400' ),
        ( 'MMILTE', '573000' ),
        ( 'EGEEL', '616200' ),
        ( 'CROSSE', '620400' ),
        ( 'LPIET3', '651100' ),
        ( 'DQUALM', '675700' ),
        ( 'JPAUL', '676200' ),
        ( 'ADUYN', '768600' ),
        ( 'NSLUMA', '771200' ),
        ( 'DZIMME', '987600' ),
        ( 'RCOCK', '83800' ),
        ( 'MVLIE3', '100860' ),
        ( 'BDULK', '117230' ),
        ( 'MMOLEN', '577500' ),
        ( 'KKeste', '3410' ),
        ( 'IWipki', '4140' ),
        ( 'LJongb', '4540' ),
        ( 'AWater', '4900' ),
        ( 'RStund', '9130' ),
        ( 'iblank', '9870' ),
        ( 'RZwets', '9880' ),
        ( 'TZwart', '9990' ),
        ( 'PDoets', '11490' ),
        ( 'GBlois', '12820' ),
        ( 'dschmi', '17780' ),
        ( 'FDam', '22040' ),
        ( 'MBuite', '28180' ),
        ( 'ERomij', '29920' ),
        ( 'MVarke', '33710' ),
        ( 'MDuij1', '34090' ),
        ( 'AVarke', '39780' ),
        ( 'SStaal', '49730' ),
        ( 'MVoorh', '66650' ),
        ( 'ROomen', '67900' ),
        ( 'EPoel1', '71030' ),
        ( 'dmeije', '90320' ),
        ( 'WGERRI', '107240' ),
        ( 'MBOS', '140200' ),
        ( 'mdekke', '176800' ),
        ( 'DBOSCH', '296900' ),
        ( 'tkrans', '368400' ),
        ( 'hlans', '564900' ),
        ( 'CMAIER', '679300' ),
        ( 'ARUSMA', '713500' ),
        ( 'mmorel', '1010' ),
        ( 'jryn', '1910' ),
        ( 'nstoja', '4400' ),
        ( 'jkoel', '5090' ),
        ( 'dzoete', '10780' ),
        ( 'kpeete', '27540' ),
        ( 'fwarme', '50400' ),
        ( 'rbent', '68570' ),
        ( 'lduijv', '69070' ),
        ( 'aduijn', '70810' ),
        ( 'WBIERE', '72220' ),
        ( 'NTukke', '76050' ),
        ( 'lprice', '76650' ),
        ( 'iolyer', '90700' ),
        ( 'idoffe', '93620' ),
        ( 'Lboeke', '94920' ),
        ( 'merkel', '95110' ),
        ( 'hvolle', '98930' ),
        ( 'sveer', '105770' ),
        ( 'mhaas2', '105780' ),
        ( 'MVEER2', '108630' ),
        ( 'CMIDDE', '110720' ),
        ( 'ARAAPH', '110760' ),
        ( 'ASELMA', '122360' ),
        ( 'MMAN', '206760' ),
        ( 'PENGEL', '225600' ),
        ( 'MRieme', '240480' ),
        ( 'nzouwe', '250065' ),
        ( 'Cverdo', '251125' ),
        ( 'EZEEUW', '336200' ),
        ( 'LJONG', '454000' ),
        ( 'MHENDR', '905400' ),
        ( 'MRIJN3', '971' ),
        ( 'wruit2', '40060' ),
        ( 'LBROER', '81140' ),
        ( 'FSCHIJ', '89200' ),
        ( 'LDAVID', '106650' ),
        ( 'GAKKER', '107730' ),
        ( 'MHAASN', '107900' ),
        ( 'SHensb', '107930' ),
        ( 'PBOSKE', '108100' ),
        ( 'MBY', '110770' ),
        ( 'ABONNE', '111780' ),
        ( 'DVERRA', '111940' ),
        ( 'PDAM', '115600' ),
        ( 'HVIS', '123230' ),
        ( 'CHEEMS', '124210' ),
        ( 'JCAMPE', '153800' ),
        ( 'CBRAND', '156700' ),
        ( 'PBRUIN', '165700' ),
        ( 'KSCHEE', '167400' ),
        ( 'DLAARO', '204900' ),
        ( 'MBOSM3', '205950' ),
        ( 'PBEIJE', '212570' ),
        ( 'BHAAR', '213770' ),
        ( 'AGROOT', '219470' ),
        ( 'BELFER', '219900' ),
        ( 'SSonme', '241240' ),
        ( 'MLAMBO', '274300' ),
        ( 'PHAAN', '292400' ),
        ( 'HHOFME', '350600' ),
        ( 'AHOUT', '368000' ),
        ( 'NKEULE', '432100' ),
        ( 'FZEEST', '498400' ),
        ( 'VLUMME', '533500' ),
        ( 'cmaat', '538200' ),
        ( 'PAMSTE', '611300' ),
        ( 'APALEN', '640600' ),
        ( 'MBLOKL', '879600' ),
        ( 'QBIEMO', '880500' ),
        ( 'JWAALS', '922700' ),
        ( 'ADIJK', '983600' ),
        ( 'SHooge', '2590' ),
        ( 'HHaasn', '3790' ),
        ( 'MKolk', '4820' ),
        ( 'MPlas4', '5850' ),
        ( 'BStrav', '6000' ),
        ( 'KBerkh', '6820' ),
        ( 'HBROEK', '6870' ),
        ( 'MRuit2', '7280' ),
        ( 'AKuike', '7340' ),
        ( 'Wdoffe', '7500' ),
        ( 'AVink1', '7980' ),
        ( 'AGraa2', '10810' ),
        ( 'LKuijt', '12730' ),
        ( 'ESouve', '13310' ),
        ( 'EHooge', '13840' ),
        ( 'CHooge', '13920' ),
        ( 'MDanie', '22090' ),
        ( 'CJong', '22110' ),
        ( 'DMol', '28500' ),
        ( 'BVeek', '31360' ),
        ( 'PZwer2', '34270' ),
        ( 'CBerne', '49980' ),
        ( 'sgijsm', '54790' ),
        ( 'TDriel', '57070' ),
        ( 'Atromp', '58950' ),
        ( 'NMeer1', '59770' ),
        ( 'LHerto', '61020' ),
        ( 'FVerku', '61160' ),
        ( 'MTol3', '61670' ),
        ( 'DSchaa', '61890' ),
        ( 'BKool', '62670' ),
        ( 'CZwet', '62940' ),
        ( 'egerli', '62980' ),
        ( 'MKromh', '64280' ),
        ( 'KWinke', '65730' ),
        ( 'WGieze', '68550' ),
        ( 'JPater', '68610' ),
        ( 'LTwisk', '68620' ),
        ( 'EBent', '68630' ),
        ( 'ASied1', '68640' ),
        ( 'HPoelm', '68750' ),
        ( 'eklok', '69120' ),
        ( 'DVisse', '69220' ),
        ( 'RBERG', '69360' ),
        ( 'AKonin', '75030' ),
        ( 'KEgmon', '75340' ),
        ( 'SKade', '79740' ),
        ( 'JTHIJS', '87560' ),
        ( 'ALigt2', '90220' ),
        ( 'LRuit2', '90460' ),
        ( 'JArend', '90930' ),
        ( 'HHeese', '93200' ),
        ( 'Mpostm', '94240' ),
        ( 'CPLUG2', '95680' ),
        ( 'Jscho2', '95780' ),
        ( 'bvelde', '96920' ),
        ( 'mjonge', '98920' ),
        ( 'jakerb', '99990' ),
        ( 'vgoede', '100180' ),
        ( 'AHeens', '104000' ),
        ( 'JSMiT', '105290' ),
        ( 'TJong', '106490' ),
        ( 'LROMKE', '107250' ),
        ( 'MLAARH', '107740' ),
        ( 'HDOLDE', '108000' ),
        ( 'ELEEU4', '108020' ),
        ( 'ESOEST', '108970' ),
        ( 'ERUMPT', '109630' ),
        ( 'DSLOF', '109810' ),
        ( 'ADuij3', '110740' ),
        ( 'DARBOU', '111580' ),
        ( 'jsasse', '111600' ),
        ( 'JSMALL', '112110' ),
        ( 'ISTIJG', '112480' ),
        ( 'AWEEDA', '112970' ),
        ( 'bhendr', '114450' ),
        ( 'CBERG3', '114930' ),
        ( 'SRIJNB', '114970' ),
        ( 'MDIJK5', '118210' ),
        ( 'arippe', '118460' ),
        ( 'MWINTE', '119500' ),
        ( 'CHOOG3', '120190' ),
        ( 'AHuyts', '120600' ),
        ( 'AVALK', '120740' ),
        ( 'ARAMKI', '122390' ),
        ( 'AMAHIE', '122900' ),
        ( 'NMATZE', '204830' ),
        ( 'WBOUDE', '205050' ),
        ( 'EWENT', '205990' ),
        ( 'SJONG3', '206850' ),
        ( 'ITroos', '240590' ),
        ( 'LLimpt', '240900' ),
        ( 'PKempe', '241760' ),
        ( 'CROODE', '252290' ),
        ( 'AGEERL', '253200' ),
        ( 'AGROFS', '282000' ),
        ( 'EKALSE', '417100' ),
        ( 'MHEEMS', '538100' ),
        ( 'MHARTO', '561500' ),
        ( 'LLELIV', '625100' ),
        ( 'IOLDEN', '848600' ),
        ( 'PUBINK', '926700' ),
        ( 'WKAPTE', '950800' ),
        ( 'WWIJSM', '980200' ),
        ( 'MHarme', '462' ),
        ( 'NVink', '12230' ),
        ( 'MWolf', '20810' ),
        ( 'CRooij', '76700' ),
        ( 'IKruit', '88400' ),
        ( 'IKeijz', '97600' ),
        ( 'EBosch', '106100' ),
        ( 'WCoenr', '106930' ),
        ( 'ALindh', '108240' ),
        ( 'SBaas', '108980' ),
        ( 'SVolke', '111320' ),
        ( 'AWaal', '112180' ),
        ( 'FBoogm', '112680' ),
        ( 'KAmmer', '177000' ),
        ( 'SDelme', '205010' ),
        ( 'SFluit', '205060' ),
        ( 'ECapte', '205070' ),
        ( 'RDemme', '205100' ),
        ( 'RPlant', '257800' ),
        ( 'IHogew', '352400' ),
        ( 'JChang', '375400' ),
        ( 'JBeek3', '570700' ),
        ( 'MMorge', '584600' ),
        ( 'EPynac', '675300' ),
        ( 'NBrumm', '702800' ),
        ( 'IMeer', '711300' ),
        ( 'AStolw', '808600' ),
        ( 'TFlohr', '820100' ),
        ( 'MWerke', '940800' ),
        ( 'AZwet', '999900' ),
        ( 'JReijs', '84110' ),
        ( 'ASloot', '106840' ),
        ( 'MMekel', '108320' ),
        ( 'LBreda', '109800' ),
        ( 'SSande', '203940' ),
        ( 'JVoge2', '204810' ),
        ( 'PDorsm', '205400' ),
        ( 'MArk', '205830' ),
        ( 'RPlug', '206340' ),
        ( 'MHoes', '346700' ),
        ( 'HNeder', '678300' ),
        ( 'MWerf', '940600' ),
        ( 'NTol2', '92680' ),
        ( 'AWerkh', '108070' ),
        ( 'RPiete', '108120' ),
        ( 'PVerm3', '111420' ),
        ( 'NWijk', '111530' ),
        ( 'MBulk', '145300' ),
        ( 'SKhata', '206810' ),
        ( 'IGodek', '355400' ),
        ( 'WKrane', '475500' ),
        ( 'ERuijg', '493600' ),
        ( 'EHoek2', '506700' ),
        ( 'SVissc', '550700' ),
        ( 'JMooij', '583500' ),
        ( 'MKempe', '842300' ),
        ( 'Muitte', '847700' ),
        ( 'AVink', '889800' ),
        ( 'MZwiet', '998200' ),
        ( '201490', '201490' ),
        ( '123090', '123090' ),
        ( '92570', '92570' ),
        ( '109540', '109540' ),
        ( '24030', '24030' ),
        ( '93130', '93130' ),
        ( 'MVis', '79000' ),
        ( 'IBos', '103000' ),
        ( 'EPronk', '107700' ),
        ( 'MHuls2', '109660' ),
        ( 'ABiema', '111180' ),
        ( 'MBontj', '192200' ),
        ( 'JJas', '283900' ),
        ( 'PNoord', '313800' ),
        ( 'WBoom', '364000' ),
        ( 'NRomei', '372500' ),
        ( 'WBouwm', '427000' ),
        ( 'PKroon', '481800' ),
        ( 'BFalla', '537400' ),
        ( 'MHeems', '538100' ),
        ( 'EZwets', '707900' ),
        ( 'NVerho', '851000' ),
        ( 'WOuter', '909300' ),
        ( 'IJagt', '109880' ),
        ( 'TELK2', '111880' ),
        ( 'CHartv', '113810' ),
        ( 'JWelbo', '115220' ),
        ( 'PKleij', '209000' ),
        ( 'AStass', '796200' ),
        ( 'MDijkm', '97350' ),
        ( 'ALeden', '109960' ),
        ( 'CGenti', '146700' ),
        ( 'JAlber', '202820' ),
        ( 'MDoesw', '208770' ),
        ( 'JVeldh', '611900' ),
        ( 'MZante', '721400' ),
        ( 'HOijen', '771700' ),
        ( 'JKrimp', '846300' ),
        ( 'KZwets', '997000' ),
        ( 'IBeer', '5500' ),
        ( 'JGorku', '10620' ),
        ( 'SVisse', '92940' ),
        ( 'EHarte', '309600' ),
        ( 'CHolt2', '357000' ),
        ( 'WVelze', '109470' ),
        ( 'AMols', '109940' ),
        ( 'JKamph', '109970' ),
        ( 'KMijl', '111750' ),
        ( 'CBerg2', '116360' ),
        ( 'LAart', '116560' ),
        ( 'CVroli', '137700' ),
        ( 'MCosta', '163500' ),
        ( 'HLelie', '204620' ),
        ( 'CLaan', '207220' ),
        ( 'JRijsk', '723900' ),
        ( 'EZwart', '958200' ),
        ( 'ASpies', '967000' ),
        ( 'AJanse', '998000' ),
        ( 'SBarak', '89470' ),
        ( 'CMoeso', '107680' ),
        ( 'ESittk', '204310' ),
        ( 'AParle', '206530' ),
        ( 'RLeijd', '517000' ),
        ( 'AKeste', '53280' ),
        ( 'THuibe', '168400' ),
        ( 'DDeege', '174400' ),
        ( 'ALamme', '204100' ),
        ( 'MWit', '233900' ),
        ( 'TLance', '498900' ),
        ( 'NVerwe', '888200' ),
        ( 'MBouwh', '928600' ),
        ( 'AMOL', '107810' ),
        ( 'LSTAD', '112010' ),
        ( 'SKWANT', '110860' ),
        ( 'JBissc', '75580' ),
        ( 'JHEIDA', '87310' ),
        ( 'mvlie3', '100860' ),
        ( 'ASCHO2', '110390' ),
        ( 'abouth', '201500' ),
        ( 'SGROOT', '201640' ),
        ( 'BEYSIN', '201840' ),
        ( 'WWIT', '202070' ),
        ( 'APrins', '202710' ),
        ( 'HLAPv', '115800' ),
        ( 'MWolswijk', '123080' ),
        ( 'lkemper', '29410' ),
        ( 'lzeehandelaar', '460500' ),
        ( 'yschu3', '822' ),
        ( 'MSWAR2', '111820' ),
        ( 'JWorku', '204040' );

SET NOCOUNT ON;

DECLARE @organizationname VARCHAR(255) = 'Alrijne zorggroep Leiderdorp & Alphen';
DECLARE @employeeid INT;
DECLARE @externid AS VARCHAR(50);
DECLARE @OLD VARCHAR(50);
DECLARE @NEW VARCHAR(50);

DECLARE employeecursor CURSOR
FOR
    SELECT  Employee.EmployeeID ,
            Employee.ExternID
    FROM    Employee WITH ( NOLOCK )
            JOIN Department WITH ( NOLOCK ) ON Employee.DepartmentID = Department.DepartmentID
            JOIN Location WITH ( NOLOCK ) ON Department.LocationID = Location.LocationID
            JOIN Organization WITH ( NOLOCK ) ON Organization.OrganizationID = Location.OrganizationID
            JOIN aspnet_Users WITH ( NOLOCK) ON aspnet_Users.UserId = Employee.UserId
    WHERE   Organization.Name = @organizationname
            AND Employee.ExternID IS NOT NULL AND Employee.InActive = 0;            

OPEN employeecursor;

PRINT 'Employee ZIS omnummering ' + @organizationname;

FETCH NEXT FROM employeecursor INTO @employeeid, @externid;

WHILE @@FETCH_STATUS = 0
    BEGIN

        IF EXISTS ( SELECT  1
                    FROM    #AlrijneLeiderdorpZIS
                    WHERE   OLD = @externid )
            BEGIN

                SELECT  @NEW = NEW
                FROM    #AlrijneLeiderdorpZIS
                WHERE   OLD = @externid;

                UPDATE Employee SET ExternID = @NEW WHERE EmployeeID = @employeeid AND ExternID = @externid

                PRINT 'ZIS old ' + @externid + ' ' + 'ZIS new ' + @NEW
                    + ' EmployeeID ' + STR(@employeeid);
            END;
        ELSE
            PRINT 'ZIS old ' + @externid + ' ' + 'has no new ZIS '
                + ' EmployeeID ' + STR(@employeeid);
    
        FETCH NEXT FROM employeecursor INTO @employeeid, @externid;
    END;

CLOSE employeecursor;
DEALLOCATE employeecursor;

PRINT 'Done!';
GO

SET NOCOUNT OFF;