﻿namespace Point.FormCopy
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flowDefinitionBox = new System.Windows.Forms.ListBox();
            this.phaseDefinitionBox = new System.Windows.Forms.ListBox();
            this.detailsBox = new System.Windows.Forms.GroupBox();
            this.lblPhase = new System.Windows.Forms.Label();
            this.txtPhase = new System.Windows.Forms.TextBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.lblName = new System.Windows.Forms.Label();
            this.txtFormType = new System.Windows.Forms.TextBox();
            this.lblFormType = new System.Windows.Forms.Label();
            this.btnCopyForm = new System.Windows.Forms.Button();
            this.detailsBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // flowDefinitionBox
            // 
            this.flowDefinitionBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.flowDefinitionBox.FormattingEnabled = true;
            this.flowDefinitionBox.Location = new System.Drawing.Point(17, 34);
            this.flowDefinitionBox.Name = "flowDefinitionBox";
            this.flowDefinitionBox.Size = new System.Drawing.Size(173, 381);
            this.flowDefinitionBox.TabIndex = 0;
            this.flowDefinitionBox.SelectedValueChanged += new System.EventHandler(this.flowDefinitionBox_SelectedValueChanged);
            // 
            // phaseDefinitionBox
            // 
            this.phaseDefinitionBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.phaseDefinitionBox.FormattingEnabled = true;
            this.phaseDefinitionBox.Location = new System.Drawing.Point(196, 34);
            this.phaseDefinitionBox.Name = "phaseDefinitionBox";
            this.phaseDefinitionBox.Size = new System.Drawing.Size(173, 381);
            this.phaseDefinitionBox.TabIndex = 1;
            this.phaseDefinitionBox.SelectedValueChanged += new System.EventHandler(this.phaseDefinitionBox_SelectedValueChanged);
            // 
            // detailsBox
            // 
            this.detailsBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.detailsBox.Controls.Add(this.btnCopyForm);
            this.detailsBox.Controls.Add(this.txtFormType);
            this.detailsBox.Controls.Add(this.lblFormType);
            this.detailsBox.Controls.Add(this.txtName);
            this.detailsBox.Controls.Add(this.lblName);
            this.detailsBox.Controls.Add(this.txtPhase);
            this.detailsBox.Controls.Add(this.lblPhase);
            this.detailsBox.Enabled = false;
            this.detailsBox.Location = new System.Drawing.Point(392, 34);
            this.detailsBox.Name = "detailsBox";
            this.detailsBox.Size = new System.Drawing.Size(478, 347);
            this.detailsBox.TabIndex = 2;
            this.detailsBox.TabStop = false;
            this.detailsBox.Text = "Selected Phase";
            // 
            // lblPhase
            // 
            this.lblPhase.AutoSize = true;
            this.lblPhase.Location = new System.Drawing.Point(18, 27);
            this.lblPhase.Name = "lblPhase";
            this.lblPhase.Size = new System.Drawing.Size(37, 13);
            this.lblPhase.TabIndex = 0;
            this.lblPhase.Text = "Phase";
            // 
            // txtPhase
            // 
            this.txtPhase.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPhase.Location = new System.Drawing.Point(137, 24);
            this.txtPhase.Name = "txtPhase";
            this.txtPhase.Size = new System.Drawing.Size(335, 20);
            this.txtPhase.TabIndex = 1;
            // 
            // txtName
            // 
            this.txtName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtName.Location = new System.Drawing.Point(137, 50);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(335, 20);
            this.txtName.TabIndex = 3;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(18, 53);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(35, 13);
            this.lblName.TabIndex = 2;
            this.lblName.Text = "Name";
            // 
            // txtFormType
            // 
            this.txtFormType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFormType.Location = new System.Drawing.Point(137, 75);
            this.txtFormType.Name = "txtFormType";
            this.txtFormType.Size = new System.Drawing.Size(251, 20);
            this.txtFormType.TabIndex = 5;
            // 
            // lblFormType
            // 
            this.lblFormType.AutoSize = true;
            this.lblFormType.Location = new System.Drawing.Point(18, 78);
            this.lblFormType.Name = "lblFormType";
            this.lblFormType.Size = new System.Drawing.Size(54, 13);
            this.lblFormType.TabIndex = 4;
            this.lblFormType.Text = "FormType";
            // 
            // btnCopyForm
            // 
            this.btnCopyForm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCopyForm.Enabled = false;
            this.btnCopyForm.Location = new System.Drawing.Point(394, 75);
            this.btnCopyForm.Margin = new System.Windows.Forms.Padding(2);
            this.btnCopyForm.Name = "btnCopyForm";
            this.btnCopyForm.Size = new System.Drawing.Size(78, 20);
            this.btnCopyForm.TabIndex = 3;
            this.btnCopyForm.Text = "Copy";
            this.btnCopyForm.UseVisualStyleBackColor = true;
            this.btnCopyForm.Click += new System.EventHandler(this.btnCopyForm_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(897, 434);
            this.Controls.Add(this.detailsBox);
            this.Controls.Add(this.phaseDefinitionBox);
            this.Controls.Add(this.flowDefinitionBox);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.detailsBox.ResumeLayout(false);
            this.detailsBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox flowDefinitionBox;
        private System.Windows.Forms.ListBox phaseDefinitionBox;
        private System.Windows.Forms.GroupBox detailsBox;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.TextBox txtPhase;
        private System.Windows.Forms.Label lblPhase;
        private System.Windows.Forms.TextBox txtFormType;
        private System.Windows.Forms.Label lblFormType;
        private System.Windows.Forms.Button btnCopyForm;
    }
}

