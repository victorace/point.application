﻿using Point.Database.Context;
using Point.Database.Models;
using Point.FormCopy.Business;
using Point.Infrastructure.Extensions;
using System;
using System.Linq;
using System.Threading;
using System.Windows.Forms;

namespace Point.FormCopy
{
    public partial class CopyFormTypeForm : Form
    {
        private PhaseDefinition PhaseDefinition { get; set; }

        public CopyFormTypeForm(PhaseDefinition phaseDefinition)
        {
            this.PhaseDefinition = phaseDefinition;
            InitializeComponent();
        }

        #region threads
        private void ShowInitialValues()
        {
            using (var context = new PointContext())
            {
                this.Invoke(new Action(() =>
                {
                    txtNewFormTypeID.Text = FormTypeBL.GetNewFormTypeID(context).ToString();
                    txtNewFormTypeName.Text = PhaseDefinition.FormType.Name + " (kopie)";
                    txtNewFormTypeURL.Text = PhaseDefinition.FormType.URL + " (kopie)";
                }));
            }
        }

        private void CopyFormStart()
        {
            using (var context = new PointContext())
            {
                var formtype = FormTypeBL.GetByID(context, PhaseDefinition.FormTypeID);

                var newform = new FormType();

                newform.Merge(formtype);
                newform.PhaseDefinition = null;
                newform.FlowFieldAttribute = null;

                var formtypeid = Convert.ToInt32(txtNewFormTypeID.Text);
                newform.FormTypeID = formtypeid;
                newform.URL = txtNewFormTypeURL.Text;
                newform.Name = txtNewFormTypeName.Text;

                int newattributeid = FlowFieldAttributeBL.GetNewFormTypeID(context);

                foreach (FlowFieldAttribute item in formtype.FlowFieldAttribute.ToList())
                {
                    var newattribute = new FlowFieldAttribute();

                    newattribute.Merge(item);

                    newattribute.FlowFieldAttributeID = newattributeid;
                    newattribute.FormTypeID = formtypeid;
                    newattribute.FormType = newform;
                    newattribute.FlowFieldAttributeException = null;

                    context.FlowFieldAttribute.Add(newattribute);

                    newattributeid++;
                }

                FormTypeBL.AddAndSave(context, newform);

                var phasedefinition = PhaseDefinitionBL.GetByID(context, PhaseDefinition.PhaseDefinitionID);
                phasedefinition.FormType = newform;
                phasedefinition.FormTypeID = formtypeid;

                context.SaveChanges();

                MessageBox.Show("FormType is gekopieerd en ingesteld op de geselecteerde Fase. Vergeet niet in de formtypecontroller een actie aan te maken op " + newform.URL);
                this.Close();
            }
        }
        #endregion

        #region events
        private void CopyFormTypeForm_Load(object sender, EventArgs e)
        {
            Thread worker = new Thread(ShowInitialValues);
            worker.Start();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnStartCopy_Click(object sender, EventArgs e)
        {
            Thread worker = new Thread(CopyFormStart);
            worker.Start();
        }
        #endregion
    }
}
