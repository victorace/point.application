﻿using Point.Database.Context;
using Point.Database.Models;
using Point.FormCopy.Business;
using System;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;

namespace Point.FormCopy
{
    public partial class Form1 : Form
    {
        #region threads
        private void LoadFlowDefinitions()
        {
            var data = new FlowDefinitionBL().GetAll();

            flowDefinitionBox.Invoke(new Action(() => {
                flowDefinitionBox.DisplayMember = "Name";
                flowDefinitionBox.DataSource = data;
            }));
        }

        private void SelectFlowDefinition(object data)
        {
            if (data is FlowDefinition)
            {
                LoadPhaseDefinitions(data as FlowDefinition);
            }
        }

        private void LoadPhaseDefinitions(FlowDefinition flowDefinition)
        {
            using (var context = new PointContext())
            {
                var data = PhaseDefinitionBL.GetByFlowDefinitionID(context, flowDefinition.FlowDefinitionID);

                phaseDefinitionBox.Invoke(new Action(() => {
                    phaseDefinitionBox.DisplayMember = "PhaseName";
                    phaseDefinitionBox.DataSource = data;
                }));
            }
        }

        private void SelectPhaseDefinition(object data)
        {
            if (data is PhaseDefinition)
            {
                LoadPhaseDefinition(data as PhaseDefinition);
            }
        }

        private void LoadPhaseDefinition(PhaseDefinition phaseDefinition)
        {
            this.Invoke(new Action(() => {
                detailsBox.Enabled = true;
                txtPhase.Text = phaseDefinition.Phase.ToString(CultureInfo.InvariantCulture);
                txtName.Text = phaseDefinition.PhaseName;
                txtFormType.Text = phaseDefinition.FormType.Name + " (" + phaseDefinition.FormTypeID.ToString() + ")";
                btnCopyForm.Enabled = true;
            }));
        }
        #endregion

        #region events
        private void Form1_Load(object sender, EventArgs e)
        {
            System.Data.Entity.Database.SetInitializer<PointContext>(null);

            Thread worker = new Thread(LoadFlowDefinitions);
            worker.Start();
        }

        private void flowDefinitionBox_SelectedValueChanged(object sender, EventArgs e)
        {
            if (((ListBox)sender).SelectedItem is FlowDefinition flowdefinition)
            {
                Thread worker = new Thread(SelectFlowDefinition);
                worker.Start(flowdefinition);
            }
        }

        private void phaseDefinitionBox_SelectedValueChanged(object sender, EventArgs e)
        {
            if (((ListBox)sender).SelectedItem is PhaseDefinition phaseDefinition)
            {
                Thread worker = new Thread(SelectPhaseDefinition);
                worker.Start(phaseDefinition);
            }
        }

        private void btnCopyForm_Click(object sender, EventArgs e)
        {
            var form = new CopyFormTypeForm(this.phaseDefinitionBox.SelectedItem as PhaseDefinition);
            form.Show();
        }
        #endregion

        public Form1()
        {
            InitializeComponent();
        }
    }
}
