﻿namespace Point.FormCopy
{
    partial class CopyFormTypeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblNewFormTypeID = new System.Windows.Forms.Label();
            this.txtNewFormTypeID = new System.Windows.Forms.TextBox();
            this.txtNewFormTypeName = new System.Windows.Forms.TextBox();
            this.lblNewFormTypeName = new System.Windows.Forms.Label();
            this.txtNewFormTypeURL = new System.Windows.Forms.TextBox();
            this.lblNewFormTypeURL = new System.Windows.Forms.Label();
            this.btnStartCopy = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblNewFormTypeID
            // 
            this.lblNewFormTypeID.AutoSize = true;
            this.lblNewFormTypeID.Location = new System.Drawing.Point(16, 16);
            this.lblNewFormTypeID.Name = "lblNewFormTypeID";
            this.lblNewFormTypeID.Size = new System.Drawing.Size(93, 13);
            this.lblNewFormTypeID.TabIndex = 0;
            this.lblNewFormTypeID.Text = "New FormType ID";
            // 
            // txtNewFormTypeID
            // 
            this.txtNewFormTypeID.Location = new System.Drawing.Point(146, 13);
            this.txtNewFormTypeID.Name = "txtNewFormTypeID";
            this.txtNewFormTypeID.Size = new System.Drawing.Size(126, 20);
            this.txtNewFormTypeID.TabIndex = 1;
            // 
            // txtNewFormTypeName
            // 
            this.txtNewFormTypeName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNewFormTypeName.Location = new System.Drawing.Point(146, 39);
            this.txtNewFormTypeName.Name = "txtNewFormTypeName";
            this.txtNewFormTypeName.Size = new System.Drawing.Size(372, 20);
            this.txtNewFormTypeName.TabIndex = 3;
            // 
            // lblNewFormTypeName
            // 
            this.lblNewFormTypeName.AutoSize = true;
            this.lblNewFormTypeName.Location = new System.Drawing.Point(16, 42);
            this.lblNewFormTypeName.Name = "lblNewFormTypeName";
            this.lblNewFormTypeName.Size = new System.Drawing.Size(110, 13);
            this.lblNewFormTypeName.TabIndex = 2;
            this.lblNewFormTypeName.Text = "New FormType Name";
            // 
            // txtNewFormTypeURL
            // 
            this.txtNewFormTypeURL.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNewFormTypeURL.Location = new System.Drawing.Point(146, 66);
            this.txtNewFormTypeURL.Name = "txtNewFormTypeURL";
            this.txtNewFormTypeURL.Size = new System.Drawing.Size(372, 20);
            this.txtNewFormTypeURL.TabIndex = 5;
            // 
            // lblNewFormTypeURL
            // 
            this.lblNewFormTypeURL.AutoSize = true;
            this.lblNewFormTypeURL.Location = new System.Drawing.Point(16, 69);
            this.lblNewFormTypeURL.Name = "lblNewFormTypeURL";
            this.lblNewFormTypeURL.Size = new System.Drawing.Size(104, 13);
            this.lblNewFormTypeURL.TabIndex = 4;
            this.lblNewFormTypeURL.Text = "New FormType URL";
            // 
            // btnStartCopy
            // 
            this.btnStartCopy.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStartCopy.Location = new System.Drawing.Point(378, 107);
            this.btnStartCopy.Name = "btnStartCopy";
            this.btnStartCopy.Size = new System.Drawing.Size(140, 30);
            this.btnStartCopy.TabIndex = 6;
            this.btnStartCopy.Text = "Start";
            this.btnStartCopy.UseVisualStyleBackColor = true;
            this.btnStartCopy.Click += new System.EventHandler(this.btnStartCopy_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCancel.Location = new System.Drawing.Point(15, 107);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(140, 30);
            this.btnCancel.TabIndex = 7;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // CopyFormTypeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(530, 149);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnStartCopy);
            this.Controls.Add(this.txtNewFormTypeURL);
            this.Controls.Add(this.lblNewFormTypeURL);
            this.Controls.Add(this.txtNewFormTypeName);
            this.Controls.Add(this.lblNewFormTypeName);
            this.Controls.Add(this.txtNewFormTypeID);
            this.Controls.Add(this.lblNewFormTypeID);
            this.Name = "CopyFormTypeForm";
            this.Text = "CopyFormTypeForm";
            this.Load += new System.EventHandler(this.CopyFormTypeForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblNewFormTypeID;
        private System.Windows.Forms.TextBox txtNewFormTypeID;
        private System.Windows.Forms.TextBox txtNewFormTypeName;
        private System.Windows.Forms.Label lblNewFormTypeName;
        private System.Windows.Forms.TextBox txtNewFormTypeURL;
        private System.Windows.Forms.Label lblNewFormTypeURL;
        private System.Windows.Forms.Button btnStartCopy;
        private System.Windows.Forms.Button btnCancel;
    }
}