﻿using Point.Database.Context;
using Point.Database.Models;
using Point.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Point.FormCopy.Business
{
    public class FormTypeBL
    {
        public static List<FormType> GetByFlowDefinitionID(PointContext context, FlowDefinitionID flowDefinitionID)
        {
            return context.FormType.Where(ft => ft.PhaseDefinition.Any(pd => pd.FlowDefinitionID == flowDefinitionID)).OrderBy(ft => ft.Name).ToList();
        }

        public static FormType GetByID(PointContext context, int formTypeID)
        {
            return context.FormType.Find(formTypeID);
        }

        public static int GetNewFormTypeID(PointContext context)
        {
            return context.FormType.Max(ft => ft.FormTypeID) + 1;
        }

        public static void AddAndSave(PointContext context, FormType formType)
        {
            context.FormType.Add(formType);
            context.SaveChanges();
        }
    }
}
