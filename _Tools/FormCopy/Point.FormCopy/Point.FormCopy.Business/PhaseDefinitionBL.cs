﻿using Point.Database.Context;
using Point.Database.Models;
using Point.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Point.FormCopy.Business
{
    public class PhaseDefinitionBL
    {
        public static List<PhaseDefinition> GetByFlowDefinitionID(PointContext context, FlowDefinitionID flowDefinitionID)
        {
            return context.PhaseDefinition.Include("FormType").Where(pd => pd.FlowDefinitionID == flowDefinitionID).OrderBy(ft => ft.Phase).ToList();
        }

        public static PhaseDefinition GetByID(PointContext context, int phaseDefinitionID)
        {
            return context.PhaseDefinition.Find(phaseDefinitionID);
        }
    }
}
