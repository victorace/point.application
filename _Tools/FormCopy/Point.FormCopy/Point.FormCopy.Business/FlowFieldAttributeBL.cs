﻿using Point.Database.Context;
using Point.Database.Models;
using Point.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Point.FormCopy.Business
{
    public class FlowFieldAttributeBL
    {
        public static int GetNewFormTypeID(PointContext context)
        {
            return context.FlowFieldAttribute.Max(ft => ft.FlowFieldAttributeID) + 1;
        }
    }
}
