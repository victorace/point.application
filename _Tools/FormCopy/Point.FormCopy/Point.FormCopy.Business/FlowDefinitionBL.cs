﻿using Point.Database.Context;
using Point.Database.Models;
using Point.Models.Enums;
using System.Collections.Generic;
using System.Linq;

namespace Point.FormCopy.Business
{
    public class FlowDefinitionBL
    {
        private PointContext context = new PointContext();

        public List<FlowDefinition> GetAll()
        {
            return context.FlowDefinition.OrderBy(ft => ft.FlowDefinitionID).ToList();
        }

        public FlowDefinition GetByID(FlowDefinitionID flowDefinitionID)
        {
            return context.FlowDefinition.Find(flowDefinitionID);
        }
    }
}
