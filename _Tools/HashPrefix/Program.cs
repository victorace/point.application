﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace HashPrefix
{
    public static class StringExtensions
    {
        /// <summary>
        /// Get substring of specified number of characters on the right.
        /// </summary>
        public static string Right(this string value, int length)
        {
            if (String.IsNullOrEmpty(value))
            {
                return string.Empty;
            }

            return value.Length <= length ? value : value.Substring(value.Length - length);
        }

    }

    //hash: <PREFIX>_<GUID>_<OrganisationID>_<SenderUserName>_<SendDate>_<PatientNumber>
    public class Hash
    {
        public Guid GUID { get; set; }
        public int OrganizationID { get; set; }
        public string SenderUserName { get; set; }
        public DateTime SendDate { get; set; }
        public string PatientNumber { get; set; }
        public string HashPrefix { get; set; }

        public string Encode()
        {
            byte[] hashbytes = Encoding.UTF8.GetBytes($"{HashPrefix}_{GUID}_{OrganizationID}_{SenderUserName}_{SendDate.ToString("yyyyMMdd")}_{PatientNumber}");
            SHA1Managed hashSHA1Managed = new SHA1Managed();
            byte[] computedhash = hashSHA1Managed.ComputeHash(hashbytes);

            string hashedstring = string.Empty;
            foreach (byte x in computedhash)
            {
                hashedstring += StringExtensions.Right("0" + x.ToString("X").ToLower(), 2);
            }

            return hashedstring.ToLower();
        }

        public void Check(string hash)
        {
            var computedhash = Encode();
            if (computedhash.ToLower() == hash.ToLower())
            {
                Console.WriteLine("OK");
            }
            else
            {
                Console.WriteLine("NOK");
                Console.WriteLine($"hash[{hash}] != computed hash[{computedhash}]");
            }
        }
    }

    public class Program
    {
        static void Main(string[] args)
        {
            var hash = new Hash()
            {
                //HashPrefix = "qk@Xt4qXS.H",
                HashPrefix = "GS7VW[y4mFv?b",
                GUID = Guid.Parse("5b387ed1-117c-4d42-552e-0db09e01e287"),
                OrganizationID = 11556,
                //OrganizationID = 11558,
                SenderUserName = "docrmst",
                SendDate = DateTime.Parse("2019-02-26T15:41:49"),
                PatientNumber = "00073019"
            };

            var senthash = "a4c9001b8930b174522a2e16c53367339eaa6c98";
            var computedhash = hash.Encode();

            hash.Check(senthash);

            System.Console.ReadKey();
        }
    }
}
