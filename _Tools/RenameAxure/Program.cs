﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RenameAxure
{
    class Program
    {
        static void Main(string[] args)
        {
            //args = new[] { @"c:\VSOnline\Point.Application\Development\PointBedCapAmsterdam\Point.UI.WebApplication\Documents\Instructies\" };

            if (args.Length == 0)
            {
                Console.WriteLine(@"Please specify a directory in which the files reside.");
                Console.WriteLine(@"f.e. RenameAxure ""c:\users\yourname\my documents\axure\""" );
                Console.WriteLine(@"Don't forget to put quotes around it, if the directoryname contains spaces.");

            }
            else
            {
                try
                {
                    DoProcess(args[0]);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Something went wrong!");
                    Console.WriteLine(ex.Message);
                    Console.WriteLine(ex.StackTrace);
                }

            }
            Console.WriteLine("");
            Console.WriteLine("Press a key to quit...");
            Console.ReadKey();

        }

        static void DoProcess(string dirName)
        {
            if (!Directory.Exists(dirName))
            {
                Console.WriteLine("Directory [" + dirName + "] does not exist!");
                return;
            }
            DirLoop(dirName);
            
        }

        static void DirLoop(string dirname)
        {
            try
            {
                foreach (var directory in Directory.GetDirectories(dirname))
                {
                    Console.WriteLine("Processing directory: " + directory);
                    DirLoop(directory);
                }


                foreach (var filename in Directory.GetFiles(dirname))
                {
                    var extension = Path.GetExtension(filename).ToLower();
                    if (extension == ".html" || extension == ".htm")
                    {
                        ProcessHtmlFile(filename);
                    }
                    if (extension == ".js")
                    {
                        ProcessJSFile(filename);
                    }
                }

            }
            catch (System.Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        static void ProcessHtmlFile(string filename)
        {
            Console.WriteLine("Processing file: " + filename);

            if (filename.ToLower().EndsWith("reload.html"))
            {
                Console.WriteLine("   Skipping file: " + filename);
                return;
            }


            var contents = File.ReadAllText(filename, Encoding.UTF8);
            contents = contents.Replace(@".html""",@".aspx""");
            contents = contents.Replace(@".html'", @".aspx'");
            contents = contents.Replace(@"http://fonts.googleapis.com", @"https://fonts.googleapis.com");
            contents = @"<%@ Page Language=""VB"" EnableTheming=""false"" Theme="""" StyleSheetTheme="""" %>" + Environment.NewLine + Environment.NewLine + contents;

            var extension = Path.GetExtension(filename).ToLower();

            var newfile = filename.Replace(extension, ".aspx");
            if (File.Exists(newfile))
                File.Delete(newfile);
            File.WriteAllText(newfile, contents, Encoding.UTF8);

            File.Delete(filename);

            Console.WriteLine("   Done processing file: " + newfile);

        }

        static void ProcessJSFile(string filename)
        {
            Console.WriteLine("Processing file: " + filename);

            var contents = File.ReadAllText(filename, Encoding.UTF8);
            contents = contents.Replace(@".html""", @".aspx""");

            File.WriteAllText(filename, contents, Encoding.UTF8);

            Console.WriteLine("   Done processing file: " + filename);
        }
    }
}
