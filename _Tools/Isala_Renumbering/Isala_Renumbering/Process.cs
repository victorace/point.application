﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;

namespace Isala_Renumbering
{
    public class Process
    {
        private List<IsalaPatient> isalaPatients = new List<IsalaPatient>();
        private List<PointClient> pointClients = new List<PointClient>();
        private string header = "";
        private string connectionstring = ConfigurationManager.ConnectionStrings["DBToUpdate"].ConnectionString;

        public void ReadFile()
        {
            var filename = ConfigurationManager.AppSettings["ImportFile"];
            if (string.IsNullOrEmpty(filename) || !File.Exists(filename))
            {
                LogError($"File {filename} not found or not specified in config");
                return;
            }

            Log($"Reading file: {filename}");

            int linecount = 0;
            foreach(var line in File.ReadAllLines(filename))
            {
                linecount++;

                if (linecount % 100 == 0)
                {
                    Progress($"{linecount} lines");
                }

                if (linecount <= 1)
                {
                    header = line;
                    continue; // skip header
                }

                if (string.IsNullOrWhiteSpace(line) || line.EndsWith("rows affected)"))
                {
                    continue; // skip empties and footer
                }
                string[] splitted = line.Split(new[] { ',' }, StringSplitOptions.None);
                var isalapatient = new IsalaPatient().FromString(splitted, linecount);
                isalaPatients.Add(isalapatient);

                if (!string.IsNullOrEmpty(isalapatient.Status))
                {
                    Log($"Line {linecount} {isalapatient.Status}");
                    continue;
                }
            }

            Log($"Done reading {linecount} lines");


        }

        public void ReadClients()
        {
            Log("Started reading Client's in POINT");

            using (SqlConnection sqlconnection = new SqlConnection(connectionstring))
            {
                string clientquery = $@"select Client.ClientID, Client.CivilServiceNumber, Client.PatientNumber, Client.Gender, Client.BirthDate, Transfer.TransferID, FlowInstance.FlowInstanceID, FlowInstance.Deleted
                                         from Client with(nolock)
                                         join Transfer with(nolock) on Transfer.ClientID = Client.ClientID
                                         join FlowInstance with(nolock) on FlowInstance.TransferID = Transfer.TransferID
                                        where FlowInstance.StartedByOrganizationID = {ConfigurationManager.AppSettings["OrganizationID"]}
                                        order by Client.ClientID";

                using (SqlCommand sqlcommand = new SqlCommand(clientquery, sqlconnection))
                {
                    sqlconnection.Open();

                    var reader = sqlcommand.ExecuteReader();
                    int linecount = 0;
                    while (reader.Read())
                    {
                        linecount++;
                        Progress($"{linecount} Client's");
                        var pointclient = new PointClient().FromReader(reader);
                        pointClients.Add(pointclient);
                    }

                    sqlconnection.Close();
                }
            }

        }

        public void CompareAndUpdate()
        {

            Log("Started comparing data and update");

            foreach(var isalapatient in isalaPatients)
            {
                Progress($"Comparing isalapatient line: {isalapatient.LineNumber}");
                if (!string.IsNullOrEmpty(isalapatient.Status))
                {
                    continue;
                }

                var pointclients = pointClients.Where(pc => pc.PatientNumber == isalapatient.Old);
                if (!pointclients.Any())
                { 
                    isalapatient.Status = "NOT FOUND: in POINT";
                }
                else
                {
                    foreach(var pointclient in pointclients)
                    {
                        pointclient.InList = true;
                        string mismatch = "";
                        if (!pointclient.Gender.Equals(isalapatient.Geslacht, StringComparison.InvariantCultureIgnoreCase))
                        {
                            mismatch += $"Gender ({pointclient.Gender.ToUpper()}) ";
                        }
                        if (!pointclient.CivilServiceNumber.Equals(isalapatient.BSN, StringComparison.InvariantCultureIgnoreCase))
                        {
                            mismatch += $"BSN ({pointclient.CivilServiceNumber}) ";
                        }
                        if (!pointclient.BirthDate.ToString("dd-MM-yyyy").Equals(isalapatient.GebDat, StringComparison.InvariantCultureIgnoreCase))
                        {
                            mismatch += $"BirthDate ({pointclient.BirthDate.ToString("dd-MM-yyyy")}) ";
                        }
                        if (!string.IsNullOrEmpty(mismatch))
                        {
                            isalapatient.Status += $"MISMATCH: POINT ClientID: {pointclient.ClientID} - {mismatch.Trim()} ";
                        }
                        else
                        {
                            doUpdate(isalapatient, pointclient);
                        }
                    }
                }
            }

            foreach(var pointclient in pointClients.Where(pc => !pc.InList && !pc.Deleted))
            {
                isalaPatients.Add(new IsalaPatient()
                {
                    BSN = pointclient.CivilServiceNumber,
                    GebDat = pointclient.BirthDate.ToString("dd-MM-yyyy"),
                    Geslacht = pointclient.Gender.ToUpper(),
                    LineNumber = isalaPatients.Count + 1,
                    Old = pointclient.PatientNumber,
                    Status = $"IN POINT: but NOT in list - ClientID {pointclient.ClientID}"
                });
            }
        }

        public void ShowTotals()
        {
            int totalOriginal = 0;
            int totalAll = 0;
            int totalNotFound = 0;
            int totalUpdated = 0;
            int totalMismatch = 0;
            int totalInPointNotInList = 0;

            foreach(var isalapatient in isalaPatients)
            {
                totalAll++;
                if (isalapatient.Status.StartsWith("IN POINT:"))
                {
                    totalInPointNotInList++;
                }
                else
                {
                    totalOriginal++;
                    if (isalapatient.Status.StartsWith("NOT FOUND:"))
                    {
                        totalNotFound++;
                    }
                    if (isalapatient.Status.StartsWith("MISMATCH:") || isalapatient.Status.StartsWith("ERROR:"))
                    {
                        totalMismatch++;
                    }
                    if (isalapatient.Status.StartsWith("UPDATED:"))
                    {
                        
                        totalUpdated++;
                    }
                }
            }

            string totals = Environment.NewLine;
            totals += $"Aangeboden: {totalOriginal}" + Environment.NewLine;
            totals += $"Match/Updated: {totalUpdated}" + Environment.NewLine;
            totals += $"Mismatch: {totalMismatch}" + Environment.NewLine;
            totals += $"Niet aanwezig: {totalNotFound}" + Environment.NewLine;
            totals += $"In POINT niet in lijst: {totalInPointNotInList}" + Environment.NewLine;
            totals += $"Totaaltelling: {totalAll} ({totalOriginal}+{totalInPointNotInList})={(totalOriginal + totalInPointNotInList)}" + Environment.NewLine;
            Log(totals);
        }


        private void doUpdate(IsalaPatient isalaPatient, PointClient pointClient)
        {
            if (ConfigurationManager.AppSettings["DummyRun"] == "False")
            {
                //REAL:
                using (SqlConnection sqlconnection = new SqlConnection(connectionstring))
                {
                    sqlconnection.Open();
                    try
                    {
                        string updateclient = $"update Client set PatientNumber = '{isalaPatient.New}' where ClientID = {pointClient.ClientID} and PatientNumber = '{isalaPatient.Old}'";
                        string updatefisv = $"update FlowInstanceSearchValues set PatientNumber = '{isalaPatient.New}' where FlowInstanceID = {pointClient.FlowInstanceID} and PatientNumber = '{isalaPatient.Old}'";
                        int rowsaffected = new SqlCommand(updateclient, sqlconnection).ExecuteNonQuery();
                        rowsaffected += new SqlCommand(updatefisv, sqlconnection).ExecuteNonQuery();
                        if (rowsaffected >= 1)
                        {
                            isalaPatient.Status += $"UPDATED: POINT ClientID: {pointClient.ClientID} - new PatientNumber: {isalaPatient.New} ";
                        }
                        else
                        {
                            isalaPatient.Status += $"ERROR: Did not update OLD-PatientNumber: {isalaPatient.Old} - 0 rows affected ";
                        }
                    }
                    catch(Exception ex)
                    {
                        isalaPatient.Status += $"ERROR: Could not update OLD-PatientNumber: {isalaPatient.Old} - [{ex.Message.Replace(",","").Replace(Environment.NewLine, "")}] ";
                    }
                    sqlconnection.Close();
                }
            }
            else
            {
                //TEST:
                isalaPatient.Status += $"UPDATED: POINT ClientID: {pointClient.ClientID} - new PatientNumber: {isalaPatient.New} ";
            }

            Log(isalaPatient.Status);

            return; 
        }

        public bool TestConnection()
        {
            int organizationid = -1;
            if (!int.TryParse(ConfigurationManager.AppSettings["OrganizationID"], out organizationid))
            {
                LogError("None or invalid 'OrganizationID' specified in .config");
                return false;
            }

            using (SqlConnection sqlconnection = new SqlConnection(connectionstring))
            {

                try
                {
                    sqlconnection.Open();
                    return true;
                }
                catch (Exception ex)
                {
                    LogError($"Could not connect to SQL-server: [{connectionstring}]");
                    LogError(ex.Message);
                    return false;
                }
                finally
                {
                    sqlconnection.Close();
                }
            }


        }

        public void WriteFile()
        {
            var filename = ConfigurationManager.AppSettings["ExportFile"];
            var filenamefiltered = ConfigurationManager.AppSettings["ExportFilteredFile"];

            var writer = new StreamWriter(filename) { AutoFlush = true };
            var writerfiltered = new StreamWriter(filenamefiltered) { AutoFlush = true };


            writer.WriteLine($"{header},STATUS");
            writerfiltered.WriteLine($"{header},STATUS");

            foreach(var isalapatient in isalaPatients.OrderBy(ip => ip.LineNumber))
            {
                writer.WriteLine(isalapatient.ToLine());
                if (!isalapatient.Status.StartsWith("NOT FOUND"))
                {
                    writerfiltered.WriteLine(isalapatient.ToLine());
                }
            }

            writer.Dispose();
            writerfiltered.Dispose();
        }



        public void LogError(string message)
        {
            Log(message, true);
        }
        public void Log(string message, bool error = false)
        {
            Console.ForegroundColor = error ? ConsoleColor.Red : ConsoleColor.White;
            Console.WriteLine($"{DateTime.Now.ToString("HH:mm:ss")} - {message}");
            Console.ResetColor();
        }

        public void Progress(string message)
        {
            Console.Title = $"{DateTime.Now.ToString("HH:mm:ss")} - {message}";
        }


    }

    public class IsalaPatient
    {
        public string Old { get; set; }
        public string GebDat { get; set; }
        public string Geslacht { get; set; }
        public string BSN { get; set; }
        public string NewUitgevuld { get; set; }
        public string NewNotMerged { get; set; }
        public string New { get; set; }

        public int LineNumber { get; set; }
        public string Status { get; set; }

        public IsalaPatient FromString(string[] splitted, int lineNumber)
        {
            LineNumber = lineNumber;

            Old = splitted.ElementAtOrDefault(0);
            GebDat = splitted.ElementAtOrDefault(1);
            Geslacht = splitted.ElementAtOrDefault(2);
            BSN = splitted.ElementAtOrDefault(3);
            NewUitgevuld = splitted.ElementAtOrDefault(4);
            NewNotMerged = splitted.ElementAtOrDefault(5);
            New = splitted.ElementAtOrDefault(6);

            if (splitted.Length != 7)
            {
                Status = $"ERROR: Wrong format not 7 parts";
            }
            else if (string.IsNullOrEmpty(Old) || string.IsNullOrEmpty(New))
            {
                Status = "ERROR: OLD or NEW is empty";
            }

            return this;
        }

        public string ToLine()
        {
            return $"{Old},{GebDat},{Geslacht},{BSN},{NewUitgevuld},{NewNotMerged},{New},{Status}";
        }

    }

    public class PointClient
    {
        public int ClientID { get; set; }
        public int TransferID { get; set; }
        public int FlowInstanceID { get; set; }
        public string CivilServiceNumber { get; set; }
        public string PatientNumber { get; set; }
        public string Gender { get; set; }
        public DateTime BirthDate { get; set; }

        public bool Deleted { get; set; }
        public bool InList { get; set; } = false;


        public PointClient FromReader(SqlDataReader reader)
        {
            ClientID = int.Parse(reader["ClientID"].ToString());
            TransferID = int.Parse(reader["TransferID"].ToString());
            FlowInstanceID = int.Parse(reader["FlowInstanceID"].ToString());
            Deleted = reader["Deleted"] != DBNull.Value && (reader["Deleted"].ToString() == "1" || reader["Deleted"].ToString() == "True");
            CivilServiceNumber = reader["CivilServiceNumber"].ToString();
            PatientNumber = reader["PatientNumber"].ToString();
            Gender = reader["Gender"].ToString();
            if (reader["BirthDate"] == DBNull.Value)
            {
                BirthDate = DateTime.MinValue;
            }
            else
            {
                try
                {
                    BirthDate = DateTime.ParseExact(reader["BirthDate"].ToString(), new[] { "yyyy-MM-dd HH:mm:ss.fff", "d-M-yyyy HH:mm:ss", "dd-MM-yy HH:mm:ss" }, CultureInfo.InvariantCulture, DateTimeStyles.AssumeLocal);
                }
                catch(Exception ex)
                {
                    Console.WriteLine("ERROR Converting birthdate: " + reader["BirthDate"].ToString());
                    BirthDate = DateTime.MinValue;

                }

            }


            return this;
        }
    }
}
