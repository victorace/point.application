﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Isala_Renumbering
{
    class Program
    {
        static void Main(string[] args)
        {

            var process = new Process();
            process.ReadFile();
            process.ReadClients();

            if (process.TestConnection())
            {

                process.CompareAndUpdate();
                process.ShowTotals();

                process.WriteFile();
            }

            Console.WriteLine($"Done @{DateTime.Now.ToString("HH:mm:ss")}");
            Console.ReadKey();


        }
    }
}
