﻿using CommandLine;
using System;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Configuration;
using System.Web.Security;

namespace digest
{
    public static class Program
    {
        static void Main(string[] args)
        {
            var c = Console.ForegroundColor;

            var configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            MachineKeySection machinekey = (MachineKeySection)configuration.GetSection("system.web/machineKey");

            Parser.Default.ParseArguments<ProtectOptions, UnprotectOptions>(args)
                .MapResult(
                (ProtectOptions opts) => RunProtectAndReturnExitCode(opts, machinekey),
                (UnprotectOptions opts) => RunUnprotectAndReturnExitCode(opts, machinekey),
            errs => 1);

            Console.ForegroundColor = c;
        }

        private static object RunProtectAndReturnExitCode(ProtectOptions opts, MachineKeySection machinekey)
        {
            try
            {
                Console.WriteLine();

                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine($"Value:{opts.Value}");

                Console.WriteLine();
                Console.WriteLine("Using the following machineKey:");
                Console.WriteLine($"\tvalidationKey: {machinekey.ValidationKey}");
                Console.WriteLine($"\tvalidationKey: {machinekey.ValidationAlgorithm}");
                Console.WriteLine($"\tdecryptionKey: {machinekey.DecryptionKey}");
                Console.WriteLine($"\tdecryptionKey: {machinekey.Decryption}");
                Console.WriteLine();
                Console.WriteLine();

                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Result:");
                Console.WriteLine($"\t{DigestHelper.CreateDigest(opts.Value)}");
                
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine();
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                Console.WriteLine();
            }

            return (bool)true;
        }

        private static object RunUnprotectAndReturnExitCode(UnprotectOptions opts, MachineKeySection machinekey)
        {
            try
            {
                Console.WriteLine();

                Console.ForegroundColor = ConsoleColor.Yellow;

                Console.WriteLine($"Value:{opts.Value}");
                Console.WriteLine($"Digest:{opts.Digest}");

                Console.WriteLine();
                Console.WriteLine("Using the following machineKey:");
                Console.WriteLine($"\tvalidationKey: {machinekey.ValidationKey}");
                Console.WriteLine($"\tvalidationKey: {machinekey.ValidationAlgorithm}");
                Console.WriteLine($"\tdecryptionKey: {machinekey.DecryptionKey}");
                Console.WriteLine($"\tdecryptionKey: {machinekey.Decryption}");
                Console.WriteLine();
                Console.WriteLine();

                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Result:");
                Console.WriteLine($"\t{DigestHelper.GetPlainDigest(opts.Digest)}");

                var checkdigest = DigestHelper.CheckDigest(opts.Value, opts.Digest);
                if (!checkdigest)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine();
                    Console.WriteLine("Digest check failed!");
                }
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine();
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                Console.WriteLine();
            }

            return (bool)true;
        }
    }

    [Verb("protect", HelpText = "MachineKey.Protect")]
    public class ProtectOptions
    {
        [Option(Required = true, HelpText = "Value to Protect")]
        public string Value { get; set; }
    }

    [Verb("unprotect", HelpText = "MachineKey.Unprotect")]
    public class UnprotectOptions
    {
        [Option(Required = true, HelpText = "Value to Unprotect")]
        public string Value { get; set; }
        [Option(Required = true, HelpText = "Digest")]
        public string Digest { get; set; }
    }

    public static class DigestHelper
    {
        public static string CreateDigest(object value)
        {
            if (value == null)
            {
                return "";
            }
            byte[] protectedbytes = MachineKey.Protect(Encoding.UTF8.GetBytes(Convert.ToString(value)), "Encryption");
            return protectedbytes.ByteArrayToHexString();
        }

        public static string GetPlainDigest(string digest)
        {
            string plaindigest = string.Empty;
            byte[] plainbytes = MachineKey.Unprotect(digest.HexStringToByteArray(), "Encryption");
            plaindigest = Encoding.UTF8.GetString(plainbytes);
            return plaindigest;
        }

        public static bool CheckDigest(object value, string digest)
        {
            if (String.IsNullOrWhiteSpace(digest) || value == null)
            {
                return false;
            }

            return GetPlainDigest(digest) == Convert.ToString(value);
        }
    }

    public static class StringExtensions
    {
        /// <summary>
        /// Fastest methods to convert byte[] to string and vice versa
        /// http://stackoverflow.com/questions/623104/byte-to-hex-string/3974535#3974535
        /// </summary>

        public static string ByteArrayToHexString(this byte[] bytes)
        {
            char[] c = new char[bytes.Length * 2];

            byte b;

            for (int bx = 0, cx = 0; bx < bytes.Length; ++bx, ++cx)
            {
                b = ((byte)(bytes[bx] >> 4));
                c[cx] = (char)(b > 9 ? b + 0x37 + 0x20 : b + 0x30);

                b = ((byte)(bytes[bx] & 0x0F));
                c[++cx] = (char)(b > 9 ? b + 0x37 + 0x20 : b + 0x30);
            }

            return new string(c);
        }

        public static byte[] HexStringToByteArray(this string s)
        {
            if (String.IsNullOrWhiteSpace(s) || s.Length % 2 != 0)
                return new byte[0];

            byte[] buffer = new byte[s.Length / 2];
            char c;
            for (int bx = 0, sx = 0; bx < buffer.Length; ++bx, ++sx)
            {
                // Convert first half of byte
                c = s[sx];
                buffer[bx] = (byte)((c > '9' ? (c > 'Z' ? (c - 'a' + 10) : (c - 'A' + 10)) : (c - '0')) << 4);

                // Convert second half of byte
                c = s[++sx];
                buffer[bx] |= (byte)(c > '9' ? (c > 'Z' ? (c - 'a' + 10) : (c - 'A' + 10)) : (c - '0'));
            }

            return buffer;
        }

        public static bool IsBase64String(this string s)
        {
            if (String.IsNullOrEmpty(s))
            {
                return false;
            }

            s = s.Trim();
            return (s.Length % 4 == 0) && Regex.IsMatch(s, @"^[a-zA-Z0-9\+/]*={0,3}$", RegexOptions.None);
        }

        public static string Base64Decode(this string s)
        {
            if (!IsBase64String(s))
            {
                return s;
            }
            return Encoding.UTF8.GetString(Convert.FromBase64String(s));
        }
    }
}
