﻿using CommandLine;
using Microsoft.ServiceBus;
using Microsoft.ServiceBus.Messaging;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace ServiceBusMaintenance
{
    public class Program
    {
        static void Main(string[] args)
        {
            var result = CommandLine.Parser.Default.ParseArguments<Options>(args).
               WithParsed(options => new ServiceBusMaintenance(options).Clear());
        }
    }

    public class Options
    {
        [Option(Required = true, HelpText = "Service Bus connection string")]
        public string ConnectionString { get; set; }

        [Option(HelpText = "List of Service Bus Queue names")]
        public IEnumerable<string> Queues { get; set; }

        [Option(HelpText = "Get list of Service Bus Queue names")]
        public bool List { get; set; }
    }


    public class ServiceBusMaintenance
    {
        private string connectionstring;
        private List<string> queues;
        private bool list;
        private NamespaceManager ns;

        public ServiceBusMaintenance(Options options)
        {
            if (ConfigurationManager.ConnectionStrings[options.ConnectionString] != null)
            {
                this.connectionstring = ConfigurationManager.ConnectionStrings[options.ConnectionString].ToString();
            }
            else
            {
                this.connectionstring = options.ConnectionString;
            }

            ns = NamespaceManager.CreateFromConnectionString(connectionstring);

            this.list = options.List;

            this.queues = options.Queues.ToList();
        }

        public void Clear()
        {
            if (!queues.Any())
            {
                queues = new List<string>();
                foreach (var queue in ns.GetQueues())
                {
                    queues.Add(queue.Path);
                }
            }

            foreach (var path in queues)
            {
                if (list)
                {
                    listqueue(path);
                }
                else
                {
                    clearqueue(path);
                }
            }
        }

        private void listqueue(string path)
        {
            try
            {
                if (ns.QueueExists(path))
                {
                    var queue = ns.GetQueue(path);
                    long count = 0;
                    long deadlettercount = 0;
                    if (queue != null)
                    {
                        count = ns.GetQueue(path).MessageCount;
                        deadlettercount = queue.MessageCountDetails.DeadLetterMessageCount;
                    }
                    Console.WriteLine($"{path}({count}:{deadlettercount}).");
                }
                else
                {
                    Console.WriteLine($"No queue found with the name '{path}'.");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"Fatal error: {e.Message}.");
            }
        }

        private void clearqueue(string path)
        {
            try
            {
                if (ns.QueueExists(path))
                {
                    var queue = ns.GetQueue(path);
                    long count = 0;
                    long deadlettercount = 0;
                    if (queue != null) {
                        count = ns.GetQueue(path).MessageCount;
                        deadlettercount = queue.MessageCountDetails.DeadLetterMessageCount;
                    }
                    QueueClient queueclient = QueueClient.CreateFromConnectionString(connectionstring, path, ReceiveMode.ReceiveAndDelete);
                    while (queueclient.Peek() != null)
                    {
                        queueclient.Receive();
                    }
                    queueclient.Close();
                    Console.WriteLine($"{path}({count}:{deadlettercount}) cleared.");
                }
                else
                {
                    Console.WriteLine($"No queue found with the name '{path}'.");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"Fatal error: {e.Message}.");
            }
        }
    }
}