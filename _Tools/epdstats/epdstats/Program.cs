﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace epdstats
{
    public class EPDUsage
    {
        public static T DeserializeXMLFileToObject<T>(string XmlFilename)
        {
            T returnObject = default(T);
            if (string.IsNullOrEmpty(XmlFilename)) return default(T);

            try
            {
                StreamReader xmlStream = new StreamReader(XmlFilename);
                XmlSerializer serializer = new XmlSerializer(typeof(T));
                returnObject = (T)serializer.Deserialize(xmlStream);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return returnObject;
        }

        private XmlDocument xmldocument;
        private int fields;

        public Dictionary<string, Dictionary<string, string>> UsagePerFile { get; private set; }

        public Dictionary<string, int> Usage { get; private set; }
        public Dictionary<string, int> FieldsPerRequest { get; private set; }
        public List<string> PatientNumbers { get; private set; }

        public void Get(string path)
        {
            fields = 0;
            UsagePerFile = new Dictionary<string, Dictionary<string, string>>();
            Usage = new Dictionary<string, int>();
            FieldsPerRequest = new Dictionary<string, int>();
            PatientNumbers = new List<string>();

            foreach (var xmlfile in Directory.GetFiles(path, "*.xml"))
            {
                try
                {
                    fields = 0;

                    var xmldata = File.ReadAllText(xmlfile);
                    xmldocument = new XmlDocument();
                    xmldocument.LoadXml(xmldata);

                    //PointEPD
                    //    persoonsgegevensField
                    //        PointPersoonsgegevens
                    //            patientgegevensField
                    //                patientidentificatieField
                    XmlNode patientnummerField = xmldocument.SelectSingleNode("/PointEPD//patientnummerField");
                    if (!string.IsNullOrWhiteSpace(patientnummerField?.InnerText))
                    {
                        string patientnumber = patientnummerField.InnerText;
                        if (!PatientNumbers.Contains(patientnumber))
                        {
                            PatientNumbers.Add(patientnumber);
                        }
                    }
                    traverseNode(xmlfile, xmldocument.DocumentElement);

                    FieldsPerRequest.Add(xmlfile, fields);
                }
                catch
                {
                    Console.WriteLine($"Error processing '{xmlfile}'.");
                }
            }
        }

        private void traverseNode(string filename, XmlNode node)
        {
            var file = Path.GetFileNameWithoutExtension(filename);

            if ((node.NodeType == XmlNodeType.Element && node.Value == null && !node.HasChildNodes) ||
                node.NodeType == XmlNodeType.Text)
            {
                if (node.Name == "PointMedischeGegevensAllergienAllergie" || node.Name == "beginKlachtenField")
                {
                    //Debugger.Break();
                }

                var names = new List<string>();
                var pn = node.ParentNode;
                while (pn != null)
                {
                    names.Add(pn.Name);
                    pn = pn.ParentNode;
                }
                names.Reverse();
                names.Add(node.Name);
                var fullname = string.Join(".", names);

                var count = 0;
                if (Usage.ContainsKey(fullname))
                {
                    count = Usage[fullname];
                }

                if (!string.IsNullOrEmpty(node.InnerText))
                {
                    var dict = new Dictionary<string, string>();
                    
                    if (UsagePerFile.ContainsKey(file))
                    {
                        dict = UsagePerFile[file];
                    }
                    else
                    {
                        UsagePerFile[file] = dict;
                    }

                    fullname = fullname.Replace("#document.", "");
                    fullname = fullname.Replace(".#text", "");

                    dict[fullname] = node.InnerText.Replace(Environment.NewLine, "");

                    count++;
                }

                fields++;
                Usage[fullname] = count;
            }

            XmlNodeList children = node.ChildNodes;
            foreach (XmlNode child in children)
            {
                traverseNode(file, child);
            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var epdusage = new EPDUsage();
            epdusage.Get(@"C:\VSONLINE\POINT.Application\Tools\epdstats\data\received\");

            //Console.WriteLine($"('{string.Join(",", epdusage.PatientNumbers)}')");

            foreach (var filename in epdusage.UsagePerFile.Keys)
            {
                using (StreamWriter writetext = new StreamWriter($"C:\\VSONLINE\\POINT.Application\\Tools\\epdstats\\data\\arthur\\{filename}.txt"))
                {
                    foreach (var key in epdusage.UsagePerFile[filename].Keys)
                    {
                        writetext.WriteLine($"{key} = {epdusage.UsagePerFile[filename][key]}");
                    }
                }
            }

            System.Console.ReadKey();
        }
    }
}
