PointEPD.algemeneMentaleFunctiesField.PointAlgemeneMentaleFuncties.bijzonderhedenField = Ja
PointEPD.algemeneMentaleFunctiesField.PointAlgemeneMentaleFuncties.toelichtingField = . Risico delier: positief
PointEPD.anamneseDatumField = 201802071448
PointEPD.communicatieField.PointCommunicatie.bijzonderhedenField =  Problemen met horen
PointEPD.communicatieField.PointCommunicatie.toelichtingField =  Problemen met horen
PointEPD.etenDrinkenField.PointEtenDrinken.verpleegkundigeInterventiesField =  geen
PointEPD.gezinssamenstellingEnWoonsituatieField.PointGezinssamenstellingEnWoonsituatie.burgerlijkeStaatField = Gehuwd
PointEPD.gezinssamenstellingEnWoonsituatieField.PointGezinssamenstellingEnWoonsituatie.kinderenField.PointGezinssamenstellingEnWoonsituatieKinderen.aantalField = 2
PointEPD.gezinssamenstellingEnWoonsituatieField.PointGezinssamenstellingEnWoonsituatie.kinderenField.PointGezinssamenstellingEnWoonsituatieKinderen.aanwezigField = ja
PointEPD.gezinssamenstellingEnWoonsituatieField.PointGezinssamenstellingEnWoonsituatie.soortWoningField = appartement of flatwoning met lift
PointEPD.huidField.PointHuid.bijzonderhedenField = Ja
PointEPD.huidField.PointHuid.decubitusField.PointHuidDecubitus.aanwezigField = Nee
PointEPD.huidField.PointHuid.overigeHuidaandoeningenField.PointHuidOverigeHuidaandoeningen.aanwezigField = Ja
PointEPD.huidField.PointHuid.overigeHuidaandoeningenField.PointHuidOverigeHuidaandoeningen.soortField = droge huid en verkleurde huid (maar geen categorie 1 decubitus) / Prepurse score: 52 / Risicoscore decubitus: negatief
PointEPD.levensovertuigingField.PointLevensovertuiging.religieField = Christelijk
PointEPD.medischeGegevensField.PointMedischeGegevens.allergienField.PointMedischeGegevensAllergienAllergie.soortEnReactieField = KIPPENEIPRODUCTEN,  - Kippen eiwitTRAMADOL, OPIOIDEN, 
PointEPD.medischeGegevensField.PointMedischeGegevens.allergienField.PointMedischeGegevensAllergienAllergie.volgnrField = 1
PointEPD.medischeGegevensField.PointMedischeGegevens.hoofdbehandelaarField.PointMedischeGegevensHoofdbehandelaar.persoonsnaamField.Persoonsnaam.geslachtsnaamField = Seh- Interne Geneeskunde Arts
PointEPD.medischeGegevensField.PointMedischeGegevens.hoofdbehandelaarField.PointMedischeGegevensHoofdbehandelaar.specialismeField = SEHINT
PointEPD.medischeGegevensField.PointMedischeGegevens.hoofdbehandelaarField.PointMedischeGegevensHoofdbehandelaar.zorgverlenerIDField = SEHINT
PointEPD.medischeGegevensField.PointMedischeGegevens.medischeDiagnoseField = Pneumonie
PointEPD.meetwaardenField.PointMeetwaarden.ademhalingField.PointMeetwaardenAdemhaling.toelichtingField = 2 l/min, neusbril (dd 20-02-2018 08:24:24)
PointEPD.meetwaardenField.PointMeetwaarden.ademhalingField.PointMeetwaardenAdemhaling.zuurstofgebruikField = Ja
PointEPD.meetwaardenField.PointMeetwaarden.bloeddrukField.PointMeetwaardenBloeddruk.datumBloeddrukField = 20-02-2018 08:12:00
PointEPD.meetwaardenField.PointMeetwaarden.bloeddrukField.PointMeetwaardenBloeddruk.diastoleField = 65 mmHg
PointEPD.meetwaardenField.PointMeetwaarden.bloeddrukField.PointMeetwaardenBloeddruk.systoleField = 140 mmHg
PointEPD.meetwaardenField.PointMeetwaarden.gewichtField.PointMeetwaardenGewicht.datumGewichtField = 19-02-2018 08:16:00
PointEPD.meetwaardenField.PointMeetwaarden.gewichtField.PointMeetwaardenGewicht.lichaamsGewichtField = 95.4, met kleding en schoenen kg
PointEPD.meetwaardenField.PointMeetwaarden.lengteField.PointMeetwaardenLengte.datumLengteField = 08-02-2018 13:42:00
PointEPD.meetwaardenField.PointMeetwaarden.lengteField.PointMeetwaardenLengte.lichaamsLengteField = 181 cm
PointEPD.meetwaardenField.PointMeetwaarden.polsField.PointMeetwaardenPols.datumFrequentieField = 20-02-2018 08:12:00
PointEPD.meetwaardenField.PointMeetwaarden.polsField.PointMeetwaardenPols.frequentieField = 78/min
PointEPD.meetwaardenField.PointMeetwaarden.temperatuurField.PointMeetwaardenTemperatuur.datumTemperatuurField = 20-02-2018 08:12:00
PointEPD.meetwaardenField.PointMeetwaarden.temperatuurField.PointMeetwaardenTemperatuur.lichaamsTemperatuurField = 37.5 ?C
PointEPD.mobiliteitField.PointMobiliteit.valrisicoField.PointMobiliteitValrisico.verhoogdValrisicoAanwezigField = Nee
PointEPD.mondverzorgingField.PointMondverzorging.gebitsprotheseAanwezigField = Ja
PointEPD.mondverzorgingField.PointMondverzorging.ondersteuningNodigField = Nee
PointEPD.opnameNummerField = 1000532986
PointEPD.organisatieField.PointOrganisatieContactgegevensOverdragendeOrganisatie.locatieField = Leiden
PointEPD.organisatieField.PointOrganisatieContactgegevensOverdragendeOrganisatie.naamInstellingField = LUMC
PointEPD.organisatieField.PointOrganisatieContactgegevensOverdragendeOrganisatie.organisatieIDField = 019282
PointEPD.organisatieField.PointOrganisatieContactgegevensOverdragendeOrganisatie.soortOrganisatieField = Universitair Medisch Centrum
PointEPD.patientnummerField = 3000991
PointEPD.persoonsgegevensField.PointPersoonsgegevens.adresgegevensField.PointPersoonsgegevensAdresgegevens.adrestypeField = Woonadres
PointEPD.persoonsgegevensField.PointPersoonsgegevens.adresgegevensField.PointPersoonsgegevensAdresgegevens.huisnummerField = 79
PointEPD.persoonsgegevensField.PointPersoonsgegevens.adresgegevensField.PointPersoonsgegevensAdresgegevens.plaatsnaamField = Leiden
PointEPD.persoonsgegevensField.PointPersoonsgegevens.adresgegevensField.PointPersoonsgegevensAdresgegevens.postcodeField = 2334 DA
PointEPD.persoonsgegevensField.PointPersoonsgegevens.adresgegevensField.PointPersoonsgegevensAdresgegevens.straatField = Swammerdampad
PointEPD.persoonsgegevensField.PointPersoonsgegevens.contactpersoonField.PointPersoonsgegevensContactpersoon.persoonsnaamField.Persoonsnaam.geslachtsnaamField = Mw. Eshuis
PointEPD.persoonsgegevensField.PointPersoonsgegevens.contactpersoonField.PointPersoonsgegevensContactpersoon.telefoonnummerField = 071-512601206-22666593
PointEPD.persoonsgegevensField.PointPersoonsgegevens.contactpersoonField.PointPersoonsgegevensContactpersoon.typeContactpersoonField = Echtgenote
PointEPD.persoonsgegevensField.PointPersoonsgegevens.patientgegevensField.PointPersoonsgegevensPatientgegevens.emailAdresField = sinto@hetnet.nl
PointEPD.persoonsgegevensField.PointPersoonsgegevens.patientgegevensField.PointPersoonsgegevensPatientgegevens.geboortedatumField = 27-09-1937
PointEPD.persoonsgegevensField.PointPersoonsgegevens.patientgegevensField.PointPersoonsgegevensPatientgegevens.geslachtField = mannelijk
PointEPD.persoonsgegevensField.PointPersoonsgegevens.patientgegevensField.PointPersoonsgegevensPatientgegevens.patientidentificatieField = 027076921
PointEPD.persoonsgegevensField.PointPersoonsgegevens.patientgegevensField.PointPersoonsgegevensPatientgegevens.persoonsnaamField.Persoonsnaam.geslachtsnaamField = Eshuis
PointEPD.persoonsgegevensField.PointPersoonsgegevens.patientgegevensField.PointPersoonsgegevensPatientgegevens.persoonsnaamField.Persoonsnaam.voorlettersField = B.H.
PointEPD.persoonsgegevensField.PointPersoonsgegevens.patientgegevensField.PointPersoonsgegevensPatientgegevens.persoonsnaamField.Persoonsnaam.voornamenField = Berend Herman
PointEPD.persoonsgegevensField.PointPersoonsgegevens.patientgegevensField.PointPersoonsgegevensPatientgegevens.telefoonnummerField = 071-5126012
PointEPD.persoonsgegevensField.PointPersoonsgegevens.verzekeringsgegevensField.PointPersoonsgegevensVerzekeringsgegevens.iDVerzekeringsmaatschappijField = 7085
PointEPD.persoonsgegevensField.PointPersoonsgegevens.verzekeringsgegevensField.PointPersoonsgegevensVerzekeringsgegevens.naamVerzekeringsmaatschappijField = ZORG EN ZEKERHEID
PointEPD.persoonsgegevensField.PointPersoonsgegevens.verzekeringsgegevensField.PointPersoonsgegevensVerzekeringsgegevens.polisnummerField = 1728245400
PointEPD.pijnField.PointPijn.mateVanPijnField = 4
PointEPD.pijnField.PointPijn.pijnBijBewegingField = 3
PointEPD.pijnField.PointPijn.toelichtingField = VAS: 4
PointEPD.slaapField.PointSlaap.slapenField.PointSlaapSlapen.toelichtingField = Problemen met slaap: Nee.Hulpmiddelen om te slapen: Nee.
PointEPD.timestampField = 20180220111422
PointEPD.uitscheidingField.PointUitscheiding.verpleegkundigeInterventiesField = SA Heeft u problemen met urineren: nee.Hoe veel plast u per dag (ml):  stomazakje 2x daags legen.Frequent braken: Nee.
PointEPD.voedingField.PointVoeding.ondervoedingField.PointVoedingOndervoeding.aanwezigField = Nee
PointEPD.voedingField.PointVoeding.ondervoedingField.PointVoedingOndervoeding.meetmehodeField = MUST
PointEPD.voedingField.PointVoeding.ondervoedingField.PointVoedingOndervoeding.scoreField = 0
PointEPD.wassenField.PointWassen.ondersteuningNodigField = Ja
PointEPD.wassenField.PointWassen.verpleegkundigeInterventiesField = Hulp nodig bij baden of douchen. hulp bij wassen
PointEPD.zintuigenField.PointZintuigen.verpleegkundigeInterventiesField = Problemen met horen: Ja.Hulpmiddel: gehoorapparaat.Problemen met proeven: Nee.Problemen met ruiken: Nee.Problemen met voelen/tastzin: Nee.Problemen met zien: Ja.Hulpmiddel: bril/lezen.
