-- ID's zijn gelijk op A en P (03-10-2016)

SELECT  *
FROM    Organization
WHERE   OrganizationTypeID = 1
        AND Name LIKE '%vlietland%'; --> 325
SELECT  *
FROM    Location
WHERE   OrganizationID = 325; --> "Schiedam" = 299

SELECT  *
FROM    Organization
WHERE   OrganizationTypeID = 1
        AND Name LIKE '%franciscus%'; --> 600

UPDATE  Location
SET     OrganizationID = 600
WHERE   LocationID = 299;

PRINT 'Done.';