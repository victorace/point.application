-- ID's zijn gelijk op A en P (06-10-2016)

SELECT  *
FROM    Organization
WHERE   OrganizationTypeID = 1
        AND Name LIKE '%vlietland%'; --> 325
SELECT  *
FROM    Organization
WHERE   OrganizationTypeID = 1
        AND Name LIKE '%franciscus%'; --> 600

SELECT  DoctorID
FROM    Doctor
WHERE   OrganizationID = 325;
PRINT 'DoctorIDs currently connected to Vlietland.';

UPDATE  Doctor
SET     OrganizationID = 600
WHERE   OrganizationID = 325;

PRINT 'Done.';