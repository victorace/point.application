DISABLE TRIGGER Client.Client_InsertUpdate ON Client;
GO

IF OBJECT_ID(N'dbo.Patient_WasWordt_VL', N'U') IS NOT NULL
    DROP TABLE dbo.Patient_WasWordt_VL;

CREATE TABLE dbo.Patient_WasWordt_VL
    (
      ID1 VARCHAR(50) ,
      ID2 VARCHAR(50) ,
      TYPE VARCHAR(2)
    );
GO

PRINT 'Bulk insert d:\deployments\import db\Patient_WasWordt_Combi 9 sep.csv';

SET NOCOUNT ON;

BULK
INSERT dbo.Patient_WasWordt_VL
FROM 'd:\deployments\import db\Patient_WasWordt_Combi 9 sep.csv'
WITH
(
FIELDTERMINATOR = ',',
ROWTERMINATOR = '\n'
);
GO

SET NOCOUNT ON;

DECLARE @organizationname VARCHAR(255) = 'Vlietland Ziekenhuis';
DECLARE @clientid INT;
DECLARE @patientnumber AS VARCHAR(50);
DECLARE @ID1 VARCHAR(50);
DECLARE @ID2 VARCHAR(50);
DECLARE @TYPE VARCHAR(2) = 'VL';
DECLARE @DESTINATION VARCHAR(50);

DECLARE clientCursor CURSOR
FOR
    SELECT  Client.ClientID ,
            Client.PatientNumber
    FROM    Client WITH ( NOLOCK )
            JOIN Transfer WITH ( NOLOCK ) ON Client.ClientID = Transfer.ClientID
            JOIN Department WITH ( NOLOCK ) ON Transfer.DepartmentID = Department.DepartmentID
            JOIN Location WITH ( NOLOCK ) ON Department.LocationID = Location.LocationID
            JOIN Organization WITH ( NOLOCK ) ON Organization.OrganizationID = Location.OrganizationID
    WHERE   Organization.Name = @organizationname
            AND Organization.Inactive = 0;

OPEN clientCursor;

PRINT 'Patienten omnummering ' + @organizationname;

FETCH NEXT FROM clientCursor INTO @clientid, @patientnumber;

WHILE @@FETCH_STATUS = 0
    BEGIN

        SET @DESTINATION = '';
        SELECT  @DESTINATION = ID2
        FROM    Patient_WasWordt_VL
        WHERE   ID1 = @patientnumber
                AND [TYPE] = @TYPE;

        IF EXISTS ( SELECT  1
                    FROM    Patient_WasWordt_VL
                    WHERE   ID1 = @patientnumber
                            AND [TYPE] = @TYPE )
            BEGIN

                UPDATE  Client
                SET     Client.PatientNumber = @DESTINATION
                WHERE   Client.ClientID = @clientid
                        AND Client.PatientNumber = @patientnumber;

                UPDATE  FlowInstanceSearchValues
                SET     FlowInstanceSearchValues.PatientNumber = @DESTINATION
                FROM    dbo.Client WITH ( NOLOCK )
                        INNER JOIN dbo.Transfer WITH ( NOLOCK ) ON Transfer.ClientID = Client.ClientID
                        INNER JOIN dbo.FlowInstance WITH ( NOLOCK ) ON FlowInstance.TransferID = Transfer.TransferID
                WHERE   Client.ClientID = @clientid
                        AND Client.PatientNumber = @patientnumber;

                PRINT 'Patientnumber old ' + @patientnumber + ' '
                    + 'Patientnumber new ' + @DESTINATION + ' ClientID '
                    + STR(@clientid);
            END;
    
        FETCH NEXT FROM clientCursor INTO @clientid, @patientnumber;
    END;

CLOSE clientCursor;
DEALLOCATE clientCursor;

PRINT 'Done!';
GO

ENABLE TRIGGER Client.Client_InsertUpdate ON Client;

SET NOCOUNT OFF;