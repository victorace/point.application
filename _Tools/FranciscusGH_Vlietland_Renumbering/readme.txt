PBIs / Tasks connected to this are (13696, 13539, 13628, 13538, 10400, 10414)

Steps

*Deliver <filename>.csv to ProAct, so that it can be placed on the server*

*Update Patient_WasWordt_GH.sql and Patient_WasWordt_VL.sql (with the correct path for the <filename>.csv file)*

SQL:

1_Create backups.sql
2_Patient_WasWordt_GH.sql
3_Patient_WasWordt_VL.sql
4_Loca_Schiedam_naar_SFG.sql
5_Artsen_Vlietland_Samenvoegen_SFG.sql