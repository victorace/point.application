﻿using System;
using System.Threading;
using System.Windows.Forms;

namespace GenerateBSN
{
    class Program
    {
        static void Main(string[] args)
        {
            int rest;
            string bsn;
            Random random = new Random();
            do
            {
                bsn = ""; int total = 0; for (int i = 0; i < 8; i++)
                {
                    int rndDigit = random.Next(0, i == 0 ? 2 : 9);
                    total += rndDigit * (9 - i);
                    bsn += rndDigit;
                }
                rest = total % 11;
            }
            while (rest > 9);

            Thread thread = new Thread(() => Clipboard.SetText(bsn + rest));
            thread.SetApartmentState(ApartmentState.STA); //Set the thread to STA
            thread.Start();
            thread.Join();

            Console.WriteLine(bsn + rest);
        }
    }
}
