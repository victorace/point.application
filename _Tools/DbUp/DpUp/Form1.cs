﻿using DbUp;
using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace DpUp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void ReadConnectionStrings()
        {
            var connectionStrings = System.Configuration.ConfigurationManager.ConnectionStrings;
            connectionBox.Items.Clear();
            foreach (ConnectionStringSettings connectionString in connectionStrings)
            {
                connectionBox.Items.Add(connectionString);
            }
        }

        private void ReadFolders()
        {
            folderBox.Items.Clear();
            var dirinfo = new DirectoryInfo(".");
            foreach (var folder in dirinfo.GetDirectories())
            {
                folderBox.Items.Add(folder.Name);
            }
        }

        private void ReadScripts(string folder)
        {
            scriptListBox.Items.Clear();
            var dirinfo = new DirectoryInfo(folder);
            var filelist = dirinfo.GetFiles("*.sql");
            foreach (var file in filelist)
            {
                scriptListBox.Items.Add(file.Name);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            ReadConnectionStrings();
            ReadFolders();
        }

        private void goButton_Click(object sender, EventArgs e)
        {
            if(ConnectionString == null)
            {
                MessageBox.Show("Kies eerst een connectiestring");
                return;
            }

            if(folderBox.SelectedItem == null)
            {
                MessageBox.Show("Kies eerst een Folder");
                return;
            }

            var upgrader = DeployChanges.To.
                SqlDatabase(ConnectionString.ConnectionString)
                .JournalToSqlTable("", "_SchemaVersions")
                .WithScriptsFromFileSystem(folderBox.SelectedItem as string)
                .LogToConsole()
                .Build();

            var result = upgrader.PerformUpgrade();

            if (!result.Successful)
            {
                MessageBox.Show(result.Error.Message);
            } else
            {
                MessageBox.Show("Succes! " + result.Scripts.Count() + " uitgevoerd");
            }
        }

        private ConnectionStringSettings ConnectionString { get; set; }
        private void connectionBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.ConnectionString = connectionBox.SelectedItem as ConnectionStringSettings;
        }

        private void folderBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            ReadScripts(folderBox.SelectedItem.ToString());
        }
    }
}
