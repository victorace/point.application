﻿namespace DpUp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.connectionBox = new System.Windows.Forms.ComboBox();
            this.goButton = new System.Windows.Forms.Button();
            this.scriptListBox = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.folderBox = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // connectionBox
            // 
            this.connectionBox.DisplayMember = "Name";
            this.connectionBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.connectionBox.FormattingEnabled = true;
            this.connectionBox.Location = new System.Drawing.Point(102, 12);
            this.connectionBox.Name = "connectionBox";
            this.connectionBox.Size = new System.Drawing.Size(455, 21);
            this.connectionBox.TabIndex = 0;
            this.connectionBox.SelectedIndexChanged += new System.EventHandler(this.connectionBox_SelectedIndexChanged);
            // 
            // goButton
            // 
            this.goButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.goButton.Location = new System.Drawing.Point(405, 244);
            this.goButton.Name = "goButton";
            this.goButton.Size = new System.Drawing.Size(152, 43);
            this.goButton.TabIndex = 1;
            this.goButton.Text = "Update";
            this.goButton.UseVisualStyleBackColor = true;
            this.goButton.Click += new System.EventHandler(this.goButton_Click);
            // 
            // scriptListBox
            // 
            this.scriptListBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.scriptListBox.FormattingEnabled = true;
            this.scriptListBox.Location = new System.Drawing.Point(17, 76);
            this.scriptListBox.Name = "scriptListBox";
            this.scriptListBox.Size = new System.Drawing.Size(539, 160);
            this.scriptListBox.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "ConnectieString";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Script folder";
            // 
            // folderBox
            // 
            this.folderBox.DisplayMember = "Name";
            this.folderBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.folderBox.FormattingEnabled = true;
            this.folderBox.Location = new System.Drawing.Point(102, 39);
            this.folderBox.Name = "folderBox";
            this.folderBox.Size = new System.Drawing.Size(455, 21);
            this.folderBox.TabIndex = 4;
            this.folderBox.SelectedIndexChanged += new System.EventHandler(this.folderBox_SelectedIndexChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(569, 299);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.folderBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.scriptListBox);
            this.Controls.Add(this.goButton);
            this.Controls.Add(this.connectionBox);
            this.Name = "Form1";
            this.Text = "DbUp uitvoeren";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox connectionBox;
        private System.Windows.Forms.Button goButton;
        private System.Windows.Forms.ListBox scriptListBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox folderBox;
    }
}

