﻿using Point.HL7.Processing.Logic.DataContracts;
using Point.HL7.Processing.Models;
using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Net;
using System.Net.Http;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Xml;

namespace ConsoleFormService
{
    public class Program
    {

        public static bool _remove_headers = false;
        public static bool _basichttpbinding = false;

        static void Main(string[] args)
        {
            using (HttpClient httpclient = new HttpClient())
            {
                try
                {
                    //httpclient.BaseAddress = new Uri("https://webservice2019t1.point.local/");
                    //httpclient.BaseAddress = new Uri("https://pointapireview.verzorgdeoverdracht.nl/api/");
                    //httpclient.DefaultRequestHeaders.Add("apikey", "R.6sMxDLz2ZDzsDsTcua9XLwfAwsxqx46dkuAz5KsU");
                    //httpclient.DefaultRequestHeaders.Add("apikey", "12345");

                    //var dateofbirth = DateTime.ParseExact("2008-12-03", "yyyy-MM-dd", null);
                    //int organizationid = 6312;
                    //string patientnumber = "106556472";
                    //string visitnumber = "178417579";
                    //var response = httpclient.GetAsync($"transfer/status?organizationid={organizationid.ToString()}&patientnumber={patientnumber}&visitnumber={visitnumber}&dateofbirth={dateofbirth.ToString("yyyy-MM-dd")}").GetAwaiter().GetResult();
                    //var result = response.Content.ReadAsStringAsync().GetAwaiter().GetResult();

                    //int organizationid = 6312;
                    //int employeeid = 38524;
                    //var response = httpclient.GetAsync($"Lookup/VerwijzingTypes").GetAwaiter().GetResult();
                    //var result = response.Content.ReadAsStringAsync().GetAwaiter().GetResult();

                    //var postdata = new
                    //{
                    //    employeeid = 36473,
                    //    organizationid = 6312,
                    //    departmentexternids = new List<string>()  {
                    //            "4ad8029f-911a-45b1-9342-7b393a3a38ad",
                    //            "f411a890-5505-4c45-8b45-daff19d2624e",
                    //            "foo",
                    //            "bar"
                    //        }
                    //};
                    //var content = new StringContent(JsonConvert.SerializeObject(postdata), Encoding.UTF8, "application/json");
                    //var response = httpclient.PostAsync($"employee/addtodepartment", content).GetAwaiter().GetResult();
                    //var result = response.Content.ReadAsStringAsync().GetAwaiter().GetResult();
                    //Console.WriteLine($"Status: ({(int)response.StatusCode}) {response.StatusCode}");
                    //Console.WriteLine($"{result}");

                    if (args.Length > 0)
                    {
                        string arg0 = args[0];
                        if (args.Length == 2)
                        {
                            string arg1 = args[1];
                            _remove_headers = string.Equals(arg1, "true", StringComparison.InvariantCultureIgnoreCase);
                        }
                        else if (args.Length == 3)
                        {
                            string arg2 = args[2];
                            _basichttpbinding = string.Equals(arg2, "basichttpbinding", StringComparison.InvariantCultureIgnoreCase);
                        }

                        if (string.Equals(arg0, "cerner.z_point_aanvraag", StringComparison.InvariantCultureIgnoreCase))
                        {
                            cerner_aanvraag();
                        }
                        else if (string.Equals(arg0, "cerner.z_point_overdracht", StringComparison.InvariantCultureIgnoreCase))
                        {
                            cerner_overdracht();
                        }
                        else if (string.Equals(arg0, "flevo.z_point_aanvraag", StringComparison.InvariantCultureIgnoreCase))
                        {
                            flevo_aanvraag();
                        }
                        else if (string.Equals(arg0, "flevo.z_point_overdracht", StringComparison.InvariantCultureIgnoreCase))
                        {
                            flevo_overdracht();
                        }
                    }
                    else
                    {
                        Console.WriteLine("No arguments.");
                    }
                }
                catch (WebException wex)
                {
                    var httpwebresponse = (HttpWebResponse)wex.Response;
                    Console.WriteLine("FATEL ERROR");
                    Console.WriteLine($"Status: ({(int)httpwebresponse.StatusCode}) {httpwebresponse.StatusCode}");
                    Console.WriteLine($"{httpwebresponse.StatusDescription}");

                    Console.WriteLine($"Message: {wex.Message}");
                    Console.WriteLine($"InnerException.Message: {wex.InnerException?.Message}");
                    Console.WriteLine($"StackTrace: {wex.StackTrace}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("FATEL ERROR");
                    Console.WriteLine($"Message: {ex.Message}");
                    Console.WriteLine($"InnerException.Message: {ex.InnerException?.Message}");
                    Console.WriteLine($"StackTrace: {ex.StackTrace}");
                }
            }

            if (Debugger.IsAttached)
            {
                Console.WriteLine();
                Console.WriteLine("Press any key to continue...");
                Console.ReadKey();
            }
        }

        public static Binding remove_headers(Binding binding)
        {
            // create a new binding based on the existing binding
            var customTransportSecurityBinding = new CustomBinding(binding);
            var customTransportSecurityBindingCopy = new CustomBinding(binding);

            // locate the TextMessageEncodingBindingElement - that's the party guilty of the inclusion of the "To"

            foreach (BindingElement element in customTransportSecurityBinding.Elements)
            {
                // and replace it with a version with no addressing
                // replace {Soap12 (http://www.w3.org/2003/05/soap-envelope) Addressing10 (http://www.w3.org/2005/08/addressing)}
                //    with {Soap12 (http://www.w3.org/2003/05/soap-envelope) AddressingNone (http://schemas.microsoft.com/ws/2005/05/addressing/none)}
                int index = customTransportSecurityBinding.Elements.IndexOf(element);

                if (element is TextMessageEncodingBindingElement)
                {
                    var textBindingElement = new TextMessageEncodingBindingElement
                    {
                        MessageVersion = MessageVersion.CreateVersion(EnvelopeVersion.Soap12, AddressingVersion.None)
                    };

                    customTransportSecurityBindingCopy.Elements[index] = textBindingElement;
                }
            }
            customTransportSecurityBinding = customTransportSecurityBindingCopy;
            customTransportSecurityBindingCopy = null;
            return customTransportSecurityBinding;
        }

        public static Binding create_binding()
        {
            Binding binding = new WSHttpBinding();
            ((WSHttpBinding)binding).Security.Mode = SecurityMode.Transport;
            ((WSHttpBinding)binding).Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
            if (_basichttpbinding)
            {
                binding = new BasicHttpBinding();
                ((BasicHttpBinding)binding).Security.Mode = BasicHttpSecurityMode.Transport;
                ((BasicHttpBinding)binding).Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
            }
            if (_remove_headers)
            {
                binding = remove_headers(binding);
            }
            return binding;
        }

        public static void cerner_aanvraag()
        {
            Console.WriteLine($"cerner_aanvraag");

            try
            {
                string endpoint = System.Configuration.ConfigurationManager.AppSettings["cerner.z_point_aanvraag.endpoint"];
                string username = System.Configuration.ConfigurationManager.AppSettings["cerner.z_point_aanvraag.username"];
                string password = System.Configuration.ConfigurationManager.AppSettings["cerner.z_point_aanvraag.password"];
                string patientnumber = System.Configuration.ConfigurationManager.AppSettings["cerner.z_point_aanvraag.epdrequest.patientnumber"];
                string visitnumber = System.Configuration.ConfigurationManager.AppSettings["cerner.z_point_aanvraag.epdrequest.visitnumber"];
                string civilservicenumber = System.Configuration.ConfigurationManager.AppSettings["cerner.z_point_aanvraag.epdrequest.civilservicenumber"];
                DateTime birthdate = DateTime.ParseExact(System.Configuration.ConfigurationManager.AppSettings["cerner.z_point_aanvraag.epdrequest.birthdate"], "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None);
                string gender = System.Configuration.ConfigurationManager.AppSettings["cerner.z_point_aanvraag.epdrequest.gender"];

                EPDRequest epdRequest = new EPDRequest()
                {
                    PatientNumber = patientnumber,
                    VisitNumber = visitnumber,
                    CivilServiceNumber = civilservicenumber,
                    BirthDate = birthdate,
                    Gender = gender,
                };

                // During development for Cerner they server side certificate issues.
                // This made testing only really possible in ACCEPTATIE and/or PRODUCTIE.
                // This skip flag was added so that in the event of issues; we can quickly
                // disable the certificate checks without having to deploy a new codebase - PP
                bool skipcertificatecheck = string.Equals("true", System.Configuration.ConfigurationManager.AppSettings["cerner.z_point_aanvraag.skipcertificatecheck"], StringComparison.InvariantCultureIgnoreCase);
                if (skipcertificatecheck)
                {
                    ServicePointManager.ServerCertificateValidationCallback =
                    delegate (object sender,
                        System.Security.Cryptography.X509Certificates.X509Certificate certificate,
                        System.Security.Cryptography.X509Certificates.X509Chain chain,
                        System.Net.Security.SslPolicyErrors sslPolicyErrors)
                    { return true; };
                }


                EndpointAddress endpointaddress = new EndpointAddress(endpoint);

                Binding binding = create_binding();

                z_point_aanvrClient aanvrclient = new z_point_aanvrClient(binding, endpointaddress);
                aanvrclient.ClientCredentials.UserName.UserName = username;
                aanvrclient.ClientCredentials.UserName.Password = password;

                Z_POINT_AANVRAAG aanvraag = new Z_POINT_AANVRAAG()
                {
                    I_FALNR = epdRequest.VisitNumber,
                    I_GBDAT = epdRequest.BirthDate.Value.ToString("yyyy-MM-dd"),
                    I_PATNR = epdRequest.PatientNumber
                };

                Z_POINT_AANVRAAGResponse aanvrresponse = null;

                //aanvrclient.Endpoint.Behaviors.Add(new AddUserAgentEndpointBehavior());

                //OperationContext.Current = new OperationContext(aanvrclient.InnerChannel);
                //OperationContext.Current.OutgoingMessageProperties.Add("P3", "Test User");

                //using (new OperationContextScope(aanvrclient.InnerChannel))
                //{
                //    // action="urn:sap-com:document:sap:rfc:functions:Z_POINT_AANVR:Z_POINT_AANVRAAGRequest"

                //    HttpRequestMessageProperty messageheaderproperty = new HttpRequestMessageProperty();
                //    MessageHeader mh = MessageHeader.CreateHeader("action", "urn:sap-com:document:sap:rfc:functions", "Z_POINT_AANVR:Z_POINT_AANVRAAGRequest");
                //    OperationContext.Current.OutgoingMessageHeaders.Add(mh);

                //    // ContentType:application/soap+xml;charset=UTF-8;action="urn:sap-com:document:sap:rfc:functions:Z_POINT_AANVR"
                //    HttpRequestMessageProperty messageproperty = new HttpRequestMessageProperty();
                //    messageproperty.Headers["Content-Type"] = "application/soap+xml;charset=UTF-8;action=\"urn:sap-com:document:sap:rfc:functions:Z_POINT_AANVR\"";
                //    OperationContext.Current.OutgoingMessageProperties[HttpRequestMessageProperty.Name] = messageproperty;

                //    aanvrresponse = aanvrclient.Z_POINT_AANVRAAG(aanvraag);
                //}

                aanvrresponse = aanvrclient.Z_POINT_AANVRAAG(aanvraag);

                Console.WriteLine($"ERROR: {aanvrresponse?.E_ERROR}");
                Console.WriteLine($"E_AANVRAAG.PATNR: {aanvrresponse?.E_AANVRAAG?.PATNR}");
            }
            catch (WebException wex)
            {
                var httpwebresponse = (HttpWebResponse)wex.Response;
                Console.WriteLine("FATEL ERROR");
                Console.WriteLine($"Status: ({(int)httpwebresponse.StatusCode}) {httpwebresponse.StatusCode}");
                Console.WriteLine($"{httpwebresponse.StatusDescription}");

                Console.WriteLine($"Message: {wex.Message}");
                Console.WriteLine($"InnerException.Message: {wex.InnerException?.Message}");
                Console.WriteLine($"StackTrace: {wex.StackTrace}");
            }
            catch (Exception ex)
            {
                Console.WriteLine("FATEL ERROR");
                Console.WriteLine($"Message: {ex.Message}");
                Console.WriteLine($"InnerException.Message: {ex.InnerException?.Message}");
                Console.WriteLine($"StackTrace: {ex.StackTrace}");
            }
        }

        public static void cerner_overdracht()
        {
            Console.WriteLine($"cerner_overdracht");

            try
            {
                string endpoint = System.Configuration.ConfigurationManager.AppSettings["cerner.z_point_overdracht.endpoint"];
                string username = System.Configuration.ConfigurationManager.AppSettings["cerner.z_point_overdracht.username"];
                string password = System.Configuration.ConfigurationManager.AppSettings["cerner.z_point_overdracht.password"];
                string patientnumber = System.Configuration.ConfigurationManager.AppSettings["cerner.z_point_overdracht.epdrequest.patientnumber"];
                string visitnumber = System.Configuration.ConfigurationManager.AppSettings["cerner.z_point_overdracht.epdrequest.visitnumber"];
                string civilservicenumber = System.Configuration.ConfigurationManager.AppSettings["cerner.z_point_overdracht.epdrequest.civilservicenumber"];
                DateTime birthdate = DateTime.ParseExact(System.Configuration.ConfigurationManager.AppSettings["cerner.z_point_overdracht.epdrequest.birthdate"], "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None);
                string gender = System.Configuration.ConfigurationManager.AppSettings["cerner.z_point_overdracht.epdrequest.gender"];

                EPDRequest epdRequest = new EPDRequest()
                {
                    PatientNumber = patientnumber,
                    VisitNumber = visitnumber,
                    CivilServiceNumber = civilservicenumber,
                    BirthDate = birthdate,
                    Gender = gender,
                };

                // During development for Cerner they server side certificate issues.
                // This made testing only really possible in ACCEPTATIE and/or PRODUCTIE.
                // This skip flag was added so that in the event of issues; we can quickly
                // disable the certificate checks without having to deploy a new codebase - PP
                bool skipcertificatecheck = string.Equals("true", System.Configuration.ConfigurationManager.AppSettings["cerner.z_point_overdracht.skipcertificatecheck"], StringComparison.InvariantCultureIgnoreCase);
                if (skipcertificatecheck)
                {
                    ServicePointManager.ServerCertificateValidationCallback =
                    delegate (object sender,
                        System.Security.Cryptography.X509Certificates.X509Certificate certificate,
                        System.Security.Cryptography.X509Certificates.X509Chain chain,
                        System.Net.Security.SslPolicyErrors sslPolicyErrors)
                    { return true; };
                }

                EndpointAddress endpointaddress = new EndpointAddress(endpoint);

                Binding binding = create_binding();

                Z_POINT_OVERDRClient overdrclient = new Z_POINT_OVERDRClient(binding, endpointaddress);
                overdrclient.ClientCredentials.UserName.UserName = username;
                overdrclient.ClientCredentials.UserName.Password = password;

                Z_POINT_OVERDRACHT overdracht = new Z_POINT_OVERDRACHT()
                {
                    I_FALNR = epdRequest.VisitNumber,
                    I_GBDAT = epdRequest.BirthDate.Value.ToString("yyyy-MM-dd"),
                    I_PATNR = epdRequest.PatientNumber
                };
                var overdrresponse = overdrclient.Z_POINT_OVERDRACHT(overdracht);

                Console.WriteLine($"ERROR: {overdrresponse?.E_ERROR}");
            }
            catch (WebException wex)
            {
                var httpwebresponse = (HttpWebResponse)wex.Response;
                Console.WriteLine("FATEL ERROR");
                Console.WriteLine($"Status: ({(int)httpwebresponse.StatusCode}) {httpwebresponse.StatusCode}");
                Console.WriteLine($"{httpwebresponse.StatusDescription}");

                Console.WriteLine($"Message: {wex.Message}");
                Console.WriteLine($"InnerException.Message: {wex.InnerException?.Message}");
                Console.WriteLine($"StackTrace: {wex.StackTrace}");
            }
            catch (Exception ex)
            {
                Console.WriteLine("FATEL ERROR");
                Console.WriteLine($"Message: {ex.Message}");
                Console.WriteLine($"InnerException.Message: {ex.InnerException?.Message}");
                Console.WriteLine($"StackTrace: {ex.StackTrace}");
            }
        }

        public static void flevo_aanvraag()
        {
            Console.WriteLine($"flevo_aanvraag");

            try
            {
                string endpoint = System.Configuration.ConfigurationManager.AppSettings["flevo.z_point_aanvraag.endpoint"];
                string username = System.Configuration.ConfigurationManager.AppSettings["flevo.z_point_aanvraag.username"];
                string password = System.Configuration.ConfigurationManager.AppSettings["z_poflevo.z_point_aanvraagint_aanvraag.password"];
                string patientnumber = System.Configuration.ConfigurationManager.AppSettings["flevo.z_point_aanvraag.epdrequest.patientnumber"];
                string visitnumber = System.Configuration.ConfigurationManager.AppSettings["flevo.z_point_aanvraag.epdrequest.visitnumber"];
                string civilservicenumber = System.Configuration.ConfigurationManager.AppSettings["flevo.z_point_aanvraag.epdrequest.civilservicenumber"];
                DateTime birthdate = DateTime.ParseExact(System.Configuration.ConfigurationManager.AppSettings["flevo.z_point_aanvraag.epdrequest.birthdate"], "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None);
                string gender = System.Configuration.ConfigurationManager.AppSettings["flevo.z_point_aanvraag.epdrequest.gender"];

                EPDRequest epdRequest = new EPDRequest()
                {
                    PatientNumber = patientnumber,
                    VisitNumber = visitnumber,
                    CivilServiceNumber = civilservicenumber,
                    BirthDate = birthdate,
                    Gender = gender,
                };

                // During development for Cerner they server side certificate issues.
                // This made testing only really possible in ACCEPTATIE and/or PRODUCTIE.
                // This skip flag was added so that in the event of issues; we can quickly
                // disable the certificate checks without having to deploy a new codebase - PP
                bool skipcertificatecheck = string.Equals("true", System.Configuration.ConfigurationManager.AppSettings["flevo.z_point_aanvraag.skipcertificatecheck"], StringComparison.InvariantCultureIgnoreCase);
                if (skipcertificatecheck)
                {
                    ServicePointManager.ServerCertificateValidationCallback =
                    delegate (object sender,
                        System.Security.Cryptography.X509Certificates.X509Certificate certificate,
                        System.Security.Cryptography.X509Certificates.X509Chain chain,
                        System.Net.Security.SslPolicyErrors sslPolicyErrors)
                    { return true; };
                }

                EndpointAddress endpointaddress = new EndpointAddress(endpoint);

                Binding binding = create_binding();

                z_point_aanvrClient aanvrclient = new z_point_aanvrClient(binding, endpointaddress);
                aanvrclient.ClientCredentials.UserName.UserName = username;
                aanvrclient.ClientCredentials.UserName.Password = password;

                Z_POINT_AANVRAAG aanvraag = new Z_POINT_AANVRAAG()
                {
                    I_FALNR = epdRequest.VisitNumber,
                    I_GBDAT = epdRequest.BirthDate.Value.ToString("yyyy-MM-dd"),
                    I_PATNR = epdRequest.PatientNumber
                };

                Z_POINT_AANVRAAGResponse aanvrresponse = null;
                aanvrresponse = aanvrclient.Z_POINT_AANVRAAG(aanvraag);

                Console.WriteLine($"ERROR: {aanvrresponse?.E_ERROR}");
                Console.WriteLine($"E_AANVRAAG.PATNR: {aanvrresponse?.E_AANVRAAG?.PATNR}");
            }
            catch (WebException wex)
            {
                var httpwebresponse = (HttpWebResponse)wex.Response;
                Console.WriteLine("FATEL ERROR");
                Console.WriteLine($"Status: ({(int)httpwebresponse.StatusCode}) {httpwebresponse.StatusCode}");
                Console.WriteLine($"{httpwebresponse.StatusDescription}");

                Console.WriteLine($"Message: {wex.Message}");
                Console.WriteLine($"InnerException.Message: {wex.InnerException?.Message}");
                Console.WriteLine($"StackTrace: {wex.StackTrace}");
            }
            catch (Exception ex)
            {
                Console.WriteLine("FATEL ERROR");
                Console.WriteLine($"Message: {ex.Message}");
                Console.WriteLine($"InnerException.Message: {ex.InnerException?.Message}");
                Console.WriteLine($"StackTrace: {ex.StackTrace}");
            }
        }

        public static void flevo_overdracht()
        {
            Console.WriteLine($"flevo_overdracht");

            try
            {
                string endpoint = System.Configuration.ConfigurationManager.AppSettings["flevo.z_point_overdracht.endpoint"];
                string username = System.Configuration.ConfigurationManager.AppSettings["flevo.z_point_overdracht.username"];
                string password = System.Configuration.ConfigurationManager.AppSettings["flevo.z_point_overdracht.password"];
                string patientnumber = System.Configuration.ConfigurationManager.AppSettings["flevo.z_point_overdracht.epdrequest.patientnumber"];
                string visitnumber = System.Configuration.ConfigurationManager.AppSettings["flevo.z_point_overdracht.epdrequest.visitnumber"];
                string civilservicenumber = System.Configuration.ConfigurationManager.AppSettings["flevo.z_point_overdracht.epdrequest.civilservicenumber"];
                DateTime birthdate = DateTime.ParseExact(System.Configuration.ConfigurationManager.AppSettings["flevo.z_point_overdracht.epdrequest.birthdate"], "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None);
                string gender = System.Configuration.ConfigurationManager.AppSettings["flevo.z_point_overdracht.epdrequest.gender"];

                EPDRequest epdRequest = new EPDRequest()
                {
                    PatientNumber = patientnumber,
                    VisitNumber = visitnumber,
                    CivilServiceNumber = civilservicenumber,
                    BirthDate = birthdate,
                    Gender = gender,
                };

                // During development for Cerner they server side certificate issues.
                // This made testing only really possible in ACCEPTATIE and/or PRODUCTIE.
                // This skip flag was added so that in the event of issues; we can quickly
                // disable the certificate checks without having to deploy a new codebase - PP
                bool skipcertificatecheck = string.Equals("true", System.Configuration.ConfigurationManager.AppSettings["flevo.z_point_overdracht.skipcertificatecheck"], StringComparison.InvariantCultureIgnoreCase);
                if (skipcertificatecheck)
                {
                    ServicePointManager.ServerCertificateValidationCallback =
                    delegate (object sender,
                        System.Security.Cryptography.X509Certificates.X509Certificate certificate,
                        System.Security.Cryptography.X509Certificates.X509Chain chain,
                        System.Net.Security.SslPolicyErrors sslPolicyErrors)
                    { return true; };
                }

                EndpointAddress endpointaddress = new EndpointAddress(endpoint);

                Binding binding = create_binding();

                Z_POINT_OVERDRClient overdrclient = new Z_POINT_OVERDRClient(binding, endpointaddress);
                overdrclient.ClientCredentials.UserName.UserName = username;
                overdrclient.ClientCredentials.UserName.Password = password;
                Z_POINT_OVERDRACHT overdracht = new Z_POINT_OVERDRACHT()
                {
                    I_FALNR = epdRequest.VisitNumber,
                    I_GBDAT = epdRequest.BirthDate.Value.ToString("yyyy-MM-dd"),
                    I_PATNR = epdRequest.PatientNumber
                };
                var overdrresponse = overdrclient.Z_POINT_OVERDRACHT(overdracht);

                Console.WriteLine($"ERROR: {overdrresponse?.E_ERROR}");
            }
            catch (WebException wex)
            {
                var httpwebresponse = (HttpWebResponse)wex.Response;
                Console.WriteLine("FATEL ERROR");
                Console.WriteLine($"Status: ({(int)httpwebresponse.StatusCode}) {httpwebresponse.StatusCode}");
                Console.WriteLine($"{httpwebresponse.StatusDescription}");

                Console.WriteLine($"Message: {wex.Message}");
                Console.WriteLine($"InnerException.Message: {wex.InnerException?.Message}");
                Console.WriteLine($"StackTrace: {wex.StackTrace}");
            }
            catch (Exception ex)
            {
                Console.WriteLine("FATEL ERROR");
                Console.WriteLine($"Message: {ex.Message}");
                Console.WriteLine($"InnerException.Message: {ex.InnerException?.Message}");
                Console.WriteLine($"StackTrace: {ex.StackTrace}");
            }
        }
    }

    public class AddUserAgentClientMessageInspector : IClientMessageInspector
    {
        public object BeforeSendRequest(ref System.ServiceModel.Channels.Message request, IClientChannel channel)
        {
            XmlDocument xmldocument = new XmlDocument();

            XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmldocument.NameTable);
            nsmgr.AddNamespace("env", "http://www.w3.org/2003/05/soap-envelope");
            nsmgr.AddNamespace("wsa", "http://www.w3.org/2005/08/addressing");

            MemoryStream memorystream = new MemoryStream();
            XmlWriter xmlwriter = XmlWriter.Create(memorystream);
            request.WriteMessage(xmlwriter);
            xmlwriter.Flush();
            memorystream.Position = 0;
            xmldocument.Load(memorystream);
            memorystream.SetLength(0);

            XmlNode envelope = xmldocument.DocumentElement.SelectSingleNode("/env:Envelope", nsmgr);
            if (envelope != null)
            {
                XmlNode header = envelope.SelectSingleNode("/env:Envelope/env:Header", nsmgr);
                if (header != null)
                    envelope.RemoveChild(header);
            }

            xmlwriter = XmlWriter.Create(memorystream);
            xmldocument.WriteTo(xmlwriter);
            xmlwriter.Flush();
            memorystream.Position = 0;
            XmlReader xmlreader = XmlReader.Create(memorystream);
            request = Message.CreateMessage(xmlreader, int.MaxValue, request.Version);

            HttpRequestMessageProperty prop;
            if (request.Properties.ContainsKey(HttpRequestMessageProperty.Name))
            {
                prop = (HttpRequestMessageProperty)request.Properties[HttpRequestMessageProperty.Name];
            }
            else
            {
                prop = new HttpRequestMessageProperty();
                request.Properties.Add(HttpRequestMessageProperty.Name, prop);
            }

            prop.Headers["Content-Type"] = "application/soap+xml;charset=UTF-8;action=\"urn:sap-com:document:sap:rfc:functions:Z_POINT_AANVR\"";

            return null;
        }

        public void AfterReceiveReply(ref System.ServiceModel.Channels.Message reply, object correlationState)
        {
        }
    }

    // Endpoint behavior used to add the User-Agent HTTP Header to WCF calls for Server
    public class AddUserAgentEndpointBehavior : IEndpointBehavior
    {
        public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {
            clientRuntime.MessageInspectors.Add(new AddUserAgentClientMessageInspector());
        }

        public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
        {
        }

        public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
        {
        }

        public void Validate(ServiceEndpoint endpoint)
        {
        }
    }
}
