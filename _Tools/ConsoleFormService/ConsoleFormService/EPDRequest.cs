﻿using System;

namespace Point.HL7.Processing.Models
{
    public class EPDRequest
    {
        public string PatientNumber { get; set; }
        public string VisitNumber { get; set; }
        public string CivilServiceNumber { get; set; }
        public DateTime? BirthDate { get; set; }
        public string Gender { get; set; }

        public override string ToString()
        {
            string result = "";
            foreach (var prop in this.GetType().GetProperties())
            {
                result += $"{prop.Name}: {prop.GetValue(this, null)}, ";
            }
            result = result.TrimEnd(new[] { ',', ' ' });
            return result;
        }
    }
}
