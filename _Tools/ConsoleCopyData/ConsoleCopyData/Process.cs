﻿using ConsoleCopyData.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;

namespace ConsoleCopyData
{





    public class Process : BaseClass
    {

        List<TableInfo> TableInfos = new List<TableInfo>();
        List<TableConfig> TableConfigs = new List<TableConfig>();

        public void GetConfig()
        {
            foreach(var line in File.ReadAllLines("TableConfig.txt"))
            {
                TableConfigs.Add(new TableConfig().FromString(line));
            }
        }

        public void GetRelations()
        {

            string alltables = $"select object_id, name" +
                               $"  from sys.objects " +
                               $" where type = 'U' and is_ms_shipped = 0" +
                               $" order by name";

            string extrainfo = $"select count(*) as idcolumns" +
                               $"  from syscolumns " +
                               $" where colstat = 1 and id = **table_id**";

            string relations = $"select object_id, object_name(object_id) as objectname, parent_object_id, object_name(parent_object_id) as parentname, referenced_object_id, object_name(referenced_object_id) as referencedname" +
                               $"  from sys.foreign_keys " +
                               $" order by parentname, referencedname";

            string columnnames = $"select fkpc.name as parentcolumn, fkrc.name as childcolumn" +
                                 $"  from sys.foreign_key_columns fkc" +
                                 $" join sys.all_columns fkpc on fkc.parent_object_id = fkpc.object_id and fkc.parent_column_id = fkpc.column_id" +
                                 $" join sys.all_columns fkrc on fkc.referenced_object_id = fkrc.object_id and fkc.referenced_column_id = fkrc.column_id" +
                                 $" where fkc.constraint_object_id = **constraint_object_id**";

            using (SqlConnection sqlconnection = new SqlConnection(DBTarget))
            {
                using (SqlCommand sqlcommand = new SqlCommand(alltables, sqlconnection))
                {
                    sqlconnection.Open();

                    var reader = sqlcommand.ExecuteReader();

                    while (reader.Read())
                    {
                        int id = Int32.Parse(reader["object_id"].ToString());
                        string name = reader["name"].ToString();

                        var idreader = new SqlCommand(extrainfo.Replace("**table_id**", id.ToString()), sqlconnection).ExecuteReader();
                        idreader.Read();
                        var idcolumns = Int32.Parse(idreader["idcolumns"].ToString());
                        idreader.Close();

                        TableInfos.Add(new TableInfo() { ID = id, Name = name, HasIdentity = idcolumns > 0 });
                    }

                    reader.Close();

                    sqlconnection.Close();
                }

                using (SqlCommand sqlcommand = new SqlCommand(relations, sqlconnection))
                {
                    sqlconnection.Open();

                    var reader = sqlcommand.ExecuteReader();

                    while (reader.Read())
                    {
                        int parentid = Int32.Parse(reader["parent_object_id"].ToString());
                        string parentname = reader["parentname"].ToString();
                        int referencedid = Int32.Parse(reader["referenced_object_id"].ToString());
                        string referencename = reader["referencedname"].ToString();
                        int fkid = Int32.Parse(reader["object_id"].ToString());
                        string fkname = reader["objectname"].ToString();

                        var tableinfo = TableInfos.FirstOrDefault(ti => ti.ID == parentid);
                        if (tableinfo == null)
                        {
                            AddToLogError($"Table {parentname} not defined from sys.objects");
                        }
                        else
                        {
                            tableinfo.ChildIDs.Add(referencedid);
                            tableinfo.DropFKs.Add($"ALTER TABLE [dbo].[{parentname}] DROP CONSTRAINT [{fkname}]");

                            var columnreader = new SqlCommand(columnnames.Replace("**constraint_object_id**", fkid.ToString()), sqlconnection).ExecuteReader();
                            columnreader.Read();
                            var parentcolumn = columnreader["parentcolumn"].ToString();
                            var childcolumn = columnreader["childcolumn"].ToString();
                            columnreader.Close();

                            tableinfo.CreateFKs.Add($"ALTER TABLE [dbo].[{parentname}] WITH CHECK ADD CONSTRAINT [{fkname}] FOREIGN KEY([{parentcolumn}]) REFERENCES [dbo].[{referencename}] ([{childcolumn}])\r\n" +
                                                    $"ALTER TABLE [dbo].[{parentname}] CHECK CONSTRAINT[{fkname}]");

                        }

                    }

                    reader.Close();

                    sqlconnection.Close();
                }
            }



        }

        public void ClearTables(TableAction tableAction)
        {
            List<TableInfo> referingtables = new List<TableInfo>();
            List<TableInfo> tocleartables = new List<TableInfo>();

            foreach (var tableconfig in TableConfigs.Where(tc => tc.TableAction == tableAction))
            {
                AddToLog($"-- {tableAction}: {tableconfig.Name} ");
                var tableinfo = TableInfos.FirstOrDefault(ti => ti.Name == tableconfig.Name);
                if (tableinfo == null)
                {
                    AddToLogError($"{tableconfig.Name} in configuration, not found as a real table in DBTarget");
                    continue;
                }
                foreach (var parenttableinfo in TableInfos.Where(ti => ti.ChildIDs.Contains(tableinfo.ID)))
                {
                    AddToLog($"   referenced by: " + parenttableinfo.Name);
                    if (!referingtables.Contains(parenttableinfo))
                    {
                        referingtables.Add(parenttableinfo);
                    }
                }
                tocleartables.Add(tableinfo);
            }

            using (SqlConnection targetconnection = new SqlConnection(DBTarget))
            {
                targetconnection.Open();

                AddToLog("Dropping views with schemabinding");
                new SqlCommand(File.ReadAllText("ViewsDrop.txt"), targetconnection).ExecuteNonQuery(); // Quick solution, not the nicest

                AddToLog("Dropping FK's");
                foreach (var tableinfo in referingtables)
                {
                    tableinfo.DropFKs.ForEach(s => { new SqlCommand(s, targetconnection).ExecuteNonQuery(); } );
                }

                foreach (var tocleartable in tocleartables)
                {
                    AddToLog($"truncating {tocleartable.Name}");
                    new SqlCommand($"truncate table {tocleartable.Name}", targetconnection).ExecuteNonQuery();
                    if (tocleartable.HasIdentity)
                    {
                        new SqlCommand($"dbcc checkident('{tocleartable.Name}', RESEED, 0)", targetconnection).ExecuteNonQuery();
                    }
                }

                AddToLog("Creating FK's");
                foreach (var tableinfo in referingtables)
                {
                    tableinfo.CreateFKs.ForEach(s => { new SqlCommand(s, targetconnection).ExecuteNonQuery(); });
                }

                AddToLog("Creating views with schemabinding");
                new SqlCommand(File.ReadAllText("ViewsCreate.txt"), targetconnection).ExecuteNonQuery(); // Quick solution, not the nicest


                targetconnection.Close();
            }

        }


        public void CopyData()
        {
            var obfuscator = new Obfuscator();

            foreach (var tableconfig in TableConfigs.Where(tc => tc.TableAction == TableAction.REBUILD))
            {
                // disable triggers: seem to be not affected on bulkCopy



                using (SqlConnection sourceconnection = new SqlConnection(DBSource))
                {
                    sourceconnection.Open();

                    SqlCommand commandRowCount = new SqlCommand($"SELECT COUNT(*) FROM [dbo].[{tableconfig.Name}]", sourceconnection);
                    long totalCount = Convert.ToInt32(commandRowCount.ExecuteScalar());
                    AddToLog($"{tableconfig.Name} - row count: {totalCount}");

                    SqlCommand commandSourceData = new SqlCommand($"SELECT * from [dbo].[{tableconfig.Name}]", sourceconnection);
                    WrapperDataReader reader = new WrapperDataReader(commandSourceData.ExecuteReader(), obfuscator, tableconfig); 

                    AddToLogInfo($"{tableconfig.Name} - start copying {totalCount} rows @{DateTime.Now.ToString("dd-MM HH:mm:ss")}");
                    SetTitle($"{tableconfig.Name} - start copying {totalCount} rows @{DateTime.Now.ToString("dd-MM HH:mm:ss")}");

                    using (SqlBulkCopy bulkCopy = new SqlBulkCopy(DBTarget, SqlBulkCopyOptions.KeepIdentity))
                    {
                        try
                        {
                            handleColumnMapping(bulkCopy, tableconfig.Name);

                            bulkCopy.DestinationTableName = $"dbo.[{tableconfig.Name}]";
                            bulkCopy.BatchSize = (totalCount > 100000 ? 1000 : 100);
                            bulkCopy.BulkCopyTimeout = Int32.MaxValue;
                            bulkCopy.NotifyAfter = bulkCopy.BatchSize * 10;
                            bulkCopy.SqlRowsCopied += (s, e) => AddToLogInfo($"   copied {e.RowsCopied}/{totalCount} rows");

                            bulkCopy.WriteToServer(reader);
                        }
                        catch (Exception ex)
                        {
                            AddToLogError(ex.Message);
                        }
                        finally
                        {
                            reader.Close();
                        }
                    }

                    AddToLogInfo($"   ended copying {totalCount} rows @{DateTime.Now.ToString("dd-MM HH:mm:ss")}");

                    sourceconnection.Close();
                }



            }
            SetTitle("Done with CopyData()");
        

        }

        public void FixBSN()
        {
            using (SqlConnection connection = new SqlConnection(DBTarget))
            {
                connection.Open();

                SqlCommand sqlcommand = new SqlCommand($"SELECT ClientID, CivilServiceNumber from Client where isnull(CivilServiceNumber,'') <> '' order by ClientID", connection);

                var reader = sqlcommand.ExecuteReader();
                
                while (reader.Read())
                {
                    string clientid = reader["ClientID"].ToString();
                    string civilservicenumber = reader["CivilServiceNumber"].ToString();
                    string correctcivilservicenumber = correctBSN(civilservicenumber);
                    if (civilservicenumber != correctcivilservicenumber)
                    {
                        var updatecommand = new SqlCommand($"update Client set CivilServiceNumber = '{correctcivilservicenumber}' where ClientID = {clientid}", connection);
                        updatecommand.ExecuteNonQuery();
                        Console.WriteLine($"{clientid}, {civilservicenumber} -> {correctcivilservicenumber}");
                    }

                }


                sqlcommand = new SqlCommand($"SELECT FlowInstanceID, ClientCivilServiceNumber from FlowInstanceSearchValues where isnull(ClientCivilServiceNumber,'') <> '' order by FlowInstanceID", connection);

                reader = sqlcommand.ExecuteReader();

                while (reader.Read())
                {
                    string flowinstanceid = reader["FlowInstanceID"].ToString();
                    string civilservicenumber = reader["ClientCivilServiceNumber"].ToString();
                    string correctcivilservicenumber = correctBSN(civilservicenumber);
                    if (civilservicenumber != correctcivilservicenumber)
                    {
                        var updatecommand = new SqlCommand($"update FlowInstanceSearchValues set ClientCivilServiceNumber = '{correctcivilservicenumber}' where FlowInstanceID = {flowinstanceid}", connection);
                        updatecommand.ExecuteNonQuery();
                        Console.WriteLine($"{flowinstanceid}, {civilservicenumber} -> {correctcivilservicenumber}");
                    }

                }


                connection.Close();
            }
        }

        private string correctBSN(string bsn)
        {
            if (bsn.Length != 9)
            {
                return bsn;
            }

            string resulttmp = bsn;

            int divisor = 1000000000;
            int total = 0;
            //int intresult = Convert.ToInt32(bsn);

            if (!int.TryParse(bsn, out int intresult))
            {
                return bsn;
            }

            for (int i = 9; i > 1; i--)
            {
                total += i * Math.DivRem(intresult, divisor /= 10, out intresult);
            }
            Math.DivRem(total, 11, out int rest);

            if (rest >= 10)
            {
                return bsn;
            }

            return resulttmp.Substring(0, resulttmp.Length - 1) + rest.ToString();
        }

        private void handleColumnMapping(SqlBulkCopy bulkCopy, string tableName)
        {
            List<string> sourceColumns = new List<string>();
            List<string> targetColumns = new List<string>();

            string[] restrictions = new string[4] { null, null, tableName, null };

            using (SqlConnection connection = new SqlConnection(DBSource))
            {
                connection.Open();
                sourceColumns = connection.GetSchema("Columns", restrictions).AsEnumerable().Select(s => s.Field<String>("Column_Name")).ToList();
                connection.Close();
            }

            using (SqlConnection conn = new SqlConnection(DBTarget))
            {
                conn.Open();
                targetColumns = conn.GetSchema("Columns", restrictions).AsEnumerable().Select(s => s.Field<String>("Column_Name")).ToList();
                conn.Close();
            }

            foreach(var columnname in sourceColumns.Except(targetColumns, StringComparer.InvariantCultureIgnoreCase))
            {
                AddToLogInfo($"   [{columnname}] exists in DBSource but not in DBTarget");
            }

            foreach (var columnname in targetColumns.Except(sourceColumns, StringComparer.InvariantCultureIgnoreCase))
            {
                AddToLogInfo($"   [{columnname}] exists in DBTarget but not in DBSource");
            }

            foreach (var columnname in sourceColumns.Where(sc => targetColumns.Contains(sc, StringComparer.InvariantCultureIgnoreCase)))
            {
                bulkCopy.ColumnMappings.Add(columnname, targetColumns.FirstOrDefault(tc => tc.Equals(columnname, StringComparison.InvariantCultureIgnoreCase)));
            }


        }

        public void ShowRelation()
        {
            foreach (var tableinfo in TableInfos.OrderBy(ti => ti.Name))
            {
                ShowRelation(tableinfo, new List<int>() { tableinfo.ID });
            }
        }

        private void ShowRelation(TableInfo tableInfo, List<int> parents)
        {
            AddToLog(new string('-', (parents.Count-1) * 3) + tableInfo.Name);

            foreach(var childid in tableInfo.ChildIDs)
            {
                if (tableInfo.ID == childid)
                {
                    AddToLog(new string('-', (parents.Count-1) * 3) + " (internal reference) " + tableInfo.Name);
                    continue;
                }
                if (parents.Contains(childid))
                {
                    continue;
                }
                parents.Add(childid);
                var tableinfo = TableInfos.FirstOrDefault(ti => ti.ID == childid);
                ShowRelation(tableinfo, parents);
            }
        }

    }
}
