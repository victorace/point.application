﻿using System;
using System.Configuration;
using System.IO;

namespace ConsoleCopyData
{
    public class BaseClass
    {
        protected StreamWriter writer = new StreamWriter($"log_{DateTime.Now.ToString("yyyyMMdd-HHmmss")}.txt") { AutoFlush = true };

        public void AddToLog()
        {
            addToLog("");
        }

        public void SetTitle(string message)
        {
            Console.Title = message;
        }

        public void AddToLog(string message)
        {
            Console.ForegroundColor = ConsoleColor.Gray;
            addToLog(message);
        }

        public void AddToLogInfo(string message)
        {
            Console.ForegroundColor = ConsoleColor.White;
            addToLog(message);
        }

        public void AddToLogSuccess(string message)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            addToLog(message);
        }

        public void AddToLogError(string message)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            addToLog(message);
        }

        private void addToLog(string message)
        {
            Console.WriteLine(message);
            writer.WriteLine($"{DateTime.Now.ToString("HH:mm:ss")} - {message}");
        }

        public string DBSource
        {
            get
            {
                string dbsource = ConfigurationManager.ConnectionStrings["DBSource"].ConnectionString;
                if (String.IsNullOrEmpty(dbsource))
                {
                    throw new Exception("Connectionstring 'DBSource' not set");
                }
                return dbsource;
            }
        }

        public string DBTarget
        {
            get
            {
                string dbsource = ConfigurationManager.ConnectionStrings["DBTarget"].ConnectionString;
                if (String.IsNullOrEmpty(dbsource))
                {
                    throw new Exception("Connectionstring 'DBTarget' not set");
                }
                return dbsource;
            }
        }

    }
}
