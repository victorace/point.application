﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleCopyData.Models
{
    public enum TableAction
    {
        CLEAR,
        KEEP,
        REBUILD
    }

    public class ColumnConfig
    {
        public string Name { get; set; }

        public int[] IDs { get; set; }
        public string Replacer { get; set; }

        public bool DoNull()
        {
            return Replacer.Equals("NULL", StringComparison.InvariantCultureIgnoreCase);
        }

        public bool DoObfuscate()
        {
            return Replacer.Equals("OBFUSCATE", StringComparison.InvariantCultureIgnoreCase);
        }

        public bool DoTruncate()
        {
            return Replacer.Equals("TRUNCATE", StringComparison.InvariantCultureIgnoreCase);
        }
        public bool IsBSN()
        {
            return Name.IndexOf("CivilServiceNumber", StringComparison.InvariantCultureIgnoreCase) > 0 || Name.EndsWith("BSN", StringComparison.InvariantCultureIgnoreCase);
        }

    }

    public class TableConfig
    {
        public string Name { get; set; }

        public TableAction TableAction { get; set; }
        public List<ColumnConfig> ColumnConfigs { get; set; } = new List<ColumnConfig>();

        public TableConfig FromString(string line) 
        {
            string[] splitted = line.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            Name = splitted[0];
            TableAction = (TableAction)Enum.Parse(typeof(TableAction), splitted[1]);
            if (splitted.Length >= 3 && !String.IsNullOrEmpty(splitted[2]))
            {
                foreach(string columnsplitted in splitted[2].Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    var configsplitted = columnsplitted.Split(new[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
                    if (splitted[0] == "FlowFieldValue")
                    {
                        ColumnConfigs.Add(new ColumnConfig() { Name = "FlowFieldID", IDs = configsplitted[0].Split('/').Select(Int32.Parse).ToArray(), Replacer = configsplitted[1] });
                    }
                    else
                    {
                        ColumnConfigs.Add(new ColumnConfig() { Name = configsplitted[0], Replacer = configsplitted[1] });
                    }
                }
            }
            return this;
        }
    }

    public class TableInfo
    {
        public int ID { get; set; }
        public string Name { get; set; }

        public bool HasIdentity { get; set; }

        public List<int> ChildIDs { get; set; } = new List<int>();

        public List<string> DropFKs { get; set; } = new List<string>();

        public List<string> CreateFKs { get; set; } = new List<string>();
    }
}
