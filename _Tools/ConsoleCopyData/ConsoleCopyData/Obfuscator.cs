﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleCopyData
{
    public class Obfuscator
    {

        char[] randomLetters;
        char[] randomDigits;
        Random random = new Random(DateTime.Now.Second);

        public Obfuscator()
        {

            randomLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray().OrderBy(s => (random.Next())).ToArray();
            randomDigits = "1234567890".ToCharArray().OrderBy(s => (random.Next())).ToArray();
        }

        private Dictionary<string, string> obfuscated = new Dictionary<string, string>();
        private Dictionary<DateTime, DateTime> obfuscateddates = new Dictionary<DateTime, DateTime>();
        public string Obfuscate(string value)
        {
            if (obfuscated.ContainsKey(value))
            {
                return obfuscated[value];
            }

            string result = "";
            foreach (var character in value.ToCharArray())
            {
                int ascii = (int)character;
                if (ascii >= 65 && ascii <= 90)
                {
                    result += randomLetters[random.Next(65,90) - 65];
                }
                else if (ascii >= 97 && ascii <= 122)
                {
                    result += randomLetters[random.Next(97,122) - 97].ToString().ToLower();
                }
                else if (ascii >= 48 && ascii <= 57)
                {
                    result += randomDigits[random.Next(48,57) - 48];
                }
                else
                {
                    result += character;
                }
            }

            obfuscated.Add(value, result);
            return result;
        }

        public string ObfuscateBSN(string value)
        {
            if (value == null)
            {
                return null;
            }

            string resulttmp = value;

            if (resulttmp.Length != 9)
            {
                return resulttmp;
            }

            int divisor = 1000000000;
            int total = 0;
            int intresult = Convert.ToInt32(value);
            for (int i = 9; i > 1; i--)
            {
                total += i * Math.DivRem(intresult, divisor /= 10, out intresult);
            }
            Math.DivRem(total, 11, out int rest);

            resulttmp = resulttmp.Substring(0, resulttmp.Length - 1) + rest.ToString();

            return resulttmp;
        }

        public DateTime Obfuscate(DateTime value)
        {
            if (obfuscateddates.ContainsKey(value.Date))
            {
                return obfuscateddates[value.Date];
            }

            int days = random.Next(0, 500) * (random.Next() % 2 == 0 ? -1 : 1);

            var result = value.AddDays(days);

            if (result.Year <= 1900)
            {
                result = new DateTime(1900 + random.Next(5,85), value.Month, value.Day, value.Hour, value.Minute, value.Second);
            }
            if (result.Year > DateTime.Now.Year)
            {
                result = new DateTime(DateTime.Now.Year - random.Next(1, 15), value.Month, value.Day, value.Hour, value.Minute, value.Second);
            }

            obfuscateddates.Add(value.Date, result);
            return result;
        }

        public string Truncate(string value)
        {
            if (String.IsNullOrEmpty(value))
            {
                return value;
            }
            if ( value.Length <= 15 )
            {
                return value;
            }
            else
            {
                return value.Substring(0, 15) + "...";
            }
        }

    }
}
