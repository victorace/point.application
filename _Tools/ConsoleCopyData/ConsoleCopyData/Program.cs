﻿using System;

namespace ConsoleCopyData
{
    class Program 
    {
        static void Main(string[] args)
        {


            var process = new Process();

            try
            {

                process.AddToLogSuccess($"Started program @{DateTime.Now.ToString("dd-MM HH:mm:ss")}");
                process.GetConfig();

                //Complete copy
                //process.GetRelations();
                //process.AddToLogSuccess($"Clearing 'unnecessary' tables @{DateTime.Now.ToString("dd-MM HH:mm:ss")}");
                //process.ClearTables(Models.TableAction.CLEAR);
                //process.AddToLogSuccess($"Clearing 'rebuild' tables @{DateTime.Now.ToString("dd-MM HH:mm:ss")}");
                //process.ClearTables(Models.TableAction.REBUILD);
                //process.AddToLogSuccess($"Started CopyData() @{DateTime.Now.ToString("dd-MM HH:mm:ss")}");
                //process.CopyData();

                //Copy data of one or more tables only:
                process.AddToLogSuccess($"Started CopyData() @{DateTime.Now.ToString("dd-MM HH:mm:ss")}");
                process.CopyData();

                //Fix the BSN's:
                //process.FixBSN();

                process.AddToLogSuccess($"Done? @{DateTime.Now.ToString("dd-MM HH:mm:ss")}");

            }
            catch(Exception ex)
            {
                process.AddToLogError(ex.Message);
                process.AddToLogError(ex.StackTrace);
                process.AddToLogInfo("Press a key to close");
            }

            Console.ReadKey();
        }
    }
}
