﻿using ConsoleCopyData.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace ConsoleCopyData
{

    public class WrapperDataReader : IDataReader
    {
        protected IDataReader reader;
        private TableConfig tableConfig;
        private Obfuscator obfuscator;

        private bool Initialized = false;

        private class WrapperInfo
        {
            public int Ordinal { get; set; }
            public string ColumnName { get; set; }
            public ColumnConfig ColumnConfig { get; set; }
            public List<ColumnConfig> ColumnConfigs { get; set; } = new List<ColumnConfig>();
        }

        private List<WrapperInfo> columnInfos = new List<WrapperInfo>();

        private void DoInitialize()
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {

                if (tableConfig.Name == "FlowFieldValue")
                {
                    var wrapperinfo = new WrapperInfo() { Ordinal = i, ColumnName = reader.GetName(i) };
                    columnInfos.Add(wrapperinfo);

                    foreach (var columnconfig in tableConfig.ColumnConfigs.Where(cc => cc.Name.Equals(reader.GetName(i), StringComparison.InvariantCultureIgnoreCase)))
                    {
                        wrapperinfo.ColumnConfigs.Add(columnconfig);
                    }
                }
                else
                {
                    var columnconfig = tableConfig.ColumnConfigs.FirstOrDefault(cc => cc.Name.Equals(reader.GetName(i), StringComparison.InvariantCultureIgnoreCase));
                    columnInfos.Add(new WrapperInfo() { Ordinal = i, ColumnName = reader.GetName(i), ColumnConfig = columnconfig });
                }
            }
            Initialized = true;
        }

        public WrapperDataReader(IDataReader reader, Obfuscator obfuscator, TableConfig tableConfig)
        {
            this.reader = reader;
            this.tableConfig = tableConfig;
            this.obfuscator = obfuscator;
        }

        public object this[int i] => reader[i];

        public object this[string name] => reader[name];

        public int Depth => reader.Depth;

        public bool IsClosed => reader.IsClosed;

        public int RecordsAffected => reader.RecordsAffected;

        public int FieldCount => reader.FieldCount;

        public void Close()
        {
            reader.Close();
        }

        public void Dispose()
        {
            reader.Dispose();
        }

        public bool GetBoolean(int i)
        {
            return reader.GetBoolean(i);
        }

        public byte GetByte(int i)
        {
            return reader.GetByte(i);
        }

        public long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
        {
            return reader.GetBytes(i, fieldOffset, buffer, bufferoffset, length);
        }

        public char GetChar(int i)
        {
            return reader.GetChar(i);
        }

        public long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length)
        {
            return reader.GetChars(i, fieldoffset, buffer, bufferoffset, length);
        }

        public IDataReader GetData(int i)
        {
            return reader.GetData(i);
        }

        public string GetDataTypeName(int i)
        {
            return reader.GetDataTypeName(i);
        }

        public DateTime GetDateTime(int i)
        {
            return reader.GetDateTime(i);
        }

        public decimal GetDecimal(int i)
        {
            return reader.GetDecimal(i);
        }

        public double GetDouble(int i)
        {
            return reader.GetDouble(i);
        }

        public Type GetFieldType(int i)
        {
            return reader.GetFieldType(i);
        }

        public float GetFloat(int i)
        {
            return reader.GetFloat(i);
        }

        public Guid GetGuid(int i)
        {
            return reader.GetGuid(i);
        }

        public short GetInt16(int i)
        {
            return reader.GetInt16(i);
        }

        public int GetInt32(int i)
        {
            return reader.GetInt32(i);
        }

        public long GetInt64(int i)
        {
            return reader.GetInt64(i);
        }

        public string GetName(int i)
        {
            return reader.GetName(i);
        }

        public int GetOrdinal(string name)
        {
            return reader.GetOrdinal(name);
        }

        public DataTable GetSchemaTable()
        {
            return reader.GetSchemaTable();
        }

        public string GetString(int i)
        {
            return reader.GetString(i);
        }

        public object GetValue(int i)
        {
            if (reader.IsDBNull(i))
            {
                return DBNull.Value;
            }

            if (tableConfig.Name == "FlowFieldValue")
            {
                if (reader.GetOrdinal("Value") == i)
                {
                    int ordinal = reader.GetOrdinal("FlowFieldID");
                    var columnconfig = columnInfos.FirstOrDefault(ci => ci.Ordinal == ordinal).ColumnConfigs.FirstOrDefault(cc => cc.IDs.Contains(reader.GetInt32(ordinal)));
                    return GetValue(columnconfig, i);
                }
            }
            else if (columnInfos[i].ColumnConfig != null)
            {
                return GetValue(columnInfos[i].ColumnConfig, i);
            }

            return reader.GetValue(i);
        }

        private object GetValue(ColumnConfig columnconfig, int i)
        {
            if (columnconfig == null)
            {
                return reader.GetValue(i);
            }
            else
            {
                if (columnconfig.DoNull())
                {
                    return DBNull.Value;
                }
                else if (columnconfig.DoTruncate())
                {
                    return obfuscator.Truncate(reader.GetString(i));
                }
                else if (columnconfig.DoObfuscate())
                {
                    if (reader.GetDataTypeName(i).Equals("DateTime", StringComparison.InvariantCultureIgnoreCase))
                    {
                        return obfuscator.Obfuscate(reader.GetDateTime(i));
                    }
                    else
                    {
                        return obfuscator.Obfuscate(reader.GetString(i));
                    }
                }
                else
                {
                    return reader.GetValue(i);
                }
            }
        }
        
        public int GetValues(object[] values)
        {
            return reader.GetValues(values);
        }

        public bool IsDBNull(int i)
        {
            return reader.IsDBNull(i);
        }

        public bool NextResult()
        {
            return reader.NextResult();
        }

        public bool Read()
        {
            if (!Initialized)
            {
                DoInitialize();
            }

            return reader.Read();
        }
    }
}
