﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ServiceBusConsole
{
    public class ServiceBus
    {

        public List<string> MessageList = new List<string>();

        private void addMessage(string message)
        {
            MessageList.Add(String.Format("{0:dd-MM HH:mm:ss} - {1}", DateTime.Now, message));
            Console.WriteLine(message);
        }

        public void Check()
        {
            try
            {
                foreach (var configName in new[] { "ServiceBusConnectionString", "ServiceBusSecondaryConnectionString" })
                {
                    var connectionstring = ConfigurationManager.ConnectionStrings[configName];
                    if (connectionstring != null)
                    {
                        doCheck(connectionstring);
                    }
                }
                doExpiredCheck();
            }
            catch
            {
                /* ignore */
            }

        }

        private void doCheck(ConnectionStringSettings connectionString)
        {
            char[] splitter = { ',' };
            var namespacemanager = Microsoft.ServiceBus.NamespaceManager.CreateFromConnectionString(connectionString.ConnectionString);

            string[] queues = (ConfigurationManager.AppSettings["Queues"]??"").Split(splitter);
            string[] tresholds = (ConfigurationManager.AppSettings["Tresholds"]??"").Split(splitter);

            if (queues.Length == 0 || tresholds.Length == 0)
            {
                addMessage("Queues or Tresholds not specified in config!");
                return;
            }

            if (queues.Length != tresholds.Length)
            {
                addMessage("Queues and Tresholds do not match count in config!");
                return;
            }

            Console.WriteLine(String.Format("Checking connection '{0}'", connectionString.Name));

            for (int i = 0; i < queues.Length; i++)
            {
                var messagecountdetails = namespacemanager.GetQueue(queues[i]).MessageCountDetails;
                long? count = messagecountdetails?.ActiveMessageCount;

                if (count > Convert.ToInt32(tresholds[i]))
                {
                    addMessage(String.Format("Connection '{0}' - Queue '{1}' is currently holding {2} messages. Treshold {3}", connectionString.Name, queues[i], count, tresholds[i])); 
                }
                Console.WriteLine(String.Format("Queue '{0}' has {1} messages", queues[i], count));
            }

            Console.WriteLine("");
        }

        private void doExpiredCheck()
        {
            var servicebusexpiredcounturl = ConfigurationManager.AppSettings["ServiceBusExpiredCountUrl"];
            var servicebusexpiredtreshold = ConfigurationManager.AppSettings["ServiceBusExpiredTreshold"];
            if ( servicebusexpiredcounturl == null || servicebusexpiredtreshold == null)
            {
                addMessage("ServiceBusExpiredCountUrl or ServiceBusExpiredTreshold not specified in config!");
                return;
            }

            using (var webclient = new WebClient())
            {
                var result = webclient.DownloadString(servicebusexpiredcounturl);
                var match = new Regex(@"""ExpiredItems""\s?:\s?(\d*)").Match(result);
                if (match.Success)
                {
                    int expiredcount = 0;
                    if (!Int32.TryParse(match.Groups[1].Value, out expiredcount))
                    {
                        addMessage(String.Format("Could not parse expiredcount for: [{0}]", match.Groups[1].Value));
                    }
                    else
                    {
                        if (expiredcount > Int32.Parse(servicebusexpiredtreshold))
                        {
                            addMessage(String.Format("ServiceBusExpired is currently holding {0} messages. Treshold {1}", expiredcount, servicebusexpiredtreshold));
                        }
                        else
                        {
                            Console.WriteLine(String.Format("ServiceBusExpired is currently holding {0} messages", expiredcount));
                        }
                    }

                }
                else
                {
                    addMessage(String.Format("Could not get a regex-match on result: [{0}]", result));
                }
            }

        }
    }
}
