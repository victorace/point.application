﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace ServiceBusConsole
{
    public class Mail
    {
        public string Body { get; set; }

        public void SendMail()
        {
            var smtpclient = new SmtpClient();
            var mailmessage = new MailMessage();
            var mailto = ConfigurationManager.AppSettings["MailTo"].ToString();

            foreach (var mailaddress in mailto.Split(';'))
            {
                mailmessage.To.Add(mailaddress);
            }

            mailmessage.Subject = "Error Servicebus";

            mailmessage.Body = getBody();
            mailmessage.IsBodyHtml = true;

            smtpclient.Send(mailmessage);
        }


        private string getBody()
        {
            return @"<html>
                 <style> body {
	                font-family: Calibri,Candara,Segoe,Segoe UI,Optima,Arial,sans-serif;
	                font-size: 14px;
	                font-style: normal;
                    }
                 </style>
                 <body>"
                 + Body + @"
                 </body>
                 </html>";
        }
    }
}
