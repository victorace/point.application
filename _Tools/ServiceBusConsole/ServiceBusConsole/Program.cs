﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceBusConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            doCheck();


            if (bool.Parse(ConfigurationManager.AppSettings["CloseWhenDone"]) == false)
            {
                Console.WriteLine("");
                Console.WriteLine("Done. Press a key.");
                Console.ReadKey();
            }
        }

        private static void doCheck()
        {
            var servicebus = new ServiceBus();
            servicebus.Check();

            if (servicebus.MessageList.Count > 0)
            {
                Mail mail = new Mail();
                mail.Body = String.Join(Environment.NewLine, servicebus.MessageList).Replace(Environment.NewLine, "<br />");
                mail.SendMail();
            }

        }
    }
}
