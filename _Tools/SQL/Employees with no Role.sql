


select aspnet_Membership.IsLockedOut, employee.InActive, aspnet_Membership.LastLoginDate, aspnet_Users.LoweredUserName, aspnet_Roles.RoleId, autocreateset.AutoCreateCode, organization.name as o, organization.OrganizationTypeID, location.name as l, department.name as d
from aspnet_Users
    inner join aspnet_membership on aspnet_Membership.UserId = aspnet_Users.UserId
    inner join employee on employee.UserId = aspnet_Users.UserId
    inner join department on department.departmentID = employee.DepartmentID
    inner join location on location.locationid = department.LocationID
    inner join organization on organization.organizationid = location.OrganizationID
    left outer join AutoCreateSet on AutoCreateSet.LocationID = location.LocationID
    left outer join aspnet_UsersInRoles on aspnet_UsersInRoles.UserId = aspnet_Users.UserId
    left outer join aspnet_Roles on aspnet_Roles.RoleId = aspnet_UsersInRoles.RoleId
where aspnet_Roles.RoleId is null
order by aspnet_Membership.LastLoginDate