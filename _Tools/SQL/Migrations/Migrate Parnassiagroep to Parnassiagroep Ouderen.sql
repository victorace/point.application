
-- Alle locaties onder organisatie Parnassiagroep (ID 174) moeten worden verplaatst naar organisatie Parnassiagroep Ouderen (ID 210)

print 'Migrating locations from Parnassiagroep to Parnassiagroep Ouderen.';

declare @parnassiagroep as int;
declare @parnassiagroepouderen as int;

declare @tablename as varchar(50);
declare @command as varchar(255);

if ( exists (select * from sys.sysobjects where xtype = 'U' and name = 'Location' ) )
    begin
		print 'Backup Location.';
        set @tablename = concat('Location_Parnassiagroep_', format(getdate(), 'yyyyMMddHHmmss', 'nl-NL'));
        if not (exists(select * from sys.sysobjects where xtype = 'U' and name = @tablename))
            begin
                set @command = 'SELECT * INTO ' + @tablename + ' FROM dbo.Location;';
                exec(@command);
            end;
    end;

 if (exists (select * from sys.sysobjects where xtype = 'U' and name = 'PhaseInstance'))
    begin
		print 'Backup PhaseInstance.';
        set @tablename = concat('PhaseInstance_Parnassiagroep_', format(getdate(), 'yyyyMMddHHmmss', 'nl-NL'));
        if not (exists(select * from sys.sysobjects where xtype = 'U' and name = @tablename))
            begin
                set @command = 'SELECT * INTO ' + @tablename + ' FROM dbo.PhaseInstance;';
                exec(@command);
            end;
    end;


set @parnassiagroep = (select OrganizationID from dbo.Organization where Name = 'Parnassiagroep');
set @parnassiagroepouderen = (select OrganizationID from dbo.Organization where Name = 'Parnassiagroep Ouderen');

if @parnassiagroep is not null and @parnassiagroepouderen is not null
begin
	
	if exists (select 1 from dbo.Location where OrganizationID = @parnassiagroep)
	begin

		update dbo.Location set OrganizationID = @parnassiagroepouderen
		where OrganizationID = @parnassiagroep;

		print 'TransferIDs that need to be updated with admin/Recalculate:'
		select transfer.transferid from phaseinstance inner join flowinstance on flowinstance.flowinstanceid = phaseinstance.flowinstanceid 
			inner join transfer on transfer.transferid = flowinstance.transferid
				where phaseinstance.ReceivingOrganizationID = @parnassiagroep

		update dbo.PhaseInstance set ReceivingOrganizationID = @parnassiagroepouderen
		where ReceivingOrganizationID = @parnassiagroep;

		print 'Done.';
	end;
	else
	begin
		print 'No locations connected to Parnassiagroep.';
	end;

end;
else
begin
	print 'Parnassiagroep and/or Parnassiagroep Ouderen not found. Script will now exit without making changes.';
end;