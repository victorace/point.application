
-- Alle locaties onder organisatie Zorggroep Tellens (ID 3338) moeten worden verplaatst naar organisatie Patyna (ID 10797)

print 'Migrating locations from Zorggroep Tellens to Patyna.';

declare @zorggroeptellens as int;
declare @patyna as int;

declare @tablename as varchar(50);
declare @command as varchar(255);

if ( exists (select * from sys.sysobjects where xtype = 'U' and name = 'Location' ) )
    begin
		print 'Backup Location.';
        set @tablename = concat('Location_ZorggroepTellens_', format(getdate(), 'yyyyMMddHHmmss', 'nl-NL'));
        if not (exists(select * from sys.sysobjects where xtype = 'U' and name = @tablename))
            begin
                set @command = 'SELECT * INTO ' + @tablename + ' FROM dbo.Location;';
                exec(@command);
            end;
    end;

 if (exists (select * from sys.sysobjects where xtype = 'U' and name = 'PhaseInstance'))
    begin
		print 'Backup PhaseInstance.';
        set @tablename = concat('PhaseInstance_ZorggroepTellens_', format(getdate(), 'yyyyMMddHHmmss', 'nl-NL'));
        if not (exists(select * from sys.sysobjects where xtype = 'U' and name = @tablename))
            begin
                set @command = 'SELECT * INTO ' + @tablename + ' FROM dbo.PhaseInstance;';
                exec(@command);
            end;
    end;


set @zorggroeptellens = (select OrganizationID from dbo.Organization where Name = 'Zorggroep Tellens');
set @patyna = (select OrganizationID from dbo.Organization where Name = 'Patyna');

if @zorggroeptellens is not null and @patyna is not null
begin
	
	if exists (select 1 from dbo.Location where OrganizationID = @zorggroeptellens)
	begin

		update dbo.Location set OrganizationID = @patyna
		where OrganizationID = @zorggroeptellens;

		print 'TransferIDs that need to be updated with admin/Recalculate:'
		select transfer.transferid from phaseinstance inner join flowinstance on flowinstance.flowinstanceid = phaseinstance.flowinstanceid 
			inner join transfer on transfer.transferid = flowinstance.transferid
				where phaseinstance.ReceivingOrganizationID = @zorggroeptellens

		update dbo.PhaseInstance set ReceivingOrganizationID = @patyna
		where ReceivingOrganizationID = @zorggroeptellens;

		print 'Done.';
	end;
	else
	begin
		print 'No locations connected to Zorggroep Tellens.';
	end;

end;
else
begin
	print 'Zorggroep Tellens and/or Patyna not found. Script will now exit without making changes.';
end;