
--  Samenvoegen Zorgcombinatie Noorderboog (3718) en De Stouwe (9218)

-- Dus locaties onder VVT organisatie De stouwe verplaatsen naar VVT organisatie Zorgcombinatie Noorderboog

print 'Migrating locations from De stouwe to Zorgcombinatie Noorderboog.';

declare @destouwe as int;
declare @zorgcombinatienoorderboog as int;

declare @tablename as varchar(50);
declare @command as varchar(255);

if ( exists (select * from sys.sysobjects where xtype = 'U' and name = 'Location' ) )
    begin
		print 'Backup Location.';
        set @tablename = concat('Location', format(getdate(), 'yyyyMMddHHmmss', 'nl-NL'));
        if not (exists(select * from sys.sysobjects where xtype = 'U' and name = @tablename))
            begin
                set @command = 'SELECT * INTO ' + @tablename + ' FROM dbo.Location;';
                exec(@command);
            end;
    end;

 if (exists (select * from sys.sysobjects where xtype = 'U' and name = 'PhaseInstance'))
    begin
		print 'Backup PhaseInstance.';
        set @tablename = concat('PhaseInstance', format(getdate(), 'yyyyMMddHHmmss', 'nl-NL'));
        if not (exists(select * from sys.sysobjects where xtype = 'U' and name = @tablename))
            begin
                set @command = 'SELECT * INTO ' + @tablename + ' FROM dbo.PhaseInstance;';
                exec(@command);
            end;
    end;


set @destouwe = (select OrganizationID from dbo.Organization where Name = 'De stouwe');
set @zorgcombinatienoorderboog = (select OrganizationID from dbo.Organization where Name = 'Zorgcombinatie Noorderboog');

if @destouwe is not null and @zorgcombinatienoorderboog is not null
begin
	
	if exists (select 1 from dbo.Location where OrganizationID = @destouwe)
	begin

		update dbo.Location set OrganizationID = @zorgcombinatienoorderboog
		where OrganizationID = @destouwe;

		print 'TransferIDs that need to be updated with admin/Recalculate:'
		select transfer.transferid from phaseinstance inner join flowinstance on flowinstance.flowinstanceid = phaseinstance.flowinstanceid 
			inner join transfer on transfer.transferid = flowinstance.transferid
				where phaseinstance.ReceivingOrganizationID = @destouwe

		update dbo.PhaseInstance set ReceivingOrganizationID = @zorgcombinatienoorderboog
		where ReceivingOrganizationID = @destouwe;

		print 'Done.';
	end;
	else
	begin
		print 'No locations connected to De stouwe.';
	end;

end;
else
begin
	print 'De stouwe and/or Zorgcombinatie Noorderboog not found. Script will now exit without making changes.';
end;