
print 'Migrating a single location from Waadwente to ZuidOost Zorg.'

declare @location_waadwente as int;
declare @organization_waadwente as int;
declare @zuidoostzorg as int;

-- this
set @location_waadwente = (select locationid from dbo.Location where name = 'Waadwenthe' and inactive = 0)
-- from
set @organization_waadwente = (select organizationid from dbo.Organization where Name = 'Waadwente' and organizationtypeid = 2 and inactive = 0);
-- to
set @zuidoostzorg = (select organizationid from dbo.Organization where Name = 'ZuidOost Zorg'  and organizationtypeid = 2 and inactive = 0);

if @location_waadwente is not null and @organization_waadwente is not null and @zuidoostzorg is not null
begin
	
	update location set organizationid = @zuidoostzorg where locationid = @location_waadwente

	print 'Updating PhaseInstance records:'
	select phaseinstance.phaseinstanceid from phaseinstance where phaseinstance.ReceivingDepartmentID in 
			(select department.departmentid from department inner join location on location.locationid = department.locationid and location.locationid = @location_waadwente)

	print 'TransferIDs that need to be updated with admin/Recalculate:'
	select transfer.transferid from phaseinstance inner join flowinstance on flowinstance.flowinstanceid = phaseinstance.flowinstanceid 
		inner join transfer on transfer.transferid = flowinstance.transferid
		where phaseinstance.ReceivingDepartmentID in 
			(select department.departmentid from department inner join location on location.locationid = department.locationid and location.locationid = @location_waadwente)			

	update phaseinstance set receivingorganizationid = @zuidoostzorg
	where phaseinstance.receivingdepartmentid in 
		(select department.departmentid from department inner join location on location.locationid = department.locationid and location.locationid = @location_waadwente)

	print 'Done.'

end;
else
begin
	print 'Waadwente and/or ZuidOost Zorg not found. Script will now exit without making changes.';
end;