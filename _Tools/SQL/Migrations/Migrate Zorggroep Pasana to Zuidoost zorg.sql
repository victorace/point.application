
-- Alle locaties onder organisatie Zorggroep Pasana (ID 3726) moeten worden verplaatst naar organisatie Zuidoost zorg (ID 3361).

print 'Migrating locations from Zorggroep Pasana to Zuidoost zorg.';

declare @zorggroeppasana as int;
declare @zuidoostzorg as int;

declare @tablename as varchar(50);
declare @command as varchar(255);

if ( exists (select * from sys.sysobjects where xtype = 'U' and name = 'Location' ) )
    begin
		print 'Backup Location.';
        set @tablename = concat('Location_ZorggroepPasana_', format(getdate(), 'yyyyMMddHHmmss', 'nl-NL'));
        if not (exists(select * from sys.sysobjects where xtype = 'U' and name = @tablename))
            begin
                set @command = 'SELECT * INTO ' + @tablename + ' FROM dbo.Location;';
                exec(@command);
            end;
    end;

 if (exists (select * from sys.sysobjects where xtype = 'U' and name = 'PhaseInstance'))
    begin
		print 'Backup PhaseInstance.';
        set @tablename = concat('PhaseInstance_ZorggroepPasana_', format(getdate(), 'yyyyMMddHHmmss', 'nl-NL'));
        if not (exists(select * from sys.sysobjects where xtype = 'U' and name = @tablename))
            begin
                set @command = 'SELECT * INTO ' + @tablename + ' FROM dbo.PhaseInstance;';
                exec(@command);
            end;
    end;


set @zorggroeppasana = (select OrganizationID from dbo.Organization where Name = 'Zorggroep Tellens');
set @zuidoostzorg = (select OrganizationID from dbo.Organization where Name = 'Patyna');

if @zorggroeppasana is not null and @zuidoostzorg is not null
begin
	
	if exists (select 1 from dbo.Location where OrganizationID = @zorggroeppasana)
	begin

		update dbo.Location set OrganizationID = @zuidoostzorg
		where OrganizationID = @zorggroeppasana;

		print 'TransferIDs that need to be updated with admin/Recalculate:'
		select transfer.transferid from phaseinstance inner join flowinstance on flowinstance.flowinstanceid = phaseinstance.flowinstanceid 
			inner join transfer on transfer.transferid = flowinstance.transferid
				where phaseinstance.ReceivingOrganizationID = @zorggroeppasana

		update dbo.PhaseInstance set ReceivingOrganizationID = @zuidoostzorg
		where ReceivingOrganizationID = @zorggroeppasana;

		print 'Done.';
	end;
	else
	begin
		print 'No locations connected to Zorggroep Pasana.';
	end;

end;
else
begin
	print 'Zorggroep Pasana and/or Zuidoost zorg not found. Script will now exit without making changes.';
end;