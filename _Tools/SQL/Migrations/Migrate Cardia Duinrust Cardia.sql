
print 'Migrating locations from Cardia to Duinrust [Cardia].'

declare @cardia as int;
declare @duinrustcardia as int;

set @cardia = (select OrganizationID from dbo.Organization where Name = 'Cardia');
set @duinrustcardia = (select OrganizationID from dbo.Organization where Name = 'Duinrust [Cardia]');

if @cardia is not null and @duinrustcardia is not null
begin
	
	if exists (select 1 from location where organizationid = @duinrustcardia)
	begin
		update location set organizationid = @cardia
		where organizationid = @duinrustcardia

		update PhaseInstance set ReceivingOrganizationID = @cardia
		where ReceivingOrganizationID = @duinrustcardia

		print 'Done.'
	end
	else
	begin
		print 'No locations connected to Duinrust [Cardia].'
	end

end;
else
begin
	print 'Cardia and/or Duinrust [Cardia] not found. Script will now exit without making changes.';
end;