
-- Alle locaties onder organisatie IJsselheem Thuiszorg (10813) moeten worden verplaatst naar organisatie IJsselheem Intramuraal (3463)

print 'Migrating locations from IJsselheem Thuiszorg to IJsselheem Intramuraal.';

declare @ijsselheemthuiszorg as int;
declare @ijsselheemintramuraal as int;

declare @tablename as varchar(50);
declare @command as varchar(255);

if ( exists (select * from sys.sysobjects where xtype = 'U' and name = 'Location' ) )
    begin
		print 'Backup Location.';
        set @tablename = concat('Location_IJsselheemThuiszorg_', format(getdate(), 'yyyyMMddHHmmss', 'nl-NL'));
        if not (exists(select * from sys.sysobjects where xtype = 'U' and name = @tablename))
            begin
                set @command = 'SELECT * INTO ' + @tablename + ' FROM dbo.Location;';
                exec(@command);
            end;
    end;

 if (exists (select * from sys.sysobjects where xtype = 'U' and name = 'PhaseInstance'))
    begin
		print 'Backup PhaseInstance.';
        set @tablename = concat('PhaseInstance_IJsselheemThuiszorg_', format(getdate(), 'yyyyMMddHHmmss', 'nl-NL'));
        if not (exists(select * from sys.sysobjects where xtype = 'U' and name = @tablename))
            begin
                set @command = 'SELECT * INTO ' + @tablename + ' FROM dbo.PhaseInstance;';
                exec(@command);
            end;
    end;


set @ijsselheemthuiszorg = (select OrganizationID from dbo.Organization where Name = 'IJsselheem Thuiszorg');
set @ijsselheemintramuraal = (select OrganizationID from dbo.Organization where Name = 'IJsselheem Intramuraal');

if @ijsselheemthuiszorg is not null and @ijsselheemintramuraal is not null
begin
	
	if exists (select 1 from dbo.Location where OrganizationID = @ijsselheemthuiszorg)
	begin

		update dbo.Location set OrganizationID = @ijsselheemintramuraal
		where OrganizationID = @ijsselheemthuiszorg;

		print 'TransferIDs that need to be updated with admin/Recalculate:'
		select transfer.transferid from phaseinstance inner join flowinstance on flowinstance.flowinstanceid = phaseinstance.flowinstanceid 
			inner join transfer on transfer.transferid = flowinstance.transferid
				where phaseinstance.ReceivingOrganizationID = @ijsselheemthuiszorg

		update dbo.PhaseInstance set ReceivingOrganizationID = @ijsselheemintramuraal
		where ReceivingOrganizationID = @ijsselheemthuiszorg;

		print 'Done.';
	end;
	else
	begin
		print 'No locations connected to IJsselheem Thuiszorg.';
	end;

end;
else
begin
	print 'IJsselheem Thuiszorg and/or IJsselheem Intramuraal not found. Script will now exit without making changes.';
end;