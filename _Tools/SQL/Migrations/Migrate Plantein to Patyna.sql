
-- Alle locaties onder organisatie Plantein (ID 3378) moeten worden verplaatst naar organisatie Patyna (ID 10797)

print 'Migrating locations from Plantein to Patyna.';

declare @plantein as int;
declare @patyna as int;

declare @tablename as varchar(50);
declare @command as varchar(255);

if ( exists (select * from sys.sysobjects where xtype = 'U' and name = 'Location' ) )
    begin
		print 'Backup Location.';
        set @tablename = concat('Location_Plantein_', format(getdate(), 'yyyyMMddHHmmss', 'nl-NL'));
        if not (exists(select * from sys.sysobjects where xtype = 'U' and name = @tablename))
            begin
                set @command = 'SELECT * INTO ' + @tablename + ' FROM dbo.Location;';
                exec(@command);
            end;
    end;

 if (exists (select * from sys.sysobjects where xtype = 'U' and name = 'PhaseInstance'))
    begin
		print 'Backup PhaseInstance.';
        set @tablename = concat('PhaseInstance_Plantein_', format(getdate(), 'yyyyMMddHHmmss', 'nl-NL'));
        if not (exists(select * from sys.sysobjects where xtype = 'U' and name = @tablename))
            begin
                set @command = 'SELECT * INTO ' + @tablename + ' FROM dbo.PhaseInstance;';
                exec(@command);
            end;
    end;


set @plantein = (select OrganizationID from dbo.Organization where Name = 'Plantein');
set @patyna = (select OrganizationID from dbo.Organization where Name = 'Patyna');

if @plantein is not null and @patyna is not null
begin
	
	if exists (select 1 from dbo.Location where OrganizationID = @plantein)
	begin

		update dbo.Location set OrganizationID = @patyna
		where OrganizationID = @plantein;

		print 'TransferIDs that need to be updated with admin/Recalculate:'
		select transfer.transferid from phaseinstance inner join flowinstance on flowinstance.flowinstanceid = phaseinstance.flowinstanceid 
			inner join transfer on transfer.transferid = flowinstance.transferid
				where phaseinstance.ReceivingOrganizationID = @plantein

		update dbo.PhaseInstance set ReceivingOrganizationID = @patyna
		where ReceivingOrganizationID = @plantein;

		print 'Done.';
	end;
	else
	begin
		print 'No locations connected to Plantein.';
	end;

end;
else
begin
	print 'Plantein and/or Patyna not found. Script will now exit without making changes.';
end;