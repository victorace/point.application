
print 'Migrating a single location from Noorderbreedte to Zorgcentrum Het Bildt, De Beuckelaer.'

declare @nijbethanietzummarum as int;
declare @noorderbreedte as int;
declare @zorgcentrumhetbildt as int;

-- this
set @nijbethanietzummarum = (select locationid from dbo.Location where name = 'Nij Bethanie Tzummarum' and city = 'Tzummarum' and inactive = 0)
-- from
set @noorderbreedte = (select organizationid from dbo.Organization where Name = 'Noorderbreedte' and organizationtypeid = 2 and inactive = 0);
-- to
set @zorgcentrumhetbildt = (select organizationid from dbo.Organization where Name = 'Zorgcentrum Het Bildt'  and organizationtypeid = 2 and inactive = 0);

if @nijbethanietzummarum is not null and @noorderbreedte is not null and @zorgcentrumhetbildt is not null
begin
	
	update location set organizationid = @zorgcentrumhetbildt where locationid = @nijbethanietzummarum

	print 'Updating PhaseInstance records:'
	select phaseinstance.phaseinstanceid from phaseinstance where phaseinstance.ReceivingDepartmentID in 
			(select department.departmentid from department inner join location on location.locationid = department.locationid and location.locationid = @nijbethanietzummarum)

	print 'TransferIDs that need to be updated with admin/Recalculate:'
	select transfer.transferid from phaseinstance inner join flowinstance on flowinstance.flowinstanceid = phaseinstance.flowinstanceid 
		inner join transfer on transfer.transferid = flowinstance.transferid
		where phaseinstance.ReceivingDepartmentID in 
			(select department.departmentid from department inner join location on location.locationid = department.locationid and location.locationid = @nijbethanietzummarum)			

	update phaseinstance set receivingorganizationid = @zorgcentrumhetbildt
	where phaseinstance.receivingdepartmentid in 
		(select department.departmentid from department inner join location on location.locationid = department.locationid and location.locationid = @nijbethanietzummarum)

	print 'Done.'

end;
else
begin
	print 'Noorderbreedte and/or Zorgcentrum Het Bildt, De Beuckelaer not found. Script will now exit without making changes.';
end;