DECLARE @organizationid AS INT
DECLARE @flowfieldattributeid AS INT = 2317 -- InfectieIsolatie
IF (EXISTS (SELECT 1 FROM organization WHERE name = 'Haaglanden Medisch Centrum'))
BEGIN

	SET @organizationid = (SELECT TOP 1 OrganizationID FROM Organization WHERE name = 'Haaglanden Medisch Centrum')

	INSERT INTO dbo.FlowFieldAttributeException (FlowFieldAttributeID, RegionID, OrganizationID, ReadOnly, Visible, Required, SignalOnChange, PhaseDefinitionID, RequiredByFlowFieldID, RequiredByFlowFieldValue)
	  VALUES (@flowfieldattributeid, NULL, @organizationid, 0, 1, 1, 0, NULL, NULL, NULL )

END