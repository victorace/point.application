

--select *  from organization where name like '%meander%'

declare    @OrganizationID varchar(max) = '5610'
declare    @LocationID varchar(max) = '-1' 
declare    @DepartmentID varchar(max) = '-1' 
declare    @OrganizationIDHealthCareProvider int = -1 
declare    @OrganizationHealthcareID int = -1 
declare    @DateFrom datetime = '2017-01-01' 
declare    @DateTo datetime = '2017-09-30'

        set nocount on;

		declare @delimiter char(1) = ',';

		declare @myorganizationid table ( OrganizationID int );  
        insert  into @myorganizationid
                select  [Value]
                from    dbo.ArrayTableInt(@OrganizationID, @delimiter);  

		declare @mylocationid table ( LocationID int );  
        insert  into @mylocationid
                select  [Value]
                from    dbo.ArrayTableInt(@LocationID, @delimiter);  

        declare @mydepartmentid table ( DepartmentID int );  
        insert  into @mydepartmentid
                select  [Value]
                from    dbo.ArrayTableInt(@DepartmentID, @delimiter);

        declare @Diagnose_I60 varchar(255) = 'I60 (Subarachno�dale bloeding)';
        declare @Diagnose_I61 varchar(255) = 'I61 (Intracerebrale bloeding)';
        declare @Diagnose_I63 varchar(255) = 'I63 (Cerebraal infarct)';
        declare @Diagnose_1112 varchar(255) = '1112 (TIA)';
        declare @Diagnose_I60_I61 varchar(255) = 'I60 (Subarachno�dale bloeding);I61 (Intracerebrale bloeding)';
        declare @Diagnose_I60_I61_1112 varchar(255) = 'I60 (Subarachno�dale bloeding);I61 (Intracerebrale bloeding);1112 (TIA)';
        declare @NullDate datetime = '9/19/1809';
		
        declare @FlowFieldID int;
        select  @FlowFieldID = FlowFieldID
        from    dbo.FlowField
        where   Name = 'PatientWordtOpgenomenDatum';


        declare @transfer table
            (
              TransferID int ,
              PatientWordOpgenomenDatum datetime ,
              FlowInstanceID int
            );

        insert  into @transfer
                select  tr.TransferID, ( select top 1 Value from dbo.FlowFieldValue with ( nolock ) where FlowInstanceID = FlowInstance.FlowInstanceID and FlowFieldID = @FlowFieldID), FlowInstanceID
                from    dbo.Transfer as tr
                        join dbo.FlowInstance on tr.TransferID = FlowInstance.TransferID
                                             and FlowDefinitionID = 4 -- CVA flow
                                             and Deleted = 0
						join dbo.Organization with (nolock) on Organization.OrganizationID = StartedByOrganizationID
						join dbo.Department with (nolock) on Department.DepartmentID = StartedByDepartmentID
						join dbo.Location with (nolock) on Location.LocationID = Department.LocationID
                where   ( ( @OrganizationID = '-1' ) or ( Organization.OrganizationID in ( select OrganizationID from @myorganizationid) ))
				and ( ( @LocationID = '-1' ) or ( Location.LocationID in ( select LocationID from @mylocationid) ) )
                and ( ( @DepartmentID = '-1' ) or ( Department.DepartmentID in ( select DepartmentID from @mydepartmentid) ) );


    -- Remove unwanted
        delete from @transfer where PatientWordOpgenomenDatum is null or PatientWordOpgenomenDatum = '' or PatientWordOpgenomenDatum not between @DateFrom and @DateTo; 

        declare @tromtype table
            (
              TransferID int ,
              TromIV varchar(1) ,
              TromIA varchar(1) ,
              MedDiagType varchar(1) ,
              Overleden varchar(1)
            );

        insert  into @tromtype
                select  Tr.TransferID ,
                        isnull(dbo.flowFieldValue_bit_version(Tr.FlowInstanceID, 'InZHIntraveneusGetrombolyseerd'), 0) ,
                        isnull(dbo.flowFieldValue_bit_version(Tr.FlowInstanceID, 'InZHIntraArterieelBehandeld'), 0) ,
                        isnull(dbo.flowFieldValue_char_version(Tr.FlowInstanceID, 'Diagnose'), '') ,
                        case when isnull(dbo.flowFieldValue_char_version(Tr.FlowInstanceID, 'BeloopOpname'), 0) = 2
                             then 1
                             else 0
                        end
                from    @transfer Tr
                where   Tr.FlowInstanceID > 0;
 

        select  case when Tr.FlowInstanceID > 0 then 1
                     else 0
                end as IsFlow ,
                Transfer.TransferID as TransferID ,
                Region.Name as Regio ,
                Organization.Name as Organisatie ,
                Location.Name as Locatie ,
                Department.Name as Afdeling ,
                Organization.Name as organisation_uri ,
                '51' as ketennaam ,
				case when Tr.FlowInstanceID > 0 then isnull(dbo.flowFieldValue_char_version(Tr.FlowInstanceID, 'DICALand'), '')
                     else ''
                end as land,
                isnull(( select top 1
                                oop.Code
                         from   dbo.OrganizationOutsidePoint oop
                         where  oop.OrganizationID = Organization.OrganizationID
                       ), '-1') as diagnoseid ,
                isnull(CivilServiceNumber, '') as idcode ,
                left(Initials, 5) as voorl ,
                left(MiddleName, 10) as tussen ,
                left(LastName, 40) as naam ,
                '' as tussenp ,
                '' as naamp ,
                Transfer.TransferID as upn ,
                BirthDate as gebdat ,
                case Gender
                  when 'm' then '1'
                  when 'v' then '2'
                  else ''
                end as geslacht ,
                isnull(Client.PostalCode, '') as pcode ,
                Trom.MedDiagType as diagnosetype ,
                case Trom.MedDiagType
                  when '1' then 'CVA bloedig'
                  when '2' then 'CVA onbloedig'
                  when '3' then 'TIA'
                  else ''
                end as diagnose_naam ,
                isnull(dbo.flowFieldValue_date_version(Tr.FlowInstanceID, 'DiagnoseEersteKlachtDatum'), @NullDate) as eersteklacht ,
                isnull(dbo.split_string(dbo.flowFieldValue_char_version(Tr.FlowInstanceID, 'DiagnoseEersteKlachtTijd'), 1, ':'), '99') as eersteklachtuur ,
                isnull(dbo.split_string(dbo.flowFieldValue_char_version(Tr.FlowInstanceID, 'DiagnoseEersteKlachtTijd'), 2, ':'), '99') as eersteklachtminuut ,
                dbo.flowFieldValue_char_version(Tr.FlowInstanceID, 'Behandelwijze') as behandelwijze ,
                isnull(dbo.flowFieldValue_date_version(Tr.FlowInstanceID, 'DiagnoseAankomstSEHDatum'), @NullDate) as presseh ,
                isnull(dbo.split_string(dbo.flowFieldValue_char_version(Tr.FlowInstanceID, 'DiagnoseAankomstSEHTijd'), 1, ':'), '99') as pressehuur ,
                isnull(dbo.split_string(dbo.flowFieldValue_char_version(Tr.FlowInstanceID, 'DiagnoseAankomstSEHTijd'), 2, ':'), '99') as pressehminuut ,
                Trom.TromIV as tromiv ,
                case when Trom.TromIV = '0'
                     then case when ( dbo.flowFieldValue_bit_version(Tr.FlowInstanceID, 'RedenTrombolyseNietUitgevoerdGeringeUitval') ) = 1 then '1'
                               else '0'
                          end
                     else ''
                end as redengeentromgering ,
                case when Trom.TromIV = '0'
                     then case when ( dbo.flowFieldValue_bit_version(Tr.FlowInstanceID, 'RedenTrombolyseNietUitgevoerdVerbetering') ) = 1 then '1'
                               else '0'
                          end
                     else ''
                end as redengeentromverbeter ,
                case when Trom.TromIV = '0'
                     then case when ( dbo.flowFieldValue_bit_version(Tr.FlowInstanceID, 'RedenTrombolyseNietUitgevoerdTeHogeBloeddruk') ) = 1 then '1'
                               else '0'
                          end
                     else ''
                end as redengeentrombloedhoog ,
                case when Trom.TromIV = '0'
                     then case when ( dbo.flowFieldValue_bit_version(Tr.FlowInstanceID, 'RedenTrombolyseNietUitgevoerdAntistolling') ) = 1 then '1'
                               else '0'
                          end
                     else ''
                end as redengeentromantistolling ,
                case when Trom.TromIV = '0'
                     then case when ( dbo.flowFieldValue_bit_version(Tr.FlowInstanceID, 'RedenTrombolyseNietUitgevoerdTeLaatGekomen') ) = 1 then '1'
                               else '0'
                          end
                     else ''
                end as redengeentromtelaat ,
                case when Trom.TromIV = '0'
                     then case when ( dbo.flowFieldValue_bit_version(Tr.FlowInstanceID, 'RedenTrombolyseNietUitgevoerdTijdstipCVAOnbekend') ) = 1 then '1'
                               else '0'
                          end
                     else ''
                end as redengeentromonbekendtijdcva ,
                case when Trom.TromIV = '0'
                     then case when ( dbo.flowFieldValue_bit_version(Tr.FlowInstanceID, 'RedenTrombolyseNietUitgevoerdAnders') ) = 1 then '1'
                               else '0'
                          end
                     else ''
                end as redengeentromanders ,
                dbo.flowFieldValue_char_version(Tr.FlowInstanceID, 'RedenTrombolyseNietUitgevoerdNamelijk') as redengeentromcomment,
                case when Trom.TromIV = 1
                     then convert(varchar, isnull(dbo.flowFieldValue_date_version(Tr.FlowInstanceID, 'TrombolyseIntraveneusDatum'), @NullDate), 105)
                     else ''
                end as tromven ,
                case when Trom.TromIV = 1
                     then isnull(dbo.split_string(dbo.flowFieldValue_char_version(Tr.FlowInstanceID, 'TrombolyseIntraveneusTijd'), 1, ':'), '99')
                     else ''
                end as tromvenuur ,
                case when Trom.TromIV = 1
                     then isnull(dbo.split_string(dbo.flowFieldValue_char_version(Tr.FlowInstanceID, 'TrombolyseIntraveneusTijd'), 2, ':'), '99')
                     else ''
                end as tromvenminuut ,
                Trom.TromIA as tromia ,
                case when Trom.TromIA = 1
                     then convert(varchar, isnull(dbo.flowFieldValue_date_version(Tr.FlowInstanceID, 'TrombolyseArterieelDatum'), @NullDate), 105)
                     else ''
                end as tromart ,
                case when Trom.TromIA = 1
                     then isnull(dbo.split_string(dbo.flowFieldValue_char_version(Tr.FlowInstanceID, 'TrombolyseArterieelTijd'), 1, ':'), '99')
                     else ''
                end as tromartuur ,
                case when Trom.TromIA = 1
                     then isnull(dbo.split_string(dbo.flowFieldValue_char_version(Tr.FlowInstanceID, 'TrombolyseArterieelTijd'), 2, ':'), '99')
                     else ''
                end as tromartminuut ,
                dbo.flowFieldValue_char_version(Tr.FlowInstanceID, 'VertragingIntraveneuzeTrombolyse') as directtromb ,
                dbo.flowFieldValue_char_version(Tr.FlowInstanceID, 'RedenTrombolyseVerlaatUitgevoerd') as redenlatetrom ,
                case dbo.flowFieldValue_char_version(Tr.FlowInstanceID, 'BarthelIndexOpDagVierOfEerder')
                    when '-1' then '99'
                    else dbo.flowFieldValue_char_version(Tr.FlowInstanceID, 'BarthelIndexOpDagVierOfEerder')
                end as bardag4opn ,
                dbo.flowFieldValue_char_version(Tr.FlowInstanceID, 'SlikscreeningUitgevoerdDirectNaOpname') as sliktest ,
                dbo.flowFieldValue_char_version(Tr.FlowInstanceID, 'IO2SaturatieGedurendeSlikscreening') as sliktesto2 ,
                case when Trom.Overleden = '1' then '2'
                     when Trom.Overleden = '0' then '1'
                     else ''
                end as ontslagenopn ,
                dbo.flowFieldValue_date_version(Tr.FlowInstanceID, 'PatientOntslagenDatum') as datbeloop ,
                case when Trom.MedDiagType = 3 then 
					isnull(dbo.flowFieldValue_date_version(Tr.FlowInstanceID, 'StartTIADiagnostiekDatum'), @NullDate)
                end as tiadat ,
                dbo.flowFieldValue_date_version(Tr.FlowInstanceID, 'StartTIADiagnostiekDatum') as tiadagstart ,
                null --  11-03-16: No values of this field on production.
				as duplex ,
                null --  11-03-16: No values of this field on production.
				as duplextijd ,

                case when Trom.Overleden = '0'
                     then 
						case isnull(dbo.flowFieldValue_char_version(Tr.FlowInstanceID, 'Ontslagbestemming'), -1)
                            when '-1' then ''
                            else case when dbo.flowFieldValue_char_version(Tr.FlowInstanceID, 'Ontslagbestemming') = '5'
                                    then '0' --Ander Ziekenhuis
                                    when dbo.flowFieldValue_char_version(Tr.FlowInstanceID, 'Ontslagbestemming') = '1' --Huis
                                            then case isnull(dbo.flowFieldValue_char_version(Tr.FlowInstanceID, 'OntslagbestemmingThuis'), -1)
                                                when '-1' then ''
                                                else case
                                                    when dbo.flowFieldValue_char_version(Tr.FlowInstanceID, 'OntslagbestemmingThuis') = '1'
                                                    then '1' --Thuis zonder zorg
                                                    when dbo.flowFieldValue_char_version(Tr.FlowInstanceID, 'OntslagbestemmingThuis') = '2'
                                                    then '2' --Thuis met zorg
                                                    when dbo.flowFieldValue_char_version(Tr.FlowInstanceID, 'OntslagbestemmingThuis') = '3'
                                                    then '6' --Thuis met behandeling
                                                    when dbo.flowFieldValue_char_version(Tr.FlowInstanceID, 'OntslagbestemmingThuis') = '4'
                                                    then '8' --Thuis met zorg �n behandeling
                                                    end
                                                end
                                    when dbo.flowFieldValue_char_version(Tr.FlowInstanceID, 'Ontslagbestemming') = '2'
                                    then '3' --Revalidatiecentrum
                                    when dbo.flowFieldValue_char_version(Tr.FlowInstanceID, 'Ontslagbestemming') = '3'
                                    then '4' --GRZ CVA verpleeghuis
                                    when dbo.flowFieldValue_char_version(Tr.FlowInstanceID, 'Ontslagbestemming') = '4'
                                    then '5' --Verpleeghuis
                                    when dbo.flowFieldValue_char_version(Tr.FlowInstanceID, 'Ontslagbestemming') = '6' --Buitenland
                                            then case isnull(dbo.flowFieldValue_char_version(Tr.FlowInstanceID, 'OntslagbestemmingBuitenland'), -1)
                                                when '-1' then ''
                                                else case
                                                    when dbo.flowFieldValue_char_version(Tr.FlowInstanceID, 'OntslagbestemmingBuitenland') = '1'
                                                    then '9'  --Buitenlands ziekenhuis
                                                    when dbo.flowFieldValue_char_version(Tr.FlowInstanceID, 'OntslagbestemmingBuitenland') = '2'
                                                    then '10' --Buitenlands Verpleeghuis
                                                    end
                                                end
                                    when dbo.flowFieldValue_char_version(Tr.FlowInstanceID, 'Ontslagbestemming') = '7'
                                    then '7' --Overig
                                end
                          end
                end as bestemmingvv1 ,
                case when Trom.Overleden = '0' then 
					case when dbo.flowFieldValue_char_version(Tr.FlowInstanceID, 'PatientOntslagenVVTNaar') = '7'
						then substring(dbo.flowFieldValue_char_version(Tr.FlowInstanceID, 'PatientOntslagenVVTNaarAnders'), 1, 40)
					end
                end as bestemmingandersvv1 ,
                dbo.flowFieldValue_char_version(Tr.FlowInstanceID, 'ZorginstellingPatientNaarVerwezen') as bestemmingzkhsvv1,
                dbo.flowFieldValue_date_version(Tr.FlowInstanceID, 'FollowUpDatum') as datumfu ,
                case when isnull(dbo.flowFieldValue_char_version(Tr.FlowInstanceID, 'ModifiedRankingScale'), -1) > -1
                    then dbo.flowFieldValue_char_version(Tr.FlowInstanceID, 'ModifiedRankingScale')
                    else ''
                end as mrs3maandfu ,
                case isnull(dbo.flowFieldValue_char_version(Tr.FlowInstanceID, 'PatientOntslagenVVTNaar'), -1)
                    when '-1' then ''
                    when '5' then '0' --Ander Ziekenhuis
                    when '1' then '1' --Thuis zonder zorg
                    when '2' then '2' --Thuis met zorg
                    when '3' then '6' --Thuis met behandeling
                    when '4' then '8' --Thuis met zorg �n behandeling
                    when '2' then '3' --Revalidatiecentrum
                    when '3' then '4' --GRZ CVA verpleeghuis
                    when '4' then '5' --Verpleeghuis
                    when '1' then '9' --Buitenlands ziekenhuis
                    when '2' then '9' --Buitenlands Verpleeghuis
                    when '7' then '7' --Overig
                end as bestemmingvvfu ,
                case when dbo.flowFieldValue_char_version(Tr.FlowInstanceID, 'VerblijfplaatsNa3mnd') = '0'
                        and dbo.flowFieldValue_char_version(Tr.TransferID, 'VerblijfplaatsNa3mndNaamZH') > 0
                    then ( select top 1 Code from dbo.OrganizationOutsidePoint where Code = dbo.flowFieldValue_char_version(Tr.FlowInstanceID, 'VerblijfplaatsNa3mndNaamZH') )
                end as bestemmingzkhsvvfu ,
                '' as bestemminglokatievvfu ,
                substring(dbo.flowFieldValue_char_version(Tr.FlowInstanceID, 'VerblijfplaatsNa3mndAnders'), 1, 40) as bestemmingandersvvfu ,
                '' as ontslagpcvvfu ,
                dbo.flowFieldValue_date_version(Tr.FlowInstanceID, 'DatumVanOverlijdenNa3mnd') as datoverlijdenfu ,
                dbo.flowFieldValue_date_version(Tr.FlowInstanceID, 'DatumOpnameRevalidatieCentrum') as nazorgdatopnamevv ,
                case when dbo.flowFieldValue_char_version(Tr.FlowInstanceID, 'BarthelIndexBijOpnameVVT') = '-1' -- <<< Juiste veld ?? Er zijn meerder Barthels
                        then '99'
                    else isnull(dbo.flowFieldValue_char_version(Tr.FlowInstanceID, 'BarthelIndexBijOpnameVVT'), '')
                end as nazorgbaropnamevv ,
                case when dbo.flowFieldValue_char_version(Tr.FlowInstanceID, 'BarthelIndexBijOntslagVVT') = '-1' -- <<< Juiste veld ?? Er zijn meerder Barthels
                        then '99'
                    else isnull(dbo.flowFieldValue_char_version(Tr.FlowInstanceID, 'BarthelIndexBijOntslagVVT'), '')
                end as nazorgbarontslagvv ,
                isnull(dbo.flowFieldValue_char_version(Tr.FlowInstanceID, 'BeloopOpnameVVT'), '') as nazorgontslagenvv ,
                dbo.flowFieldValue_char_version(Tr.FlowInstanceID, 'PatientOntslagenOverledenDatumVVT') as nazorgdatontslagvv 

        from    @transfer as Tr
                join dbo.Transfer with ( nolock ) on Tr.TransferID = Transfer.TransferID
		        join dbo.FlowInstance with ( nolock ) on Tr.FlowInstanceID = FlowInstance.FlowInstanceID
                join dbo.Department with ( nolock ) on FlowInstance.StartedByDepartmentID = Department.DepartmentID
                join dbo.Location with ( nolock ) on Department.LocationID = Location.LocationID
                join dbo.Organization with ( nolock ) on Location.OrganizationID = Organization.OrganizationID
                join dbo.Region with ( nolock ) on Organization.RegionID = Region.RegionID
                join dbo.Client with ( nolock ) on Transfer.ClientID = Client.ClientID
                join @tromtype as Trom on Trom.TransferID = Tr.TransferID
        order by TransferID desc;

