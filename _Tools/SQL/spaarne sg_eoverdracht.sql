select transferattachment.timestamp, 
--transferattachment.filename as tafilename, 
--transferattachment.name as taname, 
--transferattachment.contenttype as tacontenttype,
genericfile.contenttype as gfcontenttype,
genericfile.filename as gffilename 
from TransferAttachment with (nolock)
inner join transfer with (nolock) on transfer.transferid = transferattachment.transferid
inner join flowinstance with (nolock) on transfer.transferid = flowinstance.transferid and flowinstance.startedbyorganizationid = 8500
inner join genericfile with (nolock) on transferattachment.genericfileid = genericfile.genericfileid
where genericfile.filename like 'SG_%'
order by transferattachment.timestamp desc