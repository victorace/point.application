/*
Run this script on:

pointreview.verzorgdeoverdracht.nl.TXX-DB-POINT-R    -  This database will be modified

to synchronize it with:

..2018T3

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 12.0.33.3389 from Red Gate Software Ltd at 10/2/2018 10:45:09

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
-- Pointer used for text / image updates. This might not be needed, but is declared here just in case
DECLARE @pv binary(16)

PRINT(N'Drop constraints from [dbo].[ActionCodeHealthInsurerList]')
ALTER TABLE [dbo].[ActionCodeHealthInsurerList] NOCHECK CONSTRAINT [FK_ActionCodeHealthInsurerList_ActionCodeHealthInsurer]
ALTER TABLE [dbo].[ActionCodeHealthInsurerList] NOCHECK CONSTRAINT [FK_ActionCodeHealthInsurerList_ActionCodeHealthInsurerRegio]

PRINT(N'Drop constraints from [dbo].[ActionCodeHealthInsurerRegio]')
ALTER TABLE [dbo].[ActionCodeHealthInsurerRegio] NOCHECK CONSTRAINT [FK_ActionCodeHealthInsurerRegio_Region]

PRINT(N'Delete rows from [dbo].[ActionCodeHealthInsurerList]')
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9277
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9278
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9279
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9280
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9281
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9282
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9283
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9284
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9285
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9286
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9287
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9288
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9289
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9290
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9291
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9292
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9293
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9294
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9295
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9296
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9297
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9298
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9299
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9300
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9301
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9302
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9303
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9304
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9305
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9306
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9307
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9308
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9309
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9310
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9311
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9312
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9313
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9314
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9315
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9316
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9317
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9318
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9319
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9320
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9321
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9322
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9323
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9324
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9325
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9326
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9327
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9328
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9329
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9330
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9331
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9332
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9333
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9334
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9335
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9336
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9337
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9338
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9339
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9340
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9341
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9342
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9343
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9344
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9345
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9346
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9347
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9348
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9349
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9350
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9351
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9352
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9353
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9354
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9355
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9356
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9357
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9358
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9359
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9360
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9361
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9362
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9363
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9364
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9365
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9366
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9367
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9368
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9369
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9370
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9371
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9372
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9373
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9374
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9375
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9376
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9377
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9378
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9379
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9380
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9381
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9382
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9383
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9384
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9385
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9386
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9387
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9388
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9389
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9390
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9391
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9392
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9393
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9394
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9395
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9396
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9397
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9398
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9399
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9400
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9401
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9402
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9403
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9404
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9405
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9406
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9407
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9408
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9409
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9410
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9411
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9412
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9413
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9414
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9415
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9416
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9417
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9418
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9419
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9420
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9421
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9422
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9423
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9424
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9425
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9426
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9427
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9428
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9429
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9430
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9431
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9432
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9433
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9434
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9435
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9436
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9437
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9438
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9439
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9440
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9441
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9442
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9443
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9444
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9445
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9446
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9447
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9448
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9449
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9450
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9451
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9452
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9453
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9454
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9455
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9456
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9457
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9458
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9459
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9460
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9461
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9462
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9463
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9464
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9465
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9466
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9467
DELETE FROM [dbo].[ActionCodeHealthInsurerList] WHERE [ActionCodeHealthInsurerListID] = 9468
PRINT(N'Operation applied to 192 rows out of 192')

PRINT(N'Delete rows from [dbo].[ActionCodeHealthInsurerRegio]')
DELETE FROM [dbo].[ActionCodeHealthInsurerRegio] WHERE [ActionCodeHealthInsurerRegionID] = 113
DELETE FROM [dbo].[ActionCodeHealthInsurerRegio] WHERE [ActionCodeHealthInsurerRegionID] = 114
DELETE FROM [dbo].[ActionCodeHealthInsurerRegio] WHERE [ActionCodeHealthInsurerRegionID] = 115
PRINT(N'Operation applied to 3 rows out of 3')

PRINT(N'Delete row from [dbo].[Region]')
DELETE FROM [dbo].[Region] WHERE [RegionID] = 36

PRINT(N'Update rows in [dbo].[Region]')
UPDATE [dbo].[Region] SET [ModifiedTimeStamp]=NULL WHERE [RegionID] = 4
UPDATE [dbo].[Region] SET [Name]='Twente', [CapacityBeds]=1 WHERE [RegionID] = 11
UPDATE [dbo].[Region] SET [ModifiedTimeStamp]=NULL WHERE [RegionID] = 18
PRINT(N'Operation applied to 3 rows out of 3')

PRINT(N'Add constraints to [dbo].[ActionCodeHealthInsurerList]')
ALTER TABLE [dbo].[ActionCodeHealthInsurerList] WITH CHECK CHECK CONSTRAINT [FK_ActionCodeHealthInsurerList_ActionCodeHealthInsurer]
ALTER TABLE [dbo].[ActionCodeHealthInsurerList] WITH CHECK CHECK CONSTRAINT [FK_ActionCodeHealthInsurerList_ActionCodeHealthInsurerRegio]

PRINT(N'Add constraints to [dbo].[ActionCodeHealthInsurerRegio]')
ALTER TABLE [dbo].[ActionCodeHealthInsurerRegio] WITH CHECK CHECK CONSTRAINT [FK_ActionCodeHealthInsurerRegio_Region]
COMMIT TRANSACTION
GO