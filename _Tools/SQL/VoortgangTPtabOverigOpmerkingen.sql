-- Script Alle dossier van 1-1-2014 tot vandaag
-- Kolommen in resultaat:
-- - TransferId
-- - Datum aanmaak transfer
-- - Regio
-- - ZH organisatienaam
-- - ZH locatienaam
-- - ZH afdelingsnaam
-- - Veld Voortgang tranferproces/tab Overzicht/veld Opmerkingen leeg of niet-leeg (0 = leeg, 1= niet-leeg)
-- - Veld Voortgang tranferproces/tab Overzicht/veld Opmerkingen (inhoud)

DECLARE @DateBegin DATETIME

SET @DateBegin = '20140101' 

SELECT  Transfer.TransferID, 
		Transfer.CreatedDate, 
		region.Name AS 'Regio',
		organization.Name AS 'ZH organisatienaam',
		location.Name AS 'ZH locatienaam',
		department.Name AS 'ZH afdelingsnaam', 		
		(SELECT CASE 
		WHEN (Transfer.Remarks) IS NULL THEN '0'
		WHEN LTRIM(RTRIM((Transfer.Remarks))) = '' THEN '0'
		ELSE '1'
		END) AS 'Opmerkingen',		
		ISNULL (Transfer.Remarks, '') AS 'Opmerkingen inhoud'
FROM dbo.Transfer
JOIN department ON transfer.DepartmentID = Department.DepartmentID
JOIN location ON department.LocationID = location.LocationID
JOIN organization ON location.OrganizationID = organization.OrganizationID
JOIN region ON organization.RegionID = region.RegionID
WHERE transfer.CreatedDate >=@DateBegin
