
ALTER FUNCTION [dbo].[interruptionFlowBeginDate] ( @flowinstanceid INT )  
RETURNS DATETIME  
AS  
    BEGIN  
 
 
 
		DECLARE @ReturnValue DATETIME = NULL; 
 
        Select TOP 1 @ReturnValue = fis.StatusDate from FlowInstanceStatus fis 
		where fis.FlowInstanceID = @flowinstanceid and fis.FlowInstanceStatusTypeID = 1 
        ORDER BY fis.FlowInstanceStatusID DESC; 
 
        RETURN @ReturnValue; 
  
    END;  
  


--update dbo.DumpTransferExport
--	set [TO startdatum Onderbreking Transferproces] = dbo.interruptionFlowBeginDate(dbo.DumpTransferExport.FlowInstanceID)
--where FlowInstanceID is not null

--update dbo.DumpTransferReport
--	set [TO startdatum Onderbreking Transferproces] = dbo.interruptionFlowBeginDate(dbo.DumpTransferReport.FlowInstanceID)
--where FlowInstanceID is not null

