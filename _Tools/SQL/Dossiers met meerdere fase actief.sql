SELECT  dbo.FlowDefinition.Name ,
        Organization.Name AS Organization ,
        Transfer.TransferID ,
        dbo.clientname_formatted(Client.ClientID) AS Patient ,
        CAST(FlowInstance.CreatedDate AS SMALLDATETIME) AS CreatedDate ,
        ISNULL(dbo.flowFormtypeNameByFlowFieldName(Transfer.TransferID,
                                                   'RequestFormZHVVTType', -1),
               '') AS FormType ,
        dbo.PhaseDefinition.PhaseName
FROM    dbo.PhaseInstance WITH ( NOLOCK )
        INNER JOIN FlowInstance WITH ( NOLOCK ) ON FlowInstance.FlowInstanceID = PhaseInstance.FlowInstanceID
        INNER JOIN Transfer WITH ( NOLOCK ) ON Transfer.TransferID = FlowInstance.TransferID
        INNER JOIN Client WITH ( NOLOCK ) ON Client.ClientID = Transfer.ClientID
        INNER JOIN dbo.FlowDefinition WITH ( NOLOCK ) ON FlowDefinition.FlowDefinitionID = FlowInstance.FlowDefinitionID
        INNER JOIN dbo.PhaseDefinition WITH ( NOLOCK ) ON PhaseDefinition.PhaseDefinitionID = PhaseInstance.PhaseDefinitionID
                                                          AND PhaseDefinition.Phase > 0
        INNER JOIN dbo.PhaseInstance AS ActivePhaseInstance WITH ( NOLOCK ) ON ActivePhaseInstance.PhaseInstanceID = PhaseInstance.PhaseInstanceID
                                                              AND ActivePhaseInstance.Status = 1
                                                              AND ActivePhaseInstance.Cancelled = 0
        INNER JOIN dbo.Organization WITH ( NOLOCK ) ON Organization.OrganizationID = dbo.phaseInstanceLastOrganizationID(Transfer.TransferID,
                                                              ( CASE
                                                              WHEN FlowDefinition.FlowDefinitionID = 2
                                                              THEN 7
                                                              WHEN FlowDefinition.FlowDefinitionID = 4
                                                              THEN 42
                                                              END ))
WHERE   dbo.flowIsClosed(Transfer.TransferID) = 0
        AND PhaseInstance.FlowInstanceID IN (
        SELECT  PhaseInstance.FlowInstanceID
        FROM    dbo.PhaseInstance WITH ( NOLOCK )
                INNER JOIN dbo.PhaseInstance AS ActivePhaseInstance WITH ( NOLOCK ) ON ActivePhaseInstance.PhaseInstanceID = PhaseInstance.PhaseInstanceID
                                                              AND ActivePhaseInstance.Status = 1
                                                              AND ActivePhaseInstance.Cancelled = 0
                INNER JOIN dbo.PhaseDefinition ON PhaseDefinition.PhaseDefinitionID = ActivePhaseInstance.PhaseDefinitionID
                                                  AND PhaseDefinition.Phase > 0
        GROUP BY PhaseInstance.FlowInstanceID
        HAVING  COUNT(PhaseInstance.PhaseInstanceID) > 1 );
