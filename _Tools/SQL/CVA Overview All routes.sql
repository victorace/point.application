select organization.Name, count(flowinstance.FlowInstanceID)
from flowinstance 
inner join organization on organization.organizationid = flowinstance.startedbyorganizationid
where flowinstance.deleted = 0 and flowinstance.flowdefinitionid = 4 and createddate >= '20180101'
group by organization.Name