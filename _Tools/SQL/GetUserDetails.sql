
DECLARE @id AS UNIQUEIDENTIFIER;

SELECT TOP 1
        @id = UserId
FROM    aspnet_Users
WHERE   UserName = 'ismil tp';

SELECT  Employee.EmployeeID ,
        dbo.aspnet_Users.UserName ,
        Organization.Name ,
        dbo.Organization.OrganizationTypeID ,
		dbo.Organization.OrganizationID,
        dbo.Location.Name AS Location ,
		dbo.Location.LocationID,
        Department.Name AS Department,
		Department.DepartmentID
FROM    dbo.Employee
        INNER JOIN dbo.Department ON Department.DepartmentID = Employee.DepartmentID
        INNER JOIN dbo.Location ON Location.LocationID = Department.LocationID
        INNER JOIN dbo.Organization ON Organization.OrganizationID = Location.OrganizationID
        INNER JOIN dbo.aspnet_Users ON aspnet_Users.UserId = Employee.UserId
WHERE   Employee.UserId = @id;


--DECLARE @employeeid INT= 6540
--SELECT  Employee.EmployeeID ,
--        dbo.aspnet_Users.UserName ,
--        Organization.Name ,
--        dbo.Organization.OrganizationTypeID ,
--		dbo.Organization.OrganizationID,
--        dbo.Location.Name AS Location ,
--		dbo.Location.LocationID,
--        Department.Name AS Department,
--		Department.DepartmentID
--FROM    dbo.Employee
--        INNER JOIN dbo.Department ON Department.DepartmentID = Employee.DepartmentID
--        INNER JOIN dbo.Location ON Location.LocationID = Department.LocationID
--        INNER JOIN dbo.Organization ON Organization.OrganizationID = Location.OrganizationID
--        INNER JOIN dbo.aspnet_Users ON aspnet_Users.UserId = Employee.UserId
--WHERE   Employee.EmployeeID = @employeeid
