DECLARE @jaar AS INT = 2017;


SELECT 
    Region.RegionID AS RegioID,
    Organization.OrganizationID AS OrganisatieID,
    LocationID AS LocationID,

    Region.Name AS RegioNaam,
    Organization.Name AS OrganisatieNaam,
    Location.Name AS LocatieNaam,

    ISNULL((SELECT CASE Participation WHEN 0 THEN 'Geen' WHEN 1 THEN 'Alleen Fax/Mail' WHEN 2 THEN 'Actief' END  FROM dbo.FlowDefinitionParticipation WHERE LocationID = Location.LocationID AND FlowDefinitionID = 1 AND DepartmentID IS NULL),'') AS Deelname_VVTZH,
    ISNULL((SELECT CASE FlowDirection WHEN -1 THEN 'Geen' WHEN 0 THEN 'Verzender' WHEN 1 THEN 'Ontvanger' WHEN 2 THEN 'Beide' END  FROM dbo.FlowDefinitionParticipation WHERE LocationID = Location.LocationID AND FlowDefinitionID = 1 AND DepartmentID IS NULL),'') AS Richting_VVTZH,
    ISNULL((SELECT CASE Participation WHEN 0 THEN 'Geen' WHEN 1 THEN 'Alleen Fax/Mail' WHEN 2 THEN 'Actief' END  FROM dbo.FlowDefinitionParticipation WHERE LocationID = Location.LocationID AND FlowDefinitionID = 2 AND DepartmentID IS NULL),'') AS Deelname_ZHVVT,
    ISNULL((SELECT CASE FlowDirection WHEN -1 THEN 'Geen' WHEN 0 THEN 'Verzender' WHEN 1 THEN 'Ontvanger' WHEN 2 THEN 'Beide' END  FROM dbo.FlowDefinitionParticipation WHERE LocationID = Location.LocationID AND FlowDefinitionID = 2 AND DepartmentID IS NULL),'') AS Richting_ZHVVT,
    ISNULL((SELECT CASE Participation WHEN 0 THEN 'Geen' WHEN 1 THEN 'Alleen Fax/Mail' WHEN 2 THEN 'Actief' END  FROM dbo.FlowDefinitionParticipation WHERE LocationID = Location.LocationID AND FlowDefinitionID = 3 AND DepartmentID IS NULL),'') AS Deelname_RzTP,
    ISNULL((SELECT CASE FlowDirection WHEN -1 THEN 'Geen' WHEN 0 THEN 'Verzender' WHEN 1 THEN 'Ontvanger' WHEN 2 THEN 'Beide' END  FROM dbo.FlowDefinitionParticipation WHERE LocationID = Location.LocationID AND FlowDefinitionID = 3 AND DepartmentID IS NULL),'') AS Richting_RzTP,
    ISNULL((SELECT CASE Participation WHEN 0 THEN 'Geen' WHEN 1 THEN 'Alleen Fax/Mail' WHEN 2 THEN 'Actief' END  FROM dbo.FlowDefinitionParticipation WHERE LocationID = Location.LocationID AND FlowDefinitionID = 4 AND DepartmentID IS NULL),'') AS Deelname_CVA,
    ISNULL((SELECT CASE FlowDirection WHEN -1 THEN 'Geen' WHEN 0 THEN 'Verzender' WHEN 1 THEN 'Ontvanger' WHEN 2 THEN 'Beide' END  FROM dbo.FlowDefinitionParticipation WHERE LocationID = Location.LocationID AND FlowDefinitionID = 4 AND DepartmentID IS NULL),'') AS Richting_CVA,
    ISNULL((SELECT CASE Participation WHEN 0 THEN 'Geen' WHEN 1 THEN 'Alleen Fax/Mail' WHEN 2 THEN 'Actief' END  FROM dbo.FlowDefinitionParticipation WHERE LocationID = Location.LocationID AND FlowDefinitionID = 5 AND DepartmentID IS NULL),'') AS Deelname_ZHZH,
    ISNULL((SELECT CASE FlowDirection WHEN -1 THEN 'Geen' WHEN 0 THEN 'Verzender' WHEN 1 THEN 'Ontvanger' WHEN 2 THEN 'Beide' END  FROM dbo.FlowDefinitionParticipation WHERE LocationID = Location.LocationID AND FlowDefinitionID = 5 AND DepartmentID IS NULL),'') AS Richting_ZHZH,
    ISNULL((SELECT CASE Participation WHEN 0 THEN 'Geen' WHEN 1 THEN 'Alleen Fax/Mail' WHEN 2 THEN 'Actief' END  FROM dbo.FlowDefinitionParticipation WHERE LocationID = Location.LocationID AND FlowDefinitionID = 6 AND DepartmentID IS NULL),'') AS Deelname_HAVVT,
    ISNULL((SELECT CASE FlowDirection WHEN -1 THEN 'Geen' WHEN 0 THEN 'Verzender' WHEN 1 THEN 'Ontvanger' WHEN 2 THEN 'Beide' END  FROM dbo.FlowDefinitionParticipation WHERE LocationID = Location.LocationID AND FlowDefinitionID = 6 AND DepartmentID IS NULL),'') AS Richting_HAVVT,
    ISNULL((SELECT CASE Participation WHEN 0 THEN 'Geen' WHEN 1 THEN 'Alleen Fax/Mail' WHEN 2 THEN 'Actief' END  FROM dbo.FlowDefinitionParticipation WHERE LocationID = Location.LocationID AND FlowDefinitionID = 7 AND DepartmentID IS NULL),'') AS Deelname_VVTVVT,
    ISNULL((SELECT CASE FlowDirection WHEN -1 THEN 'Geen' WHEN 0 THEN 'Verzender' WHEN 1 THEN 'Ontvanger' WHEN 2 THEN 'Beide' END  FROM dbo.FlowDefinitionParticipation WHERE LocationID = Location.LocationID AND FlowDefinitionID = 7 AND DepartmentID IS NULL),'') AS Richting_VVTVVT,

    (SELECT COUNT(*) FROM dbo.FormSetVersion WITH(NOLOCK)
    INNER JOIN dbo.FlowInstance WITH(NOLOCK) ON FlowInstance.TransferID = FormSetVersion.TransferID
    INNER JOIN dbo.FlowInstanceOrganization WITH(NOLOCK) ON FlowInstanceOrganization.FlowInstanceID = FlowInstance.FlowInstanceID AND FlowDirection = 0 AND LocationID = Location.LocationID
    WHERE FormTypeID = 209 AND FormSetVersion.Deleted = 0 AND YEAR(CreateDate) = @jaar) AS VO,

    (SELECT COUNT(*) FROM dbo.FormSetVersion WITH(NOLOCK)
    INNER JOIN dbo.FlowFieldValue WITH(NOLOCK) ON FormSetVersion.FormSetVersionID = FlowFieldValue.FormSetVersionID AND FlowFieldID = 642 AND Value = 'Pdf'
    INNER JOIN dbo.FlowInstance WITH(NOLOCK) ON FlowInstance.TransferID = FormSetVersion.TransferID
    INNER JOIN dbo.FlowInstanceOrganization WITH(NOLOCK) ON FlowInstanceOrganization.FlowInstanceID = FlowInstance.FlowInstanceID AND FlowDirection = 0 AND LocationID = Location.LocationID
    WHERE FormTypeID = 209 AND FormSetVersion.Deleted = 0 AND YEAR(CreateDate) = @jaar) AS 'VO als bijlage',

    (SELECT COUNT(*) FROM dbo.FormSetVersion WITH(NOLOCK)
    INNER JOIN dbo.FlowInstance WITH(NOLOCK) ON FlowInstance.TransferID = FormSetVersion.TransferID
    INNER JOIN dbo.FlowInstanceOrganization WITH(NOLOCK) ON FlowInstanceOrganization.FlowInstanceID = FlowInstance.FlowInstanceID AND FlowDirection = 0 AND LocationID = Location.LocationID
    WHERE FormTypeID = 216 AND FormSetVersion.Deleted = 0 AND YEAR(CreateDate) = @jaar) AS GRZUitslagFormulieren,

    (SELECT COUNT(*) FROM dbo.FormSetVersion WITH(NOLOCK)
    INNER JOIN dbo.FlowInstance WITH(NOLOCK) ON FlowInstance.TransferID = FormSetVersion.TransferID
    INNER JOIN dbo.FlowInstanceOrganization WITH(NOLOCK) ON FlowInstanceOrganization.FlowInstanceID = FlowInstance.FlowInstanceID AND FlowDirection = 0 AND LocationID = Location.LocationID
    WHERE FormTypeID = 220 AND FormSetVersion.Deleted = 0 AND YEAR(CreateDate) = @jaar) AS GRZTriageFormulieren,

    (SELECT COUNT(*) FROM dbo.FormSetVersion WITH(NOLOCK)
    INNER JOIN dbo.FlowInstance WITH(NOLOCK) ON FlowInstance.TransferID = FormSetVersion.TransferID
    INNER JOIN dbo.FlowInstanceOrganization WITH(NOLOCK) ON FlowInstanceOrganization.FlowInstanceID = FlowInstance.FlowInstanceID AND FlowDirection = 0 AND LocationID = Location.LocationID
    WHERE FormTypeID = 238 AND FormSetVersion.Deleted = 0 AND YEAR(CreateDate) = @jaar) AS ELVFormulieren,

    (SELECT COUNT(*) FROM dbo.FormSetVersion WITH(NOLOCK)
    INNER JOIN dbo.FlowInstance WITH(NOLOCK) ON FlowInstance.TransferID = FormSetVersion.TransferID
    INNER JOIN dbo.FlowInstanceOrganization WITH(NOLOCK) ON FlowInstanceOrganization.FlowInstanceID = FlowInstance.FlowInstanceID AND FlowDirection = 0 AND LocationID = Location.LocationID
    WHERE FormTypeID = 214 AND FormSetVersion.Deleted = 0 AND YEAR(CreateDate) = @jaar) AS UVVFormulieren,

    --(SELECT COUNT(*) FROM Frequency WITH(NOLOCK)
  --WHERE Frequency.Cancelled = 0) AS DoorlopendDossiers,

    ISNULL(IsAdminOrganization, 0) AS AdminOrganisatie,
    ISNULL(HL7Interface, 0) AS KoppelingA19,
    ISNULL(HL7InterfaceExtraFields, 0) AS KoppelingA19AFVelden, --AFVoorvullen (5 velden)
    ISNULL(WSInterface, 0) AS KoppelingWebservice,
    ISNULL(WSInterfaceExtraFields, 0) AS KoppelingWebserviceAFVelden, --AFVoorvullen (alle velden)
    ISNULL(SSO, 0) AS SSO,

    -- Taken from the POINT.Auth web config. We should probably make this an OrganizationSetting
    CASE WHEN Organization.OrganizationID IN (8812, 3345, 7605, 10305) THEN 1 ELSE 0 END AS SAML,

    CASE ISNULL(HL7ConnectionCallFromPoint, 0) WHEN 0 THEN 1 ELSE 0 END AS PatientFocusGeenKnopAlleDossier,
    ISNULL(CreateUserViaSSO, 0) AS AutoCreateUser,
    CASE WHEN ISNULL(ZorgmailUsername,'') = '' THEN 0 ELSE 1 END AS Huisartsbericht,
    (SELECT CASE COUNT(*) WHEN 0 THEN 0 ELSE 1 END FROM dbo.EndpointConfiguration WHERE OrganizationID = Organization.OrganizationID) AS VOVoorvullen,
    (SELECT COUNT(*) FROM dbo.FlowFieldAttributeException WHERE FlowFieldAttributeID = 642 AND ReadOnly = 0 AND OrganizationID = Organization.OrganizationID) AS BijlagenVOOvernemen,
    'niet meer gebruikt' AS PatientPortaal,
    ISNULL(UsesPalliativeCareInVO, 0) AS PalliatieveOverdracht,
    ISNULL(CapacityBeds, 0) AS BedcapaciteitRegio
  FROM dbo.Organization 
  JOIN dbo.Region ON Region.RegionID = Organization.RegionID 
  JOIN dbo.Location ON Location.OrganizationID = Organization.OrganizationID 
 WHERE OrganizationTypeID = 1
   AND Organization.Inactive = 0
   AND Location.Inactive = 0
   --AND Organization.OrganizationID = 8500
ORDER BY Organization.Name, Location.Name;
