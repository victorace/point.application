DECLARE @transferid AS INT = 765073

SELECT FormType.Name AS [Form], FlowField.Name AS [FlowField], FLowFIeld.Label, FlowFieldValue.Value FROM dbo.FlowFieldValue
INNER JOIN FLowFIeld ON FlowFieldValue.FlowFieldID = FlowField.FlowFieldID
INNER JOIN FormSetVersion ON FlowFieldValue.FormSetVersionID = FormSetVersion.FormSetVersionID AND FormSetVersion.TransferID = @transferid
INNER JOIN PhaseInstance ON FormSetVersion.PhaseInstanceID = PhaseInstance.PhaseInstanceID
INNER JOIN PhaseDefinition ON PhaseInstance.PhaseDefinitionID = PhaseDefinition.PhaseDefinitionID
INNER JOIN FormType ON FormSetVersion.FormTypeID = FormType.FormTypeID
WHERE LEN(FlowFieldValue.Value) > 0
ORDER BY PhaseDefinition.Phase
