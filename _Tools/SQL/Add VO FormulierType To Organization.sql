DECLARE @organizationid AS INT  = 6312
DECLARE @organizationname AS VARCHAR(255) = 'unknown'

DECLARE @flowfieldattributeid AS INT

DECLARE @requestformzhvvt AS INT = 202
DECLARE @eoverdrachtflow AS INT = 209

DECLARE @readonly AS BIT = 0
DECLARE @visible AS BIT = 1
DECLARE @required AS BIT = 1
DECLARE @signonchange AS BIT = 1

DECLARE @voformuliertype AS INT = 642
DECLARE @PatientOntvingZorgVoorOpnameJaNee AS INT = 190
DECLARE @ZorgInZorginstellingVoorkeurPatient1 AS INT = 295
DECLARE @AfterCareCategory AS INT = 1051

SET @organizationname = (SELECT TOP 1 Name FROM dbo.Organization WHERE OrganizationID = @organizationid)

SET @flowfieldattributeid = (SELECT FlowFieldAttribute.FlowFieldAttributeID FROM dbo.FlowFieldAttribute WHERE dbo.FlowFieldAttribute.FlowFieldID = @voformuliertype AND FlowFieldAttribute.FormTypeID = @eoverdrachtflow)
IF @flowfieldattributeid IS NOT NULL
BEGIN
	IF NOT EXISTS (SELECT 1 FROM dbo.FlowFieldAttributeException WHERE OrganizationID = @organizationid AND FlowFieldAttributeID = @flowfieldattributeid)
	BEGIN
		INSERT INTO dbo.FlowFieldAttributeException (FlowFieldAttributeID, RegionID, OrganizationID, ReadOnly, Visible, Required, SignalOnChange, PhaseDefinitionID, RequiredByFlowFieldID, RequiredByFlowFieldValue)
			VALUES (@flowfieldattributeid, NULL, @organizationid, @readonly, @visible, @required, @signonchange, NULL, NULL, NULL)
		PRINT 'Setting VOFormulierType ON for Organization: ' + @organizationname
	END
END

SET @flowfieldattributeid = (SELECT FlowFieldAttribute.FlowFieldAttributeID FROM dbo.FlowFieldAttribute WHERE dbo.FlowFieldAttribute.FlowFieldID = @PatientOntvingZorgVoorOpnameJaNee AND FlowFieldAttribute.FormTypeID = @requestformzhvvt)
IF @flowfieldattributeid IS NOT NULL
BEGIN
	IF NOT EXISTS (SELECT 1 FROM dbo.FlowFieldAttributeException WHERE OrganizationID = @organizationid AND FlowFieldAttributeID = @flowfieldattributeid)
	BEGIN
		INSERT INTO dbo.FlowFieldAttributeException (FlowFieldAttributeID, RegionID, OrganizationID, ReadOnly, Visible, Required, SignalOnChange, PhaseDefinitionID, RequiredByFlowFieldID, RequiredByFlowFieldValue)
		VALUES (@flowfieldattributeid, NULL, @organizationid, @readonly, @visible, @required, @signonchange, NULL, 275, '{"Values": ["210", "211", "213"]}')
		PRINT 'Setting PatientOntvingZorgVoorOpnameJaNee as Required for Organization: ' + @organizationname
	END
END


SET @flowfieldattributeid = (SELECT FlowFieldAttribute.FlowFieldAttributeID FROM dbo.FlowFieldAttribute WHERE dbo.FlowFieldAttribute.FlowFieldID = @ZorgInZorginstellingVoorkeurPatient1 AND FlowFieldAttribute.FormTypeID = @requestformzhvvt)
IF @flowfieldattributeid IS NOT NULL
BEGIN
	IF NOT EXISTS (SELECT 1 FROM dbo.FlowFieldAttributeException WHERE OrganizationID = @organizationid AND FlowFieldAttributeID = @flowfieldattributeid)
	BEGIN
		INSERT INTO dbo.FlowFieldAttributeException (FlowFieldAttributeID, RegionID, OrganizationID, ReadOnly, Visible, Required, SignalOnChange, PhaseDefinitionID, RequiredByFlowFieldID, RequiredByFlowFieldValue)
		VALUES (@flowfieldattributeid, NULL, @organizationid, @readonly, @visible, @required, @signonchange, NULL, 275, '{"Values": ["210", "211", "213"]}')
		PRINT 'Setting ZorgInZorginstellingVoorkeurPatient1 as Required for Organization: ' + @organizationname
	END
END

SET @flowfieldattributeid = (SELECT FlowFieldAttribute.FlowFieldAttributeID FROM dbo.FlowFieldAttribute WHERE dbo.FlowFieldAttribute.FlowFieldID = @AfterCareCategory AND FlowFieldAttribute.FormTypeID = @requestformzhvvt)
IF @flowfieldattributeid IS NOT NULL
BEGIN
	IF NOT EXISTS (SELECT 1 FROM dbo.FlowFieldAttributeException WHERE OrganizationID = @organizationid AND FlowFieldAttributeID = @flowfieldattributeid)
	BEGIN
		INSERT INTO dbo.FlowFieldAttributeException (FlowFieldAttributeID, RegionID, OrganizationID, ReadOnly, Visible, Required, SignalOnChange, PhaseDefinitionID, RequiredByFlowFieldID, RequiredByFlowFieldValue)
		VALUES (@flowfieldattributeid, NULL, @organizationid, @readonly, @visible, @required, @signonchange, NULL, 275, '{"Values": ["210", "211", "213"]}')
		PRINT 'Setting AfterCareCategory as Required for Organization: ' + @organizationname
	END
END

PRINT 'Done.'

