SELECT
	region.regionid,
	organization.organizationid,
	organization.name AS organization,
	organization.inactive AS oinactive,
	--organization.hashprefix,
	--organization.sso, organization.ssohashtype, organization.ssokey,
	location.locationid,
	location.name AS location,
	location.inactive AS linactive,
	department.departmentid,
	department.name	AS department,
	department.inactive AS dinactive,
	employee.employeeid,
	--dbo.employeename_formatted(employee.employeeid) AS employeename,
	employee.ExternID,
	aspnet_users.loweredusername
	--autocreateset.AutoCreateCode
FROM department
	INNER JOIN location ON department.locationid = location.locationid
	INNER JOIN organization ON location.organizationid = organization.organizationid
	INNER JOIN region ON region.regionid = organization.regionid
	INNER JOIN employee ON department.departmentid = employee.departmentid
	INNER JOIN aspnet_users ON employee.userid = aspnet_users.userid
	--RIGHT OUTER JOIN AutoCreateSet ON Location.LocationID = AutoCreateSet.LocationID
WHERE 
	organization.organizationid = 774
--	department.departmentid = 6312
--	aspnet_users.LoweredUserName = 'ismil tp3'
--	AND autocreateset.autocreatecode IS NOT null
	AND employee.externid IS NOT NULL