PRINT 'Add PointAdministrator and OrganizationAdministrator employees to the EmployeeReportRole table for the report Genereer POINT dump in Excel.';

DECLARE @reportroleid AS INT;
DECLARE @reportid AS INT;

IF EXISTS ( SELECT TOP 1
                    1
            FROM    Report
            WHERE   Name = 'Genereer POINT dump in Excel' )
    BEGIN

        IF EXISTS ( SELECT TOP 1
                            1
                    FROM    dbo.ReportRole
                    WHERE   Name = 'Regio' )
            BEGIN

                SET @reportid = ( SELECT TOP 1
                                            ReportID
                                  FROM      dbo.Report
                                  WHERE     Name = 'Genereer POINT dump in Excel'
                                );

                SET @reportroleid = ( SELECT TOP 1
                                                ReportRoleID
                                      FROM      dbo.ReportRole
                                      WHERE     Name = 'Regio'
                                    );


                IF NOT EXISTS ( SELECT TOP 1
                                        1
                                FROM    dbo.ReportReportRole
                                WHERE   ReportID = @reportid
                                        AND ReportRoleID = @reportroleid )
                    INSERT  INTO dbo.ReportReportRole
                            ( ReportID, ReportRoleID )
                    VALUES  ( @reportid, -- ReportID - int
                              @reportroleid  -- ReportRoleID - int
                              );

                INSERT  INTO dbo.EmployeeReportRole
                        ( EmployeeID ,
                          ReportRoleID
                        )
                        SELECT  DISTINCT
                                Employee.EmployeeID ,
                                @reportroleid
                        FROM    dbo.Employee
                                INNER JOIN dbo.aspnet_Users ON aspnet_Users.UserId = Employee.UserId
                                INNER JOIN dbo.vw_aspnet_UsersInRoles ON vw_aspnet_UsersInRoles.UserId = Employee.UserId
                                INNER JOIN dbo.aspnet_Roles ON aspnet_Roles.RoleId = vw_aspnet_UsersInRoles.RoleId
                                LEFT OUTER JOIN dbo.EmployeeReportRole ON EmployeeReportRole.EmployeeID = Employee.EmployeeID
                                                              AND EmployeeReportRole.ReportRoleID = @reportroleid
                        WHERE   aspnet_Roles.RoleName IN (
                                'PointAdministrator',
                                'OrganizationAdministrator' ) 
                                AND EmployeeReportRole.EmployeeReportRoleID IS NULL


            END;
    END;

PRINT 'Done.';