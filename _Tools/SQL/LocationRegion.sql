-- ORIGINAL


USE [TXX-DB-POINT-P]
GO

ALTER TABLE [dbo].[LocationRegion] DROP CONSTRAINT [FK_LocationRegion_Location]
GO

ALTER TABLE [dbo].[LocationRegion]  WITH CHECK ADD  CONSTRAINT [FK_LocationRegion_Location] FOREIGN KEY([LocationID])
REFERENCES [dbo].[Location] ([LocationID])
GO

ALTER TABLE [dbo].[LocationRegion] CHECK CONSTRAINT [FK_LocationRegion_Location]
GO


ALTER TABLE [dbo].[LocationRegion] DROP CONSTRAINT [FK_LocationRegion_Region]
GO

ALTER TABLE [dbo].[LocationRegion]  WITH CHECK ADD  CONSTRAINT [FK_LocationRegion_Region] FOREIGN KEY([RegionID])
REFERENCES [dbo].[Region] ([RegionID])
GO

ALTER TABLE [dbo].[LocationRegion] CHECK CONSTRAINT [FK_LocationRegion_Region]
GO




