
--begin tran

update LocationCare
   set OrganizationCareID = 106 -- "Florence"
 where OrganizationCareID = 140 -- "Humanitas Woonzorg+"

delete from OrganizationCare
 where OrganizationCareID = 140

update Organization 
   set Name = Name
 where LocationCareID in ( 557, 558, 559, 3754 )

--rollback tran

 --select *  from organization where LocationCareID in ( 557, 558, 559, 3754 )

