
-- review
INSERT INTO dbo.EndpointConfiguration (OrganizationID, FormTypeID, Model, Method, Address, Username, Password, Label, Inactive, SkipCertificateCheck)
  VALUES (6385, 209, N'EOverdracht', N'Tests.LUMCEOverdrachtTest', N'https://wspoint.lumc.nl/wsPoint.svc', NULL, NULL, N'Ophalen gegevens uit EPD', 0, 0)

INSERT INTO dbo.EndpointConfiguration (OrganizationID, FormTypeID, Model, Method, Address, Username, Password, Label, Inactive, SkipCertificateCheck)
  VALUES (6312, 209, N'EOverdracht', N'Z_POINT_EOverdracht', N'https://cernerbenelux.nl:10444/sap/bc/srt/rfc/sap/z_point_overdr/444/service/binding', N'Linkassist', N'C@rner23', N'Ophalen gegevens uit EPD', 0, 1)

INSERT INTO dbo.EndpointConfiguration (OrganizationID, FormTypeID, Model, Method, Address, Username, Password, Label, Inactive, SkipCertificateCheck)
  VALUES (6312, 202, N'RequestZHVVT', N'Z_POINT_RequestFormZHVVT', N'https://cernerbenelux.nl:10444/sap/bc/srt/rfc/sap/z_point_aanvr/444/service/binding', N'Linkassist', N'C@rner23', N'Ophalen gegevens uit EPD', 0, 1)

-- test
--INSERT INTO dbo.EndpointConfiguration (OrganizationID, FormTypeID, Model, Method, Address, Username, Password, Label, Inactive, SkipCertificateCheck)
--  VALUES (6385, 209, N'EOverdracht', N'Tests.LUMCEOverdrachtTest', N'https://wspoint.lumc.nl/wsPoint.svc', NULL, NULL, N'Ophalen gegevens uit EPD', 0, 0)

--INSERT INTO dbo.EndpointConfiguration (OrganizationID, FormTypeID, Model, Method, Address, Username, Password, Label, Inactive, SkipCertificateCheck)
--  VALUES (6312, 209, N'EOverdracht', N'Z_POINT_EOverdracht', N'https://cernerbenelux.nl:10444/sap/bc/srt/rfc/sap/z_point_overdr/444/service/binding', N'Linkassist', N'C@rner23', N'Ophalen gegevens uit EPD', 0, 1)

--INSERT INTO dbo.EndpointConfiguration (OrganizationID, FormTypeID, Model, Method, Address, Username, Password, Label, Inactive, SkipCertificateCheck)
--  VALUES (6312, 202, N'RequestZHVVT', N'Z_POINT_RequestFormZHVVT', N'https://cernerbenelux.nl:10444/sap/bc/srt/rfc/sap/z_point_aanvr/444/service/binding', N'Linkassist', N'C@rner23', N'Ophalen gegevens uit EPD', 0, 1)

-- acceptatie
--INSERT INTO dbo.EndpointConfiguration (OrganizationID, FormTypeID, Model, Method, Address, Username, Password, Label, Inactive, SkipCertificateCheck)
--  VALUES (6385, 209, N'EOverdracht', N'LUMCEOverdracht', N'https://wspoint.test.lumc.nl/wsPoint.svc', NULL, NULL, N'Ophalen gegevens uit EPD', 0, 0)

--INSERT INTO dbo.EndpointConfiguration (OrganizationID, FormTypeID, Model, Method, Address, Username, Password, Label, Inactive, SkipCertificateCheck)
--  VALUES (6312, 209, N'EOverdracht', N'Z_POINT_EOverdracht', N'https://cernerbenelux.nl:10444/sap/bc/srt/rfc/sap/z_point_overdr/444/service/binding', N'Linkassist', N'C@rner23', N'Ophalen gegevens uit EPD', 0, 0)

--INSERT INTO dbo.EndpointConfiguration (OrganizationID, FormTypeID, Model, Method, Address, Username, Password, Label, Inactive, SkipCertificateCheck)
--  VALUES (6312, 202, N'RequestZHVVT', N'Z_POINT_RequestFormZHVVT', N'https://cernerbenelux.nl:10444/sap/bc/srt/rfc/sap/z_point_aanvr/444/service/binding', N'Linkassist', N'C@rner23', N'Ophalen gegevens uit EPD', 0, 0)

-- productie
--INSERT INTO dbo.EndpointConfiguration (OrganizationID, FormTypeID, Model, Method, Address, Username, Password, Label, Inactive, SkipCertificateCheck)
--  VALUES (6385, 209, N'EOverdracht', N'LUMCEOverdracht', N'https://wspoint.lumc.nl/wsPoint.svc', NULL, NULL, N'Ophalen gegevens uit EPD', 0, 0)

--INSERT INTO dbo.EndpointConfiguration (OrganizationID, FormTypeID, Model, Method, Address, Username, Password, Label, Inactive, SkipCertificateCheck)
--  VALUES (6312, 209, N'EOverdracht', N'Z_POINT_EOverdracht', N'https://cernerbenelux.nl:10444/sap/bc/srt/rfc/sap/z_point_overdr/444/service/binding', N'Linkassist', N'C@rner23', N'Ophalen gegevens uit EPD', 0, 0)

--INSERT INTO dbo.EndpointConfiguration (OrganizationID, FormTypeID, Model, Method, Address, Username, Password, Label, Inactive, SkipCertificateCheck)
--  VALUES (6312, 202, N'RequestZHVVT', N'Z_POINT_RequestFormZHVVT', N'https://cernerbenelux.nl:10444/sap/bc/srt/rfc/sap/z_point_aanvr/444/service/binding', N'Linkassist', N'C@rner23', N'Ophalen gegevens uit EPD', 0, 0)