
DECLARE @id AS UNIQUEIDENTIFIER;

SELECT TOP 1
        @id = UserId
FROM    aspnet_Users
WHERE   UserName = 'TechxxAdmin';

UPDATE  dbo.aspnet_Membership
SET     IsLockedOut = 0 ,
        FailedPasswordAttemptCount = 0 ,
        FailedPasswordAnswerAttemptCount = 0 ,
        LastPasswordChangedDate = GETDATE()
WHERE   UserId = @id;
