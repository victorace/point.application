--SET IDENTITY_INSERT [dbo].[Region] ON
--INSERT INTO [dbo].[Region] ([RegionID], [Name], [CapacityBeds], [ModifiedTimeStamp]) VALUES (36, 'Twente', 1, NULL)
--SET IDENTITY_INSERT [dbo].[Region] OFF

declare @newregion int = 36

declare @idlist1 int
declare @idlijst2014 int
declare @idlijstgvp int

declare @idlist1B int
declare @idlijst2014B int
declare @idlijstgvpB int


select @idlist1 = ActionCodeHealthInsurerRegionID
  from ActionCodeHealthInsurerRegio 
 where Name = 'List 1' and RegionID = 30

insert into ActionCodeHealthInsurerRegio 
select Name, @newregion, DateBegin, DateEnd
  from ActionCodeHealthInsurerRegio
 where ActionCodeHealthInsurerRegionID = @idlist1

select @idlist1B = @@IDENTITY


select @idlijst2014 = ActionCodeHealthInsurerRegionID
  from ActionCodeHealthInsurerRegio 
 where Name = 'Lijst 2014' and RegionID = 30

insert into ActionCodeHealthInsurerRegio 
select Name, @newregion, DateBegin, DateEnd
  from ActionCodeHealthInsurerRegio
 where ActionCodeHealthInsurerRegionID = @idlijst2014

select @idlijst2014B = @@IDENTITY


select @idlijstgvp = ActionCodeHealthInsurerRegionID
  from ActionCodeHealthInsurerRegio 
 where Name = 'Lijst GVP 2018' and RegionID = 30

insert into ActionCodeHealthInsurerRegio 
select Name, @newregion, DateBegin, DateEnd
  from ActionCodeHealthInsurerRegio
 where ActionCodeHealthInsurerRegionID = @idlijstgvp

select @idlijstgvpB = @@IDENTITY


insert into ActionCodeHealthInsurerList
select @idlist1B, ActionCodeHealthInsurerID
  from ActionCodeHealthInsurerList
 where ActionCodeHealthInsurerRegionID = @idlist1

insert into ActionCodeHealthInsurerList
select @idlijst2014B, ActionCodeHealthInsurerID
  from ActionCodeHealthInsurerList
 where ActionCodeHealthInsurerRegionID = @idlijst2014

insert into ActionCodeHealthInsurerList
select @idlijstgvpB, ActionCodeHealthInsurerID
  from ActionCodeHealthInsurerList
 where ActionCodeHealthInsurerRegionID = @idlijstgvp