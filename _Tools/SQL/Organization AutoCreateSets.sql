SELECT Organization.Name, dbo.AutoCreateSet.AutoCreateCode, dbo.AutoCreateSet.IsDefaultForOrganization FROM dbo.AutoCreateSet
INNER JOIN location ON AutoCreateSet.LocationID = Location.LocationID
INNER JOIN dbo.Organization ON Location.OrganizationID = Organization.OrganizationID AND Organization.OrganizationID IN (10305, 7605)
