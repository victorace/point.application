-- ignore errors for non-existing functions

drop function RandomLetter
go
drop function RandomVowel_NotY
go
drop function RandomConsonate_NotY
go
drop function RandomDigit
go
drop function RandomDate
go
drop function RandomReplace_Constrained
go
drop view NewIDForFunction
go

CREATE VIEW NewIDForFunction AS
 SELECT NEWID() idNew
 GO
 
CREATE FUNCTION RandomLetter() RETURNS CHAR(1) BEGIN
 DECLARE @cReturn CHAR
 SELECT @cReturn=CHAR(ABS(CHECKSUM(idNew))%26+65) FROM NewIDForFunction
 RETURN @cReturn
 END
 GO
 
CREATE FUNCTION RandomVowel_NotY() RETURNS CHAR(1) BEGIN
 DECLARE @cReturn CHAR
 SELECT @cReturn=SUBSTRING('AEIOU',ABS(CHECKSUM(idNew))%5+1,1)
 FROM NewIDForFunction
 RETURN @cReturn
 END
 GO
 
CREATE FUNCTION RandomConsonate_NotY() RETURNS CHAR(1) BEGIN
 DECLARE @cReturn CHAR
 SELECT @cReturn=SUBSTRING('BCDFGHJKLMNPQRSTVWXZ',ABS(CHECKSUM(idNew))%20+1,1)
 FROM NewIDForFunction
 RETURN @cReturn
 END
 GO
 
CREATE FUNCTION RandomDigit() RETURNS CHAR(1) BEGIN
 DECLARE @cReturn CHAR
 SELECT @cReturn=CAST(ABS(CHECKSUM(idNew))%10 AS CHAR(1)) FROM NewIDForFunction
 RETURN @cReturn
 END
 GO

CREATE FUNCTION RandomDate(@dIn datetime) RETURNS DATETIME BEGIN
 DECLARE @cReturn DATETIME
 if @dIn is null return null
 select @cReturn = dateadd(d, -1 * ABS(CHECKSUM(idNew))%20000 , '19991231') from NewIDForFunction
 RETURN @cReturn 
 END
 GO

CREATE FUNCTION RandomReplace_Constrained(@sIn VARCHAR(255))
 RETURNS VARCHAR(255) AS
 BEGIN
 
DECLARE @cbIn TINYINT
 DECLARE @sResult VARCHAR(50)
 DECLARE @cbProcessed TINYINT
 DECLARE @sNextChar CHAR(1)
 DECLARE @nNextChar TINYINT
 
SET @cbProcessed = 1
 SET @sResult = ''
SELECT @cbIn=LEN(@sIn)
 
WHILE @cbProcessed <= @cbIn  BEGIN
 SET @sNextChar = SUBSTRING(@sIn,@cbProcessed,1)
 SET @nNextChar = ASCII(@sNextChar)
 SELECT @sResult = @sResult +
 CASE
 WHEN @nNextChar BETWEEN 48 AND 57 THEN dbo.RandomDigit()
 WHEN @nNextChar IN (65,69,73,79,85) THEN dbo.RandomVowel_NotY()
 WHEN @nNextChar IN (89,121) THEN @sNextChar
 WHEN @nNextChar BETWEEN 65 AND 90 THEN dbo.RandomConsonate_NotY()
 WHEN @nNextChar IN (97,101,105,111,117) THEN LOWER(dbo.RandomVowel_NotY())
 WHEN @nNextChar BETWEEN 97 AND 122 THEN LOWER(dbo.RandomConsonate_NotY())
 ELSE @sNextChar
 END
 IF @cbProcessed >= 255
    BREAK

 SET @cbProcessed = @cbProcessed + 1
 END 

RETURN @sResult 

END
go

print 'Done!'
go


