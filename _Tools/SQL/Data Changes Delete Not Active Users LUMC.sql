	--****************************************************************************--
	--** Delete not active users of LUMC (locked out or not loged in sinds...)  **--
	--****************************************************************************--
	

	-- ATTENTION:
	-- There are 3098 users in PR based on these criteria
	-- Peildatum must be defined by LUMC. following date is only for test.

	Declare @Peildatum datetime = Convert (datetime, '01-01-2015',105); -- not active sinds ...
	Declare @OrganizationID int = 6385; -- LUMC ziekenhuis, ID in production DB. 
	
	

	-- This query can be expanded to a Function or a SP.
	-- [aspnet_Users_DeleteUser] can be used too. but I think this query is easier!


	Declare @UserID uniqueidentifier;
	Declare @EmployeeID int;
	Declare Cur Cursor For
			select 
				EmployeeID,
				Employee.UserID	
			from Employee
			left join department on 
			employee.departmentid = department.departmentid
			left join location on 
			department.locationid = location.locationid
			left join organization on
			organization.organizationid = location.OrganizationID
			Left Join aspnet_Membership on
			Employee.userid = aspnet_Membership.userid
			where organization.OrganizationID = @OrganizationID And
			Employee.UserId Is Not Null And 
			(
				aspnet_Membership.IsLockedOut = 1 AND
				aspnet_Membership.LastLoginDate  < @Peildatum
			);
			Open Cur;
	Begin Try
		Begin Tran			
			Fetch Next From Cur Into @EmployeeID, @UserID;
			While @@FETCH_STATUS = 0
			Begin
				Delete from aspnet_Membership where UserID = @UserID;
				Delete from aspnet_UsersInRoles where UserID = @UserID;
				Delete from FunctionRole where EmployeeID = @EmployeeID;
				Update Employee Set Inactive = 1, UserID = Null  where EmployeeID = @EmployeeID;
				Delete from aspnet_Users where Userid = @UserID;										
			Fetch Next From Cur Into @EmployeeID, @UserID 
			End -- while
		Commit Tran
	End Try
	Begin Catch
		If @@Trancount > 0 
			Rollback Tran;
	End Catch
	Close Cur;	
	Deallocate Cur;
