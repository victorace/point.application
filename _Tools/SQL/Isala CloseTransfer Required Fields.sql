DECLARE @organizationid AS INT = 9128;
DECLARE @organizationname AS VARCHAR(255) = 'unknown';

DECLARE @flowfieldattributeid AS INT;

DECLARE @closetransferzhvvt AS INT = 215;

DECLARE @readonly AS BIT = 0;
DECLARE @visible AS BIT = 1;
DECLARE @required AS BIT = 1;
DECLARE @signonchange AS BIT = 0;

DECLARE @DatumEindeBehandelingMedischSpecialist AS INT = 73;
DECLARE @MedischeSituatieDatumOpname AS INT = 149;
DECLARE @CareBeginDate AS INT =	288;
DECLARE @DischargeProposedStartDate AS INT = 310;
DECLARE @RealizedHCPInstitution	AS INT = 1156;
DECLARE @RealizedDischargeDate	AS INT = 1157;

SET @organizationname = (SELECT TOP 1 Name FROM dbo.Organization WHERE OrganizationID = @organizationid);

SET @flowfieldattributeid = (SELECT FlowFieldAttributeID FROM dbo.FlowFieldAttribute 
	WHERE FlowFieldID = @DatumEindeBehandelingMedischSpecialist AND FormTypeID = @closetransferzhvvt);
IF @flowfieldattributeid IS NOT NULL
BEGIN
	IF NOT EXISTS (SELECT 1 FROM dbo.FlowFieldAttributeException WHERE OrganizationID = @organizationid AND FlowFieldAttributeID = @flowfieldattributeid)
	BEGIN
		INSERT INTO dbo.FlowFieldAttributeException (FlowFieldAttributeID, RegionID, OrganizationID, ReadOnly, Visible, Required, SignalOnChange, PhaseDefinitionID, RequiredByFlowFieldID, RequiredByFlowFieldValue)
		VALUES (@flowfieldattributeid, NULL, @organizationid, @readonly, @visible, @required, @signonchange, NULL, 275, '{"Values": ["210", "211", "213"]}');
		PRINT 'Setting DatumEindeBehandelingMedischSpecialist as Required for Organization: ' + @organizationname;
	END;
	ELSE
		PRINT 'DatumEindeBehandelingMedischSpecialist already has a FlowFieldAttributeException for Organization: ' + @organizationname;
END;

SET @flowfieldattributeid = (SELECT FlowFieldAttributeID FROM dbo.FlowFieldAttribute 
	WHERE FlowFieldID = @MedischeSituatieDatumOpname AND FormTypeID = @closetransferzhvvt);
IF @flowfieldattributeid IS NOT NULL
BEGIN
	IF NOT EXISTS (SELECT 1 FROM dbo.FlowFieldAttributeException WHERE OrganizationID = @organizationid AND FlowFieldAttributeID = @flowfieldattributeid)
	BEGIN
		INSERT INTO dbo.FlowFieldAttributeException (FlowFieldAttributeID, RegionID, OrganizationID, ReadOnly, Visible, Required, SignalOnChange, PhaseDefinitionID, RequiredByFlowFieldID, RequiredByFlowFieldValue)
		VALUES (@flowfieldattributeid, NULL, @organizationid, @readonly, @visible, @required, @signonchange, NULL, 275, '{"Values": ["210", "211", "213"]}');
		PRINT 'Setting MedischeSituatieDatumOpname as Required for Organization: ' + @organizationname;
	END;
	ELSE
		PRINT 'PatientOntvingZorgVoorOpnameJaNee already has a FlowFieldAttributeException for Organization: ' + @organizationname;
END;

SET @flowfieldattributeid = (SELECT FlowFieldAttributeID FROM dbo.FlowFieldAttribute 
	WHERE FlowFieldID = @CareBeginDate AND FormTypeID = @closetransferzhvvt);
IF @flowfieldattributeid IS NOT NULL
BEGIN
	IF NOT EXISTS (SELECT 1 FROM dbo.FlowFieldAttributeException WHERE OrganizationID = @organizationid AND FlowFieldAttributeID = @flowfieldattributeid)
	BEGIN
		INSERT INTO dbo.FlowFieldAttributeException (FlowFieldAttributeID, RegionID, OrganizationID, ReadOnly, Visible, Required, SignalOnChange, PhaseDefinitionID, RequiredByFlowFieldID, RequiredByFlowFieldValue)
		VALUES (@flowfieldattributeid, NULL, @organizationid, @readonly, @visible, @required, @signonchange, NULL, 275, '{"Values": ["244", "245", "247"]}');
		PRINT 'Setting CareBeginDate as Required for Organization: ' + @organizationname;
	END;
	ELSE
		PRINT 'CareBeginDate already has a FlowFieldAttributeException for Organization: ' + @organizationname;
END;

SET @flowfieldattributeid = (SELECT FlowFieldAttributeID FROM dbo.FlowFieldAttribute 
	WHERE FlowFieldID = @DischargeProposedStartDate AND FormTypeID = @closetransferzhvvt);
IF @flowfieldattributeid IS NOT NULL
BEGIN
	IF NOT EXISTS (SELECT 1 FROM dbo.FlowFieldAttributeException WHERE OrganizationID = @organizationid AND FlowFieldAttributeID = @flowfieldattributeid)
	BEGIN
		INSERT INTO dbo.FlowFieldAttributeException (FlowFieldAttributeID, RegionID, OrganizationID, ReadOnly, Visible, Required, SignalOnChange, PhaseDefinitionID, RequiredByFlowFieldID, RequiredByFlowFieldValue)
		VALUES (@flowfieldattributeid, NULL, @organizationid, @readonly, @visible, @required, @signonchange, NULL, 309, '{"Values": ["ja"]}');
		PRINT 'Setting DischargeProposedStartDate as Required for Organization: ' + @organizationname;
	END;
	ELSE
		PRINT 'DischargeProposedStartDate already has a FlowFieldAttributeException for Organization: ' + @organizationname;
END;

SET @flowfieldattributeid = (SELECT FlowFieldAttributeID FROM dbo.FlowFieldAttribute 
	WHERE FlowFieldID = @RealizedHCPInstitution AND FormTypeID = @closetransferzhvvt);
IF @flowfieldattributeid IS NOT NULL
BEGIN
	IF NOT EXISTS (SELECT 1 FROM dbo.FlowFieldAttributeException WHERE OrganizationID = @organizationid AND FlowFieldAttributeID = @flowfieldattributeid)
	BEGIN
		INSERT INTO dbo.FlowFieldAttributeException (FlowFieldAttributeID, RegionID, OrganizationID, ReadOnly, Visible, Required, SignalOnChange, PhaseDefinitionID, RequiredByFlowFieldID, RequiredByFlowFieldValue)
		VALUES (@flowfieldattributeid, NULL, @organizationid, @readonly, @visible, @required, @signonchange, NULL, NULL, NULL);
		PRINT 'Setting RealizedHCPInstitution as Required for Organization: ' + @organizationname;
	END;
	ELSE
		PRINT 'RealizedHCPInstitution already has a FlowFieldAttributeException for Organization: ' + @organizationname;
END;

SET @flowfieldattributeid = (SELECT FlowFieldAttributeID FROM dbo.FlowFieldAttribute 
	WHERE FlowFieldID = @RealizedDischargeDate AND FormTypeID = @closetransferzhvvt);
IF @flowfieldattributeid IS NOT NULL
BEGIN
	IF NOT EXISTS (SELECT 1 FROM dbo.FlowFieldAttributeException WHERE OrganizationID = @organizationid AND FlowFieldAttributeID = @flowfieldattributeid)
	BEGIN
		INSERT INTO dbo.FlowFieldAttributeException (FlowFieldAttributeID, RegionID, OrganizationID, ReadOnly, Visible, Required, SignalOnChange, PhaseDefinitionID, RequiredByFlowFieldID, RequiredByFlowFieldValue)
		VALUES (@flowfieldattributeid, NULL, @organizationid, @readonly, @visible, @required, @signonchange, NULL, NULL, NULL);
		PRINT 'Setting RealizedDischargeDate as Required for Organization: ' + @organizationname;
	END;
	ELSE
		PRINT 'RealizedDischargeDate already has a FlowFieldAttributeException for Organization: ' + @organizationname;
END;

PRINT 'Done.';