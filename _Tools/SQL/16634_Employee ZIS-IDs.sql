SELECT 
	e.EmployeeID, 
	e.FirstName, 
	e.MiddleName, 
	e.LastName, 
	e.Gender, 
	e.EmailAddress AS 'Email', 
	e.ExternID AS 'ZIS-ID', 
	CASE e.InActive
		WHEN 0 THEN 'Ja'
		ELSE 'Nee'
	END AS Actief, 
	u.LoweredUserName AS 'Gebruikersnaam',
	m.LastLoginDate
FROM dbo.Employee AS e
INNER JOIN dbo.aspnet_Users AS u ON u.UserId = e.UserId
INNER JOIN dbo.aspnet_Membership AS m ON u.UserId = m.UserId
INNER JOIN department AS d ON d.DepartmentID = e.DepartmentID
INNER JOIN location AS l ON l.LocationID = d.LocationID
INNER JOIN organization AS o ON o.OrganizationID = l.OrganizationID AND o.OrganizationID = 766
ORDER BY e.LastName