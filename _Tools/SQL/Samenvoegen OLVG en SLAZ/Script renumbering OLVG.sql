-- Script voor omnummeren van patient nummers voor ziekenhuis
----------------------------------
--
-- BELANGRIJK
-- Bij samenvoegen van twee ziekenhuizen eerst wordt 'Script renumbering ....sql' uitgevoerd en daarna 'Script merge tow hospitals'
--
-- Voorbereidingen op text bestand met data voor import
-- Kontroleren velden in header: PATIENTNUMBER_OLD;PATIENTNUMBER_NEW
----------------------------------
-- Voorbereiding van script voor importeren
-- Aanpassen @organizationname van ziekenhuis
----------------------------------
--
-- Importeren:
-- 
-- 1: Importeer .txt bestand met import data naar tabel 'MAPPING_PATIENT' (indien dit bestand is nog niet geimporteerd). In .txt bestand staat data 
--    voor ziekenhuis voor merge.
-- 2: Pas de headers eventueel aan.
-- 3: Draai script 'Script renumbering patient.sql'.
-- 4: Kopieer het nu geproduceerde 'log' vanuit de messages-tab naar een tekstbestand en sla deze op.
--
----------------------------------

disable trigger Client.Client_InsertUpdate on Client
go

declare @organizationname varchar(255)
declare @clientid int
declare @patientnumber as varchar(50)
declare @patientnumbernew as varchar(50)

set @organizationname = 'Onze Lieve Vrouwe Gasthuis' 
set @patientnumbernew = ''

declare clientCursor cursor for
	select Client.ClientID, 
		Client.PatientNumber from Client
		join Transfer on Client.ClientID = Transfer.ClientID 
		join Department on Transfer.DepartmentID = Department.DepartmentID
		join Location on Department.LocationID = Location.LocationID
		join Organization on Organization.OrganizationID = Location.OrganizationID
		where OrganizationTypeID = 1 and Organization.Name like ( '%' + @organizationname + '%') and Organization.Inactive = 0

open clientCursor

Print 'Patienten omnummering ' + @organizationname

fetch next from clientCursor into
    @clientid,
    @patientnumber

while @@FETCH_STATUS = 0

begin
	set @patientnumbernew = ''
	select @patientnumbernew = PATIENTNUMBER_NEW
	from MAPPING_PATIENT
	where PATIENTNUMBER_OLD = @patientnumber

	if exists (select 1 from MAPPING_PATIENT
		where PATIENTNUMBER_OLD = @patientnumber)
		begin

			update Client set Client.PatientNumber = @patientnumbernew
			where Client.ClientID = @clientid and Client.PatientNumber = @patientnumber

			Print 'Patientnumber old ' + @patientnumber + ' ' + 'Patientnumber new ' + @patientnumbernew + ' ClientID ' + str(@clientID)
		end	
	
	fetch next from clientCursor into
		@clientid,
		@patientnumber
end

close clientCursor
deallocate clientCursor

Print 'Done!'
go

enable trigger Client.Client_InsertUpdate on Client

