I. Omnummering OLVG.

1.Verwijderen tabel 'MAPPING_PATIENT' indien bestaat. Importeer MAPPING_PATIENT.csv bestand met import data naar tabel 'MAPPING_PATIENT'(handmatig). In MAPPING_PATIENT.csv bestand staat data 
   voor ziekenhuis voor merge.
2.Draai script 'Script renumbering OLVG.sql'.
3.Kopieer het nu geproduceerde 'log' vanuit de messages-tab naar een tekstbestand en sla deze op.



II. Samenvoegen twee ziekenhuizen.

1.Uitvoeren 'Script merge two hospitalst.sql'. 
2.Kopieer het nu geproduceerde 'log' vanuit de messages-tab naar een tekstbestand en sla deze op.
