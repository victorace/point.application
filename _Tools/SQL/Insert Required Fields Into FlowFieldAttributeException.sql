--Insert verplichte velden in FlowFieldAttributeException
Begin Try
	Begin Tran
	-- FlowFieldAttributeException Fields. 
	-- SET DEFAULT VALUES IF NEEDED.
	Declare @OrganizationID int = 11554;
	Declare @RegionID int = null;
	Declare @ReadOnly bit = 0;
	Declare @Visible bit = 1;
	Declare @Required bit = 1;
	Declare @SignalOnChange bit = 0;
	Declare @PhaseDefinitionID int = null;
	Declare @RequiredByFlowFieldID int = null;
	Declare @RequiredByFlowFieldValue varchar(max) = null;

	-- List of FlowFieldAttributeID's based on FlowFieldID and FormTypeID from FlowFiwldAttributes table.
	DECLARE @FlowFieldAttributeIDs TABLE (FlowFieldAttributeID INT);
	insert into @FlowFieldAttributeIDs values (45),(73),(118),(149),(150),(151),(190),(1051),(2303),(2317);

	print 'FlowFieldAttributeIDs in temp table.'

	Declare AttrIDCursor cursor for 
	select FlowFieldAttributeID from @FlowFieldAttributeIDs;

	Declare @FID int;

	Open AttrIDCursor;
	Fetch Next From AttrIDCursor Into @FID;

	While @@FETCH_STATUS = 0
	Begin
		print @FID;

		insert into FlowFieldAttributeException (
		FlowFieldAttributeID,
		RegionID,
		organizationID,
		ReadOnly,
		Visible,
		Required,
		SignalOnChange,
		PhaseDefinitionID,
		RequiredByFlowFieldID,
		RequiredByFlowFieldValue
		)

		Values 
		(
		@FID,
		@RegionID, 
		@OrganizationID, 
		@ReadOnly,
		@Visible,
		@Required,
		@SignalOnChange,
		@PhaseDefinitionID,
		@RequiredByFlowFieldID,
		@RequiredByFlowFieldValue)

		Fetch Next From AttrIDCursor Into @FID;
	End

	Close AttrIDCursor;
	Deallocate AttrIDCursor;
	Commit Tran
End Try
Begin Catch
	IF(@@TRANCOUNT > 0)
		ROLLBACK TRAN;
	print 'Error';
End Catch
