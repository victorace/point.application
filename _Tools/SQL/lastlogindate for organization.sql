select
	aspnet_users.loweredusername as gebruikersnaam, 
	employee.lastname as achternaam, 
	employee.externid as 'zis-od', 
	aspnet_membership.LastLoginDate as 'laatste inlogdatum',
	employee.InActive as 'verwijderde',
	dbo.aspnet_Membership.IsLockedOut as 'geblokkeerd',
	case dbo.aspnet_Membership.LastLockoutDate 
		when '1754-01-01 00:00:00.000' then null
		else dbo.aspnet_Membership.LastLockoutDate
	end as 'laatste geblokkeerddatum',
	employee.ChangePasswordByEmployee as 'Moet wachtwoord wijzigen bij volgende login'
from employee
	inner join department on employee.departmentid = department.departmentid
	inner join location on department.locationid = location.locationid
	inner join organization on location.organizationid = organization.organizationid
	inner join aspnet_users on employee.userid = aspnet_users.userid
	inner join aspnet_membership on aspnet_users.userid = aspnet_membership.userid
where organization.name = 'LUMC'
order by 'laatste inlogdatum'
