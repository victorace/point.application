SELECT  
		--Employee.UserId ,
        Employee.EmployeeID ,
	    Organization.OrganizationID ,
        Organization.Name AS O,
		Location.LocationID,
		Location.Name AS L,
		Department.DepartmentID,
		Department.Name AS D,
		Department.ExternID AS DepartmentExternID,
        dbo.aspnet_Users.LoweredUserName
        --Organization.MFARequired ,
        --Employee.MFANumber ,
        --Employee.MFAConfirmedDate
FROM    dbo.Employee
        INNER JOIN dbo.Department ON Department.DepartmentID = Employee.DepartmentID
        INNER JOIN dbo.Location ON Location.LocationID = Department.LocationID
        INNER JOIN dbo.Organization ON Organization.OrganizationID = Location.OrganizationID
        INNER JOIN dbo.aspnet_Users ON aspnet_Users.UserId = Employee.UserId
        INNER JOIN dbo.aspnet_Membership ON aspnet_Membership.UserId = aspnet_Users.UserId
WHERE   Organization.OrganizationID = 6312;