--SELECT 
--	Organization.OrganizationID,
--	Organization.Name AS OrganizationName,
--	OrganizationType.Name AS OrganizationType,
--	Organization.OrganizationTypeID,
--	Organization.Inactive AS OrganizationInactive,
--	Location.LocationID,
--	Location.Name AS LocationName,
--	Location.Inactive AS LocationInactive,
--	Department.DepartmentID,
--	Department.Name AS DepartmentName,
--	Department.Inactive AS DepartmentInactive
--FROM dbo.Organization 
--INNER JOIN dbo.Location ON Location.OrganizationID = Organization.OrganizationID
--INNER JOIN OrganizationType ON OrganizationType.OrganizationTypeID = Organization.OrganizationTypeID
--INNER JOIN dbo.Department ON Department.LocationID = Location.LocationID
--WHERE Organization.OrganizationID = 264

SELECT  Organization.OrganizationID,
		Organization.Name AS [Oranization Name],
		dbo.OrganizationType.Name AS [Type],
		Location.LocationID,
		Location.Name AS [Location Name],
		Department.DepartmentID ,
        Department.Name AS [Department Name]
FROM    dbo.Department
        INNER JOIN dbo.Location ON Location.LocationID = Department.LocationID
        INNER JOIN dbo.Organization ON Organization.OrganizationID = Location.OrganizationID
		INNER JOIN dbo.OrganizationType ON OrganizationType.OrganizationTypeID = Organization.OrganizationTypeID
WHERE   DepartmentID IN ( 2745, 320 );