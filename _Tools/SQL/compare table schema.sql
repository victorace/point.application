SELECT      name, system_type_id, max_length, precision, 'In DumpTransferExport'
FROM        sys.columns WHERE object_id = OBJECT_ID(N'DumpTransferExport')
EXCEPT
SELECT      name, system_type_id, max_length, precision, 'In DumpTransferExport'
FROM        sys.columns WHERE object_id = OBJECT_ID(N'DumpTransferReport')
 
UNION ALL
 
SELECT      name, system_type_id, max_length, precision, 'In DumpTransferReport'
FROM        sys.columns WHERE object_id = OBJECT_ID(N'DumpTransferReport')
EXCEPT
SELECT      name, system_type_id, max_length, precision, 'In DumpTransferReport'
FROM        sys.columns WHERE object_id = OBJECT_ID(N'DumpTransferExport')
 
UNION ALL
 
SELECT      name, system_type_id, max_length, precision, 'In Both'
FROM        sys.columns WHERE object_id = OBJECT_ID(N'DumpTransferExport')
INTERSECT
SELECT      name, system_type_id, max_length, precision, 'In Both'
FROM        sys.columns WHERE object_id = OBJECT_ID(N'DumpTransferReport')