IF OBJECT_ID(N'dbo.LocationDepartmentInactive$', N'U') IS NOT NULL
BEGIN
	DROP TABLE [dbo].[LocationDepartmentInactive$];
END;

SET ANSI_NULLS ON;
GO

SET QUOTED_IDENTIFIER ON;
GO

SET ANSI_PADDING ON;
GO

CREATE TABLE [dbo].[LocationDepartmentInactive$] (
	[LocationID] [INT] NOT NULL,
	[Locatie] [NVARCHAR](255) NULL,
	[Locatie inactief] [BIT] NULL  DEFAULT(0),
	[DepartmentID] [INT] NOT NULL,
	[Afdeling] [NVARCHAR](255) NULL,
	[Identifier] [NVARCHAR](50) NULL,
	[Afdeling inactief] [BIT] NOT NULL DEFAULT(0),
	[Laatst gewijzigd] [DATETIME] NULL,
) ON [PRIMARY];

GO

SET ANSI_PADDING OFF;
GO

PRINT 'Table LocationDepartmentInactive$ created.';
GO

PRINT 'Done..';
GO