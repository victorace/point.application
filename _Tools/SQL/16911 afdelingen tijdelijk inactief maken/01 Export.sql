select 
	l.locationid as LocationID, 
	l.Name as 'Locatie', 
	l.Inactive as 'Locatie inactief', 
	d.DepartmentID as DepartmentID,
	d.Name as 'Afdeling', 
	d.ExternID as 'Identifier',
	d.Inactive as 'Afdeling inactief',
	d.ModifiedTimeStamp as 'Laatst gewijzigd'
from organization as o
inner join location as l on o.organizationid = l.organizationid
inner join department as d on l.locationid = d.locationid 
where o.organizationid = 336
order by l.name, d.name, d.ModifiedTimeStamp desc