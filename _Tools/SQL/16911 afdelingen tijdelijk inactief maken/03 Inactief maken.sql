
PRINT 'Using LocationDepartmentInactive$ to set Departments in-active.'

UPDATE d
	SET d.Inactive = 1
FROM Department AS d
	INNER JOIN LocationDepartmentInactive$ AS lda ON d.DepartmentID = lda.DepartmentID
WHERE d.Inactive = 0 AND lda.[Afdeling inactief] = 1

PRINT 'Done.'