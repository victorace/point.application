
PRINT 'Using LocationDepartmentInactive$ to set Departments active.'

UPDATE d
	SET d.Inactive = 0
FROM Department AS d
	INNER JOIN LocationDepartmentInactive$ AS lda ON d.DepartmentID = lda.DepartmentID
WHERE d.Inactive = 1 AND lda.[Afdeling inactief] = 1

PRINT 'Done.'