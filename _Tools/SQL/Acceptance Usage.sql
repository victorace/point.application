select 
region.name as Region,
organization.name as Organization,
location.name as Location,
Department.name as Department,
dbo.employeename_formatted(employee.employeeid) as Employee,
PointLog_LogRead.Timestamp, PointLog_LogRead.Action, PointLog_LogRead.Url from PointLog_LogRead
inner join employee on employee.employeeid = PointLog_LogRead.employeeid
inner join department on employee.departmentid = department.departmentid
inner join location on location.locationid = department.locationid
inner join organization on organization.organizationid = location.organizationid
inner join region on region.regionid = organization.regionid
where PointLog_LogRead.timestamp >= '20180608'
order by PointLog_LogRead.Timestamp