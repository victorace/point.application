select Employee.EmployeeID, Employee.BIG, Employee.ExternID, Employee.UserId, dbo.aspnet_Users.LoweredUserName,
LastLoginDate, Department.AGB from dbo.Employee
inner join dbo.aspnet_Users on Employee.UserId = aspnet_Users.UserId
inner join dbo.aspnet_Membership on aspnet_Users.UserId = aspnet_Membership.UserId
inner join dbo.Department on Employee.DepartmentID = Department.DepartmentID
inner join dbo.Location on Department.LocationID = Location.LocationID
inner join dbo.Organization on Location.OrganizationID = Organization.OrganizationID
where Organization.OrganizationID = 76
order by LastLoginDate desc