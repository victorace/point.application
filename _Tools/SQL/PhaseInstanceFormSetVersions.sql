SELECT Deleted, PhaseInstance.PhaseInstanceID, PhaseInstance.Status, PhaseInstance.Cancelled, PD_FormType.Name AS PD_Form, FSV_FormType.Name AS FSV_Name FROM dbo.FormSetVersion
INNER JOIN dbo.PhaseInstance ON FormSetVersion.PhaseInstanceID = PhaseInstance.PhaseInstanceID
INNER JOIN dbo.PhaseDefinition ON PhaseInstance.PhaseDefinitionID = PhaseDefinition.PhaseDefinitionID
INNER JOIN dbo.FormType AS PD_FormType ON PhaseDefinition.FormTypeID = PD_FormType.FormTypeID
INNER JOIN FormType AS FSV_FormType ON FormSetVersion.FormTypeID = FSV_FormType.FormTypeID
WHERE TransferID = 573345