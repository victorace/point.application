declare @transferid int = ##
declare @startingphase int = ##

DELETE dbo.MutFlowFieldValue
FROM MutFlowFieldValue AS mmffv
	INNER JOIN dbo.FormSetVersion fsv
		ON fsv.FormSetVersionID = mmffv.FormSetVersionID
	INNER JOIN dbo.PhaseInstance phi
		ON phi.PhaseInstanceID = fsv.PhaseInstanceID
	INNER JOIN dbo.PhaseDefinition pdi
		ON pdi.PhaseDefinitionID = phi.PhaseDefinitionID
	AND pdi.Phase >= @startingphase
	AND TransferID = @transferid

DELETE FlowFieldValue
FROM   FlowFieldValue ffv
	INNER JOIN FormSetVersion fsv
		ON ffv.FormSetVersionID = fsv.FormSetVersionID
	INNER JOIN dbo.PhaseInstance phi
		ON phi.PhaseInstanceID = fsv.PhaseInstanceID
	INNER JOIN dbo.PhaseDefinition pdi
		ON pdi.PhaseDefinitionID = phi.PhaseDefinitionID
	AND pdi.Phase >= @startingphase
	AND TransferID = @transferid

DELETE FROM dbo.FormSetVersion 
	FROM dbo.FormSetVersion fsv
	INNER JOIN dbo.PhaseInstance phi
		ON phi.PhaseInstanceID = fsv.PhaseInstanceID
	INNER JOIN dbo.PhaseDefinition pdi
		ON pdi.PhaseDefinitionID = phi.PhaseDefinitionID
	AND pdi.Phase >= @startingphase
	AND TransferID = @transferid

DELETE dbo.PhaseInstance
FROM dbo.PhaseInstance AS pi
	INNER JOIN dbo.FlowInstance fi
		ON fi.FlowInstanceID = pi.FlowInstanceID
	INNER JOIN dbo.PhaseDefinition pd
		ON pd.PhaseDefinitionID = pi.PhaseDefinitionID
	AND fi.TransferID = @transferid
	AND pd.Phase >= @startingphase

