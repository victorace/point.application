
declare @today as datetime = getdate();

update department
set 
	department.CapacityDepartment = 1,
	department.CapacityDepartmentNext = 1,
	department.CapacityDepartment3 = 1,
	department.CapacityDepartment4 = 1,

	department.AdjustmentCapacityDate = @today,
	department.CapacityDepartmentNextDate = DATEADD(d, 1, @today),
	department.CapacityDepartment3Date = DATEADD(d, 2, @today),
	department.CapacityDepartment4Date = DATEADD(d, 3, @today),

	CapacityDepartmentHomeCare = null,
	CapacityDepartmentNextHomeCare = null,
	CapacityDepartment3HomeCare = null,
	CapacityDepartment4HomeCare = null
	
where departmentid in (select department.DepartmentID from department
	inner join location on location.locationid = department.locationid
	inner join organization on organization.OrganizationID = location.OrganizationID
	--inner join region on organization.RegionID = region.RegionID and region.regionid = 20
	where Department.CapacityFunctionality = 1);


--delete from SamlDepartment where OrganizationID = 3875
--delete from SamlOrganization where OrganizationID = 3875
--select * from SamlOrganization