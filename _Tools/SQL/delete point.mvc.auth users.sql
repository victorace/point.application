--SELECT organization.name, employee.employeeid, employee.InActive, employee.ExternID, aspnet_Users.LoweredUserName,
--organization.OrganizationID, location.locationid, department.DepartmentID FROM Employee
--INNER join Department ON Employee.DepartmentID = Department.DepartmentID
--INNER join Location ON Department.LocationID = Location.LocationID
--INNER join Organization ON Location.OrganizationID = Organization.OrganizationID
--INNER join aspnet_Users ON Employee.UserId = aspnet_Users.UserId
--WHERE employee.ExternID = 'phillip.partridge@gmail.com'

-- T-ADFS001@A.CORP
-- phillip.partridge@gmail.com
-- ppartridge@linkassist.nl
-- POINT_Testuser1@test-maasstadziekenhuis.nl

--DECLARE @externid AS VARCHAR(255) = 'ppartridge@linkassist.nl'
--SELECT * FROM Employee WHERE ExternID = @externid
--SELECT * FROM LogCreateUser WHERE ExternID = @externid

--DELETE FROM LogCreateUser WHERE ExternID = @externid
--DELETE FROM UsedHashCodes WHERE employeeid in (SELECT employee.employeeid FROM Employee INNER join aspnet_Users ON aspnet_users.userid = employee.userid and aspnet_users.LoweredUserName = @externid)
--DELETE FROM Employee WHERE userid in (SELECT userid FROM aspnet_Users WHERE LoweredUserName = @externid)
--DELETE FROM aspnet_Membership WHERE userid in (SELECT userid FROM aspnet_Users WHERE LoweredUserName = @externid)
--DELETE FROM aspnet_UsersInRoles WHERE userid in (SELECT userid FROM aspnet_Users WHERE LoweredUserName = @externid)
--DELETE FROM aspnet_Users WHERE userid in (SELECT userid FROM aspnet_Users WHERE LoweredUserName = @externid)

SELECT employee.employeeid, employee.ExternID, employee.UserId, dbo.aspnet_Users.LoweredUserName, Organization.Name, Organization.OrganizationID FROM dbo.Employee 
INNER JOIN dbo.Department ON Employee.DepartmentID = Department.DepartmentID
INNER JOIN dbo.Location ON Department.LocationID = Location.LocationID
INNER JOIN dbo.Organization ON Location.OrganizationID  = Organization.OrganizationID
LEFT OUTER JOIN dbo.aspnet_Users ON Employee.UserId = aspnet_Users.UserId
--WHERE employee.ExternID = 'ppartridge@linkassist.nl'
WHERE LEN(employee.ExternID) > 0 AND employee.ExternID IS NOT NULL AND Employee.ExternID <> 'NULL'
AND Organization.OrganizationID 
	IN (336, 5268, 6312, 3951, 271)
ORDER BY Organization.Name


--SELECT PhaseInstanceID FROM dbo.PhaseInstance WHERE RealizedByEmployeeID = 67186
--SELECT PhaseInstanceID FROM dbo.PhaseInstance WHERE StartedByEmployeeID = 67186
--SELECT PhaseInstanceID FROM dbo.PhaseInstance WHERE EmployeeID = 67186

--SELECT formsetversionid FROM dbo.FormSetVersion WHERE PhaseInstanceID = 3360439
--SELECT formsetversionid FROM dbo.FormSetVersion WHERE PhaseInstanceID = 3360440

DECLARE @userid AS UNIQUEIDENTIFIER = '7A5AD9F9-0A3E-4BC5-B590-B9A1AF9AE625'
--DELETE FROM dbo.FormSetVersion WHERE FormSetVersionID IN (3360439, 3360440)
--DELETE FROM PhaseInstance WHERE PhaseInstanceID IN (3360439, 3360440)
DELETE FROM dbo.UsedHashCodes
DELETE FROM dbo.aspnet_UsersInRoles WHERE UserId = @userid
DELETE FROM dbo.aspnet_Membership WHERE UserId = @userid
DELETE FROM dbo.EmployeeDepartment WHERE EmployeeID IN ( SELECT EmployeeID FROM dbo.Employee WHERE UserId = @userid)
DELETE FROM dbo.Employee WHERE UserId = @userid
DELETE FROM dbo.aspnet_Users WHERE UserId = @userid


