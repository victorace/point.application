DECLARE @cur AS CURSOR;

DECLARE @projectid AS INT;
DECLARE @name AS VARCHAR(255);
DECLARE @organizationid AS INT;
DECLARE @departmentid AS INT;
DECLARE @inactive AS BIT;

DECLARE @aftercareprojects AS INT = 1520;
DECLARE @flowfieldvalueid AS INT;
DECLARE @value AS VARCHAR(MAX);
DECLARE @newvalue AS VARCHAR(MAX);

SET NOCOUNT ON;

PRINT 'Migrating Projects from Department to Organization.';

DECLARE @tablename AS VARCHAR(50); 
DECLARE @command AS VARCHAR(255); 
SET @tablename = 'ProjectMigration_Project';
IF NOT ( EXISTS ( SELECT    *
                  FROM      sysobjects
                  WHERE     xtype = 'U'
                            AND name = @tablename ) )
    BEGIN 

        --CREATE backup tables
        PRINT '- Creating backups';

        SET @command = 'SELECT * INTO ' + @tablename + ' FROM dbo.Project;'; 
        EXEC(@command); 
    

        SET @tablename = 'ProjectMigration_FlowFieldValue';
        IF NOT ( EXISTS ( SELECT    *
                          FROM      sysobjects
                          WHERE     xtype = 'U'
                                    AND name = @tablename ) )
            BEGIN 
 
                SET @command = 'SELECT * INTO ' + @tablename
                    + ' FROM dbo.FlowFieldValue WHERE FlowFieldID = 1520 AND LEN(Value) > 0;'; 
                EXEC(@command); 
            END; 

        IF OBJECT_ID('tempdb..##temp_project_orgdep') IS NOT NULL
            DROP TABLE ##temp_project_orgdep;

        CREATE TABLE ##temp_project_orgdep
            (
              ProjectID INT NOT NULL ,
              Name VARCHAR(255) NOT NULL ,
              OrganizationID INT NOT NULL ,
              DepartmentID INT NOT NULL ,
              InActive BIT NOT NULL
            );

        IF OBJECT_ID('tempdb..##flowfieldvalue_project') IS NOT NULL
            DROP TABLE ##flowfieldvalue_project;

        CREATE TABLE ##flowfieldvalue_project
            (
              ProjectID INT NOT NULL
            );

        --Migrate Project from Department to Organization, removing duplicates. 
        PRINT '- Migrate Project from Department to Organization';
        INSERT  INTO ##temp_project_orgdep
                ( ProjectID ,
                  Name ,
                  OrganizationID ,
                  DepartmentID ,
                  InActive
                )
                SELECT  Project.ProjectID ,
                        Project.Name ,
                        Organization.OrganizationID ,
                        Department.DepartmentID ,
                        Project.Inactive
                FROM    Project
                        INNER JOIN dbo.Department ON Department.DepartmentID = Project.DepartmentID
                        INNER JOIN dbo.Location ON Location.LocationID = Department.LocationID
                        INNER JOIN dbo.Organization ON Organization.OrganizationID = Location.OrganizationID
                ORDER BY Project.Name ,
                        Organization.OrganizationID ,
                        Location.LocationID ,
                        Department.DepartmentID;

        SET @cur = CURSOR FORWARD_ONLY FOR
    SELECT  ProjectID ,
            Name ,
            OrganizationID ,
            DepartmentID ,
            InActive
    FROM    ##temp_project_orgdep;
        OPEN @cur;
        FETCH NEXT FROM @cur INTO @projectid, @name, @organizationid,
            @departmentid, @inactive;

        WHILE @@FETCH_STATUS = 0
            BEGIN
        
                IF NOT EXISTS ( SELECT TOP 1
                                        1
                                FROM    OrganizationProject
                                WHERE   Name = @name
                                        AND OrganizationID = @organizationid )
                    BEGIN
                        INSERT  INTO OrganizationProject
                                ( OrganizationID, Name, InActive )
                        VALUES  ( @organizationid, @name, @inactive );
                    END;

                FETCH NEXT FROM @cur INTO @projectid, @name, @organizationid,
                    @departmentid, @inactive;

            END;
        CLOSE @cur;   
        DEALLOCATE @cur;

        --Migrate FlowFieldValue records for 'AftercareProjects' 
        --to Project / Organization structure.
        PRINT '- Migrate FlowFieldValue records for "AftercareProjects" to Project / Organization structure';
        SET @cur = CURSOR FORWARD_ONLY FOR
    SELECT FlowFieldValueID, Value FROM FlowFieldValue WHERE FlowFieldID = @aftercareprojects AND LEN(Value) > 0;
        OPEN @cur;
        FETCH NEXT FROM @cur INTO @flowfieldvalueid, @value;

        WHILE @@FETCH_STATUS = 0
            BEGIN
        
                DELETE  FROM ##flowfieldvalue_project;

                INSERT  INTO ##flowfieldvalue_project
                        ( ProjectID
                        )
                        SELECT  Value
                        FROM    dbo.ArrayTable(@value, ',');

                SET @newvalue = NULL;
                SELECT  @newvalue = COALESCE(@newvalue + ',', '')
                        + CONVERT(VARCHAR, OrganizationProject.OrganizationProjectID)
                FROM    ##temp_project_orgdep
                        INNER JOIN ##flowfieldvalue_project ON ##flowfieldvalue_project.ProjectID = ##temp_project_orgdep.ProjectID
                        INNER JOIN OrganizationProject ON OrganizationProject.Name = ##temp_project_orgdep.Name COLLATE DATABASE_DEFAULT
                                                          AND OrganizationProject.OrganizationID = ##temp_project_orgdep.OrganizationID;

                UPDATE  dbo.FlowFieldValue
                SET     Value = @newvalue
                WHERE   FlowFieldValueID = @flowfieldvalueid
                        AND FlowFieldID = @aftercareprojects;

                UPDATE  dbo.MutFlowFieldValue
                SET     Value = @newvalue
                WHERE   FlowFieldValueID = @flowfieldvalueid
                        AND FlowFieldID = @aftercareprojects;
        
                FETCH NEXT FROM @cur INTO @flowfieldvalueid, @value;

            END;
        CLOSE @cur;   
        DEALLOCATE @cur;
    END;
ELSE
    BEGIN

        DECLARE @createdate AS DATETIME;
        SET @createdate = ( SELECT  create_date
                            FROM    sys.tables
                            WHERE   name = @tablename
                          );

        PRINT 'THIS MIGRATION RAN ALREADY ON '
            + CONVERT(VARCHAR(25), @createdate, 120) + '. THIS SCRIPT WILL NOW EXIT.';
    END;

PRINT 'Done.';