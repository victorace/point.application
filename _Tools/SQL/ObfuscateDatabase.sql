-- RUN ObfuscateDatabaseFunctions first to create the functions needed !!!
DISABLE TRIGGER Employee.Employee_InsertUpdate ON Employee
go
DISABLE TRIGGER Client.Client_InsertUpdate ON Client
GO
DISABLE TRIGGER Department.Department_MultipleFields_InsertUpdate ON Department
GO
DISABLE TRIGGER TransferAttachment.TransferAttachment_InsertUpdate ON TransferAttachment
GO
DISABLE TRIGGER Location.Location_MultipleFields_InsertUpdate ON Location
go

Print 'Updating mailaddresses'
go
update Client set Email = 'support@linkassist.nl'
where ISNULL(Email,'') <> ''
go
update Client set ContactPersonEmail = 'support@linkassist.nl'
where ISNULL(ContactPersonEmail,'') <> ''
go
update Client set ContactPersonEmail2 = 'support@linkassist.nl'
where ISNULL(ContactPersonEmail2,'') <> ''
go
update Client set ContactPersonEmail3 = 'support@linkassist.nl'
where ISNULL(ContactPersonEmail3,'') <> ''
go
update Client set AddressGPForZorgmail = '500063492'
where ISNULL(AddressGPForZorgmail,'') <> ''
go
update MutClient set Email = 'support@linkassist.nl'
where ISNULL(Email,'') <> ''
go
update MutClient set ContactPersonEmail = 'support@linkassist.nl'
where ISNULL(ContactPersonEmail,'') <> ''
go
update MutClient set ContactPersonEmail2 = 'support@linkassist.nl'
where ISNULL(ContactPersonEmail2,'') <> ''
go
update MutClient set ContactPersonEmail3 = 'support@linkassist.nl'
where ISNULL(ContactPersonEmail3,'') <> ''
go
update MutClient set AddressGPForZorgmail = '500063492'
where ISNULL(AddressGPForZorgmail,'') <> ''
go 
update Employee set EmailAddress = 'support@linkassist.nl'
where ISNULL(EmailAddress,'') <> ''
go
update MutEmployee set EmailAddress = 'support@linkassist.nl'
where ISNULL(EmailAddress,'') <> ''
GO
update Department set EmailAddressTransferSend = 'support@linkassist.nl', EmailAddress = 'support@linkassist.nl'
where ISNULL(EmailAddress,'') <> ''
GO
update MutDepartment set EmailAddressTransferSend = 'support@linkassist.nl', EmailAddress = 'support@linkassist.nl'
where ISNULL(EmailAddress,'') <> ''
GO
update dbo.Location set EmailAddress = 'support@linkassist.nl'
where ISNULL(EmailAddress,'') <> ''
GO
update dbo.MutLocation set EmailAddress = 'support@linkassist.nl'
where ISNULL(EmailAddress,'') <> ''
GO

print 'Obfuscating client-details'
go

update Client 
   set CivilServiceNumber = dbo.RandomReplace_Constrained(CivilServiceNumber),
       PatientNumber = dbo.RandomReplace_Constrained(PatientNumber),
       Initials = dbo.RandomReplace_Constrained(Initials),
       FirstName = dbo.RandomReplace_Constrained(FirstName),
       LastName = dbo.RandomReplace_Constrained(LastName),
       MaidenName = dbo.RandomReplace_Constrained(MaidenName),
       BirthDate = dbo.RandomDate(BirthDate),
       InsuranceNumber = dbo.RandomReplace_Constrained(InsuranceNumber),
       StreetName = dbo.RandomReplace_Constrained(StreetName),
       Number = dbo.RandomReplace_Constrained(Number),
       PostalCode = dbo.RandomReplace_Constrained(PostalCode),
       City = dbo.RandomReplace_Constrained(City),
       PhoneNumberGeneral = dbo.RandomReplace_Constrained(PhoneNumberGeneral),
       PhoneNumberMobile = dbo.RandomReplace_Constrained(PhoneNumberMobile),
       PhoneNumberWork = dbo.RandomReplace_Constrained(PhoneNumberWork),
       GeneralPractitionerName = dbo.RandomReplace_Constrained(GeneralPractitionerName),
       GeneralPractitionerPhoneNumber = dbo.RandomReplace_Constrained(GeneralPractitionerPhoneNumber),
       PharmacyName = dbo.RandomReplace_Constrained(PharmacyName),
       PharmacyPhoneNumber = dbo.RandomReplace_Constrained(PharmacyPhoneNumber),
       ContactPersonName = dbo.RandomReplace_Constrained(ContactPersonName),
       ContactPersonBirthDate= dbo.RandomDate(ContactPersonBirthDate),
       ContactPersonPhoneNumberGeneral = dbo.RandomReplace_Constrained(ContactPersonPhoneNumberGeneral),
       ContactPersonPhoneNumberMobile = dbo.RandomReplace_Constrained(ContactPersonPhoneNumberMobile),
       ContactPersonPhoneWork = dbo.RandomReplace_Constrained(ContactPersonPhoneWork),
       ContactPersonName2 = dbo.RandomReplace_Constrained(ContactPersonName2),
       ContactPersonBirthDate2= dbo.RandomDate(ContactPersonBirthDate2),
       ContactPersonPhoneNumberGeneral2 = dbo.RandomReplace_Constrained(ContactPersonPhoneNumberGeneral2),
       ContactPersonPhoneNumberMobile2 = dbo.RandomReplace_Constrained(ContactPersonPhoneNumberMobile2),
       ContactPersonPhoneNumberWork2 = dbo.RandomReplace_Constrained(ContactPersonPhoneNumberWork2),
       ContactPersonName3 = dbo.RandomReplace_Constrained(ContactPersonName3),
       ContactPersonBirthDate3= dbo.RandomDate(ContactPersonBirthDate3),
       ContactPersonPhoneNumberGeneral3 = dbo.RandomReplace_Constrained(ContactPersonPhoneNumberGeneral3),
       ContactPersonPhoneNumberMobile3 = dbo.RandomReplace_Constrained(ContactPersonPhoneNumberMobile3),
       ContactPersonPhoneNumberWork3 = dbo.RandomReplace_Constrained(ContactPersonPhoneNumberWork3)
from client
GO

print 'Obfuscating mutclient-details'
go

update MutClient 
   set CivilServiceNumber = dbo.RandomReplace_Constrained(CivilServiceNumber),
       PatientNumber = dbo.RandomReplace_Constrained(PatientNumber),
       Initials = dbo.RandomReplace_Constrained(Initials),
       FirstName = dbo.RandomReplace_Constrained(FirstName),
       LastName = dbo.RandomReplace_Constrained(LastName),
       MaidenName = dbo.RandomReplace_Constrained(MaidenName),
       BirthDate = dbo.RandomDate(BirthDate),
       InsuranceNumber = dbo.RandomReplace_Constrained(InsuranceNumber),
       StreetName = dbo.RandomReplace_Constrained(StreetName),
       Number = dbo.RandomReplace_Constrained(Number),
       PostalCode = dbo.RandomReplace_Constrained(PostalCode),
       City = dbo.RandomReplace_Constrained(City),
       PhoneNumberGeneral = dbo.RandomReplace_Constrained(PhoneNumberGeneral),
       PhoneNumberMobile = dbo.RandomReplace_Constrained(PhoneNumberMobile),
       PhoneNumberWork = dbo.RandomReplace_Constrained(PhoneNumberWork),
       GeneralPractitionerName = dbo.RandomReplace_Constrained(GeneralPractitionerName),
       GeneralPractitionerPhoneNumber = dbo.RandomReplace_Constrained(GeneralPractitionerPhoneNumber),
       PharmacyName = dbo.RandomReplace_Constrained(PharmacyName),
       PharmacyPhoneNumber = dbo.RandomReplace_Constrained(PharmacyPhoneNumber),
       ContactPersonName = dbo.RandomReplace_Constrained(ContactPersonName),
       ContactPersonBirthDate= dbo.RandomDate(ContactPersonBirthDate),
       ContactPersonPhoneNumberGeneral = dbo.RandomReplace_Constrained(ContactPersonPhoneNumberGeneral),
       ContactPersonPhoneNumberMobile = dbo.RandomReplace_Constrained(ContactPersonPhoneNumberMobile),
       ContactPersonPhoneWork = dbo.RandomReplace_Constrained(ContactPersonPhoneWork),
       ContactPersonName2 = dbo.RandomReplace_Constrained(ContactPersonName2),
       ContactPersonBirthDate2= dbo.RandomDate(ContactPersonBirthDate2),
       ContactPersonPhoneNumberGeneral2 = dbo.RandomReplace_Constrained(ContactPersonPhoneNumberGeneral2),
       ContactPersonPhoneNumberMobile2 = dbo.RandomReplace_Constrained(ContactPersonPhoneNumberMobile2),
       ContactPersonPhoneNumberWork2 = dbo.RandomReplace_Constrained(ContactPersonPhoneNumberWork2),
       ContactPersonName3 = dbo.RandomReplace_Constrained(ContactPersonName3),
       ContactPersonBirthDate3= dbo.RandomDate(ContactPersonBirthDate3),
       ContactPersonPhoneNumberGeneral3 = dbo.RandomReplace_Constrained(ContactPersonPhoneNumberGeneral3),
       ContactPersonPhoneNumberMobile3 = dbo.RandomReplace_Constrained(ContactPersonPhoneNumberMobile3),
       ContactPersonPhoneNumberWork3 = dbo.RandomReplace_Constrained(ContactPersonPhoneNumberWork3)
from MutClient
GO



print 'Obfuscating employee-details'
go
update employee
   set 
		FirstName = dbo.RandomReplace_Constrained(FirstName),
		MiddleName = dbo.RandomReplace_Constrained(FirstName),
		LastName = dbo.RandomReplace_Constrained(LastName),
		PhoneNumber = dbo.RandomReplace_Constrained(PhoneNumber),
		ExternID = dbo.RandomReplace_Constrained(ExternID),
		BIG =dbo.RandomReplace_Constrained(BIG)     
from employee
GO

print 'Obfuscating mutemployee-details'
go
update MutEmployee
   set 
		FirstName = dbo.RandomReplace_Constrained(FirstName),
		MiddleName = dbo.RandomReplace_Constrained(FirstName),
		LastName = dbo.RandomReplace_Constrained(LastName),
		PhoneNumber = dbo.RandomReplace_Constrained(PhoneNumber),
		ExternID = dbo.RandomReplace_Constrained(ExternID),
		BIG = dbo.RandomReplace_Constrained(BIG)
from MutEmployee
GO

print 'Obfuscating doctor-details'
go
update doctor
   set 
   Name = dbo.RandomReplace_Constrained(Name),
   SearchName = dbo.RandomReplace_Constrained(SearchName),
   AGB = dbo.RandomReplace_Constrained(AGB),
   BIG = dbo.RandomReplace_Constrained(BIG),
   PhoneNumber = dbo.RandomReplace_Constrained(PhoneNumber)  
from doctor
GO

exec sp_MSforeachtable @command1 = 'truncate table ? print ''truncated ?''',
 @whereand = ' and ( o.name LIKE ''ClientQRY%'' 
                  or o.name LIKE ''ClientQuery%'' 
                  or o.name LIKE ''ClientReceived%'' 
                  or o.name LIKE ''bronovo%'' 
                  or o.name LIKE ''log_indices%'' 
                  or o.name LIKE ''mch_%'' 
                  or o.name LIKE ''olvg_%'' 
                  or o.name LIKE ''rdgg_%'' 
                  or o.name LIKE ''tmp_%''                   
                   )'
GO
                   
exec sp_MSforeachtable @command1 = 'drop table ? print ''dropped ?''',
 @whereand = ' and ( o.name LIKE ''%$%'' 
                   or o.name LIKE ''tmp_%''                   
                   )'
GO

Print 'Updating password'
GO
update aspnet_membership
set Password = 'kKgRd+6ewMsT0PPpU01+unfpwqw=', PasswordSalt = 'FW+rKLvjGoiCaMTsgjou7g=='
GO

Print 'Updating bijlages'
GO
update TransferAttachment set FileData = 'Hello World!'
GO
update MutTransferAttachment set FileData = 'Hello World!'
GO

Print 'Updating memos'
GO
update TransferMemo set MemoContent = 'Was gevuld'
GO

Print 'Updating communicatielog'
GO
update CommunicationLog set Content = 'Was gevuld'
GO

Print 'Update PersonData'
GO
update PersonData SET
Initials = dbo.RandomReplace_Constrained(Initials),
FirstName = dbo.RandomReplace_Constrained(FirstName),
MiddleName = dbo.RandomReplace_Constrained(MiddleName),
LastName = dbo.RandomReplace_Constrained(LastName),
MaidenName = dbo.RandomReplace_Constrained(MaidenName),
PartnerName = dbo.RandomReplace_Constrained(PartnerName),
PartnerMiddleName = dbo.RandomReplace_Constrained(PartnerMiddleName),
PhoneNumberGeneral = dbo.RandomReplace_Constrained(PhoneNumberGeneral),
PhoneNumberMobile = dbo.RandomReplace_Constrained(PhoneNumberMobile),
PhoneNumberWork = dbo.RandomReplace_Constrained(PhoneNumberWork),
Email = 'support@linkassist.nl'
GO

Print 'Nedap certificaten data'
GO
Update Department set 
	NedapCertificate = NULL,
	NedapCertificatePrivateKey = NULL
	where not NedapCertificate is NULL
GO

Print 'Update readmodel'
GO
update FlowInstanceSearchValues 
   set ClientFullname = dbo.clientname_formatted(Client.ClientID),
       ClientCivilServiceNumber = CivilServiceNumber
  from FlowInstanceSearchValues 
  join Transfer on Transfer.TransferID = FlowInstanceSearchValues.TransferID
  join Client on Client.ClientID = Transfer.ClientID
GO

ENABLE TRIGGER Department.Department_MultipleFields_InsertUpdate ON Department
GO
ENABLE TRIGGER Employee.Employee_InsertUpdate ON Employee
GO
ENABLE TRIGGER Client.Client_InsertUpdate ON Client
GO
ENABLE TRIGGER TransferAttachment.TransferAttachment_InsertUpdate ON TransferAttachment
GO
ENABLE TRIGGER Location.Location_MultipleFields_InsertUpdate ON Location
go