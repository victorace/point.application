select 
	aspnet_membership.lastlogindate, 
	aspnet_users.username, 
	aspnet_membership.FailedPasswordAttemptCount,
	employee.employeeid 
from aspnet_membership
inner join aspnet_users on aspnet_users.userid = aspnet_membership.userid
inner join employee on aspnet_users.userid = employee.userid
order by LastLoginDate desc

select aspnet_users.username, flowinstance.* from flowinstance
inner join employee on employee.employeeid = flowinstance.employeeid
inner join aspnet_users on aspnet_users.userid = employee.userid
order by flowinstance.createddate

select * from phaseinstance
where 
	phaseinstance.EmployeeID in (55249, 55250, 18019, 46244, 50005, 55252)
	or phaseinstance.RealizedByEmployeeID in (55249, 55250, 18019, 46244, 50005, 55252)
	or phaseinstance.StartedByEmployeeID in (55249, 55250, 18019, 46244, 50005, 55252)
	or phaseinstance.RequestedByEmployeeID in (55249, 55250, 18019, 46244, 50005, 55252)
order by phaseinstance.PhaseInstanceID desc