/* 

Note: If there is a "copy"-database a.k.a. PointOld a.k.a. Point2015 a.k.a. PointNazorg 
      then uncomment the lines in the script and change the [NameOftheOtherDatabase] 
      Assuming this script is run on the "main"-database (with the "live" aspnet_Users-table)

*/

set nocount on

alter table Employee disable trigger all
-- alter table [NameOfTheOtherDatabase].dbo.Employee disable trigger all


declare @olduserid uniqueidentifier
declare @newuserid uniqueidentifier
declare @loweredusername nvarchar(512)

declare useridcursor cursor for
 select UserId, LoweredUserName 
   from aspnet_Users 

open useridcursor

fetch next from useridcursor
 into @olduserid, @loweredusername

while @@fetch_status = 0
begin
    
    set @newuserid = newid()

    print concat(@loweredusername, ': ')
    print concat(@olduserid, ' => ', @newuserid)



    update aspnet_Users 
       set LoweredUserName = '###' + LoweredUserName
     where UserId = @olduserid

    insert into aspnet_Users
    select ApplicationId,  @newuserid, UserName, @loweredusername, MobileAlias, IsAnonymous, LastActivityDate
      from aspnet_Users
     where UserID = @olduserid

--    update [NameOfTheOtherDatabase].dbo._aspnet_Users 
--       set LoweredUserName = '###' + LoweredUserName
--     where UserId = @olduserid

--    insert into [NameOfTheOtherDatabase].dbo._aspnet_Users
--    select ApplicationId,  @newuserid, UserName, @loweredusername, MobileAlias, IsAnonymous, LastActivityDate
--      from aspnet_Users
--     where UserID = @olduserid

    update Employee set UserId = @newuserid where UserId = @olduserid 

    update MutEmployee set UserId = @newuserid where UserId = @olduserid 

    update aspnet_Membership set UserId = @newuserid where UserId = @olduserid 

    update aspnet_UsersInRoles set UserId = @newuserid where UserId = @olduserid 

--    update [NameOfTheOtherDatabase].dbo.Employee set UserId = @newuserid where UserId = @olduserid 
	
--	  update [NameOfTheOtherDatabase].dbo.MutEmployee set UserId = @newuserid where UserId = @olduserid 

--    update [NameOfTheOtherDatabase].dbo._aspnet_Membership set UserId = @newuserid where UserId = @olduserid 

--    update [NameOfTheOtherDatabase].dbo._aspnet_UsersInRoles set UserId = @newuserid where UserId = @olduserid 

--    delete from [NameOfTheOtherDatabase].dbo._aspnet_Users where UserId = @olduserid

	delete from aspnet_Users where UserId = @olduserid



    fetch next from useridcursor
     into @olduserid, @loweredusername

end

close useridcursor
deallocate useridcursor

alter table Employee enable trigger all
-- alter table [NameOfTheOtherDatabase].dbo.Employee enable trigger all

set nocount off

