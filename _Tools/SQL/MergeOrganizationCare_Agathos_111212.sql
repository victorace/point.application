
--BEGIN TRAN

set nocount on

declare @OrganizationCareName varchar(255)
declare @OrganizationCareNames varchar(2000)
declare @MainRegionID int
declare @MainRegionName varchar(255)

---------------------------------------------------------------------------------------
-- SPECIFY THE ORGANIZATIONCARENAME AND REGION IN THIS SECTION AND RUN THE SCRIPT    --
---------------------------------------------------------------------------------------
set @OrganizationCareName = 'Agathos'                       --< Name of the (duplicate) OrganizationCare
set @OrganizationCareNames = ''                             --< Name(s) of OrganizationCare to merge first
--set @MainRegionID = 7                                     --< Region in which all organization should be
set @MainRegionName = 'Midden-Holland Gouda'                --< or specify the region by Name
---------------------------------------------------------------------------------------


if ( isnull(@MainRegionID,0) <= 0 )
begin
   select @MainRegionID = RegionID from Region where ltrim(rtrim(Region.Name)) = ltrim(rtrim(@MainRegionName))
end

if ( isnull(@MainRegionID,0) <= 0 )
begin
    print 'Geen regio gevonden!'
    return
end

if ( len(@OrganizationCareName) <= 0 )
begin
    print 'Geen @OrganizationCareName opgegeven!'
    return
end


if ( len(@OrganizationCareNames) > 0 )
begin
    update OrganizationCare set OrganizationCare.Name = @OrganizationCareName
     where OrganizationCare.Name in (SELECT [Value] FROM dbo.ArrayTable(@OrganizationCareNames,';'))
     
    print 'Updated OrganizationCare.Name van [' + @OrganizationCareNames +  '] naar [' + @OrganizationCareName + ']'
end

declare @NewOrganizationCareID int
set @NewOrganizationCareID = null

-- Get the new OrganizationCareID which matches the specified name and region
select top 1 @NewOrganizationCareID = OrganizationCare.OrganizationCareID 
  from OrganizationCare 
  join LocationCare on locationcare.OrganizationCareID = OrganizationCare.OrganizationCareID 
  join Organization on Organization.LocationCareID = LocationCare.LocationCareID 
 where OrganizationCare.Name = @OrganizationCareName
   and Organization.RegionID = @MainRegionID 

print 'Nieuwe OrganizationCareID [' + cast(@NewOrganizationCareID as varchar) + ']'

if ( isnull(@NewOrganizationCareID,0) <= 0 )
begin
    print 'Geen OrganizationCare gevonden met Name ['+@OrganizationCareName+'] in RegionID [' + cast(@MainRegionID as varchar) + ']'
    if ( @@Trancount > 0 ) rollback tran
    return
end

if ( isnull(@NewOrganizationCareID,0) > 0 )
begin

    -- Save the region of the organization on location-level
    insert into LocationRegion
    select Location.LocationID, Organization.RegionID
      from OrganizationCare 
      join LocationCare on locationcare.OrganizationCareID = OrganizationCare.OrganizationCareID 
      join Organization on Organization.LocationCareID = LocationCare.LocationCareID 
      join Location on Location.OrganizationID = Organization.OrganizationID 
     where OrganizationCare.Name = @OrganizationCareName
       and Organization.RegionID <> @MainRegionID 
       and Location.LocationID not in ( select LocationRegion.LocationID from LocationRegion )
       
     print 'RegionIDs opgeslagen in LocationRegion'       
       
    -- Disable this unique index, to temporarily allow duplicate LocationCareName+OrganizationCareID
    alter index [IX_LocationCare_NameOrgaID] ON dbo.LocationCare DISABLE

    -- Update all LocationCare, which match the name but mismatch the region, to the new OrganizationCareID,
    update LocationCare
       set OrganizationCareID = @NewOrganizationCareID 
      from OrganizationCare 
      join LocationCare on locationcare.OrganizationCareID = OrganizationCare.OrganizationCareID 
      join Organization on Organization.LocationCareID = LocationCare.LocationCareID 
      --join Location on Location.OrganizationID = Organization.OrganizationID 
     where OrganizationCare.Name = @OrganizationCareName
       and Organization.RegionID <> @MainRegionID 
       
     print 'Verwijzing naar OrganizationCareID van LocationCares aangepast naar nieuwe OrganizationCareID [' + cast(@OrganizationCareName as varchar) + ']' 

    declare @MultipleOrganizationCareID int
    declare @MultipleLocationCareName varchar(255)

    declare LocationCareCursor Cursor for
            select LocationCare.OrganizationCareID, LocationCare.Name 
              from LocationCare 
             group by LocationCare.OrganizationCareID, LocationCare.Name 
            having count(*) > 1

    open LocationCareCursor

    fetch next from LocationCareCursor 
     into @MultipleOrganizationCareID, @MultipleLocationCareName

    -- Loop all the LocationCare having a duplicate Name for the same OrganizationCareID
    while (@@FETCH_STATUS <> -1)
    begin

        -- Update their Organization.RegionID to the specified one
        update Organization 
           set Organization.RegionID = @MainRegionID, 
               Organization.LocationCareID = ( select min(LocationCareID) 
                                                 from LocationCare 
                                                where OrganizationCareID = @MultipleOrganizationCareID
                                                  and Name = @MultipleLocationCareName )
          from Organization 
         where Organization.LocationCareID in ( select LocationCareID 
                                                  from LocationCare 
                                                 where OrganizationCareID = @MultipleOrganizationCareID
                                                   and Name = @MultipleLocationCareName )
        
         print 'Dubbele Organization.RegionID en Organization.LocationCareID bijgewerkt voor LocationCareName [' + @MultipleLocationCareName + ']'
        
        -- Delete the LocationCare(s) not used in the previous update
        delete from LocationCare
         where LocationCareID in ( select LocationCareID 
                                     from LocationCare 
                                    where OrganizationCareID = @MultipleOrganizationCareID
                                      and Name = @MultipleLocationCareName )
           and LocationCareID <> ( select min(LocationCareID) 
                                     from LocationCare 
                                    where OrganizationCareID = @MultipleOrganizationCareID
                                      and Name = @MultipleLocationCareName )

          print 'Dubbele LocationCare verwijderd voor [' + @MultipleLocationCareName + ']'                           
  
        fetch next from LocationCareCursor into @MultipleOrganizationCareID, @MultipleLocationCareName
    end

    close LocationCareCursor
    deallocate LocationCareCursor
    
    
    -- Update the regions of all the organizations
    update Organization
       set Organization.RegionID = @MainRegionID
      from OrganizationCare 
      join LocationCare on LocationCare.OrganizationCareID = OrganizationCare.OrganizationCareID 
      join Organization on Organization.LocationCareID = LocationCare.LocationCareID 
     where OrganizationCare.OrganizationCareID = @NewOrganizationCareID
       and Organization.RegionID <> @MainRegionID     
     
     print 'Updated Organization.RegionID naar [' + cast(@MainRegionID as varchar) + ']'

    -- Enable the unique index again (duplicates are updated and removed in the previous statements)
    alter index [IX_LocationCare_NameOrgaID] ON dbo.LocationCare REBUILD 
    
    -- Remove the OrganizationCare(s) not in use anymore
    delete from OrganizationCare 
     where OrganizationCare.Name = @OrganizationCareName
       and OrganizationCareID not in (select OrganizationCareID from LocationCare )
       
     print 'Dubbele OrganizationCare verwijderd'

end

--ROLLBACK TRAN
--COMMIT TRAN


