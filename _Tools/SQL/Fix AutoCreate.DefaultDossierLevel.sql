update autocs
set autocs.DefaultDossierLevel = 'Organization'
from AutoCreateSet as autocs
    inner join location as l on l.locationid = autocs.LocationID
    inner join organization as o on o.organizationid = l.OrganizationID
where (autocs.DefaultDossierLevel is null or autocs.DefaultDossierLevel = 'None') and o.OrganizationTypeID = 1

update autocs
set autocs.DefaultDossierLevel = 'Location'
from AutoCreateSet as autocs
    inner join location as l on l.locationid = autocs.LocationID
    inner join organization as o on o.organizationid = l.OrganizationID
where (autocs.DefaultDossierLevel is null or autocs.DefaultDossierLevel = 'None') and o.OrganizationTypeID = 2

update autocs
set autocs.DefaultDossierLevel = 'Organization'
from AutoCreateSet as autocs
    inner join location as l on l.locationid = autocs.LocationID
    inner join organization as o on o.organizationid = l.OrganizationID
where (autocs.DefaultDossierLevel is null or autocs.DefaultDossierLevel = 'None') and o.OrganizationTypeID = 12