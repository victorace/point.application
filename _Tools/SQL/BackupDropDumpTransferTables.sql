DECLARE @tablename AS VARCHAR(50);
DECLARE @command AS VARCHAR(255);

IF ( EXISTS ( SELECT    *
              FROM      sysobjects
              WHERE     xtype = 'U'
                        AND name = 'DumpTransferDeletedExport' ) )
    BEGIN

        SET @tablename = CONCAT('DumpTransferDeletedExport',
                                FORMAT(GETDATE(), 'yyyyMMddHHmmss', 'nl-NL'));
 
        IF NOT ( EXISTS ( SELECT    *
                          FROM      sysobjects
                          WHERE     xtype = 'U'
                                    AND name = @tablename ) )
            BEGIN

                SET @command = 'SELECT * INTO ' + @tablename
                    + ' FROM dbo.DumpTransferDeletedExport;';
                EXEC(@command);
            END;


    END;

IF ( EXISTS ( SELECT    *
              FROM      sysobjects
              WHERE     xtype = 'U'
                        AND name = 'DumpTransferExport' ) )
    BEGIN

        SET @tablename = CONCAT('DumpTransferExport',
                                FORMAT(GETDATE(), 'yyyyMMddHHmmss', 'nl-NL'));
 
        IF NOT ( EXISTS ( SELECT    *
                          FROM      sysobjects
                          WHERE     xtype = 'U'
                                    AND name = @tablename ) )
            BEGIN

                SET @command = 'SELECT * INTO ' + @tablename
                    + ' FROM dbo.DumpTransferExport;';
                EXEC(@command);
            END;


    END;

IF ( EXISTS ( SELECT    *
              FROM      sysobjects
              WHERE     xtype = 'U'
                        AND name = 'DumpTransferReport' ) )
    BEGIN

        SET @tablename = CONCAT('DumpTransferReport',
                                FORMAT(GETDATE(), 'yyyyMMddHHmmss', 'nl-NL'));
 
        IF NOT ( EXISTS ( SELECT    *
                          FROM      sysobjects
                          WHERE     xtype = 'U'
                                    AND name = @tablename ) )
            BEGIN

                SET @command = 'SELECT * INTO ' + @tablename
                    + ' FROM dbo.DumpTransferReport;';
                EXEC(@command);
            END;


    END;


DROP TABLE dbo.DumpTransferDeletedExport;
DROP TABLE dbo.DumpTransferExport;
DROP TABLE dbo.DumpTransferReport;

PRINT 'DumpTransferX tables have been dropped.';
PRINT 'Done.';