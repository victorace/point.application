DECLARE @regionid AS INT
DECLARE @flowfieldattributeid AS INT = 2317 -- InfectieIsolatie
IF (EXISTS (SELECT 1 FROM region WHERE name = 'Haarlem-Hoofddorp'))
BEGIN

	SET @regionid = (SELECT TOP 1 RegionID FROM region WHERE name = 'Haarlem-Hoofddorp')

	INSERT INTO dbo.FlowFieldAttributeException (FlowFieldAttributeID, RegionID, OrganizationID, ReadOnly, Visible, Required, SignalOnChange, PhaseDefinitionID, RequiredByFlowFieldID, RequiredByFlowFieldValue)
	  VALUES (@flowfieldattributeid, @regionid, NULL, 0, 1, 1, 0, NULL, NULL, NULL )

END