set nocount on

declare @regions table (RegionID int)
declare @organizations table (OrganizationID int)
declare @locations table (LocationID int)
declare @departments table (DepartmentID int)
declare @userids table (UserID uniqueidentifier)

declare @flowdefinitionid int = 1
declare @rolename varchar(255) = ''
declare @roleid uniqueidentifier = null

-- PBI 12126: VVT-ZH: Aafje aan VVT-ZH koppelen

delete from @organizations 
insert into @organizations 
select OrganizationID 
  from Organization 
  join LocationCare on Organization.LocationCareID = LocationCare.LocationCareID 
  join OrganizationCare on LocationCare.OrganizationCareID = OrganizationCare.OrganizationCareID 
 where OrganizationCare.Name = 'Aafje'
   --and Organization.RegionID in ( select RegionID from @regions )
   and Organization.Inactive = 0

delete from @locations
insert into @locations 
select LocationID
  from Location 
 where Location.OrganizationID in ( select OrganizationID from @organizations )
   and Location.Inactive = 0

delete from @departments
insert into @departments 
select DepartmentID 
  from Department
 where Department.LocationID in ( select LocationID from @locations )
   and Department.Inactive = 0


-- Bovengenoemde VVT's worden gekoppeld aan VVT-ZH flow
insert into OrganizationFlowDefinition 
select OrganizationID, @flowdefinitionid 
  from @organizations 
 where OrganizationID not in ( select OrganizationID from OrganizationFlowDefinition where FlowDefinitionID = @flowdefinitionid )
   and OrganizationID in ( select OrganizationID from Organization )

print 'Done OrganizationFlowDefinition VVT, flowdefinition: ' + cast(@flowdefinitionid as varchar) + ', count: ' + cast(@@rowcount as varchar)


delete from @userids
insert into @userids 
select UserID from Employee 
 where Employee.DepartmentID in ( select DepartmentID from @departments )
   and UserId is not null 
   and InActive = 0
 
select @roleid = RoleID 
  from aspnet_roles 
 where RoleName = 'Employee'

delete from @userids 
 where UserID in ( select UserID from aspnet_UsersInRoles where RoleId = @roleid )

print 'Already have ' + cast(@@rowcount as varchar) + ' employees with "Employee" role'


-- Alle medewerkers van deze VVT's krijgen de PF rol medewerker
insert into aspnet_UsersInRoles
select UserID, @roleid from aspnet_UsersInRoles 
 where UserID in ( select UserID from @userids )
 group by UserID
 having count(UserID) = 1

print 'Done UsersInRoles Employee VVT, count: ' + cast(@@rowcount as varchar)


