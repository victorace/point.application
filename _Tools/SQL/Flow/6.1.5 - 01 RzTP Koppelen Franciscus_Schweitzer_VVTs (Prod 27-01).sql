declare @FlowDefinition int = 3 -- RzTP Flow


insert into OrganizationFlowDefinition 
select OrganizationID, @FlowDefinition 
  from Organization 
 where OrganizationID in ( 600,   -- Sint Franciscus Gasthuis
                           9523 ) -- Albert Schweitzerziekenhuis
   and OrganizationTypeID = 1
   and OrganizationID not in ( select OrganizationID from OrganizationFlowDefinition where FlowDefinitionID = @FlowDefinition )

insert into OrganizationFlowDefinition 
select OrganizationID, @FlowDefinition 
  from Organization
 where RegionID = ( select RegionID from Region where Name = 'Demo' )
   and OrganizationTypeID = 1
   and OrganizationID not in ( select OrganizationID from OrganizationFlowDefinition where FlowDefinitionID = @FlowDefinition )


insert into OrganizationFlowDefinition 
select OrganizationID, @FlowDefinition 
  from vw_OrgaLocaDepCare_Indexed 
 where OrganizationTypeID = 2
   and OrganizationID not in ( select OrganizationID from OrganizationFlowDefinition where FlowDefinitionID = @FlowDefinition ) 


