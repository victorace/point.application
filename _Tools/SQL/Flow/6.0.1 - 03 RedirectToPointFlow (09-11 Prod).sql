-- Set all organization in these two regions. As requested by  AM 12-10-15
update Organization 
   set RedirectToPointFlow = 1
 where RegionID in ( select RegionID from Region where Name in ('Midden-Holland Gouda','Leeuwarden') )
   and InActive = 0

   
   