
DECLARE @organizations table (OrganizationID int)
declare @locations table (LocationID int)
declare @regions table (RegionID int)
DECLARE @formtypes TABLE (FormTypeID INT)
declare @departments table (DepartmentID int)
declare @userids table (UserID uniqueidentifier)

declare @flowdefinitionid int = 2
declare @rolename varchar(255) = ''
declare @roleid uniqueidentifier = NULL

-- ZH's -----------------------------------------------------------------------------------------------------------------------------------------------------------------
delete from @organizations
insert into @organizations SELECT OrganizationID FROM dbo.Organization WHERE (Name = 'MCL' ) AND Inactive = 0;

delete from @locations 
insert into @locations 
select LocationID from Location where Location.OrganizationID in ( select OrganizationID from @organizations ) and Location.Inactive = 0

DELETE FROM @regions
INSERT INTO @regions
SELECT distinct Organization.RegionID from Organization
WHERE Organization.OrganizationID in ( select OrganizationID from @organizations ) and Organization.Inactive = 0


--Voeg de aanvraagformuliertypes toe aan de regio (210 tm 213)
declare @i int
select @i = 210
declare @max int
select @max = 213
while @i <= @max begin
    INSERT INTO dbo.FormTypeRegion (FormTypeID, RegionID)
	SELECT @i, RegionID FROM @regions
	where @i not in ( select FormTypeID from FormTypeRegion where FormTypeRegion.RegionID = [@regions].RegionID )

    set @i = @i + 1
END
print 'Done FormTypeRegion ZH'

delete from @departments 
insert into @departments 
select DepartmentID from Department where Department.LocationID in ( select LocationID from @locations ) and Department.Inactive = 0

INSERT INTO dbo.OrganizationFlowDefinition
select OrganizationID, @flowdefinitionid 
  from @organizations     

insert into OrganizationFlowDefinition 
select OrganizationID, @flowdefinitionid 
  from @organizations 
 where OrganizationID not in ( select OrganizationID from OrganizationFlowDefinition where FlowDefinitionID = @flowdefinitionid )
   and OrganizationID in ( select OrganizationID from Organization )

print 'Done OrganizationFlowDefinition ZH'

delete from @userids
insert into @userids 
select UserID from Employee 
 where Employee.DepartmentID in ( select DepartmentID from @departments )
   and UserId is not null 
   and InActive = 0
 
-- IH 25-09-15: 3- aan alle medewerkers met de rol transferpunt, beheerder transferpunt en medewerker meerdere transferpunten van deze ziekenhuizen de rol Transferpunt medewerker (in Point Flow) toevoegt.
select @roleid = RoleID 
  from aspnet_roles 
 where RoleName = 'TransferEmployee'

insert into aspnet_UsersInRoles
select UserID, @roleid from aspnet_UsersInRoles 
 where UserID in ( select UserID from @userids )
   and RoleID in ( select RoleID from aspnet_Roles where RoleName in ( 'TransferpointEmployee', 'TransferpointAdministrator', 'TransferpointServicesEmployee' ) )
   and UserID not in ( select UserID from aspnet_UsersInRoles where RoleID = @roleid )

print 'Done UsersInRoles TransferEmployee ZH (transferpoints)'
 
