declare @organizations table (OrganizationID int)
declare @locations table (LocationID int)
declare @departments table (DepartmentID int)
declare @userids table (UserID uniqueidentifier)

declare @flowdefinitionid int = 1
declare @rolename varchar(255) = ''
declare @roleid uniqueidentifier = null

-- VVT's -----------------------------------------------------------------------------------------------------------------------------------------------------------------
insert into @organizations 
select Organization.OrganizationID --, Organization.Name
  from LocationRegion  
  join Location on Location.LocationID = LocationRegion.LocationID 
  join Organization on Organization.OrganizationID = Location.OrganizationID 
 where LocationRegion.RegionID = 5
   and Location.Inactive = 0 
   and Organization.Inactive = 0 
   and Organization.OrganizationTypeID = 2 
   and Organization.Name like '%Buurtzorg%'
 order by Organization.Name


insert into @locations 
select Location.LocationID 
  from Location 
 where Location.OrganizationID in ( select OrganizationID from @organizations )
   and Location.Inactive = 0

insert into @departments 
select Department.DepartmentID 
  from Department 
 where Department.LocationID in ( select LocationID from @locations )
   and Department.Inactive = 0


/* De VVT's worden alvast in de OrganizationFlowDefinition-tabel geplaats. Niet in de LocationFlowDefinition-tabel
   Bij livegang van de ZH-VVT flow is de betekenis van deze tabel "omgedraaid" */

insert into OrganizationFlowDefinition 
select OrganizationID, @flowdefinitionid 
  from @organizations 
 where OrganizationID not in ( select OrganizationID from OrganizationFlowDefinition where FlowDefinitionID = @flowdefinitionid )
   and OrganizationID in ( select OrganizationID from Organization )

print 'Done OrganizationFlowDefinition VVT'

delete from @userids

insert into @userids 
select UserID from Employee 
 where Employee.DepartmentID in ( select DepartmentID from @departments )
   and UserId is not null 
   and InActive = 0
 
-- IH 25-09-15: 2- aan alle medewerkers (ongeacht de rol) van alle VVT instellingen in regio A�dam de rol Medewerker (in Point Flow) toevoegt.
select @roleid = RoleID 
  from aspnet_roles 
 where RoleName = 'Employee'

insert into aspnet_UsersInRoles
select UserID, @roleid from aspnet_UsersInRoles 
 where UserID in ( select UserID from @userids )
 group by UserID
 having count(UserID) = 1

print 'Done UsersInRoles Employee VVT'




