declare @regions table (RegionID int)
declare @organizations table (OrganizationID int)
declare @locations table (LocationID int)
declare @departments table (DepartmentID int)
declare @userids table (UserID uniqueidentifier)

declare @flowdefinitionid int = 2
declare @rolename varchar(255) = ''
declare @roleid uniqueidentifier = null


-- Product Backlog Item 10559:ZH-VVT: fase 2 organisatie koppelen aan ZH-VVT flow in Acceptatie------------------------------------------------

-- 1. Koppelen van de volgende ziekenhuizen in ZH-VVT flow: 
delete from @organizations 
insert into @organizations 
select OrganizationID 
  from Organization 
 where name in ( 'Spaarne Gasthuis', 'azMaastricht', 'Diaconessenhuis Meppel', 'Viecuri' )
   and OrganizationTypeID = 1
   and Inactive = 0

delete from @regions
insert into @regions 
select distinct regionid from organization
 where organizationid in ( select organizationid from @organizations )

delete from @locations 
insert into @locations 
select LocationID 
  from Location 
 where Location.OrganizationID in ( select OrganizationID from @organizations ) 
   and Location.Inactive = 0

delete from @departments 
insert into @departments 
select DepartmentID 
  from Department 
 where Department.LocationID in ( select LocationID from @locations ) 
   and Department.Inactive = 0


insert into OrganizationFlowDefinition 
select OrganizationID, @flowdefinitionid 
  from @organizations 
 where OrganizationID not in ( select OrganizationID from OrganizationFlowDefinition where FlowDefinitionID = @flowdefinitionid )
   and OrganizationID in ( select OrganizationID from Organization )

print 'Done OrganizationFlowDefinition ZH'



delete from FormTypeRegion 
 where FormTypeID between 210 and 213
   and RegionID in ( select RegionID from @regions )

insert into FormTypeRegion
select FormTypeID, RegionID 
  from FormType, @regions
  where FormTypeID between 210 and 213

print 'Done FormTypeRegion'





delete from @userids
insert into @userids 
select UserID from Employee 
 where Employee.DepartmentID in ( select DepartmentID from @departments )
   and UserId is not null 
   and InActive = 0
 
-- 2. medewerkers van bovengenoemde ziekenhuizen krijgen PF rol er bij: - alle andere rollen krijgen transferpunt rol

select @roleid = RoleID 
  from aspnet_roles 
 where RoleName = 'TransferEmployee'

insert into aspnet_UsersInRoles
select UserID, @roleid from aspnet_UsersInRoles 
 where UserID in ( select UserID from @userids )
   and RoleID in ( select RoleID from aspnet_Roles where RoleName in ( 'TransferpointEmployee', 'TransferpointAdministrator', 'TransferpointServicesEmployee', 'OrganizationAdministrator', 'PointAdministrator' ) )
   and UserID not in ( select UserID from aspnet_UsersInRoles where RoleID = @roleid )


print 'Done UsersInRoles TransferEmployee ZH (transferpoints)'
 
-- 2. medewerkers van bovengenoemde ziekenhuizen krijgen PF rol er bij: - rol medewerker afdeling en beheerder afdeling krijgen PF rol medewerker
select @roleid = RoleID 
  from aspnet_roles 
 where RoleName = 'Employee'

insert into aspnet_UsersInRoles
select UserID, @roleid from aspnet_UsersInRoles 
 where UserID in ( select UserID from @userids )
   and RoleID in ( select RoleID from aspnet_Roles where RoleName in ( 'HospitalDepartmentEmployee', 'HospitalDepartmentAdministrator' ) )
   and UserID not in ( select UserID from aspnet_UsersInRoles where RoleID = @roleid )

print 'Done UsersInRoles Employee ZH (employee/dep.admin)'


-- 3. medewerkers van bovengenoemde ziekenhuizen loggen standaard in in PF

update Organization 
   set RedirectToPointFlow = 1
 where OrganizationID in ( select OrganizationID from @organizations )

print 'Done RedirectToPointFlow ZH'




-- 4. Alle VVT's in de regio's van bovengenoemde ZH's loggen standaard in in PF

delete from @organizations 
insert into @organizations
select OrganizationID
  from Organization 
 where OrganizationTypeID = 2
   and Inactive = 0
   and RegionID in ( select RegionID from @regions )

-- 5. Punt 4 geldt ook voor VVT's uit andere regio's maar die als afwijkende regio 1 van deze 5 regio's hebben ingesteld
insert into @organizations 
select OrganizationID 
  from LocationRegion 
  join Location on LocationRegion.LocationID = Location.LocationID 
   and RegionID in ( select RegionID from @regions )

update Organization 
   set RedirectToPointFlow = 1
 where OrganizationID in ( select OrganizationID from @organizations )

print 'Done RedirectToPointFlow VVT'

