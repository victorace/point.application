declare @regions table (RegionID int)
declare @organizations table (OrganizationID int)
declare @locations table (LocationID int)
declare @departments table (DepartmentID int)
declare @userids table (UserID uniqueidentifier)

declare @flowdefinitionid int = 1
declare @rolename varchar(255) = ''
declare @roleid uniqueidentifier = null


-- Product Backlog Item 10400:9/11 VVT-ZH: IJsselland ZH en SFG aanzetten plus VVT's ----------------------------------------------------------
-- Op maandag 9 nov dient voor IJsselland ZH en SFG (Sint Franciscus Gasthuis) de VVT-ZH flow te worden geactiveerd. --------------------------

delete from @regions
insert into @regions 
select regionid from region 
 where name in ( 'Rotterdam' )

-- Dat geldt ook voor de volgende VVT's: Laurens (in regio Rotterdam), De Zellingen (in regio Rotterdam), Lelie zorggroep (in regio Rotterdam)
delete from @organizations 
insert into @organizations 
select OrganizationID 
  from Organization 
  join LocationCare on Organization.LocationCareID = LocationCare.LocationCareID 
  join OrganizationCare on LocationCare.OrganizationCareID = OrganizationCare.OrganizationCareID 
 where OrganizationCare.Name in ( 'Laurens', 'De Zellingen', 'Lelie Zorggroep' )
   and Organization.RegionID in ( select RegionID from @regions )
   and Organization.Inactive = 0

-- Note: No locations with an overruling LocationRegion at this moment (03-11-15)
delete from @locations
insert into @locations 
select LocationID
  from Location 
 where Location.OrganizationID in ( select OrganizationID from @organizations )
   and Location.Inactive = 0

delete from @departments
insert into @departments 
select DepartmentID 
  from Department
 where Department.LocationID in ( select LocationID from @locations )
   and Department.Inactive = 0


-- IH 03-11-15: Bovengenoemde VVT's worden gekoppeld aan VVT-ZH flow
insert into OrganizationFlowDefinition 
select OrganizationID, @flowdefinitionid 
  from @organizations 
 where OrganizationID not in ( select OrganizationID from OrganizationFlowDefinition where FlowDefinitionID = @flowdefinitionid )
   and OrganizationID in ( select OrganizationID from Organization )

print 'Done OrganizationFlowDefinition VVT, flowdefinition: ' + cast(@flowdefinitionid as varchar)


delete from @userids
insert into @userids 
select UserID from Employee 
 where Employee.DepartmentID in ( select DepartmentID from @departments )
   and UserId is not null 
   and InActive = 0
 
select @roleid = RoleID 
  from aspnet_roles 
 where RoleName = 'Employee'

delete from @userids 
 where UserID in ( select UserID from aspnet_UsersInRoles where RoleId = @roleid )

-- IH 03-11-15: Alle medewerkers van deze VVT's krijgen de PG rol medewerker
insert into aspnet_UsersInRoles
select UserID, @roleid from aspnet_UsersInRoles 
 where UserID in ( select UserID from @userids )
 group by UserID
 having count(UserID) = 1

print 'Done UsersInRoles Employee VVT'






-- Ziekenhuizen ----------------------------------------------------------------------------------------------------------------------------------------------------------

delete from @organizations
insert into @organizations  
select OrganizationID 
  from Organization
 where Name in ( 'IJsselland Ziekenhuis', 'Sint Franciscus Gasthuis', 'Franciscus, locatie Gasthuis')
   and OrganizationtypeID = 1
   and Inactive = 0


delete from @locations 
insert into @locations 
select LocationID 
  from Location 
 where Location.OrganizationID in ( select OrganizationID from @organizations ) 
   and Location.Inactive = 0

delete from @departments 
insert into @departments 
select DepartmentID 
  from Department 
 where Department.LocationID in ( select LocationID from @locations ) 
   and Department.Inactive = 0


insert into OrganizationFlowDefinition 
select OrganizationID, @flowdefinitionid 
  from @organizations 
 where OrganizationID not in ( select OrganizationID from OrganizationFlowDefinition where FlowDefinitionID = @flowdefinitionid )
   and OrganizationID in ( select OrganizationID from Organization )

print 'Done OrganizationFlowDefinition ZH'

delete from @userids
insert into @userids 
select UserID from Employee 
 where Employee.DepartmentID in ( select DepartmentID from @departments )
   and UserId is not null 
   and InActive = 0
 
-- IH 03-11-15: Alle medewerkers van IJsselland Ziekenhuis en Franciscus, locatie Gasthuis met PO rol Transferpunt medewerker (of hoger of medewerker meerdere TP's) krijgen PF rol transferpunt
select @roleid = RoleID 
  from aspnet_roles 
 where RoleName = 'TransferEmployee'

insert into aspnet_UsersInRoles
select UserID, @roleid from aspnet_UsersInRoles 
 where UserID in ( select UserID from @userids )
   and RoleID in ( select RoleID from aspnet_Roles where RoleName in ( 'TransferpointEmployee', 'TransferpointAdministrator', 'TransferpointServicesEmployee', 'OrganizationAdministrator' ) )
   and UserID not in ( select UserID from aspnet_UsersInRoles where RoleID = @roleid )

print 'Done UsersInRoles TransferEmployee ZH (transferpoints)'
 
-- IH 03-11-15: En alle medewerkers met PO rol Medewerker afdeling (of beheerder afdeling) krijgen PF rol medewerker
select @roleid = RoleID 
  from aspnet_roles 
 where RoleName = 'Employee'

insert into aspnet_UsersInRoles
select UserID, @roleid from aspnet_UsersInRoles 
 where UserID in ( select UserID from @userids )
   and RoleID in ( select RoleID from aspnet_Roles where RoleName in ( 'HospitalDepartmentEmployee', 'HospitalDepartmentAdministrator' ) )
   and UserID not in ( select UserID from aspnet_UsersInRoles where RoleID = @roleid )

print 'Done UsersInRoles Employee ZH (employee/dep.admin)'