declare @regions table (RegionID int)
declare @organizations table (OrganizationID int)
declare @locations table (LocationID int)
declare @departments table (DepartmentID int)
declare @userids table (UserID uniqueidentifier)

declare @flowdefinitionid int = 1 
declare @rolename varchar(255) = ''
declare @roleid uniqueidentifier = null


-- Product Backlog Item 11270: VVT-ZH: Scipt voor start regio Den Haag met VVT-ZH flow ------------------------------------------------

-- 1. Alle ziekenhuizen in regio Den haag worden gekoppeld aan de VVT-ZH flow: 

delete from @organizations 
insert into @organizations 
select OrganizationID 
  from Organization 
 where RegionID = (select RegionID from Region where Name = 'Den Haag')
   and OrganizationTypeID = 1
   and Inactive = 0

delete from @regions
insert into @regions 
select distinct regionid from organization
 where organizationid in ( select organizationid from @organizations )

delete from @locations 
insert into @locations 
select LocationID 
  from Location 
 where Location.OrganizationID in ( select OrganizationID from @organizations ) 
   and Location.Inactive = 0

delete from @departments 
insert into @departments 
select DepartmentID 
  from Department 
 where Department.LocationID in ( select LocationID from @locations ) 
   and Department.Inactive = 0


insert into OrganizationFlowDefinition 
select OrganizationID, @flowdefinitionid 
  from @organizations 
 where OrganizationID not in ( select OrganizationID from OrganizationFlowDefinition where FlowDefinitionID = @flowdefinitionid )
   and OrganizationID in ( select OrganizationID from Organization )

print 'Done OrganizationFlowDefinition ZH'

-- 19-01-16: Besproken met IH, niet standaard RedirectToPointFlow voor deze orga's

delete from @userids
insert into @userids 
select UserID from Employee 
 where Employee.DepartmentID in ( select DepartmentID from @departments )
   and UserId is not null 
   and InActive = 0
 
-- 3. Alle accounts met de rol transferpunt, beheerder transferpunt en medewerker meerdere transferpunten van alle ziekenhuizen in regio Den Haag de rol Transferpunt medewerker (in Point Flow) krijgen.
-- 4. Alle accounts met de rol beheerder organisatie en regio beheerder van alle ziekenhuizen in regio Den Haag de rol Transferpunt medewerker (in Point Flow) krijgen.

select @roleid = RoleID 
  from aspnet_roles 
 where RoleName = 'TransferEmployee'

insert into aspnet_UsersInRoles
select UserID, @roleid from aspnet_UsersInRoles 
 where UserID in ( select UserID from @userids )
   and RoleID in ( select RoleID from aspnet_Roles where RoleName in ( 'TransferpointEmployee', 'TransferpointAdministrator', 'TransferpointServicesEmployee' , 'OrganizationAdministrator', 'PointAdministrator' ) )
   and UserID not in ( select UserID from aspnet_UsersInRoles where RoleID = @roleid )


print 'Done UsersInRoles TransferEmployee ZH (transferpoints)'
 
-- 5. Alle accounts met de rol medewerker afdeling en beheerder afdeling van alle ziekenhuizen in regio Den Haag de rol Medewerker (in Point Flow) krijgen.
select @roleid = RoleID 
  from aspnet_roles 
 where RoleName = 'Employee'

insert into aspnet_UsersInRoles
select UserID, @roleid from aspnet_UsersInRoles 
 where UserID in ( select UserID from @userids )
   and RoleID in ( select RoleID from aspnet_Roles where RoleName in ( 'HospitalDepartmentEmployee', 'HospitalDepartmentAdministrator' ) )
   and UserID not in ( select UserID from aspnet_UsersInRoles where RoleID = @roleid )

print 'Done UsersInRoles Employee ZH (employee/dep.admin)'



-- 2. Alle VVT's in regio Den haag worden gekoppeld aan de VVT-ZH flow

delete from @organizations 
insert into @organizations
select OrganizationID
  from Organization 
 where OrganizationTypeID = 2
   and Inactive = 0
   and RegionID in ( select RegionID from @regions )

-- ... ook voor VVT's uit andere regio's maar die als afwijkende regio deze regio's hebben ingesteld
insert into @organizations 
select OrganizationID 
  from LocationRegion 
  join Location on LocationRegion.LocationID = Location.LocationID 
   and RegionID in ( select RegionID from @regions )

-- ... maar dan weer niet voor VVT's in deze regio die een afwijkende regio erbuiten hebben, dus die halen we weer uit het lijstje
delete from @organizations 
 where OrganizationID in (
    select OrganizationID 
      from LocationRegion 
      join Location on LocationRegion.LocationID = Location.LocationID 
       and RegionID not in ( select RegionID from @regions ) )


insert into OrganizationFlowDefinition 
select OrganizationID, @flowdefinitionid 
  from @organizations 
 where OrganizationID not in ( select OrganizationID from OrganizationFlowDefinition where FlowDefinitionID = @flowdefinitionid )
   and OrganizationID in ( select OrganizationID from Organization )

print 'Done OrganizationFlowDefinition VVT'

-- 19-01-16: Besproken met IH, niet standaard RedirectToPointFlow voor deze orga's

-- 19-01-16: Nagenoeg alle VVT-medewerkers hebben al een dubbele rol.
