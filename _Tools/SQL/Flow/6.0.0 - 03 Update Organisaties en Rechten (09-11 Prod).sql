-- Clean assignments of the "admin"-roles --
delete from aspnet_UsersInRoles 
 where RoleId in ( select RoleId from aspnet_Roles where RoleName in ( 'AdministratorOrganization', 'AdministratorDepartment' ) )


DECLARE @organizations table (OrganizationID int)
declare @locations table (LocationID int)
declare @regions table (RegionID int)
DECLARE @formtypes TABLE (FormTypeID INT)
declare @departments table (DepartmentID int)
declare @userids table (UserID uniqueidentifier)

declare @flowdefinitionid int = 2
declare @rolename varchar(255) = ''
declare @roleid uniqueidentifier = NULL

-- VVT's -----------------------------------------------------------------------------------------------------------------------------------------------------------------
insert into @organizations 
select Organization.OrganizationID --, Organization.Name
  from dbo.Organization
   WHERE Organization.Inactive = 0 AND Organization.OrganizationTypeID = 2 
 order by Organization.Name

insert into @locations 
select Location.LocationID 
  from Location 
 where Location.OrganizationID in ( select OrganizationID from @organizations )
   and Location.Inactive = 0

insert into @departments 
select Department.DepartmentID 
  from Department 
 where Department.LocationID in ( select LocationID from @locations )
   and Department.Inactive = 0

/* De VVT's worden alvast in de OrganizationFlowDefinition-tabel geplaats. Niet in de LocationFlowDefinition-tabel
   Bij livegang van de ZH-VVT flow is de betekenis van deze tabel "omgedraaid" */

insert into OrganizationFlowDefinition 
select OrganizationID, @flowdefinitionid 
  from @organizations 
 where OrganizationID not in ( select OrganizationID from OrganizationFlowDefinition where FlowDefinitionID = @flowdefinitionid )
   and OrganizationID in ( select OrganizationID from Organization )

print 'Done OrganizationFlowDefinition VVT'

delete from @userids
insert into @userids 
select UserID from Employee 
 where Employee.DepartmentID in ( select DepartmentID from @departments )
   and UserId is not null 
   and InActive = 0
 
-- IH 25-09-15: 2- aan alle medewerkers (ongeacht de rol) van alle VVT instellingen de rol Medewerker (in Point Flow) toevoegt.
select @roleid = RoleID 
  from aspnet_roles 
 where RoleName = 'Employee'

delete from @userids 
 where UserID in ( select UserID from aspnet_UsersInRoles where RoleId = @roleid )

insert into aspnet_UsersInRoles
select UserID, @roleid from aspnet_UsersInRoles 
 where UserID in ( select UserID from @userids )
 group by UserID
 having count(UserID) = 1

print 'Done UsersInRoles Employee VVT'


-- ZH's -----------------------------------------------------------------------------------------------------------------------------------------------------------------
delete from @organizations
insert into @organizations SELECT OrganizationID FROM dbo.Organization WHERE (Name LIKE '%Medisch Centrum Leeuwarden%' OR name LIKE '%Groene Hart Ziekenhuis%') AND Inactive = 0;

delete from @locations 
insert into @locations 
select LocationID from Location where Location.OrganizationID in ( select OrganizationID from @organizations ) and Location.Inactive = 0

DELETE FROM @regions
INSERT INTO @regions
SELECT distinct Organization.RegionID from Organization
WHERE Organization.OrganizationID in ( select OrganizationID from @organizations ) and Organization.Inactive = 0


--Voeg de aanvraagformuliertypes toe aan de regio (210 tm 213)
declare @i int
select @i = 210
declare @max int
select @max = 213
while @i <= @max begin
    INSERT INTO dbo.FormTypeRegion (FormTypeID, RegionID)
	SELECT @i, RegionID FROM @regions
	where @i not in ( select FormTypeID from FormTypeRegion where FormTypeRegion.RegionID = [@regions].RegionID )

    set @i = @i + 1
END
print 'Done FormTypeRegion ZH'

delete from @departments 
insert into @departments 
select DepartmentID from Department where Department.LocationID in ( select LocationID from @locations ) and Department.Inactive = 0

INSERT INTO dbo.OrganizationFlowDefinition
select OrganizationID, @flowdefinitionid 
  from @organizations     

insert into OrganizationFlowDefinition 
select OrganizationID, @flowdefinitionid 
  from @organizations 
 where OrganizationID not in ( select OrganizationID from OrganizationFlowDefinition where FlowDefinitionID = @flowdefinitionid )
   and OrganizationID in ( select OrganizationID from Organization )

print 'Done OrganizationFlowDefinition ZH'

delete from @userids
insert into @userids 
select UserID from Employee 
 where Employee.DepartmentID in ( select DepartmentID from @departments )
   and UserId is not null 
   and InActive = 0
 
-- IH 25-09-15: 3- aan alle medewerkers met de rol transferpunt, beheerder transferpunt en medewerker meerdere transferpunten van deze ziekenhuizen de rol Transferpunt medewerker (in Point Flow) toevoegt.
select @roleid = RoleID 
  from aspnet_roles 
 where RoleName = 'TransferEmployee'

insert into aspnet_UsersInRoles
select UserID, @roleid from aspnet_UsersInRoles 
 where UserID in ( select UserID from @userids )
   and RoleID in ( select RoleID from aspnet_Roles where RoleName in ( 'TransferpointEmployee', 'TransferpointAdministrator', 'TransferpointServicesEmployee' ) )
   and UserID not in ( select UserID from aspnet_UsersInRoles where RoleID = @roleid )

print 'Done UsersInRoles TransferEmployee ZH (transferpoints)'
 
-- IH 25-09-15: 4- aan alle medewerkers met de rol beheerder organisatie en regio beheerder van deze ziekenhuizen de rol Transferpunt medewerker (in Point Flow) toevoegt.
select @roleid = RoleID 
  from aspnet_roles 
 where RoleName = 'TransferEmployee'

insert into aspnet_UsersInRoles
select UserID, @roleid from aspnet_UsersInRoles 
 where UserID in ( select UserID from @userids )
   and RoleID in ( select RoleID from aspnet_Roles where RoleName in ( 'OrganizationAdministrator', 'PointAdministrator' ) )
   and UserID not in ( select UserID from aspnet_UsersInRoles where RoleID = @roleid )

print 'Done UsersInRoles TransferEmployee ZH (admins)'

-- IH 25-09-15: 5- aan alle medewerkers met de rol medewerker afdeling en beheerder afdeling van deze ziekenhuizen de rol Medewerker (in Point Flow) toevoegt.
select @roleid = RoleID 
  from aspnet_roles 
 where RoleName = 'Employee'

insert into aspnet_UsersInRoles
select UserID, @roleid from aspnet_UsersInRoles 
 where UserID in ( select UserID from @userids )
   and RoleID in ( select RoleID from aspnet_Roles where RoleName in ( 'HospitalDepartmentEmployee', 'HospitalDepartmentAdministrator' ) )
   and UserID not in ( select UserID from aspnet_UsersInRoles where RoleID = @roleid )

print 'Done UsersInRoles Employee ZH (employee/dep.admin)'