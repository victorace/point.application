declare @organizations table (OrganizationID int)
declare @locations table (LocationID int)
declare @regions table (RegionID int)
declare @formtypes TABLE (FormTypeID INT)
declare @departments table (DepartmentID int)
declare @userids table (UserID uniqueidentifier)

declare @flowdefinitionid int = 2
declare @rolename varchar(255) = ''
declare @roleid uniqueidentifier = NULL

-- Product Backlog Item 10252:VVT's instellen voor default login in POINT Flow ----------------------------------------------------------

delete from @regions

insert into @regions 
select regionid from region 
 where name in ( 'Midden-Holland Gouda', 'Leeuwarden' )


-- Ismil 27-10: Ook medewerkers van VVT instellingen in andere regio's dan Leeuwarden en Midden-Holland Gouda waarvan de locatie als afwijkende regio 1 van deze 2 regio's heeft ingesteld moeten ook standaard in PF inloggen.

-- Get all organizations with an "overruling" location in one of the regions
delete from @organizations 
insert into @organizations 
select Organization.OrganizationID 
  from Organization 
  join Location on Location.OrganizationID = Organization.OrganizationID 
  join LocationRegion on LocationRegion.LocationID = Location.LocationID and LocationRegion.RegionID in ( select RegionID from @regions )
 where Organization.Inactive = 0
   and Organization.OrganizationTypeID = 2
   and Location.Inactive = 0


update Organization
   set RedirectToPointFlow = 1
 where OrganizationID in ( select OrganizationID from @organizations )

print 'Done setting redirect for organizations with location in one of the regions'


-- Ismil 27-10: Medewerkers van VVT instellingen in regio Leeuwarden en Midden-Holland Gouda waarvan de locatie een afwijkende regio heeft ingesteld (anders dan deze 2 regio's) moeten standaard in PO inloggen.

-- Get all organizations in one of the regions, with an "overruling" location _not_ in one of the regions
delete from @organizations 
insert into @organizations 
select Organization.OrganizationID
  from Organization 
  join Location on Location.OrganizationID = Organization.OrganizationID 
  join LocationRegion on LocationRegion.LocationID = Location.LocationID and LocationRegion.RegionID not in ( select RegionID from @regions )
 where Organization.RegionID in ( select RegionID from @regions )
   and Organization.Inactive = 0
   and Organization.OrganizationTypeID = 2
   and Location.Inactive = 0


update Organization
   set RedirectToPointFlow = 0
 where OrganizationID in ( select OrganizationID from @organizations )

print 'Done "un"setting redirect for organizations in the regions with a location out of these regions'




-- Ismil 27-10: Alle organisaties van type CIZ mogen gekoppeld worden aan ZH-VVT flow. En de accounts die daar onder hangen mogen de rol medewerker krijgen in PF.

delete from @organizations
insert into @organizations 
select Organization.OrganizationID 
  from Organization 
  join Location on Location.OrganizationID = Organization.OrganizationID 
  join Department on Department.LocationID = Location.LocationID 
 where OrganizationTypeID = 6 --CIZ
   and Organization.Inactive = 0 and Location.Inactive = 0 and Department.Inactive = 0


delete from @departments
insert into @departments 
select Department.DepartmentID
  from Organization 
  join Location on Location.OrganizationID = Organization.OrganizationID 
  join Department on Department.LocationID = Location.LocationID 
 where OrganizationTypeID = 6 --CIZ
   and Organization.Inactive = 0 and Location.Inactive = 0 and Department.Inactive = 0




insert into OrganizationFlowDefinition 
select OrganizationID, @flowdefinitionid 
  from @organizations 
 where OrganizationID not in ( select OrganizationID from OrganizationFlowDefinition where FlowDefinitionID = @flowdefinitionid )
   and OrganizationID in ( select OrganizationID from Organization )

print 'Done adding CIZ-organizations to flowdefinition' + cast(@flowdefinitionid as varchar)




delete from @userids
insert into @userids 
select UserID from Employee 
 where Employee.DepartmentID in ( select DepartmentID from @departments )
   and UserId is not null 
   and InActive = 0
 
select @roleid = RoleID 
  from aspnet_roles 
 where RoleName = 'Employee'

delete from @userids 
 where UserID in ( select UserID from aspnet_UsersInRoles where RoleId = @roleid )

insert into aspnet_UsersInRoles
select UserID, @roleid from aspnet_UsersInRoles 
 where UserID in ( select UserID from @userids )
 group by UserID
 having count(UserID) = 1

print 'Done adding role Employee to the employees'


-- Ismil 27-10 Ziekenhuizen in de regio demo (waarschijnlijk maar 1) mogen aan ZH-VVT gekoppeld worden. En de accounts die daar onder hangen mogen de rol transferpunt krijgen in PF.

declare @regionid int = ( select top 1 RegionID from Region where Name = 'Demo' )

delete from @organizations
insert into @organizations 
select OrganizationID 
  from Organization 
 where Organization.RegionID = @regionid
   and Organization.Inactive = 0
   and Organization.OrganizationTypeID = 1

delete from @locations 
insert into @locations 
select LocationID 
  from Location 
 where Location.OrganizationID in ( select OrganizationID from @organizations ) 
   and Location.Inactive = 0

delete from @departments 
insert into @departments 
select DepartmentID 
  from Department 
 where Department.LocationID in ( select LocationID from @locations ) 
   and Department.Inactive = 0



delete from FormTypeRegion 
 where RegionID = @regionid 
   and FormTypeID >= 210 and FormTypeID <= 213

insert into FormTypeRegion values ( 210, @regionid )
insert into FormTypeRegion values ( 211, @regionid )
insert into FormTypeRegion values ( 212, @regionid )
insert into FormTypeRegion values ( 213, @regionid )

print 'Done deleting/inserting FormTypes 210-213 for "Demo"-region'

insert into OrganizationFlowDefinition 
select OrganizationID, @flowdefinitionid 
  from @organizations 
 where OrganizationID not in ( select OrganizationID from OrganizationFlowDefinition where FlowDefinitionID = @flowdefinitionid )
   and OrganizationID in ( select OrganizationID from Organization )

print 'Done adding DEMO ziekenhuizen to flowdefinition' + cast(@flowdefinitionid as varchar)

delete from @userids
insert into @userids 
select UserID from Employee 
 where Employee.DepartmentID in ( select DepartmentID from @departments )
   and UserId is not null 
   and InActive = 0

select @roleid = RoleID 
  from aspnet_roles 
 where RoleName = 'TransferEmployee'

insert into aspnet_UsersInRoles
select UserID, @roleid from aspnet_UsersInRoles 
 where UserID in ( select UserID from @userids )
   and UserID not in ( select UserID from aspnet_UsersInRoles where RoleID = @roleid )
 group by UserID
having count(UserID) = 1

print 'Done adding role TransferEmployee to all employees DEMO ziekenhuizen'
