DECLARE @organizationid AS INT = 9128;
DECLARE @organizationname AS VARCHAR(255) = 'unknown';

DECLARE @flowfieldattributeid AS INT;

DECLARE @requestformzhvvt AS INT = 202;

DECLARE @readonly AS BIT = 0;
DECLARE @visible AS BIT = 1;
DECLARE @required AS BIT = 1;
DECLARE @signonchange AS BIT = 1;

DECLARE @PatientOntvingZorgVoorOpnameJaNee AS INT = 190;
DECLARE @AfterCareCategory AS INT = 1051;

SET @organizationname = (SELECT TOP 1 Name FROM dbo.Organization WHERE OrganizationID = @organizationid);

SET @flowfieldattributeid = (SELECT FlowFieldAttributeID FROM dbo.FlowFieldAttribute WHERE FlowFieldID = @PatientOntvingZorgVoorOpnameJaNee AND FormTypeID = @requestformzhvvt);
IF @flowfieldattributeid IS NOT NULL
BEGIN
	IF NOT EXISTS (SELECT 1 FROM dbo.FlowFieldAttributeException WHERE OrganizationID = @organizationid AND FlowFieldAttributeID = @flowfieldattributeid)
	BEGIN
		INSERT INTO dbo.FlowFieldAttributeException (FlowFieldAttributeID, RegionID, OrganizationID, ReadOnly, Visible, Required, SignalOnChange, PhaseDefinitionID, RequiredByFlowFieldID, RequiredByFlowFieldValue)
		VALUES (@flowfieldattributeid, NULL, @organizationid, @readonly, @visible, @required, @signonchange, NULL, 275, '{"Values": ["210", "211", "213"]}');
		PRINT 'Setting PatientOntvingZorgVoorOpnameJaNee as Required for Organization: ' + @organizationname;
	END;
	ELSE
		PRINT 'PatientOntvingZorgVoorOpnameJaNee already has a FlowFieldAttributeException for Organization: ' + @organizationname;
END;

SET @flowfieldattributeid = (SELECT FlowFieldAttributeID FROM dbo.FlowFieldAttribute WHERE FlowFieldID = @AfterCareCategory AND FormTypeID = @requestformzhvvt);
IF @flowfieldattributeid IS NOT NULL
BEGIN
	IF NOT EXISTS (SELECT 1 FROM dbo.FlowFieldAttributeException WHERE OrganizationID = @organizationid AND FlowFieldAttributeID = @flowfieldattributeid)
	BEGIN
		INSERT INTO dbo.FlowFieldAttributeException (FlowFieldAttributeID, RegionID, OrganizationID, ReadOnly, Visible, Required, SignalOnChange, PhaseDefinitionID, RequiredByFlowFieldID, RequiredByFlowFieldValue)
		VALUES (@flowfieldattributeid, NULL, @organizationid, @readonly, @visible, @required, @signonchange, NULL, 275, '{"Values": ["210", "211", "213"]}');
		PRINT 'Setting AfterCareCategory as Required for Organization: ' + @organizationname;
	END;
	ELSE
		PRINT 'AfterCareCategory already has a FlowFieldAttributeException for Organization: ' + @organizationname;
END;

PRINT 'Done.';