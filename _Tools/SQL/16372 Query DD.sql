SELECT 
	firvd.FlowInstanceID,
	fi.TransferID,
	fiddsv.FrequencyID,
	firv.TransferCreatedDate AS CreationDate,
	r.Name AS SendingRegion,
	o.Name AS SendingOrganization,
	l.Name AS SendingLocation,
	d.Name AS SendingDepartment,
	firvd.ReceivingOrganizationName AS ReceivingOrganization,
	firvd.ReceivingLocationName AS ReceivingLocation,
	firvd.ReceivingDepartmentName AS ReceivingDepartment,
	firv.ClientBirthDate AS DateOfBirth,
	firv.ClientGender AS Gender,
	fiddsv.FrequencyName AS FrequencyName,
	CASE fiddsv.FrequencyType
		WHEN 1 THEN 'msvt'
		WHEN 2 THEN 'cva'
		WHEN 3 THEN 'algemeen'
		ELSE 'geen'
	END AS FrequencyType, 
	fiddsv.FrequencyStartDate,
	fiddsv.FrequencyEndDate,
	CASE fiddsv.FrequencyScopeType
		WHEN 18 THEN 'actief binnen de periode'
		WHEN 19 THEN 'actief buiten de periode'
		WHEN 20 THEN 'gesloten'
		ELSE 'geen'
	END AS FrequencyScopeType,
	(SELECT COUNT(TransferMemoID) FROM dbo.FrequencyTransferMemo WITH (NOLOCK) WHERE FrequencyID = fiddsv.FrequencyID) AS MemoCount,
	(SELECT COUNT(AttachmentID) FROM dbo.FrequencyTransferAttachment WITH (NOLOCK) WHERE FrequencyID = fiddsv.FrequencyID) AS AttachmentCount
FROM dbo.FlowInstanceReportValuesDump AS firvd WITH (NOLOCK)
INNER JOIN dbo.FlowInstanceReportValues AS firv WITH (NOLOCK) ON firvd.FlowInstanceID = firv.FlowInstanceID
INNER JOIN dbo.FlowInstanceDoorlopendDossierSearchValues AS fiddsv WITH (NOLOCK) ON firvd.FlowInstanceID = fiddsv.FlowInstanceID
INNER JOIN dbo.FlowInstance AS fi WITH (NOLOCK) ON firvd.FlowInstanceID = fi.FlowInstanceID
INNER JOIN dbo.Organization AS o WITH (NOLOCK)  ON fi.StartedByOrganizationID = o.OrganizationID
INNER JOIN dbo.Region AS r WITH (NOLOCK)  ON o.RegionID = r.RegionID
INNER JOIN dbo.Department AS d WITH (NOLOCK)  ON fi.StartedByDepartmentID = d.DepartmentID
INNER JOIN dbo.Location AS l WITH (NOLOCK)  ON d.LocationID = l.LocationID
WHERE 
	firvd.DoorlDossierCount > 0  
	--firvd.ReceivingOrganizationName = 'Laurens' AND
    --firvd.ReceivingRegionName = 'Rotterdam' AND
    --o.Name = 'IJsselland Ziekenhuis' AND
    --fi.CreatedDate >= '20180101' AND fi.CreatedDate <= '20181231'
ORDER BY fi.CreatedDate