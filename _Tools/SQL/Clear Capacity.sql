truncate table departmentcapacity
update dbo.Department
	set CapacityDepartment = null, 
	CapacityDepartmentNext = null, 
	CapacityDepartment3 = null, 
	CapacityDepartment4 = null,
	CapacityDepartmentHomeCare = null,
	CapacityDepartmentNextHomeCare = null,
	CapacityDepartment3HomeCare = null,
	CapacityDepartment4HomeCare = null,
	CapacityDepartmentDate = null,
	CapacityDepartmentNextDate = null,
	CapacityDepartment3Date = null,
	CapacityDepartment4Date = null,
	AdjustmentCapacityDate = null

