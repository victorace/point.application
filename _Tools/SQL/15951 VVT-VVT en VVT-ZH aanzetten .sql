 --public enum Participation
 --   {
 --       [Description("Geen")]
 --       None = 0,
 --       [Description("Alleen Fax/Mail")]
 --       FaxMail = 1,
 --       [Description("Actief")]
 --       Active = 2
 --   }
    -- public enum FlowDirection
    --{
    --    [Description("Geen")] None = -1,
    --    [Description("Verzender")] Sending = 0,
    --    [Description("Ontvanger")] Receiving = 1,
    --    [Description("Beide")] Any = 2
    --}


	  --public enum FlowDefinitionID
   -- {
   --     [Description("VVTZH")]
   --     VVT_ZH = 1,
   --     [Description("ZHVVT")]
   --     ZH_VVT = 2,
   --     [Description("RzTP")]
   --     RzTP = 3,
   --     [Description("CVA")]
   --     CVA = 4,
   --     [Description("ZHZH")]
   --     ZH_ZH = 5,
   --     [Description("HAVVT")]
   --     HA_VVT = 6,
   --     [Description("VVTVVT")]
   --     VVT_VVT = 7
   -- }

  --select count( location.locationid ) as ZH_Locations
  --from location 
  --left join organization on
  --location.organizationid = organization.organizationid 
  --Where OrganizationTypeID = 1 and location.Inactive = 0 and organization.Inactive = 0

  --select Count( location.locationid ) ZH_Location_With_VVT_ZH 
  --from location 
  --left join organization on
  --location.organizationid = organization.organizationid 
  --left join FlowDefinitionParticipation
  --on location.locationid=  FlowDefinitionParticipation.locationid
  --Where OrganizationTypeID = 1 and location.Inactive = 0 and organization.Inactive = 0
  --and  FlowDefinitionParticipation.FlowDefinitionID = 1


 -- 	select 
	--count(locationID) As ZH_LOCATION_TO_ADD_FOR_VVT_ZH
	--from location 
 -- where inactive = 0 and  OrganizationID in 
 -- (	select organizationid from organization where organizationtypeid = 1 and Inactive =0) and  
 -- LocationID not in 
 --(
 --  --ZH waar VVT-ZH Well aan is:
 -- select location.locationid 
 -- from location 
 -- left join organization on
 -- location.organizationid = organization.organizationid 
 -- left join FlowDefinitionParticipation
 -- on location.locationid=  FlowDefinitionParticipation.locationid
 -- Where OrganizationTypeID = 1 and location.Inactive = 0 and organization.Inactive = 0
 -- and  FlowDefinitionParticipation.FlowDefinitionID = 1
 -- )


 --FlowDefinitionID
 Declare @VVT_ZH int = 1;
 Declare @ZH_VVT int = 2;
 Declare @VVT_VVT int = 7;
 Declare @HA_VVT int = 6;

 --FlowDirection
 Declare @Verzender int = 0;
 Declare @Ontvanger int = 1;
 Declare @Beide int = 2;

--Participation
 Declare @FaxMail int = 1;
 Declare @Active int = 2;

 Declare @VVT_OrganizationType int = 2;
 Declare @ZH_OrganizationType int = 1;

 -- part 1: Voor alle VVT organisaties die ZH-VVT al aan (ontvanger) heeft staan, de flow VVT-VVT aanzetten op beide-actief
  Insert into FlowDefinitionParticipation
  (
	FlowDefinitionID ,
	LocationID , 
	DepartmentID ,
	Participation, 
	FlowDirection , 
	ModifiedTimeStamp 
  )
  select 
	@VVT_VVT  ,
	VVTLocations.LocationID , 
	null As DepartmentID ,	
	@Active As Participation, 
	@Beide As FlowDirection ,  -- VVT-VVT -> actief aan zetten 
	null As ModifiedTimeStamp 
  from
  (
	select FlowDefinitionParticipation.LocationID  from FlowDefinitionParticipation 
	Left join Location on location.locationid = FlowDefinitionParticipation.locationid
	left join organization on location.organizationid =Organization.organizationid

	where 
	organization.OrganizationTypeID = @VVT_OrganizationType and 
	FlowDefinitionParticipation.FlowDefinitionID = @ZH_VVT  and 
	FlowDirection = @Ontvanger and 
	Location.Inactive = 0 and 
	Organization.Inactive = 0 
  ) as VVTLocations
  -- If This record is not already exists
  Where not exists 	(
	select FlowDefinitionParticipationID from FlowDefinitionParticipation where
	LocationID = VVTLocations.locationid and 
	FlowDefinitionID = @VVT_VVT and departmentID is null  and Participation = @Beide and FlowDirection = @Active
  )

 -- part 2: Voor alle VVT organisaties die ZH-VVT al aan (ontvanger) heeft staan, de flow VVT-ZH aanzetten als Verzender-Actief
   Insert into FlowDefinitionParticipation
  (
	  FlowDefinitionID ,
	  LocationID , 
	  DepartmentID ,
	  Participation, 
	  FlowDirection , 
	  ModifiedTimeStamp 
  )
  select 
	  @VVT_ZH as FlowDefinitionID ,
	  VVTLocations.LocationID , 
	  null As DepartmentID ,
	  @Active As Participation, 
	  @Verzender As FlowDirection ,  -- VVT-ZH -> actief aan zetten 
	  null As ModifiedTimeStamp 
  from
  (
	  select FlowDefinitionParticipation.LocationID  from FlowDefinitionParticipation 
	  Left join Location on location.locationid = FlowDefinitionParticipation.locationid
	  left join organization on location.organizationid =Organization.organizationid
	  where organization.OrganizationTypeID = @VVT_OrganizationType and 
	  FlowDefinitionParticipation.FlowDefinitionID = @ZH_VVT and 
	  FlowDirection = @Ontvanger and Location.Inactive = 0 and Organization.Inactive = 0 
  ) as VVTLocations
  -- If This record is not already exists
  Where not exists (
	  select FlowDefinitionParticipationID from FlowDefinitionParticipation where
	  LocationID = VVTLocations.locationid and 
	  FlowDefinitionID = @VVT_ZH and departmentID is null  and Participation = @Active and FlowDirection = @Verzender
  )

--  part 3: Voor alle ZH die VVT-ZH NIET aan hebben staan, de flow op Ontvanger-faxmail zetten
  Insert into FlowDefinitionParticipation
  (
	  FlowDefinitionID ,
	  LocationID , 
	  DepartmentID ,
	  Participation, 
	  FlowDirection , 
	  ModifiedTimeStamp 
  )
  select 
	  @VVT_ZH as FlowDefinitionID ,
	  LocationID , 
	  null As DepartmentID ,
	  @FaxMail As Participation, 
	  @Ontvanger As FlowDirection , 
	  null As ModifiedTimeStamp 
  from location 
  where inactive = 0 and  
  OrganizationID in 
	(
		select organizationid from organization where organizationtypeid = @ZH_OrganizationType and Inactive =0
	) and  
  LocationID not in 
	 (
	   --alle ZH's die VVT-ZH Well aan is:
	  select location.locationid 
	  from location 
	  left join organization on
	  location.organizationid = organization.organizationid 
	  left join FlowDefinitionParticipation
	  on location.locationid=  FlowDefinitionParticipation.locationid
	  Where OrganizationTypeID = @ZH_OrganizationType and location.Inactive = 0 and organization.Inactive = 0
	  and  FlowDefinitionParticipation.FlowDefinitionID = @VVT_ZH
	 )

 -- part 4: Voor alle VVT organisaties die ZH-VVT al aan (ontvanger) heeft staan, de flow HA-VVT aanzetten op ontvanger-actief
   Insert into FlowDefinitionParticipation
  (
	  FlowDefinitionID ,
	  LocationID , 
	  DepartmentID ,
	  Participation, 
	  FlowDirection , 
	  ModifiedTimeStamp 
  )
  select 
	  @HA_VVT as FlowDefinitionID ,
	  VVTLocations.LocationID , 
	  null As DepartmentID ,
	  @Active As Participation, 
	  @Ontvanger As FlowDirection ,  -- Ha-VVT-> actief aan zetten 
	  null As ModifiedTimeStamp 
  from
  (
	  select FlowDefinitionParticipation.LocationID  from FlowDefinitionParticipation 
	  Left join Location on location.locationid = FlowDefinitionParticipation.locationid
	  left join organization on location.organizationid =Organization.organizationid
	  where organization.OrganizationTypeID = @VVT_OrganizationType and 
	  FlowDefinitionParticipation.FlowDefinitionID = @ZH_VVT and 
	  FlowDirection = @Ontvanger and Location.Inactive = 0 and Organization.Inactive = 0 
  ) as VVTLocations
  -- If This record is not already exists
  Where not exists (
	  select FlowDefinitionParticipationID from FlowDefinitionParticipation where
	  LocationID = VVTLocations.locationid and 
	  FlowDefinitionID = @HA_VVT and departmentID is null  and Participation = @Active and FlowDirection = @Ontvanger
  )