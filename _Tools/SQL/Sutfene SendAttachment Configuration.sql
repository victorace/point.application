declare @sutfene as int;
if exists (select top 1 1 from dbo.Organization where Name = 'Sutfene' and OrganizationTypeID = 2 and Inactive = 0)
begin
	set @sutfene = (select top 1 OrganizationID from dbo.Organization where Name = 'Sutfene' and OrganizationTypeID = 2 and Inactive = 0);
	insert into dbo.SendAttachment (SendDestinationType, OrganizationID, AttachmentTypeID, Allowed)
	select 1, @sutfene, AttachmentTypeID, 0 from dbo.SendAttachment where OrganizationID is null;
end;