-- Detail per login:

select LoginDateTime,
       LoginOrganizationName,
       case OrganizationTypeID when 1 then 'ZH' when 2 then 'VVT' else 'Ander' end as OrganizationType,
       (select case when charindex('MSIE 6', LoginInfo) > 0 then 'IE6'
                    when charindex('MSIE 7', LoginInfo) > 0 then 
                        case when charindex('Trident/4.0', LoginInfo) > 0 then 'IE8  (in IE7 comp.mode)'
                             when charindex('Trident/5.0', LoginInfo) > 0 then 'IE9  (in IE7 comp.mode)'
                             when charindex('Trident/6.0', LoginInfo) > 0 then 'IE10 (in IE7 comp.mode)'
                             when charindex('Trident/7.0', LoginInfo) > 0 then 'IE11 (in IE7 comp.mode)' else 'IE7 ()' end
                    when charindex('MSIE 8', LoginInfo) > 0 then 
                        case when charindex('Trident/4.0', LoginInfo) > 0 then 'IE8  ()'
                             when charindex('Trident/5.0', LoginInfo) > 0 then 'IE9  (in IE8 comp.mode)'
                             when charindex('Trident/6.0', LoginInfo) > 0 then 'IE10 (in IE8 comp.mode)'
                             when charindex('Trident/7.0', LoginInfo) > 0 then 'IE11 (in IE8 comp.mode)' else 'IE8 ()' end
                    when charindex('MSIE 9', LoginInfo) > 0 then 
                        case when charindex('Trident/4.0', LoginInfo) > 0 then 'IE8  (in IE9 comp.mode)'
                             when charindex('Trident/5.0', LoginInfo) > 0 then 'IE9  ()'
                             when charindex('Trident/6.0', LoginInfo) > 0 then 'IE10 (in IE9 comp.mode)'
                             when charindex('Trident/7.0', LoginInfo) > 0 then 'IE11 (in IE9 comp.mode)' else 'IE9 ()' end
                    when charindex('MSIE 10', LoginInfo) > 0 then 
                        case when charindex('Trident/4.0', LoginInfo) > 0 then 'IE8  (in IE10 comp.mode)'
                             when charindex('Trident/5.0', LoginInfo) > 0 then 'IE9  (in IE10 comp.mode)'
                             when charindex('Trident/6.0', LoginInfo) > 0 then 'IE10 ()'
                             when charindex('Trident/7.0', LoginInfo) > 0 then 'IE11 (in IE10 comp.mode)' else 'IE10 ()' end
                    when charindex('rv:11.0', LoginInfo) > 0 then 
                        case when charindex('Trident/4.0', LoginInfo) > 0 then 'IE8  (in IE11 comp.mode)'
                             when charindex('Trident/5.0', LoginInfo) > 0 then 'IE9  (in IE11 comp.mode)'
                             when charindex('Trident/6.0', LoginInfo) > 0 then 'IE10 (in IE11 comp.mode)'
                             when charindex('Trident/7.0', LoginInfo) > 0 then 'IE11 ()' else 'IE11 ()' end
                    when charindex('Chrome', LoginInfo) > 0 then 'Chrome'                        
                    when charindex('Firefox', LoginInfo) > 0 then 'Firefox'
                    when charindex('Mobile Safari', LoginInfo) > 0 then 'Mobile Safari'
                    when charindex('iPad;', LoginInfo) > 0 then 'iPad Safar1'
                    when charindex('Safari', LoginInfo) > 0 then 'Safari'
                    when charindex('Opera', LoginInfo) > 0 then 'Opera'
                    else substring(LoginInfo,1,255) end ) as Browser,
       LoginInfo
  from LoginHistory with(nolock)   
  left outer join Organization with(nolock) on Organization.Name = LoginHistory.LoginOrganizationName  
 where YEAR(logindatetime) = 2016
   --and Month(LoginDateTime) >= 1
   and LoginFailed = 0
   and OrganizationTypeId = 1 
 order by LoginDateTime


-- Gegroepeerd per orga en browser
 
select LoginOrganizationName, Browser, count(*) cnt from (
--select Browser, count(*) cnt from (

select LoginDateTime,
       LoginOrganizationName,
       case OrganizationTypeID when 1 then 'ZH' when 2 then 'VVT' else 'Ander' end as OrganizationType,
       (select case when charindex('MSIE 6', LoginInfo) > 0 then 'IE6'
                    when charindex('MSIE 7', LoginInfo) > 0 then 
                        case when charindex('Trident/4.0', LoginInfo) > 0 then 'IE8  (in IE7 comp.mode)'
                             when charindex('Trident/5.0', LoginInfo) > 0 then 'IE9  (in IE7 comp.mode)'
                             when charindex('Trident/6.0', LoginInfo) > 0 then 'IE10 (in IE7 comp.mode)'
                             when charindex('Trident/7.0', LoginInfo) > 0 then 'IE11 (in IE7 comp.mode)' else 'IE7 ()' end
                    when charindex('MSIE 8', LoginInfo) > 0 then 
                        case when charindex('Trident/4.0', LoginInfo) > 0 then 'IE8  ()'
                             when charindex('Trident/5.0', LoginInfo) > 0 then 'IE9  (in IE8 comp.mode)'
                             when charindex('Trident/6.0', LoginInfo) > 0 then 'IE10 (in IE8 comp.mode)'
                             when charindex('Trident/7.0', LoginInfo) > 0 then 'IE11 (in IE8 comp.mode)' else 'IE8 ()' end
                    when charindex('MSIE 9', LoginInfo) > 0 then 
                        case when charindex('Trident/4.0', LoginInfo) > 0 then 'IE8  (in IE9 comp.mode)'
                             when charindex('Trident/5.0', LoginInfo) > 0 then 'IE9  ()'
                             when charindex('Trident/6.0', LoginInfo) > 0 then 'IE10 (in IE9 comp.mode)'
                             when charindex('Trident/7.0', LoginInfo) > 0 then 'IE11 (in IE9 comp.mode)' else 'IE9 ()' end
                    when charindex('MSIE 10', LoginInfo) > 0 then 
                        case when charindex('Trident/4.0', LoginInfo) > 0 then 'IE8  (in IE10 comp.mode)'
                             when charindex('Trident/5.0', LoginInfo) > 0 then 'IE9  (in IE10 comp.mode)'
                             when charindex('Trident/6.0', LoginInfo) > 0 then 'IE10 ()'
                             when charindex('Trident/7.0', LoginInfo) > 0 then 'IE11 (in IE10 comp.mode)' else 'IE10 ()' end
                    when charindex('rv:11.0', LoginInfo) > 0 then 
                        case when charindex('Trident/4.0', LoginInfo) > 0 then 'IE8  (in IE11 comp.mode)'
                             when charindex('Trident/5.0', LoginInfo) > 0 then 'IE9  (in IE11 comp.mode)'
                             when charindex('Trident/6.0', LoginInfo) > 0 then 'IE10 (in IE11 comp.mode)'
                             when charindex('Trident/7.0', LoginInfo) > 0 then 'IE11 ()' else 'IE11 ()' end
                    when charindex('Chrome', LoginInfo) > 0 then 'Chrome'                        
                    when charindex('Firefox', LoginInfo) > 0 then 'Firefox'
                    when charindex('Mobile Safari', LoginInfo) > 0 then 'Mobile Safari'
                    when charindex('iPad;', LoginInfo) > 0 then 'iPad Safar1'
                    when charindex('Safari', LoginInfo) > 0 then 'Safari'
                    when charindex('Opera', LoginInfo) > 0 then 'Opera'
                    else substring(LoginInfo,1,255) end ) as Browser,
       LoginInfo
  from LoginHistory with(nolock)   
  left outer join Organization with(nolock) on Organization.Name = LoginHistory.LoginOrganizationName  
 where YEAR(logindatetime) = 2016
   --and Month(LoginDateTime) >= 10
   and LoginFailed = 0
   and OrganizationTypeId = 1 
 ) as tbl
 --where tbl.Browser like '%IE8%'
 --where tbl.LoginOrganizationName like '%meppel%'
 group by tbl.LoginOrganizationName, tbl.Browser
 --group by tbl.Browser 
 order by cnt desc 
*/


/*

select replace(isnull(Organization.Name, '[oud] ' + LoginOrganizationName),';',''),
	   isnull(Organization.OrganizationTypeID, 0),
       replace(LoginUserName,';',''),
	   LoginDateTime,
	   LoginResolution,
       LoginIP,
	   LoginInfo
  from LoginHistory with(nolock)   
  left outer join Organization with(nolock) on Organization.Name = LoginHistory.LoginOrganizationName  
 where YEAR(logindatetime) = 2017
 and LoginFailed = 0
 order by LoginDateTime 

*/
