select 
	case PL.Url
		when '/Report/ZHStuurinfo' 					then 'ZH Stuurinformatie'
		when '/Report/VVTWhereIsPatient' 			then 'Waar is patient gebleven'
		when '/Report/VVTAantallenOntvangen' 		then 'Aantallen ontvangen'
		when '/Report/ZHDischargeOverdue' 			then 'Gewenste ontslagdatum gepasseerd'
		when '/Report/ZHHerkomstUitstroom' 			then 'ZH Herkomst Uitstroom'
		when '/Report/AfdelingBeheer' 				then 'Afdeling'
		when '/Report/VVTControlInformation' 		then 'VVT Stuurinformatie'
		when '/Report/CSVDump' 						then 'Genereer POINT dump in Excel'
		when '/Report/VVTTotals' 					then 'VVT Totalen'
		when '/Report/VVTNotDesired' 				then 'VVT Uitval'
		when '/Report/CVADICA' 						then 'DICA'
		when '/Report/VVTCapacity' 					then 'Capaciteit'
		when '/Report/VVTKPIInBehandeling' 			then 'KPI In Behandeling nemen VVT'
		when '/Report/VVTKPITerugmelding' 			then 'KPI Terugmelding van VVT'
		when '/Report/ZHKPIInBehandeling' 			then 'KPI In Behandeling nemen VVT'
		when '/Report/ZHKPITerugmelding' 			then 'KPI Terugmelding van VVT'
		when '/Report/ZHTrendGrafieken' 			then 'Trend Ziekenhuizen'
		when '/Report/ZHTrendGrafieken' 			then 'Aantallen'
		when '/Report/ZHTrendGrafieken' 			then 'Uitstroom VVT'
		when '/Report/ZHTrendGrafieken' 			then 'Uitstroom naar zorgtype'
		when '/Report/ZHTrendGrafieken' 			then 'Verkeerde Bed Dagen (VKB)'
		when '/Report/ZHTrendGrafieken' 			then 'Gewenste ontslagdatum behaald'
		when '/Report/ZHTrendGrafieken' 			then 'Procestijden'
		when '/Report/ZHTrendGrafieken' 			then 'Dump'
		when '/Report/VVTTrendGrafieken' 			then 'Trend VVT'
		when '/Report/VVTTrendGrafieken' 			then 'Aantallen'
		when '/Report/VVTTrendGrafieken' 			then 'Instroom'
		when '/Report/VVTTrendGrafieken' 			then 'Gewenste ontslagdatum behaald'
		when '/Report/VVTTrendGrafieken' 			then 'Procestijden'
		when '/Report/VVTTrendGrafieken' 			then 'Uitstroom naar zorgtype'
		when '/Report/VVTTrendGrafieken'	 		then 'Verkeerde Bed Dagen (VKB)'
		when '/Report/VVTTrendGrafieken' 			then 'Dump'
		when '/Report/NursingTransferCompleteness' 	then 'Compleetheid verpleegkundige overdracht'
	else 										'geen'
end as Url, 
region.name as Region,
organization.name as Organization,
location.name as Location,
department.name as Department,
COUNT(PL.LogReadID) AS ExecutionCount,
MAX(PL.Timestamp) AS LastExecutionDate 
from PointLog_LogRead as PL with (nolock)
inner join aspnet_Roles with (nolock) on aspnet_Roles.RoleId = PL.RoleID
inner join employee with (nolock) on employee.employeeid = PL.EmployeeID
inner join department with (nolock) on department.departmentid = employee.departmentid
inner join location with (nolock) on location.locationid = department.locationid
inner join organization with (nolock) on organization.organizationid = location.organizationid
inner join region with (nolock) on region.regionid = organization.regionid
where PL.Url like '%/Report/%' and PL.Url not in ('/Report/Result', '/Report/List', '/Report/LoadReport')
group by PL.Url, region.name, organization.name, location.name, department.name
order by ExecutionCount, LastExecutionDate