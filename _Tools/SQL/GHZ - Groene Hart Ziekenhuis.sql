DECLARE @DateBegin DATETIME 
DECLARE @DateEnd DATETIME
DECLARE @organizationID INT

--Script voor Groene Hart Ziekenhuis, op aanvraag uitvoeren

SET @DateBegin = '20131101'
SET @DateEnd = '20131201'
SET @organizationID = '788'

SELECT     

			Transfer.TransferID                                                 AS 'Transfer',
			Transfer.CreatedDate                                                AS 'Aanmaak datum transfer',
			Client.CivilServiceNumber											AS 'BSN nummer',
			dbo.clientname_formatted(Client.ClientID)                           AS 'Naam',
			Client.StreetName 													AS 'Straat',
			client.Number														AS 'Nummer',
			Client.PostalCode													AS 'Postcode',			
			Client.City                                                         AS 'Plaats',			 
			Client.BirthDate													AS 'Geboortedatum',
			Client.Gender                                                       AS 'Geslacht',
			dbo.fieldValue_date(transferid, 'CareBeginDate') AS 'Scherm Aanvullende gegevens door transferpunt: Patient moet zorg ontvangen vanaf'
			
FROM         Transfer 
LEFT OUTER JOIN Client ON Transfer.ClientID = Client.ClientID 
LEFT OUTER JOIN Department ON Transfer.DepartmentID = Department.DepartmentID 
INNER JOIN Location ON Department.LocationID = Location.LocationID 
INNER JOIN Organization ON Location.OrganizationID = Organization.OrganizationID 
INNER JOIN Region ON Organization.RegionID = Region.RegionID 
LEFT JOIN Specialism ON Specialism.SpecialismID=Department.SpecialismID
LEFT JOIN Aftercareclassification ON AftercareClassification.AftercareClassificationID=Transfer.RequestAftercareClassificationID

                                      
WHERE      (Transfer.CreatedDate Between @DateBegin And @DateEnd) 
			AND	Organization.OrganizationID = @OrganizationID
			and Phase10 = 2

ORDER BY Transfer.TransferID