SET NOCOUNT on

DECLARE @BarthelIndexDatum AS INT = 1201;
DECLARE @BarthelIndexResultaat AS INT = 1202;
DECLARE @BarthelIndexVoorOpname AS INT = 1203;
DECLARE @DOSDatrum AS INT = 1231;
DECLARE @DOSResultaat AS INT = 1232;
DECLARE @DOSVoorOpname AS INT = 1233;
DECLARE @FACDatrum AS INT = 1234;
DECLARE @FACResultaat AS INT = 1235;
DECLARE @FACVoorOpname AS INT = 1236;
DECLARE @KatzADLDatrum AS INT = 1268;
DECLARE @KatzADLResultaat AS INT = 1269;
DECLARE @KatzADLVoorOpname AS INT = 1270;
DECLARE @SnaqDatrum AS INT = 1306;
DECLARE @SnaqResultaat AS INT = 1307;
DECLARE @SnaqVoorOpname AS INT = 1308;
DECLARE @WOLCDatrum AS INT = 1320;
DECLARE @WOLCResultaat AS INT = 1321;
DECLARE @WOLCVoorOpname AS INT = 1322;
DECLARE @PRPPDatrum AS INT = 1294;
DECLARE @PRPPResultaat AS INT = 1295;
DECLARE @PRPPVoorOpname AS INT = 1296;
DECLARE @WatersliktestDatrum AS INT = 1317;
DECLARE @WatersliktestResultaat AS INT = 1318;
DECLARE @WatersliktestVoorOpname AS INT = 1319;
DECLARE @SANDatrum AS INT = 1303;
DECLARE @SANResultaat AS INT = 1304;
DECLARE @SANVoorOpname AS INT = 1305;
DECLARE @MIDatrum AS INT = 1272;
DECLARE @MIResultaat AS INT = 1273;
DECLARE @MIVoorOpname AS INT = 1274;
DECLARE @MMSEDatrum AS INT = 1275;
DECLARE @MMSEResultaat AS INT = 1276;
DECLARE @MMSEVoorOpname AS INT = 1277;
DECLARE @AndersSoortMeting AS INT = 1200;

DECLARE @transferid AS INT
DECLARE @flowinstanceid AS INT
DECLARE @name AS VARCHAR(MAX)
DECLARE @flow AS VARCHAR(50)
DECLARE @datum AS DATETIME
DECLARE @empty AS BIT = 1

PRINT 'TransferID,Organization,Flow,Date,Empty'

DECLARE @c as CURSOR;
SET @c = CURSOR FOR
	SELECT Transfer.TransferID, FlowInstance.FlowInstanceID, Organization.Name, FlowDefinition.Name, FormSetVersion.CreateDate
	FROM dbo.FormSetVersion WITH (NOLOCK)
	INNER JOIN Transfer WITH (NOLOCK) ON FormSetVersion.TransferID = Transfer.TransferID
	INNER JOIN dbo.FlowInstance WITH (NOLOCK) ON Transfer.TransferID = FlowInstance.TransferID
	INNER JOIN dbo.Organization WITH (NOLOCK) ON FlowInstance.StartedByOrganizationID = Organization.OrganizationID 
	INNER JOIN dbo.FlowDefinition WITH (NOLOCK) ON FlowInstance.FlowDefinitionID = FlowDefinition.FlowDefinitionID
	WHERE FormSetVersion.FormTypeID = 216 AND FormSetVersion.Deleted = 0 AND FormSetVersion.DeletedDate IS NULL
	--AND Transfer.TransferID = 722137 -- 751510
	AND Organization.OrganizationTypeID = 1
	AND FlowInstance.FlowDefinitionID IN (2,4)
	AND FlowInstance.CreatedDate >= '20180101'
	AND FlowInstance.Deleted = 0
OPEN @c;
FETCH NEXT FROM @c INTO @transferid, @flowinstanceid, @name, @flow, @datum;
WHILE @@FETCH_STATUS = 0
BEGIN

	IF dbo.flowFieldidValue_char_version(@flowinstanceid, @BarthelIndexDatum) <> '' SET @empty = 0
	IF @empty = 1 AND dbo.flowFieldidValue_char_version(@flowinstanceid, @BarthelIndexResultaat) <> '' SET @empty = 0
	IF @empty = 1 AND dbo.flowFieldidValue_char_version(@flowinstanceid, @BarthelIndexVoorOpname) <> '' SET @empty = 0
	IF @empty = 1 AND dbo.flowFieldidValue_char_version(@flowinstanceid, @DOSDatrum) <> '' SET @empty = 0
	IF @empty = 1 AND dbo.flowFieldidValue_char_version(@flowinstanceid, @DOSResultaat) <> '' SET @empty = 0
	IF @empty = 1 AND dbo.flowFieldidValue_char_version(@flowinstanceid, @DOSVoorOpname) <> '' SET @empty = 0
	IF @empty = 1 AND dbo.flowFieldidValue_char_version(@flowinstanceid, @FACDatrum) <> '' SET @empty = 0
	IF @empty = 1 AND dbo.flowFieldidValue_char_version(@flowinstanceid, @FACResultaat) <> '' SET @empty = 0
	IF @empty = 1 AND dbo.flowFieldidValue_char_version(@flowinstanceid, @FACVoorOpname) <> '' SET @empty = 0
	IF @empty = 1 AND dbo.flowFieldidValue_char_version(@flowinstanceid, @KatzADLDatrum) <> '' SET @empty = 0
	IF @empty = 1 AND dbo.flowFieldidValue_char_version(@flowinstanceid, @KatzADLResultaat) <> '' SET @empty = 0
	IF @empty = 1 AND dbo.flowFieldidValue_char_version(@flowinstanceid, @KatzADLVoorOpname) <> '' SET @empty = 0
	IF @empty = 1 AND dbo.flowFieldidValue_char_version(@flowinstanceid, @SnaqDatrum) <> '' SET @empty = 0
	IF @empty = 1 AND dbo.flowFieldidValue_char_version(@flowinstanceid, @SnaqResultaat) <> '' SET @empty = 0
	IF @empty = 1 AND dbo.flowFieldidValue_char_version(@flowinstanceid, @SnaqVoorOpname) <> '' SET @empty = 0
	IF @empty = 1 AND dbo.flowFieldidValue_char_version(@flowinstanceid, @WOLCDatrum) <> '' SET @empty = 0
	IF @empty = 1 AND dbo.flowFieldidValue_char_version(@flowinstanceid, @WOLCResultaat) <> '' SET @empty = 0
	IF @empty = 1 AND dbo.flowFieldidValue_char_version(@flowinstanceid, @WOLCVoorOpname) <> '' SET @empty = 0
	IF @empty = 1 AND dbo.flowFieldidValue_char_version(@flowinstanceid, @PRPPDatrum) <> '' SET @empty = 0
	IF @empty = 1 AND dbo.flowFieldidValue_char_version(@flowinstanceid, @PRPPResultaat) <> '' SET @empty = 0
	IF @empty = 1 AND dbo.flowFieldidValue_char_version(@flowinstanceid, @PRPPVoorOpname) <> '' SET @empty = 0
	IF @empty = 1 AND dbo.flowFieldidValue_char_version(@flowinstanceid, @WatersliktestDatrum) <> '' SET @empty = 0
	IF @empty = 1 AND dbo.flowFieldidValue_char_version(@flowinstanceid, @WatersliktestResultaat) <> '' SET @empty = 0
	IF @empty = 1 AND dbo.flowFieldidValue_char_version(@flowinstanceid, @WatersliktestVoorOpname) <> '' SET @empty = 0
	IF @empty = 1 AND dbo.flowFieldidValue_char_version(@flowinstanceid, @SANDatrum) <> '' SET @empty = 0
	IF @empty = 1 AND dbo.flowFieldidValue_char_version(@flowinstanceid, @SANResultaat) <> '' SET @empty = 0
	IF @empty = 1 AND dbo.flowFieldidValue_char_version(@flowinstanceid, @SANVoorOpname) <> '' SET @empty = 0
	IF @empty = 1 AND dbo.flowFieldidValue_char_version(@flowinstanceid, @MIDatrum) <> '' SET @empty = 0
	IF @empty = 1 AND dbo.flowFieldidValue_char_version(@flowinstanceid, @MIResultaat) <> '' SET @empty = 0
	IF @empty = 1 AND dbo.flowFieldidValue_char_version(@flowinstanceid, @MIVoorOpname) <> '' SET @empty = 0
	IF @empty = 1 AND dbo.flowFieldidValue_char_version(@flowinstanceid, @MMSEDatrum) <> '' SET @empty = 0
	IF @empty = 1 AND dbo.flowFieldidValue_char_version(@flowinstanceid, @MMSEResultaat) <> '' SET @empty = 0
	IF @empty = 1 AND dbo.flowFieldidValue_char_version(@flowinstanceid, @MMSEVoorOpname) <> '' SET @empty = 0
	IF @empty = 1 AND dbo.flowFieldidValue_char_version(@flowinstanceid, @AndersSoortMeting) <> '' SET @empty = 0

	PRINT CAST(@transferid AS varchar(50)) + ',' + @name + ',' + @flow + ',' + CAST(CAST(@datum AS DATE) AS VARCHAR(255)) + ',' + CAST(@empty AS VARCHAR(1))

	SET @empty = 1

	FETCH NEXT FROM @c INTO @transferid, @flowinstanceid, @name, @flow, @datum;	
END
CLOSE @c