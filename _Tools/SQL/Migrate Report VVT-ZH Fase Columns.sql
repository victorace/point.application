
declare @flowdefinitionid as int = 1;
declare @TPInBeh as int = 4;

update Report
	set 
		Report.[TP Gerealiseerde zorgaanbieder] = Orgview.FullLocationName,
		Report.[TP Gerealiseerde zorgaanbieder: Gebruikt Point] = Orgview.PointParticipator,
		Report.[TP Gerealiseerde zorgaanbieder: Heeft toegang tot Point] = cast(null as bit),
		Report.[TP Gerealiseerde zorgaanbieder, Postcode] = Orgview.PostalCode,
		Report.[TP Gerealiseerde zorgaanbieder, RegionID] = Orgview.RegionID,
		Report.[TP Gerealiseerde zorgaanbieder, Regio naam] = Orgview.RegionName,
		Report.[TP Gerealiseerde zorgaanbieder, OrganizationID] = Orgview.OrganizationID,
		Report.[TP Gerealiseerde zorgaanbieder, Organizatie naam] = Orgview.OrganizationName,
		Report.[TP Gerealiseerde zorgaanbieder, LocationID] = Orgview.LocationID,
		Report.[TP Gerealiseerde zorgaanbieder, Locatie gebied] = Orgview.Area,
		Report.[TP Gerealiseerde zorgaanbieder, Locatie naam] = Orgview.LocationName,
		Report.[TP Gerealiseerde zorgaanbieder, DepartmentID] = Orgview.DepartmentID,
		Report.[TP Gerealiseerde zorgaanbieder, Afdeling naam] = Orgview.DepartmentName,

		Report.[Fase VVT besluit] = Report.[Fase TP in beh. nemen],
		Report.[Fase VVT besluit Datum] = Report.[Fase TP in beh. nemen Datum],
		Report.[Fase VVT besluit Einddatum] = dbo.phaseInstanceLastRealizedDateByPhaseDefinitionID(Report.[FlowInstanceID], @TPInBeh),

		Report.[Fase Afsluiten] = Report.[Fase Afsluiten VVT-ZH],
		Report.[Fase Afsluiten Datum] = Report.[Fase Afsluiten VVT-ZH Datum]

from dbo.DumpTransferReport as Report
inner join dbo.vw_OrganizationStructure as Orgview on Orgview.DepartmentID = Report.[Ontvanger DepartmentID]
where Report.FlowDefinitionID = @flowdefinitionid;