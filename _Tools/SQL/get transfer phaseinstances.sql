DECLARE @transferid AS INT = 765089;
DECLARE @flowinstanceid AS INT;

SELECT  @flowinstanceid = FlowInstanceID
FROM    FlowInstance
WHERE   TransferID = @transferid;

SELECT  dbo.FlowInstance.FlowInstanceID ,
		dbo.FlowInstance.TransferID,
		dbo.PhaseDefinition.PhaseDefinitionID,
		dbo.PhaseDefinition.Phase,
		dbo.PhaseDefinition.PhaseName,
		dbo.PhaseInstance.PhaseInstanceID,
		dbo.PhaseInstance.Status,
		dbo.PhaseInstance.Cancelled,
		dbo.FormSetVersion.FormSetVersionID,
		dbo.FormSetVersion.Deleted
FROM    dbo.PhaseInstance
        INNER JOIN dbo.FlowInstance ON FlowInstance.FlowInstanceID = PhaseInstance.FlowInstanceID
        INNER JOIN dbo.Organization AS StartedByOrganization ON StartedByOrganization.OrganizationID = FlowInstance.StartedByOrganizationID
		INNER JOIN dbo.Department AS StartedByDepartment ON StartedByDepartment.DepartmentID = FlowInstance.StartedByDepartmentID
        INNER JOIN dbo.FlowDefinition ON FlowDefinition.FlowDefinitionID = FlowInstance.FlowDefinitionID
        INNER JOIN dbo.PhaseDefinition ON PhaseDefinition.PhaseDefinitionID = PhaseInstance.PhaseDefinitionID
        LEFT JOIN dbo.FormSetVersion ON FormSetVersion.PhaseInstanceID = PhaseInstance.PhaseInstanceID
		LEFT JOIN dbo.Frequency ON Frequency.FormSetVersionID = FormSetVersion.FormSetVersionID
WHERE   PhaseInstance.FlowInstanceID = @flowinstanceid
ORDER BY PhaseInstance.PhaseInstanceID;