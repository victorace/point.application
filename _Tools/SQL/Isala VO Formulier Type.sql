DECLARE @organizationid AS INT = 9128;
DECLARE @organizationname AS VARCHAR(255) = 'unknown';

DECLARE @flowfieldattributeid AS INT;

DECLARE @eoverdrachtflow AS INT = 209;

DECLARE @readonly AS BIT = 0;
DECLARE @visible AS BIT = 1;
DECLARE @required AS BIT = 1;
DECLARE @signonchange AS BIT = 1;

DECLARE @voformuliertype AS INT = 642;

SET @organizationname = (SELECT TOP 1 Name FROM dbo.Organization WHERE OrganizationID = @organizationid);

SET @flowfieldattributeid = (SELECT FlowFieldAttributeID FROM dbo.FlowFieldAttribute WHERE FlowFieldID = @voformuliertype AND FormTypeID = @eoverdrachtflow);
IF @flowfieldattributeid IS NOT NULL
BEGIN
	IF NOT EXISTS (SELECT 1 FROM dbo.FlowFieldAttributeException WHERE OrganizationID = @organizationid AND FlowFieldAttributeID = @flowfieldattributeid)
	BEGIN
		INSERT INTO dbo.FlowFieldAttributeException (FlowFieldAttributeID, RegionID, OrganizationID, ReadOnly, Visible, Required, SignalOnChange, PhaseDefinitionID, RequiredByFlowFieldID, RequiredByFlowFieldValue)
			VALUES (@flowfieldattributeid, NULL, @organizationid, @readonly, @visible, @required, @signonchange, NULL, NULL, NULL);
		PRINT 'Setting VOFormulierType ON for Organization: ' + @organizationname;
	END;
	ELSE
		PRINT 'VOFormulierType already has a FlowFieldAttributeException for Organization: ' + @organizationname;
END;

PRINT 'Done.';