DECLARE @organization VARCHAR(MAX) = 'LUMC';

SELECT  Organization.Name ,
        Location.Name ,
        Department.Name ,
        aspnet_Users.UserId,
        aspnet_Users.LoweredUserName ,
        Employee.EmployeeID,
        PageLock.TransferID ,
        FlowDefinition.Name ,
        PageLock.PageLockID ,
        PageLock.URL ,
        PageLock.SessionID ,
        PageLock.TimeStamp
FROM    PageLock
        INNER JOIN Employee ON Employee.EmployeeID = PageLock.EmployeeID
        INNER JOIN aspnet_Users ON aspnet_Users.UserId = Employee.UserId
        INNER JOIN Department ON Department.DepartmentID = Employee.DepartmentID
        INNER JOIN Location ON Location.LocationID = Department.LocationID
        INNER JOIN Organization ON Organization.OrganizationID = Location.OrganizationID
        INNER JOIN FlowInstance ON FlowInstance.TransferID = PageLock.TransferID
        INNER JOIN FlowDefinition ON FlowDefinition.FlowDefinitionID = FlowInstance.FlowDefinitionID
WHERE   ( @organization IS NULL
          OR Organization.Name = @organization
        )
ORDER BY Timestamp;