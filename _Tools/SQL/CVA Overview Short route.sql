select flowinstance.CreatedDate, flowinstance.TransferID, organization.Name, formsetversion.TimeStamp as 'Doorlopend dossier CVA Timestamp', flowfieldvalue.Value as 'Follow-up datum'
from flowinstance
inner join organization on organization.organizationid = flowinstance.startedbyorganizationid
inner join phaseinstance on phaseinstance.flowinstanceid = flowinstance.flowinstanceid
left join formsetversion on formsetversion.transferid = flowinstance.transferid
left join flowfieldvalue on flowfieldvalue.formsetversionid = formsetversion.formsetversionid
where flowinstance.deleted = 0 and flowinstance.flowdefinitionid = 4 and flowinstance.createddate >= '20180101' 
and phaseinstance.phasedefinitionid = 44 and phaseinstance.cancelled = 0
and formsetversion.formtypeid = 232 and flowfieldvalue.flowfieldid = 1660
order by organization.organizationid, flowinstance.CreatedDate desc