
declare @organizationid int = 76    -- <---- HAGA  <-------- Review: 76 Test: 76, Acc/Prod: 76

declare @testmode bit = 1           -- <---- Set to 0, to update the records <----------------

/*
1.       Alle POINT dossiers van Haga ziekenhuis met een valide patiŽntennummer worden omgenummerd
2.       POINT dossiers met een invalide patiŽntennummer worden NIET omgenummerd
3.       Invalide wil zeggen:
  a.       Korter dan 5 cijfers
  b.       Langer dan 8 cijfers
  c.       een andere teken bevattend dan een cijfer
  d.       een spatie bevattend
4.       Omnummer logica voor valide nummers:
  a.       Verwijder altijd het laatste cijfer
  b.       Geef het nummer een aantal voorloop nullen dusdanig dat het nummer precies 7 cijfers bevat
*/

set nocount on

declare @transferid int
declare @clientid int
declare @patientNumber varchar(50)
declare @newpatientnumber varchar(7)

declare transfercursor cursor for
select TransferID, Client.ClientID, Client.PatientNumber
  from Transfer
  join Client on Transfer.ClientID = Client.ClientID
  join Department on Transfer.DepartmentID = Department.DepartmentID 
  join Location on Department.LocationID = Location.LocationID
  join Organization on Location.OrganizationID = Organization.OrganizationID 
 where Organization.OrganizationID = @organizationid


open transfercursor

fetch next from transfercursor
 into @transferid, @clientid, @patientnumber

while @@FETCH_STATUS = 0
begin

  
    set @newpatientnumber = ''
    set @patientnumber = ltrim(rtrim(@patientnumber))

    if ( len(@patientnumber) < 5 or len(@patientnumber) > 8 or ISNUMERIC(@patientnumber) = 0)
    begin
        print 'NOK - TransferID: [' + cast(@transferid as varchar) + '] ClientID: [' + cast(@transferid as varchar) + '] Invalid patientnumber: [' + @patientnumber + ']'
    end
    else
    begin
        set @newpatientnumber = substring(@patientnumber,1, len(@patientnumber)-1)
        set @newpatientnumber = right('0000000' + @newpatientnumber, 7)

        print 'OK - TransferID: [' + cast(@transferid as varchar) + '] ClientID: [' + cast(@transferid as varchar) + '] Old patientnumber: [' + @patientnumber + '] New patientnumber: [' + @newpatientnumber + ']'

        if @testmode = 0
        begin
            update Client 
               set PatientNumber = @newpatientnumber 
             where ClientID = @clientid 
               and PatientNumber = @patientnumber 
        end
    end

    fetch next from transfercursor
     into @transferid, @clientid, @patientnumber

end
close transfercursor
deallocate transfercursor




