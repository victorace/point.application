SELECT top 100 Organization.Name, dbo.aspnet_Users.LoweredUserName, dbo.UsedHashCodes.IsAuth FROM dbo.UsedHashCodes
INNER JOIN dbo.Employee ON UsedHashCodes.EmployeeID = Employee.EmployeeID
INNER JOIN dbo.aspnet_Users ON Employee.UserId = aspnet_Users.UserId
INNER JOIN dbo.aspnet_Membership ON aspnet_Users.UserId = aspnet_Membership.UserId
INNER JOIN dbo.Department ON Employee.DepartmentID = Department.DepartmentID
INNER JOIN dbo.Location ON Department.LocationID = Location.LocationID
INNER JOIN dbo.Organization ON Location.OrganizationID = Organization.OrganizationID
--WHERE Organization.OrganizationID = 7605
ORDER BY dbo.aspnet_Membership.LastLoginDate desc