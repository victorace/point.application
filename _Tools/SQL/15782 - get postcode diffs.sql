--postalcode diffs.
--postalcodes are empty if department inherits service area from its organization. In this case, servicearea is filled.

select 
ServiceAreaPostalCodeID,
PostalCode.StartPostalCode,
PostalCode.EndPostalCode,
(Convert(int, left(PostalCode.EndPostalCode,4))-Convert(int, left(PostalCode.StartPostalCode,4))) As DiffPostalCodes,
postalcode.Description as PostalCodeDescription,
Department.Name As DepartmentName,
Location.Name As LocationName,
Organization.Name As Department_Organization,
ServiceArea.Description As ServiceAreaDescription,
ORG.Name As OrganizationName

from 
ServiceAreaPostalCode
Left Join PostalCode
On PostalCode.PostalCodeID = ServiceAreaPostalCode.PostalCodeID
Left Join Department
On Department.DepartmentID = ServiceAreaPostalCode.DepartmentID
Left Join Location
On Location.LocationID = Department.LocationID
Left Join Organization
On Organization.OrganizationID = Location.OrganizationID
Left Join ServiceArea
On ServiceArea.ServiceAreaID = ServiceAreaPostalCode.ServiceAreaID
Left Join Organization As ORG
On ORG.OrganizationID = ServiceArea.OrganizationID

Where len(PostalCode.EndPostalCode) = 6 And len (PostalCode.StartPostalCode) = 6
And ( ServiceArea.Inactive = 0 OR ServiceAreaPostalCode.DepartmentID is not null)
