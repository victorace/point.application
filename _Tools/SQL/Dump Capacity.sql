SELECT region.name AS Region, organization.Name AS Organization, location.name AS Location, department.DepartmentID, dbo.Department.Name AS Department,
dbo.DepartmentCapacity.AdjustmentCapacityDate,
dbo.DepartmentCapacity.CapacityDepartment, dbo.DepartmentCapacity.CapacityDepartmentDate,
dbo.DepartmentCapacity.CapacityDepartmentNext, dbo.DepartmentCapacity.CapacityDepartmentNextDate,
DepartmentCapacity.CapacityDepartment3, dbo.DepartmentCapacity.CapacityDepartment3Date,
dbo.DepartmentCapacity.CapacityDepartment4, DepartmentCapacity.CapacityDepartment4Date
FROM dbo.Organization
INNER JOIN dbo.Region ON Organization.RegionID = Region.RegionID
INNER JOIN dbo.Location ON Organization.OrganizationID = Location.OrganizationID
INNER JOIN dbo.Department ON Location.LocationID = Department.LocationID
INNER JOIN dbo.DepartmentCapacity ON Department.DepartmentID = DepartmentCapacity.DepartmentID
WHERE Organization.RegionID IN (25,31,32,33)
AND organization.Inactive = 0 AND location.Inactive = 0 AND department.Inactive = 0
AND department.CapacityFunctionality = 1
ORDER BY dbo.DepartmentCapacity.AdjustmentCapacityDate, region.name, Organization.name, location.name, Department.NAME