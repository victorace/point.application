﻿using Point.AutoSaveView.Context;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Point.AutoSaveView.BL
{
    public class CshtmlBL
    {
        public static void SaveView(FileInfo file, string viewcontents)
        {
            var webconfig = GetProjectWebConfigFromFile(file);
            if (webconfig == null) return;

            var map = new System.Configuration.ExeConfigurationFileMap();
            map.ExeConfigFilename = webconfig.FullName;
            var configuration = System.Configuration.ConfigurationManager.OpenMappedExeConfiguration(map, System.Configuration.ConfigurationUserLevel.None);

            try
            {
                //Todo : make settings screen to save connectionstring name
                if (configuration.ConnectionStrings.ConnectionStrings["PointContext"] == null) return;

                var connectionstring = configuration.ConnectionStrings.ConnectionStrings["PointContext"].ConnectionString;
                var context = new RazorTemplateContext(connectionstring);


                var existing = context.RazorTemplates.Where(tmp => tmp.ViewFileName.ToLower() == file.Name.ToLower()).FirstOrDefault();
                if (existing == null)
                {
                    return;
                    //existing = new Models.RazorTemplate();
                    //existing.TemplateCode = file.Name.Length > 20 ? file.Name.Substring(0, 20) : file.Name;
                    //existing.TemplateName = file.Name;
                    //existing.ViewFileName = file.Name;
                    //context.RazorTemplates.Add(existing);
                }

                existing.Contents = viewcontents;

                context.SaveChanges();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
            }
            
        }

        private static System.IO.FileInfo GetProjectWebConfigFromFile(FileInfo file)
        {
            var projectfolder = FindProjectFileDirectory(file.Directory);
            if (projectfolder == null) return null;

            var webconfig = projectfolder.GetFiles("web.config").FirstOrDefault();
            return webconfig;
        }

        private static System.IO.DirectoryInfo FindProjectFileDirectory(DirectoryInfo dir)
        {
            //website has no *proj file...
            var files = dir.GetFiles("global.asax", System.IO.SearchOption.TopDirectoryOnly);
            if (files.Count() > 0)
            {
                return dir;
            }
            else if (files.Count() == 0 && dir.Parent != null)
            {
                return FindProjectFileDirectory(dir.Parent);
            }
            else
                return null;


        }
    }
}
