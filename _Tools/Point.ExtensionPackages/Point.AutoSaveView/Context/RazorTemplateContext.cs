﻿using Point.AutoSaveView.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Point.AutoSaveView.Context
{
    public class RazorTemplateContext : DbContext
    {
        public RazorTemplateContext(string connectionstring) : base(connectionstring) { }

        public DbSet<RazorTemplate> RazorTemplates { get; set; }
    }
}
