using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Point.AutoSaveView.Models
{
    [Table("RazorTemplate")]
    public partial class RazorTemplate
    {
        public int RazorTemplateID { get; set; }
        public string TemplateName { get; set; }
        public string TemplateCode { get; set; }
        public string Contents { get; set; }
        public string ViewFileName { get; set; }

        [NotMapped]
        public int EntityID
        {
            get
            {
                return RazorTemplateID;
            }
            set
            {
                RazorTemplateID = EntityID;
            }
        }
    }
}
