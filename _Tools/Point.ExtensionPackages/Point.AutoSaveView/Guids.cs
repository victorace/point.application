﻿// Guids.cs
// MUST match guids.h
using System;

namespace Techxx.Point_AutoSaveView
{
    static class GuidList
    {
        public const string guidPoint_AutoSaveViewPkgString = "699b2cc3-3cba-4f84-a4df-879162d9a6b2";
        public const string guidPoint_AutoSaveViewCmdSetString = "0fa3f75a-78ba-423d-966e-0cc4d8f64b20";

        public static readonly Guid guidPoint_AutoSaveViewCmdSet = new Guid(guidPoint_AutoSaveViewCmdSetString);
    };
}