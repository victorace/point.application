IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE  TABLE_NAME = 'MaasstadImport$' AND TABLE_SCHEMA = 'schema')
BEGIN
	DROP TABLE [dbo].[IsalaImport$];
END;

SET ANSI_NULLS ON;
GO

SET QUOTED_IDENTIFIER ON;
GO

SET ANSI_PADDING ON;
GO

CREATE TABLE [dbo].[MaasstadImport$](
	[Voornaam] [NVARCHAR](255) NOT NULL,
	[Tussenvoegsel] [NVARCHAR](50) NULL,
	[Achternaam] [NVARCHAR](255) NOT NULL,
	[Meisjesnaam] [NVARCHAR](255) NULL,
	[Geslacht] [CHAR](1) NOT NULL,
	[Telnr] [NVARCHAR](50) NOT NULL,
	[EmailAddress] [VARCHAR](255) NULL,
	[Functie] [NVARCHAR](255) NOT NULL,
	[ExternID] [NVARCHAR](50) NOT NULL,
	[Inlognaam] [NVARCHAR](255) NOT NULL,
	[Afdelingsnaam] [NVARCHAR](255) NULL,
	[KoppelenAlleAfdelingen] [NVARCHAR](50) NULL
) ON [PRIMARY];

GO

SET ANSI_PADDING OFF;
GO

PRINT 'Table MaasstadImport$ created.';
GO

PRINT 'Done..';
GO