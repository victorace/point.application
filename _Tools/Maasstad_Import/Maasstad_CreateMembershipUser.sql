----------------------------------
--
-- Voorbereidingen op excelsheet
-- - Kolomnamen:  [Voornaam], [Tussenvoegsel], [Achternaam], [Meisjesnaam], [Geslacht], [Telnr], [EmailAddress], [Functie],
-- [ExternID], [Inlognaam], [Afdelingsnaam], [KoppelenAlleAfdelingen]
--
----------------------------------
--
-- Importeren:
-- 
-- 1: Importeer de excelsheet naar tabel 'MaasstadImport$'
-- 2: Pas de 5 variabelen evt aan
-- 3: Draai het script "Maasstad_CreateMembershipUser.sql"
-- 4: Kopieer het nu geproduceerde 'log' vanuit de messages-tab naar een tekstbestand en sla deze op
-- 5: Verwijder de tijdelijke tabel:
--    DROP TABLE [MaasstadImport$]
--
----------------------------------

declare @organizationtypeid as int
declare @organizationname varchar(255)
declare @locationname varchar(255)
declare @departmentname varchar(255)
declare @defaultpassword varchar(50)
declare @defaultrole varchar(50)

declare @changepasswordbyemployee as bit

set @organizationtypeid = 1 -- HOSPITAL
set @organizationname = 'Maasstad Ziekenhuis'
set @locationname = 'Maasstadweg'
set @departmentname = 'vpk'
set @defaultpassword = 'Welkom1!'
set @defaultrole = 'Organization'

set @changepasswordbyemployee = 1

DECLARE @functionroletypeid AS INT
DECLARE @functionrolelevelid AS INT

SET @functionroletypeid = 22 -- Beheer organisatie(s)
SET @functionrolelevelid = 3 -- Organization

----------------------------------
set nocount on

declare @organizationid int
declare @locationid int
declare @departmentid int

declare @line int
declare @cont bit

declare @salt varchar(50)
declare @saltbase64 varchar(50)
declare @saltcount int
declare @saltchars varchar(255)
declare @password varchar(50)
declare @passwordbase64 varchar(50) 
declare @base64output varchar(255)
declare @base64bits varbinary(3)
declare @base64pos int

declare @roleID uniqueidentifier

declare @voornaam varchar(255)
declare @tussenvoegsel varchar(50)
declare @achternaam varchar(255)
declare @meisjesnaam varchar(255)
declare @geslacht char(1)
declare @telnr varchar(50)
declare @emailaddress varchar(255)
declare @functie varchar(255)
declare @externid varchar(50)
declare @inlognaam varchar(255)
declare @afdelingsnaam varchar(255)
declare @koppelenalleafdelingen varchar(50)



select @organizationid = OrganizationID
  from Organization 
 where Name = @organizationname and OrganizationTypeID = @organizationtypeid and Inactive = 0
 
if ISNULL(@organizationid,0) = 0 
begin
    print 'FOUT: Organisatie "' + @organizationname + '" niet gevonden!'
    return
end
--print 'INFO: OrganizationID = ' + cast(@organizationid as varchar(10))

select @locationid = LocationID
  from Location 
 where Name = @locationname 
 
if ISNULL(@locationid,0) = 0 
begin
    print 'FOUT: Locatie "' + @locationname + '" niet gevonden!'
    return
end
--print 'INFO: LocationID = ' + cast(@locationid as varchar(10))

select @departmentid = DepartmentID
  from Department 
 where Name = @departmentname
 
if ISNULL(@departmentid,0) = 0 
begin
    print 'FOUT: Department "' + @departmentname + '" niet gevonden!'
    return
end
--print 'INFO: DepartmentID = ' + cast(@departmentid as varchar(10))

select @roleID = RoleId 
  from aspnet_roles
 where RoleName = @defaultrole

if @roleID is null
begin
    print 'FOUT: Rol "' + @defaultrole + '" niet gevonden!'
    return
end
--print 'INFO: RoleID = ' + cast(@defaultrole as varchar(50))


declare accCursor cursor for
    select top 2 [Voornaam],
       NULLIF([Tussenvoegsel], '') AS [Tussenvoegsel],
           [Achternaam],
           NULLIF([Meisjesnaam], '') AS [Meisjesnaam],
           [Geslacht],
           [Telnr],
       NULLIF([EmailAddress], '') AS [EmailAddress],
           [Functie],
           [ExternID],
           [Inlognaam],
       NULLIF([Afdelingsnaam], '') AS [Afdelingsnaam],
           [KoppelenAlleAfdelingen]
      from [MaasstadImport$]

open accCursor

fetch next from accCursor into
  @voornaam,
  @tussenvoegsel,
  @achternaam,
  @meisjesnaam,
  @geslacht,
  @telnr,
  @emailaddress,
  @functie,
  @externid,
  @inlognaam,
  @afdelingsnaam,
  @koppelenalleafdelingen

set @line = 1
while @@FETCH_STATUS = 0
begin
    ------------------------------------------------------------------------------  
  set @cont = 1 -- Continue-boolean
  
  --set @externid = right(replicate('0',5) + cast(@externid as varchar(5)), 5) -- Padding zeroes to have the id with 5 digits --

  --set @departmentid = 0
  --set @locationid = 0
  --select @departmentid = DepartmentID, @locationid = LocationID 
  --  from Department
  -- where Department.Name = @afdelingsnaam
  --   and Department.Inactive = 0
  
  --set @departmentid = 0
  --select @departmentid = DepartmentID 
  --  from Department
  -- where Department.Name = @afdelingsnaam
  --   and Department.Inactive = 0
  --   and DepartmentID in ( select DepartmentID 
  --                           from Department
  --                          where LocationID = @locationid )

  if isnull(@departmentid,0) = 0 
  begin
      print 'FOUT' + char(9) + 'REGEL ' + cast(@line as varchar) + char(9) + 'Afdeling "' + @afdelingsnaam + '" niet gevonden.'
      print char(9) + char(9) + 'ZIS: ' + @externid + ', Achternaam: ' + @achternaam
      set @cont = 0
  end

  if isnull(@locationid,0) = 0 
  begin
      print 'FOUT' + char(9) + 'REGEL ' + cast(@line as varchar) + char(9) + 'Afdeling "' + @afdelingsnaam + '" (' + cast(@departmentid as varchar) + ') has no Location.'
      print char(9) + char(9) + 'ZIS: ' + @externid + ', Achternaam: ' + @achternaam
      set @cont = 0
  end
  
  -- Users with empty ExternID will be imported
  if exists ( select 1 from Employee where isnull(ExternID,'') = @externid and len(ltrim(rtrim(isnull(ExternID,'')))) > 0 and DepartmentID in ( select DepartmentID from Department where LocationID = @locationid ) )
  begin
      print 'FOUT' + char(9) + 'REGEL ' + cast(@line as varchar) + char(9) + 'Medewerker met ZIS-ID "' + isnull(@externid,'') + '" bestaat al.' 
      print char(9) + char(9) + 'ZIS: ' + isnull(@externid,'') + ', Achternaam: ' + @achternaam
      set @cont = 0
  end
   
   
  if exists ( select 1 from aspnet_users where LoweredUserName = @inlognaam )
  begin
      print 'FOUT' + char(9) + 'REGEL ' + cast(@line as varchar) + char(9) + 'Gebruiker met username "' + @inlognaam + '" bestaat al.' 
      print char(9) + char(9) + 'ZIS: ' + @externid + ', achternaam: ' + @achternaam
      set @cont = 0
  end

    ----- All ok, let's continue
    if @cont = 1
    begin
  
        ----- Create SALT as (random) string
        set @saltcount = 0
        set @saltchars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'
        set @salt = ''
        
        while @saltcount < 16
        begin
            set @salt = @salt + SUBSTRING(@saltchars,CAST(ABS(CHECKSUM(NEWID()))*RAND(@saltcount) as int)%LEN(@saltchars)+1,1)
            set @saltcount = @saltcount + 1
        end

        ----- Base64 the SALT
        set @base64pos = 1
        set @base64output = ''
        while @base64pos <= len(@salt) 
        begin
            set @base64bits = convert(varbinary(3), substring(@salt, @base64pos, 3))
            set @base64output = @base64output + substring('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/', substring(@base64bits, 1, 1) / 4 + 1, 1)
            set @base64output = @base64output + substring('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/', substring(@base64bits, 1, 1) % 4 * 16 + substring(@base64bits, 2, 1) / 16 + 1, 1)
            set @base64output = @base64output + substring('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/', substring(@base64bits, 2, 1) % 16 * 4 + substring(@base64bits, 3, 1) / 64 + 1, 1)
            set @base64output = @base64output + substring('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/', substring(@base64bits, 3, 1) % 64 + 1, 1)
            set @base64pos = @base64pos + 3
        end
        select @saltbase64 = (left(@base64output, len(@base64output) - 3 + len(@base64bits)) + replicate('=', 3 - len(@base64bits)))

        ----- Create PASSWORD as (random) string, in this case using the default password
        set @password = @defaultpassword 

        ----- Encode and Base64 the PASSWORD with the SALT
        declare @combi varbinary(512)
        set @combi = hashbytes('sha1',cast('' as  xml).value('xs:base64Binary(sql:variable(''@saltbase64''))','varbinary(256)') + convert(varbinary(256),N''+@password))
        set @passwordbase64 = cast('' as xml).value('xs:base64Binary(xs:hexBinary(sql:variable(''@combi'')))','varchar(64)')

        declare @UserID uniqueidentifier 
        --declare @UserName varchar(50)
        declare @Now varchar(50)

        set @UserID = null
        --set @UserName = @externid
        set @Now = convert(varchar, getdate(),120)

        exec dbo.aspnet_Membership_CreateUser
              @ApplicationName = N'POINT', 
              @UserName = @inlognaam, 
              @Password = @passwordbase64,
              @PasswordSalt = @saltbase64, 
              @Email = NULL,
              @PasswordQuestion = NULL,
              @PasswordAnswer = NULL,
              @IsApproved = 1, 
              @UniqueEmail = 0, 
              @PasswordFormat = 1,   
              @CurrentTimeUtc = @Now, 
              @UserId = @UserID OUTPUT 

        --print 'INFO' + char(9) + 'REGEL ' + cast(@line as varchar) + char(9) + 'User aangemaakt met UserID ' + cast(@UserID as varchar(50))

        exec dbo.aspnet_UsersInRoles_AddUsersToRoles
              @ApplicationName = N'POINT', 
              @UserNames = @inlognaam, 
              @RoleNames = @defaultrole,
              @CurrentTimeUtc= @Now


        -- ROLLBACK TRAN
        declare @employeeId int
        set @employeeId = null
        set @geslacht = LOWER(@geslacht)
       
        INSERT INTO [Employee] ([DepartmentID]
             ,[FirstName]
             ,[MiddleName]
             ,[LastName]
             ,[Gender]
             ,[Position]
             ,[PhoneNumber]
             ,[EmailAddress]
             ,[ExternID]
             ,[ChangePasswordByEmployee]
             ,[BIG]
             ,[MFARequired]
             ,UserId)
           VALUES (@DepartmentID,
              @voornaam,
              @tussenvoegsel,
              @achternaam,
              @geslacht,
              @functie,
              @telnr,
              @emailaddress,
              @externid,
              @changepasswordbyemployee,
              NULL,
              0,
              @UserID)

        SET @employeeID = SCOPE_IDENTITY()

            print 'INFO' + char(9) + 'REGEL ' + cast(@line as varchar) + char(9) + 'Medewerker aangemaakt met Gebruikersnaam ' + @inlognaam -- + ' en EmployeeID ' + cast(@employeeId as varchar(50)) 
            print char(9) + char(9) + 'ZIS: ' + @externid + ', Achternaam: ' + @achternaam

        -- don't need a FunctionRole
        --INSERT INTO dbo.FunctionRole (EmployeeID, FunctionRoleTypeID, FunctionRoleLevelID, ModifiedTimeStamp)
        --  VALUES (@employeeId, 22, 3, GETDATE())

    end  -- if @cont = 1
  
    set @line = @line + 1 
    print ''
    ------------------------------------------------------------------------------  
fetch next from accCursor into
    @voornaam,
  @tussenvoegsel,
  @achternaam,
  @meisjesnaam,
  @geslacht,
  @telnr,
  @emailaddress,
  @functie,
  @externid,
  @inlognaam,
  @afdelingsnaam,
  @koppelenalleafdelingen
    
end

close accCursor
deallocate accCursor