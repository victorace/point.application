set nocount on

DECLARE @organizationid int
declare @employeeid int
declare @userid UNIQUEIDENTIFIER
declare @username AS VARCHAR(500)
declare @timestamp datetime
declare @defaultpassword varchar(50)

declare @salt varchar(50)
declare @saltbase64 varchar(50)
declare @saltcount int
declare @saltchars varchar(255)
declare @password varchar(50)
declare @passwordbase64 varchar(50) 
declare @base64output varchar(255)
declare @base64bits varbinary(3)
declare @base64pos int

SET @organizationid = 10305
set @defaultpassword = 'Rw#4B2q&w'
set @timestamp = GETDATE()

declare accCursor cursor FOR
	select TOP 2 employee.EmployeeID, aspnet_Users.UserId, aspnet_Users.LoweredUserName AS [username] FROM dbo.aspnet_Users 
	inner join dbo.Employee ON aspnet_Users.UserId = Employee.UserId
	INNER JOIN dbo.Department ON Employee.DepartmentID = Department.DepartmentID
	INNER JOIN dbo.Location ON Department.LocationID = Location.LocationID
	INNER JOIN dbo.Organization ON Location.OrganizationID = Organization.OrganizationID
	INNER JOIN aspnet_Membership ON aspnet_Users.UserId = aspnet_Membership.UserId
	where aspnet_Users.LoweredUserName LIKE '%@maasstadziekenhuis.nl' AND Organization.OrganizationID = @organizationid
	--AND aspnet_Membership.Password = @defaultpassword

open accCursor

fetch next from accCursor into
  @employeeid,
  @userid,
  @username

while @@FETCH_STATUS = 0
BEGIN

	----- Create SALT as (random) string
	set @saltcount = 0
	set @saltchars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'
	set @salt = ''

	while @saltcount < 16
	begin
		set @salt = @salt + SUBSTRING(@saltchars,CAST(ABS(CHECKSUM(NEWID()))*RAND(@saltcount) as int)%LEN(@saltchars)+1,1)
		set @saltcount = @saltcount + 1
	end

	----- Base64 the SALT
	set @base64pos = 1
	set @base64output = ''
	while @base64pos <= len(@salt) 
	begin
		set @base64bits = convert(varbinary(3), substring(@salt, @base64pos, 3))
		set @base64output = @base64output + substring('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/', substring(@base64bits, 1, 1) / 4 + 1, 1)
		set @base64output = @base64output + substring('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/', substring(@base64bits, 1, 1) % 4 * 16 + substring(@base64bits, 2, 1) / 16 + 1, 1)
		set @base64output = @base64output + substring('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/', substring(@base64bits, 2, 1) % 16 * 4 + substring(@base64bits, 3, 1) / 64 + 1, 1)
		set @base64output = @base64output + substring('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/', substring(@base64bits, 3, 1) % 64 + 1, 1)
		set @base64pos = @base64pos + 3
	end
	select @saltbase64 = (left(@base64output, len(@base64output) - 3 + len(@base64bits)) + replicate('=', 3 - len(@base64bits)))

	----- Create PASSWORD as (random) string, in this case using the default password
	set @password = @defaultpassword 

	----- Encode and Base64 the PASSWORD with the SALT
    declare @combi varbinary(512)
    set @combi = hashbytes('sha1',cast('' as  xml).value('xs:base64Binary(sql:variable(''@saltbase64''))','varbinary(256)') + convert(varbinary(256),N''+@password))
    set @passwordbase64 = cast('' as xml).value('xs:base64Binary(xs:hexBinary(sql:variable(''@combi'')))','varchar(64)')

	--EXEC dbo.aspnet_Membership_SetPassword 
	--	@ApplicationName=N'POINT',			-- nvarchar(256)
	--	@UserName=@UserName,				-- nvarchar(256)
	--	@NewPassword=@passwordbase64,	    -- nvarchar(128)
	--	@PasswordSalt=@saltbase64,			-- nvarchar(128)
	--	@CurrentTimeUtc=@timestamp,			-- datetime
	--	@PasswordFormat=0					-- int

	--UPDATE dbo.Employee
	--	SET ChangePasswordByEmployee = 0
	--WHERE UserId = @userid AND EmployeeID = @employeeid
   
	PRINT @username + ' has been updated'

fetch next from accCursor into
   @employeeid,
   @userid,
   @username
    
end

close accCursor
deallocate accCursor

PRINT 'Done.'