﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SSOGenerator
{
    public partial class MainWindow : Window
    {

        public MainWindow()
        {
            InitializeComponent();

            bindPointSites();
            bindTextBoxes();
        }

        private void btnCreateURLSSO_Click(object sender, RoutedEventArgs e)
        {
            txbURLSSO.Text = generateUrlSSO();
            txbURLSSO.Foreground = new SolidColorBrush(Colors.Black);
        }

        private void btnClipboardSSO_Click(object sender, RoutedEventArgs e)
        {
            Browser.Copy(txbURLSSO.Text);
        }

        private void btnBrowseUrlSSO_Click(object sender, RoutedEventArgs e)
        {
            Browser.Get(txbURLSSO.Text);
        }

        private void btnBrowsePostSSO_Click(object sender, RoutedEventArgs e)
        {
            string date = Utils.GetDateString();
            string sign = generateSign(txbSSOKey.Text, txbOrgExternalReference1.Text, txbExternalReference.Text, date);
            string postdata = String.Format("ExternalReference1={0}&OrgExternalReference1={1}&Date={2}&Sign={3}", txbExternalReference.Text, txbOrgExternalReference1.Text, date, sign);
            if (!String.IsNullOrEmpty(txbDepartmentReference.Text))
            {
                postdata += String.Format("&DepartmentReference1={0}", txbDepartmentReference.Text);
            }
            if (!String.IsNullOrEmpty(txbPatExternalReference.Text))
            {
                postdata += String.Format("&PatExternalReference1={0}", txbPatExternalReference.Text);
            }
            if (!String.IsNullOrEmpty(txbEmplReference.Text))
            {
                postdata += String.Format("&EmplReference1={0}", generateEmplReferenceString(txbSSOKey.Text, txbEmplReference.Text));
            }
            txbURLSSO.Clear();
            txbURLSSO.Text = "POSTED @" +DateTime.Now.ToString("dd-MM HH:mm:ss") + Environment.NewLine + postdata.Replace("&", Environment.NewLine);

            Browser.Post(String.Format("{0}Login/SSO", cmbPointSites.GetValue().FixSite()), postdata); //// <<< + /SSO ???
        }

        private void btnCreateURL_Click(object sender, RoutedEventArgs e)
        {
            txbURL.Text = generateUrl(false);
        }

        private void btnCreateURLWS_Click(object sender, RoutedEventArgs e)
        {
            txbURL.Text = generateUrl(true);
        }

        private void btnClipboard_Click(object sender, RoutedEventArgs e)
        {
            Browser.Copy(txbURL.Text);
        }

        private void btnBrowseUrl_Click(object sender, RoutedEventArgs e)
        {
            Browser.Get(txbURL.Text);
        }

        private void bindPointSites()
        {
            cmbPointSites.ItemsSource = Config.GetAppSettingList("PointSite");
            int selected = 1;
            Int32.TryParse(Config.GetAppSetting("DefaultPointSite"), out selected);
            cmbPointSites.SelectedIndex = (selected-1);
        }

        private void bindTextBoxes()
        {
            txbSSOKey.Text = Config.GetAppSetting("SSOKey");
            txbOrgExternalReference1.Text = Config.GetAppSetting("OrgExternalReference1");
            txbExternalReference.Text = Config.GetAppSetting("ExternalReference");
            txbPatExternalReference.Text = Config.GetAppSetting("PatExternalReference");
            txbPatExternalReferenceBSN.Text = Config.GetAppSetting("PatExternalReferenceBSN");
            txbDepartmentReference.Text = Config.GetAppSetting("DepartmentReference");
        }


        private string generateUrlSSO()
        {
            string date = Utils.GetDateString();

            string site = cmbPointSites.GetValue().FixSite();
            string path = new Uri(site).AbsolutePath.TrimEnd('/');

            string orgparametername = "orgexternalreference1";
            if (rbtVVT.IsChecked == true)
            {
                orgparametername = "orgcareexternalreference1";
            }

            string patparameter = "";
            if (rbtZH.IsChecked == true)
            {
                patparameter = String.Format("!patexternalreference1={0}", txbPatExternalReference.Text);
            }

            string patBSNparameter = String.Format("!patexternalreferencebsn1={0}", txbPatExternalReferenceBSN.Text);

            string extraparameter = "";
            if (!String.IsNullOrEmpty(txbDepartmentReference.Text))
            {
                extraparameter = String.Format("!departmentreference1={0}", txbDepartmentReference.Text);
            }

            string emplparameter = "";
            if (!String.IsNullOrEmpty(txbEmplReference.Text) && !String.IsNullOrEmpty(txbSSOKey.Text))
            {
                emplparameter = String.Format("!emplReference1={0}", generateEmplReferenceString(txbSSOKey.Text, txbEmplReference.Text));
            }

            string sign = generateSign(txbSSOKey.Text, txbOrgExternalReference1.Text, txbExternalReference.Text, date);

            return String.Format("{0}Security/Login.aspx?externalreference1={1}&"+ orgparametername + "={2}&ReturnUrl={3}/default.aspx?Date={4}!Sign={5}{6}{7}{8}{9}",
                   site, txbExternalReference.Text, txbOrgExternalReference1.Text, path, date, sign, patparameter, patBSNparameter, emplparameter, extraparameter);
        }






        private string generateUrl(bool isWebservice = false)
        {
            string patparametername = (isWebservice ? "externalreference1" : "patexternalreference1");

            string patBSNparameter = String.Format("&patexternalreferencebsn1={0}", txbPatExternalReferenceBSN.Text);

            string extraparameter = "";
            if (!String.IsNullOrEmpty(txbDepartmentReference.Text))
            {
                extraparameter = String.Format("&departmentreference1={0}", txbDepartmentReference.Text);
            }

            return String.Format("{0}Transfer/Transfers.aspx?" + patparametername + "={1}{2}{3}", cmbPointSites.GetValue().FixSite(), txbPatExternalReference.Text, patBSNparameter, extraparameter);
        }
        



        private void cmbPointSites_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            clearURLFields();
        }

        private void txb_TextChanged(object sender, TextChangedEventArgs e)
        {
            clearURLFields();
        }

        private void rbt_Checked(object sender, RoutedEventArgs e)
        {
            clearURLFields();
        }

        private void clearURLFields()
        {
            txbURL.Clear();
            txbURLSSO.Clear();
        }

        private void btnExampleJsonString_Click(object sender, RoutedEventArgs e)
        {
            if (!File.Exists("Example.json"))
            {
                txbEmplReference.Text = "File 'Example.json' does not exist!";
            }

            var jsontext = File.ReadAllText("Example.json");
            jsontext = jsontext.Replace("[Department]", txbDepartment.Text);
            jsontext = jsontext.Replace("[AutoCreateCode]", txbAutoCreateCode.Text);

            txbEmplReference.Text = jsontext;
        }

        private string generateEmplReferenceString(string ssoKey, string json)
        {

            if (String.IsNullOrEmpty(ssoKey) || String.IsNullOrEmpty(json))
            {
                return "";
            }

            string error = "";
            dynamic arrayJsonEmployee = JsonConvert.DeserializeObject(json, new JsonSerializerSettings { Error = delegate (object sender, Newtonsoft.Json.Serialization.ErrorEventArgs args) { error = args.ErrorContext.Error.Message; args.ErrorContext.Handled = true; } });
            if (!String.IsNullOrEmpty(error))
            {
                return "JSON is niet correct: " + error;
            }
            if (arrayJsonEmployee.LastName == "" || arrayJsonEmployee.Location == "")
            {
                return "JSON is niet correct: Lastname en/of Location is niet gevuld";
            }

            byte[] hashBytesCheckKey = Encoding.UTF8.GetBytes(String.Format("{0}_{1}_{2}", ssoKey, arrayJsonEmployee.LastName, arrayJsonEmployee.Location));
            string hashStringCheckKey = Utils.GenerateHashedString(hashBytesCheckKey, rbtSHA256.IsChecked).ToLower();
            json = json.Insert(json.IndexOf("}"), String.Format(@", ""CheckKey"": ""{0}"" ", hashStringCheckKey));

            return Utils.Base64Encode(json);

        }

        private string generateSign(string ssoKey, string organizationReference, string externalReference, string date)
        {
            byte[] hashBytes = Encoding.UTF8.GetBytes(String.Format("{0}_{1}_{2}_{3}", txbSSOKey.Text, txbOrgExternalReference1.Text, txbExternalReference.Text, date));

            return Utils.GenerateHashedString(hashBytes, rbtSHA256.IsChecked).ToLower();
        }


    }


}
