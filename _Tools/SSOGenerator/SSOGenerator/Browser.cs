﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace SSOGenerator
{
    public static class Browser
    {

        public static void Post(string url, string postdata)
        {
            if (String.IsNullOrEmpty(url) || String.IsNullOrEmpty(postdata))
            {
                return;
            }

            SHDocVw.InternetExplorer browser = new SHDocVw.InternetExplorer();

            object urlvar = url;
            object flags = null;
            object frame = null;
            object headers = "Content-Type: application/x-www-form-urlencoded" + Environment.NewLine;
            object postvars = ASCIIEncoding.ASCII.GetBytes(postdata);

            browser.Visible = true;
            browser.Navigate2(ref urlvar, ref flags, ref frame, ref postvars, ref headers);

        }

        public static void Get(string url)
        {
            if (String.IsNullOrEmpty(url))
            {
                return;
            }

            System.Diagnostics.Process.Start(url);
        }

        public static void Copy(string url)
        {
            if (String.IsNullOrEmpty(url))
            {
                return;
            }

            Clipboard.SetText(url);
        }

    }
}
