﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace SSOGenerator
{
    public static class Utils
    {

        public static string Base64Decode(string data)
        {
            byte[] toDecodeByte = Convert.FromBase64String(data);

            System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();
            System.Text.Decoder utf8Decode = encoder.GetDecoder();

            int charCount = utf8Decode.GetCharCount(toDecodeByte, 0, toDecodeByte.Length);

            char[] decodedChar = new char[charCount];
            utf8Decode.GetChars(toDecodeByte, 0, toDecodeByte.Length, decodedChar, 0);
            string result = new String(decodedChar);
            return result;
        }

        public static string Base64Encode(string data)
        {
            var toEncodeBytes = System.Text.Encoding.UTF8.GetBytes(data);
            string base64 = System.Convert.ToBase64String(toEncodeBytes);

            return base64;
        }

        public static string GenerateHashedString(byte[] hashBytes, bool? sha256 = true)
        {
            HashAlgorithm hashalgorithm = new SHA256Managed();
            if (sha256 == false)
            {
                hashalgorithm = new SHA1Managed();
            }

            byte[] computedHash = hashalgorithm.ComputeHash(hashBytes);

            string resultHexString = String.Empty;
            string hashedString = String.Empty;

            foreach (Byte b in computedHash)
            {
                hashedString += b.ToString("X2");
            }

            return hashedString;
        }

        public static string GetDateString()
        {
            return DateTime.Now.ToString("yyyyMMddHHmmss");
        }



    }
}
