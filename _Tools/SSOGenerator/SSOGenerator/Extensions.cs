﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;

namespace SSOGenerator
{
    public static class Extensions
    {
        public static string FixSite(this string s)
        {
            if (!s.EndsWith("/"))
            {
                s += "/";
            }
            return s;
        }

        public static string GetValue(this ComboBox comboBox)
        {
            if (comboBox.SelectedIndex == -1)
            {
                return "";
            }
            return comboBox.SelectedValue.ToString();
        }

    }
}
