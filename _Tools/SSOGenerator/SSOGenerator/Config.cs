﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace SSOGenerator
{
    public static class Config
    {
        public static List<string> GetAppSettingList(string searchTerm)
        {
            var keys = ConfigurationManager.AppSettings.Keys;
            return keys.Cast<object>()
                       .Where(key => key.ToString().ToLower()
                       .StartsWith(searchTerm.ToLower()))
                       .Select(key => ConfigurationManager.AppSettings.Get(key.ToString())).ToList();
        }

        public static string GetAppSetting(string key)
        {
            string value = "";

            try
            {
                value = ConfigurationManager.AppSettings.Get(key);
            }
            catch
            {
                value = "[" + key + "] not found !!!";
            }

            return value;
        }


    }
}
