﻿//using KiesBeterConsole.Models.Context;
//using KiesBeterConsole.Models.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using KiesBeterConsole.Models.Kiesbeter;


namespace KiesBeterConsole
{

    class Import
    {
        
        public static void Main(string[] args)
        {



            // Get the info from KiesBeter
            KiesBeter kiesbeter = new KiesBeter();

            // These two can be skipped if the file is already filled. Set forceNew=true to update (or create file)
            kiesbeter.RetrieveLocationLists(false); // Retrieve list(s) with locationdata from KiesBeter-site
            kiesbeter.RetrieveLocationExtendedData(false); // Get extended data from KiesBeter-site


            // Get the data from the extended-file:
            List<KiesBeterConsole.Models.Kiesbeter.Location> kiesbeterLocations = kiesbeter.ReadLocationData();


            // Get the data from the POINT-database:
            IEnumerable<KiesBeterConsole.Models.Point.CareFull> pointLocations = new Point().GetLocations();


            // Compare them:
            HandleLocationData(pointLocations, kiesbeterLocations);


            // Done ...
            Logger.DoneAndWait();
        }



        static void HandleLocationData(IEnumerable<KiesBeterConsole.Models.Point.CareFull> pointLocations, List<KiesBeterConsole.Models.Kiesbeter.Location> kiesbeterLocations)
        {


            var workbook = new NPOI.XSSF.UserModel.XSSFWorkbook();
            var sheet = workbook.CreateSheet("KiesBeter");
            writeExcelHeader(sheet);

            // Set our internal field to false, so we can see if the location is found in our database.;
            kiesbeterLocations.ForEach(kbl=>kbl.foundmatch = false);


            var notfound = 0;
            var notfoundlandelijk = 0;

            foreach (var pointLoc in pointLocations.OrderBy(pl=>pl.OrganizationCare.Name).ThenBy(pl=>pl.LocationCare.Name)) //.Where(pl=>pl.OrganizationCare.OrganizationCareID == 57))
            {
                Logger.WriteLine("Processing:");
                Logger.WriteLine("     OrgaCare: {0}", pointLoc.OrganizationCare.Name);
                Logger.WriteLine("     LocaCare: {0}", pointLoc.LocationCare.Name);
                Logger.WriteLine("     Orga: {0}", pointLoc.Organization.Name);
                Logger.WriteLine("     Region: {0}", pointLoc.Region.Name);

                var kbLocationsFound = kiesbeterLocations.Where(kbl => kbl.name.IndexOf(pointLoc.LocationCare.Name, StringComparison.InvariantCultureIgnoreCase)>=0 
                                                                    && kbl.ConcernName().IndexOf(pointLoc.OrganizationCare.Name.Replace("+",""),StringComparison.InvariantCultureIgnoreCase)>=0);

                if (kbLocationsFound.Count() == 0) // Try again, only with the locationname
                {
                    kbLocationsFound = kiesbeterLocations.Where(kbl => kbl.name.Equals(pointLoc.OrganizationCare.Name, StringComparison.InvariantCultureIgnoreCase) );
                }

                if (kbLocationsFound.Count() == 0) // Try again, with zipcode
                {
                    kbLocationsFound = kiesbeterLocations.Where(kbl => ((kbl.zip??"").Replace(" ", "")+(kbl.letters??"")).ToLower() == (pointLoc.Location.PostalCode??"").Replace(" ", "").ToLower() && kbl.house_number == pointLoc.Location.Number);
                }

                if (kbLocationsFound.Count() == 0)
                {
                    Logger.WriteLine("Processing:");
                    Logger.WriteLine("     OrgaCare: {0}", pointLoc.OrganizationCare.Name);
                    Logger.WriteLine("     LocaCare: {0}", pointLoc.LocationCare.Name);
                    Logger.WriteLine("     Orga: {0}", pointLoc.Organization.Name);
                    Logger.WriteLine("     Region: {0}", pointLoc.Region.Name);

                    Logger.WriteLine(" --> NOT FOUND");
                    notfound++;
                    if (pointLoc.Region.Name == "Landelijk") notfoundlandelijk++;

                    writeToExcel(sheet, "NOT FOUND", pointLoc, null);

                }
                else
                {
                    foreach (var kbLocation in kbLocationsFound)
                    {
                        Logger.WriteLine(" --> Found");
                        Logger.WriteLine("     OrgaCare: {0}", kbLocation.ConcernName());
                        Logger.WriteLine("     LocaCare: {0}", kbLocation.name);
                        kbLocation.foundmatch = true;
                        writeToExcel(sheet, "FOUND", pointLoc, kbLocation);
                    }
                }
                Logger.EmptyLine();

            }

            Logger.EmptyLine();
            Logger.WriteLine("Not found 'All': {0}", notfound);
            Logger.WriteLine("Not found 'Landelijk': {0}", notfoundlandelijk);
            Logger.EmptyLine();

            foreach (var kbLocation in kiesbeterLocations.Where(kbl => !kbl.foundmatch).OrderBy(kbl=>kbl.name))
            {
                Logger.WriteLine(" No match found for:");
                Logger.WriteLine("     OrgaCare: {0}", kbLocation.ConcernName());
                Logger.WriteLine("     LocaCare: {0}", kbLocation.name);
                writeToExcel(sheet, "NO MATCH", null, kbLocation);
            }

            Logger.EmptyLine();
            Logger.WriteLine("Not matched: {0}", kiesbeterLocations.Count(kbl => !kbl.foundmatch));
            Logger.EmptyLine();

            Logger.Dispose();

            using (var fileData = new FileStream(Path.Combine(Config.ResourceDirectory(), "out.xlsx"), FileMode.Create, FileAccess.Write))
            {
                workbook.Write(fileData);
            }

        }

        static int excelRowindex = 0;

        static void writeExcelHeader(NPOI.SS.UserModel.ISheet sheet)
        {

            var row = sheet.CreateRow(excelRowindex);
            
            var style = sheet.Workbook.CreateCellStyle();
            var font = sheet.Workbook.CreateFont();
            font.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.BOLD;
            style.SetFont(font);

            row.RowStyle = style;


            var colindex = 0;

            row.CreateCell(colindex++).SetCellValue("Status");
            row.CreateCell(colindex++).SetCellValue("OrgaCareID");
            row.CreateCell(colindex++).SetCellValue("OrgaCareName");
            row.CreateCell(colindex++).SetCellValue("LocaCareID");
            row.CreateCell(colindex++).SetCellValue("LocaCareName");
            row.CreateCell(colindex++).SetCellValue("Region");
            row.CreateCell(colindex++).SetCellValue("OrgaID");
            row.CreateCell(colindex++).SetCellValue("OrgaName");
            row.CreateCell(colindex++).SetCellValue("LocaID");
            row.CreateCell(colindex++).SetCellValue("LocaName");
            row.CreateCell(colindex++).SetCellValue("LocaCity");
            row.CreateCell(colindex++).SetCellValue("LocaStreetName");
            row.CreateCell(colindex++).SetCellValue("LocaNumber");
            row.CreateCell(colindex++).SetCellValue("LocaPostalCode");
            row.CreateCell(colindex++).SetCellValue("DepName");

            colindex = 16;
            row.CreateCell(colindex++).SetCellValue("KB_CareProviderID");
            row.CreateCell(colindex++).SetCellValue("KB_ConcernName");
            row.CreateCell(colindex++).SetCellValue("KB_Name");
            row.CreateCell(colindex++).SetCellValue("KB_City");
            row.CreateCell(colindex++).SetCellValue("KB_Street");
            row.CreateCell(colindex++).SetCellValue("KB_Housenumber");
            row.CreateCell(colindex++).SetCellValue("KB_Zip");

            excelRowindex++;
        }

        static void writeToExcel(NPOI.SS.UserModel.ISheet sheet, string label, KiesBeterConsole.Models.Point.CareFull pointLocation, KiesBeterConsole.Models.Kiesbeter.Location kiesbeterLocation)
        {
            var row = sheet.CreateRow(excelRowindex);
            var colindex = 0;

            row.CreateCell(colindex++).SetCellValue(label);

            if ( pointLocation != null )
            {
                row.CreateCell(colindex++).SetCellValue(pointLocation.OrganizationCare.OrganizationCareID);
                row.CreateCell(colindex++).SetCellValue(pointLocation.OrganizationCare.Name);
                row.CreateCell(colindex++).SetCellValue(pointLocation.LocationCare.LocationCareID);
                row.CreateCell(colindex++).SetCellValue(pointLocation.LocationCare.Name);
                row.CreateCell(colindex++).SetCellValue(pointLocation.Region.Name);
                row.CreateCell(colindex++).SetCellValue(pointLocation.Organization.OrganizationID);
                row.CreateCell(colindex++).SetCellValue(pointLocation.Organization.Name);
                row.CreateCell(colindex++).SetCellValue(pointLocation.Location.LocationID);
                row.CreateCell(colindex++).SetCellValue(pointLocation.Location.Name);
                row.CreateCell(colindex++).SetCellValue(pointLocation.Location.City);
                row.CreateCell(colindex++).SetCellValue(pointLocation.Location.StreetName);
                row.CreateCell(colindex++).SetCellValue(pointLocation.Location.Number);
                row.CreateCell(colindex++).SetCellValue(pointLocation.Location.PostalCode);
                row.CreateCell(colindex++).SetCellValue(pointLocation.Department.Name);
            }

            colindex = 16; // colindex makes it easier to add some in between, but be aware of this next 'section'
            if (kiesbeterLocation != null) 
            {
                row.CreateCell(colindex++).SetCellValue(kiesbeterLocation.careprovider_id);
                row.CreateCell(colindex++).SetCellValue(kiesbeterLocation.ConcernName());
                row.CreateCell(colindex++).SetCellValue(kiesbeterLocation.name);
                row.CreateCell(colindex++).SetCellValue(kiesbeterLocation.city);
                row.CreateCell(colindex++).SetCellValue(kiesbeterLocation.street);
                row.CreateCell(colindex++).SetCellValue(kiesbeterLocation.house_number);
                row.CreateCell(colindex++).SetCellValue(kiesbeterLocation.zip + kiesbeterLocation.letters);
            }


            excelRowindex++;
        }

    }

}
