﻿using System;
using System.Configuration;
using System.IO;

namespace KiesBeterConsole
{
    public static class Logger
    {

//using (myStream = new StreamWriter(sLogFilename, true))    {        myStream.WriteLine(sText);    }

        private static StreamWriter _sw = null;

        private static StreamWriter sw()
        {
            if (_sw == null)
            {
                _sw = new StreamWriter(Path.Combine(Config.ResourceDirectory(), "LogFile_" + DateTime.Now.ToString("ddMMyy_HHmm") + ".txt"));
                _sw.AutoFlush = true;
            }
            return _sw;
        }




        public static void WriteLine(string format, params object[] args)
        {
            System.Console.WriteLine(format, args);
            sw().WriteLine(String.Format(format,args));
        }
        public static void Write(string message)
        {
            Write(message, null);
        }
        public static void Write(string format, params object[] args)
        {
            System.Console.Write(format, args);
            sw().Write(String.Format(format,args));
        }
        public static void EmptyLine()
        {
            System.Console.WriteLine("");
        }
        public static void DoneAndWait()
        {
            System.Console.WriteLine("");
            System.Console.WriteLine("Done...");
            System.Console.WriteLine("Press any key...");
            System.Console.ReadKey();
        }


        #region --- DISPOSE ---

        private static bool _disposed;

        public static void Dispose()
        {
            Dispose(true);
        }

        private static void Dispose(bool disposing)
        {
            // If you need thread safety, use a lock around these  
            // operations, as well as in your methods that use the resource. 
            if (!_disposed)
            {
                if (disposing)
                {
                    if (_sw != null)
                    {
                        _sw.Flush();
                        _sw.Dispose();
                    }
                }

                _sw = null;
                _disposed = true;
            }
        }

        #endregion


    }
}
