
declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'A&G Thuiszorgbureau', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'A & G Thuiszorgbureau', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Dorpsweg',
       Number = '135',
       PostalCode = '3082LH',
       City = 'Rotterdam'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'ABC-Zorgcomfort', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'ABC-Zorgcomfort', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Kastanjelaan',
       Number = '2',
       PostalCode = '6921ES',
       City = 'Duiven'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Agathos, Rayon Midden-Nederland', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Agathos, Rayon Midden-Nederland', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Vendelier',
       Number = '59',
       PostalCode = '3905PD',
       City = 'Veenendaal'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Agathos, Rayon Waardenland', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Agathos, Rayon Waardenland', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Stationspark',
       Number = '500',
       PostalCode = '3364DA',
       City = 'Sliedrecht'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Agathos, Rayon Zuidhoek/Westland', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Agathos, Rayon Zuidhoek/Westland', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Ravenswaard',
       Number = '5',
       PostalCode = '3078PG',
       City = 'Rotterdam'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Archipel', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Archipel Berkenstaete', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'De Bontstraat',
       Number = '71',
       PostalCode = '5691SV',
       City = 'Son en Breugel'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Archipel', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Archipel Gagelbosch', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Gagelboschplein',
       Number = '200',
       PostalCode = '5654KS',
       City = 'Eindhoven'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'AriënsZorgpalet', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'AriënsZorgpalet, Klaaskateplein', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Klaaskateplein',
       Number = '70',
       PostalCode = '7523JZ',
       City = 'Enschede'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'AriënsZorgpalet', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'AriënsZorgpalet, Klaaskateweg', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Klaaskateweg',
       Number = '13',
       PostalCode = '7523DA',
       City = 'Enschede'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'AriënsZorgpalet', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'AriënsZorgpalet, Korteland', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'C.F. Klaarstraat',
       Number = '2',
       PostalCode = '7511CP',
       City = 'Enschede'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'AriënsZorgpalet', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'AriënsZorgpalet, Schakelafdeling G2&G4', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Ariënsplein',
       Number = '1',
       PostalCode = '7511JX',
       City = 'Enschede'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'AristoZorg', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Aristo Zorg', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Nijverheidsweg',
       Number = '31',
       PostalCode = '1851NW',
       City = 'Heiloo'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Arlero Thuiszorg BV', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Arlero Thuiszorg BV.', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Azieweg',
       Number = '13',
       PostalCode = '9407TC',
       City = 'Assen'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'AV Zorggroep', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Emma', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Raadsliedenstraat',
       Number = '120',
       PostalCode = '4142BV',
       City = 'Leerdam'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'AV Zorggroep', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Hof van Ammers', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Bernhardstraat',
       Number = '15',
       PostalCode = '2964BC',
       City = 'Groot-Ammers'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'AV Zorggroep', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Meesplein', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Burg. Meesplein',
       Number = '7',
       PostalCode = '4142AZ',
       City = 'Leerdam'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'AV Zorggroep', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Present Thuiszorg', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Postbus',
       Number = '33',
       PostalCode = '4233ZG',
       City = 'Ameide'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Avicen Amsterdam B.V.', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Avicen Amsterdam B.V.', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Derkinderenstraat',
       Number = '96',
       PostalCode = '1061VX',
       City = 'Amsterdam'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Avicen Haaglanden B.V.', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Avicen Haaglanden B.V.', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Heeswijkplein',
       Number = '78',
       PostalCode = '2531HE',
       City = 'Den Haag'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Avicen Midden-Holland B.V.', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Avicen Midden-Holland B.V.', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'C. Huygensstraat',
       Number = '121',
       PostalCode = '2802LV',
       City = 'Gouda'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Avicen Rotterdam B.V.', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Avicen Rotterdam B.V.', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Aelbrechtskade',
       Number = '195',
       PostalCode = '3023JM',
       City = 'Rotterdam'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Azora', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Azora Thuiszorg', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Industrieweg',
       Number = '115',
       PostalCode = '7060AA',
       City = 'Terborg'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Baalderborg Groep', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Avondlicht', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'de Tjalk',
       Number = '49',
       PostalCode = '7701LR',
       City = 'Dedemsvaart'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Betsaïda Thuiszorg', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Betsaida Thuiszorg', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Kraaiennest',
       Number = '120',
       PostalCode = '1104CH',
       City = 'Amsterdam Zuidoost'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'BrabantZorg', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'De Wellen', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Driek van Erpstraat',
       Number = '2',
       PostalCode = '5341AL',
       City = 'Oss'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'BTN', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Thuiszorg de Versterking', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Wagenmakerbaan',
       Number = '17',
       PostalCode = '1315BB',
       City = 'Almere'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'BTN', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Tugra thuiszorg', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Akkerwindestraat',
       Number = '1',
       PostalCode = '6835BZ',
       City = 'Arnhem'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'BTN', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Zorggroep Medelanders', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Roessingshbleekweg',
       Number = '171',
       PostalCode = '7522AH',
       City = 'Enschede'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Careyn', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Careyn Blankenburg', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Blankenburg',
       Number = '4',
       PostalCode = '3181BC',
       City = 'Rozenburg'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Careyn', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Careyn De Ark', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Herenstraat',
       Number = '85',
       PostalCode = '2291BD',
       City = 'Wateringen'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Careyn', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Careyn Dierenriem', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Polaris',
       Number = '60',
       PostalCode = '3225GT',
       City = 'Hellevoetsluis'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Careyn', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Careyn Pijletuinenhof', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Verdilaan',
       Number = '43',
       PostalCode = '2671VW',
       City = 'Naaldwijk'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Careyn', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Careyn Rozenhof', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Anjerlaan',
       Number = '49',
       PostalCode = '2671KH',
       City = 'Naaldwijk'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Careyn', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Careyn Zes Rozen', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Rozenlaan',
       Number = '9',
       PostalCode = '3202KC',
       City = 'Spijkenisse'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Careyn', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Stichting Flexicare', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Voorheuvel',
       Number = '42',
       PostalCode = '3701JG',
       City = 'Zeist'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Carintreggeland, De Elshof', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Carintreggeland, De Elshof', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Rustweg',
       Number = '1',
       PostalCode = '7608RM',
       City = 'ALMELO'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Catharina Stichting', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Stuifakkers', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Briggemandreef',
       Number = '2',
       PostalCode = '3235DZ',
       City = 'Rockanje'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Cedrah', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'd'' Amandelhof', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Fluiterlaan',
       Number = '52',
       PostalCode = '2903RL',
       City = 'Capelle aan den IJssel'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Chalcedoonzorg', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Chalcedoonzorg', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Kanaalweg',
       Number = '200',
       PostalCode = '9421TC',
       City = 'Bovensmilde'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'CIBG', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Olcea Nieuwe Zorglandschap', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Sportlaan Driene',
       Number = '8',
       PostalCode = '7552HA',
       City = 'Hengelo (O)'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Comfortzorg', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Comfortzorg AWBZ', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Abe Lenstraboulevard',
       Number = '23',
       PostalCode = '8448JA',
       City = 'HEERENVEEN'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Cordaan', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'De Drecht', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Niftrikhof',
       Number = '1',
       PostalCode = '1106SB',
       City = 'Amsterdam'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Cordaan', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'De Overtoomse Hof', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Jan Voermanstraat',
       Number = '35',
       PostalCode = '1061XB',
       City = 'Amsterdam'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Cordaan', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Marius ten Catehof', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Van Nijenrodeweg',
       Number = '945',
       PostalCode = '1081ES',
       City = 'Amsterdam'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Cordaan', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Nieuw Geuzenveld', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Cornelis Outshoornstraat',
       Number = '126',
       PostalCode = '1067HG',
       City = 'Amsterdam'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Cordaan', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Oostoever', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Bonnefantenstraat',
       Number = '10',
       PostalCode = '1064PN',
       City = 'Amsterdam'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Cordaan', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Osdorperhof', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Pieter Calandlaan',
       Number = '86',
       PostalCode = '1068NP',
       City = 'Amsterdam'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Curadomi, organisatie voor thuiszorg', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Curadomi, Rotterdam', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Puitstraat',
       Number = '101',
       PostalCode = '3192SH',
       City = 'Rotterdam'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'CuraMare', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'CuraMare Nieuw Rijsenbrugh, vh. De Samaritaan', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Sperwer',
       Number = '615',
       PostalCode = '3245VP',
       City = 'Sommelsdijk'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'CuraMare', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'CuraMare Thuiszorg', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Beatrixlaan',
       Number = '13',
       PostalCode = '3247AA',
       City = 'Dirksland'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'De Buurtzuster BV', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'De Buurtzuster BV', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Markeweg',
       Number = '18',
       PostalCode = '9306TE',
       City = 'Alteveer'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'De Hale', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'De Hale', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Nijewei',
       Number = '38',
       PostalCode = '9104DL',
       City = 'Damwâld'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'De Riethorst Stromenland', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Aernswaert', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Julianastraat',
       Number = '9',
       PostalCode = '4273CD',
       City = 'Hank'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'De Riethorst Stromenland', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'De Dotter', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'In ''t Rietwerf',
       Number = '101',
       PostalCode = '4273CG',
       City = 'Hank'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'De Riethorst Stromenland', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'De Kloosterhoeve', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Kloosterweg',
       Number = '1',
       PostalCode = '4941EG',
       City = 'Raamsdonksveer'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'De Riethorst Stromenland', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Logeerhuis De Veste', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Stadsweg',
       Number = '2',
       PostalCode = '4931ED',
       City = 'Geertruidenberg'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'De Wever', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Mater Misericordiae   | De Wever', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Kloosterstraat',
       Number = '10',
       PostalCode = '5038VP',
       City = 'Tilburg'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'De Woonmensen', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'De Vlier - Kleinschalig wonen', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Marconistraat',
       Number = '245',
       PostalCode = '7316PB',
       City = 'Apeldoorn'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'de Woonmensen', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Woonmensen Thuiszorg', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Deventerstraat',
       Number = '54',
       PostalCode = '7321CA',
       City = 'Apeldoorn'
 where LocationID = @LocID

GO

/*declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'De Zellingen', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Thuiszorg De Zellingen', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Meeuwensingel',
       Number = '1',
       PostalCode = '2903TA',
       City = 'Capelle aan den IJssel'
 where LocationID = @LocID

GO*/

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'De Zorgcirkel', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Middelwijck (Zorgcirkel)', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Prinses Beatrixpark',
       Number = '54',
       PostalCode = '1462JN',
       City = 'Middenbeemster'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'De Zorgmantel', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'De Zorgmantel', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Jeroen Boschlaan',
       Number = '28',
       PostalCode = '5056CW',
       City = 'Berkel- Enschot'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'De ZorgSter', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'De ZorgSter', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Paviljoensgracht',
       Number = '52',
       PostalCode = '2512BR',
       City = ''' s Gravenhage'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Emanuel Thuiszorg & Activering Centrum', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Emanuel Thuiszorg & Activerings Centrum', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Kruitberg',
       Number = '2006',
       PostalCode = '1104CA',
       City = 'Amsterdam Zuidoost'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Felay Thuiszorg', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Felay Thuiszorg', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Goudsteen',
       Number = '4',
       PostalCode = '3142CA',
       City = 'Maassluis'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Fener Zorg', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Fener Zorg', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Frits Ruysstraat',
       Number = '57',
       PostalCode = '3061MD',
       City = 'Rotterdam'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Florence', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Marienpark', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Mariënpark',
       Number = '1',
       PostalCode = '2264CH',
       City = 'Leidschendam'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Hervormde Stichting Bejaardenzorg Capelle aan den IJssel', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'De Vijverhof', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Reigerlaan',
       Number = '251',
       PostalCode = '2903LJ',
       City = 'Capelle aan den Ijssel'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Hervormde Stichting Sonneburgh', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Groene Kruisweg', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Groene Kruisweg',
       Number = '269',
       PostalCode = '3084LM',
       City = 'Rotterdam'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'HilverZorg', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'St. Joseph', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Minckelersstraat',
       Number = '81',
       PostalCode = '1223LD',
       City = 'HILVERSUM'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'HKZ', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Zorggroep Meander Thuisbegeleiding', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Postbus',
       Number = '76',
       PostalCode = '9640AB',
       City = 'Veendam'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Hospice en logeerhuis Valkenhaeghe', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Hospice en logeerhuis Valkenhaeghe', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Eerste Haagstraat',
       Number = '89',
       PostalCode = '5707XN',
       City = 'Helmond'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'HOZO', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Maronia', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Maronia',
       Number = '42',
       PostalCode = '2182RL',
       City = 'Hillegom'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Icare/Evean (Espria)', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'umcGroningen Thuis', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Hanzeplein',
       Number = '1',
       PostalCode = '9713GZ',
       City = 'Groningen'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Inez Zorg', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Inez Zorg', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Radboudweg',
       Number = '3',
       PostalCode = '3911BE',
       City = 'Rhenen'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Interzorg Noord-Nederland', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Kornoeljehof, verpleegafd. De Eekhoorn', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Tynaarlosestraat',
       Number = '56',
       PostalCode = '9481AD',
       City = 'Vries'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Joint venture Stichting Zorggroep Noordwest-Veluwe en ziekenhuis St Jansdal', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Klimop', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Wethouder Jansenlaan',
       Number = '90',
       PostalCode = '3844DG',
       City = 'Harderwijk'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'King Arthur Groep', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'King Arthur Groep Zorg', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Valutaboulevard',
       Number = '5',
       PostalCode = '3825BS',
       City = 'Amersfoort'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Kruseman Aretz Zorgvilla''s', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Kruseman Aretz Zorgvilla''s', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Rijksstraatweg',
       Number = '785',
       PostalCode = '2245CE',
       City = 'Wassenaar'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'landelijk netwerk van PrivaZorg steunpunten', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'PrivaZorg Utrecht e.o.', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'De Bouw',
       Number = '149',
       PostalCode = '3991SZ',
       City = 'Houten'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Laurens', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'De Bollaarshoeve', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Bollaarsdijk',
       Number = '1',
       PostalCode = '3233LB',
       City = 'Oostvoorne'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Laurens', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Laurens De Elf Ranken', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Middeldijkerplein',
       Number = '28',
       PostalCode = '2993DL',
       City = 'Barendrecht'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Laurens', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Laurens De Tuinen', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Oranjetuin',
       Number = '124',
       PostalCode = '2665VJ',
       City = 'Bleiswijk'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Laurens', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Laurens Den Hoogenban', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Burg. de Josselin de Jonglaan',
       Number = '35',
       PostalCode = '3042NC',
       City = 'Rotterdam'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Laurens', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Laurens Nijeveld', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'G.B. Shawplaats',
       Number = '100',
       PostalCode = '3068WD',
       City = 'Rotterdam'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Laurens', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Laurens Simeon en Anna', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Strevelsweg',
       Number = '350',
       PostalCode = '3075BZ',
       City = 'Rotterdam'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Leven & Zorg B.V.', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Leven & Zorg B.V., Locatie Utrecht', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Giessenplein',
       Number = '59',
       PostalCode = '3522KE',
       City = 'Utrecht'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Logeerhuis en hospice In Via', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Logeerhuis en hospice In Via', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Aalsterweg',
       Number = '289',
       PostalCode = '5644RE',
       City = 'Eindhoven'
 where LocationID = @LocID

GO

/*declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Marente', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Marente, Bolero', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Ravellaan',
       Number = '42',
       PostalCode = '2215LZ',
       City = 'Voorhout'
 where LocationID = @LocID

GO*/

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Meandergroep Zuid-limburg', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Dr. Calshof', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Achter Den Winkel',
       Number = '65',
       PostalCode = '6372SP',
       City = 'Landgraaf'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'MoniCare', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'MoniCare', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Hoogeweg',
       Number = '21',
       PostalCode = '6911KP',
       City = 'Pannerden'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Multi Care Thuiszorg', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Multi Care Thuiszorg', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Lau Mazirellaan',
       Number = '446',
       PostalCode = '2525WV',
       City = 'Den Haag'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Nova Zorgbemiddeling BV', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Nova Thuiszorg BV.', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Ampèrestraat',
       Number = '12',
       PostalCode = '1221GJ',
       City = 'Hilversum'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Nursing Care BV', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Nursing Care', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Nieuwe Daarlerveenseweg',
       Number = '10',
       PostalCode = '7671SK',
       City = 'Vriezenveen'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Omring', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'De Zeester', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Marsdiepstraat',
       Number = '300',
       PostalCode = '1784AW',
       City = 'Den Helder'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Omring', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Op ''t Snijdersveld', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Dr. Lohmanstraat',
       Number = '21',
       PostalCode = '1713TG',
       City = 'Obdam'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Opella', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Cornélie', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Hendrik van Poelwijcklaan',
       Number = '13',
       PostalCode = '6721PL',
       City = 'Bennekom'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Opella', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'De Valkenburcht', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Valkenburglaan',
       Number = '35',
       PostalCode = '6861AJ',
       City = 'Oosterbeek'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Opella', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Het Baken', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Barones van Lyndenlaan',
       Number = '69',
       PostalCode = '6721PK',
       City = 'Bennekom'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Opella', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Machtella', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Barones van Lyndenlaan',
       Number = '1',
       PostalCode = '6721PK',
       City = 'Bennekom'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Opella', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Opella Behandelcentrum', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Robert Kochlaan',
       Number = '1',
       PostalCode = '6721BE',
       City = 'Bennekom'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Opella', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Walraven', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Oud Breukelderweg',
       Number = '1',
       PostalCode = '6721PG',
       City = 'Bennekom'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Perspectief stichting voor welzijn, zorg en sociaal en cultureelwerk Beuningen', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Dagverzorging ''t Praothuis Perspectief', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Den Elt',
       Number = '17',
       PostalCode = '6644EA',
       City = 'Ewijk'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Privazorg AWBZ BV', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'PrivaZorg West Groningen', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Volmachtenlaan',
       Number = '17',
       PostalCode = '9331BK',
       City = 'Norg'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'PrivaZorg B.V.', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'PrivaZorg Zuid Friesland', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Boijlerweg',
       Number = '101',
       PostalCode = '8392ND',
       City = 'Boijl'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Privazorg', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'PrivaZorg Alblasserwaard en Drechtsteden', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Burgemeester Viezeestraat',
       Number = '1',
       PostalCode = '2971CA',
       City = 'BLESKENSGRAAF'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'PrivaZorg', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'PrivaZorg Arnhem e.o.', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Koningslinde',
       Number = '13',
       PostalCode = '6903CV',
       City = 'Zevenaar'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'PrivaZorg', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'PrivaZorg Baarn/Soest', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'P.C.Borstraat',
       Number = '21',
       PostalCode = '3515XX',
       City = 'Utrecht'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'PrivaZorg', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'PrivaZorg Breda e.o', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Weteringlaan',
       Number = '188',
       PostalCode = '5032XW',
       City = 'Tilburg'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'PrivaZorg', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'PrivaZorg Den Bosch-Oss', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Kromstraat',
       Number = '130',
       PostalCode = '5345AE',
       City = 'Oss'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'PrivaZorg', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'PrivaZorg Gelderse Vallei', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Valleistraat',
       Number = '117',
       PostalCode = '3902ZB',
       City = 'Veenendaal'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'PrivaZorg', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'PrivaZorg Leiden e.o.', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Schipholweg',
       Number = '103',
       PostalCode = '2316XC',
       City = 'Leiden'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Privazorg', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'PrivaZorg Ludgerus, wonen voor senioren', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Karel de Grotelaan',
       Number = '1',
       PostalCode = '7415LM',
       City = 'Deventer'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'PrivaZorg', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'PrivaZorg Maas en Waal', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Burgemeester raymakerslaan',
       Number = '159',
       PostalCode = '5361KL',
       City = 'Grave'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'PrivaZorg', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'PrivaZorg NAH Friesland', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Andringasingel',
       Number = '15',
       PostalCode = '9062GL',
       City = 'Oentsjerk'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'PrivaZorg', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'PrivaZorg Noord- en Midden Drenthe', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Volmachtenlaan',
       Number = '17',
       PostalCode = '9331BK',
       City = 'Norg'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'PrivaZorg', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'PrivaZorg Rivierenland e.o.', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Tielsestraat',
       Number = '6',
       PostalCode = '4021HC',
       City = 'Maurik'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'PrivaZorg', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'PrivaZorg Utrechtse Heuvelrug e.o', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Amersfoortseweg',
       Number = '50',
       PostalCode = '3941EN',
       City = 'Doorn'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'PrivaZorg', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'PrivaZorg Veluwemeer', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Marconiweg',
       Number = '22',
       PostalCode = '8071RA',
       City = 'Nunspeet'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'PrivaZorg', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'PrivaZorg West-Brabant/Tholen', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Oude Moerstraatsebaan',
       Number = '37',
       PostalCode = '4614RN',
       City = 'Bergen Op Zoom'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Privazorg', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'PrivaZorg Zeeuws-Vlaanderen', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = '''s Gravenhofplein',
       Number = '4',
       PostalCode = '4561AJ',
       City = 'Hulst'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'PrivaZorg', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'PrivaZorg Zuid-West Drenthe', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Boijlerweg',
       Number = '101',
       PostalCode = '8392ND',
       City = 'Boijl'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'proteion', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Proteion De Wachtpost', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Wachtpoststraat',
       Number = '85',
       PostalCode = '5914AE',
       City = 'Venlo'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Raphaëlstichting', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Raphaëlstichting - Rudolf Steiner Zorg  (RS RSZ )', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Nieuwe Parklaan',
       Number = '58',
       PostalCode = '2597LD',
       City = '''s-Gravenhage'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Reggeland Zorgvoorzieners', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Carintreggeland, Hengelo West Thuiszorg', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Roskamstraat',
       Number = '5',
       PostalCode = '7602JX',
       City = 'ALMELO'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Reggeland Zorgvoorzieners', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Carintreggeland, Thuiszorg Rijssen', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Wijnand Zeeuwstraat',
       Number = '26',
       PostalCode = '7462DE',
       City = 'Rijssen'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Roshni/Zorg en Hoop', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Roshni/Zorg en Hoop', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Postbus',
       Number = '24035',
       PostalCode = '2490AA',
       City = 'S GRAVENHAGE'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Royaal Zorg', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Royaal Zorg', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Televisiestraat',
       Number = '2',
       PostalCode = '2525KD',
       City = 'Den Haag'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Royal Topzorg BV', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Royal Topzorg', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Maanlander',
       Number = '14',
       PostalCode = '3824MP',
       City = 'Amersfoort'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Sensire', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Sensire, Kleinschalig wonen De Bloesem', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Meijersweg',
       Number = '31',
       PostalCode = '7161CK',
       City = 'Neede'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Sensire', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Sensire, Kleinschalig wonen De Garven', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Nieuwe Weg',
       Number = '42',
       PostalCode = '7261NL',
       City = 'Ruurlo'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Sensire', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Sensire, Kleinschalig wonen De Lindenhof', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Smidsstraat',
       Number = '48',
       PostalCode = '7251XS',
       City = 'Vorden'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Sevagram', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Oranjehof/Sevagram', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Vullingsweg',
       Number = '54',
       PostalCode = '6418HT',
       City = 'Heerlen'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Sint Jozef Wonen en Zorg', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Sint Jozef Wonen en Zorg', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Kapelkesweg',
       Number = '1',
       PostalCode = '5768AW',
       City = 'Meijel'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Sonneburgh', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Ravenswaard', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Ravenswaard',
       Number = '5',
       PostalCode = '3078PG',
       City = 'Rotterdam'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Splendid Care zorgprofessionals', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Splendid Care zorgprofessionals', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'De Binderij',
       Number = '65',
       PostalCode = '1321EC',
       City = 'Almere'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'St. Woonzorgboerderij Moriahoeve', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Woonzorgboerderij Moriahoeve', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'De Steeg',
       Number = '4',
       PostalCode = '3931PM',
       City = 'Woudenberg'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'STDO', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Thuiszorg De Orchidee', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Develstein',
       Number = '100',
       PostalCode = '1102AK',
       City = 'Amsterdam ZO'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Stichting Actief Zorg', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'ActiefZorg', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Nimrodstraat',
       Number = '27',
       PostalCode = '5042WX',
       City = 'Tilburg'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Stichting De Stouwe', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'De Stouwe Thuis', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Reestlaan',
       Number = '2',
       PostalCode = '7944BB',
       City = 'Meppel'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Stichting De Stouwe', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Kaailanden', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Heerengracht',
       Number = '21',
       PostalCode = '7941JJ',
       City = 'Meppel'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Stichting Gedi Verpleging', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Stichting Gedi Verpleging', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Samoastraat',
       Number = '28',
       PostalCode = '1339PG',
       City = 'Almere'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'stichting Groenhuysen', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Dagbehandeling Buitenhof', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Westelaarsestraat',
       Number = '62',
       PostalCode = '4725SC',
       City = 'Wouwse Plantage'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Stichting Hanzeheerd', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Stichting Hanzeheerd extramurale zorg Hattem/Heerde', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Bongerd',
       Number = '32',
       PostalCode = '8051VL',
       City = 'Hattem'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Stichting Icare', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'HdS Christelijke organisatie voor Zorg en Welzijn', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Amersfoortsestraat',
       Number = '18',
       PostalCode = '3772AC',
       City = 'Barneveld'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Stichting Icare', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Icare Verpleging en Verzorging (rayon Veluwe Zuid)', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Blankenstein',
       Number = '400',
       PostalCode = '7943PH',
       City = 'Meppel'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Stichting Icare', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Kloosterakker', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Rondgang',
       Number = '7',
       PostalCode = '9408MC',
       City = 'Assen'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Stichting Icare', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Missiehuis Vrijland', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Johannahoeve',
       Number = '2',
       PostalCode = '6861WJ',
       City = 'Oosterbeek'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Stichting Internationale Zorg aan Huis', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'IZAH Thuiszorg Dordrecht', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Voorstraat',
       Number = '68',
       PostalCode = '3311ER',
       City = 'Dordrecht'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Stichting Internationale Zorg aan Huis', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'IZAH Thuiszorg Rotterdam', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Oranjeboomstraat',
       Number = '179',
       PostalCode = '3071SH',
       City = 'Rotterdam'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Stichting MaasDuinen', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Vita', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Triangellaan',
       Number = '2',
       PostalCode = '5101AE',
       City = 'Dongen'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Stichting Pleyade', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Eimersstaete', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Eimerssingel-West',
       Number = '5',
       PostalCode = '6832EW',
       City = 'Arnhem'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Stichting Pleyade', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Hof van Bourtange', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Slochterenweg',
       Number = '2',
       PostalCode = '6835CB',
       City = 'Arnhem'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Stichting QuaRijn', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'De Tabakshof', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Het Bosje',
       Number = '11',
       PostalCode = '3921EH',
       City = 'Elst (Ut)'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Stichting QuaRijn', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'ZorgThuis Midden', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Gansfortstraat',
       Number = '4',
       PostalCode = '3961CR',
       City = 'Wijk bij Duurstede'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Stichting QuaRijn', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'ZorgThuis Oost', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Gansfortstraat',
       Number = '4',
       PostalCode = '3961CR',
       City = 'Wijk bij Duurstede'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Stichting QuaRijn', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'ZorgThuis West', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Gansfortstraat',
       Number = '4',
       PostalCode = '3961CR',
       City = 'Wijk bij Duurstede'
 where LocationID = @LocID

GO

/* declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Stichting RAZ', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'RAZ residentiële thuiszorg  Schalkwijk', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Provincialeweg',
       Number = '69',
       PostalCode = '3998JK',
       City = 'Schalkwijk'
 where LocationID = @LocID

GO */

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Stichting Residentiële & Ambulante Zorg', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'RAZ ambulante thuiszorg Schalkwijk', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Provincialeweg',
       Number = '69',
       PostalCode = '3998JK',
       City = 'Schalkwijk'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Stichting Rijn & Valleizorg', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Rijn en Valleizorg', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Majoorlandzaatweg',
       Number = '5',
       PostalCode = '3911AW',
       City = 'Rhenen'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Stichting S.M.N. Thuiszorg', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Service Modern Nederland Thuiszorg', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Bijlmerdreef',
       Number = '123',
       PostalCode = '1102BP',
       City = 'Amstedam'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Stichting Sint Jacob', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Klein Belgie', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Belgielaan',
       Number = '3',
       PostalCode = '2034AV',
       City = 'Haarlem'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Stichting tanteLouise-Vivensis', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Hospice de Markies', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Kardinaal de Jonglaan',
       Number = '1',
       PostalCode = '4624ES',
       City = 'Bergen op Zoom'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Stichting tanteLouise-Vivensis', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Kardinaal de Jonglaan', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Kardinaal de Jonglaan',
       Number = '15',
       PostalCode = '4624ES',
       City = 'Bergen op Zoom'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Stichting tanteLouise-Vivensis', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Residentie Moermont', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Veilingdreef',
       Number = '6',
       PostalCode = '4614RX',
       City = 'Bergen op Zoom'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Stichting tanteLouise-Vivensis', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Serviceflat De Schelde', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Borgvlietsedreef',
       Number = '150',
       PostalCode = '4615EJ',
       City = 'Bergen op Zoom'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Stichting tanteLouise-Vivensis', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Serviceflat Meilust', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Bloemendaal',
       Number = '61',
       PostalCode = '4614CZ',
       City = 'Bergen op Zoom'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Stichting Thuiszorg Helpende Hand', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Stichting Thuiszorg Helpende Hand', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Frederik van Eedenstraat',
       Number = '1',
       PostalCode = '5921BE',
       City = 'VENLO'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Stichting Vilente', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Vilente De Sonnenberg', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Sonnenberglaan',
       Number = '4',
       PostalCode = '6861AM',
       City = 'Oosterbeek'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Stichting Volckaert', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Dongepark', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Dongepark',
       Number = '1',
       PostalCode = '5102DB',
       City = 'Dongen'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Stichting Volckaert', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Oosterheem', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Aletta Jacobspad',
       Number = '1',
       PostalCode = '4901MN',
       City = 'Oosterhout'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Stichting Volckaert', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Thuiszorg Dongen-Volckaert', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Dongepark',
       Number = '1',
       PostalCode = '5104DB',
       City = 'Dongen'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Stichting Volckaert', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Thuiszorg Oosterhout-Volckaert', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Aletta Jacobspad',
       Number = '1',
       PostalCode = '4901MN',
       City = 'Oosterhout'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Stichting Voormekaar', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Dagverpleging Kulturhus d''n Dulper', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Pastoor Schoenmakerstraat',
       Number = '5',
       PostalCode = '6657CB',
       City = 'Boven Leeuwen'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Stichting Voormekaar', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Dagverzorging Druten', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Ringelenberg',
       Number = '7',
       PostalCode = '6654BW',
       City = 'Afferden'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Stichting Voormekaar', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Dagverzorging West Maas en Waal', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Schoolstraat',
       Number = '7',
       PostalCode = '6626AC',
       City = 'Alphen'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Stichting Voormekaar', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Stichting Voormekaar', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Leliestraat',
       Number = '17',
       PostalCode = '6658XN',
       City = 'Beneden Leeuwen'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Stichting WarmThuis', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'WarmThuis De Hulst', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Noordschermerdijk',
       Number = '5',
       PostalCode = '1842EL',
       City = 'Oterleek'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Stichting WarmThuis', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'WarmThuis, Klein Suydermeer', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'De Omloop',
       Number = '1',
       PostalCode = '1652CM',
       City = 'Zuidermeer'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Stichting Woonzorgcentra Flevoland', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Woonzorg Flevoland -Thuiszorg', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Meentweg',
       Number = '12',
       PostalCode = '8224BP',
       City = 'Lelystad'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Stichting Woonzorgcentra Flevoland', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Woonzorgcentrum Hanzeborg', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Koningsbergenstraat',
       Number = '210',
       PostalCode = '8232DC',
       City = 'Lelystad'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Stichting Woonzorgcentrum Raffy', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Lâle', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Boschstraat',
       Number = '57',
       PostalCode = '4811GC',
       City = 'Breda'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Stichting Zorg Ondersteuning Nederland', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'ZONZORG Almere', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Händelstraat',
       Number = '18',
       PostalCode = '1323AD',
       City = 'ALMERE'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Stichting Zorg Ondersteuning Nederland', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'ZONZORG Amsterdam', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Middenweg',
       Number = '183',
       PostalCode = '1098AM',
       City = 'Amsterdam'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Stichting Zorg Ondersteuning Nederland', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'ZONZORG Gouda', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Willem en Marialaan',
       Number = '8',
       PostalCode = '2805AR',
       City = 'GOUDA'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Stichting Zorg Ondersteuning Nederland', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'ZONZORG Zeist', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Rozenstraat',
       Number = '17',
       PostalCode = '3702VK',
       City = 'ZEIST'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Stichting ZorgAccent', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'ZorgAccent Zorg Thuis locatie Almelo', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Bellavistastraat',
       Number = '3',
       PostalCode = '7604AD',
       City = 'Almelo'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Stichting ZorgBedrijf Zuid Holland Heenvliet', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Stichting ZorgBedrijf Zuid Holland Heenvliet', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Korte Welleweg',
       Number = '1',
       PostalCode = '3218AZ',
       City = 'HEENVLIET'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Stichting ZorgBedrijf Zuid Holland', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Stichting ZorgBedrijf Zuid Holland', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'De La Reyweg',
       Number = '409',
       PostalCode = '2571EK',
       City = 'Den Haag'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Stichting Zorggroep Noordwest-Veluwe', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'De Amaniet', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Varenlaan',
       Number = '129',
       PostalCode = '3852CP',
       City = 'Ermelo'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Stichting Zorggroep Tellus', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Zorggroep Tellus locatie De Groene Vecht', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Vechtstraat',
       Number = '250',
       PostalCode = '1784TG',
       City = 'Den Helder'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Stichting Zorgplatform Geranós', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Zorgplatform Geranós', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Utrechtseweg',
       Number = '34',
       PostalCode = '3402PL',
       City = 'IJsselstein'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Stichting Zorgrésidence Regina', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Stichting Résidence Regina', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Arnhemsestraatweg',
       Number = '21',
       PostalCode = '6886NC',
       City = 'Velp'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Stichting Zorgwaard', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Sabina', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Randweg',
       Number = '25',
       PostalCode = '3263RA',
       City = 'Oud Beijerland'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Temel Zorg', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Temelzorg', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Hastelweg',
       Number = '129',
       PostalCode = '5652CJ',
       City = 'Eindhoven'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Thuis in Zorg', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Thuis in Zorg', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Oudedijk',
       Number = '33',
       PostalCode = '3062AC',
       City = 'Rotterdam'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Thuiszorg Bureau Care 4 Life', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Thuiszorg Bureau Care 4 Life', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Wilhelminastraat',
       Number = '25',
       PostalCode = '2941CA',
       City = 'Lekkerkerk'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Thuiszorg De Lage Landen', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Thuiszorg De Lage Landen', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Kerkstraat',
       Number = '32',
       PostalCode = '4191AB',
       City = 'Geldermalsen'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Thuiszorg Hart voor Nederland', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'PrivaZorg Hart voor Nederland', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Van Bijkershoeklaan',
       Number = '328',
       PostalCode = '3527XL',
       City = 'Utrecht'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Thuiszorg Hartenwens', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Thuiszorg Hartenwens', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Leidsevaart',
       Number = '396',
       PostalCode = '2014HM',
       City = 'Haarlem'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Thuiszorg Matilda', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Matilda', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Smederijstraat',
       Number = '2',
       PostalCode = '4814DB',
       City = 'Breda'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Thuiszorg Mozaiek', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Mozaiek', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Schaarbergenstraat',
       Number = '190',
       PostalCode = '2573CB',
       City = '''s-Gravenhage'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Thuiszorg Noord-Nederland', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Thuiszorg Noord-Nederland', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Leonard Springerlaan',
       Number = '9',
       PostalCode = '9727KB',
       City = 'Groningen'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Thuiszorg Oost van Zorgcentra Pantein', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Thuiszorg Oost van Zorgcentra Pantein', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Dokter Kopstraat',
       Number = '2',
       PostalCode = '5835DV',
       City = 'BEUGEN'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Thuiszorg Samen Verder BV', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Thuiszorg Samen Verder BV', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Vossenbeemd',
       Number = '111',
       PostalCode = '5705CL',
       City = 'Helmond'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Thuiszorg Zorgzaam', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Thuishulp Zorgzaam', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Vechtensteinlaan',
       Number = '16',
       PostalCode = '3555XS',
       City = 'Utrecht'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Thuiszorgmakelaar', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Thuiszorgmakelaar', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Bredaseweg',
       Number = '226',
       PostalCode = '5038NM',
       City = 'Tilburg'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'TriviumMeulenbeltZorg', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'TMZ Borne', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = '''t Dijkhuis',
       Number = '',
       PostalCode = '7622CM',
       City = 'Borne'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'TriviumMeulenbeltZorg', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'TMZ Hengelo (o)', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Boerhaavelaan',
       Number = '85',
       PostalCode = '7555BK',
       City = 'Hengelo (O)'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'TriviumMeulenbeltZorg', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'TMZ Het Hof', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'P.C. Borstlaan',
       Number = '123',
       PostalCode = '7555SL',
       City = 'Hengelo'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'TriviumMeulenbeltZorg', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'TMZ Tubbergen', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Borgstaete',
       Number = '31',
       PostalCode = '7651LB',
       City = 'Tubbergen'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'TriviumMeulenbeltZorg', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'TMZ Twenterand', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Koningsweg',
       Number = '24',
       PostalCode = '7672GD',
       City = 'Vriezenveen'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'TriviumMeulenbeltZorg', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'TMZ Weemelanden', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Koningsweg',
       Number = '24',
       PostalCode = '7672GD',
       City = 'Vriezenveen'
 where LocationID = @LocID

GO

/*declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Verpleeghuis De Stromenhof', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Verpleeghuis De Stromenhof', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Scheldestraat',
       Number = '76',
       PostalCode = '2515TG',
       City = 'Den Haag'
 where LocationID = @LocID

GO*/

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Verpleegunit Vredewold', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Verpleegunit Vredewold', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Pulvertorenstraat',
       Number = '1',
       PostalCode = '9351BP',
       City = 'LEEK'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Viattence', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Wezep', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Waterloopweg',
       Number = '2',
       PostalCode = '8091EA',
       City = 'Wezep'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Vierhuizen', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Vierhuizen', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Rijnstraat',
       Number = '1',
       PostalCode = '6882LR',
       City = 'VELP'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Vivent', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Vivent Hof van Hintham', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Vincent van Goghlaan',
       Number = '1',
       PostalCode = '5246GA',
       City = '''s-Hertogenbosch'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Vivium Zorggroep', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'De Gooise Warande', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Mezenlaan',
       Number = '19',
       PostalCode = '1403BR',
       City = 'Bussum'
 where LocationID = @LocID

GO

/*declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Vivre', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Hagerpoort', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Laurierhoven',
       Number = '30',
       PostalCode = '6225GA',
       City = 'Maastricht'
 where LocationID = @LocID

GO*/

/*declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Vivre', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Vivre, De Mins', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Ponjaardruwe',
       Number = '91',
       PostalCode = '6218SM',
       City = 'Maastricht'
 where LocationID = @LocID

GO*/

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Werkt voor Ouderen', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Ter Reede', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Vondellaan',
       Number = '21',
       PostalCode = '4382BV',
       City = 'Vlissingen'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Woon- en zorgvoorziening Bloemendael', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Woon- en zorgvoorziening Bloemendael', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Nassaulaan',
       Number = '80',
       PostalCode = '3743CE',
       City = 'BAARN'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Woonzorggroep Samen', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Thuiszorg Samen', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Kogerlaan',
       Number = '36',
       PostalCode = '1741DA',
       City = 'Schagen'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Woonzorggroep Samen', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Woon- en zorgcomplex De Trambaan', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Koperwiek',
       Number = '30',
       PostalCode = '1733BA',
       City = 'Nieuwe Niedorp'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'www.zorgiseenkeuze.nl', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Sanare Thuiszorg Barendrecht', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'gebroken meeldijk',
       Number = '123',
       PostalCode = '2991cg',
       City = 'Barendrecht'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'www.zorgiseenkeuze.nl', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Sanare Thuiszorg Nieuwerkerk aan den ijssel', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'raadhuisplein',
       Number = '5',
       PostalCode = '2914km',
       City = 'nieuwerkerk aan den ijssel'
 where LocationID = @LocID

GO

/*declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Zonnehuisgroep IJssel-Vecht', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Thuiszorg Zonnehuisgroep IJssel-Vecht', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Bonkenhavestraat',
       Number = '84',
       PostalCode = '8043TC',
       City = 'Zwolle'
 where LocationID = @LocID

GO*/

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Zonnehuisgroep Noord', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Zonnehuis Thuis', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Atlasstraat',
       Number = '8',
       PostalCode = '9801VA',
       City = 'Zuidhorn'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Zonnehuisgroep Vlaardingen', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'De Laurenburg', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Dillenburgsingel',
       Number = '7',
       PostalCode = '3136EA',
       City = 'Vlaardingen'
 where LocationID = @LocID

GO

/*declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'zonnehuisgroepnoord', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Zonnehuis Gockingaheem', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Nanno Mulderstraat',
       Number = '22',
       PostalCode = '9635CL',
       City = 'Noordbroek'
 where LocationID = @LocID

GO*/

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Zorg op Maat B.V.', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Zorg op Maat BV.', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Oostzeedijk',
       Number = '232',
       PostalCode = '3063BP',
       City = 'Rotterdam'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Zorg Thuis', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Zorg Thuis', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'De Hoge Hof',
       Number = '1',
       PostalCode = '6674BR',
       City = 'Herveld'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Zorgbruggen Thuiszorg', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Zorgbruggen Thuiszorg', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Neterselsedijk',
       Number = '22',
       PostalCode = '5094BD',
       City = 'Lage Mierde'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Zorgcentra Pantein', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'De Schittering', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Burgemeester van Hultenstraat',
       Number = '6',
       PostalCode = '5443AR',
       City = 'Haps'
 where LocationID = @LocID

GO

/*declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Zorgcentrum het Bildt', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Zorgcentrum het Bildt', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Beuckelaerstraat',
       Number = '25',
       PostalCode = '9076DA',
       City = 'St.-Annaparochie'
 where LocationID = @LocID

GO*/

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Zorgcentrum Valentino', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Zorgcentrum Valentino', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Saffatinstraat',
       Number = '86',
       PostalCode = '4021HN',
       City = 'MAURIK'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Zorgfamilie', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'ZorgFamilie', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Spoorweghaven',
       Number = '323',
       PostalCode = '3071ZE',
       City = 'Rotterdam'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Zorggroep Almere', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Castrovalva', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Juan Grisstraat',
       Number = '50',
       PostalCode = '1328SV',
       City = 'Almere'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Zorggroep Almere', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Flevoburen', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Hospitaalweg',
       Number = '1',
       PostalCode = '1315RA',
       City = 'Almere'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Zorggroep Almere', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Woonzorgcentrum Vizier', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Duitslandstraat',
       Number = '1',
       PostalCode = '1363BG',
       City = 'Almere'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Zorggroep Almere', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Zephyr', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Fiep Westendorpstraat',
       Number = '3',
       PostalCode = '1336CR',
       City = 'Almere'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Zorggroep Apeldoorn en omstreken', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'De Vier Dorpen', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Loenenseweg',
       Number = '39',
       PostalCode = '7361GB',
       City = 'Beekbergen'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Zorggroep Apeldoorn en omstreken', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Mandala', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Sportlaan',
       Number = '2',
       PostalCode = '7312TV',
       City = 'Apeldoorn'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Zorggroep Elde', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Sefkat', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Bosscheweg',
       Number = '113',
       PostalCode = '5282WV',
       City = 'Boxtel'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Zorggroep Ena Extramuraal', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Zorggroep Ena Extramuraal', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Van den Berglaan',
       Number = '30',
       PostalCode = '3781GH',
       City = 'Voorthuizen'
 where LocationID = @LocID

GO

/*declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Zorggroep Hof en Hiem', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Hof en Hiem Thuisservice', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'De Oergong',
       Number = '2',
       PostalCode = '8521GA',
       City = 'Sint Nicolaasga'
 where LocationID = @LocID

GO*/

/*declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Zorggroep Hof en Hiem', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Vegelin State', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Hardraversweg',
       Number = '4',
       PostalCode = '8501CM',
       City = 'Joure'
 where LocationID = @LocID

GO*/

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Zorggroep Meander', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Borg Westerwolde', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Dr. Bekenkampstraat',
       Number = '41',
       PostalCode = '9561RD',
       City = 'Ter Apel'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Zorggroep Oude en Nieuwe Land', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Kerspelhof', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Klaas Muisstraat',
       Number = '12',
       PostalCode = '8375BX',
       City = 'Oldemarkt'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Zorggroep Oude en Nieuwe Land', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Zonnewiede', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Ds. J.J. Ketstraat',
       Number = '43',
       PostalCode = '8355DE',
       City = 'Giethoorn'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Zorggroep Raalte', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Brugstede', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Meester Gorisstraat',
       Number = '3',
       PostalCode = '8151AG',
       City = 'Lemelerveld'
 where LocationID = @LocID

GO

/*declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Zorggroep Rijnmond', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Atrium', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Karel Doormanstraat',
       Number = '343',
       PostalCode = '3012GH',
       City = 'Rotterdam'
 where LocationID = @LocID

GO*/

/*declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Zorggroep Rijnmond', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'De Burcht', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Van Moorselplaats',
       Number = '1',
       PostalCode = '3067SH',
       City = 'Rotterdam'
 where LocationID = @LocID

GO*/

/*declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Zorggroep Rijnmond', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'De Koningshof', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Spinozaweg',
       Number = '451',
       PostalCode = '3076EW',
       City = 'Rotterdam'
 where LocationID = @LocID

GO*/

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Zorggroep Rijnmond', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Pniël', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Oudedijk',
       Number = '15',
       PostalCode = '3062AB',
       City = 'Rotterdam'
 where LocationID = @LocID

GO

/*declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Zorggroep Rijnmond', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Siloam', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Kruisnetlaan',
       Number = '410',
       PostalCode = '3192KE',
       City = 'Hoogvliet'
 where LocationID = @LocID

GO*/

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Zorggroep Rijnmond', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Slingedael', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Dreischorstraat',
       Number = '85',
       PostalCode = '3086PP',
       City = 'Rotterdam'
 where LocationID = @LocID

GO

/*declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Zorggroep Rijnmond', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Tiendhove', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Patrijzenstraat',
       Number = '57',
       PostalCode = '2922GN',
       City = 'Krimpen aan den IJssel'
 where LocationID = @LocID

GO*/

/*declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Zorggroep Rijnmond', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Waelestein', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Schulpplein',
       Number = '50',
       PostalCode = '3082PX',
       City = 'Rotterdam'
 where LocationID = @LocID

GO*/

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Zorggroep Sint Maarten', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Zorggroep Sint Maarten, Sint Jozef Weerselo', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'De Aanleg',
       Number = '2',
       PostalCode = '7595AP',
       City = 'Weerselo'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Zorggroep Solis', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Spikvoorde', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'M.C. Escherweg',
       Number = '2',
       PostalCode = '7425RJ',
       City = 'Deventer'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Zorggroep Tangenborgh, Emmen', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'De Olmen', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Vossepad',
       Number = '98',
       PostalCode = '7822BD',
       City = 'Emmen'
 where LocationID = @LocID

GO

/*declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Zorggroep Tellens', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Noorderhoek', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Napjusstraat',
       Number = '114',
       PostalCode = '8602TE',
       City = 'Sneek'
 where LocationID = @LocID

GO*/

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Zorggroep Ter Weel Goes', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Maria-Oord', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Eendracht',
       Number = '1',
       PostalCode = '4417CA',
       City = 'Hansweert'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Zorggroep Ter Weel Goes', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Moerzicht', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Moerplein',
       Number = '73',
       PostalCode = '4401HZ',
       City = 'Yerseke'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Zorggroep Ter Weel Goes', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Nieulande', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Willem Kosterlaan',
       Number = '1',
       PostalCode = '4413CP',
       City = 'Krabbendijke'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Zorggroep Ter Weel Goes', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Ter Weel Krabbendijke', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Willem Kosterlaan',
       Number = '1',
       PostalCode = '4413CP',
       City = 'Krabbendijke'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Zorggroep Ter Weel', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Gasthuis', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Kievitlaan',
       Number = '64',
       PostalCode = '4461RB',
       City = 'Goes'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Zorggroep Ter Weel', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Hof Cruninghe', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Lange Viele',
       Number = '41',
       PostalCode = '4416CE',
       City = 'Kruiningen'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Zorggroep Ter Weel', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'St. Maarten in de Groe', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'JLaantje van Westdorp',
       Number = '1',
       PostalCode = '4462AS',
       City = 'Goes'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Zorgnet ThuisBest en V&V Dagcentrum Palulu', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Zorgnet ThuisBest', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Paasheuvelweg',
       Number = '15',
       PostalCode = '1105BE',
       City = 'Amsterdam'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Zorgorganisatie Het Hoge Veer', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Hoge Veer Rivierzicht', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Scheepswerflaan',
       Number = '47',
       PostalCode = '4941GZ',
       City = 'Raamsdonksveer'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Zorgpartners Friesland', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'De Batting', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Achlumerdijk',
       Number = '2',
       PostalCode = '8862AJ',
       City = 'Harlingen'
 where LocationID = @LocID

GO

/*declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Zorgpartners Friesland', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Friesma State', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Oostergoostraat',
       Number = '52',
       PostalCode = '9001CM',
       City = 'Grou'
 where LocationID = @LocID

GO*/

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Zorgpartners Midden-Holland', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'De Riethoek', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Ridder van Catsweg',
       Number = '258',
       PostalCode = '2804RS',
       City = 'Gouda'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'ZORGSERVICETHUIS', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'ZorgServiceThuis', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Wolfhezerweg',
       Number = '120',
       PostalCode = '6874AW',
       City = 'Wolfheze'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Zorgstart', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Zorgstart', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Lotterstraat',
       Number = '34',
       PostalCode = '2021TG',
       City = 'Haarlem'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Zorgstroom', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Zorgstroom kleinschalig wonen', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Kraayerthavenstraat',
       Number = '1',
       PostalCode = '4339BV',
       City = 'Nieue- en Sint Joosland'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'ZorgThuis Gorredijk', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'ZorgThuis Gorredijk', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Stationsweg',
       Number = '76',
       PostalCode = '8401DT',
       City = 'Gorredijk'
 where LocationID = @LocID

GO

/*declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Zorgverlening De Leijgraaf B.V.', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Zorgverlening de Leijgraaf B.V.', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Schakelplein',
       Number = '13',
       PostalCode = '5408AW',
       City = 'Uden'
 where LocationID = @LocID

GO*/

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Zorgvilla''s Twente', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Huize Den Oostenborgh', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Putstraat',
       Number = '7',
       PostalCode = '7631GB',
       City = 'Ootmarsum'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Zorgvilla''s Twente', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Zorgvilla Meijling', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Stationsstraat',
       Number = '74',
       PostalCode = '7622LZ',
       City = 'Borne'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Zorgwaard', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Bliekenhof', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Kerkstraat',
       Number = '30',
       PostalCode = '3286AK',
       City = 'Klaaswaal'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Zorgwaard', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Hofwijk', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Hertoglaan',
       Number = '1',
       PostalCode = '3271TL',
       City = 'Mijnsheerenland'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Zorgwaard', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Korenschoof', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Wouter van den Walestraat',
       Number = '1',
       PostalCode = '3274CR',
       City = 'Heinenoord'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Zorgwaard', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Poorthuis', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Maximastraat',
       Number = '4',
       PostalCode = '3264WB',
       City = 'Nieuw-beijerland'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'Zorgwaard', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = '''t Huys te Hoecke', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Zomerplein',
       Number = '15',
       PostalCode = '3297SE',
       City = 'Puttershoek'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'ZuidOostZorg', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'Lijtehiem', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Dr. Prakkenleane',
       Number = '14',
       PostalCode = '9247BZ',
       City = 'Ureterp'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'ZZG zorggroep', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'De Pas, de Waaij en de Elstweg', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Passedwarsstraat',
       Number = '69',
       PostalCode = '6601AR',
       City = 'Wijchen'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'ZZG zorggroep', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'ZZG Gespecialiseerde verpleging', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Postbus',
       Number = '6810',
       PostalCode = '6503GH',
       City = 'Nijmegen'
 where LocationID = @LocID

GO

declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = 'ZZG zorggroep', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = 'ZZG Thuisbegeleiding', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = 'Postbus',
       Number = '6810',
       PostalCode = '6503GH',
       City = 'Nijmegen'
 where LocationID = @LocID

GO
