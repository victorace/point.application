﻿using System;
using System.Collections.Generic;

namespace KiesBeterConsole
{
    public static class Extensions
    {
        public static string ConcernName(this KiesBeterConsole.Models.Kiesbeter.Location location)
        {
            if (location.concern == null || location.concern.name == null)
                return location.name ?? "";
            else
                return location.concern.name;
        }

        public static int ColNum(this char columnLetter)
        {
            return ((int)columnLetter) - 65;
        }

        public static int ColNum(this char[] columnLetters)
        {
            int num = 0;
            int pos = 0;
            foreach (char columnLetter in columnLetters)
            {
                num += columnLetter.ColNum() + pos * 26;
                pos++;
            }

            return num;
        }


        public static int GetIntValue(this NPOI.SS.UserModel.IRow row, char[] column)
        {
            double val = -1;
            try
            {
                val = row.GetCell(column.ColNum()).NumericCellValue;
            }
            catch {  /* ignore */ }

            return (int)val;
        }

        public static string GetValueSQL(this NPOI.SS.UserModel.IRow row, char[] column)
        {
            return GetValue(row, column).Replace("'", "''");
        }

        public static string GetValue(this NPOI.SS.UserModel.IRow row, char[] column)
        {
            string val = "";
            try
            {
                val = row.GetCell(column.ColNum()).StringCellValue;
            }
            catch
            {
                try
                {
                    double numval = row.GetCell(column.ColNum()).NumericCellValue;
                    val = numval.ToString();
                }
                catch { /* ignore */ }
            }
            return val;
        }

        public static bool IsEmpty(this NPOI.SS.UserModel.IRow row, char[] column)
        {
            return String.IsNullOrWhiteSpace(GetValue(row, column));
        }

        public static string GetValue(this NPOI.SS.UserModel.ISheet sheet, int row, char[] column)
        {
            string val = "";
            if (sheet.GetRow(row) != null)
            {
                try
                {
                    val = sheet.GetRow(row).GetCell(column.ColNum()).StringCellValue;
                }
                catch
                {
                    try
                    {
                        double numval = sheet.GetRow(row).GetCell(column.ColNum()).NumericCellValue;
                        val = numval.ToString();
                    }
                    catch { /* ignore */ }
                }
            }
            return val;
        }

        public static bool IsEmpty(this NPOI.SS.UserModel.ISheet sheet, int row, char[] column)
        {
            return String.IsNullOrWhiteSpace(GetValue(sheet, row, column));
        }

    }

}



