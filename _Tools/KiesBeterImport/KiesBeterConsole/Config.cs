﻿using System.Configuration;

namespace KiesBeterConsole
{
    public class Config
    {
        public static string ApiKey()
        {
            return ConfigurationManager.AppSettings["ApiKey"] ?? "ApiKey1/ApiKey2 not set in app.config !!!";
        }
        public static string ResourceDirectory()
        {
            return ConfigurationManager.AppSettings["ResourceDirectory"] ?? "ResourceDirectory not set in app.config !!!";
        }
    }
}
