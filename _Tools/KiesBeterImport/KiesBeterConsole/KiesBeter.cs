﻿using KiesBeterConsole.Models.Kiesbeter;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace KiesBeterConsole
{
    class KiesBeter
    {

        public List<T> ReadLocationLists<T>()
        {
            List<T> locationLists = new List<T>();

            foreach (FileInfo file in new DirectoryInfo(Config.ResourceDirectory()).GetFiles("Locations_*.json"))
            {
                if (!Path.GetFileNameWithoutExtension(file.FullName).EndsWith("Extended"))
                {
                    string s = File.ReadAllText(file.FullName);

                    var locationlist = JsonConvert.DeserializeObject<T>(s);
                    locationLists.Add(locationlist);
                }
            }
            return locationLists;

        }

        public void RetrieveLocationLists(bool forceNew = false)
        {
            string url = "http://www.kiesbeter.nl/open-data/api/care/locations/{0}/?apikey={1}";

            int[] careProviderCategoryIDs = new[] { 4, 5 }; // We're only allowed to get 4 and 5


            WebClient wc = new WebClient();
            wc.Encoding = Encoding.UTF8;

            foreach (var careProviderCategoryID in careProviderCategoryIDs)
            {
                string output = Path.Combine(Config.ResourceDirectory(), String.Format("Locations_{0}.json", careProviderCategoryID));
                if (!File.Exists(output) || forceNew)
                {
                    Logger.EmptyLine();
                    Logger.WriteLine("Getting LocationList: {0}", careProviderCategoryID);
                    try
                    {
                        string s = wc.DownloadString(String.Format(url, careProviderCategoryID, Config.ApiKey()));
                        Logger.WriteLine("Writing to file: {0}", output);
                        File.WriteAllText(output, s);
                        Logger.WriteLine("Done writing {0} bytes", s.Length);
                    }
                    catch (UnauthorizedAccessException uex)
                    {
                        Logger.EmptyLine();
                        Logger.WriteLine("File {0} is readonly!", output);
                        Logger.WriteLine("Probably it isn't checked-out from TFS.");
                    }
                    catch (Exception ex)
                    {
                        Logger.EmptyLine();
                        Logger.WriteLine("Error while retrieving data for {0}", output);
                        Logger.WriteLine(ex.Message);
                    }

                }
                else
                {
                    Logger.EmptyLine();
                    Logger.WriteLine("File {0} already exists.", output);
                    Logger.WriteLine("Use forceNew=true to overwrite and get new.");
                }
            }

            wc.Dispose();

        }


        public List<Location> ReadLocationData()
        {

            string input = Path.Combine(Config.ResourceDirectory(), String.Format("Locations_Extended.json"));

            string s = File.ReadAllText(input);

            var locationDetails = JsonConvert.DeserializeObject<CareExtended_GetData>(s);

            return locationDetails.locations.location;
        }

        public void RetrieveLocationExtendedData(bool forceNew = false)
        {
            List<Care_Locations> locationLists = ReadLocationLists<Care_Locations>(); 

            string url = "http://www.kiesbeter.nl/open-data/api/care/getdata/{0}/{1}/all/?apikey={2}";

            string output = Path.Combine(Config.ResourceDirectory(), String.Format("Locations_Extended.json"));

            if (!File.Exists(output) || forceNew)
            {
                CareExtended_GetData careextended_getdata = new CareExtended_GetData();

                WebClient wc = new WebClient();
                wc.Encoding = Encoding.UTF8;

                var total = locationLists.Sum(ll => ll.locations.location.Count());
                var current = 0;

                foreach (var care_locations in locationLists)
                {
                    foreach (var location in care_locations.locations.location)
                    {
                        try
                        {
                            Logger.WriteLine("");
                            Logger.WriteLine("Getting locationdetails [{0}/{1}]", current++, total);
                            Logger.WriteLine("for #{0} {1}", location.id, location.name);
                            string s = wc.DownloadString(String.Format(url, location.careprovider_id, location.id, Config.ApiKey()));
                            var locationextended = JsonConvert.DeserializeObject<Care_GetData>(s);

                            // Pass these values from the original records, since we didn't get them from 'getdata' (or they have a different name id = @id)
                            locationextended.concerns.location.id = location.id;
                            locationextended.concerns.location.isconcern = location.isconcern;

                            careextended_getdata.locations.location.Add(locationextended.concerns.location);
                            Logger.WriteLine("Got locationdetails for id: {0}, {1}", location.id, location.name);

                            //if (current > 30) break;

                        }
                        catch (Exception ex)
                        {
                            Logger.WriteLine("Error while retrieving locationdetails for {0}", location.name);
                            Logger.WriteLine(ex.Message);
                        }
                    }
                }
                wc.Dispose();

                try
                {
                    string s = JsonConvert.SerializeObject(careextended_getdata, Formatting.Indented);
                    File.WriteAllText(Path.Combine(Config.ResourceDirectory(), output), s);
                    Logger.EmptyLine();
                    Logger.WriteLine("Written {0} locations with extended data.", careextended_getdata.locations.location.Count());
                }
                catch (UnauthorizedAccessException uex)
                {
                    Logger.EmptyLine();
                    Logger.WriteLine("File {0} is readonly!", output);
                    Logger.WriteLine("Probably it isn't checked-out from TFS.");
                }
                catch (Exception ex)
                {
                    Logger.EmptyLine();
                    Logger.WriteLine("Error while retrieving data for {0}", output);
                    Logger.WriteLine(ex.Message);
                }
            }
            else
            {
                Logger.EmptyLine();
                Logger.WriteLine("File {0} already exists.", output);
                Logger.WriteLine("Use forceNew=true to overwrite and get new.");
            }
        }



    }
}
