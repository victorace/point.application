﻿using System;
using System.Collections.Generic;

namespace KiesBeterConsole.Models.Kiesbeter
{

    /// http://www.kiesbeter.nl/open-data/api/care/locations/[CareproviderID]/?apikey=[APIKEY]
    /// careprovider_id:
    ///     4: Thuiszorg
    ///     5: Verpleging- en verzorgingshuis
    ///     all can be retrieved from: http://www.kiesbeter.nl/open-data/api/care/careproviders/?apikey=[APIKEY]    
    public class Care_Locations 
    {
        public Locations locations = new Locations();
    }

    public class Locations
    {
        public List<Location> location = new List<Location>();
    }


    /// http://www.kiesbeter.nl/open-data/api/care/getdata/[CareproviderID]/[LocationID]/all/?apikey=[APIKEY]
    public class Care_GetData
    {
        public Concerns concerns { get; set; }
    }

    public class Concerns
    {
        public Location location { get; set; }
    }

    public class CareExtended_GetData
    {
        public Locations locations = new Locations();
    }

    public class Location
    {
        public string careprovider_id { get; set; }
        public string concern_id { get; set; }
        public string id { get; set; }
        public string friendly_name { get; set; }
        public string servicearea { get; set; }
        public string name { get; set; }
        public string street { get; set; }
        public string house_number { get; set; }
        public string zip { get; set; }
        public string letters { get; set; }
        public string city { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public string website { get; set; }
        public string vision { get; set; }
        public string description { get; set; }
        public DateTime last_updated { get; set; }
        public string prefix { get; set; }
        public string algemeen { get; set; }
        public string isconcern { get; set; }
        public Concern concern { get; set; }
        public bool foundmatch { get; set; }
    }

    public class Concern
    {
        public string id { get; set; }
        public string name { get; set; }
    }



}



