﻿using KiesBeterConsole.Models.Point;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace KiesBeterConsole.Models.Context
{

    public class PointContext : DbContext
    {
        public PointContext() : base("Name=POINT") { }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            Database.SetInitializer<PointContext>(null); //Set to null, to "ignore" the existing database-scheme
        }

        public DbSet<OrganizationCare> OrganizationCare { get; set; }
        public DbSet<LocationCare> LocationCare { get; set; }
        public DbSet<Region> Region { get; set; }
        public DbSet<Organization> Organization { get; set; }
        public DbSet<Location> Location { get; set; }
        public DbSet<Department> Department { get; set; }
        
    }

}
