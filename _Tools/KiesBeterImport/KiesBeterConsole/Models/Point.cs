﻿using System;

namespace KiesBeterConsole.Models.Point
{

    public class CareFull
    {
        public OrganizationCare OrganizationCare { get; set; }
        public LocationCare LocationCare { get; set; }
        public Region Region { get; set; }
        public Organization Organization { get; set; }
        public Location Location { get; set; }
        public Department Department { get; set; }
    }

    public class OrganizationCare
    {
        public int OrganizationCareID { get; set; } //(int, not null)
        public string Name { get; set; } //(varchar(100), not null)
    }

    public class LocationCare
    {
        public int LocationCareID { get; set; } //(int, not null)
        public int OrganizationCareID { get; set; } //(int, not null)
        public string Name { get; set; } //(varchar(100), not null)
    }

    public class Region
    {
        public int RegionID { get; set; } //(int, not null)
        public string Name { get; set; } //(varchar(255), null)
        public int? ConnectionZorgkantoorNumber { get; set; } //(int, null)
        public string AGBCodeZorgkantoor { get; set; } //(varchar(20), null)
        public int? RegionType { get; set; } //(int, null)
    }


    public class Organization
    {
        public int OrganizationID { get; set; } //(int, not null)
        public string Name { get; set; } //(varchar(500), null)
        public string NameBackup { get; set; } //(varchar(500), null)
        public int OrganizationTypeID { get; set; } //(int, not null)
        public int? LocationCareID { get; set; } //(int, null)
        public string DepartmentCareName { get; set; } //(varchar(100), null)
        public bool? Inactive { get; set; } //(bit, null)
        public bool? HL7Interface { get; set; } //(bit, null)
        public bool? WSInterface { get; set; } //(bit, null)
        public int? InterfaceNumber { get; set; } //(int, null)
        public int? RegionID { get; set; } //(int, null)
        public int? ChangePasswordPeriod { get; set; } //(int, null)
        public bool? ShortConnectionID { get; set; } //(bit, null)
        public string AGB { get; set; } //(varchar(20), null)
        public string URL { get; set; } //(varchar(255), null)
        public bool? ConnectionZorgkantoor { get; set; } //(bit, null)
        public bool? HL7InterfaceExtraFields { get; set; } //(bit, null)
        public int? OrganizationSubType { get; set; } //(int, null)
        public bool? HL7ConnectionDifferentMenu { get; set; } //(bit, null)
        public bool? HL7ConnectionCallFromPoint { get; set; } //(bit, null)
        public bool UsesRadius { get; set; } //(bit, not null)
        public bool? UsesPatientPortal { get; set; } //(bit, null)
        public bool? ShowUnloadMessage { get; set; } //(bit, null)
        public bool? ButtonRedirectOrganization { get; set; } //(bit, null)
        public string ButtonRedirectOrganizationName { get; set; } //(varchar(25), null)
        public string URLRedirectOrganization { get; set; } //(varchar(255), null)
        public bool? Image { get; set; } //(bit, null)
        public string ImageName { get; set; } //(varchar(50), null)
        public bool? SSO { get; set; } //(bit, null)
        public string SSOKey { get; set; } //(varchar(50), null)
        public DateTime? TimeStamp { get; set; } //(datetime, null)
        public int? EmployeeID { get; set; } //(int, null)
        public int? ScreenID { get; set; } //(int, null)
        public bool? MessageToGPByClose { get; set; } //(bit, null)
        public bool? MessageToGPByDefinitive { get; set; } //(bit, null)
        public string ZorgmailUsername { get; set; } //(varchar(255), null)
        public string ZorgmailPassword { get; set; } //(varchar(255), null)
    }

    public class Location
    {
        public int LocationID { get; set; } //(int, not null)
        public int OrganizationID { get; set; } //(int, not null)
        public string Name { get; set; } //(varchar(255), not null)
        public string NameBackup { get; set; } //(varchar(255), null)
        public string StreetName { get; set; } //(varchar(255), null)
        public string Number { get; set; } //(varchar(50), null)
        public string Country { get; set; } //(varchar(255), null)
        public string PostalCode { get; set; } //(char(6), null)
        public string City { get; set; } //(varchar(255), null)
        public int? TransferPointID { get; set; } //(int, null)
        public bool? Inactive { get; set; } //(bit, null)
        public string PhoneNumber { get; set; } //(varchar(50), null)
        public string FaxNumber { get; set; } //(varchar(50), null)
        public string EmailAddress { get; set; } //(varchar(255), null)
        public DateTime? CapacityNotAvailbleTill { get; set; } //(datetime, null)
        public bool RecieveEmail { get; set; } //(bit, not null)
        public bool Capacity { get; set; } //(bit, not null)
        public int NumberPlaces { get; set; } //(int, not null)
        public string CapacityInfo { get; set; } //(varchar(255), null)
        public DateTime? CapacityInfoChanged { get; set; } //(datetime, null)
        public bool? IPCheck { get; set; } //(bit, null)
        public string IPAddress { get; set; } //(varchar(2000), null)
        public string Area { get; set; } //(varchar(255), null)
        public DateTime? TimeStamp { get; set; } //(datetime, null)
        public int? EmployeeID { get; set; } //(int, null)
        public int? ScreenID { get; set; } //(int, null)
    }

    public class Department
    {
        public int DepartmentID { get; set; } //(int, not null)
        public int LocationID { get; set; } //(int, not null)
        public int DepartmentTypeID { get; set; } //(int, not null)
        public int SpecialismID { get; set; } //(int, not null)
        public string Name { get; set; } //(varchar(255), not null)
        public string NameBackup { get; set; } //(varchar(255), null)
        public string PhoneNumber { get; set; } //(varchar(50), null)
        public string FaxNumber { get; set; } //(varchar(50), null)
        public string EmailAddress { get; set; } //(varchar(255), null)
        public bool RecieveEmail { get; set; } //(bit, not null)
        public int? MainContactID { get; set; } //(int, null)
        public bool Inactive { get; set; } //(bit, null)
        public int? SpecialismID2 { get; set; } //(int, null)
        public bool? PointParticipator { get; set; } //(bit, null)
        public bool? RehabilitationCenter { get; set; } //(bit, null)
        public bool? IntensiveRehabilitationNursinghome { get; set; } //(bit, null)
        public string EmailAddressTransferSend { get; set; } //(varchar(255), null)
        public bool? RecieveEmailTransferSend { get; set; } //(bit, null)
        public string ZorgmailTransferSend { get; set; } //(varchar(255), null)
        public bool? RecieveZorgmailTransferSend { get; set; } //(bit, null)
        public DateTime? TimeStamp { get; set; } //(datetime, null)
        public int? EmployeeID { get; set; } //(int, null)
        public int? ScreenID { get; set; } //(int, null)
    }




}
