﻿using System;

namespace KiesBeterConsole.Models.Excel
{

    public class ExcelColumn
    {
        public int RowNum { get; set; }
        public string FoundNotFound { get; set; }
        public string MatchNoMatch { get; set; }

        public int OrgaCareID { get; set; }
        public int LocaCareID { get; set; }
        public int OrgaID { get; set; }
        public int LocaID { get; set; }
        public string DepName { get; set; }
    }

}
