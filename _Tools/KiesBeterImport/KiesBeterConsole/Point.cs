﻿using KiesBeterConsole.Models.Context;
using KiesBeterConsole.Models.Point;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace KiesBeterConsole
{
    public class Point
    {

        public IEnumerable<CareFull> GetLocations()
        {
            //Get the locationdata (and all that's attached) from the POINT-database
            var context = new PointContext();

            IEnumerable<CareFull> pointLocations = (from oc in context.OrganizationCare
                                                    join lc in context.LocationCare on oc.OrganizationCareID equals lc.OrganizationCareID
                                                    join o in context.Organization on lc.LocationCareID equals o.LocationCareID
                                                        where o.OrganizationTypeID == 2 && o.LocationCareID != null
                                                    join r in context.Region on o.RegionID equals r.RegionID 
                                                    join l in context.Location on o.OrganizationID equals l.OrganizationID
                                                        where l.Inactive == false
                                                    join d in context.Department on l.LocationID equals d.LocationID
                                                    select new CareFull
                                                    {
                                                        OrganizationCare = oc,
                                                        LocationCare = lc,
                                                        Region = r,
                                                        Organization = o,
                                                        Location = l,
                                                        Department = d
                                                    }).ToList(); // ToList() to "force" it to a new instance and close the dbContext

            context.Dispose();

            Logger.EmptyLine();
            Logger.WriteLine("Read {0} active location from POINT-database.", pointLocations.Count());

            return pointLocations;
        }
    }
}
