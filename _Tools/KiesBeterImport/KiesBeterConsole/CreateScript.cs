﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System.IO;
using NPOI.XSSF.UserModel;
using KiesBeterConsole.Models.Excel;


namespace KiesBeterConsole
{

    class CreateScript
    {

        static char[] colFoundNotFound = { 'B' };
        static char[] colMatchNoMatch = { 'C' };
        static char[] colCheckOrg = { 'E' };
        static char[] colOrgaCareID = { 'F' };
        static char[] colLocaCareID = { 'H' };
        static char[] colRegion = { 'J' };
        static char[] colOrgaID = { 'K' };
        static char[] colOrgaName = { 'L' };
        static char[] colLocaId = { 'M' };
        static char[] colDepName = { 'S' };
        static char[] colKBConcernName = { 'V' };
        static char[] colKBName = { 'W' };
        static char[] colKBCity = { 'X' };
        static char[] colKBStreet = { 'Y' };
        static char[] colKBHouseNumber = { 'Z' };
        static char[] colKBZip = { 'A', 'A' };


        static List<string> statusList = new List<string>();
        static List<ExcelColumn> cacheList = new List<ExcelColumn>();
        static List<string> insertList = new List<string>();


        public static void Main(string[] args)
        {
            Logger.WriteLine("CreateScript started...");

            if (File.Exists(Path.Combine(Config.ResourceDirectory(), "Inserting.txt"))) File.Delete(Path.Combine(Config.ResourceDirectory(), "Inserting.txt"));
            if (File.Exists(Path.Combine(Config.ResourceDirectory(), "Insert.sql"))) File.Delete(Path.Combine(Config.ResourceDirectory(), "Insert.sql"));
            if (File.Exists(Path.Combine(Config.ResourceDirectory(), "Removing.txt"))) File.Delete(Path.Combine(Config.ResourceDirectory(), "Removing.txt"));
            if (File.Exists(Path.Combine(Config.ResourceDirectory(), "Remove.sql"))) File.Delete(Path.Combine(Config.ResourceDirectory(), "Remove.sql"));
            if (File.Exists(Path.Combine(Config.ResourceDirectory(), "Status.txt"))) File.Delete(Path.Combine(Config.ResourceDirectory(), "Status.txt"));

            ReadExcel();

            // Done ...
            Logger.DoneAndWait();

        }


        static void ReadExcel()
        {


            XSSFWorkbook workbook;
            using (FileStream file = new FileStream(Path.Combine(Config.ResourceDirectory(), "ZALijst_21052014.xlsx"), FileMode.Open, FileAccess.Read))
            {
                workbook = new XSSFWorkbook(file);
            }
            ISheet sheet = workbook.GetSheetAt(0);

            Logger.WriteLine("Excelsheet read, {0} rows.", sheet.LastRowNum);

            //if (!ValidateData(sheet)) return;

            FillCacheList(sheet);

            for (int rownum = 1; rownum <= sheet.LastRowNum; rownum++)
            {

                IRow row = sheet.GetRow(rownum);
                if (row != null)
                {
                    if (row.IsEmpty(colFoundNotFound)) // No status from POINT, most likely a KiesBeter-record
                    {
                        if (row.IsEmpty(colMatchNoMatch))
                            ErrorRow(row, rownum);
                        else if (row.GetValue(colMatchNoMatch) == "MATCH") // ***
                            SkippingOrganization(row, rownum);
                        else if (row.GetValue(colMatchNoMatch) == "NO MATCH") // ***
                            InsertOrganization(row, rownum, sheet);
                        else
                            ErrorRow(row, rownum);
                    }
                    else if (row.GetValue(colFoundNotFound) == "FOUND") // Found in POINT, so keep it and "ignore" any status for MATCH/NO MATCH
                    {
                        if (row.IsEmpty(colMatchNoMatch)) // ***
                            SkippingOrganization(row, rownum);
                        else if (row.GetValue(colMatchNoMatch) == "MATCH") // *** 
                            SkippingOrganization(row, rownum);
                        else if (row.GetValue(colMatchNoMatch) == "NO MATCH")
                            SkippingOrganization(row, rownum);
                        else
                            ErrorRow(row, rownum);
                    }
                    else if (row.GetValue(colFoundNotFound) == "NOT FOUND") // Record in POINT, but marked as NOT FOUND so removing the organization (setting it to InActive)
                    {
                        if (row.IsEmpty(colMatchNoMatch)) // ***
                            RemoveOrganization(row, rownum, sheet);
                        else if (row.GetValue(colMatchNoMatch) == "MATCH") // *** 
                        {
                            if (!row.IsEmpty(colOrgaID) && !row.IsEmpty(colKBConcernName)) // Marked for removing, but have a match in KiesBeter. Keeping it.
                                SkippingOrganization(row, rownum);
                            else if (row.IsEmpty(colOrgaID) && !row.IsEmpty(colKBConcernName)) // Marked for removing, but no record in POINT. Insert the KiesBeter-one
                                InsertOrganization(row, rownum, sheet);
                            else
                                ErrorRow(row, rownum);
                        }
                        else if (row.GetValue(colMatchNoMatch) == "NO MATCH")
                            RemoveOrganization(row, rownum, sheet);
                        else
                            ErrorRow(row, rownum);
                    }
                    else
                    {
                        ErrorRow(row, rownum);
                    }

                }
            }


            File.WriteAllLines(Path.Combine(Config.ResourceDirectory(), "Status.txt"), statusList);

            if (statusList.Count != sheet.LastRowNum)
                Logger.WriteLine("WARNING: number of lines in 'Status.txt' {0} does not match the sheet {1}", statusList.Count, sheet.LastRowNum);
            else
                Logger.WriteLine("Handled {0} lines. Manually copy contents of 'Status.txt' to the excelsheet", statusList.Count);




        }

        static void FillCacheList(ISheet sheet)
        {
            Logger.WriteLine("Filling CacheList");

            for (int rownum = 1; rownum <= sheet.LastRowNum; rownum++)
            {

                IRow row = sheet.GetRow(rownum);
                if (row != null)
                {
                    cacheList.Add(new ExcelColumn()
                    {
                        DepName = row.GetValue(colDepName),
                        FoundNotFound = row.GetValue(colFoundNotFound),
                        LocaCareID = row.GetIntValue(colLocaCareID),
                        LocaID = row.GetIntValue(colLocaId),
                        MatchNoMatch = row.GetValue(colMatchNoMatch),
                        OrgaCareID = row.GetIntValue(colOrgaCareID),
                        OrgaID = row.GetIntValue(colOrgaID),
                        RowNum = rownum
                    });
                }
            }
        }

        static bool ValidateData(ISheet sheet)
        {
            int tmpCount = 0;

            Logger.WriteLine("Checking if importtype can be determined, At least one column ({0}/{1}) should be filled.", colFoundNotFound, colMatchNoMatch);
            tmpCount = 0;
            for (int row = 1; row <= sheet.LastRowNum; row++)
            {
                if (sheet.IsEmpty(row, colFoundNotFound) && sheet.IsEmpty(row, colMatchNoMatch))
                {
                    Logger.WriteLine("{0}: {1} - {2}", row, sheet.GetValue(row, colFoundNotFound), sheet.GetValue(row, colMatchNoMatch));
                    tmpCount++;
                }
            }
            if (tmpCount > 0) return false;

            //List<string> combilist = new List<string>();
            //{
            //    for (int row = 1; row <= sheet.LastRowNum; row++)
            //    {
            //        string combi = String.Format("[{0}] - [{1}]", sheet.GetValue(row, colFoundNotFound), sheet.GetValue(row, colMatchNoMatch));
            //        if (combilist.IndexOf(combi) < 0)
            //            combilist.Add(combi);
            //    }
            //}
            //Logger.WriteLine("Found the following combinations of F/NF - M/NM:");
            //foreach(string combi in combilist)
            //    Logger.WriteLine(combi);

            return true;

        }

        static void RemoveOrganization(IRow row, int rownum, ISheet sheet)
        {
            int orgaid = Int32.Parse(row.GetValue(colOrgaID));
            string depname = row.GetValue(colDepName);

            bool recordwithstatusfound = false;

            var matchList = cacheList.Where(c => c.OrgaID == orgaid && c.DepName == depname);
            if (matchList.Count() > 1) //Got more than one record with the same orga/department
                recordwithstatusfound = matchList.Count(c => c.FoundNotFound == "FOUND") >= 1; //Got a 'FOUND' in one of them, so keeping the record

            if (recordwithstatusfound)
            {
                Logger.WriteLine("Duplicate @Line {0}: {3}", (rownum + 1), row.GetValue(colFoundNotFound), row.GetValue(colMatchNoMatch), row.GetValue(colCheckOrg));
                statusList.Add("DUPLICATE - NOT REMOVING");
            }
            else
            {
                // And finally check if the organisation is in use

                int result = -1;
                using (KiesBeterConsole.Models.Context.PointContext context = new Models.Context.PointContext())
                {
                    string sql = String.Format("select count(*) as cnt from transfer with(nolock) where year(CreatedDate) >= 2014 and ( RequestDesiredOrganisationID1 = {0} or DischargeRealizedOrganizationID = {0} )", orgaid);
                    result = context.Database.SqlQuery<int>(sql).First();
                }
                if (result > 0)
                {
                    Logger.WriteLine("In use @Line {0}: {3}", (rownum + 1), row.GetValue(colFoundNotFound), row.GetValue(colMatchNoMatch), row.GetValue(colCheckOrg));
                    statusList.Add("IN USE - NOT REMOVING");
                }
                else
                {
                    Logger.WriteLine("Removing @Line {0}: {3}", (rownum + 1), row.GetValue(colFoundNotFound), row.GetValue(colMatchNoMatch), row.GetValue(colCheckOrg));
                    statusList.Add("REMOVING");
                    File.AppendAllText(Path.Combine(Config.ResourceDirectory(), "Removing.txt"),
                        String.Format("{0}\r\n          Regio: {1}\r\n          Excelregel: {2}\r\n\r\n", row.GetValue(colOrgaName), row.GetValue(colRegion), (rownum + 1)));

                    File.AppendAllText(Path.Combine(Config.ResourceDirectory(), "Remove.sql"), getDeleteScript(row));
                }
            }
        }


        static void InsertOrganization(IRow row, int rownum, ISheet sheet)
        {
            string fullname = row.GetValue(colKBConcernName) + " " + row.GetValue(colKBName);
            if (insertList.IndexOf(fullname) >= 0)
            {
                Logger.WriteLine("Duplicate @Line {0}: {3} ", (rownum + 1), row.GetValue(colFoundNotFound), row.GetValue(colMatchNoMatch), row.GetValue(colCheckOrg), fullname);
                statusList.Add("DUPLICATE - NOT INSERTING");
            }
            else
            {
                insertList.Add(fullname);
                Logger.WriteLine("Inserting @Line {0}: {3} ", (rownum + 1), row.GetValue(colFoundNotFound), row.GetValue(colMatchNoMatch), row.GetValue(colCheckOrg), fullname);
                statusList.Add("INSERTING");

                File.AppendAllText(Path.Combine(Config.ResourceDirectory(), "Inserting.txt"),
                    String.Format("{0}\r\n          Excelregel: {1}\r\n\r\n", fullname, (rownum + 1)));

                File.AppendAllText(Path.Combine(Config.ResourceDirectory(), "Insert.sql"), getInsertScript(row));

            }
        }

        static void SkippingOrganization(IRow row, int rownum)
        {
            //Logger.WriteLine("Skipping @Line {0}: {1} - {2} - {3}", (rownum+1),  row.GetValue(colFoundNotFound), row.GetValue(colMatchNoMatch), row.GetValue(colCheckOrg));
            statusList.Add("-");
        }

        static void ErrorRow(IRow row, int rownum)
        {
            Logger.WriteLine("ERROR! @Line {0}: {1} - {2} - {3}", (rownum + 1), row.GetValue(colFoundNotFound), row.GetValue(colMatchNoMatch), row.GetValue(colCheckOrg));
            statusList.Add("ERROR");
        }

        static string getDeleteScript(IRow row)
        {
            string script = @"
update department 
   set InActive = 1 
 where LocationID = {0}
   and Name = '{1}'

GO
";
            return String.Format(script, row.GetValueSQL(colLocaId), row.GetValueSQL(colDepName));
        }

        static string getInsertScript(IRow row)
        {
            string script = @"
declare @LocID INT = -1
declare @Now datetime = getdate()

exec InsertOrganizationCareFull 
     @OrganizationCareName = '{0}', 
     @OrganizationTypeID = 2, 
     @RegionID = 13, 
     @LocationCareName = '{1}', 
     @DepartmentCareName = '',
     @OrganizationCareID = -1,
     @LocationCareID = -1,
     @OrganizationID = -1,
     @LocationID = @LocID output,
     @DepartmentID = -1,
     @TimeStamp = @Now, 
     @EmployeeId = 3487, 
     @ScreenID = 51 

update Location 
   set StreetName = '{2}',
       Number = '{3}',
       PostalCode = '{4}',
       City = '{5}'
 where LocationID = @LocID

GO
";
            return String.Format(script, row.GetValueSQL(colKBConcernName), row.GetValueSQL(colKBName),
                                         row.GetValueSQL(colKBStreet), row.GetValueSQL(colKBHouseNumber), row.GetValueSQL(colKBZip), row.GetValueSQL(colKBCity));
        }


    }
}
