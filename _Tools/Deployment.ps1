 param (
    [string]$version = "v6.8.5.5",
    [string]$sourcepath = "C:\VSONLINE\POINT.Application\Development\2018T2",
    [string]$deploymentpath = "C:\Deployments",
    [Parameter(Mandatory=$false)][ValidateSet("true", "false")][string]$zip="true",
    [Parameter(Mandatory=$false)][ValidateSet("true", "false")][string]$backup="false" 
 )

Set-StrictMode -Version latest
$ErrorActionPreference = "Stop"

if ($sourcepath -match '.+?\\$')
{
    $sourcepath = $sourcepath.Substring(0, $sourcepath.Length-1)
}

if ($deploymentpath -match '.+?\\$')
{
    $deploymentpath = $deploymentpath.Substring(0, $deploymentpath.Length-1)
}

$deploymentpath = "$deploymentpath\$version"

###[Environment]::SetEnvironmentVariable("DeploymentPath", "$deploymentpath", "Process")
$env:DeploymentPath = "$deploymentpath"

write-host
write-host "SourcePath: $sourcepath" -ForegroundColor black -BackgroundColor green
write-host
write-host "DeploymentPath: $deploymentpath" -ForegroundColor black -BackgroundColor green
write-host
write-host  "Press any key to continue or CTRL+C to quit" -ForegroundColor white -BackgroundColor red
read-host


if (Test-Path -Path "$deploymentpath\*")
{
    if ($backup -eq "true") {
        Compress-Archive "$deploymentpath" -DestinationPath ("$deploymentpath" + "_" + (get-date -Format yyyyMMddHHmmss) + ".zip") | Out-Null
    }

    write-host "Are you Sure You Want To remove $deploymentpath and all its contents?" -ForegroundColor white -BackgroundColor red
    $prompt = Read-Host
    if ($prompt  -eq 'y') {
        Remove-Item "$deploymentpath" -Force -Recurse
    }
}

msbuild "$sourcepath\Deployment.proj" /t:Publish /fl

$services = @(
    @{Source="$sourcepath\Point.HL7.WindowsService.Review";Destination="$deploymentpath\windows hl7service\review"},
    @{Source="$sourcepath\Point.HL7.WindowsService.Test";Destination="$deploymentpath\windows hl7service\test"},
    @{Source="$sourcepath\Point.HL7.WindowsService.Acceptatie";Destination="$deploymentpath\windows hl7service\acceptatie"},
    @{Source="$sourcepath\Point.HL7.WindowsService.Productie";Destination="$deploymentpath\windows hl7service\productie"},
    @{Source="$sourcepath\Point.WindowsService.Review";Destination="$deploymentpath\windows service\review"},
    @{Source="$sourcepath\Point.WindowsService.Test";Destination="$deploymentpath\windows service\test"},
    @{Source="$sourcepath\Point.WindowsService.Acceptatie";Destination="$deploymentpath\windows service\acceptatie"},
    @{Source="$sourcepath\Point.WindowsService.Productie";Destination="$deploymentpath\windows service\productie"}
)

foreach ($service in $services)
{
    New-Item -ItemType Directory -Force -Path ( "{0}\unpacked" -f $service.Destination ) | Out-Null
    Copy-Item ( "{0}\bin\release\*.*" -f $service.Source ) -Destination ( "{0}\" -f $service.Destination ) -Recurse  | Out-Null
    Expand ( "{0}\cab1.cab" -f $service.Destination ) -F:* ( "{0}\unpacked" -f $service.Destination )  | Out-Null
}

if ($zip -eq "true") {
    if (Test-Path -Path "$deploymentpath.zip")
    {
        write-host "Are you Sure You Want To remove $deploymentpath.zip?" -ForegroundColor white -BackgroundColor red
        $prompt = Read-Host
        if ($prompt -eq 'y') {
            Remove-Item "$deploymentpath.zip" -Force -Recurse | Out-Null
        }
    }
    Compress-Archive "$deploymentpath" -DestinationPath ("$deploymentpath.zip") | Out-Null
}
